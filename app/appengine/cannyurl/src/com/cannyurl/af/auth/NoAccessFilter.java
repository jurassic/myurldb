package com.cannyurl.af.auth;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class NoAccessFilter implements Filter
{
    private static final Logger log = Logger.getLogger(NoAccessFilter.class.getName());

    // TBD
    private FilterConfig config = null;
    
    public NoAccessFilter()
    {
    }

    @Override
    public void destroy()
    {
        this.config = null;
    }

    @Override
    public void init(FilterConfig config) throws ServletException
    {
        this.config = config;
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res,
            FilterChain chain) throws IOException, ServletException
    {
        // No access
        boolean accessAllowed = false;
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "NoAccessFilter.doFilter(): accessAllowed = " + accessAllowed);

        // TBD
        // Redirect to the error or login page. ???
        config.getServletContext().getRequestDispatcher("/").forward(req, res);
    }
    
}
