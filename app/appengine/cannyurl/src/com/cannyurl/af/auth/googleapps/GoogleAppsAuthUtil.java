package com.cannyurl.af.auth.googleapps;

import java.util.logging.Logger;
import java.util.logging.Level;


public class GoogleAppsAuthUtil
{
    private static final Logger log = Logger.getLogger(GoogleAppsAuthUtil.class.getName());

    // Config keys...
    public static final String CONFIG_KEY_DEFAULT_APPSDOMAIN = "cannyurlapp.googleapps.default.appsdomain";
    public static final String CONFIG_KEY_LOGINREALM = "cannyurlapp.googleapps.loginrealm";
    public static final String CONFIG_KEY_DATAACCESS_SCOPES = "cannyurlapp.googleapps.dataaccess.scopes";

    // TBD: Is this taken care of by Google App Engine ???
    public static final String CONFIG_KEY_CONSUMERKEY = "cannyurlapp.googleapps.consumerkey";
    public static final String CONFIG_KEY_CONSUMERSECRET = "cannyurlapp.googleapps.consumersecret";
    // ....
    
    // Google Apps Integration points.
    // URLs. May need to include ${DOMAIN_NAME} if necessary.
    public static final String CONFIG_KEY_APPPAGE_SETUP = "cannyurlapp.googleapps.apppage.setup";
    public static final String CONFIG_KEY_APPPAGE_MANAGE = "cannyurlapp.googleapps.apppage.manage";
    public static final String CONFIG_KEY_APPPAGE_SUPPORT = "cannyurlapp.googleapps.apppage.support";
    public static final String CONFIG_KEY_APPPAGE_DELETION_POLICY = "cannyurlapp.googleapps.apppage.deletion_policy";
    public static final String CONFIG_KEY_APPPAGE_ADD_SEATS = "cannyurlapp.googleapps.apppage.add_seats";
    // For Google "universal nav bar" integration...
    public static final String CONFIG_KEY_APPPAGE_NAVLINK = "cannyurlapp.googleapps.apppage.navlink";
    // This is not exactly Google Apps integration point, but might be convenient to read it from the config...
    public static final String CONFIG_KEY_APPPAGE_LANDING_PAGE = "cannyurlapp.googleapps.apppage.landing_page";
    // Etc...
    // ...

    // ??? Requesting Apps Domain --> To be used as "federatedIdentity" arg? (stands for "hosted domain"?)
    public static final String PARAM_HD = "hd";
    // Callback param may be passed in by Google Apps when relevant (e.g., during the installation by a client).
    // The app needs to redirect to this callback URL when processing is done.
    public static final String PARAM_CALLBACK = "callback";
    // "callback" is such commonly used param (and hence potential for mixing different params).
    // Using "googleapps_callback" instead once Google Apps calls out app...
    public static final String PARAM_GOOGLEAPPS_CALLBACK = "googleapps_callback";
    // This is used (internally) to pass Google Apps domain in a GET request.
    public static final String PARAM_APPSDOMAIN = "appsdomain";
    // This needs to be added in certain API calls (to Google Apps) to indicate the requesting user (email).
    public static final String PARAM_OAUTH_RQUESTER_ID = "xoauth_requestor_id";
    // ...

    // ???
    public static final String REQUEST_ATTR_APPSDOMAIN = "com.cannyurl.af.auth.googleapps.sso.apps_domain";
    public static final String SESSION_ATTR_LOGINREALM = "com.cannyurl.af.auth.googleapps.sso.login_realm";
    // ...

    
    private GoogleAppsAuthUtil() {}

    
    // TBD:
    // ...

}
