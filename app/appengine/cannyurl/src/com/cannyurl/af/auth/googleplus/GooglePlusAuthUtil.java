package com.cannyurl.af.auth.googleplus;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.cannyurl.af.auth.common.OAuthConstants;


public class GooglePlusAuthUtil
{
    private static final Logger log = Logger.getLogger(GooglePlusAuthUtil.class.getName());

    // Constants
    private static final String CALLBACK_URL_OOB = "oob";    // Out of band.
    // ...

    public static final String CONFIG_KEY_APPLICATION_NAME = "cannyurlapp.googleplus.application.name";
    public static final String CONFIG_KEY_CLIENT_ID = "cannyurlapp.googleplus.clientid";
    public static final String CONFIG_KEY_CLIENT_SECRET = "cannyurlapp.googleplus.clientsecret";
    // public static final String CONFIG_KEY_DEFAULT_ACCESSTOKEN = "cannyurlapp.googleplus.default.accesstoken";
    // public static final String CONFIG_KEY_DEFAULT_ACCESSTOKENSECRET = "cannyurlapp.googleplus.default.accesstokensecret";
    public static final String CONFIG_KEY_CALLBACK_TOKENHANDLER_URLPATH = "cannyurlapp.googleplus.callback.tokenhandler.urlpath";
    public static final String CONFIG_KEY_CALLBACK_AUTHAJAX_URLPATH = "cannyurlapp.googleplus.callback.authajax.urlpath";
    // public static final String CONFIG_KEY_OAUTH_REDIRECTURLPATH = "cannyurlapp.googleplus.oauth.redirecturlpath";
    public static final String CONFIG_KEY_JAVASCRIPT_SIGNIN_CALLBACK = "cannyurlapp.googleplus.javascript.signin.callback";     // Javascript callback function name
    // ...
    
    public static final String PARAM_CALLBACK = OAuthConstants.PARAM_OAUTH_CALLBACK;
    public static final String PARAM_CONSUMER_KEY = OAuthConstants.PARAM_OAUTH_CONSUMER_KEY;
    public static final String PARAM_NONCE = OAuthConstants.PARAM_OAUTH_NONCE;
    public static final String PARAM_SIGNATURE = OAuthConstants.PARAM_OAUTH_SIGNATURE;
    public static final String PARAM_SIGNATURE_METHOD = OAuthConstants.PARAM_OAUTH_SIGNATURE_METHOD;
    public static final String PARAM_TIMESTAMP = OAuthConstants.PARAM_OAUTH_TIMESTAMP;
    public static final String PARAM_VERSION = OAuthConstants.PARAM_OAUTH_VERSION;
    public static final String PARAM_REQUEST_TOKEN = OAuthConstants.PARAM_OAUTH_REQUEST_TOKEN;
    public static final String PARAM_TOKEN_VERIFIER = OAuthConstants.PARAM_OAUTH_TOKEN_VERIFIER;
    // These are specific to "google+ signin", which is a variation of OAuth2....
    // public static final String PARAM_CLIENT_ID = "client_id";
    // public static final String PARAM_APPLICATION_NAME = "application_name";
    public static final String PARAM_ACCESS_TOKEN = "access_token";
    public static final String PARAM_REFRESH_TOKEN = "refresh_token";
    public static final String PARAM_CODE = "code";              // Equivalent to OAuth request token ???
    // ...

    // Always the same value for the google+ signin???
    public static final String DEFAULT_DATA_SCOPE = "https://www.googleapis.com/auth/plus.login";         // ???
    public static final String DEFAULT_DATA_REDIRECT_URL = "postmessage";                                 // ???
    public static final String DEFAULT_DATA_ACCESS_TYPE = "offline";                                      // ???
    public static final String DEFAULT_DATA_COOKIE_POLICY = "single_host_origin";                         // ???
    // ...

    // To be used for generating session attribute name for csrf state...
    public static final String DEFAULT_FORM_ID_GOOGLEPLUS_SIGNIN = "_googleplus_signin_form";         // ???
    // ...

    // "TokenResponse" object returned from Google+.
    public static final String SESSION_ATTR_TOKENRESPONSE = "com.cannyurl.af.auth.googleplus.tokenresponse";
    // ...
    
    private GooglePlusAuthUtil() {}

    
    // temporary
    public static String getDefaultGooglePlusSignInFormId()
    {
        return DEFAULT_FORM_ID_GOOGLEPLUS_SIGNIN;
    }

    // TBD:
    // ...
    
}
