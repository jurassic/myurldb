package com.cannyurl.af.auth.twitter;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.cannyurl.af.auth.common.OAuthConstants;


public class TwitterAuthUtil
{
    private static final Logger log = Logger.getLogger(TwitterAuthUtil.class.getName());

    // Constants
    private static final String CALLBACK_URL_OOB = "oob";    // Out of band.
    // ...

    public static final String CONFIG_KEY_CONSUMERKEY = "cannyurlapp.twitter.consumerkey";
    public static final String CONFIG_KEY_CONSUMERSECRET = "cannyurlapp.twitter.consumersecret";
    public static final String CONFIG_KEY_DEFAULT_ACCESSTOKEN = "cannyurlapp.twitter.default.accesstoken";
    public static final String CONFIG_KEY_DEFAULT_ACCESSTOKENSECRET = "cannyurlapp.twitter.default.accesstokensecret";
    public static final String CONFIG_KEY_OAUTH_REDIRECTURLPATH = "cannyurlapp.twitter.oauth.redirecturlpath";
    // public static final String CONFIG_KEY_REQUESTTOKEN_REDIRECTURLPATH = "cannyurlapp.twitter.requesttoken.redirecturlpath";
    // public static final String CONFIG_KEY_ACCESSTOKEN_REDIRECTURLPATH = "cannyurlapp.twitter.accesstoken.redirecturlpath"; 
    // ...
    
    public static final String PARAM_CALLBACK = OAuthConstants.PARAM_OAUTH_CALLBACK;
    public static final String PARAM_CONSUMER_KEY = OAuthConstants.PARAM_OAUTH_CONSUMER_KEY;
    public static final String PARAM_NONCE = OAuthConstants.PARAM_OAUTH_NONCE;
    public static final String PARAM_SIGNATURE = OAuthConstants.PARAM_OAUTH_SIGNATURE;
    public static final String PARAM_SIGNATURE_METHOD = OAuthConstants.PARAM_OAUTH_SIGNATURE_METHOD;
    public static final String PARAM_TIMESTAMP = OAuthConstants.PARAM_OAUTH_TIMESTAMP;
    public static final String PARAM_VERSION = OAuthConstants.PARAM_OAUTH_VERSION;
    public static final String PARAM_REQUEST_TOKEN = OAuthConstants.PARAM_OAUTH_REQUEST_TOKEN;
    public static final String PARAM_TOKEN_VERIFIER = OAuthConstants.PARAM_OAUTH_TOKEN_VERIFIER;
    // ...
    
    public static final String BASE_URL_AUTHORIZE = "https://api.twitter.com/oauth/authorize";         // ???
    public static final String BASE_URL_AUTHENTICATE = "https://api.twitter.com/oauth/authenticate";   // ???
    public static final String BASE_URL_REQUEST_TOKEN_FETCH = "https://api.twitter.com/oauth/request_token";
    public static final String BASE_URL_ACCESS_TOKEN_EXCHANGE = "https://api.twitter.com/oauth/access_token";
    // ...

    public static final String SESSION_ATTR_REQUESTTOKEN = "com.cannyurl.af.auth.twitter.oauth.requesttoken";
    // ...
    
    private TwitterAuthUtil() {}

    
    // TBD:
    // ...
    
}
