package com.cannyurl.af.auth.user;

import java.util.logging.Logger;

import com.google.appengine.api.users.UserServiceFailureException;

public class AuthUserServiceFailureException extends UserServiceFailureException
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(AuthUserServiceFailureException.class.getName());

    public AuthUserServiceFailureException(String message)
    {
        super(message);
    }

}
