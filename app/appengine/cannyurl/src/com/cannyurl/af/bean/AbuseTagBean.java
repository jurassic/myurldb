package com.cannyurl.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.AbuseTag;
import com.myurldb.ws.stub.AbuseTagStub;


// Wrapper class + bean combo.
public class AbuseTagBean implements AbuseTag, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(AbuseTagBean.class.getName());

    // [1] With an embedded object.
    private AbuseTagStub stub = null;

    // [2] Or, without an embedded object.
    private String guid;
    private String shortLink;
    private String shortUrl;
    private String longUrl;
    private String longUrlHash;
    private String user;
    private String ipAddress;
    private Integer tag;
    private String comment;
    private String status;
    private String reviewer;
    private String action;
    private String note;
    private String pastActions;
    private Long reviewedTime;
    private Long actionTime;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public AbuseTagBean()
    {
        //this((String) null);
    }
    public AbuseTagBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public AbuseTagBean(String guid, String shortLink, String shortUrl, String longUrl, String longUrlHash, String user, String ipAddress, Integer tag, String comment, String status, String reviewer, String action, String note, String pastActions, Long reviewedTime, Long actionTime)
    {
        this(guid, shortLink, shortUrl, longUrl, longUrlHash, user, ipAddress, tag, comment, status, reviewer, action, note, pastActions, reviewedTime, actionTime, null, null);
    }
    public AbuseTagBean(String guid, String shortLink, String shortUrl, String longUrl, String longUrlHash, String user, String ipAddress, Integer tag, String comment, String status, String reviewer, String action, String note, String pastActions, Long reviewedTime, Long actionTime, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.shortLink = shortLink;
        this.shortUrl = shortUrl;
        this.longUrl = longUrl;
        this.longUrlHash = longUrlHash;
        this.user = user;
        this.ipAddress = ipAddress;
        this.tag = tag;
        this.comment = comment;
        this.status = status;
        this.reviewer = reviewer;
        this.action = action;
        this.note = note;
        this.pastActions = pastActions;
        this.reviewedTime = reviewedTime;
        this.actionTime = actionTime;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public AbuseTagBean(AbuseTag stub)
    {
        if(stub instanceof AbuseTagStub) {
            this.stub = (AbuseTagStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setShortLink(stub.getShortLink());   
            setShortUrl(stub.getShortUrl());   
            setLongUrl(stub.getLongUrl());   
            setLongUrlHash(stub.getLongUrlHash());   
            setUser(stub.getUser());   
            setIpAddress(stub.getIpAddress());   
            setTag(stub.getTag());   
            setComment(stub.getComment());   
            setStatus(stub.getStatus());   
            setReviewer(stub.getReviewer());   
            setAction(stub.getAction());   
            setNote(stub.getNote());   
            setPastActions(stub.getPastActions());   
            setReviewedTime(stub.getReviewedTime());   
            setActionTime(stub.getActionTime());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public String getShortLink()
    {
        if(getStub() != null) {
            return getStub().getShortLink();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.shortLink;
        }
    }
    public void setShortLink(String shortLink)
    {
        if(getStub() != null) {
            getStub().setShortLink(shortLink);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.shortLink = shortLink;
        }
    }

    public String getShortUrl()
    {
        if(getStub() != null) {
            return getStub().getShortUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.shortUrl;
        }
    }
    public void setShortUrl(String shortUrl)
    {
        if(getStub() != null) {
            getStub().setShortUrl(shortUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.shortUrl = shortUrl;
        }
    }

    public String getLongUrl()
    {
        if(getStub() != null) {
            return getStub().getLongUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.longUrl;
        }
    }
    public void setLongUrl(String longUrl)
    {
        if(getStub() != null) {
            getStub().setLongUrl(longUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.longUrl = longUrl;
        }
    }

    public String getLongUrlHash()
    {
        if(getStub() != null) {
            return getStub().getLongUrlHash();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.longUrlHash;
        }
    }
    public void setLongUrlHash(String longUrlHash)
    {
        if(getStub() != null) {
            getStub().setLongUrlHash(longUrlHash);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.longUrlHash = longUrlHash;
        }
    }

    public String getUser()
    {
        if(getStub() != null) {
            return getStub().getUser();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.user;
        }
    }
    public void setUser(String user)
    {
        if(getStub() != null) {
            getStub().setUser(user);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.user = user;
        }
    }

    public String getIpAddress()
    {
        if(getStub() != null) {
            return getStub().getIpAddress();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.ipAddress;
        }
    }
    public void setIpAddress(String ipAddress)
    {
        if(getStub() != null) {
            getStub().setIpAddress(ipAddress);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.ipAddress = ipAddress;
        }
    }

    public Integer getTag()
    {
        if(getStub() != null) {
            return getStub().getTag();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.tag;
        }
    }
    public void setTag(Integer tag)
    {
        if(getStub() != null) {
            getStub().setTag(tag);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.tag = tag;
        }
    }

    public String getComment()
    {
        if(getStub() != null) {
            return getStub().getComment();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.comment;
        }
    }
    public void setComment(String comment)
    {
        if(getStub() != null) {
            getStub().setComment(comment);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.comment = comment;
        }
    }

    public String getStatus()
    {
        if(getStub() != null) {
            return getStub().getStatus();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.status;
        }
    }
    public void setStatus(String status)
    {
        if(getStub() != null) {
            getStub().setStatus(status);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.status = status;
        }
    }

    public String getReviewer()
    {
        if(getStub() != null) {
            return getStub().getReviewer();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.reviewer;
        }
    }
    public void setReviewer(String reviewer)
    {
        if(getStub() != null) {
            getStub().setReviewer(reviewer);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.reviewer = reviewer;
        }
    }

    public String getAction()
    {
        if(getStub() != null) {
            return getStub().getAction();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.action;
        }
    }
    public void setAction(String action)
    {
        if(getStub() != null) {
            getStub().setAction(action);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.action = action;
        }
    }

    public String getNote()
    {
        if(getStub() != null) {
            return getStub().getNote();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.note;
        }
    }
    public void setNote(String note)
    {
        if(getStub() != null) {
            getStub().setNote(note);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.note = note;
        }
    }

    public String getPastActions()
    {
        if(getStub() != null) {
            return getStub().getPastActions();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.pastActions;
        }
    }
    public void setPastActions(String pastActions)
    {
        if(getStub() != null) {
            getStub().setPastActions(pastActions);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.pastActions = pastActions;
        }
    }

    public Long getReviewedTime()
    {
        if(getStub() != null) {
            return getStub().getReviewedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.reviewedTime;
        }
    }
    public void setReviewedTime(Long reviewedTime)
    {
        if(getStub() != null) {
            getStub().setReviewedTime(reviewedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.reviewedTime = reviewedTime;
        }
    }

    public Long getActionTime()
    {
        if(getStub() != null) {
            return getStub().getActionTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.actionTime;
        }
    }
    public void setActionTime(Long actionTime)
    {
        if(getStub() != null) {
            getStub().setActionTime(actionTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.actionTime = actionTime;
        }
    }

    public Long getCreatedTime()
    {
        if(getStub() != null) {
            return getStub().getCreatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.createdTime;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getStub() != null) {
            getStub().setCreatedTime(createdTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.createdTime = createdTime;
        }
    }

    public Long getModifiedTime()
    {
        if(getStub() != null) {
            return getStub().getModifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.modifiedTime;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getStub() != null) {
            getStub().setModifiedTime(modifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.modifiedTime = modifiedTime;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public AbuseTagStub getStub()
    {
        return this.stub;
    }
    protected void setStub(AbuseTagStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("guid = " + this.guid).append(";");
            sb.append("shortLink = " + this.shortLink).append(";");
            sb.append("shortUrl = " + this.shortUrl).append(";");
            sb.append("longUrl = " + this.longUrl).append(";");
            sb.append("longUrlHash = " + this.longUrlHash).append(";");
            sb.append("user = " + this.user).append(";");
            sb.append("ipAddress = " + this.ipAddress).append(";");
            sb.append("tag = " + this.tag).append(";");
            sb.append("comment = " + this.comment).append(";");
            sb.append("status = " + this.status).append(";");
            sb.append("reviewer = " + this.reviewer).append(";");
            sb.append("action = " + this.action).append(";");
            sb.append("note = " + this.note).append(";");
            sb.append("pastActions = " + this.pastActions).append(";");
            sb.append("reviewedTime = " + this.reviewedTime).append(";");
            sb.append("actionTime = " + this.actionTime).append(";");
            sb.append("createdTime = " + this.createdTime).append(";");
            sb.append("modifiedTime = " + this.modifiedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = guid == null ? 0 : guid.hashCode();
            _hash = 31 * _hash + delta;
            delta = shortLink == null ? 0 : shortLink.hashCode();
            _hash = 31 * _hash + delta;
            delta = shortUrl == null ? 0 : shortUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = longUrl == null ? 0 : longUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = longUrlHash == null ? 0 : longUrlHash.hashCode();
            _hash = 31 * _hash + delta;
            delta = user == null ? 0 : user.hashCode();
            _hash = 31 * _hash + delta;
            delta = ipAddress == null ? 0 : ipAddress.hashCode();
            _hash = 31 * _hash + delta;
            delta = tag == null ? 0 : tag.hashCode();
            _hash = 31 * _hash + delta;
            delta = comment == null ? 0 : comment.hashCode();
            _hash = 31 * _hash + delta;
            delta = status == null ? 0 : status.hashCode();
            _hash = 31 * _hash + delta;
            delta = reviewer == null ? 0 : reviewer.hashCode();
            _hash = 31 * _hash + delta;
            delta = action == null ? 0 : action.hashCode();
            _hash = 31 * _hash + delta;
            delta = note == null ? 0 : note.hashCode();
            _hash = 31 * _hash + delta;
            delta = pastActions == null ? 0 : pastActions.hashCode();
            _hash = 31 * _hash + delta;
            delta = reviewedTime == null ? 0 : reviewedTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = actionTime == null ? 0 : actionTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = createdTime == null ? 0 : createdTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
