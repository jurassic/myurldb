package com.cannyurl.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.AlbumShortLink;
import com.myurldb.ws.stub.AlbumShortLinkStub;


// Wrapper class + bean combo.
public class AlbumShortLinkBean implements AlbumShortLink, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(AlbumShortLinkBean.class.getName());

    // [1] With an embedded object.
    private AlbumShortLinkStub stub = null;

    // [2] Or, without an embedded object.
    private String guid;
    private String user;
    private String linkAlbum;
    private String shortLink;
    private String shortUrl;
    private String longUrl;
    private String note;
    private String status;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public AlbumShortLinkBean()
    {
        //this((String) null);
    }
    public AlbumShortLinkBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null);
    }
    public AlbumShortLinkBean(String guid, String user, String linkAlbum, String shortLink, String shortUrl, String longUrl, String note, String status)
    {
        this(guid, user, linkAlbum, shortLink, shortUrl, longUrl, note, status, null, null);
    }
    public AlbumShortLinkBean(String guid, String user, String linkAlbum, String shortLink, String shortUrl, String longUrl, String note, String status, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.user = user;
        this.linkAlbum = linkAlbum;
        this.shortLink = shortLink;
        this.shortUrl = shortUrl;
        this.longUrl = longUrl;
        this.note = note;
        this.status = status;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public AlbumShortLinkBean(AlbumShortLink stub)
    {
        if(stub instanceof AlbumShortLinkStub) {
            this.stub = (AlbumShortLinkStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setUser(stub.getUser());   
            setLinkAlbum(stub.getLinkAlbum());   
            setShortLink(stub.getShortLink());   
            setShortUrl(stub.getShortUrl());   
            setLongUrl(stub.getLongUrl());   
            setNote(stub.getNote());   
            setStatus(stub.getStatus());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public String getUser()
    {
        if(getStub() != null) {
            return getStub().getUser();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.user;
        }
    }
    public void setUser(String user)
    {
        if(getStub() != null) {
            getStub().setUser(user);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.user = user;
        }
    }

    public String getLinkAlbum()
    {
        if(getStub() != null) {
            return getStub().getLinkAlbum();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.linkAlbum;
        }
    }
    public void setLinkAlbum(String linkAlbum)
    {
        if(getStub() != null) {
            getStub().setLinkAlbum(linkAlbum);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.linkAlbum = linkAlbum;
        }
    }

    public String getShortLink()
    {
        if(getStub() != null) {
            return getStub().getShortLink();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.shortLink;
        }
    }
    public void setShortLink(String shortLink)
    {
        if(getStub() != null) {
            getStub().setShortLink(shortLink);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.shortLink = shortLink;
        }
    }

    public String getShortUrl()
    {
        if(getStub() != null) {
            return getStub().getShortUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.shortUrl;
        }
    }
    public void setShortUrl(String shortUrl)
    {
        if(getStub() != null) {
            getStub().setShortUrl(shortUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.shortUrl = shortUrl;
        }
    }

    public String getLongUrl()
    {
        if(getStub() != null) {
            return getStub().getLongUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.longUrl;
        }
    }
    public void setLongUrl(String longUrl)
    {
        if(getStub() != null) {
            getStub().setLongUrl(longUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.longUrl = longUrl;
        }
    }

    public String getNote()
    {
        if(getStub() != null) {
            return getStub().getNote();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.note;
        }
    }
    public void setNote(String note)
    {
        if(getStub() != null) {
            getStub().setNote(note);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.note = note;
        }
    }

    public String getStatus()
    {
        if(getStub() != null) {
            return getStub().getStatus();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.status;
        }
    }
    public void setStatus(String status)
    {
        if(getStub() != null) {
            getStub().setStatus(status);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.status = status;
        }
    }

    public Long getCreatedTime()
    {
        if(getStub() != null) {
            return getStub().getCreatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.createdTime;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getStub() != null) {
            getStub().setCreatedTime(createdTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.createdTime = createdTime;
        }
    }

    public Long getModifiedTime()
    {
        if(getStub() != null) {
            return getStub().getModifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.modifiedTime;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getStub() != null) {
            getStub().setModifiedTime(modifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.modifiedTime = modifiedTime;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public AlbumShortLinkStub getStub()
    {
        return this.stub;
    }
    protected void setStub(AlbumShortLinkStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("guid = " + this.guid).append(";");
            sb.append("user = " + this.user).append(";");
            sb.append("linkAlbum = " + this.linkAlbum).append(";");
            sb.append("shortLink = " + this.shortLink).append(";");
            sb.append("shortUrl = " + this.shortUrl).append(";");
            sb.append("longUrl = " + this.longUrl).append(";");
            sb.append("note = " + this.note).append(";");
            sb.append("status = " + this.status).append(";");
            sb.append("createdTime = " + this.createdTime).append(";");
            sb.append("modifiedTime = " + this.modifiedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = guid == null ? 0 : guid.hashCode();
            _hash = 31 * _hash + delta;
            delta = user == null ? 0 : user.hashCode();
            _hash = 31 * _hash + delta;
            delta = linkAlbum == null ? 0 : linkAlbum.hashCode();
            _hash = 31 * _hash + delta;
            delta = shortLink == null ? 0 : shortLink.hashCode();
            _hash = 31 * _hash + delta;
            delta = shortUrl == null ? 0 : shortUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = longUrl == null ? 0 : longUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = note == null ? 0 : note.hashCode();
            _hash = 31 * _hash + delta;
            delta = status == null ? 0 : status.hashCode();
            _hash = 31 * _hash + delta;
            delta = createdTime == null ? 0 : createdTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
