package com.cannyurl.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.ApiConsumer;
import com.myurldb.ws.stub.ApiConsumerStub;


// Wrapper class + bean combo.
public class ApiConsumerBean implements ApiConsumer, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ApiConsumerBean.class.getName());

    // [1] With an embedded object.
    private ApiConsumerStub stub = null;

    // [2] Or, without an embedded object.
    private String guid;
    private String aeryId;
    private String name;
    private String description;
    private String appKey;
    private String appSecret;
    private String status;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public ApiConsumerBean()
    {
        //this((String) null);
    }
    public ApiConsumerBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null);
    }
    public ApiConsumerBean(String guid, String aeryId, String name, String description, String appKey, String appSecret, String status)
    {
        this(guid, aeryId, name, description, appKey, appSecret, status, null, null);
    }
    public ApiConsumerBean(String guid, String aeryId, String name, String description, String appKey, String appSecret, String status, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.aeryId = aeryId;
        this.name = name;
        this.description = description;
        this.appKey = appKey;
        this.appSecret = appSecret;
        this.status = status;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public ApiConsumerBean(ApiConsumer stub)
    {
        if(stub instanceof ApiConsumerStub) {
            this.stub = (ApiConsumerStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setAeryId(stub.getAeryId());   
            setName(stub.getName());   
            setDescription(stub.getDescription());   
            setAppKey(stub.getAppKey());   
            setAppSecret(stub.getAppSecret());   
            setStatus(stub.getStatus());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public String getAeryId()
    {
        if(getStub() != null) {
            return getStub().getAeryId();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.aeryId;
        }
    }
    public void setAeryId(String aeryId)
    {
        if(getStub() != null) {
            getStub().setAeryId(aeryId);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.aeryId = aeryId;
        }
    }

    public String getName()
    {
        if(getStub() != null) {
            return getStub().getName();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.name;
        }
    }
    public void setName(String name)
    {
        if(getStub() != null) {
            getStub().setName(name);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.name = name;
        }
    }

    public String getDescription()
    {
        if(getStub() != null) {
            return getStub().getDescription();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.description;
        }
    }
    public void setDescription(String description)
    {
        if(getStub() != null) {
            getStub().setDescription(description);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.description = description;
        }
    }

    public String getAppKey()
    {
        if(getStub() != null) {
            return getStub().getAppKey();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.appKey;
        }
    }
    public void setAppKey(String appKey)
    {
        if(getStub() != null) {
            getStub().setAppKey(appKey);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.appKey = appKey;
        }
    }

    public String getAppSecret()
    {
        if(getStub() != null) {
            return getStub().getAppSecret();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.appSecret;
        }
    }
    public void setAppSecret(String appSecret)
    {
        if(getStub() != null) {
            getStub().setAppSecret(appSecret);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.appSecret = appSecret;
        }
    }

    public String getStatus()
    {
        if(getStub() != null) {
            return getStub().getStatus();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.status;
        }
    }
    public void setStatus(String status)
    {
        if(getStub() != null) {
            getStub().setStatus(status);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.status = status;
        }
    }

    public Long getCreatedTime()
    {
        if(getStub() != null) {
            return getStub().getCreatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.createdTime;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getStub() != null) {
            getStub().setCreatedTime(createdTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.createdTime = createdTime;
        }
    }

    public Long getModifiedTime()
    {
        if(getStub() != null) {
            return getStub().getModifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.modifiedTime;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getStub() != null) {
            getStub().setModifiedTime(modifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.modifiedTime = modifiedTime;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public ApiConsumerStub getStub()
    {
        return this.stub;
    }
    protected void setStub(ApiConsumerStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("guid = " + this.guid).append(";");
            sb.append("aeryId = " + this.aeryId).append(";");
            sb.append("name = " + this.name).append(";");
            sb.append("description = " + this.description).append(";");
            sb.append("appKey = " + this.appKey).append(";");
            sb.append("appSecret = " + this.appSecret).append(";");
            sb.append("status = " + this.status).append(";");
            sb.append("createdTime = " + this.createdTime).append(";");
            sb.append("modifiedTime = " + this.modifiedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = guid == null ? 0 : guid.hashCode();
            _hash = 31 * _hash + delta;
            delta = aeryId == null ? 0 : aeryId.hashCode();
            _hash = 31 * _hash + delta;
            delta = name == null ? 0 : name.hashCode();
            _hash = 31 * _hash + delta;
            delta = description == null ? 0 : description.hashCode();
            _hash = 31 * _hash + delta;
            delta = appKey == null ? 0 : appKey.hashCode();
            _hash = 31 * _hash + delta;
            delta = appSecret == null ? 0 : appSecret.hashCode();
            _hash = 31 * _hash + delta;
            delta = status == null ? 0 : status.hashCode();
            _hash = 31 * _hash + delta;
            delta = createdTime == null ? 0 : createdTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
