package com.cannyurl.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.AppBrandStruct;
import com.myurldb.ws.stub.AppBrandStructStub;


// Wrapper class + bean combo.
public class AppBrandStructBean implements AppBrandStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(AppBrandStructBean.class.getName());

    // [1] With an embedded object.
    private AppBrandStructStub stub = null;

    // [2] Or, without an embedded object.
    private String brand;
    private String name;
    private String description;

    // Ctors.
    public AppBrandStructBean()
    {
        //this((String) null);
    }
    public AppBrandStructBean(String brand, String name, String description)
    {
        this.brand = brand;
        this.name = name;
        this.description = description;
    }
    public AppBrandStructBean(AppBrandStruct stub)
    {
        if(stub instanceof AppBrandStructStub) {
            this.stub = (AppBrandStructStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setBrand(stub.getBrand());   
            setName(stub.getName());   
            setDescription(stub.getDescription());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getBrand()
    {
        if(getStub() != null) {
            return getStub().getBrand();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.brand;
        }
    }
    public void setBrand(String brand)
    {
        if(getStub() != null) {
            getStub().setBrand(brand);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.brand = brand;
        }
    }

    public String getName()
    {
        if(getStub() != null) {
            return getStub().getName();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.name;
        }
    }
    public void setName(String name)
    {
        if(getStub() != null) {
            getStub().setName(name);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.name = name;
        }
    }

    public String getDescription()
    {
        if(getStub() != null) {
            return getStub().getDescription();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.description;
        }
    }
    public void setDescription(String description)
    {
        if(getStub() != null) {
            getStub().setDescription(description);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.description = description;
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getBrand() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getName() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getDescription() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // Returns the reference to the embedded object. (Could be null.)
    public AppBrandStructStub getStub()
    {
        return this.stub;
    }
    protected void setStub(AppBrandStructStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("brand = " + this.brand).append(";");
            sb.append("name = " + this.name).append(";");
            sb.append("description = " + this.description).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = brand == null ? 0 : brand.hashCode();
            _hash = 31 * _hash + delta;
            delta = name == null ? 0 : name.hashCode();
            _hash = 31 * _hash + delta;
            delta = description == null ? 0 : description.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
