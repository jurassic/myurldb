package com.cannyurl.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.AppClient;
import com.myurldb.ws.stub.AppClientStub;


// Wrapper class + bean combo.
public class AppClientBean implements AppClient, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(AppClientBean.class.getName());

    // [1] With an embedded object.
    private AppClientStub stub = null;

    // [2] Or, without an embedded object.
    private String guid;
    private String owner;
    private String admin;
    private String name;
    private String clientId;
    private String clientCode;
    private String rootDomain;
    private String appDomain;
    private Boolean useAppDomain;
    private Boolean enabled;
    private String licenseMode;
    private String status;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public AppClientBean()
    {
        //this((String) null);
    }
    public AppClientBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public AppClientBean(String guid, String owner, String admin, String name, String clientId, String clientCode, String rootDomain, String appDomain, Boolean useAppDomain, Boolean enabled, String licenseMode, String status)
    {
        this(guid, owner, admin, name, clientId, clientCode, rootDomain, appDomain, useAppDomain, enabled, licenseMode, status, null, null);
    }
    public AppClientBean(String guid, String owner, String admin, String name, String clientId, String clientCode, String rootDomain, String appDomain, Boolean useAppDomain, Boolean enabled, String licenseMode, String status, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.owner = owner;
        this.admin = admin;
        this.name = name;
        this.clientId = clientId;
        this.clientCode = clientCode;
        this.rootDomain = rootDomain;
        this.appDomain = appDomain;
        this.useAppDomain = useAppDomain;
        this.enabled = enabled;
        this.licenseMode = licenseMode;
        this.status = status;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public AppClientBean(AppClient stub)
    {
        if(stub instanceof AppClientStub) {
            this.stub = (AppClientStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setOwner(stub.getOwner());   
            setAdmin(stub.getAdmin());   
            setName(stub.getName());   
            setClientId(stub.getClientId());   
            setClientCode(stub.getClientCode());   
            setRootDomain(stub.getRootDomain());   
            setAppDomain(stub.getAppDomain());   
            setUseAppDomain(stub.isUseAppDomain());   
            setEnabled(stub.isEnabled());   
            setLicenseMode(stub.getLicenseMode());   
            setStatus(stub.getStatus());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public String getOwner()
    {
        if(getStub() != null) {
            return getStub().getOwner();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.owner;
        }
    }
    public void setOwner(String owner)
    {
        if(getStub() != null) {
            getStub().setOwner(owner);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.owner = owner;
        }
    }

    public String getAdmin()
    {
        if(getStub() != null) {
            return getStub().getAdmin();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.admin;
        }
    }
    public void setAdmin(String admin)
    {
        if(getStub() != null) {
            getStub().setAdmin(admin);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.admin = admin;
        }
    }

    public String getName()
    {
        if(getStub() != null) {
            return getStub().getName();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.name;
        }
    }
    public void setName(String name)
    {
        if(getStub() != null) {
            getStub().setName(name);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.name = name;
        }
    }

    public String getClientId()
    {
        if(getStub() != null) {
            return getStub().getClientId();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.clientId;
        }
    }
    public void setClientId(String clientId)
    {
        if(getStub() != null) {
            getStub().setClientId(clientId);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.clientId = clientId;
        }
    }

    public String getClientCode()
    {
        if(getStub() != null) {
            return getStub().getClientCode();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.clientCode;
        }
    }
    public void setClientCode(String clientCode)
    {
        if(getStub() != null) {
            getStub().setClientCode(clientCode);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.clientCode = clientCode;
        }
    }

    public String getRootDomain()
    {
        if(getStub() != null) {
            return getStub().getRootDomain();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.rootDomain;
        }
    }
    public void setRootDomain(String rootDomain)
    {
        if(getStub() != null) {
            getStub().setRootDomain(rootDomain);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.rootDomain = rootDomain;
        }
    }

    public String getAppDomain()
    {
        if(getStub() != null) {
            return getStub().getAppDomain();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.appDomain;
        }
    }
    public void setAppDomain(String appDomain)
    {
        if(getStub() != null) {
            getStub().setAppDomain(appDomain);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.appDomain = appDomain;
        }
    }

    public Boolean isUseAppDomain()
    {
        if(getStub() != null) {
            return getStub().isUseAppDomain();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.useAppDomain;
        }
    }
    public void setUseAppDomain(Boolean useAppDomain)
    {
        if(getStub() != null) {
            getStub().setUseAppDomain(useAppDomain);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.useAppDomain = useAppDomain;
        }
    }

    public Boolean isEnabled()
    {
        if(getStub() != null) {
            return getStub().isEnabled();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.enabled;
        }
    }
    public void setEnabled(Boolean enabled)
    {
        if(getStub() != null) {
            getStub().setEnabled(enabled);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.enabled = enabled;
        }
    }

    public String getLicenseMode()
    {
        if(getStub() != null) {
            return getStub().getLicenseMode();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.licenseMode;
        }
    }
    public void setLicenseMode(String licenseMode)
    {
        if(getStub() != null) {
            getStub().setLicenseMode(licenseMode);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.licenseMode = licenseMode;
        }
    }

    public String getStatus()
    {
        if(getStub() != null) {
            return getStub().getStatus();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.status;
        }
    }
    public void setStatus(String status)
    {
        if(getStub() != null) {
            getStub().setStatus(status);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.status = status;
        }
    }

    public Long getCreatedTime()
    {
        if(getStub() != null) {
            return getStub().getCreatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.createdTime;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getStub() != null) {
            getStub().setCreatedTime(createdTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.createdTime = createdTime;
        }
    }

    public Long getModifiedTime()
    {
        if(getStub() != null) {
            return getStub().getModifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.modifiedTime;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getStub() != null) {
            getStub().setModifiedTime(modifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.modifiedTime = modifiedTime;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public AppClientStub getStub()
    {
        return this.stub;
    }
    protected void setStub(AppClientStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("guid = " + this.guid).append(";");
            sb.append("owner = " + this.owner).append(";");
            sb.append("admin = " + this.admin).append(";");
            sb.append("name = " + this.name).append(";");
            sb.append("clientId = " + this.clientId).append(";");
            sb.append("clientCode = " + this.clientCode).append(";");
            sb.append("rootDomain = " + this.rootDomain).append(";");
            sb.append("appDomain = " + this.appDomain).append(";");
            sb.append("useAppDomain = " + this.useAppDomain).append(";");
            sb.append("enabled = " + this.enabled).append(";");
            sb.append("licenseMode = " + this.licenseMode).append(";");
            sb.append("status = " + this.status).append(";");
            sb.append("createdTime = " + this.createdTime).append(";");
            sb.append("modifiedTime = " + this.modifiedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = guid == null ? 0 : guid.hashCode();
            _hash = 31 * _hash + delta;
            delta = owner == null ? 0 : owner.hashCode();
            _hash = 31 * _hash + delta;
            delta = admin == null ? 0 : admin.hashCode();
            _hash = 31 * _hash + delta;
            delta = name == null ? 0 : name.hashCode();
            _hash = 31 * _hash + delta;
            delta = clientId == null ? 0 : clientId.hashCode();
            _hash = 31 * _hash + delta;
            delta = clientCode == null ? 0 : clientCode.hashCode();
            _hash = 31 * _hash + delta;
            delta = rootDomain == null ? 0 : rootDomain.hashCode();
            _hash = 31 * _hash + delta;
            delta = appDomain == null ? 0 : appDomain.hashCode();
            _hash = 31 * _hash + delta;
            delta = useAppDomain == null ? 0 : useAppDomain.hashCode();
            _hash = 31 * _hash + delta;
            delta = enabled == null ? 0 : enabled.hashCode();
            _hash = 31 * _hash + delta;
            delta = licenseMode == null ? 0 : licenseMode.hashCode();
            _hash = 31 * _hash + delta;
            delta = status == null ? 0 : status.hashCode();
            _hash = 31 * _hash + delta;
            delta = createdTime == null ? 0 : createdTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
