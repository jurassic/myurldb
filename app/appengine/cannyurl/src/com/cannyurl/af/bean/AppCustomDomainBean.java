package com.cannyurl.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.AppCustomDomain;
import com.myurldb.ws.stub.AppCustomDomainStub;


// Wrapper class + bean combo.
public class AppCustomDomainBean implements AppCustomDomain, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(AppCustomDomainBean.class.getName());

    // [1] With an embedded object.
    private AppCustomDomainStub stub = null;

    // [2] Or, without an embedded object.
    private String guid;
    private String appId;
    private String siteDomain;
    private String domain;
    private Boolean verified;
    private String status;
    private Long verifiedTime;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public AppCustomDomainBean()
    {
        //this((String) null);
    }
    public AppCustomDomainBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null);
    }
    public AppCustomDomainBean(String guid, String appId, String siteDomain, String domain, Boolean verified, String status, Long verifiedTime)
    {
        this(guid, appId, siteDomain, domain, verified, status, verifiedTime, null, null);
    }
    public AppCustomDomainBean(String guid, String appId, String siteDomain, String domain, Boolean verified, String status, Long verifiedTime, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.appId = appId;
        this.siteDomain = siteDomain;
        this.domain = domain;
        this.verified = verified;
        this.status = status;
        this.verifiedTime = verifiedTime;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public AppCustomDomainBean(AppCustomDomain stub)
    {
        if(stub instanceof AppCustomDomainStub) {
            this.stub = (AppCustomDomainStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setAppId(stub.getAppId());   
            setSiteDomain(stub.getSiteDomain());   
            setDomain(stub.getDomain());   
            setVerified(stub.isVerified());   
            setStatus(stub.getStatus());   
            setVerifiedTime(stub.getVerifiedTime());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public String getAppId()
    {
        if(getStub() != null) {
            return getStub().getAppId();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.appId;
        }
    }
    public void setAppId(String appId)
    {
        if(getStub() != null) {
            getStub().setAppId(appId);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.appId = appId;
        }
    }

    public String getSiteDomain()
    {
        if(getStub() != null) {
            return getStub().getSiteDomain();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.siteDomain;
        }
    }
    public void setSiteDomain(String siteDomain)
    {
        if(getStub() != null) {
            getStub().setSiteDomain(siteDomain);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.siteDomain = siteDomain;
        }
    }

    public String getDomain()
    {
        if(getStub() != null) {
            return getStub().getDomain();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.domain;
        }
    }
    public void setDomain(String domain)
    {
        if(getStub() != null) {
            getStub().setDomain(domain);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.domain = domain;
        }
    }

    public Boolean isVerified()
    {
        if(getStub() != null) {
            return getStub().isVerified();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.verified;
        }
    }
    public void setVerified(Boolean verified)
    {
        if(getStub() != null) {
            getStub().setVerified(verified);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.verified = verified;
        }
    }

    public String getStatus()
    {
        if(getStub() != null) {
            return getStub().getStatus();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.status;
        }
    }
    public void setStatus(String status)
    {
        if(getStub() != null) {
            getStub().setStatus(status);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.status = status;
        }
    }

    public Long getVerifiedTime()
    {
        if(getStub() != null) {
            return getStub().getVerifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.verifiedTime;
        }
    }
    public void setVerifiedTime(Long verifiedTime)
    {
        if(getStub() != null) {
            getStub().setVerifiedTime(verifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.verifiedTime = verifiedTime;
        }
    }

    public Long getCreatedTime()
    {
        if(getStub() != null) {
            return getStub().getCreatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.createdTime;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getStub() != null) {
            getStub().setCreatedTime(createdTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.createdTime = createdTime;
        }
    }

    public Long getModifiedTime()
    {
        if(getStub() != null) {
            return getStub().getModifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.modifiedTime;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getStub() != null) {
            getStub().setModifiedTime(modifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.modifiedTime = modifiedTime;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public AppCustomDomainStub getStub()
    {
        return this.stub;
    }
    protected void setStub(AppCustomDomainStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("guid = " + this.guid).append(";");
            sb.append("appId = " + this.appId).append(";");
            sb.append("siteDomain = " + this.siteDomain).append(";");
            sb.append("domain = " + this.domain).append(";");
            sb.append("verified = " + this.verified).append(";");
            sb.append("status = " + this.status).append(";");
            sb.append("verifiedTime = " + this.verifiedTime).append(";");
            sb.append("createdTime = " + this.createdTime).append(";");
            sb.append("modifiedTime = " + this.modifiedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = guid == null ? 0 : guid.hashCode();
            _hash = 31 * _hash + delta;
            delta = appId == null ? 0 : appId.hashCode();
            _hash = 31 * _hash + delta;
            delta = siteDomain == null ? 0 : siteDomain.hashCode();
            _hash = 31 * _hash + delta;
            delta = domain == null ? 0 : domain.hashCode();
            _hash = 31 * _hash + delta;
            delta = verified == null ? 0 : verified.hashCode();
            _hash = 31 * _hash + delta;
            delta = status == null ? 0 : status.hashCode();
            _hash = 31 * _hash + delta;
            delta = verifiedTime == null ? 0 : verifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = createdTime == null ? 0 : createdTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
