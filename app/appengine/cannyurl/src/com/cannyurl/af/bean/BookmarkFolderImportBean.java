package com.cannyurl.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.BookmarkFolderImport;
import com.myurldb.ws.stub.FolderImportBaseStub;
import com.myurldb.ws.stub.BookmarkFolderImportStub;


// Wrapper class + bean combo.
public class BookmarkFolderImportBean extends FolderImportBaseBean implements BookmarkFolderImport, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(BookmarkFolderImportBean.class.getName());


    // [2] Or, without an embedded object.
    private String bookmarkFolder;

    // Ctors.
    public BookmarkFolderImportBean()
    {
        //this((String) null);
    }
    public BookmarkFolderImportBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BookmarkFolderImportBean(String guid, String user, Integer precedence, String importType, String importedFolder, String importedFolderUser, String importedFolderTitle, String importedFolderPath, String status, String note, String bookmarkFolder)
    {
        this(guid, user, precedence, importType, importedFolder, importedFolderUser, importedFolderTitle, importedFolderPath, status, note, bookmarkFolder, null, null);
    }
    public BookmarkFolderImportBean(String guid, String user, Integer precedence, String importType, String importedFolder, String importedFolderUser, String importedFolderTitle, String importedFolderPath, String status, String note, String bookmarkFolder, Long createdTime, Long modifiedTime)
    {
        super(guid, user, precedence, importType, importedFolder, importedFolderUser, importedFolderTitle, importedFolderPath, status, note, createdTime, modifiedTime);

        this.bookmarkFolder = bookmarkFolder;
    }
    public BookmarkFolderImportBean(BookmarkFolderImport stub)
    {
        if(stub instanceof BookmarkFolderImportStub) {
            super.setStub((FolderImportBaseStub) stub);
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setUser(stub.getUser());   
            setPrecedence(stub.getPrecedence());   
            setImportType(stub.getImportType());   
            setImportedFolder(stub.getImportedFolder());   
            setImportedFolderUser(stub.getImportedFolderUser());   
            setImportedFolderTitle(stub.getImportedFolderTitle());   
            setImportedFolderPath(stub.getImportedFolderPath());   
            setStatus(stub.getStatus());   
            setNote(stub.getNote());   
            setBookmarkFolder(stub.getBookmarkFolder());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    public String getUser()
    {
        return super.getUser();
    }
    public void setUser(String user)
    {
        super.setUser(user);
    }

    public Integer getPrecedence()
    {
        return super.getPrecedence();
    }
    public void setPrecedence(Integer precedence)
    {
        super.setPrecedence(precedence);
    }

    public String getImportType()
    {
        return super.getImportType();
    }
    public void setImportType(String importType)
    {
        super.setImportType(importType);
    }

    public String getImportedFolder()
    {
        return super.getImportedFolder();
    }
    public void setImportedFolder(String importedFolder)
    {
        super.setImportedFolder(importedFolder);
    }

    public String getImportedFolderUser()
    {
        return super.getImportedFolderUser();
    }
    public void setImportedFolderUser(String importedFolderUser)
    {
        super.setImportedFolderUser(importedFolderUser);
    }

    public String getImportedFolderTitle()
    {
        return super.getImportedFolderTitle();
    }
    public void setImportedFolderTitle(String importedFolderTitle)
    {
        super.setImportedFolderTitle(importedFolderTitle);
    }

    public String getImportedFolderPath()
    {
        return super.getImportedFolderPath();
    }
    public void setImportedFolderPath(String importedFolderPath)
    {
        super.setImportedFolderPath(importedFolderPath);
    }

    public String getStatus()
    {
        return super.getStatus();
    }
    public void setStatus(String status)
    {
        super.setStatus(status);
    }

    public String getNote()
    {
        return super.getNote();
    }
    public void setNote(String note)
    {
        super.setNote(note);
    }

    public String getBookmarkFolder()
    {
        if(getStub() != null) {
            return getStub().getBookmarkFolder();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.bookmarkFolder;
        }
    }
    public void setBookmarkFolder(String bookmarkFolder)
    {
        if(getStub() != null) {
            getStub().setBookmarkFolder(bookmarkFolder);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.bookmarkFolder = bookmarkFolder;
        }
    }

    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    // Returns the reference to the embedded object. (Could be null.)
    public BookmarkFolderImportStub getStub()
    {
        return (BookmarkFolderImportStub) super.getStub();
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder( super.toString() );
            sb.append("bookmarkFolder = " + this.bookmarkFolder).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = bookmarkFolder == null ? 0 : bookmarkFolder.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
