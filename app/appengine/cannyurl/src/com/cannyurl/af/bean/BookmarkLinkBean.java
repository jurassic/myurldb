package com.cannyurl.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.BookmarkLink;
import com.myurldb.ws.stub.NavLinkBaseStub;
import com.myurldb.ws.stub.BookmarkLinkStub;


// Wrapper class + bean combo.
public class BookmarkLinkBean extends NavLinkBaseBean implements BookmarkLink, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(BookmarkLinkBean.class.getName());


    // [2] Or, without an embedded object.
    private String bookmarkFolder;
    private String contentTag;
    private String referenceElement;
    private String elementType;
    private String keywordLink;

    // Ctors.
    public BookmarkLinkBean()
    {
        //this((String) null);
    }
    public BookmarkLinkBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BookmarkLinkBean(String guid, String appClient, String clientRootDomain, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, Boolean internal, Boolean caching, String memo, String status, String note, Long expirationTime, String bookmarkFolder, String contentTag, String referenceElement, String elementType, String keywordLink)
    {
        this(guid, appClient, clientRootDomain, user, shortLink, domain, token, longUrl, shortUrl, internal, caching, memo, status, note, expirationTime, bookmarkFolder, contentTag, referenceElement, elementType, keywordLink, null, null);
    }
    public BookmarkLinkBean(String guid, String appClient, String clientRootDomain, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, Boolean internal, Boolean caching, String memo, String status, String note, Long expirationTime, String bookmarkFolder, String contentTag, String referenceElement, String elementType, String keywordLink, Long createdTime, Long modifiedTime)
    {
        super(guid, appClient, clientRootDomain, user, shortLink, domain, token, longUrl, shortUrl, internal, caching, memo, status, note, expirationTime, createdTime, modifiedTime);

        this.bookmarkFolder = bookmarkFolder;
        this.contentTag = contentTag;
        this.referenceElement = referenceElement;
        this.elementType = elementType;
        this.keywordLink = keywordLink;
    }
    public BookmarkLinkBean(BookmarkLink stub)
    {
        if(stub instanceof BookmarkLinkStub) {
            super.setStub((NavLinkBaseStub) stub);
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setAppClient(stub.getAppClient());   
            setClientRootDomain(stub.getClientRootDomain());   
            setUser(stub.getUser());   
            setShortLink(stub.getShortLink());   
            setDomain(stub.getDomain());   
            setToken(stub.getToken());   
            setLongUrl(stub.getLongUrl());   
            setShortUrl(stub.getShortUrl());   
            setInternal(stub.isInternal());   
            setCaching(stub.isCaching());   
            setMemo(stub.getMemo());   
            setStatus(stub.getStatus());   
            setNote(stub.getNote());   
            setExpirationTime(stub.getExpirationTime());   
            setBookmarkFolder(stub.getBookmarkFolder());   
            setContentTag(stub.getContentTag());   
            setReferenceElement(stub.getReferenceElement());   
            setElementType(stub.getElementType());   
            setKeywordLink(stub.getKeywordLink());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    public String getAppClient()
    {
        return super.getAppClient();
    }
    public void setAppClient(String appClient)
    {
        super.setAppClient(appClient);
    }

    public String getClientRootDomain()
    {
        return super.getClientRootDomain();
    }
    public void setClientRootDomain(String clientRootDomain)
    {
        super.setClientRootDomain(clientRootDomain);
    }

    public String getUser()
    {
        return super.getUser();
    }
    public void setUser(String user)
    {
        super.setUser(user);
    }

    public String getShortLink()
    {
        return super.getShortLink();
    }
    public void setShortLink(String shortLink)
    {
        super.setShortLink(shortLink);
    }

    public String getDomain()
    {
        return super.getDomain();
    }
    public void setDomain(String domain)
    {
        super.setDomain(domain);
    }

    public String getToken()
    {
        return super.getToken();
    }
    public void setToken(String token)
    {
        super.setToken(token);
    }

    public String getLongUrl()
    {
        return super.getLongUrl();
    }
    public void setLongUrl(String longUrl)
    {
        super.setLongUrl(longUrl);
    }

    public String getShortUrl()
    {
        return super.getShortUrl();
    }
    public void setShortUrl(String shortUrl)
    {
        super.setShortUrl(shortUrl);
    }

    public Boolean isInternal()
    {
        return super.isInternal();
    }
    public void setInternal(Boolean internal)
    {
        super.setInternal(internal);
    }

    public Boolean isCaching()
    {
        return super.isCaching();
    }
    public void setCaching(Boolean caching)
    {
        super.setCaching(caching);
    }

    public String getMemo()
    {
        return super.getMemo();
    }
    public void setMemo(String memo)
    {
        super.setMemo(memo);
    }

    public String getStatus()
    {
        return super.getStatus();
    }
    public void setStatus(String status)
    {
        super.setStatus(status);
    }

    public String getNote()
    {
        return super.getNote();
    }
    public void setNote(String note)
    {
        super.setNote(note);
    }

    public Long getExpirationTime()
    {
        return super.getExpirationTime();
    }
    public void setExpirationTime(Long expirationTime)
    {
        super.setExpirationTime(expirationTime);
    }

    public String getBookmarkFolder()
    {
        if(getStub() != null) {
            return getStub().getBookmarkFolder();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.bookmarkFolder;
        }
    }
    public void setBookmarkFolder(String bookmarkFolder)
    {
        if(getStub() != null) {
            getStub().setBookmarkFolder(bookmarkFolder);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.bookmarkFolder = bookmarkFolder;
        }
    }

    public String getContentTag()
    {
        if(getStub() != null) {
            return getStub().getContentTag();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.contentTag;
        }
    }
    public void setContentTag(String contentTag)
    {
        if(getStub() != null) {
            getStub().setContentTag(contentTag);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.contentTag = contentTag;
        }
    }

    public String getReferenceElement()
    {
        if(getStub() != null) {
            return getStub().getReferenceElement();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.referenceElement;
        }
    }
    public void setReferenceElement(String referenceElement)
    {
        if(getStub() != null) {
            getStub().setReferenceElement(referenceElement);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.referenceElement = referenceElement;
        }
    }

    public String getElementType()
    {
        if(getStub() != null) {
            return getStub().getElementType();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.elementType;
        }
    }
    public void setElementType(String elementType)
    {
        if(getStub() != null) {
            getStub().setElementType(elementType);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.elementType = elementType;
        }
    }

    public String getKeywordLink()
    {
        if(getStub() != null) {
            return getStub().getKeywordLink();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.keywordLink;
        }
    }
    public void setKeywordLink(String keywordLink)
    {
        if(getStub() != null) {
            getStub().setKeywordLink(keywordLink);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.keywordLink = keywordLink;
        }
    }

    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    // Returns the reference to the embedded object. (Could be null.)
    public BookmarkLinkStub getStub()
    {
        return (BookmarkLinkStub) super.getStub();
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder( super.toString() );
            sb.append("bookmarkFolder = " + this.bookmarkFolder).append(";");
            sb.append("contentTag = " + this.contentTag).append(";");
            sb.append("referenceElement = " + this.referenceElement).append(";");
            sb.append("elementType = " + this.elementType).append(";");
            sb.append("keywordLink = " + this.keywordLink).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = bookmarkFolder == null ? 0 : bookmarkFolder.hashCode();
            _hash = 31 * _hash + delta;
            delta = contentTag == null ? 0 : contentTag.hashCode();
            _hash = 31 * _hash + delta;
            delta = referenceElement == null ? 0 : referenceElement.hashCode();
            _hash = 31 * _hash + delta;
            delta = elementType == null ? 0 : elementType.hashCode();
            _hash = 31 * _hash + delta;
            delta = keywordLink == null ? 0 : keywordLink.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
