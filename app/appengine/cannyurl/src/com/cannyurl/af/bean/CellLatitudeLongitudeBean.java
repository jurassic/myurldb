package com.cannyurl.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.CellLatitudeLongitude;
import com.myurldb.ws.stub.CellLatitudeLongitudeStub;


// Wrapper class + bean combo.
public class CellLatitudeLongitudeBean implements CellLatitudeLongitude, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(CellLatitudeLongitudeBean.class.getName());

    // [1] With an embedded object.
    private CellLatitudeLongitudeStub stub = null;

    // [2] Or, without an embedded object.
    private Integer scale;
    private Integer latitude;
    private Integer longitude;

    // Ctors.
    public CellLatitudeLongitudeBean()
    {
        //this((String) null);
    }
    public CellLatitudeLongitudeBean(Integer scale, Integer latitude, Integer longitude)
    {
        this.scale = scale;
        this.latitude = latitude;
        this.longitude = longitude;
    }
    public CellLatitudeLongitudeBean(CellLatitudeLongitude stub)
    {
        if(stub instanceof CellLatitudeLongitudeStub) {
            this.stub = (CellLatitudeLongitudeStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setScale(stub.getScale());   
            setLatitude(stub.getLatitude());   
            setLongitude(stub.getLongitude());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public Integer getScale()
    {
        if(getStub() != null) {
            return getStub().getScale();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.scale;
        }
    }
    public void setScale(Integer scale)
    {
        if(getStub() != null) {
            getStub().setScale(scale);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.scale = scale;
        }
    }

    public Integer getLatitude()
    {
        if(getStub() != null) {
            return getStub().getLatitude();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.latitude;
        }
    }
    public void setLatitude(Integer latitude)
    {
        if(getStub() != null) {
            getStub().setLatitude(latitude);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.latitude = latitude;
        }
    }

    public Integer getLongitude()
    {
        if(getStub() != null) {
            return getStub().getLongitude();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.longitude;
        }
    }
    public void setLongitude(Integer longitude)
    {
        if(getStub() != null) {
            getStub().setLongitude(longitude);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.longitude = longitude;
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getScale() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLatitude() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLongitude() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // Returns the reference to the embedded object. (Could be null.)
    public CellLatitudeLongitudeStub getStub()
    {
        return this.stub;
    }
    protected void setStub(CellLatitudeLongitudeStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("scale = " + this.scale).append(";");
            sb.append("latitude = " + this.latitude).append(";");
            sb.append("longitude = " + this.longitude).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = scale == null ? 0 : scale.hashCode();
            _hash = 31 * _hash + delta;
            delta = latitude == null ? 0 : latitude.hashCode();
            _hash = 31 * _hash + delta;
            delta = longitude == null ? 0 : longitude.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
