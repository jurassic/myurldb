package com.cannyurl.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.ClientSetting;
import com.myurldb.ws.stub.PersonalSettingStub;
import com.myurldb.ws.stub.ClientSettingStub;


// Wrapper class + bean combo.
public class ClientSettingBean extends PersonalSettingBean implements ClientSetting, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ClientSettingBean.class.getName());


    // [2] Or, without an embedded object.
    private String appClient;
    private String admin;
    private Boolean autoRedirectEnabled;
    private Boolean viewEnabled;
    private Boolean shareEnabled;
    private String defaultBaseDomain;
    private String defaultDomainType;
    private String defaultTokenType;
    private String defaultRedirectType;
    private Long defaultFlashDuration;
    private String defaultAccessType;
    private String defaultViewType;

    // Ctors.
    public ClientSettingBean()
    {
        //this((String) null);
    }
    public ClientSettingBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public ClientSettingBean(String guid, String appClient, String admin, Boolean autoRedirectEnabled, Boolean viewEnabled, Boolean shareEnabled, String defaultBaseDomain, String defaultDomainType, String defaultTokenType, String defaultRedirectType, Long defaultFlashDuration, String defaultAccessType, String defaultViewType)
    {
        this(guid, appClient, admin, autoRedirectEnabled, viewEnabled, shareEnabled, defaultBaseDomain, defaultDomainType, defaultTokenType, defaultRedirectType, defaultFlashDuration, defaultAccessType, defaultViewType, null, null);
    }
    public ClientSettingBean(String guid, String appClient, String admin, Boolean autoRedirectEnabled, Boolean viewEnabled, Boolean shareEnabled, String defaultBaseDomain, String defaultDomainType, String defaultTokenType, String defaultRedirectType, Long defaultFlashDuration, String defaultAccessType, String defaultViewType, Long createdTime, Long modifiedTime)
    {
        super(guid, createdTime, modifiedTime);

        this.appClient = appClient;
        this.admin = admin;
        this.autoRedirectEnabled = autoRedirectEnabled;
        this.viewEnabled = viewEnabled;
        this.shareEnabled = shareEnabled;
        this.defaultBaseDomain = defaultBaseDomain;
        this.defaultDomainType = defaultDomainType;
        this.defaultTokenType = defaultTokenType;
        this.defaultRedirectType = defaultRedirectType;
        this.defaultFlashDuration = defaultFlashDuration;
        this.defaultAccessType = defaultAccessType;
        this.defaultViewType = defaultViewType;
    }
    public ClientSettingBean(ClientSetting stub)
    {
        if(stub instanceof ClientSettingStub) {
            super.setStub((PersonalSettingStub) stub);
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setAppClient(stub.getAppClient());   
            setAdmin(stub.getAdmin());   
            setAutoRedirectEnabled(stub.isAutoRedirectEnabled());   
            setViewEnabled(stub.isViewEnabled());   
            setShareEnabled(stub.isShareEnabled());   
            setDefaultBaseDomain(stub.getDefaultBaseDomain());   
            setDefaultDomainType(stub.getDefaultDomainType());   
            setDefaultTokenType(stub.getDefaultTokenType());   
            setDefaultRedirectType(stub.getDefaultRedirectType());   
            setDefaultFlashDuration(stub.getDefaultFlashDuration());   
            setDefaultAccessType(stub.getDefaultAccessType());   
            setDefaultViewType(stub.getDefaultViewType());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    public String getAppClient()
    {
        if(getStub() != null) {
            return getStub().getAppClient();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.appClient;
        }
    }
    public void setAppClient(String appClient)
    {
        if(getStub() != null) {
            getStub().setAppClient(appClient);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.appClient = appClient;
        }
    }

    public String getAdmin()
    {
        if(getStub() != null) {
            return getStub().getAdmin();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.admin;
        }
    }
    public void setAdmin(String admin)
    {
        if(getStub() != null) {
            getStub().setAdmin(admin);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.admin = admin;
        }
    }

    public Boolean isAutoRedirectEnabled()
    {
        if(getStub() != null) {
            return getStub().isAutoRedirectEnabled();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.autoRedirectEnabled;
        }
    }
    public void setAutoRedirectEnabled(Boolean autoRedirectEnabled)
    {
        if(getStub() != null) {
            getStub().setAutoRedirectEnabled(autoRedirectEnabled);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.autoRedirectEnabled = autoRedirectEnabled;
        }
    }

    public Boolean isViewEnabled()
    {
        if(getStub() != null) {
            return getStub().isViewEnabled();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.viewEnabled;
        }
    }
    public void setViewEnabled(Boolean viewEnabled)
    {
        if(getStub() != null) {
            getStub().setViewEnabled(viewEnabled);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.viewEnabled = viewEnabled;
        }
    }

    public Boolean isShareEnabled()
    {
        if(getStub() != null) {
            return getStub().isShareEnabled();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.shareEnabled;
        }
    }
    public void setShareEnabled(Boolean shareEnabled)
    {
        if(getStub() != null) {
            getStub().setShareEnabled(shareEnabled);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.shareEnabled = shareEnabled;
        }
    }

    public String getDefaultBaseDomain()
    {
        if(getStub() != null) {
            return getStub().getDefaultBaseDomain();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.defaultBaseDomain;
        }
    }
    public void setDefaultBaseDomain(String defaultBaseDomain)
    {
        if(getStub() != null) {
            getStub().setDefaultBaseDomain(defaultBaseDomain);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.defaultBaseDomain = defaultBaseDomain;
        }
    }

    public String getDefaultDomainType()
    {
        if(getStub() != null) {
            return getStub().getDefaultDomainType();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.defaultDomainType;
        }
    }
    public void setDefaultDomainType(String defaultDomainType)
    {
        if(getStub() != null) {
            getStub().setDefaultDomainType(defaultDomainType);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.defaultDomainType = defaultDomainType;
        }
    }

    public String getDefaultTokenType()
    {
        if(getStub() != null) {
            return getStub().getDefaultTokenType();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.defaultTokenType;
        }
    }
    public void setDefaultTokenType(String defaultTokenType)
    {
        if(getStub() != null) {
            getStub().setDefaultTokenType(defaultTokenType);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.defaultTokenType = defaultTokenType;
        }
    }

    public String getDefaultRedirectType()
    {
        if(getStub() != null) {
            return getStub().getDefaultRedirectType();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.defaultRedirectType;
        }
    }
    public void setDefaultRedirectType(String defaultRedirectType)
    {
        if(getStub() != null) {
            getStub().setDefaultRedirectType(defaultRedirectType);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.defaultRedirectType = defaultRedirectType;
        }
    }

    public Long getDefaultFlashDuration()
    {
        if(getStub() != null) {
            return getStub().getDefaultFlashDuration();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.defaultFlashDuration;
        }
    }
    public void setDefaultFlashDuration(Long defaultFlashDuration)
    {
        if(getStub() != null) {
            getStub().setDefaultFlashDuration(defaultFlashDuration);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.defaultFlashDuration = defaultFlashDuration;
        }
    }

    public String getDefaultAccessType()
    {
        if(getStub() != null) {
            return getStub().getDefaultAccessType();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.defaultAccessType;
        }
    }
    public void setDefaultAccessType(String defaultAccessType)
    {
        if(getStub() != null) {
            getStub().setDefaultAccessType(defaultAccessType);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.defaultAccessType = defaultAccessType;
        }
    }

    public String getDefaultViewType()
    {
        if(getStub() != null) {
            return getStub().getDefaultViewType();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.defaultViewType;
        }
    }
    public void setDefaultViewType(String defaultViewType)
    {
        if(getStub() != null) {
            getStub().setDefaultViewType(defaultViewType);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.defaultViewType = defaultViewType;
        }
    }

    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    // Returns the reference to the embedded object. (Could be null.)
    public ClientSettingStub getStub()
    {
        return (ClientSettingStub) super.getStub();
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder( super.toString() );
            sb.append("appClient = " + this.appClient).append(";");
            sb.append("admin = " + this.admin).append(";");
            sb.append("autoRedirectEnabled = " + this.autoRedirectEnabled).append(";");
            sb.append("viewEnabled = " + this.viewEnabled).append(";");
            sb.append("shareEnabled = " + this.shareEnabled).append(";");
            sb.append("defaultBaseDomain = " + this.defaultBaseDomain).append(";");
            sb.append("defaultDomainType = " + this.defaultDomainType).append(";");
            sb.append("defaultTokenType = " + this.defaultTokenType).append(";");
            sb.append("defaultRedirectType = " + this.defaultRedirectType).append(";");
            sb.append("defaultFlashDuration = " + this.defaultFlashDuration).append(";");
            sb.append("defaultAccessType = " + this.defaultAccessType).append(";");
            sb.append("defaultViewType = " + this.defaultViewType).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = appClient == null ? 0 : appClient.hashCode();
            _hash = 31 * _hash + delta;
            delta = admin == null ? 0 : admin.hashCode();
            _hash = 31 * _hash + delta;
            delta = autoRedirectEnabled == null ? 0 : autoRedirectEnabled.hashCode();
            _hash = 31 * _hash + delta;
            delta = viewEnabled == null ? 0 : viewEnabled.hashCode();
            _hash = 31 * _hash + delta;
            delta = shareEnabled == null ? 0 : shareEnabled.hashCode();
            _hash = 31 * _hash + delta;
            delta = defaultBaseDomain == null ? 0 : defaultBaseDomain.hashCode();
            _hash = 31 * _hash + delta;
            delta = defaultDomainType == null ? 0 : defaultDomainType.hashCode();
            _hash = 31 * _hash + delta;
            delta = defaultTokenType == null ? 0 : defaultTokenType.hashCode();
            _hash = 31 * _hash + delta;
            delta = defaultRedirectType == null ? 0 : defaultRedirectType.hashCode();
            _hash = 31 * _hash + delta;
            delta = defaultFlashDuration == null ? 0 : defaultFlashDuration.hashCode();
            _hash = 31 * _hash + delta;
            delta = defaultAccessType == null ? 0 : defaultAccessType.hashCode();
            _hash = 31 * _hash + delta;
            delta = defaultViewType == null ? 0 : defaultViewType.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
