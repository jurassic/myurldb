package com.cannyurl.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.ContactInfoStruct;
import com.myurldb.ws.stub.ContactInfoStructStub;


// Wrapper class + bean combo.
public class ContactInfoStructBean implements ContactInfoStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ContactInfoStructBean.class.getName());

    // [1] With an embedded object.
    private ContactInfoStructStub stub = null;

    // [2] Or, without an embedded object.
    private String uuid;
    private String streetAddress;
    private String locality;
    private String region;
    private String postalCode;
    private String countryName;
    private String emailAddress;
    private String phoneNumber;
    private String faxNumber;
    private String website;
    private String note;

    // Ctors.
    public ContactInfoStructBean()
    {
        //this((String) null);
    }
    public ContactInfoStructBean(String uuid, String streetAddress, String locality, String region, String postalCode, String countryName, String emailAddress, String phoneNumber, String faxNumber, String website, String note)
    {
        this.uuid = uuid;
        this.streetAddress = streetAddress;
        this.locality = locality;
        this.region = region;
        this.postalCode = postalCode;
        this.countryName = countryName;
        this.emailAddress = emailAddress;
        this.phoneNumber = phoneNumber;
        this.faxNumber = faxNumber;
        this.website = website;
        this.note = note;
    }
    public ContactInfoStructBean(ContactInfoStruct stub)
    {
        if(stub instanceof ContactInfoStructStub) {
            this.stub = (ContactInfoStructStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setUuid(stub.getUuid());   
            setStreetAddress(stub.getStreetAddress());   
            setLocality(stub.getLocality());   
            setRegion(stub.getRegion());   
            setPostalCode(stub.getPostalCode());   
            setCountryName(stub.getCountryName());   
            setEmailAddress(stub.getEmailAddress());   
            setPhoneNumber(stub.getPhoneNumber());   
            setFaxNumber(stub.getFaxNumber());   
            setWebsite(stub.getWebsite());   
            setNote(stub.getNote());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getUuid()
    {
        if(getStub() != null) {
            return getStub().getUuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.uuid;
        }
    }
    public void setUuid(String uuid)
    {
        if(getStub() != null) {
            getStub().setUuid(uuid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.uuid = uuid;
        }
    }

    public String getStreetAddress()
    {
        if(getStub() != null) {
            return getStub().getStreetAddress();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.streetAddress;
        }
    }
    public void setStreetAddress(String streetAddress)
    {
        if(getStub() != null) {
            getStub().setStreetAddress(streetAddress);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.streetAddress = streetAddress;
        }
    }

    public String getLocality()
    {
        if(getStub() != null) {
            return getStub().getLocality();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.locality;
        }
    }
    public void setLocality(String locality)
    {
        if(getStub() != null) {
            getStub().setLocality(locality);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.locality = locality;
        }
    }

    public String getRegion()
    {
        if(getStub() != null) {
            return getStub().getRegion();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.region;
        }
    }
    public void setRegion(String region)
    {
        if(getStub() != null) {
            getStub().setRegion(region);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.region = region;
        }
    }

    public String getPostalCode()
    {
        if(getStub() != null) {
            return getStub().getPostalCode();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.postalCode;
        }
    }
    public void setPostalCode(String postalCode)
    {
        if(getStub() != null) {
            getStub().setPostalCode(postalCode);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.postalCode = postalCode;
        }
    }

    public String getCountryName()
    {
        if(getStub() != null) {
            return getStub().getCountryName();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.countryName;
        }
    }
    public void setCountryName(String countryName)
    {
        if(getStub() != null) {
            getStub().setCountryName(countryName);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.countryName = countryName;
        }
    }

    public String getEmailAddress()
    {
        if(getStub() != null) {
            return getStub().getEmailAddress();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.emailAddress;
        }
    }
    public void setEmailAddress(String emailAddress)
    {
        if(getStub() != null) {
            getStub().setEmailAddress(emailAddress);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.emailAddress = emailAddress;
        }
    }

    public String getPhoneNumber()
    {
        if(getStub() != null) {
            return getStub().getPhoneNumber();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.phoneNumber;
        }
    }
    public void setPhoneNumber(String phoneNumber)
    {
        if(getStub() != null) {
            getStub().setPhoneNumber(phoneNumber);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.phoneNumber = phoneNumber;
        }
    }

    public String getFaxNumber()
    {
        if(getStub() != null) {
            return getStub().getFaxNumber();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.faxNumber;
        }
    }
    public void setFaxNumber(String faxNumber)
    {
        if(getStub() != null) {
            getStub().setFaxNumber(faxNumber);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.faxNumber = faxNumber;
        }
    }

    public String getWebsite()
    {
        if(getStub() != null) {
            return getStub().getWebsite();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.website;
        }
    }
    public void setWebsite(String website)
    {
        if(getStub() != null) {
            getStub().setWebsite(website);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.website = website;
        }
    }

    public String getNote()
    {
        if(getStub() != null) {
            return getStub().getNote();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.note;
        }
    }
    public void setNote(String note)
    {
        if(getStub() != null) {
            getStub().setNote(note);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.note = note;
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getStreetAddress() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLocality() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getRegion() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPostalCode() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getCountryName() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getEmailAddress() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPhoneNumber() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getFaxNumber() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getWebsite() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // Returns the reference to the embedded object. (Could be null.)
    public ContactInfoStructStub getStub()
    {
        return this.stub;
    }
    protected void setStub(ContactInfoStructStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("uuid = " + this.uuid).append(";");
            sb.append("streetAddress = " + this.streetAddress).append(";");
            sb.append("locality = " + this.locality).append(";");
            sb.append("region = " + this.region).append(";");
            sb.append("postalCode = " + this.postalCode).append(";");
            sb.append("countryName = " + this.countryName).append(";");
            sb.append("emailAddress = " + this.emailAddress).append(";");
            sb.append("phoneNumber = " + this.phoneNumber).append(";");
            sb.append("faxNumber = " + this.faxNumber).append(";");
            sb.append("website = " + this.website).append(";");
            sb.append("note = " + this.note).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = uuid == null ? 0 : uuid.hashCode();
            _hash = 31 * _hash + delta;
            delta = streetAddress == null ? 0 : streetAddress.hashCode();
            _hash = 31 * _hash + delta;
            delta = locality == null ? 0 : locality.hashCode();
            _hash = 31 * _hash + delta;
            delta = region == null ? 0 : region.hashCode();
            _hash = 31 * _hash + delta;
            delta = postalCode == null ? 0 : postalCode.hashCode();
            _hash = 31 * _hash + delta;
            delta = countryName == null ? 0 : countryName.hashCode();
            _hash = 31 * _hash + delta;
            delta = emailAddress == null ? 0 : emailAddress.hashCode();
            _hash = 31 * _hash + delta;
            delta = phoneNumber == null ? 0 : phoneNumber.hashCode();
            _hash = 31 * _hash + delta;
            delta = faxNumber == null ? 0 : faxNumber.hashCode();
            _hash = 31 * _hash + delta;
            delta = website == null ? 0 : website.hashCode();
            _hash = 31 * _hash + delta;
            delta = note == null ? 0 : note.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
