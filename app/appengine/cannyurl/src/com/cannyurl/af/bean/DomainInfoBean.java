package com.cannyurl.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.DomainInfo;
import com.myurldb.ws.stub.DomainInfoStub;


// Wrapper class + bean combo.
public class DomainInfoBean implements DomainInfo, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(DomainInfoBean.class.getName());

    // [1] With an embedded object.
    private DomainInfoStub stub = null;

    // [2] Or, without an embedded object.
    private String guid;
    private String domain;
    private Boolean banned;
    private Boolean urlShortener;
    private String category;
    private String reputation;
    private String authority;
    private String note;
    private Long verifiedTime;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public DomainInfoBean()
    {
        //this((String) null);
    }
    public DomainInfoBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null);
    }
    public DomainInfoBean(String guid, String domain, Boolean banned, Boolean urlShortener, String category, String reputation, String authority, String note, Long verifiedTime)
    {
        this(guid, domain, banned, urlShortener, category, reputation, authority, note, verifiedTime, null, null);
    }
    public DomainInfoBean(String guid, String domain, Boolean banned, Boolean urlShortener, String category, String reputation, String authority, String note, Long verifiedTime, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.domain = domain;
        this.banned = banned;
        this.urlShortener = urlShortener;
        this.category = category;
        this.reputation = reputation;
        this.authority = authority;
        this.note = note;
        this.verifiedTime = verifiedTime;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public DomainInfoBean(DomainInfo stub)
    {
        if(stub instanceof DomainInfoStub) {
            this.stub = (DomainInfoStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setDomain(stub.getDomain());   
            setBanned(stub.isBanned());   
            setUrlShortener(stub.isUrlShortener());   
            setCategory(stub.getCategory());   
            setReputation(stub.getReputation());   
            setAuthority(stub.getAuthority());   
            setNote(stub.getNote());   
            setVerifiedTime(stub.getVerifiedTime());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public String getDomain()
    {
        if(getStub() != null) {
            return getStub().getDomain();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.domain;
        }
    }
    public void setDomain(String domain)
    {
        if(getStub() != null) {
            getStub().setDomain(domain);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.domain = domain;
        }
    }

    public Boolean isBanned()
    {
        if(getStub() != null) {
            return getStub().isBanned();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.banned;
        }
    }
    public void setBanned(Boolean banned)
    {
        if(getStub() != null) {
            getStub().setBanned(banned);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.banned = banned;
        }
    }

    public Boolean isUrlShortener()
    {
        if(getStub() != null) {
            return getStub().isUrlShortener();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.urlShortener;
        }
    }
    public void setUrlShortener(Boolean urlShortener)
    {
        if(getStub() != null) {
            getStub().setUrlShortener(urlShortener);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.urlShortener = urlShortener;
        }
    }

    public String getCategory()
    {
        if(getStub() != null) {
            return getStub().getCategory();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.category;
        }
    }
    public void setCategory(String category)
    {
        if(getStub() != null) {
            getStub().setCategory(category);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.category = category;
        }
    }

    public String getReputation()
    {
        if(getStub() != null) {
            return getStub().getReputation();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.reputation;
        }
    }
    public void setReputation(String reputation)
    {
        if(getStub() != null) {
            getStub().setReputation(reputation);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.reputation = reputation;
        }
    }

    public String getAuthority()
    {
        if(getStub() != null) {
            return getStub().getAuthority();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.authority;
        }
    }
    public void setAuthority(String authority)
    {
        if(getStub() != null) {
            getStub().setAuthority(authority);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.authority = authority;
        }
    }

    public String getNote()
    {
        if(getStub() != null) {
            return getStub().getNote();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.note;
        }
    }
    public void setNote(String note)
    {
        if(getStub() != null) {
            getStub().setNote(note);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.note = note;
        }
    }

    public Long getVerifiedTime()
    {
        if(getStub() != null) {
            return getStub().getVerifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.verifiedTime;
        }
    }
    public void setVerifiedTime(Long verifiedTime)
    {
        if(getStub() != null) {
            getStub().setVerifiedTime(verifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.verifiedTime = verifiedTime;
        }
    }

    public Long getCreatedTime()
    {
        if(getStub() != null) {
            return getStub().getCreatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.createdTime;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getStub() != null) {
            getStub().setCreatedTime(createdTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.createdTime = createdTime;
        }
    }

    public Long getModifiedTime()
    {
        if(getStub() != null) {
            return getStub().getModifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.modifiedTime;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getStub() != null) {
            getStub().setModifiedTime(modifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.modifiedTime = modifiedTime;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public DomainInfoStub getStub()
    {
        return this.stub;
    }
    protected void setStub(DomainInfoStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("guid = " + this.guid).append(";");
            sb.append("domain = " + this.domain).append(";");
            sb.append("banned = " + this.banned).append(";");
            sb.append("urlShortener = " + this.urlShortener).append(";");
            sb.append("category = " + this.category).append(";");
            sb.append("reputation = " + this.reputation).append(";");
            sb.append("authority = " + this.authority).append(";");
            sb.append("note = " + this.note).append(";");
            sb.append("verifiedTime = " + this.verifiedTime).append(";");
            sb.append("createdTime = " + this.createdTime).append(";");
            sb.append("modifiedTime = " + this.modifiedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = guid == null ? 0 : guid.hashCode();
            _hash = 31 * _hash + delta;
            delta = domain == null ? 0 : domain.hashCode();
            _hash = 31 * _hash + delta;
            delta = banned == null ? 0 : banned.hashCode();
            _hash = 31 * _hash + delta;
            delta = urlShortener == null ? 0 : urlShortener.hashCode();
            _hash = 31 * _hash + delta;
            delta = category == null ? 0 : category.hashCode();
            _hash = 31 * _hash + delta;
            delta = reputation == null ? 0 : reputation.hashCode();
            _hash = 31 * _hash + delta;
            delta = authority == null ? 0 : authority.hashCode();
            _hash = 31 * _hash + delta;
            delta = note == null ? 0 : note.hashCode();
            _hash = 31 * _hash + delta;
            delta = verifiedTime == null ? 0 : verifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = createdTime == null ? 0 : createdTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
