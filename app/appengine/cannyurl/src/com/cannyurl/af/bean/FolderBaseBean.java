package com.cannyurl.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.FolderBase;
import com.myurldb.ws.stub.FolderBaseStub;


// Wrapper class + bean combo.
public abstract class FolderBaseBean implements FolderBase, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(FolderBaseBean.class.getName());

    // [1] With an embedded object.
    private FolderBaseStub stub = null;

    // [2] Or, without an embedded object.
    private String guid;
    private String appClient;
    private String clientRootDomain;
    private String user;
    private String title;
    private String description;
    private String type;
    private String category;
    private String parent;
    private String aggregate;
    private String acl;
    private Boolean exportable;
    private String status;
    private String note;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public FolderBaseBean()
    {
        //this((String) null);
    }
    public FolderBaseBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public FolderBaseBean(String guid, String appClient, String clientRootDomain, String user, String title, String description, String type, String category, String parent, String aggregate, String acl, Boolean exportable, String status, String note)
    {
        this(guid, appClient, clientRootDomain, user, title, description, type, category, parent, aggregate, acl, exportable, status, note, null, null);
    }
    public FolderBaseBean(String guid, String appClient, String clientRootDomain, String user, String title, String description, String type, String category, String parent, String aggregate, String acl, Boolean exportable, String status, String note, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.appClient = appClient;
        this.clientRootDomain = clientRootDomain;
        this.user = user;
        this.title = title;
        this.description = description;
        this.type = type;
        this.category = category;
        this.parent = parent;
        this.aggregate = aggregate;
        this.acl = acl;
        this.exportable = exportable;
        this.status = status;
        this.note = note;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public FolderBaseBean(FolderBase stub)
    {
        if(stub instanceof FolderBaseStub) {
            this.stub = (FolderBaseStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setAppClient(stub.getAppClient());   
            setClientRootDomain(stub.getClientRootDomain());   
            setUser(stub.getUser());   
            setTitle(stub.getTitle());   
            setDescription(stub.getDescription());   
            setType(stub.getType());   
            setCategory(stub.getCategory());   
            setParent(stub.getParent());   
            setAggregate(stub.getAggregate());   
            setAcl(stub.getAcl());   
            setExportable(stub.isExportable());   
            setStatus(stub.getStatus());   
            setNote(stub.getNote());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public String getAppClient()
    {
        if(getStub() != null) {
            return getStub().getAppClient();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.appClient;
        }
    }
    public void setAppClient(String appClient)
    {
        if(getStub() != null) {
            getStub().setAppClient(appClient);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.appClient = appClient;
        }
    }

    public String getClientRootDomain()
    {
        if(getStub() != null) {
            return getStub().getClientRootDomain();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.clientRootDomain;
        }
    }
    public void setClientRootDomain(String clientRootDomain)
    {
        if(getStub() != null) {
            getStub().setClientRootDomain(clientRootDomain);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.clientRootDomain = clientRootDomain;
        }
    }

    public String getUser()
    {
        if(getStub() != null) {
            return getStub().getUser();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.user;
        }
    }
    public void setUser(String user)
    {
        if(getStub() != null) {
            getStub().setUser(user);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.user = user;
        }
    }

    public String getTitle()
    {
        if(getStub() != null) {
            return getStub().getTitle();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.title;
        }
    }
    public void setTitle(String title)
    {
        if(getStub() != null) {
            getStub().setTitle(title);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.title = title;
        }
    }

    public String getDescription()
    {
        if(getStub() != null) {
            return getStub().getDescription();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.description;
        }
    }
    public void setDescription(String description)
    {
        if(getStub() != null) {
            getStub().setDescription(description);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.description = description;
        }
    }

    public String getType()
    {
        if(getStub() != null) {
            return getStub().getType();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.type;
        }
    }
    public void setType(String type)
    {
        if(getStub() != null) {
            getStub().setType(type);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.type = type;
        }
    }

    public String getCategory()
    {
        if(getStub() != null) {
            return getStub().getCategory();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.category;
        }
    }
    public void setCategory(String category)
    {
        if(getStub() != null) {
            getStub().setCategory(category);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.category = category;
        }
    }

    public String getParent()
    {
        if(getStub() != null) {
            return getStub().getParent();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.parent;
        }
    }
    public void setParent(String parent)
    {
        if(getStub() != null) {
            getStub().setParent(parent);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.parent = parent;
        }
    }

    public String getAggregate()
    {
        if(getStub() != null) {
            return getStub().getAggregate();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.aggregate;
        }
    }
    public void setAggregate(String aggregate)
    {
        if(getStub() != null) {
            getStub().setAggregate(aggregate);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.aggregate = aggregate;
        }
    }

    public String getAcl()
    {
        if(getStub() != null) {
            return getStub().getAcl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.acl;
        }
    }
    public void setAcl(String acl)
    {
        if(getStub() != null) {
            getStub().setAcl(acl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.acl = acl;
        }
    }

    public Boolean isExportable()
    {
        if(getStub() != null) {
            return getStub().isExportable();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.exportable;
        }
    }
    public void setExportable(Boolean exportable)
    {
        if(getStub() != null) {
            getStub().setExportable(exportable);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.exportable = exportable;
        }
    }

    public String getStatus()
    {
        if(getStub() != null) {
            return getStub().getStatus();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.status;
        }
    }
    public void setStatus(String status)
    {
        if(getStub() != null) {
            getStub().setStatus(status);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.status = status;
        }
    }

    public String getNote()
    {
        if(getStub() != null) {
            return getStub().getNote();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.note;
        }
    }
    public void setNote(String note)
    {
        if(getStub() != null) {
            getStub().setNote(note);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.note = note;
        }
    }

    public Long getCreatedTime()
    {
        if(getStub() != null) {
            return getStub().getCreatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.createdTime;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getStub() != null) {
            getStub().setCreatedTime(createdTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.createdTime = createdTime;
        }
    }

    public Long getModifiedTime()
    {
        if(getStub() != null) {
            return getStub().getModifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.modifiedTime;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getStub() != null) {
            getStub().setModifiedTime(modifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.modifiedTime = modifiedTime;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public FolderBaseStub getStub()
    {
        return this.stub;
    }
    protected void setStub(FolderBaseStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("guid = " + this.guid).append(";");
            sb.append("appClient = " + this.appClient).append(";");
            sb.append("clientRootDomain = " + this.clientRootDomain).append(";");
            sb.append("user = " + this.user).append(";");
            sb.append("title = " + this.title).append(";");
            sb.append("description = " + this.description).append(";");
            sb.append("type = " + this.type).append(";");
            sb.append("category = " + this.category).append(";");
            sb.append("parent = " + this.parent).append(";");
            sb.append("aggregate = " + this.aggregate).append(";");
            sb.append("acl = " + this.acl).append(";");
            sb.append("exportable = " + this.exportable).append(";");
            sb.append("status = " + this.status).append(";");
            sb.append("note = " + this.note).append(";");
            sb.append("createdTime = " + this.createdTime).append(";");
            sb.append("modifiedTime = " + this.modifiedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = guid == null ? 0 : guid.hashCode();
            _hash = 31 * _hash + delta;
            delta = appClient == null ? 0 : appClient.hashCode();
            _hash = 31 * _hash + delta;
            delta = clientRootDomain == null ? 0 : clientRootDomain.hashCode();
            _hash = 31 * _hash + delta;
            delta = user == null ? 0 : user.hashCode();
            _hash = 31 * _hash + delta;
            delta = title == null ? 0 : title.hashCode();
            _hash = 31 * _hash + delta;
            delta = description == null ? 0 : description.hashCode();
            _hash = 31 * _hash + delta;
            delta = type == null ? 0 : type.hashCode();
            _hash = 31 * _hash + delta;
            delta = category == null ? 0 : category.hashCode();
            _hash = 31 * _hash + delta;
            delta = parent == null ? 0 : parent.hashCode();
            _hash = 31 * _hash + delta;
            delta = aggregate == null ? 0 : aggregate.hashCode();
            _hash = 31 * _hash + delta;
            delta = acl == null ? 0 : acl.hashCode();
            _hash = 31 * _hash + delta;
            delta = exportable == null ? 0 : exportable.hashCode();
            _hash = 31 * _hash + delta;
            delta = status == null ? 0 : status.hashCode();
            _hash = 31 * _hash + delta;
            delta = note == null ? 0 : note.hashCode();
            _hash = 31 * _hash + delta;
            delta = createdTime == null ? 0 : createdTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
