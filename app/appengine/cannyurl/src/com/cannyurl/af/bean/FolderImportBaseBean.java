package com.cannyurl.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.FolderImportBase;
import com.myurldb.ws.stub.FolderImportBaseStub;


// Wrapper class + bean combo.
public abstract class FolderImportBaseBean implements FolderImportBase, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(FolderImportBaseBean.class.getName());

    // [1] With an embedded object.
    private FolderImportBaseStub stub = null;

    // [2] Or, without an embedded object.
    private String guid;
    private String user;
    private Integer precedence;
    private String importType;
    private String importedFolder;
    private String importedFolderUser;
    private String importedFolderTitle;
    private String importedFolderPath;
    private String status;
    private String note;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public FolderImportBaseBean()
    {
        //this((String) null);
    }
    public FolderImportBaseBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null);
    }
    public FolderImportBaseBean(String guid, String user, Integer precedence, String importType, String importedFolder, String importedFolderUser, String importedFolderTitle, String importedFolderPath, String status, String note)
    {
        this(guid, user, precedence, importType, importedFolder, importedFolderUser, importedFolderTitle, importedFolderPath, status, note, null, null);
    }
    public FolderImportBaseBean(String guid, String user, Integer precedence, String importType, String importedFolder, String importedFolderUser, String importedFolderTitle, String importedFolderPath, String status, String note, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.user = user;
        this.precedence = precedence;
        this.importType = importType;
        this.importedFolder = importedFolder;
        this.importedFolderUser = importedFolderUser;
        this.importedFolderTitle = importedFolderTitle;
        this.importedFolderPath = importedFolderPath;
        this.status = status;
        this.note = note;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public FolderImportBaseBean(FolderImportBase stub)
    {
        if(stub instanceof FolderImportBaseStub) {
            this.stub = (FolderImportBaseStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setUser(stub.getUser());   
            setPrecedence(stub.getPrecedence());   
            setImportType(stub.getImportType());   
            setImportedFolder(stub.getImportedFolder());   
            setImportedFolderUser(stub.getImportedFolderUser());   
            setImportedFolderTitle(stub.getImportedFolderTitle());   
            setImportedFolderPath(stub.getImportedFolderPath());   
            setStatus(stub.getStatus());   
            setNote(stub.getNote());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public String getUser()
    {
        if(getStub() != null) {
            return getStub().getUser();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.user;
        }
    }
    public void setUser(String user)
    {
        if(getStub() != null) {
            getStub().setUser(user);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.user = user;
        }
    }

    public Integer getPrecedence()
    {
        if(getStub() != null) {
            return getStub().getPrecedence();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.precedence;
        }
    }
    public void setPrecedence(Integer precedence)
    {
        if(getStub() != null) {
            getStub().setPrecedence(precedence);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.precedence = precedence;
        }
    }

    public String getImportType()
    {
        if(getStub() != null) {
            return getStub().getImportType();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.importType;
        }
    }
    public void setImportType(String importType)
    {
        if(getStub() != null) {
            getStub().setImportType(importType);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.importType = importType;
        }
    }

    public String getImportedFolder()
    {
        if(getStub() != null) {
            return getStub().getImportedFolder();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.importedFolder;
        }
    }
    public void setImportedFolder(String importedFolder)
    {
        if(getStub() != null) {
            getStub().setImportedFolder(importedFolder);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.importedFolder = importedFolder;
        }
    }

    public String getImportedFolderUser()
    {
        if(getStub() != null) {
            return getStub().getImportedFolderUser();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.importedFolderUser;
        }
    }
    public void setImportedFolderUser(String importedFolderUser)
    {
        if(getStub() != null) {
            getStub().setImportedFolderUser(importedFolderUser);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.importedFolderUser = importedFolderUser;
        }
    }

    public String getImportedFolderTitle()
    {
        if(getStub() != null) {
            return getStub().getImportedFolderTitle();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.importedFolderTitle;
        }
    }
    public void setImportedFolderTitle(String importedFolderTitle)
    {
        if(getStub() != null) {
            getStub().setImportedFolderTitle(importedFolderTitle);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.importedFolderTitle = importedFolderTitle;
        }
    }

    public String getImportedFolderPath()
    {
        if(getStub() != null) {
            return getStub().getImportedFolderPath();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.importedFolderPath;
        }
    }
    public void setImportedFolderPath(String importedFolderPath)
    {
        if(getStub() != null) {
            getStub().setImportedFolderPath(importedFolderPath);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.importedFolderPath = importedFolderPath;
        }
    }

    public String getStatus()
    {
        if(getStub() != null) {
            return getStub().getStatus();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.status;
        }
    }
    public void setStatus(String status)
    {
        if(getStub() != null) {
            getStub().setStatus(status);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.status = status;
        }
    }

    public String getNote()
    {
        if(getStub() != null) {
            return getStub().getNote();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.note;
        }
    }
    public void setNote(String note)
    {
        if(getStub() != null) {
            getStub().setNote(note);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.note = note;
        }
    }

    public Long getCreatedTime()
    {
        if(getStub() != null) {
            return getStub().getCreatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.createdTime;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getStub() != null) {
            getStub().setCreatedTime(createdTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.createdTime = createdTime;
        }
    }

    public Long getModifiedTime()
    {
        if(getStub() != null) {
            return getStub().getModifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.modifiedTime;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getStub() != null) {
            getStub().setModifiedTime(modifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.modifiedTime = modifiedTime;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public FolderImportBaseStub getStub()
    {
        return this.stub;
    }
    protected void setStub(FolderImportBaseStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("guid = " + this.guid).append(";");
            sb.append("user = " + this.user).append(";");
            sb.append("precedence = " + this.precedence).append(";");
            sb.append("importType = " + this.importType).append(";");
            sb.append("importedFolder = " + this.importedFolder).append(";");
            sb.append("importedFolderUser = " + this.importedFolderUser).append(";");
            sb.append("importedFolderTitle = " + this.importedFolderTitle).append(";");
            sb.append("importedFolderPath = " + this.importedFolderPath).append(";");
            sb.append("status = " + this.status).append(";");
            sb.append("note = " + this.note).append(";");
            sb.append("createdTime = " + this.createdTime).append(";");
            sb.append("modifiedTime = " + this.modifiedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = guid == null ? 0 : guid.hashCode();
            _hash = 31 * _hash + delta;
            delta = user == null ? 0 : user.hashCode();
            _hash = 31 * _hash + delta;
            delta = precedence == null ? 0 : precedence.hashCode();
            _hash = 31 * _hash + delta;
            delta = importType == null ? 0 : importType.hashCode();
            _hash = 31 * _hash + delta;
            delta = importedFolder == null ? 0 : importedFolder.hashCode();
            _hash = 31 * _hash + delta;
            delta = importedFolderUser == null ? 0 : importedFolderUser.hashCode();
            _hash = 31 * _hash + delta;
            delta = importedFolderTitle == null ? 0 : importedFolderTitle.hashCode();
            _hash = 31 * _hash + delta;
            delta = importedFolderPath == null ? 0 : importedFolderPath.hashCode();
            _hash = 31 * _hash + delta;
            delta = status == null ? 0 : status.hashCode();
            _hash = 31 * _hash + delta;
            delta = note == null ? 0 : note.hashCode();
            _hash = 31 * _hash + delta;
            delta = createdTime == null ? 0 : createdTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
