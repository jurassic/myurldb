package com.cannyurl.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.FullNameStruct;
import com.myurldb.ws.stub.FullNameStructStub;


// Wrapper class + bean combo.
public class FullNameStructBean implements FullNameStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(FullNameStructBean.class.getName());

    // [1] With an embedded object.
    private FullNameStructStub stub = null;

    // [2] Or, without an embedded object.
    private String uuid;
    private String displayName;
    private String lastName;
    private String firstName;
    private String middleName1;
    private String middleName2;
    private String middleInitial;
    private String salutation;
    private String suffix;
    private String note;

    // Ctors.
    public FullNameStructBean()
    {
        //this((String) null);
    }
    public FullNameStructBean(String uuid, String displayName, String lastName, String firstName, String middleName1, String middleName2, String middleInitial, String salutation, String suffix, String note)
    {
        this.uuid = uuid;
        this.displayName = displayName;
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName1 = middleName1;
        this.middleName2 = middleName2;
        this.middleInitial = middleInitial;
        this.salutation = salutation;
        this.suffix = suffix;
        this.note = note;
    }
    public FullNameStructBean(FullNameStruct stub)
    {
        if(stub instanceof FullNameStructStub) {
            this.stub = (FullNameStructStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setUuid(stub.getUuid());   
            setDisplayName(stub.getDisplayName());   
            setLastName(stub.getLastName());   
            setFirstName(stub.getFirstName());   
            setMiddleName1(stub.getMiddleName1());   
            setMiddleName2(stub.getMiddleName2());   
            setMiddleInitial(stub.getMiddleInitial());   
            setSalutation(stub.getSalutation());   
            setSuffix(stub.getSuffix());   
            setNote(stub.getNote());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getUuid()
    {
        if(getStub() != null) {
            return getStub().getUuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.uuid;
        }
    }
    public void setUuid(String uuid)
    {
        if(getStub() != null) {
            getStub().setUuid(uuid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.uuid = uuid;
        }
    }

    public String getDisplayName()
    {
        if(getStub() != null) {
            return getStub().getDisplayName();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.displayName;
        }
    }
    public void setDisplayName(String displayName)
    {
        if(getStub() != null) {
            getStub().setDisplayName(displayName);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.displayName = displayName;
        }
    }

    public String getLastName()
    {
        if(getStub() != null) {
            return getStub().getLastName();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.lastName;
        }
    }
    public void setLastName(String lastName)
    {
        if(getStub() != null) {
            getStub().setLastName(lastName);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.lastName = lastName;
        }
    }

    public String getFirstName()
    {
        if(getStub() != null) {
            return getStub().getFirstName();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.firstName;
        }
    }
    public void setFirstName(String firstName)
    {
        if(getStub() != null) {
            getStub().setFirstName(firstName);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.firstName = firstName;
        }
    }

    public String getMiddleName1()
    {
        if(getStub() != null) {
            return getStub().getMiddleName1();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.middleName1;
        }
    }
    public void setMiddleName1(String middleName1)
    {
        if(getStub() != null) {
            getStub().setMiddleName1(middleName1);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.middleName1 = middleName1;
        }
    }

    public String getMiddleName2()
    {
        if(getStub() != null) {
            return getStub().getMiddleName2();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.middleName2;
        }
    }
    public void setMiddleName2(String middleName2)
    {
        if(getStub() != null) {
            getStub().setMiddleName2(middleName2);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.middleName2 = middleName2;
        }
    }

    public String getMiddleInitial()
    {
        if(getStub() != null) {
            return getStub().getMiddleInitial();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.middleInitial;
        }
    }
    public void setMiddleInitial(String middleInitial)
    {
        if(getStub() != null) {
            getStub().setMiddleInitial(middleInitial);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.middleInitial = middleInitial;
        }
    }

    public String getSalutation()
    {
        if(getStub() != null) {
            return getStub().getSalutation();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.salutation;
        }
    }
    public void setSalutation(String salutation)
    {
        if(getStub() != null) {
            getStub().setSalutation(salutation);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.salutation = salutation;
        }
    }

    public String getSuffix()
    {
        if(getStub() != null) {
            return getStub().getSuffix();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.suffix;
        }
    }
    public void setSuffix(String suffix)
    {
        if(getStub() != null) {
            getStub().setSuffix(suffix);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.suffix = suffix;
        }
    }

    public String getNote()
    {
        if(getStub() != null) {
            return getStub().getNote();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.note;
        }
    }
    public void setNote(String note)
    {
        if(getStub() != null) {
            getStub().setNote(note);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.note = note;
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getDisplayName() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLastName() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getFirstName() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getMiddleName1() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getMiddleName2() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getMiddleInitial() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSalutation() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSuffix() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // Returns the reference to the embedded object. (Could be null.)
    public FullNameStructStub getStub()
    {
        return this.stub;
    }
    protected void setStub(FullNameStructStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("uuid = " + this.uuid).append(";");
            sb.append("displayName = " + this.displayName).append(";");
            sb.append("lastName = " + this.lastName).append(";");
            sb.append("firstName = " + this.firstName).append(";");
            sb.append("middleName1 = " + this.middleName1).append(";");
            sb.append("middleName2 = " + this.middleName2).append(";");
            sb.append("middleInitial = " + this.middleInitial).append(";");
            sb.append("salutation = " + this.salutation).append(";");
            sb.append("suffix = " + this.suffix).append(";");
            sb.append("note = " + this.note).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = uuid == null ? 0 : uuid.hashCode();
            _hash = 31 * _hash + delta;
            delta = displayName == null ? 0 : displayName.hashCode();
            _hash = 31 * _hash + delta;
            delta = lastName == null ? 0 : lastName.hashCode();
            _hash = 31 * _hash + delta;
            delta = firstName == null ? 0 : firstName.hashCode();
            _hash = 31 * _hash + delta;
            delta = middleName1 == null ? 0 : middleName1.hashCode();
            _hash = 31 * _hash + delta;
            delta = middleName2 == null ? 0 : middleName2.hashCode();
            _hash = 31 * _hash + delta;
            delta = middleInitial == null ? 0 : middleInitial.hashCode();
            _hash = 31 * _hash + delta;
            delta = salutation == null ? 0 : salutation.hashCode();
            _hash = 31 * _hash + delta;
            delta = suffix == null ? 0 : suffix.hashCode();
            _hash = 31 * _hash + delta;
            delta = note == null ? 0 : note.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
