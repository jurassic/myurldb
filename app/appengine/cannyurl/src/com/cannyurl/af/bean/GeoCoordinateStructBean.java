package com.cannyurl.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.GeoCoordinateStruct;
import com.myurldb.ws.stub.GeoCoordinateStructStub;


// Wrapper class + bean combo.
public class GeoCoordinateStructBean implements GeoCoordinateStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(GeoCoordinateStructBean.class.getName());

    // [1] With an embedded object.
    private GeoCoordinateStructStub stub = null;

    // [2] Or, without an embedded object.
    private String uuid;
    private Double latitude;
    private Double longitude;
    private Double altitude;
    private Boolean sensorUsed;
    private String accuracy;
    private String altitudeAccuracy;
    private Double heading;
    private Double speed;
    private String note;

    // Ctors.
    public GeoCoordinateStructBean()
    {
        //this((String) null);
    }
    public GeoCoordinateStructBean(String uuid, Double latitude, Double longitude, Double altitude, Boolean sensorUsed, String accuracy, String altitudeAccuracy, Double heading, Double speed, String note)
    {
        this.uuid = uuid;
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
        this.sensorUsed = sensorUsed;
        this.accuracy = accuracy;
        this.altitudeAccuracy = altitudeAccuracy;
        this.heading = heading;
        this.speed = speed;
        this.note = note;
    }
    public GeoCoordinateStructBean(GeoCoordinateStruct stub)
    {
        if(stub instanceof GeoCoordinateStructStub) {
            this.stub = (GeoCoordinateStructStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setUuid(stub.getUuid());   
            setLatitude(stub.getLatitude());   
            setLongitude(stub.getLongitude());   
            setAltitude(stub.getAltitude());   
            setSensorUsed(stub.isSensorUsed());   
            setAccuracy(stub.getAccuracy());   
            setAltitudeAccuracy(stub.getAltitudeAccuracy());   
            setHeading(stub.getHeading());   
            setSpeed(stub.getSpeed());   
            setNote(stub.getNote());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getUuid()
    {
        if(getStub() != null) {
            return getStub().getUuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.uuid;
        }
    }
    public void setUuid(String uuid)
    {
        if(getStub() != null) {
            getStub().setUuid(uuid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.uuid = uuid;
        }
    }

    public Double getLatitude()
    {
        if(getStub() != null) {
            return getStub().getLatitude();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.latitude;
        }
    }
    public void setLatitude(Double latitude)
    {
        if(getStub() != null) {
            getStub().setLatitude(latitude);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.latitude = latitude;
        }
    }

    public Double getLongitude()
    {
        if(getStub() != null) {
            return getStub().getLongitude();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.longitude;
        }
    }
    public void setLongitude(Double longitude)
    {
        if(getStub() != null) {
            getStub().setLongitude(longitude);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.longitude = longitude;
        }
    }

    public Double getAltitude()
    {
        if(getStub() != null) {
            return getStub().getAltitude();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.altitude;
        }
    }
    public void setAltitude(Double altitude)
    {
        if(getStub() != null) {
            getStub().setAltitude(altitude);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.altitude = altitude;
        }
    }

    public Boolean isSensorUsed()
    {
        if(getStub() != null) {
            return getStub().isSensorUsed();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.sensorUsed;
        }
    }
    public void setSensorUsed(Boolean sensorUsed)
    {
        if(getStub() != null) {
            getStub().setSensorUsed(sensorUsed);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.sensorUsed = sensorUsed;
        }
    }

    public String getAccuracy()
    {
        if(getStub() != null) {
            return getStub().getAccuracy();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.accuracy;
        }
    }
    public void setAccuracy(String accuracy)
    {
        if(getStub() != null) {
            getStub().setAccuracy(accuracy);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.accuracy = accuracy;
        }
    }

    public String getAltitudeAccuracy()
    {
        if(getStub() != null) {
            return getStub().getAltitudeAccuracy();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.altitudeAccuracy;
        }
    }
    public void setAltitudeAccuracy(String altitudeAccuracy)
    {
        if(getStub() != null) {
            getStub().setAltitudeAccuracy(altitudeAccuracy);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.altitudeAccuracy = altitudeAccuracy;
        }
    }

    public Double getHeading()
    {
        if(getStub() != null) {
            return getStub().getHeading();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.heading;
        }
    }
    public void setHeading(Double heading)
    {
        if(getStub() != null) {
            getStub().setHeading(heading);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.heading = heading;
        }
    }

    public Double getSpeed()
    {
        if(getStub() != null) {
            return getStub().getSpeed();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.speed;
        }
    }
    public void setSpeed(Double speed)
    {
        if(getStub() != null) {
            getStub().setSpeed(speed);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.speed = speed;
        }
    }

    public String getNote()
    {
        if(getStub() != null) {
            return getStub().getNote();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.note;
        }
    }
    public void setNote(String note)
    {
        if(getStub() != null) {
            getStub().setNote(note);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.note = note;
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getLatitude() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLongitude() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAltitude() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && isSensorUsed() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAccuracy() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAltitudeAccuracy() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHeading() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSpeed() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // Returns the reference to the embedded object. (Could be null.)
    public GeoCoordinateStructStub getStub()
    {
        return this.stub;
    }
    protected void setStub(GeoCoordinateStructStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("uuid = " + this.uuid).append(";");
            sb.append("latitude = " + this.latitude).append(";");
            sb.append("longitude = " + this.longitude).append(";");
            sb.append("altitude = " + this.altitude).append(";");
            sb.append("sensorUsed = " + this.sensorUsed).append(";");
            sb.append("accuracy = " + this.accuracy).append(";");
            sb.append("altitudeAccuracy = " + this.altitudeAccuracy).append(";");
            sb.append("heading = " + this.heading).append(";");
            sb.append("speed = " + this.speed).append(";");
            sb.append("note = " + this.note).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = uuid == null ? 0 : uuid.hashCode();
            _hash = 31 * _hash + delta;
            delta = latitude == null ? 0 : latitude.hashCode();
            _hash = 31 * _hash + delta;
            delta = longitude == null ? 0 : longitude.hashCode();
            _hash = 31 * _hash + delta;
            delta = altitude == null ? 0 : altitude.hashCode();
            _hash = 31 * _hash + delta;
            delta = sensorUsed == null ? 0 : sensorUsed.hashCode();
            _hash = 31 * _hash + delta;
            delta = accuracy == null ? 0 : accuracy.hashCode();
            _hash = 31 * _hash + delta;
            delta = altitudeAccuracy == null ? 0 : altitudeAccuracy.hashCode();
            _hash = 31 * _hash + delta;
            delta = heading == null ? 0 : heading.hashCode();
            _hash = 31 * _hash + delta;
            delta = speed == null ? 0 : speed.hashCode();
            _hash = 31 * _hash + delta;
            delta = note == null ? 0 : note.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
