package com.cannyurl.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.CellLatitudeLongitude;
import com.myurldb.ws.GeoCoordinateStruct;
import com.myurldb.ws.stub.CellLatitudeLongitudeStub;
import com.myurldb.ws.stub.GeoCoordinateStructStub;
import com.myurldb.ws.GeoLink;
import com.myurldb.ws.stub.GeoLinkStub;


// Wrapper class + bean combo.
public class GeoLinkBean implements GeoLink, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(GeoLinkBean.class.getName());

    // [1] With an embedded object.
    private GeoLinkStub stub = null;

    // [2] Or, without an embedded object.
    private String guid;
    private String shortLink;
    private String shortUrl;
    private GeoCoordinateStructBean geoCoordinate;
    private CellLatitudeLongitudeBean geoCell;
    private String status;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public GeoLinkBean()
    {
        //this((String) null);
    }
    public GeoLinkBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null);
    }
    public GeoLinkBean(String guid, String shortLink, String shortUrl, GeoCoordinateStructBean geoCoordinate, CellLatitudeLongitudeBean geoCell, String status)
    {
        this(guid, shortLink, shortUrl, geoCoordinate, geoCell, status, null, null);
    }
    public GeoLinkBean(String guid, String shortLink, String shortUrl, GeoCoordinateStructBean geoCoordinate, CellLatitudeLongitudeBean geoCell, String status, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.shortLink = shortLink;
        this.shortUrl = shortUrl;
        this.geoCoordinate = geoCoordinate;
        this.geoCell = geoCell;
        this.status = status;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public GeoLinkBean(GeoLink stub)
    {
        if(stub instanceof GeoLinkStub) {
            this.stub = (GeoLinkStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setShortLink(stub.getShortLink());   
            setShortUrl(stub.getShortUrl());   
            GeoCoordinateStruct geoCoordinate = stub.getGeoCoordinate();
            if(geoCoordinate instanceof GeoCoordinateStructBean) {
                setGeoCoordinate((GeoCoordinateStructBean) geoCoordinate);   
            } else {
                setGeoCoordinate(new GeoCoordinateStructBean(geoCoordinate));   
            }
            CellLatitudeLongitude geoCell = stub.getGeoCell();
            if(geoCell instanceof CellLatitudeLongitudeBean) {
                setGeoCell((CellLatitudeLongitudeBean) geoCell);   
            } else {
                setGeoCell(new CellLatitudeLongitudeBean(geoCell));   
            }
            setStatus(stub.getStatus());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public String getShortLink()
    {
        if(getStub() != null) {
            return getStub().getShortLink();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.shortLink;
        }
    }
    public void setShortLink(String shortLink)
    {
        if(getStub() != null) {
            getStub().setShortLink(shortLink);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.shortLink = shortLink;
        }
    }

    public String getShortUrl()
    {
        if(getStub() != null) {
            return getStub().getShortUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.shortUrl;
        }
    }
    public void setShortUrl(String shortUrl)
    {
        if(getStub() != null) {
            getStub().setShortUrl(shortUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.shortUrl = shortUrl;
        }
    }

    public GeoCoordinateStruct getGeoCoordinate()
    {  
        if(getStub() != null) {
            // Note the object type.
            GeoCoordinateStruct _stub_field = getStub().getGeoCoordinate();
            if(_stub_field == null) {
                return null;
            } else {
                return new GeoCoordinateStructBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.geoCoordinate;
        }
    }
    public void setGeoCoordinate(GeoCoordinateStruct geoCoordinate)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setGeoCoordinate(geoCoordinate);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(geoCoordinate == null) {
                this.geoCoordinate = null;
            } else {
                if(geoCoordinate instanceof GeoCoordinateStructBean) {
                    this.geoCoordinate = (GeoCoordinateStructBean) geoCoordinate;
                } else {
                    this.geoCoordinate = new GeoCoordinateStructBean(geoCoordinate);
                }
            }
        }
    }

    public CellLatitudeLongitude getGeoCell()
    {  
        if(getStub() != null) {
            // Note the object type.
            CellLatitudeLongitude _stub_field = getStub().getGeoCell();
            if(_stub_field == null) {
                return null;
            } else {
                return new CellLatitudeLongitudeBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.geoCell;
        }
    }
    public void setGeoCell(CellLatitudeLongitude geoCell)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setGeoCell(geoCell);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(geoCell == null) {
                this.geoCell = null;
            } else {
                if(geoCell instanceof CellLatitudeLongitudeBean) {
                    this.geoCell = (CellLatitudeLongitudeBean) geoCell;
                } else {
                    this.geoCell = new CellLatitudeLongitudeBean(geoCell);
                }
            }
        }
    }

    public String getStatus()
    {
        if(getStub() != null) {
            return getStub().getStatus();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.status;
        }
    }
    public void setStatus(String status)
    {
        if(getStub() != null) {
            getStub().setStatus(status);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.status = status;
        }
    }

    public Long getCreatedTime()
    {
        if(getStub() != null) {
            return getStub().getCreatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.createdTime;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getStub() != null) {
            getStub().setCreatedTime(createdTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.createdTime = createdTime;
        }
    }

    public Long getModifiedTime()
    {
        if(getStub() != null) {
            return getStub().getModifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.modifiedTime;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getStub() != null) {
            getStub().setModifiedTime(modifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.modifiedTime = modifiedTime;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public GeoLinkStub getStub()
    {
        return this.stub;
    }
    protected void setStub(GeoLinkStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("guid = " + this.guid).append(";");
            sb.append("shortLink = " + this.shortLink).append(";");
            sb.append("shortUrl = " + this.shortUrl).append(";");
            sb.append("geoCoordinate = " + this.geoCoordinate).append(";");
            sb.append("geoCell = " + this.geoCell).append(";");
            sb.append("status = " + this.status).append(";");
            sb.append("createdTime = " + this.createdTime).append(";");
            sb.append("modifiedTime = " + this.modifiedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = guid == null ? 0 : guid.hashCode();
            _hash = 31 * _hash + delta;
            delta = shortLink == null ? 0 : shortLink.hashCode();
            _hash = 31 * _hash + delta;
            delta = shortUrl == null ? 0 : shortUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = geoCoordinate == null ? 0 : geoCoordinate.hashCode();
            _hash = 31 * _hash + delta;
            delta = geoCell == null ? 0 : geoCell.hashCode();
            _hash = 31 * _hash + delta;
            delta = status == null ? 0 : status.hashCode();
            _hash = 31 * _hash + delta;
            delta = createdTime == null ? 0 : createdTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
