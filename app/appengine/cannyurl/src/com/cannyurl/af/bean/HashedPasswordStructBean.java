package com.cannyurl.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.HashedPasswordStruct;
import com.myurldb.ws.stub.HashedPasswordStructStub;


// Wrapper class + bean combo.
public class HashedPasswordStructBean implements HashedPasswordStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(HashedPasswordStructBean.class.getName());

    // [1] With an embedded object.
    private HashedPasswordStructStub stub = null;

    // [2] Or, without an embedded object.
    private String uuid;
    private String plainText;
    private String hashedText;
    private String salt;
    private String algorithm;

    // Ctors.
    public HashedPasswordStructBean()
    {
        //this((String) null);
    }
    public HashedPasswordStructBean(String uuid, String plainText, String hashedText, String salt, String algorithm)
    {
        this.uuid = uuid;
        this.plainText = plainText;
        this.hashedText = hashedText;
        this.salt = salt;
        this.algorithm = algorithm;
    }
    public HashedPasswordStructBean(HashedPasswordStruct stub)
    {
        if(stub instanceof HashedPasswordStructStub) {
            this.stub = (HashedPasswordStructStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setUuid(stub.getUuid());   
            setPlainText(stub.getPlainText());   
            setHashedText(stub.getHashedText());   
            setSalt(stub.getSalt());   
            setAlgorithm(stub.getAlgorithm());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getUuid()
    {
        if(getStub() != null) {
            return getStub().getUuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.uuid;
        }
    }
    public void setUuid(String uuid)
    {
        if(getStub() != null) {
            getStub().setUuid(uuid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.uuid = uuid;
        }
    }

    public String getPlainText()
    {
        if(getStub() != null) {
            return getStub().getPlainText();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.plainText;
        }
    }
    public void setPlainText(String plainText)
    {
        if(getStub() != null) {
            getStub().setPlainText(plainText);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.plainText = plainText;
        }
    }

    public String getHashedText()
    {
        if(getStub() != null) {
            return getStub().getHashedText();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.hashedText;
        }
    }
    public void setHashedText(String hashedText)
    {
        if(getStub() != null) {
            getStub().setHashedText(hashedText);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.hashedText = hashedText;
        }
    }

    public String getSalt()
    {
        if(getStub() != null) {
            return getStub().getSalt();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.salt;
        }
    }
    public void setSalt(String salt)
    {
        if(getStub() != null) {
            getStub().setSalt(salt);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.salt = salt;
        }
    }

    public String getAlgorithm()
    {
        if(getStub() != null) {
            return getStub().getAlgorithm();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.algorithm;
        }
    }
    public void setAlgorithm(String algorithm)
    {
        if(getStub() != null) {
            getStub().setAlgorithm(algorithm);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.algorithm = algorithm;
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getPlainText() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHashedText() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSalt() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAlgorithm() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // Returns the reference to the embedded object. (Could be null.)
    public HashedPasswordStructStub getStub()
    {
        return this.stub;
    }
    protected void setStub(HashedPasswordStructStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("uuid = " + this.uuid).append(";");
            sb.append("plainText = " + this.plainText).append(";");
            sb.append("hashedText = " + this.hashedText).append(";");
            sb.append("salt = " + this.salt).append(";");
            sb.append("algorithm = " + this.algorithm).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = uuid == null ? 0 : uuid.hashCode();
            _hash = 31 * _hash + delta;
            delta = plainText == null ? 0 : plainText.hashCode();
            _hash = 31 * _hash + delta;
            delta = hashedText == null ? 0 : hashedText.hashCode();
            _hash = 31 * _hash + delta;
            delta = salt == null ? 0 : salt.hashCode();
            _hash = 31 * _hash + delta;
            delta = algorithm == null ? 0 : algorithm.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
