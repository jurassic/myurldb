package com.cannyurl.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.HelpNotice;
import com.myurldb.ws.stub.HelpNoticeStub;


// Wrapper class + bean combo.
public class HelpNoticeBean implements HelpNotice, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(HelpNoticeBean.class.getName());

    // [1] With an embedded object.
    private HelpNoticeStub stub = null;

    // [2] Or, without an embedded object.
    private String uuid;
    private String title;
    private String content;
    private String format;
    private String note;
    private String status;

    // Ctors.
    public HelpNoticeBean()
    {
        //this((String) null);
    }
    public HelpNoticeBean(String uuid, String title, String content, String format, String note, String status)
    {
        this.uuid = uuid;
        this.title = title;
        this.content = content;
        this.format = format;
        this.note = note;
        this.status = status;
    }
    public HelpNoticeBean(HelpNotice stub)
    {
        if(stub instanceof HelpNoticeStub) {
            this.stub = (HelpNoticeStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setUuid(stub.getUuid());   
            setTitle(stub.getTitle());   
            setContent(stub.getContent());   
            setFormat(stub.getFormat());   
            setNote(stub.getNote());   
            setStatus(stub.getStatus());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getUuid()
    {
        if(getStub() != null) {
            return getStub().getUuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.uuid;
        }
    }
    public void setUuid(String uuid)
    {
        if(getStub() != null) {
            getStub().setUuid(uuid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.uuid = uuid;
        }
    }

    public String getTitle()
    {
        if(getStub() != null) {
            return getStub().getTitle();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.title;
        }
    }
    public void setTitle(String title)
    {
        if(getStub() != null) {
            getStub().setTitle(title);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.title = title;
        }
    }

    public String getContent()
    {
        if(getStub() != null) {
            return getStub().getContent();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.content;
        }
    }
    public void setContent(String content)
    {
        if(getStub() != null) {
            getStub().setContent(content);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.content = content;
        }
    }

    public String getFormat()
    {
        if(getStub() != null) {
            return getStub().getFormat();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.format;
        }
    }
    public void setFormat(String format)
    {
        if(getStub() != null) {
            getStub().setFormat(format);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.format = format;
        }
    }

    public String getNote()
    {
        if(getStub() != null) {
            return getStub().getNote();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.note;
        }
    }
    public void setNote(String note)
    {
        if(getStub() != null) {
            getStub().setNote(note);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.note = note;
        }
    }

    public String getStatus()
    {
        if(getStub() != null) {
            return getStub().getStatus();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.status;
        }
    }
    public void setStatus(String status)
    {
        if(getStub() != null) {
            getStub().setStatus(status);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.status = status;
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getTitle() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getContent() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getFormat() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getStatus() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // Returns the reference to the embedded object. (Could be null.)
    public HelpNoticeStub getStub()
    {
        return this.stub;
    }
    protected void setStub(HelpNoticeStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("uuid = " + this.uuid).append(";");
            sb.append("title = " + this.title).append(";");
            sb.append("content = " + this.content).append(";");
            sb.append("format = " + this.format).append(";");
            sb.append("note = " + this.note).append(";");
            sb.append("status = " + this.status).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = uuid == null ? 0 : uuid.hashCode();
            _hash = 31 * _hash + delta;
            delta = title == null ? 0 : title.hashCode();
            _hash = 31 * _hash + delta;
            delta = content == null ? 0 : content.hashCode();
            _hash = 31 * _hash + delta;
            delta = format == null ? 0 : format.hashCode();
            _hash = 31 * _hash + delta;
            delta = note == null ? 0 : note.hashCode();
            _hash = 31 * _hash + delta;
            delta = status == null ? 0 : status.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
