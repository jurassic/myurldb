package com.cannyurl.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.KeyValuePairStruct;
import com.myurldb.ws.stub.KeyValuePairStructStub;


// Wrapper class + bean combo.
public class KeyValuePairStructBean implements KeyValuePairStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(KeyValuePairStructBean.class.getName());

    // [1] With an embedded object.
    private KeyValuePairStructStub stub = null;

    // [2] Or, without an embedded object.
    private String uuid;
    private String key;
    private String value;
    private String note;

    // Ctors.
    public KeyValuePairStructBean()
    {
        //this((String) null);
    }
    public KeyValuePairStructBean(String uuid, String key, String value, String note)
    {
        this.uuid = uuid;
        this.key = key;
        this.value = value;
        this.note = note;
    }
    public KeyValuePairStructBean(KeyValuePairStruct stub)
    {
        if(stub instanceof KeyValuePairStructStub) {
            this.stub = (KeyValuePairStructStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setUuid(stub.getUuid());   
            setKey(stub.getKey());   
            setValue(stub.getValue());   
            setNote(stub.getNote());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getUuid()
    {
        if(getStub() != null) {
            return getStub().getUuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.uuid;
        }
    }
    public void setUuid(String uuid)
    {
        if(getStub() != null) {
            getStub().setUuid(uuid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.uuid = uuid;
        }
    }

    public String getKey()
    {
        if(getStub() != null) {
            return getStub().getKey();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.key;
        }
    }
    public void setKey(String key)
    {
        if(getStub() != null) {
            getStub().setKey(key);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.key = key;
        }
    }

    public String getValue()
    {
        if(getStub() != null) {
            return getStub().getValue();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.value;
        }
    }
    public void setValue(String value)
    {
        if(getStub() != null) {
            getStub().setValue(value);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.value = value;
        }
    }

    public String getNote()
    {
        if(getStub() != null) {
            return getStub().getNote();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.note;
        }
    }
    public void setNote(String note)
    {
        if(getStub() != null) {
            getStub().setNote(note);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.note = note;
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getKey() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getValue() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // Returns the reference to the embedded object. (Could be null.)
    public KeyValuePairStructStub getStub()
    {
        return this.stub;
    }
    protected void setStub(KeyValuePairStructStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("uuid = " + this.uuid).append(";");
            sb.append("key = " + this.key).append(";");
            sb.append("value = " + this.value).append(";");
            sb.append("note = " + this.note).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = uuid == null ? 0 : uuid.hashCode();
            _hash = 31 * _hash + delta;
            delta = key == null ? 0 : key.hashCode();
            _hash = 31 * _hash + delta;
            delta = value == null ? 0 : value.hashCode();
            _hash = 31 * _hash + delta;
            delta = note == null ? 0 : note.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
