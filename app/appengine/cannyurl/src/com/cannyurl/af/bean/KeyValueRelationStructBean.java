package com.cannyurl.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.KeyValueRelationStruct;
import com.myurldb.ws.stub.KeyValueRelationStructStub;


// Wrapper class + bean combo.
public class KeyValueRelationStructBean implements KeyValueRelationStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(KeyValueRelationStructBean.class.getName());

    // [1] With an embedded object.
    private KeyValueRelationStructStub stub = null;

    // [2] Or, without an embedded object.
    private String uuid;
    private String key;
    private String value;
    private String note;
    private String relation;

    // Ctors.
    public KeyValueRelationStructBean()
    {
        //this((String) null);
    }
    public KeyValueRelationStructBean(String uuid, String key, String value, String note, String relation)
    {
        this.uuid = uuid;
        this.key = key;
        this.value = value;
        this.note = note;
        this.relation = relation;
    }
    public KeyValueRelationStructBean(KeyValueRelationStruct stub)
    {
        if(stub instanceof KeyValueRelationStructStub) {
            this.stub = (KeyValueRelationStructStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setUuid(stub.getUuid());   
            setKey(stub.getKey());   
            setValue(stub.getValue());   
            setNote(stub.getNote());   
            setRelation(stub.getRelation());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getUuid()
    {
        if(getStub() != null) {
            return getStub().getUuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.uuid;
        }
    }
    public void setUuid(String uuid)
    {
        if(getStub() != null) {
            getStub().setUuid(uuid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.uuid = uuid;
        }
    }

    public String getKey()
    {
        if(getStub() != null) {
            return getStub().getKey();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.key;
        }
    }
    public void setKey(String key)
    {
        if(getStub() != null) {
            getStub().setKey(key);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.key = key;
        }
    }

    public String getValue()
    {
        if(getStub() != null) {
            return getStub().getValue();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.value;
        }
    }
    public void setValue(String value)
    {
        if(getStub() != null) {
            getStub().setValue(value);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.value = value;
        }
    }

    public String getNote()
    {
        if(getStub() != null) {
            return getStub().getNote();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.note;
        }
    }
    public void setNote(String note)
    {
        if(getStub() != null) {
            getStub().setNote(note);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.note = note;
        }
    }

    public String getRelation()
    {
        if(getStub() != null) {
            return getStub().getRelation();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.relation;
        }
    }
    public void setRelation(String relation)
    {
        if(getStub() != null) {
            getStub().setRelation(relation);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.relation = relation;
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getKey() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getValue() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getRelation() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // Returns the reference to the embedded object. (Could be null.)
    public KeyValueRelationStructStub getStub()
    {
        return this.stub;
    }
    protected void setStub(KeyValueRelationStructStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("uuid = " + this.uuid).append(";");
            sb.append("key = " + this.key).append(";");
            sb.append("value = " + this.value).append(";");
            sb.append("note = " + this.note).append(";");
            sb.append("relation = " + this.relation).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = uuid == null ? 0 : uuid.hashCode();
            _hash = 31 * _hash + delta;
            delta = key == null ? 0 : key.hashCode();
            _hash = 31 * _hash + delta;
            delta = value == null ? 0 : value.hashCode();
            _hash = 31 * _hash + delta;
            delta = note == null ? 0 : note.hashCode();
            _hash = 31 * _hash + delta;
            delta = relation == null ? 0 : relation.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
