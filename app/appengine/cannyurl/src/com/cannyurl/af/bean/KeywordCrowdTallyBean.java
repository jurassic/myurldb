package com.cannyurl.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.KeywordCrowdTally;
import com.myurldb.ws.stub.CrowdTallyBaseStub;
import com.myurldb.ws.stub.KeywordCrowdTallyStub;


// Wrapper class + bean combo.
public class KeywordCrowdTallyBean extends CrowdTallyBaseBean implements KeywordCrowdTally, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(KeywordCrowdTallyBean.class.getName());


    // [2] Or, without an embedded object.
    private String keywordFolder;
    private String folderPath;
    private String keyword;
    private String queryKey;
    private String scope;
    private Boolean caseSensitive;

    // Ctors.
    public KeywordCrowdTallyBean()
    {
        //this((String) null);
    }
    public KeywordCrowdTallyBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public KeywordCrowdTallyBean(String guid, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, String tallyDate, String status, String note, Long expirationTime, String keywordFolder, String folderPath, String keyword, String queryKey, String scope, Boolean caseSensitive)
    {
        this(guid, user, shortLink, domain, token, longUrl, shortUrl, tallyDate, status, note, expirationTime, keywordFolder, folderPath, keyword, queryKey, scope, caseSensitive, null, null);
    }
    public KeywordCrowdTallyBean(String guid, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, String tallyDate, String status, String note, Long expirationTime, String keywordFolder, String folderPath, String keyword, String queryKey, String scope, Boolean caseSensitive, Long createdTime, Long modifiedTime)
    {
        super(guid, user, shortLink, domain, token, longUrl, shortUrl, tallyDate, status, note, expirationTime, createdTime, modifiedTime);

        this.keywordFolder = keywordFolder;
        this.folderPath = folderPath;
        this.keyword = keyword;
        this.queryKey = queryKey;
        this.scope = scope;
        this.caseSensitive = caseSensitive;
    }
    public KeywordCrowdTallyBean(KeywordCrowdTally stub)
    {
        if(stub instanceof KeywordCrowdTallyStub) {
            super.setStub((CrowdTallyBaseStub) stub);
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setUser(stub.getUser());   
            setShortLink(stub.getShortLink());   
            setDomain(stub.getDomain());   
            setToken(stub.getToken());   
            setLongUrl(stub.getLongUrl());   
            setShortUrl(stub.getShortUrl());   
            setTallyDate(stub.getTallyDate());   
            setStatus(stub.getStatus());   
            setNote(stub.getNote());   
            setExpirationTime(stub.getExpirationTime());   
            setKeywordFolder(stub.getKeywordFolder());   
            setFolderPath(stub.getFolderPath());   
            setKeyword(stub.getKeyword());   
            setQueryKey(stub.getQueryKey());   
            setScope(stub.getScope());   
            setCaseSensitive(stub.isCaseSensitive());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    public String getUser()
    {
        return super.getUser();
    }
    public void setUser(String user)
    {
        super.setUser(user);
    }

    public String getShortLink()
    {
        return super.getShortLink();
    }
    public void setShortLink(String shortLink)
    {
        super.setShortLink(shortLink);
    }

    public String getDomain()
    {
        return super.getDomain();
    }
    public void setDomain(String domain)
    {
        super.setDomain(domain);
    }

    public String getToken()
    {
        return super.getToken();
    }
    public void setToken(String token)
    {
        super.setToken(token);
    }

    public String getLongUrl()
    {
        return super.getLongUrl();
    }
    public void setLongUrl(String longUrl)
    {
        super.setLongUrl(longUrl);
    }

    public String getShortUrl()
    {
        return super.getShortUrl();
    }
    public void setShortUrl(String shortUrl)
    {
        super.setShortUrl(shortUrl);
    }

    public String getTallyDate()
    {
        return super.getTallyDate();
    }
    public void setTallyDate(String tallyDate)
    {
        super.setTallyDate(tallyDate);
    }

    public String getStatus()
    {
        return super.getStatus();
    }
    public void setStatus(String status)
    {
        super.setStatus(status);
    }

    public String getNote()
    {
        return super.getNote();
    }
    public void setNote(String note)
    {
        super.setNote(note);
    }

    public Long getExpirationTime()
    {
        return super.getExpirationTime();
    }
    public void setExpirationTime(Long expirationTime)
    {
        super.setExpirationTime(expirationTime);
    }

    public String getKeywordFolder()
    {
        if(getStub() != null) {
            return getStub().getKeywordFolder();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.keywordFolder;
        }
    }
    public void setKeywordFolder(String keywordFolder)
    {
        if(getStub() != null) {
            getStub().setKeywordFolder(keywordFolder);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.keywordFolder = keywordFolder;
        }
    }

    public String getFolderPath()
    {
        if(getStub() != null) {
            return getStub().getFolderPath();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.folderPath;
        }
    }
    public void setFolderPath(String folderPath)
    {
        if(getStub() != null) {
            getStub().setFolderPath(folderPath);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.folderPath = folderPath;
        }
    }

    public String getKeyword()
    {
        if(getStub() != null) {
            return getStub().getKeyword();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.keyword;
        }
    }
    public void setKeyword(String keyword)
    {
        if(getStub() != null) {
            getStub().setKeyword(keyword);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.keyword = keyword;
        }
    }

    public String getQueryKey()
    {
        if(getStub() != null) {
            return getStub().getQueryKey();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.queryKey;
        }
    }
    public void setQueryKey(String queryKey)
    {
        if(getStub() != null) {
            getStub().setQueryKey(queryKey);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.queryKey = queryKey;
        }
    }

    public String getScope()
    {
        if(getStub() != null) {
            return getStub().getScope();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.scope;
        }
    }
    public void setScope(String scope)
    {
        if(getStub() != null) {
            getStub().setScope(scope);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.scope = scope;
        }
    }

    public Boolean isCaseSensitive()
    {
        if(getStub() != null) {
            return getStub().isCaseSensitive();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.caseSensitive;
        }
    }
    public void setCaseSensitive(Boolean caseSensitive)
    {
        if(getStub() != null) {
            getStub().setCaseSensitive(caseSensitive);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.caseSensitive = caseSensitive;
        }
    }

    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    // Returns the reference to the embedded object. (Could be null.)
    public KeywordCrowdTallyStub getStub()
    {
        return (KeywordCrowdTallyStub) super.getStub();
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder( super.toString() );
            sb.append("keywordFolder = " + this.keywordFolder).append(";");
            sb.append("folderPath = " + this.folderPath).append(";");
            sb.append("keyword = " + this.keyword).append(";");
            sb.append("queryKey = " + this.queryKey).append(";");
            sb.append("scope = " + this.scope).append(";");
            sb.append("caseSensitive = " + this.caseSensitive).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = keywordFolder == null ? 0 : keywordFolder.hashCode();
            _hash = 31 * _hash + delta;
            delta = folderPath == null ? 0 : folderPath.hashCode();
            _hash = 31 * _hash + delta;
            delta = keyword == null ? 0 : keyword.hashCode();
            _hash = 31 * _hash + delta;
            delta = queryKey == null ? 0 : queryKey.hashCode();
            _hash = 31 * _hash + delta;
            delta = scope == null ? 0 : scope.hashCode();
            _hash = 31 * _hash + delta;
            delta = caseSensitive == null ? 0 : caseSensitive.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
