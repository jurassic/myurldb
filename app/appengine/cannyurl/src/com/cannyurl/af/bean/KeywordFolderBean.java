package com.cannyurl.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.KeywordFolder;
import com.myurldb.ws.stub.FolderBaseStub;
import com.myurldb.ws.stub.KeywordFolderStub;


// Wrapper class + bean combo.
public class KeywordFolderBean extends FolderBaseBean implements KeywordFolder, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(KeywordFolderBean.class.getName());


    // [2] Or, without an embedded object.
    private String folderPath;

    // Ctors.
    public KeywordFolderBean()
    {
        //this((String) null);
    }
    public KeywordFolderBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public KeywordFolderBean(String guid, String appClient, String clientRootDomain, String user, String title, String description, String type, String category, String parent, String aggregate, String acl, Boolean exportable, String status, String note, String folderPath)
    {
        this(guid, appClient, clientRootDomain, user, title, description, type, category, parent, aggregate, acl, exportable, status, note, folderPath, null, null);
    }
    public KeywordFolderBean(String guid, String appClient, String clientRootDomain, String user, String title, String description, String type, String category, String parent, String aggregate, String acl, Boolean exportable, String status, String note, String folderPath, Long createdTime, Long modifiedTime)
    {
        super(guid, appClient, clientRootDomain, user, title, description, type, category, parent, aggregate, acl, exportable, status, note, createdTime, modifiedTime);

        this.folderPath = folderPath;
    }
    public KeywordFolderBean(KeywordFolder stub)
    {
        if(stub instanceof KeywordFolderStub) {
            super.setStub((FolderBaseStub) stub);
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setAppClient(stub.getAppClient());   
            setClientRootDomain(stub.getClientRootDomain());   
            setUser(stub.getUser());   
            setTitle(stub.getTitle());   
            setDescription(stub.getDescription());   
            setType(stub.getType());   
            setCategory(stub.getCategory());   
            setParent(stub.getParent());   
            setAggregate(stub.getAggregate());   
            setAcl(stub.getAcl());   
            setExportable(stub.isExportable());   
            setStatus(stub.getStatus());   
            setNote(stub.getNote());   
            setFolderPath(stub.getFolderPath());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    public String getAppClient()
    {
        return super.getAppClient();
    }
    public void setAppClient(String appClient)
    {
        super.setAppClient(appClient);
    }

    public String getClientRootDomain()
    {
        return super.getClientRootDomain();
    }
    public void setClientRootDomain(String clientRootDomain)
    {
        super.setClientRootDomain(clientRootDomain);
    }

    public String getUser()
    {
        return super.getUser();
    }
    public void setUser(String user)
    {
        super.setUser(user);
    }

    public String getTitle()
    {
        return super.getTitle();
    }
    public void setTitle(String title)
    {
        super.setTitle(title);
    }

    public String getDescription()
    {
        return super.getDescription();
    }
    public void setDescription(String description)
    {
        super.setDescription(description);
    }

    public String getType()
    {
        return super.getType();
    }
    public void setType(String type)
    {
        super.setType(type);
    }

    public String getCategory()
    {
        return super.getCategory();
    }
    public void setCategory(String category)
    {
        super.setCategory(category);
    }

    public String getParent()
    {
        return super.getParent();
    }
    public void setParent(String parent)
    {
        super.setParent(parent);
    }

    public String getAggregate()
    {
        return super.getAggregate();
    }
    public void setAggregate(String aggregate)
    {
        super.setAggregate(aggregate);
    }

    public String getAcl()
    {
        return super.getAcl();
    }
    public void setAcl(String acl)
    {
        super.setAcl(acl);
    }

    public Boolean isExportable()
    {
        return super.isExportable();
    }
    public void setExportable(Boolean exportable)
    {
        super.setExportable(exportable);
    }

    public String getStatus()
    {
        return super.getStatus();
    }
    public void setStatus(String status)
    {
        super.setStatus(status);
    }

    public String getNote()
    {
        return super.getNote();
    }
    public void setNote(String note)
    {
        super.setNote(note);
    }

    public String getFolderPath()
    {
        if(getStub() != null) {
            return getStub().getFolderPath();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.folderPath;
        }
    }
    public void setFolderPath(String folderPath)
    {
        if(getStub() != null) {
            getStub().setFolderPath(folderPath);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.folderPath = folderPath;
        }
    }

    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    // Returns the reference to the embedded object. (Could be null.)
    public KeywordFolderStub getStub()
    {
        return (KeywordFolderStub) super.getStub();
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder( super.toString() );
            sb.append("folderPath = " + this.folderPath).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = folderPath == null ? 0 : folderPath.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
