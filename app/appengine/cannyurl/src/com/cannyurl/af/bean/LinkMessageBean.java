package com.cannyurl.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.LinkMessage;
import com.myurldb.ws.stub.LinkMessageStub;


// Wrapper class + bean combo.
public class LinkMessageBean implements LinkMessage, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(LinkMessageBean.class.getName());

    // [1] With an embedded object.
    private LinkMessageStub stub = null;

    // [2] Or, without an embedded object.
    private String guid;
    private String shortLink;
    private String message;
    private String password;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public LinkMessageBean()
    {
        //this((String) null);
    }
    public LinkMessageBean(String guid)
    {
        this(guid, null, null, null, null, null);
    }
    public LinkMessageBean(String guid, String shortLink, String message, String password)
    {
        this(guid, shortLink, message, password, null, null);
    }
    public LinkMessageBean(String guid, String shortLink, String message, String password, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.shortLink = shortLink;
        this.message = message;
        this.password = password;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public LinkMessageBean(LinkMessage stub)
    {
        if(stub instanceof LinkMessageStub) {
            this.stub = (LinkMessageStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setShortLink(stub.getShortLink());   
            setMessage(stub.getMessage());   
            setPassword(stub.getPassword());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public String getShortLink()
    {
        if(getStub() != null) {
            return getStub().getShortLink();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.shortLink;
        }
    }
    public void setShortLink(String shortLink)
    {
        if(getStub() != null) {
            getStub().setShortLink(shortLink);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.shortLink = shortLink;
        }
    }

    public String getMessage()
    {
        if(getStub() != null) {
            return getStub().getMessage();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.message;
        }
    }
    public void setMessage(String message)
    {
        if(getStub() != null) {
            getStub().setMessage(message);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.message = message;
        }
    }

    public String getPassword()
    {
        if(getStub() != null) {
            return getStub().getPassword();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.password;
        }
    }
    public void setPassword(String password)
    {
        if(getStub() != null) {
            getStub().setPassword(password);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.password = password;
        }
    }

    public Long getCreatedTime()
    {
        if(getStub() != null) {
            return getStub().getCreatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.createdTime;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getStub() != null) {
            getStub().setCreatedTime(createdTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.createdTime = createdTime;
        }
    }

    public Long getModifiedTime()
    {
        if(getStub() != null) {
            return getStub().getModifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.modifiedTime;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getStub() != null) {
            getStub().setModifiedTime(modifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.modifiedTime = modifiedTime;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public LinkMessageStub getStub()
    {
        return this.stub;
    }
    protected void setStub(LinkMessageStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("guid = " + this.guid).append(";");
            sb.append("shortLink = " + this.shortLink).append(";");
            sb.append("message = " + this.message).append(";");
            sb.append("password = " + this.password).append(";");
            sb.append("createdTime = " + this.createdTime).append(";");
            sb.append("modifiedTime = " + this.modifiedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = guid == null ? 0 : guid.hashCode();
            _hash = 31 * _hash + delta;
            delta = shortLink == null ? 0 : shortLink.hashCode();
            _hash = 31 * _hash + delta;
            delta = message == null ? 0 : message.hashCode();
            _hash = 31 * _hash + delta;
            delta = password == null ? 0 : password.hashCode();
            _hash = 31 * _hash + delta;
            delta = createdTime == null ? 0 : createdTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
