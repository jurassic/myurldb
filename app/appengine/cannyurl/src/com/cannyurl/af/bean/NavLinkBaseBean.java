package com.cannyurl.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.NavLinkBase;
import com.myurldb.ws.stub.NavLinkBaseStub;


// Wrapper class + bean combo.
public abstract class NavLinkBaseBean implements NavLinkBase, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(NavLinkBaseBean.class.getName());

    // [1] With an embedded object.
    private NavLinkBaseStub stub = null;

    // [2] Or, without an embedded object.
    private String guid;
    private String appClient;
    private String clientRootDomain;
    private String user;
    private String shortLink;
    private String domain;
    private String token;
    private String longUrl;
    private String shortUrl;
    private Boolean internal;
    private Boolean caching;
    private String memo;
    private String status;
    private String note;
    private Long expirationTime;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public NavLinkBaseBean()
    {
        //this((String) null);
    }
    public NavLinkBaseBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public NavLinkBaseBean(String guid, String appClient, String clientRootDomain, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, Boolean internal, Boolean caching, String memo, String status, String note, Long expirationTime)
    {
        this(guid, appClient, clientRootDomain, user, shortLink, domain, token, longUrl, shortUrl, internal, caching, memo, status, note, expirationTime, null, null);
    }
    public NavLinkBaseBean(String guid, String appClient, String clientRootDomain, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, Boolean internal, Boolean caching, String memo, String status, String note, Long expirationTime, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.appClient = appClient;
        this.clientRootDomain = clientRootDomain;
        this.user = user;
        this.shortLink = shortLink;
        this.domain = domain;
        this.token = token;
        this.longUrl = longUrl;
        this.shortUrl = shortUrl;
        this.internal = internal;
        this.caching = caching;
        this.memo = memo;
        this.status = status;
        this.note = note;
        this.expirationTime = expirationTime;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public NavLinkBaseBean(NavLinkBase stub)
    {
        if(stub instanceof NavLinkBaseStub) {
            this.stub = (NavLinkBaseStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setAppClient(stub.getAppClient());   
            setClientRootDomain(stub.getClientRootDomain());   
            setUser(stub.getUser());   
            setShortLink(stub.getShortLink());   
            setDomain(stub.getDomain());   
            setToken(stub.getToken());   
            setLongUrl(stub.getLongUrl());   
            setShortUrl(stub.getShortUrl());   
            setInternal(stub.isInternal());   
            setCaching(stub.isCaching());   
            setMemo(stub.getMemo());   
            setStatus(stub.getStatus());   
            setNote(stub.getNote());   
            setExpirationTime(stub.getExpirationTime());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public String getAppClient()
    {
        if(getStub() != null) {
            return getStub().getAppClient();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.appClient;
        }
    }
    public void setAppClient(String appClient)
    {
        if(getStub() != null) {
            getStub().setAppClient(appClient);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.appClient = appClient;
        }
    }

    public String getClientRootDomain()
    {
        if(getStub() != null) {
            return getStub().getClientRootDomain();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.clientRootDomain;
        }
    }
    public void setClientRootDomain(String clientRootDomain)
    {
        if(getStub() != null) {
            getStub().setClientRootDomain(clientRootDomain);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.clientRootDomain = clientRootDomain;
        }
    }

    public String getUser()
    {
        if(getStub() != null) {
            return getStub().getUser();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.user;
        }
    }
    public void setUser(String user)
    {
        if(getStub() != null) {
            getStub().setUser(user);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.user = user;
        }
    }

    public String getShortLink()
    {
        if(getStub() != null) {
            return getStub().getShortLink();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.shortLink;
        }
    }
    public void setShortLink(String shortLink)
    {
        if(getStub() != null) {
            getStub().setShortLink(shortLink);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.shortLink = shortLink;
        }
    }

    public String getDomain()
    {
        if(getStub() != null) {
            return getStub().getDomain();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.domain;
        }
    }
    public void setDomain(String domain)
    {
        if(getStub() != null) {
            getStub().setDomain(domain);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.domain = domain;
        }
    }

    public String getToken()
    {
        if(getStub() != null) {
            return getStub().getToken();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.token;
        }
    }
    public void setToken(String token)
    {
        if(getStub() != null) {
            getStub().setToken(token);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.token = token;
        }
    }

    public String getLongUrl()
    {
        if(getStub() != null) {
            return getStub().getLongUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.longUrl;
        }
    }
    public void setLongUrl(String longUrl)
    {
        if(getStub() != null) {
            getStub().setLongUrl(longUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.longUrl = longUrl;
        }
    }

    public String getShortUrl()
    {
        if(getStub() != null) {
            return getStub().getShortUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.shortUrl;
        }
    }
    public void setShortUrl(String shortUrl)
    {
        if(getStub() != null) {
            getStub().setShortUrl(shortUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.shortUrl = shortUrl;
        }
    }

    public Boolean isInternal()
    {
        if(getStub() != null) {
            return getStub().isInternal();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.internal;
        }
    }
    public void setInternal(Boolean internal)
    {
        if(getStub() != null) {
            getStub().setInternal(internal);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.internal = internal;
        }
    }

    public Boolean isCaching()
    {
        if(getStub() != null) {
            return getStub().isCaching();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.caching;
        }
    }
    public void setCaching(Boolean caching)
    {
        if(getStub() != null) {
            getStub().setCaching(caching);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.caching = caching;
        }
    }

    public String getMemo()
    {
        if(getStub() != null) {
            return getStub().getMemo();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.memo;
        }
    }
    public void setMemo(String memo)
    {
        if(getStub() != null) {
            getStub().setMemo(memo);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.memo = memo;
        }
    }

    public String getStatus()
    {
        if(getStub() != null) {
            return getStub().getStatus();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.status;
        }
    }
    public void setStatus(String status)
    {
        if(getStub() != null) {
            getStub().setStatus(status);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.status = status;
        }
    }

    public String getNote()
    {
        if(getStub() != null) {
            return getStub().getNote();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.note;
        }
    }
    public void setNote(String note)
    {
        if(getStub() != null) {
            getStub().setNote(note);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.note = note;
        }
    }

    public Long getExpirationTime()
    {
        if(getStub() != null) {
            return getStub().getExpirationTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.expirationTime;
        }
    }
    public void setExpirationTime(Long expirationTime)
    {
        if(getStub() != null) {
            getStub().setExpirationTime(expirationTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.expirationTime = expirationTime;
        }
    }

    public Long getCreatedTime()
    {
        if(getStub() != null) {
            return getStub().getCreatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.createdTime;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getStub() != null) {
            getStub().setCreatedTime(createdTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.createdTime = createdTime;
        }
    }

    public Long getModifiedTime()
    {
        if(getStub() != null) {
            return getStub().getModifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.modifiedTime;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getStub() != null) {
            getStub().setModifiedTime(modifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.modifiedTime = modifiedTime;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public NavLinkBaseStub getStub()
    {
        return this.stub;
    }
    protected void setStub(NavLinkBaseStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("guid = " + this.guid).append(";");
            sb.append("appClient = " + this.appClient).append(";");
            sb.append("clientRootDomain = " + this.clientRootDomain).append(";");
            sb.append("user = " + this.user).append(";");
            sb.append("shortLink = " + this.shortLink).append(";");
            sb.append("domain = " + this.domain).append(";");
            sb.append("token = " + this.token).append(";");
            sb.append("longUrl = " + this.longUrl).append(";");
            sb.append("shortUrl = " + this.shortUrl).append(";");
            sb.append("internal = " + this.internal).append(";");
            sb.append("caching = " + this.caching).append(";");
            sb.append("memo = " + this.memo).append(";");
            sb.append("status = " + this.status).append(";");
            sb.append("note = " + this.note).append(";");
            sb.append("expirationTime = " + this.expirationTime).append(";");
            sb.append("createdTime = " + this.createdTime).append(";");
            sb.append("modifiedTime = " + this.modifiedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = guid == null ? 0 : guid.hashCode();
            _hash = 31 * _hash + delta;
            delta = appClient == null ? 0 : appClient.hashCode();
            _hash = 31 * _hash + delta;
            delta = clientRootDomain == null ? 0 : clientRootDomain.hashCode();
            _hash = 31 * _hash + delta;
            delta = user == null ? 0 : user.hashCode();
            _hash = 31 * _hash + delta;
            delta = shortLink == null ? 0 : shortLink.hashCode();
            _hash = 31 * _hash + delta;
            delta = domain == null ? 0 : domain.hashCode();
            _hash = 31 * _hash + delta;
            delta = token == null ? 0 : token.hashCode();
            _hash = 31 * _hash + delta;
            delta = longUrl == null ? 0 : longUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = shortUrl == null ? 0 : shortUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = internal == null ? 0 : internal.hashCode();
            _hash = 31 * _hash + delta;
            delta = caching == null ? 0 : caching.hashCode();
            _hash = 31 * _hash + delta;
            delta = memo == null ? 0 : memo.hashCode();
            _hash = 31 * _hash + delta;
            delta = status == null ? 0 : status.hashCode();
            _hash = 31 * _hash + delta;
            delta = note == null ? 0 : note.hashCode();
            _hash = 31 * _hash + delta;
            delta = expirationTime == null ? 0 : expirationTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = createdTime == null ? 0 : createdTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
