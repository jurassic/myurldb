package com.cannyurl.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.QrCode;
import com.myurldb.ws.stub.QrCodeStub;


// Wrapper class + bean combo.
public class QrCodeBean implements QrCode, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(QrCodeBean.class.getName());

    // [1] With an embedded object.
    private QrCodeStub stub = null;

    // [2] Or, without an embedded object.
    private String guid;
    private String shortLink;
    private String imageLink;
    private String imageUrl;
    private String type;
    private String status;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public QrCodeBean()
    {
        //this((String) null);
    }
    public QrCodeBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null);
    }
    public QrCodeBean(String guid, String shortLink, String imageLink, String imageUrl, String type, String status)
    {
        this(guid, shortLink, imageLink, imageUrl, type, status, null, null);
    }
    public QrCodeBean(String guid, String shortLink, String imageLink, String imageUrl, String type, String status, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.shortLink = shortLink;
        this.imageLink = imageLink;
        this.imageUrl = imageUrl;
        this.type = type;
        this.status = status;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public QrCodeBean(QrCode stub)
    {
        if(stub instanceof QrCodeStub) {
            this.stub = (QrCodeStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setShortLink(stub.getShortLink());   
            setImageLink(stub.getImageLink());   
            setImageUrl(stub.getImageUrl());   
            setType(stub.getType());   
            setStatus(stub.getStatus());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public String getShortLink()
    {
        if(getStub() != null) {
            return getStub().getShortLink();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.shortLink;
        }
    }
    public void setShortLink(String shortLink)
    {
        if(getStub() != null) {
            getStub().setShortLink(shortLink);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.shortLink = shortLink;
        }
    }

    public String getImageLink()
    {
        if(getStub() != null) {
            return getStub().getImageLink();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.imageLink;
        }
    }
    public void setImageLink(String imageLink)
    {
        if(getStub() != null) {
            getStub().setImageLink(imageLink);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.imageLink = imageLink;
        }
    }

    public String getImageUrl()
    {
        if(getStub() != null) {
            return getStub().getImageUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.imageUrl;
        }
    }
    public void setImageUrl(String imageUrl)
    {
        if(getStub() != null) {
            getStub().setImageUrl(imageUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.imageUrl = imageUrl;
        }
    }

    public String getType()
    {
        if(getStub() != null) {
            return getStub().getType();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.type;
        }
    }
    public void setType(String type)
    {
        if(getStub() != null) {
            getStub().setType(type);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.type = type;
        }
    }

    public String getStatus()
    {
        if(getStub() != null) {
            return getStub().getStatus();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.status;
        }
    }
    public void setStatus(String status)
    {
        if(getStub() != null) {
            getStub().setStatus(status);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.status = status;
        }
    }

    public Long getCreatedTime()
    {
        if(getStub() != null) {
            return getStub().getCreatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.createdTime;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getStub() != null) {
            getStub().setCreatedTime(createdTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.createdTime = createdTime;
        }
    }

    public Long getModifiedTime()
    {
        if(getStub() != null) {
            return getStub().getModifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.modifiedTime;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getStub() != null) {
            getStub().setModifiedTime(modifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.modifiedTime = modifiedTime;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public QrCodeStub getStub()
    {
        return this.stub;
    }
    protected void setStub(QrCodeStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("guid = " + this.guid).append(";");
            sb.append("shortLink = " + this.shortLink).append(";");
            sb.append("imageLink = " + this.imageLink).append(";");
            sb.append("imageUrl = " + this.imageUrl).append(";");
            sb.append("type = " + this.type).append(";");
            sb.append("status = " + this.status).append(";");
            sb.append("createdTime = " + this.createdTime).append(";");
            sb.append("modifiedTime = " + this.modifiedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = guid == null ? 0 : guid.hashCode();
            _hash = 31 * _hash + delta;
            delta = shortLink == null ? 0 : shortLink.hashCode();
            _hash = 31 * _hash + delta;
            delta = imageLink == null ? 0 : imageLink.hashCode();
            _hash = 31 * _hash + delta;
            delta = imageUrl == null ? 0 : imageUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = type == null ? 0 : type.hashCode();
            _hash = 31 * _hash + delta;
            delta = status == null ? 0 : status.hashCode();
            _hash = 31 * _hash + delta;
            delta = createdTime == null ? 0 : createdTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
