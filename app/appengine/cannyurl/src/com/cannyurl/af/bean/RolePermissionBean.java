package com.cannyurl.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.RolePermission;
import com.myurldb.ws.stub.RolePermissionStub;


// Wrapper class + bean combo.
public class RolePermissionBean implements RolePermission, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(RolePermissionBean.class.getName());

    // [1] With an embedded object.
    private RolePermissionStub stub = null;

    // [2] Or, without an embedded object.
    private String guid;
    private String role;
    private String permissionName;
    private String resource;
    private String instance;
    private String action;
    private String status;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public RolePermissionBean()
    {
        //this((String) null);
    }
    public RolePermissionBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null);
    }
    public RolePermissionBean(String guid, String role, String permissionName, String resource, String instance, String action, String status)
    {
        this(guid, role, permissionName, resource, instance, action, status, null, null);
    }
    public RolePermissionBean(String guid, String role, String permissionName, String resource, String instance, String action, String status, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.role = role;
        this.permissionName = permissionName;
        this.resource = resource;
        this.instance = instance;
        this.action = action;
        this.status = status;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public RolePermissionBean(RolePermission stub)
    {
        if(stub instanceof RolePermissionStub) {
            this.stub = (RolePermissionStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setRole(stub.getRole());   
            setPermissionName(stub.getPermissionName());   
            setResource(stub.getResource());   
            setInstance(stub.getInstance());   
            setAction(stub.getAction());   
            setStatus(stub.getStatus());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public String getRole()
    {
        if(getStub() != null) {
            return getStub().getRole();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.role;
        }
    }
    public void setRole(String role)
    {
        if(getStub() != null) {
            getStub().setRole(role);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.role = role;
        }
    }

    public String getPermissionName()
    {
        if(getStub() != null) {
            return getStub().getPermissionName();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.permissionName;
        }
    }
    public void setPermissionName(String permissionName)
    {
        if(getStub() != null) {
            getStub().setPermissionName(permissionName);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.permissionName = permissionName;
        }
    }

    public String getResource()
    {
        if(getStub() != null) {
            return getStub().getResource();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.resource;
        }
    }
    public void setResource(String resource)
    {
        if(getStub() != null) {
            getStub().setResource(resource);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.resource = resource;
        }
    }

    public String getInstance()
    {
        if(getStub() != null) {
            return getStub().getInstance();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.instance;
        }
    }
    public void setInstance(String instance)
    {
        if(getStub() != null) {
            getStub().setInstance(instance);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.instance = instance;
        }
    }

    public String getAction()
    {
        if(getStub() != null) {
            return getStub().getAction();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.action;
        }
    }
    public void setAction(String action)
    {
        if(getStub() != null) {
            getStub().setAction(action);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.action = action;
        }
    }

    public String getStatus()
    {
        if(getStub() != null) {
            return getStub().getStatus();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.status;
        }
    }
    public void setStatus(String status)
    {
        if(getStub() != null) {
            getStub().setStatus(status);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.status = status;
        }
    }

    public Long getCreatedTime()
    {
        if(getStub() != null) {
            return getStub().getCreatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.createdTime;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getStub() != null) {
            getStub().setCreatedTime(createdTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.createdTime = createdTime;
        }
    }

    public Long getModifiedTime()
    {
        if(getStub() != null) {
            return getStub().getModifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.modifiedTime;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getStub() != null) {
            getStub().setModifiedTime(modifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.modifiedTime = modifiedTime;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public RolePermissionStub getStub()
    {
        return this.stub;
    }
    protected void setStub(RolePermissionStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("guid = " + this.guid).append(";");
            sb.append("role = " + this.role).append(";");
            sb.append("permissionName = " + this.permissionName).append(";");
            sb.append("resource = " + this.resource).append(";");
            sb.append("instance = " + this.instance).append(";");
            sb.append("action = " + this.action).append(";");
            sb.append("status = " + this.status).append(";");
            sb.append("createdTime = " + this.createdTime).append(";");
            sb.append("modifiedTime = " + this.modifiedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = guid == null ? 0 : guid.hashCode();
            _hash = 31 * _hash + delta;
            delta = role == null ? 0 : role.hashCode();
            _hash = 31 * _hash + delta;
            delta = permissionName == null ? 0 : permissionName.hashCode();
            _hash = 31 * _hash + delta;
            delta = resource == null ? 0 : resource.hashCode();
            _hash = 31 * _hash + delta;
            delta = instance == null ? 0 : instance.hashCode();
            _hash = 31 * _hash + delta;
            delta = action == null ? 0 : action.hashCode();
            _hash = 31 * _hash + delta;
            delta = status == null ? 0 : status.hashCode();
            _hash = 31 * _hash + delta;
            delta = createdTime == null ? 0 : createdTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
