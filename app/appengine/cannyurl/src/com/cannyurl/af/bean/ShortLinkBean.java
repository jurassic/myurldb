package com.cannyurl.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.ReferrerInfoStruct;
import com.myurldb.ws.stub.ReferrerInfoStructStub;
import com.myurldb.ws.ShortLink;
import com.myurldb.ws.stub.ShortLinkStub;


// Wrapper class + bean combo.
public class ShortLinkBean implements ShortLink, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ShortLinkBean.class.getName());

    // [1] With an embedded object.
    private ShortLinkStub stub = null;

    // [2] Or, without an embedded object.
    private String guid;
    private String appClient;
    private String clientRootDomain;
    private String owner;
    private String longUrlDomain;
    private String longUrl;
    private String longUrlFull;
    private String longUrlHash;
    private Boolean reuseExistingShortUrl;
    private Boolean failIfShortUrlExists;
    private String domain;
    private String domainType;
    private String usercode;
    private String username;
    private String tokenPrefix;
    private String token;
    private String tokenType;
    private String sassyTokenType;
    private String shortUrl;
    private String shortPassage;
    private String redirectType;
    private Long flashDuration;
    private String accessType;
    private String viewType;
    private String shareType;
    private Boolean readOnly;
    private String displayMessage;
    private String shortMessage;
    private Boolean keywordEnabled;
    private Boolean bookmarkEnabled;
    private ReferrerInfoStructBean referrerInfo;
    private String status;
    private String note;
    private Long expirationTime;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public ShortLinkBean()
    {
        //this((String) null);
    }
    public ShortLinkBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public ShortLinkBean(String guid, String appClient, String clientRootDomain, String owner, String longUrlDomain, String longUrl, String longUrlFull, String longUrlHash, Boolean reuseExistingShortUrl, Boolean failIfShortUrlExists, String domain, String domainType, String usercode, String username, String tokenPrefix, String token, String tokenType, String sassyTokenType, String shortUrl, String shortPassage, String redirectType, Long flashDuration, String accessType, String viewType, String shareType, Boolean readOnly, String displayMessage, String shortMessage, Boolean keywordEnabled, Boolean bookmarkEnabled, ReferrerInfoStructBean referrerInfo, String status, String note, Long expirationTime)
    {
        this(guid, appClient, clientRootDomain, owner, longUrlDomain, longUrl, longUrlFull, longUrlHash, reuseExistingShortUrl, failIfShortUrlExists, domain, domainType, usercode, username, tokenPrefix, token, tokenType, sassyTokenType, shortUrl, shortPassage, redirectType, flashDuration, accessType, viewType, shareType, readOnly, displayMessage, shortMessage, keywordEnabled, bookmarkEnabled, referrerInfo, status, note, expirationTime, null, null);
    }
    public ShortLinkBean(String guid, String appClient, String clientRootDomain, String owner, String longUrlDomain, String longUrl, String longUrlFull, String longUrlHash, Boolean reuseExistingShortUrl, Boolean failIfShortUrlExists, String domain, String domainType, String usercode, String username, String tokenPrefix, String token, String tokenType, String sassyTokenType, String shortUrl, String shortPassage, String redirectType, Long flashDuration, String accessType, String viewType, String shareType, Boolean readOnly, String displayMessage, String shortMessage, Boolean keywordEnabled, Boolean bookmarkEnabled, ReferrerInfoStructBean referrerInfo, String status, String note, Long expirationTime, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.appClient = appClient;
        this.clientRootDomain = clientRootDomain;
        this.owner = owner;
        this.longUrlDomain = longUrlDomain;
        this.longUrl = longUrl;
        this.longUrlFull = longUrlFull;
        this.longUrlHash = longUrlHash;
        this.reuseExistingShortUrl = reuseExistingShortUrl;
        this.failIfShortUrlExists = failIfShortUrlExists;
        this.domain = domain;
        this.domainType = domainType;
        this.usercode = usercode;
        this.username = username;
        this.tokenPrefix = tokenPrefix;
        this.token = token;
        this.tokenType = tokenType;
        this.sassyTokenType = sassyTokenType;
        this.shortUrl = shortUrl;
        this.shortPassage = shortPassage;
        this.redirectType = redirectType;
        this.flashDuration = flashDuration;
        this.accessType = accessType;
        this.viewType = viewType;
        this.shareType = shareType;
        this.readOnly = readOnly;
        this.displayMessage = displayMessage;
        this.shortMessage = shortMessage;
        this.keywordEnabled = keywordEnabled;
        this.bookmarkEnabled = bookmarkEnabled;
        this.referrerInfo = referrerInfo;
        this.status = status;
        this.note = note;
        this.expirationTime = expirationTime;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public ShortLinkBean(ShortLink stub)
    {
        if(stub instanceof ShortLinkStub) {
            this.stub = (ShortLinkStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setAppClient(stub.getAppClient());   
            setClientRootDomain(stub.getClientRootDomain());   
            setOwner(stub.getOwner());   
            setLongUrlDomain(stub.getLongUrlDomain());   
            setLongUrl(stub.getLongUrl());   
            setLongUrlFull(stub.getLongUrlFull());   
            setLongUrlHash(stub.getLongUrlHash());   
            setReuseExistingShortUrl(stub.isReuseExistingShortUrl());   
            setFailIfShortUrlExists(stub.isFailIfShortUrlExists());   
            setDomain(stub.getDomain());   
            setDomainType(stub.getDomainType());   
            setUsercode(stub.getUsercode());   
            setUsername(stub.getUsername());   
            setTokenPrefix(stub.getTokenPrefix());   
            setToken(stub.getToken());   
            setTokenType(stub.getTokenType());   
            setSassyTokenType(stub.getSassyTokenType());   
            setShortUrl(stub.getShortUrl());   
            setShortPassage(stub.getShortPassage());   
            setRedirectType(stub.getRedirectType());   
            setFlashDuration(stub.getFlashDuration());   
            setAccessType(stub.getAccessType());   
            setViewType(stub.getViewType());   
            setShareType(stub.getShareType());   
            setReadOnly(stub.isReadOnly());   
            setDisplayMessage(stub.getDisplayMessage());   
            setShortMessage(stub.getShortMessage());   
            setKeywordEnabled(stub.isKeywordEnabled());   
            setBookmarkEnabled(stub.isBookmarkEnabled());   
            ReferrerInfoStruct referrerInfo = stub.getReferrerInfo();
            if(referrerInfo instanceof ReferrerInfoStructBean) {
                setReferrerInfo((ReferrerInfoStructBean) referrerInfo);   
            } else {
                setReferrerInfo(new ReferrerInfoStructBean(referrerInfo));   
            }
            setStatus(stub.getStatus());   
            setNote(stub.getNote());   
            setExpirationTime(stub.getExpirationTime());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public String getAppClient()
    {
        if(getStub() != null) {
            return getStub().getAppClient();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.appClient;
        }
    }
    public void setAppClient(String appClient)
    {
        if(getStub() != null) {
            getStub().setAppClient(appClient);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.appClient = appClient;
        }
    }

    public String getClientRootDomain()
    {
        if(getStub() != null) {
            return getStub().getClientRootDomain();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.clientRootDomain;
        }
    }
    public void setClientRootDomain(String clientRootDomain)
    {
        if(getStub() != null) {
            getStub().setClientRootDomain(clientRootDomain);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.clientRootDomain = clientRootDomain;
        }
    }

    public String getOwner()
    {
        if(getStub() != null) {
            return getStub().getOwner();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.owner;
        }
    }
    public void setOwner(String owner)
    {
        if(getStub() != null) {
            getStub().setOwner(owner);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.owner = owner;
        }
    }

    public String getLongUrlDomain()
    {
        if(getStub() != null) {
            return getStub().getLongUrlDomain();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.longUrlDomain;
        }
    }
    public void setLongUrlDomain(String longUrlDomain)
    {
        if(getStub() != null) {
            getStub().setLongUrlDomain(longUrlDomain);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.longUrlDomain = longUrlDomain;
        }
    }

    public String getLongUrl()
    {
        if(getStub() != null) {
            return getStub().getLongUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.longUrl;
        }
    }
    public void setLongUrl(String longUrl)
    {
        if(getStub() != null) {
            getStub().setLongUrl(longUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.longUrl = longUrl;
        }
    }

    public String getLongUrlFull()
    {
        if(getStub() != null) {
            return getStub().getLongUrlFull();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.longUrlFull;
        }
    }
    public void setLongUrlFull(String longUrlFull)
    {
        if(getStub() != null) {
            getStub().setLongUrlFull(longUrlFull);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.longUrlFull = longUrlFull;
        }
    }

    public String getLongUrlHash()
    {
        if(getStub() != null) {
            return getStub().getLongUrlHash();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.longUrlHash;
        }
    }
    public void setLongUrlHash(String longUrlHash)
    {
        if(getStub() != null) {
            getStub().setLongUrlHash(longUrlHash);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.longUrlHash = longUrlHash;
        }
    }

    public Boolean isReuseExistingShortUrl()
    {
        if(getStub() != null) {
            return getStub().isReuseExistingShortUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.reuseExistingShortUrl;
        }
    }
    public void setReuseExistingShortUrl(Boolean reuseExistingShortUrl)
    {
        if(getStub() != null) {
            getStub().setReuseExistingShortUrl(reuseExistingShortUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.reuseExistingShortUrl = reuseExistingShortUrl;
        }
    }

    public Boolean isFailIfShortUrlExists()
    {
        if(getStub() != null) {
            return getStub().isFailIfShortUrlExists();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.failIfShortUrlExists;
        }
    }
    public void setFailIfShortUrlExists(Boolean failIfShortUrlExists)
    {
        if(getStub() != null) {
            getStub().setFailIfShortUrlExists(failIfShortUrlExists);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.failIfShortUrlExists = failIfShortUrlExists;
        }
    }

    public String getDomain()
    {
        if(getStub() != null) {
            return getStub().getDomain();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.domain;
        }
    }
    public void setDomain(String domain)
    {
        if(getStub() != null) {
            getStub().setDomain(domain);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.domain = domain;
        }
    }

    public String getDomainType()
    {
        if(getStub() != null) {
            return getStub().getDomainType();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.domainType;
        }
    }
    public void setDomainType(String domainType)
    {
        if(getStub() != null) {
            getStub().setDomainType(domainType);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.domainType = domainType;
        }
    }

    public String getUsercode()
    {
        if(getStub() != null) {
            return getStub().getUsercode();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.usercode;
        }
    }
    public void setUsercode(String usercode)
    {
        if(getStub() != null) {
            getStub().setUsercode(usercode);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.usercode = usercode;
        }
    }

    public String getUsername()
    {
        if(getStub() != null) {
            return getStub().getUsername();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.username;
        }
    }
    public void setUsername(String username)
    {
        if(getStub() != null) {
            getStub().setUsername(username);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.username = username;
        }
    }

    public String getTokenPrefix()
    {
        if(getStub() != null) {
            return getStub().getTokenPrefix();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.tokenPrefix;
        }
    }
    public void setTokenPrefix(String tokenPrefix)
    {
        if(getStub() != null) {
            getStub().setTokenPrefix(tokenPrefix);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.tokenPrefix = tokenPrefix;
        }
    }

    public String getToken()
    {
        if(getStub() != null) {
            return getStub().getToken();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.token;
        }
    }
    public void setToken(String token)
    {
        if(getStub() != null) {
            getStub().setToken(token);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.token = token;
        }
    }

    public String getTokenType()
    {
        if(getStub() != null) {
            return getStub().getTokenType();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.tokenType;
        }
    }
    public void setTokenType(String tokenType)
    {
        if(getStub() != null) {
            getStub().setTokenType(tokenType);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.tokenType = tokenType;
        }
    }

    public String getSassyTokenType()
    {
        if(getStub() != null) {
            return getStub().getSassyTokenType();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.sassyTokenType;
        }
    }
    public void setSassyTokenType(String sassyTokenType)
    {
        if(getStub() != null) {
            getStub().setSassyTokenType(sassyTokenType);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.sassyTokenType = sassyTokenType;
        }
    }

    public String getShortUrl()
    {
        if(getStub() != null) {
            return getStub().getShortUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.shortUrl;
        }
    }
    public void setShortUrl(String shortUrl)
    {
        if(getStub() != null) {
            getStub().setShortUrl(shortUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.shortUrl = shortUrl;
        }
    }

    public String getShortPassage()
    {
        if(getStub() != null) {
            return getStub().getShortPassage();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.shortPassage;
        }
    }
    public void setShortPassage(String shortPassage)
    {
        if(getStub() != null) {
            getStub().setShortPassage(shortPassage);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.shortPassage = shortPassage;
        }
    }

    public String getRedirectType()
    {
        if(getStub() != null) {
            return getStub().getRedirectType();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.redirectType;
        }
    }
    public void setRedirectType(String redirectType)
    {
        if(getStub() != null) {
            getStub().setRedirectType(redirectType);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.redirectType = redirectType;
        }
    }

    public Long getFlashDuration()
    {
        if(getStub() != null) {
            return getStub().getFlashDuration();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.flashDuration;
        }
    }
    public void setFlashDuration(Long flashDuration)
    {
        if(getStub() != null) {
            getStub().setFlashDuration(flashDuration);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.flashDuration = flashDuration;
        }
    }

    public String getAccessType()
    {
        if(getStub() != null) {
            return getStub().getAccessType();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.accessType;
        }
    }
    public void setAccessType(String accessType)
    {
        if(getStub() != null) {
            getStub().setAccessType(accessType);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.accessType = accessType;
        }
    }

    public String getViewType()
    {
        if(getStub() != null) {
            return getStub().getViewType();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.viewType;
        }
    }
    public void setViewType(String viewType)
    {
        if(getStub() != null) {
            getStub().setViewType(viewType);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.viewType = viewType;
        }
    }

    public String getShareType()
    {
        if(getStub() != null) {
            return getStub().getShareType();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.shareType;
        }
    }
    public void setShareType(String shareType)
    {
        if(getStub() != null) {
            getStub().setShareType(shareType);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.shareType = shareType;
        }
    }

    public Boolean isReadOnly()
    {
        if(getStub() != null) {
            return getStub().isReadOnly();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.readOnly;
        }
    }
    public void setReadOnly(Boolean readOnly)
    {
        if(getStub() != null) {
            getStub().setReadOnly(readOnly);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.readOnly = readOnly;
        }
    }

    public String getDisplayMessage()
    {
        if(getStub() != null) {
            return getStub().getDisplayMessage();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.displayMessage;
        }
    }
    public void setDisplayMessage(String displayMessage)
    {
        if(getStub() != null) {
            getStub().setDisplayMessage(displayMessage);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.displayMessage = displayMessage;
        }
    }

    public String getShortMessage()
    {
        if(getStub() != null) {
            return getStub().getShortMessage();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.shortMessage;
        }
    }
    public void setShortMessage(String shortMessage)
    {
        if(getStub() != null) {
            getStub().setShortMessage(shortMessage);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.shortMessage = shortMessage;
        }
    }

    public Boolean isKeywordEnabled()
    {
        if(getStub() != null) {
            return getStub().isKeywordEnabled();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.keywordEnabled;
        }
    }
    public void setKeywordEnabled(Boolean keywordEnabled)
    {
        if(getStub() != null) {
            getStub().setKeywordEnabled(keywordEnabled);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.keywordEnabled = keywordEnabled;
        }
    }

    public Boolean isBookmarkEnabled()
    {
        if(getStub() != null) {
            return getStub().isBookmarkEnabled();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.bookmarkEnabled;
        }
    }
    public void setBookmarkEnabled(Boolean bookmarkEnabled)
    {
        if(getStub() != null) {
            getStub().setBookmarkEnabled(bookmarkEnabled);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.bookmarkEnabled = bookmarkEnabled;
        }
    }

    public ReferrerInfoStruct getReferrerInfo()
    {  
        if(getStub() != null) {
            // Note the object type.
            ReferrerInfoStruct _stub_field = getStub().getReferrerInfo();
            if(_stub_field == null) {
                return null;
            } else {
                return new ReferrerInfoStructBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.referrerInfo;
        }
    }
    public void setReferrerInfo(ReferrerInfoStruct referrerInfo)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setReferrerInfo(referrerInfo);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(referrerInfo == null) {
                this.referrerInfo = null;
            } else {
                if(referrerInfo instanceof ReferrerInfoStructBean) {
                    this.referrerInfo = (ReferrerInfoStructBean) referrerInfo;
                } else {
                    this.referrerInfo = new ReferrerInfoStructBean(referrerInfo);
                }
            }
        }
    }

    public String getStatus()
    {
        if(getStub() != null) {
            return getStub().getStatus();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.status;
        }
    }
    public void setStatus(String status)
    {
        if(getStub() != null) {
            getStub().setStatus(status);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.status = status;
        }
    }

    public String getNote()
    {
        if(getStub() != null) {
            return getStub().getNote();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.note;
        }
    }
    public void setNote(String note)
    {
        if(getStub() != null) {
            getStub().setNote(note);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.note = note;
        }
    }

    public Long getExpirationTime()
    {
        if(getStub() != null) {
            return getStub().getExpirationTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.expirationTime;
        }
    }
    public void setExpirationTime(Long expirationTime)
    {
        if(getStub() != null) {
            getStub().setExpirationTime(expirationTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.expirationTime = expirationTime;
        }
    }

    public Long getCreatedTime()
    {
        if(getStub() != null) {
            return getStub().getCreatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.createdTime;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getStub() != null) {
            getStub().setCreatedTime(createdTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.createdTime = createdTime;
        }
    }

    public Long getModifiedTime()
    {
        if(getStub() != null) {
            return getStub().getModifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.modifiedTime;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getStub() != null) {
            getStub().setModifiedTime(modifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.modifiedTime = modifiedTime;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public ShortLinkStub getStub()
    {
        return this.stub;
    }
    protected void setStub(ShortLinkStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("guid = " + this.guid).append(";");
            sb.append("appClient = " + this.appClient).append(";");
            sb.append("clientRootDomain = " + this.clientRootDomain).append(";");
            sb.append("owner = " + this.owner).append(";");
            sb.append("longUrlDomain = " + this.longUrlDomain).append(";");
            sb.append("longUrl = " + this.longUrl).append(";");
            sb.append("longUrlFull = " + this.longUrlFull).append(";");
            sb.append("longUrlHash = " + this.longUrlHash).append(";");
            sb.append("reuseExistingShortUrl = " + this.reuseExistingShortUrl).append(";");
            sb.append("failIfShortUrlExists = " + this.failIfShortUrlExists).append(";");
            sb.append("domain = " + this.domain).append(";");
            sb.append("domainType = " + this.domainType).append(";");
            sb.append("usercode = " + this.usercode).append(";");
            sb.append("username = " + this.username).append(";");
            sb.append("tokenPrefix = " + this.tokenPrefix).append(";");
            sb.append("token = " + this.token).append(";");
            sb.append("tokenType = " + this.tokenType).append(";");
            sb.append("sassyTokenType = " + this.sassyTokenType).append(";");
            sb.append("shortUrl = " + this.shortUrl).append(";");
            sb.append("shortPassage = " + this.shortPassage).append(";");
            sb.append("redirectType = " + this.redirectType).append(";");
            sb.append("flashDuration = " + this.flashDuration).append(";");
            sb.append("accessType = " + this.accessType).append(";");
            sb.append("viewType = " + this.viewType).append(";");
            sb.append("shareType = " + this.shareType).append(";");
            sb.append("readOnly = " + this.readOnly).append(";");
            sb.append("displayMessage = " + this.displayMessage).append(";");
            sb.append("shortMessage = " + this.shortMessage).append(";");
            sb.append("keywordEnabled = " + this.keywordEnabled).append(";");
            sb.append("bookmarkEnabled = " + this.bookmarkEnabled).append(";");
            sb.append("referrerInfo = " + this.referrerInfo).append(";");
            sb.append("status = " + this.status).append(";");
            sb.append("note = " + this.note).append(";");
            sb.append("expirationTime = " + this.expirationTime).append(";");
            sb.append("createdTime = " + this.createdTime).append(";");
            sb.append("modifiedTime = " + this.modifiedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = guid == null ? 0 : guid.hashCode();
            _hash = 31 * _hash + delta;
            delta = appClient == null ? 0 : appClient.hashCode();
            _hash = 31 * _hash + delta;
            delta = clientRootDomain == null ? 0 : clientRootDomain.hashCode();
            _hash = 31 * _hash + delta;
            delta = owner == null ? 0 : owner.hashCode();
            _hash = 31 * _hash + delta;
            delta = longUrlDomain == null ? 0 : longUrlDomain.hashCode();
            _hash = 31 * _hash + delta;
            delta = longUrl == null ? 0 : longUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = longUrlFull == null ? 0 : longUrlFull.hashCode();
            _hash = 31 * _hash + delta;
            delta = longUrlHash == null ? 0 : longUrlHash.hashCode();
            _hash = 31 * _hash + delta;
            delta = reuseExistingShortUrl == null ? 0 : reuseExistingShortUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = failIfShortUrlExists == null ? 0 : failIfShortUrlExists.hashCode();
            _hash = 31 * _hash + delta;
            delta = domain == null ? 0 : domain.hashCode();
            _hash = 31 * _hash + delta;
            delta = domainType == null ? 0 : domainType.hashCode();
            _hash = 31 * _hash + delta;
            delta = usercode == null ? 0 : usercode.hashCode();
            _hash = 31 * _hash + delta;
            delta = username == null ? 0 : username.hashCode();
            _hash = 31 * _hash + delta;
            delta = tokenPrefix == null ? 0 : tokenPrefix.hashCode();
            _hash = 31 * _hash + delta;
            delta = token == null ? 0 : token.hashCode();
            _hash = 31 * _hash + delta;
            delta = tokenType == null ? 0 : tokenType.hashCode();
            _hash = 31 * _hash + delta;
            delta = sassyTokenType == null ? 0 : sassyTokenType.hashCode();
            _hash = 31 * _hash + delta;
            delta = shortUrl == null ? 0 : shortUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = shortPassage == null ? 0 : shortPassage.hashCode();
            _hash = 31 * _hash + delta;
            delta = redirectType == null ? 0 : redirectType.hashCode();
            _hash = 31 * _hash + delta;
            delta = flashDuration == null ? 0 : flashDuration.hashCode();
            _hash = 31 * _hash + delta;
            delta = accessType == null ? 0 : accessType.hashCode();
            _hash = 31 * _hash + delta;
            delta = viewType == null ? 0 : viewType.hashCode();
            _hash = 31 * _hash + delta;
            delta = shareType == null ? 0 : shareType.hashCode();
            _hash = 31 * _hash + delta;
            delta = readOnly == null ? 0 : readOnly.hashCode();
            _hash = 31 * _hash + delta;
            delta = displayMessage == null ? 0 : displayMessage.hashCode();
            _hash = 31 * _hash + delta;
            delta = shortMessage == null ? 0 : shortMessage.hashCode();
            _hash = 31 * _hash + delta;
            delta = keywordEnabled == null ? 0 : keywordEnabled.hashCode();
            _hash = 31 * _hash + delta;
            delta = bookmarkEnabled == null ? 0 : bookmarkEnabled.hashCode();
            _hash = 31 * _hash + delta;
            delta = referrerInfo == null ? 0 : referrerInfo.hashCode();
            _hash = 31 * _hash + delta;
            delta = status == null ? 0 : status.hashCode();
            _hash = 31 * _hash + delta;
            delta = note == null ? 0 : note.hashCode();
            _hash = 31 * _hash + delta;
            delta = expirationTime == null ? 0 : expirationTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = createdTime == null ? 0 : createdTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
