package com.cannyurl.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.ShortPassageAttribute;
import com.myurldb.ws.stub.ShortPassageAttributeStub;


// Wrapper class + bean combo.
public class ShortPassageAttributeBean implements ShortPassageAttribute, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ShortPassageAttributeBean.class.getName());

    // [1] With an embedded object.
    private ShortPassageAttributeStub stub = null;

    // [2] Or, without an embedded object.
    private String domain;
    private String tokenType;
    private String displayMessage;
    private String redirectType;
    private Long flashDuration;
    private String accessType;
    private String viewType;
    private String shareType;

    // Ctors.
    public ShortPassageAttributeBean()
    {
        //this((String) null);
    }
    public ShortPassageAttributeBean(String domain, String tokenType, String displayMessage, String redirectType, Long flashDuration, String accessType, String viewType, String shareType)
    {
        this.domain = domain;
        this.tokenType = tokenType;
        this.displayMessage = displayMessage;
        this.redirectType = redirectType;
        this.flashDuration = flashDuration;
        this.accessType = accessType;
        this.viewType = viewType;
        this.shareType = shareType;
    }
    public ShortPassageAttributeBean(ShortPassageAttribute stub)
    {
        if(stub instanceof ShortPassageAttributeStub) {
            this.stub = (ShortPassageAttributeStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setDomain(stub.getDomain());   
            setTokenType(stub.getTokenType());   
            setDisplayMessage(stub.getDisplayMessage());   
            setRedirectType(stub.getRedirectType());   
            setFlashDuration(stub.getFlashDuration());   
            setAccessType(stub.getAccessType());   
            setViewType(stub.getViewType());   
            setShareType(stub.getShareType());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getDomain()
    {
        if(getStub() != null) {
            return getStub().getDomain();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.domain;
        }
    }
    public void setDomain(String domain)
    {
        if(getStub() != null) {
            getStub().setDomain(domain);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.domain = domain;
        }
    }

    public String getTokenType()
    {
        if(getStub() != null) {
            return getStub().getTokenType();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.tokenType;
        }
    }
    public void setTokenType(String tokenType)
    {
        if(getStub() != null) {
            getStub().setTokenType(tokenType);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.tokenType = tokenType;
        }
    }

    public String getDisplayMessage()
    {
        if(getStub() != null) {
            return getStub().getDisplayMessage();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.displayMessage;
        }
    }
    public void setDisplayMessage(String displayMessage)
    {
        if(getStub() != null) {
            getStub().setDisplayMessage(displayMessage);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.displayMessage = displayMessage;
        }
    }

    public String getRedirectType()
    {
        if(getStub() != null) {
            return getStub().getRedirectType();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.redirectType;
        }
    }
    public void setRedirectType(String redirectType)
    {
        if(getStub() != null) {
            getStub().setRedirectType(redirectType);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.redirectType = redirectType;
        }
    }

    public Long getFlashDuration()
    {
        if(getStub() != null) {
            return getStub().getFlashDuration();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.flashDuration;
        }
    }
    public void setFlashDuration(Long flashDuration)
    {
        if(getStub() != null) {
            getStub().setFlashDuration(flashDuration);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.flashDuration = flashDuration;
        }
    }

    public String getAccessType()
    {
        if(getStub() != null) {
            return getStub().getAccessType();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.accessType;
        }
    }
    public void setAccessType(String accessType)
    {
        if(getStub() != null) {
            getStub().setAccessType(accessType);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.accessType = accessType;
        }
    }

    public String getViewType()
    {
        if(getStub() != null) {
            return getStub().getViewType();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.viewType;
        }
    }
    public void setViewType(String viewType)
    {
        if(getStub() != null) {
            getStub().setViewType(viewType);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.viewType = viewType;
        }
    }

    public String getShareType()
    {
        if(getStub() != null) {
            return getStub().getShareType();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.shareType;
        }
    }
    public void setShareType(String shareType)
    {
        if(getStub() != null) {
            getStub().setShareType(shareType);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.shareType = shareType;
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getDomain() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getTokenType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getDisplayMessage() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getRedirectType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getFlashDuration() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAccessType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getViewType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getShareType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // Returns the reference to the embedded object. (Could be null.)
    public ShortPassageAttributeStub getStub()
    {
        return this.stub;
    }
    protected void setStub(ShortPassageAttributeStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("domain = " + this.domain).append(";");
            sb.append("tokenType = " + this.tokenType).append(";");
            sb.append("displayMessage = " + this.displayMessage).append(";");
            sb.append("redirectType = " + this.redirectType).append(";");
            sb.append("flashDuration = " + this.flashDuration).append(";");
            sb.append("accessType = " + this.accessType).append(";");
            sb.append("viewType = " + this.viewType).append(";");
            sb.append("shareType = " + this.shareType).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = domain == null ? 0 : domain.hashCode();
            _hash = 31 * _hash + delta;
            delta = tokenType == null ? 0 : tokenType.hashCode();
            _hash = 31 * _hash + delta;
            delta = displayMessage == null ? 0 : displayMessage.hashCode();
            _hash = 31 * _hash + delta;
            delta = redirectType == null ? 0 : redirectType.hashCode();
            _hash = 31 * _hash + delta;
            delta = flashDuration == null ? 0 : flashDuration.hashCode();
            _hash = 31 * _hash + delta;
            delta = accessType == null ? 0 : accessType.hashCode();
            _hash = 31 * _hash + delta;
            delta = viewType == null ? 0 : viewType.hashCode();
            _hash = 31 * _hash + delta;
            delta = shareType == null ? 0 : shareType.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
