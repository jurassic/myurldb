package com.cannyurl.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.ShortPassageAttribute;
import com.myurldb.ws.stub.ShortPassageAttributeStub;
import com.myurldb.ws.ShortPassage;
import com.myurldb.ws.stub.ShortPassageStub;


// Wrapper class + bean combo.
public class ShortPassageBean implements ShortPassage, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ShortPassageBean.class.getName());

    // [1] With an embedded object.
    private ShortPassageStub stub = null;

    // [2] Or, without an embedded object.
    private String guid;
    private String owner;
    private String longText;
    private String shortText;
    private ShortPassageAttributeBean attribute;
    private Boolean readOnly;
    private String status;
    private String note;
    private Long expirationTime;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public ShortPassageBean()
    {
        //this((String) null);
    }
    public ShortPassageBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null);
    }
    public ShortPassageBean(String guid, String owner, String longText, String shortText, ShortPassageAttributeBean attribute, Boolean readOnly, String status, String note, Long expirationTime)
    {
        this(guid, owner, longText, shortText, attribute, readOnly, status, note, expirationTime, null, null);
    }
    public ShortPassageBean(String guid, String owner, String longText, String shortText, ShortPassageAttributeBean attribute, Boolean readOnly, String status, String note, Long expirationTime, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.owner = owner;
        this.longText = longText;
        this.shortText = shortText;
        this.attribute = attribute;
        this.readOnly = readOnly;
        this.status = status;
        this.note = note;
        this.expirationTime = expirationTime;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public ShortPassageBean(ShortPassage stub)
    {
        if(stub instanceof ShortPassageStub) {
            this.stub = (ShortPassageStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setOwner(stub.getOwner());   
            setLongText(stub.getLongText());   
            setShortText(stub.getShortText());   
            ShortPassageAttribute attribute = stub.getAttribute();
            if(attribute instanceof ShortPassageAttributeBean) {
                setAttribute((ShortPassageAttributeBean) attribute);   
            } else {
                setAttribute(new ShortPassageAttributeBean(attribute));   
            }
            setReadOnly(stub.isReadOnly());   
            setStatus(stub.getStatus());   
            setNote(stub.getNote());   
            setExpirationTime(stub.getExpirationTime());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public String getOwner()
    {
        if(getStub() != null) {
            return getStub().getOwner();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.owner;
        }
    }
    public void setOwner(String owner)
    {
        if(getStub() != null) {
            getStub().setOwner(owner);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.owner = owner;
        }
    }

    public String getLongText()
    {
        if(getStub() != null) {
            return getStub().getLongText();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.longText;
        }
    }
    public void setLongText(String longText)
    {
        if(getStub() != null) {
            getStub().setLongText(longText);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.longText = longText;
        }
    }

    public String getShortText()
    {
        if(getStub() != null) {
            return getStub().getShortText();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.shortText;
        }
    }
    public void setShortText(String shortText)
    {
        if(getStub() != null) {
            getStub().setShortText(shortText);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.shortText = shortText;
        }
    }

    public ShortPassageAttribute getAttribute()
    {  
        if(getStub() != null) {
            // Note the object type.
            ShortPassageAttribute _stub_field = getStub().getAttribute();
            if(_stub_field == null) {
                return null;
            } else {
                return new ShortPassageAttributeBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.attribute;
        }
    }
    public void setAttribute(ShortPassageAttribute attribute)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setAttribute(attribute);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(attribute == null) {
                this.attribute = null;
            } else {
                if(attribute instanceof ShortPassageAttributeBean) {
                    this.attribute = (ShortPassageAttributeBean) attribute;
                } else {
                    this.attribute = new ShortPassageAttributeBean(attribute);
                }
            }
        }
    }

    public Boolean isReadOnly()
    {
        if(getStub() != null) {
            return getStub().isReadOnly();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.readOnly;
        }
    }
    public void setReadOnly(Boolean readOnly)
    {
        if(getStub() != null) {
            getStub().setReadOnly(readOnly);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.readOnly = readOnly;
        }
    }

    public String getStatus()
    {
        if(getStub() != null) {
            return getStub().getStatus();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.status;
        }
    }
    public void setStatus(String status)
    {
        if(getStub() != null) {
            getStub().setStatus(status);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.status = status;
        }
    }

    public String getNote()
    {
        if(getStub() != null) {
            return getStub().getNote();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.note;
        }
    }
    public void setNote(String note)
    {
        if(getStub() != null) {
            getStub().setNote(note);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.note = note;
        }
    }

    public Long getExpirationTime()
    {
        if(getStub() != null) {
            return getStub().getExpirationTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.expirationTime;
        }
    }
    public void setExpirationTime(Long expirationTime)
    {
        if(getStub() != null) {
            getStub().setExpirationTime(expirationTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.expirationTime = expirationTime;
        }
    }

    public Long getCreatedTime()
    {
        if(getStub() != null) {
            return getStub().getCreatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.createdTime;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getStub() != null) {
            getStub().setCreatedTime(createdTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.createdTime = createdTime;
        }
    }

    public Long getModifiedTime()
    {
        if(getStub() != null) {
            return getStub().getModifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.modifiedTime;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getStub() != null) {
            getStub().setModifiedTime(modifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.modifiedTime = modifiedTime;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public ShortPassageStub getStub()
    {
        return this.stub;
    }
    protected void setStub(ShortPassageStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("guid = " + this.guid).append(";");
            sb.append("owner = " + this.owner).append(";");
            sb.append("longText = " + this.longText).append(";");
            sb.append("shortText = " + this.shortText).append(";");
            sb.append("attribute = " + this.attribute).append(";");
            sb.append("readOnly = " + this.readOnly).append(";");
            sb.append("status = " + this.status).append(";");
            sb.append("note = " + this.note).append(";");
            sb.append("expirationTime = " + this.expirationTime).append(";");
            sb.append("createdTime = " + this.createdTime).append(";");
            sb.append("modifiedTime = " + this.modifiedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = guid == null ? 0 : guid.hashCode();
            _hash = 31 * _hash + delta;
            delta = owner == null ? 0 : owner.hashCode();
            _hash = 31 * _hash + delta;
            delta = longText == null ? 0 : longText.hashCode();
            _hash = 31 * _hash + delta;
            delta = shortText == null ? 0 : shortText.hashCode();
            _hash = 31 * _hash + delta;
            delta = attribute == null ? 0 : attribute.hashCode();
            _hash = 31 * _hash + delta;
            delta = readOnly == null ? 0 : readOnly.hashCode();
            _hash = 31 * _hash + delta;
            delta = status == null ? 0 : status.hashCode();
            _hash = 31 * _hash + delta;
            delta = note == null ? 0 : note.hashCode();
            _hash = 31 * _hash + delta;
            delta = expirationTime == null ? 0 : expirationTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = createdTime == null ? 0 : createdTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
