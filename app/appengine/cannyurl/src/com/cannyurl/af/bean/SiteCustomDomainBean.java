package com.cannyurl.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.SiteCustomDomain;
import com.myurldb.ws.stub.SiteCustomDomainStub;


// Wrapper class + bean combo.
public class SiteCustomDomainBean implements SiteCustomDomain, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(SiteCustomDomainBean.class.getName());

    // [1] With an embedded object.
    private SiteCustomDomainStub stub = null;

    // [2] Or, without an embedded object.
    private String guid;
    private String siteDomain;
    private String domain;
    private String status;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public SiteCustomDomainBean()
    {
        //this((String) null);
    }
    public SiteCustomDomainBean(String guid)
    {
        this(guid, null, null, null, null, null);
    }
    public SiteCustomDomainBean(String guid, String siteDomain, String domain, String status)
    {
        this(guid, siteDomain, domain, status, null, null);
    }
    public SiteCustomDomainBean(String guid, String siteDomain, String domain, String status, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.siteDomain = siteDomain;
        this.domain = domain;
        this.status = status;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public SiteCustomDomainBean(SiteCustomDomain stub)
    {
        if(stub instanceof SiteCustomDomainStub) {
            this.stub = (SiteCustomDomainStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setSiteDomain(stub.getSiteDomain());   
            setDomain(stub.getDomain());   
            setStatus(stub.getStatus());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public String getSiteDomain()
    {
        if(getStub() != null) {
            return getStub().getSiteDomain();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.siteDomain;
        }
    }
    public void setSiteDomain(String siteDomain)
    {
        if(getStub() != null) {
            getStub().setSiteDomain(siteDomain);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.siteDomain = siteDomain;
        }
    }

    public String getDomain()
    {
        if(getStub() != null) {
            return getStub().getDomain();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.domain;
        }
    }
    public void setDomain(String domain)
    {
        if(getStub() != null) {
            getStub().setDomain(domain);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.domain = domain;
        }
    }

    public String getStatus()
    {
        if(getStub() != null) {
            return getStub().getStatus();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.status;
        }
    }
    public void setStatus(String status)
    {
        if(getStub() != null) {
            getStub().setStatus(status);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.status = status;
        }
    }

    public Long getCreatedTime()
    {
        if(getStub() != null) {
            return getStub().getCreatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.createdTime;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getStub() != null) {
            getStub().setCreatedTime(createdTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.createdTime = createdTime;
        }
    }

    public Long getModifiedTime()
    {
        if(getStub() != null) {
            return getStub().getModifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.modifiedTime;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getStub() != null) {
            getStub().setModifiedTime(modifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.modifiedTime = modifiedTime;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public SiteCustomDomainStub getStub()
    {
        return this.stub;
    }
    protected void setStub(SiteCustomDomainStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("guid = " + this.guid).append(";");
            sb.append("siteDomain = " + this.siteDomain).append(";");
            sb.append("domain = " + this.domain).append(";");
            sb.append("status = " + this.status).append(";");
            sb.append("createdTime = " + this.createdTime).append(";");
            sb.append("modifiedTime = " + this.modifiedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = guid == null ? 0 : guid.hashCode();
            _hash = 31 * _hash + delta;
            delta = siteDomain == null ? 0 : siteDomain.hashCode();
            _hash = 31 * _hash + delta;
            delta = domain == null ? 0 : domain.hashCode();
            _hash = 31 * _hash + delta;
            delta = status == null ? 0 : status.hashCode();
            _hash = 31 * _hash + delta;
            delta = createdTime == null ? 0 : createdTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
