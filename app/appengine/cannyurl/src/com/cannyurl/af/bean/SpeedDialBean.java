package com.cannyurl.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.SpeedDial;
import com.myurldb.ws.stub.SpeedDialStub;


// Wrapper class + bean combo.
public class SpeedDialBean implements SpeedDial, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(SpeedDialBean.class.getName());

    // [1] With an embedded object.
    private SpeedDialStub stub = null;

    // [2] Or, without an embedded object.
    private String guid;
    private String user;
    private String code;
    private String shortcut;
    private String shortLink;
    private String keywordLink;
    private String bookmarkLink;
    private String longUrl;
    private String shortUrl;
    private Boolean caseSensitive;
    private String status;
    private String note;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public SpeedDialBean()
    {
        //this((String) null);
    }
    public SpeedDialBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public SpeedDialBean(String guid, String user, String code, String shortcut, String shortLink, String keywordLink, String bookmarkLink, String longUrl, String shortUrl, Boolean caseSensitive, String status, String note)
    {
        this(guid, user, code, shortcut, shortLink, keywordLink, bookmarkLink, longUrl, shortUrl, caseSensitive, status, note, null, null);
    }
    public SpeedDialBean(String guid, String user, String code, String shortcut, String shortLink, String keywordLink, String bookmarkLink, String longUrl, String shortUrl, Boolean caseSensitive, String status, String note, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.user = user;
        this.code = code;
        this.shortcut = shortcut;
        this.shortLink = shortLink;
        this.keywordLink = keywordLink;
        this.bookmarkLink = bookmarkLink;
        this.longUrl = longUrl;
        this.shortUrl = shortUrl;
        this.caseSensitive = caseSensitive;
        this.status = status;
        this.note = note;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public SpeedDialBean(SpeedDial stub)
    {
        if(stub instanceof SpeedDialStub) {
            this.stub = (SpeedDialStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setUser(stub.getUser());   
            setCode(stub.getCode());   
            setShortcut(stub.getShortcut());   
            setShortLink(stub.getShortLink());   
            setKeywordLink(stub.getKeywordLink());   
            setBookmarkLink(stub.getBookmarkLink());   
            setLongUrl(stub.getLongUrl());   
            setShortUrl(stub.getShortUrl());   
            setCaseSensitive(stub.isCaseSensitive());   
            setStatus(stub.getStatus());   
            setNote(stub.getNote());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public String getUser()
    {
        if(getStub() != null) {
            return getStub().getUser();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.user;
        }
    }
    public void setUser(String user)
    {
        if(getStub() != null) {
            getStub().setUser(user);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.user = user;
        }
    }

    public String getCode()
    {
        if(getStub() != null) {
            return getStub().getCode();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.code;
        }
    }
    public void setCode(String code)
    {
        if(getStub() != null) {
            getStub().setCode(code);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.code = code;
        }
    }

    public String getShortcut()
    {
        if(getStub() != null) {
            return getStub().getShortcut();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.shortcut;
        }
    }
    public void setShortcut(String shortcut)
    {
        if(getStub() != null) {
            getStub().setShortcut(shortcut);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.shortcut = shortcut;
        }
    }

    public String getShortLink()
    {
        if(getStub() != null) {
            return getStub().getShortLink();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.shortLink;
        }
    }
    public void setShortLink(String shortLink)
    {
        if(getStub() != null) {
            getStub().setShortLink(shortLink);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.shortLink = shortLink;
        }
    }

    public String getKeywordLink()
    {
        if(getStub() != null) {
            return getStub().getKeywordLink();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.keywordLink;
        }
    }
    public void setKeywordLink(String keywordLink)
    {
        if(getStub() != null) {
            getStub().setKeywordLink(keywordLink);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.keywordLink = keywordLink;
        }
    }

    public String getBookmarkLink()
    {
        if(getStub() != null) {
            return getStub().getBookmarkLink();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.bookmarkLink;
        }
    }
    public void setBookmarkLink(String bookmarkLink)
    {
        if(getStub() != null) {
            getStub().setBookmarkLink(bookmarkLink);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.bookmarkLink = bookmarkLink;
        }
    }

    public String getLongUrl()
    {
        if(getStub() != null) {
            return getStub().getLongUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.longUrl;
        }
    }
    public void setLongUrl(String longUrl)
    {
        if(getStub() != null) {
            getStub().setLongUrl(longUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.longUrl = longUrl;
        }
    }

    public String getShortUrl()
    {
        if(getStub() != null) {
            return getStub().getShortUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.shortUrl;
        }
    }
    public void setShortUrl(String shortUrl)
    {
        if(getStub() != null) {
            getStub().setShortUrl(shortUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.shortUrl = shortUrl;
        }
    }

    public Boolean isCaseSensitive()
    {
        if(getStub() != null) {
            return getStub().isCaseSensitive();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.caseSensitive;
        }
    }
    public void setCaseSensitive(Boolean caseSensitive)
    {
        if(getStub() != null) {
            getStub().setCaseSensitive(caseSensitive);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.caseSensitive = caseSensitive;
        }
    }

    public String getStatus()
    {
        if(getStub() != null) {
            return getStub().getStatus();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.status;
        }
    }
    public void setStatus(String status)
    {
        if(getStub() != null) {
            getStub().setStatus(status);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.status = status;
        }
    }

    public String getNote()
    {
        if(getStub() != null) {
            return getStub().getNote();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.note;
        }
    }
    public void setNote(String note)
    {
        if(getStub() != null) {
            getStub().setNote(note);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.note = note;
        }
    }

    public Long getCreatedTime()
    {
        if(getStub() != null) {
            return getStub().getCreatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.createdTime;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getStub() != null) {
            getStub().setCreatedTime(createdTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.createdTime = createdTime;
        }
    }

    public Long getModifiedTime()
    {
        if(getStub() != null) {
            return getStub().getModifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.modifiedTime;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getStub() != null) {
            getStub().setModifiedTime(modifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.modifiedTime = modifiedTime;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public SpeedDialStub getStub()
    {
        return this.stub;
    }
    protected void setStub(SpeedDialStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("guid = " + this.guid).append(";");
            sb.append("user = " + this.user).append(";");
            sb.append("code = " + this.code).append(";");
            sb.append("shortcut = " + this.shortcut).append(";");
            sb.append("shortLink = " + this.shortLink).append(";");
            sb.append("keywordLink = " + this.keywordLink).append(";");
            sb.append("bookmarkLink = " + this.bookmarkLink).append(";");
            sb.append("longUrl = " + this.longUrl).append(";");
            sb.append("shortUrl = " + this.shortUrl).append(";");
            sb.append("caseSensitive = " + this.caseSensitive).append(";");
            sb.append("status = " + this.status).append(";");
            sb.append("note = " + this.note).append(";");
            sb.append("createdTime = " + this.createdTime).append(";");
            sb.append("modifiedTime = " + this.modifiedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = guid == null ? 0 : guid.hashCode();
            _hash = 31 * _hash + delta;
            delta = user == null ? 0 : user.hashCode();
            _hash = 31 * _hash + delta;
            delta = code == null ? 0 : code.hashCode();
            _hash = 31 * _hash + delta;
            delta = shortcut == null ? 0 : shortcut.hashCode();
            _hash = 31 * _hash + delta;
            delta = shortLink == null ? 0 : shortLink.hashCode();
            _hash = 31 * _hash + delta;
            delta = keywordLink == null ? 0 : keywordLink.hashCode();
            _hash = 31 * _hash + delta;
            delta = bookmarkLink == null ? 0 : bookmarkLink.hashCode();
            _hash = 31 * _hash + delta;
            delta = longUrl == null ? 0 : longUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = shortUrl == null ? 0 : shortUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = caseSensitive == null ? 0 : caseSensitive.hashCode();
            _hash = 31 * _hash + delta;
            delta = status == null ? 0 : status.hashCode();
            _hash = 31 * _hash + delta;
            delta = note == null ? 0 : note.hashCode();
            _hash = 31 * _hash + delta;
            delta = createdTime == null ? 0 : createdTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
