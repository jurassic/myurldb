package com.cannyurl.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.StreetAddressStruct;
import com.myurldb.ws.stub.StreetAddressStructStub;


// Wrapper class + bean combo.
public class StreetAddressStructBean implements StreetAddressStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(StreetAddressStructBean.class.getName());

    // [1] With an embedded object.
    private StreetAddressStructStub stub = null;

    // [2] Or, without an embedded object.
    private String uuid;
    private String street1;
    private String street2;
    private String city;
    private String county;
    private String postalCode;
    private String state;
    private String province;
    private String country;
    private String countryName;
    private String note;

    // Ctors.
    public StreetAddressStructBean()
    {
        //this((String) null);
    }
    public StreetAddressStructBean(String uuid, String street1, String street2, String city, String county, String postalCode, String state, String province, String country, String countryName, String note)
    {
        this.uuid = uuid;
        this.street1 = street1;
        this.street2 = street2;
        this.city = city;
        this.county = county;
        this.postalCode = postalCode;
        this.state = state;
        this.province = province;
        this.country = country;
        this.countryName = countryName;
        this.note = note;
    }
    public StreetAddressStructBean(StreetAddressStruct stub)
    {
        if(stub instanceof StreetAddressStructStub) {
            this.stub = (StreetAddressStructStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setUuid(stub.getUuid());   
            setStreet1(stub.getStreet1());   
            setStreet2(stub.getStreet2());   
            setCity(stub.getCity());   
            setCounty(stub.getCounty());   
            setPostalCode(stub.getPostalCode());   
            setState(stub.getState());   
            setProvince(stub.getProvince());   
            setCountry(stub.getCountry());   
            setCountryName(stub.getCountryName());   
            setNote(stub.getNote());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getUuid()
    {
        if(getStub() != null) {
            return getStub().getUuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.uuid;
        }
    }
    public void setUuid(String uuid)
    {
        if(getStub() != null) {
            getStub().setUuid(uuid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.uuid = uuid;
        }
    }

    public String getStreet1()
    {
        if(getStub() != null) {
            return getStub().getStreet1();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.street1;
        }
    }
    public void setStreet1(String street1)
    {
        if(getStub() != null) {
            getStub().setStreet1(street1);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.street1 = street1;
        }
    }

    public String getStreet2()
    {
        if(getStub() != null) {
            return getStub().getStreet2();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.street2;
        }
    }
    public void setStreet2(String street2)
    {
        if(getStub() != null) {
            getStub().setStreet2(street2);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.street2 = street2;
        }
    }

    public String getCity()
    {
        if(getStub() != null) {
            return getStub().getCity();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.city;
        }
    }
    public void setCity(String city)
    {
        if(getStub() != null) {
            getStub().setCity(city);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.city = city;
        }
    }

    public String getCounty()
    {
        if(getStub() != null) {
            return getStub().getCounty();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.county;
        }
    }
    public void setCounty(String county)
    {
        if(getStub() != null) {
            getStub().setCounty(county);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.county = county;
        }
    }

    public String getPostalCode()
    {
        if(getStub() != null) {
            return getStub().getPostalCode();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.postalCode;
        }
    }
    public void setPostalCode(String postalCode)
    {
        if(getStub() != null) {
            getStub().setPostalCode(postalCode);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.postalCode = postalCode;
        }
    }

    public String getState()
    {
        if(getStub() != null) {
            return getStub().getState();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.state;
        }
    }
    public void setState(String state)
    {
        if(getStub() != null) {
            getStub().setState(state);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.state = state;
        }
    }

    public String getProvince()
    {
        if(getStub() != null) {
            return getStub().getProvince();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.province;
        }
    }
    public void setProvince(String province)
    {
        if(getStub() != null) {
            getStub().setProvince(province);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.province = province;
        }
    }

    public String getCountry()
    {
        if(getStub() != null) {
            return getStub().getCountry();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.country;
        }
    }
    public void setCountry(String country)
    {
        if(getStub() != null) {
            getStub().setCountry(country);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.country = country;
        }
    }

    public String getCountryName()
    {
        if(getStub() != null) {
            return getStub().getCountryName();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.countryName;
        }
    }
    public void setCountryName(String countryName)
    {
        if(getStub() != null) {
            getStub().setCountryName(countryName);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.countryName = countryName;
        }
    }

    public String getNote()
    {
        if(getStub() != null) {
            return getStub().getNote();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.note;
        }
    }
    public void setNote(String note)
    {
        if(getStub() != null) {
            getStub().setNote(note);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.note = note;
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getStreet1() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getStreet2() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getCity() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getCounty() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPostalCode() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getState() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getProvince() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getCountry() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getCountryName() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // Returns the reference to the embedded object. (Could be null.)
    public StreetAddressStructStub getStub()
    {
        return this.stub;
    }
    protected void setStub(StreetAddressStructStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("uuid = " + this.uuid).append(";");
            sb.append("street1 = " + this.street1).append(";");
            sb.append("street2 = " + this.street2).append(";");
            sb.append("city = " + this.city).append(";");
            sb.append("county = " + this.county).append(";");
            sb.append("postalCode = " + this.postalCode).append(";");
            sb.append("state = " + this.state).append(";");
            sb.append("province = " + this.province).append(";");
            sb.append("country = " + this.country).append(";");
            sb.append("countryName = " + this.countryName).append(";");
            sb.append("note = " + this.note).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = uuid == null ? 0 : uuid.hashCode();
            _hash = 31 * _hash + delta;
            delta = street1 == null ? 0 : street1.hashCode();
            _hash = 31 * _hash + delta;
            delta = street2 == null ? 0 : street2.hashCode();
            _hash = 31 * _hash + delta;
            delta = city == null ? 0 : city.hashCode();
            _hash = 31 * _hash + delta;
            delta = county == null ? 0 : county.hashCode();
            _hash = 31 * _hash + delta;
            delta = postalCode == null ? 0 : postalCode.hashCode();
            _hash = 31 * _hash + delta;
            delta = state == null ? 0 : state.hashCode();
            _hash = 31 * _hash + delta;
            delta = province == null ? 0 : province.hashCode();
            _hash = 31 * _hash + delta;
            delta = country == null ? 0 : country.hashCode();
            _hash = 31 * _hash + delta;
            delta = countryName == null ? 0 : countryName.hashCode();
            _hash = 31 * _hash + delta;
            delta = note == null ? 0 : note.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
