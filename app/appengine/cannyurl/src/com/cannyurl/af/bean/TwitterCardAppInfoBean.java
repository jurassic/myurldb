package com.cannyurl.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.stub.TwitterCardAppInfoStub;


// Wrapper class + bean combo.
public class TwitterCardAppInfoBean implements TwitterCardAppInfo, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(TwitterCardAppInfoBean.class.getName());

    // [1] With an embedded object.
    private TwitterCardAppInfoStub stub = null;

    // [2] Or, without an embedded object.
    private String name;
    private String id;
    private String url;

    // Ctors.
    public TwitterCardAppInfoBean()
    {
        //this((String) null);
    }
    public TwitterCardAppInfoBean(String name, String id, String url)
    {
        this.name = name;
        this.id = id;
        this.url = url;
    }
    public TwitterCardAppInfoBean(TwitterCardAppInfo stub)
    {
        if(stub instanceof TwitterCardAppInfoStub) {
            this.stub = (TwitterCardAppInfoStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setName(stub.getName());   
            setId(stub.getId());   
            setUrl(stub.getUrl());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getName()
    {
        if(getStub() != null) {
            return getStub().getName();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.name;
        }
    }
    public void setName(String name)
    {
        if(getStub() != null) {
            getStub().setName(name);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.name = name;
        }
    }

    public String getId()
    {
        if(getStub() != null) {
            return getStub().getId();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.id;
        }
    }
    public void setId(String id)
    {
        if(getStub() != null) {
            getStub().setId(id);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.id = id;
        }
    }

    public String getUrl()
    {
        if(getStub() != null) {
            return getStub().getUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.url;
        }
    }
    public void setUrl(String url)
    {
        if(getStub() != null) {
            getStub().setUrl(url);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.url = url;
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getName() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getId() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getUrl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // Returns the reference to the embedded object. (Could be null.)
    public TwitterCardAppInfoStub getStub()
    {
        return this.stub;
    }
    protected void setStub(TwitterCardAppInfoStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("name = " + this.name).append(";");
            sb.append("id = " + this.id).append(";");
            sb.append("url = " + this.url).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = name == null ? 0 : name.hashCode();
            _hash = 31 * _hash + delta;
            delta = id == null ? 0 : id.hashCode();
            _hash = 31 * _hash + delta;
            delta = url == null ? 0 : url.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
