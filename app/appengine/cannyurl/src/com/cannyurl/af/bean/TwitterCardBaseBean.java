package com.cannyurl.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.stub.TwitterCardAppInfoStub;
import com.myurldb.ws.stub.TwitterCardProductDataStub;
import com.myurldb.ws.TwitterCardBase;
import com.myurldb.ws.stub.TwitterCardBaseStub;


// Wrapper class + bean combo.
public abstract class TwitterCardBaseBean implements TwitterCardBase, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(TwitterCardBaseBean.class.getName());

    // [1] With an embedded object.
    private TwitterCardBaseStub stub = null;

    // [2] Or, without an embedded object.
    private String guid;
    private String card;
    private String url;
    private String title;
    private String description;
    private String site;
    private String siteId;
    private String creator;
    private String creatorId;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public TwitterCardBaseBean()
    {
        //this((String) null);
    }
    public TwitterCardBaseBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null);
    }
    public TwitterCardBaseBean(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId)
    {
        this(guid, card, url, title, description, site, siteId, creator, creatorId, null, null);
    }
    public TwitterCardBaseBean(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.card = card;
        this.url = url;
        this.title = title;
        this.description = description;
        this.site = site;
        this.siteId = siteId;
        this.creator = creator;
        this.creatorId = creatorId;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public TwitterCardBaseBean(TwitterCardBase stub)
    {
        if(stub instanceof TwitterCardBaseStub) {
            this.stub = (TwitterCardBaseStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setCard(stub.getCard());   
            setUrl(stub.getUrl());   
            setTitle(stub.getTitle());   
            setDescription(stub.getDescription());   
            setSite(stub.getSite());   
            setSiteId(stub.getSiteId());   
            setCreator(stub.getCreator());   
            setCreatorId(stub.getCreatorId());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public String getCard()
    {
        if(getStub() != null) {
            return getStub().getCard();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.card;
        }
    }
    public void setCard(String card)
    {
        if(getStub() != null) {
            getStub().setCard(card);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.card = card;
        }
    }

    public String getUrl()
    {
        if(getStub() != null) {
            return getStub().getUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.url;
        }
    }
    public void setUrl(String url)
    {
        if(getStub() != null) {
            getStub().setUrl(url);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.url = url;
        }
    }

    public String getTitle()
    {
        if(getStub() != null) {
            return getStub().getTitle();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.title;
        }
    }
    public void setTitle(String title)
    {
        if(getStub() != null) {
            getStub().setTitle(title);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.title = title;
        }
    }

    public String getDescription()
    {
        if(getStub() != null) {
            return getStub().getDescription();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.description;
        }
    }
    public void setDescription(String description)
    {
        if(getStub() != null) {
            getStub().setDescription(description);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.description = description;
        }
    }

    public String getSite()
    {
        if(getStub() != null) {
            return getStub().getSite();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.site;
        }
    }
    public void setSite(String site)
    {
        if(getStub() != null) {
            getStub().setSite(site);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.site = site;
        }
    }

    public String getSiteId()
    {
        if(getStub() != null) {
            return getStub().getSiteId();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.siteId;
        }
    }
    public void setSiteId(String siteId)
    {
        if(getStub() != null) {
            getStub().setSiteId(siteId);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.siteId = siteId;
        }
    }

    public String getCreator()
    {
        if(getStub() != null) {
            return getStub().getCreator();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.creator;
        }
    }
    public void setCreator(String creator)
    {
        if(getStub() != null) {
            getStub().setCreator(creator);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.creator = creator;
        }
    }

    public String getCreatorId()
    {
        if(getStub() != null) {
            return getStub().getCreatorId();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.creatorId;
        }
    }
    public void setCreatorId(String creatorId)
    {
        if(getStub() != null) {
            getStub().setCreatorId(creatorId);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.creatorId = creatorId;
        }
    }

    public Long getCreatedTime()
    {
        if(getStub() != null) {
            return getStub().getCreatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.createdTime;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getStub() != null) {
            getStub().setCreatedTime(createdTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.createdTime = createdTime;
        }
    }

    public Long getModifiedTime()
    {
        if(getStub() != null) {
            return getStub().getModifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.modifiedTime;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getStub() != null) {
            getStub().setModifiedTime(modifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.modifiedTime = modifiedTime;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public TwitterCardBaseStub getStub()
    {
        return this.stub;
    }
    protected void setStub(TwitterCardBaseStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("guid = " + this.guid).append(";");
            sb.append("card = " + this.card).append(";");
            sb.append("url = " + this.url).append(";");
            sb.append("title = " + this.title).append(";");
            sb.append("description = " + this.description).append(";");
            sb.append("site = " + this.site).append(";");
            sb.append("siteId = " + this.siteId).append(";");
            sb.append("creator = " + this.creator).append(";");
            sb.append("creatorId = " + this.creatorId).append(";");
            sb.append("createdTime = " + this.createdTime).append(";");
            sb.append("modifiedTime = " + this.modifiedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = guid == null ? 0 : guid.hashCode();
            _hash = 31 * _hash + delta;
            delta = card == null ? 0 : card.hashCode();
            _hash = 31 * _hash + delta;
            delta = url == null ? 0 : url.hashCode();
            _hash = 31 * _hash + delta;
            delta = title == null ? 0 : title.hashCode();
            _hash = 31 * _hash + delta;
            delta = description == null ? 0 : description.hashCode();
            _hash = 31 * _hash + delta;
            delta = site == null ? 0 : site.hashCode();
            _hash = 31 * _hash + delta;
            delta = siteId == null ? 0 : siteId.hashCode();
            _hash = 31 * _hash + delta;
            delta = creator == null ? 0 : creator.hashCode();
            _hash = 31 * _hash + delta;
            delta = creatorId == null ? 0 : creatorId.hashCode();
            _hash = 31 * _hash + delta;
            delta = createdTime == null ? 0 : createdTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
