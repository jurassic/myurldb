package com.cannyurl.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.stub.TwitterCardProductDataStub;


// Wrapper class + bean combo.
public class TwitterCardProductDataBean implements TwitterCardProductData, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(TwitterCardProductDataBean.class.getName());

    // [1] With an embedded object.
    private TwitterCardProductDataStub stub = null;

    // [2] Or, without an embedded object.
    private String data;
    private String label;

    // Ctors.
    public TwitterCardProductDataBean()
    {
        //this((String) null);
    }
    public TwitterCardProductDataBean(String data, String label)
    {
        this.data = data;
        this.label = label;
    }
    public TwitterCardProductDataBean(TwitterCardProductData stub)
    {
        if(stub instanceof TwitterCardProductDataStub) {
            this.stub = (TwitterCardProductDataStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setData(stub.getData());   
            setLabel(stub.getLabel());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getData()
    {
        if(getStub() != null) {
            return getStub().getData();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.data;
        }
    }
    public void setData(String data)
    {
        if(getStub() != null) {
            getStub().setData(data);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.data = data;
        }
    }

    public String getLabel()
    {
        if(getStub() != null) {
            return getStub().getLabel();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.label;
        }
    }
    public void setLabel(String label)
    {
        if(getStub() != null) {
            getStub().setLabel(label);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.label = label;
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getData() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLabel() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // Returns the reference to the embedded object. (Could be null.)
    public TwitterCardProductDataStub getStub()
    {
        return this.stub;
    }
    protected void setStub(TwitterCardProductDataStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("data = " + this.data).append(";");
            sb.append("label = " + this.label).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = data == null ? 0 : data.hashCode();
            _hash = 31 * _hash + delta;
            delta = label == null ? 0 : label.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
