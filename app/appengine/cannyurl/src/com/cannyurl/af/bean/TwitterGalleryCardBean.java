package com.cannyurl.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.stub.TwitterCardAppInfoStub;
import com.myurldb.ws.stub.TwitterCardProductDataStub;
import com.myurldb.ws.TwitterGalleryCard;
import com.myurldb.ws.stub.TwitterCardBaseStub;
import com.myurldb.ws.stub.TwitterGalleryCardStub;


// Wrapper class + bean combo.
public class TwitterGalleryCardBean extends TwitterCardBaseBean implements TwitterGalleryCard, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(TwitterGalleryCardBean.class.getName());


    // [2] Or, without an embedded object.
    private String image0;
    private String image1;
    private String image2;
    private String image3;

    // Ctors.
    public TwitterGalleryCardBean()
    {
        //this((String) null);
    }
    public TwitterGalleryCardBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public TwitterGalleryCardBean(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image0, String image1, String image2, String image3)
    {
        this(guid, card, url, title, description, site, siteId, creator, creatorId, image0, image1, image2, image3, null, null);
    }
    public TwitterGalleryCardBean(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image0, String image1, String image2, String image3, Long createdTime, Long modifiedTime)
    {
        super(guid, card, url, title, description, site, siteId, creator, creatorId, createdTime, modifiedTime);

        this.image0 = image0;
        this.image1 = image1;
        this.image2 = image2;
        this.image3 = image3;
    }
    public TwitterGalleryCardBean(TwitterGalleryCard stub)
    {
        if(stub instanceof TwitterGalleryCardStub) {
            super.setStub((TwitterCardBaseStub) stub);
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setCard(stub.getCard());   
            setUrl(stub.getUrl());   
            setTitle(stub.getTitle());   
            setDescription(stub.getDescription());   
            setSite(stub.getSite());   
            setSiteId(stub.getSiteId());   
            setCreator(stub.getCreator());   
            setCreatorId(stub.getCreatorId());   
            setImage0(stub.getImage0());   
            setImage1(stub.getImage1());   
            setImage2(stub.getImage2());   
            setImage3(stub.getImage3());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    public String getCard()
    {
        return super.getCard();
    }
    public void setCard(String card)
    {
        super.setCard(card);
    }

    public String getUrl()
    {
        return super.getUrl();
    }
    public void setUrl(String url)
    {
        super.setUrl(url);
    }

    public String getTitle()
    {
        return super.getTitle();
    }
    public void setTitle(String title)
    {
        super.setTitle(title);
    }

    public String getDescription()
    {
        return super.getDescription();
    }
    public void setDescription(String description)
    {
        super.setDescription(description);
    }

    public String getSite()
    {
        return super.getSite();
    }
    public void setSite(String site)
    {
        super.setSite(site);
    }

    public String getSiteId()
    {
        return super.getSiteId();
    }
    public void setSiteId(String siteId)
    {
        super.setSiteId(siteId);
    }

    public String getCreator()
    {
        return super.getCreator();
    }
    public void setCreator(String creator)
    {
        super.setCreator(creator);
    }

    public String getCreatorId()
    {
        return super.getCreatorId();
    }
    public void setCreatorId(String creatorId)
    {
        super.setCreatorId(creatorId);
    }

    public String getImage0()
    {
        if(getStub() != null) {
            return getStub().getImage0();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.image0;
        }
    }
    public void setImage0(String image0)
    {
        if(getStub() != null) {
            getStub().setImage0(image0);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.image0 = image0;
        }
    }

    public String getImage1()
    {
        if(getStub() != null) {
            return getStub().getImage1();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.image1;
        }
    }
    public void setImage1(String image1)
    {
        if(getStub() != null) {
            getStub().setImage1(image1);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.image1 = image1;
        }
    }

    public String getImage2()
    {
        if(getStub() != null) {
            return getStub().getImage2();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.image2;
        }
    }
    public void setImage2(String image2)
    {
        if(getStub() != null) {
            getStub().setImage2(image2);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.image2 = image2;
        }
    }

    public String getImage3()
    {
        if(getStub() != null) {
            return getStub().getImage3();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.image3;
        }
    }
    public void setImage3(String image3)
    {
        if(getStub() != null) {
            getStub().setImage3(image3);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.image3 = image3;
        }
    }

    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    // Returns the reference to the embedded object. (Could be null.)
    public TwitterGalleryCardStub getStub()
    {
        return (TwitterGalleryCardStub) super.getStub();
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder( super.toString() );
            sb.append("image0 = " + this.image0).append(";");
            sb.append("image1 = " + this.image1).append(";");
            sb.append("image2 = " + this.image2).append(";");
            sb.append("image3 = " + this.image3).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = image0 == null ? 0 : image0.hashCode();
            _hash = 31 * _hash + delta;
            delta = image1 == null ? 0 : image1.hashCode();
            _hash = 31 * _hash + delta;
            delta = image2 == null ? 0 : image2.hashCode();
            _hash = 31 * _hash + delta;
            delta = image3 == null ? 0 : image3.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
