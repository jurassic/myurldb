package com.cannyurl.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.stub.TwitterCardAppInfoStub;
import com.myurldb.ws.stub.TwitterCardProductDataStub;
import com.myurldb.ws.TwitterPhotoCard;
import com.myurldb.ws.stub.TwitterCardBaseStub;
import com.myurldb.ws.stub.TwitterPhotoCardStub;


// Wrapper class + bean combo.
public class TwitterPhotoCardBean extends TwitterCardBaseBean implements TwitterPhotoCard, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(TwitterPhotoCardBean.class.getName());


    // [2] Or, without an embedded object.
    private String image;
    private Integer imageWidth;
    private Integer imageHeight;

    // Ctors.
    public TwitterPhotoCardBean()
    {
        //this((String) null);
    }
    public TwitterPhotoCardBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public TwitterPhotoCardBean(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight)
    {
        this(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, null, null);
    }
    public TwitterPhotoCardBean(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, Long createdTime, Long modifiedTime)
    {
        super(guid, card, url, title, description, site, siteId, creator, creatorId, createdTime, modifiedTime);

        this.image = image;
        this.imageWidth = imageWidth;
        this.imageHeight = imageHeight;
    }
    public TwitterPhotoCardBean(TwitterPhotoCard stub)
    {
        if(stub instanceof TwitterPhotoCardStub) {
            super.setStub((TwitterCardBaseStub) stub);
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setCard(stub.getCard());   
            setUrl(stub.getUrl());   
            setTitle(stub.getTitle());   
            setDescription(stub.getDescription());   
            setSite(stub.getSite());   
            setSiteId(stub.getSiteId());   
            setCreator(stub.getCreator());   
            setCreatorId(stub.getCreatorId());   
            setImage(stub.getImage());   
            setImageWidth(stub.getImageWidth());   
            setImageHeight(stub.getImageHeight());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    public String getCard()
    {
        return super.getCard();
    }
    public void setCard(String card)
    {
        super.setCard(card);
    }

    public String getUrl()
    {
        return super.getUrl();
    }
    public void setUrl(String url)
    {
        super.setUrl(url);
    }

    public String getTitle()
    {
        return super.getTitle();
    }
    public void setTitle(String title)
    {
        super.setTitle(title);
    }

    public String getDescription()
    {
        return super.getDescription();
    }
    public void setDescription(String description)
    {
        super.setDescription(description);
    }

    public String getSite()
    {
        return super.getSite();
    }
    public void setSite(String site)
    {
        super.setSite(site);
    }

    public String getSiteId()
    {
        return super.getSiteId();
    }
    public void setSiteId(String siteId)
    {
        super.setSiteId(siteId);
    }

    public String getCreator()
    {
        return super.getCreator();
    }
    public void setCreator(String creator)
    {
        super.setCreator(creator);
    }

    public String getCreatorId()
    {
        return super.getCreatorId();
    }
    public void setCreatorId(String creatorId)
    {
        super.setCreatorId(creatorId);
    }

    public String getImage()
    {
        if(getStub() != null) {
            return getStub().getImage();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.image;
        }
    }
    public void setImage(String image)
    {
        if(getStub() != null) {
            getStub().setImage(image);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.image = image;
        }
    }

    public Integer getImageWidth()
    {
        if(getStub() != null) {
            return getStub().getImageWidth();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.imageWidth;
        }
    }
    public void setImageWidth(Integer imageWidth)
    {
        if(getStub() != null) {
            getStub().setImageWidth(imageWidth);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.imageWidth = imageWidth;
        }
    }

    public Integer getImageHeight()
    {
        if(getStub() != null) {
            return getStub().getImageHeight();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.imageHeight;
        }
    }
    public void setImageHeight(Integer imageHeight)
    {
        if(getStub() != null) {
            getStub().setImageHeight(imageHeight);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.imageHeight = imageHeight;
        }
    }

    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    // Returns the reference to the embedded object. (Could be null.)
    public TwitterPhotoCardStub getStub()
    {
        return (TwitterPhotoCardStub) super.getStub();
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder( super.toString() );
            sb.append("image = " + this.image).append(";");
            sb.append("imageWidth = " + this.imageWidth).append(";");
            sb.append("imageHeight = " + this.imageHeight).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = image == null ? 0 : image.hashCode();
            _hash = 31 * _hash + delta;
            delta = imageWidth == null ? 0 : imageWidth.hashCode();
            _hash = 31 * _hash + delta;
            delta = imageHeight == null ? 0 : imageHeight.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
