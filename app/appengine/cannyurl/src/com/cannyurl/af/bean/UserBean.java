package com.cannyurl.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.GeoPointStruct;
import com.myurldb.ws.StreetAddressStruct;
import com.myurldb.ws.GaeAppStruct;
import com.myurldb.ws.FullNameStruct;
import com.myurldb.ws.GaeUserStruct;
import com.myurldb.ws.stub.GeoPointStructStub;
import com.myurldb.ws.stub.StreetAddressStructStub;
import com.myurldb.ws.stub.GaeAppStructStub;
import com.myurldb.ws.stub.FullNameStructStub;
import com.myurldb.ws.stub.GaeUserStructStub;
import com.myurldb.ws.User;
import com.myurldb.ws.stub.UserStub;


// Wrapper class + bean combo.
public class UserBean implements User, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UserBean.class.getName());

    // [1] With an embedded object.
    private UserStub stub = null;

    // [2] Or, without an embedded object.
    private String guid;
    private String managerApp;
    private Long appAcl;
    private GaeAppStructBean gaeApp;
    private String aeryId;
    private String sessionId;
    private String ancestorGuid;
    private FullNameStructBean name;
    private String usercode;
    private String username;
    private String nickname;
    private String avatar;
    private String email;
    private String openId;
    private GaeUserStructBean gaeUser;
    private String entityType;
    private Boolean surrogate;
    private Boolean obsolete;
    private String timeZone;
    private String location;
    private StreetAddressStructBean streetAddress;
    private GeoPointStructBean geoPoint;
    private String ipAddress;
    private String referer;
    private String status;
    private Long emailVerifiedTime;
    private Long openIdVerifiedTime;
    private Long authenticatedTime;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public UserBean()
    {
        //this((String) null);
    }
    public UserBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public UserBean(String guid, String managerApp, Long appAcl, GaeAppStructBean gaeApp, String aeryId, String sessionId, String ancestorGuid, FullNameStructBean name, String usercode, String username, String nickname, String avatar, String email, String openId, GaeUserStructBean gaeUser, String entityType, Boolean surrogate, Boolean obsolete, String timeZone, String location, StreetAddressStructBean streetAddress, GeoPointStructBean geoPoint, String ipAddress, String referer, String status, Long emailVerifiedTime, Long openIdVerifiedTime, Long authenticatedTime)
    {
        this(guid, managerApp, appAcl, gaeApp, aeryId, sessionId, ancestorGuid, name, usercode, username, nickname, avatar, email, openId, gaeUser, entityType, surrogate, obsolete, timeZone, location, streetAddress, geoPoint, ipAddress, referer, status, emailVerifiedTime, openIdVerifiedTime, authenticatedTime, null, null);
    }
    public UserBean(String guid, String managerApp, Long appAcl, GaeAppStructBean gaeApp, String aeryId, String sessionId, String ancestorGuid, FullNameStructBean name, String usercode, String username, String nickname, String avatar, String email, String openId, GaeUserStructBean gaeUser, String entityType, Boolean surrogate, Boolean obsolete, String timeZone, String location, StreetAddressStructBean streetAddress, GeoPointStructBean geoPoint, String ipAddress, String referer, String status, Long emailVerifiedTime, Long openIdVerifiedTime, Long authenticatedTime, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.managerApp = managerApp;
        this.appAcl = appAcl;
        this.gaeApp = gaeApp;
        this.aeryId = aeryId;
        this.sessionId = sessionId;
        this.ancestorGuid = ancestorGuid;
        this.name = name;
        this.usercode = usercode;
        this.username = username;
        this.nickname = nickname;
        this.avatar = avatar;
        this.email = email;
        this.openId = openId;
        this.gaeUser = gaeUser;
        this.entityType = entityType;
        this.surrogate = surrogate;
        this.obsolete = obsolete;
        this.timeZone = timeZone;
        this.location = location;
        this.streetAddress = streetAddress;
        this.geoPoint = geoPoint;
        this.ipAddress = ipAddress;
        this.referer = referer;
        this.status = status;
        this.emailVerifiedTime = emailVerifiedTime;
        this.openIdVerifiedTime = openIdVerifiedTime;
        this.authenticatedTime = authenticatedTime;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public UserBean(User stub)
    {
        if(stub instanceof UserStub) {
            this.stub = (UserStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setManagerApp(stub.getManagerApp());   
            setAppAcl(stub.getAppAcl());   
            GaeAppStruct gaeApp = stub.getGaeApp();
            if(gaeApp instanceof GaeAppStructBean) {
                setGaeApp((GaeAppStructBean) gaeApp);   
            } else {
                setGaeApp(new GaeAppStructBean(gaeApp));   
            }
            setAeryId(stub.getAeryId());   
            setSessionId(stub.getSessionId());   
            setAncestorGuid(stub.getAncestorGuid());   
            FullNameStruct name = stub.getName();
            if(name instanceof FullNameStructBean) {
                setName((FullNameStructBean) name);   
            } else {
                setName(new FullNameStructBean(name));   
            }
            setUsercode(stub.getUsercode());   
            setUsername(stub.getUsername());   
            setNickname(stub.getNickname());   
            setAvatar(stub.getAvatar());   
            setEmail(stub.getEmail());   
            setOpenId(stub.getOpenId());   
            GaeUserStruct gaeUser = stub.getGaeUser();
            if(gaeUser instanceof GaeUserStructBean) {
                setGaeUser((GaeUserStructBean) gaeUser);   
            } else {
                setGaeUser(new GaeUserStructBean(gaeUser));   
            }
            setEntityType(stub.getEntityType());   
            setSurrogate(stub.isSurrogate());   
            setObsolete(stub.isObsolete());   
            setTimeZone(stub.getTimeZone());   
            setLocation(stub.getLocation());   
            StreetAddressStruct streetAddress = stub.getStreetAddress();
            if(streetAddress instanceof StreetAddressStructBean) {
                setStreetAddress((StreetAddressStructBean) streetAddress);   
            } else {
                setStreetAddress(new StreetAddressStructBean(streetAddress));   
            }
            GeoPointStruct geoPoint = stub.getGeoPoint();
            if(geoPoint instanceof GeoPointStructBean) {
                setGeoPoint((GeoPointStructBean) geoPoint);   
            } else {
                setGeoPoint(new GeoPointStructBean(geoPoint));   
            }
            setIpAddress(stub.getIpAddress());   
            setReferer(stub.getReferer());   
            setStatus(stub.getStatus());   
            setEmailVerifiedTime(stub.getEmailVerifiedTime());   
            setOpenIdVerifiedTime(stub.getOpenIdVerifiedTime());   
            setAuthenticatedTime(stub.getAuthenticatedTime());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public String getManagerApp()
    {
        if(getStub() != null) {
            return getStub().getManagerApp();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.managerApp;
        }
    }
    public void setManagerApp(String managerApp)
    {
        if(getStub() != null) {
            getStub().setManagerApp(managerApp);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.managerApp = managerApp;
        }
    }

    public Long getAppAcl()
    {
        if(getStub() != null) {
            return getStub().getAppAcl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.appAcl;
        }
    }
    public void setAppAcl(Long appAcl)
    {
        if(getStub() != null) {
            getStub().setAppAcl(appAcl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.appAcl = appAcl;
        }
    }

    public GaeAppStruct getGaeApp()
    {  
        if(getStub() != null) {
            // Note the object type.
            GaeAppStruct _stub_field = getStub().getGaeApp();
            if(_stub_field == null) {
                return null;
            } else {
                return new GaeAppStructBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.gaeApp;
        }
    }
    public void setGaeApp(GaeAppStruct gaeApp)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setGaeApp(gaeApp);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(gaeApp == null) {
                this.gaeApp = null;
            } else {
                if(gaeApp instanceof GaeAppStructBean) {
                    this.gaeApp = (GaeAppStructBean) gaeApp;
                } else {
                    this.gaeApp = new GaeAppStructBean(gaeApp);
                }
            }
        }
    }

    public String getAeryId()
    {
        if(getStub() != null) {
            return getStub().getAeryId();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.aeryId;
        }
    }
    public void setAeryId(String aeryId)
    {
        if(getStub() != null) {
            getStub().setAeryId(aeryId);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.aeryId = aeryId;
        }
    }

    public String getSessionId()
    {
        if(getStub() != null) {
            return getStub().getSessionId();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.sessionId;
        }
    }
    public void setSessionId(String sessionId)
    {
        if(getStub() != null) {
            getStub().setSessionId(sessionId);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.sessionId = sessionId;
        }
    }

    public String getAncestorGuid()
    {
        if(getStub() != null) {
            return getStub().getAncestorGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.ancestorGuid;
        }
    }
    public void setAncestorGuid(String ancestorGuid)
    {
        if(getStub() != null) {
            getStub().setAncestorGuid(ancestorGuid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.ancestorGuid = ancestorGuid;
        }
    }

    public FullNameStruct getName()
    {  
        if(getStub() != null) {
            // Note the object type.
            FullNameStruct _stub_field = getStub().getName();
            if(_stub_field == null) {
                return null;
            } else {
                return new FullNameStructBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.name;
        }
    }
    public void setName(FullNameStruct name)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setName(name);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(name == null) {
                this.name = null;
            } else {
                if(name instanceof FullNameStructBean) {
                    this.name = (FullNameStructBean) name;
                } else {
                    this.name = new FullNameStructBean(name);
                }
            }
        }
    }

    public String getUsercode()
    {
        if(getStub() != null) {
            return getStub().getUsercode();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.usercode;
        }
    }
    public void setUsercode(String usercode)
    {
        if(getStub() != null) {
            getStub().setUsercode(usercode);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.usercode = usercode;
        }
    }

    public String getUsername()
    {
        if(getStub() != null) {
            return getStub().getUsername();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.username;
        }
    }
    public void setUsername(String username)
    {
        if(getStub() != null) {
            getStub().setUsername(username);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.username = username;
        }
    }

    public String getNickname()
    {
        if(getStub() != null) {
            return getStub().getNickname();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.nickname;
        }
    }
    public void setNickname(String nickname)
    {
        if(getStub() != null) {
            getStub().setNickname(nickname);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.nickname = nickname;
        }
    }

    public String getAvatar()
    {
        if(getStub() != null) {
            return getStub().getAvatar();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.avatar;
        }
    }
    public void setAvatar(String avatar)
    {
        if(getStub() != null) {
            getStub().setAvatar(avatar);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.avatar = avatar;
        }
    }

    public String getEmail()
    {
        if(getStub() != null) {
            return getStub().getEmail();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.email;
        }
    }
    public void setEmail(String email)
    {
        if(getStub() != null) {
            getStub().setEmail(email);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.email = email;
        }
    }

    public String getOpenId()
    {
        if(getStub() != null) {
            return getStub().getOpenId();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.openId;
        }
    }
    public void setOpenId(String openId)
    {
        if(getStub() != null) {
            getStub().setOpenId(openId);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.openId = openId;
        }
    }

    public GaeUserStruct getGaeUser()
    {  
        if(getStub() != null) {
            // Note the object type.
            GaeUserStruct _stub_field = getStub().getGaeUser();
            if(_stub_field == null) {
                return null;
            } else {
                return new GaeUserStructBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.gaeUser;
        }
    }
    public void setGaeUser(GaeUserStruct gaeUser)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setGaeUser(gaeUser);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(gaeUser == null) {
                this.gaeUser = null;
            } else {
                if(gaeUser instanceof GaeUserStructBean) {
                    this.gaeUser = (GaeUserStructBean) gaeUser;
                } else {
                    this.gaeUser = new GaeUserStructBean(gaeUser);
                }
            }
        }
    }

    public String getEntityType()
    {
        if(getStub() != null) {
            return getStub().getEntityType();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.entityType;
        }
    }
    public void setEntityType(String entityType)
    {
        if(getStub() != null) {
            getStub().setEntityType(entityType);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.entityType = entityType;
        }
    }

    public Boolean isSurrogate()
    {
        if(getStub() != null) {
            return getStub().isSurrogate();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.surrogate;
        }
    }
    public void setSurrogate(Boolean surrogate)
    {
        if(getStub() != null) {
            getStub().setSurrogate(surrogate);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.surrogate = surrogate;
        }
    }

    public Boolean isObsolete()
    {
        if(getStub() != null) {
            return getStub().isObsolete();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.obsolete;
        }
    }
    public void setObsolete(Boolean obsolete)
    {
        if(getStub() != null) {
            getStub().setObsolete(obsolete);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.obsolete = obsolete;
        }
    }

    public String getTimeZone()
    {
        if(getStub() != null) {
            return getStub().getTimeZone();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.timeZone;
        }
    }
    public void setTimeZone(String timeZone)
    {
        if(getStub() != null) {
            getStub().setTimeZone(timeZone);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.timeZone = timeZone;
        }
    }

    public String getLocation()
    {
        if(getStub() != null) {
            return getStub().getLocation();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.location;
        }
    }
    public void setLocation(String location)
    {
        if(getStub() != null) {
            getStub().setLocation(location);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.location = location;
        }
    }

    public StreetAddressStruct getStreetAddress()
    {  
        if(getStub() != null) {
            // Note the object type.
            StreetAddressStruct _stub_field = getStub().getStreetAddress();
            if(_stub_field == null) {
                return null;
            } else {
                return new StreetAddressStructBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.streetAddress;
        }
    }
    public void setStreetAddress(StreetAddressStruct streetAddress)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setStreetAddress(streetAddress);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(streetAddress == null) {
                this.streetAddress = null;
            } else {
                if(streetAddress instanceof StreetAddressStructBean) {
                    this.streetAddress = (StreetAddressStructBean) streetAddress;
                } else {
                    this.streetAddress = new StreetAddressStructBean(streetAddress);
                }
            }
        }
    }

    public GeoPointStruct getGeoPoint()
    {  
        if(getStub() != null) {
            // Note the object type.
            GeoPointStruct _stub_field = getStub().getGeoPoint();
            if(_stub_field == null) {
                return null;
            } else {
                return new GeoPointStructBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.geoPoint;
        }
    }
    public void setGeoPoint(GeoPointStruct geoPoint)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setGeoPoint(geoPoint);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(geoPoint == null) {
                this.geoPoint = null;
            } else {
                if(geoPoint instanceof GeoPointStructBean) {
                    this.geoPoint = (GeoPointStructBean) geoPoint;
                } else {
                    this.geoPoint = new GeoPointStructBean(geoPoint);
                }
            }
        }
    }

    public String getIpAddress()
    {
        if(getStub() != null) {
            return getStub().getIpAddress();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.ipAddress;
        }
    }
    public void setIpAddress(String ipAddress)
    {
        if(getStub() != null) {
            getStub().setIpAddress(ipAddress);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.ipAddress = ipAddress;
        }
    }

    public String getReferer()
    {
        if(getStub() != null) {
            return getStub().getReferer();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.referer;
        }
    }
    public void setReferer(String referer)
    {
        if(getStub() != null) {
            getStub().setReferer(referer);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.referer = referer;
        }
    }

    public String getStatus()
    {
        if(getStub() != null) {
            return getStub().getStatus();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.status;
        }
    }
    public void setStatus(String status)
    {
        if(getStub() != null) {
            getStub().setStatus(status);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.status = status;
        }
    }

    public Long getEmailVerifiedTime()
    {
        if(getStub() != null) {
            return getStub().getEmailVerifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.emailVerifiedTime;
        }
    }
    public void setEmailVerifiedTime(Long emailVerifiedTime)
    {
        if(getStub() != null) {
            getStub().setEmailVerifiedTime(emailVerifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.emailVerifiedTime = emailVerifiedTime;
        }
    }

    public Long getOpenIdVerifiedTime()
    {
        if(getStub() != null) {
            return getStub().getOpenIdVerifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.openIdVerifiedTime;
        }
    }
    public void setOpenIdVerifiedTime(Long openIdVerifiedTime)
    {
        if(getStub() != null) {
            getStub().setOpenIdVerifiedTime(openIdVerifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.openIdVerifiedTime = openIdVerifiedTime;
        }
    }

    public Long getAuthenticatedTime()
    {
        if(getStub() != null) {
            return getStub().getAuthenticatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.authenticatedTime;
        }
    }
    public void setAuthenticatedTime(Long authenticatedTime)
    {
        if(getStub() != null) {
            getStub().setAuthenticatedTime(authenticatedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.authenticatedTime = authenticatedTime;
        }
    }

    public Long getCreatedTime()
    {
        if(getStub() != null) {
            return getStub().getCreatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.createdTime;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getStub() != null) {
            getStub().setCreatedTime(createdTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.createdTime = createdTime;
        }
    }

    public Long getModifiedTime()
    {
        if(getStub() != null) {
            return getStub().getModifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.modifiedTime;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getStub() != null) {
            getStub().setModifiedTime(modifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.modifiedTime = modifiedTime;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public UserStub getStub()
    {
        return this.stub;
    }
    protected void setStub(UserStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("guid = " + this.guid).append(";");
            sb.append("managerApp = " + this.managerApp).append(";");
            sb.append("appAcl = " + this.appAcl).append(";");
            sb.append("gaeApp = " + this.gaeApp).append(";");
            sb.append("aeryId = " + this.aeryId).append(";");
            sb.append("sessionId = " + this.sessionId).append(";");
            sb.append("ancestorGuid = " + this.ancestorGuid).append(";");
            sb.append("name = " + this.name).append(";");
            sb.append("usercode = " + this.usercode).append(";");
            sb.append("username = " + this.username).append(";");
            sb.append("nickname = " + this.nickname).append(";");
            sb.append("avatar = " + this.avatar).append(";");
            sb.append("email = " + this.email).append(";");
            sb.append("openId = " + this.openId).append(";");
            sb.append("gaeUser = " + this.gaeUser).append(";");
            sb.append("entityType = " + this.entityType).append(";");
            sb.append("surrogate = " + this.surrogate).append(";");
            sb.append("obsolete = " + this.obsolete).append(";");
            sb.append("timeZone = " + this.timeZone).append(";");
            sb.append("location = " + this.location).append(";");
            sb.append("streetAddress = " + this.streetAddress).append(";");
            sb.append("geoPoint = " + this.geoPoint).append(";");
            sb.append("ipAddress = " + this.ipAddress).append(";");
            sb.append("referer = " + this.referer).append(";");
            sb.append("status = " + this.status).append(";");
            sb.append("emailVerifiedTime = " + this.emailVerifiedTime).append(";");
            sb.append("openIdVerifiedTime = " + this.openIdVerifiedTime).append(";");
            sb.append("authenticatedTime = " + this.authenticatedTime).append(";");
            sb.append("createdTime = " + this.createdTime).append(";");
            sb.append("modifiedTime = " + this.modifiedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = guid == null ? 0 : guid.hashCode();
            _hash = 31 * _hash + delta;
            delta = managerApp == null ? 0 : managerApp.hashCode();
            _hash = 31 * _hash + delta;
            delta = appAcl == null ? 0 : appAcl.hashCode();
            _hash = 31 * _hash + delta;
            delta = gaeApp == null ? 0 : gaeApp.hashCode();
            _hash = 31 * _hash + delta;
            delta = aeryId == null ? 0 : aeryId.hashCode();
            _hash = 31 * _hash + delta;
            delta = sessionId == null ? 0 : sessionId.hashCode();
            _hash = 31 * _hash + delta;
            delta = ancestorGuid == null ? 0 : ancestorGuid.hashCode();
            _hash = 31 * _hash + delta;
            delta = name == null ? 0 : name.hashCode();
            _hash = 31 * _hash + delta;
            delta = usercode == null ? 0 : usercode.hashCode();
            _hash = 31 * _hash + delta;
            delta = username == null ? 0 : username.hashCode();
            _hash = 31 * _hash + delta;
            delta = nickname == null ? 0 : nickname.hashCode();
            _hash = 31 * _hash + delta;
            delta = avatar == null ? 0 : avatar.hashCode();
            _hash = 31 * _hash + delta;
            delta = email == null ? 0 : email.hashCode();
            _hash = 31 * _hash + delta;
            delta = openId == null ? 0 : openId.hashCode();
            _hash = 31 * _hash + delta;
            delta = gaeUser == null ? 0 : gaeUser.hashCode();
            _hash = 31 * _hash + delta;
            delta = entityType == null ? 0 : entityType.hashCode();
            _hash = 31 * _hash + delta;
            delta = surrogate == null ? 0 : surrogate.hashCode();
            _hash = 31 * _hash + delta;
            delta = obsolete == null ? 0 : obsolete.hashCode();
            _hash = 31 * _hash + delta;
            delta = timeZone == null ? 0 : timeZone.hashCode();
            _hash = 31 * _hash + delta;
            delta = location == null ? 0 : location.hashCode();
            _hash = 31 * _hash + delta;
            delta = streetAddress == null ? 0 : streetAddress.hashCode();
            _hash = 31 * _hash + delta;
            delta = geoPoint == null ? 0 : geoPoint.hashCode();
            _hash = 31 * _hash + delta;
            delta = ipAddress == null ? 0 : ipAddress.hashCode();
            _hash = 31 * _hash + delta;
            delta = referer == null ? 0 : referer.hashCode();
            _hash = 31 * _hash + delta;
            delta = status == null ? 0 : status.hashCode();
            _hash = 31 * _hash + delta;
            delta = emailVerifiedTime == null ? 0 : emailVerifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = openIdVerifiedTime == null ? 0 : openIdVerifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = authenticatedTime == null ? 0 : authenticatedTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = createdTime == null ? 0 : createdTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
