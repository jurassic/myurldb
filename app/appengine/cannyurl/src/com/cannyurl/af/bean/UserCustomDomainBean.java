package com.cannyurl.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.UserCustomDomain;
import com.myurldb.ws.stub.UserCustomDomainStub;


// Wrapper class + bean combo.
public class UserCustomDomainBean implements UserCustomDomain, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UserCustomDomainBean.class.getName());

    // [1] With an embedded object.
    private UserCustomDomainStub stub = null;

    // [2] Or, without an embedded object.
    private String guid;
    private String owner;
    private String domain;
    private Boolean verified;
    private String status;
    private Long verifiedTime;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public UserCustomDomainBean()
    {
        //this((String) null);
    }
    public UserCustomDomainBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null);
    }
    public UserCustomDomainBean(String guid, String owner, String domain, Boolean verified, String status, Long verifiedTime)
    {
        this(guid, owner, domain, verified, status, verifiedTime, null, null);
    }
    public UserCustomDomainBean(String guid, String owner, String domain, Boolean verified, String status, Long verifiedTime, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.owner = owner;
        this.domain = domain;
        this.verified = verified;
        this.status = status;
        this.verifiedTime = verifiedTime;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public UserCustomDomainBean(UserCustomDomain stub)
    {
        if(stub instanceof UserCustomDomainStub) {
            this.stub = (UserCustomDomainStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setOwner(stub.getOwner());   
            setDomain(stub.getDomain());   
            setVerified(stub.isVerified());   
            setStatus(stub.getStatus());   
            setVerifiedTime(stub.getVerifiedTime());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public String getOwner()
    {
        if(getStub() != null) {
            return getStub().getOwner();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.owner;
        }
    }
    public void setOwner(String owner)
    {
        if(getStub() != null) {
            getStub().setOwner(owner);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.owner = owner;
        }
    }

    public String getDomain()
    {
        if(getStub() != null) {
            return getStub().getDomain();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.domain;
        }
    }
    public void setDomain(String domain)
    {
        if(getStub() != null) {
            getStub().setDomain(domain);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.domain = domain;
        }
    }

    public Boolean isVerified()
    {
        if(getStub() != null) {
            return getStub().isVerified();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.verified;
        }
    }
    public void setVerified(Boolean verified)
    {
        if(getStub() != null) {
            getStub().setVerified(verified);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.verified = verified;
        }
    }

    public String getStatus()
    {
        if(getStub() != null) {
            return getStub().getStatus();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.status;
        }
    }
    public void setStatus(String status)
    {
        if(getStub() != null) {
            getStub().setStatus(status);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.status = status;
        }
    }

    public Long getVerifiedTime()
    {
        if(getStub() != null) {
            return getStub().getVerifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.verifiedTime;
        }
    }
    public void setVerifiedTime(Long verifiedTime)
    {
        if(getStub() != null) {
            getStub().setVerifiedTime(verifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.verifiedTime = verifiedTime;
        }
    }

    public Long getCreatedTime()
    {
        if(getStub() != null) {
            return getStub().getCreatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.createdTime;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getStub() != null) {
            getStub().setCreatedTime(createdTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.createdTime = createdTime;
        }
    }

    public Long getModifiedTime()
    {
        if(getStub() != null) {
            return getStub().getModifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.modifiedTime;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getStub() != null) {
            getStub().setModifiedTime(modifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.modifiedTime = modifiedTime;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public UserCustomDomainStub getStub()
    {
        return this.stub;
    }
    protected void setStub(UserCustomDomainStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("guid = " + this.guid).append(";");
            sb.append("owner = " + this.owner).append(";");
            sb.append("domain = " + this.domain).append(";");
            sb.append("verified = " + this.verified).append(";");
            sb.append("status = " + this.status).append(";");
            sb.append("verifiedTime = " + this.verifiedTime).append(";");
            sb.append("createdTime = " + this.createdTime).append(";");
            sb.append("modifiedTime = " + this.modifiedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = guid == null ? 0 : guid.hashCode();
            _hash = 31 * _hash + delta;
            delta = owner == null ? 0 : owner.hashCode();
            _hash = 31 * _hash + delta;
            delta = domain == null ? 0 : domain.hashCode();
            _hash = 31 * _hash + delta;
            delta = verified == null ? 0 : verified.hashCode();
            _hash = 31 * _hash + delta;
            delta = status == null ? 0 : status.hashCode();
            _hash = 31 * _hash + delta;
            delta = verifiedTime == null ? 0 : verifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = createdTime == null ? 0 : createdTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
