package com.cannyurl.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.HashedPasswordStruct;
import com.myurldb.ws.stub.HashedPasswordStructStub;
import com.myurldb.ws.UserPassword;
import com.myurldb.ws.stub.UserPasswordStub;


// Wrapper class + bean combo.
public class UserPasswordBean implements UserPassword, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UserPasswordBean.class.getName());

    // [1] With an embedded object.
    private UserPasswordStub stub = null;

    // [2] Or, without an embedded object.
    private String guid;
    private String admin;
    private String user;
    private String username;
    private String email;
    private String openId;
    private HashedPasswordStructBean password;
    private Boolean resetRequired;
    private String challengeQuestion;
    private String challengeAnswer;
    private String status;
    private Long lastResetTime;
    private Long expirationTime;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public UserPasswordBean()
    {
        //this((String) null);
    }
    public UserPasswordBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public UserPasswordBean(String guid, String admin, String user, String username, String email, String openId, HashedPasswordStructBean password, Boolean resetRequired, String challengeQuestion, String challengeAnswer, String status, Long lastResetTime, Long expirationTime)
    {
        this(guid, admin, user, username, email, openId, password, resetRequired, challengeQuestion, challengeAnswer, status, lastResetTime, expirationTime, null, null);
    }
    public UserPasswordBean(String guid, String admin, String user, String username, String email, String openId, HashedPasswordStructBean password, Boolean resetRequired, String challengeQuestion, String challengeAnswer, String status, Long lastResetTime, Long expirationTime, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.admin = admin;
        this.user = user;
        this.username = username;
        this.email = email;
        this.openId = openId;
        this.password = password;
        this.resetRequired = resetRequired;
        this.challengeQuestion = challengeQuestion;
        this.challengeAnswer = challengeAnswer;
        this.status = status;
        this.lastResetTime = lastResetTime;
        this.expirationTime = expirationTime;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public UserPasswordBean(UserPassword stub)
    {
        if(stub instanceof UserPasswordStub) {
            this.stub = (UserPasswordStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setAdmin(stub.getAdmin());   
            setUser(stub.getUser());   
            setUsername(stub.getUsername());   
            setEmail(stub.getEmail());   
            setOpenId(stub.getOpenId());   
            HashedPasswordStruct password = stub.getPassword();
            if(password instanceof HashedPasswordStructBean) {
                setPassword((HashedPasswordStructBean) password);   
            } else {
                setPassword(new HashedPasswordStructBean(password));   
            }
            setResetRequired(stub.isResetRequired());   
            setChallengeQuestion(stub.getChallengeQuestion());   
            setChallengeAnswer(stub.getChallengeAnswer());   
            setStatus(stub.getStatus());   
            setLastResetTime(stub.getLastResetTime());   
            setExpirationTime(stub.getExpirationTime());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public String getAdmin()
    {
        if(getStub() != null) {
            return getStub().getAdmin();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.admin;
        }
    }
    public void setAdmin(String admin)
    {
        if(getStub() != null) {
            getStub().setAdmin(admin);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.admin = admin;
        }
    }

    public String getUser()
    {
        if(getStub() != null) {
            return getStub().getUser();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.user;
        }
    }
    public void setUser(String user)
    {
        if(getStub() != null) {
            getStub().setUser(user);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.user = user;
        }
    }

    public String getUsername()
    {
        if(getStub() != null) {
            return getStub().getUsername();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.username;
        }
    }
    public void setUsername(String username)
    {
        if(getStub() != null) {
            getStub().setUsername(username);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.username = username;
        }
    }

    public String getEmail()
    {
        if(getStub() != null) {
            return getStub().getEmail();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.email;
        }
    }
    public void setEmail(String email)
    {
        if(getStub() != null) {
            getStub().setEmail(email);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.email = email;
        }
    }

    public String getOpenId()
    {
        if(getStub() != null) {
            return getStub().getOpenId();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.openId;
        }
    }
    public void setOpenId(String openId)
    {
        if(getStub() != null) {
            getStub().setOpenId(openId);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.openId = openId;
        }
    }

    public HashedPasswordStruct getPassword()
    {  
        if(getStub() != null) {
            // Note the object type.
            HashedPasswordStruct _stub_field = getStub().getPassword();
            if(_stub_field == null) {
                return null;
            } else {
                return new HashedPasswordStructBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.password;
        }
    }
    public void setPassword(HashedPasswordStruct password)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setPassword(password);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(password == null) {
                this.password = null;
            } else {
                if(password instanceof HashedPasswordStructBean) {
                    this.password = (HashedPasswordStructBean) password;
                } else {
                    this.password = new HashedPasswordStructBean(password);
                }
            }
        }
    }

    public Boolean isResetRequired()
    {
        if(getStub() != null) {
            return getStub().isResetRequired();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.resetRequired;
        }
    }
    public void setResetRequired(Boolean resetRequired)
    {
        if(getStub() != null) {
            getStub().setResetRequired(resetRequired);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.resetRequired = resetRequired;
        }
    }

    public String getChallengeQuestion()
    {
        if(getStub() != null) {
            return getStub().getChallengeQuestion();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.challengeQuestion;
        }
    }
    public void setChallengeQuestion(String challengeQuestion)
    {
        if(getStub() != null) {
            getStub().setChallengeQuestion(challengeQuestion);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.challengeQuestion = challengeQuestion;
        }
    }

    public String getChallengeAnswer()
    {
        if(getStub() != null) {
            return getStub().getChallengeAnswer();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.challengeAnswer;
        }
    }
    public void setChallengeAnswer(String challengeAnswer)
    {
        if(getStub() != null) {
            getStub().setChallengeAnswer(challengeAnswer);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.challengeAnswer = challengeAnswer;
        }
    }

    public String getStatus()
    {
        if(getStub() != null) {
            return getStub().getStatus();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.status;
        }
    }
    public void setStatus(String status)
    {
        if(getStub() != null) {
            getStub().setStatus(status);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.status = status;
        }
    }

    public Long getLastResetTime()
    {
        if(getStub() != null) {
            return getStub().getLastResetTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.lastResetTime;
        }
    }
    public void setLastResetTime(Long lastResetTime)
    {
        if(getStub() != null) {
            getStub().setLastResetTime(lastResetTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.lastResetTime = lastResetTime;
        }
    }

    public Long getExpirationTime()
    {
        if(getStub() != null) {
            return getStub().getExpirationTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.expirationTime;
        }
    }
    public void setExpirationTime(Long expirationTime)
    {
        if(getStub() != null) {
            getStub().setExpirationTime(expirationTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.expirationTime = expirationTime;
        }
    }

    public Long getCreatedTime()
    {
        if(getStub() != null) {
            return getStub().getCreatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.createdTime;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getStub() != null) {
            getStub().setCreatedTime(createdTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.createdTime = createdTime;
        }
    }

    public Long getModifiedTime()
    {
        if(getStub() != null) {
            return getStub().getModifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.modifiedTime;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getStub() != null) {
            getStub().setModifiedTime(modifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.modifiedTime = modifiedTime;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public UserPasswordStub getStub()
    {
        return this.stub;
    }
    protected void setStub(UserPasswordStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("guid = " + this.guid).append(";");
            sb.append("admin = " + this.admin).append(";");
            sb.append("user = " + this.user).append(";");
            sb.append("username = " + this.username).append(";");
            sb.append("email = " + this.email).append(";");
            sb.append("openId = " + this.openId).append(";");
            sb.append("password = " + this.password).append(";");
            sb.append("resetRequired = " + this.resetRequired).append(";");
            sb.append("challengeQuestion = " + this.challengeQuestion).append(";");
            sb.append("challengeAnswer = " + this.challengeAnswer).append(";");
            sb.append("status = " + this.status).append(";");
            sb.append("lastResetTime = " + this.lastResetTime).append(";");
            sb.append("expirationTime = " + this.expirationTime).append(";");
            sb.append("createdTime = " + this.createdTime).append(";");
            sb.append("modifiedTime = " + this.modifiedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = guid == null ? 0 : guid.hashCode();
            _hash = 31 * _hash + delta;
            delta = admin == null ? 0 : admin.hashCode();
            _hash = 31 * _hash + delta;
            delta = user == null ? 0 : user.hashCode();
            _hash = 31 * _hash + delta;
            delta = username == null ? 0 : username.hashCode();
            _hash = 31 * _hash + delta;
            delta = email == null ? 0 : email.hashCode();
            _hash = 31 * _hash + delta;
            delta = openId == null ? 0 : openId.hashCode();
            _hash = 31 * _hash + delta;
            delta = password == null ? 0 : password.hashCode();
            _hash = 31 * _hash + delta;
            delta = resetRequired == null ? 0 : resetRequired.hashCode();
            _hash = 31 * _hash + delta;
            delta = challengeQuestion == null ? 0 : challengeQuestion.hashCode();
            _hash = 31 * _hash + delta;
            delta = challengeAnswer == null ? 0 : challengeAnswer.hashCode();
            _hash = 31 * _hash + delta;
            delta = status == null ? 0 : status.hashCode();
            _hash = 31 * _hash + delta;
            delta = lastResetTime == null ? 0 : lastResetTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = expirationTime == null ? 0 : expirationTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = createdTime == null ? 0 : createdTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
