package com.cannyurl.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.UserRating;
import com.myurldb.ws.stub.UserRatingStub;


// Wrapper class + bean combo.
public class UserRatingBean implements UserRating, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UserRatingBean.class.getName());

    // [1] With an embedded object.
    private UserRatingStub stub = null;

    // [2] Or, without an embedded object.
    private String guid;
    private String user;
    private Double rating;
    private String note;
    private Long ratedTime;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public UserRatingBean()
    {
        //this((String) null);
    }
    public UserRatingBean(String guid)
    {
        this(guid, null, null, null, null, null, null);
    }
    public UserRatingBean(String guid, String user, Double rating, String note, Long ratedTime)
    {
        this(guid, user, rating, note, ratedTime, null, null);
    }
    public UserRatingBean(String guid, String user, Double rating, String note, Long ratedTime, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.user = user;
        this.rating = rating;
        this.note = note;
        this.ratedTime = ratedTime;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public UserRatingBean(UserRating stub)
    {
        if(stub instanceof UserRatingStub) {
            this.stub = (UserRatingStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setUser(stub.getUser());   
            setRating(stub.getRating());   
            setNote(stub.getNote());   
            setRatedTime(stub.getRatedTime());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public String getUser()
    {
        if(getStub() != null) {
            return getStub().getUser();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.user;
        }
    }
    public void setUser(String user)
    {
        if(getStub() != null) {
            getStub().setUser(user);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.user = user;
        }
    }

    public Double getRating()
    {
        if(getStub() != null) {
            return getStub().getRating();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.rating;
        }
    }
    public void setRating(Double rating)
    {
        if(getStub() != null) {
            getStub().setRating(rating);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.rating = rating;
        }
    }

    public String getNote()
    {
        if(getStub() != null) {
            return getStub().getNote();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.note;
        }
    }
    public void setNote(String note)
    {
        if(getStub() != null) {
            getStub().setNote(note);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.note = note;
        }
    }

    public Long getRatedTime()
    {
        if(getStub() != null) {
            return getStub().getRatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.ratedTime;
        }
    }
    public void setRatedTime(Long ratedTime)
    {
        if(getStub() != null) {
            getStub().setRatedTime(ratedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.ratedTime = ratedTime;
        }
    }

    public Long getCreatedTime()
    {
        if(getStub() != null) {
            return getStub().getCreatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.createdTime;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getStub() != null) {
            getStub().setCreatedTime(createdTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.createdTime = createdTime;
        }
    }

    public Long getModifiedTime()
    {
        if(getStub() != null) {
            return getStub().getModifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.modifiedTime;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getStub() != null) {
            getStub().setModifiedTime(modifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.modifiedTime = modifiedTime;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public UserRatingStub getStub()
    {
        return this.stub;
    }
    protected void setStub(UserRatingStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("guid = " + this.guid).append(";");
            sb.append("user = " + this.user).append(";");
            sb.append("rating = " + this.rating).append(";");
            sb.append("note = " + this.note).append(";");
            sb.append("ratedTime = " + this.ratedTime).append(";");
            sb.append("createdTime = " + this.createdTime).append(";");
            sb.append("modifiedTime = " + this.modifiedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = guid == null ? 0 : guid.hashCode();
            _hash = 31 * _hash + delta;
            delta = user == null ? 0 : user.hashCode();
            _hash = 31 * _hash + delta;
            delta = rating == null ? 0 : rating.hashCode();
            _hash = 31 * _hash + delta;
            delta = note == null ? 0 : note.hashCode();
            _hash = 31 * _hash + delta;
            delta = ratedTime == null ? 0 : ratedTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = createdTime == null ? 0 : createdTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
