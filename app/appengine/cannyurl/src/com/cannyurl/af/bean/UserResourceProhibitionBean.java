package com.cannyurl.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.UserResourceProhibition;
import com.myurldb.ws.stub.UserResourceProhibitionStub;


// Wrapper class + bean combo.
public class UserResourceProhibitionBean implements UserResourceProhibition, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UserResourceProhibitionBean.class.getName());

    // [1] With an embedded object.
    private UserResourceProhibitionStub stub = null;

    // [2] Or, without an embedded object.
    private String guid;
    private String user;
    private String permissionName;
    private String resource;
    private String instance;
    private String action;
    private Boolean prohibited;
    private String status;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public UserResourceProhibitionBean()
    {
        //this((String) null);
    }
    public UserResourceProhibitionBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null);
    }
    public UserResourceProhibitionBean(String guid, String user, String permissionName, String resource, String instance, String action, Boolean prohibited, String status)
    {
        this(guid, user, permissionName, resource, instance, action, prohibited, status, null, null);
    }
    public UserResourceProhibitionBean(String guid, String user, String permissionName, String resource, String instance, String action, Boolean prohibited, String status, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.user = user;
        this.permissionName = permissionName;
        this.resource = resource;
        this.instance = instance;
        this.action = action;
        this.prohibited = prohibited;
        this.status = status;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public UserResourceProhibitionBean(UserResourceProhibition stub)
    {
        if(stub instanceof UserResourceProhibitionStub) {
            this.stub = (UserResourceProhibitionStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setUser(stub.getUser());   
            setPermissionName(stub.getPermissionName());   
            setResource(stub.getResource());   
            setInstance(stub.getInstance());   
            setAction(stub.getAction());   
            setProhibited(stub.isProhibited());   
            setStatus(stub.getStatus());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public String getUser()
    {
        if(getStub() != null) {
            return getStub().getUser();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.user;
        }
    }
    public void setUser(String user)
    {
        if(getStub() != null) {
            getStub().setUser(user);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.user = user;
        }
    }

    public String getPermissionName()
    {
        if(getStub() != null) {
            return getStub().getPermissionName();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.permissionName;
        }
    }
    public void setPermissionName(String permissionName)
    {
        if(getStub() != null) {
            getStub().setPermissionName(permissionName);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.permissionName = permissionName;
        }
    }

    public String getResource()
    {
        if(getStub() != null) {
            return getStub().getResource();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.resource;
        }
    }
    public void setResource(String resource)
    {
        if(getStub() != null) {
            getStub().setResource(resource);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.resource = resource;
        }
    }

    public String getInstance()
    {
        if(getStub() != null) {
            return getStub().getInstance();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.instance;
        }
    }
    public void setInstance(String instance)
    {
        if(getStub() != null) {
            getStub().setInstance(instance);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.instance = instance;
        }
    }

    public String getAction()
    {
        if(getStub() != null) {
            return getStub().getAction();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.action;
        }
    }
    public void setAction(String action)
    {
        if(getStub() != null) {
            getStub().setAction(action);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.action = action;
        }
    }

    public Boolean isProhibited()
    {
        if(getStub() != null) {
            return getStub().isProhibited();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.prohibited;
        }
    }
    public void setProhibited(Boolean prohibited)
    {
        if(getStub() != null) {
            getStub().setProhibited(prohibited);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.prohibited = prohibited;
        }
    }

    public String getStatus()
    {
        if(getStub() != null) {
            return getStub().getStatus();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.status;
        }
    }
    public void setStatus(String status)
    {
        if(getStub() != null) {
            getStub().setStatus(status);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.status = status;
        }
    }

    public Long getCreatedTime()
    {
        if(getStub() != null) {
            return getStub().getCreatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.createdTime;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getStub() != null) {
            getStub().setCreatedTime(createdTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.createdTime = createdTime;
        }
    }

    public Long getModifiedTime()
    {
        if(getStub() != null) {
            return getStub().getModifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.modifiedTime;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getStub() != null) {
            getStub().setModifiedTime(modifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.modifiedTime = modifiedTime;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public UserResourceProhibitionStub getStub()
    {
        return this.stub;
    }
    protected void setStub(UserResourceProhibitionStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("guid = " + this.guid).append(";");
            sb.append("user = " + this.user).append(";");
            sb.append("permissionName = " + this.permissionName).append(";");
            sb.append("resource = " + this.resource).append(";");
            sb.append("instance = " + this.instance).append(";");
            sb.append("action = " + this.action).append(";");
            sb.append("prohibited = " + this.prohibited).append(";");
            sb.append("status = " + this.status).append(";");
            sb.append("createdTime = " + this.createdTime).append(";");
            sb.append("modifiedTime = " + this.modifiedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = guid == null ? 0 : guid.hashCode();
            _hash = 31 * _hash + delta;
            delta = user == null ? 0 : user.hashCode();
            _hash = 31 * _hash + delta;
            delta = permissionName == null ? 0 : permissionName.hashCode();
            _hash = 31 * _hash + delta;
            delta = resource == null ? 0 : resource.hashCode();
            _hash = 31 * _hash + delta;
            delta = instance == null ? 0 : instance.hashCode();
            _hash = 31 * _hash + delta;
            delta = action == null ? 0 : action.hashCode();
            _hash = 31 * _hash + delta;
            delta = prohibited == null ? 0 : prohibited.hashCode();
            _hash = 31 * _hash + delta;
            delta = status == null ? 0 : status.hashCode();
            _hash = 31 * _hash + delta;
            delta = createdTime == null ? 0 : createdTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
