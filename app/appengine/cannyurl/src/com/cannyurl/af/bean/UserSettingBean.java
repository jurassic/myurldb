package com.cannyurl.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.UserSetting;
import com.myurldb.ws.stub.PersonalSettingStub;
import com.myurldb.ws.stub.UserSettingStub;


// Wrapper class + bean combo.
public class UserSettingBean extends PersonalSettingBean implements UserSetting, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UserSettingBean.class.getName());


    // [2] Or, without an embedded object.
    private String user;
    private String homePage;
    private String selfBio;
    private String userUrlDomain;
    private String userUrlDomainNormalized;
    private Boolean autoRedirectEnabled;
    private Boolean viewEnabled;
    private Boolean shareEnabled;
    private String defaultDomain;
    private String defaultTokenType;
    private String defaultRedirectType;
    private Long defaultFlashDuration;
    private String defaultAccessType;
    private String defaultViewType;
    private String defaultShareType;
    private String defaultPassphrase;
    private String defaultSignature;
    private Boolean useSignature;
    private String defaultEmblem;
    private Boolean useEmblem;
    private String defaultBackground;
    private Boolean useBackground;
    private String sponsorUrl;
    private String sponsorBanner;
    private String sponsorNote;
    private Boolean useSponsor;
    private String extraParams;
    private Long expirationDuration;

    // Ctors.
    public UserSettingBean()
    {
        //this((String) null);
    }
    public UserSettingBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public UserSettingBean(String guid, String user, String homePage, String selfBio, String userUrlDomain, String userUrlDomainNormalized, Boolean autoRedirectEnabled, Boolean viewEnabled, Boolean shareEnabled, String defaultDomain, String defaultTokenType, String defaultRedirectType, Long defaultFlashDuration, String defaultAccessType, String defaultViewType, String defaultShareType, String defaultPassphrase, String defaultSignature, Boolean useSignature, String defaultEmblem, Boolean useEmblem, String defaultBackground, Boolean useBackground, String sponsorUrl, String sponsorBanner, String sponsorNote, Boolean useSponsor, String extraParams, Long expirationDuration)
    {
        this(guid, user, homePage, selfBio, userUrlDomain, userUrlDomainNormalized, autoRedirectEnabled, viewEnabled, shareEnabled, defaultDomain, defaultTokenType, defaultRedirectType, defaultFlashDuration, defaultAccessType, defaultViewType, defaultShareType, defaultPassphrase, defaultSignature, useSignature, defaultEmblem, useEmblem, defaultBackground, useBackground, sponsorUrl, sponsorBanner, sponsorNote, useSponsor, extraParams, expirationDuration, null, null);
    }
    public UserSettingBean(String guid, String user, String homePage, String selfBio, String userUrlDomain, String userUrlDomainNormalized, Boolean autoRedirectEnabled, Boolean viewEnabled, Boolean shareEnabled, String defaultDomain, String defaultTokenType, String defaultRedirectType, Long defaultFlashDuration, String defaultAccessType, String defaultViewType, String defaultShareType, String defaultPassphrase, String defaultSignature, Boolean useSignature, String defaultEmblem, Boolean useEmblem, String defaultBackground, Boolean useBackground, String sponsorUrl, String sponsorBanner, String sponsorNote, Boolean useSponsor, String extraParams, Long expirationDuration, Long createdTime, Long modifiedTime)
    {
        super(guid, createdTime, modifiedTime);

        this.user = user;
        this.homePage = homePage;
        this.selfBio = selfBio;
        this.userUrlDomain = userUrlDomain;
        this.userUrlDomainNormalized = userUrlDomainNormalized;
        this.autoRedirectEnabled = autoRedirectEnabled;
        this.viewEnabled = viewEnabled;
        this.shareEnabled = shareEnabled;
        this.defaultDomain = defaultDomain;
        this.defaultTokenType = defaultTokenType;
        this.defaultRedirectType = defaultRedirectType;
        this.defaultFlashDuration = defaultFlashDuration;
        this.defaultAccessType = defaultAccessType;
        this.defaultViewType = defaultViewType;
        this.defaultShareType = defaultShareType;
        this.defaultPassphrase = defaultPassphrase;
        this.defaultSignature = defaultSignature;
        this.useSignature = useSignature;
        this.defaultEmblem = defaultEmblem;
        this.useEmblem = useEmblem;
        this.defaultBackground = defaultBackground;
        this.useBackground = useBackground;
        this.sponsorUrl = sponsorUrl;
        this.sponsorBanner = sponsorBanner;
        this.sponsorNote = sponsorNote;
        this.useSponsor = useSponsor;
        this.extraParams = extraParams;
        this.expirationDuration = expirationDuration;
    }
    public UserSettingBean(UserSetting stub)
    {
        if(stub instanceof UserSettingStub) {
            super.setStub((PersonalSettingStub) stub);
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setUser(stub.getUser());   
            setHomePage(stub.getHomePage());   
            setSelfBio(stub.getSelfBio());   
            setUserUrlDomain(stub.getUserUrlDomain());   
            setUserUrlDomainNormalized(stub.getUserUrlDomainNormalized());   
            setAutoRedirectEnabled(stub.isAutoRedirectEnabled());   
            setViewEnabled(stub.isViewEnabled());   
            setShareEnabled(stub.isShareEnabled());   
            setDefaultDomain(stub.getDefaultDomain());   
            setDefaultTokenType(stub.getDefaultTokenType());   
            setDefaultRedirectType(stub.getDefaultRedirectType());   
            setDefaultFlashDuration(stub.getDefaultFlashDuration());   
            setDefaultAccessType(stub.getDefaultAccessType());   
            setDefaultViewType(stub.getDefaultViewType());   
            setDefaultShareType(stub.getDefaultShareType());   
            setDefaultPassphrase(stub.getDefaultPassphrase());   
            setDefaultSignature(stub.getDefaultSignature());   
            setUseSignature(stub.isUseSignature());   
            setDefaultEmblem(stub.getDefaultEmblem());   
            setUseEmblem(stub.isUseEmblem());   
            setDefaultBackground(stub.getDefaultBackground());   
            setUseBackground(stub.isUseBackground());   
            setSponsorUrl(stub.getSponsorUrl());   
            setSponsorBanner(stub.getSponsorBanner());   
            setSponsorNote(stub.getSponsorNote());   
            setUseSponsor(stub.isUseSponsor());   
            setExtraParams(stub.getExtraParams());   
            setExpirationDuration(stub.getExpirationDuration());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    public String getUser()
    {
        if(getStub() != null) {
            return getStub().getUser();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.user;
        }
    }
    public void setUser(String user)
    {
        if(getStub() != null) {
            getStub().setUser(user);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.user = user;
        }
    }

    public String getHomePage()
    {
        if(getStub() != null) {
            return getStub().getHomePage();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.homePage;
        }
    }
    public void setHomePage(String homePage)
    {
        if(getStub() != null) {
            getStub().setHomePage(homePage);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.homePage = homePage;
        }
    }

    public String getSelfBio()
    {
        if(getStub() != null) {
            return getStub().getSelfBio();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.selfBio;
        }
    }
    public void setSelfBio(String selfBio)
    {
        if(getStub() != null) {
            getStub().setSelfBio(selfBio);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.selfBio = selfBio;
        }
    }

    public String getUserUrlDomain()
    {
        if(getStub() != null) {
            return getStub().getUserUrlDomain();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.userUrlDomain;
        }
    }
    public void setUserUrlDomain(String userUrlDomain)
    {
        if(getStub() != null) {
            getStub().setUserUrlDomain(userUrlDomain);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.userUrlDomain = userUrlDomain;
        }
    }

    public String getUserUrlDomainNormalized()
    {
        if(getStub() != null) {
            return getStub().getUserUrlDomainNormalized();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.userUrlDomainNormalized;
        }
    }
    public void setUserUrlDomainNormalized(String userUrlDomainNormalized)
    {
        if(getStub() != null) {
            getStub().setUserUrlDomainNormalized(userUrlDomainNormalized);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.userUrlDomainNormalized = userUrlDomainNormalized;
        }
    }

    public Boolean isAutoRedirectEnabled()
    {
        if(getStub() != null) {
            return getStub().isAutoRedirectEnabled();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.autoRedirectEnabled;
        }
    }
    public void setAutoRedirectEnabled(Boolean autoRedirectEnabled)
    {
        if(getStub() != null) {
            getStub().setAutoRedirectEnabled(autoRedirectEnabled);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.autoRedirectEnabled = autoRedirectEnabled;
        }
    }

    public Boolean isViewEnabled()
    {
        if(getStub() != null) {
            return getStub().isViewEnabled();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.viewEnabled;
        }
    }
    public void setViewEnabled(Boolean viewEnabled)
    {
        if(getStub() != null) {
            getStub().setViewEnabled(viewEnabled);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.viewEnabled = viewEnabled;
        }
    }

    public Boolean isShareEnabled()
    {
        if(getStub() != null) {
            return getStub().isShareEnabled();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.shareEnabled;
        }
    }
    public void setShareEnabled(Boolean shareEnabled)
    {
        if(getStub() != null) {
            getStub().setShareEnabled(shareEnabled);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.shareEnabled = shareEnabled;
        }
    }

    public String getDefaultDomain()
    {
        if(getStub() != null) {
            return getStub().getDefaultDomain();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.defaultDomain;
        }
    }
    public void setDefaultDomain(String defaultDomain)
    {
        if(getStub() != null) {
            getStub().setDefaultDomain(defaultDomain);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.defaultDomain = defaultDomain;
        }
    }

    public String getDefaultTokenType()
    {
        if(getStub() != null) {
            return getStub().getDefaultTokenType();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.defaultTokenType;
        }
    }
    public void setDefaultTokenType(String defaultTokenType)
    {
        if(getStub() != null) {
            getStub().setDefaultTokenType(defaultTokenType);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.defaultTokenType = defaultTokenType;
        }
    }

    public String getDefaultRedirectType()
    {
        if(getStub() != null) {
            return getStub().getDefaultRedirectType();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.defaultRedirectType;
        }
    }
    public void setDefaultRedirectType(String defaultRedirectType)
    {
        if(getStub() != null) {
            getStub().setDefaultRedirectType(defaultRedirectType);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.defaultRedirectType = defaultRedirectType;
        }
    }

    public Long getDefaultFlashDuration()
    {
        if(getStub() != null) {
            return getStub().getDefaultFlashDuration();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.defaultFlashDuration;
        }
    }
    public void setDefaultFlashDuration(Long defaultFlashDuration)
    {
        if(getStub() != null) {
            getStub().setDefaultFlashDuration(defaultFlashDuration);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.defaultFlashDuration = defaultFlashDuration;
        }
    }

    public String getDefaultAccessType()
    {
        if(getStub() != null) {
            return getStub().getDefaultAccessType();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.defaultAccessType;
        }
    }
    public void setDefaultAccessType(String defaultAccessType)
    {
        if(getStub() != null) {
            getStub().setDefaultAccessType(defaultAccessType);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.defaultAccessType = defaultAccessType;
        }
    }

    public String getDefaultViewType()
    {
        if(getStub() != null) {
            return getStub().getDefaultViewType();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.defaultViewType;
        }
    }
    public void setDefaultViewType(String defaultViewType)
    {
        if(getStub() != null) {
            getStub().setDefaultViewType(defaultViewType);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.defaultViewType = defaultViewType;
        }
    }

    public String getDefaultShareType()
    {
        if(getStub() != null) {
            return getStub().getDefaultShareType();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.defaultShareType;
        }
    }
    public void setDefaultShareType(String defaultShareType)
    {
        if(getStub() != null) {
            getStub().setDefaultShareType(defaultShareType);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.defaultShareType = defaultShareType;
        }
    }

    public String getDefaultPassphrase()
    {
        if(getStub() != null) {
            return getStub().getDefaultPassphrase();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.defaultPassphrase;
        }
    }
    public void setDefaultPassphrase(String defaultPassphrase)
    {
        if(getStub() != null) {
            getStub().setDefaultPassphrase(defaultPassphrase);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.defaultPassphrase = defaultPassphrase;
        }
    }

    public String getDefaultSignature()
    {
        if(getStub() != null) {
            return getStub().getDefaultSignature();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.defaultSignature;
        }
    }
    public void setDefaultSignature(String defaultSignature)
    {
        if(getStub() != null) {
            getStub().setDefaultSignature(defaultSignature);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.defaultSignature = defaultSignature;
        }
    }

    public Boolean isUseSignature()
    {
        if(getStub() != null) {
            return getStub().isUseSignature();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.useSignature;
        }
    }
    public void setUseSignature(Boolean useSignature)
    {
        if(getStub() != null) {
            getStub().setUseSignature(useSignature);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.useSignature = useSignature;
        }
    }

    public String getDefaultEmblem()
    {
        if(getStub() != null) {
            return getStub().getDefaultEmblem();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.defaultEmblem;
        }
    }
    public void setDefaultEmblem(String defaultEmblem)
    {
        if(getStub() != null) {
            getStub().setDefaultEmblem(defaultEmblem);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.defaultEmblem = defaultEmblem;
        }
    }

    public Boolean isUseEmblem()
    {
        if(getStub() != null) {
            return getStub().isUseEmblem();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.useEmblem;
        }
    }
    public void setUseEmblem(Boolean useEmblem)
    {
        if(getStub() != null) {
            getStub().setUseEmblem(useEmblem);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.useEmblem = useEmblem;
        }
    }

    public String getDefaultBackground()
    {
        if(getStub() != null) {
            return getStub().getDefaultBackground();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.defaultBackground;
        }
    }
    public void setDefaultBackground(String defaultBackground)
    {
        if(getStub() != null) {
            getStub().setDefaultBackground(defaultBackground);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.defaultBackground = defaultBackground;
        }
    }

    public Boolean isUseBackground()
    {
        if(getStub() != null) {
            return getStub().isUseBackground();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.useBackground;
        }
    }
    public void setUseBackground(Boolean useBackground)
    {
        if(getStub() != null) {
            getStub().setUseBackground(useBackground);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.useBackground = useBackground;
        }
    }

    public String getSponsorUrl()
    {
        if(getStub() != null) {
            return getStub().getSponsorUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.sponsorUrl;
        }
    }
    public void setSponsorUrl(String sponsorUrl)
    {
        if(getStub() != null) {
            getStub().setSponsorUrl(sponsorUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.sponsorUrl = sponsorUrl;
        }
    }

    public String getSponsorBanner()
    {
        if(getStub() != null) {
            return getStub().getSponsorBanner();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.sponsorBanner;
        }
    }
    public void setSponsorBanner(String sponsorBanner)
    {
        if(getStub() != null) {
            getStub().setSponsorBanner(sponsorBanner);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.sponsorBanner = sponsorBanner;
        }
    }

    public String getSponsorNote()
    {
        if(getStub() != null) {
            return getStub().getSponsorNote();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.sponsorNote;
        }
    }
    public void setSponsorNote(String sponsorNote)
    {
        if(getStub() != null) {
            getStub().setSponsorNote(sponsorNote);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.sponsorNote = sponsorNote;
        }
    }

    public Boolean isUseSponsor()
    {
        if(getStub() != null) {
            return getStub().isUseSponsor();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.useSponsor;
        }
    }
    public void setUseSponsor(Boolean useSponsor)
    {
        if(getStub() != null) {
            getStub().setUseSponsor(useSponsor);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.useSponsor = useSponsor;
        }
    }

    public String getExtraParams()
    {
        if(getStub() != null) {
            return getStub().getExtraParams();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.extraParams;
        }
    }
    public void setExtraParams(String extraParams)
    {
        if(getStub() != null) {
            getStub().setExtraParams(extraParams);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.extraParams = extraParams;
        }
    }

    public Long getExpirationDuration()
    {
        if(getStub() != null) {
            return getStub().getExpirationDuration();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.expirationDuration;
        }
    }
    public void setExpirationDuration(Long expirationDuration)
    {
        if(getStub() != null) {
            getStub().setExpirationDuration(expirationDuration);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.expirationDuration = expirationDuration;
        }
    }

    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    // Returns the reference to the embedded object. (Could be null.)
    public UserSettingStub getStub()
    {
        return (UserSettingStub) super.getStub();
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder( super.toString() );
            sb.append("user = " + this.user).append(";");
            sb.append("homePage = " + this.homePage).append(";");
            sb.append("selfBio = " + this.selfBio).append(";");
            sb.append("userUrlDomain = " + this.userUrlDomain).append(";");
            sb.append("userUrlDomainNormalized = " + this.userUrlDomainNormalized).append(";");
            sb.append("autoRedirectEnabled = " + this.autoRedirectEnabled).append(";");
            sb.append("viewEnabled = " + this.viewEnabled).append(";");
            sb.append("shareEnabled = " + this.shareEnabled).append(";");
            sb.append("defaultDomain = " + this.defaultDomain).append(";");
            sb.append("defaultTokenType = " + this.defaultTokenType).append(";");
            sb.append("defaultRedirectType = " + this.defaultRedirectType).append(";");
            sb.append("defaultFlashDuration = " + this.defaultFlashDuration).append(";");
            sb.append("defaultAccessType = " + this.defaultAccessType).append(";");
            sb.append("defaultViewType = " + this.defaultViewType).append(";");
            sb.append("defaultShareType = " + this.defaultShareType).append(";");
            sb.append("defaultPassphrase = " + this.defaultPassphrase).append(";");
            sb.append("defaultSignature = " + this.defaultSignature).append(";");
            sb.append("useSignature = " + this.useSignature).append(";");
            sb.append("defaultEmblem = " + this.defaultEmblem).append(";");
            sb.append("useEmblem = " + this.useEmblem).append(";");
            sb.append("defaultBackground = " + this.defaultBackground).append(";");
            sb.append("useBackground = " + this.useBackground).append(";");
            sb.append("sponsorUrl = " + this.sponsorUrl).append(";");
            sb.append("sponsorBanner = " + this.sponsorBanner).append(";");
            sb.append("sponsorNote = " + this.sponsorNote).append(";");
            sb.append("useSponsor = " + this.useSponsor).append(";");
            sb.append("extraParams = " + this.extraParams).append(";");
            sb.append("expirationDuration = " + this.expirationDuration).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = user == null ? 0 : user.hashCode();
            _hash = 31 * _hash + delta;
            delta = homePage == null ? 0 : homePage.hashCode();
            _hash = 31 * _hash + delta;
            delta = selfBio == null ? 0 : selfBio.hashCode();
            _hash = 31 * _hash + delta;
            delta = userUrlDomain == null ? 0 : userUrlDomain.hashCode();
            _hash = 31 * _hash + delta;
            delta = userUrlDomainNormalized == null ? 0 : userUrlDomainNormalized.hashCode();
            _hash = 31 * _hash + delta;
            delta = autoRedirectEnabled == null ? 0 : autoRedirectEnabled.hashCode();
            _hash = 31 * _hash + delta;
            delta = viewEnabled == null ? 0 : viewEnabled.hashCode();
            _hash = 31 * _hash + delta;
            delta = shareEnabled == null ? 0 : shareEnabled.hashCode();
            _hash = 31 * _hash + delta;
            delta = defaultDomain == null ? 0 : defaultDomain.hashCode();
            _hash = 31 * _hash + delta;
            delta = defaultTokenType == null ? 0 : defaultTokenType.hashCode();
            _hash = 31 * _hash + delta;
            delta = defaultRedirectType == null ? 0 : defaultRedirectType.hashCode();
            _hash = 31 * _hash + delta;
            delta = defaultFlashDuration == null ? 0 : defaultFlashDuration.hashCode();
            _hash = 31 * _hash + delta;
            delta = defaultAccessType == null ? 0 : defaultAccessType.hashCode();
            _hash = 31 * _hash + delta;
            delta = defaultViewType == null ? 0 : defaultViewType.hashCode();
            _hash = 31 * _hash + delta;
            delta = defaultShareType == null ? 0 : defaultShareType.hashCode();
            _hash = 31 * _hash + delta;
            delta = defaultPassphrase == null ? 0 : defaultPassphrase.hashCode();
            _hash = 31 * _hash + delta;
            delta = defaultSignature == null ? 0 : defaultSignature.hashCode();
            _hash = 31 * _hash + delta;
            delta = useSignature == null ? 0 : useSignature.hashCode();
            _hash = 31 * _hash + delta;
            delta = defaultEmblem == null ? 0 : defaultEmblem.hashCode();
            _hash = 31 * _hash + delta;
            delta = useEmblem == null ? 0 : useEmblem.hashCode();
            _hash = 31 * _hash + delta;
            delta = defaultBackground == null ? 0 : defaultBackground.hashCode();
            _hash = 31 * _hash + delta;
            delta = useBackground == null ? 0 : useBackground.hashCode();
            _hash = 31 * _hash + delta;
            delta = sponsorUrl == null ? 0 : sponsorUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = sponsorBanner == null ? 0 : sponsorBanner.hashCode();
            _hash = 31 * _hash + delta;
            delta = sponsorNote == null ? 0 : sponsorNote.hashCode();
            _hash = 31 * _hash + delta;
            delta = useSponsor == null ? 0 : useSponsor.hashCode();
            _hash = 31 * _hash + delta;
            delta = extraParams == null ? 0 : extraParams.hashCode();
            _hash = 31 * _hash + delta;
            delta = expirationDuration == null ? 0 : expirationDuration.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
