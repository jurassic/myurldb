package com.cannyurl.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.UserUsercode;
import com.myurldb.ws.stub.UserUsercodeStub;


// Wrapper class + bean combo.
public class UserUsercodeBean implements UserUsercode, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UserUsercodeBean.class.getName());

    // [1] With an embedded object.
    private UserUsercodeStub stub = null;

    // [2] Or, without an embedded object.
    private String guid;
    private String admin;
    private String user;
    private String username;
    private String usercode;
    private String status;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public UserUsercodeBean()
    {
        //this((String) null);
    }
    public UserUsercodeBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null);
    }
    public UserUsercodeBean(String guid, String admin, String user, String username, String usercode, String status)
    {
        this(guid, admin, user, username, usercode, status, null, null);
    }
    public UserUsercodeBean(String guid, String admin, String user, String username, String usercode, String status, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.admin = admin;
        this.user = user;
        this.username = username;
        this.usercode = usercode;
        this.status = status;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public UserUsercodeBean(UserUsercode stub)
    {
        if(stub instanceof UserUsercodeStub) {
            this.stub = (UserUsercodeStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setAdmin(stub.getAdmin());   
            setUser(stub.getUser());   
            setUsername(stub.getUsername());   
            setUsercode(stub.getUsercode());   
            setStatus(stub.getStatus());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public String getAdmin()
    {
        if(getStub() != null) {
            return getStub().getAdmin();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.admin;
        }
    }
    public void setAdmin(String admin)
    {
        if(getStub() != null) {
            getStub().setAdmin(admin);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.admin = admin;
        }
    }

    public String getUser()
    {
        if(getStub() != null) {
            return getStub().getUser();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.user;
        }
    }
    public void setUser(String user)
    {
        if(getStub() != null) {
            getStub().setUser(user);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.user = user;
        }
    }

    public String getUsername()
    {
        if(getStub() != null) {
            return getStub().getUsername();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.username;
        }
    }
    public void setUsername(String username)
    {
        if(getStub() != null) {
            getStub().setUsername(username);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.username = username;
        }
    }

    public String getUsercode()
    {
        if(getStub() != null) {
            return getStub().getUsercode();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.usercode;
        }
    }
    public void setUsercode(String usercode)
    {
        if(getStub() != null) {
            getStub().setUsercode(usercode);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.usercode = usercode;
        }
    }

    public String getStatus()
    {
        if(getStub() != null) {
            return getStub().getStatus();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.status;
        }
    }
    public void setStatus(String status)
    {
        if(getStub() != null) {
            getStub().setStatus(status);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.status = status;
        }
    }

    public Long getCreatedTime()
    {
        if(getStub() != null) {
            return getStub().getCreatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.createdTime;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getStub() != null) {
            getStub().setCreatedTime(createdTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.createdTime = createdTime;
        }
    }

    public Long getModifiedTime()
    {
        if(getStub() != null) {
            return getStub().getModifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.modifiedTime;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getStub() != null) {
            getStub().setModifiedTime(modifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.modifiedTime = modifiedTime;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public UserUsercodeStub getStub()
    {
        return this.stub;
    }
    protected void setStub(UserUsercodeStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("guid = " + this.guid).append(";");
            sb.append("admin = " + this.admin).append(";");
            sb.append("user = " + this.user).append(";");
            sb.append("username = " + this.username).append(";");
            sb.append("usercode = " + this.usercode).append(";");
            sb.append("status = " + this.status).append(";");
            sb.append("createdTime = " + this.createdTime).append(";");
            sb.append("modifiedTime = " + this.modifiedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = guid == null ? 0 : guid.hashCode();
            _hash = 31 * _hash + delta;
            delta = admin == null ? 0 : admin.hashCode();
            _hash = 31 * _hash + delta;
            delta = user == null ? 0 : user.hashCode();
            _hash = 31 * _hash + delta;
            delta = username == null ? 0 : username.hashCode();
            _hash = 31 * _hash + delta;
            delta = usercode == null ? 0 : usercode.hashCode();
            _hash = 31 * _hash + delta;
            delta = status == null ? 0 : status.hashCode();
            _hash = 31 * _hash + delta;
            delta = createdTime == null ? 0 : createdTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
