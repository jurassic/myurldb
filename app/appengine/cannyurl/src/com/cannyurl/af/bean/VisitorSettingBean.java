package com.cannyurl.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.VisitorSetting;
import com.myurldb.ws.stub.PersonalSettingStub;
import com.myurldb.ws.stub.VisitorSettingStub;


// Wrapper class + bean combo.
public class VisitorSettingBean extends PersonalSettingBean implements VisitorSetting, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(VisitorSettingBean.class.getName());


    // [2] Or, without an embedded object.
    private String user;
    private String redirectType;
    private Long flashDuration;
    private String privacyType;

    // Ctors.
    public VisitorSettingBean()
    {
        //this((String) null);
    }
    public VisitorSettingBean(String guid)
    {
        this(guid, null, null, null, null, null, null);
    }
    public VisitorSettingBean(String guid, String user, String redirectType, Long flashDuration, String privacyType)
    {
        this(guid, user, redirectType, flashDuration, privacyType, null, null);
    }
    public VisitorSettingBean(String guid, String user, String redirectType, Long flashDuration, String privacyType, Long createdTime, Long modifiedTime)
    {
        super(guid, createdTime, modifiedTime);

        this.user = user;
        this.redirectType = redirectType;
        this.flashDuration = flashDuration;
        this.privacyType = privacyType;
    }
    public VisitorSettingBean(VisitorSetting stub)
    {
        if(stub instanceof VisitorSettingStub) {
            super.setStub((PersonalSettingStub) stub);
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setUser(stub.getUser());   
            setRedirectType(stub.getRedirectType());   
            setFlashDuration(stub.getFlashDuration());   
            setPrivacyType(stub.getPrivacyType());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    public String getUser()
    {
        if(getStub() != null) {
            return getStub().getUser();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.user;
        }
    }
    public void setUser(String user)
    {
        if(getStub() != null) {
            getStub().setUser(user);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.user = user;
        }
    }

    public String getRedirectType()
    {
        if(getStub() != null) {
            return getStub().getRedirectType();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.redirectType;
        }
    }
    public void setRedirectType(String redirectType)
    {
        if(getStub() != null) {
            getStub().setRedirectType(redirectType);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.redirectType = redirectType;
        }
    }

    public Long getFlashDuration()
    {
        if(getStub() != null) {
            return getStub().getFlashDuration();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.flashDuration;
        }
    }
    public void setFlashDuration(Long flashDuration)
    {
        if(getStub() != null) {
            getStub().setFlashDuration(flashDuration);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.flashDuration = flashDuration;
        }
    }

    public String getPrivacyType()
    {
        if(getStub() != null) {
            return getStub().getPrivacyType();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.privacyType;
        }
    }
    public void setPrivacyType(String privacyType)
    {
        if(getStub() != null) {
            getStub().setPrivacyType(privacyType);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.privacyType = privacyType;
        }
    }

    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    // Returns the reference to the embedded object. (Could be null.)
    public VisitorSettingStub getStub()
    {
        return (VisitorSettingStub) super.getStub();
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder( super.toString() );
            sb.append("user = " + this.user).append(";");
            sb.append("redirectType = " + this.redirectType).append(";");
            sb.append("flashDuration = " + this.flashDuration).append(";");
            sb.append("privacyType = " + this.privacyType).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = user == null ? 0 : user.hashCode();
            _hash = 31 * _hash + delta;
            delta = redirectType == null ? 0 : redirectType.hashCode();
            _hash = 31 * _hash + delta;
            delta = flashDuration == null ? 0 : flashDuration.hashCode();
            _hash = 31 * _hash + delta;
            delta = privacyType == null ? 0 : privacyType.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
