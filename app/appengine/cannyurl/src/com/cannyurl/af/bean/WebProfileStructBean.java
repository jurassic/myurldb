package com.cannyurl.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.WebProfileStruct;
import com.myurldb.ws.stub.WebProfileStructStub;


// Wrapper class + bean combo.
public class WebProfileStructBean implements WebProfileStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(WebProfileStructBean.class.getName());

    // [1] With an embedded object.
    private WebProfileStructStub stub = null;

    // [2] Or, without an embedded object.
    private String uuid;
    private String type;
    private String serviceName;
    private String serviceUrl;
    private String profileUrl;
    private String note;

    // Ctors.
    public WebProfileStructBean()
    {
        //this((String) null);
    }
    public WebProfileStructBean(String uuid, String type, String serviceName, String serviceUrl, String profileUrl, String note)
    {
        this.uuid = uuid;
        this.type = type;
        this.serviceName = serviceName;
        this.serviceUrl = serviceUrl;
        this.profileUrl = profileUrl;
        this.note = note;
    }
    public WebProfileStructBean(WebProfileStruct stub)
    {
        if(stub instanceof WebProfileStructStub) {
            this.stub = (WebProfileStructStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setUuid(stub.getUuid());   
            setType(stub.getType());   
            setServiceName(stub.getServiceName());   
            setServiceUrl(stub.getServiceUrl());   
            setProfileUrl(stub.getProfileUrl());   
            setNote(stub.getNote());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getUuid()
    {
        if(getStub() != null) {
            return getStub().getUuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.uuid;
        }
    }
    public void setUuid(String uuid)
    {
        if(getStub() != null) {
            getStub().setUuid(uuid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.uuid = uuid;
        }
    }

    public String getType()
    {
        if(getStub() != null) {
            return getStub().getType();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.type;
        }
    }
    public void setType(String type)
    {
        if(getStub() != null) {
            getStub().setType(type);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.type = type;
        }
    }

    public String getServiceName()
    {
        if(getStub() != null) {
            return getStub().getServiceName();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.serviceName;
        }
    }
    public void setServiceName(String serviceName)
    {
        if(getStub() != null) {
            getStub().setServiceName(serviceName);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.serviceName = serviceName;
        }
    }

    public String getServiceUrl()
    {
        if(getStub() != null) {
            return getStub().getServiceUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.serviceUrl;
        }
    }
    public void setServiceUrl(String serviceUrl)
    {
        if(getStub() != null) {
            getStub().setServiceUrl(serviceUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.serviceUrl = serviceUrl;
        }
    }

    public String getProfileUrl()
    {
        if(getStub() != null) {
            return getStub().getProfileUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.profileUrl;
        }
    }
    public void setProfileUrl(String profileUrl)
    {
        if(getStub() != null) {
            getStub().setProfileUrl(profileUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.profileUrl = profileUrl;
        }
    }

    public String getNote()
    {
        if(getStub() != null) {
            return getStub().getNote();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.note;
        }
    }
    public void setNote(String note)
    {
        if(getStub() != null) {
            getStub().setNote(note);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.note = note;
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getServiceName() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getServiceUrl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getProfileUrl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // Returns the reference to the embedded object. (Could be null.)
    public WebProfileStructStub getStub()
    {
        return this.stub;
    }
    protected void setStub(WebProfileStructStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("uuid = " + this.uuid).append(";");
            sb.append("type = " + this.type).append(";");
            sb.append("serviceName = " + this.serviceName).append(";");
            sb.append("serviceUrl = " + this.serviceUrl).append(";");
            sb.append("profileUrl = " + this.profileUrl).append(";");
            sb.append("note = " + this.note).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = uuid == null ? 0 : uuid.hashCode();
            _hash = 31 * _hash + delta;
            delta = type == null ? 0 : type.hashCode();
            _hash = 31 * _hash + delta;
            delta = serviceName == null ? 0 : serviceName.hashCode();
            _hash = 31 * _hash + delta;
            delta = serviceUrl == null ? 0 : serviceUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = profileUrl == null ? 0 : profileUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = note == null ? 0 : note.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
