package com.cannyurl.af.cert.proxy;

public abstract class AbstractProxyFactory
{
    public abstract PublicCertificateInfoServiceProxy getPublicCertificateInfoServiceProxy();
}
