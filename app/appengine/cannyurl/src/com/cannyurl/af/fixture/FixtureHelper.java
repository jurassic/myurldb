package com.cannyurl.af.fixture;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.cannyurl.af.config.Config;
import com.cannyurl.af.bean.ApiConsumerBean;
import com.cannyurl.af.bean.AppCustomDomainBean;
import com.cannyurl.af.bean.SiteCustomDomainBean;
import com.cannyurl.af.bean.UserBean;
import com.cannyurl.af.bean.UserUsercodeBean;
import com.cannyurl.af.bean.UserPasswordBean;
import com.cannyurl.af.bean.ExternalUserAuthBean;
import com.cannyurl.af.bean.UserAuthStateBean;
import com.cannyurl.af.bean.UserResourcePermissionBean;
import com.cannyurl.af.bean.UserResourceProhibitionBean;
import com.cannyurl.af.bean.RolePermissionBean;
import com.cannyurl.af.bean.UserRoleBean;
import com.cannyurl.af.bean.AppClientBean;
import com.cannyurl.af.bean.ClientUserBean;
import com.cannyurl.af.bean.UserCustomDomainBean;
import com.cannyurl.af.bean.ClientSettingBean;
import com.cannyurl.af.bean.UserSettingBean;
import com.cannyurl.af.bean.VisitorSettingBean;
import com.cannyurl.af.bean.TwitterSummaryCardBean;
import com.cannyurl.af.bean.TwitterPhotoCardBean;
import com.cannyurl.af.bean.TwitterGalleryCardBean;
import com.cannyurl.af.bean.TwitterAppCardBean;
import com.cannyurl.af.bean.TwitterPlayerCardBean;
import com.cannyurl.af.bean.TwitterProductCardBean;
import com.cannyurl.af.bean.ShortPassageBean;
import com.cannyurl.af.bean.ShortLinkBean;
import com.cannyurl.af.bean.GeoLinkBean;
import com.cannyurl.af.bean.QrCodeBean;
import com.cannyurl.af.bean.LinkPassphraseBean;
import com.cannyurl.af.bean.LinkMessageBean;
import com.cannyurl.af.bean.LinkAlbumBean;
import com.cannyurl.af.bean.AlbumShortLinkBean;
import com.cannyurl.af.bean.KeywordFolderBean;
import com.cannyurl.af.bean.BookmarkFolderBean;
import com.cannyurl.af.bean.KeywordLinkBean;
import com.cannyurl.af.bean.BookmarkLinkBean;
import com.cannyurl.af.bean.SpeedDialBean;
import com.cannyurl.af.bean.KeywordFolderImportBean;
import com.cannyurl.af.bean.BookmarkFolderImportBean;
import com.cannyurl.af.bean.KeywordCrowdTallyBean;
import com.cannyurl.af.bean.BookmarkCrowdTallyBean;
import com.cannyurl.af.bean.DomainInfoBean;
import com.cannyurl.af.bean.UrlRatingBean;
import com.cannyurl.af.bean.UserRatingBean;
import com.cannyurl.af.bean.AbuseTagBean;
import com.cannyurl.af.bean.ServiceInfoBean;
import com.cannyurl.af.bean.FiveTenBean;
import com.myurldb.ws.stub.ApiConsumerStub;
import com.myurldb.ws.stub.AppCustomDomainStub;
import com.myurldb.ws.stub.SiteCustomDomainStub;
import com.myurldb.ws.stub.UserStub;
import com.myurldb.ws.stub.UserUsercodeStub;
import com.myurldb.ws.stub.UserPasswordStub;
import com.myurldb.ws.stub.ExternalUserAuthStub;
import com.myurldb.ws.stub.UserAuthStateStub;
import com.myurldb.ws.stub.UserResourcePermissionStub;
import com.myurldb.ws.stub.UserResourceProhibitionStub;
import com.myurldb.ws.stub.RolePermissionStub;
import com.myurldb.ws.stub.UserRoleStub;
import com.myurldb.ws.stub.AppClientStub;
import com.myurldb.ws.stub.ClientUserStub;
import com.myurldb.ws.stub.UserCustomDomainStub;
import com.myurldb.ws.stub.ClientSettingStub;
import com.myurldb.ws.stub.UserSettingStub;
import com.myurldb.ws.stub.VisitorSettingStub;
import com.myurldb.ws.stub.TwitterSummaryCardStub;
import com.myurldb.ws.stub.TwitterPhotoCardStub;
import com.myurldb.ws.stub.TwitterGalleryCardStub;
import com.myurldb.ws.stub.TwitterAppCardStub;
import com.myurldb.ws.stub.TwitterPlayerCardStub;
import com.myurldb.ws.stub.TwitterProductCardStub;
import com.myurldb.ws.stub.ShortPassageStub;
import com.myurldb.ws.stub.ShortLinkStub;
import com.myurldb.ws.stub.GeoLinkStub;
import com.myurldb.ws.stub.QrCodeStub;
import com.myurldb.ws.stub.LinkPassphraseStub;
import com.myurldb.ws.stub.LinkMessageStub;
import com.myurldb.ws.stub.LinkAlbumStub;
import com.myurldb.ws.stub.AlbumShortLinkStub;
import com.myurldb.ws.stub.KeywordFolderStub;
import com.myurldb.ws.stub.BookmarkFolderStub;
import com.myurldb.ws.stub.KeywordLinkStub;
import com.myurldb.ws.stub.BookmarkLinkStub;
import com.myurldb.ws.stub.SpeedDialStub;
import com.myurldb.ws.stub.KeywordFolderImportStub;
import com.myurldb.ws.stub.BookmarkFolderImportStub;
import com.myurldb.ws.stub.KeywordCrowdTallyStub;
import com.myurldb.ws.stub.BookmarkCrowdTallyStub;
import com.myurldb.ws.stub.DomainInfoStub;
import com.myurldb.ws.stub.UrlRatingStub;
import com.myurldb.ws.stub.UserRatingStub;
import com.myurldb.ws.stub.AbuseTagStub;
import com.myurldb.ws.stub.ServiceInfoStub;
import com.myurldb.ws.stub.FiveTenStub;
import com.myurldb.ws.stub.ApiConsumerListStub;
import com.myurldb.ws.stub.AppCustomDomainListStub;
import com.myurldb.ws.stub.SiteCustomDomainListStub;
import com.myurldb.ws.stub.UserListStub;
import com.myurldb.ws.stub.UserUsercodeListStub;
import com.myurldb.ws.stub.UserPasswordListStub;
import com.myurldb.ws.stub.ExternalUserAuthListStub;
import com.myurldb.ws.stub.UserAuthStateListStub;
import com.myurldb.ws.stub.UserResourcePermissionListStub;
import com.myurldb.ws.stub.UserResourceProhibitionListStub;
import com.myurldb.ws.stub.RolePermissionListStub;
import com.myurldb.ws.stub.UserRoleListStub;
import com.myurldb.ws.stub.AppClientListStub;
import com.myurldb.ws.stub.ClientUserListStub;
import com.myurldb.ws.stub.UserCustomDomainListStub;
import com.myurldb.ws.stub.ClientSettingListStub;
import com.myurldb.ws.stub.UserSettingListStub;
import com.myurldb.ws.stub.VisitorSettingListStub;
import com.myurldb.ws.stub.TwitterSummaryCardListStub;
import com.myurldb.ws.stub.TwitterPhotoCardListStub;
import com.myurldb.ws.stub.TwitterGalleryCardListStub;
import com.myurldb.ws.stub.TwitterAppCardListStub;
import com.myurldb.ws.stub.TwitterPlayerCardListStub;
import com.myurldb.ws.stub.TwitterProductCardListStub;
import com.myurldb.ws.stub.ShortPassageListStub;
import com.myurldb.ws.stub.ShortLinkListStub;
import com.myurldb.ws.stub.GeoLinkListStub;
import com.myurldb.ws.stub.QrCodeListStub;
import com.myurldb.ws.stub.LinkPassphraseListStub;
import com.myurldb.ws.stub.LinkMessageListStub;
import com.myurldb.ws.stub.LinkAlbumListStub;
import com.myurldb.ws.stub.AlbumShortLinkListStub;
import com.myurldb.ws.stub.KeywordFolderListStub;
import com.myurldb.ws.stub.BookmarkFolderListStub;
import com.myurldb.ws.stub.KeywordLinkListStub;
import com.myurldb.ws.stub.BookmarkLinkListStub;
import com.myurldb.ws.stub.SpeedDialListStub;
import com.myurldb.ws.stub.KeywordFolderImportListStub;
import com.myurldb.ws.stub.BookmarkFolderImportListStub;
import com.myurldb.ws.stub.KeywordCrowdTallyListStub;
import com.myurldb.ws.stub.BookmarkCrowdTallyListStub;
import com.myurldb.ws.stub.DomainInfoListStub;
import com.myurldb.ws.stub.UrlRatingListStub;
import com.myurldb.ws.stub.UserRatingListStub;
import com.myurldb.ws.stub.AbuseTagListStub;
import com.myurldb.ws.stub.ServiceInfoListStub;
import com.myurldb.ws.stub.FiveTenListStub;
import com.cannyurl.af.service.manager.ServiceManager;
import com.cannyurl.af.resource.impl.AppCustomDomainResourceImpl;
import com.cannyurl.af.resource.impl.SiteCustomDomainResourceImpl;
import com.cannyurl.af.resource.impl.UserResourceImpl;
import com.cannyurl.af.resource.impl.UserUsercodeResourceImpl;
import com.cannyurl.af.resource.impl.UserResourcePermissionResourceImpl;
import com.cannyurl.af.resource.impl.UserResourceProhibitionResourceImpl;
import com.cannyurl.af.resource.impl.RolePermissionResourceImpl;
import com.cannyurl.af.resource.impl.UserRoleResourceImpl;
import com.cannyurl.af.resource.impl.AppClientResourceImpl;
import com.cannyurl.af.resource.impl.ClientUserResourceImpl;
import com.cannyurl.af.resource.impl.UserCustomDomainResourceImpl;
import com.cannyurl.af.resource.impl.ClientSettingResourceImpl;
import com.cannyurl.af.resource.impl.UserSettingResourceImpl;
import com.cannyurl.af.resource.impl.VisitorSettingResourceImpl;
import com.cannyurl.af.resource.impl.TwitterSummaryCardResourceImpl;
import com.cannyurl.af.resource.impl.TwitterPhotoCardResourceImpl;
import com.cannyurl.af.resource.impl.TwitterGalleryCardResourceImpl;
import com.cannyurl.af.resource.impl.TwitterAppCardResourceImpl;
import com.cannyurl.af.resource.impl.TwitterPlayerCardResourceImpl;
import com.cannyurl.af.resource.impl.TwitterProductCardResourceImpl;
import com.cannyurl.af.resource.impl.ShortPassageResourceImpl;
import com.cannyurl.af.resource.impl.ShortLinkResourceImpl;
import com.cannyurl.af.resource.impl.GeoLinkResourceImpl;
import com.cannyurl.af.resource.impl.QrCodeResourceImpl;
import com.cannyurl.af.resource.impl.LinkPassphraseResourceImpl;
import com.cannyurl.af.resource.impl.LinkMessageResourceImpl;
import com.cannyurl.af.resource.impl.LinkAlbumResourceImpl;
import com.cannyurl.af.resource.impl.AlbumShortLinkResourceImpl;
import com.cannyurl.af.resource.impl.KeywordFolderResourceImpl;
import com.cannyurl.af.resource.impl.BookmarkFolderResourceImpl;
import com.cannyurl.af.resource.impl.KeywordLinkResourceImpl;
import com.cannyurl.af.resource.impl.BookmarkLinkResourceImpl;
import com.cannyurl.af.resource.impl.SpeedDialResourceImpl;
import com.cannyurl.af.resource.impl.KeywordFolderImportResourceImpl;
import com.cannyurl.af.resource.impl.BookmarkFolderImportResourceImpl;
import com.cannyurl.af.resource.impl.KeywordCrowdTallyResourceImpl;
import com.cannyurl.af.resource.impl.BookmarkCrowdTallyResourceImpl;
import com.cannyurl.af.resource.impl.DomainInfoResourceImpl;
import com.cannyurl.af.resource.impl.UrlRatingResourceImpl;
import com.cannyurl.af.resource.impl.UserRatingResourceImpl;
import com.cannyurl.af.resource.impl.AbuseTagResourceImpl;
import com.cannyurl.af.resource.impl.ServiceInfoResourceImpl;
import com.cannyurl.af.resource.impl.FiveTenResourceImpl;


// "Helper" functions...
// Get the list of fixture files,
// Read them, and
// Load the data into the datastore, etc....
// See the note in FixtureUtil...
public class FixtureHelper
{
    private static final Logger log = Logger.getLogger(FixtureHelper.class.getName());

    // ???
    private static final String CONFIG_KEY_FIXTURE_LOAD = "cannyurlapp.fixture.load";
    private static final String CONFIG_KEY_FIXTURE_DIR = "cannyurlapp.fixture.directory";
    // ...

    // Dummy var.
    protected boolean dummyFalse1 = false;
    // ...

    
    private FixtureHelper()
    {
        // ...
    }
    
    // Initialization-on-demand holder.
    private static class FixtureHelperHolder
    {
        private static final FixtureHelper INSTANCE = new FixtureHelper();
    }

    // Singleton method
    public static FixtureHelper getInstance()
    {
        return FixtureHelperHolder.INSTANCE;
    }


    public boolean isFixtureLoad()
    {
        // temporary
        return Config.getInstance().getBoolean(CONFIG_KEY_FIXTURE_LOAD, FixtureUtil.getDefaultFixtureLoad());
    }

    public String getFixtureDirName()
    {
        // temporary
        return Config.getInstance().getString(CONFIG_KEY_FIXTURE_DIR, FixtureUtil.getDefaultFixtureDir());
    }

    
    // TBD
    private List<FixtureFile> buildFixtureFileList()
    {
        List<FixtureFile> list = new ArrayList<FixtureFile>();

        String fixtureDirName = getFixtureDirName();
        File fixtureDir = new File(fixtureDirName);
        if(!fixtureDir.exists() || !fixtureDir.isDirectory()) {
            // error. what to do????
            if(log.isLoggable(Level.WARNING)) log.warning("Fixture file directory does not exist. fixtureDirName = " + fixtureDirName);
        } else {
            File[] files = fixtureDir.listFiles();
            for(File f : files) {
                if(f.isFile()) {
                    String filePath = f.getPath();  // ???
                    FixtureFile ff = new FixtureFile(filePath);
                    list.add(ff);
                } else {
                    // This should not happen. Ignore...                    
                }
            }
        }
        
        return list;
    }
    
    public int processFixtureFiles()
    {
        List<FixtureFile> list = buildFixtureFileList();
        if(log.isLoggable(Level.FINE)) {
            for(FixtureFile f : list) {
                log.fine("FixtureFile f = " + f);
            }
        }
        
        int count = 0;
        for(FixtureFile f : list) {  // list cannot be null.
            // [1] Read the file
            // [2] Convert the file content to objects
            // [3] Then, save it. (We use update/overwrite rather than create to ensure "idempotency".)
            String filePath = f.getFilePath();
            String objectType = f.getObjectType();
            String mediaType = f.getMediaType();
            
            // JSON only, for now....
            if(! FixtureUtil.MEDIA_TYPE_JSON.equals(mediaType)) {
                if(log.isLoggable(Level.WARNING)) log.warning("Currently supports only Json fixture files. Skipping filePath = " + filePath);
                continue;
            }

            BufferedReader reader = null;
            File inputFile = new File(filePath);
            if(inputFile.canRead()) {
                try {
                    reader = new BufferedReader(new FileReader(inputFile));
                    StringBuilder sb = new StringBuilder(0x1000);
                    final char[] buf = new char[0x1000];
                    int read = -1;
                    do {
                        read = reader.read(buf, 0, buf.length);
                        if (read>0) {
                            sb.append(buf, 0, read);
                        }
                    } while (read>=0);

                    if(dummyFalse1) {
                    } else if("AppCustomDomain".equals(objectType)) {
                        AppCustomDomainStub stub = AppCustomDomainStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        AppCustomDomainBean bean = AppCustomDomainResourceImpl.convertAppCustomDomainStubToBean(stub);
                        boolean suc = ServiceManager.getAppCustomDomainService().updateAppCustomDomain(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }                        
                    } else if("AppCustomDomainList".equals(objectType)) {
                        AppCustomDomainListStub listStub = AppCustomDomainListStub.fromJsonString(sb.toString());
                        List<AppCustomDomainBean> beanList = AppCustomDomainResourceImpl.convertAppCustomDomainListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(AppCustomDomainBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getAppCustomDomainService().updateAppCustomDomain(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("SiteCustomDomain".equals(objectType)) {
                        SiteCustomDomainStub stub = SiteCustomDomainStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        SiteCustomDomainBean bean = SiteCustomDomainResourceImpl.convertSiteCustomDomainStubToBean(stub);
                        boolean suc = ServiceManager.getSiteCustomDomainService().updateSiteCustomDomain(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }                        
                    } else if("SiteCustomDomainList".equals(objectType)) {
                        SiteCustomDomainListStub listStub = SiteCustomDomainListStub.fromJsonString(sb.toString());
                        List<SiteCustomDomainBean> beanList = SiteCustomDomainResourceImpl.convertSiteCustomDomainListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(SiteCustomDomainBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getSiteCustomDomainService().updateSiteCustomDomain(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("User".equals(objectType)) {
                        UserStub stub = UserStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        UserBean bean = UserResourceImpl.convertUserStubToBean(stub);
                        boolean suc = ServiceManager.getUserService().updateUser(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }                        
                    } else if("UserList".equals(objectType)) {
                        UserListStub listStub = UserListStub.fromJsonString(sb.toString());
                        List<UserBean> beanList = UserResourceImpl.convertUserListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(UserBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getUserService().updateUser(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("UserUsercode".equals(objectType)) {
                        UserUsercodeStub stub = UserUsercodeStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        UserUsercodeBean bean = UserUsercodeResourceImpl.convertUserUsercodeStubToBean(stub);
                        boolean suc = ServiceManager.getUserUsercodeService().updateUserUsercode(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }                        
                    } else if("UserUsercodeList".equals(objectType)) {
                        UserUsercodeListStub listStub = UserUsercodeListStub.fromJsonString(sb.toString());
                        List<UserUsercodeBean> beanList = UserUsercodeResourceImpl.convertUserUsercodeListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(UserUsercodeBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getUserUsercodeService().updateUserUsercode(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("UserResourcePermission".equals(objectType)) {
                        UserResourcePermissionStub stub = UserResourcePermissionStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        UserResourcePermissionBean bean = UserResourcePermissionResourceImpl.convertUserResourcePermissionStubToBean(stub);
                        boolean suc = ServiceManager.getUserResourcePermissionService().updateUserResourcePermission(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }                        
                    } else if("UserResourcePermissionList".equals(objectType)) {
                        UserResourcePermissionListStub listStub = UserResourcePermissionListStub.fromJsonString(sb.toString());
                        List<UserResourcePermissionBean> beanList = UserResourcePermissionResourceImpl.convertUserResourcePermissionListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(UserResourcePermissionBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getUserResourcePermissionService().updateUserResourcePermission(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("UserResourceProhibition".equals(objectType)) {
                        UserResourceProhibitionStub stub = UserResourceProhibitionStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        UserResourceProhibitionBean bean = UserResourceProhibitionResourceImpl.convertUserResourceProhibitionStubToBean(stub);
                        boolean suc = ServiceManager.getUserResourceProhibitionService().updateUserResourceProhibition(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }                        
                    } else if("UserResourceProhibitionList".equals(objectType)) {
                        UserResourceProhibitionListStub listStub = UserResourceProhibitionListStub.fromJsonString(sb.toString());
                        List<UserResourceProhibitionBean> beanList = UserResourceProhibitionResourceImpl.convertUserResourceProhibitionListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(UserResourceProhibitionBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getUserResourceProhibitionService().updateUserResourceProhibition(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("RolePermission".equals(objectType)) {
                        RolePermissionStub stub = RolePermissionStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        RolePermissionBean bean = RolePermissionResourceImpl.convertRolePermissionStubToBean(stub);
                        boolean suc = ServiceManager.getRolePermissionService().updateRolePermission(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }                        
                    } else if("RolePermissionList".equals(objectType)) {
                        RolePermissionListStub listStub = RolePermissionListStub.fromJsonString(sb.toString());
                        List<RolePermissionBean> beanList = RolePermissionResourceImpl.convertRolePermissionListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(RolePermissionBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getRolePermissionService().updateRolePermission(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("UserRole".equals(objectType)) {
                        UserRoleStub stub = UserRoleStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        UserRoleBean bean = UserRoleResourceImpl.convertUserRoleStubToBean(stub);
                        boolean suc = ServiceManager.getUserRoleService().updateUserRole(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }                        
                    } else if("UserRoleList".equals(objectType)) {
                        UserRoleListStub listStub = UserRoleListStub.fromJsonString(sb.toString());
                        List<UserRoleBean> beanList = UserRoleResourceImpl.convertUserRoleListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(UserRoleBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getUserRoleService().updateUserRole(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("AppClient".equals(objectType)) {
                        AppClientStub stub = AppClientStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        AppClientBean bean = AppClientResourceImpl.convertAppClientStubToBean(stub);
                        boolean suc = ServiceManager.getAppClientService().updateAppClient(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }                        
                    } else if("AppClientList".equals(objectType)) {
                        AppClientListStub listStub = AppClientListStub.fromJsonString(sb.toString());
                        List<AppClientBean> beanList = AppClientResourceImpl.convertAppClientListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(AppClientBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getAppClientService().updateAppClient(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("ClientUser".equals(objectType)) {
                        ClientUserStub stub = ClientUserStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        ClientUserBean bean = ClientUserResourceImpl.convertClientUserStubToBean(stub);
                        boolean suc = ServiceManager.getClientUserService().updateClientUser(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }                        
                    } else if("ClientUserList".equals(objectType)) {
                        ClientUserListStub listStub = ClientUserListStub.fromJsonString(sb.toString());
                        List<ClientUserBean> beanList = ClientUserResourceImpl.convertClientUserListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(ClientUserBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getClientUserService().updateClientUser(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("UserCustomDomain".equals(objectType)) {
                        UserCustomDomainStub stub = UserCustomDomainStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        UserCustomDomainBean bean = UserCustomDomainResourceImpl.convertUserCustomDomainStubToBean(stub);
                        boolean suc = ServiceManager.getUserCustomDomainService().updateUserCustomDomain(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }                        
                    } else if("UserCustomDomainList".equals(objectType)) {
                        UserCustomDomainListStub listStub = UserCustomDomainListStub.fromJsonString(sb.toString());
                        List<UserCustomDomainBean> beanList = UserCustomDomainResourceImpl.convertUserCustomDomainListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(UserCustomDomainBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getUserCustomDomainService().updateUserCustomDomain(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("ClientSetting".equals(objectType)) {
                        ClientSettingStub stub = ClientSettingStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        ClientSettingBean bean = ClientSettingResourceImpl.convertClientSettingStubToBean(stub);
                        boolean suc = ServiceManager.getClientSettingService().updateClientSetting(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }                        
                    } else if("ClientSettingList".equals(objectType)) {
                        ClientSettingListStub listStub = ClientSettingListStub.fromJsonString(sb.toString());
                        List<ClientSettingBean> beanList = ClientSettingResourceImpl.convertClientSettingListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(ClientSettingBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getClientSettingService().updateClientSetting(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("UserSetting".equals(objectType)) {
                        UserSettingStub stub = UserSettingStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        UserSettingBean bean = UserSettingResourceImpl.convertUserSettingStubToBean(stub);
                        boolean suc = ServiceManager.getUserSettingService().updateUserSetting(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }                        
                    } else if("UserSettingList".equals(objectType)) {
                        UserSettingListStub listStub = UserSettingListStub.fromJsonString(sb.toString());
                        List<UserSettingBean> beanList = UserSettingResourceImpl.convertUserSettingListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(UserSettingBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getUserSettingService().updateUserSetting(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("VisitorSetting".equals(objectType)) {
                        VisitorSettingStub stub = VisitorSettingStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        VisitorSettingBean bean = VisitorSettingResourceImpl.convertVisitorSettingStubToBean(stub);
                        boolean suc = ServiceManager.getVisitorSettingService().updateVisitorSetting(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }                        
                    } else if("VisitorSettingList".equals(objectType)) {
                        VisitorSettingListStub listStub = VisitorSettingListStub.fromJsonString(sb.toString());
                        List<VisitorSettingBean> beanList = VisitorSettingResourceImpl.convertVisitorSettingListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(VisitorSettingBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getVisitorSettingService().updateVisitorSetting(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("TwitterSummaryCard".equals(objectType)) {
                        TwitterSummaryCardStub stub = TwitterSummaryCardStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        TwitterSummaryCardBean bean = TwitterSummaryCardResourceImpl.convertTwitterSummaryCardStubToBean(stub);
                        boolean suc = ServiceManager.getTwitterSummaryCardService().updateTwitterSummaryCard(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }                        
                    } else if("TwitterSummaryCardList".equals(objectType)) {
                        TwitterSummaryCardListStub listStub = TwitterSummaryCardListStub.fromJsonString(sb.toString());
                        List<TwitterSummaryCardBean> beanList = TwitterSummaryCardResourceImpl.convertTwitterSummaryCardListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(TwitterSummaryCardBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getTwitterSummaryCardService().updateTwitterSummaryCard(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("TwitterPhotoCard".equals(objectType)) {
                        TwitterPhotoCardStub stub = TwitterPhotoCardStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        TwitterPhotoCardBean bean = TwitterPhotoCardResourceImpl.convertTwitterPhotoCardStubToBean(stub);
                        boolean suc = ServiceManager.getTwitterPhotoCardService().updateTwitterPhotoCard(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }                        
                    } else if("TwitterPhotoCardList".equals(objectType)) {
                        TwitterPhotoCardListStub listStub = TwitterPhotoCardListStub.fromJsonString(sb.toString());
                        List<TwitterPhotoCardBean> beanList = TwitterPhotoCardResourceImpl.convertTwitterPhotoCardListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(TwitterPhotoCardBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getTwitterPhotoCardService().updateTwitterPhotoCard(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("TwitterGalleryCard".equals(objectType)) {
                        TwitterGalleryCardStub stub = TwitterGalleryCardStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        TwitterGalleryCardBean bean = TwitterGalleryCardResourceImpl.convertTwitterGalleryCardStubToBean(stub);
                        boolean suc = ServiceManager.getTwitterGalleryCardService().updateTwitterGalleryCard(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }                        
                    } else if("TwitterGalleryCardList".equals(objectType)) {
                        TwitterGalleryCardListStub listStub = TwitterGalleryCardListStub.fromJsonString(sb.toString());
                        List<TwitterGalleryCardBean> beanList = TwitterGalleryCardResourceImpl.convertTwitterGalleryCardListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(TwitterGalleryCardBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getTwitterGalleryCardService().updateTwitterGalleryCard(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("TwitterAppCard".equals(objectType)) {
                        TwitterAppCardStub stub = TwitterAppCardStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        TwitterAppCardBean bean = TwitterAppCardResourceImpl.convertTwitterAppCardStubToBean(stub);
                        boolean suc = ServiceManager.getTwitterAppCardService().updateTwitterAppCard(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }                        
                    } else if("TwitterAppCardList".equals(objectType)) {
                        TwitterAppCardListStub listStub = TwitterAppCardListStub.fromJsonString(sb.toString());
                        List<TwitterAppCardBean> beanList = TwitterAppCardResourceImpl.convertTwitterAppCardListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(TwitterAppCardBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getTwitterAppCardService().updateTwitterAppCard(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("TwitterPlayerCard".equals(objectType)) {
                        TwitterPlayerCardStub stub = TwitterPlayerCardStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        TwitterPlayerCardBean bean = TwitterPlayerCardResourceImpl.convertTwitterPlayerCardStubToBean(stub);
                        boolean suc = ServiceManager.getTwitterPlayerCardService().updateTwitterPlayerCard(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }                        
                    } else if("TwitterPlayerCardList".equals(objectType)) {
                        TwitterPlayerCardListStub listStub = TwitterPlayerCardListStub.fromJsonString(sb.toString());
                        List<TwitterPlayerCardBean> beanList = TwitterPlayerCardResourceImpl.convertTwitterPlayerCardListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(TwitterPlayerCardBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getTwitterPlayerCardService().updateTwitterPlayerCard(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("TwitterProductCard".equals(objectType)) {
                        TwitterProductCardStub stub = TwitterProductCardStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        TwitterProductCardBean bean = TwitterProductCardResourceImpl.convertTwitterProductCardStubToBean(stub);
                        boolean suc = ServiceManager.getTwitterProductCardService().updateTwitterProductCard(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }                        
                    } else if("TwitterProductCardList".equals(objectType)) {
                        TwitterProductCardListStub listStub = TwitterProductCardListStub.fromJsonString(sb.toString());
                        List<TwitterProductCardBean> beanList = TwitterProductCardResourceImpl.convertTwitterProductCardListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(TwitterProductCardBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getTwitterProductCardService().updateTwitterProductCard(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("ShortPassage".equals(objectType)) {
                        ShortPassageStub stub = ShortPassageStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        ShortPassageBean bean = ShortPassageResourceImpl.convertShortPassageStubToBean(stub);
                        boolean suc = ServiceManager.getShortPassageService().updateShortPassage(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }                        
                    } else if("ShortPassageList".equals(objectType)) {
                        ShortPassageListStub listStub = ShortPassageListStub.fromJsonString(sb.toString());
                        List<ShortPassageBean> beanList = ShortPassageResourceImpl.convertShortPassageListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(ShortPassageBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getShortPassageService().updateShortPassage(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("ShortLink".equals(objectType)) {
                        ShortLinkStub stub = ShortLinkStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        ShortLinkBean bean = ShortLinkResourceImpl.convertShortLinkStubToBean(stub);
                        boolean suc = ServiceManager.getShortLinkService().updateShortLink(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }                        
                    } else if("ShortLinkList".equals(objectType)) {
                        ShortLinkListStub listStub = ShortLinkListStub.fromJsonString(sb.toString());
                        List<ShortLinkBean> beanList = ShortLinkResourceImpl.convertShortLinkListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(ShortLinkBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getShortLinkService().updateShortLink(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("GeoLink".equals(objectType)) {
                        GeoLinkStub stub = GeoLinkStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        GeoLinkBean bean = GeoLinkResourceImpl.convertGeoLinkStubToBean(stub);
                        boolean suc = ServiceManager.getGeoLinkService().updateGeoLink(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }                        
                    } else if("GeoLinkList".equals(objectType)) {
                        GeoLinkListStub listStub = GeoLinkListStub.fromJsonString(sb.toString());
                        List<GeoLinkBean> beanList = GeoLinkResourceImpl.convertGeoLinkListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(GeoLinkBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getGeoLinkService().updateGeoLink(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("QrCode".equals(objectType)) {
                        QrCodeStub stub = QrCodeStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        QrCodeBean bean = QrCodeResourceImpl.convertQrCodeStubToBean(stub);
                        boolean suc = ServiceManager.getQrCodeService().updateQrCode(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }                        
                    } else if("QrCodeList".equals(objectType)) {
                        QrCodeListStub listStub = QrCodeListStub.fromJsonString(sb.toString());
                        List<QrCodeBean> beanList = QrCodeResourceImpl.convertQrCodeListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(QrCodeBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getQrCodeService().updateQrCode(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("LinkPassphrase".equals(objectType)) {
                        LinkPassphraseStub stub = LinkPassphraseStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        LinkPassphraseBean bean = LinkPassphraseResourceImpl.convertLinkPassphraseStubToBean(stub);
                        boolean suc = ServiceManager.getLinkPassphraseService().updateLinkPassphrase(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }                        
                    } else if("LinkPassphraseList".equals(objectType)) {
                        LinkPassphraseListStub listStub = LinkPassphraseListStub.fromJsonString(sb.toString());
                        List<LinkPassphraseBean> beanList = LinkPassphraseResourceImpl.convertLinkPassphraseListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(LinkPassphraseBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getLinkPassphraseService().updateLinkPassphrase(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("LinkMessage".equals(objectType)) {
                        LinkMessageStub stub = LinkMessageStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        LinkMessageBean bean = LinkMessageResourceImpl.convertLinkMessageStubToBean(stub);
                        boolean suc = ServiceManager.getLinkMessageService().updateLinkMessage(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }                        
                    } else if("LinkMessageList".equals(objectType)) {
                        LinkMessageListStub listStub = LinkMessageListStub.fromJsonString(sb.toString());
                        List<LinkMessageBean> beanList = LinkMessageResourceImpl.convertLinkMessageListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(LinkMessageBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getLinkMessageService().updateLinkMessage(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("LinkAlbum".equals(objectType)) {
                        LinkAlbumStub stub = LinkAlbumStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        LinkAlbumBean bean = LinkAlbumResourceImpl.convertLinkAlbumStubToBean(stub);
                        boolean suc = ServiceManager.getLinkAlbumService().updateLinkAlbum(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }                        
                    } else if("LinkAlbumList".equals(objectType)) {
                        LinkAlbumListStub listStub = LinkAlbumListStub.fromJsonString(sb.toString());
                        List<LinkAlbumBean> beanList = LinkAlbumResourceImpl.convertLinkAlbumListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(LinkAlbumBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getLinkAlbumService().updateLinkAlbum(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("AlbumShortLink".equals(objectType)) {
                        AlbumShortLinkStub stub = AlbumShortLinkStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        AlbumShortLinkBean bean = AlbumShortLinkResourceImpl.convertAlbumShortLinkStubToBean(stub);
                        boolean suc = ServiceManager.getAlbumShortLinkService().updateAlbumShortLink(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }                        
                    } else if("AlbumShortLinkList".equals(objectType)) {
                        AlbumShortLinkListStub listStub = AlbumShortLinkListStub.fromJsonString(sb.toString());
                        List<AlbumShortLinkBean> beanList = AlbumShortLinkResourceImpl.convertAlbumShortLinkListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(AlbumShortLinkBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getAlbumShortLinkService().updateAlbumShortLink(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("KeywordFolder".equals(objectType)) {
                        KeywordFolderStub stub = KeywordFolderStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        KeywordFolderBean bean = KeywordFolderResourceImpl.convertKeywordFolderStubToBean(stub);
                        boolean suc = ServiceManager.getKeywordFolderService().updateKeywordFolder(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }                        
                    } else if("KeywordFolderList".equals(objectType)) {
                        KeywordFolderListStub listStub = KeywordFolderListStub.fromJsonString(sb.toString());
                        List<KeywordFolderBean> beanList = KeywordFolderResourceImpl.convertKeywordFolderListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(KeywordFolderBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getKeywordFolderService().updateKeywordFolder(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("BookmarkFolder".equals(objectType)) {
                        BookmarkFolderStub stub = BookmarkFolderStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        BookmarkFolderBean bean = BookmarkFolderResourceImpl.convertBookmarkFolderStubToBean(stub);
                        boolean suc = ServiceManager.getBookmarkFolderService().updateBookmarkFolder(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }                        
                    } else if("BookmarkFolderList".equals(objectType)) {
                        BookmarkFolderListStub listStub = BookmarkFolderListStub.fromJsonString(sb.toString());
                        List<BookmarkFolderBean> beanList = BookmarkFolderResourceImpl.convertBookmarkFolderListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(BookmarkFolderBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getBookmarkFolderService().updateBookmarkFolder(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("KeywordLink".equals(objectType)) {
                        KeywordLinkStub stub = KeywordLinkStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        KeywordLinkBean bean = KeywordLinkResourceImpl.convertKeywordLinkStubToBean(stub);
                        boolean suc = ServiceManager.getKeywordLinkService().updateKeywordLink(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }                        
                    } else if("KeywordLinkList".equals(objectType)) {
                        KeywordLinkListStub listStub = KeywordLinkListStub.fromJsonString(sb.toString());
                        List<KeywordLinkBean> beanList = KeywordLinkResourceImpl.convertKeywordLinkListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(KeywordLinkBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getKeywordLinkService().updateKeywordLink(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("BookmarkLink".equals(objectType)) {
                        BookmarkLinkStub stub = BookmarkLinkStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        BookmarkLinkBean bean = BookmarkLinkResourceImpl.convertBookmarkLinkStubToBean(stub);
                        boolean suc = ServiceManager.getBookmarkLinkService().updateBookmarkLink(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }                        
                    } else if("BookmarkLinkList".equals(objectType)) {
                        BookmarkLinkListStub listStub = BookmarkLinkListStub.fromJsonString(sb.toString());
                        List<BookmarkLinkBean> beanList = BookmarkLinkResourceImpl.convertBookmarkLinkListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(BookmarkLinkBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getBookmarkLinkService().updateBookmarkLink(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("SpeedDial".equals(objectType)) {
                        SpeedDialStub stub = SpeedDialStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        SpeedDialBean bean = SpeedDialResourceImpl.convertSpeedDialStubToBean(stub);
                        boolean suc = ServiceManager.getSpeedDialService().updateSpeedDial(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }                        
                    } else if("SpeedDialList".equals(objectType)) {
                        SpeedDialListStub listStub = SpeedDialListStub.fromJsonString(sb.toString());
                        List<SpeedDialBean> beanList = SpeedDialResourceImpl.convertSpeedDialListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(SpeedDialBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getSpeedDialService().updateSpeedDial(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("KeywordFolderImport".equals(objectType)) {
                        KeywordFolderImportStub stub = KeywordFolderImportStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        KeywordFolderImportBean bean = KeywordFolderImportResourceImpl.convertKeywordFolderImportStubToBean(stub);
                        boolean suc = ServiceManager.getKeywordFolderImportService().updateKeywordFolderImport(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }                        
                    } else if("KeywordFolderImportList".equals(objectType)) {
                        KeywordFolderImportListStub listStub = KeywordFolderImportListStub.fromJsonString(sb.toString());
                        List<KeywordFolderImportBean> beanList = KeywordFolderImportResourceImpl.convertKeywordFolderImportListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(KeywordFolderImportBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getKeywordFolderImportService().updateKeywordFolderImport(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("BookmarkFolderImport".equals(objectType)) {
                        BookmarkFolderImportStub stub = BookmarkFolderImportStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        BookmarkFolderImportBean bean = BookmarkFolderImportResourceImpl.convertBookmarkFolderImportStubToBean(stub);
                        boolean suc = ServiceManager.getBookmarkFolderImportService().updateBookmarkFolderImport(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }                        
                    } else if("BookmarkFolderImportList".equals(objectType)) {
                        BookmarkFolderImportListStub listStub = BookmarkFolderImportListStub.fromJsonString(sb.toString());
                        List<BookmarkFolderImportBean> beanList = BookmarkFolderImportResourceImpl.convertBookmarkFolderImportListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(BookmarkFolderImportBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getBookmarkFolderImportService().updateBookmarkFolderImport(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("KeywordCrowdTally".equals(objectType)) {
                        KeywordCrowdTallyStub stub = KeywordCrowdTallyStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        KeywordCrowdTallyBean bean = KeywordCrowdTallyResourceImpl.convertKeywordCrowdTallyStubToBean(stub);
                        boolean suc = ServiceManager.getKeywordCrowdTallyService().updateKeywordCrowdTally(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }                        
                    } else if("KeywordCrowdTallyList".equals(objectType)) {
                        KeywordCrowdTallyListStub listStub = KeywordCrowdTallyListStub.fromJsonString(sb.toString());
                        List<KeywordCrowdTallyBean> beanList = KeywordCrowdTallyResourceImpl.convertKeywordCrowdTallyListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(KeywordCrowdTallyBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getKeywordCrowdTallyService().updateKeywordCrowdTally(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("BookmarkCrowdTally".equals(objectType)) {
                        BookmarkCrowdTallyStub stub = BookmarkCrowdTallyStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        BookmarkCrowdTallyBean bean = BookmarkCrowdTallyResourceImpl.convertBookmarkCrowdTallyStubToBean(stub);
                        boolean suc = ServiceManager.getBookmarkCrowdTallyService().updateBookmarkCrowdTally(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }                        
                    } else if("BookmarkCrowdTallyList".equals(objectType)) {
                        BookmarkCrowdTallyListStub listStub = BookmarkCrowdTallyListStub.fromJsonString(sb.toString());
                        List<BookmarkCrowdTallyBean> beanList = BookmarkCrowdTallyResourceImpl.convertBookmarkCrowdTallyListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(BookmarkCrowdTallyBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getBookmarkCrowdTallyService().updateBookmarkCrowdTally(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("DomainInfo".equals(objectType)) {
                        DomainInfoStub stub = DomainInfoStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        DomainInfoBean bean = DomainInfoResourceImpl.convertDomainInfoStubToBean(stub);
                        boolean suc = ServiceManager.getDomainInfoService().updateDomainInfo(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }                        
                    } else if("DomainInfoList".equals(objectType)) {
                        DomainInfoListStub listStub = DomainInfoListStub.fromJsonString(sb.toString());
                        List<DomainInfoBean> beanList = DomainInfoResourceImpl.convertDomainInfoListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(DomainInfoBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getDomainInfoService().updateDomainInfo(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("UrlRating".equals(objectType)) {
                        UrlRatingStub stub = UrlRatingStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        UrlRatingBean bean = UrlRatingResourceImpl.convertUrlRatingStubToBean(stub);
                        boolean suc = ServiceManager.getUrlRatingService().updateUrlRating(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }                        
                    } else if("UrlRatingList".equals(objectType)) {
                        UrlRatingListStub listStub = UrlRatingListStub.fromJsonString(sb.toString());
                        List<UrlRatingBean> beanList = UrlRatingResourceImpl.convertUrlRatingListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(UrlRatingBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getUrlRatingService().updateUrlRating(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("UserRating".equals(objectType)) {
                        UserRatingStub stub = UserRatingStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        UserRatingBean bean = UserRatingResourceImpl.convertUserRatingStubToBean(stub);
                        boolean suc = ServiceManager.getUserRatingService().updateUserRating(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }                        
                    } else if("UserRatingList".equals(objectType)) {
                        UserRatingListStub listStub = UserRatingListStub.fromJsonString(sb.toString());
                        List<UserRatingBean> beanList = UserRatingResourceImpl.convertUserRatingListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(UserRatingBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getUserRatingService().updateUserRating(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("AbuseTag".equals(objectType)) {
                        AbuseTagStub stub = AbuseTagStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        AbuseTagBean bean = AbuseTagResourceImpl.convertAbuseTagStubToBean(stub);
                        boolean suc = ServiceManager.getAbuseTagService().updateAbuseTag(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }                        
                    } else if("AbuseTagList".equals(objectType)) {
                        AbuseTagListStub listStub = AbuseTagListStub.fromJsonString(sb.toString());
                        List<AbuseTagBean> beanList = AbuseTagResourceImpl.convertAbuseTagListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(AbuseTagBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getAbuseTagService().updateAbuseTag(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("ServiceInfo".equals(objectType)) {
                        ServiceInfoStub stub = ServiceInfoStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        ServiceInfoBean bean = ServiceInfoResourceImpl.convertServiceInfoStubToBean(stub);
                        boolean suc = ServiceManager.getServiceInfoService().updateServiceInfo(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }                        
                    } else if("ServiceInfoList".equals(objectType)) {
                        ServiceInfoListStub listStub = ServiceInfoListStub.fromJsonString(sb.toString());
                        List<ServiceInfoBean> beanList = ServiceInfoResourceImpl.convertServiceInfoListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(ServiceInfoBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getServiceInfoService().updateServiceInfo(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("FiveTen".equals(objectType)) {
                        FiveTenStub stub = FiveTenStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        FiveTenBean bean = FiveTenResourceImpl.convertFiveTenStubToBean(stub);
                        boolean suc = ServiceManager.getFiveTenService().updateFiveTen(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }                        
                    } else if("FiveTenList".equals(objectType)) {
                        FiveTenListStub listStub = FiveTenListStub.fromJsonString(sb.toString());
                        List<FiveTenBean> beanList = FiveTenResourceImpl.convertFiveTenListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(FiveTenBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getFiveTenService().updateFiveTen(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else {
                        // This cannot happen
                    }
                } catch (FileNotFoundException e) {
                    if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to read a fixture file: filePath = " + filePath, e);
                } catch (IOException e) {
                    if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to read a fixture file: filePath = " + filePath, e);
                } catch (BaseException e) {
                    if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process a fixture file: filePath = " + filePath, e);
                } catch (Exception e) {
                    if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected error while processing a fixture file: filePath = " + filePath, e);
                } finally {
                    if(reader != null) {
                        try {
                            reader.close();
                        } catch (IOException e) {
                            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Error while closing the fixture file: filePath = " + filePath, e);
                        }
                    }
                }                
            } else {
                if(log.isLoggable(Level.WARNING)) log.warning("Skipping a fixture file because it is not readable: filePath = " + filePath);
            }
        }
                
        return count;
    }
    
}
