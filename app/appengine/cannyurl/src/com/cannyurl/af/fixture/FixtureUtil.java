package com.cannyurl.af.fixture;

import java.io.File;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.ApiConsumer;
import com.myurldb.ws.AppCustomDomain;
import com.myurldb.ws.SiteCustomDomain;
import com.myurldb.ws.User;
import com.myurldb.ws.UserUsercode;
import com.myurldb.ws.UserPassword;
import com.myurldb.ws.ExternalUserAuth;
import com.myurldb.ws.UserAuthState;
import com.myurldb.ws.UserResourcePermission;
import com.myurldb.ws.UserResourceProhibition;
import com.myurldb.ws.RolePermission;
import com.myurldb.ws.UserRole;
import com.myurldb.ws.AppClient;
import com.myurldb.ws.ClientUser;
import com.myurldb.ws.UserCustomDomain;
import com.myurldb.ws.ClientSetting;
import com.myurldb.ws.UserSetting;
import com.myurldb.ws.VisitorSetting;
import com.myurldb.ws.TwitterSummaryCard;
import com.myurldb.ws.TwitterPhotoCard;
import com.myurldb.ws.TwitterGalleryCard;
import com.myurldb.ws.TwitterAppCard;
import com.myurldb.ws.TwitterPlayerCard;
import com.myurldb.ws.TwitterProductCard;
import com.myurldb.ws.ShortPassage;
import com.myurldb.ws.ShortLink;
import com.myurldb.ws.GeoLink;
import com.myurldb.ws.QrCode;
import com.myurldb.ws.LinkPassphrase;
import com.myurldb.ws.LinkMessage;
import com.myurldb.ws.LinkAlbum;
import com.myurldb.ws.AlbumShortLink;
import com.myurldb.ws.KeywordFolder;
import com.myurldb.ws.BookmarkFolder;
import com.myurldb.ws.KeywordLink;
import com.myurldb.ws.BookmarkLink;
import com.myurldb.ws.SpeedDial;
import com.myurldb.ws.KeywordFolderImport;
import com.myurldb.ws.BookmarkFolderImport;
import com.myurldb.ws.KeywordCrowdTally;
import com.myurldb.ws.BookmarkCrowdTally;
import com.myurldb.ws.DomainInfo;
import com.myurldb.ws.UrlRating;
import com.myurldb.ws.UserRating;
import com.myurldb.ws.AbuseTag;
import com.myurldb.ws.ServiceInfo;
import com.myurldb.ws.FiveTen;
import com.myurldb.ws.BaseException;


// Note:
// Fixture file name conventions.
// File path may in the form of "/a/b/c/[Type]Yyy.[suf]"
// The [Type] part determins the object type (class),
// whereas the file suffix [suf] determines the media type, xml or json.
// ...
public class FixtureUtil
{
    private static final Logger log = Logger.getLogger(FixtureUtil.class.getName());

    // temporary
    public static final String MEDIA_TYPE_XML = "XML";
    public static final String MEDIA_TYPE_JSON = "JSON";
    // ...
    private static final String DEFAULT_MEDIA_TYPE = MEDIA_TYPE_JSON;
    // ...
    // Fixture dir is, in the current implementation, under "/war" dir. 
    // Using "/war/WEB-INF/classes" (e.g., copied from "src" dir during build) is more secure,
    // but it requires using a class loader (whereas we can just use File API for files under "/war").
    private static final String DEFAULT_FIXTURE_DIR = "fixture";
    private static final String DEFAULT_FIXTURE_FILE = DEFAULT_FIXTURE_DIR + File.separator + "fixture.json";   // ????
    // ...
    private static final boolean DEFAULT_FIXTURE_LOAD = false;
    
    
    private FixtureUtil() {}

    
    public static boolean getDefaultFixtureLoad()
    {
        return DEFAULT_FIXTURE_LOAD;
    }

//    public static String getDefaultObjectType()
//    {
//        // ????
//        return null;
//    }
    
    public static String getDefaultMediaType()
    {
        // ????
        return DEFAULT_MEDIA_TYPE;
    }

    public static String getMediaType(String filePath)
    {
        if(filePath == null || filePath.isEmpty()) {
            return getDefaultMediaType();  // ???
        }
        int idx = filePath.lastIndexOf(".");
        if(idx >= 0) {
            String suffix = filePath.substring(idx + 1);
            if(suffix.equalsIgnoreCase(MEDIA_TYPE_XML)) {
                return MEDIA_TYPE_XML;
            } else if(suffix.equalsIgnoreCase(MEDIA_TYPE_JSON)) {
                return MEDIA_TYPE_JSON;
            } else {
                return getDefaultMediaType();  // ???
            }
        } else {
            return getDefaultMediaType();  // ???            
        }
    }

    // Returns the class name..
    // TBD: Or, return Class ????
    public static String getObjectType(String filePath)
    {
        if(filePath == null || filePath.isEmpty()) {
            return null;   // ???
        }
        int idx1 = filePath.lastIndexOf(File.separator);
        if(idx1 >= 0) {
            filePath = filePath.substring(idx1 + 1);
        }
        int idx2 = filePath.lastIndexOf(".");
        if(idx2 >= 0) {
            filePath = filePath.substring(0, idx2);
        }
        // At this point,
        // "filePath" refers to the root part of the file name. Just reusing the name filePath...

        // temporary
        // TBD: Type names should be sorted and longer type names should be put in front... 
        if(filePath.toLowerCase().startsWith("ApiConsumer".toLowerCase())) {
            String className = ApiConsumer.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("ApiConsumer".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("AppCustomDomain".toLowerCase())) {
            String className = AppCustomDomain.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("AppCustomDomain".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("SiteCustomDomain".toLowerCase())) {
            String className = SiteCustomDomain.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("SiteCustomDomain".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("User".toLowerCase())) {
            String className = User.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("User".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("UserUsercode".toLowerCase())) {
            String className = UserUsercode.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("UserUsercode".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("UserPassword".toLowerCase())) {
            String className = UserPassword.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("UserPassword".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("ExternalUserAuth".toLowerCase())) {
            String className = ExternalUserAuth.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("ExternalUserAuth".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("UserAuthState".toLowerCase())) {
            String className = UserAuthState.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("UserAuthState".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("UserResourcePermission".toLowerCase())) {
            String className = UserResourcePermission.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("UserResourcePermission".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("UserResourceProhibition".toLowerCase())) {
            String className = UserResourceProhibition.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("UserResourceProhibition".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("RolePermission".toLowerCase())) {
            String className = RolePermission.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("RolePermission".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("UserRole".toLowerCase())) {
            String className = UserRole.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("UserRole".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("AppClient".toLowerCase())) {
            String className = AppClient.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("AppClient".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("ClientUser".toLowerCase())) {
            String className = ClientUser.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("ClientUser".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("UserCustomDomain".toLowerCase())) {
            String className = UserCustomDomain.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("UserCustomDomain".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("ClientSetting".toLowerCase())) {
            String className = ClientSetting.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("ClientSetting".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("UserSetting".toLowerCase())) {
            String className = UserSetting.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("UserSetting".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("VisitorSetting".toLowerCase())) {
            String className = VisitorSetting.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("VisitorSetting".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("TwitterSummaryCard".toLowerCase())) {
            String className = TwitterSummaryCard.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("TwitterSummaryCard".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("TwitterPhotoCard".toLowerCase())) {
            String className = TwitterPhotoCard.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("TwitterPhotoCard".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("TwitterGalleryCard".toLowerCase())) {
            String className = TwitterGalleryCard.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("TwitterGalleryCard".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("TwitterAppCard".toLowerCase())) {
            String className = TwitterAppCard.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("TwitterAppCard".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("TwitterPlayerCard".toLowerCase())) {
            String className = TwitterPlayerCard.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("TwitterPlayerCard".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("TwitterProductCard".toLowerCase())) {
            String className = TwitterProductCard.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("TwitterProductCard".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("ShortPassage".toLowerCase())) {
            String className = ShortPassage.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("ShortPassage".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("ShortLink".toLowerCase())) {
            String className = ShortLink.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("ShortLink".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("GeoLink".toLowerCase())) {
            String className = GeoLink.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("GeoLink".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("QrCode".toLowerCase())) {
            String className = QrCode.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("QrCode".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("LinkPassphrase".toLowerCase())) {
            String className = LinkPassphrase.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("LinkPassphrase".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("LinkMessage".toLowerCase())) {
            String className = LinkMessage.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("LinkMessage".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("LinkAlbum".toLowerCase())) {
            String className = LinkAlbum.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("LinkAlbum".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("AlbumShortLink".toLowerCase())) {
            String className = AlbumShortLink.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("AlbumShortLink".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("KeywordFolder".toLowerCase())) {
            String className = KeywordFolder.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("KeywordFolder".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("BookmarkFolder".toLowerCase())) {
            String className = BookmarkFolder.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("BookmarkFolder".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("KeywordLink".toLowerCase())) {
            String className = KeywordLink.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("KeywordLink".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("BookmarkLink".toLowerCase())) {
            String className = BookmarkLink.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("BookmarkLink".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("SpeedDial".toLowerCase())) {
            String className = SpeedDial.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("SpeedDial".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("KeywordFolderImport".toLowerCase())) {
            String className = KeywordFolderImport.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("KeywordFolderImport".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("BookmarkFolderImport".toLowerCase())) {
            String className = BookmarkFolderImport.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("BookmarkFolderImport".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("KeywordCrowdTally".toLowerCase())) {
            String className = KeywordCrowdTally.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("KeywordCrowdTally".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("BookmarkCrowdTally".toLowerCase())) {
            String className = BookmarkCrowdTally.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("BookmarkCrowdTally".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("DomainInfo".toLowerCase())) {
            String className = DomainInfo.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("DomainInfo".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("UrlRating".toLowerCase())) {
            String className = UrlRating.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("UrlRating".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("UserRating".toLowerCase())) {
            String className = UserRating.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("UserRating".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("AbuseTag".toLowerCase())) {
            String className = AbuseTag.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("AbuseTag".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("ServiceInfo".toLowerCase())) {
            String className = ServiceInfo.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("ServiceInfo".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("FiveTen".toLowerCase())) {
            String className = FiveTen.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("FiveTen".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        
        //...
        return null;        
    }

    // TBD
    public static String getDefaultFixtureDir()
    {
        // temporary
        return DEFAULT_FIXTURE_DIR;
    }
    public static String getDefaultFixtureFile()
    {
        // temporary
        return DEFAULT_FIXTURE_FILE;
    }

}
