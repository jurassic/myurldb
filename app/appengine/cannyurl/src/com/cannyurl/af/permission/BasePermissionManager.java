package com.cannyurl.af.permission;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.permission.ApiConsumerBasePermission;
import com.myurldb.ws.permission.AppCustomDomainBasePermission;
import com.myurldb.ws.permission.SiteCustomDomainBasePermission;
import com.myurldb.ws.permission.UserBasePermission;
import com.myurldb.ws.permission.UserUsercodeBasePermission;
import com.myurldb.ws.permission.UserPasswordBasePermission;
import com.myurldb.ws.permission.ExternalUserAuthBasePermission;
import com.myurldb.ws.permission.UserAuthStateBasePermission;
import com.myurldb.ws.permission.UserResourcePermissionBasePermission;
import com.myurldb.ws.permission.UserResourceProhibitionBasePermission;
import com.myurldb.ws.permission.RolePermissionBasePermission;
import com.myurldb.ws.permission.UserRoleBasePermission;
import com.myurldb.ws.permission.AppClientBasePermission;
import com.myurldb.ws.permission.ClientUserBasePermission;
import com.myurldb.ws.permission.UserCustomDomainBasePermission;
import com.myurldb.ws.permission.ClientSettingBasePermission;
import com.myurldb.ws.permission.UserSettingBasePermission;
import com.myurldb.ws.permission.VisitorSettingBasePermission;
import com.myurldb.ws.permission.TwitterSummaryCardBasePermission;
import com.myurldb.ws.permission.TwitterPhotoCardBasePermission;
import com.myurldb.ws.permission.TwitterGalleryCardBasePermission;
import com.myurldb.ws.permission.TwitterAppCardBasePermission;
import com.myurldb.ws.permission.TwitterPlayerCardBasePermission;
import com.myurldb.ws.permission.TwitterProductCardBasePermission;
import com.myurldb.ws.permission.ShortPassageBasePermission;
import com.myurldb.ws.permission.ShortLinkBasePermission;
import com.myurldb.ws.permission.GeoLinkBasePermission;
import com.myurldb.ws.permission.QrCodeBasePermission;
import com.myurldb.ws.permission.LinkPassphraseBasePermission;
import com.myurldb.ws.permission.LinkMessageBasePermission;
import com.myurldb.ws.permission.LinkAlbumBasePermission;
import com.myurldb.ws.permission.AlbumShortLinkBasePermission;
import com.myurldb.ws.permission.KeywordFolderBasePermission;
import com.myurldb.ws.permission.BookmarkFolderBasePermission;
import com.myurldb.ws.permission.KeywordLinkBasePermission;
import com.myurldb.ws.permission.BookmarkLinkBasePermission;
import com.myurldb.ws.permission.SpeedDialBasePermission;
import com.myurldb.ws.permission.KeywordFolderImportBasePermission;
import com.myurldb.ws.permission.BookmarkFolderImportBasePermission;
import com.myurldb.ws.permission.KeywordCrowdTallyBasePermission;
import com.myurldb.ws.permission.BookmarkCrowdTallyBasePermission;
import com.myurldb.ws.permission.DomainInfoBasePermission;
import com.myurldb.ws.permission.UrlRatingBasePermission;
import com.myurldb.ws.permission.UserRatingBasePermission;
import com.myurldb.ws.permission.AbuseTagBasePermission;
import com.myurldb.ws.permission.ServiceInfoBasePermission;
import com.myurldb.ws.permission.FiveTenBasePermission;


// TBD:
public class BasePermissionManager
{
    private static final Logger log = Logger.getLogger(BasePermissionManager.class.getName());

    private ApiConsumerBasePermission apiConsumerPermission = null;
    private AppCustomDomainBasePermission appCustomDomainPermission = null;
    private SiteCustomDomainBasePermission siteCustomDomainPermission = null;
    private UserBasePermission userPermission = null;
    private UserUsercodeBasePermission userUsercodePermission = null;
    private UserPasswordBasePermission userPasswordPermission = null;
    private ExternalUserAuthBasePermission externalUserAuthPermission = null;
    private UserAuthStateBasePermission userAuthStatePermission = null;
    private UserResourcePermissionBasePermission userResourcePermissionPermission = null;
    private UserResourceProhibitionBasePermission userResourceProhibitionPermission = null;
    private RolePermissionBasePermission rolePermissionPermission = null;
    private UserRoleBasePermission userRolePermission = null;
    private AppClientBasePermission appClientPermission = null;
    private ClientUserBasePermission clientUserPermission = null;
    private UserCustomDomainBasePermission userCustomDomainPermission = null;
    private ClientSettingBasePermission clientSettingPermission = null;
    private UserSettingBasePermission userSettingPermission = null;
    private VisitorSettingBasePermission visitorSettingPermission = null;
    private TwitterSummaryCardBasePermission twitterSummaryCardPermission = null;
    private TwitterPhotoCardBasePermission twitterPhotoCardPermission = null;
    private TwitterGalleryCardBasePermission twitterGalleryCardPermission = null;
    private TwitterAppCardBasePermission twitterAppCardPermission = null;
    private TwitterPlayerCardBasePermission twitterPlayerCardPermission = null;
    private TwitterProductCardBasePermission twitterProductCardPermission = null;
    private ShortPassageBasePermission shortPassagePermission = null;
    private ShortLinkBasePermission shortLinkPermission = null;
    private GeoLinkBasePermission geoLinkPermission = null;
    private QrCodeBasePermission qrCodePermission = null;
    private LinkPassphraseBasePermission linkPassphrasePermission = null;
    private LinkMessageBasePermission linkMessagePermission = null;
    private LinkAlbumBasePermission linkAlbumPermission = null;
    private AlbumShortLinkBasePermission albumShortLinkPermission = null;
    private KeywordFolderBasePermission keywordFolderPermission = null;
    private BookmarkFolderBasePermission bookmarkFolderPermission = null;
    private KeywordLinkBasePermission keywordLinkPermission = null;
    private BookmarkLinkBasePermission bookmarkLinkPermission = null;
    private SpeedDialBasePermission speedDialPermission = null;
    private KeywordFolderImportBasePermission keywordFolderImportPermission = null;
    private BookmarkFolderImportBasePermission bookmarkFolderImportPermission = null;
    private KeywordCrowdTallyBasePermission keywordCrowdTallyPermission = null;
    private BookmarkCrowdTallyBasePermission bookmarkCrowdTallyPermission = null;
    private DomainInfoBasePermission domainInfoPermission = null;
    private UrlRatingBasePermission urlRatingPermission = null;
    private UserRatingBasePermission userRatingPermission = null;
    private AbuseTagBasePermission abuseTagPermission = null;
    private ServiceInfoBasePermission serviceInfoPermission = null;
    private FiveTenBasePermission fiveTenPermission = null;

    // Ctor.
    public BasePermissionManager()
    {
        // TBD:
    }

	public ApiConsumerBasePermission getApiConsumerPermission() 
    {
        if(apiConsumerPermission == null) {
            apiConsumerPermission = new ApiConsumerBasePermission();
        }
        return apiConsumerPermission;
    }
	public void setApiConsumerPermission(ApiConsumerBasePermission apiConsumerPermission) 
    {
        this.apiConsumerPermission = apiConsumerPermission;
    }

	public AppCustomDomainBasePermission getAppCustomDomainPermission() 
    {
        if(appCustomDomainPermission == null) {
            appCustomDomainPermission = new AppCustomDomainBasePermission();
        }
        return appCustomDomainPermission;
    }
	public void setAppCustomDomainPermission(AppCustomDomainBasePermission appCustomDomainPermission) 
    {
        this.appCustomDomainPermission = appCustomDomainPermission;
    }

	public SiteCustomDomainBasePermission getSiteCustomDomainPermission() 
    {
        if(siteCustomDomainPermission == null) {
            siteCustomDomainPermission = new SiteCustomDomainBasePermission();
        }
        return siteCustomDomainPermission;
    }
	public void setSiteCustomDomainPermission(SiteCustomDomainBasePermission siteCustomDomainPermission) 
    {
        this.siteCustomDomainPermission = siteCustomDomainPermission;
    }

	public UserBasePermission getUserPermission() 
    {
        if(userPermission == null) {
            userPermission = new UserBasePermission();
        }
        return userPermission;
    }
	public void setUserPermission(UserBasePermission userPermission) 
    {
        this.userPermission = userPermission;
    }

	public UserUsercodeBasePermission getUserUsercodePermission() 
    {
        if(userUsercodePermission == null) {
            userUsercodePermission = new UserUsercodeBasePermission();
        }
        return userUsercodePermission;
    }
	public void setUserUsercodePermission(UserUsercodeBasePermission userUsercodePermission) 
    {
        this.userUsercodePermission = userUsercodePermission;
    }

	public UserPasswordBasePermission getUserPasswordPermission() 
    {
        if(userPasswordPermission == null) {
            userPasswordPermission = new UserPasswordBasePermission();
        }
        return userPasswordPermission;
    }
	public void setUserPasswordPermission(UserPasswordBasePermission userPasswordPermission) 
    {
        this.userPasswordPermission = userPasswordPermission;
    }

	public ExternalUserAuthBasePermission getExternalUserAuthPermission() 
    {
        if(externalUserAuthPermission == null) {
            externalUserAuthPermission = new ExternalUserAuthBasePermission();
        }
        return externalUserAuthPermission;
    }
	public void setExternalUserAuthPermission(ExternalUserAuthBasePermission externalUserAuthPermission) 
    {
        this.externalUserAuthPermission = externalUserAuthPermission;
    }

	public UserAuthStateBasePermission getUserAuthStatePermission() 
    {
        if(userAuthStatePermission == null) {
            userAuthStatePermission = new UserAuthStateBasePermission();
        }
        return userAuthStatePermission;
    }
	public void setUserAuthStatePermission(UserAuthStateBasePermission userAuthStatePermission) 
    {
        this.userAuthStatePermission = userAuthStatePermission;
    }

	public UserResourcePermissionBasePermission getUserResourcePermissionPermission() 
    {
        if(userResourcePermissionPermission == null) {
            userResourcePermissionPermission = new UserResourcePermissionBasePermission();
        }
        return userResourcePermissionPermission;
    }
	public void setUserResourcePermissionPermission(UserResourcePermissionBasePermission userResourcePermissionPermission) 
    {
        this.userResourcePermissionPermission = userResourcePermissionPermission;
    }

	public UserResourceProhibitionBasePermission getUserResourceProhibitionPermission() 
    {
        if(userResourceProhibitionPermission == null) {
            userResourceProhibitionPermission = new UserResourceProhibitionBasePermission();
        }
        return userResourceProhibitionPermission;
    }
	public void setUserResourceProhibitionPermission(UserResourceProhibitionBasePermission userResourceProhibitionPermission) 
    {
        this.userResourceProhibitionPermission = userResourceProhibitionPermission;
    }

	public RolePermissionBasePermission getRolePermissionPermission() 
    {
        if(rolePermissionPermission == null) {
            rolePermissionPermission = new RolePermissionBasePermission();
        }
        return rolePermissionPermission;
    }
	public void setRolePermissionPermission(RolePermissionBasePermission rolePermissionPermission) 
    {
        this.rolePermissionPermission = rolePermissionPermission;
    }

	public UserRoleBasePermission getUserRolePermission() 
    {
        if(userRolePermission == null) {
            userRolePermission = new UserRoleBasePermission();
        }
        return userRolePermission;
    }
	public void setUserRolePermission(UserRoleBasePermission userRolePermission) 
    {
        this.userRolePermission = userRolePermission;
    }

	public AppClientBasePermission getAppClientPermission() 
    {
        if(appClientPermission == null) {
            appClientPermission = new AppClientBasePermission();
        }
        return appClientPermission;
    }
	public void setAppClientPermission(AppClientBasePermission appClientPermission) 
    {
        this.appClientPermission = appClientPermission;
    }

	public ClientUserBasePermission getClientUserPermission() 
    {
        if(clientUserPermission == null) {
            clientUserPermission = new ClientUserBasePermission();
        }
        return clientUserPermission;
    }
	public void setClientUserPermission(ClientUserBasePermission clientUserPermission) 
    {
        this.clientUserPermission = clientUserPermission;
    }

	public UserCustomDomainBasePermission getUserCustomDomainPermission() 
    {
        if(userCustomDomainPermission == null) {
            userCustomDomainPermission = new UserCustomDomainBasePermission();
        }
        return userCustomDomainPermission;
    }
	public void setUserCustomDomainPermission(UserCustomDomainBasePermission userCustomDomainPermission) 
    {
        this.userCustomDomainPermission = userCustomDomainPermission;
    }

	public ClientSettingBasePermission getClientSettingPermission() 
    {
        if(clientSettingPermission == null) {
            clientSettingPermission = new ClientSettingBasePermission();
        }
        return clientSettingPermission;
    }
	public void setClientSettingPermission(ClientSettingBasePermission clientSettingPermission) 
    {
        this.clientSettingPermission = clientSettingPermission;
    }

	public UserSettingBasePermission getUserSettingPermission() 
    {
        if(userSettingPermission == null) {
            userSettingPermission = new UserSettingBasePermission();
        }
        return userSettingPermission;
    }
	public void setUserSettingPermission(UserSettingBasePermission userSettingPermission) 
    {
        this.userSettingPermission = userSettingPermission;
    }

	public VisitorSettingBasePermission getVisitorSettingPermission() 
    {
        if(visitorSettingPermission == null) {
            visitorSettingPermission = new VisitorSettingBasePermission();
        }
        return visitorSettingPermission;
    }
	public void setVisitorSettingPermission(VisitorSettingBasePermission visitorSettingPermission) 
    {
        this.visitorSettingPermission = visitorSettingPermission;
    }

	public TwitterSummaryCardBasePermission getTwitterSummaryCardPermission() 
    {
        if(twitterSummaryCardPermission == null) {
            twitterSummaryCardPermission = new TwitterSummaryCardBasePermission();
        }
        return twitterSummaryCardPermission;
    }
	public void setTwitterSummaryCardPermission(TwitterSummaryCardBasePermission twitterSummaryCardPermission) 
    {
        this.twitterSummaryCardPermission = twitterSummaryCardPermission;
    }

	public TwitterPhotoCardBasePermission getTwitterPhotoCardPermission() 
    {
        if(twitterPhotoCardPermission == null) {
            twitterPhotoCardPermission = new TwitterPhotoCardBasePermission();
        }
        return twitterPhotoCardPermission;
    }
	public void setTwitterPhotoCardPermission(TwitterPhotoCardBasePermission twitterPhotoCardPermission) 
    {
        this.twitterPhotoCardPermission = twitterPhotoCardPermission;
    }

	public TwitterGalleryCardBasePermission getTwitterGalleryCardPermission() 
    {
        if(twitterGalleryCardPermission == null) {
            twitterGalleryCardPermission = new TwitterGalleryCardBasePermission();
        }
        return twitterGalleryCardPermission;
    }
	public void setTwitterGalleryCardPermission(TwitterGalleryCardBasePermission twitterGalleryCardPermission) 
    {
        this.twitterGalleryCardPermission = twitterGalleryCardPermission;
    }

	public TwitterAppCardBasePermission getTwitterAppCardPermission() 
    {
        if(twitterAppCardPermission == null) {
            twitterAppCardPermission = new TwitterAppCardBasePermission();
        }
        return twitterAppCardPermission;
    }
	public void setTwitterAppCardPermission(TwitterAppCardBasePermission twitterAppCardPermission) 
    {
        this.twitterAppCardPermission = twitterAppCardPermission;
    }

	public TwitterPlayerCardBasePermission getTwitterPlayerCardPermission() 
    {
        if(twitterPlayerCardPermission == null) {
            twitterPlayerCardPermission = new TwitterPlayerCardBasePermission();
        }
        return twitterPlayerCardPermission;
    }
	public void setTwitterPlayerCardPermission(TwitterPlayerCardBasePermission twitterPlayerCardPermission) 
    {
        this.twitterPlayerCardPermission = twitterPlayerCardPermission;
    }

	public TwitterProductCardBasePermission getTwitterProductCardPermission() 
    {
        if(twitterProductCardPermission == null) {
            twitterProductCardPermission = new TwitterProductCardBasePermission();
        }
        return twitterProductCardPermission;
    }
	public void setTwitterProductCardPermission(TwitterProductCardBasePermission twitterProductCardPermission) 
    {
        this.twitterProductCardPermission = twitterProductCardPermission;
    }

	public ShortPassageBasePermission getShortPassagePermission() 
    {
        if(shortPassagePermission == null) {
            shortPassagePermission = new ShortPassageBasePermission();
        }
        return shortPassagePermission;
    }
	public void setShortPassagePermission(ShortPassageBasePermission shortPassagePermission) 
    {
        this.shortPassagePermission = shortPassagePermission;
    }

	public ShortLinkBasePermission getShortLinkPermission() 
    {
        if(shortLinkPermission == null) {
            shortLinkPermission = new ShortLinkBasePermission();
        }
        return shortLinkPermission;
    }
	public void setShortLinkPermission(ShortLinkBasePermission shortLinkPermission) 
    {
        this.shortLinkPermission = shortLinkPermission;
    }

	public GeoLinkBasePermission getGeoLinkPermission() 
    {
        if(geoLinkPermission == null) {
            geoLinkPermission = new GeoLinkBasePermission();
        }
        return geoLinkPermission;
    }
	public void setGeoLinkPermission(GeoLinkBasePermission geoLinkPermission) 
    {
        this.geoLinkPermission = geoLinkPermission;
    }

	public QrCodeBasePermission getQrCodePermission() 
    {
        if(qrCodePermission == null) {
            qrCodePermission = new QrCodeBasePermission();
        }
        return qrCodePermission;
    }
	public void setQrCodePermission(QrCodeBasePermission qrCodePermission) 
    {
        this.qrCodePermission = qrCodePermission;
    }

	public LinkPassphraseBasePermission getLinkPassphrasePermission() 
    {
        if(linkPassphrasePermission == null) {
            linkPassphrasePermission = new LinkPassphraseBasePermission();
        }
        return linkPassphrasePermission;
    }
	public void setLinkPassphrasePermission(LinkPassphraseBasePermission linkPassphrasePermission) 
    {
        this.linkPassphrasePermission = linkPassphrasePermission;
    }

	public LinkMessageBasePermission getLinkMessagePermission() 
    {
        if(linkMessagePermission == null) {
            linkMessagePermission = new LinkMessageBasePermission();
        }
        return linkMessagePermission;
    }
	public void setLinkMessagePermission(LinkMessageBasePermission linkMessagePermission) 
    {
        this.linkMessagePermission = linkMessagePermission;
    }

	public LinkAlbumBasePermission getLinkAlbumPermission() 
    {
        if(linkAlbumPermission == null) {
            linkAlbumPermission = new LinkAlbumBasePermission();
        }
        return linkAlbumPermission;
    }
	public void setLinkAlbumPermission(LinkAlbumBasePermission linkAlbumPermission) 
    {
        this.linkAlbumPermission = linkAlbumPermission;
    }

	public AlbumShortLinkBasePermission getAlbumShortLinkPermission() 
    {
        if(albumShortLinkPermission == null) {
            albumShortLinkPermission = new AlbumShortLinkBasePermission();
        }
        return albumShortLinkPermission;
    }
	public void setAlbumShortLinkPermission(AlbumShortLinkBasePermission albumShortLinkPermission) 
    {
        this.albumShortLinkPermission = albumShortLinkPermission;
    }

	public KeywordFolderBasePermission getKeywordFolderPermission() 
    {
        if(keywordFolderPermission == null) {
            keywordFolderPermission = new KeywordFolderBasePermission();
        }
        return keywordFolderPermission;
    }
	public void setKeywordFolderPermission(KeywordFolderBasePermission keywordFolderPermission) 
    {
        this.keywordFolderPermission = keywordFolderPermission;
    }

	public BookmarkFolderBasePermission getBookmarkFolderPermission() 
    {
        if(bookmarkFolderPermission == null) {
            bookmarkFolderPermission = new BookmarkFolderBasePermission();
        }
        return bookmarkFolderPermission;
    }
	public void setBookmarkFolderPermission(BookmarkFolderBasePermission bookmarkFolderPermission) 
    {
        this.bookmarkFolderPermission = bookmarkFolderPermission;
    }

	public KeywordLinkBasePermission getKeywordLinkPermission() 
    {
        if(keywordLinkPermission == null) {
            keywordLinkPermission = new KeywordLinkBasePermission();
        }
        return keywordLinkPermission;
    }
	public void setKeywordLinkPermission(KeywordLinkBasePermission keywordLinkPermission) 
    {
        this.keywordLinkPermission = keywordLinkPermission;
    }

	public BookmarkLinkBasePermission getBookmarkLinkPermission() 
    {
        if(bookmarkLinkPermission == null) {
            bookmarkLinkPermission = new BookmarkLinkBasePermission();
        }
        return bookmarkLinkPermission;
    }
	public void setBookmarkLinkPermission(BookmarkLinkBasePermission bookmarkLinkPermission) 
    {
        this.bookmarkLinkPermission = bookmarkLinkPermission;
    }

	public SpeedDialBasePermission getSpeedDialPermission() 
    {
        if(speedDialPermission == null) {
            speedDialPermission = new SpeedDialBasePermission();
        }
        return speedDialPermission;
    }
	public void setSpeedDialPermission(SpeedDialBasePermission speedDialPermission) 
    {
        this.speedDialPermission = speedDialPermission;
    }

	public KeywordFolderImportBasePermission getKeywordFolderImportPermission() 
    {
        if(keywordFolderImportPermission == null) {
            keywordFolderImportPermission = new KeywordFolderImportBasePermission();
        }
        return keywordFolderImportPermission;
    }
	public void setKeywordFolderImportPermission(KeywordFolderImportBasePermission keywordFolderImportPermission) 
    {
        this.keywordFolderImportPermission = keywordFolderImportPermission;
    }

	public BookmarkFolderImportBasePermission getBookmarkFolderImportPermission() 
    {
        if(bookmarkFolderImportPermission == null) {
            bookmarkFolderImportPermission = new BookmarkFolderImportBasePermission();
        }
        return bookmarkFolderImportPermission;
    }
	public void setBookmarkFolderImportPermission(BookmarkFolderImportBasePermission bookmarkFolderImportPermission) 
    {
        this.bookmarkFolderImportPermission = bookmarkFolderImportPermission;
    }

	public KeywordCrowdTallyBasePermission getKeywordCrowdTallyPermission() 
    {
        if(keywordCrowdTallyPermission == null) {
            keywordCrowdTallyPermission = new KeywordCrowdTallyBasePermission();
        }
        return keywordCrowdTallyPermission;
    }
	public void setKeywordCrowdTallyPermission(KeywordCrowdTallyBasePermission keywordCrowdTallyPermission) 
    {
        this.keywordCrowdTallyPermission = keywordCrowdTallyPermission;
    }

	public BookmarkCrowdTallyBasePermission getBookmarkCrowdTallyPermission() 
    {
        if(bookmarkCrowdTallyPermission == null) {
            bookmarkCrowdTallyPermission = new BookmarkCrowdTallyBasePermission();
        }
        return bookmarkCrowdTallyPermission;
    }
	public void setBookmarkCrowdTallyPermission(BookmarkCrowdTallyBasePermission bookmarkCrowdTallyPermission) 
    {
        this.bookmarkCrowdTallyPermission = bookmarkCrowdTallyPermission;
    }

	public DomainInfoBasePermission getDomainInfoPermission() 
    {
        if(domainInfoPermission == null) {
            domainInfoPermission = new DomainInfoBasePermission();
        }
        return domainInfoPermission;
    }
	public void setDomainInfoPermission(DomainInfoBasePermission domainInfoPermission) 
    {
        this.domainInfoPermission = domainInfoPermission;
    }

	public UrlRatingBasePermission getUrlRatingPermission() 
    {
        if(urlRatingPermission == null) {
            urlRatingPermission = new UrlRatingBasePermission();
        }
        return urlRatingPermission;
    }
	public void setUrlRatingPermission(UrlRatingBasePermission urlRatingPermission) 
    {
        this.urlRatingPermission = urlRatingPermission;
    }

	public UserRatingBasePermission getUserRatingPermission() 
    {
        if(userRatingPermission == null) {
            userRatingPermission = new UserRatingBasePermission();
        }
        return userRatingPermission;
    }
	public void setUserRatingPermission(UserRatingBasePermission userRatingPermission) 
    {
        this.userRatingPermission = userRatingPermission;
    }

	public AbuseTagBasePermission getAbuseTagPermission() 
    {
        if(abuseTagPermission == null) {
            abuseTagPermission = new AbuseTagBasePermission();
        }
        return abuseTagPermission;
    }
	public void setAbuseTagPermission(AbuseTagBasePermission abuseTagPermission) 
    {
        this.abuseTagPermission = abuseTagPermission;
    }

	public ServiceInfoBasePermission getServiceInfoPermission() 
    {
        if(serviceInfoPermission == null) {
            serviceInfoPermission = new ServiceInfoBasePermission();
        }
        return serviceInfoPermission;
    }
	public void setServiceInfoPermission(ServiceInfoBasePermission serviceInfoPermission) 
    {
        this.serviceInfoPermission = serviceInfoPermission;
    }

	public FiveTenBasePermission getFiveTenPermission() 
    {
        if(fiveTenPermission == null) {
            fiveTenPermission = new FiveTenBasePermission();
        }
        return fiveTenPermission;
    }
	public void setFiveTenPermission(FiveTenBasePermission fiveTenPermission) 
    {
        this.fiveTenPermission = fiveTenPermission;
    }


	public boolean isPermissionRequired(String resource, String action) 
    {
        if(resource == null || resource.isEmpty()) {
            return false;   // ???
        } else if(resource.equals("ApiConsumer")) {
            return getApiConsumerPermission().isPermissionRequired(action);
        } else if(resource.equals("AppCustomDomain")) {
            return getAppCustomDomainPermission().isPermissionRequired(action);
        } else if(resource.equals("SiteCustomDomain")) {
            return getSiteCustomDomainPermission().isPermissionRequired(action);
        } else if(resource.equals("User")) {
            return getUserPermission().isPermissionRequired(action);
        } else if(resource.equals("UserUsercode")) {
            return getUserUsercodePermission().isPermissionRequired(action);
        } else if(resource.equals("UserPassword")) {
            return getUserPasswordPermission().isPermissionRequired(action);
        } else if(resource.equals("ExternalUserAuth")) {
            return getExternalUserAuthPermission().isPermissionRequired(action);
        } else if(resource.equals("UserAuthState")) {
            return getUserAuthStatePermission().isPermissionRequired(action);
        } else if(resource.equals("UserResourcePermission")) {
            return getUserResourcePermissionPermission().isPermissionRequired(action);
        } else if(resource.equals("UserResourceProhibition")) {
            return getUserResourceProhibitionPermission().isPermissionRequired(action);
        } else if(resource.equals("RolePermission")) {
            return getRolePermissionPermission().isPermissionRequired(action);
        } else if(resource.equals("UserRole")) {
            return getUserRolePermission().isPermissionRequired(action);
        } else if(resource.equals("AppClient")) {
            return getAppClientPermission().isPermissionRequired(action);
        } else if(resource.equals("ClientUser")) {
            return getClientUserPermission().isPermissionRequired(action);
        } else if(resource.equals("UserCustomDomain")) {
            return getUserCustomDomainPermission().isPermissionRequired(action);
        } else if(resource.equals("ClientSetting")) {
            return getClientSettingPermission().isPermissionRequired(action);
        } else if(resource.equals("UserSetting")) {
            return getUserSettingPermission().isPermissionRequired(action);
        } else if(resource.equals("VisitorSetting")) {
            return getVisitorSettingPermission().isPermissionRequired(action);
        } else if(resource.equals("TwitterSummaryCard")) {
            return getTwitterSummaryCardPermission().isPermissionRequired(action);
        } else if(resource.equals("TwitterPhotoCard")) {
            return getTwitterPhotoCardPermission().isPermissionRequired(action);
        } else if(resource.equals("TwitterGalleryCard")) {
            return getTwitterGalleryCardPermission().isPermissionRequired(action);
        } else if(resource.equals("TwitterAppCard")) {
            return getTwitterAppCardPermission().isPermissionRequired(action);
        } else if(resource.equals("TwitterPlayerCard")) {
            return getTwitterPlayerCardPermission().isPermissionRequired(action);
        } else if(resource.equals("TwitterProductCard")) {
            return getTwitterProductCardPermission().isPermissionRequired(action);
        } else if(resource.equals("ShortPassage")) {
            return getShortPassagePermission().isPermissionRequired(action);
        } else if(resource.equals("ShortLink")) {
            return getShortLinkPermission().isPermissionRequired(action);
        } else if(resource.equals("GeoLink")) {
            return getGeoLinkPermission().isPermissionRequired(action);
        } else if(resource.equals("QrCode")) {
            return getQrCodePermission().isPermissionRequired(action);
        } else if(resource.equals("LinkPassphrase")) {
            return getLinkPassphrasePermission().isPermissionRequired(action);
        } else if(resource.equals("LinkMessage")) {
            return getLinkMessagePermission().isPermissionRequired(action);
        } else if(resource.equals("LinkAlbum")) {
            return getLinkAlbumPermission().isPermissionRequired(action);
        } else if(resource.equals("AlbumShortLink")) {
            return getAlbumShortLinkPermission().isPermissionRequired(action);
        } else if(resource.equals("KeywordFolder")) {
            return getKeywordFolderPermission().isPermissionRequired(action);
        } else if(resource.equals("BookmarkFolder")) {
            return getBookmarkFolderPermission().isPermissionRequired(action);
        } else if(resource.equals("KeywordLink")) {
            return getKeywordLinkPermission().isPermissionRequired(action);
        } else if(resource.equals("BookmarkLink")) {
            return getBookmarkLinkPermission().isPermissionRequired(action);
        } else if(resource.equals("SpeedDial")) {
            return getSpeedDialPermission().isPermissionRequired(action);
        } else if(resource.equals("KeywordFolderImport")) {
            return getKeywordFolderImportPermission().isPermissionRequired(action);
        } else if(resource.equals("BookmarkFolderImport")) {
            return getBookmarkFolderImportPermission().isPermissionRequired(action);
        } else if(resource.equals("KeywordCrowdTally")) {
            return getKeywordCrowdTallyPermission().isPermissionRequired(action);
        } else if(resource.equals("BookmarkCrowdTally")) {
            return getBookmarkCrowdTallyPermission().isPermissionRequired(action);
        } else if(resource.equals("DomainInfo")) {
            return getDomainInfoPermission().isPermissionRequired(action);
        } else if(resource.equals("UrlRating")) {
            return getUrlRatingPermission().isPermissionRequired(action);
        } else if(resource.equals("UserRating")) {
            return getUserRatingPermission().isPermissionRequired(action);
        } else if(resource.equals("AbuseTag")) {
            return getAbuseTagPermission().isPermissionRequired(action);
        } else if(resource.equals("ServiceInfo")) {
            return getServiceInfoPermission().isPermissionRequired(action);
        } else if(resource.equals("FiveTen")) {
            return getFiveTenPermission().isPermissionRequired(action);
        } else {
            log.warning("Unrecognized resource = " + resource);
            return true;   // ???
        }
    }

    // TBD: "instance" field is currently ignored...
	public boolean isPermissionRequired(String permissionName) 
    {
        if(permissionName == null || permissionName.isEmpty()) {
            return false;   // ???
        } else if(permissionName.startsWith("ApiConsumer::")) {
            String resource = "ApiConsumer";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("AppCustomDomain::")) {
            String resource = "AppCustomDomain";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("SiteCustomDomain::")) {
            String resource = "SiteCustomDomain";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("User::")) {
            String resource = "User";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("UserUsercode::")) {
            String resource = "UserUsercode";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("UserPassword::")) {
            String resource = "UserPassword";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("ExternalUserAuth::")) {
            String resource = "ExternalUserAuth";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("UserAuthState::")) {
            String resource = "UserAuthState";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("UserResourcePermission::")) {
            String resource = "UserResourcePermission";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("UserResourceProhibition::")) {
            String resource = "UserResourceProhibition";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("RolePermission::")) {
            String resource = "RolePermission";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("UserRole::")) {
            String resource = "UserRole";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("AppClient::")) {
            String resource = "AppClient";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("ClientUser::")) {
            String resource = "ClientUser";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("UserCustomDomain::")) {
            String resource = "UserCustomDomain";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("ClientSetting::")) {
            String resource = "ClientSetting";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("UserSetting::")) {
            String resource = "UserSetting";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("VisitorSetting::")) {
            String resource = "VisitorSetting";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("TwitterSummaryCard::")) {
            String resource = "TwitterSummaryCard";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("TwitterPhotoCard::")) {
            String resource = "TwitterPhotoCard";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("TwitterGalleryCard::")) {
            String resource = "TwitterGalleryCard";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("TwitterAppCard::")) {
            String resource = "TwitterAppCard";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("TwitterPlayerCard::")) {
            String resource = "TwitterPlayerCard";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("TwitterProductCard::")) {
            String resource = "TwitterProductCard";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("ShortPassage::")) {
            String resource = "ShortPassage";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("ShortLink::")) {
            String resource = "ShortLink";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("GeoLink::")) {
            String resource = "GeoLink";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("QrCode::")) {
            String resource = "QrCode";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("LinkPassphrase::")) {
            String resource = "LinkPassphrase";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("LinkMessage::")) {
            String resource = "LinkMessage";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("LinkAlbum::")) {
            String resource = "LinkAlbum";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("AlbumShortLink::")) {
            String resource = "AlbumShortLink";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("KeywordFolder::")) {
            String resource = "KeywordFolder";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("BookmarkFolder::")) {
            String resource = "BookmarkFolder";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("KeywordLink::")) {
            String resource = "KeywordLink";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("BookmarkLink::")) {
            String resource = "BookmarkLink";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("SpeedDial::")) {
            String resource = "SpeedDial";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("KeywordFolderImport::")) {
            String resource = "KeywordFolderImport";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("BookmarkFolderImport::")) {
            String resource = "BookmarkFolderImport";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("KeywordCrowdTally::")) {
            String resource = "KeywordCrowdTally";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("BookmarkCrowdTally::")) {
            String resource = "BookmarkCrowdTally";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("DomainInfo::")) {
            String resource = "DomainInfo";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("UrlRating::")) {
            String resource = "UrlRating";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("UserRating::")) {
            String resource = "UserRating";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("AbuseTag::")) {
            String resource = "AbuseTag";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("ServiceInfo::")) {
            String resource = "ServiceInfo";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("FiveTen::")) {
            String resource = "FiveTen";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else {
            log.warning("Unrecognized permissionName = " + permissionName);
            return true;   // ???
        }
    }

}
