package com.cannyurl.af.proxy.async;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.AlbumShortLink;
import com.myurldb.ws.stub.ErrorStub;
import com.myurldb.ws.stub.AlbumShortLinkStub;
import com.myurldb.ws.stub.AlbumShortLinkListStub;
import com.cannyurl.af.util.MarshalHelper;
import com.cannyurl.af.bean.AlbumShortLinkBean;
import com.myurldb.ws.service.AlbumShortLinkService;
import com.cannyurl.af.proxy.AlbumShortLinkServiceProxy;
import com.cannyurl.af.proxy.remote.RemoteAlbumShortLinkServiceProxy;


// Implements the asynchronous call logic (using GAE task queue).
// GET:             Directly calls the remoteProxy interface.
// POST/PUT/DELETE: Creates a task and puts in a queue.
public class AsyncAlbumShortLinkServiceProxy extends BaseAsyncServiceProxy implements AlbumShortLinkServiceProxy
{
    private static final Logger log = Logger.getLogger(AsyncAlbumShortLinkServiceProxy.class.getName());

    // Actual implementation is delegated to the corresponding remote proxy class.
    private RemoteAlbumShortLinkServiceProxy remoteProxy;

    public AsyncAlbumShortLinkServiceProxy()
    {
        remoteProxy = new RemoteAlbumShortLinkServiceProxy();
    }

    @Override
    public AlbumShortLink getAlbumShortLink(String guid) throws BaseException
    {
        return remoteProxy.getAlbumShortLink(guid);
    }

    @Override
    public Object getAlbumShortLink(String guid, String field) throws BaseException
    {
        return remoteProxy.getAlbumShortLink(guid, field);       
    }

    @Override
    public List<AlbumShortLink> getAlbumShortLinks(List<String> guids) throws BaseException
    {
        return remoteProxy.getAlbumShortLinks(guids);
    }

    @Override
    public List<AlbumShortLink> getAllAlbumShortLinks() throws BaseException
    {
        return getAllAlbumShortLinks(null, null, null);
    }

    @Override
    public List<AlbumShortLink> getAllAlbumShortLinks(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllAlbumShortLinks(ordering, offset, count);
    }

    @Override
    public List<String> getAllAlbumShortLinkKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllAlbumShortLinkKeys(ordering, offset, count);
    }

    @Override
    public List<AlbumShortLink> findAlbumShortLinks(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findAlbumShortLinks(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<AlbumShortLink> findAlbumShortLinks(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findAlbumShortLinks(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findAlbumShortLinkKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findAlbumShortLinkKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return remoteProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createAlbumShortLink(String user, String linkAlbum, String shortLink, String shortUrl, String longUrl, String note, String status) throws BaseException
    {
        AlbumShortLinkBean bean = new AlbumShortLinkBean(null, user, linkAlbum, shortLink, shortUrl, longUrl, note, status);
        return createAlbumShortLink(bean);        
    }

    @Override
    public String createAlbumShortLink(AlbumShortLink albumShortLink) throws BaseException
    {
        log.finer("BEGIN");

        String guid = albumShortLink.getGuid();
        if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
            guid = GUID.generate();
            ((AlbumShortLinkBean) albumShortLink).setGuid(guid);
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsCreateAlbumShortLink-" + guid;
        String taskName = "RsCreateAlbumShortLink-" + guid + "-" + (new Date()).getTime();
        AlbumShortLinkStub stub = MarshalHelper.convertAlbumShortLinkToStub(albumShortLink);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(AlbumShortLinkStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = albumShortLink.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    AlbumShortLinkStub dummyStub = new AlbumShortLinkStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createAlbumShortLink(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "albumShortLinks/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createAlbumShortLink(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "albumShortLinks/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;  // ???
    }

    @Override
    public Boolean updateAlbumShortLink(String guid, String user, String linkAlbum, String shortLink, String shortUrl, String longUrl, String note, String status) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "AlbumShortLink guid is invalid.");
        	throw new BaseException("AlbumShortLink guid is invalid.");
        }
        AlbumShortLinkBean bean = new AlbumShortLinkBean(guid, user, linkAlbum, shortLink, shortUrl, longUrl, note, status);
        return updateAlbumShortLink(bean);        
    }

    @Override
    public Boolean updateAlbumShortLink(AlbumShortLink albumShortLink) throws BaseException
    {
        log.finer("BEGIN");

        String guid = albumShortLink.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "AlbumShortLink object is invalid.");
        	throw new BaseException("AlbumShortLink object is invalid.");
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsUpdateAlbumShortLink-" + guid;
        String taskName = "RsUpdateAlbumShortLink-" + guid + "-" + (new Date()).getTime();
        AlbumShortLinkStub stub = MarshalHelper.convertAlbumShortLinkToStub(albumShortLink);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(AlbumShortLinkStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = albumShortLink.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    AlbumShortLinkStub dummyStub = new AlbumShortLinkStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateAlbumShortLink(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "albumShortLinks/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateAlbumShortLink(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "albumShortLinks/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteAlbumShortLink(String guid) throws BaseException
    {
        log.finer("BEGIN");

        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsDeleteAlbumShortLink-" + guid;
        String taskName = "RsDeleteAlbumShortLink-" + guid + "-" + (new Date()).getTime();
        TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "albumShortLinks/" + guid).method(Method.DELETE).taskName(taskName);
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteAlbumShortLink(AlbumShortLink albumShortLink) throws BaseException
    {
        String guid = albumShortLink.getGuid();
        return deleteAlbumShortLink(guid);
    }

    @Override
    public Long deleteAlbumShortLinks(String filter, String params, List<String> values) throws BaseException
    {
        return remoteProxy.deleteAlbumShortLinks(filter, params, values);
    }

}
