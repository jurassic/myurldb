package com.cannyurl.af.proxy.async;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.AppClient;
import com.myurldb.ws.stub.ErrorStub;
import com.myurldb.ws.stub.AppClientStub;
import com.myurldb.ws.stub.AppClientListStub;
import com.cannyurl.af.util.MarshalHelper;
import com.cannyurl.af.bean.AppClientBean;
import com.myurldb.ws.service.AppClientService;
import com.cannyurl.af.proxy.AppClientServiceProxy;
import com.cannyurl.af.proxy.remote.RemoteAppClientServiceProxy;


// Implements the asynchronous call logic (using GAE task queue).
// GET:             Directly calls the remoteProxy interface.
// POST/PUT/DELETE: Creates a task and puts in a queue.
public class AsyncAppClientServiceProxy extends BaseAsyncServiceProxy implements AppClientServiceProxy
{
    private static final Logger log = Logger.getLogger(AsyncAppClientServiceProxy.class.getName());

    // Actual implementation is delegated to the corresponding remote proxy class.
    private RemoteAppClientServiceProxy remoteProxy;

    public AsyncAppClientServiceProxy()
    {
        remoteProxy = new RemoteAppClientServiceProxy();
    }

    @Override
    public AppClient getAppClient(String guid) throws BaseException
    {
        return remoteProxy.getAppClient(guid);
    }

    @Override
    public Object getAppClient(String guid, String field) throws BaseException
    {
        return remoteProxy.getAppClient(guid, field);       
    }

    @Override
    public List<AppClient> getAppClients(List<String> guids) throws BaseException
    {
        return remoteProxy.getAppClients(guids);
    }

    @Override
    public List<AppClient> getAllAppClients() throws BaseException
    {
        return getAllAppClients(null, null, null);
    }

    @Override
    public List<AppClient> getAllAppClients(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllAppClients(ordering, offset, count);
    }

    @Override
    public List<String> getAllAppClientKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllAppClientKeys(ordering, offset, count);
    }

    @Override
    public List<AppClient> findAppClients(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findAppClients(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<AppClient> findAppClients(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findAppClients(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findAppClientKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findAppClientKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return remoteProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createAppClient(String owner, String admin, String name, String clientId, String clientCode, String rootDomain, String appDomain, Boolean useAppDomain, Boolean enabled, String licenseMode, String status) throws BaseException
    {
        AppClientBean bean = new AppClientBean(null, owner, admin, name, clientId, clientCode, rootDomain, appDomain, useAppDomain, enabled, licenseMode, status);
        return createAppClient(bean);        
    }

    @Override
    public String createAppClient(AppClient appClient) throws BaseException
    {
        log.finer("BEGIN");

        String guid = appClient.getGuid();
        if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
            guid = GUID.generate();
            ((AppClientBean) appClient).setGuid(guid);
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsCreateAppClient-" + guid;
        String taskName = "RsCreateAppClient-" + guid + "-" + (new Date()).getTime();
        AppClientStub stub = MarshalHelper.convertAppClientToStub(appClient);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(AppClientStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = appClient.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    AppClientStub dummyStub = new AppClientStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createAppClient(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "appClients/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createAppClient(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "appClients/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;  // ???
    }

    @Override
    public Boolean updateAppClient(String guid, String owner, String admin, String name, String clientId, String clientCode, String rootDomain, String appDomain, Boolean useAppDomain, Boolean enabled, String licenseMode, String status) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "AppClient guid is invalid.");
        	throw new BaseException("AppClient guid is invalid.");
        }
        AppClientBean bean = new AppClientBean(guid, owner, admin, name, clientId, clientCode, rootDomain, appDomain, useAppDomain, enabled, licenseMode, status);
        return updateAppClient(bean);        
    }

    @Override
    public Boolean updateAppClient(AppClient appClient) throws BaseException
    {
        log.finer("BEGIN");

        String guid = appClient.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "AppClient object is invalid.");
        	throw new BaseException("AppClient object is invalid.");
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsUpdateAppClient-" + guid;
        String taskName = "RsUpdateAppClient-" + guid + "-" + (new Date()).getTime();
        AppClientStub stub = MarshalHelper.convertAppClientToStub(appClient);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(AppClientStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = appClient.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    AppClientStub dummyStub = new AppClientStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateAppClient(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "appClients/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateAppClient(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "appClients/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteAppClient(String guid) throws BaseException
    {
        log.finer("BEGIN");

        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsDeleteAppClient-" + guid;
        String taskName = "RsDeleteAppClient-" + guid + "-" + (new Date()).getTime();
        TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "appClients/" + guid).method(Method.DELETE).taskName(taskName);
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteAppClient(AppClient appClient) throws BaseException
    {
        String guid = appClient.getGuid();
        return deleteAppClient(guid);
    }

    @Override
    public Long deleteAppClients(String filter, String params, List<String> values) throws BaseException
    {
        return remoteProxy.deleteAppClients(filter, params, values);
    }

}
