package com.cannyurl.af.proxy.async;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.AppCustomDomain;
import com.myurldb.ws.stub.ErrorStub;
import com.myurldb.ws.stub.AppCustomDomainStub;
import com.myurldb.ws.stub.AppCustomDomainListStub;
import com.cannyurl.af.util.MarshalHelper;
import com.cannyurl.af.bean.AppCustomDomainBean;
import com.myurldb.ws.service.AppCustomDomainService;
import com.cannyurl.af.proxy.AppCustomDomainServiceProxy;
import com.cannyurl.af.proxy.remote.RemoteAppCustomDomainServiceProxy;


// Implements the asynchronous call logic (using GAE task queue).
// GET:             Directly calls the remoteProxy interface.
// POST/PUT/DELETE: Creates a task and puts in a queue.
public class AsyncAppCustomDomainServiceProxy extends BaseAsyncServiceProxy implements AppCustomDomainServiceProxy
{
    private static final Logger log = Logger.getLogger(AsyncAppCustomDomainServiceProxy.class.getName());

    // Actual implementation is delegated to the corresponding remote proxy class.
    private RemoteAppCustomDomainServiceProxy remoteProxy;

    public AsyncAppCustomDomainServiceProxy()
    {
        remoteProxy = new RemoteAppCustomDomainServiceProxy();
    }

    @Override
    public AppCustomDomain getAppCustomDomain(String guid) throws BaseException
    {
        return remoteProxy.getAppCustomDomain(guid);
    }

    @Override
    public Object getAppCustomDomain(String guid, String field) throws BaseException
    {
        return remoteProxy.getAppCustomDomain(guid, field);       
    }

    @Override
    public List<AppCustomDomain> getAppCustomDomains(List<String> guids) throws BaseException
    {
        return remoteProxy.getAppCustomDomains(guids);
    }

    @Override
    public List<AppCustomDomain> getAllAppCustomDomains() throws BaseException
    {
        return getAllAppCustomDomains(null, null, null);
    }

    @Override
    public List<AppCustomDomain> getAllAppCustomDomains(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllAppCustomDomains(ordering, offset, count);
    }

    @Override
    public List<String> getAllAppCustomDomainKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllAppCustomDomainKeys(ordering, offset, count);
    }

    @Override
    public List<AppCustomDomain> findAppCustomDomains(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findAppCustomDomains(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<AppCustomDomain> findAppCustomDomains(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findAppCustomDomains(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findAppCustomDomainKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findAppCustomDomainKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return remoteProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createAppCustomDomain(String appId, String siteDomain, String domain, Boolean verified, String status, Long verifiedTime) throws BaseException
    {
        AppCustomDomainBean bean = new AppCustomDomainBean(null, appId, siteDomain, domain, verified, status, verifiedTime);
        return createAppCustomDomain(bean);        
    }

    @Override
    public String createAppCustomDomain(AppCustomDomain appCustomDomain) throws BaseException
    {
        log.finer("BEGIN");

        String guid = appCustomDomain.getGuid();
        if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
            guid = GUID.generate();
            ((AppCustomDomainBean) appCustomDomain).setGuid(guid);
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsCreateAppCustomDomain-" + guid;
        String taskName = "RsCreateAppCustomDomain-" + guid + "-" + (new Date()).getTime();
        AppCustomDomainStub stub = MarshalHelper.convertAppCustomDomainToStub(appCustomDomain);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(AppCustomDomainStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = appCustomDomain.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    AppCustomDomainStub dummyStub = new AppCustomDomainStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createAppCustomDomain(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "appCustomDomains/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createAppCustomDomain(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "appCustomDomains/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;  // ???
    }

    @Override
    public Boolean updateAppCustomDomain(String guid, String appId, String siteDomain, String domain, Boolean verified, String status, Long verifiedTime) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "AppCustomDomain guid is invalid.");
        	throw new BaseException("AppCustomDomain guid is invalid.");
        }
        AppCustomDomainBean bean = new AppCustomDomainBean(guid, appId, siteDomain, domain, verified, status, verifiedTime);
        return updateAppCustomDomain(bean);        
    }

    @Override
    public Boolean updateAppCustomDomain(AppCustomDomain appCustomDomain) throws BaseException
    {
        log.finer("BEGIN");

        String guid = appCustomDomain.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "AppCustomDomain object is invalid.");
        	throw new BaseException("AppCustomDomain object is invalid.");
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsUpdateAppCustomDomain-" + guid;
        String taskName = "RsUpdateAppCustomDomain-" + guid + "-" + (new Date()).getTime();
        AppCustomDomainStub stub = MarshalHelper.convertAppCustomDomainToStub(appCustomDomain);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(AppCustomDomainStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = appCustomDomain.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    AppCustomDomainStub dummyStub = new AppCustomDomainStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateAppCustomDomain(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "appCustomDomains/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateAppCustomDomain(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "appCustomDomains/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteAppCustomDomain(String guid) throws BaseException
    {
        log.finer("BEGIN");

        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsDeleteAppCustomDomain-" + guid;
        String taskName = "RsDeleteAppCustomDomain-" + guid + "-" + (new Date()).getTime();
        TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "appCustomDomains/" + guid).method(Method.DELETE).taskName(taskName);
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteAppCustomDomain(AppCustomDomain appCustomDomain) throws BaseException
    {
        String guid = appCustomDomain.getGuid();
        return deleteAppCustomDomain(guid);
    }

    @Override
    public Long deleteAppCustomDomains(String filter, String params, List<String> values) throws BaseException
    {
        return remoteProxy.deleteAppCustomDomains(filter, params, values);
    }

}
