package com.cannyurl.af.proxy.async;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.BookmarkCrowdTally;
import com.myurldb.ws.stub.ErrorStub;
import com.myurldb.ws.stub.BookmarkCrowdTallyStub;
import com.myurldb.ws.stub.BookmarkCrowdTallyListStub;
import com.cannyurl.af.util.MarshalHelper;
import com.cannyurl.af.bean.BookmarkCrowdTallyBean;
import com.myurldb.ws.service.BookmarkCrowdTallyService;
import com.cannyurl.af.proxy.BookmarkCrowdTallyServiceProxy;
import com.cannyurl.af.proxy.remote.RemoteBookmarkCrowdTallyServiceProxy;


// Implements the asynchronous call logic (using GAE task queue).
// GET:             Directly calls the remoteProxy interface.
// POST/PUT/DELETE: Creates a task and puts in a queue.
public class AsyncBookmarkCrowdTallyServiceProxy extends BaseAsyncServiceProxy implements BookmarkCrowdTallyServiceProxy
{
    private static final Logger log = Logger.getLogger(AsyncBookmarkCrowdTallyServiceProxy.class.getName());

    // Actual implementation is delegated to the corresponding remote proxy class.
    private RemoteBookmarkCrowdTallyServiceProxy remoteProxy;

    public AsyncBookmarkCrowdTallyServiceProxy()
    {
        remoteProxy = new RemoteBookmarkCrowdTallyServiceProxy();
    }

    @Override
    public BookmarkCrowdTally getBookmarkCrowdTally(String guid) throws BaseException
    {
        return remoteProxy.getBookmarkCrowdTally(guid);
    }

    @Override
    public Object getBookmarkCrowdTally(String guid, String field) throws BaseException
    {
        return remoteProxy.getBookmarkCrowdTally(guid, field);       
    }

    @Override
    public List<BookmarkCrowdTally> getBookmarkCrowdTallies(List<String> guids) throws BaseException
    {
        return remoteProxy.getBookmarkCrowdTallies(guids);
    }

    @Override
    public List<BookmarkCrowdTally> getAllBookmarkCrowdTallies() throws BaseException
    {
        return getAllBookmarkCrowdTallies(null, null, null);
    }

    @Override
    public List<BookmarkCrowdTally> getAllBookmarkCrowdTallies(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllBookmarkCrowdTallies(ordering, offset, count);
    }

    @Override
    public List<String> getAllBookmarkCrowdTallyKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllBookmarkCrowdTallyKeys(ordering, offset, count);
    }

    @Override
    public List<BookmarkCrowdTally> findBookmarkCrowdTallies(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findBookmarkCrowdTallies(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<BookmarkCrowdTally> findBookmarkCrowdTallies(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findBookmarkCrowdTallies(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findBookmarkCrowdTallyKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findBookmarkCrowdTallyKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return remoteProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createBookmarkCrowdTally(String user, String shortLink, String domain, String token, String longUrl, String shortUrl, String tallyDate, String status, String note, Long expirationTime, String bookmarkFolder, String contentTag, String referenceElement, String elementType, String keywordLink) throws BaseException
    {
        BookmarkCrowdTallyBean bean = new BookmarkCrowdTallyBean(null, user, shortLink, domain, token, longUrl, shortUrl, tallyDate, status, note, expirationTime, bookmarkFolder, contentTag, referenceElement, elementType, keywordLink);
        return createBookmarkCrowdTally(bean);        
    }

    @Override
    public String createBookmarkCrowdTally(BookmarkCrowdTally bookmarkCrowdTally) throws BaseException
    {
        log.finer("BEGIN");

        String guid = bookmarkCrowdTally.getGuid();
        if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
            guid = GUID.generate();
            ((BookmarkCrowdTallyBean) bookmarkCrowdTally).setGuid(guid);
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsCreateBookmarkCrowdTally-" + guid;
        String taskName = "RsCreateBookmarkCrowdTally-" + guid + "-" + (new Date()).getTime();
        BookmarkCrowdTallyStub stub = MarshalHelper.convertBookmarkCrowdTallyToStub(bookmarkCrowdTally);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(BookmarkCrowdTallyStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = bookmarkCrowdTally.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    BookmarkCrowdTallyStub dummyStub = new BookmarkCrowdTallyStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createBookmarkCrowdTally(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "bookmarkCrowdTallies/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createBookmarkCrowdTally(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "bookmarkCrowdTallies/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;  // ???
    }

    @Override
    public Boolean updateBookmarkCrowdTally(String guid, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, String tallyDate, String status, String note, Long expirationTime, String bookmarkFolder, String contentTag, String referenceElement, String elementType, String keywordLink) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "BookmarkCrowdTally guid is invalid.");
        	throw new BaseException("BookmarkCrowdTally guid is invalid.");
        }
        BookmarkCrowdTallyBean bean = new BookmarkCrowdTallyBean(guid, user, shortLink, domain, token, longUrl, shortUrl, tallyDate, status, note, expirationTime, bookmarkFolder, contentTag, referenceElement, elementType, keywordLink);
        return updateBookmarkCrowdTally(bean);        
    }

    @Override
    public Boolean updateBookmarkCrowdTally(BookmarkCrowdTally bookmarkCrowdTally) throws BaseException
    {
        log.finer("BEGIN");

        String guid = bookmarkCrowdTally.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "BookmarkCrowdTally object is invalid.");
        	throw new BaseException("BookmarkCrowdTally object is invalid.");
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsUpdateBookmarkCrowdTally-" + guid;
        String taskName = "RsUpdateBookmarkCrowdTally-" + guid + "-" + (new Date()).getTime();
        BookmarkCrowdTallyStub stub = MarshalHelper.convertBookmarkCrowdTallyToStub(bookmarkCrowdTally);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(BookmarkCrowdTallyStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = bookmarkCrowdTally.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    BookmarkCrowdTallyStub dummyStub = new BookmarkCrowdTallyStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateBookmarkCrowdTally(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "bookmarkCrowdTallies/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateBookmarkCrowdTally(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "bookmarkCrowdTallies/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteBookmarkCrowdTally(String guid) throws BaseException
    {
        log.finer("BEGIN");

        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsDeleteBookmarkCrowdTally-" + guid;
        String taskName = "RsDeleteBookmarkCrowdTally-" + guid + "-" + (new Date()).getTime();
        TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "bookmarkCrowdTallies/" + guid).method(Method.DELETE).taskName(taskName);
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteBookmarkCrowdTally(BookmarkCrowdTally bookmarkCrowdTally) throws BaseException
    {
        String guid = bookmarkCrowdTally.getGuid();
        return deleteBookmarkCrowdTally(guid);
    }

    @Override
    public Long deleteBookmarkCrowdTallies(String filter, String params, List<String> values) throws BaseException
    {
        return remoteProxy.deleteBookmarkCrowdTallies(filter, params, values);
    }

}
