package com.cannyurl.af.proxy.async;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.BookmarkFolderImport;
import com.myurldb.ws.stub.ErrorStub;
import com.myurldb.ws.stub.BookmarkFolderImportStub;
import com.myurldb.ws.stub.BookmarkFolderImportListStub;
import com.cannyurl.af.util.MarshalHelper;
import com.cannyurl.af.bean.BookmarkFolderImportBean;
import com.myurldb.ws.service.BookmarkFolderImportService;
import com.cannyurl.af.proxy.BookmarkFolderImportServiceProxy;
import com.cannyurl.af.proxy.remote.RemoteBookmarkFolderImportServiceProxy;


// Implements the asynchronous call logic (using GAE task queue).
// GET:             Directly calls the remoteProxy interface.
// POST/PUT/DELETE: Creates a task and puts in a queue.
public class AsyncBookmarkFolderImportServiceProxy extends BaseAsyncServiceProxy implements BookmarkFolderImportServiceProxy
{
    private static final Logger log = Logger.getLogger(AsyncBookmarkFolderImportServiceProxy.class.getName());

    // Actual implementation is delegated to the corresponding remote proxy class.
    private RemoteBookmarkFolderImportServiceProxy remoteProxy;

    public AsyncBookmarkFolderImportServiceProxy()
    {
        remoteProxy = new RemoteBookmarkFolderImportServiceProxy();
    }

    @Override
    public BookmarkFolderImport getBookmarkFolderImport(String guid) throws BaseException
    {
        return remoteProxy.getBookmarkFolderImport(guid);
    }

    @Override
    public Object getBookmarkFolderImport(String guid, String field) throws BaseException
    {
        return remoteProxy.getBookmarkFolderImport(guid, field);       
    }

    @Override
    public List<BookmarkFolderImport> getBookmarkFolderImports(List<String> guids) throws BaseException
    {
        return remoteProxy.getBookmarkFolderImports(guids);
    }

    @Override
    public List<BookmarkFolderImport> getAllBookmarkFolderImports() throws BaseException
    {
        return getAllBookmarkFolderImports(null, null, null);
    }

    @Override
    public List<BookmarkFolderImport> getAllBookmarkFolderImports(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllBookmarkFolderImports(ordering, offset, count);
    }

    @Override
    public List<String> getAllBookmarkFolderImportKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllBookmarkFolderImportKeys(ordering, offset, count);
    }

    @Override
    public List<BookmarkFolderImport> findBookmarkFolderImports(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findBookmarkFolderImports(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<BookmarkFolderImport> findBookmarkFolderImports(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findBookmarkFolderImports(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findBookmarkFolderImportKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findBookmarkFolderImportKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return remoteProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createBookmarkFolderImport(String user, Integer precedence, String importType, String importedFolder, String importedFolderUser, String importedFolderTitle, String importedFolderPath, String status, String note, String bookmarkFolder) throws BaseException
    {
        BookmarkFolderImportBean bean = new BookmarkFolderImportBean(null, user, precedence, importType, importedFolder, importedFolderUser, importedFolderTitle, importedFolderPath, status, note, bookmarkFolder);
        return createBookmarkFolderImport(bean);        
    }

    @Override
    public String createBookmarkFolderImport(BookmarkFolderImport bookmarkFolderImport) throws BaseException
    {
        log.finer("BEGIN");

        String guid = bookmarkFolderImport.getGuid();
        if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
            guid = GUID.generate();
            ((BookmarkFolderImportBean) bookmarkFolderImport).setGuid(guid);
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsCreateBookmarkFolderImport-" + guid;
        String taskName = "RsCreateBookmarkFolderImport-" + guid + "-" + (new Date()).getTime();
        BookmarkFolderImportStub stub = MarshalHelper.convertBookmarkFolderImportToStub(bookmarkFolderImport);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(BookmarkFolderImportStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = bookmarkFolderImport.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    BookmarkFolderImportStub dummyStub = new BookmarkFolderImportStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createBookmarkFolderImport(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "bookmarkFolderImports/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createBookmarkFolderImport(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "bookmarkFolderImports/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;  // ???
    }

    @Override
    public Boolean updateBookmarkFolderImport(String guid, String user, Integer precedence, String importType, String importedFolder, String importedFolderUser, String importedFolderTitle, String importedFolderPath, String status, String note, String bookmarkFolder) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "BookmarkFolderImport guid is invalid.");
        	throw new BaseException("BookmarkFolderImport guid is invalid.");
        }
        BookmarkFolderImportBean bean = new BookmarkFolderImportBean(guid, user, precedence, importType, importedFolder, importedFolderUser, importedFolderTitle, importedFolderPath, status, note, bookmarkFolder);
        return updateBookmarkFolderImport(bean);        
    }

    @Override
    public Boolean updateBookmarkFolderImport(BookmarkFolderImport bookmarkFolderImport) throws BaseException
    {
        log.finer("BEGIN");

        String guid = bookmarkFolderImport.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "BookmarkFolderImport object is invalid.");
        	throw new BaseException("BookmarkFolderImport object is invalid.");
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsUpdateBookmarkFolderImport-" + guid;
        String taskName = "RsUpdateBookmarkFolderImport-" + guid + "-" + (new Date()).getTime();
        BookmarkFolderImportStub stub = MarshalHelper.convertBookmarkFolderImportToStub(bookmarkFolderImport);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(BookmarkFolderImportStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = bookmarkFolderImport.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    BookmarkFolderImportStub dummyStub = new BookmarkFolderImportStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateBookmarkFolderImport(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "bookmarkFolderImports/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateBookmarkFolderImport(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "bookmarkFolderImports/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteBookmarkFolderImport(String guid) throws BaseException
    {
        log.finer("BEGIN");

        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsDeleteBookmarkFolderImport-" + guid;
        String taskName = "RsDeleteBookmarkFolderImport-" + guid + "-" + (new Date()).getTime();
        TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "bookmarkFolderImports/" + guid).method(Method.DELETE).taskName(taskName);
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteBookmarkFolderImport(BookmarkFolderImport bookmarkFolderImport) throws BaseException
    {
        String guid = bookmarkFolderImport.getGuid();
        return deleteBookmarkFolderImport(guid);
    }

    @Override
    public Long deleteBookmarkFolderImports(String filter, String params, List<String> values) throws BaseException
    {
        return remoteProxy.deleteBookmarkFolderImports(filter, params, values);
    }

}
