package com.cannyurl.af.proxy.async;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.BookmarkFolder;
import com.myurldb.ws.stub.ErrorStub;
import com.myurldb.ws.stub.BookmarkFolderStub;
import com.myurldb.ws.stub.BookmarkFolderListStub;
import com.cannyurl.af.util.MarshalHelper;
import com.cannyurl.af.bean.BookmarkFolderBean;
import com.myurldb.ws.service.BookmarkFolderService;
import com.cannyurl.af.proxy.BookmarkFolderServiceProxy;
import com.cannyurl.af.proxy.remote.RemoteBookmarkFolderServiceProxy;


// Implements the asynchronous call logic (using GAE task queue).
// GET:             Directly calls the remoteProxy interface.
// POST/PUT/DELETE: Creates a task and puts in a queue.
public class AsyncBookmarkFolderServiceProxy extends BaseAsyncServiceProxy implements BookmarkFolderServiceProxy
{
    private static final Logger log = Logger.getLogger(AsyncBookmarkFolderServiceProxy.class.getName());

    // Actual implementation is delegated to the corresponding remote proxy class.
    private RemoteBookmarkFolderServiceProxy remoteProxy;

    public AsyncBookmarkFolderServiceProxy()
    {
        remoteProxy = new RemoteBookmarkFolderServiceProxy();
    }

    @Override
    public BookmarkFolder getBookmarkFolder(String guid) throws BaseException
    {
        return remoteProxy.getBookmarkFolder(guid);
    }

    @Override
    public Object getBookmarkFolder(String guid, String field) throws BaseException
    {
        return remoteProxy.getBookmarkFolder(guid, field);       
    }

    @Override
    public List<BookmarkFolder> getBookmarkFolders(List<String> guids) throws BaseException
    {
        return remoteProxy.getBookmarkFolders(guids);
    }

    @Override
    public List<BookmarkFolder> getAllBookmarkFolders() throws BaseException
    {
        return getAllBookmarkFolders(null, null, null);
    }

    @Override
    public List<BookmarkFolder> getAllBookmarkFolders(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllBookmarkFolders(ordering, offset, count);
    }

    @Override
    public List<String> getAllBookmarkFolderKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllBookmarkFolderKeys(ordering, offset, count);
    }

    @Override
    public List<BookmarkFolder> findBookmarkFolders(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findBookmarkFolders(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<BookmarkFolder> findBookmarkFolders(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findBookmarkFolders(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findBookmarkFolderKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findBookmarkFolderKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return remoteProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createBookmarkFolder(String appClient, String clientRootDomain, String user, String title, String description, String type, String category, String parent, String aggregate, String acl, Boolean exportable, String status, String note, String keywordFolder) throws BaseException
    {
        BookmarkFolderBean bean = new BookmarkFolderBean(null, appClient, clientRootDomain, user, title, description, type, category, parent, aggregate, acl, exportable, status, note, keywordFolder);
        return createBookmarkFolder(bean);        
    }

    @Override
    public String createBookmarkFolder(BookmarkFolder bookmarkFolder) throws BaseException
    {
        log.finer("BEGIN");

        String guid = bookmarkFolder.getGuid();
        if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
            guid = GUID.generate();
            ((BookmarkFolderBean) bookmarkFolder).setGuid(guid);
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsCreateBookmarkFolder-" + guid;
        String taskName = "RsCreateBookmarkFolder-" + guid + "-" + (new Date()).getTime();
        BookmarkFolderStub stub = MarshalHelper.convertBookmarkFolderToStub(bookmarkFolder);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(BookmarkFolderStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = bookmarkFolder.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    BookmarkFolderStub dummyStub = new BookmarkFolderStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createBookmarkFolder(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "bookmarkFolders/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createBookmarkFolder(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "bookmarkFolders/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;  // ???
    }

    @Override
    public Boolean updateBookmarkFolder(String guid, String appClient, String clientRootDomain, String user, String title, String description, String type, String category, String parent, String aggregate, String acl, Boolean exportable, String status, String note, String keywordFolder) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "BookmarkFolder guid is invalid.");
        	throw new BaseException("BookmarkFolder guid is invalid.");
        }
        BookmarkFolderBean bean = new BookmarkFolderBean(guid, appClient, clientRootDomain, user, title, description, type, category, parent, aggregate, acl, exportable, status, note, keywordFolder);
        return updateBookmarkFolder(bean);        
    }

    @Override
    public Boolean updateBookmarkFolder(BookmarkFolder bookmarkFolder) throws BaseException
    {
        log.finer("BEGIN");

        String guid = bookmarkFolder.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "BookmarkFolder object is invalid.");
        	throw new BaseException("BookmarkFolder object is invalid.");
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsUpdateBookmarkFolder-" + guid;
        String taskName = "RsUpdateBookmarkFolder-" + guid + "-" + (new Date()).getTime();
        BookmarkFolderStub stub = MarshalHelper.convertBookmarkFolderToStub(bookmarkFolder);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(BookmarkFolderStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = bookmarkFolder.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    BookmarkFolderStub dummyStub = new BookmarkFolderStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateBookmarkFolder(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "bookmarkFolders/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateBookmarkFolder(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "bookmarkFolders/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteBookmarkFolder(String guid) throws BaseException
    {
        log.finer("BEGIN");

        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsDeleteBookmarkFolder-" + guid;
        String taskName = "RsDeleteBookmarkFolder-" + guid + "-" + (new Date()).getTime();
        TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "bookmarkFolders/" + guid).method(Method.DELETE).taskName(taskName);
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteBookmarkFolder(BookmarkFolder bookmarkFolder) throws BaseException
    {
        String guid = bookmarkFolder.getGuid();
        return deleteBookmarkFolder(guid);
    }

    @Override
    public Long deleteBookmarkFolders(String filter, String params, List<String> values) throws BaseException
    {
        return remoteProxy.deleteBookmarkFolders(filter, params, values);
    }

}
