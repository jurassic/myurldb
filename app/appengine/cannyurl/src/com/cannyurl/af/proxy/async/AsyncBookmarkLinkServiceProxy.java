package com.cannyurl.af.proxy.async;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.BookmarkLink;
import com.myurldb.ws.stub.ErrorStub;
import com.myurldb.ws.stub.BookmarkLinkStub;
import com.myurldb.ws.stub.BookmarkLinkListStub;
import com.cannyurl.af.util.MarshalHelper;
import com.cannyurl.af.bean.BookmarkLinkBean;
import com.myurldb.ws.service.BookmarkLinkService;
import com.cannyurl.af.proxy.BookmarkLinkServiceProxy;
import com.cannyurl.af.proxy.remote.RemoteBookmarkLinkServiceProxy;


// Implements the asynchronous call logic (using GAE task queue).
// GET:             Directly calls the remoteProxy interface.
// POST/PUT/DELETE: Creates a task and puts in a queue.
public class AsyncBookmarkLinkServiceProxy extends BaseAsyncServiceProxy implements BookmarkLinkServiceProxy
{
    private static final Logger log = Logger.getLogger(AsyncBookmarkLinkServiceProxy.class.getName());

    // Actual implementation is delegated to the corresponding remote proxy class.
    private RemoteBookmarkLinkServiceProxy remoteProxy;

    public AsyncBookmarkLinkServiceProxy()
    {
        remoteProxy = new RemoteBookmarkLinkServiceProxy();
    }

    @Override
    public BookmarkLink getBookmarkLink(String guid) throws BaseException
    {
        return remoteProxy.getBookmarkLink(guid);
    }

    @Override
    public Object getBookmarkLink(String guid, String field) throws BaseException
    {
        return remoteProxy.getBookmarkLink(guid, field);       
    }

    @Override
    public List<BookmarkLink> getBookmarkLinks(List<String> guids) throws BaseException
    {
        return remoteProxy.getBookmarkLinks(guids);
    }

    @Override
    public List<BookmarkLink> getAllBookmarkLinks() throws BaseException
    {
        return getAllBookmarkLinks(null, null, null);
    }

    @Override
    public List<BookmarkLink> getAllBookmarkLinks(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllBookmarkLinks(ordering, offset, count);
    }

    @Override
    public List<String> getAllBookmarkLinkKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllBookmarkLinkKeys(ordering, offset, count);
    }

    @Override
    public List<BookmarkLink> findBookmarkLinks(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findBookmarkLinks(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<BookmarkLink> findBookmarkLinks(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findBookmarkLinks(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findBookmarkLinkKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findBookmarkLinkKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return remoteProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createBookmarkLink(String appClient, String clientRootDomain, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, Boolean internal, Boolean caching, String memo, String status, String note, Long expirationTime, String bookmarkFolder, String contentTag, String referenceElement, String elementType, String keywordLink) throws BaseException
    {
        BookmarkLinkBean bean = new BookmarkLinkBean(null, appClient, clientRootDomain, user, shortLink, domain, token, longUrl, shortUrl, internal, caching, memo, status, note, expirationTime, bookmarkFolder, contentTag, referenceElement, elementType, keywordLink);
        return createBookmarkLink(bean);        
    }

    @Override
    public String createBookmarkLink(BookmarkLink bookmarkLink) throws BaseException
    {
        log.finer("BEGIN");

        String guid = bookmarkLink.getGuid();
        if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
            guid = GUID.generate();
            ((BookmarkLinkBean) bookmarkLink).setGuid(guid);
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsCreateBookmarkLink-" + guid;
        String taskName = "RsCreateBookmarkLink-" + guid + "-" + (new Date()).getTime();
        BookmarkLinkStub stub = MarshalHelper.convertBookmarkLinkToStub(bookmarkLink);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(BookmarkLinkStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = bookmarkLink.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    BookmarkLinkStub dummyStub = new BookmarkLinkStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createBookmarkLink(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "bookmarkLinks/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createBookmarkLink(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "bookmarkLinks/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;  // ???
    }

    @Override
    public Boolean updateBookmarkLink(String guid, String appClient, String clientRootDomain, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, Boolean internal, Boolean caching, String memo, String status, String note, Long expirationTime, String bookmarkFolder, String contentTag, String referenceElement, String elementType, String keywordLink) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "BookmarkLink guid is invalid.");
        	throw new BaseException("BookmarkLink guid is invalid.");
        }
        BookmarkLinkBean bean = new BookmarkLinkBean(guid, appClient, clientRootDomain, user, shortLink, domain, token, longUrl, shortUrl, internal, caching, memo, status, note, expirationTime, bookmarkFolder, contentTag, referenceElement, elementType, keywordLink);
        return updateBookmarkLink(bean);        
    }

    @Override
    public Boolean updateBookmarkLink(BookmarkLink bookmarkLink) throws BaseException
    {
        log.finer("BEGIN");

        String guid = bookmarkLink.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "BookmarkLink object is invalid.");
        	throw new BaseException("BookmarkLink object is invalid.");
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsUpdateBookmarkLink-" + guid;
        String taskName = "RsUpdateBookmarkLink-" + guid + "-" + (new Date()).getTime();
        BookmarkLinkStub stub = MarshalHelper.convertBookmarkLinkToStub(bookmarkLink);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(BookmarkLinkStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = bookmarkLink.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    BookmarkLinkStub dummyStub = new BookmarkLinkStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateBookmarkLink(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "bookmarkLinks/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateBookmarkLink(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "bookmarkLinks/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteBookmarkLink(String guid) throws BaseException
    {
        log.finer("BEGIN");

        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsDeleteBookmarkLink-" + guid;
        String taskName = "RsDeleteBookmarkLink-" + guid + "-" + (new Date()).getTime();
        TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "bookmarkLinks/" + guid).method(Method.DELETE).taskName(taskName);
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteBookmarkLink(BookmarkLink bookmarkLink) throws BaseException
    {
        String guid = bookmarkLink.getGuid();
        return deleteBookmarkLink(guid);
    }

    @Override
    public Long deleteBookmarkLinks(String filter, String params, List<String> values) throws BaseException
    {
        return remoteProxy.deleteBookmarkLinks(filter, params, values);
    }

}
