package com.cannyurl.af.proxy.async;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.ClientSetting;
import com.myurldb.ws.stub.ErrorStub;
import com.myurldb.ws.stub.ClientSettingStub;
import com.myurldb.ws.stub.ClientSettingListStub;
import com.cannyurl.af.util.MarshalHelper;
import com.cannyurl.af.bean.ClientSettingBean;
import com.myurldb.ws.service.ClientSettingService;
import com.cannyurl.af.proxy.ClientSettingServiceProxy;
import com.cannyurl.af.proxy.remote.RemoteClientSettingServiceProxy;


// Implements the asynchronous call logic (using GAE task queue).
// GET:             Directly calls the remoteProxy interface.
// POST/PUT/DELETE: Creates a task and puts in a queue.
public class AsyncClientSettingServiceProxy extends BaseAsyncServiceProxy implements ClientSettingServiceProxy
{
    private static final Logger log = Logger.getLogger(AsyncClientSettingServiceProxy.class.getName());

    // Actual implementation is delegated to the corresponding remote proxy class.
    private RemoteClientSettingServiceProxy remoteProxy;

    public AsyncClientSettingServiceProxy()
    {
        remoteProxy = new RemoteClientSettingServiceProxy();
    }

    @Override
    public ClientSetting getClientSetting(String guid) throws BaseException
    {
        return remoteProxy.getClientSetting(guid);
    }

    @Override
    public Object getClientSetting(String guid, String field) throws BaseException
    {
        return remoteProxy.getClientSetting(guid, field);       
    }

    @Override
    public List<ClientSetting> getClientSettings(List<String> guids) throws BaseException
    {
        return remoteProxy.getClientSettings(guids);
    }

    @Override
    public List<ClientSetting> getAllClientSettings() throws BaseException
    {
        return getAllClientSettings(null, null, null);
    }

    @Override
    public List<ClientSetting> getAllClientSettings(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllClientSettings(ordering, offset, count);
    }

    @Override
    public List<String> getAllClientSettingKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllClientSettingKeys(ordering, offset, count);
    }

    @Override
    public List<ClientSetting> findClientSettings(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findClientSettings(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<ClientSetting> findClientSettings(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findClientSettings(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findClientSettingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findClientSettingKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return remoteProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createClientSetting(String appClient, String admin, Boolean autoRedirectEnabled, Boolean viewEnabled, Boolean shareEnabled, String defaultBaseDomain, String defaultDomainType, String defaultTokenType, String defaultRedirectType, Long defaultFlashDuration, String defaultAccessType, String defaultViewType) throws BaseException
    {
        ClientSettingBean bean = new ClientSettingBean(null, appClient, admin, autoRedirectEnabled, viewEnabled, shareEnabled, defaultBaseDomain, defaultDomainType, defaultTokenType, defaultRedirectType, defaultFlashDuration, defaultAccessType, defaultViewType);
        return createClientSetting(bean);        
    }

    @Override
    public String createClientSetting(ClientSetting clientSetting) throws BaseException
    {
        log.finer("BEGIN");

        String guid = clientSetting.getGuid();
        if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
            guid = GUID.generate();
            ((ClientSettingBean) clientSetting).setGuid(guid);
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsCreateClientSetting-" + guid;
        String taskName = "RsCreateClientSetting-" + guid + "-" + (new Date()).getTime();
        ClientSettingStub stub = MarshalHelper.convertClientSettingToStub(clientSetting);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(ClientSettingStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = clientSetting.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    ClientSettingStub dummyStub = new ClientSettingStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createClientSetting(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "clientSettings/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createClientSetting(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "clientSettings/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;  // ???
    }

    @Override
    public Boolean updateClientSetting(String guid, String appClient, String admin, Boolean autoRedirectEnabled, Boolean viewEnabled, Boolean shareEnabled, String defaultBaseDomain, String defaultDomainType, String defaultTokenType, String defaultRedirectType, Long defaultFlashDuration, String defaultAccessType, String defaultViewType) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "ClientSetting guid is invalid.");
        	throw new BaseException("ClientSetting guid is invalid.");
        }
        ClientSettingBean bean = new ClientSettingBean(guid, appClient, admin, autoRedirectEnabled, viewEnabled, shareEnabled, defaultBaseDomain, defaultDomainType, defaultTokenType, defaultRedirectType, defaultFlashDuration, defaultAccessType, defaultViewType);
        return updateClientSetting(bean);        
    }

    @Override
    public Boolean updateClientSetting(ClientSetting clientSetting) throws BaseException
    {
        log.finer("BEGIN");

        String guid = clientSetting.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "ClientSetting object is invalid.");
        	throw new BaseException("ClientSetting object is invalid.");
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsUpdateClientSetting-" + guid;
        String taskName = "RsUpdateClientSetting-" + guid + "-" + (new Date()).getTime();
        ClientSettingStub stub = MarshalHelper.convertClientSettingToStub(clientSetting);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(ClientSettingStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = clientSetting.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    ClientSettingStub dummyStub = new ClientSettingStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateClientSetting(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "clientSettings/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateClientSetting(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "clientSettings/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteClientSetting(String guid) throws BaseException
    {
        log.finer("BEGIN");

        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsDeleteClientSetting-" + guid;
        String taskName = "RsDeleteClientSetting-" + guid + "-" + (new Date()).getTime();
        TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "clientSettings/" + guid).method(Method.DELETE).taskName(taskName);
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteClientSetting(ClientSetting clientSetting) throws BaseException
    {
        String guid = clientSetting.getGuid();
        return deleteClientSetting(guid);
    }

    @Override
    public Long deleteClientSettings(String filter, String params, List<String> values) throws BaseException
    {
        return remoteProxy.deleteClientSettings(filter, params, values);
    }

}
