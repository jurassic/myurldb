package com.cannyurl.af.proxy.async;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.ClientUser;
import com.myurldb.ws.stub.ErrorStub;
import com.myurldb.ws.stub.ClientUserStub;
import com.myurldb.ws.stub.ClientUserListStub;
import com.cannyurl.af.util.MarshalHelper;
import com.cannyurl.af.bean.ClientUserBean;
import com.myurldb.ws.service.ClientUserService;
import com.cannyurl.af.proxy.ClientUserServiceProxy;
import com.cannyurl.af.proxy.remote.RemoteClientUserServiceProxy;


// Implements the asynchronous call logic (using GAE task queue).
// GET:             Directly calls the remoteProxy interface.
// POST/PUT/DELETE: Creates a task and puts in a queue.
public class AsyncClientUserServiceProxy extends BaseAsyncServiceProxy implements ClientUserServiceProxy
{
    private static final Logger log = Logger.getLogger(AsyncClientUserServiceProxy.class.getName());

    // Actual implementation is delegated to the corresponding remote proxy class.
    private RemoteClientUserServiceProxy remoteProxy;

    public AsyncClientUserServiceProxy()
    {
        remoteProxy = new RemoteClientUserServiceProxy();
    }

    @Override
    public ClientUser getClientUser(String guid) throws BaseException
    {
        return remoteProxy.getClientUser(guid);
    }

    @Override
    public Object getClientUser(String guid, String field) throws BaseException
    {
        return remoteProxy.getClientUser(guid, field);       
    }

    @Override
    public List<ClientUser> getClientUsers(List<String> guids) throws BaseException
    {
        return remoteProxy.getClientUsers(guids);
    }

    @Override
    public List<ClientUser> getAllClientUsers() throws BaseException
    {
        return getAllClientUsers(null, null, null);
    }

    @Override
    public List<ClientUser> getAllClientUsers(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllClientUsers(ordering, offset, count);
    }

    @Override
    public List<String> getAllClientUserKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllClientUserKeys(ordering, offset, count);
    }

    @Override
    public List<ClientUser> findClientUsers(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findClientUsers(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<ClientUser> findClientUsers(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findClientUsers(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findClientUserKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findClientUserKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return remoteProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createClientUser(String appClient, String user, String role, Boolean provisioned, Boolean licensed, String status) throws BaseException
    {
        ClientUserBean bean = new ClientUserBean(null, appClient, user, role, provisioned, licensed, status);
        return createClientUser(bean);        
    }

    @Override
    public String createClientUser(ClientUser clientUser) throws BaseException
    {
        log.finer("BEGIN");

        String guid = clientUser.getGuid();
        if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
            guid = GUID.generate();
            ((ClientUserBean) clientUser).setGuid(guid);
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsCreateClientUser-" + guid;
        String taskName = "RsCreateClientUser-" + guid + "-" + (new Date()).getTime();
        ClientUserStub stub = MarshalHelper.convertClientUserToStub(clientUser);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(ClientUserStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = clientUser.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    ClientUserStub dummyStub = new ClientUserStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createClientUser(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "clientUsers/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createClientUser(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "clientUsers/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;  // ???
    }

    @Override
    public Boolean updateClientUser(String guid, String appClient, String user, String role, Boolean provisioned, Boolean licensed, String status) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "ClientUser guid is invalid.");
        	throw new BaseException("ClientUser guid is invalid.");
        }
        ClientUserBean bean = new ClientUserBean(guid, appClient, user, role, provisioned, licensed, status);
        return updateClientUser(bean);        
    }

    @Override
    public Boolean updateClientUser(ClientUser clientUser) throws BaseException
    {
        log.finer("BEGIN");

        String guid = clientUser.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "ClientUser object is invalid.");
        	throw new BaseException("ClientUser object is invalid.");
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsUpdateClientUser-" + guid;
        String taskName = "RsUpdateClientUser-" + guid + "-" + (new Date()).getTime();
        ClientUserStub stub = MarshalHelper.convertClientUserToStub(clientUser);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(ClientUserStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = clientUser.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    ClientUserStub dummyStub = new ClientUserStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateClientUser(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "clientUsers/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateClientUser(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "clientUsers/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteClientUser(String guid) throws BaseException
    {
        log.finer("BEGIN");

        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsDeleteClientUser-" + guid;
        String taskName = "RsDeleteClientUser-" + guid + "-" + (new Date()).getTime();
        TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "clientUsers/" + guid).method(Method.DELETE).taskName(taskName);
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteClientUser(ClientUser clientUser) throws BaseException
    {
        String guid = clientUser.getGuid();
        return deleteClientUser(guid);
    }

    @Override
    public Long deleteClientUsers(String filter, String params, List<String> values) throws BaseException
    {
        return remoteProxy.deleteClientUsers(filter, params, values);
    }

}
