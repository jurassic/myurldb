package com.cannyurl.af.proxy.async;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.DomainInfo;
import com.myurldb.ws.stub.ErrorStub;
import com.myurldb.ws.stub.DomainInfoStub;
import com.myurldb.ws.stub.DomainInfoListStub;
import com.cannyurl.af.util.MarshalHelper;
import com.cannyurl.af.bean.DomainInfoBean;
import com.myurldb.ws.service.DomainInfoService;
import com.cannyurl.af.proxy.DomainInfoServiceProxy;
import com.cannyurl.af.proxy.remote.RemoteDomainInfoServiceProxy;


// Implements the asynchronous call logic (using GAE task queue).
// GET:             Directly calls the remoteProxy interface.
// POST/PUT/DELETE: Creates a task and puts in a queue.
public class AsyncDomainInfoServiceProxy extends BaseAsyncServiceProxy implements DomainInfoServiceProxy
{
    private static final Logger log = Logger.getLogger(AsyncDomainInfoServiceProxy.class.getName());

    // Actual implementation is delegated to the corresponding remote proxy class.
    private RemoteDomainInfoServiceProxy remoteProxy;

    public AsyncDomainInfoServiceProxy()
    {
        remoteProxy = new RemoteDomainInfoServiceProxy();
    }

    @Override
    public DomainInfo getDomainInfo(String guid) throws BaseException
    {
        return remoteProxy.getDomainInfo(guid);
    }

    @Override
    public Object getDomainInfo(String guid, String field) throws BaseException
    {
        return remoteProxy.getDomainInfo(guid, field);       
    }

    @Override
    public List<DomainInfo> getDomainInfos(List<String> guids) throws BaseException
    {
        return remoteProxy.getDomainInfos(guids);
    }

    @Override
    public List<DomainInfo> getAllDomainInfos() throws BaseException
    {
        return getAllDomainInfos(null, null, null);
    }

    @Override
    public List<DomainInfo> getAllDomainInfos(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllDomainInfos(ordering, offset, count);
    }

    @Override
    public List<String> getAllDomainInfoKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllDomainInfoKeys(ordering, offset, count);
    }

    @Override
    public List<DomainInfo> findDomainInfos(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findDomainInfos(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<DomainInfo> findDomainInfos(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findDomainInfos(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findDomainInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findDomainInfoKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return remoteProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createDomainInfo(String domain, Boolean banned, Boolean urlShortener, String category, String reputation, String authority, String note, Long verifiedTime) throws BaseException
    {
        DomainInfoBean bean = new DomainInfoBean(null, domain, banned, urlShortener, category, reputation, authority, note, verifiedTime);
        return createDomainInfo(bean);        
    }

    @Override
    public String createDomainInfo(DomainInfo domainInfo) throws BaseException
    {
        log.finer("BEGIN");

        String guid = domainInfo.getGuid();
        if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
            guid = GUID.generate();
            ((DomainInfoBean) domainInfo).setGuid(guid);
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsCreateDomainInfo-" + guid;
        String taskName = "RsCreateDomainInfo-" + guid + "-" + (new Date()).getTime();
        DomainInfoStub stub = MarshalHelper.convertDomainInfoToStub(domainInfo);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(DomainInfoStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = domainInfo.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    DomainInfoStub dummyStub = new DomainInfoStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createDomainInfo(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "domainInfos/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createDomainInfo(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "domainInfos/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;  // ???
    }

    @Override
    public Boolean updateDomainInfo(String guid, String domain, Boolean banned, Boolean urlShortener, String category, String reputation, String authority, String note, Long verifiedTime) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "DomainInfo guid is invalid.");
        	throw new BaseException("DomainInfo guid is invalid.");
        }
        DomainInfoBean bean = new DomainInfoBean(guid, domain, banned, urlShortener, category, reputation, authority, note, verifiedTime);
        return updateDomainInfo(bean);        
    }

    @Override
    public Boolean updateDomainInfo(DomainInfo domainInfo) throws BaseException
    {
        log.finer("BEGIN");

        String guid = domainInfo.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "DomainInfo object is invalid.");
        	throw new BaseException("DomainInfo object is invalid.");
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsUpdateDomainInfo-" + guid;
        String taskName = "RsUpdateDomainInfo-" + guid + "-" + (new Date()).getTime();
        DomainInfoStub stub = MarshalHelper.convertDomainInfoToStub(domainInfo);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(DomainInfoStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = domainInfo.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    DomainInfoStub dummyStub = new DomainInfoStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateDomainInfo(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "domainInfos/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateDomainInfo(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "domainInfos/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteDomainInfo(String guid) throws BaseException
    {
        log.finer("BEGIN");

        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsDeleteDomainInfo-" + guid;
        String taskName = "RsDeleteDomainInfo-" + guid + "-" + (new Date()).getTime();
        TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "domainInfos/" + guid).method(Method.DELETE).taskName(taskName);
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteDomainInfo(DomainInfo domainInfo) throws BaseException
    {
        String guid = domainInfo.getGuid();
        return deleteDomainInfo(guid);
    }

    @Override
    public Long deleteDomainInfos(String filter, String params, List<String> values) throws BaseException
    {
        return remoteProxy.deleteDomainInfos(filter, params, values);
    }

}
