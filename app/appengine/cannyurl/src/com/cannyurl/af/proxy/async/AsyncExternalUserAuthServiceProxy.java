package com.cannyurl.af.proxy.async;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.ExternalUserIdStruct;
import com.myurldb.ws.ExternalUserAuth;
import com.myurldb.ws.stub.ErrorStub;
import com.myurldb.ws.stub.ExternalUserIdStructStub;
import com.myurldb.ws.stub.ExternalUserIdStructListStub;
import com.myurldb.ws.stub.ExternalUserAuthStub;
import com.myurldb.ws.stub.ExternalUserAuthListStub;
import com.cannyurl.af.util.MarshalHelper;
import com.cannyurl.af.bean.ExternalUserIdStructBean;
import com.cannyurl.af.bean.ExternalUserAuthBean;
import com.myurldb.ws.service.ExternalUserAuthService;
import com.cannyurl.af.proxy.ExternalUserAuthServiceProxy;
import com.cannyurl.af.proxy.remote.RemoteExternalUserAuthServiceProxy;


// Implements the asynchronous call logic (using GAE task queue).
// GET:             Directly calls the remoteProxy interface.
// POST/PUT/DELETE: Creates a task and puts in a queue.
public class AsyncExternalUserAuthServiceProxy extends BaseAsyncServiceProxy implements ExternalUserAuthServiceProxy
{
    private static final Logger log = Logger.getLogger(AsyncExternalUserAuthServiceProxy.class.getName());

    // Actual implementation is delegated to the corresponding remote proxy class.
    private RemoteExternalUserAuthServiceProxy remoteProxy;

    public AsyncExternalUserAuthServiceProxy()
    {
        remoteProxy = new RemoteExternalUserAuthServiceProxy();
    }

    @Override
    public ExternalUserAuth getExternalUserAuth(String guid) throws BaseException
    {
        return remoteProxy.getExternalUserAuth(guid);
    }

    @Override
    public Object getExternalUserAuth(String guid, String field) throws BaseException
    {
        return remoteProxy.getExternalUserAuth(guid, field);       
    }

    @Override
    public List<ExternalUserAuth> getExternalUserAuths(List<String> guids) throws BaseException
    {
        return remoteProxy.getExternalUserAuths(guids);
    }

    @Override
    public List<ExternalUserAuth> getAllExternalUserAuths() throws BaseException
    {
        return getAllExternalUserAuths(null, null, null);
    }

    @Override
    public List<ExternalUserAuth> getAllExternalUserAuths(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllExternalUserAuths(ordering, offset, count);
    }

    @Override
    public List<String> getAllExternalUserAuthKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllExternalUserAuthKeys(ordering, offset, count);
    }

    @Override
    public List<ExternalUserAuth> findExternalUserAuths(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findExternalUserAuths(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<ExternalUserAuth> findExternalUserAuths(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findExternalUserAuths(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findExternalUserAuthKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findExternalUserAuthKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return remoteProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createExternalUserAuth(String user, String authType, String providerId, String providerDomain, ExternalUserIdStruct externalUserId, String requestToken, String accessToken, String accessTokenSecret, String email, String firstName, String lastName, String fullName, String displayName, String description, String gender, String dateOfBirth, String profileImageUrl, String timeZone, String postalCode, String location, String country, String language, String status, Long authTime, Long expirationTime) throws BaseException
    {
        ExternalUserAuthBean bean = new ExternalUserAuthBean(null, user, authType, providerId, providerDomain, MarshalHelper.convertExternalUserIdStructToBean(externalUserId), requestToken, accessToken, accessTokenSecret, email, firstName, lastName, fullName, displayName, description, gender, dateOfBirth, profileImageUrl, timeZone, postalCode, location, country, language, status, authTime, expirationTime);
        return createExternalUserAuth(bean);        
    }

    @Override
    public String createExternalUserAuth(ExternalUserAuth externalUserAuth) throws BaseException
    {
        log.finer("BEGIN");

        String guid = externalUserAuth.getGuid();
        if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
            guid = GUID.generate();
            ((ExternalUserAuthBean) externalUserAuth).setGuid(guid);
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsCreateExternalUserAuth-" + guid;
        String taskName = "RsCreateExternalUserAuth-" + guid + "-" + (new Date()).getTime();
        ExternalUserAuthStub stub = MarshalHelper.convertExternalUserAuthToStub(externalUserAuth);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(ExternalUserAuthStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = externalUserAuth.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    ExternalUserAuthStub dummyStub = new ExternalUserAuthStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createExternalUserAuth(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "externalUserAuths/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createExternalUserAuth(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "externalUserAuths/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;  // ???
    }

    @Override
    public Boolean updateExternalUserAuth(String guid, String user, String authType, String providerId, String providerDomain, ExternalUserIdStruct externalUserId, String requestToken, String accessToken, String accessTokenSecret, String email, String firstName, String lastName, String fullName, String displayName, String description, String gender, String dateOfBirth, String profileImageUrl, String timeZone, String postalCode, String location, String country, String language, String status, Long authTime, Long expirationTime) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "ExternalUserAuth guid is invalid.");
        	throw new BaseException("ExternalUserAuth guid is invalid.");
        }
        ExternalUserAuthBean bean = new ExternalUserAuthBean(guid, user, authType, providerId, providerDomain, MarshalHelper.convertExternalUserIdStructToBean(externalUserId), requestToken, accessToken, accessTokenSecret, email, firstName, lastName, fullName, displayName, description, gender, dateOfBirth, profileImageUrl, timeZone, postalCode, location, country, language, status, authTime, expirationTime);
        return updateExternalUserAuth(bean);        
    }

    @Override
    public Boolean updateExternalUserAuth(ExternalUserAuth externalUserAuth) throws BaseException
    {
        log.finer("BEGIN");

        String guid = externalUserAuth.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "ExternalUserAuth object is invalid.");
        	throw new BaseException("ExternalUserAuth object is invalid.");
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsUpdateExternalUserAuth-" + guid;
        String taskName = "RsUpdateExternalUserAuth-" + guid + "-" + (new Date()).getTime();
        ExternalUserAuthStub stub = MarshalHelper.convertExternalUserAuthToStub(externalUserAuth);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(ExternalUserAuthStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = externalUserAuth.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    ExternalUserAuthStub dummyStub = new ExternalUserAuthStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateExternalUserAuth(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "externalUserAuths/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateExternalUserAuth(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "externalUserAuths/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteExternalUserAuth(String guid) throws BaseException
    {
        log.finer("BEGIN");

        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsDeleteExternalUserAuth-" + guid;
        String taskName = "RsDeleteExternalUserAuth-" + guid + "-" + (new Date()).getTime();
        TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "externalUserAuths/" + guid).method(Method.DELETE).taskName(taskName);
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteExternalUserAuth(ExternalUserAuth externalUserAuth) throws BaseException
    {
        String guid = externalUserAuth.getGuid();
        return deleteExternalUserAuth(guid);
    }

    @Override
    public Long deleteExternalUserAuths(String filter, String params, List<String> values) throws BaseException
    {
        return remoteProxy.deleteExternalUserAuths(filter, params, values);
    }

}
