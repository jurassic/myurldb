package com.cannyurl.af.proxy.async;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.KeywordCrowdTally;
import com.myurldb.ws.stub.ErrorStub;
import com.myurldb.ws.stub.KeywordCrowdTallyStub;
import com.myurldb.ws.stub.KeywordCrowdTallyListStub;
import com.cannyurl.af.util.MarshalHelper;
import com.cannyurl.af.bean.KeywordCrowdTallyBean;
import com.myurldb.ws.service.KeywordCrowdTallyService;
import com.cannyurl.af.proxy.KeywordCrowdTallyServiceProxy;
import com.cannyurl.af.proxy.remote.RemoteKeywordCrowdTallyServiceProxy;


// Implements the asynchronous call logic (using GAE task queue).
// GET:             Directly calls the remoteProxy interface.
// POST/PUT/DELETE: Creates a task and puts in a queue.
public class AsyncKeywordCrowdTallyServiceProxy extends BaseAsyncServiceProxy implements KeywordCrowdTallyServiceProxy
{
    private static final Logger log = Logger.getLogger(AsyncKeywordCrowdTallyServiceProxy.class.getName());

    // Actual implementation is delegated to the corresponding remote proxy class.
    private RemoteKeywordCrowdTallyServiceProxy remoteProxy;

    public AsyncKeywordCrowdTallyServiceProxy()
    {
        remoteProxy = new RemoteKeywordCrowdTallyServiceProxy();
    }

    @Override
    public KeywordCrowdTally getKeywordCrowdTally(String guid) throws BaseException
    {
        return remoteProxy.getKeywordCrowdTally(guid);
    }

    @Override
    public Object getKeywordCrowdTally(String guid, String field) throws BaseException
    {
        return remoteProxy.getKeywordCrowdTally(guid, field);       
    }

    @Override
    public List<KeywordCrowdTally> getKeywordCrowdTallies(List<String> guids) throws BaseException
    {
        return remoteProxy.getKeywordCrowdTallies(guids);
    }

    @Override
    public List<KeywordCrowdTally> getAllKeywordCrowdTallies() throws BaseException
    {
        return getAllKeywordCrowdTallies(null, null, null);
    }

    @Override
    public List<KeywordCrowdTally> getAllKeywordCrowdTallies(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllKeywordCrowdTallies(ordering, offset, count);
    }

    @Override
    public List<String> getAllKeywordCrowdTallyKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllKeywordCrowdTallyKeys(ordering, offset, count);
    }

    @Override
    public List<KeywordCrowdTally> findKeywordCrowdTallies(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findKeywordCrowdTallies(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<KeywordCrowdTally> findKeywordCrowdTallies(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findKeywordCrowdTallies(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findKeywordCrowdTallyKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findKeywordCrowdTallyKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return remoteProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createKeywordCrowdTally(String user, String shortLink, String domain, String token, String longUrl, String shortUrl, String tallyDate, String status, String note, Long expirationTime, String keywordFolder, String folderPath, String keyword, String queryKey, String scope, Boolean caseSensitive) throws BaseException
    {
        KeywordCrowdTallyBean bean = new KeywordCrowdTallyBean(null, user, shortLink, domain, token, longUrl, shortUrl, tallyDate, status, note, expirationTime, keywordFolder, folderPath, keyword, queryKey, scope, caseSensitive);
        return createKeywordCrowdTally(bean);        
    }

    @Override
    public String createKeywordCrowdTally(KeywordCrowdTally keywordCrowdTally) throws BaseException
    {
        log.finer("BEGIN");

        String guid = keywordCrowdTally.getGuid();
        if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
            guid = GUID.generate();
            ((KeywordCrowdTallyBean) keywordCrowdTally).setGuid(guid);
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsCreateKeywordCrowdTally-" + guid;
        String taskName = "RsCreateKeywordCrowdTally-" + guid + "-" + (new Date()).getTime();
        KeywordCrowdTallyStub stub = MarshalHelper.convertKeywordCrowdTallyToStub(keywordCrowdTally);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(KeywordCrowdTallyStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = keywordCrowdTally.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    KeywordCrowdTallyStub dummyStub = new KeywordCrowdTallyStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createKeywordCrowdTally(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "keywordCrowdTallies/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createKeywordCrowdTally(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "keywordCrowdTallies/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;  // ???
    }

    @Override
    public Boolean updateKeywordCrowdTally(String guid, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, String tallyDate, String status, String note, Long expirationTime, String keywordFolder, String folderPath, String keyword, String queryKey, String scope, Boolean caseSensitive) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "KeywordCrowdTally guid is invalid.");
        	throw new BaseException("KeywordCrowdTally guid is invalid.");
        }
        KeywordCrowdTallyBean bean = new KeywordCrowdTallyBean(guid, user, shortLink, domain, token, longUrl, shortUrl, tallyDate, status, note, expirationTime, keywordFolder, folderPath, keyword, queryKey, scope, caseSensitive);
        return updateKeywordCrowdTally(bean);        
    }

    @Override
    public Boolean updateKeywordCrowdTally(KeywordCrowdTally keywordCrowdTally) throws BaseException
    {
        log.finer("BEGIN");

        String guid = keywordCrowdTally.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "KeywordCrowdTally object is invalid.");
        	throw new BaseException("KeywordCrowdTally object is invalid.");
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsUpdateKeywordCrowdTally-" + guid;
        String taskName = "RsUpdateKeywordCrowdTally-" + guid + "-" + (new Date()).getTime();
        KeywordCrowdTallyStub stub = MarshalHelper.convertKeywordCrowdTallyToStub(keywordCrowdTally);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(KeywordCrowdTallyStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = keywordCrowdTally.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    KeywordCrowdTallyStub dummyStub = new KeywordCrowdTallyStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateKeywordCrowdTally(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "keywordCrowdTallies/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateKeywordCrowdTally(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "keywordCrowdTallies/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteKeywordCrowdTally(String guid) throws BaseException
    {
        log.finer("BEGIN");

        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsDeleteKeywordCrowdTally-" + guid;
        String taskName = "RsDeleteKeywordCrowdTally-" + guid + "-" + (new Date()).getTime();
        TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "keywordCrowdTallies/" + guid).method(Method.DELETE).taskName(taskName);
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteKeywordCrowdTally(KeywordCrowdTally keywordCrowdTally) throws BaseException
    {
        String guid = keywordCrowdTally.getGuid();
        return deleteKeywordCrowdTally(guid);
    }

    @Override
    public Long deleteKeywordCrowdTallies(String filter, String params, List<String> values) throws BaseException
    {
        return remoteProxy.deleteKeywordCrowdTallies(filter, params, values);
    }

}
