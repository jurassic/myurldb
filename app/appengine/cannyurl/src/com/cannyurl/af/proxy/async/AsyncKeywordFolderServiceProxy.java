package com.cannyurl.af.proxy.async;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.KeywordFolder;
import com.myurldb.ws.stub.ErrorStub;
import com.myurldb.ws.stub.KeywordFolderStub;
import com.myurldb.ws.stub.KeywordFolderListStub;
import com.cannyurl.af.util.MarshalHelper;
import com.cannyurl.af.bean.KeywordFolderBean;
import com.myurldb.ws.service.KeywordFolderService;
import com.cannyurl.af.proxy.KeywordFolderServiceProxy;
import com.cannyurl.af.proxy.remote.RemoteKeywordFolderServiceProxy;


// Implements the asynchronous call logic (using GAE task queue).
// GET:             Directly calls the remoteProxy interface.
// POST/PUT/DELETE: Creates a task and puts in a queue.
public class AsyncKeywordFolderServiceProxy extends BaseAsyncServiceProxy implements KeywordFolderServiceProxy
{
    private static final Logger log = Logger.getLogger(AsyncKeywordFolderServiceProxy.class.getName());

    // Actual implementation is delegated to the corresponding remote proxy class.
    private RemoteKeywordFolderServiceProxy remoteProxy;

    public AsyncKeywordFolderServiceProxy()
    {
        remoteProxy = new RemoteKeywordFolderServiceProxy();
    }

    @Override
    public KeywordFolder getKeywordFolder(String guid) throws BaseException
    {
        return remoteProxy.getKeywordFolder(guid);
    }

    @Override
    public Object getKeywordFolder(String guid, String field) throws BaseException
    {
        return remoteProxy.getKeywordFolder(guid, field);       
    }

    @Override
    public List<KeywordFolder> getKeywordFolders(List<String> guids) throws BaseException
    {
        return remoteProxy.getKeywordFolders(guids);
    }

    @Override
    public List<KeywordFolder> getAllKeywordFolders() throws BaseException
    {
        return getAllKeywordFolders(null, null, null);
    }

    @Override
    public List<KeywordFolder> getAllKeywordFolders(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllKeywordFolders(ordering, offset, count);
    }

    @Override
    public List<String> getAllKeywordFolderKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllKeywordFolderKeys(ordering, offset, count);
    }

    @Override
    public List<KeywordFolder> findKeywordFolders(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findKeywordFolders(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<KeywordFolder> findKeywordFolders(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findKeywordFolders(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findKeywordFolderKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findKeywordFolderKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return remoteProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createKeywordFolder(String appClient, String clientRootDomain, String user, String title, String description, String type, String category, String parent, String aggregate, String acl, Boolean exportable, String status, String note, String folderPath) throws BaseException
    {
        KeywordFolderBean bean = new KeywordFolderBean(null, appClient, clientRootDomain, user, title, description, type, category, parent, aggregate, acl, exportable, status, note, folderPath);
        return createKeywordFolder(bean);        
    }

    @Override
    public String createKeywordFolder(KeywordFolder keywordFolder) throws BaseException
    {
        log.finer("BEGIN");

        String guid = keywordFolder.getGuid();
        if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
            guid = GUID.generate();
            ((KeywordFolderBean) keywordFolder).setGuid(guid);
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsCreateKeywordFolder-" + guid;
        String taskName = "RsCreateKeywordFolder-" + guid + "-" + (new Date()).getTime();
        KeywordFolderStub stub = MarshalHelper.convertKeywordFolderToStub(keywordFolder);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(KeywordFolderStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = keywordFolder.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    KeywordFolderStub dummyStub = new KeywordFolderStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createKeywordFolder(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "keywordFolders/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createKeywordFolder(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "keywordFolders/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;  // ???
    }

    @Override
    public Boolean updateKeywordFolder(String guid, String appClient, String clientRootDomain, String user, String title, String description, String type, String category, String parent, String aggregate, String acl, Boolean exportable, String status, String note, String folderPath) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "KeywordFolder guid is invalid.");
        	throw new BaseException("KeywordFolder guid is invalid.");
        }
        KeywordFolderBean bean = new KeywordFolderBean(guid, appClient, clientRootDomain, user, title, description, type, category, parent, aggregate, acl, exportable, status, note, folderPath);
        return updateKeywordFolder(bean);        
    }

    @Override
    public Boolean updateKeywordFolder(KeywordFolder keywordFolder) throws BaseException
    {
        log.finer("BEGIN");

        String guid = keywordFolder.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "KeywordFolder object is invalid.");
        	throw new BaseException("KeywordFolder object is invalid.");
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsUpdateKeywordFolder-" + guid;
        String taskName = "RsUpdateKeywordFolder-" + guid + "-" + (new Date()).getTime();
        KeywordFolderStub stub = MarshalHelper.convertKeywordFolderToStub(keywordFolder);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(KeywordFolderStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = keywordFolder.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    KeywordFolderStub dummyStub = new KeywordFolderStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateKeywordFolder(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "keywordFolders/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateKeywordFolder(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "keywordFolders/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteKeywordFolder(String guid) throws BaseException
    {
        log.finer("BEGIN");

        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsDeleteKeywordFolder-" + guid;
        String taskName = "RsDeleteKeywordFolder-" + guid + "-" + (new Date()).getTime();
        TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "keywordFolders/" + guid).method(Method.DELETE).taskName(taskName);
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteKeywordFolder(KeywordFolder keywordFolder) throws BaseException
    {
        String guid = keywordFolder.getGuid();
        return deleteKeywordFolder(guid);
    }

    @Override
    public Long deleteKeywordFolders(String filter, String params, List<String> values) throws BaseException
    {
        return remoteProxy.deleteKeywordFolders(filter, params, values);
    }

}
