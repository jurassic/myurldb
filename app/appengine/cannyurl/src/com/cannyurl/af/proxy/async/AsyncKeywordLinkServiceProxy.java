package com.cannyurl.af.proxy.async;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.KeywordLink;
import com.myurldb.ws.stub.ErrorStub;
import com.myurldb.ws.stub.KeywordLinkStub;
import com.myurldb.ws.stub.KeywordLinkListStub;
import com.cannyurl.af.util.MarshalHelper;
import com.cannyurl.af.bean.KeywordLinkBean;
import com.myurldb.ws.service.KeywordLinkService;
import com.cannyurl.af.proxy.KeywordLinkServiceProxy;
import com.cannyurl.af.proxy.remote.RemoteKeywordLinkServiceProxy;


// Implements the asynchronous call logic (using GAE task queue).
// GET:             Directly calls the remoteProxy interface.
// POST/PUT/DELETE: Creates a task and puts in a queue.
public class AsyncKeywordLinkServiceProxy extends BaseAsyncServiceProxy implements KeywordLinkServiceProxy
{
    private static final Logger log = Logger.getLogger(AsyncKeywordLinkServiceProxy.class.getName());

    // Actual implementation is delegated to the corresponding remote proxy class.
    private RemoteKeywordLinkServiceProxy remoteProxy;

    public AsyncKeywordLinkServiceProxy()
    {
        remoteProxy = new RemoteKeywordLinkServiceProxy();
    }

    @Override
    public KeywordLink getKeywordLink(String guid) throws BaseException
    {
        return remoteProxy.getKeywordLink(guid);
    }

    @Override
    public Object getKeywordLink(String guid, String field) throws BaseException
    {
        return remoteProxy.getKeywordLink(guid, field);       
    }

    @Override
    public List<KeywordLink> getKeywordLinks(List<String> guids) throws BaseException
    {
        return remoteProxy.getKeywordLinks(guids);
    }

    @Override
    public List<KeywordLink> getAllKeywordLinks() throws BaseException
    {
        return getAllKeywordLinks(null, null, null);
    }

    @Override
    public List<KeywordLink> getAllKeywordLinks(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllKeywordLinks(ordering, offset, count);
    }

    @Override
    public List<String> getAllKeywordLinkKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllKeywordLinkKeys(ordering, offset, count);
    }

    @Override
    public List<KeywordLink> findKeywordLinks(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findKeywordLinks(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<KeywordLink> findKeywordLinks(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findKeywordLinks(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findKeywordLinkKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findKeywordLinkKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return remoteProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createKeywordLink(String appClient, String clientRootDomain, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, Boolean internal, Boolean caching, String memo, String status, String note, Long expirationTime, String keywordFolder, String folderPath, String keyword, String queryKey, String scope, Boolean dynamic, Boolean caseSensitive) throws BaseException
    {
        KeywordLinkBean bean = new KeywordLinkBean(null, appClient, clientRootDomain, user, shortLink, domain, token, longUrl, shortUrl, internal, caching, memo, status, note, expirationTime, keywordFolder, folderPath, keyword, queryKey, scope, dynamic, caseSensitive);
        return createKeywordLink(bean);        
    }

    @Override
    public String createKeywordLink(KeywordLink keywordLink) throws BaseException
    {
        log.finer("BEGIN");

        String guid = keywordLink.getGuid();
        if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
            guid = GUID.generate();
            ((KeywordLinkBean) keywordLink).setGuid(guid);
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsCreateKeywordLink-" + guid;
        String taskName = "RsCreateKeywordLink-" + guid + "-" + (new Date()).getTime();
        KeywordLinkStub stub = MarshalHelper.convertKeywordLinkToStub(keywordLink);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(KeywordLinkStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = keywordLink.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    KeywordLinkStub dummyStub = new KeywordLinkStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createKeywordLink(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "keywordLinks/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createKeywordLink(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "keywordLinks/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;  // ???
    }

    @Override
    public Boolean updateKeywordLink(String guid, String appClient, String clientRootDomain, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, Boolean internal, Boolean caching, String memo, String status, String note, Long expirationTime, String keywordFolder, String folderPath, String keyword, String queryKey, String scope, Boolean dynamic, Boolean caseSensitive) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "KeywordLink guid is invalid.");
        	throw new BaseException("KeywordLink guid is invalid.");
        }
        KeywordLinkBean bean = new KeywordLinkBean(guid, appClient, clientRootDomain, user, shortLink, domain, token, longUrl, shortUrl, internal, caching, memo, status, note, expirationTime, keywordFolder, folderPath, keyword, queryKey, scope, dynamic, caseSensitive);
        return updateKeywordLink(bean);        
    }

    @Override
    public Boolean updateKeywordLink(KeywordLink keywordLink) throws BaseException
    {
        log.finer("BEGIN");

        String guid = keywordLink.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "KeywordLink object is invalid.");
        	throw new BaseException("KeywordLink object is invalid.");
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsUpdateKeywordLink-" + guid;
        String taskName = "RsUpdateKeywordLink-" + guid + "-" + (new Date()).getTime();
        KeywordLinkStub stub = MarshalHelper.convertKeywordLinkToStub(keywordLink);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(KeywordLinkStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = keywordLink.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    KeywordLinkStub dummyStub = new KeywordLinkStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateKeywordLink(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "keywordLinks/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateKeywordLink(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "keywordLinks/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteKeywordLink(String guid) throws BaseException
    {
        log.finer("BEGIN");

        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsDeleteKeywordLink-" + guid;
        String taskName = "RsDeleteKeywordLink-" + guid + "-" + (new Date()).getTime();
        TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "keywordLinks/" + guid).method(Method.DELETE).taskName(taskName);
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteKeywordLink(KeywordLink keywordLink) throws BaseException
    {
        String guid = keywordLink.getGuid();
        return deleteKeywordLink(guid);
    }

    @Override
    public Long deleteKeywordLinks(String filter, String params, List<String> values) throws BaseException
    {
        return remoteProxy.deleteKeywordLinks(filter, params, values);
    }

}
