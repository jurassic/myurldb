package com.cannyurl.af.proxy.async;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.LinkMessage;
import com.myurldb.ws.stub.ErrorStub;
import com.myurldb.ws.stub.LinkMessageStub;
import com.myurldb.ws.stub.LinkMessageListStub;
import com.cannyurl.af.util.MarshalHelper;
import com.cannyurl.af.bean.LinkMessageBean;
import com.myurldb.ws.service.LinkMessageService;
import com.cannyurl.af.proxy.LinkMessageServiceProxy;
import com.cannyurl.af.proxy.remote.RemoteLinkMessageServiceProxy;


// Implements the asynchronous call logic (using GAE task queue).
// GET:             Directly calls the remoteProxy interface.
// POST/PUT/DELETE: Creates a task and puts in a queue.
public class AsyncLinkMessageServiceProxy extends BaseAsyncServiceProxy implements LinkMessageServiceProxy
{
    private static final Logger log = Logger.getLogger(AsyncLinkMessageServiceProxy.class.getName());

    // Actual implementation is delegated to the corresponding remote proxy class.
    private RemoteLinkMessageServiceProxy remoteProxy;

    public AsyncLinkMessageServiceProxy()
    {
        remoteProxy = new RemoteLinkMessageServiceProxy();
    }

    @Override
    public LinkMessage getLinkMessage(String guid) throws BaseException
    {
        return remoteProxy.getLinkMessage(guid);
    }

    @Override
    public Object getLinkMessage(String guid, String field) throws BaseException
    {
        return remoteProxy.getLinkMessage(guid, field);       
    }

    @Override
    public List<LinkMessage> getLinkMessages(List<String> guids) throws BaseException
    {
        return remoteProxy.getLinkMessages(guids);
    }

    @Override
    public List<LinkMessage> getAllLinkMessages() throws BaseException
    {
        return getAllLinkMessages(null, null, null);
    }

    @Override
    public List<LinkMessage> getAllLinkMessages(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllLinkMessages(ordering, offset, count);
    }

    @Override
    public List<String> getAllLinkMessageKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllLinkMessageKeys(ordering, offset, count);
    }

    @Override
    public List<LinkMessage> findLinkMessages(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findLinkMessages(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<LinkMessage> findLinkMessages(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findLinkMessages(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findLinkMessageKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findLinkMessageKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return remoteProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createLinkMessage(String shortLink, String message, String password) throws BaseException
    {
        LinkMessageBean bean = new LinkMessageBean(null, shortLink, message, password);
        return createLinkMessage(bean);        
    }

    @Override
    public String createLinkMessage(LinkMessage linkMessage) throws BaseException
    {
        log.finer("BEGIN");

        String guid = linkMessage.getGuid();
        if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
            guid = GUID.generate();
            ((LinkMessageBean) linkMessage).setGuid(guid);
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsCreateLinkMessage-" + guid;
        String taskName = "RsCreateLinkMessage-" + guid + "-" + (new Date()).getTime();
        LinkMessageStub stub = MarshalHelper.convertLinkMessageToStub(linkMessage);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(LinkMessageStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = linkMessage.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    LinkMessageStub dummyStub = new LinkMessageStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createLinkMessage(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "linkMessages/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createLinkMessage(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "linkMessages/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;  // ???
    }

    @Override
    public Boolean updateLinkMessage(String guid, String shortLink, String message, String password) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "LinkMessage guid is invalid.");
        	throw new BaseException("LinkMessage guid is invalid.");
        }
        LinkMessageBean bean = new LinkMessageBean(guid, shortLink, message, password);
        return updateLinkMessage(bean);        
    }

    @Override
    public Boolean updateLinkMessage(LinkMessage linkMessage) throws BaseException
    {
        log.finer("BEGIN");

        String guid = linkMessage.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "LinkMessage object is invalid.");
        	throw new BaseException("LinkMessage object is invalid.");
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsUpdateLinkMessage-" + guid;
        String taskName = "RsUpdateLinkMessage-" + guid + "-" + (new Date()).getTime();
        LinkMessageStub stub = MarshalHelper.convertLinkMessageToStub(linkMessage);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(LinkMessageStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = linkMessage.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    LinkMessageStub dummyStub = new LinkMessageStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateLinkMessage(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "linkMessages/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateLinkMessage(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "linkMessages/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteLinkMessage(String guid) throws BaseException
    {
        log.finer("BEGIN");

        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsDeleteLinkMessage-" + guid;
        String taskName = "RsDeleteLinkMessage-" + guid + "-" + (new Date()).getTime();
        TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "linkMessages/" + guid).method(Method.DELETE).taskName(taskName);
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteLinkMessage(LinkMessage linkMessage) throws BaseException
    {
        String guid = linkMessage.getGuid();
        return deleteLinkMessage(guid);
    }

    @Override
    public Long deleteLinkMessages(String filter, String params, List<String> values) throws BaseException
    {
        return remoteProxy.deleteLinkMessages(filter, params, values);
    }

}
