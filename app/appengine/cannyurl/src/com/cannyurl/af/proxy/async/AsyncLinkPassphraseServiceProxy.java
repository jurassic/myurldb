package com.cannyurl.af.proxy.async;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.LinkPassphrase;
import com.myurldb.ws.stub.ErrorStub;
import com.myurldb.ws.stub.LinkPassphraseStub;
import com.myurldb.ws.stub.LinkPassphraseListStub;
import com.cannyurl.af.util.MarshalHelper;
import com.cannyurl.af.bean.LinkPassphraseBean;
import com.myurldb.ws.service.LinkPassphraseService;
import com.cannyurl.af.proxy.LinkPassphraseServiceProxy;
import com.cannyurl.af.proxy.remote.RemoteLinkPassphraseServiceProxy;


// Implements the asynchronous call logic (using GAE task queue).
// GET:             Directly calls the remoteProxy interface.
// POST/PUT/DELETE: Creates a task and puts in a queue.
public class AsyncLinkPassphraseServiceProxy extends BaseAsyncServiceProxy implements LinkPassphraseServiceProxy
{
    private static final Logger log = Logger.getLogger(AsyncLinkPassphraseServiceProxy.class.getName());

    // Actual implementation is delegated to the corresponding remote proxy class.
    private RemoteLinkPassphraseServiceProxy remoteProxy;

    public AsyncLinkPassphraseServiceProxy()
    {
        remoteProxy = new RemoteLinkPassphraseServiceProxy();
    }

    @Override
    public LinkPassphrase getLinkPassphrase(String guid) throws BaseException
    {
        return remoteProxy.getLinkPassphrase(guid);
    }

    @Override
    public Object getLinkPassphrase(String guid, String field) throws BaseException
    {
        return remoteProxy.getLinkPassphrase(guid, field);       
    }

    @Override
    public List<LinkPassphrase> getLinkPassphrases(List<String> guids) throws BaseException
    {
        return remoteProxy.getLinkPassphrases(guids);
    }

    @Override
    public List<LinkPassphrase> getAllLinkPassphrases() throws BaseException
    {
        return getAllLinkPassphrases(null, null, null);
    }

    @Override
    public List<LinkPassphrase> getAllLinkPassphrases(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllLinkPassphrases(ordering, offset, count);
    }

    @Override
    public List<String> getAllLinkPassphraseKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllLinkPassphraseKeys(ordering, offset, count);
    }

    @Override
    public List<LinkPassphrase> findLinkPassphrases(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findLinkPassphrases(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<LinkPassphrase> findLinkPassphrases(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findLinkPassphrases(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findLinkPassphraseKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findLinkPassphraseKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return remoteProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createLinkPassphrase(String shortLink, String passphrase) throws BaseException
    {
        LinkPassphraseBean bean = new LinkPassphraseBean(null, shortLink, passphrase);
        return createLinkPassphrase(bean);        
    }

    @Override
    public String createLinkPassphrase(LinkPassphrase linkPassphrase) throws BaseException
    {
        log.finer("BEGIN");

        String guid = linkPassphrase.getGuid();
        if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
            guid = GUID.generate();
            ((LinkPassphraseBean) linkPassphrase).setGuid(guid);
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsCreateLinkPassphrase-" + guid;
        String taskName = "RsCreateLinkPassphrase-" + guid + "-" + (new Date()).getTime();
        LinkPassphraseStub stub = MarshalHelper.convertLinkPassphraseToStub(linkPassphrase);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(LinkPassphraseStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = linkPassphrase.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    LinkPassphraseStub dummyStub = new LinkPassphraseStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createLinkPassphrase(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "linkPassphrases/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createLinkPassphrase(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "linkPassphrases/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;  // ???
    }

    @Override
    public Boolean updateLinkPassphrase(String guid, String shortLink, String passphrase) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "LinkPassphrase guid is invalid.");
        	throw new BaseException("LinkPassphrase guid is invalid.");
        }
        LinkPassphraseBean bean = new LinkPassphraseBean(guid, shortLink, passphrase);
        return updateLinkPassphrase(bean);        
    }

    @Override
    public Boolean updateLinkPassphrase(LinkPassphrase linkPassphrase) throws BaseException
    {
        log.finer("BEGIN");

        String guid = linkPassphrase.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "LinkPassphrase object is invalid.");
        	throw new BaseException("LinkPassphrase object is invalid.");
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsUpdateLinkPassphrase-" + guid;
        String taskName = "RsUpdateLinkPassphrase-" + guid + "-" + (new Date()).getTime();
        LinkPassphraseStub stub = MarshalHelper.convertLinkPassphraseToStub(linkPassphrase);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(LinkPassphraseStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = linkPassphrase.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    LinkPassphraseStub dummyStub = new LinkPassphraseStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateLinkPassphrase(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "linkPassphrases/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateLinkPassphrase(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "linkPassphrases/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteLinkPassphrase(String guid) throws BaseException
    {
        log.finer("BEGIN");

        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsDeleteLinkPassphrase-" + guid;
        String taskName = "RsDeleteLinkPassphrase-" + guid + "-" + (new Date()).getTime();
        TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "linkPassphrases/" + guid).method(Method.DELETE).taskName(taskName);
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteLinkPassphrase(LinkPassphrase linkPassphrase) throws BaseException
    {
        String guid = linkPassphrase.getGuid();
        return deleteLinkPassphrase(guid);
    }

    @Override
    public Long deleteLinkPassphrases(String filter, String params, List<String> values) throws BaseException
    {
        return remoteProxy.deleteLinkPassphrases(filter, params, values);
    }

}
