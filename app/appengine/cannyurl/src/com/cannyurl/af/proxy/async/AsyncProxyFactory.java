package com.cannyurl.af.proxy.async;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.ApiConsumerServiceProxy;
import com.cannyurl.af.proxy.AppCustomDomainServiceProxy;
import com.cannyurl.af.proxy.SiteCustomDomainServiceProxy;
import com.cannyurl.af.proxy.UserServiceProxy;
import com.cannyurl.af.proxy.UserUsercodeServiceProxy;
import com.cannyurl.af.proxy.UserPasswordServiceProxy;
import com.cannyurl.af.proxy.ExternalUserAuthServiceProxy;
import com.cannyurl.af.proxy.UserAuthStateServiceProxy;
import com.cannyurl.af.proxy.UserResourcePermissionServiceProxy;
import com.cannyurl.af.proxy.UserResourceProhibitionServiceProxy;
import com.cannyurl.af.proxy.RolePermissionServiceProxy;
import com.cannyurl.af.proxy.UserRoleServiceProxy;
import com.cannyurl.af.proxy.AppClientServiceProxy;
import com.cannyurl.af.proxy.ClientUserServiceProxy;
import com.cannyurl.af.proxy.UserCustomDomainServiceProxy;
import com.cannyurl.af.proxy.ClientSettingServiceProxy;
import com.cannyurl.af.proxy.UserSettingServiceProxy;
import com.cannyurl.af.proxy.VisitorSettingServiceProxy;
import com.cannyurl.af.proxy.TwitterSummaryCardServiceProxy;
import com.cannyurl.af.proxy.TwitterPhotoCardServiceProxy;
import com.cannyurl.af.proxy.TwitterGalleryCardServiceProxy;
import com.cannyurl.af.proxy.TwitterAppCardServiceProxy;
import com.cannyurl.af.proxy.TwitterPlayerCardServiceProxy;
import com.cannyurl.af.proxy.TwitterProductCardServiceProxy;
import com.cannyurl.af.proxy.ShortPassageServiceProxy;
import com.cannyurl.af.proxy.ShortLinkServiceProxy;
import com.cannyurl.af.proxy.GeoLinkServiceProxy;
import com.cannyurl.af.proxy.QrCodeServiceProxy;
import com.cannyurl.af.proxy.LinkPassphraseServiceProxy;
import com.cannyurl.af.proxy.LinkMessageServiceProxy;
import com.cannyurl.af.proxy.LinkAlbumServiceProxy;
import com.cannyurl.af.proxy.AlbumShortLinkServiceProxy;
import com.cannyurl.af.proxy.KeywordFolderServiceProxy;
import com.cannyurl.af.proxy.BookmarkFolderServiceProxy;
import com.cannyurl.af.proxy.KeywordLinkServiceProxy;
import com.cannyurl.af.proxy.BookmarkLinkServiceProxy;
import com.cannyurl.af.proxy.SpeedDialServiceProxy;
import com.cannyurl.af.proxy.KeywordFolderImportServiceProxy;
import com.cannyurl.af.proxy.BookmarkFolderImportServiceProxy;
import com.cannyurl.af.proxy.KeywordCrowdTallyServiceProxy;
import com.cannyurl.af.proxy.BookmarkCrowdTallyServiceProxy;
import com.cannyurl.af.proxy.DomainInfoServiceProxy;
import com.cannyurl.af.proxy.UrlRatingServiceProxy;
import com.cannyurl.af.proxy.UserRatingServiceProxy;
import com.cannyurl.af.proxy.AbuseTagServiceProxy;
import com.cannyurl.af.proxy.ServiceInfoServiceProxy;
import com.cannyurl.af.proxy.FiveTenServiceProxy;

public class AsyncProxyFactory extends AbstractProxyFactory
{
    private static final Logger log = Logger.getLogger(AsyncProxyFactory.class.getName());

    private AsyncProxyFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class AsyncProxyFactoryHolder
    {
        private static final AsyncProxyFactory INSTANCE = new AsyncProxyFactory();
    }

    // Singleton method
    public static AsyncProxyFactory getInstance()
    {
        return AsyncProxyFactoryHolder.INSTANCE;
    }

    @Override
    public ApiConsumerServiceProxy getApiConsumerServiceProxy()
    {
        return new AsyncApiConsumerServiceProxy();
    }

    @Override
    public AppCustomDomainServiceProxy getAppCustomDomainServiceProxy()
    {
        return new AsyncAppCustomDomainServiceProxy();
    }

    @Override
    public SiteCustomDomainServiceProxy getSiteCustomDomainServiceProxy()
    {
        return new AsyncSiteCustomDomainServiceProxy();
    }

    @Override
    public UserServiceProxy getUserServiceProxy()
    {
        return new AsyncUserServiceProxy();
    }

    @Override
    public UserUsercodeServiceProxy getUserUsercodeServiceProxy()
    {
        return new AsyncUserUsercodeServiceProxy();
    }

    @Override
    public UserPasswordServiceProxy getUserPasswordServiceProxy()
    {
        return new AsyncUserPasswordServiceProxy();
    }

    @Override
    public ExternalUserAuthServiceProxy getExternalUserAuthServiceProxy()
    {
        return new AsyncExternalUserAuthServiceProxy();
    }

    @Override
    public UserAuthStateServiceProxy getUserAuthStateServiceProxy()
    {
        return new AsyncUserAuthStateServiceProxy();
    }

    @Override
    public UserResourcePermissionServiceProxy getUserResourcePermissionServiceProxy()
    {
        return new AsyncUserResourcePermissionServiceProxy();
    }

    @Override
    public UserResourceProhibitionServiceProxy getUserResourceProhibitionServiceProxy()
    {
        return new AsyncUserResourceProhibitionServiceProxy();
    }

    @Override
    public RolePermissionServiceProxy getRolePermissionServiceProxy()
    {
        return new AsyncRolePermissionServiceProxy();
    }

    @Override
    public UserRoleServiceProxy getUserRoleServiceProxy()
    {
        return new AsyncUserRoleServiceProxy();
    }

    @Override
    public AppClientServiceProxy getAppClientServiceProxy()
    {
        return new AsyncAppClientServiceProxy();
    }

    @Override
    public ClientUserServiceProxy getClientUserServiceProxy()
    {
        return new AsyncClientUserServiceProxy();
    }

    @Override
    public UserCustomDomainServiceProxy getUserCustomDomainServiceProxy()
    {
        return new AsyncUserCustomDomainServiceProxy();
    }

    @Override
    public ClientSettingServiceProxy getClientSettingServiceProxy()
    {
        return new AsyncClientSettingServiceProxy();
    }

    @Override
    public UserSettingServiceProxy getUserSettingServiceProxy()
    {
        return new AsyncUserSettingServiceProxy();
    }

    @Override
    public VisitorSettingServiceProxy getVisitorSettingServiceProxy()
    {
        return new AsyncVisitorSettingServiceProxy();
    }

    @Override
    public TwitterSummaryCardServiceProxy getTwitterSummaryCardServiceProxy()
    {
        return new AsyncTwitterSummaryCardServiceProxy();
    }

    @Override
    public TwitterPhotoCardServiceProxy getTwitterPhotoCardServiceProxy()
    {
        return new AsyncTwitterPhotoCardServiceProxy();
    }

    @Override
    public TwitterGalleryCardServiceProxy getTwitterGalleryCardServiceProxy()
    {
        return new AsyncTwitterGalleryCardServiceProxy();
    }

    @Override
    public TwitterAppCardServiceProxy getTwitterAppCardServiceProxy()
    {
        return new AsyncTwitterAppCardServiceProxy();
    }

    @Override
    public TwitterPlayerCardServiceProxy getTwitterPlayerCardServiceProxy()
    {
        return new AsyncTwitterPlayerCardServiceProxy();
    }

    @Override
    public TwitterProductCardServiceProxy getTwitterProductCardServiceProxy()
    {
        return new AsyncTwitterProductCardServiceProxy();
    }

    @Override
    public ShortPassageServiceProxy getShortPassageServiceProxy()
    {
        return new AsyncShortPassageServiceProxy();
    }

    @Override
    public ShortLinkServiceProxy getShortLinkServiceProxy()
    {
        return new AsyncShortLinkServiceProxy();
    }

    @Override
    public GeoLinkServiceProxy getGeoLinkServiceProxy()
    {
        return new AsyncGeoLinkServiceProxy();
    }

    @Override
    public QrCodeServiceProxy getQrCodeServiceProxy()
    {
        return new AsyncQrCodeServiceProxy();
    }

    @Override
    public LinkPassphraseServiceProxy getLinkPassphraseServiceProxy()
    {
        return new AsyncLinkPassphraseServiceProxy();
    }

    @Override
    public LinkMessageServiceProxy getLinkMessageServiceProxy()
    {
        return new AsyncLinkMessageServiceProxy();
    }

    @Override
    public LinkAlbumServiceProxy getLinkAlbumServiceProxy()
    {
        return new AsyncLinkAlbumServiceProxy();
    }

    @Override
    public AlbumShortLinkServiceProxy getAlbumShortLinkServiceProxy()
    {
        return new AsyncAlbumShortLinkServiceProxy();
    }

    @Override
    public KeywordFolderServiceProxy getKeywordFolderServiceProxy()
    {
        return new AsyncKeywordFolderServiceProxy();
    }

    @Override
    public BookmarkFolderServiceProxy getBookmarkFolderServiceProxy()
    {
        return new AsyncBookmarkFolderServiceProxy();
    }

    @Override
    public KeywordLinkServiceProxy getKeywordLinkServiceProxy()
    {
        return new AsyncKeywordLinkServiceProxy();
    }

    @Override
    public BookmarkLinkServiceProxy getBookmarkLinkServiceProxy()
    {
        return new AsyncBookmarkLinkServiceProxy();
    }

    @Override
    public SpeedDialServiceProxy getSpeedDialServiceProxy()
    {
        return new AsyncSpeedDialServiceProxy();
    }

    @Override
    public KeywordFolderImportServiceProxy getKeywordFolderImportServiceProxy()
    {
        return new AsyncKeywordFolderImportServiceProxy();
    }

    @Override
    public BookmarkFolderImportServiceProxy getBookmarkFolderImportServiceProxy()
    {
        return new AsyncBookmarkFolderImportServiceProxy();
    }

    @Override
    public KeywordCrowdTallyServiceProxy getKeywordCrowdTallyServiceProxy()
    {
        return new AsyncKeywordCrowdTallyServiceProxy();
    }

    @Override
    public BookmarkCrowdTallyServiceProxy getBookmarkCrowdTallyServiceProxy()
    {
        return new AsyncBookmarkCrowdTallyServiceProxy();
    }

    @Override
    public DomainInfoServiceProxy getDomainInfoServiceProxy()
    {
        return new AsyncDomainInfoServiceProxy();
    }

    @Override
    public UrlRatingServiceProxy getUrlRatingServiceProxy()
    {
        return new AsyncUrlRatingServiceProxy();
    }

    @Override
    public UserRatingServiceProxy getUserRatingServiceProxy()
    {
        return new AsyncUserRatingServiceProxy();
    }

    @Override
    public AbuseTagServiceProxy getAbuseTagServiceProxy()
    {
        return new AsyncAbuseTagServiceProxy();
    }

    @Override
    public ServiceInfoServiceProxy getServiceInfoServiceProxy()
    {
        return new AsyncServiceInfoServiceProxy();
    }

    @Override
    public FiveTenServiceProxy getFiveTenServiceProxy()
    {
        return new AsyncFiveTenServiceProxy();
    }

}
