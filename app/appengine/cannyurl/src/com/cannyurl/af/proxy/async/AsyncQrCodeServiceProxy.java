package com.cannyurl.af.proxy.async;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.QrCode;
import com.myurldb.ws.stub.ErrorStub;
import com.myurldb.ws.stub.QrCodeStub;
import com.myurldb.ws.stub.QrCodeListStub;
import com.cannyurl.af.util.MarshalHelper;
import com.cannyurl.af.bean.QrCodeBean;
import com.myurldb.ws.service.QrCodeService;
import com.cannyurl.af.proxy.QrCodeServiceProxy;
import com.cannyurl.af.proxy.remote.RemoteQrCodeServiceProxy;


// Implements the asynchronous call logic (using GAE task queue).
// GET:             Directly calls the remoteProxy interface.
// POST/PUT/DELETE: Creates a task and puts in a queue.
public class AsyncQrCodeServiceProxy extends BaseAsyncServiceProxy implements QrCodeServiceProxy
{
    private static final Logger log = Logger.getLogger(AsyncQrCodeServiceProxy.class.getName());

    // Actual implementation is delegated to the corresponding remote proxy class.
    private RemoteQrCodeServiceProxy remoteProxy;

    public AsyncQrCodeServiceProxy()
    {
        remoteProxy = new RemoteQrCodeServiceProxy();
    }

    @Override
    public QrCode getQrCode(String guid) throws BaseException
    {
        return remoteProxy.getQrCode(guid);
    }

    @Override
    public Object getQrCode(String guid, String field) throws BaseException
    {
        return remoteProxy.getQrCode(guid, field);       
    }

    @Override
    public List<QrCode> getQrCodes(List<String> guids) throws BaseException
    {
        return remoteProxy.getQrCodes(guids);
    }

    @Override
    public List<QrCode> getAllQrCodes() throws BaseException
    {
        return getAllQrCodes(null, null, null);
    }

    @Override
    public List<QrCode> getAllQrCodes(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllQrCodes(ordering, offset, count);
    }

    @Override
    public List<String> getAllQrCodeKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllQrCodeKeys(ordering, offset, count);
    }

    @Override
    public List<QrCode> findQrCodes(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findQrCodes(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<QrCode> findQrCodes(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findQrCodes(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findQrCodeKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findQrCodeKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return remoteProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createQrCode(String shortLink, String imageLink, String imageUrl, String type, String status) throws BaseException
    {
        QrCodeBean bean = new QrCodeBean(null, shortLink, imageLink, imageUrl, type, status);
        return createQrCode(bean);        
    }

    @Override
    public String createQrCode(QrCode qrCode) throws BaseException
    {
        log.finer("BEGIN");

        String guid = qrCode.getGuid();
        if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
            guid = GUID.generate();
            ((QrCodeBean) qrCode).setGuid(guid);
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsCreateQrCode-" + guid;
        String taskName = "RsCreateQrCode-" + guid + "-" + (new Date()).getTime();
        QrCodeStub stub = MarshalHelper.convertQrCodeToStub(qrCode);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(QrCodeStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = qrCode.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    QrCodeStub dummyStub = new QrCodeStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createQrCode(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "qrCodes/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createQrCode(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "qrCodes/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;  // ???
    }

    @Override
    public Boolean updateQrCode(String guid, String shortLink, String imageLink, String imageUrl, String type, String status) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "QrCode guid is invalid.");
        	throw new BaseException("QrCode guid is invalid.");
        }
        QrCodeBean bean = new QrCodeBean(guid, shortLink, imageLink, imageUrl, type, status);
        return updateQrCode(bean);        
    }

    @Override
    public Boolean updateQrCode(QrCode qrCode) throws BaseException
    {
        log.finer("BEGIN");

        String guid = qrCode.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "QrCode object is invalid.");
        	throw new BaseException("QrCode object is invalid.");
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsUpdateQrCode-" + guid;
        String taskName = "RsUpdateQrCode-" + guid + "-" + (new Date()).getTime();
        QrCodeStub stub = MarshalHelper.convertQrCodeToStub(qrCode);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(QrCodeStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = qrCode.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    QrCodeStub dummyStub = new QrCodeStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateQrCode(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "qrCodes/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateQrCode(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "qrCodes/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteQrCode(String guid) throws BaseException
    {
        log.finer("BEGIN");

        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsDeleteQrCode-" + guid;
        String taskName = "RsDeleteQrCode-" + guid + "-" + (new Date()).getTime();
        TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "qrCodes/" + guid).method(Method.DELETE).taskName(taskName);
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteQrCode(QrCode qrCode) throws BaseException
    {
        String guid = qrCode.getGuid();
        return deleteQrCode(guid);
    }

    @Override
    public Long deleteQrCodes(String filter, String params, List<String> values) throws BaseException
    {
        return remoteProxy.deleteQrCodes(filter, params, values);
    }

}
