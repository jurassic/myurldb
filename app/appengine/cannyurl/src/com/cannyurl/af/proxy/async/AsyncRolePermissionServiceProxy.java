package com.cannyurl.af.proxy.async;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.RolePermission;
import com.myurldb.ws.stub.ErrorStub;
import com.myurldb.ws.stub.RolePermissionStub;
import com.myurldb.ws.stub.RolePermissionListStub;
import com.cannyurl.af.util.MarshalHelper;
import com.cannyurl.af.bean.RolePermissionBean;
import com.myurldb.ws.service.RolePermissionService;
import com.cannyurl.af.proxy.RolePermissionServiceProxy;
import com.cannyurl.af.proxy.remote.RemoteRolePermissionServiceProxy;


// Implements the asynchronous call logic (using GAE task queue).
// GET:             Directly calls the remoteProxy interface.
// POST/PUT/DELETE: Creates a task and puts in a queue.
public class AsyncRolePermissionServiceProxy extends BaseAsyncServiceProxy implements RolePermissionServiceProxy
{
    private static final Logger log = Logger.getLogger(AsyncRolePermissionServiceProxy.class.getName());

    // Actual implementation is delegated to the corresponding remote proxy class.
    private RemoteRolePermissionServiceProxy remoteProxy;

    public AsyncRolePermissionServiceProxy()
    {
        remoteProxy = new RemoteRolePermissionServiceProxy();
    }

    @Override
    public RolePermission getRolePermission(String guid) throws BaseException
    {
        return remoteProxy.getRolePermission(guid);
    }

    @Override
    public Object getRolePermission(String guid, String field) throws BaseException
    {
        return remoteProxy.getRolePermission(guid, field);       
    }

    @Override
    public List<RolePermission> getRolePermissions(List<String> guids) throws BaseException
    {
        return remoteProxy.getRolePermissions(guids);
    }

    @Override
    public List<RolePermission> getAllRolePermissions() throws BaseException
    {
        return getAllRolePermissions(null, null, null);
    }

    @Override
    public List<RolePermission> getAllRolePermissions(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllRolePermissions(ordering, offset, count);
    }

    @Override
    public List<String> getAllRolePermissionKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllRolePermissionKeys(ordering, offset, count);
    }

    @Override
    public List<RolePermission> findRolePermissions(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findRolePermissions(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<RolePermission> findRolePermissions(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findRolePermissions(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findRolePermissionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findRolePermissionKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return remoteProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createRolePermission(String role, String permissionName, String resource, String instance, String action, String status) throws BaseException
    {
        RolePermissionBean bean = new RolePermissionBean(null, role, permissionName, resource, instance, action, status);
        return createRolePermission(bean);        
    }

    @Override
    public String createRolePermission(RolePermission rolePermission) throws BaseException
    {
        log.finer("BEGIN");

        String guid = rolePermission.getGuid();
        if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
            guid = GUID.generate();
            ((RolePermissionBean) rolePermission).setGuid(guid);
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsCreateRolePermission-" + guid;
        String taskName = "RsCreateRolePermission-" + guid + "-" + (new Date()).getTime();
        RolePermissionStub stub = MarshalHelper.convertRolePermissionToStub(rolePermission);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(RolePermissionStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = rolePermission.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    RolePermissionStub dummyStub = new RolePermissionStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createRolePermission(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "rolePermissions/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createRolePermission(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "rolePermissions/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;  // ???
    }

    @Override
    public Boolean updateRolePermission(String guid, String role, String permissionName, String resource, String instance, String action, String status) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "RolePermission guid is invalid.");
        	throw new BaseException("RolePermission guid is invalid.");
        }
        RolePermissionBean bean = new RolePermissionBean(guid, role, permissionName, resource, instance, action, status);
        return updateRolePermission(bean);        
    }

    @Override
    public Boolean updateRolePermission(RolePermission rolePermission) throws BaseException
    {
        log.finer("BEGIN");

        String guid = rolePermission.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "RolePermission object is invalid.");
        	throw new BaseException("RolePermission object is invalid.");
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsUpdateRolePermission-" + guid;
        String taskName = "RsUpdateRolePermission-" + guid + "-" + (new Date()).getTime();
        RolePermissionStub stub = MarshalHelper.convertRolePermissionToStub(rolePermission);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(RolePermissionStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = rolePermission.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    RolePermissionStub dummyStub = new RolePermissionStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateRolePermission(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "rolePermissions/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateRolePermission(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "rolePermissions/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteRolePermission(String guid) throws BaseException
    {
        log.finer("BEGIN");

        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsDeleteRolePermission-" + guid;
        String taskName = "RsDeleteRolePermission-" + guid + "-" + (new Date()).getTime();
        TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "rolePermissions/" + guid).method(Method.DELETE).taskName(taskName);
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteRolePermission(RolePermission rolePermission) throws BaseException
    {
        String guid = rolePermission.getGuid();
        return deleteRolePermission(guid);
    }

    @Override
    public Long deleteRolePermissions(String filter, String params, List<String> values) throws BaseException
    {
        return remoteProxy.deleteRolePermissions(filter, params, values);
    }

}
