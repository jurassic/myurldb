package com.cannyurl.af.proxy.async;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.ShortPassageAttribute;
import com.myurldb.ws.ShortPassage;
import com.myurldb.ws.stub.ErrorStub;
import com.myurldb.ws.stub.ShortPassageAttributeStub;
import com.myurldb.ws.stub.ShortPassageAttributeListStub;
import com.myurldb.ws.stub.ShortPassageStub;
import com.myurldb.ws.stub.ShortPassageListStub;
import com.cannyurl.af.util.MarshalHelper;
import com.cannyurl.af.bean.ShortPassageAttributeBean;
import com.cannyurl.af.bean.ShortPassageBean;
import com.myurldb.ws.service.ShortPassageService;
import com.cannyurl.af.proxy.ShortPassageServiceProxy;
import com.cannyurl.af.proxy.remote.RemoteShortPassageServiceProxy;


// Implements the asynchronous call logic (using GAE task queue).
// GET:             Directly calls the remoteProxy interface.
// POST/PUT/DELETE: Creates a task and puts in a queue.
public class AsyncShortPassageServiceProxy extends BaseAsyncServiceProxy implements ShortPassageServiceProxy
{
    private static final Logger log = Logger.getLogger(AsyncShortPassageServiceProxy.class.getName());

    // Actual implementation is delegated to the corresponding remote proxy class.
    private RemoteShortPassageServiceProxy remoteProxy;

    public AsyncShortPassageServiceProxy()
    {
        remoteProxy = new RemoteShortPassageServiceProxy();
    }

    @Override
    public ShortPassage getShortPassage(String guid) throws BaseException
    {
        return remoteProxy.getShortPassage(guid);
    }

    @Override
    public Object getShortPassage(String guid, String field) throws BaseException
    {
        return remoteProxy.getShortPassage(guid, field);       
    }

    @Override
    public List<ShortPassage> getShortPassages(List<String> guids) throws BaseException
    {
        return remoteProxy.getShortPassages(guids);
    }

    @Override
    public List<ShortPassage> getAllShortPassages() throws BaseException
    {
        return getAllShortPassages(null, null, null);
    }

    @Override
    public List<ShortPassage> getAllShortPassages(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllShortPassages(ordering, offset, count);
    }

    @Override
    public List<String> getAllShortPassageKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllShortPassageKeys(ordering, offset, count);
    }

    @Override
    public List<ShortPassage> findShortPassages(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findShortPassages(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<ShortPassage> findShortPassages(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findShortPassages(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findShortPassageKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findShortPassageKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return remoteProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createShortPassage(String owner, String longText, String shortText, ShortPassageAttribute attribute, Boolean readOnly, String status, String note, Long expirationTime) throws BaseException
    {
        ShortPassageBean bean = new ShortPassageBean(null, owner, longText, shortText, MarshalHelper.convertShortPassageAttributeToBean(attribute), readOnly, status, note, expirationTime);
        return createShortPassage(bean);        
    }

    @Override
    public String createShortPassage(ShortPassage shortPassage) throws BaseException
    {
        log.finer("BEGIN");

        String guid = shortPassage.getGuid();
        if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
            guid = GUID.generate();
            ((ShortPassageBean) shortPassage).setGuid(guid);
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsCreateShortPassage-" + guid;
        String taskName = "RsCreateShortPassage-" + guid + "-" + (new Date()).getTime();
        ShortPassageStub stub = MarshalHelper.convertShortPassageToStub(shortPassage);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(ShortPassageStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = shortPassage.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    ShortPassageStub dummyStub = new ShortPassageStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createShortPassage(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "shortPassages/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createShortPassage(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "shortPassages/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;  // ???
    }

    @Override
    public Boolean updateShortPassage(String guid, String owner, String longText, String shortText, ShortPassageAttribute attribute, Boolean readOnly, String status, String note, Long expirationTime) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "ShortPassage guid is invalid.");
        	throw new BaseException("ShortPassage guid is invalid.");
        }
        ShortPassageBean bean = new ShortPassageBean(guid, owner, longText, shortText, MarshalHelper.convertShortPassageAttributeToBean(attribute), readOnly, status, note, expirationTime);
        return updateShortPassage(bean);        
    }

    @Override
    public Boolean updateShortPassage(ShortPassage shortPassage) throws BaseException
    {
        log.finer("BEGIN");

        String guid = shortPassage.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "ShortPassage object is invalid.");
        	throw new BaseException("ShortPassage object is invalid.");
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsUpdateShortPassage-" + guid;
        String taskName = "RsUpdateShortPassage-" + guid + "-" + (new Date()).getTime();
        ShortPassageStub stub = MarshalHelper.convertShortPassageToStub(shortPassage);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(ShortPassageStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = shortPassage.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    ShortPassageStub dummyStub = new ShortPassageStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateShortPassage(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "shortPassages/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateShortPassage(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "shortPassages/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteShortPassage(String guid) throws BaseException
    {
        log.finer("BEGIN");

        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsDeleteShortPassage-" + guid;
        String taskName = "RsDeleteShortPassage-" + guid + "-" + (new Date()).getTime();
        TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "shortPassages/" + guid).method(Method.DELETE).taskName(taskName);
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteShortPassage(ShortPassage shortPassage) throws BaseException
    {
        String guid = shortPassage.getGuid();
        return deleteShortPassage(guid);
    }

    @Override
    public Long deleteShortPassages(String filter, String params, List<String> values) throws BaseException
    {
        return remoteProxy.deleteShortPassages(filter, params, values);
    }

}
