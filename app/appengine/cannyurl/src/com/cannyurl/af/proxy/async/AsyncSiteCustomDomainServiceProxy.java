package com.cannyurl.af.proxy.async;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.SiteCustomDomain;
import com.myurldb.ws.stub.ErrorStub;
import com.myurldb.ws.stub.SiteCustomDomainStub;
import com.myurldb.ws.stub.SiteCustomDomainListStub;
import com.cannyurl.af.util.MarshalHelper;
import com.cannyurl.af.bean.SiteCustomDomainBean;
import com.myurldb.ws.service.SiteCustomDomainService;
import com.cannyurl.af.proxy.SiteCustomDomainServiceProxy;
import com.cannyurl.af.proxy.remote.RemoteSiteCustomDomainServiceProxy;


// Implements the asynchronous call logic (using GAE task queue).
// GET:             Directly calls the remoteProxy interface.
// POST/PUT/DELETE: Creates a task and puts in a queue.
public class AsyncSiteCustomDomainServiceProxy extends BaseAsyncServiceProxy implements SiteCustomDomainServiceProxy
{
    private static final Logger log = Logger.getLogger(AsyncSiteCustomDomainServiceProxy.class.getName());

    // Actual implementation is delegated to the corresponding remote proxy class.
    private RemoteSiteCustomDomainServiceProxy remoteProxy;

    public AsyncSiteCustomDomainServiceProxy()
    {
        remoteProxy = new RemoteSiteCustomDomainServiceProxy();
    }

    @Override
    public SiteCustomDomain getSiteCustomDomain(String guid) throws BaseException
    {
        return remoteProxy.getSiteCustomDomain(guid);
    }

    @Override
    public Object getSiteCustomDomain(String guid, String field) throws BaseException
    {
        return remoteProxy.getSiteCustomDomain(guid, field);       
    }

    @Override
    public List<SiteCustomDomain> getSiteCustomDomains(List<String> guids) throws BaseException
    {
        return remoteProxy.getSiteCustomDomains(guids);
    }

    @Override
    public List<SiteCustomDomain> getAllSiteCustomDomains() throws BaseException
    {
        return getAllSiteCustomDomains(null, null, null);
    }

    @Override
    public List<SiteCustomDomain> getAllSiteCustomDomains(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllSiteCustomDomains(ordering, offset, count);
    }

    @Override
    public List<String> getAllSiteCustomDomainKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllSiteCustomDomainKeys(ordering, offset, count);
    }

    @Override
    public List<SiteCustomDomain> findSiteCustomDomains(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findSiteCustomDomains(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<SiteCustomDomain> findSiteCustomDomains(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findSiteCustomDomains(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findSiteCustomDomainKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findSiteCustomDomainKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return remoteProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createSiteCustomDomain(String siteDomain, String domain, String status) throws BaseException
    {
        SiteCustomDomainBean bean = new SiteCustomDomainBean(null, siteDomain, domain, status);
        return createSiteCustomDomain(bean);        
    }

    @Override
    public String createSiteCustomDomain(SiteCustomDomain siteCustomDomain) throws BaseException
    {
        log.finer("BEGIN");

        String guid = siteCustomDomain.getGuid();
        if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
            guid = GUID.generate();
            ((SiteCustomDomainBean) siteCustomDomain).setGuid(guid);
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsCreateSiteCustomDomain-" + guid;
        String taskName = "RsCreateSiteCustomDomain-" + guid + "-" + (new Date()).getTime();
        SiteCustomDomainStub stub = MarshalHelper.convertSiteCustomDomainToStub(siteCustomDomain);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(SiteCustomDomainStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = siteCustomDomain.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    SiteCustomDomainStub dummyStub = new SiteCustomDomainStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createSiteCustomDomain(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "siteCustomDomains/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createSiteCustomDomain(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "siteCustomDomains/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;  // ???
    }

    @Override
    public Boolean updateSiteCustomDomain(String guid, String siteDomain, String domain, String status) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "SiteCustomDomain guid is invalid.");
        	throw new BaseException("SiteCustomDomain guid is invalid.");
        }
        SiteCustomDomainBean bean = new SiteCustomDomainBean(guid, siteDomain, domain, status);
        return updateSiteCustomDomain(bean);        
    }

    @Override
    public Boolean updateSiteCustomDomain(SiteCustomDomain siteCustomDomain) throws BaseException
    {
        log.finer("BEGIN");

        String guid = siteCustomDomain.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "SiteCustomDomain object is invalid.");
        	throw new BaseException("SiteCustomDomain object is invalid.");
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsUpdateSiteCustomDomain-" + guid;
        String taskName = "RsUpdateSiteCustomDomain-" + guid + "-" + (new Date()).getTime();
        SiteCustomDomainStub stub = MarshalHelper.convertSiteCustomDomainToStub(siteCustomDomain);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(SiteCustomDomainStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = siteCustomDomain.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    SiteCustomDomainStub dummyStub = new SiteCustomDomainStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateSiteCustomDomain(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "siteCustomDomains/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateSiteCustomDomain(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "siteCustomDomains/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteSiteCustomDomain(String guid) throws BaseException
    {
        log.finer("BEGIN");

        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsDeleteSiteCustomDomain-" + guid;
        String taskName = "RsDeleteSiteCustomDomain-" + guid + "-" + (new Date()).getTime();
        TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "siteCustomDomains/" + guid).method(Method.DELETE).taskName(taskName);
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteSiteCustomDomain(SiteCustomDomain siteCustomDomain) throws BaseException
    {
        String guid = siteCustomDomain.getGuid();
        return deleteSiteCustomDomain(guid);
    }

    @Override
    public Long deleteSiteCustomDomains(String filter, String params, List<String> values) throws BaseException
    {
        return remoteProxy.deleteSiteCustomDomains(filter, params, values);
    }

}
