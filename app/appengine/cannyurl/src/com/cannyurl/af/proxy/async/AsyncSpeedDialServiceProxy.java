package com.cannyurl.af.proxy.async;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.SpeedDial;
import com.myurldb.ws.stub.ErrorStub;
import com.myurldb.ws.stub.SpeedDialStub;
import com.myurldb.ws.stub.SpeedDialListStub;
import com.cannyurl.af.util.MarshalHelper;
import com.cannyurl.af.bean.SpeedDialBean;
import com.myurldb.ws.service.SpeedDialService;
import com.cannyurl.af.proxy.SpeedDialServiceProxy;
import com.cannyurl.af.proxy.remote.RemoteSpeedDialServiceProxy;


// Implements the asynchronous call logic (using GAE task queue).
// GET:             Directly calls the remoteProxy interface.
// POST/PUT/DELETE: Creates a task and puts in a queue.
public class AsyncSpeedDialServiceProxy extends BaseAsyncServiceProxy implements SpeedDialServiceProxy
{
    private static final Logger log = Logger.getLogger(AsyncSpeedDialServiceProxy.class.getName());

    // Actual implementation is delegated to the corresponding remote proxy class.
    private RemoteSpeedDialServiceProxy remoteProxy;

    public AsyncSpeedDialServiceProxy()
    {
        remoteProxy = new RemoteSpeedDialServiceProxy();
    }

    @Override
    public SpeedDial getSpeedDial(String guid) throws BaseException
    {
        return remoteProxy.getSpeedDial(guid);
    }

    @Override
    public Object getSpeedDial(String guid, String field) throws BaseException
    {
        return remoteProxy.getSpeedDial(guid, field);       
    }

    @Override
    public List<SpeedDial> getSpeedDials(List<String> guids) throws BaseException
    {
        return remoteProxy.getSpeedDials(guids);
    }

    @Override
    public List<SpeedDial> getAllSpeedDials() throws BaseException
    {
        return getAllSpeedDials(null, null, null);
    }

    @Override
    public List<SpeedDial> getAllSpeedDials(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllSpeedDials(ordering, offset, count);
    }

    @Override
    public List<String> getAllSpeedDialKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllSpeedDialKeys(ordering, offset, count);
    }

    @Override
    public List<SpeedDial> findSpeedDials(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findSpeedDials(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<SpeedDial> findSpeedDials(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findSpeedDials(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findSpeedDialKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findSpeedDialKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return remoteProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createSpeedDial(String user, String code, String shortcut, String shortLink, String keywordLink, String bookmarkLink, String longUrl, String shortUrl, Boolean caseSensitive, String status, String note) throws BaseException
    {
        SpeedDialBean bean = new SpeedDialBean(null, user, code, shortcut, shortLink, keywordLink, bookmarkLink, longUrl, shortUrl, caseSensitive, status, note);
        return createSpeedDial(bean);        
    }

    @Override
    public String createSpeedDial(SpeedDial speedDial) throws BaseException
    {
        log.finer("BEGIN");

        String guid = speedDial.getGuid();
        if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
            guid = GUID.generate();
            ((SpeedDialBean) speedDial).setGuid(guid);
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsCreateSpeedDial-" + guid;
        String taskName = "RsCreateSpeedDial-" + guid + "-" + (new Date()).getTime();
        SpeedDialStub stub = MarshalHelper.convertSpeedDialToStub(speedDial);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(SpeedDialStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = speedDial.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    SpeedDialStub dummyStub = new SpeedDialStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createSpeedDial(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "speedDials/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createSpeedDial(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "speedDials/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;  // ???
    }

    @Override
    public Boolean updateSpeedDial(String guid, String user, String code, String shortcut, String shortLink, String keywordLink, String bookmarkLink, String longUrl, String shortUrl, Boolean caseSensitive, String status, String note) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "SpeedDial guid is invalid.");
        	throw new BaseException("SpeedDial guid is invalid.");
        }
        SpeedDialBean bean = new SpeedDialBean(guid, user, code, shortcut, shortLink, keywordLink, bookmarkLink, longUrl, shortUrl, caseSensitive, status, note);
        return updateSpeedDial(bean);        
    }

    @Override
    public Boolean updateSpeedDial(SpeedDial speedDial) throws BaseException
    {
        log.finer("BEGIN");

        String guid = speedDial.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "SpeedDial object is invalid.");
        	throw new BaseException("SpeedDial object is invalid.");
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsUpdateSpeedDial-" + guid;
        String taskName = "RsUpdateSpeedDial-" + guid + "-" + (new Date()).getTime();
        SpeedDialStub stub = MarshalHelper.convertSpeedDialToStub(speedDial);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(SpeedDialStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = speedDial.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    SpeedDialStub dummyStub = new SpeedDialStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateSpeedDial(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "speedDials/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateSpeedDial(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "speedDials/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteSpeedDial(String guid) throws BaseException
    {
        log.finer("BEGIN");

        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsDeleteSpeedDial-" + guid;
        String taskName = "RsDeleteSpeedDial-" + guid + "-" + (new Date()).getTime();
        TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "speedDials/" + guid).method(Method.DELETE).taskName(taskName);
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteSpeedDial(SpeedDial speedDial) throws BaseException
    {
        String guid = speedDial.getGuid();
        return deleteSpeedDial(guid);
    }

    @Override
    public Long deleteSpeedDials(String filter, String params, List<String> values) throws BaseException
    {
        return remoteProxy.deleteSpeedDials(filter, params, values);
    }

}
