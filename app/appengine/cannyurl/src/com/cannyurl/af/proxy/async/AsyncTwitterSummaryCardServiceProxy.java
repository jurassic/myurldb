package com.cannyurl.af.proxy.async;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.TwitterSummaryCard;
import com.myurldb.ws.stub.ErrorStub;
import com.myurldb.ws.stub.TwitterCardAppInfoStub;
import com.myurldb.ws.stub.TwitterCardAppInfoListStub;
import com.myurldb.ws.stub.TwitterCardProductDataStub;
import com.myurldb.ws.stub.TwitterCardProductDataListStub;
import com.myurldb.ws.stub.TwitterSummaryCardStub;
import com.myurldb.ws.stub.TwitterSummaryCardListStub;
import com.cannyurl.af.util.MarshalHelper;
import com.cannyurl.af.bean.TwitterCardAppInfoBean;
import com.cannyurl.af.bean.TwitterCardProductDataBean;
import com.cannyurl.af.bean.TwitterSummaryCardBean;
import com.myurldb.ws.service.TwitterSummaryCardService;
import com.cannyurl.af.proxy.TwitterSummaryCardServiceProxy;
import com.cannyurl.af.proxy.remote.RemoteTwitterSummaryCardServiceProxy;


// Implements the asynchronous call logic (using GAE task queue).
// GET:             Directly calls the remoteProxy interface.
// POST/PUT/DELETE: Creates a task and puts in a queue.
public class AsyncTwitterSummaryCardServiceProxy extends BaseAsyncServiceProxy implements TwitterSummaryCardServiceProxy
{
    private static final Logger log = Logger.getLogger(AsyncTwitterSummaryCardServiceProxy.class.getName());

    // Actual implementation is delegated to the corresponding remote proxy class.
    private RemoteTwitterSummaryCardServiceProxy remoteProxy;

    public AsyncTwitterSummaryCardServiceProxy()
    {
        remoteProxy = new RemoteTwitterSummaryCardServiceProxy();
    }

    @Override
    public TwitterSummaryCard getTwitterSummaryCard(String guid) throws BaseException
    {
        return remoteProxy.getTwitterSummaryCard(guid);
    }

    @Override
    public Object getTwitterSummaryCard(String guid, String field) throws BaseException
    {
        return remoteProxy.getTwitterSummaryCard(guid, field);       
    }

    @Override
    public List<TwitterSummaryCard> getTwitterSummaryCards(List<String> guids) throws BaseException
    {
        return remoteProxy.getTwitterSummaryCards(guids);
    }

    @Override
    public List<TwitterSummaryCard> getAllTwitterSummaryCards() throws BaseException
    {
        return getAllTwitterSummaryCards(null, null, null);
    }

    @Override
    public List<TwitterSummaryCard> getAllTwitterSummaryCards(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllTwitterSummaryCards(ordering, offset, count);
    }

    @Override
    public List<String> getAllTwitterSummaryCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllTwitterSummaryCardKeys(ordering, offset, count);
    }

    @Override
    public List<TwitterSummaryCard> findTwitterSummaryCards(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findTwitterSummaryCards(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<TwitterSummaryCard> findTwitterSummaryCards(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findTwitterSummaryCards(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findTwitterSummaryCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findTwitterSummaryCardKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return remoteProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createTwitterSummaryCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight) throws BaseException
    {
        TwitterSummaryCardBean bean = new TwitterSummaryCardBean(null, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight);
        return createTwitterSummaryCard(bean);        
    }

    @Override
    public String createTwitterSummaryCard(TwitterSummaryCard twitterSummaryCard) throws BaseException
    {
        log.finer("BEGIN");

        String guid = twitterSummaryCard.getGuid();
        if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
            guid = GUID.generate();
            ((TwitterSummaryCardBean) twitterSummaryCard).setGuid(guid);
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsCreateTwitterSummaryCard-" + guid;
        String taskName = "RsCreateTwitterSummaryCard-" + guid + "-" + (new Date()).getTime();
        TwitterSummaryCardStub stub = MarshalHelper.convertTwitterSummaryCardToStub(twitterSummaryCard);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(TwitterSummaryCardStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = twitterSummaryCard.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    TwitterSummaryCardStub dummyStub = new TwitterSummaryCardStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createTwitterSummaryCard(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "twitterSummaryCards/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createTwitterSummaryCard(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "twitterSummaryCards/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;  // ???
    }

    @Override
    public Boolean updateTwitterSummaryCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "TwitterSummaryCard guid is invalid.");
        	throw new BaseException("TwitterSummaryCard guid is invalid.");
        }
        TwitterSummaryCardBean bean = new TwitterSummaryCardBean(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight);
        return updateTwitterSummaryCard(bean);        
    }

    @Override
    public Boolean updateTwitterSummaryCard(TwitterSummaryCard twitterSummaryCard) throws BaseException
    {
        log.finer("BEGIN");

        String guid = twitterSummaryCard.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "TwitterSummaryCard object is invalid.");
        	throw new BaseException("TwitterSummaryCard object is invalid.");
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsUpdateTwitterSummaryCard-" + guid;
        String taskName = "RsUpdateTwitterSummaryCard-" + guid + "-" + (new Date()).getTime();
        TwitterSummaryCardStub stub = MarshalHelper.convertTwitterSummaryCardToStub(twitterSummaryCard);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(TwitterSummaryCardStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = twitterSummaryCard.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    TwitterSummaryCardStub dummyStub = new TwitterSummaryCardStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateTwitterSummaryCard(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "twitterSummaryCards/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateTwitterSummaryCard(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "twitterSummaryCards/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteTwitterSummaryCard(String guid) throws BaseException
    {
        log.finer("BEGIN");

        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsDeleteTwitterSummaryCard-" + guid;
        String taskName = "RsDeleteTwitterSummaryCard-" + guid + "-" + (new Date()).getTime();
        TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "twitterSummaryCards/" + guid).method(Method.DELETE).taskName(taskName);
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteTwitterSummaryCard(TwitterSummaryCard twitterSummaryCard) throws BaseException
    {
        String guid = twitterSummaryCard.getGuid();
        return deleteTwitterSummaryCard(guid);
    }

    @Override
    public Long deleteTwitterSummaryCards(String filter, String params, List<String> values) throws BaseException
    {
        return remoteProxy.deleteTwitterSummaryCards(filter, params, values);
    }

}
