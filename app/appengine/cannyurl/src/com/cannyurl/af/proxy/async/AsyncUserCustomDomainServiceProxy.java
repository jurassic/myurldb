package com.cannyurl.af.proxy.async;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.UserCustomDomain;
import com.myurldb.ws.stub.ErrorStub;
import com.myurldb.ws.stub.UserCustomDomainStub;
import com.myurldb.ws.stub.UserCustomDomainListStub;
import com.cannyurl.af.util.MarshalHelper;
import com.cannyurl.af.bean.UserCustomDomainBean;
import com.myurldb.ws.service.UserCustomDomainService;
import com.cannyurl.af.proxy.UserCustomDomainServiceProxy;
import com.cannyurl.af.proxy.remote.RemoteUserCustomDomainServiceProxy;


// Implements the asynchronous call logic (using GAE task queue).
// GET:             Directly calls the remoteProxy interface.
// POST/PUT/DELETE: Creates a task and puts in a queue.
public class AsyncUserCustomDomainServiceProxy extends BaseAsyncServiceProxy implements UserCustomDomainServiceProxy
{
    private static final Logger log = Logger.getLogger(AsyncUserCustomDomainServiceProxy.class.getName());

    // Actual implementation is delegated to the corresponding remote proxy class.
    private RemoteUserCustomDomainServiceProxy remoteProxy;

    public AsyncUserCustomDomainServiceProxy()
    {
        remoteProxy = new RemoteUserCustomDomainServiceProxy();
    }

    @Override
    public UserCustomDomain getUserCustomDomain(String guid) throws BaseException
    {
        return remoteProxy.getUserCustomDomain(guid);
    }

    @Override
    public Object getUserCustomDomain(String guid, String field) throws BaseException
    {
        return remoteProxy.getUserCustomDomain(guid, field);       
    }

    @Override
    public List<UserCustomDomain> getUserCustomDomains(List<String> guids) throws BaseException
    {
        return remoteProxy.getUserCustomDomains(guids);
    }

    @Override
    public List<UserCustomDomain> getAllUserCustomDomains() throws BaseException
    {
        return getAllUserCustomDomains(null, null, null);
    }

    @Override
    public List<UserCustomDomain> getAllUserCustomDomains(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllUserCustomDomains(ordering, offset, count);
    }

    @Override
    public List<String> getAllUserCustomDomainKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllUserCustomDomainKeys(ordering, offset, count);
    }

    @Override
    public List<UserCustomDomain> findUserCustomDomains(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findUserCustomDomains(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<UserCustomDomain> findUserCustomDomains(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findUserCustomDomains(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findUserCustomDomainKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findUserCustomDomainKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return remoteProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createUserCustomDomain(String owner, String domain, Boolean verified, String status, Long verifiedTime) throws BaseException
    {
        UserCustomDomainBean bean = new UserCustomDomainBean(null, owner, domain, verified, status, verifiedTime);
        return createUserCustomDomain(bean);        
    }

    @Override
    public String createUserCustomDomain(UserCustomDomain userCustomDomain) throws BaseException
    {
        log.finer("BEGIN");

        String guid = userCustomDomain.getGuid();
        if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
            guid = GUID.generate();
            ((UserCustomDomainBean) userCustomDomain).setGuid(guid);
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsCreateUserCustomDomain-" + guid;
        String taskName = "RsCreateUserCustomDomain-" + guid + "-" + (new Date()).getTime();
        UserCustomDomainStub stub = MarshalHelper.convertUserCustomDomainToStub(userCustomDomain);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(UserCustomDomainStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = userCustomDomain.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    UserCustomDomainStub dummyStub = new UserCustomDomainStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createUserCustomDomain(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "userCustomDomains/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createUserCustomDomain(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "userCustomDomains/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;  // ???
    }

    @Override
    public Boolean updateUserCustomDomain(String guid, String owner, String domain, Boolean verified, String status, Long verifiedTime) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "UserCustomDomain guid is invalid.");
        	throw new BaseException("UserCustomDomain guid is invalid.");
        }
        UserCustomDomainBean bean = new UserCustomDomainBean(guid, owner, domain, verified, status, verifiedTime);
        return updateUserCustomDomain(bean);        
    }

    @Override
    public Boolean updateUserCustomDomain(UserCustomDomain userCustomDomain) throws BaseException
    {
        log.finer("BEGIN");

        String guid = userCustomDomain.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "UserCustomDomain object is invalid.");
        	throw new BaseException("UserCustomDomain object is invalid.");
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsUpdateUserCustomDomain-" + guid;
        String taskName = "RsUpdateUserCustomDomain-" + guid + "-" + (new Date()).getTime();
        UserCustomDomainStub stub = MarshalHelper.convertUserCustomDomainToStub(userCustomDomain);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(UserCustomDomainStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = userCustomDomain.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    UserCustomDomainStub dummyStub = new UserCustomDomainStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateUserCustomDomain(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "userCustomDomains/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateUserCustomDomain(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "userCustomDomains/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteUserCustomDomain(String guid) throws BaseException
    {
        log.finer("BEGIN");

        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsDeleteUserCustomDomain-" + guid;
        String taskName = "RsDeleteUserCustomDomain-" + guid + "-" + (new Date()).getTime();
        TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "userCustomDomains/" + guid).method(Method.DELETE).taskName(taskName);
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteUserCustomDomain(UserCustomDomain userCustomDomain) throws BaseException
    {
        String guid = userCustomDomain.getGuid();
        return deleteUserCustomDomain(guid);
    }

    @Override
    public Long deleteUserCustomDomains(String filter, String params, List<String> values) throws BaseException
    {
        return remoteProxy.deleteUserCustomDomains(filter, params, values);
    }

}
