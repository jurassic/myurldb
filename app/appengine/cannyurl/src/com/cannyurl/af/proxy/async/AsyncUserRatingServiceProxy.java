package com.cannyurl.af.proxy.async;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.UserRating;
import com.myurldb.ws.stub.ErrorStub;
import com.myurldb.ws.stub.UserRatingStub;
import com.myurldb.ws.stub.UserRatingListStub;
import com.cannyurl.af.util.MarshalHelper;
import com.cannyurl.af.bean.UserRatingBean;
import com.myurldb.ws.service.UserRatingService;
import com.cannyurl.af.proxy.UserRatingServiceProxy;
import com.cannyurl.af.proxy.remote.RemoteUserRatingServiceProxy;


// Implements the asynchronous call logic (using GAE task queue).
// GET:             Directly calls the remoteProxy interface.
// POST/PUT/DELETE: Creates a task and puts in a queue.
public class AsyncUserRatingServiceProxy extends BaseAsyncServiceProxy implements UserRatingServiceProxy
{
    private static final Logger log = Logger.getLogger(AsyncUserRatingServiceProxy.class.getName());

    // Actual implementation is delegated to the corresponding remote proxy class.
    private RemoteUserRatingServiceProxy remoteProxy;

    public AsyncUserRatingServiceProxy()
    {
        remoteProxy = new RemoteUserRatingServiceProxy();
    }

    @Override
    public UserRating getUserRating(String guid) throws BaseException
    {
        return remoteProxy.getUserRating(guid);
    }

    @Override
    public Object getUserRating(String guid, String field) throws BaseException
    {
        return remoteProxy.getUserRating(guid, field);       
    }

    @Override
    public List<UserRating> getUserRatings(List<String> guids) throws BaseException
    {
        return remoteProxy.getUserRatings(guids);
    }

    @Override
    public List<UserRating> getAllUserRatings() throws BaseException
    {
        return getAllUserRatings(null, null, null);
    }

    @Override
    public List<UserRating> getAllUserRatings(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllUserRatings(ordering, offset, count);
    }

    @Override
    public List<String> getAllUserRatingKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllUserRatingKeys(ordering, offset, count);
    }

    @Override
    public List<UserRating> findUserRatings(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findUserRatings(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<UserRating> findUserRatings(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findUserRatings(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findUserRatingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findUserRatingKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return remoteProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createUserRating(String user, Double rating, String note, Long ratedTime) throws BaseException
    {
        UserRatingBean bean = new UserRatingBean(null, user, rating, note, ratedTime);
        return createUserRating(bean);        
    }

    @Override
    public String createUserRating(UserRating userRating) throws BaseException
    {
        log.finer("BEGIN");

        String guid = userRating.getGuid();
        if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
            guid = GUID.generate();
            ((UserRatingBean) userRating).setGuid(guid);
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsCreateUserRating-" + guid;
        String taskName = "RsCreateUserRating-" + guid + "-" + (new Date()).getTime();
        UserRatingStub stub = MarshalHelper.convertUserRatingToStub(userRating);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(UserRatingStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = userRating.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    UserRatingStub dummyStub = new UserRatingStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createUserRating(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "userRatings/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createUserRating(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "userRatings/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;  // ???
    }

    @Override
    public Boolean updateUserRating(String guid, String user, Double rating, String note, Long ratedTime) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "UserRating guid is invalid.");
        	throw new BaseException("UserRating guid is invalid.");
        }
        UserRatingBean bean = new UserRatingBean(guid, user, rating, note, ratedTime);
        return updateUserRating(bean);        
    }

    @Override
    public Boolean updateUserRating(UserRating userRating) throws BaseException
    {
        log.finer("BEGIN");

        String guid = userRating.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "UserRating object is invalid.");
        	throw new BaseException("UserRating object is invalid.");
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsUpdateUserRating-" + guid;
        String taskName = "RsUpdateUserRating-" + guid + "-" + (new Date()).getTime();
        UserRatingStub stub = MarshalHelper.convertUserRatingToStub(userRating);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(UserRatingStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = userRating.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    UserRatingStub dummyStub = new UserRatingStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateUserRating(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "userRatings/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateUserRating(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "userRatings/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteUserRating(String guid) throws BaseException
    {
        log.finer("BEGIN");

        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsDeleteUserRating-" + guid;
        String taskName = "RsDeleteUserRating-" + guid + "-" + (new Date()).getTime();
        TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "userRatings/" + guid).method(Method.DELETE).taskName(taskName);
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteUserRating(UserRating userRating) throws BaseException
    {
        String guid = userRating.getGuid();
        return deleteUserRating(guid);
    }

    @Override
    public Long deleteUserRatings(String filter, String params, List<String> values) throws BaseException
    {
        return remoteProxy.deleteUserRatings(filter, params, values);
    }

}
