package com.cannyurl.af.proxy.async;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.UserResourcePermission;
import com.myurldb.ws.stub.ErrorStub;
import com.myurldb.ws.stub.UserResourcePermissionStub;
import com.myurldb.ws.stub.UserResourcePermissionListStub;
import com.cannyurl.af.util.MarshalHelper;
import com.cannyurl.af.bean.UserResourcePermissionBean;
import com.myurldb.ws.service.UserResourcePermissionService;
import com.cannyurl.af.proxy.UserResourcePermissionServiceProxy;
import com.cannyurl.af.proxy.remote.RemoteUserResourcePermissionServiceProxy;


// Implements the asynchronous call logic (using GAE task queue).
// GET:             Directly calls the remoteProxy interface.
// POST/PUT/DELETE: Creates a task and puts in a queue.
public class AsyncUserResourcePermissionServiceProxy extends BaseAsyncServiceProxy implements UserResourcePermissionServiceProxy
{
    private static final Logger log = Logger.getLogger(AsyncUserResourcePermissionServiceProxy.class.getName());

    // Actual implementation is delegated to the corresponding remote proxy class.
    private RemoteUserResourcePermissionServiceProxy remoteProxy;

    public AsyncUserResourcePermissionServiceProxy()
    {
        remoteProxy = new RemoteUserResourcePermissionServiceProxy();
    }

    @Override
    public UserResourcePermission getUserResourcePermission(String guid) throws BaseException
    {
        return remoteProxy.getUserResourcePermission(guid);
    }

    @Override
    public Object getUserResourcePermission(String guid, String field) throws BaseException
    {
        return remoteProxy.getUserResourcePermission(guid, field);       
    }

    @Override
    public List<UserResourcePermission> getUserResourcePermissions(List<String> guids) throws BaseException
    {
        return remoteProxy.getUserResourcePermissions(guids);
    }

    @Override
    public List<UserResourcePermission> getAllUserResourcePermissions() throws BaseException
    {
        return getAllUserResourcePermissions(null, null, null);
    }

    @Override
    public List<UserResourcePermission> getAllUserResourcePermissions(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllUserResourcePermissions(ordering, offset, count);
    }

    @Override
    public List<String> getAllUserResourcePermissionKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllUserResourcePermissionKeys(ordering, offset, count);
    }

    @Override
    public List<UserResourcePermission> findUserResourcePermissions(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findUserResourcePermissions(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<UserResourcePermission> findUserResourcePermissions(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findUserResourcePermissions(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findUserResourcePermissionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findUserResourcePermissionKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return remoteProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createUserResourcePermission(String user, String permissionName, String resource, String instance, String action, Boolean permitted, String status) throws BaseException
    {
        UserResourcePermissionBean bean = new UserResourcePermissionBean(null, user, permissionName, resource, instance, action, permitted, status);
        return createUserResourcePermission(bean);        
    }

    @Override
    public String createUserResourcePermission(UserResourcePermission userResourcePermission) throws BaseException
    {
        log.finer("BEGIN");

        String guid = userResourcePermission.getGuid();
        if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
            guid = GUID.generate();
            ((UserResourcePermissionBean) userResourcePermission).setGuid(guid);
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsCreateUserResourcePermission-" + guid;
        String taskName = "RsCreateUserResourcePermission-" + guid + "-" + (new Date()).getTime();
        UserResourcePermissionStub stub = MarshalHelper.convertUserResourcePermissionToStub(userResourcePermission);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(UserResourcePermissionStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = userResourcePermission.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    UserResourcePermissionStub dummyStub = new UserResourcePermissionStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createUserResourcePermission(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "userResourcePermissions/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createUserResourcePermission(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "userResourcePermissions/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;  // ???
    }

    @Override
    public Boolean updateUserResourcePermission(String guid, String user, String permissionName, String resource, String instance, String action, Boolean permitted, String status) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "UserResourcePermission guid is invalid.");
        	throw new BaseException("UserResourcePermission guid is invalid.");
        }
        UserResourcePermissionBean bean = new UserResourcePermissionBean(guid, user, permissionName, resource, instance, action, permitted, status);
        return updateUserResourcePermission(bean);        
    }

    @Override
    public Boolean updateUserResourcePermission(UserResourcePermission userResourcePermission) throws BaseException
    {
        log.finer("BEGIN");

        String guid = userResourcePermission.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "UserResourcePermission object is invalid.");
        	throw new BaseException("UserResourcePermission object is invalid.");
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsUpdateUserResourcePermission-" + guid;
        String taskName = "RsUpdateUserResourcePermission-" + guid + "-" + (new Date()).getTime();
        UserResourcePermissionStub stub = MarshalHelper.convertUserResourcePermissionToStub(userResourcePermission);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(UserResourcePermissionStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = userResourcePermission.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    UserResourcePermissionStub dummyStub = new UserResourcePermissionStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateUserResourcePermission(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "userResourcePermissions/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateUserResourcePermission(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "userResourcePermissions/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteUserResourcePermission(String guid) throws BaseException
    {
        log.finer("BEGIN");

        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsDeleteUserResourcePermission-" + guid;
        String taskName = "RsDeleteUserResourcePermission-" + guid + "-" + (new Date()).getTime();
        TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "userResourcePermissions/" + guid).method(Method.DELETE).taskName(taskName);
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteUserResourcePermission(UserResourcePermission userResourcePermission) throws BaseException
    {
        String guid = userResourcePermission.getGuid();
        return deleteUserResourcePermission(guid);
    }

    @Override
    public Long deleteUserResourcePermissions(String filter, String params, List<String> values) throws BaseException
    {
        return remoteProxy.deleteUserResourcePermissions(filter, params, values);
    }

}
