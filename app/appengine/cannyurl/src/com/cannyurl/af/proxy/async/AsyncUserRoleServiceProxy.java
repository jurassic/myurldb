package com.cannyurl.af.proxy.async;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.UserRole;
import com.myurldb.ws.stub.ErrorStub;
import com.myurldb.ws.stub.UserRoleStub;
import com.myurldb.ws.stub.UserRoleListStub;
import com.cannyurl.af.util.MarshalHelper;
import com.cannyurl.af.bean.UserRoleBean;
import com.myurldb.ws.service.UserRoleService;
import com.cannyurl.af.proxy.UserRoleServiceProxy;
import com.cannyurl.af.proxy.remote.RemoteUserRoleServiceProxy;


// Implements the asynchronous call logic (using GAE task queue).
// GET:             Directly calls the remoteProxy interface.
// POST/PUT/DELETE: Creates a task and puts in a queue.
public class AsyncUserRoleServiceProxy extends BaseAsyncServiceProxy implements UserRoleServiceProxy
{
    private static final Logger log = Logger.getLogger(AsyncUserRoleServiceProxy.class.getName());

    // Actual implementation is delegated to the corresponding remote proxy class.
    private RemoteUserRoleServiceProxy remoteProxy;

    public AsyncUserRoleServiceProxy()
    {
        remoteProxy = new RemoteUserRoleServiceProxy();
    }

    @Override
    public UserRole getUserRole(String guid) throws BaseException
    {
        return remoteProxy.getUserRole(guid);
    }

    @Override
    public Object getUserRole(String guid, String field) throws BaseException
    {
        return remoteProxy.getUserRole(guid, field);       
    }

    @Override
    public List<UserRole> getUserRoles(List<String> guids) throws BaseException
    {
        return remoteProxy.getUserRoles(guids);
    }

    @Override
    public List<UserRole> getAllUserRoles() throws BaseException
    {
        return getAllUserRoles(null, null, null);
    }

    @Override
    public List<UserRole> getAllUserRoles(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllUserRoles(ordering, offset, count);
    }

    @Override
    public List<String> getAllUserRoleKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllUserRoleKeys(ordering, offset, count);
    }

    @Override
    public List<UserRole> findUserRoles(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findUserRoles(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<UserRole> findUserRoles(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findUserRoles(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findUserRoleKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findUserRoleKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return remoteProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createUserRole(String user, String role, String status) throws BaseException
    {
        UserRoleBean bean = new UserRoleBean(null, user, role, status);
        return createUserRole(bean);        
    }

    @Override
    public String createUserRole(UserRole userRole) throws BaseException
    {
        log.finer("BEGIN");

        String guid = userRole.getGuid();
        if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
            guid = GUID.generate();
            ((UserRoleBean) userRole).setGuid(guid);
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsCreateUserRole-" + guid;
        String taskName = "RsCreateUserRole-" + guid + "-" + (new Date()).getTime();
        UserRoleStub stub = MarshalHelper.convertUserRoleToStub(userRole);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(UserRoleStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = userRole.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    UserRoleStub dummyStub = new UserRoleStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createUserRole(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "userRoles/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createUserRole(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "userRoles/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;  // ???
    }

    @Override
    public Boolean updateUserRole(String guid, String user, String role, String status) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "UserRole guid is invalid.");
        	throw new BaseException("UserRole guid is invalid.");
        }
        UserRoleBean bean = new UserRoleBean(guid, user, role, status);
        return updateUserRole(bean);        
    }

    @Override
    public Boolean updateUserRole(UserRole userRole) throws BaseException
    {
        log.finer("BEGIN");

        String guid = userRole.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "UserRole object is invalid.");
        	throw new BaseException("UserRole object is invalid.");
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsUpdateUserRole-" + guid;
        String taskName = "RsUpdateUserRole-" + guid + "-" + (new Date()).getTime();
        UserRoleStub stub = MarshalHelper.convertUserRoleToStub(userRole);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(UserRoleStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = userRole.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    UserRoleStub dummyStub = new UserRoleStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateUserRole(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "userRoles/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateUserRole(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "userRoles/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteUserRole(String guid) throws BaseException
    {
        log.finer("BEGIN");

        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsDeleteUserRole-" + guid;
        String taskName = "RsDeleteUserRole-" + guid + "-" + (new Date()).getTime();
        TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "userRoles/" + guid).method(Method.DELETE).taskName(taskName);
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteUserRole(UserRole userRole) throws BaseException
    {
        String guid = userRole.getGuid();
        return deleteUserRole(guid);
    }

    @Override
    public Long deleteUserRoles(String filter, String params, List<String> values) throws BaseException
    {
        return remoteProxy.deleteUserRoles(filter, params, values);
    }

}
