package com.cannyurl.af.proxy.async;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.UserUsercode;
import com.myurldb.ws.stub.ErrorStub;
import com.myurldb.ws.stub.UserUsercodeStub;
import com.myurldb.ws.stub.UserUsercodeListStub;
import com.cannyurl.af.util.MarshalHelper;
import com.cannyurl.af.bean.UserUsercodeBean;
import com.myurldb.ws.service.UserUsercodeService;
import com.cannyurl.af.proxy.UserUsercodeServiceProxy;
import com.cannyurl.af.proxy.remote.RemoteUserUsercodeServiceProxy;


// Implements the asynchronous call logic (using GAE task queue).
// GET:             Directly calls the remoteProxy interface.
// POST/PUT/DELETE: Creates a task and puts in a queue.
public class AsyncUserUsercodeServiceProxy extends BaseAsyncServiceProxy implements UserUsercodeServiceProxy
{
    private static final Logger log = Logger.getLogger(AsyncUserUsercodeServiceProxy.class.getName());

    // Actual implementation is delegated to the corresponding remote proxy class.
    private RemoteUserUsercodeServiceProxy remoteProxy;

    public AsyncUserUsercodeServiceProxy()
    {
        remoteProxy = new RemoteUserUsercodeServiceProxy();
    }

    @Override
    public UserUsercode getUserUsercode(String guid) throws BaseException
    {
        return remoteProxy.getUserUsercode(guid);
    }

    @Override
    public Object getUserUsercode(String guid, String field) throws BaseException
    {
        return remoteProxy.getUserUsercode(guid, field);       
    }

    @Override
    public List<UserUsercode> getUserUsercodes(List<String> guids) throws BaseException
    {
        return remoteProxy.getUserUsercodes(guids);
    }

    @Override
    public List<UserUsercode> getAllUserUsercodes() throws BaseException
    {
        return getAllUserUsercodes(null, null, null);
    }

    @Override
    public List<UserUsercode> getAllUserUsercodes(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllUserUsercodes(ordering, offset, count);
    }

    @Override
    public List<String> getAllUserUsercodeKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllUserUsercodeKeys(ordering, offset, count);
    }

    @Override
    public List<UserUsercode> findUserUsercodes(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findUserUsercodes(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<UserUsercode> findUserUsercodes(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findUserUsercodes(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findUserUsercodeKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findUserUsercodeKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return remoteProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createUserUsercode(String admin, String user, String username, String usercode, String status) throws BaseException
    {
        UserUsercodeBean bean = new UserUsercodeBean(null, admin, user, username, usercode, status);
        return createUserUsercode(bean);        
    }

    @Override
    public String createUserUsercode(UserUsercode userUsercode) throws BaseException
    {
        log.finer("BEGIN");

        String guid = userUsercode.getGuid();
        if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
            guid = GUID.generate();
            ((UserUsercodeBean) userUsercode).setGuid(guid);
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsCreateUserUsercode-" + guid;
        String taskName = "RsCreateUserUsercode-" + guid + "-" + (new Date()).getTime();
        UserUsercodeStub stub = MarshalHelper.convertUserUsercodeToStub(userUsercode);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(UserUsercodeStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = userUsercode.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    UserUsercodeStub dummyStub = new UserUsercodeStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createUserUsercode(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "userUsercodes/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createUserUsercode(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "userUsercodes/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;  // ???
    }

    @Override
    public Boolean updateUserUsercode(String guid, String admin, String user, String username, String usercode, String status) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "UserUsercode guid is invalid.");
        	throw new BaseException("UserUsercode guid is invalid.");
        }
        UserUsercodeBean bean = new UserUsercodeBean(guid, admin, user, username, usercode, status);
        return updateUserUsercode(bean);        
    }

    @Override
    public Boolean updateUserUsercode(UserUsercode userUsercode) throws BaseException
    {
        log.finer("BEGIN");

        String guid = userUsercode.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "UserUsercode object is invalid.");
        	throw new BaseException("UserUsercode object is invalid.");
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsUpdateUserUsercode-" + guid;
        String taskName = "RsUpdateUserUsercode-" + guid + "-" + (new Date()).getTime();
        UserUsercodeStub stub = MarshalHelper.convertUserUsercodeToStub(userUsercode);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(UserUsercodeStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = userUsercode.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    UserUsercodeStub dummyStub = new UserUsercodeStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateUserUsercode(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "userUsercodes/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateUserUsercode(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "userUsercodes/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteUserUsercode(String guid) throws BaseException
    {
        log.finer("BEGIN");

        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsDeleteUserUsercode-" + guid;
        String taskName = "RsDeleteUserUsercode-" + guid + "-" + (new Date()).getTime();
        TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "userUsercodes/" + guid).method(Method.DELETE).taskName(taskName);
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteUserUsercode(UserUsercode userUsercode) throws BaseException
    {
        String guid = userUsercode.getGuid();
        return deleteUserUsercode(guid);
    }

    @Override
    public Long deleteUserUsercodes(String filter, String params, List<String> values) throws BaseException
    {
        return remoteProxy.deleteUserUsercodes(filter, params, values);
    }

}
