package com.cannyurl.af.proxy.async;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.VisitorSetting;
import com.myurldb.ws.stub.ErrorStub;
import com.myurldb.ws.stub.VisitorSettingStub;
import com.myurldb.ws.stub.VisitorSettingListStub;
import com.cannyurl.af.util.MarshalHelper;
import com.cannyurl.af.bean.VisitorSettingBean;
import com.myurldb.ws.service.VisitorSettingService;
import com.cannyurl.af.proxy.VisitorSettingServiceProxy;
import com.cannyurl.af.proxy.remote.RemoteVisitorSettingServiceProxy;


// Implements the asynchronous call logic (using GAE task queue).
// GET:             Directly calls the remoteProxy interface.
// POST/PUT/DELETE: Creates a task and puts in a queue.
public class AsyncVisitorSettingServiceProxy extends BaseAsyncServiceProxy implements VisitorSettingServiceProxy
{
    private static final Logger log = Logger.getLogger(AsyncVisitorSettingServiceProxy.class.getName());

    // Actual implementation is delegated to the corresponding remote proxy class.
    private RemoteVisitorSettingServiceProxy remoteProxy;

    public AsyncVisitorSettingServiceProxy()
    {
        remoteProxy = new RemoteVisitorSettingServiceProxy();
    }

    @Override
    public VisitorSetting getVisitorSetting(String guid) throws BaseException
    {
        return remoteProxy.getVisitorSetting(guid);
    }

    @Override
    public Object getVisitorSetting(String guid, String field) throws BaseException
    {
        return remoteProxy.getVisitorSetting(guid, field);       
    }

    @Override
    public List<VisitorSetting> getVisitorSettings(List<String> guids) throws BaseException
    {
        return remoteProxy.getVisitorSettings(guids);
    }

    @Override
    public List<VisitorSetting> getAllVisitorSettings() throws BaseException
    {
        return getAllVisitorSettings(null, null, null);
    }

    @Override
    public List<VisitorSetting> getAllVisitorSettings(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllVisitorSettings(ordering, offset, count);
    }

    @Override
    public List<String> getAllVisitorSettingKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllVisitorSettingKeys(ordering, offset, count);
    }

    @Override
    public List<VisitorSetting> findVisitorSettings(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findVisitorSettings(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<VisitorSetting> findVisitorSettings(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findVisitorSettings(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findVisitorSettingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findVisitorSettingKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return remoteProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createVisitorSetting(String user, String redirectType, Long flashDuration, String privacyType) throws BaseException
    {
        VisitorSettingBean bean = new VisitorSettingBean(null, user, redirectType, flashDuration, privacyType);
        return createVisitorSetting(bean);        
    }

    @Override
    public String createVisitorSetting(VisitorSetting visitorSetting) throws BaseException
    {
        log.finer("BEGIN");

        String guid = visitorSetting.getGuid();
        if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
            guid = GUID.generate();
            ((VisitorSettingBean) visitorSetting).setGuid(guid);
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsCreateVisitorSetting-" + guid;
        String taskName = "RsCreateVisitorSetting-" + guid + "-" + (new Date()).getTime();
        VisitorSettingStub stub = MarshalHelper.convertVisitorSettingToStub(visitorSetting);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(VisitorSettingStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = visitorSetting.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    VisitorSettingStub dummyStub = new VisitorSettingStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createVisitorSetting(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "visitorSettings/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createVisitorSetting(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "visitorSettings/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;  // ???
    }

    @Override
    public Boolean updateVisitorSetting(String guid, String user, String redirectType, Long flashDuration, String privacyType) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "VisitorSetting guid is invalid.");
        	throw new BaseException("VisitorSetting guid is invalid.");
        }
        VisitorSettingBean bean = new VisitorSettingBean(guid, user, redirectType, flashDuration, privacyType);
        return updateVisitorSetting(bean);        
    }

    @Override
    public Boolean updateVisitorSetting(VisitorSetting visitorSetting) throws BaseException
    {
        log.finer("BEGIN");

        String guid = visitorSetting.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "VisitorSetting object is invalid.");
        	throw new BaseException("VisitorSetting object is invalid.");
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsUpdateVisitorSetting-" + guid;
        String taskName = "RsUpdateVisitorSetting-" + guid + "-" + (new Date()).getTime();
        VisitorSettingStub stub = MarshalHelper.convertVisitorSettingToStub(visitorSetting);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(VisitorSettingStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = visitorSetting.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    VisitorSettingStub dummyStub = new VisitorSettingStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateVisitorSetting(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "visitorSettings/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateVisitorSetting(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "visitorSettings/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteVisitorSetting(String guid) throws BaseException
    {
        log.finer("BEGIN");

        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsDeleteVisitorSetting-" + guid;
        String taskName = "RsDeleteVisitorSetting-" + guid + "-" + (new Date()).getTime();
        TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "visitorSettings/" + guid).method(Method.DELETE).taskName(taskName);
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteVisitorSetting(VisitorSetting visitorSetting) throws BaseException
    {
        String guid = visitorSetting.getGuid();
        return deleteVisitorSetting(guid);
    }

    @Override
    public Long deleteVisitorSettings(String filter, String params, List<String> values) throws BaseException
    {
        return remoteProxy.deleteVisitorSettings(filter, params, values);
    }

}
