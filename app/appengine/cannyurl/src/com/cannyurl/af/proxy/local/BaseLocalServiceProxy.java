package com.cannyurl.af.proxy.local;

import com.myurldb.ws.service.ApiConsumerService;
import com.myurldb.ws.service.AppCustomDomainService;
import com.myurldb.ws.service.SiteCustomDomainService;
import com.myurldb.ws.service.UserService;
import com.myurldb.ws.service.UserUsercodeService;
import com.myurldb.ws.service.UserPasswordService;
import com.myurldb.ws.service.ExternalUserAuthService;
import com.myurldb.ws.service.UserAuthStateService;
import com.myurldb.ws.service.UserResourcePermissionService;
import com.myurldb.ws.service.UserResourceProhibitionService;
import com.myurldb.ws.service.RolePermissionService;
import com.myurldb.ws.service.UserRoleService;
import com.myurldb.ws.service.AppClientService;
import com.myurldb.ws.service.ClientUserService;
import com.myurldb.ws.service.UserCustomDomainService;
import com.myurldb.ws.service.ClientSettingService;
import com.myurldb.ws.service.UserSettingService;
import com.myurldb.ws.service.VisitorSettingService;
import com.myurldb.ws.service.TwitterSummaryCardService;
import com.myurldb.ws.service.TwitterPhotoCardService;
import com.myurldb.ws.service.TwitterGalleryCardService;
import com.myurldb.ws.service.TwitterAppCardService;
import com.myurldb.ws.service.TwitterPlayerCardService;
import com.myurldb.ws.service.TwitterProductCardService;
import com.myurldb.ws.service.ShortPassageService;
import com.myurldb.ws.service.ShortLinkService;
import com.myurldb.ws.service.GeoLinkService;
import com.myurldb.ws.service.QrCodeService;
import com.myurldb.ws.service.LinkPassphraseService;
import com.myurldb.ws.service.LinkMessageService;
import com.myurldb.ws.service.LinkAlbumService;
import com.myurldb.ws.service.AlbumShortLinkService;
import com.myurldb.ws.service.KeywordFolderService;
import com.myurldb.ws.service.BookmarkFolderService;
import com.myurldb.ws.service.KeywordLinkService;
import com.myurldb.ws.service.BookmarkLinkService;
import com.myurldb.ws.service.SpeedDialService;
import com.myurldb.ws.service.KeywordFolderImportService;
import com.myurldb.ws.service.BookmarkFolderImportService;
import com.myurldb.ws.service.KeywordCrowdTallyService;
import com.myurldb.ws.service.BookmarkCrowdTallyService;
import com.myurldb.ws.service.DomainInfoService;
import com.myurldb.ws.service.UrlRatingService;
import com.myurldb.ws.service.UserRatingService;
import com.myurldb.ws.service.AbuseTagService;
import com.myurldb.ws.service.ServiceInfoService;
import com.myurldb.ws.service.FiveTenService;

// TBD: How to best inject the service instances?
public abstract class BaseLocalServiceProxy
{
    private ApiConsumerService apiConsumerService;
    private AppCustomDomainService appCustomDomainService;
    private SiteCustomDomainService siteCustomDomainService;
    private UserService userService;
    private UserUsercodeService userUsercodeService;
    private UserPasswordService userPasswordService;
    private ExternalUserAuthService externalUserAuthService;
    private UserAuthStateService userAuthStateService;
    private UserResourcePermissionService userResourcePermissionService;
    private UserResourceProhibitionService userResourceProhibitionService;
    private RolePermissionService rolePermissionService;
    private UserRoleService userRoleService;
    private AppClientService appClientService;
    private ClientUserService clientUserService;
    private UserCustomDomainService userCustomDomainService;
    private ClientSettingService clientSettingService;
    private UserSettingService userSettingService;
    private VisitorSettingService visitorSettingService;
    private TwitterSummaryCardService twitterSummaryCardService;
    private TwitterPhotoCardService twitterPhotoCardService;
    private TwitterGalleryCardService twitterGalleryCardService;
    private TwitterAppCardService twitterAppCardService;
    private TwitterPlayerCardService twitterPlayerCardService;
    private TwitterProductCardService twitterProductCardService;
    private ShortPassageService shortPassageService;
    private ShortLinkService shortLinkService;
    private GeoLinkService geoLinkService;
    private QrCodeService qrCodeService;
    private LinkPassphraseService linkPassphraseService;
    private LinkMessageService linkMessageService;
    private LinkAlbumService linkAlbumService;
    private AlbumShortLinkService albumShortLinkService;
    private KeywordFolderService keywordFolderService;
    private BookmarkFolderService bookmarkFolderService;
    private KeywordLinkService keywordLinkService;
    private BookmarkLinkService bookmarkLinkService;
    private SpeedDialService speedDialService;
    private KeywordFolderImportService keywordFolderImportService;
    private BookmarkFolderImportService bookmarkFolderImportService;
    private KeywordCrowdTallyService keywordCrowdTallyService;
    private BookmarkCrowdTallyService bookmarkCrowdTallyService;
    private DomainInfoService domainInfoService;
    private UrlRatingService urlRatingService;
    private UserRatingService userRatingService;
    private AbuseTagService abuseTagService;
    private ServiceInfoService serviceInfoService;
    private FiveTenService fiveTenService;

    public BaseLocalServiceProxy()
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(ApiConsumerService apiConsumerService)
    {
        this(apiConsumerService, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(AppCustomDomainService appCustomDomainService)
    {
        this(null, appCustomDomainService, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(SiteCustomDomainService siteCustomDomainService)
    {
        this(null, null, siteCustomDomainService, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(UserService userService)
    {
        this(null, null, null, userService, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(UserUsercodeService userUsercodeService)
    {
        this(null, null, null, null, userUsercodeService, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(UserPasswordService userPasswordService)
    {
        this(null, null, null, null, null, userPasswordService, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(ExternalUserAuthService externalUserAuthService)
    {
        this(null, null, null, null, null, null, externalUserAuthService, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(UserAuthStateService userAuthStateService)
    {
        this(null, null, null, null, null, null, null, userAuthStateService, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(UserResourcePermissionService userResourcePermissionService)
    {
        this(null, null, null, null, null, null, null, null, userResourcePermissionService, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(UserResourceProhibitionService userResourceProhibitionService)
    {
        this(null, null, null, null, null, null, null, null, null, userResourceProhibitionService, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(RolePermissionService rolePermissionService)
    {
        this(null, null, null, null, null, null, null, null, null, null, rolePermissionService, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(UserRoleService userRoleService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, userRoleService, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(AppClientService appClientService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, appClientService, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(ClientUserService clientUserService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, clientUserService, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(UserCustomDomainService userCustomDomainService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, userCustomDomainService, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(ClientSettingService clientSettingService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, clientSettingService, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(UserSettingService userSettingService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, userSettingService, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(VisitorSettingService visitorSettingService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, visitorSettingService, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(TwitterSummaryCardService twitterSummaryCardService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, twitterSummaryCardService, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(TwitterPhotoCardService twitterPhotoCardService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, twitterPhotoCardService, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(TwitterGalleryCardService twitterGalleryCardService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, twitterGalleryCardService, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(TwitterAppCardService twitterAppCardService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, twitterAppCardService, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(TwitterPlayerCardService twitterPlayerCardService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, twitterPlayerCardService, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(TwitterProductCardService twitterProductCardService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, twitterProductCardService, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(ShortPassageService shortPassageService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, shortPassageService, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(ShortLinkService shortLinkService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, shortLinkService, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(GeoLinkService geoLinkService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, geoLinkService, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(QrCodeService qrCodeService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, qrCodeService, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(LinkPassphraseService linkPassphraseService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, linkPassphraseService, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(LinkMessageService linkMessageService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, linkMessageService, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(LinkAlbumService linkAlbumService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, linkAlbumService, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(AlbumShortLinkService albumShortLinkService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, albumShortLinkService, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(KeywordFolderService keywordFolderService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, keywordFolderService, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(BookmarkFolderService bookmarkFolderService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, bookmarkFolderService, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(KeywordLinkService keywordLinkService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, keywordLinkService, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(BookmarkLinkService bookmarkLinkService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, bookmarkLinkService, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(SpeedDialService speedDialService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, speedDialService, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(KeywordFolderImportService keywordFolderImportService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, keywordFolderImportService, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(BookmarkFolderImportService bookmarkFolderImportService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, bookmarkFolderImportService, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(KeywordCrowdTallyService keywordCrowdTallyService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, keywordCrowdTallyService, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(BookmarkCrowdTallyService bookmarkCrowdTallyService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, bookmarkCrowdTallyService, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(DomainInfoService domainInfoService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, domainInfoService, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(UrlRatingService urlRatingService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, urlRatingService, null, null, null, null);
    }
    public BaseLocalServiceProxy(UserRatingService userRatingService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, userRatingService, null, null, null);
    }
    public BaseLocalServiceProxy(AbuseTagService abuseTagService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, abuseTagService, null, null);
    }
    public BaseLocalServiceProxy(ServiceInfoService serviceInfoService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, serviceInfoService, null);
    }
    public BaseLocalServiceProxy(FiveTenService fiveTenService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, fiveTenService);
    }
    public BaseLocalServiceProxy(ApiConsumerService apiConsumerService, AppCustomDomainService appCustomDomainService, SiteCustomDomainService siteCustomDomainService, UserService userService, UserUsercodeService userUsercodeService, UserPasswordService userPasswordService, ExternalUserAuthService externalUserAuthService, UserAuthStateService userAuthStateService, UserResourcePermissionService userResourcePermissionService, UserResourceProhibitionService userResourceProhibitionService, RolePermissionService rolePermissionService, UserRoleService userRoleService, AppClientService appClientService, ClientUserService clientUserService, UserCustomDomainService userCustomDomainService, ClientSettingService clientSettingService, UserSettingService userSettingService, VisitorSettingService visitorSettingService, TwitterSummaryCardService twitterSummaryCardService, TwitterPhotoCardService twitterPhotoCardService, TwitterGalleryCardService twitterGalleryCardService, TwitterAppCardService twitterAppCardService, TwitterPlayerCardService twitterPlayerCardService, TwitterProductCardService twitterProductCardService, ShortPassageService shortPassageService, ShortLinkService shortLinkService, GeoLinkService geoLinkService, QrCodeService qrCodeService, LinkPassphraseService linkPassphraseService, LinkMessageService linkMessageService, LinkAlbumService linkAlbumService, AlbumShortLinkService albumShortLinkService, KeywordFolderService keywordFolderService, BookmarkFolderService bookmarkFolderService, KeywordLinkService keywordLinkService, BookmarkLinkService bookmarkLinkService, SpeedDialService speedDialService, KeywordFolderImportService keywordFolderImportService, BookmarkFolderImportService bookmarkFolderImportService, KeywordCrowdTallyService keywordCrowdTallyService, BookmarkCrowdTallyService bookmarkCrowdTallyService, DomainInfoService domainInfoService, UrlRatingService urlRatingService, UserRatingService userRatingService, AbuseTagService abuseTagService, ServiceInfoService serviceInfoService, FiveTenService fiveTenService)
    {
        this.apiConsumerService = apiConsumerService;
        this.appCustomDomainService = appCustomDomainService;
        this.siteCustomDomainService = siteCustomDomainService;
        this.userService = userService;
        this.userUsercodeService = userUsercodeService;
        this.userPasswordService = userPasswordService;
        this.externalUserAuthService = externalUserAuthService;
        this.userAuthStateService = userAuthStateService;
        this.userResourcePermissionService = userResourcePermissionService;
        this.userResourceProhibitionService = userResourceProhibitionService;
        this.rolePermissionService = rolePermissionService;
        this.userRoleService = userRoleService;
        this.appClientService = appClientService;
        this.clientUserService = clientUserService;
        this.userCustomDomainService = userCustomDomainService;
        this.clientSettingService = clientSettingService;
        this.userSettingService = userSettingService;
        this.visitorSettingService = visitorSettingService;
        this.twitterSummaryCardService = twitterSummaryCardService;
        this.twitterPhotoCardService = twitterPhotoCardService;
        this.twitterGalleryCardService = twitterGalleryCardService;
        this.twitterAppCardService = twitterAppCardService;
        this.twitterPlayerCardService = twitterPlayerCardService;
        this.twitterProductCardService = twitterProductCardService;
        this.shortPassageService = shortPassageService;
        this.shortLinkService = shortLinkService;
        this.geoLinkService = geoLinkService;
        this.qrCodeService = qrCodeService;
        this.linkPassphraseService = linkPassphraseService;
        this.linkMessageService = linkMessageService;
        this.linkAlbumService = linkAlbumService;
        this.albumShortLinkService = albumShortLinkService;
        this.keywordFolderService = keywordFolderService;
        this.bookmarkFolderService = bookmarkFolderService;
        this.keywordLinkService = keywordLinkService;
        this.bookmarkLinkService = bookmarkLinkService;
        this.speedDialService = speedDialService;
        this.keywordFolderImportService = keywordFolderImportService;
        this.bookmarkFolderImportService = bookmarkFolderImportService;
        this.keywordCrowdTallyService = keywordCrowdTallyService;
        this.bookmarkCrowdTallyService = bookmarkCrowdTallyService;
        this.domainInfoService = domainInfoService;
        this.urlRatingService = urlRatingService;
        this.userRatingService = userRatingService;
        this.abuseTagService = abuseTagService;
        this.serviceInfoService = serviceInfoService;
        this.fiveTenService = fiveTenService;
    }
    
    // Inject dependencies.
    public void setApiConsumerService(ApiConsumerService apiConsumerService)
    {
        this.apiConsumerService = apiConsumerService;
    }
    public void setAppCustomDomainService(AppCustomDomainService appCustomDomainService)
    {
        this.appCustomDomainService = appCustomDomainService;
    }
    public void setSiteCustomDomainService(SiteCustomDomainService siteCustomDomainService)
    {
        this.siteCustomDomainService = siteCustomDomainService;
    }
    public void setUserService(UserService userService)
    {
        this.userService = userService;
    }
    public void setUserUsercodeService(UserUsercodeService userUsercodeService)
    {
        this.userUsercodeService = userUsercodeService;
    }
    public void setUserPasswordService(UserPasswordService userPasswordService)
    {
        this.userPasswordService = userPasswordService;
    }
    public void setExternalUserAuthService(ExternalUserAuthService externalUserAuthService)
    {
        this.externalUserAuthService = externalUserAuthService;
    }
    public void setUserAuthStateService(UserAuthStateService userAuthStateService)
    {
        this.userAuthStateService = userAuthStateService;
    }
    public void setUserResourcePermissionService(UserResourcePermissionService userResourcePermissionService)
    {
        this.userResourcePermissionService = userResourcePermissionService;
    }
    public void setUserResourceProhibitionService(UserResourceProhibitionService userResourceProhibitionService)
    {
        this.userResourceProhibitionService = userResourceProhibitionService;
    }
    public void setRolePermissionService(RolePermissionService rolePermissionService)
    {
        this.rolePermissionService = rolePermissionService;
    }
    public void setUserRoleService(UserRoleService userRoleService)
    {
        this.userRoleService = userRoleService;
    }
    public void setAppClientService(AppClientService appClientService)
    {
        this.appClientService = appClientService;
    }
    public void setClientUserService(ClientUserService clientUserService)
    {
        this.clientUserService = clientUserService;
    }
    public void setUserCustomDomainService(UserCustomDomainService userCustomDomainService)
    {
        this.userCustomDomainService = userCustomDomainService;
    }
    public void setClientSettingService(ClientSettingService clientSettingService)
    {
        this.clientSettingService = clientSettingService;
    }
    public void setUserSettingService(UserSettingService userSettingService)
    {
        this.userSettingService = userSettingService;
    }
    public void setVisitorSettingService(VisitorSettingService visitorSettingService)
    {
        this.visitorSettingService = visitorSettingService;
    }
    public void setTwitterSummaryCardService(TwitterSummaryCardService twitterSummaryCardService)
    {
        this.twitterSummaryCardService = twitterSummaryCardService;
    }
    public void setTwitterPhotoCardService(TwitterPhotoCardService twitterPhotoCardService)
    {
        this.twitterPhotoCardService = twitterPhotoCardService;
    }
    public void setTwitterGalleryCardService(TwitterGalleryCardService twitterGalleryCardService)
    {
        this.twitterGalleryCardService = twitterGalleryCardService;
    }
    public void setTwitterAppCardService(TwitterAppCardService twitterAppCardService)
    {
        this.twitterAppCardService = twitterAppCardService;
    }
    public void setTwitterPlayerCardService(TwitterPlayerCardService twitterPlayerCardService)
    {
        this.twitterPlayerCardService = twitterPlayerCardService;
    }
    public void setTwitterProductCardService(TwitterProductCardService twitterProductCardService)
    {
        this.twitterProductCardService = twitterProductCardService;
    }
    public void setShortPassageService(ShortPassageService shortPassageService)
    {
        this.shortPassageService = shortPassageService;
    }
    public void setShortLinkService(ShortLinkService shortLinkService)
    {
        this.shortLinkService = shortLinkService;
    }
    public void setGeoLinkService(GeoLinkService geoLinkService)
    {
        this.geoLinkService = geoLinkService;
    }
    public void setQrCodeService(QrCodeService qrCodeService)
    {
        this.qrCodeService = qrCodeService;
    }
    public void setLinkPassphraseService(LinkPassphraseService linkPassphraseService)
    {
        this.linkPassphraseService = linkPassphraseService;
    }
    public void setLinkMessageService(LinkMessageService linkMessageService)
    {
        this.linkMessageService = linkMessageService;
    }
    public void setLinkAlbumService(LinkAlbumService linkAlbumService)
    {
        this.linkAlbumService = linkAlbumService;
    }
    public void setAlbumShortLinkService(AlbumShortLinkService albumShortLinkService)
    {
        this.albumShortLinkService = albumShortLinkService;
    }
    public void setKeywordFolderService(KeywordFolderService keywordFolderService)
    {
        this.keywordFolderService = keywordFolderService;
    }
    public void setBookmarkFolderService(BookmarkFolderService bookmarkFolderService)
    {
        this.bookmarkFolderService = bookmarkFolderService;
    }
    public void setKeywordLinkService(KeywordLinkService keywordLinkService)
    {
        this.keywordLinkService = keywordLinkService;
    }
    public void setBookmarkLinkService(BookmarkLinkService bookmarkLinkService)
    {
        this.bookmarkLinkService = bookmarkLinkService;
    }
    public void setSpeedDialService(SpeedDialService speedDialService)
    {
        this.speedDialService = speedDialService;
    }
    public void setKeywordFolderImportService(KeywordFolderImportService keywordFolderImportService)
    {
        this.keywordFolderImportService = keywordFolderImportService;
    }
    public void setBookmarkFolderImportService(BookmarkFolderImportService bookmarkFolderImportService)
    {
        this.bookmarkFolderImportService = bookmarkFolderImportService;
    }
    public void setKeywordCrowdTallyService(KeywordCrowdTallyService keywordCrowdTallyService)
    {
        this.keywordCrowdTallyService = keywordCrowdTallyService;
    }
    public void setBookmarkCrowdTallyService(BookmarkCrowdTallyService bookmarkCrowdTallyService)
    {
        this.bookmarkCrowdTallyService = bookmarkCrowdTallyService;
    }
    public void setDomainInfoService(DomainInfoService domainInfoService)
    {
        this.domainInfoService = domainInfoService;
    }
    public void setUrlRatingService(UrlRatingService urlRatingService)
    {
        this.urlRatingService = urlRatingService;
    }
    public void setUserRatingService(UserRatingService userRatingService)
    {
        this.userRatingService = userRatingService;
    }
    public void setAbuseTagService(AbuseTagService abuseTagService)
    {
        this.abuseTagService = abuseTagService;
    }
    public void setServiceInfoService(ServiceInfoService serviceInfoService)
    {
        this.serviceInfoService = serviceInfoService;
    }
    public void setFiveTenService(FiveTenService fiveTenService)
    {
        this.fiveTenService = fiveTenService;
    }
   
    // Returns a ApiConsumerService instance.
    public ApiConsumerService getApiConsumerService() 
    {
        return apiConsumerService;
    }

    // Returns a AppCustomDomainService instance.
    public AppCustomDomainService getAppCustomDomainService() 
    {
        return appCustomDomainService;
    }

    // Returns a SiteCustomDomainService instance.
    public SiteCustomDomainService getSiteCustomDomainService() 
    {
        return siteCustomDomainService;
    }

    // Returns a UserService instance.
    public UserService getUserService() 
    {
        return userService;
    }

    // Returns a UserUsercodeService instance.
    public UserUsercodeService getUserUsercodeService() 
    {
        return userUsercodeService;
    }

    // Returns a UserPasswordService instance.
    public UserPasswordService getUserPasswordService() 
    {
        return userPasswordService;
    }

    // Returns a ExternalUserAuthService instance.
    public ExternalUserAuthService getExternalUserAuthService() 
    {
        return externalUserAuthService;
    }

    // Returns a UserAuthStateService instance.
    public UserAuthStateService getUserAuthStateService() 
    {
        return userAuthStateService;
    }

    // Returns a UserResourcePermissionService instance.
    public UserResourcePermissionService getUserResourcePermissionService() 
    {
        return userResourcePermissionService;
    }

    // Returns a UserResourceProhibitionService instance.
    public UserResourceProhibitionService getUserResourceProhibitionService() 
    {
        return userResourceProhibitionService;
    }

    // Returns a RolePermissionService instance.
    public RolePermissionService getRolePermissionService() 
    {
        return rolePermissionService;
    }

    // Returns a UserRoleService instance.
    public UserRoleService getUserRoleService() 
    {
        return userRoleService;
    }

    // Returns a AppClientService instance.
    public AppClientService getAppClientService() 
    {
        return appClientService;
    }

    // Returns a ClientUserService instance.
    public ClientUserService getClientUserService() 
    {
        return clientUserService;
    }

    // Returns a UserCustomDomainService instance.
    public UserCustomDomainService getUserCustomDomainService() 
    {
        return userCustomDomainService;
    }

    // Returns a ClientSettingService instance.
    public ClientSettingService getClientSettingService() 
    {
        return clientSettingService;
    }

    // Returns a UserSettingService instance.
    public UserSettingService getUserSettingService() 
    {
        return userSettingService;
    }

    // Returns a VisitorSettingService instance.
    public VisitorSettingService getVisitorSettingService() 
    {
        return visitorSettingService;
    }

    // Returns a TwitterSummaryCardService instance.
    public TwitterSummaryCardService getTwitterSummaryCardService() 
    {
        return twitterSummaryCardService;
    }

    // Returns a TwitterPhotoCardService instance.
    public TwitterPhotoCardService getTwitterPhotoCardService() 
    {
        return twitterPhotoCardService;
    }

    // Returns a TwitterGalleryCardService instance.
    public TwitterGalleryCardService getTwitterGalleryCardService() 
    {
        return twitterGalleryCardService;
    }

    // Returns a TwitterAppCardService instance.
    public TwitterAppCardService getTwitterAppCardService() 
    {
        return twitterAppCardService;
    }

    // Returns a TwitterPlayerCardService instance.
    public TwitterPlayerCardService getTwitterPlayerCardService() 
    {
        return twitterPlayerCardService;
    }

    // Returns a TwitterProductCardService instance.
    public TwitterProductCardService getTwitterProductCardService() 
    {
        return twitterProductCardService;
    }

    // Returns a ShortPassageService instance.
    public ShortPassageService getShortPassageService() 
    {
        return shortPassageService;
    }

    // Returns a ShortLinkService instance.
    public ShortLinkService getShortLinkService() 
    {
        return shortLinkService;
    }

    // Returns a GeoLinkService instance.
    public GeoLinkService getGeoLinkService() 
    {
        return geoLinkService;
    }

    // Returns a QrCodeService instance.
    public QrCodeService getQrCodeService() 
    {
        return qrCodeService;
    }

    // Returns a LinkPassphraseService instance.
    public LinkPassphraseService getLinkPassphraseService() 
    {
        return linkPassphraseService;
    }

    // Returns a LinkMessageService instance.
    public LinkMessageService getLinkMessageService() 
    {
        return linkMessageService;
    }

    // Returns a LinkAlbumService instance.
    public LinkAlbumService getLinkAlbumService() 
    {
        return linkAlbumService;
    }

    // Returns a AlbumShortLinkService instance.
    public AlbumShortLinkService getAlbumShortLinkService() 
    {
        return albumShortLinkService;
    }

    // Returns a KeywordFolderService instance.
    public KeywordFolderService getKeywordFolderService() 
    {
        return keywordFolderService;
    }

    // Returns a BookmarkFolderService instance.
    public BookmarkFolderService getBookmarkFolderService() 
    {
        return bookmarkFolderService;
    }

    // Returns a KeywordLinkService instance.
    public KeywordLinkService getKeywordLinkService() 
    {
        return keywordLinkService;
    }

    // Returns a BookmarkLinkService instance.
    public BookmarkLinkService getBookmarkLinkService() 
    {
        return bookmarkLinkService;
    }

    // Returns a SpeedDialService instance.
    public SpeedDialService getSpeedDialService() 
    {
        return speedDialService;
    }

    // Returns a KeywordFolderImportService instance.
    public KeywordFolderImportService getKeywordFolderImportService() 
    {
        return keywordFolderImportService;
    }

    // Returns a BookmarkFolderImportService instance.
    public BookmarkFolderImportService getBookmarkFolderImportService() 
    {
        return bookmarkFolderImportService;
    }

    // Returns a KeywordCrowdTallyService instance.
    public KeywordCrowdTallyService getKeywordCrowdTallyService() 
    {
        return keywordCrowdTallyService;
    }

    // Returns a BookmarkCrowdTallyService instance.
    public BookmarkCrowdTallyService getBookmarkCrowdTallyService() 
    {
        return bookmarkCrowdTallyService;
    }

    // Returns a DomainInfoService instance.
    public DomainInfoService getDomainInfoService() 
    {
        return domainInfoService;
    }

    // Returns a UrlRatingService instance.
    public UrlRatingService getUrlRatingService() 
    {
        return urlRatingService;
    }

    // Returns a UserRatingService instance.
    public UserRatingService getUserRatingService() 
    {
        return userRatingService;
    }

    // Returns a AbuseTagService instance.
    public AbuseTagService getAbuseTagService() 
    {
        return abuseTagService;
    }

    // Returns a ServiceInfoService instance.
    public ServiceInfoService getServiceInfoService() 
    {
        return serviceInfoService;
    }

    // Returns a FiveTenService instance.
    public FiveTenService getFiveTenService() 
    {
        return fiveTenService;
    }

}
