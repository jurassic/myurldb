package com.cannyurl.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.AbuseTag;
import com.myurldb.ws.service.AbuseTagService;
import com.cannyurl.af.proxy.AbuseTagServiceProxy;

public class LocalAbuseTagServiceProxy extends BaseLocalServiceProxy implements AbuseTagServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalAbuseTagServiceProxy.class.getName());

    public LocalAbuseTagServiceProxy()
    {
    }

    @Override
    public AbuseTag getAbuseTag(String guid) throws BaseException
    {
        return getAbuseTagService().getAbuseTag(guid);
    }

    @Override
    public Object getAbuseTag(String guid, String field) throws BaseException
    {
        return getAbuseTagService().getAbuseTag(guid, field);       
    }

    @Override
    public List<AbuseTag> getAbuseTags(List<String> guids) throws BaseException
    {
        return getAbuseTagService().getAbuseTags(guids);
    }

    @Override
    public List<AbuseTag> getAllAbuseTags() throws BaseException
    {
        return getAllAbuseTags(null, null, null);
    }

    @Override
    public List<AbuseTag> getAllAbuseTags(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAbuseTagService().getAllAbuseTags(ordering, offset, count);
    }

    @Override
    public List<String> getAllAbuseTagKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAbuseTagService().getAllAbuseTagKeys(ordering, offset, count);
    }

    @Override
    public List<AbuseTag> findAbuseTags(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findAbuseTags(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<AbuseTag> findAbuseTags(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getAbuseTagService().findAbuseTags(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findAbuseTagKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getAbuseTagService().findAbuseTagKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getAbuseTagService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createAbuseTag(String shortLink, String shortUrl, String longUrl, String longUrlHash, String user, String ipAddress, Integer tag, String comment, String status, String reviewer, String action, String note, String pastActions, Long reviewedTime, Long actionTime) throws BaseException
    {
        return getAbuseTagService().createAbuseTag(shortLink, shortUrl, longUrl, longUrlHash, user, ipAddress, tag, comment, status, reviewer, action, note, pastActions, reviewedTime, actionTime);
    }

    @Override
    public String createAbuseTag(AbuseTag abuseTag) throws BaseException
    {
        return getAbuseTagService().createAbuseTag(abuseTag);
    }

    @Override
    public Boolean updateAbuseTag(String guid, String shortLink, String shortUrl, String longUrl, String longUrlHash, String user, String ipAddress, Integer tag, String comment, String status, String reviewer, String action, String note, String pastActions, Long reviewedTime, Long actionTime) throws BaseException
    {
        return getAbuseTagService().updateAbuseTag(guid, shortLink, shortUrl, longUrl, longUrlHash, user, ipAddress, tag, comment, status, reviewer, action, note, pastActions, reviewedTime, actionTime);
    }

    @Override
    public Boolean updateAbuseTag(AbuseTag abuseTag) throws BaseException
    {
        return getAbuseTagService().updateAbuseTag(abuseTag);
    }

    @Override
    public Boolean deleteAbuseTag(String guid) throws BaseException
    {
        return getAbuseTagService().deleteAbuseTag(guid);
    }

    @Override
    public Boolean deleteAbuseTag(AbuseTag abuseTag) throws BaseException
    {
        return getAbuseTagService().deleteAbuseTag(abuseTag);
    }

    @Override
    public Long deleteAbuseTags(String filter, String params, List<String> values) throws BaseException
    {
        return getAbuseTagService().deleteAbuseTags(filter, params, values);
    }

}
