package com.cannyurl.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.AlbumShortLink;
import com.myurldb.ws.service.AlbumShortLinkService;
import com.cannyurl.af.proxy.AlbumShortLinkServiceProxy;

public class LocalAlbumShortLinkServiceProxy extends BaseLocalServiceProxy implements AlbumShortLinkServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalAlbumShortLinkServiceProxy.class.getName());

    public LocalAlbumShortLinkServiceProxy()
    {
    }

    @Override
    public AlbumShortLink getAlbumShortLink(String guid) throws BaseException
    {
        return getAlbumShortLinkService().getAlbumShortLink(guid);
    }

    @Override
    public Object getAlbumShortLink(String guid, String field) throws BaseException
    {
        return getAlbumShortLinkService().getAlbumShortLink(guid, field);       
    }

    @Override
    public List<AlbumShortLink> getAlbumShortLinks(List<String> guids) throws BaseException
    {
        return getAlbumShortLinkService().getAlbumShortLinks(guids);
    }

    @Override
    public List<AlbumShortLink> getAllAlbumShortLinks() throws BaseException
    {
        return getAllAlbumShortLinks(null, null, null);
    }

    @Override
    public List<AlbumShortLink> getAllAlbumShortLinks(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAlbumShortLinkService().getAllAlbumShortLinks(ordering, offset, count);
    }

    @Override
    public List<String> getAllAlbumShortLinkKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAlbumShortLinkService().getAllAlbumShortLinkKeys(ordering, offset, count);
    }

    @Override
    public List<AlbumShortLink> findAlbumShortLinks(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findAlbumShortLinks(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<AlbumShortLink> findAlbumShortLinks(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getAlbumShortLinkService().findAlbumShortLinks(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findAlbumShortLinkKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getAlbumShortLinkService().findAlbumShortLinkKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getAlbumShortLinkService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createAlbumShortLink(String user, String linkAlbum, String shortLink, String shortUrl, String longUrl, String note, String status) throws BaseException
    {
        return getAlbumShortLinkService().createAlbumShortLink(user, linkAlbum, shortLink, shortUrl, longUrl, note, status);
    }

    @Override
    public String createAlbumShortLink(AlbumShortLink albumShortLink) throws BaseException
    {
        return getAlbumShortLinkService().createAlbumShortLink(albumShortLink);
    }

    @Override
    public Boolean updateAlbumShortLink(String guid, String user, String linkAlbum, String shortLink, String shortUrl, String longUrl, String note, String status) throws BaseException
    {
        return getAlbumShortLinkService().updateAlbumShortLink(guid, user, linkAlbum, shortLink, shortUrl, longUrl, note, status);
    }

    @Override
    public Boolean updateAlbumShortLink(AlbumShortLink albumShortLink) throws BaseException
    {
        return getAlbumShortLinkService().updateAlbumShortLink(albumShortLink);
    }

    @Override
    public Boolean deleteAlbumShortLink(String guid) throws BaseException
    {
        return getAlbumShortLinkService().deleteAlbumShortLink(guid);
    }

    @Override
    public Boolean deleteAlbumShortLink(AlbumShortLink albumShortLink) throws BaseException
    {
        return getAlbumShortLinkService().deleteAlbumShortLink(albumShortLink);
    }

    @Override
    public Long deleteAlbumShortLinks(String filter, String params, List<String> values) throws BaseException
    {
        return getAlbumShortLinkService().deleteAlbumShortLinks(filter, params, values);
    }

}
