package com.cannyurl.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.ApiConsumer;
import com.myurldb.ws.service.ApiConsumerService;
import com.cannyurl.af.proxy.ApiConsumerServiceProxy;

public class LocalApiConsumerServiceProxy extends BaseLocalServiceProxy implements ApiConsumerServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalApiConsumerServiceProxy.class.getName());

    public LocalApiConsumerServiceProxy()
    {
    }

    @Override
    public ApiConsumer getApiConsumer(String guid) throws BaseException
    {
        return getApiConsumerService().getApiConsumer(guid);
    }

    @Override
    public Object getApiConsumer(String guid, String field) throws BaseException
    {
        return getApiConsumerService().getApiConsumer(guid, field);       
    }

    @Override
    public List<ApiConsumer> getApiConsumers(List<String> guids) throws BaseException
    {
        return getApiConsumerService().getApiConsumers(guids);
    }

    @Override
    public List<ApiConsumer> getAllApiConsumers() throws BaseException
    {
        return getAllApiConsumers(null, null, null);
    }

    @Override
    public List<ApiConsumer> getAllApiConsumers(String ordering, Long offset, Integer count) throws BaseException
    {
        return getApiConsumerService().getAllApiConsumers(ordering, offset, count);
    }

    @Override
    public List<String> getAllApiConsumerKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getApiConsumerService().getAllApiConsumerKeys(ordering, offset, count);
    }

    @Override
    public List<ApiConsumer> findApiConsumers(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findApiConsumers(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<ApiConsumer> findApiConsumers(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getApiConsumerService().findApiConsumers(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findApiConsumerKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getApiConsumerService().findApiConsumerKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getApiConsumerService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createApiConsumer(String aeryId, String name, String description, String appKey, String appSecret, String status) throws BaseException
    {
        return getApiConsumerService().createApiConsumer(aeryId, name, description, appKey, appSecret, status);
    }

    @Override
    public String createApiConsumer(ApiConsumer apiConsumer) throws BaseException
    {
        return getApiConsumerService().createApiConsumer(apiConsumer);
    }

    @Override
    public Boolean updateApiConsumer(String guid, String aeryId, String name, String description, String appKey, String appSecret, String status) throws BaseException
    {
        return getApiConsumerService().updateApiConsumer(guid, aeryId, name, description, appKey, appSecret, status);
    }

    @Override
    public Boolean updateApiConsumer(ApiConsumer apiConsumer) throws BaseException
    {
        return getApiConsumerService().updateApiConsumer(apiConsumer);
    }

    @Override
    public Boolean deleteApiConsumer(String guid) throws BaseException
    {
        return getApiConsumerService().deleteApiConsumer(guid);
    }

    @Override
    public Boolean deleteApiConsumer(ApiConsumer apiConsumer) throws BaseException
    {
        return getApiConsumerService().deleteApiConsumer(apiConsumer);
    }

    @Override
    public Long deleteApiConsumers(String filter, String params, List<String> values) throws BaseException
    {
        return getApiConsumerService().deleteApiConsumers(filter, params, values);
    }

}
