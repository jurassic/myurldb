package com.cannyurl.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.AppClient;
import com.myurldb.ws.service.AppClientService;
import com.cannyurl.af.proxy.AppClientServiceProxy;

public class LocalAppClientServiceProxy extends BaseLocalServiceProxy implements AppClientServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalAppClientServiceProxy.class.getName());

    public LocalAppClientServiceProxy()
    {
    }

    @Override
    public AppClient getAppClient(String guid) throws BaseException
    {
        return getAppClientService().getAppClient(guid);
    }

    @Override
    public Object getAppClient(String guid, String field) throws BaseException
    {
        return getAppClientService().getAppClient(guid, field);       
    }

    @Override
    public List<AppClient> getAppClients(List<String> guids) throws BaseException
    {
        return getAppClientService().getAppClients(guids);
    }

    @Override
    public List<AppClient> getAllAppClients() throws BaseException
    {
        return getAllAppClients(null, null, null);
    }

    @Override
    public List<AppClient> getAllAppClients(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAppClientService().getAllAppClients(ordering, offset, count);
    }

    @Override
    public List<String> getAllAppClientKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAppClientService().getAllAppClientKeys(ordering, offset, count);
    }

    @Override
    public List<AppClient> findAppClients(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findAppClients(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<AppClient> findAppClients(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getAppClientService().findAppClients(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findAppClientKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getAppClientService().findAppClientKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getAppClientService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createAppClient(String owner, String admin, String name, String clientId, String clientCode, String rootDomain, String appDomain, Boolean useAppDomain, Boolean enabled, String licenseMode, String status) throws BaseException
    {
        return getAppClientService().createAppClient(owner, admin, name, clientId, clientCode, rootDomain, appDomain, useAppDomain, enabled, licenseMode, status);
    }

    @Override
    public String createAppClient(AppClient appClient) throws BaseException
    {
        return getAppClientService().createAppClient(appClient);
    }

    @Override
    public Boolean updateAppClient(String guid, String owner, String admin, String name, String clientId, String clientCode, String rootDomain, String appDomain, Boolean useAppDomain, Boolean enabled, String licenseMode, String status) throws BaseException
    {
        return getAppClientService().updateAppClient(guid, owner, admin, name, clientId, clientCode, rootDomain, appDomain, useAppDomain, enabled, licenseMode, status);
    }

    @Override
    public Boolean updateAppClient(AppClient appClient) throws BaseException
    {
        return getAppClientService().updateAppClient(appClient);
    }

    @Override
    public Boolean deleteAppClient(String guid) throws BaseException
    {
        return getAppClientService().deleteAppClient(guid);
    }

    @Override
    public Boolean deleteAppClient(AppClient appClient) throws BaseException
    {
        return getAppClientService().deleteAppClient(appClient);
    }

    @Override
    public Long deleteAppClients(String filter, String params, List<String> values) throws BaseException
    {
        return getAppClientService().deleteAppClients(filter, params, values);
    }

}
