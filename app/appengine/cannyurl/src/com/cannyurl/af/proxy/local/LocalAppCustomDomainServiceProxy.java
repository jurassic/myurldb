package com.cannyurl.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.AppCustomDomain;
import com.myurldb.ws.service.AppCustomDomainService;
import com.cannyurl.af.proxy.AppCustomDomainServiceProxy;

public class LocalAppCustomDomainServiceProxy extends BaseLocalServiceProxy implements AppCustomDomainServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalAppCustomDomainServiceProxy.class.getName());

    public LocalAppCustomDomainServiceProxy()
    {
    }

    @Override
    public AppCustomDomain getAppCustomDomain(String guid) throws BaseException
    {
        return getAppCustomDomainService().getAppCustomDomain(guid);
    }

    @Override
    public Object getAppCustomDomain(String guid, String field) throws BaseException
    {
        return getAppCustomDomainService().getAppCustomDomain(guid, field);       
    }

    @Override
    public List<AppCustomDomain> getAppCustomDomains(List<String> guids) throws BaseException
    {
        return getAppCustomDomainService().getAppCustomDomains(guids);
    }

    @Override
    public List<AppCustomDomain> getAllAppCustomDomains() throws BaseException
    {
        return getAllAppCustomDomains(null, null, null);
    }

    @Override
    public List<AppCustomDomain> getAllAppCustomDomains(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAppCustomDomainService().getAllAppCustomDomains(ordering, offset, count);
    }

    @Override
    public List<String> getAllAppCustomDomainKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAppCustomDomainService().getAllAppCustomDomainKeys(ordering, offset, count);
    }

    @Override
    public List<AppCustomDomain> findAppCustomDomains(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findAppCustomDomains(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<AppCustomDomain> findAppCustomDomains(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getAppCustomDomainService().findAppCustomDomains(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findAppCustomDomainKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getAppCustomDomainService().findAppCustomDomainKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getAppCustomDomainService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createAppCustomDomain(String appId, String siteDomain, String domain, Boolean verified, String status, Long verifiedTime) throws BaseException
    {
        return getAppCustomDomainService().createAppCustomDomain(appId, siteDomain, domain, verified, status, verifiedTime);
    }

    @Override
    public String createAppCustomDomain(AppCustomDomain appCustomDomain) throws BaseException
    {
        return getAppCustomDomainService().createAppCustomDomain(appCustomDomain);
    }

    @Override
    public Boolean updateAppCustomDomain(String guid, String appId, String siteDomain, String domain, Boolean verified, String status, Long verifiedTime) throws BaseException
    {
        return getAppCustomDomainService().updateAppCustomDomain(guid, appId, siteDomain, domain, verified, status, verifiedTime);
    }

    @Override
    public Boolean updateAppCustomDomain(AppCustomDomain appCustomDomain) throws BaseException
    {
        return getAppCustomDomainService().updateAppCustomDomain(appCustomDomain);
    }

    @Override
    public Boolean deleteAppCustomDomain(String guid) throws BaseException
    {
        return getAppCustomDomainService().deleteAppCustomDomain(guid);
    }

    @Override
    public Boolean deleteAppCustomDomain(AppCustomDomain appCustomDomain) throws BaseException
    {
        return getAppCustomDomainService().deleteAppCustomDomain(appCustomDomain);
    }

    @Override
    public Long deleteAppCustomDomains(String filter, String params, List<String> values) throws BaseException
    {
        return getAppCustomDomainService().deleteAppCustomDomains(filter, params, values);
    }

}
