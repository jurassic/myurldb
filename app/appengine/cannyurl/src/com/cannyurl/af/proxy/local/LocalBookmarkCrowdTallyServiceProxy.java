package com.cannyurl.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.BookmarkCrowdTally;
import com.myurldb.ws.service.BookmarkCrowdTallyService;
import com.cannyurl.af.proxy.BookmarkCrowdTallyServiceProxy;

public class LocalBookmarkCrowdTallyServiceProxy extends BaseLocalServiceProxy implements BookmarkCrowdTallyServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalBookmarkCrowdTallyServiceProxy.class.getName());

    public LocalBookmarkCrowdTallyServiceProxy()
    {
    }

    @Override
    public BookmarkCrowdTally getBookmarkCrowdTally(String guid) throws BaseException
    {
        return getBookmarkCrowdTallyService().getBookmarkCrowdTally(guid);
    }

    @Override
    public Object getBookmarkCrowdTally(String guid, String field) throws BaseException
    {
        return getBookmarkCrowdTallyService().getBookmarkCrowdTally(guid, field);       
    }

    @Override
    public List<BookmarkCrowdTally> getBookmarkCrowdTallies(List<String> guids) throws BaseException
    {
        return getBookmarkCrowdTallyService().getBookmarkCrowdTallies(guids);
    }

    @Override
    public List<BookmarkCrowdTally> getAllBookmarkCrowdTallies() throws BaseException
    {
        return getAllBookmarkCrowdTallies(null, null, null);
    }

    @Override
    public List<BookmarkCrowdTally> getAllBookmarkCrowdTallies(String ordering, Long offset, Integer count) throws BaseException
    {
        return getBookmarkCrowdTallyService().getAllBookmarkCrowdTallies(ordering, offset, count);
    }

    @Override
    public List<String> getAllBookmarkCrowdTallyKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getBookmarkCrowdTallyService().getAllBookmarkCrowdTallyKeys(ordering, offset, count);
    }

    @Override
    public List<BookmarkCrowdTally> findBookmarkCrowdTallies(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findBookmarkCrowdTallies(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<BookmarkCrowdTally> findBookmarkCrowdTallies(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getBookmarkCrowdTallyService().findBookmarkCrowdTallies(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findBookmarkCrowdTallyKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getBookmarkCrowdTallyService().findBookmarkCrowdTallyKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getBookmarkCrowdTallyService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createBookmarkCrowdTally(String user, String shortLink, String domain, String token, String longUrl, String shortUrl, String tallyDate, String status, String note, Long expirationTime, String bookmarkFolder, String contentTag, String referenceElement, String elementType, String keywordLink) throws BaseException
    {
        return getBookmarkCrowdTallyService().createBookmarkCrowdTally(user, shortLink, domain, token, longUrl, shortUrl, tallyDate, status, note, expirationTime, bookmarkFolder, contentTag, referenceElement, elementType, keywordLink);
    }

    @Override
    public String createBookmarkCrowdTally(BookmarkCrowdTally bookmarkCrowdTally) throws BaseException
    {
        return getBookmarkCrowdTallyService().createBookmarkCrowdTally(bookmarkCrowdTally);
    }

    @Override
    public Boolean updateBookmarkCrowdTally(String guid, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, String tallyDate, String status, String note, Long expirationTime, String bookmarkFolder, String contentTag, String referenceElement, String elementType, String keywordLink) throws BaseException
    {
        return getBookmarkCrowdTallyService().updateBookmarkCrowdTally(guid, user, shortLink, domain, token, longUrl, shortUrl, tallyDate, status, note, expirationTime, bookmarkFolder, contentTag, referenceElement, elementType, keywordLink);
    }

    @Override
    public Boolean updateBookmarkCrowdTally(BookmarkCrowdTally bookmarkCrowdTally) throws BaseException
    {
        return getBookmarkCrowdTallyService().updateBookmarkCrowdTally(bookmarkCrowdTally);
    }

    @Override
    public Boolean deleteBookmarkCrowdTally(String guid) throws BaseException
    {
        return getBookmarkCrowdTallyService().deleteBookmarkCrowdTally(guid);
    }

    @Override
    public Boolean deleteBookmarkCrowdTally(BookmarkCrowdTally bookmarkCrowdTally) throws BaseException
    {
        return getBookmarkCrowdTallyService().deleteBookmarkCrowdTally(bookmarkCrowdTally);
    }

    @Override
    public Long deleteBookmarkCrowdTallies(String filter, String params, List<String> values) throws BaseException
    {
        return getBookmarkCrowdTallyService().deleteBookmarkCrowdTallies(filter, params, values);
    }

}
