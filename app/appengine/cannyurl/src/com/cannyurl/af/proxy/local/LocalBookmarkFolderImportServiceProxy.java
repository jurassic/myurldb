package com.cannyurl.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.BookmarkFolderImport;
import com.myurldb.ws.service.BookmarkFolderImportService;
import com.cannyurl.af.proxy.BookmarkFolderImportServiceProxy;

public class LocalBookmarkFolderImportServiceProxy extends BaseLocalServiceProxy implements BookmarkFolderImportServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalBookmarkFolderImportServiceProxy.class.getName());

    public LocalBookmarkFolderImportServiceProxy()
    {
    }

    @Override
    public BookmarkFolderImport getBookmarkFolderImport(String guid) throws BaseException
    {
        return getBookmarkFolderImportService().getBookmarkFolderImport(guid);
    }

    @Override
    public Object getBookmarkFolderImport(String guid, String field) throws BaseException
    {
        return getBookmarkFolderImportService().getBookmarkFolderImport(guid, field);       
    }

    @Override
    public List<BookmarkFolderImport> getBookmarkFolderImports(List<String> guids) throws BaseException
    {
        return getBookmarkFolderImportService().getBookmarkFolderImports(guids);
    }

    @Override
    public List<BookmarkFolderImport> getAllBookmarkFolderImports() throws BaseException
    {
        return getAllBookmarkFolderImports(null, null, null);
    }

    @Override
    public List<BookmarkFolderImport> getAllBookmarkFolderImports(String ordering, Long offset, Integer count) throws BaseException
    {
        return getBookmarkFolderImportService().getAllBookmarkFolderImports(ordering, offset, count);
    }

    @Override
    public List<String> getAllBookmarkFolderImportKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getBookmarkFolderImportService().getAllBookmarkFolderImportKeys(ordering, offset, count);
    }

    @Override
    public List<BookmarkFolderImport> findBookmarkFolderImports(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findBookmarkFolderImports(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<BookmarkFolderImport> findBookmarkFolderImports(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getBookmarkFolderImportService().findBookmarkFolderImports(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findBookmarkFolderImportKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getBookmarkFolderImportService().findBookmarkFolderImportKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getBookmarkFolderImportService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createBookmarkFolderImport(String user, Integer precedence, String importType, String importedFolder, String importedFolderUser, String importedFolderTitle, String importedFolderPath, String status, String note, String bookmarkFolder) throws BaseException
    {
        return getBookmarkFolderImportService().createBookmarkFolderImport(user, precedence, importType, importedFolder, importedFolderUser, importedFolderTitle, importedFolderPath, status, note, bookmarkFolder);
    }

    @Override
    public String createBookmarkFolderImport(BookmarkFolderImport bookmarkFolderImport) throws BaseException
    {
        return getBookmarkFolderImportService().createBookmarkFolderImport(bookmarkFolderImport);
    }

    @Override
    public Boolean updateBookmarkFolderImport(String guid, String user, Integer precedence, String importType, String importedFolder, String importedFolderUser, String importedFolderTitle, String importedFolderPath, String status, String note, String bookmarkFolder) throws BaseException
    {
        return getBookmarkFolderImportService().updateBookmarkFolderImport(guid, user, precedence, importType, importedFolder, importedFolderUser, importedFolderTitle, importedFolderPath, status, note, bookmarkFolder);
    }

    @Override
    public Boolean updateBookmarkFolderImport(BookmarkFolderImport bookmarkFolderImport) throws BaseException
    {
        return getBookmarkFolderImportService().updateBookmarkFolderImport(bookmarkFolderImport);
    }

    @Override
    public Boolean deleteBookmarkFolderImport(String guid) throws BaseException
    {
        return getBookmarkFolderImportService().deleteBookmarkFolderImport(guid);
    }

    @Override
    public Boolean deleteBookmarkFolderImport(BookmarkFolderImport bookmarkFolderImport) throws BaseException
    {
        return getBookmarkFolderImportService().deleteBookmarkFolderImport(bookmarkFolderImport);
    }

    @Override
    public Long deleteBookmarkFolderImports(String filter, String params, List<String> values) throws BaseException
    {
        return getBookmarkFolderImportService().deleteBookmarkFolderImports(filter, params, values);
    }

}
