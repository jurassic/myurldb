package com.cannyurl.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.BookmarkFolder;
import com.myurldb.ws.service.BookmarkFolderService;
import com.cannyurl.af.proxy.BookmarkFolderServiceProxy;

public class LocalBookmarkFolderServiceProxy extends BaseLocalServiceProxy implements BookmarkFolderServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalBookmarkFolderServiceProxy.class.getName());

    public LocalBookmarkFolderServiceProxy()
    {
    }

    @Override
    public BookmarkFolder getBookmarkFolder(String guid) throws BaseException
    {
        return getBookmarkFolderService().getBookmarkFolder(guid);
    }

    @Override
    public Object getBookmarkFolder(String guid, String field) throws BaseException
    {
        return getBookmarkFolderService().getBookmarkFolder(guid, field);       
    }

    @Override
    public List<BookmarkFolder> getBookmarkFolders(List<String> guids) throws BaseException
    {
        return getBookmarkFolderService().getBookmarkFolders(guids);
    }

    @Override
    public List<BookmarkFolder> getAllBookmarkFolders() throws BaseException
    {
        return getAllBookmarkFolders(null, null, null);
    }

    @Override
    public List<BookmarkFolder> getAllBookmarkFolders(String ordering, Long offset, Integer count) throws BaseException
    {
        return getBookmarkFolderService().getAllBookmarkFolders(ordering, offset, count);
    }

    @Override
    public List<String> getAllBookmarkFolderKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getBookmarkFolderService().getAllBookmarkFolderKeys(ordering, offset, count);
    }

    @Override
    public List<BookmarkFolder> findBookmarkFolders(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findBookmarkFolders(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<BookmarkFolder> findBookmarkFolders(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getBookmarkFolderService().findBookmarkFolders(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findBookmarkFolderKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getBookmarkFolderService().findBookmarkFolderKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getBookmarkFolderService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createBookmarkFolder(String appClient, String clientRootDomain, String user, String title, String description, String type, String category, String parent, String aggregate, String acl, Boolean exportable, String status, String note, String keywordFolder) throws BaseException
    {
        return getBookmarkFolderService().createBookmarkFolder(appClient, clientRootDomain, user, title, description, type, category, parent, aggregate, acl, exportable, status, note, keywordFolder);
    }

    @Override
    public String createBookmarkFolder(BookmarkFolder bookmarkFolder) throws BaseException
    {
        return getBookmarkFolderService().createBookmarkFolder(bookmarkFolder);
    }

    @Override
    public Boolean updateBookmarkFolder(String guid, String appClient, String clientRootDomain, String user, String title, String description, String type, String category, String parent, String aggregate, String acl, Boolean exportable, String status, String note, String keywordFolder) throws BaseException
    {
        return getBookmarkFolderService().updateBookmarkFolder(guid, appClient, clientRootDomain, user, title, description, type, category, parent, aggregate, acl, exportable, status, note, keywordFolder);
    }

    @Override
    public Boolean updateBookmarkFolder(BookmarkFolder bookmarkFolder) throws BaseException
    {
        return getBookmarkFolderService().updateBookmarkFolder(bookmarkFolder);
    }

    @Override
    public Boolean deleteBookmarkFolder(String guid) throws BaseException
    {
        return getBookmarkFolderService().deleteBookmarkFolder(guid);
    }

    @Override
    public Boolean deleteBookmarkFolder(BookmarkFolder bookmarkFolder) throws BaseException
    {
        return getBookmarkFolderService().deleteBookmarkFolder(bookmarkFolder);
    }

    @Override
    public Long deleteBookmarkFolders(String filter, String params, List<String> values) throws BaseException
    {
        return getBookmarkFolderService().deleteBookmarkFolders(filter, params, values);
    }

}
