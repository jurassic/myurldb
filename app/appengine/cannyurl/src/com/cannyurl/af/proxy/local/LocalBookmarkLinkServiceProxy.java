package com.cannyurl.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.BookmarkLink;
import com.myurldb.ws.service.BookmarkLinkService;
import com.cannyurl.af.proxy.BookmarkLinkServiceProxy;

public class LocalBookmarkLinkServiceProxy extends BaseLocalServiceProxy implements BookmarkLinkServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalBookmarkLinkServiceProxy.class.getName());

    public LocalBookmarkLinkServiceProxy()
    {
    }

    @Override
    public BookmarkLink getBookmarkLink(String guid) throws BaseException
    {
        return getBookmarkLinkService().getBookmarkLink(guid);
    }

    @Override
    public Object getBookmarkLink(String guid, String field) throws BaseException
    {
        return getBookmarkLinkService().getBookmarkLink(guid, field);       
    }

    @Override
    public List<BookmarkLink> getBookmarkLinks(List<String> guids) throws BaseException
    {
        return getBookmarkLinkService().getBookmarkLinks(guids);
    }

    @Override
    public List<BookmarkLink> getAllBookmarkLinks() throws BaseException
    {
        return getAllBookmarkLinks(null, null, null);
    }

    @Override
    public List<BookmarkLink> getAllBookmarkLinks(String ordering, Long offset, Integer count) throws BaseException
    {
        return getBookmarkLinkService().getAllBookmarkLinks(ordering, offset, count);
    }

    @Override
    public List<String> getAllBookmarkLinkKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getBookmarkLinkService().getAllBookmarkLinkKeys(ordering, offset, count);
    }

    @Override
    public List<BookmarkLink> findBookmarkLinks(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findBookmarkLinks(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<BookmarkLink> findBookmarkLinks(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getBookmarkLinkService().findBookmarkLinks(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findBookmarkLinkKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getBookmarkLinkService().findBookmarkLinkKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getBookmarkLinkService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createBookmarkLink(String appClient, String clientRootDomain, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, Boolean internal, Boolean caching, String memo, String status, String note, Long expirationTime, String bookmarkFolder, String contentTag, String referenceElement, String elementType, String keywordLink) throws BaseException
    {
        return getBookmarkLinkService().createBookmarkLink(appClient, clientRootDomain, user, shortLink, domain, token, longUrl, shortUrl, internal, caching, memo, status, note, expirationTime, bookmarkFolder, contentTag, referenceElement, elementType, keywordLink);
    }

    @Override
    public String createBookmarkLink(BookmarkLink bookmarkLink) throws BaseException
    {
        return getBookmarkLinkService().createBookmarkLink(bookmarkLink);
    }

    @Override
    public Boolean updateBookmarkLink(String guid, String appClient, String clientRootDomain, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, Boolean internal, Boolean caching, String memo, String status, String note, Long expirationTime, String bookmarkFolder, String contentTag, String referenceElement, String elementType, String keywordLink) throws BaseException
    {
        return getBookmarkLinkService().updateBookmarkLink(guid, appClient, clientRootDomain, user, shortLink, domain, token, longUrl, shortUrl, internal, caching, memo, status, note, expirationTime, bookmarkFolder, contentTag, referenceElement, elementType, keywordLink);
    }

    @Override
    public Boolean updateBookmarkLink(BookmarkLink bookmarkLink) throws BaseException
    {
        return getBookmarkLinkService().updateBookmarkLink(bookmarkLink);
    }

    @Override
    public Boolean deleteBookmarkLink(String guid) throws BaseException
    {
        return getBookmarkLinkService().deleteBookmarkLink(guid);
    }

    @Override
    public Boolean deleteBookmarkLink(BookmarkLink bookmarkLink) throws BaseException
    {
        return getBookmarkLinkService().deleteBookmarkLink(bookmarkLink);
    }

    @Override
    public Long deleteBookmarkLinks(String filter, String params, List<String> values) throws BaseException
    {
        return getBookmarkLinkService().deleteBookmarkLinks(filter, params, values);
    }

}
