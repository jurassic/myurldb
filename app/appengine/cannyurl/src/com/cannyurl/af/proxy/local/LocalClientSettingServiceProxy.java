package com.cannyurl.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.ClientSetting;
import com.myurldb.ws.service.ClientSettingService;
import com.cannyurl.af.proxy.ClientSettingServiceProxy;

public class LocalClientSettingServiceProxy extends BaseLocalServiceProxy implements ClientSettingServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalClientSettingServiceProxy.class.getName());

    public LocalClientSettingServiceProxy()
    {
    }

    @Override
    public ClientSetting getClientSetting(String guid) throws BaseException
    {
        return getClientSettingService().getClientSetting(guid);
    }

    @Override
    public Object getClientSetting(String guid, String field) throws BaseException
    {
        return getClientSettingService().getClientSetting(guid, field);       
    }

    @Override
    public List<ClientSetting> getClientSettings(List<String> guids) throws BaseException
    {
        return getClientSettingService().getClientSettings(guids);
    }

    @Override
    public List<ClientSetting> getAllClientSettings() throws BaseException
    {
        return getAllClientSettings(null, null, null);
    }

    @Override
    public List<ClientSetting> getAllClientSettings(String ordering, Long offset, Integer count) throws BaseException
    {
        return getClientSettingService().getAllClientSettings(ordering, offset, count);
    }

    @Override
    public List<String> getAllClientSettingKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getClientSettingService().getAllClientSettingKeys(ordering, offset, count);
    }

    @Override
    public List<ClientSetting> findClientSettings(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findClientSettings(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<ClientSetting> findClientSettings(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getClientSettingService().findClientSettings(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findClientSettingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getClientSettingService().findClientSettingKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getClientSettingService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createClientSetting(String appClient, String admin, Boolean autoRedirectEnabled, Boolean viewEnabled, Boolean shareEnabled, String defaultBaseDomain, String defaultDomainType, String defaultTokenType, String defaultRedirectType, Long defaultFlashDuration, String defaultAccessType, String defaultViewType) throws BaseException
    {
        return getClientSettingService().createClientSetting(appClient, admin, autoRedirectEnabled, viewEnabled, shareEnabled, defaultBaseDomain, defaultDomainType, defaultTokenType, defaultRedirectType, defaultFlashDuration, defaultAccessType, defaultViewType);
    }

    @Override
    public String createClientSetting(ClientSetting clientSetting) throws BaseException
    {
        return getClientSettingService().createClientSetting(clientSetting);
    }

    @Override
    public Boolean updateClientSetting(String guid, String appClient, String admin, Boolean autoRedirectEnabled, Boolean viewEnabled, Boolean shareEnabled, String defaultBaseDomain, String defaultDomainType, String defaultTokenType, String defaultRedirectType, Long defaultFlashDuration, String defaultAccessType, String defaultViewType) throws BaseException
    {
        return getClientSettingService().updateClientSetting(guid, appClient, admin, autoRedirectEnabled, viewEnabled, shareEnabled, defaultBaseDomain, defaultDomainType, defaultTokenType, defaultRedirectType, defaultFlashDuration, defaultAccessType, defaultViewType);
    }

    @Override
    public Boolean updateClientSetting(ClientSetting clientSetting) throws BaseException
    {
        return getClientSettingService().updateClientSetting(clientSetting);
    }

    @Override
    public Boolean deleteClientSetting(String guid) throws BaseException
    {
        return getClientSettingService().deleteClientSetting(guid);
    }

    @Override
    public Boolean deleteClientSetting(ClientSetting clientSetting) throws BaseException
    {
        return getClientSettingService().deleteClientSetting(clientSetting);
    }

    @Override
    public Long deleteClientSettings(String filter, String params, List<String> values) throws BaseException
    {
        return getClientSettingService().deleteClientSettings(filter, params, values);
    }

}
