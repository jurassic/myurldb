package com.cannyurl.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.ClientUser;
import com.myurldb.ws.service.ClientUserService;
import com.cannyurl.af.proxy.ClientUserServiceProxy;

public class LocalClientUserServiceProxy extends BaseLocalServiceProxy implements ClientUserServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalClientUserServiceProxy.class.getName());

    public LocalClientUserServiceProxy()
    {
    }

    @Override
    public ClientUser getClientUser(String guid) throws BaseException
    {
        return getClientUserService().getClientUser(guid);
    }

    @Override
    public Object getClientUser(String guid, String field) throws BaseException
    {
        return getClientUserService().getClientUser(guid, field);       
    }

    @Override
    public List<ClientUser> getClientUsers(List<String> guids) throws BaseException
    {
        return getClientUserService().getClientUsers(guids);
    }

    @Override
    public List<ClientUser> getAllClientUsers() throws BaseException
    {
        return getAllClientUsers(null, null, null);
    }

    @Override
    public List<ClientUser> getAllClientUsers(String ordering, Long offset, Integer count) throws BaseException
    {
        return getClientUserService().getAllClientUsers(ordering, offset, count);
    }

    @Override
    public List<String> getAllClientUserKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getClientUserService().getAllClientUserKeys(ordering, offset, count);
    }

    @Override
    public List<ClientUser> findClientUsers(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findClientUsers(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<ClientUser> findClientUsers(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getClientUserService().findClientUsers(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findClientUserKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getClientUserService().findClientUserKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getClientUserService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createClientUser(String appClient, String user, String role, Boolean provisioned, Boolean licensed, String status) throws BaseException
    {
        return getClientUserService().createClientUser(appClient, user, role, provisioned, licensed, status);
    }

    @Override
    public String createClientUser(ClientUser clientUser) throws BaseException
    {
        return getClientUserService().createClientUser(clientUser);
    }

    @Override
    public Boolean updateClientUser(String guid, String appClient, String user, String role, Boolean provisioned, Boolean licensed, String status) throws BaseException
    {
        return getClientUserService().updateClientUser(guid, appClient, user, role, provisioned, licensed, status);
    }

    @Override
    public Boolean updateClientUser(ClientUser clientUser) throws BaseException
    {
        return getClientUserService().updateClientUser(clientUser);
    }

    @Override
    public Boolean deleteClientUser(String guid) throws BaseException
    {
        return getClientUserService().deleteClientUser(guid);
    }

    @Override
    public Boolean deleteClientUser(ClientUser clientUser) throws BaseException
    {
        return getClientUserService().deleteClientUser(clientUser);
    }

    @Override
    public Long deleteClientUsers(String filter, String params, List<String> values) throws BaseException
    {
        return getClientUserService().deleteClientUsers(filter, params, values);
    }

}
