package com.cannyurl.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.DomainInfo;
import com.myurldb.ws.service.DomainInfoService;
import com.cannyurl.af.proxy.DomainInfoServiceProxy;

public class LocalDomainInfoServiceProxy extends BaseLocalServiceProxy implements DomainInfoServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalDomainInfoServiceProxy.class.getName());

    public LocalDomainInfoServiceProxy()
    {
    }

    @Override
    public DomainInfo getDomainInfo(String guid) throws BaseException
    {
        return getDomainInfoService().getDomainInfo(guid);
    }

    @Override
    public Object getDomainInfo(String guid, String field) throws BaseException
    {
        return getDomainInfoService().getDomainInfo(guid, field);       
    }

    @Override
    public List<DomainInfo> getDomainInfos(List<String> guids) throws BaseException
    {
        return getDomainInfoService().getDomainInfos(guids);
    }

    @Override
    public List<DomainInfo> getAllDomainInfos() throws BaseException
    {
        return getAllDomainInfos(null, null, null);
    }

    @Override
    public List<DomainInfo> getAllDomainInfos(String ordering, Long offset, Integer count) throws BaseException
    {
        return getDomainInfoService().getAllDomainInfos(ordering, offset, count);
    }

    @Override
    public List<String> getAllDomainInfoKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getDomainInfoService().getAllDomainInfoKeys(ordering, offset, count);
    }

    @Override
    public List<DomainInfo> findDomainInfos(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findDomainInfos(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<DomainInfo> findDomainInfos(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getDomainInfoService().findDomainInfos(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findDomainInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getDomainInfoService().findDomainInfoKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getDomainInfoService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createDomainInfo(String domain, Boolean banned, Boolean urlShortener, String category, String reputation, String authority, String note, Long verifiedTime) throws BaseException
    {
        return getDomainInfoService().createDomainInfo(domain, banned, urlShortener, category, reputation, authority, note, verifiedTime);
    }

    @Override
    public String createDomainInfo(DomainInfo domainInfo) throws BaseException
    {
        return getDomainInfoService().createDomainInfo(domainInfo);
    }

    @Override
    public Boolean updateDomainInfo(String guid, String domain, Boolean banned, Boolean urlShortener, String category, String reputation, String authority, String note, Long verifiedTime) throws BaseException
    {
        return getDomainInfoService().updateDomainInfo(guid, domain, banned, urlShortener, category, reputation, authority, note, verifiedTime);
    }

    @Override
    public Boolean updateDomainInfo(DomainInfo domainInfo) throws BaseException
    {
        return getDomainInfoService().updateDomainInfo(domainInfo);
    }

    @Override
    public Boolean deleteDomainInfo(String guid) throws BaseException
    {
        return getDomainInfoService().deleteDomainInfo(guid);
    }

    @Override
    public Boolean deleteDomainInfo(DomainInfo domainInfo) throws BaseException
    {
        return getDomainInfoService().deleteDomainInfo(domainInfo);
    }

    @Override
    public Long deleteDomainInfos(String filter, String params, List<String> values) throws BaseException
    {
        return getDomainInfoService().deleteDomainInfos(filter, params, values);
    }

}
