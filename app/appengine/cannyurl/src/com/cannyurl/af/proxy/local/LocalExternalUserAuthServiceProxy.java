package com.cannyurl.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.ExternalUserIdStruct;
import com.myurldb.ws.ExternalUserAuth;
import com.myurldb.ws.service.ExternalUserAuthService;
import com.cannyurl.af.proxy.ExternalUserAuthServiceProxy;

public class LocalExternalUserAuthServiceProxy extends BaseLocalServiceProxy implements ExternalUserAuthServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalExternalUserAuthServiceProxy.class.getName());

    public LocalExternalUserAuthServiceProxy()
    {
    }

    @Override
    public ExternalUserAuth getExternalUserAuth(String guid) throws BaseException
    {
        return getExternalUserAuthService().getExternalUserAuth(guid);
    }

    @Override
    public Object getExternalUserAuth(String guid, String field) throws BaseException
    {
        return getExternalUserAuthService().getExternalUserAuth(guid, field);       
    }

    @Override
    public List<ExternalUserAuth> getExternalUserAuths(List<String> guids) throws BaseException
    {
        return getExternalUserAuthService().getExternalUserAuths(guids);
    }

    @Override
    public List<ExternalUserAuth> getAllExternalUserAuths() throws BaseException
    {
        return getAllExternalUserAuths(null, null, null);
    }

    @Override
    public List<ExternalUserAuth> getAllExternalUserAuths(String ordering, Long offset, Integer count) throws BaseException
    {
        return getExternalUserAuthService().getAllExternalUserAuths(ordering, offset, count);
    }

    @Override
    public List<String> getAllExternalUserAuthKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getExternalUserAuthService().getAllExternalUserAuthKeys(ordering, offset, count);
    }

    @Override
    public List<ExternalUserAuth> findExternalUserAuths(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findExternalUserAuths(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<ExternalUserAuth> findExternalUserAuths(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getExternalUserAuthService().findExternalUserAuths(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findExternalUserAuthKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getExternalUserAuthService().findExternalUserAuthKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getExternalUserAuthService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createExternalUserAuth(String user, String authType, String providerId, String providerDomain, ExternalUserIdStruct externalUserId, String requestToken, String accessToken, String accessTokenSecret, String email, String firstName, String lastName, String fullName, String displayName, String description, String gender, String dateOfBirth, String profileImageUrl, String timeZone, String postalCode, String location, String country, String language, String status, Long authTime, Long expirationTime) throws BaseException
    {
        return getExternalUserAuthService().createExternalUserAuth(user, authType, providerId, providerDomain, externalUserId, requestToken, accessToken, accessTokenSecret, email, firstName, lastName, fullName, displayName, description, gender, dateOfBirth, profileImageUrl, timeZone, postalCode, location, country, language, status, authTime, expirationTime);
    }

    @Override
    public String createExternalUserAuth(ExternalUserAuth externalUserAuth) throws BaseException
    {
        return getExternalUserAuthService().createExternalUserAuth(externalUserAuth);
    }

    @Override
    public Boolean updateExternalUserAuth(String guid, String user, String authType, String providerId, String providerDomain, ExternalUserIdStruct externalUserId, String requestToken, String accessToken, String accessTokenSecret, String email, String firstName, String lastName, String fullName, String displayName, String description, String gender, String dateOfBirth, String profileImageUrl, String timeZone, String postalCode, String location, String country, String language, String status, Long authTime, Long expirationTime) throws BaseException
    {
        return getExternalUserAuthService().updateExternalUserAuth(guid, user, authType, providerId, providerDomain, externalUserId, requestToken, accessToken, accessTokenSecret, email, firstName, lastName, fullName, displayName, description, gender, dateOfBirth, profileImageUrl, timeZone, postalCode, location, country, language, status, authTime, expirationTime);
    }

    @Override
    public Boolean updateExternalUserAuth(ExternalUserAuth externalUserAuth) throws BaseException
    {
        return getExternalUserAuthService().updateExternalUserAuth(externalUserAuth);
    }

    @Override
    public Boolean deleteExternalUserAuth(String guid) throws BaseException
    {
        return getExternalUserAuthService().deleteExternalUserAuth(guid);
    }

    @Override
    public Boolean deleteExternalUserAuth(ExternalUserAuth externalUserAuth) throws BaseException
    {
        return getExternalUserAuthService().deleteExternalUserAuth(externalUserAuth);
    }

    @Override
    public Long deleteExternalUserAuths(String filter, String params, List<String> values) throws BaseException
    {
        return getExternalUserAuthService().deleteExternalUserAuths(filter, params, values);
    }

}
