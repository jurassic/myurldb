package com.cannyurl.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.CellLatitudeLongitude;
import com.myurldb.ws.GeoCoordinateStruct;
import com.myurldb.ws.GeoLink;
import com.myurldb.ws.service.GeoLinkService;
import com.cannyurl.af.proxy.GeoLinkServiceProxy;

public class LocalGeoLinkServiceProxy extends BaseLocalServiceProxy implements GeoLinkServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalGeoLinkServiceProxy.class.getName());

    public LocalGeoLinkServiceProxy()
    {
    }

    @Override
    public GeoLink getGeoLink(String guid) throws BaseException
    {
        return getGeoLinkService().getGeoLink(guid);
    }

    @Override
    public Object getGeoLink(String guid, String field) throws BaseException
    {
        return getGeoLinkService().getGeoLink(guid, field);       
    }

    @Override
    public List<GeoLink> getGeoLinks(List<String> guids) throws BaseException
    {
        return getGeoLinkService().getGeoLinks(guids);
    }

    @Override
    public List<GeoLink> getAllGeoLinks() throws BaseException
    {
        return getAllGeoLinks(null, null, null);
    }

    @Override
    public List<GeoLink> getAllGeoLinks(String ordering, Long offset, Integer count) throws BaseException
    {
        return getGeoLinkService().getAllGeoLinks(ordering, offset, count);
    }

    @Override
    public List<String> getAllGeoLinkKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getGeoLinkService().getAllGeoLinkKeys(ordering, offset, count);
    }

    @Override
    public List<GeoLink> findGeoLinks(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findGeoLinks(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<GeoLink> findGeoLinks(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getGeoLinkService().findGeoLinks(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findGeoLinkKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getGeoLinkService().findGeoLinkKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getGeoLinkService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createGeoLink(String shortLink, String shortUrl, GeoCoordinateStruct geoCoordinate, CellLatitudeLongitude geoCell, String status) throws BaseException
    {
        return getGeoLinkService().createGeoLink(shortLink, shortUrl, geoCoordinate, geoCell, status);
    }

    @Override
    public String createGeoLink(GeoLink geoLink) throws BaseException
    {
        return getGeoLinkService().createGeoLink(geoLink);
    }

    @Override
    public Boolean updateGeoLink(String guid, String shortLink, String shortUrl, GeoCoordinateStruct geoCoordinate, CellLatitudeLongitude geoCell, String status) throws BaseException
    {
        return getGeoLinkService().updateGeoLink(guid, shortLink, shortUrl, geoCoordinate, geoCell, status);
    }

    @Override
    public Boolean updateGeoLink(GeoLink geoLink) throws BaseException
    {
        return getGeoLinkService().updateGeoLink(geoLink);
    }

    @Override
    public Boolean deleteGeoLink(String guid) throws BaseException
    {
        return getGeoLinkService().deleteGeoLink(guid);
    }

    @Override
    public Boolean deleteGeoLink(GeoLink geoLink) throws BaseException
    {
        return getGeoLinkService().deleteGeoLink(geoLink);
    }

    @Override
    public Long deleteGeoLinks(String filter, String params, List<String> values) throws BaseException
    {
        return getGeoLinkService().deleteGeoLinks(filter, params, values);
    }

}
