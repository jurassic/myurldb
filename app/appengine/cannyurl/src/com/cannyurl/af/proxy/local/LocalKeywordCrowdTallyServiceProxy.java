package com.cannyurl.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.KeywordCrowdTally;
import com.myurldb.ws.service.KeywordCrowdTallyService;
import com.cannyurl.af.proxy.KeywordCrowdTallyServiceProxy;

public class LocalKeywordCrowdTallyServiceProxy extends BaseLocalServiceProxy implements KeywordCrowdTallyServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalKeywordCrowdTallyServiceProxy.class.getName());

    public LocalKeywordCrowdTallyServiceProxy()
    {
    }

    @Override
    public KeywordCrowdTally getKeywordCrowdTally(String guid) throws BaseException
    {
        return getKeywordCrowdTallyService().getKeywordCrowdTally(guid);
    }

    @Override
    public Object getKeywordCrowdTally(String guid, String field) throws BaseException
    {
        return getKeywordCrowdTallyService().getKeywordCrowdTally(guid, field);       
    }

    @Override
    public List<KeywordCrowdTally> getKeywordCrowdTallies(List<String> guids) throws BaseException
    {
        return getKeywordCrowdTallyService().getKeywordCrowdTallies(guids);
    }

    @Override
    public List<KeywordCrowdTally> getAllKeywordCrowdTallies() throws BaseException
    {
        return getAllKeywordCrowdTallies(null, null, null);
    }

    @Override
    public List<KeywordCrowdTally> getAllKeywordCrowdTallies(String ordering, Long offset, Integer count) throws BaseException
    {
        return getKeywordCrowdTallyService().getAllKeywordCrowdTallies(ordering, offset, count);
    }

    @Override
    public List<String> getAllKeywordCrowdTallyKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getKeywordCrowdTallyService().getAllKeywordCrowdTallyKeys(ordering, offset, count);
    }

    @Override
    public List<KeywordCrowdTally> findKeywordCrowdTallies(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findKeywordCrowdTallies(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<KeywordCrowdTally> findKeywordCrowdTallies(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getKeywordCrowdTallyService().findKeywordCrowdTallies(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findKeywordCrowdTallyKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getKeywordCrowdTallyService().findKeywordCrowdTallyKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getKeywordCrowdTallyService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createKeywordCrowdTally(String user, String shortLink, String domain, String token, String longUrl, String shortUrl, String tallyDate, String status, String note, Long expirationTime, String keywordFolder, String folderPath, String keyword, String queryKey, String scope, Boolean caseSensitive) throws BaseException
    {
        return getKeywordCrowdTallyService().createKeywordCrowdTally(user, shortLink, domain, token, longUrl, shortUrl, tallyDate, status, note, expirationTime, keywordFolder, folderPath, keyword, queryKey, scope, caseSensitive);
    }

    @Override
    public String createKeywordCrowdTally(KeywordCrowdTally keywordCrowdTally) throws BaseException
    {
        return getKeywordCrowdTallyService().createKeywordCrowdTally(keywordCrowdTally);
    }

    @Override
    public Boolean updateKeywordCrowdTally(String guid, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, String tallyDate, String status, String note, Long expirationTime, String keywordFolder, String folderPath, String keyword, String queryKey, String scope, Boolean caseSensitive) throws BaseException
    {
        return getKeywordCrowdTallyService().updateKeywordCrowdTally(guid, user, shortLink, domain, token, longUrl, shortUrl, tallyDate, status, note, expirationTime, keywordFolder, folderPath, keyword, queryKey, scope, caseSensitive);
    }

    @Override
    public Boolean updateKeywordCrowdTally(KeywordCrowdTally keywordCrowdTally) throws BaseException
    {
        return getKeywordCrowdTallyService().updateKeywordCrowdTally(keywordCrowdTally);
    }

    @Override
    public Boolean deleteKeywordCrowdTally(String guid) throws BaseException
    {
        return getKeywordCrowdTallyService().deleteKeywordCrowdTally(guid);
    }

    @Override
    public Boolean deleteKeywordCrowdTally(KeywordCrowdTally keywordCrowdTally) throws BaseException
    {
        return getKeywordCrowdTallyService().deleteKeywordCrowdTally(keywordCrowdTally);
    }

    @Override
    public Long deleteKeywordCrowdTallies(String filter, String params, List<String> values) throws BaseException
    {
        return getKeywordCrowdTallyService().deleteKeywordCrowdTallies(filter, params, values);
    }

}
