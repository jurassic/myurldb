package com.cannyurl.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.KeywordFolderImport;
import com.myurldb.ws.service.KeywordFolderImportService;
import com.cannyurl.af.proxy.KeywordFolderImportServiceProxy;

public class LocalKeywordFolderImportServiceProxy extends BaseLocalServiceProxy implements KeywordFolderImportServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalKeywordFolderImportServiceProxy.class.getName());

    public LocalKeywordFolderImportServiceProxy()
    {
    }

    @Override
    public KeywordFolderImport getKeywordFolderImport(String guid) throws BaseException
    {
        return getKeywordFolderImportService().getKeywordFolderImport(guid);
    }

    @Override
    public Object getKeywordFolderImport(String guid, String field) throws BaseException
    {
        return getKeywordFolderImportService().getKeywordFolderImport(guid, field);       
    }

    @Override
    public List<KeywordFolderImport> getKeywordFolderImports(List<String> guids) throws BaseException
    {
        return getKeywordFolderImportService().getKeywordFolderImports(guids);
    }

    @Override
    public List<KeywordFolderImport> getAllKeywordFolderImports() throws BaseException
    {
        return getAllKeywordFolderImports(null, null, null);
    }

    @Override
    public List<KeywordFolderImport> getAllKeywordFolderImports(String ordering, Long offset, Integer count) throws BaseException
    {
        return getKeywordFolderImportService().getAllKeywordFolderImports(ordering, offset, count);
    }

    @Override
    public List<String> getAllKeywordFolderImportKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getKeywordFolderImportService().getAllKeywordFolderImportKeys(ordering, offset, count);
    }

    @Override
    public List<KeywordFolderImport> findKeywordFolderImports(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findKeywordFolderImports(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<KeywordFolderImport> findKeywordFolderImports(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getKeywordFolderImportService().findKeywordFolderImports(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findKeywordFolderImportKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getKeywordFolderImportService().findKeywordFolderImportKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getKeywordFolderImportService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createKeywordFolderImport(String user, Integer precedence, String importType, String importedFolder, String importedFolderUser, String importedFolderTitle, String importedFolderPath, String status, String note, String keywordFolder) throws BaseException
    {
        return getKeywordFolderImportService().createKeywordFolderImport(user, precedence, importType, importedFolder, importedFolderUser, importedFolderTitle, importedFolderPath, status, note, keywordFolder);
    }

    @Override
    public String createKeywordFolderImport(KeywordFolderImport keywordFolderImport) throws BaseException
    {
        return getKeywordFolderImportService().createKeywordFolderImport(keywordFolderImport);
    }

    @Override
    public Boolean updateKeywordFolderImport(String guid, String user, Integer precedence, String importType, String importedFolder, String importedFolderUser, String importedFolderTitle, String importedFolderPath, String status, String note, String keywordFolder) throws BaseException
    {
        return getKeywordFolderImportService().updateKeywordFolderImport(guid, user, precedence, importType, importedFolder, importedFolderUser, importedFolderTitle, importedFolderPath, status, note, keywordFolder);
    }

    @Override
    public Boolean updateKeywordFolderImport(KeywordFolderImport keywordFolderImport) throws BaseException
    {
        return getKeywordFolderImportService().updateKeywordFolderImport(keywordFolderImport);
    }

    @Override
    public Boolean deleteKeywordFolderImport(String guid) throws BaseException
    {
        return getKeywordFolderImportService().deleteKeywordFolderImport(guid);
    }

    @Override
    public Boolean deleteKeywordFolderImport(KeywordFolderImport keywordFolderImport) throws BaseException
    {
        return getKeywordFolderImportService().deleteKeywordFolderImport(keywordFolderImport);
    }

    @Override
    public Long deleteKeywordFolderImports(String filter, String params, List<String> values) throws BaseException
    {
        return getKeywordFolderImportService().deleteKeywordFolderImports(filter, params, values);
    }

}
