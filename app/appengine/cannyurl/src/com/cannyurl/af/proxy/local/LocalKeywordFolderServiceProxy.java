package com.cannyurl.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.KeywordFolder;
import com.myurldb.ws.service.KeywordFolderService;
import com.cannyurl.af.proxy.KeywordFolderServiceProxy;

public class LocalKeywordFolderServiceProxy extends BaseLocalServiceProxy implements KeywordFolderServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalKeywordFolderServiceProxy.class.getName());

    public LocalKeywordFolderServiceProxy()
    {
    }

    @Override
    public KeywordFolder getKeywordFolder(String guid) throws BaseException
    {
        return getKeywordFolderService().getKeywordFolder(guid);
    }

    @Override
    public Object getKeywordFolder(String guid, String field) throws BaseException
    {
        return getKeywordFolderService().getKeywordFolder(guid, field);       
    }

    @Override
    public List<KeywordFolder> getKeywordFolders(List<String> guids) throws BaseException
    {
        return getKeywordFolderService().getKeywordFolders(guids);
    }

    @Override
    public List<KeywordFolder> getAllKeywordFolders() throws BaseException
    {
        return getAllKeywordFolders(null, null, null);
    }

    @Override
    public List<KeywordFolder> getAllKeywordFolders(String ordering, Long offset, Integer count) throws BaseException
    {
        return getKeywordFolderService().getAllKeywordFolders(ordering, offset, count);
    }

    @Override
    public List<String> getAllKeywordFolderKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getKeywordFolderService().getAllKeywordFolderKeys(ordering, offset, count);
    }

    @Override
    public List<KeywordFolder> findKeywordFolders(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findKeywordFolders(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<KeywordFolder> findKeywordFolders(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getKeywordFolderService().findKeywordFolders(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findKeywordFolderKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getKeywordFolderService().findKeywordFolderKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getKeywordFolderService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createKeywordFolder(String appClient, String clientRootDomain, String user, String title, String description, String type, String category, String parent, String aggregate, String acl, Boolean exportable, String status, String note, String folderPath) throws BaseException
    {
        return getKeywordFolderService().createKeywordFolder(appClient, clientRootDomain, user, title, description, type, category, parent, aggregate, acl, exportable, status, note, folderPath);
    }

    @Override
    public String createKeywordFolder(KeywordFolder keywordFolder) throws BaseException
    {
        return getKeywordFolderService().createKeywordFolder(keywordFolder);
    }

    @Override
    public Boolean updateKeywordFolder(String guid, String appClient, String clientRootDomain, String user, String title, String description, String type, String category, String parent, String aggregate, String acl, Boolean exportable, String status, String note, String folderPath) throws BaseException
    {
        return getKeywordFolderService().updateKeywordFolder(guid, appClient, clientRootDomain, user, title, description, type, category, parent, aggregate, acl, exportable, status, note, folderPath);
    }

    @Override
    public Boolean updateKeywordFolder(KeywordFolder keywordFolder) throws BaseException
    {
        return getKeywordFolderService().updateKeywordFolder(keywordFolder);
    }

    @Override
    public Boolean deleteKeywordFolder(String guid) throws BaseException
    {
        return getKeywordFolderService().deleteKeywordFolder(guid);
    }

    @Override
    public Boolean deleteKeywordFolder(KeywordFolder keywordFolder) throws BaseException
    {
        return getKeywordFolderService().deleteKeywordFolder(keywordFolder);
    }

    @Override
    public Long deleteKeywordFolders(String filter, String params, List<String> values) throws BaseException
    {
        return getKeywordFolderService().deleteKeywordFolders(filter, params, values);
    }

}
