package com.cannyurl.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.KeywordLink;
import com.myurldb.ws.service.KeywordLinkService;
import com.cannyurl.af.proxy.KeywordLinkServiceProxy;

public class LocalKeywordLinkServiceProxy extends BaseLocalServiceProxy implements KeywordLinkServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalKeywordLinkServiceProxy.class.getName());

    public LocalKeywordLinkServiceProxy()
    {
    }

    @Override
    public KeywordLink getKeywordLink(String guid) throws BaseException
    {
        return getKeywordLinkService().getKeywordLink(guid);
    }

    @Override
    public Object getKeywordLink(String guid, String field) throws BaseException
    {
        return getKeywordLinkService().getKeywordLink(guid, field);       
    }

    @Override
    public List<KeywordLink> getKeywordLinks(List<String> guids) throws BaseException
    {
        return getKeywordLinkService().getKeywordLinks(guids);
    }

    @Override
    public List<KeywordLink> getAllKeywordLinks() throws BaseException
    {
        return getAllKeywordLinks(null, null, null);
    }

    @Override
    public List<KeywordLink> getAllKeywordLinks(String ordering, Long offset, Integer count) throws BaseException
    {
        return getKeywordLinkService().getAllKeywordLinks(ordering, offset, count);
    }

    @Override
    public List<String> getAllKeywordLinkKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getKeywordLinkService().getAllKeywordLinkKeys(ordering, offset, count);
    }

    @Override
    public List<KeywordLink> findKeywordLinks(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findKeywordLinks(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<KeywordLink> findKeywordLinks(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getKeywordLinkService().findKeywordLinks(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findKeywordLinkKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getKeywordLinkService().findKeywordLinkKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getKeywordLinkService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createKeywordLink(String appClient, String clientRootDomain, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, Boolean internal, Boolean caching, String memo, String status, String note, Long expirationTime, String keywordFolder, String folderPath, String keyword, String queryKey, String scope, Boolean dynamic, Boolean caseSensitive) throws BaseException
    {
        return getKeywordLinkService().createKeywordLink(appClient, clientRootDomain, user, shortLink, domain, token, longUrl, shortUrl, internal, caching, memo, status, note, expirationTime, keywordFolder, folderPath, keyword, queryKey, scope, dynamic, caseSensitive);
    }

    @Override
    public String createKeywordLink(KeywordLink keywordLink) throws BaseException
    {
        return getKeywordLinkService().createKeywordLink(keywordLink);
    }

    @Override
    public Boolean updateKeywordLink(String guid, String appClient, String clientRootDomain, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, Boolean internal, Boolean caching, String memo, String status, String note, Long expirationTime, String keywordFolder, String folderPath, String keyword, String queryKey, String scope, Boolean dynamic, Boolean caseSensitive) throws BaseException
    {
        return getKeywordLinkService().updateKeywordLink(guid, appClient, clientRootDomain, user, shortLink, domain, token, longUrl, shortUrl, internal, caching, memo, status, note, expirationTime, keywordFolder, folderPath, keyword, queryKey, scope, dynamic, caseSensitive);
    }

    @Override
    public Boolean updateKeywordLink(KeywordLink keywordLink) throws BaseException
    {
        return getKeywordLinkService().updateKeywordLink(keywordLink);
    }

    @Override
    public Boolean deleteKeywordLink(String guid) throws BaseException
    {
        return getKeywordLinkService().deleteKeywordLink(guid);
    }

    @Override
    public Boolean deleteKeywordLink(KeywordLink keywordLink) throws BaseException
    {
        return getKeywordLinkService().deleteKeywordLink(keywordLink);
    }

    @Override
    public Long deleteKeywordLinks(String filter, String params, List<String> values) throws BaseException
    {
        return getKeywordLinkService().deleteKeywordLinks(filter, params, values);
    }

}
