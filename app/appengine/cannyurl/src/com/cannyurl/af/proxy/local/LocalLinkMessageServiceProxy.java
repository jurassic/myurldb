package com.cannyurl.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.LinkMessage;
import com.myurldb.ws.service.LinkMessageService;
import com.cannyurl.af.proxy.LinkMessageServiceProxy;

public class LocalLinkMessageServiceProxy extends BaseLocalServiceProxy implements LinkMessageServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalLinkMessageServiceProxy.class.getName());

    public LocalLinkMessageServiceProxy()
    {
    }

    @Override
    public LinkMessage getLinkMessage(String guid) throws BaseException
    {
        return getLinkMessageService().getLinkMessage(guid);
    }

    @Override
    public Object getLinkMessage(String guid, String field) throws BaseException
    {
        return getLinkMessageService().getLinkMessage(guid, field);       
    }

    @Override
    public List<LinkMessage> getLinkMessages(List<String> guids) throws BaseException
    {
        return getLinkMessageService().getLinkMessages(guids);
    }

    @Override
    public List<LinkMessage> getAllLinkMessages() throws BaseException
    {
        return getAllLinkMessages(null, null, null);
    }

    @Override
    public List<LinkMessage> getAllLinkMessages(String ordering, Long offset, Integer count) throws BaseException
    {
        return getLinkMessageService().getAllLinkMessages(ordering, offset, count);
    }

    @Override
    public List<String> getAllLinkMessageKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getLinkMessageService().getAllLinkMessageKeys(ordering, offset, count);
    }

    @Override
    public List<LinkMessage> findLinkMessages(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findLinkMessages(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<LinkMessage> findLinkMessages(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getLinkMessageService().findLinkMessages(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findLinkMessageKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getLinkMessageService().findLinkMessageKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getLinkMessageService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createLinkMessage(String shortLink, String message, String password) throws BaseException
    {
        return getLinkMessageService().createLinkMessage(shortLink, message, password);
    }

    @Override
    public String createLinkMessage(LinkMessage linkMessage) throws BaseException
    {
        return getLinkMessageService().createLinkMessage(linkMessage);
    }

    @Override
    public Boolean updateLinkMessage(String guid, String shortLink, String message, String password) throws BaseException
    {
        return getLinkMessageService().updateLinkMessage(guid, shortLink, message, password);
    }

    @Override
    public Boolean updateLinkMessage(LinkMessage linkMessage) throws BaseException
    {
        return getLinkMessageService().updateLinkMessage(linkMessage);
    }

    @Override
    public Boolean deleteLinkMessage(String guid) throws BaseException
    {
        return getLinkMessageService().deleteLinkMessage(guid);
    }

    @Override
    public Boolean deleteLinkMessage(LinkMessage linkMessage) throws BaseException
    {
        return getLinkMessageService().deleteLinkMessage(linkMessage);
    }

    @Override
    public Long deleteLinkMessages(String filter, String params, List<String> values) throws BaseException
    {
        return getLinkMessageService().deleteLinkMessages(filter, params, values);
    }

}
