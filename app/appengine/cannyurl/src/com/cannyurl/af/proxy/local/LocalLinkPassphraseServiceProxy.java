package com.cannyurl.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.LinkPassphrase;
import com.myurldb.ws.service.LinkPassphraseService;
import com.cannyurl.af.proxy.LinkPassphraseServiceProxy;

public class LocalLinkPassphraseServiceProxy extends BaseLocalServiceProxy implements LinkPassphraseServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalLinkPassphraseServiceProxy.class.getName());

    public LocalLinkPassphraseServiceProxy()
    {
    }

    @Override
    public LinkPassphrase getLinkPassphrase(String guid) throws BaseException
    {
        return getLinkPassphraseService().getLinkPassphrase(guid);
    }

    @Override
    public Object getLinkPassphrase(String guid, String field) throws BaseException
    {
        return getLinkPassphraseService().getLinkPassphrase(guid, field);       
    }

    @Override
    public List<LinkPassphrase> getLinkPassphrases(List<String> guids) throws BaseException
    {
        return getLinkPassphraseService().getLinkPassphrases(guids);
    }

    @Override
    public List<LinkPassphrase> getAllLinkPassphrases() throws BaseException
    {
        return getAllLinkPassphrases(null, null, null);
    }

    @Override
    public List<LinkPassphrase> getAllLinkPassphrases(String ordering, Long offset, Integer count) throws BaseException
    {
        return getLinkPassphraseService().getAllLinkPassphrases(ordering, offset, count);
    }

    @Override
    public List<String> getAllLinkPassphraseKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getLinkPassphraseService().getAllLinkPassphraseKeys(ordering, offset, count);
    }

    @Override
    public List<LinkPassphrase> findLinkPassphrases(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findLinkPassphrases(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<LinkPassphrase> findLinkPassphrases(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getLinkPassphraseService().findLinkPassphrases(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findLinkPassphraseKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getLinkPassphraseService().findLinkPassphraseKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getLinkPassphraseService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createLinkPassphrase(String shortLink, String passphrase) throws BaseException
    {
        return getLinkPassphraseService().createLinkPassphrase(shortLink, passphrase);
    }

    @Override
    public String createLinkPassphrase(LinkPassphrase linkPassphrase) throws BaseException
    {
        return getLinkPassphraseService().createLinkPassphrase(linkPassphrase);
    }

    @Override
    public Boolean updateLinkPassphrase(String guid, String shortLink, String passphrase) throws BaseException
    {
        return getLinkPassphraseService().updateLinkPassphrase(guid, shortLink, passphrase);
    }

    @Override
    public Boolean updateLinkPassphrase(LinkPassphrase linkPassphrase) throws BaseException
    {
        return getLinkPassphraseService().updateLinkPassphrase(linkPassphrase);
    }

    @Override
    public Boolean deleteLinkPassphrase(String guid) throws BaseException
    {
        return getLinkPassphraseService().deleteLinkPassphrase(guid);
    }

    @Override
    public Boolean deleteLinkPassphrase(LinkPassphrase linkPassphrase) throws BaseException
    {
        return getLinkPassphraseService().deleteLinkPassphrase(linkPassphrase);
    }

    @Override
    public Long deleteLinkPassphrases(String filter, String params, List<String> values) throws BaseException
    {
        return getLinkPassphraseService().deleteLinkPassphrases(filter, params, values);
    }

}
