package com.cannyurl.af.proxy.local;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.ApiConsumerServiceProxy;
import com.cannyurl.af.proxy.AppCustomDomainServiceProxy;
import com.cannyurl.af.proxy.SiteCustomDomainServiceProxy;
import com.cannyurl.af.proxy.UserServiceProxy;
import com.cannyurl.af.proxy.UserUsercodeServiceProxy;
import com.cannyurl.af.proxy.UserPasswordServiceProxy;
import com.cannyurl.af.proxy.ExternalUserAuthServiceProxy;
import com.cannyurl.af.proxy.UserAuthStateServiceProxy;
import com.cannyurl.af.proxy.UserResourcePermissionServiceProxy;
import com.cannyurl.af.proxy.UserResourceProhibitionServiceProxy;
import com.cannyurl.af.proxy.RolePermissionServiceProxy;
import com.cannyurl.af.proxy.UserRoleServiceProxy;
import com.cannyurl.af.proxy.AppClientServiceProxy;
import com.cannyurl.af.proxy.ClientUserServiceProxy;
import com.cannyurl.af.proxy.UserCustomDomainServiceProxy;
import com.cannyurl.af.proxy.ClientSettingServiceProxy;
import com.cannyurl.af.proxy.UserSettingServiceProxy;
import com.cannyurl.af.proxy.VisitorSettingServiceProxy;
import com.cannyurl.af.proxy.TwitterSummaryCardServiceProxy;
import com.cannyurl.af.proxy.TwitterPhotoCardServiceProxy;
import com.cannyurl.af.proxy.TwitterGalleryCardServiceProxy;
import com.cannyurl.af.proxy.TwitterAppCardServiceProxy;
import com.cannyurl.af.proxy.TwitterPlayerCardServiceProxy;
import com.cannyurl.af.proxy.TwitterProductCardServiceProxy;
import com.cannyurl.af.proxy.ShortPassageServiceProxy;
import com.cannyurl.af.proxy.ShortLinkServiceProxy;
import com.cannyurl.af.proxy.GeoLinkServiceProxy;
import com.cannyurl.af.proxy.QrCodeServiceProxy;
import com.cannyurl.af.proxy.LinkPassphraseServiceProxy;
import com.cannyurl.af.proxy.LinkMessageServiceProxy;
import com.cannyurl.af.proxy.LinkAlbumServiceProxy;
import com.cannyurl.af.proxy.AlbumShortLinkServiceProxy;
import com.cannyurl.af.proxy.KeywordFolderServiceProxy;
import com.cannyurl.af.proxy.BookmarkFolderServiceProxy;
import com.cannyurl.af.proxy.KeywordLinkServiceProxy;
import com.cannyurl.af.proxy.BookmarkLinkServiceProxy;
import com.cannyurl.af.proxy.SpeedDialServiceProxy;
import com.cannyurl.af.proxy.KeywordFolderImportServiceProxy;
import com.cannyurl.af.proxy.BookmarkFolderImportServiceProxy;
import com.cannyurl.af.proxy.KeywordCrowdTallyServiceProxy;
import com.cannyurl.af.proxy.BookmarkCrowdTallyServiceProxy;
import com.cannyurl.af.proxy.DomainInfoServiceProxy;
import com.cannyurl.af.proxy.UrlRatingServiceProxy;
import com.cannyurl.af.proxy.UserRatingServiceProxy;
import com.cannyurl.af.proxy.AbuseTagServiceProxy;
import com.cannyurl.af.proxy.ServiceInfoServiceProxy;
import com.cannyurl.af.proxy.FiveTenServiceProxy;

public class LocalProxyFactory extends AbstractProxyFactory
{
    private static final Logger log = Logger.getLogger(LocalProxyFactory.class.getName());

    private LocalProxyFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class LocalProxyFactoryHolder
    {
        private static final LocalProxyFactory INSTANCE = new LocalProxyFactory();
    }

    // Singleton method
    public static LocalProxyFactory getInstance()
    {
        return LocalProxyFactoryHolder.INSTANCE;
    }

    @Override
    public ApiConsumerServiceProxy getApiConsumerServiceProxy()
    {
        return new LocalApiConsumerServiceProxy();
    }

    @Override
    public AppCustomDomainServiceProxy getAppCustomDomainServiceProxy()
    {
        return new LocalAppCustomDomainServiceProxy();
    }

    @Override
    public SiteCustomDomainServiceProxy getSiteCustomDomainServiceProxy()
    {
        return new LocalSiteCustomDomainServiceProxy();
    }

    @Override
    public UserServiceProxy getUserServiceProxy()
    {
        return new LocalUserServiceProxy();
    }

    @Override
    public UserUsercodeServiceProxy getUserUsercodeServiceProxy()
    {
        return new LocalUserUsercodeServiceProxy();
    }

    @Override
    public UserPasswordServiceProxy getUserPasswordServiceProxy()
    {
        return new LocalUserPasswordServiceProxy();
    }

    @Override
    public ExternalUserAuthServiceProxy getExternalUserAuthServiceProxy()
    {
        return new LocalExternalUserAuthServiceProxy();
    }

    @Override
    public UserAuthStateServiceProxy getUserAuthStateServiceProxy()
    {
        return new LocalUserAuthStateServiceProxy();
    }

    @Override
    public UserResourcePermissionServiceProxy getUserResourcePermissionServiceProxy()
    {
        return new LocalUserResourcePermissionServiceProxy();
    }

    @Override
    public UserResourceProhibitionServiceProxy getUserResourceProhibitionServiceProxy()
    {
        return new LocalUserResourceProhibitionServiceProxy();
    }

    @Override
    public RolePermissionServiceProxy getRolePermissionServiceProxy()
    {
        return new LocalRolePermissionServiceProxy();
    }

    @Override
    public UserRoleServiceProxy getUserRoleServiceProxy()
    {
        return new LocalUserRoleServiceProxy();
    }

    @Override
    public AppClientServiceProxy getAppClientServiceProxy()
    {
        return new LocalAppClientServiceProxy();
    }

    @Override
    public ClientUserServiceProxy getClientUserServiceProxy()
    {
        return new LocalClientUserServiceProxy();
    }

    @Override
    public UserCustomDomainServiceProxy getUserCustomDomainServiceProxy()
    {
        return new LocalUserCustomDomainServiceProxy();
    }

    @Override
    public ClientSettingServiceProxy getClientSettingServiceProxy()
    {
        return new LocalClientSettingServiceProxy();
    }

    @Override
    public UserSettingServiceProxy getUserSettingServiceProxy()
    {
        return new LocalUserSettingServiceProxy();
    }

    @Override
    public VisitorSettingServiceProxy getVisitorSettingServiceProxy()
    {
        return new LocalVisitorSettingServiceProxy();
    }

    @Override
    public TwitterSummaryCardServiceProxy getTwitterSummaryCardServiceProxy()
    {
        return new LocalTwitterSummaryCardServiceProxy();
    }

    @Override
    public TwitterPhotoCardServiceProxy getTwitterPhotoCardServiceProxy()
    {
        return new LocalTwitterPhotoCardServiceProxy();
    }

    @Override
    public TwitterGalleryCardServiceProxy getTwitterGalleryCardServiceProxy()
    {
        return new LocalTwitterGalleryCardServiceProxy();
    }

    @Override
    public TwitterAppCardServiceProxy getTwitterAppCardServiceProxy()
    {
        return new LocalTwitterAppCardServiceProxy();
    }

    @Override
    public TwitterPlayerCardServiceProxy getTwitterPlayerCardServiceProxy()
    {
        return new LocalTwitterPlayerCardServiceProxy();
    }

    @Override
    public TwitterProductCardServiceProxy getTwitterProductCardServiceProxy()
    {
        return new LocalTwitterProductCardServiceProxy();
    }

    @Override
    public ShortPassageServiceProxy getShortPassageServiceProxy()
    {
        return new LocalShortPassageServiceProxy();
    }

    @Override
    public ShortLinkServiceProxy getShortLinkServiceProxy()
    {
        return new LocalShortLinkServiceProxy();
    }

    @Override
    public GeoLinkServiceProxy getGeoLinkServiceProxy()
    {
        return new LocalGeoLinkServiceProxy();
    }

    @Override
    public QrCodeServiceProxy getQrCodeServiceProxy()
    {
        return new LocalQrCodeServiceProxy();
    }

    @Override
    public LinkPassphraseServiceProxy getLinkPassphraseServiceProxy()
    {
        return new LocalLinkPassphraseServiceProxy();
    }

    @Override
    public LinkMessageServiceProxy getLinkMessageServiceProxy()
    {
        return new LocalLinkMessageServiceProxy();
    }

    @Override
    public LinkAlbumServiceProxy getLinkAlbumServiceProxy()
    {
        return new LocalLinkAlbumServiceProxy();
    }

    @Override
    public AlbumShortLinkServiceProxy getAlbumShortLinkServiceProxy()
    {
        return new LocalAlbumShortLinkServiceProxy();
    }

    @Override
    public KeywordFolderServiceProxy getKeywordFolderServiceProxy()
    {
        return new LocalKeywordFolderServiceProxy();
    }

    @Override
    public BookmarkFolderServiceProxy getBookmarkFolderServiceProxy()
    {
        return new LocalBookmarkFolderServiceProxy();
    }

    @Override
    public KeywordLinkServiceProxy getKeywordLinkServiceProxy()
    {
        return new LocalKeywordLinkServiceProxy();
    }

    @Override
    public BookmarkLinkServiceProxy getBookmarkLinkServiceProxy()
    {
        return new LocalBookmarkLinkServiceProxy();
    }

    @Override
    public SpeedDialServiceProxy getSpeedDialServiceProxy()
    {
        return new LocalSpeedDialServiceProxy();
    }

    @Override
    public KeywordFolderImportServiceProxy getKeywordFolderImportServiceProxy()
    {
        return new LocalKeywordFolderImportServiceProxy();
    }

    @Override
    public BookmarkFolderImportServiceProxy getBookmarkFolderImportServiceProxy()
    {
        return new LocalBookmarkFolderImportServiceProxy();
    }

    @Override
    public KeywordCrowdTallyServiceProxy getKeywordCrowdTallyServiceProxy()
    {
        return new LocalKeywordCrowdTallyServiceProxy();
    }

    @Override
    public BookmarkCrowdTallyServiceProxy getBookmarkCrowdTallyServiceProxy()
    {
        return new LocalBookmarkCrowdTallyServiceProxy();
    }

    @Override
    public DomainInfoServiceProxy getDomainInfoServiceProxy()
    {
        return new LocalDomainInfoServiceProxy();
    }

    @Override
    public UrlRatingServiceProxy getUrlRatingServiceProxy()
    {
        return new LocalUrlRatingServiceProxy();
    }

    @Override
    public UserRatingServiceProxy getUserRatingServiceProxy()
    {
        return new LocalUserRatingServiceProxy();
    }

    @Override
    public AbuseTagServiceProxy getAbuseTagServiceProxy()
    {
        return new LocalAbuseTagServiceProxy();
    }

    @Override
    public ServiceInfoServiceProxy getServiceInfoServiceProxy()
    {
        return new LocalServiceInfoServiceProxy();
    }

    @Override
    public FiveTenServiceProxy getFiveTenServiceProxy()
    {
        return new LocalFiveTenServiceProxy();
    }

}
