package com.cannyurl.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.QrCode;
import com.myurldb.ws.service.QrCodeService;
import com.cannyurl.af.proxy.QrCodeServiceProxy;

public class LocalQrCodeServiceProxy extends BaseLocalServiceProxy implements QrCodeServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalQrCodeServiceProxy.class.getName());

    public LocalQrCodeServiceProxy()
    {
    }

    @Override
    public QrCode getQrCode(String guid) throws BaseException
    {
        return getQrCodeService().getQrCode(guid);
    }

    @Override
    public Object getQrCode(String guid, String field) throws BaseException
    {
        return getQrCodeService().getQrCode(guid, field);       
    }

    @Override
    public List<QrCode> getQrCodes(List<String> guids) throws BaseException
    {
        return getQrCodeService().getQrCodes(guids);
    }

    @Override
    public List<QrCode> getAllQrCodes() throws BaseException
    {
        return getAllQrCodes(null, null, null);
    }

    @Override
    public List<QrCode> getAllQrCodes(String ordering, Long offset, Integer count) throws BaseException
    {
        return getQrCodeService().getAllQrCodes(ordering, offset, count);
    }

    @Override
    public List<String> getAllQrCodeKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getQrCodeService().getAllQrCodeKeys(ordering, offset, count);
    }

    @Override
    public List<QrCode> findQrCodes(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findQrCodes(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<QrCode> findQrCodes(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getQrCodeService().findQrCodes(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findQrCodeKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getQrCodeService().findQrCodeKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getQrCodeService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createQrCode(String shortLink, String imageLink, String imageUrl, String type, String status) throws BaseException
    {
        return getQrCodeService().createQrCode(shortLink, imageLink, imageUrl, type, status);
    }

    @Override
    public String createQrCode(QrCode qrCode) throws BaseException
    {
        return getQrCodeService().createQrCode(qrCode);
    }

    @Override
    public Boolean updateQrCode(String guid, String shortLink, String imageLink, String imageUrl, String type, String status) throws BaseException
    {
        return getQrCodeService().updateQrCode(guid, shortLink, imageLink, imageUrl, type, status);
    }

    @Override
    public Boolean updateQrCode(QrCode qrCode) throws BaseException
    {
        return getQrCodeService().updateQrCode(qrCode);
    }

    @Override
    public Boolean deleteQrCode(String guid) throws BaseException
    {
        return getQrCodeService().deleteQrCode(guid);
    }

    @Override
    public Boolean deleteQrCode(QrCode qrCode) throws BaseException
    {
        return getQrCodeService().deleteQrCode(qrCode);
    }

    @Override
    public Long deleteQrCodes(String filter, String params, List<String> values) throws BaseException
    {
        return getQrCodeService().deleteQrCodes(filter, params, values);
    }

}
