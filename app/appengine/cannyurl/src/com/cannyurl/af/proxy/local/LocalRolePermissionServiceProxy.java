package com.cannyurl.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.RolePermission;
import com.myurldb.ws.service.RolePermissionService;
import com.cannyurl.af.proxy.RolePermissionServiceProxy;

public class LocalRolePermissionServiceProxy extends BaseLocalServiceProxy implements RolePermissionServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalRolePermissionServiceProxy.class.getName());

    public LocalRolePermissionServiceProxy()
    {
    }

    @Override
    public RolePermission getRolePermission(String guid) throws BaseException
    {
        return getRolePermissionService().getRolePermission(guid);
    }

    @Override
    public Object getRolePermission(String guid, String field) throws BaseException
    {
        return getRolePermissionService().getRolePermission(guid, field);       
    }

    @Override
    public List<RolePermission> getRolePermissions(List<String> guids) throws BaseException
    {
        return getRolePermissionService().getRolePermissions(guids);
    }

    @Override
    public List<RolePermission> getAllRolePermissions() throws BaseException
    {
        return getAllRolePermissions(null, null, null);
    }

    @Override
    public List<RolePermission> getAllRolePermissions(String ordering, Long offset, Integer count) throws BaseException
    {
        return getRolePermissionService().getAllRolePermissions(ordering, offset, count);
    }

    @Override
    public List<String> getAllRolePermissionKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getRolePermissionService().getAllRolePermissionKeys(ordering, offset, count);
    }

    @Override
    public List<RolePermission> findRolePermissions(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findRolePermissions(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<RolePermission> findRolePermissions(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getRolePermissionService().findRolePermissions(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findRolePermissionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getRolePermissionService().findRolePermissionKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getRolePermissionService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createRolePermission(String role, String permissionName, String resource, String instance, String action, String status) throws BaseException
    {
        return getRolePermissionService().createRolePermission(role, permissionName, resource, instance, action, status);
    }

    @Override
    public String createRolePermission(RolePermission rolePermission) throws BaseException
    {
        return getRolePermissionService().createRolePermission(rolePermission);
    }

    @Override
    public Boolean updateRolePermission(String guid, String role, String permissionName, String resource, String instance, String action, String status) throws BaseException
    {
        return getRolePermissionService().updateRolePermission(guid, role, permissionName, resource, instance, action, status);
    }

    @Override
    public Boolean updateRolePermission(RolePermission rolePermission) throws BaseException
    {
        return getRolePermissionService().updateRolePermission(rolePermission);
    }

    @Override
    public Boolean deleteRolePermission(String guid) throws BaseException
    {
        return getRolePermissionService().deleteRolePermission(guid);
    }

    @Override
    public Boolean deleteRolePermission(RolePermission rolePermission) throws BaseException
    {
        return getRolePermissionService().deleteRolePermission(rolePermission);
    }

    @Override
    public Long deleteRolePermissions(String filter, String params, List<String> values) throws BaseException
    {
        return getRolePermissionService().deleteRolePermissions(filter, params, values);
    }

}
