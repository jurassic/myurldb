package com.cannyurl.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.ReferrerInfoStruct;
import com.myurldb.ws.ShortLink;
import com.myurldb.ws.service.ShortLinkService;
import com.cannyurl.af.proxy.ShortLinkServiceProxy;

public class LocalShortLinkServiceProxy extends BaseLocalServiceProxy implements ShortLinkServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalShortLinkServiceProxy.class.getName());

    public LocalShortLinkServiceProxy()
    {
    }

    @Override
    public ShortLink getShortLink(String guid) throws BaseException
    {
        return getShortLinkService().getShortLink(guid);
    }

    @Override
    public Object getShortLink(String guid, String field) throws BaseException
    {
        return getShortLinkService().getShortLink(guid, field);       
    }

    @Override
    public List<ShortLink> getShortLinks(List<String> guids) throws BaseException
    {
        return getShortLinkService().getShortLinks(guids);
    }

    @Override
    public List<ShortLink> getAllShortLinks() throws BaseException
    {
        return getAllShortLinks(null, null, null);
    }

    @Override
    public List<ShortLink> getAllShortLinks(String ordering, Long offset, Integer count) throws BaseException
    {
        return getShortLinkService().getAllShortLinks(ordering, offset, count);
    }

    @Override
    public List<String> getAllShortLinkKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getShortLinkService().getAllShortLinkKeys(ordering, offset, count);
    }

    @Override
    public List<ShortLink> findShortLinks(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findShortLinks(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<ShortLink> findShortLinks(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getShortLinkService().findShortLinks(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findShortLinkKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getShortLinkService().findShortLinkKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getShortLinkService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createShortLink(String appClient, String clientRootDomain, String owner, String longUrlDomain, String longUrl, String longUrlFull, String longUrlHash, Boolean reuseExistingShortUrl, Boolean failIfShortUrlExists, String domain, String domainType, String usercode, String username, String tokenPrefix, String token, String tokenType, String sassyTokenType, String shortUrl, String shortPassage, String redirectType, Long flashDuration, String accessType, String viewType, String shareType, Boolean readOnly, String displayMessage, String shortMessage, Boolean keywordEnabled, Boolean bookmarkEnabled, ReferrerInfoStruct referrerInfo, String status, String note, Long expirationTime) throws BaseException
    {
        return getShortLinkService().createShortLink(appClient, clientRootDomain, owner, longUrlDomain, longUrl, longUrlFull, longUrlHash, reuseExistingShortUrl, failIfShortUrlExists, domain, domainType, usercode, username, tokenPrefix, token, tokenType, sassyTokenType, shortUrl, shortPassage, redirectType, flashDuration, accessType, viewType, shareType, readOnly, displayMessage, shortMessage, keywordEnabled, bookmarkEnabled, referrerInfo, status, note, expirationTime);
    }

    @Override
    public String createShortLink(ShortLink shortLink) throws BaseException
    {
        return getShortLinkService().createShortLink(shortLink);
    }

    @Override
    public Boolean updateShortLink(String guid, String appClient, String clientRootDomain, String owner, String longUrlDomain, String longUrl, String longUrlFull, String longUrlHash, Boolean reuseExistingShortUrl, Boolean failIfShortUrlExists, String domain, String domainType, String usercode, String username, String tokenPrefix, String token, String tokenType, String sassyTokenType, String shortUrl, String shortPassage, String redirectType, Long flashDuration, String accessType, String viewType, String shareType, Boolean readOnly, String displayMessage, String shortMessage, Boolean keywordEnabled, Boolean bookmarkEnabled, ReferrerInfoStruct referrerInfo, String status, String note, Long expirationTime) throws BaseException
    {
        return getShortLinkService().updateShortLink(guid, appClient, clientRootDomain, owner, longUrlDomain, longUrl, longUrlFull, longUrlHash, reuseExistingShortUrl, failIfShortUrlExists, domain, domainType, usercode, username, tokenPrefix, token, tokenType, sassyTokenType, shortUrl, shortPassage, redirectType, flashDuration, accessType, viewType, shareType, readOnly, displayMessage, shortMessage, keywordEnabled, bookmarkEnabled, referrerInfo, status, note, expirationTime);
    }

    @Override
    public Boolean updateShortLink(ShortLink shortLink) throws BaseException
    {
        return getShortLinkService().updateShortLink(shortLink);
    }

    @Override
    public Boolean deleteShortLink(String guid) throws BaseException
    {
        return getShortLinkService().deleteShortLink(guid);
    }

    @Override
    public Boolean deleteShortLink(ShortLink shortLink) throws BaseException
    {
        return getShortLinkService().deleteShortLink(shortLink);
    }

    @Override
    public Long deleteShortLinks(String filter, String params, List<String> values) throws BaseException
    {
        return getShortLinkService().deleteShortLinks(filter, params, values);
    }

}
