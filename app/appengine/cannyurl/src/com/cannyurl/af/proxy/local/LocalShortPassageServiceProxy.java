package com.cannyurl.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.ShortPassageAttribute;
import com.myurldb.ws.ShortPassage;
import com.myurldb.ws.service.ShortPassageService;
import com.cannyurl.af.proxy.ShortPassageServiceProxy;

public class LocalShortPassageServiceProxy extends BaseLocalServiceProxy implements ShortPassageServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalShortPassageServiceProxy.class.getName());

    public LocalShortPassageServiceProxy()
    {
    }

    @Override
    public ShortPassage getShortPassage(String guid) throws BaseException
    {
        return getShortPassageService().getShortPassage(guid);
    }

    @Override
    public Object getShortPassage(String guid, String field) throws BaseException
    {
        return getShortPassageService().getShortPassage(guid, field);       
    }

    @Override
    public List<ShortPassage> getShortPassages(List<String> guids) throws BaseException
    {
        return getShortPassageService().getShortPassages(guids);
    }

    @Override
    public List<ShortPassage> getAllShortPassages() throws BaseException
    {
        return getAllShortPassages(null, null, null);
    }

    @Override
    public List<ShortPassage> getAllShortPassages(String ordering, Long offset, Integer count) throws BaseException
    {
        return getShortPassageService().getAllShortPassages(ordering, offset, count);
    }

    @Override
    public List<String> getAllShortPassageKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getShortPassageService().getAllShortPassageKeys(ordering, offset, count);
    }

    @Override
    public List<ShortPassage> findShortPassages(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findShortPassages(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<ShortPassage> findShortPassages(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getShortPassageService().findShortPassages(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findShortPassageKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getShortPassageService().findShortPassageKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getShortPassageService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createShortPassage(String owner, String longText, String shortText, ShortPassageAttribute attribute, Boolean readOnly, String status, String note, Long expirationTime) throws BaseException
    {
        return getShortPassageService().createShortPassage(owner, longText, shortText, attribute, readOnly, status, note, expirationTime);
    }

    @Override
    public String createShortPassage(ShortPassage shortPassage) throws BaseException
    {
        return getShortPassageService().createShortPassage(shortPassage);
    }

    @Override
    public Boolean updateShortPassage(String guid, String owner, String longText, String shortText, ShortPassageAttribute attribute, Boolean readOnly, String status, String note, Long expirationTime) throws BaseException
    {
        return getShortPassageService().updateShortPassage(guid, owner, longText, shortText, attribute, readOnly, status, note, expirationTime);
    }

    @Override
    public Boolean updateShortPassage(ShortPassage shortPassage) throws BaseException
    {
        return getShortPassageService().updateShortPassage(shortPassage);
    }

    @Override
    public Boolean deleteShortPassage(String guid) throws BaseException
    {
        return getShortPassageService().deleteShortPassage(guid);
    }

    @Override
    public Boolean deleteShortPassage(ShortPassage shortPassage) throws BaseException
    {
        return getShortPassageService().deleteShortPassage(shortPassage);
    }

    @Override
    public Long deleteShortPassages(String filter, String params, List<String> values) throws BaseException
    {
        return getShortPassageService().deleteShortPassages(filter, params, values);
    }

}
