package com.cannyurl.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.SiteCustomDomain;
import com.myurldb.ws.service.SiteCustomDomainService;
import com.cannyurl.af.proxy.SiteCustomDomainServiceProxy;

public class LocalSiteCustomDomainServiceProxy extends BaseLocalServiceProxy implements SiteCustomDomainServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalSiteCustomDomainServiceProxy.class.getName());

    public LocalSiteCustomDomainServiceProxy()
    {
    }

    @Override
    public SiteCustomDomain getSiteCustomDomain(String guid) throws BaseException
    {
        return getSiteCustomDomainService().getSiteCustomDomain(guid);
    }

    @Override
    public Object getSiteCustomDomain(String guid, String field) throws BaseException
    {
        return getSiteCustomDomainService().getSiteCustomDomain(guid, field);       
    }

    @Override
    public List<SiteCustomDomain> getSiteCustomDomains(List<String> guids) throws BaseException
    {
        return getSiteCustomDomainService().getSiteCustomDomains(guids);
    }

    @Override
    public List<SiteCustomDomain> getAllSiteCustomDomains() throws BaseException
    {
        return getAllSiteCustomDomains(null, null, null);
    }

    @Override
    public List<SiteCustomDomain> getAllSiteCustomDomains(String ordering, Long offset, Integer count) throws BaseException
    {
        return getSiteCustomDomainService().getAllSiteCustomDomains(ordering, offset, count);
    }

    @Override
    public List<String> getAllSiteCustomDomainKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getSiteCustomDomainService().getAllSiteCustomDomainKeys(ordering, offset, count);
    }

    @Override
    public List<SiteCustomDomain> findSiteCustomDomains(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findSiteCustomDomains(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<SiteCustomDomain> findSiteCustomDomains(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getSiteCustomDomainService().findSiteCustomDomains(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findSiteCustomDomainKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getSiteCustomDomainService().findSiteCustomDomainKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getSiteCustomDomainService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createSiteCustomDomain(String siteDomain, String domain, String status) throws BaseException
    {
        return getSiteCustomDomainService().createSiteCustomDomain(siteDomain, domain, status);
    }

    @Override
    public String createSiteCustomDomain(SiteCustomDomain siteCustomDomain) throws BaseException
    {
        return getSiteCustomDomainService().createSiteCustomDomain(siteCustomDomain);
    }

    @Override
    public Boolean updateSiteCustomDomain(String guid, String siteDomain, String domain, String status) throws BaseException
    {
        return getSiteCustomDomainService().updateSiteCustomDomain(guid, siteDomain, domain, status);
    }

    @Override
    public Boolean updateSiteCustomDomain(SiteCustomDomain siteCustomDomain) throws BaseException
    {
        return getSiteCustomDomainService().updateSiteCustomDomain(siteCustomDomain);
    }

    @Override
    public Boolean deleteSiteCustomDomain(String guid) throws BaseException
    {
        return getSiteCustomDomainService().deleteSiteCustomDomain(guid);
    }

    @Override
    public Boolean deleteSiteCustomDomain(SiteCustomDomain siteCustomDomain) throws BaseException
    {
        return getSiteCustomDomainService().deleteSiteCustomDomain(siteCustomDomain);
    }

    @Override
    public Long deleteSiteCustomDomains(String filter, String params, List<String> values) throws BaseException
    {
        return getSiteCustomDomainService().deleteSiteCustomDomains(filter, params, values);
    }

}
