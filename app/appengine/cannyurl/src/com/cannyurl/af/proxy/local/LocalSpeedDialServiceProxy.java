package com.cannyurl.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.SpeedDial;
import com.myurldb.ws.service.SpeedDialService;
import com.cannyurl.af.proxy.SpeedDialServiceProxy;

public class LocalSpeedDialServiceProxy extends BaseLocalServiceProxy implements SpeedDialServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalSpeedDialServiceProxy.class.getName());

    public LocalSpeedDialServiceProxy()
    {
    }

    @Override
    public SpeedDial getSpeedDial(String guid) throws BaseException
    {
        return getSpeedDialService().getSpeedDial(guid);
    }

    @Override
    public Object getSpeedDial(String guid, String field) throws BaseException
    {
        return getSpeedDialService().getSpeedDial(guid, field);       
    }

    @Override
    public List<SpeedDial> getSpeedDials(List<String> guids) throws BaseException
    {
        return getSpeedDialService().getSpeedDials(guids);
    }

    @Override
    public List<SpeedDial> getAllSpeedDials() throws BaseException
    {
        return getAllSpeedDials(null, null, null);
    }

    @Override
    public List<SpeedDial> getAllSpeedDials(String ordering, Long offset, Integer count) throws BaseException
    {
        return getSpeedDialService().getAllSpeedDials(ordering, offset, count);
    }

    @Override
    public List<String> getAllSpeedDialKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getSpeedDialService().getAllSpeedDialKeys(ordering, offset, count);
    }

    @Override
    public List<SpeedDial> findSpeedDials(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findSpeedDials(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<SpeedDial> findSpeedDials(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getSpeedDialService().findSpeedDials(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findSpeedDialKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getSpeedDialService().findSpeedDialKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getSpeedDialService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createSpeedDial(String user, String code, String shortcut, String shortLink, String keywordLink, String bookmarkLink, String longUrl, String shortUrl, Boolean caseSensitive, String status, String note) throws BaseException
    {
        return getSpeedDialService().createSpeedDial(user, code, shortcut, shortLink, keywordLink, bookmarkLink, longUrl, shortUrl, caseSensitive, status, note);
    }

    @Override
    public String createSpeedDial(SpeedDial speedDial) throws BaseException
    {
        return getSpeedDialService().createSpeedDial(speedDial);
    }

    @Override
    public Boolean updateSpeedDial(String guid, String user, String code, String shortcut, String shortLink, String keywordLink, String bookmarkLink, String longUrl, String shortUrl, Boolean caseSensitive, String status, String note) throws BaseException
    {
        return getSpeedDialService().updateSpeedDial(guid, user, code, shortcut, shortLink, keywordLink, bookmarkLink, longUrl, shortUrl, caseSensitive, status, note);
    }

    @Override
    public Boolean updateSpeedDial(SpeedDial speedDial) throws BaseException
    {
        return getSpeedDialService().updateSpeedDial(speedDial);
    }

    @Override
    public Boolean deleteSpeedDial(String guid) throws BaseException
    {
        return getSpeedDialService().deleteSpeedDial(guid);
    }

    @Override
    public Boolean deleteSpeedDial(SpeedDial speedDial) throws BaseException
    {
        return getSpeedDialService().deleteSpeedDial(speedDial);
    }

    @Override
    public Long deleteSpeedDials(String filter, String params, List<String> values) throws BaseException
    {
        return getSpeedDialService().deleteSpeedDials(filter, params, values);
    }

}
