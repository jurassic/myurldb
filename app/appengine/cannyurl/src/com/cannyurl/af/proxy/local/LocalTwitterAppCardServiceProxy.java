package com.cannyurl.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.TwitterAppCard;
import com.myurldb.ws.service.TwitterAppCardService;
import com.cannyurl.af.proxy.TwitterAppCardServiceProxy;

public class LocalTwitterAppCardServiceProxy extends BaseLocalServiceProxy implements TwitterAppCardServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalTwitterAppCardServiceProxy.class.getName());

    public LocalTwitterAppCardServiceProxy()
    {
    }

    @Override
    public TwitterAppCard getTwitterAppCard(String guid) throws BaseException
    {
        return getTwitterAppCardService().getTwitterAppCard(guid);
    }

    @Override
    public Object getTwitterAppCard(String guid, String field) throws BaseException
    {
        return getTwitterAppCardService().getTwitterAppCard(guid, field);       
    }

    @Override
    public List<TwitterAppCard> getTwitterAppCards(List<String> guids) throws BaseException
    {
        return getTwitterAppCardService().getTwitterAppCards(guids);
    }

    @Override
    public List<TwitterAppCard> getAllTwitterAppCards() throws BaseException
    {
        return getAllTwitterAppCards(null, null, null);
    }

    @Override
    public List<TwitterAppCard> getAllTwitterAppCards(String ordering, Long offset, Integer count) throws BaseException
    {
        return getTwitterAppCardService().getAllTwitterAppCards(ordering, offset, count);
    }

    @Override
    public List<String> getAllTwitterAppCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getTwitterAppCardService().getAllTwitterAppCardKeys(ordering, offset, count);
    }

    @Override
    public List<TwitterAppCard> findTwitterAppCards(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findTwitterAppCards(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<TwitterAppCard> findTwitterAppCards(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getTwitterAppCardService().findTwitterAppCards(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findTwitterAppCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getTwitterAppCardService().findTwitterAppCardKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getTwitterAppCardService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createTwitterAppCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, TwitterCardAppInfo iphoneApp, TwitterCardAppInfo ipadApp, TwitterCardAppInfo googlePlayApp) throws BaseException
    {
        return getTwitterAppCardService().createTwitterAppCard(card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, iphoneApp, ipadApp, googlePlayApp);
    }

    @Override
    public String createTwitterAppCard(TwitterAppCard twitterAppCard) throws BaseException
    {
        return getTwitterAppCardService().createTwitterAppCard(twitterAppCard);
    }

    @Override
    public Boolean updateTwitterAppCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, TwitterCardAppInfo iphoneApp, TwitterCardAppInfo ipadApp, TwitterCardAppInfo googlePlayApp) throws BaseException
    {
        return getTwitterAppCardService().updateTwitterAppCard(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, iphoneApp, ipadApp, googlePlayApp);
    }

    @Override
    public Boolean updateTwitterAppCard(TwitterAppCard twitterAppCard) throws BaseException
    {
        return getTwitterAppCardService().updateTwitterAppCard(twitterAppCard);
    }

    @Override
    public Boolean deleteTwitterAppCard(String guid) throws BaseException
    {
        return getTwitterAppCardService().deleteTwitterAppCard(guid);
    }

    @Override
    public Boolean deleteTwitterAppCard(TwitterAppCard twitterAppCard) throws BaseException
    {
        return getTwitterAppCardService().deleteTwitterAppCard(twitterAppCard);
    }

    @Override
    public Long deleteTwitterAppCards(String filter, String params, List<String> values) throws BaseException
    {
        return getTwitterAppCardService().deleteTwitterAppCards(filter, params, values);
    }

}
