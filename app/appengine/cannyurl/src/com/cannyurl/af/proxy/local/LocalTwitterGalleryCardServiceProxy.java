package com.cannyurl.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.TwitterGalleryCard;
import com.myurldb.ws.service.TwitterGalleryCardService;
import com.cannyurl.af.proxy.TwitterGalleryCardServiceProxy;

public class LocalTwitterGalleryCardServiceProxy extends BaseLocalServiceProxy implements TwitterGalleryCardServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalTwitterGalleryCardServiceProxy.class.getName());

    public LocalTwitterGalleryCardServiceProxy()
    {
    }

    @Override
    public TwitterGalleryCard getTwitterGalleryCard(String guid) throws BaseException
    {
        return getTwitterGalleryCardService().getTwitterGalleryCard(guid);
    }

    @Override
    public Object getTwitterGalleryCard(String guid, String field) throws BaseException
    {
        return getTwitterGalleryCardService().getTwitterGalleryCard(guid, field);       
    }

    @Override
    public List<TwitterGalleryCard> getTwitterGalleryCards(List<String> guids) throws BaseException
    {
        return getTwitterGalleryCardService().getTwitterGalleryCards(guids);
    }

    @Override
    public List<TwitterGalleryCard> getAllTwitterGalleryCards() throws BaseException
    {
        return getAllTwitterGalleryCards(null, null, null);
    }

    @Override
    public List<TwitterGalleryCard> getAllTwitterGalleryCards(String ordering, Long offset, Integer count) throws BaseException
    {
        return getTwitterGalleryCardService().getAllTwitterGalleryCards(ordering, offset, count);
    }

    @Override
    public List<String> getAllTwitterGalleryCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getTwitterGalleryCardService().getAllTwitterGalleryCardKeys(ordering, offset, count);
    }

    @Override
    public List<TwitterGalleryCard> findTwitterGalleryCards(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findTwitterGalleryCards(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<TwitterGalleryCard> findTwitterGalleryCards(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getTwitterGalleryCardService().findTwitterGalleryCards(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findTwitterGalleryCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getTwitterGalleryCardService().findTwitterGalleryCardKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getTwitterGalleryCardService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createTwitterGalleryCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image0, String image1, String image2, String image3) throws BaseException
    {
        return getTwitterGalleryCardService().createTwitterGalleryCard(card, url, title, description, site, siteId, creator, creatorId, image0, image1, image2, image3);
    }

    @Override
    public String createTwitterGalleryCard(TwitterGalleryCard twitterGalleryCard) throws BaseException
    {
        return getTwitterGalleryCardService().createTwitterGalleryCard(twitterGalleryCard);
    }

    @Override
    public Boolean updateTwitterGalleryCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image0, String image1, String image2, String image3) throws BaseException
    {
        return getTwitterGalleryCardService().updateTwitterGalleryCard(guid, card, url, title, description, site, siteId, creator, creatorId, image0, image1, image2, image3);
    }

    @Override
    public Boolean updateTwitterGalleryCard(TwitterGalleryCard twitterGalleryCard) throws BaseException
    {
        return getTwitterGalleryCardService().updateTwitterGalleryCard(twitterGalleryCard);
    }

    @Override
    public Boolean deleteTwitterGalleryCard(String guid) throws BaseException
    {
        return getTwitterGalleryCardService().deleteTwitterGalleryCard(guid);
    }

    @Override
    public Boolean deleteTwitterGalleryCard(TwitterGalleryCard twitterGalleryCard) throws BaseException
    {
        return getTwitterGalleryCardService().deleteTwitterGalleryCard(twitterGalleryCard);
    }

    @Override
    public Long deleteTwitterGalleryCards(String filter, String params, List<String> values) throws BaseException
    {
        return getTwitterGalleryCardService().deleteTwitterGalleryCards(filter, params, values);
    }

}
