package com.cannyurl.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.TwitterPhotoCard;
import com.myurldb.ws.service.TwitterPhotoCardService;
import com.cannyurl.af.proxy.TwitterPhotoCardServiceProxy;

public class LocalTwitterPhotoCardServiceProxy extends BaseLocalServiceProxy implements TwitterPhotoCardServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalTwitterPhotoCardServiceProxy.class.getName());

    public LocalTwitterPhotoCardServiceProxy()
    {
    }

    @Override
    public TwitterPhotoCard getTwitterPhotoCard(String guid) throws BaseException
    {
        return getTwitterPhotoCardService().getTwitterPhotoCard(guid);
    }

    @Override
    public Object getTwitterPhotoCard(String guid, String field) throws BaseException
    {
        return getTwitterPhotoCardService().getTwitterPhotoCard(guid, field);       
    }

    @Override
    public List<TwitterPhotoCard> getTwitterPhotoCards(List<String> guids) throws BaseException
    {
        return getTwitterPhotoCardService().getTwitterPhotoCards(guids);
    }

    @Override
    public List<TwitterPhotoCard> getAllTwitterPhotoCards() throws BaseException
    {
        return getAllTwitterPhotoCards(null, null, null);
    }

    @Override
    public List<TwitterPhotoCard> getAllTwitterPhotoCards(String ordering, Long offset, Integer count) throws BaseException
    {
        return getTwitterPhotoCardService().getAllTwitterPhotoCards(ordering, offset, count);
    }

    @Override
    public List<String> getAllTwitterPhotoCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getTwitterPhotoCardService().getAllTwitterPhotoCardKeys(ordering, offset, count);
    }

    @Override
    public List<TwitterPhotoCard> findTwitterPhotoCards(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findTwitterPhotoCards(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<TwitterPhotoCard> findTwitterPhotoCards(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getTwitterPhotoCardService().findTwitterPhotoCards(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findTwitterPhotoCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getTwitterPhotoCardService().findTwitterPhotoCardKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getTwitterPhotoCardService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createTwitterPhotoCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight) throws BaseException
    {
        return getTwitterPhotoCardService().createTwitterPhotoCard(card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight);
    }

    @Override
    public String createTwitterPhotoCard(TwitterPhotoCard twitterPhotoCard) throws BaseException
    {
        return getTwitterPhotoCardService().createTwitterPhotoCard(twitterPhotoCard);
    }

    @Override
    public Boolean updateTwitterPhotoCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight) throws BaseException
    {
        return getTwitterPhotoCardService().updateTwitterPhotoCard(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight);
    }

    @Override
    public Boolean updateTwitterPhotoCard(TwitterPhotoCard twitterPhotoCard) throws BaseException
    {
        return getTwitterPhotoCardService().updateTwitterPhotoCard(twitterPhotoCard);
    }

    @Override
    public Boolean deleteTwitterPhotoCard(String guid) throws BaseException
    {
        return getTwitterPhotoCardService().deleteTwitterPhotoCard(guid);
    }

    @Override
    public Boolean deleteTwitterPhotoCard(TwitterPhotoCard twitterPhotoCard) throws BaseException
    {
        return getTwitterPhotoCardService().deleteTwitterPhotoCard(twitterPhotoCard);
    }

    @Override
    public Long deleteTwitterPhotoCards(String filter, String params, List<String> values) throws BaseException
    {
        return getTwitterPhotoCardService().deleteTwitterPhotoCards(filter, params, values);
    }

}
