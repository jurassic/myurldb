package com.cannyurl.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.TwitterPlayerCard;
import com.myurldb.ws.service.TwitterPlayerCardService;
import com.cannyurl.af.proxy.TwitterPlayerCardServiceProxy;

public class LocalTwitterPlayerCardServiceProxy extends BaseLocalServiceProxy implements TwitterPlayerCardServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalTwitterPlayerCardServiceProxy.class.getName());

    public LocalTwitterPlayerCardServiceProxy()
    {
    }

    @Override
    public TwitterPlayerCard getTwitterPlayerCard(String guid) throws BaseException
    {
        return getTwitterPlayerCardService().getTwitterPlayerCard(guid);
    }

    @Override
    public Object getTwitterPlayerCard(String guid, String field) throws BaseException
    {
        return getTwitterPlayerCardService().getTwitterPlayerCard(guid, field);       
    }

    @Override
    public List<TwitterPlayerCard> getTwitterPlayerCards(List<String> guids) throws BaseException
    {
        return getTwitterPlayerCardService().getTwitterPlayerCards(guids);
    }

    @Override
    public List<TwitterPlayerCard> getAllTwitterPlayerCards() throws BaseException
    {
        return getAllTwitterPlayerCards(null, null, null);
    }

    @Override
    public List<TwitterPlayerCard> getAllTwitterPlayerCards(String ordering, Long offset, Integer count) throws BaseException
    {
        return getTwitterPlayerCardService().getAllTwitterPlayerCards(ordering, offset, count);
    }

    @Override
    public List<String> getAllTwitterPlayerCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getTwitterPlayerCardService().getAllTwitterPlayerCardKeys(ordering, offset, count);
    }

    @Override
    public List<TwitterPlayerCard> findTwitterPlayerCards(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findTwitterPlayerCards(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<TwitterPlayerCard> findTwitterPlayerCards(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getTwitterPlayerCardService().findTwitterPlayerCards(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findTwitterPlayerCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getTwitterPlayerCardService().findTwitterPlayerCardKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getTwitterPlayerCardService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createTwitterPlayerCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, String player, Integer playerWidth, Integer playerHeight, String playerStream, String playerStreamContentType) throws BaseException
    {
        return getTwitterPlayerCardService().createTwitterPlayerCard(card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, player, playerWidth, playerHeight, playerStream, playerStreamContentType);
    }

    @Override
    public String createTwitterPlayerCard(TwitterPlayerCard twitterPlayerCard) throws BaseException
    {
        return getTwitterPlayerCardService().createTwitterPlayerCard(twitterPlayerCard);
    }

    @Override
    public Boolean updateTwitterPlayerCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, String player, Integer playerWidth, Integer playerHeight, String playerStream, String playerStreamContentType) throws BaseException
    {
        return getTwitterPlayerCardService().updateTwitterPlayerCard(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, player, playerWidth, playerHeight, playerStream, playerStreamContentType);
    }

    @Override
    public Boolean updateTwitterPlayerCard(TwitterPlayerCard twitterPlayerCard) throws BaseException
    {
        return getTwitterPlayerCardService().updateTwitterPlayerCard(twitterPlayerCard);
    }

    @Override
    public Boolean deleteTwitterPlayerCard(String guid) throws BaseException
    {
        return getTwitterPlayerCardService().deleteTwitterPlayerCard(guid);
    }

    @Override
    public Boolean deleteTwitterPlayerCard(TwitterPlayerCard twitterPlayerCard) throws BaseException
    {
        return getTwitterPlayerCardService().deleteTwitterPlayerCard(twitterPlayerCard);
    }

    @Override
    public Long deleteTwitterPlayerCards(String filter, String params, List<String> values) throws BaseException
    {
        return getTwitterPlayerCardService().deleteTwitterPlayerCards(filter, params, values);
    }

}
