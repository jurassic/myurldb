package com.cannyurl.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.TwitterSummaryCard;
import com.myurldb.ws.service.TwitterSummaryCardService;
import com.cannyurl.af.proxy.TwitterSummaryCardServiceProxy;

public class LocalTwitterSummaryCardServiceProxy extends BaseLocalServiceProxy implements TwitterSummaryCardServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalTwitterSummaryCardServiceProxy.class.getName());

    public LocalTwitterSummaryCardServiceProxy()
    {
    }

    @Override
    public TwitterSummaryCard getTwitterSummaryCard(String guid) throws BaseException
    {
        return getTwitterSummaryCardService().getTwitterSummaryCard(guid);
    }

    @Override
    public Object getTwitterSummaryCard(String guid, String field) throws BaseException
    {
        return getTwitterSummaryCardService().getTwitterSummaryCard(guid, field);       
    }

    @Override
    public List<TwitterSummaryCard> getTwitterSummaryCards(List<String> guids) throws BaseException
    {
        return getTwitterSummaryCardService().getTwitterSummaryCards(guids);
    }

    @Override
    public List<TwitterSummaryCard> getAllTwitterSummaryCards() throws BaseException
    {
        return getAllTwitterSummaryCards(null, null, null);
    }

    @Override
    public List<TwitterSummaryCard> getAllTwitterSummaryCards(String ordering, Long offset, Integer count) throws BaseException
    {
        return getTwitterSummaryCardService().getAllTwitterSummaryCards(ordering, offset, count);
    }

    @Override
    public List<String> getAllTwitterSummaryCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getTwitterSummaryCardService().getAllTwitterSummaryCardKeys(ordering, offset, count);
    }

    @Override
    public List<TwitterSummaryCard> findTwitterSummaryCards(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findTwitterSummaryCards(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<TwitterSummaryCard> findTwitterSummaryCards(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getTwitterSummaryCardService().findTwitterSummaryCards(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findTwitterSummaryCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getTwitterSummaryCardService().findTwitterSummaryCardKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getTwitterSummaryCardService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createTwitterSummaryCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight) throws BaseException
    {
        return getTwitterSummaryCardService().createTwitterSummaryCard(card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight);
    }

    @Override
    public String createTwitterSummaryCard(TwitterSummaryCard twitterSummaryCard) throws BaseException
    {
        return getTwitterSummaryCardService().createTwitterSummaryCard(twitterSummaryCard);
    }

    @Override
    public Boolean updateTwitterSummaryCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight) throws BaseException
    {
        return getTwitterSummaryCardService().updateTwitterSummaryCard(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight);
    }

    @Override
    public Boolean updateTwitterSummaryCard(TwitterSummaryCard twitterSummaryCard) throws BaseException
    {
        return getTwitterSummaryCardService().updateTwitterSummaryCard(twitterSummaryCard);
    }

    @Override
    public Boolean deleteTwitterSummaryCard(String guid) throws BaseException
    {
        return getTwitterSummaryCardService().deleteTwitterSummaryCard(guid);
    }

    @Override
    public Boolean deleteTwitterSummaryCard(TwitterSummaryCard twitterSummaryCard) throws BaseException
    {
        return getTwitterSummaryCardService().deleteTwitterSummaryCard(twitterSummaryCard);
    }

    @Override
    public Long deleteTwitterSummaryCards(String filter, String params, List<String> values) throws BaseException
    {
        return getTwitterSummaryCardService().deleteTwitterSummaryCards(filter, params, values);
    }

}
