package com.cannyurl.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.UrlRating;
import com.myurldb.ws.service.UrlRatingService;
import com.cannyurl.af.proxy.UrlRatingServiceProxy;

public class LocalUrlRatingServiceProxy extends BaseLocalServiceProxy implements UrlRatingServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalUrlRatingServiceProxy.class.getName());

    public LocalUrlRatingServiceProxy()
    {
    }

    @Override
    public UrlRating getUrlRating(String guid) throws BaseException
    {
        return getUrlRatingService().getUrlRating(guid);
    }

    @Override
    public Object getUrlRating(String guid, String field) throws BaseException
    {
        return getUrlRatingService().getUrlRating(guid, field);       
    }

    @Override
    public List<UrlRating> getUrlRatings(List<String> guids) throws BaseException
    {
        return getUrlRatingService().getUrlRatings(guids);
    }

    @Override
    public List<UrlRating> getAllUrlRatings() throws BaseException
    {
        return getAllUrlRatings(null, null, null);
    }

    @Override
    public List<UrlRating> getAllUrlRatings(String ordering, Long offset, Integer count) throws BaseException
    {
        return getUrlRatingService().getAllUrlRatings(ordering, offset, count);
    }

    @Override
    public List<String> getAllUrlRatingKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getUrlRatingService().getAllUrlRatingKeys(ordering, offset, count);
    }

    @Override
    public List<UrlRating> findUrlRatings(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findUrlRatings(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<UrlRating> findUrlRatings(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getUrlRatingService().findUrlRatings(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findUrlRatingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getUrlRatingService().findUrlRatingKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getUrlRatingService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createUrlRating(String domain, String longUrl, String longUrlHash, String preview, String flag, Double rating, String note, Long ratedTime) throws BaseException
    {
        return getUrlRatingService().createUrlRating(domain, longUrl, longUrlHash, preview, flag, rating, note, ratedTime);
    }

    @Override
    public String createUrlRating(UrlRating urlRating) throws BaseException
    {
        return getUrlRatingService().createUrlRating(urlRating);
    }

    @Override
    public Boolean updateUrlRating(String guid, String domain, String longUrl, String longUrlHash, String preview, String flag, Double rating, String note, Long ratedTime) throws BaseException
    {
        return getUrlRatingService().updateUrlRating(guid, domain, longUrl, longUrlHash, preview, flag, rating, note, ratedTime);
    }

    @Override
    public Boolean updateUrlRating(UrlRating urlRating) throws BaseException
    {
        return getUrlRatingService().updateUrlRating(urlRating);
    }

    @Override
    public Boolean deleteUrlRating(String guid) throws BaseException
    {
        return getUrlRatingService().deleteUrlRating(guid);
    }

    @Override
    public Boolean deleteUrlRating(UrlRating urlRating) throws BaseException
    {
        return getUrlRatingService().deleteUrlRating(urlRating);
    }

    @Override
    public Long deleteUrlRatings(String filter, String params, List<String> values) throws BaseException
    {
        return getUrlRatingService().deleteUrlRatings(filter, params, values);
    }

}
