package com.cannyurl.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.ExternalUserIdStruct;
import com.myurldb.ws.UserAuthState;
import com.myurldb.ws.service.UserAuthStateService;
import com.cannyurl.af.proxy.UserAuthStateServiceProxy;

public class LocalUserAuthStateServiceProxy extends BaseLocalServiceProxy implements UserAuthStateServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalUserAuthStateServiceProxy.class.getName());

    public LocalUserAuthStateServiceProxy()
    {
    }

    @Override
    public UserAuthState getUserAuthState(String guid) throws BaseException
    {
        return getUserAuthStateService().getUserAuthState(guid);
    }

    @Override
    public Object getUserAuthState(String guid, String field) throws BaseException
    {
        return getUserAuthStateService().getUserAuthState(guid, field);       
    }

    @Override
    public List<UserAuthState> getUserAuthStates(List<String> guids) throws BaseException
    {
        return getUserAuthStateService().getUserAuthStates(guids);
    }

    @Override
    public List<UserAuthState> getAllUserAuthStates() throws BaseException
    {
        return getAllUserAuthStates(null, null, null);
    }

    @Override
    public List<UserAuthState> getAllUserAuthStates(String ordering, Long offset, Integer count) throws BaseException
    {
        return getUserAuthStateService().getAllUserAuthStates(ordering, offset, count);
    }

    @Override
    public List<String> getAllUserAuthStateKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getUserAuthStateService().getAllUserAuthStateKeys(ordering, offset, count);
    }

    @Override
    public List<UserAuthState> findUserAuthStates(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findUserAuthStates(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<UserAuthState> findUserAuthStates(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getUserAuthStateService().findUserAuthStates(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findUserAuthStateKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getUserAuthStateService().findUserAuthStateKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getUserAuthStateService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createUserAuthState(String providerId, String user, String username, String email, String openId, String deviceId, String sessionId, String authToken, String authStatus, String externalAuth, ExternalUserIdStruct externalId, String status, Long firstAuthTime, Long lastAuthTime, Long expirationTime) throws BaseException
    {
        return getUserAuthStateService().createUserAuthState(providerId, user, username, email, openId, deviceId, sessionId, authToken, authStatus, externalAuth, externalId, status, firstAuthTime, lastAuthTime, expirationTime);
    }

    @Override
    public String createUserAuthState(UserAuthState userAuthState) throws BaseException
    {
        return getUserAuthStateService().createUserAuthState(userAuthState);
    }

    @Override
    public Boolean updateUserAuthState(String guid, String providerId, String user, String username, String email, String openId, String deviceId, String sessionId, String authToken, String authStatus, String externalAuth, ExternalUserIdStruct externalId, String status, Long firstAuthTime, Long lastAuthTime, Long expirationTime) throws BaseException
    {
        return getUserAuthStateService().updateUserAuthState(guid, providerId, user, username, email, openId, deviceId, sessionId, authToken, authStatus, externalAuth, externalId, status, firstAuthTime, lastAuthTime, expirationTime);
    }

    @Override
    public Boolean updateUserAuthState(UserAuthState userAuthState) throws BaseException
    {
        return getUserAuthStateService().updateUserAuthState(userAuthState);
    }

    @Override
    public Boolean deleteUserAuthState(String guid) throws BaseException
    {
        return getUserAuthStateService().deleteUserAuthState(guid);
    }

    @Override
    public Boolean deleteUserAuthState(UserAuthState userAuthState) throws BaseException
    {
        return getUserAuthStateService().deleteUserAuthState(userAuthState);
    }

    @Override
    public Long deleteUserAuthStates(String filter, String params, List<String> values) throws BaseException
    {
        return getUserAuthStateService().deleteUserAuthStates(filter, params, values);
    }

}
