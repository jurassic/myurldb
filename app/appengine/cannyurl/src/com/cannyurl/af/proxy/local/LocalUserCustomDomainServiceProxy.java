package com.cannyurl.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.UserCustomDomain;
import com.myurldb.ws.service.UserCustomDomainService;
import com.cannyurl.af.proxy.UserCustomDomainServiceProxy;

public class LocalUserCustomDomainServiceProxy extends BaseLocalServiceProxy implements UserCustomDomainServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalUserCustomDomainServiceProxy.class.getName());

    public LocalUserCustomDomainServiceProxy()
    {
    }

    @Override
    public UserCustomDomain getUserCustomDomain(String guid) throws BaseException
    {
        return getUserCustomDomainService().getUserCustomDomain(guid);
    }

    @Override
    public Object getUserCustomDomain(String guid, String field) throws BaseException
    {
        return getUserCustomDomainService().getUserCustomDomain(guid, field);       
    }

    @Override
    public List<UserCustomDomain> getUserCustomDomains(List<String> guids) throws BaseException
    {
        return getUserCustomDomainService().getUserCustomDomains(guids);
    }

    @Override
    public List<UserCustomDomain> getAllUserCustomDomains() throws BaseException
    {
        return getAllUserCustomDomains(null, null, null);
    }

    @Override
    public List<UserCustomDomain> getAllUserCustomDomains(String ordering, Long offset, Integer count) throws BaseException
    {
        return getUserCustomDomainService().getAllUserCustomDomains(ordering, offset, count);
    }

    @Override
    public List<String> getAllUserCustomDomainKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getUserCustomDomainService().getAllUserCustomDomainKeys(ordering, offset, count);
    }

    @Override
    public List<UserCustomDomain> findUserCustomDomains(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findUserCustomDomains(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<UserCustomDomain> findUserCustomDomains(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getUserCustomDomainService().findUserCustomDomains(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findUserCustomDomainKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getUserCustomDomainService().findUserCustomDomainKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getUserCustomDomainService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createUserCustomDomain(String owner, String domain, Boolean verified, String status, Long verifiedTime) throws BaseException
    {
        return getUserCustomDomainService().createUserCustomDomain(owner, domain, verified, status, verifiedTime);
    }

    @Override
    public String createUserCustomDomain(UserCustomDomain userCustomDomain) throws BaseException
    {
        return getUserCustomDomainService().createUserCustomDomain(userCustomDomain);
    }

    @Override
    public Boolean updateUserCustomDomain(String guid, String owner, String domain, Boolean verified, String status, Long verifiedTime) throws BaseException
    {
        return getUserCustomDomainService().updateUserCustomDomain(guid, owner, domain, verified, status, verifiedTime);
    }

    @Override
    public Boolean updateUserCustomDomain(UserCustomDomain userCustomDomain) throws BaseException
    {
        return getUserCustomDomainService().updateUserCustomDomain(userCustomDomain);
    }

    @Override
    public Boolean deleteUserCustomDomain(String guid) throws BaseException
    {
        return getUserCustomDomainService().deleteUserCustomDomain(guid);
    }

    @Override
    public Boolean deleteUserCustomDomain(UserCustomDomain userCustomDomain) throws BaseException
    {
        return getUserCustomDomainService().deleteUserCustomDomain(userCustomDomain);
    }

    @Override
    public Long deleteUserCustomDomains(String filter, String params, List<String> values) throws BaseException
    {
        return getUserCustomDomainService().deleteUserCustomDomains(filter, params, values);
    }

}
