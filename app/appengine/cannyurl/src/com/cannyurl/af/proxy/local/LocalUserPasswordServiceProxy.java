package com.cannyurl.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.HashedPasswordStruct;
import com.myurldb.ws.UserPassword;
import com.myurldb.ws.service.UserPasswordService;
import com.cannyurl.af.proxy.UserPasswordServiceProxy;

public class LocalUserPasswordServiceProxy extends BaseLocalServiceProxy implements UserPasswordServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalUserPasswordServiceProxy.class.getName());

    public LocalUserPasswordServiceProxy()
    {
    }

    @Override
    public UserPassword getUserPassword(String guid) throws BaseException
    {
        return getUserPasswordService().getUserPassword(guid);
    }

    @Override
    public Object getUserPassword(String guid, String field) throws BaseException
    {
        return getUserPasswordService().getUserPassword(guid, field);       
    }

    @Override
    public List<UserPassword> getUserPasswords(List<String> guids) throws BaseException
    {
        return getUserPasswordService().getUserPasswords(guids);
    }

    @Override
    public List<UserPassword> getAllUserPasswords() throws BaseException
    {
        return getAllUserPasswords(null, null, null);
    }

    @Override
    public List<UserPassword> getAllUserPasswords(String ordering, Long offset, Integer count) throws BaseException
    {
        return getUserPasswordService().getAllUserPasswords(ordering, offset, count);
    }

    @Override
    public List<String> getAllUserPasswordKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getUserPasswordService().getAllUserPasswordKeys(ordering, offset, count);
    }

    @Override
    public List<UserPassword> findUserPasswords(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findUserPasswords(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<UserPassword> findUserPasswords(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getUserPasswordService().findUserPasswords(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findUserPasswordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getUserPasswordService().findUserPasswordKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getUserPasswordService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createUserPassword(String admin, String user, String username, String email, String openId, HashedPasswordStruct password, Boolean resetRequired, String challengeQuestion, String challengeAnswer, String status, Long lastResetTime, Long expirationTime) throws BaseException
    {
        return getUserPasswordService().createUserPassword(admin, user, username, email, openId, password, resetRequired, challengeQuestion, challengeAnswer, status, lastResetTime, expirationTime);
    }

    @Override
    public String createUserPassword(UserPassword userPassword) throws BaseException
    {
        return getUserPasswordService().createUserPassword(userPassword);
    }

    @Override
    public Boolean updateUserPassword(String guid, String admin, String user, String username, String email, String openId, HashedPasswordStruct password, Boolean resetRequired, String challengeQuestion, String challengeAnswer, String status, Long lastResetTime, Long expirationTime) throws BaseException
    {
        return getUserPasswordService().updateUserPassword(guid, admin, user, username, email, openId, password, resetRequired, challengeQuestion, challengeAnswer, status, lastResetTime, expirationTime);
    }

    @Override
    public Boolean updateUserPassword(UserPassword userPassword) throws BaseException
    {
        return getUserPasswordService().updateUserPassword(userPassword);
    }

    @Override
    public Boolean deleteUserPassword(String guid) throws BaseException
    {
        return getUserPasswordService().deleteUserPassword(guid);
    }

    @Override
    public Boolean deleteUserPassword(UserPassword userPassword) throws BaseException
    {
        return getUserPasswordService().deleteUserPassword(userPassword);
    }

    @Override
    public Long deleteUserPasswords(String filter, String params, List<String> values) throws BaseException
    {
        return getUserPasswordService().deleteUserPasswords(filter, params, values);
    }

}
