package com.cannyurl.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.UserRating;
import com.myurldb.ws.service.UserRatingService;
import com.cannyurl.af.proxy.UserRatingServiceProxy;

public class LocalUserRatingServiceProxy extends BaseLocalServiceProxy implements UserRatingServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalUserRatingServiceProxy.class.getName());

    public LocalUserRatingServiceProxy()
    {
    }

    @Override
    public UserRating getUserRating(String guid) throws BaseException
    {
        return getUserRatingService().getUserRating(guid);
    }

    @Override
    public Object getUserRating(String guid, String field) throws BaseException
    {
        return getUserRatingService().getUserRating(guid, field);       
    }

    @Override
    public List<UserRating> getUserRatings(List<String> guids) throws BaseException
    {
        return getUserRatingService().getUserRatings(guids);
    }

    @Override
    public List<UserRating> getAllUserRatings() throws BaseException
    {
        return getAllUserRatings(null, null, null);
    }

    @Override
    public List<UserRating> getAllUserRatings(String ordering, Long offset, Integer count) throws BaseException
    {
        return getUserRatingService().getAllUserRatings(ordering, offset, count);
    }

    @Override
    public List<String> getAllUserRatingKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getUserRatingService().getAllUserRatingKeys(ordering, offset, count);
    }

    @Override
    public List<UserRating> findUserRatings(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findUserRatings(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<UserRating> findUserRatings(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getUserRatingService().findUserRatings(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findUserRatingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getUserRatingService().findUserRatingKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getUserRatingService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createUserRating(String user, Double rating, String note, Long ratedTime) throws BaseException
    {
        return getUserRatingService().createUserRating(user, rating, note, ratedTime);
    }

    @Override
    public String createUserRating(UserRating userRating) throws BaseException
    {
        return getUserRatingService().createUserRating(userRating);
    }

    @Override
    public Boolean updateUserRating(String guid, String user, Double rating, String note, Long ratedTime) throws BaseException
    {
        return getUserRatingService().updateUserRating(guid, user, rating, note, ratedTime);
    }

    @Override
    public Boolean updateUserRating(UserRating userRating) throws BaseException
    {
        return getUserRatingService().updateUserRating(userRating);
    }

    @Override
    public Boolean deleteUserRating(String guid) throws BaseException
    {
        return getUserRatingService().deleteUserRating(guid);
    }

    @Override
    public Boolean deleteUserRating(UserRating userRating) throws BaseException
    {
        return getUserRatingService().deleteUserRating(userRating);
    }

    @Override
    public Long deleteUserRatings(String filter, String params, List<String> values) throws BaseException
    {
        return getUserRatingService().deleteUserRatings(filter, params, values);
    }

}
