package com.cannyurl.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.UserResourcePermission;
import com.myurldb.ws.service.UserResourcePermissionService;
import com.cannyurl.af.proxy.UserResourcePermissionServiceProxy;

public class LocalUserResourcePermissionServiceProxy extends BaseLocalServiceProxy implements UserResourcePermissionServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalUserResourcePermissionServiceProxy.class.getName());

    public LocalUserResourcePermissionServiceProxy()
    {
    }

    @Override
    public UserResourcePermission getUserResourcePermission(String guid) throws BaseException
    {
        return getUserResourcePermissionService().getUserResourcePermission(guid);
    }

    @Override
    public Object getUserResourcePermission(String guid, String field) throws BaseException
    {
        return getUserResourcePermissionService().getUserResourcePermission(guid, field);       
    }

    @Override
    public List<UserResourcePermission> getUserResourcePermissions(List<String> guids) throws BaseException
    {
        return getUserResourcePermissionService().getUserResourcePermissions(guids);
    }

    @Override
    public List<UserResourcePermission> getAllUserResourcePermissions() throws BaseException
    {
        return getAllUserResourcePermissions(null, null, null);
    }

    @Override
    public List<UserResourcePermission> getAllUserResourcePermissions(String ordering, Long offset, Integer count) throws BaseException
    {
        return getUserResourcePermissionService().getAllUserResourcePermissions(ordering, offset, count);
    }

    @Override
    public List<String> getAllUserResourcePermissionKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getUserResourcePermissionService().getAllUserResourcePermissionKeys(ordering, offset, count);
    }

    @Override
    public List<UserResourcePermission> findUserResourcePermissions(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findUserResourcePermissions(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<UserResourcePermission> findUserResourcePermissions(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getUserResourcePermissionService().findUserResourcePermissions(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findUserResourcePermissionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getUserResourcePermissionService().findUserResourcePermissionKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getUserResourcePermissionService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createUserResourcePermission(String user, String permissionName, String resource, String instance, String action, Boolean permitted, String status) throws BaseException
    {
        return getUserResourcePermissionService().createUserResourcePermission(user, permissionName, resource, instance, action, permitted, status);
    }

    @Override
    public String createUserResourcePermission(UserResourcePermission userResourcePermission) throws BaseException
    {
        return getUserResourcePermissionService().createUserResourcePermission(userResourcePermission);
    }

    @Override
    public Boolean updateUserResourcePermission(String guid, String user, String permissionName, String resource, String instance, String action, Boolean permitted, String status) throws BaseException
    {
        return getUserResourcePermissionService().updateUserResourcePermission(guid, user, permissionName, resource, instance, action, permitted, status);
    }

    @Override
    public Boolean updateUserResourcePermission(UserResourcePermission userResourcePermission) throws BaseException
    {
        return getUserResourcePermissionService().updateUserResourcePermission(userResourcePermission);
    }

    @Override
    public Boolean deleteUserResourcePermission(String guid) throws BaseException
    {
        return getUserResourcePermissionService().deleteUserResourcePermission(guid);
    }

    @Override
    public Boolean deleteUserResourcePermission(UserResourcePermission userResourcePermission) throws BaseException
    {
        return getUserResourcePermissionService().deleteUserResourcePermission(userResourcePermission);
    }

    @Override
    public Long deleteUserResourcePermissions(String filter, String params, List<String> values) throws BaseException
    {
        return getUserResourcePermissionService().deleteUserResourcePermissions(filter, params, values);
    }

}
