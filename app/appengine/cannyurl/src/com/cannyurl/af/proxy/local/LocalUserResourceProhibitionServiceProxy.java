package com.cannyurl.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.UserResourceProhibition;
import com.myurldb.ws.service.UserResourceProhibitionService;
import com.cannyurl.af.proxy.UserResourceProhibitionServiceProxy;

public class LocalUserResourceProhibitionServiceProxy extends BaseLocalServiceProxy implements UserResourceProhibitionServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalUserResourceProhibitionServiceProxy.class.getName());

    public LocalUserResourceProhibitionServiceProxy()
    {
    }

    @Override
    public UserResourceProhibition getUserResourceProhibition(String guid) throws BaseException
    {
        return getUserResourceProhibitionService().getUserResourceProhibition(guid);
    }

    @Override
    public Object getUserResourceProhibition(String guid, String field) throws BaseException
    {
        return getUserResourceProhibitionService().getUserResourceProhibition(guid, field);       
    }

    @Override
    public List<UserResourceProhibition> getUserResourceProhibitions(List<String> guids) throws BaseException
    {
        return getUserResourceProhibitionService().getUserResourceProhibitions(guids);
    }

    @Override
    public List<UserResourceProhibition> getAllUserResourceProhibitions() throws BaseException
    {
        return getAllUserResourceProhibitions(null, null, null);
    }

    @Override
    public List<UserResourceProhibition> getAllUserResourceProhibitions(String ordering, Long offset, Integer count) throws BaseException
    {
        return getUserResourceProhibitionService().getAllUserResourceProhibitions(ordering, offset, count);
    }

    @Override
    public List<String> getAllUserResourceProhibitionKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getUserResourceProhibitionService().getAllUserResourceProhibitionKeys(ordering, offset, count);
    }

    @Override
    public List<UserResourceProhibition> findUserResourceProhibitions(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findUserResourceProhibitions(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<UserResourceProhibition> findUserResourceProhibitions(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getUserResourceProhibitionService().findUserResourceProhibitions(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findUserResourceProhibitionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getUserResourceProhibitionService().findUserResourceProhibitionKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getUserResourceProhibitionService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createUserResourceProhibition(String user, String permissionName, String resource, String instance, String action, Boolean prohibited, String status) throws BaseException
    {
        return getUserResourceProhibitionService().createUserResourceProhibition(user, permissionName, resource, instance, action, prohibited, status);
    }

    @Override
    public String createUserResourceProhibition(UserResourceProhibition userResourceProhibition) throws BaseException
    {
        return getUserResourceProhibitionService().createUserResourceProhibition(userResourceProhibition);
    }

    @Override
    public Boolean updateUserResourceProhibition(String guid, String user, String permissionName, String resource, String instance, String action, Boolean prohibited, String status) throws BaseException
    {
        return getUserResourceProhibitionService().updateUserResourceProhibition(guid, user, permissionName, resource, instance, action, prohibited, status);
    }

    @Override
    public Boolean updateUserResourceProhibition(UserResourceProhibition userResourceProhibition) throws BaseException
    {
        return getUserResourceProhibitionService().updateUserResourceProhibition(userResourceProhibition);
    }

    @Override
    public Boolean deleteUserResourceProhibition(String guid) throws BaseException
    {
        return getUserResourceProhibitionService().deleteUserResourceProhibition(guid);
    }

    @Override
    public Boolean deleteUserResourceProhibition(UserResourceProhibition userResourceProhibition) throws BaseException
    {
        return getUserResourceProhibitionService().deleteUserResourceProhibition(userResourceProhibition);
    }

    @Override
    public Long deleteUserResourceProhibitions(String filter, String params, List<String> values) throws BaseException
    {
        return getUserResourceProhibitionService().deleteUserResourceProhibitions(filter, params, values);
    }

}
