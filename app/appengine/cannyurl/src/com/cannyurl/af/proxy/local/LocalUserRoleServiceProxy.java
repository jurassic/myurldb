package com.cannyurl.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.UserRole;
import com.myurldb.ws.service.UserRoleService;
import com.cannyurl.af.proxy.UserRoleServiceProxy;

public class LocalUserRoleServiceProxy extends BaseLocalServiceProxy implements UserRoleServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalUserRoleServiceProxy.class.getName());

    public LocalUserRoleServiceProxy()
    {
    }

    @Override
    public UserRole getUserRole(String guid) throws BaseException
    {
        return getUserRoleService().getUserRole(guid);
    }

    @Override
    public Object getUserRole(String guid, String field) throws BaseException
    {
        return getUserRoleService().getUserRole(guid, field);       
    }

    @Override
    public List<UserRole> getUserRoles(List<String> guids) throws BaseException
    {
        return getUserRoleService().getUserRoles(guids);
    }

    @Override
    public List<UserRole> getAllUserRoles() throws BaseException
    {
        return getAllUserRoles(null, null, null);
    }

    @Override
    public List<UserRole> getAllUserRoles(String ordering, Long offset, Integer count) throws BaseException
    {
        return getUserRoleService().getAllUserRoles(ordering, offset, count);
    }

    @Override
    public List<String> getAllUserRoleKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getUserRoleService().getAllUserRoleKeys(ordering, offset, count);
    }

    @Override
    public List<UserRole> findUserRoles(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findUserRoles(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<UserRole> findUserRoles(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getUserRoleService().findUserRoles(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findUserRoleKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getUserRoleService().findUserRoleKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getUserRoleService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createUserRole(String user, String role, String status) throws BaseException
    {
        return getUserRoleService().createUserRole(user, role, status);
    }

    @Override
    public String createUserRole(UserRole userRole) throws BaseException
    {
        return getUserRoleService().createUserRole(userRole);
    }

    @Override
    public Boolean updateUserRole(String guid, String user, String role, String status) throws BaseException
    {
        return getUserRoleService().updateUserRole(guid, user, role, status);
    }

    @Override
    public Boolean updateUserRole(UserRole userRole) throws BaseException
    {
        return getUserRoleService().updateUserRole(userRole);
    }

    @Override
    public Boolean deleteUserRole(String guid) throws BaseException
    {
        return getUserRoleService().deleteUserRole(guid);
    }

    @Override
    public Boolean deleteUserRole(UserRole userRole) throws BaseException
    {
        return getUserRoleService().deleteUserRole(userRole);
    }

    @Override
    public Long deleteUserRoles(String filter, String params, List<String> values) throws BaseException
    {
        return getUserRoleService().deleteUserRoles(filter, params, values);
    }

}
