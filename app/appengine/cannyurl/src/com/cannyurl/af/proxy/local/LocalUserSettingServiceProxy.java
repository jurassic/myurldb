package com.cannyurl.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.UserSetting;
import com.myurldb.ws.service.UserSettingService;
import com.cannyurl.af.proxy.UserSettingServiceProxy;

public class LocalUserSettingServiceProxy extends BaseLocalServiceProxy implements UserSettingServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalUserSettingServiceProxy.class.getName());

    public LocalUserSettingServiceProxy()
    {
    }

    @Override
    public UserSetting getUserSetting(String guid) throws BaseException
    {
        return getUserSettingService().getUserSetting(guid);
    }

    @Override
    public Object getUserSetting(String guid, String field) throws BaseException
    {
        return getUserSettingService().getUserSetting(guid, field);       
    }

    @Override
    public List<UserSetting> getUserSettings(List<String> guids) throws BaseException
    {
        return getUserSettingService().getUserSettings(guids);
    }

    @Override
    public List<UserSetting> getAllUserSettings() throws BaseException
    {
        return getAllUserSettings(null, null, null);
    }

    @Override
    public List<UserSetting> getAllUserSettings(String ordering, Long offset, Integer count) throws BaseException
    {
        return getUserSettingService().getAllUserSettings(ordering, offset, count);
    }

    @Override
    public List<String> getAllUserSettingKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getUserSettingService().getAllUserSettingKeys(ordering, offset, count);
    }

    @Override
    public List<UserSetting> findUserSettings(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findUserSettings(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<UserSetting> findUserSettings(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getUserSettingService().findUserSettings(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findUserSettingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getUserSettingService().findUserSettingKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getUserSettingService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createUserSetting(String user, String homePage, String selfBio, String userUrlDomain, String userUrlDomainNormalized, Boolean autoRedirectEnabled, Boolean viewEnabled, Boolean shareEnabled, String defaultDomain, String defaultTokenType, String defaultRedirectType, Long defaultFlashDuration, String defaultAccessType, String defaultViewType, String defaultShareType, String defaultPassphrase, String defaultSignature, Boolean useSignature, String defaultEmblem, Boolean useEmblem, String defaultBackground, Boolean useBackground, String sponsorUrl, String sponsorBanner, String sponsorNote, Boolean useSponsor, String extraParams, Long expirationDuration) throws BaseException
    {
        return getUserSettingService().createUserSetting(user, homePage, selfBio, userUrlDomain, userUrlDomainNormalized, autoRedirectEnabled, viewEnabled, shareEnabled, defaultDomain, defaultTokenType, defaultRedirectType, defaultFlashDuration, defaultAccessType, defaultViewType, defaultShareType, defaultPassphrase, defaultSignature, useSignature, defaultEmblem, useEmblem, defaultBackground, useBackground, sponsorUrl, sponsorBanner, sponsorNote, useSponsor, extraParams, expirationDuration);
    }

    @Override
    public String createUserSetting(UserSetting userSetting) throws BaseException
    {
        return getUserSettingService().createUserSetting(userSetting);
    }

    @Override
    public Boolean updateUserSetting(String guid, String user, String homePage, String selfBio, String userUrlDomain, String userUrlDomainNormalized, Boolean autoRedirectEnabled, Boolean viewEnabled, Boolean shareEnabled, String defaultDomain, String defaultTokenType, String defaultRedirectType, Long defaultFlashDuration, String defaultAccessType, String defaultViewType, String defaultShareType, String defaultPassphrase, String defaultSignature, Boolean useSignature, String defaultEmblem, Boolean useEmblem, String defaultBackground, Boolean useBackground, String sponsorUrl, String sponsorBanner, String sponsorNote, Boolean useSponsor, String extraParams, Long expirationDuration) throws BaseException
    {
        return getUserSettingService().updateUserSetting(guid, user, homePage, selfBio, userUrlDomain, userUrlDomainNormalized, autoRedirectEnabled, viewEnabled, shareEnabled, defaultDomain, defaultTokenType, defaultRedirectType, defaultFlashDuration, defaultAccessType, defaultViewType, defaultShareType, defaultPassphrase, defaultSignature, useSignature, defaultEmblem, useEmblem, defaultBackground, useBackground, sponsorUrl, sponsorBanner, sponsorNote, useSponsor, extraParams, expirationDuration);
    }

    @Override
    public Boolean updateUserSetting(UserSetting userSetting) throws BaseException
    {
        return getUserSettingService().updateUserSetting(userSetting);
    }

    @Override
    public Boolean deleteUserSetting(String guid) throws BaseException
    {
        return getUserSettingService().deleteUserSetting(guid);
    }

    @Override
    public Boolean deleteUserSetting(UserSetting userSetting) throws BaseException
    {
        return getUserSettingService().deleteUserSetting(userSetting);
    }

    @Override
    public Long deleteUserSettings(String filter, String params, List<String> values) throws BaseException
    {
        return getUserSettingService().deleteUserSettings(filter, params, values);
    }

}
