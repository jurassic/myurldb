package com.cannyurl.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.UserUsercode;
import com.myurldb.ws.service.UserUsercodeService;
import com.cannyurl.af.proxy.UserUsercodeServiceProxy;

public class LocalUserUsercodeServiceProxy extends BaseLocalServiceProxy implements UserUsercodeServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalUserUsercodeServiceProxy.class.getName());

    public LocalUserUsercodeServiceProxy()
    {
    }

    @Override
    public UserUsercode getUserUsercode(String guid) throws BaseException
    {
        return getUserUsercodeService().getUserUsercode(guid);
    }

    @Override
    public Object getUserUsercode(String guid, String field) throws BaseException
    {
        return getUserUsercodeService().getUserUsercode(guid, field);       
    }

    @Override
    public List<UserUsercode> getUserUsercodes(List<String> guids) throws BaseException
    {
        return getUserUsercodeService().getUserUsercodes(guids);
    }

    @Override
    public List<UserUsercode> getAllUserUsercodes() throws BaseException
    {
        return getAllUserUsercodes(null, null, null);
    }

    @Override
    public List<UserUsercode> getAllUserUsercodes(String ordering, Long offset, Integer count) throws BaseException
    {
        return getUserUsercodeService().getAllUserUsercodes(ordering, offset, count);
    }

    @Override
    public List<String> getAllUserUsercodeKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getUserUsercodeService().getAllUserUsercodeKeys(ordering, offset, count);
    }

    @Override
    public List<UserUsercode> findUserUsercodes(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findUserUsercodes(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<UserUsercode> findUserUsercodes(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getUserUsercodeService().findUserUsercodes(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findUserUsercodeKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getUserUsercodeService().findUserUsercodeKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getUserUsercodeService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createUserUsercode(String admin, String user, String username, String usercode, String status) throws BaseException
    {
        return getUserUsercodeService().createUserUsercode(admin, user, username, usercode, status);
    }

    @Override
    public String createUserUsercode(UserUsercode userUsercode) throws BaseException
    {
        return getUserUsercodeService().createUserUsercode(userUsercode);
    }

    @Override
    public Boolean updateUserUsercode(String guid, String admin, String user, String username, String usercode, String status) throws BaseException
    {
        return getUserUsercodeService().updateUserUsercode(guid, admin, user, username, usercode, status);
    }

    @Override
    public Boolean updateUserUsercode(UserUsercode userUsercode) throws BaseException
    {
        return getUserUsercodeService().updateUserUsercode(userUsercode);
    }

    @Override
    public Boolean deleteUserUsercode(String guid) throws BaseException
    {
        return getUserUsercodeService().deleteUserUsercode(guid);
    }

    @Override
    public Boolean deleteUserUsercode(UserUsercode userUsercode) throws BaseException
    {
        return getUserUsercodeService().deleteUserUsercode(userUsercode);
    }

    @Override
    public Long deleteUserUsercodes(String filter, String params, List<String> values) throws BaseException
    {
        return getUserUsercodeService().deleteUserUsercodes(filter, params, values);
    }

}
