package com.cannyurl.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.VisitorSetting;
import com.myurldb.ws.service.VisitorSettingService;
import com.cannyurl.af.proxy.VisitorSettingServiceProxy;

public class LocalVisitorSettingServiceProxy extends BaseLocalServiceProxy implements VisitorSettingServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalVisitorSettingServiceProxy.class.getName());

    public LocalVisitorSettingServiceProxy()
    {
    }

    @Override
    public VisitorSetting getVisitorSetting(String guid) throws BaseException
    {
        return getVisitorSettingService().getVisitorSetting(guid);
    }

    @Override
    public Object getVisitorSetting(String guid, String field) throws BaseException
    {
        return getVisitorSettingService().getVisitorSetting(guid, field);       
    }

    @Override
    public List<VisitorSetting> getVisitorSettings(List<String> guids) throws BaseException
    {
        return getVisitorSettingService().getVisitorSettings(guids);
    }

    @Override
    public List<VisitorSetting> getAllVisitorSettings() throws BaseException
    {
        return getAllVisitorSettings(null, null, null);
    }

    @Override
    public List<VisitorSetting> getAllVisitorSettings(String ordering, Long offset, Integer count) throws BaseException
    {
        return getVisitorSettingService().getAllVisitorSettings(ordering, offset, count);
    }

    @Override
    public List<String> getAllVisitorSettingKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getVisitorSettingService().getAllVisitorSettingKeys(ordering, offset, count);
    }

    @Override
    public List<VisitorSetting> findVisitorSettings(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findVisitorSettings(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<VisitorSetting> findVisitorSettings(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getVisitorSettingService().findVisitorSettings(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findVisitorSettingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getVisitorSettingService().findVisitorSettingKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getVisitorSettingService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createVisitorSetting(String user, String redirectType, Long flashDuration, String privacyType) throws BaseException
    {
        return getVisitorSettingService().createVisitorSetting(user, redirectType, flashDuration, privacyType);
    }

    @Override
    public String createVisitorSetting(VisitorSetting visitorSetting) throws BaseException
    {
        return getVisitorSettingService().createVisitorSetting(visitorSetting);
    }

    @Override
    public Boolean updateVisitorSetting(String guid, String user, String redirectType, Long flashDuration, String privacyType) throws BaseException
    {
        return getVisitorSettingService().updateVisitorSetting(guid, user, redirectType, flashDuration, privacyType);
    }

    @Override
    public Boolean updateVisitorSetting(VisitorSetting visitorSetting) throws BaseException
    {
        return getVisitorSettingService().updateVisitorSetting(visitorSetting);
    }

    @Override
    public Boolean deleteVisitorSetting(String guid) throws BaseException
    {
        return getVisitorSettingService().deleteVisitorSetting(guid);
    }

    @Override
    public Boolean deleteVisitorSetting(VisitorSetting visitorSetting) throws BaseException
    {
        return getVisitorSettingService().deleteVisitorSetting(visitorSetting);
    }

    @Override
    public Long deleteVisitorSettings(String filter, String params, List<String> values) throws BaseException
    {
        return getVisitorSettingService().deleteVisitorSettings(filter, params, values);
    }

}
