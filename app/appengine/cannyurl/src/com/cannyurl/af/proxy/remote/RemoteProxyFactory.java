package com.cannyurl.af.proxy.remote;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.ApiConsumerServiceProxy;
import com.cannyurl.af.proxy.AppCustomDomainServiceProxy;
import com.cannyurl.af.proxy.SiteCustomDomainServiceProxy;
import com.cannyurl.af.proxy.UserServiceProxy;
import com.cannyurl.af.proxy.UserUsercodeServiceProxy;
import com.cannyurl.af.proxy.UserPasswordServiceProxy;
import com.cannyurl.af.proxy.ExternalUserAuthServiceProxy;
import com.cannyurl.af.proxy.UserAuthStateServiceProxy;
import com.cannyurl.af.proxy.UserResourcePermissionServiceProxy;
import com.cannyurl.af.proxy.UserResourceProhibitionServiceProxy;
import com.cannyurl.af.proxy.RolePermissionServiceProxy;
import com.cannyurl.af.proxy.UserRoleServiceProxy;
import com.cannyurl.af.proxy.AppClientServiceProxy;
import com.cannyurl.af.proxy.ClientUserServiceProxy;
import com.cannyurl.af.proxy.UserCustomDomainServiceProxy;
import com.cannyurl.af.proxy.ClientSettingServiceProxy;
import com.cannyurl.af.proxy.UserSettingServiceProxy;
import com.cannyurl.af.proxy.VisitorSettingServiceProxy;
import com.cannyurl.af.proxy.TwitterSummaryCardServiceProxy;
import com.cannyurl.af.proxy.TwitterPhotoCardServiceProxy;
import com.cannyurl.af.proxy.TwitterGalleryCardServiceProxy;
import com.cannyurl.af.proxy.TwitterAppCardServiceProxy;
import com.cannyurl.af.proxy.TwitterPlayerCardServiceProxy;
import com.cannyurl.af.proxy.TwitterProductCardServiceProxy;
import com.cannyurl.af.proxy.ShortPassageServiceProxy;
import com.cannyurl.af.proxy.ShortLinkServiceProxy;
import com.cannyurl.af.proxy.GeoLinkServiceProxy;
import com.cannyurl.af.proxy.QrCodeServiceProxy;
import com.cannyurl.af.proxy.LinkPassphraseServiceProxy;
import com.cannyurl.af.proxy.LinkMessageServiceProxy;
import com.cannyurl.af.proxy.LinkAlbumServiceProxy;
import com.cannyurl.af.proxy.AlbumShortLinkServiceProxy;
import com.cannyurl.af.proxy.KeywordFolderServiceProxy;
import com.cannyurl.af.proxy.BookmarkFolderServiceProxy;
import com.cannyurl.af.proxy.KeywordLinkServiceProxy;
import com.cannyurl.af.proxy.BookmarkLinkServiceProxy;
import com.cannyurl.af.proxy.SpeedDialServiceProxy;
import com.cannyurl.af.proxy.KeywordFolderImportServiceProxy;
import com.cannyurl.af.proxy.BookmarkFolderImportServiceProxy;
import com.cannyurl.af.proxy.KeywordCrowdTallyServiceProxy;
import com.cannyurl.af.proxy.BookmarkCrowdTallyServiceProxy;
import com.cannyurl.af.proxy.DomainInfoServiceProxy;
import com.cannyurl.af.proxy.UrlRatingServiceProxy;
import com.cannyurl.af.proxy.UserRatingServiceProxy;
import com.cannyurl.af.proxy.AbuseTagServiceProxy;
import com.cannyurl.af.proxy.ServiceInfoServiceProxy;
import com.cannyurl.af.proxy.FiveTenServiceProxy;

public class RemoteProxyFactory extends AbstractProxyFactory
{
    private static final Logger log = Logger.getLogger(RemoteProxyFactory.class.getName());

    private RemoteProxyFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class RemoteProxyFactoryHolder
    {
        private static final RemoteProxyFactory INSTANCE = new RemoteProxyFactory();
    }

    // Singleton method
    public static RemoteProxyFactory getInstance()
    {
        return RemoteProxyFactoryHolder.INSTANCE;
    }

    @Override
    public ApiConsumerServiceProxy getApiConsumerServiceProxy()
    {
        return new RemoteApiConsumerServiceProxy();
    }

    @Override
    public AppCustomDomainServiceProxy getAppCustomDomainServiceProxy()
    {
        return new RemoteAppCustomDomainServiceProxy();
    }

    @Override
    public SiteCustomDomainServiceProxy getSiteCustomDomainServiceProxy()
    {
        return new RemoteSiteCustomDomainServiceProxy();
    }

    @Override
    public UserServiceProxy getUserServiceProxy()
    {
        return new RemoteUserServiceProxy();
    }

    @Override
    public UserUsercodeServiceProxy getUserUsercodeServiceProxy()
    {
        return new RemoteUserUsercodeServiceProxy();
    }

    @Override
    public UserPasswordServiceProxy getUserPasswordServiceProxy()
    {
        return new RemoteUserPasswordServiceProxy();
    }

    @Override
    public ExternalUserAuthServiceProxy getExternalUserAuthServiceProxy()
    {
        return new RemoteExternalUserAuthServiceProxy();
    }

    @Override
    public UserAuthStateServiceProxy getUserAuthStateServiceProxy()
    {
        return new RemoteUserAuthStateServiceProxy();
    }

    @Override
    public UserResourcePermissionServiceProxy getUserResourcePermissionServiceProxy()
    {
        return new RemoteUserResourcePermissionServiceProxy();
    }

    @Override
    public UserResourceProhibitionServiceProxy getUserResourceProhibitionServiceProxy()
    {
        return new RemoteUserResourceProhibitionServiceProxy();
    }

    @Override
    public RolePermissionServiceProxy getRolePermissionServiceProxy()
    {
        return new RemoteRolePermissionServiceProxy();
    }

    @Override
    public UserRoleServiceProxy getUserRoleServiceProxy()
    {
        return new RemoteUserRoleServiceProxy();
    }

    @Override
    public AppClientServiceProxy getAppClientServiceProxy()
    {
        return new RemoteAppClientServiceProxy();
    }

    @Override
    public ClientUserServiceProxy getClientUserServiceProxy()
    {
        return new RemoteClientUserServiceProxy();
    }

    @Override
    public UserCustomDomainServiceProxy getUserCustomDomainServiceProxy()
    {
        return new RemoteUserCustomDomainServiceProxy();
    }

    @Override
    public ClientSettingServiceProxy getClientSettingServiceProxy()
    {
        return new RemoteClientSettingServiceProxy();
    }

    @Override
    public UserSettingServiceProxy getUserSettingServiceProxy()
    {
        return new RemoteUserSettingServiceProxy();
    }

    @Override
    public VisitorSettingServiceProxy getVisitorSettingServiceProxy()
    {
        return new RemoteVisitorSettingServiceProxy();
    }

    @Override
    public TwitterSummaryCardServiceProxy getTwitterSummaryCardServiceProxy()
    {
        return new RemoteTwitterSummaryCardServiceProxy();
    }

    @Override
    public TwitterPhotoCardServiceProxy getTwitterPhotoCardServiceProxy()
    {
        return new RemoteTwitterPhotoCardServiceProxy();
    }

    @Override
    public TwitterGalleryCardServiceProxy getTwitterGalleryCardServiceProxy()
    {
        return new RemoteTwitterGalleryCardServiceProxy();
    }

    @Override
    public TwitterAppCardServiceProxy getTwitterAppCardServiceProxy()
    {
        return new RemoteTwitterAppCardServiceProxy();
    }

    @Override
    public TwitterPlayerCardServiceProxy getTwitterPlayerCardServiceProxy()
    {
        return new RemoteTwitterPlayerCardServiceProxy();
    }

    @Override
    public TwitterProductCardServiceProxy getTwitterProductCardServiceProxy()
    {
        return new RemoteTwitterProductCardServiceProxy();
    }

    @Override
    public ShortPassageServiceProxy getShortPassageServiceProxy()
    {
        return new RemoteShortPassageServiceProxy();
    }

    @Override
    public ShortLinkServiceProxy getShortLinkServiceProxy()
    {
        return new RemoteShortLinkServiceProxy();
    }

    @Override
    public GeoLinkServiceProxy getGeoLinkServiceProxy()
    {
        return new RemoteGeoLinkServiceProxy();
    }

    @Override
    public QrCodeServiceProxy getQrCodeServiceProxy()
    {
        return new RemoteQrCodeServiceProxy();
    }

    @Override
    public LinkPassphraseServiceProxy getLinkPassphraseServiceProxy()
    {
        return new RemoteLinkPassphraseServiceProxy();
    }

    @Override
    public LinkMessageServiceProxy getLinkMessageServiceProxy()
    {
        return new RemoteLinkMessageServiceProxy();
    }

    @Override
    public LinkAlbumServiceProxy getLinkAlbumServiceProxy()
    {
        return new RemoteLinkAlbumServiceProxy();
    }

    @Override
    public AlbumShortLinkServiceProxy getAlbumShortLinkServiceProxy()
    {
        return new RemoteAlbumShortLinkServiceProxy();
    }

    @Override
    public KeywordFolderServiceProxy getKeywordFolderServiceProxy()
    {
        return new RemoteKeywordFolderServiceProxy();
    }

    @Override
    public BookmarkFolderServiceProxy getBookmarkFolderServiceProxy()
    {
        return new RemoteBookmarkFolderServiceProxy();
    }

    @Override
    public KeywordLinkServiceProxy getKeywordLinkServiceProxy()
    {
        return new RemoteKeywordLinkServiceProxy();
    }

    @Override
    public BookmarkLinkServiceProxy getBookmarkLinkServiceProxy()
    {
        return new RemoteBookmarkLinkServiceProxy();
    }

    @Override
    public SpeedDialServiceProxy getSpeedDialServiceProxy()
    {
        return new RemoteSpeedDialServiceProxy();
    }

    @Override
    public KeywordFolderImportServiceProxy getKeywordFolderImportServiceProxy()
    {
        return new RemoteKeywordFolderImportServiceProxy();
    }

    @Override
    public BookmarkFolderImportServiceProxy getBookmarkFolderImportServiceProxy()
    {
        return new RemoteBookmarkFolderImportServiceProxy();
    }

    @Override
    public KeywordCrowdTallyServiceProxy getKeywordCrowdTallyServiceProxy()
    {
        return new RemoteKeywordCrowdTallyServiceProxy();
    }

    @Override
    public BookmarkCrowdTallyServiceProxy getBookmarkCrowdTallyServiceProxy()
    {
        return new RemoteBookmarkCrowdTallyServiceProxy();
    }

    @Override
    public DomainInfoServiceProxy getDomainInfoServiceProxy()
    {
        return new RemoteDomainInfoServiceProxy();
    }

    @Override
    public UrlRatingServiceProxy getUrlRatingServiceProxy()
    {
        return new RemoteUrlRatingServiceProxy();
    }

    @Override
    public UserRatingServiceProxy getUserRatingServiceProxy()
    {
        return new RemoteUserRatingServiceProxy();
    }

    @Override
    public AbuseTagServiceProxy getAbuseTagServiceProxy()
    {
        return new RemoteAbuseTagServiceProxy();
    }

    @Override
    public ServiceInfoServiceProxy getServiceInfoServiceProxy()
    {
        return new RemoteServiceInfoServiceProxy();
    }

    @Override
    public FiveTenServiceProxy getFiveTenServiceProxy()
    {
        return new RemoteFiveTenServiceProxy();
    }

}
