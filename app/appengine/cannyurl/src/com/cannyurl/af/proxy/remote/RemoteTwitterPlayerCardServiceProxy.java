package com.cannyurl.af.proxy.remote;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheEntry;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.ServiceUnavailableException;
import com.myurldb.ws.exception.NotImplementedException;
import com.myurldb.ws.core.StatusCode;
import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.TwitterPlayerCard;
import com.myurldb.ws.stub.ErrorStub;
import com.myurldb.ws.stub.KeyListStub;
import com.myurldb.ws.stub.TwitterCardAppInfoStub;
import com.myurldb.ws.stub.TwitterCardAppInfoListStub;
import com.myurldb.ws.stub.TwitterCardProductDataStub;
import com.myurldb.ws.stub.TwitterCardProductDataListStub;
import com.myurldb.ws.stub.TwitterPlayerCardStub;
import com.myurldb.ws.stub.TwitterPlayerCardListStub;
import com.cannyurl.af.auth.TwoLeggedOAuthClientUtil;
import com.cannyurl.af.util.MarshalHelper;
import com.cannyurl.af.util.StringUtil;
import com.cannyurl.af.bean.TwitterCardAppInfoBean;
import com.cannyurl.af.bean.TwitterCardProductDataBean;
import com.cannyurl.af.bean.TwitterPlayerCardBean;
import com.cannyurl.af.proxy.TwitterPlayerCardServiceProxy;

import com.cannyurl.af.config.Config;


// Note on retries, upon arbitrary app engine urlfetch timeout (5 secs):
// In our current implementation, create (POST) and update (PUT) have essentially the same semantics.
// If an object with the client-supplied guid does not exist in the data store, it will be created.
// If an object with the given guid already exists, it will be replaced with the supplied object.
// The only difference is, the update operation (PUT) will modify the modifiedTime field.
// (Note: Ccreate does not require guid (pk) although we will generally try to supply guid from the client side.)
// Therefore, retries are relatively safe for POST and PUT.
// GET is by definition idempotent, and hence retry is safe.
// DELETE, on the other hand, will likely cause an error if the same request is repeated.
// The consequence of this is that, a user may be told there was an error deleting an object
// when in fact the object was already deleted during the retry sequence.
public class RemoteTwitterPlayerCardServiceProxy extends BaseRemoteServiceProxy implements TwitterPlayerCardServiceProxy
{
    private static final Logger log = Logger.getLogger(RemoteTwitterPlayerCardServiceProxy.class.getName());

    // TBD: Ths resource name should match the path specified in the corresponding ws.resource.impl class.
    private final static String RESOURCE_TWITTERPLAYERCARD = "twitterPlayerCards";


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    protected Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }

    public RemoteTwitterPlayerCardServiceProxy()
    {
        initCache();
    }


    protected WebResource getTwitterPlayerCardWebResource()
    {
        return getWebResource(RESOURCE_TWITTERPLAYERCARD);
    }
    protected WebResource getTwitterPlayerCardWebResource(String path)
    {
        return getWebResource(RESOURCE_TWITTERPLAYERCARD, path);
    }
    protected WebResource getTwitterPlayerCardWebResourceByGuid(String guid)
    {
        return getTwitterPlayerCardWebResource(guid);
    }


    @Override
    public TwitterPlayerCard getTwitterPlayerCard(String guid) throws BaseException
    {
        log.finer("BEGIN");

        TwitterPlayerCard bean = null;

        String key = getResourcePath(RESOURCE_TWITTERPLAYERCARD, guid);
        CacheEntry entry = null;
        if(mCache != null) {
            entry = mCache.getCacheEntry(key);
            if(entry != null) {
                // TBD: eTag, lastModified, expires, etc....
            }
        }

        WebResource webResource = getTwitterPlayerCardWebResource(guid);

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = null;
        int numTries = 0;
        while(clientResponse == null && numTries++ < MAX_RETRIES) {
            try {
 	            clientResponse = webResource.accept(getOutputMediaType()).get(ClientResponse.class);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Request failed (" + numTries + ").", ex);
            }
        }
        if(clientResponse == null) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Request failed after " + MAX_RETRIES + " tries: bean = " + bean);
            throw new ServiceUnavailableException("Request failed after " + MAX_RETRIES + " tries: bean = " + bean);
        }

        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                TwitterPlayerCardStub stub = clientResponse.getEntity(TwitterPlayerCardStub.class);
                bean = MarshalHelper.convertTwitterPlayerCardToBean(stub);
                if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "TwitterPlayerCard bean = " + bean);
                break;
            // case StatusCode.NOT_MODIFIED:
            //     // TBD
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_TWITTERPLAYERCARD);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getTwitterPlayerCard(String guid, String field) throws BaseException
    {
        TwitterPlayerCard bean = getTwitterPlayerCard(guid);
        if(bean == null) {
            return null;
        }

        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("card")) {
            return bean.getCard();
        } else if(field.equals("url")) {
            return bean.getUrl();
        } else if(field.equals("title")) {
            return bean.getTitle();
        } else if(field.equals("description")) {
            return bean.getDescription();
        } else if(field.equals("site")) {
            return bean.getSite();
        } else if(field.equals("siteId")) {
            return bean.getSiteId();
        } else if(field.equals("creator")) {
            return bean.getCreator();
        } else if(field.equals("creatorId")) {
            return bean.getCreatorId();
        } else if(field.equals("image")) {
            return bean.getImage();
        } else if(field.equals("imageWidth")) {
            return bean.getImageWidth();
        } else if(field.equals("imageHeight")) {
            return bean.getImageHeight();
        } else if(field.equals("player")) {
            return bean.getPlayer();
        } else if(field.equals("playerWidth")) {
            return bean.getPlayerWidth();
        } else if(field.equals("playerHeight")) {
            return bean.getPlayerHeight();
        } else if(field.equals("playerStream")) {
            return bean.getPlayerStream();
        } else if(field.equals("playerStreamContentType")) {
            return bean.getPlayerStreamContentType();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<TwitterPlayerCard> getTwitterPlayerCards(List<String> guids) throws BaseException
    {
        // Temporary implementation
        if(guids == null || guids.isEmpty()) {
            return null;
        }
        List<TwitterPlayerCard> beans = new ArrayList<TwitterPlayerCard>();
        for(String guid : guids) {
            TwitterPlayerCard bean = getTwitterPlayerCard(guid);
            beans.add(bean);
        }
        return beans;
    }

    @Override
    public List<TwitterPlayerCard> getAllTwitterPlayerCards() throws BaseException
    {
        return getAllTwitterPlayerCards(null, null, null);
    }

    @Override
    public List<TwitterPlayerCard> getAllTwitterPlayerCards(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

    	List<TwitterPlayerCard> list = null;

     	WebResource webResource = getTwitterPlayerCardWebResource(RESOURCE_PATH_ALL);
        if(ordering != null && !ordering.isEmpty()) {
            webResource = webResource.queryParam("ordering", ordering);
        }
        if(offset != null) {
            webResource = webResource.queryParam("offset", offset.toString());
        }
        if(count != null) {
            webResource = webResource.queryParam("count", count.toString());
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

    	ClientResponse clientResponse = null;
        int numTries = 0;
        while(clientResponse == null && numTries++ < MAX_RETRIES) {
            try {
                clientResponse = webResource.accept(getOutputMediaType()).get(ClientResponse.class);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Request failed (" + numTries + ").", ex);
            }
        }
        if(clientResponse == null) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Request failed after " + MAX_RETRIES + " tries.");
            throw new ServiceUnavailableException("Request failed after " + MAX_RETRIES + " tries.");
        }
    	
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                TwitterPlayerCardListStub stub = clientResponse.getEntity(TwitterPlayerCardListStub.class);
                list = MarshalHelper.convertTwitterPlayerCardListStubToBeanList(stub);
                if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "TwitterPlayerCard list = " + list);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_TWITTERPLAYERCARD);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllTwitterPlayerCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

    	List<String> list = null;

     	WebResource webResource = getTwitterPlayerCardWebResource(RESOURCE_PATH_ALLKEYS);
        if(ordering != null && !ordering.isEmpty()) {
            webResource = webResource.queryParam("ordering", ordering);
        }
        if(offset != null) {
            webResource = webResource.queryParam("offset", offset.toString());
        }
        if(count != null) {
            webResource = webResource.queryParam("count", count.toString());
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

    	ClientResponse clientResponse = null;
        int numTries = 0;
        while(clientResponse == null && numTries++ < MAX_RETRIES) {
            try {
                clientResponse = webResource.accept(getOutputMediaType()).get(ClientResponse.class);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Request failed (" + numTries + ").", ex);
            }
        }
        if(clientResponse == null) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Request failed after " + MAX_RETRIES + " tries.");
            throw new ServiceUnavailableException("Request failed after " + MAX_RETRIES + " tries.");
        }
    	
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                KeyListStub stub = clientResponse.getEntity(KeyListStub.class);
                list = stub.getList();
                if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "TwitterPlayerCard key list = " + list);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_TWITTERPLAYERCARD);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<TwitterPlayerCard> findTwitterPlayerCards(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findTwitterPlayerCards(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<TwitterPlayerCard> findTwitterPlayerCards(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

    	List<TwitterPlayerCard> list = null;
    	
//        ClientResponse clientResponse = getTwitterPlayerCardWebResource()
//        	.queryParam("filter", filter)
//        	.queryParam("ordering", ordering)
//        	.queryParam("params", params)
//        	//.queryParam("values", values)  // ???
//        	.accept(getOutputMediaType())
//        	.get(ClientResponse.class);

    	WebResource webResource = getTwitterPlayerCardWebResource();
    	if(filter == null || filter.isEmpty()) {
    	    // Should we allow empty filter????
    	    // It just returns all objects.... (possibly, ordered if ordering is not empty).
    	} else {
    	    webResource = webResource.queryParam("filter", filter);
    	}
        if(ordering != null && !ordering.isEmpty()) {
            webResource = webResource.queryParam("ordering", ordering);
        }
        if(filter != null && !filter.isEmpty()) {
            // params/values make sense only if filter is set.
            if(params != null && !params.isEmpty()) {
                webResource = webResource.queryParam("params", params);
            }
        	if(values != null) {
                // TBD: Is the order of "values" preserved????
                // If not, we'll need to change the interface of the findXXX() method.
        	    for(String v : values) {
        	        webResource = webResource.queryParam("values", v);
        	    }
        	}
        }
        if(grouping != null && !grouping.isEmpty()) {
            webResource = webResource.queryParam("grouping", grouping);
        }
        if(unique != null) {
            webResource = webResource.queryParam("unique", unique.toString());
        }
        if(offset != null) {
            webResource = webResource.queryParam("offset", offset.toString());
        }
        if(count != null) {
            webResource = webResource.queryParam("count", count.toString());
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = null;
        int numTries = 0;
        while(clientResponse == null && numTries++ < MAX_RETRIES) {
            try {
                clientResponse = webResource.accept(getOutputMediaType()).get(ClientResponse.class);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Request failed (" + numTries + ").", ex);
            }
        }
        if(clientResponse == null) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Request failed after " + MAX_RETRIES + " tries.");
            throw new ServiceUnavailableException("Request failed after " + MAX_RETRIES + " tries.");
        }
        
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                TwitterPlayerCardListStub stub = clientResponse.getEntity(TwitterPlayerCardListStub.class);
                list = MarshalHelper.convertTwitterPlayerCardListStubToBeanList(stub);
                if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "TwitterPlayerCard list = " + list);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_TWITTERPLAYERCARD);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findTwitterPlayerCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

    	List<String> list = null;

    	WebResource webResource = getTwitterPlayerCardWebResource(RESOURCE_PATH_KEYS);
    	if(filter == null || filter.isEmpty()) {
    	    // Should we allow empty filter????
    	    // It just returns all objects.... (possibly, ordered if ordering is not empty).
    	} else {
    	    webResource = webResource.queryParam("filter", filter);
    	}
        if(ordering != null && !ordering.isEmpty()) {
            webResource = webResource.queryParam("ordering", ordering);
        }
        if(filter != null && !filter.isEmpty()) {
            // params/values make sense only if filter is set.
            if(params != null && !params.isEmpty()) {
                webResource = webResource.queryParam("params", params);
            }
        	if(values != null) {
                // TBD: Is the order of "values" preserved????
                // If not, we'll need to change the interface of the findXXX() method.
        	    for(String v : values) {
        	        webResource = webResource.queryParam("values", v);
        	    }
        	}
        }
        if(grouping != null && !grouping.isEmpty()) {
            webResource = webResource.queryParam("grouping", grouping);
        }
        if(unique != null) {
            webResource = webResource.queryParam("unique", unique.toString());
        }
        if(offset != null) {
            webResource = webResource.queryParam("offset", offset.toString());
        }
        if(count != null) {
            webResource = webResource.queryParam("count", count.toString());
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = null;
        int numTries = 0;
        while(clientResponse == null && numTries++ < MAX_RETRIES) {
            try {
                clientResponse = webResource.accept(getOutputMediaType()).get(ClientResponse.class);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Request failed (" + numTries + ").", ex);
            }
        }
        if(clientResponse == null) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Request failed after " + MAX_RETRIES + " tries.");
            throw new ServiceUnavailableException("Request failed after " + MAX_RETRIES + " tries.");
        }
        
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                KeyListStub stub = clientResponse.getEntity(KeyListStub.class);
                list = stub.getList();
                if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "TwitterPlayerCard key list = " + list);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_TWITTERPLAYERCARD);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }

        log.finer("END");
        return list;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        log.finer("BEGIN");

        Long count = 0L;
     	WebResource webResource = getTwitterPlayerCardWebResource(RESOURCE_PATH_COUNT);
    	if(filter == null || filter.isEmpty()) {
    	    // Should we allow empty filter????
    	    // It just returns all objects.... (possibly, ordered if ordering is not empty).
    	} else {
    	    webResource = webResource.queryParam("filter", filter);
    	}
        if(filter != null && !filter.isEmpty()) {
            // params/values make sense only if filter is set.
            if(params != null && !params.isEmpty()) {
                webResource = webResource.queryParam("params", params);
            }
        	if(values != null) {
                // TBD: Is the order of "values" preserved????
                // If not, we'll need to change the interface of the findXXX() method.
        	    for(String v : values) {
        	        webResource = webResource.queryParam("values", v);
        	    }
        	}
        }
        if(aggregate != null) {
            webResource = webResource.queryParam("aggregate", aggregate);
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = null;
        int numTries = 0;
        while(clientResponse == null && numTries++ < MAX_RETRIES) {
            try {
                // Order of the mime type ???
                //clientResponse = webResource.accept(MediaType.TEXT_PLAIN, MediaType.APPLICATION_XML).get(ClientResponse.class);
                clientResponse = webResource.accept(MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN).get(ClientResponse.class);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Request failed (" + numTries + ").", ex);
            }
        }
        if(clientResponse == null) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Request failed after " + MAX_RETRIES + " tries.");
            throw new ServiceUnavailableException("Request failed after " + MAX_RETRIES + " tries.");
        }
        
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                count = Long.parseLong(clientResponse.getEntity(String.class));
                if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "TwitterPlayerCard count = " + count);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);  // ???
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_TWITTERPLAYERCARD);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
 
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createTwitterPlayerCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, String player, Integer playerWidth, Integer playerHeight, String playerStream, String playerStreamContentType) throws BaseException
    {
        TwitterPlayerCardBean bean = new TwitterPlayerCardBean(null, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, player, playerWidth, playerHeight, playerStream, playerStreamContentType);
        return createTwitterPlayerCard(bean);        
    }

    @Override
    public String createTwitterPlayerCard(TwitterPlayerCard bean) throws BaseException
    {
        log.finer("BEGIN");

        String guid = null;
        TwitterPlayerCardStub stub = MarshalHelper.convertTwitterPlayerCardToStub(bean);
        WebResource webResource = getTwitterPlayerCardWebResource();

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = null;
        int numTries = 0;
        while(clientResponse == null && numTries++ < MAX_RETRIES) {
            try {
                clientResponse = webResource.type(getInputMediaType()).post(ClientResponse.class, stub);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Request failed (" + numTries + ").", ex);
            }
        }
        if(clientResponse == null) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Request failed after " + MAX_RETRIES + " tries: bean = " + bean);
            throw new ServiceUnavailableException("Request failed after " + MAX_RETRIES + " tries: bean = " + bean);
        }

        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.CREATED:
            case StatusCode.ACCEPTED:
                guid = clientResponse.getEntity(String.class);
                if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "New TwitterPlayerCard guid = " + guid);
                String createdUri = clientResponse.getLocation().toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "New TwitterPlayerCard resource uri = " + createdUri);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_TWITTERPLAYERCARD);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateTwitterPlayerCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, String player, Integer playerWidth, Integer playerHeight, String playerStream, String playerStreamContentType) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "TwitterPlayerCard guid is invalid.");
        	throw new BaseException("TwitterPlayerCard guid is invalid.");
        }
        TwitterPlayerCardBean bean = new TwitterPlayerCardBean(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, player, playerWidth, playerHeight, playerStream, playerStreamContentType);
        return updateTwitterPlayerCard(bean);        
    }

    @Override
    public Boolean updateTwitterPlayerCard(TwitterPlayerCard bean) throws BaseException
    {
        log.finer("BEGIN");

        String guid = bean.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "TwitterPlayerCard object is invalid.");
        	throw new BaseException("TwitterPlayerCard object is invalid.");
        }
        TwitterPlayerCardStub stub = MarshalHelper.convertTwitterPlayerCardToStub(bean);

        WebResource webResource = getTwitterPlayerCardWebResource(guid);

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);
        
        ClientResponse clientResponse = null;
        int numTries = 0;
        while(clientResponse == null && numTries++ < MAX_RETRIES) {
            try {
                clientResponse = webResource.type(getInputMediaType()).put(ClientResponse.class, stub);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Request failed (" + numTries + ").", ex);
            }
        }
        if(clientResponse == null) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Request failed after " + MAX_RETRIES + " tries: bean = " + bean);
            throw new ServiceUnavailableException("Request failed after " + MAX_RETRIES + " tries: bean = " + bean);
        }

        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
            case StatusCode.ACCEPTED:
            case StatusCode.NO_CONTENT:
            case StatusCode.RESET_CONTENT:
                if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Successfully updated the object with guid = " + guid);
                return true;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_TWITTERPLAYERCARD);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }

        log.finer("END: false");
        return false;
    }

    @Override
    public Boolean deleteTwitterPlayerCard(String guid) throws BaseException
    {
        log.finer("BEGIN");

        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "TwitterPlayerCard guid is invalid.");
        	throw new BaseException("TwitterPlayerCard guid is invalid.");
        }

        WebResource webResource = getTwitterPlayerCardWebResource(guid);

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = null;
        int numTries = 0;
        while(clientResponse == null && numTries++ < MAX_RETRIES) {
            try {
                clientResponse = webResource.delete(ClientResponse.class);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Request failed (" + numTries + ").", ex);
            }
        }
        if(clientResponse == null) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Request failed after " + MAX_RETRIES + " tries: guid = " + guid);
            throw new ServiceUnavailableException("Request failed after " + MAX_RETRIES + " tries: guid = " + guid);
        }

        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
            case StatusCode.ACCEPTED:
            case StatusCode.NO_CONTENT:
            case StatusCode.RESET_CONTENT:
                if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Successfully deleted the object with guid = " + guid);
                return true;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_TWITTERPLAYERCARD);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }

        log.finer("END: false");
        return false;
    }

    @Override
    public Boolean deleteTwitterPlayerCard(TwitterPlayerCard bean) throws BaseException
    {
        String guid = bean.getGuid();
        return deleteTwitterPlayerCard(guid);
    }

    @Override
    public Long deleteTwitterPlayerCards(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = 0L;
     	WebResource webResource = getTwitterPlayerCardWebResource();
    	if(filter == null || filter.isEmpty()) {
    	    // Should we allow empty filter????
    	    // Currently, the data access layer throws exceptionif the filter is empty.
    	} else {
    	    webResource = webResource.queryParam("filter", filter);
    	}
        if(filter != null && !filter.isEmpty()) {
            // params/values make sense only if filter is set.
            if(params != null && !params.isEmpty()) {
                webResource = webResource.queryParam("params", params);
            }
        	if(values != null) {
                // TBD: Is the order of "values" preserved????
                // If not, we'll need to change the interface. Or, try comma-separated list???
        	    for(String v : values) {
        	        webResource = webResource.queryParam("values", v);
        	    }
        	}
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = null;
        int numTries = 0;
        while(clientResponse == null && numTries++ < MAX_RETRIES) {
            try {
                // Order of the mime types ???
                clientResponse = webResource.accept(MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN).delete(ClientResponse.class);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Request failed (" + numTries + ").", ex);
            }
        }
        if(clientResponse == null) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Request failed after " + MAX_RETRIES + " tries: delete-filter = " + filter);
            throw new ServiceUnavailableException("Request failed after " + MAX_RETRIES + " tries: delete-filter = " + filter);
        }

        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
            case StatusCode.ACCEPTED:
                count = Long.parseLong(clientResponse.getEntity(String.class));
                if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Deleted TwitterPlayerCards: count = " + count);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class); // ???
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_TWITTERPLAYERCARD);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
