package com.cannyurl.af.proxy.remote;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheEntry;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.ServiceUnavailableException;
import com.myurldb.ws.exception.NotImplementedException;
import com.myurldb.ws.core.StatusCode;
import com.myurldb.ws.VisitorSetting;
import com.myurldb.ws.stub.ErrorStub;
import com.myurldb.ws.stub.KeyListStub;
import com.myurldb.ws.stub.VisitorSettingStub;
import com.myurldb.ws.stub.VisitorSettingListStub;
import com.cannyurl.af.auth.TwoLeggedOAuthClientUtil;
import com.cannyurl.af.util.MarshalHelper;
import com.cannyurl.af.util.StringUtil;
import com.cannyurl.af.bean.VisitorSettingBean;
import com.cannyurl.af.proxy.VisitorSettingServiceProxy;

import com.cannyurl.af.config.Config;


// Note on retries, upon arbitrary app engine urlfetch timeout (5 secs):
// In our current implementation, create (POST) and update (PUT) have essentially the same semantics.
// If an object with the client-supplied guid does not exist in the data store, it will be created.
// If an object with the given guid already exists, it will be replaced with the supplied object.
// The only difference is, the update operation (PUT) will modify the modifiedTime field.
// (Note: Ccreate does not require guid (pk) although we will generally try to supply guid from the client side.)
// Therefore, retries are relatively safe for POST and PUT.
// GET is by definition idempotent, and hence retry is safe.
// DELETE, on the other hand, will likely cause an error if the same request is repeated.
// The consequence of this is that, a user may be told there was an error deleting an object
// when in fact the object was already deleted during the retry sequence.
public class RemoteVisitorSettingServiceProxy extends BaseRemoteServiceProxy implements VisitorSettingServiceProxy
{
    private static final Logger log = Logger.getLogger(RemoteVisitorSettingServiceProxy.class.getName());

    // TBD: Ths resource name should match the path specified in the corresponding ws.resource.impl class.
    private final static String RESOURCE_VISITORSETTING = "visitorSettings";


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    protected Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }

    public RemoteVisitorSettingServiceProxy()
    {
        initCache();
    }


    protected WebResource getVisitorSettingWebResource()
    {
        return getWebResource(RESOURCE_VISITORSETTING);
    }
    protected WebResource getVisitorSettingWebResource(String path)
    {
        return getWebResource(RESOURCE_VISITORSETTING, path);
    }
    protected WebResource getVisitorSettingWebResourceByGuid(String guid)
    {
        return getVisitorSettingWebResource(guid);
    }


    @Override
    public VisitorSetting getVisitorSetting(String guid) throws BaseException
    {
        log.finer("BEGIN");

        VisitorSetting bean = null;

        String key = getResourcePath(RESOURCE_VISITORSETTING, guid);
        CacheEntry entry = null;
        if(mCache != null) {
            entry = mCache.getCacheEntry(key);
            if(entry != null) {
                // TBD: eTag, lastModified, expires, etc....
            }
        }

        WebResource webResource = getVisitorSettingWebResource(guid);

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = null;
        int numTries = 0;
        while(clientResponse == null && numTries++ < MAX_RETRIES) {
            try {
 	            clientResponse = webResource.accept(getOutputMediaType()).get(ClientResponse.class);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Request failed (" + numTries + ").", ex);
            }
        }
        if(clientResponse == null) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Request failed after " + MAX_RETRIES + " tries: bean = " + bean);
            throw new ServiceUnavailableException("Request failed after " + MAX_RETRIES + " tries: bean = " + bean);
        }

        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                VisitorSettingStub stub = clientResponse.getEntity(VisitorSettingStub.class);
                bean = MarshalHelper.convertVisitorSettingToBean(stub);
                if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "VisitorSetting bean = " + bean);
                break;
            // case StatusCode.NOT_MODIFIED:
            //     // TBD
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_VISITORSETTING);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getVisitorSetting(String guid, String field) throws BaseException
    {
        VisitorSetting bean = getVisitorSetting(guid);
        if(bean == null) {
            return null;
        }

        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return bean.getUser();
        } else if(field.equals("redirectType")) {
            return bean.getRedirectType();
        } else if(field.equals("flashDuration")) {
            return bean.getFlashDuration();
        } else if(field.equals("privacyType")) {
            return bean.getPrivacyType();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<VisitorSetting> getVisitorSettings(List<String> guids) throws BaseException
    {
        // Temporary implementation
        if(guids == null || guids.isEmpty()) {
            return null;
        }
        List<VisitorSetting> beans = new ArrayList<VisitorSetting>();
        for(String guid : guids) {
            VisitorSetting bean = getVisitorSetting(guid);
            beans.add(bean);
        }
        return beans;
    }

    @Override
    public List<VisitorSetting> getAllVisitorSettings() throws BaseException
    {
        return getAllVisitorSettings(null, null, null);
    }

    @Override
    public List<VisitorSetting> getAllVisitorSettings(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

    	List<VisitorSetting> list = null;

     	WebResource webResource = getVisitorSettingWebResource(RESOURCE_PATH_ALL);
        if(ordering != null && !ordering.isEmpty()) {
            webResource = webResource.queryParam("ordering", ordering);
        }
        if(offset != null) {
            webResource = webResource.queryParam("offset", offset.toString());
        }
        if(count != null) {
            webResource = webResource.queryParam("count", count.toString());
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

    	ClientResponse clientResponse = null;
        int numTries = 0;
        while(clientResponse == null && numTries++ < MAX_RETRIES) {
            try {
                clientResponse = webResource.accept(getOutputMediaType()).get(ClientResponse.class);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Request failed (" + numTries + ").", ex);
            }
        }
        if(clientResponse == null) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Request failed after " + MAX_RETRIES + " tries.");
            throw new ServiceUnavailableException("Request failed after " + MAX_RETRIES + " tries.");
        }
    	
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                VisitorSettingListStub stub = clientResponse.getEntity(VisitorSettingListStub.class);
                list = MarshalHelper.convertVisitorSettingListStubToBeanList(stub);
                if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "VisitorSetting list = " + list);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_VISITORSETTING);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllVisitorSettingKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

    	List<String> list = null;

     	WebResource webResource = getVisitorSettingWebResource(RESOURCE_PATH_ALLKEYS);
        if(ordering != null && !ordering.isEmpty()) {
            webResource = webResource.queryParam("ordering", ordering);
        }
        if(offset != null) {
            webResource = webResource.queryParam("offset", offset.toString());
        }
        if(count != null) {
            webResource = webResource.queryParam("count", count.toString());
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

    	ClientResponse clientResponse = null;
        int numTries = 0;
        while(clientResponse == null && numTries++ < MAX_RETRIES) {
            try {
                clientResponse = webResource.accept(getOutputMediaType()).get(ClientResponse.class);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Request failed (" + numTries + ").", ex);
            }
        }
        if(clientResponse == null) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Request failed after " + MAX_RETRIES + " tries.");
            throw new ServiceUnavailableException("Request failed after " + MAX_RETRIES + " tries.");
        }
    	
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                KeyListStub stub = clientResponse.getEntity(KeyListStub.class);
                list = stub.getList();
                if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "VisitorSetting key list = " + list);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_VISITORSETTING);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<VisitorSetting> findVisitorSettings(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findVisitorSettings(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<VisitorSetting> findVisitorSettings(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

    	List<VisitorSetting> list = null;
    	
//        ClientResponse clientResponse = getVisitorSettingWebResource()
//        	.queryParam("filter", filter)
//        	.queryParam("ordering", ordering)
//        	.queryParam("params", params)
//        	//.queryParam("values", values)  // ???
//        	.accept(getOutputMediaType())
//        	.get(ClientResponse.class);

    	WebResource webResource = getVisitorSettingWebResource();
    	if(filter == null || filter.isEmpty()) {
    	    // Should we allow empty filter????
    	    // It just returns all objects.... (possibly, ordered if ordering is not empty).
    	} else {
    	    webResource = webResource.queryParam("filter", filter);
    	}
        if(ordering != null && !ordering.isEmpty()) {
            webResource = webResource.queryParam("ordering", ordering);
        }
        if(filter != null && !filter.isEmpty()) {
            // params/values make sense only if filter is set.
            if(params != null && !params.isEmpty()) {
                webResource = webResource.queryParam("params", params);
            }
        	if(values != null) {
                // TBD: Is the order of "values" preserved????
                // If not, we'll need to change the interface of the findXXX() method.
        	    for(String v : values) {
        	        webResource = webResource.queryParam("values", v);
        	    }
        	}
        }
        if(grouping != null && !grouping.isEmpty()) {
            webResource = webResource.queryParam("grouping", grouping);
        }
        if(unique != null) {
            webResource = webResource.queryParam("unique", unique.toString());
        }
        if(offset != null) {
            webResource = webResource.queryParam("offset", offset.toString());
        }
        if(count != null) {
            webResource = webResource.queryParam("count", count.toString());
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = null;
        int numTries = 0;
        while(clientResponse == null && numTries++ < MAX_RETRIES) {
            try {
                clientResponse = webResource.accept(getOutputMediaType()).get(ClientResponse.class);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Request failed (" + numTries + ").", ex);
            }
        }
        if(clientResponse == null) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Request failed after " + MAX_RETRIES + " tries.");
            throw new ServiceUnavailableException("Request failed after " + MAX_RETRIES + " tries.");
        }
        
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                VisitorSettingListStub stub = clientResponse.getEntity(VisitorSettingListStub.class);
                list = MarshalHelper.convertVisitorSettingListStubToBeanList(stub);
                if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "VisitorSetting list = " + list);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_VISITORSETTING);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findVisitorSettingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

    	List<String> list = null;

    	WebResource webResource = getVisitorSettingWebResource(RESOURCE_PATH_KEYS);
    	if(filter == null || filter.isEmpty()) {
    	    // Should we allow empty filter????
    	    // It just returns all objects.... (possibly, ordered if ordering is not empty).
    	} else {
    	    webResource = webResource.queryParam("filter", filter);
    	}
        if(ordering != null && !ordering.isEmpty()) {
            webResource = webResource.queryParam("ordering", ordering);
        }
        if(filter != null && !filter.isEmpty()) {
            // params/values make sense only if filter is set.
            if(params != null && !params.isEmpty()) {
                webResource = webResource.queryParam("params", params);
            }
        	if(values != null) {
                // TBD: Is the order of "values" preserved????
                // If not, we'll need to change the interface of the findXXX() method.
        	    for(String v : values) {
        	        webResource = webResource.queryParam("values", v);
        	    }
        	}
        }
        if(grouping != null && !grouping.isEmpty()) {
            webResource = webResource.queryParam("grouping", grouping);
        }
        if(unique != null) {
            webResource = webResource.queryParam("unique", unique.toString());
        }
        if(offset != null) {
            webResource = webResource.queryParam("offset", offset.toString());
        }
        if(count != null) {
            webResource = webResource.queryParam("count", count.toString());
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = null;
        int numTries = 0;
        while(clientResponse == null && numTries++ < MAX_RETRIES) {
            try {
                clientResponse = webResource.accept(getOutputMediaType()).get(ClientResponse.class);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Request failed (" + numTries + ").", ex);
            }
        }
        if(clientResponse == null) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Request failed after " + MAX_RETRIES + " tries.");
            throw new ServiceUnavailableException("Request failed after " + MAX_RETRIES + " tries.");
        }
        
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                KeyListStub stub = clientResponse.getEntity(KeyListStub.class);
                list = stub.getList();
                if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "VisitorSetting key list = " + list);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_VISITORSETTING);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }

        log.finer("END");
        return list;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        log.finer("BEGIN");

        Long count = 0L;
     	WebResource webResource = getVisitorSettingWebResource(RESOURCE_PATH_COUNT);
    	if(filter == null || filter.isEmpty()) {
    	    // Should we allow empty filter????
    	    // It just returns all objects.... (possibly, ordered if ordering is not empty).
    	} else {
    	    webResource = webResource.queryParam("filter", filter);
    	}
        if(filter != null && !filter.isEmpty()) {
            // params/values make sense only if filter is set.
            if(params != null && !params.isEmpty()) {
                webResource = webResource.queryParam("params", params);
            }
        	if(values != null) {
                // TBD: Is the order of "values" preserved????
                // If not, we'll need to change the interface of the findXXX() method.
        	    for(String v : values) {
        	        webResource = webResource.queryParam("values", v);
        	    }
        	}
        }
        if(aggregate != null) {
            webResource = webResource.queryParam("aggregate", aggregate);
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = null;
        int numTries = 0;
        while(clientResponse == null && numTries++ < MAX_RETRIES) {
            try {
                // Order of the mime type ???
                //clientResponse = webResource.accept(MediaType.TEXT_PLAIN, MediaType.APPLICATION_XML).get(ClientResponse.class);
                clientResponse = webResource.accept(MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN).get(ClientResponse.class);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Request failed (" + numTries + ").", ex);
            }
        }
        if(clientResponse == null) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Request failed after " + MAX_RETRIES + " tries.");
            throw new ServiceUnavailableException("Request failed after " + MAX_RETRIES + " tries.");
        }
        
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                count = Long.parseLong(clientResponse.getEntity(String.class));
                if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "VisitorSetting count = " + count);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);  // ???
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_VISITORSETTING);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
 
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createVisitorSetting(String user, String redirectType, Long flashDuration, String privacyType) throws BaseException
    {
        VisitorSettingBean bean = new VisitorSettingBean(null, user, redirectType, flashDuration, privacyType);
        return createVisitorSetting(bean);        
    }

    @Override
    public String createVisitorSetting(VisitorSetting bean) throws BaseException
    {
        log.finer("BEGIN");

        String guid = null;
        VisitorSettingStub stub = MarshalHelper.convertVisitorSettingToStub(bean);
        WebResource webResource = getVisitorSettingWebResource();

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = null;
        int numTries = 0;
        while(clientResponse == null && numTries++ < MAX_RETRIES) {
            try {
                clientResponse = webResource.type(getInputMediaType()).post(ClientResponse.class, stub);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Request failed (" + numTries + ").", ex);
            }
        }
        if(clientResponse == null) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Request failed after " + MAX_RETRIES + " tries: bean = " + bean);
            throw new ServiceUnavailableException("Request failed after " + MAX_RETRIES + " tries: bean = " + bean);
        }

        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.CREATED:
            case StatusCode.ACCEPTED:
                guid = clientResponse.getEntity(String.class);
                if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "New VisitorSetting guid = " + guid);
                String createdUri = clientResponse.getLocation().toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "New VisitorSetting resource uri = " + createdUri);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_VISITORSETTING);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateVisitorSetting(String guid, String user, String redirectType, Long flashDuration, String privacyType) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "VisitorSetting guid is invalid.");
        	throw new BaseException("VisitorSetting guid is invalid.");
        }
        VisitorSettingBean bean = new VisitorSettingBean(guid, user, redirectType, flashDuration, privacyType);
        return updateVisitorSetting(bean);        
    }

    @Override
    public Boolean updateVisitorSetting(VisitorSetting bean) throws BaseException
    {
        log.finer("BEGIN");

        String guid = bean.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "VisitorSetting object is invalid.");
        	throw new BaseException("VisitorSetting object is invalid.");
        }
        VisitorSettingStub stub = MarshalHelper.convertVisitorSettingToStub(bean);

        WebResource webResource = getVisitorSettingWebResource(guid);

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);
        
        ClientResponse clientResponse = null;
        int numTries = 0;
        while(clientResponse == null && numTries++ < MAX_RETRIES) {
            try {
                clientResponse = webResource.type(getInputMediaType()).put(ClientResponse.class, stub);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Request failed (" + numTries + ").", ex);
            }
        }
        if(clientResponse == null) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Request failed after " + MAX_RETRIES + " tries: bean = " + bean);
            throw new ServiceUnavailableException("Request failed after " + MAX_RETRIES + " tries: bean = " + bean);
        }

        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
            case StatusCode.ACCEPTED:
            case StatusCode.NO_CONTENT:
            case StatusCode.RESET_CONTENT:
                if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Successfully updated the object with guid = " + guid);
                return true;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_VISITORSETTING);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }

        log.finer("END: false");
        return false;
    }

    @Override
    public Boolean deleteVisitorSetting(String guid) throws BaseException
    {
        log.finer("BEGIN");

        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "VisitorSetting guid is invalid.");
        	throw new BaseException("VisitorSetting guid is invalid.");
        }

        WebResource webResource = getVisitorSettingWebResource(guid);

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = null;
        int numTries = 0;
        while(clientResponse == null && numTries++ < MAX_RETRIES) {
            try {
                clientResponse = webResource.delete(ClientResponse.class);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Request failed (" + numTries + ").", ex);
            }
        }
        if(clientResponse == null) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Request failed after " + MAX_RETRIES + " tries: guid = " + guid);
            throw new ServiceUnavailableException("Request failed after " + MAX_RETRIES + " tries: guid = " + guid);
        }

        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
            case StatusCode.ACCEPTED:
            case StatusCode.NO_CONTENT:
            case StatusCode.RESET_CONTENT:
                if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Successfully deleted the object with guid = " + guid);
                return true;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_VISITORSETTING);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }

        log.finer("END: false");
        return false;
    }

    @Override
    public Boolean deleteVisitorSetting(VisitorSetting bean) throws BaseException
    {
        String guid = bean.getGuid();
        return deleteVisitorSetting(guid);
    }

    @Override
    public Long deleteVisitorSettings(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = 0L;
     	WebResource webResource = getVisitorSettingWebResource();
    	if(filter == null || filter.isEmpty()) {
    	    // Should we allow empty filter????
    	    // Currently, the data access layer throws exceptionif the filter is empty.
    	} else {
    	    webResource = webResource.queryParam("filter", filter);
    	}
        if(filter != null && !filter.isEmpty()) {
            // params/values make sense only if filter is set.
            if(params != null && !params.isEmpty()) {
                webResource = webResource.queryParam("params", params);
            }
        	if(values != null) {
                // TBD: Is the order of "values" preserved????
                // If not, we'll need to change the interface. Or, try comma-separated list???
        	    for(String v : values) {
        	        webResource = webResource.queryParam("values", v);
        	    }
        	}
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = null;
        int numTries = 0;
        while(clientResponse == null && numTries++ < MAX_RETRIES) {
            try {
                // Order of the mime types ???
                clientResponse = webResource.accept(MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN).delete(ClientResponse.class);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Request failed (" + numTries + ").", ex);
            }
        }
        if(clientResponse == null) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Request failed after " + MAX_RETRIES + " tries: delete-filter = " + filter);
            throw new ServiceUnavailableException("Request failed after " + MAX_RETRIES + " tries: delete-filter = " + filter);
        }

        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
            case StatusCode.ACCEPTED:
                count = Long.parseLong(clientResponse.getEntity(String.class));
                if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Deleted VisitorSettings: count = " + count);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class); // ???
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_VISITORSETTING);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
