package com.cannyurl.af.resource.async;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.CommonConstants;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.exception.InternalServerErrorException;
import com.myurldb.ws.exception.NotImplementedException;
import com.myurldb.ws.exception.RequestConflictException;
import com.myurldb.ws.exception.RequestForbiddenException;
import com.myurldb.ws.exception.ResourceGoneException;
import com.myurldb.ws.exception.ResourceNotFoundException;
import com.myurldb.ws.exception.ServiceUnavailableException;
import com.myurldb.ws.exception.resource.BaseResourceException;
import com.myurldb.ws.resource.exception.BadRequestRsException;
import com.myurldb.ws.resource.exception.InternalServerErrorRsException;
import com.myurldb.ws.resource.exception.NotImplementedRsException;
import com.myurldb.ws.resource.exception.RequestConflictRsException;
import com.myurldb.ws.resource.exception.RequestForbiddenRsException;
import com.myurldb.ws.resource.exception.ResourceGoneRsException;
import com.myurldb.ws.resource.exception.ResourceNotFoundRsException;
import com.myurldb.ws.resource.exception.ServiceUnavailableRsException;

import com.myurldb.ws.LinkPassphrase;
import com.myurldb.ws.stub.LinkPassphraseStub;
import com.myurldb.ws.stub.LinkPassphraseListStub;
import com.cannyurl.af.bean.LinkPassphraseBean;
import com.cannyurl.af.proxy.LinkPassphraseServiceProxy;
import com.cannyurl.af.proxy.remote.RemoteProxyFactory;
import com.cannyurl.af.proxy.remote.RemoteLinkPassphraseServiceProxy;
import com.cannyurl.af.resource.LinkPassphraseResource;


@Path("/_task/r/linkPassphrases/")
public class AsyncLinkPassphraseResource extends BaseAsyncResource implements LinkPassphraseResource
{
    private static final Logger log = Logger.getLogger(AsyncLinkPassphraseResource.class.getName());

    private UriInfo uriInfo;
    private HttpHeaders httpHeaders;
    private Request request;
    private String resourceUri;
    private String queueName = null;
    private String taskName = null;
    private Integer retryCount = null;
    private boolean dummyPayload = false;

    public AsyncLinkPassphraseResource(@Context javax.ws.rs.core.UriInfo uriInfo, @Context javax.ws.rs.core.HttpHeaders httpHeaders, @Context javax.ws.rs.core.Request request)
    {
        this.uriInfo = uriInfo;
        this.httpHeaders = httpHeaders;
        this.request = request;
        this.resourceUri = uriInfo.getAbsolutePath().toString();
        List<String> qns = httpHeaders.getRequestHeader("X-AppEngine-QueueName");
        if(qns != null && qns.size() > 0) {
            this.queueName = qns.get(0);
        }
        List<String> tns = httpHeaders.getRequestHeader("X-AppEngine-TaskName");
        if(tns != null && tns.size() > 0) {
            this.taskName = tns.get(0);
        }
        List<String> rcs = httpHeaders.getRequestHeader("X-AppEngine-TaskRetryCount");
        if(rcs != null && rcs.size() > 0) {
            String strCount = rcs.get(0);
            try {
                this.retryCount = Integer.parseInt(strCount);
            } catch(NumberFormatException ex) {
                // ignore.
                //this.retryCount = 0;
            }
        }
        List<String> ats = httpHeaders.getRequestHeader("X-AsyncTask-Payload");
        if(ats != null && ats.size() > 0 && ats.get(0).equals("DummyPayload")) {
            this.dummyPayload = true;
        }
    }

    private Response getLinkPassphraseList(List<LinkPassphrase> beans) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getAllLinkPassphrases(String ordering, Long offset, Integer count) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getAllLinkPassphraseKeys(String ordering, Long offset, Integer count) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response findLinkPassphraseKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response findLinkPassphrases(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response findLinkPassphrasesAsJsonp(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, String callback)
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

//    @Override
//    public Response getLinkPassphraseAsHtml(String guid) throws BaseResourceException
//    {
//        // Note: This method should never be called.
//        throw new NotImplementedRsException(resourceUri);
//    }

    @Override
    public Response getLinkPassphrase(String guid) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getLinkPassphraseAsJsonp(String guid, String callback) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getLinkPassphrase(String guid, String field) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    // TBD
    @Override
    public Response constructLinkPassphrase(LinkPassphraseStub linkPassphrase) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "constructLinkPassphrase(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            LinkPassphraseBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                LinkPassphraseStub realStub = (LinkPassphraseStub) getCache().get(taskName);
                if(realStub == null) {
                    // Cached object is likely gone/deleted. No point of retrying...
                    // throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                    // Just return 2xx status code so that there will be no more retries...
                    log.severe("Failed to retrieve the real stub object from cache. This task will be aborted.");
                    return Response.noContent().build();   // ????
                }
                if(log.isLoggable(Level.FINE)) log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertLinkPassphraseStubToBean(realStub);
            } else {
                bean = convertLinkPassphraseStubToBean(linkPassphrase);
            }
            //bean = (LinkPassphraseBean) RemoteProxyFactory.getInstance().getLinkPassphraseServiceProxy().constructLinkPassphrase(bean);
            //linkPassphrase = LinkPassphraseStub.convertBeanToStub(bean);
            //String guid = linkPassphrase.getGuid();
            // TBD: createLinkPassphrase() or constructLinkPassphrase()???  (constructLinkPassphrase() currently not implemented)
            String guid = RemoteProxyFactory.getInstance().getLinkPassphraseServiceProxy().createLinkPassphrase(bean);
            URI createdUri = URI.create(resourceUri + "/" + guid);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "constructLinkPassphrase(): Successfully processed the request: createdUri = " + createdUri.toString());
            return Response.created(createdUri).entity(linkPassphrase).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response createLinkPassphrase(LinkPassphraseStub linkPassphrase) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createLinkPassphrase(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            LinkPassphraseBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                LinkPassphraseStub realStub = (LinkPassphraseStub) getCache().get(taskName);
                if(realStub == null) {
                    // Cached object is likely gone/deleted. No point of retrying...
                    // throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                    // Just return 2xx status code so that there will be no more retries...
                    log.severe("Failed to retrieve the real stub object from cache. This task will be aborted.");
                    return Response.noContent().build();   // ????
                }
                if(log.isLoggable(Level.FINE)) log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertLinkPassphraseStubToBean(realStub);
            } else {
                bean = convertLinkPassphraseStubToBean(linkPassphrase);
            }
            String guid = RemoteProxyFactory.getInstance().getLinkPassphraseServiceProxy().createLinkPassphrase(bean);
            URI createdUri = URI.create(resourceUri + "/" + guid);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createLinkPassphrase(): Successfully processed the request: createdUri = " + createdUri.toString());
            return Response.created(createdUri).entity(guid).type(MediaType.TEXT_PLAIN).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

//    @Override
    public Response createLinkPassphrase(MultivaluedMap<String, String> formParams) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    // TBD
    @Override
    public Response refreshLinkPassphrase(String guid, LinkPassphraseStub linkPassphrase) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "refreshLinkPassphrase(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            if(linkPassphrase == null || !guid.equals(linkPassphrase.getGuid())) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Path param guid = " + guid + " is different from linkPassphrase guid = " + linkPassphrase.getGuid());
                throw new RequestForbiddenRsException("Failed to refresh the linkPassphrase with guid = " + guid);
            }
            LinkPassphraseBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                LinkPassphraseStub realStub = (LinkPassphraseStub) getCache().get(taskName);
                if(realStub == null) {
                    // Cached object is likely gone/deleted. No point of retrying...
                    // throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                    // Just return 2xx status code so that there will be no more retries...
                    log.severe("Failed to retrieve the real stub object from cache. This task will be aborted.");
                    return Response.noContent().build();   // ????
                }
                if(log.isLoggable(Level.FINE)) log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertLinkPassphraseStubToBean(realStub);
            } else {
                bean = convertLinkPassphraseStubToBean(linkPassphrase);
            }
            //bean = (LinkPassphraseBean) RemoteProxyFactory.getInstance().getLinkPassphraseServiceProxy().refreshLinkPassphrase(bean);
            //if(bean == null) {
            //    if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to refresh the linkPassphrase with guid = " + guid);
            //    throw new InternalServerErrorException("Failed to refresh the linkPassphrase with guid = " + guid);
            //}
            //linkPassphrase = LinkPassphraseStub.convertBeanToStub(bean);
            // TBD: updateLinkPassphrase() or refreshLinkPassphrase()???  (refreshLinkPassphrase() currently not implemented)
            boolean suc = RemoteProxyFactory.getInstance().getLinkPassphraseServiceProxy().updateLinkPassphrase(bean);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to refrefsh the linkPassphrase with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the linkPassphrase with guid = " + guid);
            }
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "refreshLinkPassphrase(): Successfully processed the request: guid = " + guid);
            return Response.ok(linkPassphrase).build();  // ???
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateLinkPassphrase(String guid, LinkPassphraseStub linkPassphrase) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateLinkPassphrase(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            if(linkPassphrase == null || !guid.equals(linkPassphrase.getGuid())) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Path param guid = " + guid + " is different from linkPassphrase guid = " + linkPassphrase.getGuid());
                throw new RequestForbiddenRsException("Failed to update the linkPassphrase with guid = " + guid);
            }
            LinkPassphraseBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                LinkPassphraseStub realStub = (LinkPassphraseStub) getCache().get(taskName);
                if(realStub == null) {
                    // Cached object is likely gone/deleted. No point of retrying...
                    // throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                    // Just return 2xx status code so that there will be no more retries...
                    log.severe("Failed to retrieve the real stub object from cache. This task will be aborted.");
                    return Response.noContent().build();   // ????
                }
                if(log.isLoggable(Level.FINE)) log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertLinkPassphraseStubToBean(realStub);
            } else {
                bean = convertLinkPassphraseStubToBean(linkPassphrase);
            }
            boolean suc = RemoteProxyFactory.getInstance().getLinkPassphraseServiceProxy().updateLinkPassphrase(bean);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to update the linkPassphrase with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the linkPassphrase with guid = " + guid);
            }
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateLinkPassphrase(): Successfully processed the request: guid = " + guid);
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateLinkPassphrase(String guid, String shortLink, String passphrase)
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

//    @Override
    public Response updateLinkPassphrase(String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response deleteLinkPassphrase(String guid) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "deleteLinkPassphrase(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            boolean suc = RemoteProxyFactory.getInstance().getLinkPassphraseServiceProxy().deleteLinkPassphrase(guid);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete the linkPassphrase with guid = " + guid);
                throw new InternalServerErrorException("Failed to delete the linkPassphrase with guid = " + guid);
            }
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "deleteLinkPassphrase(): Successfully processed the request: guid = " + guid);
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response deleteLinkPassphrases(String filter, String params, List<String> values) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "deleteLinkPassphrases(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            Long count = RemoteProxyFactory.getInstance().getLinkPassphraseServiceProxy().deleteLinkPassphrases(filter, params, values);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Delete count = " + count);
            return Response.ok(count.toString()).type(MediaType.TEXT_PLAIN).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }


// TBD ....
    @Override
    public Response createLinkPassphrases(LinkPassphraseListStub linkPassphrases) throws BaseResourceException
    {
        // TBD: Do we need this method????
        throw new NotImplementedRsException(resourceUri);
/*
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createLinkPassphrases(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            LinkPassphraseListStub stubs;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                LinkPassphraseListStub realStubs = (LinkPassphraseListStub) getCache().get(taskName);
                if(realStubs == null) {
                    throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                }
                if(log.isLoggable(Level.FINE)) log.fine("Real stub list retrieved from memCache. realStubs = " + realStubs);
                stubs = realStubs;
            } else {
                stubs = linkPassphrases;
            }

            List<LinkPassphraseStub> stubList = linkPassphrases.getList();
            List<VisitorSetting> beans = new ArrayList<LinkPassphrase>();
            for(LinkPassphraseStub stub : stubList) {
                LinkPassphraseBean bean = convertLinkPassphraseStubToBean(stub);
                beans.add(bean);
            }
            Integer count = RemoteProxyFactory.getInstance().getLinkPassphraseServiceProxy().createLinkPassphrases(beans);
            return Response.ok(count.toString()).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
*/
    }


    public static LinkPassphraseBean convertLinkPassphraseStubToBean(LinkPassphrase stub)
    {
        LinkPassphraseBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null bean is returned.");
        } else {
            bean = new LinkPassphraseBean();
            bean.setGuid(stub.getGuid());
            bean.setShortLink(stub.getShortLink());
            bean.setPassphrase(stub.getPassphrase());
            bean.setCreatedTime(stub.getCreatedTime());
            bean.setModifiedTime(stub.getModifiedTime());
        }
        return bean;
    }

}
