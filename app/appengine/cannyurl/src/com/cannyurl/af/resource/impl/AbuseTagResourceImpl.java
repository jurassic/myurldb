package com.cannyurl.af.resource.impl;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.ws.rs.Path;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;
import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.api.core.HttpContext;
import com.sun.jersey.api.json.JSONWithPadding;
import com.sun.jersey.oauth.server.OAuthServerRequest;
import com.sun.jersey.oauth.signature.OAuthSignatureException;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.CommonConstants;
import com.cannyurl.af.config.Config;
import com.cannyurl.af.auth.TwoLeggedOAuthProvider;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.exception.InternalServerErrorException;
import com.myurldb.ws.exception.NotImplementedException;
import com.myurldb.ws.exception.RequestConflictException;
import com.myurldb.ws.exception.RequestForbiddenException;
import com.myurldb.ws.exception.ResourceGoneException;
import com.myurldb.ws.exception.ResourceNotFoundException;
import com.myurldb.ws.exception.ServiceUnavailableException;
import com.myurldb.ws.exception.resource.BaseResourceException;
import com.myurldb.ws.resource.exception.BadRequestRsException;
import com.myurldb.ws.resource.exception.InternalServerErrorRsException;
import com.myurldb.ws.resource.exception.NotImplementedRsException;
import com.myurldb.ws.resource.exception.RequestConflictRsException;
import com.myurldb.ws.resource.exception.RequestForbiddenRsException;
import com.myurldb.ws.resource.exception.ResourceGoneRsException;
import com.myurldb.ws.resource.exception.ResourceNotFoundRsException;
import com.myurldb.ws.resource.exception.ServiceUnavailableRsException;
import com.myurldb.ws.resource.exception.UnauthorizedRsException;

import com.myurldb.ws.AbuseTag;
import com.myurldb.ws.stub.KeyListStub;
import com.myurldb.ws.stub.AbuseTagStub;
import com.myurldb.ws.stub.AbuseTagListStub;
import com.cannyurl.af.bean.AbuseTagBean;
import com.cannyurl.af.service.manager.ServiceManager;
import com.cannyurl.af.resource.AbuseTagResource;


@Path("/abuseTags/")
public class AbuseTagResourceImpl extends BaseResourceImpl implements AbuseTagResource
{
    private static final Logger log = Logger.getLogger(AbuseTagResourceImpl.class.getName());

    private UriInfo uriInfo;
    private HttpHeaders httpHeaders;
    private Request request;
    private String resourceUri;
    private boolean isRunningOnDevel;
    private HttpContext httpContext;

    public AbuseTagResourceImpl(@Context javax.ws.rs.core.UriInfo uriInfo, @Context javax.ws.rs.core.HttpHeaders httpHeaders, @Context javax.ws.rs.core.Request request, @Context ServletContext servletContext, @Context HttpContext httpContext)
    {
        this.uriInfo = uriInfo;
        this.httpHeaders = httpHeaders;
        this.request = request;
        this.resourceUri = uriInfo.getAbsolutePath().toString();

        String serverInfo = servletContext.getServerInfo();
        if(log.isLoggable(Level.INFO)) log.info("ServerInfo = " + serverInfo);
        if(serverInfo != null && serverInfo.contains("Development")) {
            isRunningOnDevel = true;
        } else {
            isRunningOnDevel = false;
        }        

        this.httpContext = httpContext;
    }

    private Response getAbuseTagList(List<AbuseTag> beans) throws BaseResourceException
    {
        if(beans == null) {
            log.log(Level.WARNING, "Failed to retrieve the list.");
            throw new RequestForbiddenRsException("Failed to retrieve the list.");
        }
        if(beans.size() == 0) {
            log.log(Level.INFO, "Retrieved an empty list.");
            return Response.ok(new AbuseTagListStub()).build();
        } else {
            List<AbuseTagStub> stubs = new ArrayList<AbuseTagStub>();
            Iterator<AbuseTag> it = beans.iterator();
            while(it.hasNext()) {
                AbuseTag bean = (AbuseTag) it.next();
                stubs.add(AbuseTagStub.convertBeanToStub(bean));
            }
            return Response.ok(new AbuseTagListStub(stubs)).build();
        }
    }

    // TBD
    private Response getAbuseTagListAsJsonp(List<AbuseTag> beans, String callback) throws BaseResourceException
    {
        if(beans == null) {
            log.log(Level.WARNING, "Failed to retrieve the list.");
            throw new RequestForbiddenRsException("Failed to retrieve the list.");
        }
        if(beans.size() == 0) {
            log.log(Level.INFO, "Retrieved an empty list.");
            return Response.ok( new JSONWithPadding( new AbuseTagListStub()) ).build();
        } else {
            List<AbuseTagStub> stubs = new ArrayList<AbuseTagStub>();
            Iterator<AbuseTag> it = beans.iterator();
            while(it.hasNext()) {
                AbuseTag bean = (AbuseTag) it.next();
                stubs.add(AbuseTagStub.convertBeanToStub(bean));
            }
            return Response.ok(new JSONWithPadding( new AbuseTagListStub(stubs), callback )).build();
        }
    }

    @Override
    public Response getAllAbuseTags(String ordering, Long offset, Integer count) throws BaseResourceException
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            if(isRunningOnDevel && isIgnoreAuth()) {
                log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
            } else {
                // Verify the signature first.
                try {
                    if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                        log.warning("Two-legged OAuth verification failed.");
                        throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                    }
                } catch (BadRequestException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                    throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
                } catch (BaseException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                    throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
                } catch (Exception e) {
                    log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                    throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
                }
            }
        }

        try {
            List<AbuseTag> beans = ServiceManager.getAbuseTagService().getAllAbuseTags(ordering, offset, count);
            return getAbuseTagList(beans);
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response getAllAbuseTagKeys(String ordering, Long offset, Integer count) throws BaseResourceException
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            if(isRunningOnDevel && isIgnoreAuth()) {
                log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
            } else {
                // Verify the signature first.
                try {
                    if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                        log.warning("Two-legged OAuth verification failed.");
                        throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                    }
                } catch (BadRequestException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                    throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
                } catch (BaseException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                    throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
                } catch (Exception e) {
                    log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                    throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
                }
            }
        }

        try {
            List<String> keys = ServiceManager.getAbuseTagService().getAllAbuseTagKeys(ordering, offset, count);
            return Response.ok(new KeyListStub(keys)).cacheControl(CacheControl.valueOf("no-cache")).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response findAbuseTagKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            if(isRunningOnDevel && isIgnoreAuth()) {
                log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
            } else {
                // Verify the signature first.
                try {
                    if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                        log.warning("Two-legged OAuth verification failed.");
                        throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                    }
                } catch (BadRequestException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                    throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
                } catch (BaseException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                    throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
                } catch (Exception e) {
                    log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                    throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
                }
            }
        }

        try {
            List<String> keys = ServiceManager.getAbuseTagService().findAbuseTagKeys(filter, ordering, params, values, grouping, unique, offset, count);
            return Response.ok(new KeyListStub(keys)).cacheControl(CacheControl.valueOf("no-cache")).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response findAbuseTags(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            if(isRunningOnDevel && isIgnoreAuth()) {
                log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
            } else {
                // Verify the signature first.
                try {
                    if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                        log.warning("Two-legged OAuth verification failed.");
                        throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                    }
                } catch (BadRequestException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                    throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
                } catch (BaseException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                    throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
                } catch (Exception e) {
                    log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                    throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
                }
            }
        }

        try {
            List<AbuseTag> beans = ServiceManager.getAbuseTagService().findAbuseTags(filter, ordering, params, values, grouping, unique, offset, count);
            return getAbuseTagList(beans);
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response findAbuseTagsAsJsonp(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, String callback)
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            if(isRunningOnDevel && isIgnoreAuth()) {
                log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
            } else {
                // Verify the signature first.
                try {
                    if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                        log.warning("Two-legged OAuth verification failed.");
                        throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                    }
                } catch (BadRequestException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                    throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
                } catch (BaseException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                    throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
                } catch (Exception e) {
                    log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                    throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
                }
            }
        }

        try {
            List<AbuseTag> beans = ServiceManager.getAbuseTagService().findAbuseTags(filter, ordering, params, values, grouping, unique, offset, count);
            return getAbuseTagListAsJsonp(beans, callback);
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            if(isRunningOnDevel && isIgnoreAuth()) {
                log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
            } else {
                // Verify the signature first.
                try {
                    if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                        log.warning("Two-legged OAuth verification failed.");
                        throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                    }
                } catch (BadRequestException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                    throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
                } catch (BaseException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                    throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
                } catch (Exception e) {
                    log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                    throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
                }
            }
        }

        try {
            Long count = ServiceManager.getAbuseTagService().getCount(filter, params, values, aggregate);
            return Response.ok(count.toString()).type(MediaType.TEXT_PLAIN).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

//    @Override
//    public Response getAbuseTagAsHtml(String guid) throws BaseResourceException
//    {
//        // TBD
//        throw new NotImplementedRsException("Html format currently not supported.", resourceUri);
//    }

    @Override
    public Response getAbuseTag(String guid) throws BaseResourceException
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            if(isRunningOnDevel && isIgnoreAuth()) {
                log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
            } else {
                // Verify the signature first.
                try {
                    if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                        log.warning("Two-legged OAuth verification failed.");
                        throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                    }
                } catch (BadRequestException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                    throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
                } catch (BaseException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                    throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
                } catch (Exception e) {
                    log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                    throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
                }
            }
        }

        try {
            AbuseTag bean = ServiceManager.getAbuseTagService().getAbuseTag(guid);
            AbuseTagStub stub = AbuseTagStub.convertBeanToStub(bean);

            EntityTag eTag = new EntityTag(Integer.toString(stub.hashCode()));
            Response.ResponseBuilder responseBuilder = request.evaluatePreconditions(eTag);
            if (responseBuilder != null) {  // Etag match
                log.info("AbuseTag stub object has not changed. Returning unmodified response code.");
                return responseBuilder.build();
            }
            log.info("Returning a full AbuseTag stub object.");

            //Date expirationDate = new Date(System.currentTimeMillis() + CommonConstants.CACHE_EXPIRATION_TIME);
            //ResponseBuilder builder = Response.ok(stub).tag(eTag).expires(expirationDate);
            ResponseBuilder builder = Response.ok(stub).tag(eTag);
            Date modifiedDate = null;
            if(bean.getModifiedTime() != null) {
                modifiedDate = new Date(bean.getModifiedTime());
            } else {
                if(bean.getCreatedTime() != null) {
                    modifiedDate = new Date(bean.getCreatedTime());
                } else {   // This should not happen.
                    modifiedDate = new Date();  // Just use "now".                        
                }
            }
            return builder.lastModified(modifiedDate).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response getAbuseTagAsJsonp(String guid, String callback) throws BaseResourceException
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            if(isRunningOnDevel && isIgnoreAuth()) {
                log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
            } else {
                // Verify the signature first.
                try {
                    if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                        log.warning("Two-legged OAuth verification failed.");
                        throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                    }
                } catch (BadRequestException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                    throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
                } catch (BaseException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                    throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
                } catch (Exception e) {
                    log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                    throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
                }
            }
        }

        try {
            AbuseTag bean = ServiceManager.getAbuseTagService().getAbuseTag(guid);
            AbuseTagStub stub = AbuseTagStub.convertBeanToStub(bean);

            EntityTag eTag = new EntityTag(Integer.toString(stub.hashCode()));
            Response.ResponseBuilder responseBuilder = request.evaluatePreconditions(eTag);
            if (responseBuilder != null) {  // Etag match
                log.info("AbuseTag stub object has not changed. Returning unmodified response code.");
                return responseBuilder.build();
            }
            log.info("Returning a full AbuseTag stub object.");

            //Date expirationDate = new Date(System.currentTimeMillis() + CommonConstants.CACHE_EXPIRATION_TIME);
            //ResponseBuilder builder = Response.ok(new JSONWithPadding( stub, callback )).tag(eTag).expires(expirationDate);
            ResponseBuilder builder = Response.ok(new JSONWithPadding( stub, callback )).tag(eTag);
            Date modifiedDate = null;
            if(bean.getModifiedTime() != null) {
                modifiedDate = new Date(bean.getModifiedTime());
            } else {
                if(bean.getCreatedTime() != null) {
                    modifiedDate = new Date(bean.getCreatedTime());
                } else {   // This should not happen.
                    modifiedDate = new Date();  // Just use "now".                        
                }
            }
            return builder.lastModified(modifiedDate).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    public Response getAbuseTag(String guid, String field) throws BaseResourceException
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            if(isRunningOnDevel && isIgnoreAuth()) {
                log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
            } else {
                // Verify the signature first.
                try {
                    if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                        log.warning("Two-legged OAuth verification failed.");
                        throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                    }
                } catch (BadRequestException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                    throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
                } catch (BaseException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                    throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
                } catch (Exception e) {
                    log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                    throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
                }
            }
        }

        try {
            if(guid != null) {
                guid = guid.toLowerCase();  // TBD: Validate/normalize guid....
            }
            if(field == null || field.trim().length() == 0) {
                return getAbuseTag(guid);
            }
            AbuseTag bean = ServiceManager.getAbuseTagService().getAbuseTag(guid);
            String value = null;
            if(bean != null) {
                if(field.equals("guid")) {
                    value = bean.getGuid();
                } else if(field.equals("shortLink")) {
                    String fval = bean.getShortLink();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("shortUrl")) {
                    String fval = bean.getShortUrl();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("longUrl")) {
                    String fval = bean.getLongUrl();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("longUrlHash")) {
                    String fval = bean.getLongUrlHash();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("user")) {
                    String fval = bean.getUser();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("ipAddress")) {
                    String fval = bean.getIpAddress();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("tag")) {
                    Integer fval = bean.getTag();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("comment")) {
                    String fval = bean.getComment();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("status")) {
                    String fval = bean.getStatus();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("reviewer")) {
                    String fval = bean.getReviewer();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("action")) {
                    String fval = bean.getAction();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("note")) {
                    String fval = bean.getNote();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("pastActions")) {
                    String fval = bean.getPastActions();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("reviewedTime")) {
                    Long fval = bean.getReviewedTime();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("actionTime")) {
                    Long fval = bean.getActionTime();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("createdTime")) {
                    Long fval = bean.getCreatedTime();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("modifiedTime")) {
                    Long fval = bean.getModifiedTime();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                }
            }
            //Date expirationDate = new Date(System.currentTimeMillis() + CommonConstants.CACHE_EXPIRATION_TIME);
            //ResponseBuilder builder = Response.ok(value).type(MediaType.TEXT_PLAIN).expires(expirationDate);
            ResponseBuilder builder = Response.ok(value).type(MediaType.TEXT_PLAIN);
            Date modifiedDate = null;
            if(bean.getModifiedTime() != null) {
                modifiedDate = new Date(bean.getModifiedTime());
            } else {
                if(bean.getCreatedTime() != null) {
                    modifiedDate = new Date(bean.getCreatedTime());
                } else {   // This should not happen.
                    modifiedDate = new Date();  // Just use "now".                        
                }
            }
            return builder.lastModified(modifiedDate).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }        
    }

    // TBD
    @Override
    public Response constructAbuseTag(AbuseTagStub abuseTag) throws BaseResourceException
    {
        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            AbuseTagBean bean = convertAbuseTagStubToBean(abuseTag);
            bean = (AbuseTagBean) ServiceManager.getAbuseTagService().constructAbuseTag(bean);
            abuseTag = AbuseTagStub.convertBeanToStub(bean);
            String guid = abuseTag.getGuid();
            URI createdUri = URI.create(resourceUri + "/" + guid);
            return Response.created(createdUri).entity(abuseTag).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response createAbuseTag(AbuseTagStub abuseTag) throws BaseResourceException
    {
        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            AbuseTagBean bean = convertAbuseTagStubToBean(abuseTag);
            String guid = ServiceManager.getAbuseTagService().createAbuseTag(bean);
            URI createdUri = URI.create(resourceUri + "/" + guid);
            return Response.created(createdUri).entity(guid).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

//    @Override
    public Response createAbuseTag(MultivaluedMap<String, String> formParams) throws BaseResourceException
    {
        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            AbuseTagBean bean = convertFormParamsToBean(formParams);
            String guid = ServiceManager.getAbuseTagService().createAbuseTag(bean);
            URI createdUri = URI.create(resourceUri + "/" + guid);
            return Response.created(createdUri).entity(guid).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    // TBD
    @Override
    public Response refreshAbuseTag(String guid, AbuseTagStub abuseTag) throws BaseResourceException
    {
        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            if(abuseTag == null || !guid.equals(abuseTag.getGuid())) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Path param guid = " + guid + " is different from abuseTag guid = " + abuseTag.getGuid());
                throw new RequestForbiddenException("Failed to refresh the abuseTag with guid = " + guid);
            }
            AbuseTagBean bean = convertAbuseTagStubToBean(abuseTag);
            bean = (AbuseTagBean) ServiceManager.getAbuseTagService().refreshAbuseTag(bean);
            if(bean == null) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to refresh the abuseTag with guid = " + guid);
                throw new InternalServerErrorException("Failed to refresh the abuseTag with guid = " + guid);
            }
            abuseTag = AbuseTagStub.convertBeanToStub(bean);
            return Response.ok(abuseTag).build();  // ???
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateAbuseTag(String guid, AbuseTagStub abuseTag) throws BaseResourceException
    {
        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            if(abuseTag == null || !guid.equals(abuseTag.getGuid())) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Path param guid = " + guid + " is different from abuseTag guid = " + abuseTag.getGuid());
                throw new RequestForbiddenException("Failed to update the abuseTag with guid = " + guid);
            }
            AbuseTagBean bean = convertAbuseTagStubToBean(abuseTag);
            boolean suc = ServiceManager.getAbuseTagService().updateAbuseTag(bean);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to update the abuseTag with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the abuseTag with guid = " + guid);
            }
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateAbuseTag(String guid, String shortLink, String shortUrl, String longUrl, String longUrlHash, String user, String ipAddress, Integer tag, String comment, String status, String reviewer, String action, String note, String pastActions, Long reviewedTime, Long actionTime)
    {
        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            /*
            boolean suc = ServiceManager.getAbuseTagService().updateAbuseTag(guid, shortLink, shortUrl, longUrl, longUrlHash, user, ipAddress, tag, comment, status, reviewer, action, note, pastActions, reviewedTime, actionTime);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to update the abuseTag with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the abuseTag with guid = " + guid);
            }
            return Response.noContent().build();
            */
            throw new NotImplementedException("This method has not been implemented yet.");
        //} catch(BadRequestException ex) {
        //    throw new BadRequestRsException(ex, resourceUri);
        //} catch(ResourceNotFoundException ex) {
        //    throw new ResourceNotFoundRsException(ex, resourceUri);
        //} catch(ResourceGoneException ex) {
        //    throw new ResourceGoneRsException(ex, resourceUri);
        //} catch(RequestForbiddenException ex) {
        //    throw new RequestForbiddenRsException(ex, resourceUri);
        //} catch(RequestConflictException ex) {
        //    throw new RequestConflictRsException(ex, resourceUri);
        //} catch(ServiceUnavailableException ex) {
        //    throw new ServiceUnavailableRsException(ex, resourceUri);
        //} catch(InternalServerErrorException ex) {
        //    throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(NotImplementedException ex) {
            throw new NotImplementedRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

//    @Override
    public Response updateAbuseTag(String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException
    {
        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            AbuseTagBean bean = convertFormParamsToBean(formParams);
            boolean suc = ServiceManager.getAbuseTagService().updateAbuseTag(bean);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to update the abuseTag with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the abuseTag with guid = " + guid);
            }
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response deleteAbuseTag(String guid) throws BaseResourceException
    {
        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            boolean suc = ServiceManager.getAbuseTagService().deleteAbuseTag(guid);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete the abuseTag with guid = " + guid);
                throw new InternalServerErrorException("Failed to delete the abuseTag with guid = " + guid);
            }
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response deleteAbuseTags(String filter, String params, List<String> values) throws BaseResourceException
    {
        try {
            Long count = ServiceManager.getAbuseTagService().deleteAbuseTags(filter, params, values);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Delete count = " + count);
            return Response.ok(count.toString()).type(MediaType.TEXT_PLAIN).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }


    @Override
    public Response createAbuseTags(AbuseTagListStub abuseTags) throws BaseResourceException
    {
        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            List<AbuseTagStub> stubs = abuseTags.getList();
            List<AbuseTag> beans = new ArrayList<AbuseTag>();
            for(AbuseTagStub stub : stubs) {
                AbuseTagBean bean = convertAbuseTagStubToBean(stub);
                beans.add(bean);
            }
            Integer count = ServiceManager.getAbuseTagService().createAbuseTags(beans);
            return Response.ok(count.toString()).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }


    public static AbuseTagBean convertAbuseTagStubToBean(AbuseTag stub)
    {
        AbuseTagBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null bean is returned.");
        } else {
            bean = new AbuseTagBean();
            bean.setGuid(stub.getGuid());
            bean.setShortLink(stub.getShortLink());
            bean.setShortUrl(stub.getShortUrl());
            bean.setLongUrl(stub.getLongUrl());
            bean.setLongUrlHash(stub.getLongUrlHash());
            bean.setUser(stub.getUser());
            bean.setIpAddress(stub.getIpAddress());
            bean.setTag(stub.getTag());
            bean.setComment(stub.getComment());
            bean.setStatus(stub.getStatus());
            bean.setReviewer(stub.getReviewer());
            bean.setAction(stub.getAction());
            bean.setNote(stub.getNote());
            bean.setPastActions(stub.getPastActions());
            bean.setReviewedTime(stub.getReviewedTime());
            bean.setActionTime(stub.getActionTime());
            bean.setCreatedTime(stub.getCreatedTime());
            bean.setModifiedTime(stub.getModifiedTime());
        }
        return bean;
    }

    public static List<AbuseTagBean> convertAbuseTagListStubToBeanList(AbuseTagListStub listStub)
    {
        if(listStub == null) {
            log.log(Level.INFO, "listStub is null. Null list is returned.");
            return null;
        } else {
            List<AbuseTagStub> stubList = listStub.getList();
            if(stubList == null) {
                log.log(Level.INFO, "Stub list is null. Null list is returned.");
                return null;                
            }
            List<AbuseTagBean> beanList = new ArrayList<AbuseTagBean>();
            if(stubList.isEmpty()) {
                log.log(Level.INFO, "Stub list is empty. Empty list is returned.");
            } else {
                for(AbuseTagStub stub : stubList) {
                    AbuseTagBean bean = convertAbuseTagStubToBean(stub);
                    beanList.add(bean);                            
                }
            }
            return beanList;
        }
    }


    // TBD
    // This needs to be implemented before url-encoded form create/update can be supported 
    public static AbuseTagBean convertFormParamsToBean(MultivaluedMap<String, String> formParams)
    {
        AbuseTagBean bean = new AbuseTagBean();
        if(formParams == null) {
            log.log(Level.INFO, "FormParams is null. Empty bean is returned.");
        } else {
            Iterator<MultivaluedMap.Entry<String,List<String>>> it = formParams.entrySet().iterator();
            while(it.hasNext()) {
                MultivaluedMap.Entry<String,List<String>> m =(MultivaluedMap.Entry<String,List<String>>) it.next();
                String key = (String) m.getKey();
                String val = null;
                List<String> list = m.getValue();
                if(list != null && list.size() > 0) {
                    val = list.get(0);
                    if(key.equals("guid")) {
                        bean.setGuid(val);                        
                    } else if(key.equals("shortLink")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setShortLink(v);
                    } else if(key.equals("shortUrl")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setShortUrl(v);
                    } else if(key.equals("longUrl")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setLongUrl(v);
                    } else if(key.equals("longUrlHash")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setLongUrlHash(v);
                    } else if(key.equals("user")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setUser(v);
                    } else if(key.equals("ipAddress")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setIpAddress(v);
                    } else if(key.equals("tag")) {
                        // TBD: Needs to use "parse()" methods.
                        //Integer v = (Integer) val;
                        //bean.setTag(v);
                    } else if(key.equals("comment")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setComment(v);
                    } else if(key.equals("status")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setStatus(v);
                    } else if(key.equals("reviewer")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setReviewer(v);
                    } else if(key.equals("action")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setAction(v);
                    } else if(key.equals("note")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setNote(v);
                    } else if(key.equals("pastActions")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setPastActions(v);
                    } else if(key.equals("reviewedTime")) {
                        // TBD: Needs to use "parse()" methods.
                        //Long v = (Long) val;
                        //bean.setReviewedTime(v);
                    } else if(key.equals("actionTime")) {
                        // TBD: Needs to use "parse()" methods.
                        //Long v = (Long) val;
                        //bean.setActionTime(v);
                    } else if(key.equals("createdTime")) {
                        // TBD: Needs to use "parse()" methods.
                        //Long v = (Long) val;
                        //bean.setCreatedTime(v);
                    } else if(key.equals("modifiedTime")) {
                        // TBD: Needs to use "parse()" methods.
                        //Long v = (Long) val;
                        //bean.setModifiedTime(v);
                    }
                }
            }
        }
        return bean;
    }

}
