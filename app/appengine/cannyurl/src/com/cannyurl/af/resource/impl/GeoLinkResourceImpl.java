package com.cannyurl.af.resource.impl;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.ws.rs.Path;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;
import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.api.core.HttpContext;
import com.sun.jersey.api.json.JSONWithPadding;
import com.sun.jersey.oauth.server.OAuthServerRequest;
import com.sun.jersey.oauth.signature.OAuthSignatureException;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.CommonConstants;
import com.cannyurl.af.config.Config;
import com.cannyurl.af.auth.TwoLeggedOAuthProvider;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.exception.InternalServerErrorException;
import com.myurldb.ws.exception.NotImplementedException;
import com.myurldb.ws.exception.RequestConflictException;
import com.myurldb.ws.exception.RequestForbiddenException;
import com.myurldb.ws.exception.ResourceGoneException;
import com.myurldb.ws.exception.ResourceNotFoundException;
import com.myurldb.ws.exception.ServiceUnavailableException;
import com.myurldb.ws.exception.resource.BaseResourceException;
import com.myurldb.ws.resource.exception.BadRequestRsException;
import com.myurldb.ws.resource.exception.InternalServerErrorRsException;
import com.myurldb.ws.resource.exception.NotImplementedRsException;
import com.myurldb.ws.resource.exception.RequestConflictRsException;
import com.myurldb.ws.resource.exception.RequestForbiddenRsException;
import com.myurldb.ws.resource.exception.ResourceGoneRsException;
import com.myurldb.ws.resource.exception.ResourceNotFoundRsException;
import com.myurldb.ws.resource.exception.ServiceUnavailableRsException;
import com.myurldb.ws.resource.exception.UnauthorizedRsException;

import com.myurldb.ws.CellLatitudeLongitude;
import com.myurldb.ws.GeoCoordinateStruct;
import com.myurldb.ws.GeoLink;
import com.myurldb.ws.stub.KeyListStub;
import com.myurldb.ws.stub.GeoLinkStub;
import com.myurldb.ws.stub.GeoLinkListStub;
import com.cannyurl.af.bean.CellLatitudeLongitudeBean;
import com.cannyurl.af.bean.GeoCoordinateStructBean;
import com.cannyurl.af.bean.GeoLinkBean;
import com.cannyurl.af.service.manager.ServiceManager;
import com.cannyurl.af.resource.GeoLinkResource;
import com.cannyurl.af.resource.util.CellLatitudeLongitudeResourceUtil;
import com.cannyurl.af.resource.util.GeoCoordinateStructResourceUtil;


@Path("/geoLinks/")
public class GeoLinkResourceImpl extends BaseResourceImpl implements GeoLinkResource
{
    private static final Logger log = Logger.getLogger(GeoLinkResourceImpl.class.getName());

    private UriInfo uriInfo;
    private HttpHeaders httpHeaders;
    private Request request;
    private String resourceUri;
    private boolean isRunningOnDevel;
    private HttpContext httpContext;

    public GeoLinkResourceImpl(@Context javax.ws.rs.core.UriInfo uriInfo, @Context javax.ws.rs.core.HttpHeaders httpHeaders, @Context javax.ws.rs.core.Request request, @Context ServletContext servletContext, @Context HttpContext httpContext)
    {
        this.uriInfo = uriInfo;
        this.httpHeaders = httpHeaders;
        this.request = request;
        this.resourceUri = uriInfo.getAbsolutePath().toString();

        String serverInfo = servletContext.getServerInfo();
        if(log.isLoggable(Level.INFO)) log.info("ServerInfo = " + serverInfo);
        if(serverInfo != null && serverInfo.contains("Development")) {
            isRunningOnDevel = true;
        } else {
            isRunningOnDevel = false;
        }        

        this.httpContext = httpContext;
    }

    private Response getGeoLinkList(List<GeoLink> beans) throws BaseResourceException
    {
        if(beans == null) {
            log.log(Level.WARNING, "Failed to retrieve the list.");
            throw new RequestForbiddenRsException("Failed to retrieve the list.");
        }
        if(beans.size() == 0) {
            log.log(Level.INFO, "Retrieved an empty list.");
            return Response.ok(new GeoLinkListStub()).build();
        } else {
            List<GeoLinkStub> stubs = new ArrayList<GeoLinkStub>();
            Iterator<GeoLink> it = beans.iterator();
            while(it.hasNext()) {
                GeoLink bean = (GeoLink) it.next();
                stubs.add(GeoLinkStub.convertBeanToStub(bean));
            }
            return Response.ok(new GeoLinkListStub(stubs)).build();
        }
    }

    // TBD
    private Response getGeoLinkListAsJsonp(List<GeoLink> beans, String callback) throws BaseResourceException
    {
        if(beans == null) {
            log.log(Level.WARNING, "Failed to retrieve the list.");
            throw new RequestForbiddenRsException("Failed to retrieve the list.");
        }
        if(beans.size() == 0) {
            log.log(Level.INFO, "Retrieved an empty list.");
            return Response.ok( new JSONWithPadding( new GeoLinkListStub()) ).build();
        } else {
            List<GeoLinkStub> stubs = new ArrayList<GeoLinkStub>();
            Iterator<GeoLink> it = beans.iterator();
            while(it.hasNext()) {
                GeoLink bean = (GeoLink) it.next();
                stubs.add(GeoLinkStub.convertBeanToStub(bean));
            }
            return Response.ok(new JSONWithPadding( new GeoLinkListStub(stubs), callback )).build();
        }
    }

    @Override
    public Response getAllGeoLinks(String ordering, Long offset, Integer count) throws BaseResourceException
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            if(isRunningOnDevel && isIgnoreAuth()) {
                log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
            } else {
                // Verify the signature first.
                try {
                    if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                        log.warning("Two-legged OAuth verification failed.");
                        throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                    }
                } catch (BadRequestException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                    throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
                } catch (BaseException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                    throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
                } catch (Exception e) {
                    log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                    throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
                }
            }
        }

        try {
            List<GeoLink> beans = ServiceManager.getGeoLinkService().getAllGeoLinks(ordering, offset, count);
            return getGeoLinkList(beans);
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response getAllGeoLinkKeys(String ordering, Long offset, Integer count) throws BaseResourceException
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            if(isRunningOnDevel && isIgnoreAuth()) {
                log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
            } else {
                // Verify the signature first.
                try {
                    if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                        log.warning("Two-legged OAuth verification failed.");
                        throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                    }
                } catch (BadRequestException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                    throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
                } catch (BaseException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                    throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
                } catch (Exception e) {
                    log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                    throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
                }
            }
        }

        try {
            List<String> keys = ServiceManager.getGeoLinkService().getAllGeoLinkKeys(ordering, offset, count);
            return Response.ok(new KeyListStub(keys)).cacheControl(CacheControl.valueOf("no-cache")).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response findGeoLinkKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            if(isRunningOnDevel && isIgnoreAuth()) {
                log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
            } else {
                // Verify the signature first.
                try {
                    if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                        log.warning("Two-legged OAuth verification failed.");
                        throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                    }
                } catch (BadRequestException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                    throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
                } catch (BaseException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                    throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
                } catch (Exception e) {
                    log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                    throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
                }
            }
        }

        try {
            List<String> keys = ServiceManager.getGeoLinkService().findGeoLinkKeys(filter, ordering, params, values, grouping, unique, offset, count);
            return Response.ok(new KeyListStub(keys)).cacheControl(CacheControl.valueOf("no-cache")).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response findGeoLinks(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            if(isRunningOnDevel && isIgnoreAuth()) {
                log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
            } else {
                // Verify the signature first.
                try {
                    if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                        log.warning("Two-legged OAuth verification failed.");
                        throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                    }
                } catch (BadRequestException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                    throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
                } catch (BaseException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                    throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
                } catch (Exception e) {
                    log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                    throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
                }
            }
        }

        try {
            List<GeoLink> beans = ServiceManager.getGeoLinkService().findGeoLinks(filter, ordering, params, values, grouping, unique, offset, count);
            return getGeoLinkList(beans);
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response findGeoLinksAsJsonp(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, String callback)
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            if(isRunningOnDevel && isIgnoreAuth()) {
                log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
            } else {
                // Verify the signature first.
                try {
                    if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                        log.warning("Two-legged OAuth verification failed.");
                        throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                    }
                } catch (BadRequestException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                    throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
                } catch (BaseException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                    throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
                } catch (Exception e) {
                    log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                    throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
                }
            }
        }

        try {
            List<GeoLink> beans = ServiceManager.getGeoLinkService().findGeoLinks(filter, ordering, params, values, grouping, unique, offset, count);
            return getGeoLinkListAsJsonp(beans, callback);
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            if(isRunningOnDevel && isIgnoreAuth()) {
                log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
            } else {
                // Verify the signature first.
                try {
                    if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                        log.warning("Two-legged OAuth verification failed.");
                        throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                    }
                } catch (BadRequestException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                    throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
                } catch (BaseException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                    throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
                } catch (Exception e) {
                    log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                    throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
                }
            }
        }

        try {
            Long count = ServiceManager.getGeoLinkService().getCount(filter, params, values, aggregate);
            return Response.ok(count.toString()).type(MediaType.TEXT_PLAIN).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

//    @Override
//    public Response getGeoLinkAsHtml(String guid) throws BaseResourceException
//    {
//        // TBD
//        throw new NotImplementedRsException("Html format currently not supported.", resourceUri);
//    }

    @Override
    public Response getGeoLink(String guid) throws BaseResourceException
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            if(isRunningOnDevel && isIgnoreAuth()) {
                log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
            } else {
                // Verify the signature first.
                try {
                    if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                        log.warning("Two-legged OAuth verification failed.");
                        throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                    }
                } catch (BadRequestException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                    throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
                } catch (BaseException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                    throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
                } catch (Exception e) {
                    log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                    throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
                }
            }
        }

        try {
            GeoLink bean = ServiceManager.getGeoLinkService().getGeoLink(guid);
            GeoLinkStub stub = GeoLinkStub.convertBeanToStub(bean);

            EntityTag eTag = new EntityTag(Integer.toString(stub.hashCode()));
            Response.ResponseBuilder responseBuilder = request.evaluatePreconditions(eTag);
            if (responseBuilder != null) {  // Etag match
                log.info("GeoLink stub object has not changed. Returning unmodified response code.");
                return responseBuilder.build();
            }
            log.info("Returning a full GeoLink stub object.");

            //Date expirationDate = new Date(System.currentTimeMillis() + CommonConstants.CACHE_EXPIRATION_TIME);
            //ResponseBuilder builder = Response.ok(stub).tag(eTag).expires(expirationDate);
            ResponseBuilder builder = Response.ok(stub).tag(eTag);
            Date modifiedDate = null;
            if(bean.getModifiedTime() != null) {
                modifiedDate = new Date(bean.getModifiedTime());
            } else {
                if(bean.getCreatedTime() != null) {
                    modifiedDate = new Date(bean.getCreatedTime());
                } else {   // This should not happen.
                    modifiedDate = new Date();  // Just use "now".                        
                }
            }
            return builder.lastModified(modifiedDate).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response getGeoLinkAsJsonp(String guid, String callback) throws BaseResourceException
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            if(isRunningOnDevel && isIgnoreAuth()) {
                log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
            } else {
                // Verify the signature first.
                try {
                    if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                        log.warning("Two-legged OAuth verification failed.");
                        throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                    }
                } catch (BadRequestException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                    throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
                } catch (BaseException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                    throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
                } catch (Exception e) {
                    log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                    throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
                }
            }
        }

        try {
            GeoLink bean = ServiceManager.getGeoLinkService().getGeoLink(guid);
            GeoLinkStub stub = GeoLinkStub.convertBeanToStub(bean);

            EntityTag eTag = new EntityTag(Integer.toString(stub.hashCode()));
            Response.ResponseBuilder responseBuilder = request.evaluatePreconditions(eTag);
            if (responseBuilder != null) {  // Etag match
                log.info("GeoLink stub object has not changed. Returning unmodified response code.");
                return responseBuilder.build();
            }
            log.info("Returning a full GeoLink stub object.");

            //Date expirationDate = new Date(System.currentTimeMillis() + CommonConstants.CACHE_EXPIRATION_TIME);
            //ResponseBuilder builder = Response.ok(new JSONWithPadding( stub, callback )).tag(eTag).expires(expirationDate);
            ResponseBuilder builder = Response.ok(new JSONWithPadding( stub, callback )).tag(eTag);
            Date modifiedDate = null;
            if(bean.getModifiedTime() != null) {
                modifiedDate = new Date(bean.getModifiedTime());
            } else {
                if(bean.getCreatedTime() != null) {
                    modifiedDate = new Date(bean.getCreatedTime());
                } else {   // This should not happen.
                    modifiedDate = new Date();  // Just use "now".                        
                }
            }
            return builder.lastModified(modifiedDate).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    public Response getGeoLink(String guid, String field) throws BaseResourceException
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            if(isRunningOnDevel && isIgnoreAuth()) {
                log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
            } else {
                // Verify the signature first.
                try {
                    if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                        log.warning("Two-legged OAuth verification failed.");
                        throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                    }
                } catch (BadRequestException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                    throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
                } catch (BaseException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                    throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
                } catch (Exception e) {
                    log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                    throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
                }
            }
        }

        try {
            if(guid != null) {
                guid = guid.toLowerCase();  // TBD: Validate/normalize guid....
            }
            if(field == null || field.trim().length() == 0) {
                return getGeoLink(guid);
            }
            GeoLink bean = ServiceManager.getGeoLinkService().getGeoLink(guid);
            String value = null;
            if(bean != null) {
                if(field.equals("guid")) {
                    value = bean.getGuid();
                } else if(field.equals("shortLink")) {
                    String fval = bean.getShortLink();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("shortUrl")) {
                    String fval = bean.getShortUrl();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("geoCoordinate")) {
                    GeoCoordinateStruct fval = bean.getGeoCoordinate();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("geoCell")) {
                    CellLatitudeLongitude fval = bean.getGeoCell();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("status")) {
                    String fval = bean.getStatus();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("createdTime")) {
                    Long fval = bean.getCreatedTime();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("modifiedTime")) {
                    Long fval = bean.getModifiedTime();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                }
            }
            //Date expirationDate = new Date(System.currentTimeMillis() + CommonConstants.CACHE_EXPIRATION_TIME);
            //ResponseBuilder builder = Response.ok(value).type(MediaType.TEXT_PLAIN).expires(expirationDate);
            ResponseBuilder builder = Response.ok(value).type(MediaType.TEXT_PLAIN);
            Date modifiedDate = null;
            if(bean.getModifiedTime() != null) {
                modifiedDate = new Date(bean.getModifiedTime());
            } else {
                if(bean.getCreatedTime() != null) {
                    modifiedDate = new Date(bean.getCreatedTime());
                } else {   // This should not happen.
                    modifiedDate = new Date();  // Just use "now".                        
                }
            }
            return builder.lastModified(modifiedDate).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }        
    }

    // TBD
    @Override
    public Response constructGeoLink(GeoLinkStub geoLink) throws BaseResourceException
    {
        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            GeoLinkBean bean = convertGeoLinkStubToBean(geoLink);
            bean = (GeoLinkBean) ServiceManager.getGeoLinkService().constructGeoLink(bean);
            geoLink = GeoLinkStub.convertBeanToStub(bean);
            String guid = geoLink.getGuid();
            URI createdUri = URI.create(resourceUri + "/" + guid);
            return Response.created(createdUri).entity(geoLink).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response createGeoLink(GeoLinkStub geoLink) throws BaseResourceException
    {
        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            GeoLinkBean bean = convertGeoLinkStubToBean(geoLink);
            String guid = ServiceManager.getGeoLinkService().createGeoLink(bean);
            URI createdUri = URI.create(resourceUri + "/" + guid);
            return Response.created(createdUri).entity(guid).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

//    @Override
    public Response createGeoLink(MultivaluedMap<String, String> formParams) throws BaseResourceException
    {
        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            GeoLinkBean bean = convertFormParamsToBean(formParams);
            String guid = ServiceManager.getGeoLinkService().createGeoLink(bean);
            URI createdUri = URI.create(resourceUri + "/" + guid);
            return Response.created(createdUri).entity(guid).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    // TBD
    @Override
    public Response refreshGeoLink(String guid, GeoLinkStub geoLink) throws BaseResourceException
    {
        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            if(geoLink == null || !guid.equals(geoLink.getGuid())) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Path param guid = " + guid + " is different from geoLink guid = " + geoLink.getGuid());
                throw new RequestForbiddenException("Failed to refresh the geoLink with guid = " + guid);
            }
            GeoLinkBean bean = convertGeoLinkStubToBean(geoLink);
            bean = (GeoLinkBean) ServiceManager.getGeoLinkService().refreshGeoLink(bean);
            if(bean == null) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to refresh the geoLink with guid = " + guid);
                throw new InternalServerErrorException("Failed to refresh the geoLink with guid = " + guid);
            }
            geoLink = GeoLinkStub.convertBeanToStub(bean);
            return Response.ok(geoLink).build();  // ???
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateGeoLink(String guid, GeoLinkStub geoLink) throws BaseResourceException
    {
        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            if(geoLink == null || !guid.equals(geoLink.getGuid())) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Path param guid = " + guid + " is different from geoLink guid = " + geoLink.getGuid());
                throw new RequestForbiddenException("Failed to update the geoLink with guid = " + guid);
            }
            GeoLinkBean bean = convertGeoLinkStubToBean(geoLink);
            boolean suc = ServiceManager.getGeoLinkService().updateGeoLink(bean);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to update the geoLink with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the geoLink with guid = " + guid);
            }
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateGeoLink(String guid, String shortLink, String shortUrl, String geoCoordinate, String geoCell, String status)
    {
        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            /*
            boolean suc = ServiceManager.getGeoLinkService().updateGeoLink(guid, shortLink, shortUrl, geoCoordinate, geoCell, status);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to update the geoLink with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the geoLink with guid = " + guid);
            }
            return Response.noContent().build();
            */
            throw new NotImplementedException("This method has not been implemented yet.");
        //} catch(BadRequestException ex) {
        //    throw new BadRequestRsException(ex, resourceUri);
        //} catch(ResourceNotFoundException ex) {
        //    throw new ResourceNotFoundRsException(ex, resourceUri);
        //} catch(ResourceGoneException ex) {
        //    throw new ResourceGoneRsException(ex, resourceUri);
        //} catch(RequestForbiddenException ex) {
        //    throw new RequestForbiddenRsException(ex, resourceUri);
        //} catch(RequestConflictException ex) {
        //    throw new RequestConflictRsException(ex, resourceUri);
        //} catch(ServiceUnavailableException ex) {
        //    throw new ServiceUnavailableRsException(ex, resourceUri);
        //} catch(InternalServerErrorException ex) {
        //    throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(NotImplementedException ex) {
            throw new NotImplementedRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

//    @Override
    public Response updateGeoLink(String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException
    {
        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            GeoLinkBean bean = convertFormParamsToBean(formParams);
            boolean suc = ServiceManager.getGeoLinkService().updateGeoLink(bean);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to update the geoLink with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the geoLink with guid = " + guid);
            }
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response deleteGeoLink(String guid) throws BaseResourceException
    {
        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            boolean suc = ServiceManager.getGeoLinkService().deleteGeoLink(guid);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete the geoLink with guid = " + guid);
                throw new InternalServerErrorException("Failed to delete the geoLink with guid = " + guid);
            }
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response deleteGeoLinks(String filter, String params, List<String> values) throws BaseResourceException
    {
        try {
            Long count = ServiceManager.getGeoLinkService().deleteGeoLinks(filter, params, values);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Delete count = " + count);
            return Response.ok(count.toString()).type(MediaType.TEXT_PLAIN).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }


    @Override
    public Response createGeoLinks(GeoLinkListStub geoLinks) throws BaseResourceException
    {
        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            List<GeoLinkStub> stubs = geoLinks.getList();
            List<GeoLink> beans = new ArrayList<GeoLink>();
            for(GeoLinkStub stub : stubs) {
                GeoLinkBean bean = convertGeoLinkStubToBean(stub);
                beans.add(bean);
            }
            Integer count = ServiceManager.getGeoLinkService().createGeoLinks(beans);
            return Response.ok(count.toString()).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }


    public static GeoLinkBean convertGeoLinkStubToBean(GeoLink stub)
    {
        GeoLinkBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null bean is returned.");
        } else {
            bean = new GeoLinkBean();
            bean.setGuid(stub.getGuid());
            bean.setShortLink(stub.getShortLink());
            bean.setShortUrl(stub.getShortUrl());
            bean.setGeoCoordinate(GeoCoordinateStructResourceUtil.convertGeoCoordinateStructStubToBean(stub.getGeoCoordinate()));
            bean.setGeoCell(CellLatitudeLongitudeResourceUtil.convertCellLatitudeLongitudeStubToBean(stub.getGeoCell()));
            bean.setStatus(stub.getStatus());
            bean.setCreatedTime(stub.getCreatedTime());
            bean.setModifiedTime(stub.getModifiedTime());
        }
        return bean;
    }

    public static List<GeoLinkBean> convertGeoLinkListStubToBeanList(GeoLinkListStub listStub)
    {
        if(listStub == null) {
            log.log(Level.INFO, "listStub is null. Null list is returned.");
            return null;
        } else {
            List<GeoLinkStub> stubList = listStub.getList();
            if(stubList == null) {
                log.log(Level.INFO, "Stub list is null. Null list is returned.");
                return null;                
            }
            List<GeoLinkBean> beanList = new ArrayList<GeoLinkBean>();
            if(stubList.isEmpty()) {
                log.log(Level.INFO, "Stub list is empty. Empty list is returned.");
            } else {
                for(GeoLinkStub stub : stubList) {
                    GeoLinkBean bean = convertGeoLinkStubToBean(stub);
                    beanList.add(bean);                            
                }
            }
            return beanList;
        }
    }


    // TBD
    // This needs to be implemented before url-encoded form create/update can be supported 
    public static GeoLinkBean convertFormParamsToBean(MultivaluedMap<String, String> formParams)
    {
        GeoLinkBean bean = new GeoLinkBean();
        if(formParams == null) {
            log.log(Level.INFO, "FormParams is null. Empty bean is returned.");
        } else {
            Iterator<MultivaluedMap.Entry<String,List<String>>> it = formParams.entrySet().iterator();
            while(it.hasNext()) {
                MultivaluedMap.Entry<String,List<String>> m =(MultivaluedMap.Entry<String,List<String>>) it.next();
                String key = (String) m.getKey();
                String val = null;
                List<String> list = m.getValue();
                if(list != null && list.size() > 0) {
                    val = list.get(0);
                    if(key.equals("guid")) {
                        bean.setGuid(val);                        
                    } else if(key.equals("shortLink")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setShortLink(v);
                    } else if(key.equals("shortUrl")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setShortUrl(v);
                    } else if(key.equals("geoCoordinate")) {
                        // TBD: Add "valueOf()" static method to the bean object.
                        //bean.setGeoCoordinate(GeoCoordinateBean.valueOf(val));
                    } else if(key.equals("geoCell")) {
                        // TBD: Add "valueOf()" static method to the bean object.
                        //bean.setGeoCell(GeoCellBean.valueOf(val));
                    } else if(key.equals("status")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setStatus(v);
                    } else if(key.equals("createdTime")) {
                        // TBD: Needs to use "parse()" methods.
                        //Long v = (Long) val;
                        //bean.setCreatedTime(v);
                    } else if(key.equals("modifiedTime")) {
                        // TBD: Needs to use "parse()" methods.
                        //Long v = (Long) val;
                        //bean.setModifiedTime(v);
                    }
                }
            }
        }
        return bean;
    }

}
