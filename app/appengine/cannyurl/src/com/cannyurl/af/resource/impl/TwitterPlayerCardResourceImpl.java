package com.cannyurl.af.resource.impl;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.ws.rs.Path;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;
import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.api.core.HttpContext;
import com.sun.jersey.api.json.JSONWithPadding;
import com.sun.jersey.oauth.server.OAuthServerRequest;
import com.sun.jersey.oauth.signature.OAuthSignatureException;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.CommonConstants;
import com.cannyurl.af.config.Config;
import com.cannyurl.af.auth.TwoLeggedOAuthProvider;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.exception.InternalServerErrorException;
import com.myurldb.ws.exception.NotImplementedException;
import com.myurldb.ws.exception.RequestConflictException;
import com.myurldb.ws.exception.RequestForbiddenException;
import com.myurldb.ws.exception.ResourceGoneException;
import com.myurldb.ws.exception.ResourceNotFoundException;
import com.myurldb.ws.exception.ServiceUnavailableException;
import com.myurldb.ws.exception.resource.BaseResourceException;
import com.myurldb.ws.resource.exception.BadRequestRsException;
import com.myurldb.ws.resource.exception.InternalServerErrorRsException;
import com.myurldb.ws.resource.exception.NotImplementedRsException;
import com.myurldb.ws.resource.exception.RequestConflictRsException;
import com.myurldb.ws.resource.exception.RequestForbiddenRsException;
import com.myurldb.ws.resource.exception.ResourceGoneRsException;
import com.myurldb.ws.resource.exception.ResourceNotFoundRsException;
import com.myurldb.ws.resource.exception.ServiceUnavailableRsException;
import com.myurldb.ws.resource.exception.UnauthorizedRsException;

import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.TwitterPlayerCard;
import com.myurldb.ws.stub.KeyListStub;
import com.myurldb.ws.stub.TwitterPlayerCardStub;
import com.myurldb.ws.stub.TwitterPlayerCardListStub;
import com.cannyurl.af.bean.TwitterCardAppInfoBean;
import com.cannyurl.af.bean.TwitterCardProductDataBean;
import com.cannyurl.af.bean.TwitterPlayerCardBean;
import com.cannyurl.af.service.manager.ServiceManager;
import com.cannyurl.af.resource.TwitterPlayerCardResource;
import com.cannyurl.af.resource.util.TwitterCardAppInfoResourceUtil;
import com.cannyurl.af.resource.util.TwitterCardProductDataResourceUtil;


@Path("/twitterPlayerCards/")
public class TwitterPlayerCardResourceImpl extends BaseResourceImpl implements TwitterPlayerCardResource
{
    private static final Logger log = Logger.getLogger(TwitterPlayerCardResourceImpl.class.getName());

    private UriInfo uriInfo;
    private HttpHeaders httpHeaders;
    private Request request;
    private String resourceUri;
    private boolean isRunningOnDevel;
    private HttpContext httpContext;

    public TwitterPlayerCardResourceImpl(@Context javax.ws.rs.core.UriInfo uriInfo, @Context javax.ws.rs.core.HttpHeaders httpHeaders, @Context javax.ws.rs.core.Request request, @Context ServletContext servletContext, @Context HttpContext httpContext)
    {
        this.uriInfo = uriInfo;
        this.httpHeaders = httpHeaders;
        this.request = request;
        this.resourceUri = uriInfo.getAbsolutePath().toString();

        String serverInfo = servletContext.getServerInfo();
        if(log.isLoggable(Level.INFO)) log.info("ServerInfo = " + serverInfo);
        if(serverInfo != null && serverInfo.contains("Development")) {
            isRunningOnDevel = true;
        } else {
            isRunningOnDevel = false;
        }        

        this.httpContext = httpContext;
    }

    private Response getTwitterPlayerCardList(List<TwitterPlayerCard> beans) throws BaseResourceException
    {
        if(beans == null) {
            log.log(Level.WARNING, "Failed to retrieve the list.");
            throw new RequestForbiddenRsException("Failed to retrieve the list.");
        }
        if(beans.size() == 0) {
            log.log(Level.INFO, "Retrieved an empty list.");
            return Response.ok(new TwitterPlayerCardListStub()).build();
        } else {
            List<TwitterPlayerCardStub> stubs = new ArrayList<TwitterPlayerCardStub>();
            Iterator<TwitterPlayerCard> it = beans.iterator();
            while(it.hasNext()) {
                TwitterPlayerCard bean = (TwitterPlayerCard) it.next();
                stubs.add(TwitterPlayerCardStub.convertBeanToStub(bean));
            }
            return Response.ok(new TwitterPlayerCardListStub(stubs)).build();
        }
    }

    // TBD
    private Response getTwitterPlayerCardListAsJsonp(List<TwitterPlayerCard> beans, String callback) throws BaseResourceException
    {
        if(beans == null) {
            log.log(Level.WARNING, "Failed to retrieve the list.");
            throw new RequestForbiddenRsException("Failed to retrieve the list.");
        }
        if(beans.size() == 0) {
            log.log(Level.INFO, "Retrieved an empty list.");
            return Response.ok( new JSONWithPadding( new TwitterPlayerCardListStub()) ).build();
        } else {
            List<TwitterPlayerCardStub> stubs = new ArrayList<TwitterPlayerCardStub>();
            Iterator<TwitterPlayerCard> it = beans.iterator();
            while(it.hasNext()) {
                TwitterPlayerCard bean = (TwitterPlayerCard) it.next();
                stubs.add(TwitterPlayerCardStub.convertBeanToStub(bean));
            }
            return Response.ok(new JSONWithPadding( new TwitterPlayerCardListStub(stubs), callback )).build();
        }
    }

    @Override
    public Response getAllTwitterPlayerCards(String ordering, Long offset, Integer count) throws BaseResourceException
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            if(isRunningOnDevel && isIgnoreAuth()) {
                log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
            } else {
                // Verify the signature first.
                try {
                    if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                        log.warning("Two-legged OAuth verification failed.");
                        throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                    }
                } catch (BadRequestException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                    throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
                } catch (BaseException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                    throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
                } catch (Exception e) {
                    log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                    throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
                }
            }
        }

        try {
            List<TwitterPlayerCard> beans = ServiceManager.getTwitterPlayerCardService().getAllTwitterPlayerCards(ordering, offset, count);
            return getTwitterPlayerCardList(beans);
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response getAllTwitterPlayerCardKeys(String ordering, Long offset, Integer count) throws BaseResourceException
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            if(isRunningOnDevel && isIgnoreAuth()) {
                log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
            } else {
                // Verify the signature first.
                try {
                    if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                        log.warning("Two-legged OAuth verification failed.");
                        throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                    }
                } catch (BadRequestException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                    throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
                } catch (BaseException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                    throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
                } catch (Exception e) {
                    log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                    throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
                }
            }
        }

        try {
            List<String> keys = ServiceManager.getTwitterPlayerCardService().getAllTwitterPlayerCardKeys(ordering, offset, count);
            return Response.ok(new KeyListStub(keys)).cacheControl(CacheControl.valueOf("no-cache")).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response findTwitterPlayerCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            if(isRunningOnDevel && isIgnoreAuth()) {
                log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
            } else {
                // Verify the signature first.
                try {
                    if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                        log.warning("Two-legged OAuth verification failed.");
                        throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                    }
                } catch (BadRequestException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                    throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
                } catch (BaseException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                    throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
                } catch (Exception e) {
                    log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                    throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
                }
            }
        }

        try {
            List<String> keys = ServiceManager.getTwitterPlayerCardService().findTwitterPlayerCardKeys(filter, ordering, params, values, grouping, unique, offset, count);
            return Response.ok(new KeyListStub(keys)).cacheControl(CacheControl.valueOf("no-cache")).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response findTwitterPlayerCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            if(isRunningOnDevel && isIgnoreAuth()) {
                log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
            } else {
                // Verify the signature first.
                try {
                    if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                        log.warning("Two-legged OAuth verification failed.");
                        throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                    }
                } catch (BadRequestException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                    throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
                } catch (BaseException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                    throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
                } catch (Exception e) {
                    log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                    throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
                }
            }
        }

        try {
            List<TwitterPlayerCard> beans = ServiceManager.getTwitterPlayerCardService().findTwitterPlayerCards(filter, ordering, params, values, grouping, unique, offset, count);
            return getTwitterPlayerCardList(beans);
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response findTwitterPlayerCardsAsJsonp(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, String callback)
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            if(isRunningOnDevel && isIgnoreAuth()) {
                log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
            } else {
                // Verify the signature first.
                try {
                    if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                        log.warning("Two-legged OAuth verification failed.");
                        throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                    }
                } catch (BadRequestException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                    throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
                } catch (BaseException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                    throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
                } catch (Exception e) {
                    log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                    throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
                }
            }
        }

        try {
            List<TwitterPlayerCard> beans = ServiceManager.getTwitterPlayerCardService().findTwitterPlayerCards(filter, ordering, params, values, grouping, unique, offset, count);
            return getTwitterPlayerCardListAsJsonp(beans, callback);
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            if(isRunningOnDevel && isIgnoreAuth()) {
                log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
            } else {
                // Verify the signature first.
                try {
                    if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                        log.warning("Two-legged OAuth verification failed.");
                        throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                    }
                } catch (BadRequestException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                    throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
                } catch (BaseException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                    throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
                } catch (Exception e) {
                    log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                    throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
                }
            }
        }

        try {
            Long count = ServiceManager.getTwitterPlayerCardService().getCount(filter, params, values, aggregate);
            return Response.ok(count.toString()).type(MediaType.TEXT_PLAIN).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

//    @Override
//    public Response getTwitterPlayerCardAsHtml(String guid) throws BaseResourceException
//    {
//        // TBD
//        throw new NotImplementedRsException("Html format currently not supported.", resourceUri);
//    }

    @Override
    public Response getTwitterPlayerCard(String guid) throws BaseResourceException
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            if(isRunningOnDevel && isIgnoreAuth()) {
                log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
            } else {
                // Verify the signature first.
                try {
                    if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                        log.warning("Two-legged OAuth verification failed.");
                        throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                    }
                } catch (BadRequestException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                    throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
                } catch (BaseException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                    throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
                } catch (Exception e) {
                    log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                    throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
                }
            }
        }

        try {
            TwitterPlayerCard bean = ServiceManager.getTwitterPlayerCardService().getTwitterPlayerCard(guid);
            TwitterPlayerCardStub stub = TwitterPlayerCardStub.convertBeanToStub(bean);

            EntityTag eTag = new EntityTag(Integer.toString(stub.hashCode()));
            Response.ResponseBuilder responseBuilder = request.evaluatePreconditions(eTag);
            if (responseBuilder != null) {  // Etag match
                log.info("TwitterPlayerCard stub object has not changed. Returning unmodified response code.");
                return responseBuilder.build();
            }
            log.info("Returning a full TwitterPlayerCard stub object.");

            //Date expirationDate = new Date(System.currentTimeMillis() + CommonConstants.CACHE_EXPIRATION_TIME);
            //ResponseBuilder builder = Response.ok(stub).tag(eTag).expires(expirationDate);
            ResponseBuilder builder = Response.ok(stub).tag(eTag);
            Date modifiedDate = null;
            if(bean.getModifiedTime() != null) {
                modifiedDate = new Date(bean.getModifiedTime());
            } else {
                if(bean.getCreatedTime() != null) {
                    modifiedDate = new Date(bean.getCreatedTime());
                } else {   // This should not happen.
                    modifiedDate = new Date();  // Just use "now".                        
                }
            }
            return builder.lastModified(modifiedDate).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response getTwitterPlayerCardAsJsonp(String guid, String callback) throws BaseResourceException
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            if(isRunningOnDevel && isIgnoreAuth()) {
                log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
            } else {
                // Verify the signature first.
                try {
                    if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                        log.warning("Two-legged OAuth verification failed.");
                        throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                    }
                } catch (BadRequestException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                    throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
                } catch (BaseException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                    throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
                } catch (Exception e) {
                    log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                    throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
                }
            }
        }

        try {
            TwitterPlayerCard bean = ServiceManager.getTwitterPlayerCardService().getTwitterPlayerCard(guid);
            TwitterPlayerCardStub stub = TwitterPlayerCardStub.convertBeanToStub(bean);

            EntityTag eTag = new EntityTag(Integer.toString(stub.hashCode()));
            Response.ResponseBuilder responseBuilder = request.evaluatePreconditions(eTag);
            if (responseBuilder != null) {  // Etag match
                log.info("TwitterPlayerCard stub object has not changed. Returning unmodified response code.");
                return responseBuilder.build();
            }
            log.info("Returning a full TwitterPlayerCard stub object.");

            //Date expirationDate = new Date(System.currentTimeMillis() + CommonConstants.CACHE_EXPIRATION_TIME);
            //ResponseBuilder builder = Response.ok(new JSONWithPadding( stub, callback )).tag(eTag).expires(expirationDate);
            ResponseBuilder builder = Response.ok(new JSONWithPadding( stub, callback )).tag(eTag);
            Date modifiedDate = null;
            if(bean.getModifiedTime() != null) {
                modifiedDate = new Date(bean.getModifiedTime());
            } else {
                if(bean.getCreatedTime() != null) {
                    modifiedDate = new Date(bean.getCreatedTime());
                } else {   // This should not happen.
                    modifiedDate = new Date();  // Just use "now".                        
                }
            }
            return builder.lastModified(modifiedDate).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    public Response getTwitterPlayerCard(String guid, String field) throws BaseResourceException
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            if(isRunningOnDevel && isIgnoreAuth()) {
                log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
            } else {
                // Verify the signature first.
                try {
                    if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                        log.warning("Two-legged OAuth verification failed.");
                        throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                    }
                } catch (BadRequestException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                    throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
                } catch (BaseException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                    throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
                } catch (Exception e) {
                    log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                    throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
                }
            }
        }

        try {
            if(guid != null) {
                guid = guid.toLowerCase();  // TBD: Validate/normalize guid....
            }
            if(field == null || field.trim().length() == 0) {
                return getTwitterPlayerCard(guid);
            }
            TwitterPlayerCard bean = ServiceManager.getTwitterPlayerCardService().getTwitterPlayerCard(guid);
            String value = null;
            if(bean != null) {
                if(field.equals("guid")) {
                    value = bean.getGuid();
                } else if(field.equals("card")) {
                    String fval = bean.getCard();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("url")) {
                    String fval = bean.getUrl();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("title")) {
                    String fval = bean.getTitle();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("description")) {
                    String fval = bean.getDescription();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("site")) {
                    String fval = bean.getSite();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("siteId")) {
                    String fval = bean.getSiteId();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("creator")) {
                    String fval = bean.getCreator();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("creatorId")) {
                    String fval = bean.getCreatorId();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("image")) {
                    String fval = bean.getImage();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("imageWidth")) {
                    Integer fval = bean.getImageWidth();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("imageHeight")) {
                    Integer fval = bean.getImageHeight();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("player")) {
                    String fval = bean.getPlayer();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("playerWidth")) {
                    Integer fval = bean.getPlayerWidth();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("playerHeight")) {
                    Integer fval = bean.getPlayerHeight();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("playerStream")) {
                    String fval = bean.getPlayerStream();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("playerStreamContentType")) {
                    String fval = bean.getPlayerStreamContentType();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("createdTime")) {
                    Long fval = bean.getCreatedTime();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("modifiedTime")) {
                    Long fval = bean.getModifiedTime();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                }
            }
            //Date expirationDate = new Date(System.currentTimeMillis() + CommonConstants.CACHE_EXPIRATION_TIME);
            //ResponseBuilder builder = Response.ok(value).type(MediaType.TEXT_PLAIN).expires(expirationDate);
            ResponseBuilder builder = Response.ok(value).type(MediaType.TEXT_PLAIN);
            Date modifiedDate = null;
            if(bean.getModifiedTime() != null) {
                modifiedDate = new Date(bean.getModifiedTime());
            } else {
                if(bean.getCreatedTime() != null) {
                    modifiedDate = new Date(bean.getCreatedTime());
                } else {   // This should not happen.
                    modifiedDate = new Date();  // Just use "now".                        
                }
            }
            return builder.lastModified(modifiedDate).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }        
    }

    // TBD
    @Override
    public Response constructTwitterPlayerCard(TwitterPlayerCardStub twitterPlayerCard) throws BaseResourceException
    {
        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            TwitterPlayerCardBean bean = convertTwitterPlayerCardStubToBean(twitterPlayerCard);
            bean = (TwitterPlayerCardBean) ServiceManager.getTwitterPlayerCardService().constructTwitterPlayerCard(bean);
            twitterPlayerCard = TwitterPlayerCardStub.convertBeanToStub(bean);
            String guid = twitterPlayerCard.getGuid();
            URI createdUri = URI.create(resourceUri + "/" + guid);
            return Response.created(createdUri).entity(twitterPlayerCard).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response createTwitterPlayerCard(TwitterPlayerCardStub twitterPlayerCard) throws BaseResourceException
    {
        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            TwitterPlayerCardBean bean = convertTwitterPlayerCardStubToBean(twitterPlayerCard);
            String guid = ServiceManager.getTwitterPlayerCardService().createTwitterPlayerCard(bean);
            URI createdUri = URI.create(resourceUri + "/" + guid);
            return Response.created(createdUri).entity(guid).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

//    @Override
    public Response createTwitterPlayerCard(MultivaluedMap<String, String> formParams) throws BaseResourceException
    {
        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            TwitterPlayerCardBean bean = convertFormParamsToBean(formParams);
            String guid = ServiceManager.getTwitterPlayerCardService().createTwitterPlayerCard(bean);
            URI createdUri = URI.create(resourceUri + "/" + guid);
            return Response.created(createdUri).entity(guid).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    // TBD
    @Override
    public Response refreshTwitterPlayerCard(String guid, TwitterPlayerCardStub twitterPlayerCard) throws BaseResourceException
    {
        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            if(twitterPlayerCard == null || !guid.equals(twitterPlayerCard.getGuid())) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Path param guid = " + guid + " is different from twitterPlayerCard guid = " + twitterPlayerCard.getGuid());
                throw new RequestForbiddenException("Failed to refresh the twitterPlayerCard with guid = " + guid);
            }
            TwitterPlayerCardBean bean = convertTwitterPlayerCardStubToBean(twitterPlayerCard);
            bean = (TwitterPlayerCardBean) ServiceManager.getTwitterPlayerCardService().refreshTwitterPlayerCard(bean);
            if(bean == null) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to refresh the twitterPlayerCard with guid = " + guid);
                throw new InternalServerErrorException("Failed to refresh the twitterPlayerCard with guid = " + guid);
            }
            twitterPlayerCard = TwitterPlayerCardStub.convertBeanToStub(bean);
            return Response.ok(twitterPlayerCard).build();  // ???
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateTwitterPlayerCard(String guid, TwitterPlayerCardStub twitterPlayerCard) throws BaseResourceException
    {
        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            if(twitterPlayerCard == null || !guid.equals(twitterPlayerCard.getGuid())) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Path param guid = " + guid + " is different from twitterPlayerCard guid = " + twitterPlayerCard.getGuid());
                throw new RequestForbiddenException("Failed to update the twitterPlayerCard with guid = " + guid);
            }
            TwitterPlayerCardBean bean = convertTwitterPlayerCardStubToBean(twitterPlayerCard);
            boolean suc = ServiceManager.getTwitterPlayerCardService().updateTwitterPlayerCard(bean);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to update the twitterPlayerCard with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the twitterPlayerCard with guid = " + guid);
            }
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateTwitterPlayerCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, String player, Integer playerWidth, Integer playerHeight, String playerStream, String playerStreamContentType)
    {
        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            /*
            boolean suc = ServiceManager.getTwitterPlayerCardService().updateTwitterPlayerCard(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, player, playerWidth, playerHeight, playerStream, playerStreamContentType);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to update the twitterPlayerCard with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the twitterPlayerCard with guid = " + guid);
            }
            return Response.noContent().build();
            */
            throw new NotImplementedException("This method has not been implemented yet.");
        //} catch(BadRequestException ex) {
        //    throw new BadRequestRsException(ex, resourceUri);
        //} catch(ResourceNotFoundException ex) {
        //    throw new ResourceNotFoundRsException(ex, resourceUri);
        //} catch(ResourceGoneException ex) {
        //    throw new ResourceGoneRsException(ex, resourceUri);
        //} catch(RequestForbiddenException ex) {
        //    throw new RequestForbiddenRsException(ex, resourceUri);
        //} catch(RequestConflictException ex) {
        //    throw new RequestConflictRsException(ex, resourceUri);
        //} catch(ServiceUnavailableException ex) {
        //    throw new ServiceUnavailableRsException(ex, resourceUri);
        //} catch(InternalServerErrorException ex) {
        //    throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(NotImplementedException ex) {
            throw new NotImplementedRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

//    @Override
    public Response updateTwitterPlayerCard(String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException
    {
        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            TwitterPlayerCardBean bean = convertFormParamsToBean(formParams);
            boolean suc = ServiceManager.getTwitterPlayerCardService().updateTwitterPlayerCard(bean);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to update the twitterPlayerCard with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the twitterPlayerCard with guid = " + guid);
            }
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response deleteTwitterPlayerCard(String guid) throws BaseResourceException
    {
        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            boolean suc = ServiceManager.getTwitterPlayerCardService().deleteTwitterPlayerCard(guid);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete the twitterPlayerCard with guid = " + guid);
                throw new InternalServerErrorException("Failed to delete the twitterPlayerCard with guid = " + guid);
            }
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response deleteTwitterPlayerCards(String filter, String params, List<String> values) throws BaseResourceException
    {
        try {
            Long count = ServiceManager.getTwitterPlayerCardService().deleteTwitterPlayerCards(filter, params, values);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Delete count = " + count);
            return Response.ok(count.toString()).type(MediaType.TEXT_PLAIN).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }


    @Override
    public Response createTwitterPlayerCards(TwitterPlayerCardListStub twitterPlayerCards) throws BaseResourceException
    {
        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            List<TwitterPlayerCardStub> stubs = twitterPlayerCards.getList();
            List<TwitterPlayerCard> beans = new ArrayList<TwitterPlayerCard>();
            for(TwitterPlayerCardStub stub : stubs) {
                TwitterPlayerCardBean bean = convertTwitterPlayerCardStubToBean(stub);
                beans.add(bean);
            }
            Integer count = ServiceManager.getTwitterPlayerCardService().createTwitterPlayerCards(beans);
            return Response.ok(count.toString()).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }


    public static TwitterPlayerCardBean convertTwitterPlayerCardStubToBean(TwitterPlayerCard stub)
    {
        TwitterPlayerCardBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null bean is returned.");
        } else {
            bean = new TwitterPlayerCardBean();
            bean.setGuid(stub.getGuid());
            bean.setCard(stub.getCard());
            bean.setUrl(stub.getUrl());
            bean.setTitle(stub.getTitle());
            bean.setDescription(stub.getDescription());
            bean.setSite(stub.getSite());
            bean.setSiteId(stub.getSiteId());
            bean.setCreator(stub.getCreator());
            bean.setCreatorId(stub.getCreatorId());
            bean.setImage(stub.getImage());
            bean.setImageWidth(stub.getImageWidth());
            bean.setImageHeight(stub.getImageHeight());
            bean.setPlayer(stub.getPlayer());
            bean.setPlayerWidth(stub.getPlayerWidth());
            bean.setPlayerHeight(stub.getPlayerHeight());
            bean.setPlayerStream(stub.getPlayerStream());
            bean.setPlayerStreamContentType(stub.getPlayerStreamContentType());
            bean.setCreatedTime(stub.getCreatedTime());
            bean.setModifiedTime(stub.getModifiedTime());
        }
        return bean;
    }

    public static List<TwitterPlayerCardBean> convertTwitterPlayerCardListStubToBeanList(TwitterPlayerCardListStub listStub)
    {
        if(listStub == null) {
            log.log(Level.INFO, "listStub is null. Null list is returned.");
            return null;
        } else {
            List<TwitterPlayerCardStub> stubList = listStub.getList();
            if(stubList == null) {
                log.log(Level.INFO, "Stub list is null. Null list is returned.");
                return null;                
            }
            List<TwitterPlayerCardBean> beanList = new ArrayList<TwitterPlayerCardBean>();
            if(stubList.isEmpty()) {
                log.log(Level.INFO, "Stub list is empty. Empty list is returned.");
            } else {
                for(TwitterPlayerCardStub stub : stubList) {
                    TwitterPlayerCardBean bean = convertTwitterPlayerCardStubToBean(stub);
                    beanList.add(bean);                            
                }
            }
            return beanList;
        }
    }


    // TBD
    // This needs to be implemented before url-encoded form create/update can be supported 
    public static TwitterPlayerCardBean convertFormParamsToBean(MultivaluedMap<String, String> formParams)
    {
        TwitterPlayerCardBean bean = new TwitterPlayerCardBean();
        if(formParams == null) {
            log.log(Level.INFO, "FormParams is null. Empty bean is returned.");
        } else {
            Iterator<MultivaluedMap.Entry<String,List<String>>> it = formParams.entrySet().iterator();
            while(it.hasNext()) {
                MultivaluedMap.Entry<String,List<String>> m =(MultivaluedMap.Entry<String,List<String>>) it.next();
                String key = (String) m.getKey();
                String val = null;
                List<String> list = m.getValue();
                if(list != null && list.size() > 0) {
                    val = list.get(0);
                    if(key.equals("guid")) {
                        bean.setGuid(val);                        
                    } else if(key.equals("card")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setCard(v);
                    } else if(key.equals("url")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setUrl(v);
                    } else if(key.equals("title")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setTitle(v);
                    } else if(key.equals("description")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setDescription(v);
                    } else if(key.equals("site")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setSite(v);
                    } else if(key.equals("siteId")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setSiteId(v);
                    } else if(key.equals("creator")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setCreator(v);
                    } else if(key.equals("creatorId")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setCreatorId(v);
                    } else if(key.equals("image")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setImage(v);
                    } else if(key.equals("imageWidth")) {
                        // TBD: Needs to use "parse()" methods.
                        //Integer v = (Integer) val;
                        //bean.setImageWidth(v);
                    } else if(key.equals("imageHeight")) {
                        // TBD: Needs to use "parse()" methods.
                        //Integer v = (Integer) val;
                        //bean.setImageHeight(v);
                    } else if(key.equals("player")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setPlayer(v);
                    } else if(key.equals("playerWidth")) {
                        // TBD: Needs to use "parse()" methods.
                        //Integer v = (Integer) val;
                        //bean.setPlayerWidth(v);
                    } else if(key.equals("playerHeight")) {
                        // TBD: Needs to use "parse()" methods.
                        //Integer v = (Integer) val;
                        //bean.setPlayerHeight(v);
                    } else if(key.equals("playerStream")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setPlayerStream(v);
                    } else if(key.equals("playerStreamContentType")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setPlayerStreamContentType(v);
                    } else if(key.equals("createdTime")) {
                        // TBD: Needs to use "parse()" methods.
                        //Long v = (Long) val;
                        //bean.setCreatedTime(v);
                    } else if(key.equals("modifiedTime")) {
                        // TBD: Needs to use "parse()" methods.
                        //Long v = (Long) val;
                        //bean.setModifiedTime(v);
                    }
                }
            }
        }
        return bean;
    }

}
