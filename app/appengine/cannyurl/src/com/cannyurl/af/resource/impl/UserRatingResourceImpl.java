package com.cannyurl.af.resource.impl;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.ws.rs.Path;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;
import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.api.core.HttpContext;
import com.sun.jersey.api.json.JSONWithPadding;
import com.sun.jersey.oauth.server.OAuthServerRequest;
import com.sun.jersey.oauth.signature.OAuthSignatureException;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.CommonConstants;
import com.cannyurl.af.config.Config;
import com.cannyurl.af.auth.TwoLeggedOAuthProvider;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.exception.InternalServerErrorException;
import com.myurldb.ws.exception.NotImplementedException;
import com.myurldb.ws.exception.RequestConflictException;
import com.myurldb.ws.exception.RequestForbiddenException;
import com.myurldb.ws.exception.ResourceGoneException;
import com.myurldb.ws.exception.ResourceNotFoundException;
import com.myurldb.ws.exception.ServiceUnavailableException;
import com.myurldb.ws.exception.resource.BaseResourceException;
import com.myurldb.ws.resource.exception.BadRequestRsException;
import com.myurldb.ws.resource.exception.InternalServerErrorRsException;
import com.myurldb.ws.resource.exception.NotImplementedRsException;
import com.myurldb.ws.resource.exception.RequestConflictRsException;
import com.myurldb.ws.resource.exception.RequestForbiddenRsException;
import com.myurldb.ws.resource.exception.ResourceGoneRsException;
import com.myurldb.ws.resource.exception.ResourceNotFoundRsException;
import com.myurldb.ws.resource.exception.ServiceUnavailableRsException;
import com.myurldb.ws.resource.exception.UnauthorizedRsException;

import com.myurldb.ws.UserRating;
import com.myurldb.ws.stub.KeyListStub;
import com.myurldb.ws.stub.UserRatingStub;
import com.myurldb.ws.stub.UserRatingListStub;
import com.cannyurl.af.bean.UserRatingBean;
import com.cannyurl.af.service.manager.ServiceManager;
import com.cannyurl.af.resource.UserRatingResource;


@Path("/userRatings/")
public class UserRatingResourceImpl extends BaseResourceImpl implements UserRatingResource
{
    private static final Logger log = Logger.getLogger(UserRatingResourceImpl.class.getName());

    private UriInfo uriInfo;
    private HttpHeaders httpHeaders;
    private Request request;
    private String resourceUri;
    private boolean isRunningOnDevel;
    private HttpContext httpContext;

    public UserRatingResourceImpl(@Context javax.ws.rs.core.UriInfo uriInfo, @Context javax.ws.rs.core.HttpHeaders httpHeaders, @Context javax.ws.rs.core.Request request, @Context ServletContext servletContext, @Context HttpContext httpContext)
    {
        this.uriInfo = uriInfo;
        this.httpHeaders = httpHeaders;
        this.request = request;
        this.resourceUri = uriInfo.getAbsolutePath().toString();

        String serverInfo = servletContext.getServerInfo();
        if(log.isLoggable(Level.INFO)) log.info("ServerInfo = " + serverInfo);
        if(serverInfo != null && serverInfo.contains("Development")) {
            isRunningOnDevel = true;
        } else {
            isRunningOnDevel = false;
        }        

        this.httpContext = httpContext;
    }

    private Response getUserRatingList(List<UserRating> beans) throws BaseResourceException
    {
        if(beans == null) {
            log.log(Level.WARNING, "Failed to retrieve the list.");
            throw new RequestForbiddenRsException("Failed to retrieve the list.");
        }
        if(beans.size() == 0) {
            log.log(Level.INFO, "Retrieved an empty list.");
            return Response.ok(new UserRatingListStub()).build();
        } else {
            List<UserRatingStub> stubs = new ArrayList<UserRatingStub>();
            Iterator<UserRating> it = beans.iterator();
            while(it.hasNext()) {
                UserRating bean = (UserRating) it.next();
                stubs.add(UserRatingStub.convertBeanToStub(bean));
            }
            return Response.ok(new UserRatingListStub(stubs)).build();
        }
    }

    // TBD
    private Response getUserRatingListAsJsonp(List<UserRating> beans, String callback) throws BaseResourceException
    {
        if(beans == null) {
            log.log(Level.WARNING, "Failed to retrieve the list.");
            throw new RequestForbiddenRsException("Failed to retrieve the list.");
        }
        if(beans.size() == 0) {
            log.log(Level.INFO, "Retrieved an empty list.");
            return Response.ok( new JSONWithPadding( new UserRatingListStub()) ).build();
        } else {
            List<UserRatingStub> stubs = new ArrayList<UserRatingStub>();
            Iterator<UserRating> it = beans.iterator();
            while(it.hasNext()) {
                UserRating bean = (UserRating) it.next();
                stubs.add(UserRatingStub.convertBeanToStub(bean));
            }
            return Response.ok(new JSONWithPadding( new UserRatingListStub(stubs), callback )).build();
        }
    }

    @Override
    public Response getAllUserRatings(String ordering, Long offset, Integer count) throws BaseResourceException
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            if(isRunningOnDevel && isIgnoreAuth()) {
                log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
            } else {
                // Verify the signature first.
                try {
                    if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                        log.warning("Two-legged OAuth verification failed.");
                        throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                    }
                } catch (BadRequestException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                    throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
                } catch (BaseException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                    throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
                } catch (Exception e) {
                    log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                    throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
                }
            }
        }

        try {
            List<UserRating> beans = ServiceManager.getUserRatingService().getAllUserRatings(ordering, offset, count);
            return getUserRatingList(beans);
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response getAllUserRatingKeys(String ordering, Long offset, Integer count) throws BaseResourceException
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            if(isRunningOnDevel && isIgnoreAuth()) {
                log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
            } else {
                // Verify the signature first.
                try {
                    if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                        log.warning("Two-legged OAuth verification failed.");
                        throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                    }
                } catch (BadRequestException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                    throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
                } catch (BaseException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                    throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
                } catch (Exception e) {
                    log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                    throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
                }
            }
        }

        try {
            List<String> keys = ServiceManager.getUserRatingService().getAllUserRatingKeys(ordering, offset, count);
            return Response.ok(new KeyListStub(keys)).cacheControl(CacheControl.valueOf("no-cache")).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response findUserRatingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            if(isRunningOnDevel && isIgnoreAuth()) {
                log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
            } else {
                // Verify the signature first.
                try {
                    if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                        log.warning("Two-legged OAuth verification failed.");
                        throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                    }
                } catch (BadRequestException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                    throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
                } catch (BaseException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                    throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
                } catch (Exception e) {
                    log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                    throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
                }
            }
        }

        try {
            List<String> keys = ServiceManager.getUserRatingService().findUserRatingKeys(filter, ordering, params, values, grouping, unique, offset, count);
            return Response.ok(new KeyListStub(keys)).cacheControl(CacheControl.valueOf("no-cache")).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response findUserRatings(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            if(isRunningOnDevel && isIgnoreAuth()) {
                log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
            } else {
                // Verify the signature first.
                try {
                    if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                        log.warning("Two-legged OAuth verification failed.");
                        throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                    }
                } catch (BadRequestException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                    throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
                } catch (BaseException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                    throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
                } catch (Exception e) {
                    log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                    throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
                }
            }
        }

        try {
            List<UserRating> beans = ServiceManager.getUserRatingService().findUserRatings(filter, ordering, params, values, grouping, unique, offset, count);
            return getUserRatingList(beans);
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response findUserRatingsAsJsonp(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, String callback)
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            if(isRunningOnDevel && isIgnoreAuth()) {
                log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
            } else {
                // Verify the signature first.
                try {
                    if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                        log.warning("Two-legged OAuth verification failed.");
                        throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                    }
                } catch (BadRequestException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                    throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
                } catch (BaseException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                    throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
                } catch (Exception e) {
                    log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                    throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
                }
            }
        }

        try {
            List<UserRating> beans = ServiceManager.getUserRatingService().findUserRatings(filter, ordering, params, values, grouping, unique, offset, count);
            return getUserRatingListAsJsonp(beans, callback);
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            if(isRunningOnDevel && isIgnoreAuth()) {
                log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
            } else {
                // Verify the signature first.
                try {
                    if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                        log.warning("Two-legged OAuth verification failed.");
                        throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                    }
                } catch (BadRequestException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                    throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
                } catch (BaseException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                    throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
                } catch (Exception e) {
                    log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                    throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
                }
            }
        }

        try {
            Long count = ServiceManager.getUserRatingService().getCount(filter, params, values, aggregate);
            return Response.ok(count.toString()).type(MediaType.TEXT_PLAIN).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

//    @Override
//    public Response getUserRatingAsHtml(String guid) throws BaseResourceException
//    {
//        // TBD
//        throw new NotImplementedRsException("Html format currently not supported.", resourceUri);
//    }

    @Override
    public Response getUserRating(String guid) throws BaseResourceException
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            if(isRunningOnDevel && isIgnoreAuth()) {
                log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
            } else {
                // Verify the signature first.
                try {
                    if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                        log.warning("Two-legged OAuth verification failed.");
                        throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                    }
                } catch (BadRequestException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                    throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
                } catch (BaseException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                    throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
                } catch (Exception e) {
                    log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                    throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
                }
            }
        }

        try {
            UserRating bean = ServiceManager.getUserRatingService().getUserRating(guid);
            UserRatingStub stub = UserRatingStub.convertBeanToStub(bean);

            EntityTag eTag = new EntityTag(Integer.toString(stub.hashCode()));
            Response.ResponseBuilder responseBuilder = request.evaluatePreconditions(eTag);
            if (responseBuilder != null) {  // Etag match
                log.info("UserRating stub object has not changed. Returning unmodified response code.");
                return responseBuilder.build();
            }
            log.info("Returning a full UserRating stub object.");

            //Date expirationDate = new Date(System.currentTimeMillis() + CommonConstants.CACHE_EXPIRATION_TIME);
            //ResponseBuilder builder = Response.ok(stub).tag(eTag).expires(expirationDate);
            ResponseBuilder builder = Response.ok(stub).tag(eTag);
            Date modifiedDate = null;
            if(bean.getModifiedTime() != null) {
                modifiedDate = new Date(bean.getModifiedTime());
            } else {
                if(bean.getCreatedTime() != null) {
                    modifiedDate = new Date(bean.getCreatedTime());
                } else {   // This should not happen.
                    modifiedDate = new Date();  // Just use "now".                        
                }
            }
            return builder.lastModified(modifiedDate).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response getUserRatingAsJsonp(String guid, String callback) throws BaseResourceException
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            if(isRunningOnDevel && isIgnoreAuth()) {
                log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
            } else {
                // Verify the signature first.
                try {
                    if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                        log.warning("Two-legged OAuth verification failed.");
                        throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                    }
                } catch (BadRequestException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                    throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
                } catch (BaseException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                    throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
                } catch (Exception e) {
                    log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                    throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
                }
            }
        }

        try {
            UserRating bean = ServiceManager.getUserRatingService().getUserRating(guid);
            UserRatingStub stub = UserRatingStub.convertBeanToStub(bean);

            EntityTag eTag = new EntityTag(Integer.toString(stub.hashCode()));
            Response.ResponseBuilder responseBuilder = request.evaluatePreconditions(eTag);
            if (responseBuilder != null) {  // Etag match
                log.info("UserRating stub object has not changed. Returning unmodified response code.");
                return responseBuilder.build();
            }
            log.info("Returning a full UserRating stub object.");

            //Date expirationDate = new Date(System.currentTimeMillis() + CommonConstants.CACHE_EXPIRATION_TIME);
            //ResponseBuilder builder = Response.ok(new JSONWithPadding( stub, callback )).tag(eTag).expires(expirationDate);
            ResponseBuilder builder = Response.ok(new JSONWithPadding( stub, callback )).tag(eTag);
            Date modifiedDate = null;
            if(bean.getModifiedTime() != null) {
                modifiedDate = new Date(bean.getModifiedTime());
            } else {
                if(bean.getCreatedTime() != null) {
                    modifiedDate = new Date(bean.getCreatedTime());
                } else {   // This should not happen.
                    modifiedDate = new Date();  // Just use "now".                        
                }
            }
            return builder.lastModified(modifiedDate).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    public Response getUserRating(String guid, String field) throws BaseResourceException
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            if(isRunningOnDevel && isIgnoreAuth()) {
                log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
            } else {
                // Verify the signature first.
                try {
                    if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                        log.warning("Two-legged OAuth verification failed.");
                        throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                    }
                } catch (BadRequestException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                    throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
                } catch (BaseException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                    throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
                } catch (Exception e) {
                    log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                    throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
                }
            }
        }

        try {
            if(guid != null) {
                guid = guid.toLowerCase();  // TBD: Validate/normalize guid....
            }
            if(field == null || field.trim().length() == 0) {
                return getUserRating(guid);
            }
            UserRating bean = ServiceManager.getUserRatingService().getUserRating(guid);
            String value = null;
            if(bean != null) {
                if(field.equals("guid")) {
                    value = bean.getGuid();
                } else if(field.equals("user")) {
                    String fval = bean.getUser();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("rating")) {
                    Double fval = bean.getRating();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("note")) {
                    String fval = bean.getNote();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("ratedTime")) {
                    Long fval = bean.getRatedTime();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("createdTime")) {
                    Long fval = bean.getCreatedTime();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("modifiedTime")) {
                    Long fval = bean.getModifiedTime();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                }
            }
            //Date expirationDate = new Date(System.currentTimeMillis() + CommonConstants.CACHE_EXPIRATION_TIME);
            //ResponseBuilder builder = Response.ok(value).type(MediaType.TEXT_PLAIN).expires(expirationDate);
            ResponseBuilder builder = Response.ok(value).type(MediaType.TEXT_PLAIN);
            Date modifiedDate = null;
            if(bean.getModifiedTime() != null) {
                modifiedDate = new Date(bean.getModifiedTime());
            } else {
                if(bean.getCreatedTime() != null) {
                    modifiedDate = new Date(bean.getCreatedTime());
                } else {   // This should not happen.
                    modifiedDate = new Date();  // Just use "now".                        
                }
            }
            return builder.lastModified(modifiedDate).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }        
    }

    // TBD
    @Override
    public Response constructUserRating(UserRatingStub userRating) throws BaseResourceException
    {
        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            UserRatingBean bean = convertUserRatingStubToBean(userRating);
            bean = (UserRatingBean) ServiceManager.getUserRatingService().constructUserRating(bean);
            userRating = UserRatingStub.convertBeanToStub(bean);
            String guid = userRating.getGuid();
            URI createdUri = URI.create(resourceUri + "/" + guid);
            return Response.created(createdUri).entity(userRating).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response createUserRating(UserRatingStub userRating) throws BaseResourceException
    {
        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            UserRatingBean bean = convertUserRatingStubToBean(userRating);
            String guid = ServiceManager.getUserRatingService().createUserRating(bean);
            URI createdUri = URI.create(resourceUri + "/" + guid);
            return Response.created(createdUri).entity(guid).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

//    @Override
    public Response createUserRating(MultivaluedMap<String, String> formParams) throws BaseResourceException
    {
        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            UserRatingBean bean = convertFormParamsToBean(formParams);
            String guid = ServiceManager.getUserRatingService().createUserRating(bean);
            URI createdUri = URI.create(resourceUri + "/" + guid);
            return Response.created(createdUri).entity(guid).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    // TBD
    @Override
    public Response refreshUserRating(String guid, UserRatingStub userRating) throws BaseResourceException
    {
        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            if(userRating == null || !guid.equals(userRating.getGuid())) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Path param guid = " + guid + " is different from userRating guid = " + userRating.getGuid());
                throw new RequestForbiddenException("Failed to refresh the userRating with guid = " + guid);
            }
            UserRatingBean bean = convertUserRatingStubToBean(userRating);
            bean = (UserRatingBean) ServiceManager.getUserRatingService().refreshUserRating(bean);
            if(bean == null) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to refresh the userRating with guid = " + guid);
                throw new InternalServerErrorException("Failed to refresh the userRating with guid = " + guid);
            }
            userRating = UserRatingStub.convertBeanToStub(bean);
            return Response.ok(userRating).build();  // ???
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateUserRating(String guid, UserRatingStub userRating) throws BaseResourceException
    {
        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            if(userRating == null || !guid.equals(userRating.getGuid())) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Path param guid = " + guid + " is different from userRating guid = " + userRating.getGuid());
                throw new RequestForbiddenException("Failed to update the userRating with guid = " + guid);
            }
            UserRatingBean bean = convertUserRatingStubToBean(userRating);
            boolean suc = ServiceManager.getUserRatingService().updateUserRating(bean);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to update the userRating with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the userRating with guid = " + guid);
            }
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateUserRating(String guid, String user, Double rating, String note, Long ratedTime)
    {
        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            /*
            boolean suc = ServiceManager.getUserRatingService().updateUserRating(guid, user, rating, note, ratedTime);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to update the userRating with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the userRating with guid = " + guid);
            }
            return Response.noContent().build();
            */
            throw new NotImplementedException("This method has not been implemented yet.");
        //} catch(BadRequestException ex) {
        //    throw new BadRequestRsException(ex, resourceUri);
        //} catch(ResourceNotFoundException ex) {
        //    throw new ResourceNotFoundRsException(ex, resourceUri);
        //} catch(ResourceGoneException ex) {
        //    throw new ResourceGoneRsException(ex, resourceUri);
        //} catch(RequestForbiddenException ex) {
        //    throw new RequestForbiddenRsException(ex, resourceUri);
        //} catch(RequestConflictException ex) {
        //    throw new RequestConflictRsException(ex, resourceUri);
        //} catch(ServiceUnavailableException ex) {
        //    throw new ServiceUnavailableRsException(ex, resourceUri);
        //} catch(InternalServerErrorException ex) {
        //    throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(NotImplementedException ex) {
            throw new NotImplementedRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

//    @Override
    public Response updateUserRating(String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException
    {
        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            UserRatingBean bean = convertFormParamsToBean(formParams);
            boolean suc = ServiceManager.getUserRatingService().updateUserRating(bean);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to update the userRating with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the userRating with guid = " + guid);
            }
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response deleteUserRating(String guid) throws BaseResourceException
    {
        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            boolean suc = ServiceManager.getUserRatingService().deleteUserRating(guid);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete the userRating with guid = " + guid);
                throw new InternalServerErrorException("Failed to delete the userRating with guid = " + guid);
            }
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response deleteUserRatings(String filter, String params, List<String> values) throws BaseResourceException
    {
        try {
            Long count = ServiceManager.getUserRatingService().deleteUserRatings(filter, params, values);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Delete count = " + count);
            return Response.ok(count.toString()).type(MediaType.TEXT_PLAIN).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }


    @Override
    public Response createUserRatings(UserRatingListStub userRatings) throws BaseResourceException
    {
        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            List<UserRatingStub> stubs = userRatings.getList();
            List<UserRating> beans = new ArrayList<UserRating>();
            for(UserRatingStub stub : stubs) {
                UserRatingBean bean = convertUserRatingStubToBean(stub);
                beans.add(bean);
            }
            Integer count = ServiceManager.getUserRatingService().createUserRatings(beans);
            return Response.ok(count.toString()).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }


    public static UserRatingBean convertUserRatingStubToBean(UserRating stub)
    {
        UserRatingBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null bean is returned.");
        } else {
            bean = new UserRatingBean();
            bean.setGuid(stub.getGuid());
            bean.setUser(stub.getUser());
            bean.setRating(stub.getRating());
            bean.setNote(stub.getNote());
            bean.setRatedTime(stub.getRatedTime());
            bean.setCreatedTime(stub.getCreatedTime());
            bean.setModifiedTime(stub.getModifiedTime());
        }
        return bean;
    }

    public static List<UserRatingBean> convertUserRatingListStubToBeanList(UserRatingListStub listStub)
    {
        if(listStub == null) {
            log.log(Level.INFO, "listStub is null. Null list is returned.");
            return null;
        } else {
            List<UserRatingStub> stubList = listStub.getList();
            if(stubList == null) {
                log.log(Level.INFO, "Stub list is null. Null list is returned.");
                return null;                
            }
            List<UserRatingBean> beanList = new ArrayList<UserRatingBean>();
            if(stubList.isEmpty()) {
                log.log(Level.INFO, "Stub list is empty. Empty list is returned.");
            } else {
                for(UserRatingStub stub : stubList) {
                    UserRatingBean bean = convertUserRatingStubToBean(stub);
                    beanList.add(bean);                            
                }
            }
            return beanList;
        }
    }


    // TBD
    // This needs to be implemented before url-encoded form create/update can be supported 
    public static UserRatingBean convertFormParamsToBean(MultivaluedMap<String, String> formParams)
    {
        UserRatingBean bean = new UserRatingBean();
        if(formParams == null) {
            log.log(Level.INFO, "FormParams is null. Empty bean is returned.");
        } else {
            Iterator<MultivaluedMap.Entry<String,List<String>>> it = formParams.entrySet().iterator();
            while(it.hasNext()) {
                MultivaluedMap.Entry<String,List<String>> m =(MultivaluedMap.Entry<String,List<String>>) it.next();
                String key = (String) m.getKey();
                String val = null;
                List<String> list = m.getValue();
                if(list != null && list.size() > 0) {
                    val = list.get(0);
                    if(key.equals("guid")) {
                        bean.setGuid(val);                        
                    } else if(key.equals("user")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setUser(v);
                    } else if(key.equals("rating")) {
                        // TBD: Needs to use "parse()" methods.
                        //Double v = (Double) val;
                        //bean.setRating(v);
                    } else if(key.equals("note")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setNote(v);
                    } else if(key.equals("ratedTime")) {
                        // TBD: Needs to use "parse()" methods.
                        //Long v = (Long) val;
                        //bean.setRatedTime(v);
                    } else if(key.equals("createdTime")) {
                        // TBD: Needs to use "parse()" methods.
                        //Long v = (Long) val;
                        //bean.setCreatedTime(v);
                    } else if(key.equals("modifiedTime")) {
                        // TBD: Needs to use "parse()" methods.
                        //Long v = (Long) val;
                        //bean.setModifiedTime(v);
                    }
                }
            }
        }
        return bean;
    }

}
