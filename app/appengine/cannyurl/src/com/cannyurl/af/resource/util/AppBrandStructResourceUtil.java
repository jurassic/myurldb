package com.cannyurl.af.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.CommonConstants;
import com.myurldb.ws.AppBrandStruct;
import com.myurldb.ws.stub.AppBrandStructStub;
import com.cannyurl.af.bean.AppBrandStructBean;


public class AppBrandStructResourceUtil
{
    private static final Logger log = Logger.getLogger(AppBrandStructResourceUtil.class.getName());

    // Static methods only.
    private AppBrandStructResourceUtil() {}

    public static AppBrandStructBean convertAppBrandStructStubToBean(AppBrandStruct stub)
    {
        AppBrandStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Empty bean is returned.");
        } else {
            bean = new AppBrandStructBean();
            bean.setBrand(stub.getBrand());
            bean.setName(stub.getName());
            bean.setDescription(stub.getDescription());
        }
        return bean;
    }

}
