package com.cannyurl.af.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.CommonConstants;
import com.myurldb.ws.CellLatitudeLongitude;
import com.myurldb.ws.stub.CellLatitudeLongitudeStub;
import com.cannyurl.af.bean.CellLatitudeLongitudeBean;


public class CellLatitudeLongitudeResourceUtil
{
    private static final Logger log = Logger.getLogger(CellLatitudeLongitudeResourceUtil.class.getName());

    // Static methods only.
    private CellLatitudeLongitudeResourceUtil() {}

    public static CellLatitudeLongitudeBean convertCellLatitudeLongitudeStubToBean(CellLatitudeLongitude stub)
    {
        CellLatitudeLongitudeBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Empty bean is returned.");
        } else {
            bean = new CellLatitudeLongitudeBean();
            bean.setScale(stub.getScale());
            bean.setLatitude(stub.getLatitude());
            bean.setLongitude(stub.getLongitude());
        }
        return bean;
    }

}
