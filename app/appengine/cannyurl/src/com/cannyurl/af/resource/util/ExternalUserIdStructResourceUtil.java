package com.cannyurl.af.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.CommonConstants;
import com.myurldb.ws.ExternalUserIdStruct;
import com.myurldb.ws.stub.ExternalUserIdStructStub;
import com.cannyurl.af.bean.ExternalUserIdStructBean;


public class ExternalUserIdStructResourceUtil
{
    private static final Logger log = Logger.getLogger(ExternalUserIdStructResourceUtil.class.getName());

    // Static methods only.
    private ExternalUserIdStructResourceUtil() {}

    public static ExternalUserIdStructBean convertExternalUserIdStructStubToBean(ExternalUserIdStruct stub)
    {
        ExternalUserIdStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Empty bean is returned.");
        } else {
            bean = new ExternalUserIdStructBean();
            bean.setUuid(stub.getUuid());
            bean.setId(stub.getId());
            bean.setName(stub.getName());
            bean.setEmail(stub.getEmail());
            bean.setUsername(stub.getUsername());
            bean.setOpenId(stub.getOpenId());
            bean.setNote(stub.getNote());
        }
        return bean;
    }

}
