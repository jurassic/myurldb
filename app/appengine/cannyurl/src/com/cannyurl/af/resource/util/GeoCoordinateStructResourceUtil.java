package com.cannyurl.af.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.CommonConstants;
import com.myurldb.ws.GeoCoordinateStruct;
import com.myurldb.ws.stub.GeoCoordinateStructStub;
import com.cannyurl.af.bean.GeoCoordinateStructBean;


public class GeoCoordinateStructResourceUtil
{
    private static final Logger log = Logger.getLogger(GeoCoordinateStructResourceUtil.class.getName());

    // Static methods only.
    private GeoCoordinateStructResourceUtil() {}

    public static GeoCoordinateStructBean convertGeoCoordinateStructStubToBean(GeoCoordinateStruct stub)
    {
        GeoCoordinateStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Empty bean is returned.");
        } else {
            bean = new GeoCoordinateStructBean();
            bean.setUuid(stub.getUuid());
            bean.setLatitude(stub.getLatitude());
            bean.setLongitude(stub.getLongitude());
            bean.setAltitude(stub.getAltitude());
            bean.setSensorUsed(stub.isSensorUsed());
            bean.setAccuracy(stub.getAccuracy());
            bean.setAltitudeAccuracy(stub.getAltitudeAccuracy());
            bean.setHeading(stub.getHeading());
            bean.setSpeed(stub.getSpeed());
            bean.setNote(stub.getNote());
        }
        return bean;
    }

}
