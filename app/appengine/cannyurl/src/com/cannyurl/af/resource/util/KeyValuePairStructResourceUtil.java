package com.cannyurl.af.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.CommonConstants;
import com.myurldb.ws.KeyValuePairStruct;
import com.myurldb.ws.stub.KeyValuePairStructStub;
import com.cannyurl.af.bean.KeyValuePairStructBean;


public class KeyValuePairStructResourceUtil
{
    private static final Logger log = Logger.getLogger(KeyValuePairStructResourceUtil.class.getName());

    // Static methods only.
    private KeyValuePairStructResourceUtil() {}

    public static KeyValuePairStructBean convertKeyValuePairStructStubToBean(KeyValuePairStruct stub)
    {
        KeyValuePairStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Empty bean is returned.");
        } else {
            bean = new KeyValuePairStructBean();
            bean.setUuid(stub.getUuid());
            bean.setKey(stub.getKey());
            bean.setValue(stub.getValue());
            bean.setNote(stub.getNote());
        }
        return bean;
    }

}
