package com.cannyurl.af.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.CommonConstants;
import com.myurldb.ws.SiteLog;
import com.myurldb.ws.stub.SiteLogStub;
import com.cannyurl.af.bean.SiteLogBean;


public class SiteLogResourceUtil
{
    private static final Logger log = Logger.getLogger(SiteLogResourceUtil.class.getName());

    // Static methods only.
    private SiteLogResourceUtil() {}

    public static SiteLogBean convertSiteLogStubToBean(SiteLog stub)
    {
        SiteLogBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Empty bean is returned.");
        } else {
            bean = new SiteLogBean();
            bean.setUuid(stub.getUuid());
            bean.setTitle(stub.getTitle());
            bean.setPubDate(stub.getPubDate());
            bean.setTag(stub.getTag());
            bean.setContent(stub.getContent());
            bean.setFormat(stub.getFormat());
            bean.setNote(stub.getNote());
            bean.setStatus(stub.getStatus());
        }
        return bean;
    }

}
