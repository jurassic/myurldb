package com.cannyurl.af.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.CommonConstants;
import com.myurldb.ws.StreetAddressStruct;
import com.myurldb.ws.stub.StreetAddressStructStub;
import com.cannyurl.af.bean.StreetAddressStructBean;


public class StreetAddressStructResourceUtil
{
    private static final Logger log = Logger.getLogger(StreetAddressStructResourceUtil.class.getName());

    // Static methods only.
    private StreetAddressStructResourceUtil() {}

    public static StreetAddressStructBean convertStreetAddressStructStubToBean(StreetAddressStruct stub)
    {
        StreetAddressStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Empty bean is returned.");
        } else {
            bean = new StreetAddressStructBean();
            bean.setUuid(stub.getUuid());
            bean.setStreet1(stub.getStreet1());
            bean.setStreet2(stub.getStreet2());
            bean.setCity(stub.getCity());
            bean.setCounty(stub.getCounty());
            bean.setPostalCode(stub.getPostalCode());
            bean.setState(stub.getState());
            bean.setProvince(stub.getProvince());
            bean.setCountry(stub.getCountry());
            bean.setCountryName(stub.getCountryName());
            bean.setNote(stub.getNote());
        }
        return bean;
    }

}
