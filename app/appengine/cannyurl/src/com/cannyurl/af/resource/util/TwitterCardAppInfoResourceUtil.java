package com.cannyurl.af.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.CommonConstants;
import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.stub.TwitterCardAppInfoStub;
import com.cannyurl.af.bean.TwitterCardAppInfoBean;


public class TwitterCardAppInfoResourceUtil
{
    private static final Logger log = Logger.getLogger(TwitterCardAppInfoResourceUtil.class.getName());

    // Static methods only.
    private TwitterCardAppInfoResourceUtil() {}

    public static TwitterCardAppInfoBean convertTwitterCardAppInfoStubToBean(TwitterCardAppInfo stub)
    {
        TwitterCardAppInfoBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Empty bean is returned.");
        } else {
            bean = new TwitterCardAppInfoBean();
            bean.setName(stub.getName());
            bean.setId(stub.getId());
            bean.setUrl(stub.getUrl());
        }
        return bean;
    }

}
