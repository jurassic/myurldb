package com.cannyurl.af.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.CommonConstants;
import com.myurldb.ws.WebProfileStruct;
import com.myurldb.ws.stub.WebProfileStructStub;
import com.cannyurl.af.bean.WebProfileStructBean;


public class WebProfileStructResourceUtil
{
    private static final Logger log = Logger.getLogger(WebProfileStructResourceUtil.class.getName());

    // Static methods only.
    private WebProfileStructResourceUtil() {}

    public static WebProfileStructBean convertWebProfileStructStubToBean(WebProfileStruct stub)
    {
        WebProfileStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Empty bean is returned.");
        } else {
            bean = new WebProfileStructBean();
            bean.setUuid(stub.getUuid());
            bean.setType(stub.getType());
            bean.setServiceName(stub.getServiceName());
            bean.setServiceUrl(stub.getServiceUrl());
            bean.setProfileUrl(stub.getProfileUrl());
            bean.setNote(stub.getNote());
        }
        return bean;
    }

}
