package com.cannyurl.af.search;

import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

//import com.google.appengine.api.search.Field;
//import com.google.appengine.api.search.GeoPoint;
//import com.google.appengine.api.search.Document;
//import com.google.appengine.api.search.Document.Builder;

import com.myurldb.ws.core.GUID;
import com.myurldb.ws.resource.exception.NotImplementedRsException;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.AlbumShortLink;
import com.myurldb.ws.search.bean.AlbumShortLinkQueryBean;
import com.myurldb.ws.search.gae.AlbumShortLinkIndexBuilder;
import com.cannyurl.af.config.Config;
import com.cannyurl.af.search.gae.AlbumShortLinkQueryHelper;


public class AlbumShortLinkSearchManager
{
    private static final Logger log = Logger.getLogger(AlbumShortLinkSearchManager.class.getName());
        
    // TBD: Use AbstractFactory....
    private AlbumShortLinkIndexBuilder albumShortLinkIndexBuilder;
    private AlbumShortLinkQueryHelper albumShortLinkQueryHelper;
    
    private AlbumShortLinkSearchManager()
    {
        albumShortLinkIndexBuilder = new AlbumShortLinkIndexBuilder();
        albumShortLinkQueryHelper = new AlbumShortLinkQueryHelper(albumShortLinkIndexBuilder);
    }

    
    // Initialization-on-demand holder.
    private static final class AlbumShortLinkSearchManagerHolder
    {
        private static final AlbumShortLinkSearchManager INSTANCE = new AlbumShortLinkSearchManager();
    }

    // Singleton method
    public static AlbumShortLinkSearchManager getInstance()
    {
        return AlbumShortLinkSearchManagerHolder.INSTANCE;
    }


    public boolean addDocument(AlbumShortLink albumShortLink)
    {
        return albumShortLinkIndexBuilder.addDocument(albumShortLink);
    }

    public List<AlbumShortLinkQueryBean> findAlbumShortLinkQueryBeans(String queryString, String ordering, Long offset, Integer count)
    {
        if("local".equals(Config.getInstance().getSearchIndexMode())) {
            return albumShortLinkQueryHelper.findAlbumShortLinkQueryBeans(queryString, ordering, offset, count);
        } else {
        	throw new NotImplementedRsException("Unsupported searchIndexMode.");
        }
    }

}

