package com.cannyurl.af.search;

import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

//import com.google.appengine.api.search.Field;
//import com.google.appengine.api.search.GeoPoint;
//import com.google.appengine.api.search.Document;
//import com.google.appengine.api.search.Document.Builder;

import com.myurldb.ws.core.GUID;
import com.myurldb.ws.resource.exception.NotImplementedRsException;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.BookmarkFolder;
import com.myurldb.ws.search.bean.BookmarkFolderQueryBean;
import com.myurldb.ws.search.gae.BookmarkFolderIndexBuilder;
import com.cannyurl.af.config.Config;
import com.cannyurl.af.search.gae.BookmarkFolderQueryHelper;


public class BookmarkFolderSearchManager
{
    private static final Logger log = Logger.getLogger(BookmarkFolderSearchManager.class.getName());
        
    // TBD: Use AbstractFactory....
    private BookmarkFolderIndexBuilder bookmarkFolderIndexBuilder;
    private BookmarkFolderQueryHelper bookmarkFolderQueryHelper;
    
    private BookmarkFolderSearchManager()
    {
        bookmarkFolderIndexBuilder = new BookmarkFolderIndexBuilder();
        bookmarkFolderQueryHelper = new BookmarkFolderQueryHelper(bookmarkFolderIndexBuilder);
    }

    
    // Initialization-on-demand holder.
    private static final class BookmarkFolderSearchManagerHolder
    {
        private static final BookmarkFolderSearchManager INSTANCE = new BookmarkFolderSearchManager();
    }

    // Singleton method
    public static BookmarkFolderSearchManager getInstance()
    {
        return BookmarkFolderSearchManagerHolder.INSTANCE;
    }


    public boolean addDocument(BookmarkFolder bookmarkFolder)
    {
        return bookmarkFolderIndexBuilder.addDocument(bookmarkFolder);
    }

    public List<BookmarkFolderQueryBean> findBookmarkFolderQueryBeans(String queryString, String ordering, Long offset, Integer count)
    {
        if("local".equals(Config.getInstance().getSearchIndexMode())) {
            return bookmarkFolderQueryHelper.findBookmarkFolderQueryBeans(queryString, ordering, offset, count);
        } else {
        	throw new NotImplementedRsException("Unsupported searchIndexMode.");
        }
    }

}

