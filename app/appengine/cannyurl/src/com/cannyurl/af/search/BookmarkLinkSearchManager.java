package com.cannyurl.af.search;

import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

//import com.google.appengine.api.search.Field;
//import com.google.appengine.api.search.GeoPoint;
//import com.google.appengine.api.search.Document;
//import com.google.appengine.api.search.Document.Builder;

import com.myurldb.ws.core.GUID;
import com.myurldb.ws.resource.exception.NotImplementedRsException;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.BookmarkLink;
import com.myurldb.ws.search.bean.BookmarkLinkQueryBean;
import com.myurldb.ws.search.gae.BookmarkLinkIndexBuilder;
import com.cannyurl.af.config.Config;
import com.cannyurl.af.search.gae.BookmarkLinkQueryHelper;


public class BookmarkLinkSearchManager
{
    private static final Logger log = Logger.getLogger(BookmarkLinkSearchManager.class.getName());
        
    // TBD: Use AbstractFactory....
    private BookmarkLinkIndexBuilder bookmarkLinkIndexBuilder;
    private BookmarkLinkQueryHelper bookmarkLinkQueryHelper;
    
    private BookmarkLinkSearchManager()
    {
        bookmarkLinkIndexBuilder = new BookmarkLinkIndexBuilder();
        bookmarkLinkQueryHelper = new BookmarkLinkQueryHelper(bookmarkLinkIndexBuilder);
    }

    
    // Initialization-on-demand holder.
    private static final class BookmarkLinkSearchManagerHolder
    {
        private static final BookmarkLinkSearchManager INSTANCE = new BookmarkLinkSearchManager();
    }

    // Singleton method
    public static BookmarkLinkSearchManager getInstance()
    {
        return BookmarkLinkSearchManagerHolder.INSTANCE;
    }


    public boolean addDocument(BookmarkLink bookmarkLink)
    {
        return bookmarkLinkIndexBuilder.addDocument(bookmarkLink);
    }

    public List<BookmarkLinkQueryBean> findBookmarkLinkQueryBeans(String queryString, String ordering, Long offset, Integer count)
    {
        if("local".equals(Config.getInstance().getSearchIndexMode())) {
            return bookmarkLinkQueryHelper.findBookmarkLinkQueryBeans(queryString, ordering, offset, count);
        } else {
        	throw new NotImplementedRsException("Unsupported searchIndexMode.");
        }
    }

}

