package com.cannyurl.af.search;

import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

//import com.google.appengine.api.search.Field;
//import com.google.appengine.api.search.GeoPoint;
//import com.google.appengine.api.search.Document;
//import com.google.appengine.api.search.Document.Builder;

import com.myurldb.ws.core.GUID;
import com.myurldb.ws.resource.exception.NotImplementedRsException;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.DomainInfo;
import com.myurldb.ws.search.bean.DomainInfoQueryBean;
import com.myurldb.ws.search.gae.DomainInfoIndexBuilder;
import com.cannyurl.af.config.Config;
import com.cannyurl.af.search.gae.DomainInfoQueryHelper;


public class DomainInfoSearchManager
{
    private static final Logger log = Logger.getLogger(DomainInfoSearchManager.class.getName());
        
    // TBD: Use AbstractFactory....
    private DomainInfoIndexBuilder domainInfoIndexBuilder;
    private DomainInfoQueryHelper domainInfoQueryHelper;
    
    private DomainInfoSearchManager()
    {
        domainInfoIndexBuilder = new DomainInfoIndexBuilder();
        domainInfoQueryHelper = new DomainInfoQueryHelper(domainInfoIndexBuilder);
    }

    
    // Initialization-on-demand holder.
    private static final class DomainInfoSearchManagerHolder
    {
        private static final DomainInfoSearchManager INSTANCE = new DomainInfoSearchManager();
    }

    // Singleton method
    public static DomainInfoSearchManager getInstance()
    {
        return DomainInfoSearchManagerHolder.INSTANCE;
    }


    public boolean addDocument(DomainInfo domainInfo)
    {
        return domainInfoIndexBuilder.addDocument(domainInfo);
    }

    public List<DomainInfoQueryBean> findDomainInfoQueryBeans(String queryString, String ordering, Long offset, Integer count)
    {
        if("local".equals(Config.getInstance().getSearchIndexMode())) {
            return domainInfoQueryHelper.findDomainInfoQueryBeans(queryString, ordering, offset, count);
        } else {
        	throw new NotImplementedRsException("Unsupported searchIndexMode.");
        }
    }

}

