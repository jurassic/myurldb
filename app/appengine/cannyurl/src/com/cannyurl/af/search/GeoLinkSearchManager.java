package com.cannyurl.af.search;

import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

//import com.google.appengine.api.search.Field;
//import com.google.appengine.api.search.GeoPoint;
//import com.google.appengine.api.search.Document;
//import com.google.appengine.api.search.Document.Builder;

import com.myurldb.ws.core.GUID;
import com.myurldb.ws.resource.exception.NotImplementedRsException;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.CellLatitudeLongitude;
import com.myurldb.ws.GeoCoordinateStruct;
import com.myurldb.ws.GeoLink;
import com.myurldb.ws.search.bean.GeoLinkQueryBean;
import com.myurldb.ws.search.gae.GeoLinkIndexBuilder;
import com.cannyurl.af.config.Config;
import com.cannyurl.af.search.gae.GeoLinkQueryHelper;


public class GeoLinkSearchManager
{
    private static final Logger log = Logger.getLogger(GeoLinkSearchManager.class.getName());
        
    // TBD: Use AbstractFactory....
    private GeoLinkIndexBuilder geoLinkIndexBuilder;
    private GeoLinkQueryHelper geoLinkQueryHelper;
    
    private GeoLinkSearchManager()
    {
        geoLinkIndexBuilder = new GeoLinkIndexBuilder();
        geoLinkQueryHelper = new GeoLinkQueryHelper(geoLinkIndexBuilder);
    }

    
    // Initialization-on-demand holder.
    private static final class GeoLinkSearchManagerHolder
    {
        private static final GeoLinkSearchManager INSTANCE = new GeoLinkSearchManager();
    }

    // Singleton method
    public static GeoLinkSearchManager getInstance()
    {
        return GeoLinkSearchManagerHolder.INSTANCE;
    }


    public boolean addDocument(GeoLink geoLink)
    {
        return geoLinkIndexBuilder.addDocument(geoLink);
    }

    public List<GeoLinkQueryBean> findGeoLinkQueryBeans(String queryString, String ordering, Long offset, Integer count)
    {
        if("local".equals(Config.getInstance().getSearchIndexMode())) {
            return geoLinkQueryHelper.findGeoLinkQueryBeans(queryString, ordering, offset, count);
        } else {
        	throw new NotImplementedRsException("Unsupported searchIndexMode.");
        }
    }

}

