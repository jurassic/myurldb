package com.cannyurl.af.search;

import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

//import com.google.appengine.api.search.Field;
//import com.google.appengine.api.search.GeoPoint;
//import com.google.appengine.api.search.Document;
//import com.google.appengine.api.search.Document.Builder;

import com.myurldb.ws.core.GUID;
import com.myurldb.ws.resource.exception.NotImplementedRsException;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.KeywordCrowdTally;
import com.myurldb.ws.search.bean.KeywordCrowdTallyQueryBean;
import com.myurldb.ws.search.gae.KeywordCrowdTallyIndexBuilder;
import com.cannyurl.af.config.Config;
import com.cannyurl.af.search.gae.KeywordCrowdTallyQueryHelper;


public class KeywordCrowdTallySearchManager
{
    private static final Logger log = Logger.getLogger(KeywordCrowdTallySearchManager.class.getName());
        
    // TBD: Use AbstractFactory....
    private KeywordCrowdTallyIndexBuilder keywordCrowdTallyIndexBuilder;
    private KeywordCrowdTallyQueryHelper keywordCrowdTallyQueryHelper;
    
    private KeywordCrowdTallySearchManager()
    {
        keywordCrowdTallyIndexBuilder = new KeywordCrowdTallyIndexBuilder();
        keywordCrowdTallyQueryHelper = new KeywordCrowdTallyQueryHelper(keywordCrowdTallyIndexBuilder);
    }

    
    // Initialization-on-demand holder.
    private static final class KeywordCrowdTallySearchManagerHolder
    {
        private static final KeywordCrowdTallySearchManager INSTANCE = new KeywordCrowdTallySearchManager();
    }

    // Singleton method
    public static KeywordCrowdTallySearchManager getInstance()
    {
        return KeywordCrowdTallySearchManagerHolder.INSTANCE;
    }


    public boolean addDocument(KeywordCrowdTally keywordCrowdTally)
    {
        return keywordCrowdTallyIndexBuilder.addDocument(keywordCrowdTally);
    }

    public List<KeywordCrowdTallyQueryBean> findKeywordCrowdTallyQueryBeans(String queryString, String ordering, Long offset, Integer count)
    {
        if("local".equals(Config.getInstance().getSearchIndexMode())) {
            return keywordCrowdTallyQueryHelper.findKeywordCrowdTallyQueryBeans(queryString, ordering, offset, count);
        } else {
        	throw new NotImplementedRsException("Unsupported searchIndexMode.");
        }
    }

}

