package com.cannyurl.af.search;

import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

//import com.google.appengine.api.search.Field;
//import com.google.appengine.api.search.GeoPoint;
//import com.google.appengine.api.search.Document;
//import com.google.appengine.api.search.Document.Builder;

import com.myurldb.ws.core.GUID;
import com.myurldb.ws.resource.exception.NotImplementedRsException;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.KeywordFolder;
import com.myurldb.ws.search.bean.KeywordFolderQueryBean;
import com.myurldb.ws.search.gae.KeywordFolderIndexBuilder;
import com.cannyurl.af.config.Config;
import com.cannyurl.af.search.gae.KeywordFolderQueryHelper;


public class KeywordFolderSearchManager
{
    private static final Logger log = Logger.getLogger(KeywordFolderSearchManager.class.getName());
        
    // TBD: Use AbstractFactory....
    private KeywordFolderIndexBuilder keywordFolderIndexBuilder;
    private KeywordFolderQueryHelper keywordFolderQueryHelper;
    
    private KeywordFolderSearchManager()
    {
        keywordFolderIndexBuilder = new KeywordFolderIndexBuilder();
        keywordFolderQueryHelper = new KeywordFolderQueryHelper(keywordFolderIndexBuilder);
    }

    
    // Initialization-on-demand holder.
    private static final class KeywordFolderSearchManagerHolder
    {
        private static final KeywordFolderSearchManager INSTANCE = new KeywordFolderSearchManager();
    }

    // Singleton method
    public static KeywordFolderSearchManager getInstance()
    {
        return KeywordFolderSearchManagerHolder.INSTANCE;
    }


    public boolean addDocument(KeywordFolder keywordFolder)
    {
        return keywordFolderIndexBuilder.addDocument(keywordFolder);
    }

    public List<KeywordFolderQueryBean> findKeywordFolderQueryBeans(String queryString, String ordering, Long offset, Integer count)
    {
        if("local".equals(Config.getInstance().getSearchIndexMode())) {
            return keywordFolderQueryHelper.findKeywordFolderQueryBeans(queryString, ordering, offset, count);
        } else {
        	throw new NotImplementedRsException("Unsupported searchIndexMode.");
        }
    }

}

