package com.cannyurl.af.search;

import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

//import com.google.appengine.api.search.Field;
//import com.google.appengine.api.search.GeoPoint;
//import com.google.appengine.api.search.Document;
//import com.google.appengine.api.search.Document.Builder;

import com.myurldb.ws.core.GUID;
import com.myurldb.ws.resource.exception.NotImplementedRsException;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.KeywordLink;
import com.myurldb.ws.search.bean.KeywordLinkQueryBean;
import com.myurldb.ws.search.gae.KeywordLinkIndexBuilder;
import com.cannyurl.af.config.Config;
import com.cannyurl.af.search.gae.KeywordLinkQueryHelper;


public class KeywordLinkSearchManager
{
    private static final Logger log = Logger.getLogger(KeywordLinkSearchManager.class.getName());
        
    // TBD: Use AbstractFactory....
    private KeywordLinkIndexBuilder keywordLinkIndexBuilder;
    private KeywordLinkQueryHelper keywordLinkQueryHelper;
    
    private KeywordLinkSearchManager()
    {
        keywordLinkIndexBuilder = new KeywordLinkIndexBuilder();
        keywordLinkQueryHelper = new KeywordLinkQueryHelper(keywordLinkIndexBuilder);
    }

    
    // Initialization-on-demand holder.
    private static final class KeywordLinkSearchManagerHolder
    {
        private static final KeywordLinkSearchManager INSTANCE = new KeywordLinkSearchManager();
    }

    // Singleton method
    public static KeywordLinkSearchManager getInstance()
    {
        return KeywordLinkSearchManagerHolder.INSTANCE;
    }


    public boolean addDocument(KeywordLink keywordLink)
    {
        return keywordLinkIndexBuilder.addDocument(keywordLink);
    }

    public List<KeywordLinkQueryBean> findKeywordLinkQueryBeans(String queryString, String ordering, Long offset, Integer count)
    {
        if("local".equals(Config.getInstance().getSearchIndexMode())) {
            return keywordLinkQueryHelper.findKeywordLinkQueryBeans(queryString, ordering, offset, count);
        } else {
        	throw new NotImplementedRsException("Unsupported searchIndexMode.");
        }
    }

}

