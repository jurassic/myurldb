package com.cannyurl.af.search;

import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

//import com.google.appengine.api.search.Field;
//import com.google.appengine.api.search.GeoPoint;
//import com.google.appengine.api.search.Document;
//import com.google.appengine.api.search.Document.Builder;

import com.myurldb.ws.core.GUID;
import com.myurldb.ws.resource.exception.NotImplementedRsException;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.LinkAlbum;
import com.myurldb.ws.search.bean.LinkAlbumQueryBean;
import com.myurldb.ws.search.gae.LinkAlbumIndexBuilder;
import com.cannyurl.af.config.Config;
import com.cannyurl.af.search.gae.LinkAlbumQueryHelper;


public class LinkAlbumSearchManager
{
    private static final Logger log = Logger.getLogger(LinkAlbumSearchManager.class.getName());
        
    // TBD: Use AbstractFactory....
    private LinkAlbumIndexBuilder linkAlbumIndexBuilder;
    private LinkAlbumQueryHelper linkAlbumQueryHelper;
    
    private LinkAlbumSearchManager()
    {
        linkAlbumIndexBuilder = new LinkAlbumIndexBuilder();
        linkAlbumQueryHelper = new LinkAlbumQueryHelper(linkAlbumIndexBuilder);
    }

    
    // Initialization-on-demand holder.
    private static final class LinkAlbumSearchManagerHolder
    {
        private static final LinkAlbumSearchManager INSTANCE = new LinkAlbumSearchManager();
    }

    // Singleton method
    public static LinkAlbumSearchManager getInstance()
    {
        return LinkAlbumSearchManagerHolder.INSTANCE;
    }


    public boolean addDocument(LinkAlbum linkAlbum)
    {
        return linkAlbumIndexBuilder.addDocument(linkAlbum);
    }

    public List<LinkAlbumQueryBean> findLinkAlbumQueryBeans(String queryString, String ordering, Long offset, Integer count)
    {
        if("local".equals(Config.getInstance().getSearchIndexMode())) {
            return linkAlbumQueryHelper.findLinkAlbumQueryBeans(queryString, ordering, offset, count);
        } else {
        	throw new NotImplementedRsException("Unsupported searchIndexMode.");
        }
    }

}

