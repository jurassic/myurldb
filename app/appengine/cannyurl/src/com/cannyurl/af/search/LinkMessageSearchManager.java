package com.cannyurl.af.search;

import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

//import com.google.appengine.api.search.Field;
//import com.google.appengine.api.search.GeoPoint;
//import com.google.appengine.api.search.Document;
//import com.google.appengine.api.search.Document.Builder;

import com.myurldb.ws.core.GUID;
import com.myurldb.ws.resource.exception.NotImplementedRsException;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.LinkMessage;
import com.myurldb.ws.search.bean.LinkMessageQueryBean;
import com.myurldb.ws.search.gae.LinkMessageIndexBuilder;
import com.cannyurl.af.config.Config;
import com.cannyurl.af.search.gae.LinkMessageQueryHelper;


public class LinkMessageSearchManager
{
    private static final Logger log = Logger.getLogger(LinkMessageSearchManager.class.getName());
        
    // TBD: Use AbstractFactory....
    private LinkMessageIndexBuilder linkMessageIndexBuilder;
    private LinkMessageQueryHelper linkMessageQueryHelper;
    
    private LinkMessageSearchManager()
    {
        linkMessageIndexBuilder = new LinkMessageIndexBuilder();
        linkMessageQueryHelper = new LinkMessageQueryHelper(linkMessageIndexBuilder);
    }

    
    // Initialization-on-demand holder.
    private static final class LinkMessageSearchManagerHolder
    {
        private static final LinkMessageSearchManager INSTANCE = new LinkMessageSearchManager();
    }

    // Singleton method
    public static LinkMessageSearchManager getInstance()
    {
        return LinkMessageSearchManagerHolder.INSTANCE;
    }


    public boolean addDocument(LinkMessage linkMessage)
    {
        return linkMessageIndexBuilder.addDocument(linkMessage);
    }

    public List<LinkMessageQueryBean> findLinkMessageQueryBeans(String queryString, String ordering, Long offset, Integer count)
    {
        if("local".equals(Config.getInstance().getSearchIndexMode())) {
            return linkMessageQueryHelper.findLinkMessageQueryBeans(queryString, ordering, offset, count);
        } else {
        	throw new NotImplementedRsException("Unsupported searchIndexMode.");
        }
    }

}

