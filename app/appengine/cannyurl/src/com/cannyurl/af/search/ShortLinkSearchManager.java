package com.cannyurl.af.search;

import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

//import com.google.appengine.api.search.Field;
//import com.google.appengine.api.search.GeoPoint;
//import com.google.appengine.api.search.Document;
//import com.google.appengine.api.search.Document.Builder;

import com.myurldb.ws.core.GUID;
import com.myurldb.ws.resource.exception.NotImplementedRsException;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.ReferrerInfoStruct;
import com.myurldb.ws.ShortLink;
import com.myurldb.ws.search.bean.ShortLinkQueryBean;
import com.myurldb.ws.search.gae.ShortLinkIndexBuilder;
import com.cannyurl.af.config.Config;
import com.cannyurl.af.search.gae.ShortLinkQueryHelper;


public class ShortLinkSearchManager
{
    private static final Logger log = Logger.getLogger(ShortLinkSearchManager.class.getName());
        
    // TBD: Use AbstractFactory....
    private ShortLinkIndexBuilder shortLinkIndexBuilder;
    private ShortLinkQueryHelper shortLinkQueryHelper;
    
    private ShortLinkSearchManager()
    {
        shortLinkIndexBuilder = new ShortLinkIndexBuilder();
        shortLinkQueryHelper = new ShortLinkQueryHelper(shortLinkIndexBuilder);
    }

    
    // Initialization-on-demand holder.
    private static final class ShortLinkSearchManagerHolder
    {
        private static final ShortLinkSearchManager INSTANCE = new ShortLinkSearchManager();
    }

    // Singleton method
    public static ShortLinkSearchManager getInstance()
    {
        return ShortLinkSearchManagerHolder.INSTANCE;
    }


    public boolean addDocument(ShortLink shortLink)
    {
        return shortLinkIndexBuilder.addDocument(shortLink);
    }

    public List<ShortLinkQueryBean> findShortLinkQueryBeans(String queryString, String ordering, Long offset, Integer count)
    {
        if("local".equals(Config.getInstance().getSearchIndexMode())) {
            return shortLinkQueryHelper.findShortLinkQueryBeans(queryString, ordering, offset, count);
        } else {
        	throw new NotImplementedRsException("Unsupported searchIndexMode.");
        }
    }

}

