package com.cannyurl.af.search;

import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

//import com.google.appengine.api.search.Field;
//import com.google.appengine.api.search.GeoPoint;
//import com.google.appengine.api.search.Document;
//import com.google.appengine.api.search.Document.Builder;

import com.myurldb.ws.core.GUID;
import com.myurldb.ws.resource.exception.NotImplementedRsException;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.ShortPassageAttribute;
import com.myurldb.ws.ShortPassage;
import com.myurldb.ws.search.bean.ShortPassageQueryBean;
import com.myurldb.ws.search.gae.ShortPassageIndexBuilder;
import com.cannyurl.af.config.Config;
import com.cannyurl.af.search.gae.ShortPassageQueryHelper;


public class ShortPassageSearchManager
{
    private static final Logger log = Logger.getLogger(ShortPassageSearchManager.class.getName());
        
    // TBD: Use AbstractFactory....
    private ShortPassageIndexBuilder shortPassageIndexBuilder;
    private ShortPassageQueryHelper shortPassageQueryHelper;
    
    private ShortPassageSearchManager()
    {
        shortPassageIndexBuilder = new ShortPassageIndexBuilder();
        shortPassageQueryHelper = new ShortPassageQueryHelper(shortPassageIndexBuilder);
    }

    
    // Initialization-on-demand holder.
    private static final class ShortPassageSearchManagerHolder
    {
        private static final ShortPassageSearchManager INSTANCE = new ShortPassageSearchManager();
    }

    // Singleton method
    public static ShortPassageSearchManager getInstance()
    {
        return ShortPassageSearchManagerHolder.INSTANCE;
    }


    public boolean addDocument(ShortPassage shortPassage)
    {
        return shortPassageIndexBuilder.addDocument(shortPassage);
    }

    public List<ShortPassageQueryBean> findShortPassageQueryBeans(String queryString, String ordering, Long offset, Integer count)
    {
        if("local".equals(Config.getInstance().getSearchIndexMode())) {
            return shortPassageQueryHelper.findShortPassageQueryBeans(queryString, ordering, offset, count);
        } else {
        	throw new NotImplementedRsException("Unsupported searchIndexMode.");
        }
    }

}

