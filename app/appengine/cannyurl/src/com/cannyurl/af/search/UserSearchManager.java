package com.cannyurl.af.search;

import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

//import com.google.appengine.api.search.Field;
//import com.google.appengine.api.search.GeoPoint;
//import com.google.appengine.api.search.Document;
//import com.google.appengine.api.search.Document.Builder;

import com.myurldb.ws.core.GUID;
import com.myurldb.ws.resource.exception.NotImplementedRsException;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.GeoPointStruct;
import com.myurldb.ws.StreetAddressStruct;
import com.myurldb.ws.GaeAppStruct;
import com.myurldb.ws.FullNameStruct;
import com.myurldb.ws.GaeUserStruct;
import com.myurldb.ws.User;
import com.myurldb.ws.search.bean.UserQueryBean;
import com.myurldb.ws.search.gae.UserIndexBuilder;
import com.cannyurl.af.config.Config;
import com.cannyurl.af.search.gae.UserQueryHelper;


public class UserSearchManager
{
    private static final Logger log = Logger.getLogger(UserSearchManager.class.getName());
        
    // TBD: Use AbstractFactory....
    private UserIndexBuilder userIndexBuilder;
    private UserQueryHelper userQueryHelper;
    
    private UserSearchManager()
    {
        userIndexBuilder = new UserIndexBuilder();
        userQueryHelper = new UserQueryHelper(userIndexBuilder);
    }

    
    // Initialization-on-demand holder.
    private static final class UserSearchManagerHolder
    {
        private static final UserSearchManager INSTANCE = new UserSearchManager();
    }

    // Singleton method
    public static UserSearchManager getInstance()
    {
        return UserSearchManagerHolder.INSTANCE;
    }


    public boolean addDocument(User user)
    {
        return userIndexBuilder.addDocument(user);
    }

    public List<UserQueryBean> findUserQueryBeans(String queryString, String ordering, Long offset, Integer count)
    {
        if("local".equals(Config.getInstance().getSearchIndexMode())) {
            return userQueryHelper.findUserQueryBeans(queryString, ordering, offset, count);
        } else {
        	throw new NotImplementedRsException("Unsupported searchIndexMode.");
        }
    }

}

