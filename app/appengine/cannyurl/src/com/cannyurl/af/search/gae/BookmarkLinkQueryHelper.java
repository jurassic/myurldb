package com.cannyurl.af.search.gae;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.search.Field;
import com.google.appengine.api.search.GeoPoint;
import com.google.appengine.api.search.Cursor;
import com.google.appengine.api.search.Query;
import com.google.appengine.api.search.QueryOptions;
import com.google.appengine.api.search.Results;
import com.google.appengine.api.search.SearchException;
import com.google.appengine.api.search.ScoredDocument;
import com.google.appengine.api.search.SortExpression;
import com.google.appengine.api.search.SortOptions;
import com.google.appengine.api.search.SearchException;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.BookmarkLink;
import com.myurldb.ws.search.bean.BookmarkLinkQueryBean;
import com.myurldb.ws.search.gae.BookmarkLinkIndexBuilder;
import com.cannyurl.af.service.manager.ServiceFactoryManager;


public class BookmarkLinkQueryHelper extends BaseQueryHelper
{
    private static final Logger log = Logger.getLogger(BookmarkLinkQueryHelper.class.getName());
    
    
    private BookmarkLinkIndexBuilder bookmarkLinkIndexBuilder = null;

    public BookmarkLinkQueryHelper()
    {
    	this(null);
    }
    public BookmarkLinkQueryHelper(BookmarkLinkIndexBuilder bookmarkLinkIndexBuilder)
    {
        super();
        if(bookmarkLinkIndexBuilder != null) {
        	this.bookmarkLinkIndexBuilder = bookmarkLinkIndexBuilder;
        } else {
        	this.bookmarkLinkIndexBuilder = new BookmarkLinkIndexBuilder();
        }
    }

    // TBD:    
    public List<BookmarkLinkQueryBean> findBookmarkLinkQueryBeans(String queryString, String ordering, Long offset, Integer count)
    {
    	log.finer("BEGIN: findBookmarkLinkQueryBeans()");

        // TBD
    	Results<ScoredDocument> docs = findDocuments(queryString, ordering, offset, count);
    	if(docs == null) {
    		return null;
    	}
    	List<BookmarkLinkQueryBean> bookmarkLinkQueryBeans = new ArrayList<BookmarkLinkQueryBean>();
    	for(ScoredDocument doc : docs) {
    		BookmarkLinkQueryBean bean = new BookmarkLinkQueryBean();

    	    int rank = doc.getRank();
    		bean.setRank(rank);
    		String guid = doc.getId();
    		bean.setBookmarkLinkGuid(guid);
    		int cntMemo = doc.getFieldCount("memo");
    		if(cntMemo >= 1) {
    			if(cntMemo == 1) {
                    Field fMemo = doc.getOnlyField("memo");
                    String vMemo = fMemo.getText();
                    bean.setMemo(vMemo);
    			} else {
    				log.warning("More than one Field, memo, present.");
    			}
    		} else {
                log.info("Field, memo, not present.");
    		}

   	        bookmarkLinkQueryBeans.add(bean);
    	}

    	log.finer("END: findBookmarkLinkQueryBeans()");
        return bookmarkLinkQueryBeans;
    }

    // Note: This is very inefficient....
    public List<BookmarkLink> findBookmarkLinks(String queryString, String ordering, Long offset, Integer count)
    {
    	log.finer("BEGIN: findBookmarkLinks()");

        // TBD
    	Results<ScoredDocument> docs = findDocuments(queryString, ordering, offset, count);
    	if(docs == null) {
    		return null;
    	}
    	List<BookmarkLink> bookmarkLinks = new ArrayList<BookmarkLink>();
    	for(ScoredDocument doc : docs) {
    		String guid = doc.getId();
    		BookmarkLink bookmarkLink = null;
    		try {
        		bookmarkLink = ServiceFactoryManager.getServiceFactory().getBookmarkLinkService().getBookmarkLink(guid);
			} catch (BaseException e) {
				if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Failed to get BookmarkLink for guid = " + guid, e);
			}
    		if(bookmarkLink != null) {
    			bookmarkLinks.add(bookmarkLink);
    		}
    	}

    	log.finer("END: findBookmarkLinks()");
        return bookmarkLinks;
    }


    public Results<ScoredDocument> findDocuments(String queryString, String ordering, Long offset, Integer count)
    {
    	log.finer("BEGIN: findDocuments()");

        try {
        	if(count == null) {
        		count = DEFAULT_COUNT;
        	} else if(count > MAX_COUNT) {
        		count = MAX_COUNT;
        	}
        	if(offset == null || offset < 0L) {
        		offset = 0L;
        	}
        	QueryOptions.Builder queryOptionsBuilder = QueryOptions.newBuilder();
        	
        	if(ordering != null && !ordering.isEmpty()) {
        		String[] parts = ordering.split("\\s+", 2);
        		SortExpression.Builder sortExpressionBuilder = SortExpression.newBuilder();
        		if(parts.length > 0) {
        			sortExpressionBuilder.setExpression(parts[0]);
        			if(parts.length > 1) {
        				if(parts[1].equalsIgnoreCase("desc")) {
        					sortExpressionBuilder.setDirection(SortExpression.SortDirection.DESCENDING);
        				} else {
        					sortExpressionBuilder.setDirection(SortExpression.SortDirection.ASCENDING);
        				}
        			} else {
        				// Default...
        				sortExpressionBuilder.setDirection(SortExpression.SortDirection.ASCENDING);
        			}
        		}
        		sortExpressionBuilder.setDefaultValue("");   // ???
        		SortExpression sortExpression = sortExpressionBuilder.build();
        		SortOptions.Builder sortOptionsBuilder = SortOptions.newBuilder(); 
        		sortOptionsBuilder.addSortExpression(sortExpression);
        		sortOptionsBuilder.setLimit((int) (count + offset));  // ???
        		SortOptions sortOptions = sortOptionsBuilder.build();
        		queryOptionsBuilder.setSortOptions(sortOptions);
        	}

        	queryOptionsBuilder.setLimit(count);
        	queryOptionsBuilder.setOffset(offset.intValue());
        	//queryOptionsBuilder.setCursor(Cursor.newBuilder().build());

        	List<String> fieldsToReturn = new ArrayList<String>();
        	List<String> fieldsToSnippet = new ArrayList<String>();        	
            fieldsToReturn.add("memo");
        	queryOptionsBuilder.setFieldsToReturn(fieldsToReturn.toArray(new String[0]));
        	if(!fieldsToSnippet.isEmpty()) {
                queryOptionsBuilder.setFieldsToSnippet(fieldsToSnippet.toArray(new String[0]));
            }

            QueryOptions options = queryOptionsBuilder.build();
            Query query = Query.newBuilder().setOptions(options).build(queryString);

            log.finer("END: findDocuments()");
            return bookmarkLinkIndexBuilder.getIndex().search(query);
        } catch (SearchException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Search request with query " + queryString + " failed", e);
            return null;
        } catch (Exception e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Search request with query " + queryString + " failed due to unknown error", e);
            return null;
        }
    }

}

