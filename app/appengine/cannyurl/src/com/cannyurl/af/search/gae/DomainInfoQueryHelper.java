package com.cannyurl.af.search.gae;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.search.Field;
import com.google.appengine.api.search.GeoPoint;
import com.google.appengine.api.search.Cursor;
import com.google.appengine.api.search.Query;
import com.google.appengine.api.search.QueryOptions;
import com.google.appengine.api.search.Results;
import com.google.appengine.api.search.SearchException;
import com.google.appengine.api.search.ScoredDocument;
import com.google.appengine.api.search.SortExpression;
import com.google.appengine.api.search.SortOptions;
import com.google.appengine.api.search.SearchException;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.DomainInfo;
import com.myurldb.ws.search.bean.DomainInfoQueryBean;
import com.myurldb.ws.search.gae.DomainInfoIndexBuilder;
import com.cannyurl.af.service.manager.ServiceFactoryManager;


public class DomainInfoQueryHelper extends BaseQueryHelper
{
    private static final Logger log = Logger.getLogger(DomainInfoQueryHelper.class.getName());
    
    
    private DomainInfoIndexBuilder domainInfoIndexBuilder = null;

    public DomainInfoQueryHelper()
    {
    	this(null);
    }
    public DomainInfoQueryHelper(DomainInfoIndexBuilder domainInfoIndexBuilder)
    {
        super();
        if(domainInfoIndexBuilder != null) {
        	this.domainInfoIndexBuilder = domainInfoIndexBuilder;
        } else {
        	this.domainInfoIndexBuilder = new DomainInfoIndexBuilder();
        }
    }

    // TBD:    
    public List<DomainInfoQueryBean> findDomainInfoQueryBeans(String queryString, String ordering, Long offset, Integer count)
    {
    	log.finer("BEGIN: findDomainInfoQueryBeans()");

        // TBD
    	Results<ScoredDocument> docs = findDocuments(queryString, ordering, offset, count);
    	if(docs == null) {
    		return null;
    	}
    	List<DomainInfoQueryBean> domainInfoQueryBeans = new ArrayList<DomainInfoQueryBean>();
    	for(ScoredDocument doc : docs) {
    		DomainInfoQueryBean bean = new DomainInfoQueryBean();

    	    int rank = doc.getRank();
    		bean.setRank(rank);
    		String guid = doc.getId();
    		bean.setDomainInfoGuid(guid);
    		int cntCategory = doc.getFieldCount("category");
    		if(cntCategory >= 1) {
    			if(cntCategory == 1) {
                    Field fCategory = doc.getOnlyField("category");
                    String vCategory = fCategory.getText();
                    bean.setCategory(vCategory);
    			} else {
    				log.warning("More than one Field, category, present.");
    			}
    		} else {
                log.info("Field, category, not present.");
    		}

   	        domainInfoQueryBeans.add(bean);
    	}

    	log.finer("END: findDomainInfoQueryBeans()");
        return domainInfoQueryBeans;
    }

    // Note: This is very inefficient....
    public List<DomainInfo> findDomainInfos(String queryString, String ordering, Long offset, Integer count)
    {
    	log.finer("BEGIN: findDomainInfos()");

        // TBD
    	Results<ScoredDocument> docs = findDocuments(queryString, ordering, offset, count);
    	if(docs == null) {
    		return null;
    	}
    	List<DomainInfo> domainInfos = new ArrayList<DomainInfo>();
    	for(ScoredDocument doc : docs) {
    		String guid = doc.getId();
    		DomainInfo domainInfo = null;
    		try {
        		domainInfo = ServiceFactoryManager.getServiceFactory().getDomainInfoService().getDomainInfo(guid);
			} catch (BaseException e) {
				if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Failed to get DomainInfo for guid = " + guid, e);
			}
    		if(domainInfo != null) {
    			domainInfos.add(domainInfo);
    		}
    	}

    	log.finer("END: findDomainInfos()");
        return domainInfos;
    }


    public Results<ScoredDocument> findDocuments(String queryString, String ordering, Long offset, Integer count)
    {
    	log.finer("BEGIN: findDocuments()");

        try {
        	if(count == null) {
        		count = DEFAULT_COUNT;
        	} else if(count > MAX_COUNT) {
        		count = MAX_COUNT;
        	}
        	if(offset == null || offset < 0L) {
        		offset = 0L;
        	}
        	QueryOptions.Builder queryOptionsBuilder = QueryOptions.newBuilder();
        	
        	if(ordering != null && !ordering.isEmpty()) {
        		String[] parts = ordering.split("\\s+", 2);
        		SortExpression.Builder sortExpressionBuilder = SortExpression.newBuilder();
        		if(parts.length > 0) {
        			sortExpressionBuilder.setExpression(parts[0]);
        			if(parts.length > 1) {
        				if(parts[1].equalsIgnoreCase("desc")) {
        					sortExpressionBuilder.setDirection(SortExpression.SortDirection.DESCENDING);
        				} else {
        					sortExpressionBuilder.setDirection(SortExpression.SortDirection.ASCENDING);
        				}
        			} else {
        				// Default...
        				sortExpressionBuilder.setDirection(SortExpression.SortDirection.ASCENDING);
        			}
        		}
        		sortExpressionBuilder.setDefaultValue("");   // ???
        		SortExpression sortExpression = sortExpressionBuilder.build();
        		SortOptions.Builder sortOptionsBuilder = SortOptions.newBuilder(); 
        		sortOptionsBuilder.addSortExpression(sortExpression);
        		sortOptionsBuilder.setLimit((int) (count + offset));  // ???
        		SortOptions sortOptions = sortOptionsBuilder.build();
        		queryOptionsBuilder.setSortOptions(sortOptions);
        	}

        	queryOptionsBuilder.setLimit(count);
        	queryOptionsBuilder.setOffset(offset.intValue());
        	//queryOptionsBuilder.setCursor(Cursor.newBuilder().build());

        	List<String> fieldsToReturn = new ArrayList<String>();
        	List<String> fieldsToSnippet = new ArrayList<String>();        	
            fieldsToReturn.add("category");
        	queryOptionsBuilder.setFieldsToReturn(fieldsToReturn.toArray(new String[0]));
        	if(!fieldsToSnippet.isEmpty()) {
                queryOptionsBuilder.setFieldsToSnippet(fieldsToSnippet.toArray(new String[0]));
            }

            QueryOptions options = queryOptionsBuilder.build();
            Query query = Query.newBuilder().setOptions(options).build(queryString);

            log.finer("END: findDocuments()");
            return domainInfoIndexBuilder.getIndex().search(query);
        } catch (SearchException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Search request with query " + queryString + " failed", e);
            return null;
        } catch (Exception e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Search request with query " + queryString + " failed due to unknown error", e);
            return null;
        }
    }

}

