package com.cannyurl.af.search.gae;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.search.Field;
import com.google.appengine.api.search.GeoPoint;
import com.google.appengine.api.search.Cursor;
import com.google.appengine.api.search.Query;
import com.google.appengine.api.search.QueryOptions;
import com.google.appengine.api.search.Results;
import com.google.appengine.api.search.SearchException;
import com.google.appengine.api.search.ScoredDocument;
import com.google.appengine.api.search.SortExpression;
import com.google.appengine.api.search.SortOptions;
import com.google.appengine.api.search.SearchException;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.CellLatitudeLongitude;
import com.myurldb.ws.GeoCoordinateStruct;
import com.myurldb.ws.GeoLink;
import com.myurldb.ws.search.bean.GeoLinkQueryBean;
import com.myurldb.ws.search.gae.GeoLinkIndexBuilder;
import com.cannyurl.af.bean.CellLatitudeLongitudeBean;
import com.cannyurl.af.bean.GeoCoordinateStructBean;
import com.cannyurl.af.service.manager.ServiceFactoryManager;


public class GeoLinkQueryHelper extends BaseQueryHelper
{
    private static final Logger log = Logger.getLogger(GeoLinkQueryHelper.class.getName());
    
    
    private GeoLinkIndexBuilder geoLinkIndexBuilder = null;

    public GeoLinkQueryHelper()
    {
    	this(null);
    }
    public GeoLinkQueryHelper(GeoLinkIndexBuilder geoLinkIndexBuilder)
    {
        super();
        if(geoLinkIndexBuilder != null) {
        	this.geoLinkIndexBuilder = geoLinkIndexBuilder;
        } else {
        	this.geoLinkIndexBuilder = new GeoLinkIndexBuilder();
        }
    }

    // TBD:    
    public List<GeoLinkQueryBean> findGeoLinkQueryBeans(String queryString, String ordering, Long offset, Integer count)
    {
    	log.finer("BEGIN: findGeoLinkQueryBeans()");

        // TBD
    	Results<ScoredDocument> docs = findDocuments(queryString, ordering, offset, count);
    	if(docs == null) {
    		return null;
    	}
    	List<GeoLinkQueryBean> geoLinkQueryBeans = new ArrayList<GeoLinkQueryBean>();
    	for(ScoredDocument doc : docs) {
    		GeoLinkQueryBean bean = new GeoLinkQueryBean();

    	    int rank = doc.getRank();
    		bean.setRank(rank);
    		String guid = doc.getId();
    		bean.setGeoLinkGuid(guid);
    		int cntGeoCoordinate = doc.getFieldCount("geoCoordinate");
    		if(cntGeoCoordinate >= 1) {
    			if(cntGeoCoordinate == 1) {
                    Field fGeoCoordinate = doc.getOnlyField("geoCoordinate");
                    GeoPoint gpGeoCoordinate = fGeoCoordinate.getGeoPoint();
                    GeoCoordinateStructBean vGeoCoordinate = new GeoCoordinateStructBean();
                    vGeoCoordinate.setLatitude(gpGeoCoordinate.getLatitude());
                    vGeoCoordinate.setLongitude(gpGeoCoordinate.getLongitude());
                    bean.setGeoCoordinate(vGeoCoordinate);
    			} else {
    				log.warning("More than one Field, geoCoordinate, present.");
    			}
    		} else {
                log.info("Field, geoCoordinate, not present.");
    		}

   	        geoLinkQueryBeans.add(bean);
    	}

    	log.finer("END: findGeoLinkQueryBeans()");
        return geoLinkQueryBeans;
    }

    // Note: This is very inefficient....
    public List<GeoLink> findGeoLinks(String queryString, String ordering, Long offset, Integer count)
    {
    	log.finer("BEGIN: findGeoLinks()");

        // TBD
    	Results<ScoredDocument> docs = findDocuments(queryString, ordering, offset, count);
    	if(docs == null) {
    		return null;
    	}
    	List<GeoLink> geoLinks = new ArrayList<GeoLink>();
    	for(ScoredDocument doc : docs) {
    		String guid = doc.getId();
    		GeoLink geoLink = null;
    		try {
        		geoLink = ServiceFactoryManager.getServiceFactory().getGeoLinkService().getGeoLink(guid);
			} catch (BaseException e) {
				if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Failed to get GeoLink for guid = " + guid, e);
			}
    		if(geoLink != null) {
    			geoLinks.add(geoLink);
    		}
    	}

    	log.finer("END: findGeoLinks()");
        return geoLinks;
    }


    public Results<ScoredDocument> findDocuments(String queryString, String ordering, Long offset, Integer count)
    {
    	log.finer("BEGIN: findDocuments()");

        try {
        	if(count == null) {
        		count = DEFAULT_COUNT;
        	} else if(count > MAX_COUNT) {
        		count = MAX_COUNT;
        	}
        	if(offset == null || offset < 0L) {
        		offset = 0L;
        	}
        	QueryOptions.Builder queryOptionsBuilder = QueryOptions.newBuilder();
        	
        	if(ordering != null && !ordering.isEmpty()) {
        		String[] parts = ordering.split("\\s+", 2);
        		SortExpression.Builder sortExpressionBuilder = SortExpression.newBuilder();
        		if(parts.length > 0) {
        			sortExpressionBuilder.setExpression(parts[0]);
        			if(parts.length > 1) {
        				if(parts[1].equalsIgnoreCase("desc")) {
        					sortExpressionBuilder.setDirection(SortExpression.SortDirection.DESCENDING);
        				} else {
        					sortExpressionBuilder.setDirection(SortExpression.SortDirection.ASCENDING);
        				}
        			} else {
        				// Default...
        				sortExpressionBuilder.setDirection(SortExpression.SortDirection.ASCENDING);
        			}
        		}
        		sortExpressionBuilder.setDefaultValue("");   // ???
        		SortExpression sortExpression = sortExpressionBuilder.build();
        		SortOptions.Builder sortOptionsBuilder = SortOptions.newBuilder(); 
        		sortOptionsBuilder.addSortExpression(sortExpression);
        		sortOptionsBuilder.setLimit((int) (count + offset));  // ???
        		SortOptions sortOptions = sortOptionsBuilder.build();
        		queryOptionsBuilder.setSortOptions(sortOptions);
        	}

        	queryOptionsBuilder.setLimit(count);
        	queryOptionsBuilder.setOffset(offset.intValue());
        	//queryOptionsBuilder.setCursor(Cursor.newBuilder().build());

        	List<String> fieldsToReturn = new ArrayList<String>();
        	List<String> fieldsToSnippet = new ArrayList<String>();        	
            fieldsToReturn.add("geoCoordinate");
        	queryOptionsBuilder.setFieldsToReturn(fieldsToReturn.toArray(new String[0]));
        	if(!fieldsToSnippet.isEmpty()) {
                queryOptionsBuilder.setFieldsToSnippet(fieldsToSnippet.toArray(new String[0]));
            }

            QueryOptions options = queryOptionsBuilder.build();
            Query query = Query.newBuilder().setOptions(options).build(queryString);

            log.finer("END: findDocuments()");
            return geoLinkIndexBuilder.getIndex().search(query);
        } catch (SearchException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Search request with query " + queryString + " failed", e);
            return null;
        } catch (Exception e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Search request with query " + queryString + " failed due to unknown error", e);
            return null;
        }
    }

}

