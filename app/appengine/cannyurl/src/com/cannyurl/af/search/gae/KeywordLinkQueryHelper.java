package com.cannyurl.af.search.gae;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.search.Field;
import com.google.appengine.api.search.GeoPoint;
import com.google.appengine.api.search.Cursor;
import com.google.appengine.api.search.Query;
import com.google.appengine.api.search.QueryOptions;
import com.google.appengine.api.search.Results;
import com.google.appengine.api.search.SearchException;
import com.google.appengine.api.search.ScoredDocument;
import com.google.appengine.api.search.SortExpression;
import com.google.appengine.api.search.SortOptions;
import com.google.appengine.api.search.SearchException;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.KeywordLink;
import com.myurldb.ws.search.bean.KeywordLinkQueryBean;
import com.myurldb.ws.search.gae.KeywordLinkIndexBuilder;
import com.cannyurl.af.service.manager.ServiceFactoryManager;


public class KeywordLinkQueryHelper extends BaseQueryHelper
{
    private static final Logger log = Logger.getLogger(KeywordLinkQueryHelper.class.getName());
    
    
    private KeywordLinkIndexBuilder keywordLinkIndexBuilder = null;

    public KeywordLinkQueryHelper()
    {
    	this(null);
    }
    public KeywordLinkQueryHelper(KeywordLinkIndexBuilder keywordLinkIndexBuilder)
    {
        super();
        if(keywordLinkIndexBuilder != null) {
        	this.keywordLinkIndexBuilder = keywordLinkIndexBuilder;
        } else {
        	this.keywordLinkIndexBuilder = new KeywordLinkIndexBuilder();
        }
    }

    // TBD:    
    public List<KeywordLinkQueryBean> findKeywordLinkQueryBeans(String queryString, String ordering, Long offset, Integer count)
    {
    	log.finer("BEGIN: findKeywordLinkQueryBeans()");

        // TBD
    	Results<ScoredDocument> docs = findDocuments(queryString, ordering, offset, count);
    	if(docs == null) {
    		return null;
    	}
    	List<KeywordLinkQueryBean> keywordLinkQueryBeans = new ArrayList<KeywordLinkQueryBean>();
    	for(ScoredDocument doc : docs) {
    		KeywordLinkQueryBean bean = new KeywordLinkQueryBean();

    	    int rank = doc.getRank();
    		bean.setRank(rank);
    		String guid = doc.getId();
    		bean.setKeywordLinkGuid(guid);
    		int cntMemo = doc.getFieldCount("memo");
    		if(cntMemo >= 1) {
    			if(cntMemo == 1) {
                    Field fMemo = doc.getOnlyField("memo");
                    String vMemo = fMemo.getText();
                    bean.setMemo(vMemo);
    			} else {
    				log.warning("More than one Field, memo, present.");
    			}
    		} else {
                log.info("Field, memo, not present.");
    		}
    		int cntKeyword = doc.getFieldCount("keyword");
    		if(cntKeyword >= 1) {
    			if(cntKeyword == 1) {
                    Field fKeyword = doc.getOnlyField("keyword");
                    String vKeyword = fKeyword.getText();
                    bean.setKeyword(vKeyword);
    			} else {
    				log.warning("More than one Field, keyword, present.");
    			}
    		} else {
                log.info("Field, keyword, not present.");
    		}

   	        keywordLinkQueryBeans.add(bean);
    	}

    	log.finer("END: findKeywordLinkQueryBeans()");
        return keywordLinkQueryBeans;
    }

    // Note: This is very inefficient....
    public List<KeywordLink> findKeywordLinks(String queryString, String ordering, Long offset, Integer count)
    {
    	log.finer("BEGIN: findKeywordLinks()");

        // TBD
    	Results<ScoredDocument> docs = findDocuments(queryString, ordering, offset, count);
    	if(docs == null) {
    		return null;
    	}
    	List<KeywordLink> keywordLinks = new ArrayList<KeywordLink>();
    	for(ScoredDocument doc : docs) {
    		String guid = doc.getId();
    		KeywordLink keywordLink = null;
    		try {
        		keywordLink = ServiceFactoryManager.getServiceFactory().getKeywordLinkService().getKeywordLink(guid);
			} catch (BaseException e) {
				if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Failed to get KeywordLink for guid = " + guid, e);
			}
    		if(keywordLink != null) {
    			keywordLinks.add(keywordLink);
    		}
    	}

    	log.finer("END: findKeywordLinks()");
        return keywordLinks;
    }


    public Results<ScoredDocument> findDocuments(String queryString, String ordering, Long offset, Integer count)
    {
    	log.finer("BEGIN: findDocuments()");

        try {
        	if(count == null) {
        		count = DEFAULT_COUNT;
        	} else if(count > MAX_COUNT) {
        		count = MAX_COUNT;
        	}
        	if(offset == null || offset < 0L) {
        		offset = 0L;
        	}
        	QueryOptions.Builder queryOptionsBuilder = QueryOptions.newBuilder();
        	
        	if(ordering != null && !ordering.isEmpty()) {
        		String[] parts = ordering.split("\\s+", 2);
        		SortExpression.Builder sortExpressionBuilder = SortExpression.newBuilder();
        		if(parts.length > 0) {
        			sortExpressionBuilder.setExpression(parts[0]);
        			if(parts.length > 1) {
        				if(parts[1].equalsIgnoreCase("desc")) {
        					sortExpressionBuilder.setDirection(SortExpression.SortDirection.DESCENDING);
        				} else {
        					sortExpressionBuilder.setDirection(SortExpression.SortDirection.ASCENDING);
        				}
        			} else {
        				// Default...
        				sortExpressionBuilder.setDirection(SortExpression.SortDirection.ASCENDING);
        			}
        		}
        		sortExpressionBuilder.setDefaultValue("");   // ???
        		SortExpression sortExpression = sortExpressionBuilder.build();
        		SortOptions.Builder sortOptionsBuilder = SortOptions.newBuilder(); 
        		sortOptionsBuilder.addSortExpression(sortExpression);
        		sortOptionsBuilder.setLimit((int) (count + offset));  // ???
        		SortOptions sortOptions = sortOptionsBuilder.build();
        		queryOptionsBuilder.setSortOptions(sortOptions);
        	}

        	queryOptionsBuilder.setLimit(count);
        	queryOptionsBuilder.setOffset(offset.intValue());
        	//queryOptionsBuilder.setCursor(Cursor.newBuilder().build());

        	List<String> fieldsToReturn = new ArrayList<String>();
        	List<String> fieldsToSnippet = new ArrayList<String>();        	
            fieldsToReturn.add("memo");
            fieldsToReturn.add("keyword");
        	queryOptionsBuilder.setFieldsToReturn(fieldsToReturn.toArray(new String[0]));
        	if(!fieldsToSnippet.isEmpty()) {
                queryOptionsBuilder.setFieldsToSnippet(fieldsToSnippet.toArray(new String[0]));
            }

            QueryOptions options = queryOptionsBuilder.build();
            Query query = Query.newBuilder().setOptions(options).build(queryString);

            log.finer("END: findDocuments()");
            return keywordLinkIndexBuilder.getIndex().search(query);
        } catch (SearchException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Search request with query " + queryString + " failed", e);
            return null;
        } catch (Exception e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Search request with query " + queryString + " failed due to unknown error", e);
            return null;
        }
    }

}

