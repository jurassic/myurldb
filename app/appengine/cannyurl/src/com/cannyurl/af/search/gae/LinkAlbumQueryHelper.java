package com.cannyurl.af.search.gae;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.search.Field;
import com.google.appengine.api.search.GeoPoint;
import com.google.appengine.api.search.Cursor;
import com.google.appengine.api.search.Query;
import com.google.appengine.api.search.QueryOptions;
import com.google.appengine.api.search.Results;
import com.google.appengine.api.search.SearchException;
import com.google.appengine.api.search.ScoredDocument;
import com.google.appengine.api.search.SortExpression;
import com.google.appengine.api.search.SortOptions;
import com.google.appengine.api.search.SearchException;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.LinkAlbum;
import com.myurldb.ws.search.bean.LinkAlbumQueryBean;
import com.myurldb.ws.search.gae.LinkAlbumIndexBuilder;
import com.cannyurl.af.service.manager.ServiceFactoryManager;


public class LinkAlbumQueryHelper extends BaseQueryHelper
{
    private static final Logger log = Logger.getLogger(LinkAlbumQueryHelper.class.getName());
    
    
    private LinkAlbumIndexBuilder linkAlbumIndexBuilder = null;

    public LinkAlbumQueryHelper()
    {
    	this(null);
    }
    public LinkAlbumQueryHelper(LinkAlbumIndexBuilder linkAlbumIndexBuilder)
    {
        super();
        if(linkAlbumIndexBuilder != null) {
        	this.linkAlbumIndexBuilder = linkAlbumIndexBuilder;
        } else {
        	this.linkAlbumIndexBuilder = new LinkAlbumIndexBuilder();
        }
    }

    // TBD:    
    public List<LinkAlbumQueryBean> findLinkAlbumQueryBeans(String queryString, String ordering, Long offset, Integer count)
    {
    	log.finer("BEGIN: findLinkAlbumQueryBeans()");

        // TBD
    	Results<ScoredDocument> docs = findDocuments(queryString, ordering, offset, count);
    	if(docs == null) {
    		return null;
    	}
    	List<LinkAlbumQueryBean> linkAlbumQueryBeans = new ArrayList<LinkAlbumQueryBean>();
    	for(ScoredDocument doc : docs) {
    		LinkAlbumQueryBean bean = new LinkAlbumQueryBean();

    	    int rank = doc.getRank();
    		bean.setRank(rank);
    		String guid = doc.getId();
    		bean.setLinkAlbumGuid(guid);
    		int cntName = doc.getFieldCount("name");
    		if(cntName >= 1) {
    			if(cntName == 1) {
                    Field fName = doc.getOnlyField("name");
                    String vName = fName.getText();
                    bean.setName(vName);
    			} else {
    				log.warning("More than one Field, name, present.");
    			}
    		} else {
                log.info("Field, name, not present.");
    		}
    		int cntSummary = doc.getFieldCount("summary");
    		if(cntSummary >= 1) {
    			if(cntSummary == 1) {
                    Field fSummary = doc.getOnlyField("summary");
                    String vSummary = fSummary.getText();
                    bean.setSummary(vSummary);
    			} else {
    				log.warning("More than one Field, summary, present.");
    			}
    		} else {
                log.info("Field, summary, not present.");
    		}
    		int cntNote = doc.getFieldCount("note");
    		if(cntNote >= 1) {
    			if(cntNote == 1) {
                    Field fNote = doc.getOnlyField("note");
                    String vNote = fNote.getText();
                    bean.setNote(vNote);
    			} else {
    				log.warning("More than one Field, note, present.");
    			}
    		} else {
                log.info("Field, note, not present.");
    		}

   	        linkAlbumQueryBeans.add(bean);
    	}

    	log.finer("END: findLinkAlbumQueryBeans()");
        return linkAlbumQueryBeans;
    }

    // Note: This is very inefficient....
    public List<LinkAlbum> findLinkAlbums(String queryString, String ordering, Long offset, Integer count)
    {
    	log.finer("BEGIN: findLinkAlbums()");

        // TBD
    	Results<ScoredDocument> docs = findDocuments(queryString, ordering, offset, count);
    	if(docs == null) {
    		return null;
    	}
    	List<LinkAlbum> linkAlbums = new ArrayList<LinkAlbum>();
    	for(ScoredDocument doc : docs) {
    		String guid = doc.getId();
    		LinkAlbum linkAlbum = null;
    		try {
        		linkAlbum = ServiceFactoryManager.getServiceFactory().getLinkAlbumService().getLinkAlbum(guid);
			} catch (BaseException e) {
				if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Failed to get LinkAlbum for guid = " + guid, e);
			}
    		if(linkAlbum != null) {
    			linkAlbums.add(linkAlbum);
    		}
    	}

    	log.finer("END: findLinkAlbums()");
        return linkAlbums;
    }


    public Results<ScoredDocument> findDocuments(String queryString, String ordering, Long offset, Integer count)
    {
    	log.finer("BEGIN: findDocuments()");

        try {
        	if(count == null) {
        		count = DEFAULT_COUNT;
        	} else if(count > MAX_COUNT) {
        		count = MAX_COUNT;
        	}
        	if(offset == null || offset < 0L) {
        		offset = 0L;
        	}
        	QueryOptions.Builder queryOptionsBuilder = QueryOptions.newBuilder();
        	
        	if(ordering != null && !ordering.isEmpty()) {
        		String[] parts = ordering.split("\\s+", 2);
        		SortExpression.Builder sortExpressionBuilder = SortExpression.newBuilder();
        		if(parts.length > 0) {
        			sortExpressionBuilder.setExpression(parts[0]);
        			if(parts.length > 1) {
        				if(parts[1].equalsIgnoreCase("desc")) {
        					sortExpressionBuilder.setDirection(SortExpression.SortDirection.DESCENDING);
        				} else {
        					sortExpressionBuilder.setDirection(SortExpression.SortDirection.ASCENDING);
        				}
        			} else {
        				// Default...
        				sortExpressionBuilder.setDirection(SortExpression.SortDirection.ASCENDING);
        			}
        		}
        		sortExpressionBuilder.setDefaultValue("");   // ???
        		SortExpression sortExpression = sortExpressionBuilder.build();
        		SortOptions.Builder sortOptionsBuilder = SortOptions.newBuilder(); 
        		sortOptionsBuilder.addSortExpression(sortExpression);
        		sortOptionsBuilder.setLimit((int) (count + offset));  // ???
        		SortOptions sortOptions = sortOptionsBuilder.build();
        		queryOptionsBuilder.setSortOptions(sortOptions);
        	}

        	queryOptionsBuilder.setLimit(count);
        	queryOptionsBuilder.setOffset(offset.intValue());
        	//queryOptionsBuilder.setCursor(Cursor.newBuilder().build());

        	List<String> fieldsToReturn = new ArrayList<String>();
        	List<String> fieldsToSnippet = new ArrayList<String>();        	
            fieldsToReturn.add("name");
            fieldsToReturn.add("summary");
            fieldsToReturn.add("note");
        	queryOptionsBuilder.setFieldsToReturn(fieldsToReturn.toArray(new String[0]));
        	if(!fieldsToSnippet.isEmpty()) {
                queryOptionsBuilder.setFieldsToSnippet(fieldsToSnippet.toArray(new String[0]));
            }

            QueryOptions options = queryOptionsBuilder.build();
            Query query = Query.newBuilder().setOptions(options).build(queryString);

            log.finer("END: findDocuments()");
            return linkAlbumIndexBuilder.getIndex().search(query);
        } catch (SearchException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Search request with query " + queryString + " failed", e);
            return null;
        } catch (Exception e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Search request with query " + queryString + " failed due to unknown error", e);
            return null;
        }
    }

}

