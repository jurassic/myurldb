package com.cannyurl.af.search.gae;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.search.Field;
import com.google.appengine.api.search.GeoPoint;
import com.google.appengine.api.search.Cursor;
import com.google.appengine.api.search.Query;
import com.google.appengine.api.search.QueryOptions;
import com.google.appengine.api.search.Results;
import com.google.appengine.api.search.SearchException;
import com.google.appengine.api.search.ScoredDocument;
import com.google.appengine.api.search.SortExpression;
import com.google.appengine.api.search.SortOptions;
import com.google.appengine.api.search.SearchException;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.ReferrerInfoStruct;
import com.myurldb.ws.ShortLink;
import com.myurldb.ws.search.bean.ShortLinkQueryBean;
import com.myurldb.ws.search.gae.ShortLinkIndexBuilder;
import com.cannyurl.af.bean.ReferrerInfoStructBean;
import com.cannyurl.af.service.manager.ServiceFactoryManager;


public class ShortLinkQueryHelper extends BaseQueryHelper
{
    private static final Logger log = Logger.getLogger(ShortLinkQueryHelper.class.getName());
    
    
    private ShortLinkIndexBuilder shortLinkIndexBuilder = null;

    public ShortLinkQueryHelper()
    {
    	this(null);
    }
    public ShortLinkQueryHelper(ShortLinkIndexBuilder shortLinkIndexBuilder)
    {
        super();
        if(shortLinkIndexBuilder != null) {
        	this.shortLinkIndexBuilder = shortLinkIndexBuilder;
        } else {
        	this.shortLinkIndexBuilder = new ShortLinkIndexBuilder();
        }
    }

    // TBD:    
    public List<ShortLinkQueryBean> findShortLinkQueryBeans(String queryString, String ordering, Long offset, Integer count)
    {
    	log.finer("BEGIN: findShortLinkQueryBeans()");

        // TBD
    	Results<ScoredDocument> docs = findDocuments(queryString, ordering, offset, count);
    	if(docs == null) {
    		return null;
    	}
    	List<ShortLinkQueryBean> shortLinkQueryBeans = new ArrayList<ShortLinkQueryBean>();
    	for(ScoredDocument doc : docs) {
    		ShortLinkQueryBean bean = new ShortLinkQueryBean();

    	    int rank = doc.getRank();
    		bean.setRank(rank);
    		String guid = doc.getId();
    		bean.setShortLinkGuid(guid);
    		int cntDisplayMessage = doc.getFieldCount("displayMessage");
    		if(cntDisplayMessage >= 1) {
    			if(cntDisplayMessage == 1) {
                    Field fDisplayMessage = doc.getOnlyField("displayMessage");
                    String vDisplayMessage = fDisplayMessage.getText();
                    bean.setDisplayMessage(vDisplayMessage);
    			} else {
    				log.warning("More than one Field, displayMessage, present.");
    			}
    		} else {
                log.info("Field, displayMessage, not present.");
    		}
    		int cntShortMessage = doc.getFieldCount("shortMessage");
    		if(cntShortMessage >= 1) {
    			if(cntShortMessage == 1) {
                    Field fShortMessage = doc.getOnlyField("shortMessage");
                    String vShortMessage = fShortMessage.getText();
                    bean.setShortMessage(vShortMessage);
    			} else {
    				log.warning("More than one Field, shortMessage, present.");
    			}
    		} else {
                log.info("Field, shortMessage, not present.");
    		}

   	        shortLinkQueryBeans.add(bean);
    	}

    	log.finer("END: findShortLinkQueryBeans()");
        return shortLinkQueryBeans;
    }

    // Note: This is very inefficient....
    public List<ShortLink> findShortLinks(String queryString, String ordering, Long offset, Integer count)
    {
    	log.finer("BEGIN: findShortLinks()");

        // TBD
    	Results<ScoredDocument> docs = findDocuments(queryString, ordering, offset, count);
    	if(docs == null) {
    		return null;
    	}
    	List<ShortLink> shortLinks = new ArrayList<ShortLink>();
    	for(ScoredDocument doc : docs) {
    		String guid = doc.getId();
    		ShortLink shortLink = null;
    		try {
        		shortLink = ServiceFactoryManager.getServiceFactory().getShortLinkService().getShortLink(guid);
			} catch (BaseException e) {
				if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Failed to get ShortLink for guid = " + guid, e);
			}
    		if(shortLink != null) {
    			shortLinks.add(shortLink);
    		}
    	}

    	log.finer("END: findShortLinks()");
        return shortLinks;
    }


    public Results<ScoredDocument> findDocuments(String queryString, String ordering, Long offset, Integer count)
    {
    	log.finer("BEGIN: findDocuments()");

        try {
        	if(count == null) {
        		count = DEFAULT_COUNT;
        	} else if(count > MAX_COUNT) {
        		count = MAX_COUNT;
        	}
        	if(offset == null || offset < 0L) {
        		offset = 0L;
        	}
        	QueryOptions.Builder queryOptionsBuilder = QueryOptions.newBuilder();
        	
        	if(ordering != null && !ordering.isEmpty()) {
        		String[] parts = ordering.split("\\s+", 2);
        		SortExpression.Builder sortExpressionBuilder = SortExpression.newBuilder();
        		if(parts.length > 0) {
        			sortExpressionBuilder.setExpression(parts[0]);
        			if(parts.length > 1) {
        				if(parts[1].equalsIgnoreCase("desc")) {
        					sortExpressionBuilder.setDirection(SortExpression.SortDirection.DESCENDING);
        				} else {
        					sortExpressionBuilder.setDirection(SortExpression.SortDirection.ASCENDING);
        				}
        			} else {
        				// Default...
        				sortExpressionBuilder.setDirection(SortExpression.SortDirection.ASCENDING);
        			}
        		}
        		sortExpressionBuilder.setDefaultValue("");   // ???
        		SortExpression sortExpression = sortExpressionBuilder.build();
        		SortOptions.Builder sortOptionsBuilder = SortOptions.newBuilder(); 
        		sortOptionsBuilder.addSortExpression(sortExpression);
        		sortOptionsBuilder.setLimit((int) (count + offset));  // ???
        		SortOptions sortOptions = sortOptionsBuilder.build();
        		queryOptionsBuilder.setSortOptions(sortOptions);
        	}

        	queryOptionsBuilder.setLimit(count);
        	queryOptionsBuilder.setOffset(offset.intValue());
        	//queryOptionsBuilder.setCursor(Cursor.newBuilder().build());

        	List<String> fieldsToReturn = new ArrayList<String>();
        	List<String> fieldsToSnippet = new ArrayList<String>();        	
            fieldsToReturn.add("displayMessage");
            fieldsToReturn.add("shortMessage");
        	queryOptionsBuilder.setFieldsToReturn(fieldsToReturn.toArray(new String[0]));
        	if(!fieldsToSnippet.isEmpty()) {
                queryOptionsBuilder.setFieldsToSnippet(fieldsToSnippet.toArray(new String[0]));
            }

            QueryOptions options = queryOptionsBuilder.build();
            Query query = Query.newBuilder().setOptions(options).build(queryString);

            log.finer("END: findDocuments()");
            return shortLinkIndexBuilder.getIndex().search(query);
        } catch (SearchException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Search request with query " + queryString + " failed", e);
            return null;
        } catch (Exception e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Search request with query " + queryString + " failed due to unknown error", e);
            return null;
        }
    }

}

