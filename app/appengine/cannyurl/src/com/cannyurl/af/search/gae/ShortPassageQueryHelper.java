package com.cannyurl.af.search.gae;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.search.Field;
import com.google.appengine.api.search.GeoPoint;
import com.google.appengine.api.search.Cursor;
import com.google.appengine.api.search.Query;
import com.google.appengine.api.search.QueryOptions;
import com.google.appengine.api.search.Results;
import com.google.appengine.api.search.SearchException;
import com.google.appengine.api.search.ScoredDocument;
import com.google.appengine.api.search.SortExpression;
import com.google.appengine.api.search.SortOptions;
import com.google.appengine.api.search.SearchException;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.ShortPassageAttribute;
import com.myurldb.ws.ShortPassage;
import com.myurldb.ws.search.bean.ShortPassageQueryBean;
import com.myurldb.ws.search.gae.ShortPassageIndexBuilder;
import com.cannyurl.af.bean.ShortPassageAttributeBean;
import com.cannyurl.af.service.manager.ServiceFactoryManager;


public class ShortPassageQueryHelper extends BaseQueryHelper
{
    private static final Logger log = Logger.getLogger(ShortPassageQueryHelper.class.getName());
    
    
    private ShortPassageIndexBuilder shortPassageIndexBuilder = null;

    public ShortPassageQueryHelper()
    {
    	this(null);
    }
    public ShortPassageQueryHelper(ShortPassageIndexBuilder shortPassageIndexBuilder)
    {
        super();
        if(shortPassageIndexBuilder != null) {
        	this.shortPassageIndexBuilder = shortPassageIndexBuilder;
        } else {
        	this.shortPassageIndexBuilder = new ShortPassageIndexBuilder();
        }
    }

    // TBD:    
    public List<ShortPassageQueryBean> findShortPassageQueryBeans(String queryString, String ordering, Long offset, Integer count)
    {
    	log.finer("BEGIN: findShortPassageQueryBeans()");

        // TBD
    	Results<ScoredDocument> docs = findDocuments(queryString, ordering, offset, count);
    	if(docs == null) {
    		return null;
    	}
    	List<ShortPassageQueryBean> shortPassageQueryBeans = new ArrayList<ShortPassageQueryBean>();
    	for(ScoredDocument doc : docs) {
    		ShortPassageQueryBean bean = new ShortPassageQueryBean();

    	    int rank = doc.getRank();
    		bean.setRank(rank);
    		String guid = doc.getId();
    		bean.setShortPassageGuid(guid);
    		int cntShortText = doc.getFieldCount("shortText");
    		if(cntShortText >= 1) {
    			if(cntShortText == 1) {
                    Field fShortText = doc.getOnlyField("shortText");
                    String vShortText = fShortText.getText();
                    bean.setShortText(vShortText);
    			} else {
    				log.warning("More than one Field, shortText, present.");
    			}
    		} else {
                log.info("Field, shortText, not present.");
    		}

   	        shortPassageQueryBeans.add(bean);
    	}

    	log.finer("END: findShortPassageQueryBeans()");
        return shortPassageQueryBeans;
    }

    // Note: This is very inefficient....
    public List<ShortPassage> findShortPassages(String queryString, String ordering, Long offset, Integer count)
    {
    	log.finer("BEGIN: findShortPassages()");

        // TBD
    	Results<ScoredDocument> docs = findDocuments(queryString, ordering, offset, count);
    	if(docs == null) {
    		return null;
    	}
    	List<ShortPassage> shortPassages = new ArrayList<ShortPassage>();
    	for(ScoredDocument doc : docs) {
    		String guid = doc.getId();
    		ShortPassage shortPassage = null;
    		try {
        		shortPassage = ServiceFactoryManager.getServiceFactory().getShortPassageService().getShortPassage(guid);
			} catch (BaseException e) {
				if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Failed to get ShortPassage for guid = " + guid, e);
			}
    		if(shortPassage != null) {
    			shortPassages.add(shortPassage);
    		}
    	}

    	log.finer("END: findShortPassages()");
        return shortPassages;
    }


    public Results<ScoredDocument> findDocuments(String queryString, String ordering, Long offset, Integer count)
    {
    	log.finer("BEGIN: findDocuments()");

        try {
        	if(count == null) {
        		count = DEFAULT_COUNT;
        	} else if(count > MAX_COUNT) {
        		count = MAX_COUNT;
        	}
        	if(offset == null || offset < 0L) {
        		offset = 0L;
        	}
        	QueryOptions.Builder queryOptionsBuilder = QueryOptions.newBuilder();
        	
        	if(ordering != null && !ordering.isEmpty()) {
        		String[] parts = ordering.split("\\s+", 2);
        		SortExpression.Builder sortExpressionBuilder = SortExpression.newBuilder();
        		if(parts.length > 0) {
        			sortExpressionBuilder.setExpression(parts[0]);
        			if(parts.length > 1) {
        				if(parts[1].equalsIgnoreCase("desc")) {
        					sortExpressionBuilder.setDirection(SortExpression.SortDirection.DESCENDING);
        				} else {
        					sortExpressionBuilder.setDirection(SortExpression.SortDirection.ASCENDING);
        				}
        			} else {
        				// Default...
        				sortExpressionBuilder.setDirection(SortExpression.SortDirection.ASCENDING);
        			}
        		}
        		sortExpressionBuilder.setDefaultValue("");   // ???
        		SortExpression sortExpression = sortExpressionBuilder.build();
        		SortOptions.Builder sortOptionsBuilder = SortOptions.newBuilder(); 
        		sortOptionsBuilder.addSortExpression(sortExpression);
        		sortOptionsBuilder.setLimit((int) (count + offset));  // ???
        		SortOptions sortOptions = sortOptionsBuilder.build();
        		queryOptionsBuilder.setSortOptions(sortOptions);
        	}

        	queryOptionsBuilder.setLimit(count);
        	queryOptionsBuilder.setOffset(offset.intValue());
        	//queryOptionsBuilder.setCursor(Cursor.newBuilder().build());

        	List<String> fieldsToReturn = new ArrayList<String>();
        	List<String> fieldsToSnippet = new ArrayList<String>();        	
            fieldsToReturn.add("shortText");
        	queryOptionsBuilder.setFieldsToReturn(fieldsToReturn.toArray(new String[0]));
        	if(!fieldsToSnippet.isEmpty()) {
                queryOptionsBuilder.setFieldsToSnippet(fieldsToSnippet.toArray(new String[0]));
            }

            QueryOptions options = queryOptionsBuilder.build();
            Query query = Query.newBuilder().setOptions(options).build(queryString);

            log.finer("END: findDocuments()");
            return shortPassageIndexBuilder.getIndex().search(query);
        } catch (SearchException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Search request with query " + queryString + " failed", e);
            return null;
        } catch (Exception e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Search request with query " + queryString + " failed due to unknown error", e);
            return null;
        }
    }

}

