package com.cannyurl.af.search.gae;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.search.Field;
import com.google.appengine.api.search.GeoPoint;
import com.google.appengine.api.search.Cursor;
import com.google.appengine.api.search.Query;
import com.google.appengine.api.search.QueryOptions;
import com.google.appengine.api.search.Results;
import com.google.appengine.api.search.SearchException;
import com.google.appengine.api.search.ScoredDocument;
import com.google.appengine.api.search.SortExpression;
import com.google.appengine.api.search.SortOptions;
import com.google.appengine.api.search.SearchException;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.GeoPointStruct;
import com.myurldb.ws.StreetAddressStruct;
import com.myurldb.ws.GaeAppStruct;
import com.myurldb.ws.FullNameStruct;
import com.myurldb.ws.GaeUserStruct;
import com.myurldb.ws.User;
import com.myurldb.ws.search.bean.UserQueryBean;
import com.myurldb.ws.search.gae.UserIndexBuilder;
import com.cannyurl.af.bean.GeoPointStructBean;
import com.cannyurl.af.bean.StreetAddressStructBean;
import com.cannyurl.af.bean.GaeAppStructBean;
import com.cannyurl.af.bean.FullNameStructBean;
import com.cannyurl.af.bean.GaeUserStructBean;
import com.cannyurl.af.service.manager.ServiceFactoryManager;


public class UserQueryHelper extends BaseQueryHelper
{
    private static final Logger log = Logger.getLogger(UserQueryHelper.class.getName());
    
    
    private UserIndexBuilder userIndexBuilder = null;

    public UserQueryHelper()
    {
    	this(null);
    }
    public UserQueryHelper(UserIndexBuilder userIndexBuilder)
    {
        super();
        if(userIndexBuilder != null) {
        	this.userIndexBuilder = userIndexBuilder;
        } else {
        	this.userIndexBuilder = new UserIndexBuilder();
        }
    }

    // TBD:    
    public List<UserQueryBean> findUserQueryBeans(String queryString, String ordering, Long offset, Integer count)
    {
    	log.finer("BEGIN: findUserQueryBeans()");

        // TBD
    	Results<ScoredDocument> docs = findDocuments(queryString, ordering, offset, count);
    	if(docs == null) {
    		return null;
    	}
    	List<UserQueryBean> userQueryBeans = new ArrayList<UserQueryBean>();
    	for(ScoredDocument doc : docs) {
    		UserQueryBean bean = new UserQueryBean();

    	    int rank = doc.getRank();
    		bean.setRank(rank);
    		String guid = doc.getId();
    		bean.setUserGuid(guid);
    		int cntNickname = doc.getFieldCount("nickname");
    		if(cntNickname >= 1) {
    			if(cntNickname == 1) {
                    Field fNickname = doc.getOnlyField("nickname");
                    String vNickname = fNickname.getText();
                    bean.setNickname(vNickname);
    			} else {
    				log.warning("More than one Field, nickname, present.");
    			}
    		} else {
                log.info("Field, nickname, not present.");
    		}
    		int cntLocation = doc.getFieldCount("location");
    		if(cntLocation >= 1) {
    			if(cntLocation == 1) {
                    Field fLocation = doc.getOnlyField("location");
                    String vLocation = fLocation.getText();
                    bean.setLocation(vLocation);
    			} else {
    				log.warning("More than one Field, location, present.");
    			}
    		} else {
                log.info("Field, location, not present.");
    		}
    		int cntGeoPoint = doc.getFieldCount("geoPoint");
    		if(cntGeoPoint >= 1) {
    			if(cntGeoPoint == 1) {
                    Field fGeoPoint = doc.getOnlyField("geoPoint");
                    GeoPoint gpGeoPoint = fGeoPoint.getGeoPoint();
                    GeoPointStructBean vGeoPoint = new GeoPointStructBean();
                    vGeoPoint.setLatitude(gpGeoPoint.getLatitude());
                    vGeoPoint.setLongitude(gpGeoPoint.getLongitude());
                    bean.setGeoPoint(vGeoPoint);
    			} else {
    				log.warning("More than one Field, geoPoint, present.");
    			}
    		} else {
                log.info("Field, geoPoint, not present.");
    		}

   	        userQueryBeans.add(bean);
    	}

    	log.finer("END: findUserQueryBeans()");
        return userQueryBeans;
    }

    // Note: This is very inefficient....
    public List<User> findUsers(String queryString, String ordering, Long offset, Integer count)
    {
    	log.finer("BEGIN: findUsers()");

        // TBD
    	Results<ScoredDocument> docs = findDocuments(queryString, ordering, offset, count);
    	if(docs == null) {
    		return null;
    	}
    	List<User> users = new ArrayList<User>();
    	for(ScoredDocument doc : docs) {
    		String guid = doc.getId();
    		User user = null;
    		try {
        		user = ServiceFactoryManager.getServiceFactory().getUserService().getUser(guid);
			} catch (BaseException e) {
				if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Failed to get User for guid = " + guid, e);
			}
    		if(user != null) {
    			users.add(user);
    		}
    	}

    	log.finer("END: findUsers()");
        return users;
    }


    public Results<ScoredDocument> findDocuments(String queryString, String ordering, Long offset, Integer count)
    {
    	log.finer("BEGIN: findDocuments()");

        try {
        	if(count == null) {
        		count = DEFAULT_COUNT;
        	} else if(count > MAX_COUNT) {
        		count = MAX_COUNT;
        	}
        	if(offset == null || offset < 0L) {
        		offset = 0L;
        	}
        	QueryOptions.Builder queryOptionsBuilder = QueryOptions.newBuilder();
        	
        	if(ordering != null && !ordering.isEmpty()) {
        		String[] parts = ordering.split("\\s+", 2);
        		SortExpression.Builder sortExpressionBuilder = SortExpression.newBuilder();
        		if(parts.length > 0) {
        			sortExpressionBuilder.setExpression(parts[0]);
        			if(parts.length > 1) {
        				if(parts[1].equalsIgnoreCase("desc")) {
        					sortExpressionBuilder.setDirection(SortExpression.SortDirection.DESCENDING);
        				} else {
        					sortExpressionBuilder.setDirection(SortExpression.SortDirection.ASCENDING);
        				}
        			} else {
        				// Default...
        				sortExpressionBuilder.setDirection(SortExpression.SortDirection.ASCENDING);
        			}
        		}
        		sortExpressionBuilder.setDefaultValue("");   // ???
        		SortExpression sortExpression = sortExpressionBuilder.build();
        		SortOptions.Builder sortOptionsBuilder = SortOptions.newBuilder(); 
        		sortOptionsBuilder.addSortExpression(sortExpression);
        		sortOptionsBuilder.setLimit((int) (count + offset));  // ???
        		SortOptions sortOptions = sortOptionsBuilder.build();
        		queryOptionsBuilder.setSortOptions(sortOptions);
        	}

        	queryOptionsBuilder.setLimit(count);
        	queryOptionsBuilder.setOffset(offset.intValue());
        	//queryOptionsBuilder.setCursor(Cursor.newBuilder().build());

        	List<String> fieldsToReturn = new ArrayList<String>();
        	List<String> fieldsToSnippet = new ArrayList<String>();        	
            fieldsToReturn.add("nickname");
            fieldsToReturn.add("location");
            fieldsToReturn.add("geoPoint");
        	queryOptionsBuilder.setFieldsToReturn(fieldsToReturn.toArray(new String[0]));
        	if(!fieldsToSnippet.isEmpty()) {
                queryOptionsBuilder.setFieldsToSnippet(fieldsToSnippet.toArray(new String[0]));
            }

            QueryOptions options = queryOptionsBuilder.build();
            Query query = Query.newBuilder().setOptions(options).build(queryString);

            log.finer("END: findDocuments()");
            return userIndexBuilder.getIndex().search(query);
        } catch (SearchException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Search request with query " + queryString + " failed", e);
            return null;
        } catch (Exception e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Search request with query " + queryString + " failed due to unknown error", e);
            return null;
        }
    }

}

