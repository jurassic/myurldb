package com.cannyurl.af.service;

public abstract class AbstractServiceFactory
{
    public abstract ApiConsumerService getApiConsumerService();
    public abstract AppCustomDomainService getAppCustomDomainService();
    public abstract SiteCustomDomainService getSiteCustomDomainService();
    public abstract UserService getUserService();
    public abstract UserUsercodeService getUserUsercodeService();
    public abstract UserPasswordService getUserPasswordService();
    public abstract ExternalUserAuthService getExternalUserAuthService();
    public abstract UserAuthStateService getUserAuthStateService();
    public abstract UserResourcePermissionService getUserResourcePermissionService();
    public abstract UserResourceProhibitionService getUserResourceProhibitionService();
    public abstract RolePermissionService getRolePermissionService();
    public abstract UserRoleService getUserRoleService();
    public abstract AppClientService getAppClientService();
    public abstract ClientUserService getClientUserService();
    public abstract UserCustomDomainService getUserCustomDomainService();
    public abstract ClientSettingService getClientSettingService();
    public abstract UserSettingService getUserSettingService();
    public abstract VisitorSettingService getVisitorSettingService();
    public abstract TwitterSummaryCardService getTwitterSummaryCardService();
    public abstract TwitterPhotoCardService getTwitterPhotoCardService();
    public abstract TwitterGalleryCardService getTwitterGalleryCardService();
    public abstract TwitterAppCardService getTwitterAppCardService();
    public abstract TwitterPlayerCardService getTwitterPlayerCardService();
    public abstract TwitterProductCardService getTwitterProductCardService();
    public abstract ShortPassageService getShortPassageService();
    public abstract ShortLinkService getShortLinkService();
    public abstract GeoLinkService getGeoLinkService();
    public abstract QrCodeService getQrCodeService();
    public abstract LinkPassphraseService getLinkPassphraseService();
    public abstract LinkMessageService getLinkMessageService();
    public abstract LinkAlbumService getLinkAlbumService();
    public abstract AlbumShortLinkService getAlbumShortLinkService();
    public abstract KeywordFolderService getKeywordFolderService();
    public abstract BookmarkFolderService getBookmarkFolderService();
    public abstract KeywordLinkService getKeywordLinkService();
    public abstract BookmarkLinkService getBookmarkLinkService();
    public abstract SpeedDialService getSpeedDialService();
    public abstract KeywordFolderImportService getKeywordFolderImportService();
    public abstract BookmarkFolderImportService getBookmarkFolderImportService();
    public abstract KeywordCrowdTallyService getKeywordCrowdTallyService();
    public abstract BookmarkCrowdTallyService getBookmarkCrowdTallyService();
    public abstract DomainInfoService getDomainInfoService();
    public abstract UrlRatingService getUrlRatingService();
    public abstract UserRatingService getUserRatingService();
    public abstract AbuseTagService getAbuseTagService();
    public abstract ServiceInfoService getServiceInfoService();
    public abstract FiveTenService getFiveTenService();

}
