package com.cannyurl.af.service;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.AlbumShortLink;

// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface AlbumShortLinkService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    AlbumShortLink getAlbumShortLink(String guid) throws BaseException;
    Object getAlbumShortLink(String guid, String field) throws BaseException;
    List<AlbumShortLink> getAlbumShortLinks(List<String> guids) throws BaseException;
    List<AlbumShortLink> getAllAlbumShortLinks() throws BaseException;
    List<AlbumShortLink> getAllAlbumShortLinks(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllAlbumShortLinkKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<AlbumShortLink> findAlbumShortLinks(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<AlbumShortLink> findAlbumShortLinks(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findAlbumShortLinkKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createAlbumShortLink(String user, String linkAlbum, String shortLink, String shortUrl, String longUrl, String note, String status) throws BaseException;
    //String createAlbumShortLink(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return AlbumShortLink?)
    String createAlbumShortLink(AlbumShortLink albumShortLink) throws BaseException;
    AlbumShortLink constructAlbumShortLink(AlbumShortLink albumShortLink) throws BaseException;
    Boolean updateAlbumShortLink(String guid, String user, String linkAlbum, String shortLink, String shortUrl, String longUrl, String note, String status) throws BaseException;
    //Boolean updateAlbumShortLink(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateAlbumShortLink(AlbumShortLink albumShortLink) throws BaseException;
    AlbumShortLink refreshAlbumShortLink(AlbumShortLink albumShortLink) throws BaseException;
    Boolean deleteAlbumShortLink(String guid) throws BaseException;
    Boolean deleteAlbumShortLink(AlbumShortLink albumShortLink) throws BaseException;
    Long deleteAlbumShortLinks(String filter, String params, List<String> values) throws BaseException;

    // TBD
    Integer createAlbumShortLinks(List<AlbumShortLink> albumShortLinks) throws BaseException;
//    Boolean updateAlbumShortLinks(List<AlbumShortLink> albumShortLinks) throws BaseException;

}
