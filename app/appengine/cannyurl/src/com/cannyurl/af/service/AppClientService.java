package com.cannyurl.af.service;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.AppClient;

// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface AppClientService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    AppClient getAppClient(String guid) throws BaseException;
    Object getAppClient(String guid, String field) throws BaseException;
    List<AppClient> getAppClients(List<String> guids) throws BaseException;
    List<AppClient> getAllAppClients() throws BaseException;
    List<AppClient> getAllAppClients(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllAppClientKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<AppClient> findAppClients(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<AppClient> findAppClients(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findAppClientKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createAppClient(String owner, String admin, String name, String clientId, String clientCode, String rootDomain, String appDomain, Boolean useAppDomain, Boolean enabled, String licenseMode, String status) throws BaseException;
    //String createAppClient(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return AppClient?)
    String createAppClient(AppClient appClient) throws BaseException;
    AppClient constructAppClient(AppClient appClient) throws BaseException;
    Boolean updateAppClient(String guid, String owner, String admin, String name, String clientId, String clientCode, String rootDomain, String appDomain, Boolean useAppDomain, Boolean enabled, String licenseMode, String status) throws BaseException;
    //Boolean updateAppClient(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateAppClient(AppClient appClient) throws BaseException;
    AppClient refreshAppClient(AppClient appClient) throws BaseException;
    Boolean deleteAppClient(String guid) throws BaseException;
    Boolean deleteAppClient(AppClient appClient) throws BaseException;
    Long deleteAppClients(String filter, String params, List<String> values) throws BaseException;

    // TBD
    Integer createAppClients(List<AppClient> appClients) throws BaseException;
//    Boolean updateAppClients(List<AppClient> appClients) throws BaseException;

}
