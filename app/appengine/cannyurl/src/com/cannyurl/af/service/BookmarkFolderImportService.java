package com.cannyurl.af.service;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.BookmarkFolderImport;

// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface BookmarkFolderImportService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    BookmarkFolderImport getBookmarkFolderImport(String guid) throws BaseException;
    Object getBookmarkFolderImport(String guid, String field) throws BaseException;
    List<BookmarkFolderImport> getBookmarkFolderImports(List<String> guids) throws BaseException;
    List<BookmarkFolderImport> getAllBookmarkFolderImports() throws BaseException;
    List<BookmarkFolderImport> getAllBookmarkFolderImports(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllBookmarkFolderImportKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<BookmarkFolderImport> findBookmarkFolderImports(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<BookmarkFolderImport> findBookmarkFolderImports(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findBookmarkFolderImportKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createBookmarkFolderImport(String user, Integer precedence, String importType, String importedFolder, String importedFolderUser, String importedFolderTitle, String importedFolderPath, String status, String note, String bookmarkFolder) throws BaseException;
    //String createBookmarkFolderImport(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return BookmarkFolderImport?)
    String createBookmarkFolderImport(BookmarkFolderImport bookmarkFolderImport) throws BaseException;
    BookmarkFolderImport constructBookmarkFolderImport(BookmarkFolderImport bookmarkFolderImport) throws BaseException;
    Boolean updateBookmarkFolderImport(String guid, String user, Integer precedence, String importType, String importedFolder, String importedFolderUser, String importedFolderTitle, String importedFolderPath, String status, String note, String bookmarkFolder) throws BaseException;
    //Boolean updateBookmarkFolderImport(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateBookmarkFolderImport(BookmarkFolderImport bookmarkFolderImport) throws BaseException;
    BookmarkFolderImport refreshBookmarkFolderImport(BookmarkFolderImport bookmarkFolderImport) throws BaseException;
    Boolean deleteBookmarkFolderImport(String guid) throws BaseException;
    Boolean deleteBookmarkFolderImport(BookmarkFolderImport bookmarkFolderImport) throws BaseException;
    Long deleteBookmarkFolderImports(String filter, String params, List<String> values) throws BaseException;

    // TBD
    Integer createBookmarkFolderImports(List<BookmarkFolderImport> bookmarkFolderImports) throws BaseException;
//    Boolean updateBookmarkFolderImports(List<BookmarkFolderImport> bookmarkFolderImports) throws BaseException;

}
