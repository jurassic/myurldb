package com.cannyurl.af.service;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.BookmarkLink;

// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface BookmarkLinkService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    BookmarkLink getBookmarkLink(String guid) throws BaseException;
    Object getBookmarkLink(String guid, String field) throws BaseException;
    List<BookmarkLink> getBookmarkLinks(List<String> guids) throws BaseException;
    List<BookmarkLink> getAllBookmarkLinks() throws BaseException;
    List<BookmarkLink> getAllBookmarkLinks(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllBookmarkLinkKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<BookmarkLink> findBookmarkLinks(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<BookmarkLink> findBookmarkLinks(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findBookmarkLinkKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createBookmarkLink(String appClient, String clientRootDomain, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, Boolean internal, Boolean caching, String memo, String status, String note, Long expirationTime, String bookmarkFolder, String contentTag, String referenceElement, String elementType, String keywordLink) throws BaseException;
    //String createBookmarkLink(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return BookmarkLink?)
    String createBookmarkLink(BookmarkLink bookmarkLink) throws BaseException;
    BookmarkLink constructBookmarkLink(BookmarkLink bookmarkLink) throws BaseException;
    Boolean updateBookmarkLink(String guid, String appClient, String clientRootDomain, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, Boolean internal, Boolean caching, String memo, String status, String note, Long expirationTime, String bookmarkFolder, String contentTag, String referenceElement, String elementType, String keywordLink) throws BaseException;
    //Boolean updateBookmarkLink(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateBookmarkLink(BookmarkLink bookmarkLink) throws BaseException;
    BookmarkLink refreshBookmarkLink(BookmarkLink bookmarkLink) throws BaseException;
    Boolean deleteBookmarkLink(String guid) throws BaseException;
    Boolean deleteBookmarkLink(BookmarkLink bookmarkLink) throws BaseException;
    Long deleteBookmarkLinks(String filter, String params, List<String> values) throws BaseException;

    // TBD
    Integer createBookmarkLinks(List<BookmarkLink> bookmarkLinks) throws BaseException;
//    Boolean updateBookmarkLinks(List<BookmarkLink> bookmarkLinks) throws BaseException;

}
