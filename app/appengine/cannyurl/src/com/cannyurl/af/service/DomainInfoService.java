package com.cannyurl.af.service;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.DomainInfo;

// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface DomainInfoService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    DomainInfo getDomainInfo(String guid) throws BaseException;
    Object getDomainInfo(String guid, String field) throws BaseException;
    List<DomainInfo> getDomainInfos(List<String> guids) throws BaseException;
    List<DomainInfo> getAllDomainInfos() throws BaseException;
    List<DomainInfo> getAllDomainInfos(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllDomainInfoKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<DomainInfo> findDomainInfos(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<DomainInfo> findDomainInfos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findDomainInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createDomainInfo(String domain, Boolean banned, Boolean urlShortener, String category, String reputation, String authority, String note, Long verifiedTime) throws BaseException;
    //String createDomainInfo(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return DomainInfo?)
    String createDomainInfo(DomainInfo domainInfo) throws BaseException;
    DomainInfo constructDomainInfo(DomainInfo domainInfo) throws BaseException;
    Boolean updateDomainInfo(String guid, String domain, Boolean banned, Boolean urlShortener, String category, String reputation, String authority, String note, Long verifiedTime) throws BaseException;
    //Boolean updateDomainInfo(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateDomainInfo(DomainInfo domainInfo) throws BaseException;
    DomainInfo refreshDomainInfo(DomainInfo domainInfo) throws BaseException;
    Boolean deleteDomainInfo(String guid) throws BaseException;
    Boolean deleteDomainInfo(DomainInfo domainInfo) throws BaseException;
    Long deleteDomainInfos(String filter, String params, List<String> values) throws BaseException;

    // TBD
    Integer createDomainInfos(List<DomainInfo> domainInfos) throws BaseException;
//    Boolean updateDomainInfos(List<DomainInfo> domainInfos) throws BaseException;

}
