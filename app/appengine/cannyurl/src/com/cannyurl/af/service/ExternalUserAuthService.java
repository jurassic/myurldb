package com.cannyurl.af.service;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.ExternalUserIdStruct;
import com.myurldb.ws.ExternalUserAuth;

// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface ExternalUserAuthService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    ExternalUserAuth getExternalUserAuth(String guid) throws BaseException;
    Object getExternalUserAuth(String guid, String field) throws BaseException;
    List<ExternalUserAuth> getExternalUserAuths(List<String> guids) throws BaseException;
    List<ExternalUserAuth> getAllExternalUserAuths() throws BaseException;
    List<ExternalUserAuth> getAllExternalUserAuths(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllExternalUserAuthKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<ExternalUserAuth> findExternalUserAuths(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<ExternalUserAuth> findExternalUserAuths(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findExternalUserAuthKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createExternalUserAuth(String user, String authType, String providerId, String providerDomain, ExternalUserIdStruct externalUserId, String requestToken, String accessToken, String accessTokenSecret, String email, String firstName, String lastName, String fullName, String displayName, String description, String gender, String dateOfBirth, String profileImageUrl, String timeZone, String postalCode, String location, String country, String language, String status, Long authTime, Long expirationTime) throws BaseException;
    //String createExternalUserAuth(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return ExternalUserAuth?)
    String createExternalUserAuth(ExternalUserAuth externalUserAuth) throws BaseException;
    ExternalUserAuth constructExternalUserAuth(ExternalUserAuth externalUserAuth) throws BaseException;
    Boolean updateExternalUserAuth(String guid, String user, String authType, String providerId, String providerDomain, ExternalUserIdStruct externalUserId, String requestToken, String accessToken, String accessTokenSecret, String email, String firstName, String lastName, String fullName, String displayName, String description, String gender, String dateOfBirth, String profileImageUrl, String timeZone, String postalCode, String location, String country, String language, String status, Long authTime, Long expirationTime) throws BaseException;
    //Boolean updateExternalUserAuth(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateExternalUserAuth(ExternalUserAuth externalUserAuth) throws BaseException;
    ExternalUserAuth refreshExternalUserAuth(ExternalUserAuth externalUserAuth) throws BaseException;
    Boolean deleteExternalUserAuth(String guid) throws BaseException;
    Boolean deleteExternalUserAuth(ExternalUserAuth externalUserAuth) throws BaseException;
    Long deleteExternalUserAuths(String filter, String params, List<String> values) throws BaseException;

    // TBD
    Integer createExternalUserAuths(List<ExternalUserAuth> externalUserAuths) throws BaseException;
//    Boolean updateExternalUserAuths(List<ExternalUserAuth> externalUserAuths) throws BaseException;

}
