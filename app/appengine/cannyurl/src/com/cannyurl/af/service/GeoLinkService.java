package com.cannyurl.af.service;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.CellLatitudeLongitude;
import com.myurldb.ws.GeoCoordinateStruct;
import com.myurldb.ws.GeoLink;

// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface GeoLinkService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    GeoLink getGeoLink(String guid) throws BaseException;
    Object getGeoLink(String guid, String field) throws BaseException;
    List<GeoLink> getGeoLinks(List<String> guids) throws BaseException;
    List<GeoLink> getAllGeoLinks() throws BaseException;
    List<GeoLink> getAllGeoLinks(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllGeoLinkKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<GeoLink> findGeoLinks(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<GeoLink> findGeoLinks(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findGeoLinkKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createGeoLink(String shortLink, String shortUrl, GeoCoordinateStruct geoCoordinate, CellLatitudeLongitude geoCell, String status) throws BaseException;
    //String createGeoLink(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return GeoLink?)
    String createGeoLink(GeoLink geoLink) throws BaseException;
    GeoLink constructGeoLink(GeoLink geoLink) throws BaseException;
    Boolean updateGeoLink(String guid, String shortLink, String shortUrl, GeoCoordinateStruct geoCoordinate, CellLatitudeLongitude geoCell, String status) throws BaseException;
    //Boolean updateGeoLink(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateGeoLink(GeoLink geoLink) throws BaseException;
    GeoLink refreshGeoLink(GeoLink geoLink) throws BaseException;
    Boolean deleteGeoLink(String guid) throws BaseException;
    Boolean deleteGeoLink(GeoLink geoLink) throws BaseException;
    Long deleteGeoLinks(String filter, String params, List<String> values) throws BaseException;

    // TBD
    Integer createGeoLinks(List<GeoLink> geoLinks) throws BaseException;
//    Boolean updateGeoLinks(List<GeoLink> geoLinks) throws BaseException;

}
