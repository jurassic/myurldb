package com.cannyurl.af.service;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.KeywordFolder;

// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface KeywordFolderService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    KeywordFolder getKeywordFolder(String guid) throws BaseException;
    Object getKeywordFolder(String guid, String field) throws BaseException;
    List<KeywordFolder> getKeywordFolders(List<String> guids) throws BaseException;
    List<KeywordFolder> getAllKeywordFolders() throws BaseException;
    List<KeywordFolder> getAllKeywordFolders(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllKeywordFolderKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<KeywordFolder> findKeywordFolders(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<KeywordFolder> findKeywordFolders(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findKeywordFolderKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createKeywordFolder(String appClient, String clientRootDomain, String user, String title, String description, String type, String category, String parent, String aggregate, String acl, Boolean exportable, String status, String note, String folderPath) throws BaseException;
    //String createKeywordFolder(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return KeywordFolder?)
    String createKeywordFolder(KeywordFolder keywordFolder) throws BaseException;
    KeywordFolder constructKeywordFolder(KeywordFolder keywordFolder) throws BaseException;
    Boolean updateKeywordFolder(String guid, String appClient, String clientRootDomain, String user, String title, String description, String type, String category, String parent, String aggregate, String acl, Boolean exportable, String status, String note, String folderPath) throws BaseException;
    //Boolean updateKeywordFolder(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateKeywordFolder(KeywordFolder keywordFolder) throws BaseException;
    KeywordFolder refreshKeywordFolder(KeywordFolder keywordFolder) throws BaseException;
    Boolean deleteKeywordFolder(String guid) throws BaseException;
    Boolean deleteKeywordFolder(KeywordFolder keywordFolder) throws BaseException;
    Long deleteKeywordFolders(String filter, String params, List<String> values) throws BaseException;

    // TBD
    Integer createKeywordFolders(List<KeywordFolder> keywordFolders) throws BaseException;
//    Boolean updateKeywordFolders(List<KeywordFolder> keywordFolders) throws BaseException;

}
