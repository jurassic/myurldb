package com.cannyurl.af.service;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.LinkMessage;

// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface LinkMessageService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    LinkMessage getLinkMessage(String guid) throws BaseException;
    Object getLinkMessage(String guid, String field) throws BaseException;
    List<LinkMessage> getLinkMessages(List<String> guids) throws BaseException;
    List<LinkMessage> getAllLinkMessages() throws BaseException;
    List<LinkMessage> getAllLinkMessages(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllLinkMessageKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<LinkMessage> findLinkMessages(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<LinkMessage> findLinkMessages(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findLinkMessageKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createLinkMessage(String shortLink, String message, String password) throws BaseException;
    //String createLinkMessage(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return LinkMessage?)
    String createLinkMessage(LinkMessage linkMessage) throws BaseException;
    LinkMessage constructLinkMessage(LinkMessage linkMessage) throws BaseException;
    Boolean updateLinkMessage(String guid, String shortLink, String message, String password) throws BaseException;
    //Boolean updateLinkMessage(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateLinkMessage(LinkMessage linkMessage) throws BaseException;
    LinkMessage refreshLinkMessage(LinkMessage linkMessage) throws BaseException;
    Boolean deleteLinkMessage(String guid) throws BaseException;
    Boolean deleteLinkMessage(LinkMessage linkMessage) throws BaseException;
    Long deleteLinkMessages(String filter, String params, List<String> values) throws BaseException;

    // TBD
    Integer createLinkMessages(List<LinkMessage> linkMessages) throws BaseException;
//    Boolean updateLinkMessages(List<LinkMessage> linkMessages) throws BaseException;

}
