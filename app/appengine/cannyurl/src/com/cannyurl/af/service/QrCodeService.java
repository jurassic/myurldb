package com.cannyurl.af.service;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.QrCode;

// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface QrCodeService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    QrCode getQrCode(String guid) throws BaseException;
    Object getQrCode(String guid, String field) throws BaseException;
    List<QrCode> getQrCodes(List<String> guids) throws BaseException;
    List<QrCode> getAllQrCodes() throws BaseException;
    List<QrCode> getAllQrCodes(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllQrCodeKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<QrCode> findQrCodes(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<QrCode> findQrCodes(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findQrCodeKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createQrCode(String shortLink, String imageLink, String imageUrl, String type, String status) throws BaseException;
    //String createQrCode(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return QrCode?)
    String createQrCode(QrCode qrCode) throws BaseException;
    QrCode constructQrCode(QrCode qrCode) throws BaseException;
    Boolean updateQrCode(String guid, String shortLink, String imageLink, String imageUrl, String type, String status) throws BaseException;
    //Boolean updateQrCode(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateQrCode(QrCode qrCode) throws BaseException;
    QrCode refreshQrCode(QrCode qrCode) throws BaseException;
    Boolean deleteQrCode(String guid) throws BaseException;
    Boolean deleteQrCode(QrCode qrCode) throws BaseException;
    Long deleteQrCodes(String filter, String params, List<String> values) throws BaseException;

    // TBD
    Integer createQrCodes(List<QrCode> qrCodes) throws BaseException;
//    Boolean updateQrCodes(List<QrCode> qrCodes) throws BaseException;

}
