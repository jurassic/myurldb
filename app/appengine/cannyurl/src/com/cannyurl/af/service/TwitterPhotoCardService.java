package com.cannyurl.af.service;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.TwitterPhotoCard;

// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface TwitterPhotoCardService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    TwitterPhotoCard getTwitterPhotoCard(String guid) throws BaseException;
    Object getTwitterPhotoCard(String guid, String field) throws BaseException;
    List<TwitterPhotoCard> getTwitterPhotoCards(List<String> guids) throws BaseException;
    List<TwitterPhotoCard> getAllTwitterPhotoCards() throws BaseException;
    List<TwitterPhotoCard> getAllTwitterPhotoCards(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllTwitterPhotoCardKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<TwitterPhotoCard> findTwitterPhotoCards(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<TwitterPhotoCard> findTwitterPhotoCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findTwitterPhotoCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createTwitterPhotoCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight) throws BaseException;
    //String createTwitterPhotoCard(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return TwitterPhotoCard?)
    String createTwitterPhotoCard(TwitterPhotoCard twitterPhotoCard) throws BaseException;
    TwitterPhotoCard constructTwitterPhotoCard(TwitterPhotoCard twitterPhotoCard) throws BaseException;
    Boolean updateTwitterPhotoCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight) throws BaseException;
    //Boolean updateTwitterPhotoCard(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateTwitterPhotoCard(TwitterPhotoCard twitterPhotoCard) throws BaseException;
    TwitterPhotoCard refreshTwitterPhotoCard(TwitterPhotoCard twitterPhotoCard) throws BaseException;
    Boolean deleteTwitterPhotoCard(String guid) throws BaseException;
    Boolean deleteTwitterPhotoCard(TwitterPhotoCard twitterPhotoCard) throws BaseException;
    Long deleteTwitterPhotoCards(String filter, String params, List<String> values) throws BaseException;

    // TBD
    Integer createTwitterPhotoCards(List<TwitterPhotoCard> twitterPhotoCards) throws BaseException;
//    Boolean updateTwitterPhotoCards(List<TwitterPhotoCard> twitterPhotoCards) throws BaseException;

}
