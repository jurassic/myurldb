package com.cannyurl.af.service;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.TwitterProductCard;

// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface TwitterProductCardService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    TwitterProductCard getTwitterProductCard(String guid) throws BaseException;
    Object getTwitterProductCard(String guid, String field) throws BaseException;
    List<TwitterProductCard> getTwitterProductCards(List<String> guids) throws BaseException;
    List<TwitterProductCard> getAllTwitterProductCards() throws BaseException;
    List<TwitterProductCard> getAllTwitterProductCards(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllTwitterProductCardKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<TwitterProductCard> findTwitterProductCards(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<TwitterProductCard> findTwitterProductCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findTwitterProductCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createTwitterProductCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, TwitterCardProductData data1, TwitterCardProductData data2) throws BaseException;
    //String createTwitterProductCard(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return TwitterProductCard?)
    String createTwitterProductCard(TwitterProductCard twitterProductCard) throws BaseException;
    TwitterProductCard constructTwitterProductCard(TwitterProductCard twitterProductCard) throws BaseException;
    Boolean updateTwitterProductCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, TwitterCardProductData data1, TwitterCardProductData data2) throws BaseException;
    //Boolean updateTwitterProductCard(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateTwitterProductCard(TwitterProductCard twitterProductCard) throws BaseException;
    TwitterProductCard refreshTwitterProductCard(TwitterProductCard twitterProductCard) throws BaseException;
    Boolean deleteTwitterProductCard(String guid) throws BaseException;
    Boolean deleteTwitterProductCard(TwitterProductCard twitterProductCard) throws BaseException;
    Long deleteTwitterProductCards(String filter, String params, List<String> values) throws BaseException;

    // TBD
    Integer createTwitterProductCards(List<TwitterProductCard> twitterProductCards) throws BaseException;
//    Boolean updateTwitterProductCards(List<TwitterProductCard> twitterProductCards) throws BaseException;

}
