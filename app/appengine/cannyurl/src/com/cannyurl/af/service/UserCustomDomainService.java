package com.cannyurl.af.service;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.UserCustomDomain;

// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface UserCustomDomainService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    UserCustomDomain getUserCustomDomain(String guid) throws BaseException;
    Object getUserCustomDomain(String guid, String field) throws BaseException;
    List<UserCustomDomain> getUserCustomDomains(List<String> guids) throws BaseException;
    List<UserCustomDomain> getAllUserCustomDomains() throws BaseException;
    List<UserCustomDomain> getAllUserCustomDomains(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllUserCustomDomainKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<UserCustomDomain> findUserCustomDomains(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<UserCustomDomain> findUserCustomDomains(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findUserCustomDomainKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createUserCustomDomain(String owner, String domain, Boolean verified, String status, Long verifiedTime) throws BaseException;
    //String createUserCustomDomain(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return UserCustomDomain?)
    String createUserCustomDomain(UserCustomDomain userCustomDomain) throws BaseException;
    UserCustomDomain constructUserCustomDomain(UserCustomDomain userCustomDomain) throws BaseException;
    Boolean updateUserCustomDomain(String guid, String owner, String domain, Boolean verified, String status, Long verifiedTime) throws BaseException;
    //Boolean updateUserCustomDomain(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateUserCustomDomain(UserCustomDomain userCustomDomain) throws BaseException;
    UserCustomDomain refreshUserCustomDomain(UserCustomDomain userCustomDomain) throws BaseException;
    Boolean deleteUserCustomDomain(String guid) throws BaseException;
    Boolean deleteUserCustomDomain(UserCustomDomain userCustomDomain) throws BaseException;
    Long deleteUserCustomDomains(String filter, String params, List<String> values) throws BaseException;

    // TBD
    Integer createUserCustomDomains(List<UserCustomDomain> userCustomDomains) throws BaseException;
//    Boolean updateUserCustomDomains(List<UserCustomDomain> userCustomDomains) throws BaseException;

}
