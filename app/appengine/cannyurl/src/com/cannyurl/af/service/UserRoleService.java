package com.cannyurl.af.service;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.UserRole;

// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface UserRoleService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    UserRole getUserRole(String guid) throws BaseException;
    Object getUserRole(String guid, String field) throws BaseException;
    List<UserRole> getUserRoles(List<String> guids) throws BaseException;
    List<UserRole> getAllUserRoles() throws BaseException;
    List<UserRole> getAllUserRoles(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllUserRoleKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<UserRole> findUserRoles(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<UserRole> findUserRoles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findUserRoleKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createUserRole(String user, String role, String status) throws BaseException;
    //String createUserRole(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return UserRole?)
    String createUserRole(UserRole userRole) throws BaseException;
    UserRole constructUserRole(UserRole userRole) throws BaseException;
    Boolean updateUserRole(String guid, String user, String role, String status) throws BaseException;
    //Boolean updateUserRole(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateUserRole(UserRole userRole) throws BaseException;
    UserRole refreshUserRole(UserRole userRole) throws BaseException;
    Boolean deleteUserRole(String guid) throws BaseException;
    Boolean deleteUserRole(UserRole userRole) throws BaseException;
    Long deleteUserRoles(String filter, String params, List<String> values) throws BaseException;

    // TBD
    Integer createUserRoles(List<UserRole> userRoles) throws BaseException;
//    Boolean updateUserRoles(List<UserRole> userRoles) throws BaseException;

}
