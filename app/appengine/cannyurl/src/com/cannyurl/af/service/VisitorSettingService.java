package com.cannyurl.af.service;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.VisitorSetting;

// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface VisitorSettingService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    VisitorSetting getVisitorSetting(String guid) throws BaseException;
    Object getVisitorSetting(String guid, String field) throws BaseException;
    List<VisitorSetting> getVisitorSettings(List<String> guids) throws BaseException;
    List<VisitorSetting> getAllVisitorSettings() throws BaseException;
    List<VisitorSetting> getAllVisitorSettings(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllVisitorSettingKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<VisitorSetting> findVisitorSettings(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<VisitorSetting> findVisitorSettings(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findVisitorSettingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createVisitorSetting(String user, String redirectType, Long flashDuration, String privacyType) throws BaseException;
    //String createVisitorSetting(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return VisitorSetting?)
    String createVisitorSetting(VisitorSetting visitorSetting) throws BaseException;
    VisitorSetting constructVisitorSetting(VisitorSetting visitorSetting) throws BaseException;
    Boolean updateVisitorSetting(String guid, String user, String redirectType, Long flashDuration, String privacyType) throws BaseException;
    //Boolean updateVisitorSetting(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateVisitorSetting(VisitorSetting visitorSetting) throws BaseException;
    VisitorSetting refreshVisitorSetting(VisitorSetting visitorSetting) throws BaseException;
    Boolean deleteVisitorSetting(String guid) throws BaseException;
    Boolean deleteVisitorSetting(VisitorSetting visitorSetting) throws BaseException;
    Long deleteVisitorSettings(String filter, String params, List<String> values) throws BaseException;

    // TBD
    Integer createVisitorSettings(List<VisitorSetting> visitorSettings) throws BaseException;
//    Boolean updateVisitorSettings(List<VisitorSetting> visitorSettings) throws BaseException;

}
