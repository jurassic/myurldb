package com.cannyurl.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.AbuseTag;
import com.cannyurl.af.config.Config;

import com.cannyurl.af.bean.HashedPasswordStructBean;
import com.cannyurl.af.bean.GeoPointStructBean;
import com.cannyurl.af.bean.WebProfileStructBean;
import com.cannyurl.af.bean.StreetAddressStructBean;
import com.cannyurl.af.bean.SiteLogBean;
import com.cannyurl.af.bean.HelpNoticeBean;
import com.cannyurl.af.bean.TwitterCardAppInfoBean;
import com.cannyurl.af.bean.ContactInfoStructBean;
import com.cannyurl.af.bean.GeoCoordinateStructBean;
import com.cannyurl.af.bean.FullNameStructBean;
import com.cannyurl.af.bean.ExternalUserIdStructBean;
import com.cannyurl.af.bean.GaeUserStructBean;
import com.cannyurl.af.bean.AppBrandStructBean;
import com.cannyurl.af.bean.CellLatitudeLongitudeBean;
import com.cannyurl.af.bean.GaeAppStructBean;
import com.cannyurl.af.bean.ShortPassageAttributeBean;
import com.cannyurl.af.bean.KeyValuePairStructBean;
import com.cannyurl.af.bean.KeyValueRelationStructBean;
import com.cannyurl.af.bean.ExternalServiceApiKeyStructBean;
import com.cannyurl.af.bean.ReferrerInfoStructBean;
import com.cannyurl.af.bean.TwitterCardProductDataBean;
import com.cannyurl.af.bean.PagerStateStructBean;

import com.cannyurl.af.bean.AbuseTagBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.AbuseTagService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class AbuseTagServiceImpl implements AbuseTagService
{
    private static final Logger log = Logger.getLogger(AbuseTagServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "AbuseTag-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("AbuseTag:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public AbuseTagServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // AbuseTag related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public AbuseTag getAbuseTag(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAbuseTag(): guid = " + guid);

        AbuseTagBean bean = null;
        if(getCache() != null) {
            bean = (AbuseTagBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (AbuseTagBean) getProxyFactory().getAbuseTagServiceProxy().getAbuseTag(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "AbuseTagBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve AbuseTagBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getAbuseTag(String guid, String field) throws BaseException
    {
        AbuseTagBean bean = null;
        if(getCache() != null) {
            bean = (AbuseTagBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (AbuseTagBean) getProxyFactory().getAbuseTagServiceProxy().getAbuseTag(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "AbuseTagBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve AbuseTagBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("shortLink")) {
            return bean.getShortLink();
        } else if(field.equals("shortUrl")) {
            return bean.getShortUrl();
        } else if(field.equals("longUrl")) {
            return bean.getLongUrl();
        } else if(field.equals("longUrlHash")) {
            return bean.getLongUrlHash();
        } else if(field.equals("user")) {
            return bean.getUser();
        } else if(field.equals("ipAddress")) {
            return bean.getIpAddress();
        } else if(field.equals("tag")) {
            return bean.getTag();
        } else if(field.equals("comment")) {
            return bean.getComment();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("reviewer")) {
            return bean.getReviewer();
        } else if(field.equals("action")) {
            return bean.getAction();
        } else if(field.equals("note")) {
            return bean.getNote();
        } else if(field.equals("pastActions")) {
            return bean.getPastActions();
        } else if(field.equals("reviewedTime")) {
            return bean.getReviewedTime();
        } else if(field.equals("actionTime")) {
            return bean.getActionTime();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<AbuseTag> getAbuseTags(List<String> guids) throws BaseException
    {
        log.fine("getAbuseTags()");

        // TBD: Is there a better way????
        List<AbuseTag> abuseTags = getProxyFactory().getAbuseTagServiceProxy().getAbuseTags(guids);
        if(abuseTags == null) {
            log.log(Level.WARNING, "Failed to retrieve AbuseTagBean list.");
        }

        log.finer("END");
        return abuseTags;
    }

    @Override
    public List<AbuseTag> getAllAbuseTags() throws BaseException
    {
        return getAllAbuseTags(null, null, null);
    }

    @Override
    public List<AbuseTag> getAllAbuseTags(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllAbuseTags(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<AbuseTag> abuseTags = getProxyFactory().getAbuseTagServiceProxy().getAllAbuseTags(ordering, offset, count);
        if(abuseTags == null) {
            log.log(Level.WARNING, "Failed to retrieve AbuseTagBean list.");
        }

        log.finer("END");
        return abuseTags;
    }

    @Override
    public List<String> getAllAbuseTagKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllAbuseTagKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getAbuseTagServiceProxy().getAllAbuseTagKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve AbuseTagBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty AbuseTagBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<AbuseTag> findAbuseTags(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findAbuseTags(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<AbuseTag> findAbuseTags(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("AbuseTagServiceImpl.findAbuseTags(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<AbuseTag> abuseTags = getProxyFactory().getAbuseTagServiceProxy().findAbuseTags(filter, ordering, params, values, grouping, unique, offset, count);
        if(abuseTags == null) {
            log.log(Level.WARNING, "Failed to find abuseTags for the given criterion.");
        }

        log.finer("END");
        return abuseTags;
    }

    @Override
    public List<String> findAbuseTagKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("AbuseTagServiceImpl.findAbuseTagKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getAbuseTagServiceProxy().findAbuseTagKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find AbuseTag keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty AbuseTag key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("AbuseTagServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getAbuseTagServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createAbuseTag(String shortLink, String shortUrl, String longUrl, String longUrlHash, String user, String ipAddress, Integer tag, String comment, String status, String reviewer, String action, String note, String pastActions, Long reviewedTime, Long actionTime) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        AbuseTagBean bean = new AbuseTagBean(null, shortLink, shortUrl, longUrl, longUrlHash, user, ipAddress, tag, comment, status, reviewer, action, note, pastActions, reviewedTime, actionTime);
        return createAbuseTag(bean);
    }

    @Override
    public String createAbuseTag(AbuseTag abuseTag) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //AbuseTag bean = constructAbuseTag(abuseTag);
        //return bean.getGuid();

        // Param abuseTag cannot be null.....
        if(abuseTag == null) {
            log.log(Level.INFO, "Param abuseTag is null!");
            throw new BadRequestException("Param abuseTag object is null!");
        }
        AbuseTagBean bean = null;
        if(abuseTag instanceof AbuseTagBean) {
            bean = (AbuseTagBean) abuseTag;
        } else if(abuseTag instanceof AbuseTag) {
            // bean = new AbuseTagBean(null, abuseTag.getShortLink(), abuseTag.getShortUrl(), abuseTag.getLongUrl(), abuseTag.getLongUrlHash(), abuseTag.getUser(), abuseTag.getIpAddress(), abuseTag.getTag(), abuseTag.getComment(), abuseTag.getStatus(), abuseTag.getReviewer(), abuseTag.getAction(), abuseTag.getNote(), abuseTag.getPastActions(), abuseTag.getReviewedTime(), abuseTag.getActionTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new AbuseTagBean(abuseTag.getGuid(), abuseTag.getShortLink(), abuseTag.getShortUrl(), abuseTag.getLongUrl(), abuseTag.getLongUrlHash(), abuseTag.getUser(), abuseTag.getIpAddress(), abuseTag.getTag(), abuseTag.getComment(), abuseTag.getStatus(), abuseTag.getReviewer(), abuseTag.getAction(), abuseTag.getNote(), abuseTag.getPastActions(), abuseTag.getReviewedTime(), abuseTag.getActionTime());
        } else {
            log.log(Level.WARNING, "createAbuseTag(): Arg abuseTag is of an unknown type.");
            //bean = new AbuseTagBean();
            bean = new AbuseTagBean(abuseTag.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getAbuseTagServiceProxy().createAbuseTag(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public AbuseTag constructAbuseTag(AbuseTag abuseTag) throws BaseException
    {
        log.finer("BEGIN");

        // Param abuseTag cannot be null.....
        if(abuseTag == null) {
            log.log(Level.INFO, "Param abuseTag is null!");
            throw new BadRequestException("Param abuseTag object is null!");
        }
        AbuseTagBean bean = null;
        if(abuseTag instanceof AbuseTagBean) {
            bean = (AbuseTagBean) abuseTag;
        } else if(abuseTag instanceof AbuseTag) {
            // bean = new AbuseTagBean(null, abuseTag.getShortLink(), abuseTag.getShortUrl(), abuseTag.getLongUrl(), abuseTag.getLongUrlHash(), abuseTag.getUser(), abuseTag.getIpAddress(), abuseTag.getTag(), abuseTag.getComment(), abuseTag.getStatus(), abuseTag.getReviewer(), abuseTag.getAction(), abuseTag.getNote(), abuseTag.getPastActions(), abuseTag.getReviewedTime(), abuseTag.getActionTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new AbuseTagBean(abuseTag.getGuid(), abuseTag.getShortLink(), abuseTag.getShortUrl(), abuseTag.getLongUrl(), abuseTag.getLongUrlHash(), abuseTag.getUser(), abuseTag.getIpAddress(), abuseTag.getTag(), abuseTag.getComment(), abuseTag.getStatus(), abuseTag.getReviewer(), abuseTag.getAction(), abuseTag.getNote(), abuseTag.getPastActions(), abuseTag.getReviewedTime(), abuseTag.getActionTime());
        } else {
            log.log(Level.WARNING, "createAbuseTag(): Arg abuseTag is of an unknown type.");
            //bean = new AbuseTagBean();
            bean = new AbuseTagBean(abuseTag.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getAbuseTagServiceProxy().createAbuseTag(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateAbuseTag(String guid, String shortLink, String shortUrl, String longUrl, String longUrlHash, String user, String ipAddress, Integer tag, String comment, String status, String reviewer, String action, String note, String pastActions, Long reviewedTime, Long actionTime) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        AbuseTagBean bean = new AbuseTagBean(guid, shortLink, shortUrl, longUrl, longUrlHash, user, ipAddress, tag, comment, status, reviewer, action, note, pastActions, reviewedTime, actionTime);
        return updateAbuseTag(bean);
    }
        
    @Override
    public Boolean updateAbuseTag(AbuseTag abuseTag) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //AbuseTag bean = refreshAbuseTag(abuseTag);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param abuseTag cannot be null.....
        if(abuseTag == null || abuseTag.getGuid() == null) {
            log.log(Level.INFO, "Param abuseTag or its guid is null!");
            throw new BadRequestException("Param abuseTag object or its guid is null!");
        }
        AbuseTagBean bean = null;
        if(abuseTag instanceof AbuseTagBean) {
            bean = (AbuseTagBean) abuseTag;
        } else {  // if(abuseTag instanceof AbuseTag)
            bean = new AbuseTagBean(abuseTag.getGuid(), abuseTag.getShortLink(), abuseTag.getShortUrl(), abuseTag.getLongUrl(), abuseTag.getLongUrlHash(), abuseTag.getUser(), abuseTag.getIpAddress(), abuseTag.getTag(), abuseTag.getComment(), abuseTag.getStatus(), abuseTag.getReviewer(), abuseTag.getAction(), abuseTag.getNote(), abuseTag.getPastActions(), abuseTag.getReviewedTime(), abuseTag.getActionTime());
        }
        Boolean suc = getProxyFactory().getAbuseTagServiceProxy().updateAbuseTag(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public AbuseTag refreshAbuseTag(AbuseTag abuseTag) throws BaseException
    {
        log.finer("BEGIN");

        // Param abuseTag cannot be null.....
        if(abuseTag == null || abuseTag.getGuid() == null) {
            log.log(Level.INFO, "Param abuseTag or its guid is null!");
            throw new BadRequestException("Param abuseTag object or its guid is null!");
        }
        AbuseTagBean bean = null;
        if(abuseTag instanceof AbuseTagBean) {
            bean = (AbuseTagBean) abuseTag;
        } else {  // if(abuseTag instanceof AbuseTag)
            bean = new AbuseTagBean(abuseTag.getGuid(), abuseTag.getShortLink(), abuseTag.getShortUrl(), abuseTag.getLongUrl(), abuseTag.getLongUrlHash(), abuseTag.getUser(), abuseTag.getIpAddress(), abuseTag.getTag(), abuseTag.getComment(), abuseTag.getStatus(), abuseTag.getReviewer(), abuseTag.getAction(), abuseTag.getNote(), abuseTag.getPastActions(), abuseTag.getReviewedTime(), abuseTag.getActionTime());
        }
        Boolean suc = getProxyFactory().getAbuseTagServiceProxy().updateAbuseTag(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteAbuseTag(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getAbuseTagServiceProxy().deleteAbuseTag(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }
            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            AbuseTag abuseTag = null;
            try {
                abuseTag = getProxyFactory().getAbuseTagServiceProxy().getAbuseTag(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch abuseTag with a key, " + guid);
                return false;
            }
            if(abuseTag != null) {
                String beanGuid = abuseTag.getGuid();
                Boolean suc1 = getProxyFactory().getAbuseTagServiceProxy().deleteAbuseTag(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("abuseTag with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteAbuseTag(AbuseTag abuseTag) throws BaseException
    {
        log.finer("BEGIN");

        // Param abuseTag cannot be null.....
        if(abuseTag == null || abuseTag.getGuid() == null) {
            log.log(Level.INFO, "Param abuseTag or its guid is null!");
            throw new BadRequestException("Param abuseTag object or its guid is null!");
        }
        AbuseTagBean bean = null;
        if(abuseTag instanceof AbuseTagBean) {
            bean = (AbuseTagBean) abuseTag;
        } else {  // if(abuseTag instanceof AbuseTag)
            // ????
            log.warning("abuseTag is not an instance of AbuseTagBean.");
            bean = new AbuseTagBean(abuseTag.getGuid(), abuseTag.getShortLink(), abuseTag.getShortUrl(), abuseTag.getLongUrl(), abuseTag.getLongUrlHash(), abuseTag.getUser(), abuseTag.getIpAddress(), abuseTag.getTag(), abuseTag.getComment(), abuseTag.getStatus(), abuseTag.getReviewer(), abuseTag.getAction(), abuseTag.getNote(), abuseTag.getPastActions(), abuseTag.getReviewedTime(), abuseTag.getActionTime());
        }
        Boolean suc = getProxyFactory().getAbuseTagServiceProxy().deleteAbuseTag(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteAbuseTags(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getAbuseTagServiceProxy().deleteAbuseTags(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createAbuseTags(List<AbuseTag> abuseTags) throws BaseException
    {
        log.finer("BEGIN");

        if(abuseTags == null) {
            log.log(Level.WARNING, "createAbuseTags() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = abuseTags.size();
        if(size == 0) {
            log.log(Level.WARNING, "createAbuseTags() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(AbuseTag abuseTag : abuseTags) {
            String guid = createAbuseTag(abuseTag);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createAbuseTags() failed for at least one abuseTag. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateAbuseTags(List<AbuseTag> abuseTags) throws BaseException
    //{
    //}

}
