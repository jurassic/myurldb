package com.cannyurl.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.AlbumShortLink;
import com.myurldb.ws.search.gae.AlbumShortLinkIndexBuilder;
import com.cannyurl.af.config.Config;

import com.cannyurl.af.bean.HashedPasswordStructBean;
import com.cannyurl.af.bean.GeoPointStructBean;
import com.cannyurl.af.bean.WebProfileStructBean;
import com.cannyurl.af.bean.StreetAddressStructBean;
import com.cannyurl.af.bean.SiteLogBean;
import com.cannyurl.af.bean.HelpNoticeBean;
import com.cannyurl.af.bean.TwitterCardAppInfoBean;
import com.cannyurl.af.bean.ContactInfoStructBean;
import com.cannyurl.af.bean.GeoCoordinateStructBean;
import com.cannyurl.af.bean.FullNameStructBean;
import com.cannyurl.af.bean.ExternalUserIdStructBean;
import com.cannyurl.af.bean.GaeUserStructBean;
import com.cannyurl.af.bean.AppBrandStructBean;
import com.cannyurl.af.bean.CellLatitudeLongitudeBean;
import com.cannyurl.af.bean.GaeAppStructBean;
import com.cannyurl.af.bean.ShortPassageAttributeBean;
import com.cannyurl.af.bean.KeyValuePairStructBean;
import com.cannyurl.af.bean.KeyValueRelationStructBean;
import com.cannyurl.af.bean.ExternalServiceApiKeyStructBean;
import com.cannyurl.af.bean.ReferrerInfoStructBean;
import com.cannyurl.af.bean.TwitterCardProductDataBean;
import com.cannyurl.af.bean.PagerStateStructBean;

import com.cannyurl.af.bean.AlbumShortLinkBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.AlbumShortLinkService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class AlbumShortLinkServiceImpl implements AlbumShortLinkService
{
    private static final Logger log = Logger.getLogger(AlbumShortLinkServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "AlbumShortLink-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("AlbumShortLink:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public AlbumShortLinkServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // AlbumShortLink related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public AlbumShortLink getAlbumShortLink(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAlbumShortLink(): guid = " + guid);

        AlbumShortLinkBean bean = null;
        if(getCache() != null) {
            bean = (AlbumShortLinkBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (AlbumShortLinkBean) getProxyFactory().getAlbumShortLinkServiceProxy().getAlbumShortLink(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "AlbumShortLinkBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve AlbumShortLinkBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getAlbumShortLink(String guid, String field) throws BaseException
    {
        AlbumShortLinkBean bean = null;
        if(getCache() != null) {
            bean = (AlbumShortLinkBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (AlbumShortLinkBean) getProxyFactory().getAlbumShortLinkServiceProxy().getAlbumShortLink(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "AlbumShortLinkBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve AlbumShortLinkBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return bean.getUser();
        } else if(field.equals("linkAlbum")) {
            return bean.getLinkAlbum();
        } else if(field.equals("shortLink")) {
            return bean.getShortLink();
        } else if(field.equals("shortUrl")) {
            return bean.getShortUrl();
        } else if(field.equals("longUrl")) {
            return bean.getLongUrl();
        } else if(field.equals("note")) {
            return bean.getNote();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<AlbumShortLink> getAlbumShortLinks(List<String> guids) throws BaseException
    {
        log.fine("getAlbumShortLinks()");

        // TBD: Is there a better way????
        List<AlbumShortLink> albumShortLinks = getProxyFactory().getAlbumShortLinkServiceProxy().getAlbumShortLinks(guids);
        if(albumShortLinks == null) {
            log.log(Level.WARNING, "Failed to retrieve AlbumShortLinkBean list.");
        }

        log.finer("END");
        return albumShortLinks;
    }

    @Override
    public List<AlbumShortLink> getAllAlbumShortLinks() throws BaseException
    {
        return getAllAlbumShortLinks(null, null, null);
    }

    @Override
    public List<AlbumShortLink> getAllAlbumShortLinks(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllAlbumShortLinks(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<AlbumShortLink> albumShortLinks = getProxyFactory().getAlbumShortLinkServiceProxy().getAllAlbumShortLinks(ordering, offset, count);
        if(albumShortLinks == null) {
            log.log(Level.WARNING, "Failed to retrieve AlbumShortLinkBean list.");
        }

        log.finer("END");
        return albumShortLinks;
    }

    @Override
    public List<String> getAllAlbumShortLinkKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllAlbumShortLinkKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getAlbumShortLinkServiceProxy().getAllAlbumShortLinkKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve AlbumShortLinkBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty AlbumShortLinkBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<AlbumShortLink> findAlbumShortLinks(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findAlbumShortLinks(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<AlbumShortLink> findAlbumShortLinks(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("AlbumShortLinkServiceImpl.findAlbumShortLinks(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<AlbumShortLink> albumShortLinks = getProxyFactory().getAlbumShortLinkServiceProxy().findAlbumShortLinks(filter, ordering, params, values, grouping, unique, offset, count);
        if(albumShortLinks == null) {
            log.log(Level.WARNING, "Failed to find albumShortLinks for the given criterion.");
        }

        log.finer("END");
        return albumShortLinks;
    }

    @Override
    public List<String> findAlbumShortLinkKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("AlbumShortLinkServiceImpl.findAlbumShortLinkKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getAlbumShortLinkServiceProxy().findAlbumShortLinkKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find AlbumShortLink keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty AlbumShortLink key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("AlbumShortLinkServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getAlbumShortLinkServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createAlbumShortLink(String user, String linkAlbum, String shortLink, String shortUrl, String longUrl, String note, String status) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        AlbumShortLinkBean bean = new AlbumShortLinkBean(null, user, linkAlbum, shortLink, shortUrl, longUrl, note, status);
        return createAlbumShortLink(bean);
    }

    @Override
    public String createAlbumShortLink(AlbumShortLink albumShortLink) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //AlbumShortLink bean = constructAlbumShortLink(albumShortLink);
        //return bean.getGuid();

        // Param albumShortLink cannot be null.....
        if(albumShortLink == null) {
            log.log(Level.INFO, "Param albumShortLink is null!");
            throw new BadRequestException("Param albumShortLink object is null!");
        }
        AlbumShortLinkBean bean = null;
        if(albumShortLink instanceof AlbumShortLinkBean) {
            bean = (AlbumShortLinkBean) albumShortLink;
        } else if(albumShortLink instanceof AlbumShortLink) {
            // bean = new AlbumShortLinkBean(null, albumShortLink.getUser(), albumShortLink.getLinkAlbum(), albumShortLink.getShortLink(), albumShortLink.getShortUrl(), albumShortLink.getLongUrl(), albumShortLink.getNote(), albumShortLink.getStatus());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new AlbumShortLinkBean(albumShortLink.getGuid(), albumShortLink.getUser(), albumShortLink.getLinkAlbum(), albumShortLink.getShortLink(), albumShortLink.getShortUrl(), albumShortLink.getLongUrl(), albumShortLink.getNote(), albumShortLink.getStatus());
        } else {
            log.log(Level.WARNING, "createAlbumShortLink(): Arg albumShortLink is of an unknown type.");
            //bean = new AlbumShortLinkBean();
            bean = new AlbumShortLinkBean(albumShortLink.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getAlbumShortLinkServiceProxy().createAlbumShortLink(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

	    // TBD:
	    if(Config.getInstance().isSearchEnabled()) {
            if(guid != null && !guid.isEmpty()) {
                log.fine("Calling addDocument()... for AlbumShortLink.");
                AlbumShortLinkIndexBuilder builder = new AlbumShortLinkIndexBuilder();
	    	    builder.addDocument(bean);
	    	}
	    }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public AlbumShortLink constructAlbumShortLink(AlbumShortLink albumShortLink) throws BaseException
    {
        log.finer("BEGIN");

        // Param albumShortLink cannot be null.....
        if(albumShortLink == null) {
            log.log(Level.INFO, "Param albumShortLink is null!");
            throw new BadRequestException("Param albumShortLink object is null!");
        }
        AlbumShortLinkBean bean = null;
        if(albumShortLink instanceof AlbumShortLinkBean) {
            bean = (AlbumShortLinkBean) albumShortLink;
        } else if(albumShortLink instanceof AlbumShortLink) {
            // bean = new AlbumShortLinkBean(null, albumShortLink.getUser(), albumShortLink.getLinkAlbum(), albumShortLink.getShortLink(), albumShortLink.getShortUrl(), albumShortLink.getLongUrl(), albumShortLink.getNote(), albumShortLink.getStatus());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new AlbumShortLinkBean(albumShortLink.getGuid(), albumShortLink.getUser(), albumShortLink.getLinkAlbum(), albumShortLink.getShortLink(), albumShortLink.getShortUrl(), albumShortLink.getLongUrl(), albumShortLink.getNote(), albumShortLink.getStatus());
        } else {
            log.log(Level.WARNING, "createAlbumShortLink(): Arg albumShortLink is of an unknown type.");
            //bean = new AlbumShortLinkBean();
            bean = new AlbumShortLinkBean(albumShortLink.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getAlbumShortLinkServiceProxy().createAlbumShortLink(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

	    // TBD:
	    if(Config.getInstance().isSearchEnabled()) {
            if(guid != null && !guid.isEmpty()) {
                log.fine("Calling addDocument()... for AlbumShortLink.");
                AlbumShortLinkIndexBuilder builder = new AlbumShortLinkIndexBuilder();
	    	    builder.addDocument(bean);
	    	}
	    }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateAlbumShortLink(String guid, String user, String linkAlbum, String shortLink, String shortUrl, String longUrl, String note, String status) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        AlbumShortLinkBean bean = new AlbumShortLinkBean(guid, user, linkAlbum, shortLink, shortUrl, longUrl, note, status);
        return updateAlbumShortLink(bean);
    }
        
    @Override
    public Boolean updateAlbumShortLink(AlbumShortLink albumShortLink) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //AlbumShortLink bean = refreshAlbumShortLink(albumShortLink);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param albumShortLink cannot be null.....
        if(albumShortLink == null || albumShortLink.getGuid() == null) {
            log.log(Level.INFO, "Param albumShortLink or its guid is null!");
            throw new BadRequestException("Param albumShortLink object or its guid is null!");
        }
        AlbumShortLinkBean bean = null;
        if(albumShortLink instanceof AlbumShortLinkBean) {
            bean = (AlbumShortLinkBean) albumShortLink;
        } else {  // if(albumShortLink instanceof AlbumShortLink)
            bean = new AlbumShortLinkBean(albumShortLink.getGuid(), albumShortLink.getUser(), albumShortLink.getLinkAlbum(), albumShortLink.getShortLink(), albumShortLink.getShortUrl(), albumShortLink.getLongUrl(), albumShortLink.getNote(), albumShortLink.getStatus());
        }
        Boolean suc = getProxyFactory().getAlbumShortLinkServiceProxy().updateAlbumShortLink(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

	    // TBD:
        if(Config.getInstance().isSearchEnabled()) {
            if(suc != null && suc.equals(Boolean.TRUE)) {
                log.fine("Calling addDocument()... for AlbumShortLink.");
	    	    AlbumShortLinkIndexBuilder builder = new AlbumShortLinkIndexBuilder();
	    	    builder.addDocument(bean);
	        }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public AlbumShortLink refreshAlbumShortLink(AlbumShortLink albumShortLink) throws BaseException
    {
        log.finer("BEGIN");

        // Param albumShortLink cannot be null.....
        if(albumShortLink == null || albumShortLink.getGuid() == null) {
            log.log(Level.INFO, "Param albumShortLink or its guid is null!");
            throw new BadRequestException("Param albumShortLink object or its guid is null!");
        }
        AlbumShortLinkBean bean = null;
        if(albumShortLink instanceof AlbumShortLinkBean) {
            bean = (AlbumShortLinkBean) albumShortLink;
        } else {  // if(albumShortLink instanceof AlbumShortLink)
            bean = new AlbumShortLinkBean(albumShortLink.getGuid(), albumShortLink.getUser(), albumShortLink.getLinkAlbum(), albumShortLink.getShortLink(), albumShortLink.getShortUrl(), albumShortLink.getLongUrl(), albumShortLink.getNote(), albumShortLink.getStatus());
        }
        Boolean suc = getProxyFactory().getAlbumShortLinkServiceProxy().updateAlbumShortLink(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

	    // TBD:
        if(Config.getInstance().isSearchEnabled()) {
            if(suc != null && suc.equals(Boolean.TRUE)) {
                log.fine("Calling addDocument()... for AlbumShortLink.");
	    	    AlbumShortLinkIndexBuilder builder = new AlbumShortLinkIndexBuilder();
	    	    builder.addDocument(bean);
	        }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteAlbumShortLink(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getAlbumShortLinkServiceProxy().deleteAlbumShortLink(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }

        // TBD:
        if(Config.getInstance().isSearchEnabled()) {
            if(suc != null && suc.equals(Boolean.TRUE)) {
                log.fine("Calling removeDocument()... for AlbumShortLink.");
	    	    AlbumShortLinkIndexBuilder builder = new AlbumShortLinkIndexBuilder();
                builder.removeDocument(guid);
	        }
        }

            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            AlbumShortLink albumShortLink = null;
            try {
                albumShortLink = getProxyFactory().getAlbumShortLinkServiceProxy().getAlbumShortLink(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch albumShortLink with a key, " + guid);
                return false;
            }
            if(albumShortLink != null) {
                String beanGuid = albumShortLink.getGuid();
                Boolean suc1 = getProxyFactory().getAlbumShortLinkServiceProxy().deleteAlbumShortLink(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("albumShortLink with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteAlbumShortLink(AlbumShortLink albumShortLink) throws BaseException
    {
        log.finer("BEGIN");

        // Param albumShortLink cannot be null.....
        if(albumShortLink == null || albumShortLink.getGuid() == null) {
            log.log(Level.INFO, "Param albumShortLink or its guid is null!");
            throw new BadRequestException("Param albumShortLink object or its guid is null!");
        }
        AlbumShortLinkBean bean = null;
        if(albumShortLink instanceof AlbumShortLinkBean) {
            bean = (AlbumShortLinkBean) albumShortLink;
        } else {  // if(albumShortLink instanceof AlbumShortLink)
            // ????
            log.warning("albumShortLink is not an instance of AlbumShortLinkBean.");
            bean = new AlbumShortLinkBean(albumShortLink.getGuid(), albumShortLink.getUser(), albumShortLink.getLinkAlbum(), albumShortLink.getShortLink(), albumShortLink.getShortUrl(), albumShortLink.getLongUrl(), albumShortLink.getNote(), albumShortLink.getStatus());
        }
        Boolean suc = getProxyFactory().getAlbumShortLinkServiceProxy().deleteAlbumShortLink(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

	    // TBD:
        if(Config.getInstance().isSearchEnabled()) {
            if(suc != null && suc.equals(Boolean.TRUE)) {
                log.fine("Calling removeDocument()... for AlbumShortLink.");
	    	    AlbumShortLinkIndexBuilder builder = new AlbumShortLinkIndexBuilder();
                builder.removeDocument(bean.getGuid());
	        }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteAlbumShortLinks(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getAlbumShortLinkServiceProxy().deleteAlbumShortLinks(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

	    // TBD:
        //if(Config.getInstance().isSearchEnabled()) {
        //    if(suc != null && suc.equals(Boolean.TRUE)) {
        //      log.fine("Calling removeDocument()... for AlbumShortLink.");
	    //	    AlbumShortLinkIndexBuilder builder = new AlbumShortLinkIndexBuilder();
        //      builder.removeDocument(guids);
	    //    }
        //}

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createAlbumShortLinks(List<AlbumShortLink> albumShortLinks) throws BaseException
    {
        log.finer("BEGIN");

        if(albumShortLinks == null) {
            log.log(Level.WARNING, "createAlbumShortLinks() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = albumShortLinks.size();
        if(size == 0) {
            log.log(Level.WARNING, "createAlbumShortLinks() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(AlbumShortLink albumShortLink : albumShortLinks) {
            String guid = createAlbumShortLink(albumShortLink);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createAlbumShortLinks() failed for at least one albumShortLink. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateAlbumShortLinks(List<AlbumShortLink> albumShortLinks) throws BaseException
    //{
    //}

}
