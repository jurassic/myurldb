package com.cannyurl.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.AppClient;
import com.cannyurl.af.config.Config;

import com.cannyurl.af.bean.HashedPasswordStructBean;
import com.cannyurl.af.bean.GeoPointStructBean;
import com.cannyurl.af.bean.WebProfileStructBean;
import com.cannyurl.af.bean.StreetAddressStructBean;
import com.cannyurl.af.bean.SiteLogBean;
import com.cannyurl.af.bean.HelpNoticeBean;
import com.cannyurl.af.bean.TwitterCardAppInfoBean;
import com.cannyurl.af.bean.ContactInfoStructBean;
import com.cannyurl.af.bean.GeoCoordinateStructBean;
import com.cannyurl.af.bean.FullNameStructBean;
import com.cannyurl.af.bean.ExternalUserIdStructBean;
import com.cannyurl.af.bean.GaeUserStructBean;
import com.cannyurl.af.bean.AppBrandStructBean;
import com.cannyurl.af.bean.CellLatitudeLongitudeBean;
import com.cannyurl.af.bean.GaeAppStructBean;
import com.cannyurl.af.bean.ShortPassageAttributeBean;
import com.cannyurl.af.bean.KeyValuePairStructBean;
import com.cannyurl.af.bean.KeyValueRelationStructBean;
import com.cannyurl.af.bean.ExternalServiceApiKeyStructBean;
import com.cannyurl.af.bean.ReferrerInfoStructBean;
import com.cannyurl.af.bean.TwitterCardProductDataBean;
import com.cannyurl.af.bean.PagerStateStructBean;

import com.cannyurl.af.bean.AppClientBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.AppClientService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class AppClientServiceImpl implements AppClientService
{
    private static final Logger log = Logger.getLogger(AppClientServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "AppClient-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("AppClient:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public AppClientServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // AppClient related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public AppClient getAppClient(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAppClient(): guid = " + guid);

        AppClientBean bean = null;
        if(getCache() != null) {
            bean = (AppClientBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (AppClientBean) getProxyFactory().getAppClientServiceProxy().getAppClient(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "AppClientBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve AppClientBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getAppClient(String guid, String field) throws BaseException
    {
        AppClientBean bean = null;
        if(getCache() != null) {
            bean = (AppClientBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (AppClientBean) getProxyFactory().getAppClientServiceProxy().getAppClient(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "AppClientBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve AppClientBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("owner")) {
            return bean.getOwner();
        } else if(field.equals("admin")) {
            return bean.getAdmin();
        } else if(field.equals("name")) {
            return bean.getName();
        } else if(field.equals("clientId")) {
            return bean.getClientId();
        } else if(field.equals("clientCode")) {
            return bean.getClientCode();
        } else if(field.equals("rootDomain")) {
            return bean.getRootDomain();
        } else if(field.equals("appDomain")) {
            return bean.getAppDomain();
        } else if(field.equals("useAppDomain")) {
            return bean.isUseAppDomain();
        } else if(field.equals("enabled")) {
            return bean.isEnabled();
        } else if(field.equals("licenseMode")) {
            return bean.getLicenseMode();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<AppClient> getAppClients(List<String> guids) throws BaseException
    {
        log.fine("getAppClients()");

        // TBD: Is there a better way????
        List<AppClient> appClients = getProxyFactory().getAppClientServiceProxy().getAppClients(guids);
        if(appClients == null) {
            log.log(Level.WARNING, "Failed to retrieve AppClientBean list.");
        }

        log.finer("END");
        return appClients;
    }

    @Override
    public List<AppClient> getAllAppClients() throws BaseException
    {
        return getAllAppClients(null, null, null);
    }

    @Override
    public List<AppClient> getAllAppClients(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllAppClients(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<AppClient> appClients = getProxyFactory().getAppClientServiceProxy().getAllAppClients(ordering, offset, count);
        if(appClients == null) {
            log.log(Level.WARNING, "Failed to retrieve AppClientBean list.");
        }

        log.finer("END");
        return appClients;
    }

    @Override
    public List<String> getAllAppClientKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllAppClientKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getAppClientServiceProxy().getAllAppClientKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve AppClientBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty AppClientBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<AppClient> findAppClients(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findAppClients(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<AppClient> findAppClients(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("AppClientServiceImpl.findAppClients(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<AppClient> appClients = getProxyFactory().getAppClientServiceProxy().findAppClients(filter, ordering, params, values, grouping, unique, offset, count);
        if(appClients == null) {
            log.log(Level.WARNING, "Failed to find appClients for the given criterion.");
        }

        log.finer("END");
        return appClients;
    }

    @Override
    public List<String> findAppClientKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("AppClientServiceImpl.findAppClientKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getAppClientServiceProxy().findAppClientKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find AppClient keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty AppClient key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("AppClientServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getAppClientServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createAppClient(String owner, String admin, String name, String clientId, String clientCode, String rootDomain, String appDomain, Boolean useAppDomain, Boolean enabled, String licenseMode, String status) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        AppClientBean bean = new AppClientBean(null, owner, admin, name, clientId, clientCode, rootDomain, appDomain, useAppDomain, enabled, licenseMode, status);
        return createAppClient(bean);
    }

    @Override
    public String createAppClient(AppClient appClient) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //AppClient bean = constructAppClient(appClient);
        //return bean.getGuid();

        // Param appClient cannot be null.....
        if(appClient == null) {
            log.log(Level.INFO, "Param appClient is null!");
            throw new BadRequestException("Param appClient object is null!");
        }
        AppClientBean bean = null;
        if(appClient instanceof AppClientBean) {
            bean = (AppClientBean) appClient;
        } else if(appClient instanceof AppClient) {
            // bean = new AppClientBean(null, appClient.getOwner(), appClient.getAdmin(), appClient.getName(), appClient.getClientId(), appClient.getClientCode(), appClient.getRootDomain(), appClient.getAppDomain(), appClient.isUseAppDomain(), appClient.isEnabled(), appClient.getLicenseMode(), appClient.getStatus());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new AppClientBean(appClient.getGuid(), appClient.getOwner(), appClient.getAdmin(), appClient.getName(), appClient.getClientId(), appClient.getClientCode(), appClient.getRootDomain(), appClient.getAppDomain(), appClient.isUseAppDomain(), appClient.isEnabled(), appClient.getLicenseMode(), appClient.getStatus());
        } else {
            log.log(Level.WARNING, "createAppClient(): Arg appClient is of an unknown type.");
            //bean = new AppClientBean();
            bean = new AppClientBean(appClient.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getAppClientServiceProxy().createAppClient(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public AppClient constructAppClient(AppClient appClient) throws BaseException
    {
        log.finer("BEGIN");

        // Param appClient cannot be null.....
        if(appClient == null) {
            log.log(Level.INFO, "Param appClient is null!");
            throw new BadRequestException("Param appClient object is null!");
        }
        AppClientBean bean = null;
        if(appClient instanceof AppClientBean) {
            bean = (AppClientBean) appClient;
        } else if(appClient instanceof AppClient) {
            // bean = new AppClientBean(null, appClient.getOwner(), appClient.getAdmin(), appClient.getName(), appClient.getClientId(), appClient.getClientCode(), appClient.getRootDomain(), appClient.getAppDomain(), appClient.isUseAppDomain(), appClient.isEnabled(), appClient.getLicenseMode(), appClient.getStatus());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new AppClientBean(appClient.getGuid(), appClient.getOwner(), appClient.getAdmin(), appClient.getName(), appClient.getClientId(), appClient.getClientCode(), appClient.getRootDomain(), appClient.getAppDomain(), appClient.isUseAppDomain(), appClient.isEnabled(), appClient.getLicenseMode(), appClient.getStatus());
        } else {
            log.log(Level.WARNING, "createAppClient(): Arg appClient is of an unknown type.");
            //bean = new AppClientBean();
            bean = new AppClientBean(appClient.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getAppClientServiceProxy().createAppClient(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateAppClient(String guid, String owner, String admin, String name, String clientId, String clientCode, String rootDomain, String appDomain, Boolean useAppDomain, Boolean enabled, String licenseMode, String status) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        AppClientBean bean = new AppClientBean(guid, owner, admin, name, clientId, clientCode, rootDomain, appDomain, useAppDomain, enabled, licenseMode, status);
        return updateAppClient(bean);
    }
        
    @Override
    public Boolean updateAppClient(AppClient appClient) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //AppClient bean = refreshAppClient(appClient);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param appClient cannot be null.....
        if(appClient == null || appClient.getGuid() == null) {
            log.log(Level.INFO, "Param appClient or its guid is null!");
            throw new BadRequestException("Param appClient object or its guid is null!");
        }
        AppClientBean bean = null;
        if(appClient instanceof AppClientBean) {
            bean = (AppClientBean) appClient;
        } else {  // if(appClient instanceof AppClient)
            bean = new AppClientBean(appClient.getGuid(), appClient.getOwner(), appClient.getAdmin(), appClient.getName(), appClient.getClientId(), appClient.getClientCode(), appClient.getRootDomain(), appClient.getAppDomain(), appClient.isUseAppDomain(), appClient.isEnabled(), appClient.getLicenseMode(), appClient.getStatus());
        }
        Boolean suc = getProxyFactory().getAppClientServiceProxy().updateAppClient(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public AppClient refreshAppClient(AppClient appClient) throws BaseException
    {
        log.finer("BEGIN");

        // Param appClient cannot be null.....
        if(appClient == null || appClient.getGuid() == null) {
            log.log(Level.INFO, "Param appClient or its guid is null!");
            throw new BadRequestException("Param appClient object or its guid is null!");
        }
        AppClientBean bean = null;
        if(appClient instanceof AppClientBean) {
            bean = (AppClientBean) appClient;
        } else {  // if(appClient instanceof AppClient)
            bean = new AppClientBean(appClient.getGuid(), appClient.getOwner(), appClient.getAdmin(), appClient.getName(), appClient.getClientId(), appClient.getClientCode(), appClient.getRootDomain(), appClient.getAppDomain(), appClient.isUseAppDomain(), appClient.isEnabled(), appClient.getLicenseMode(), appClient.getStatus());
        }
        Boolean suc = getProxyFactory().getAppClientServiceProxy().updateAppClient(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteAppClient(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getAppClientServiceProxy().deleteAppClient(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }
            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            AppClient appClient = null;
            try {
                appClient = getProxyFactory().getAppClientServiceProxy().getAppClient(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch appClient with a key, " + guid);
                return false;
            }
            if(appClient != null) {
                String beanGuid = appClient.getGuid();
                Boolean suc1 = getProxyFactory().getAppClientServiceProxy().deleteAppClient(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("appClient with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteAppClient(AppClient appClient) throws BaseException
    {
        log.finer("BEGIN");

        // Param appClient cannot be null.....
        if(appClient == null || appClient.getGuid() == null) {
            log.log(Level.INFO, "Param appClient or its guid is null!");
            throw new BadRequestException("Param appClient object or its guid is null!");
        }
        AppClientBean bean = null;
        if(appClient instanceof AppClientBean) {
            bean = (AppClientBean) appClient;
        } else {  // if(appClient instanceof AppClient)
            // ????
            log.warning("appClient is not an instance of AppClientBean.");
            bean = new AppClientBean(appClient.getGuid(), appClient.getOwner(), appClient.getAdmin(), appClient.getName(), appClient.getClientId(), appClient.getClientCode(), appClient.getRootDomain(), appClient.getAppDomain(), appClient.isUseAppDomain(), appClient.isEnabled(), appClient.getLicenseMode(), appClient.getStatus());
        }
        Boolean suc = getProxyFactory().getAppClientServiceProxy().deleteAppClient(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteAppClients(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getAppClientServiceProxy().deleteAppClients(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createAppClients(List<AppClient> appClients) throws BaseException
    {
        log.finer("BEGIN");

        if(appClients == null) {
            log.log(Level.WARNING, "createAppClients() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = appClients.size();
        if(size == 0) {
            log.log(Level.WARNING, "createAppClients() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(AppClient appClient : appClients) {
            String guid = createAppClient(appClient);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createAppClients() failed for at least one appClient. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateAppClients(List<AppClient> appClients) throws BaseException
    //{
    //}

}
