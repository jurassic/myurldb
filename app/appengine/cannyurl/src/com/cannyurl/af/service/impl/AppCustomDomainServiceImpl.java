package com.cannyurl.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.AppCustomDomain;
import com.cannyurl.af.config.Config;

import com.cannyurl.af.bean.HashedPasswordStructBean;
import com.cannyurl.af.bean.GeoPointStructBean;
import com.cannyurl.af.bean.WebProfileStructBean;
import com.cannyurl.af.bean.StreetAddressStructBean;
import com.cannyurl.af.bean.SiteLogBean;
import com.cannyurl.af.bean.HelpNoticeBean;
import com.cannyurl.af.bean.TwitterCardAppInfoBean;
import com.cannyurl.af.bean.ContactInfoStructBean;
import com.cannyurl.af.bean.GeoCoordinateStructBean;
import com.cannyurl.af.bean.FullNameStructBean;
import com.cannyurl.af.bean.ExternalUserIdStructBean;
import com.cannyurl.af.bean.GaeUserStructBean;
import com.cannyurl.af.bean.AppBrandStructBean;
import com.cannyurl.af.bean.CellLatitudeLongitudeBean;
import com.cannyurl.af.bean.GaeAppStructBean;
import com.cannyurl.af.bean.ShortPassageAttributeBean;
import com.cannyurl.af.bean.KeyValuePairStructBean;
import com.cannyurl.af.bean.KeyValueRelationStructBean;
import com.cannyurl.af.bean.ExternalServiceApiKeyStructBean;
import com.cannyurl.af.bean.ReferrerInfoStructBean;
import com.cannyurl.af.bean.TwitterCardProductDataBean;
import com.cannyurl.af.bean.PagerStateStructBean;

import com.cannyurl.af.bean.AppCustomDomainBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.AppCustomDomainService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class AppCustomDomainServiceImpl implements AppCustomDomainService
{
    private static final Logger log = Logger.getLogger(AppCustomDomainServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "AppCustomDomain-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("AppCustomDomain:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public AppCustomDomainServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // AppCustomDomain related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public AppCustomDomain getAppCustomDomain(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAppCustomDomain(): guid = " + guid);

        AppCustomDomainBean bean = null;
        if(getCache() != null) {
            bean = (AppCustomDomainBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (AppCustomDomainBean) getProxyFactory().getAppCustomDomainServiceProxy().getAppCustomDomain(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "AppCustomDomainBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve AppCustomDomainBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getAppCustomDomain(String guid, String field) throws BaseException
    {
        AppCustomDomainBean bean = null;
        if(getCache() != null) {
            bean = (AppCustomDomainBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (AppCustomDomainBean) getProxyFactory().getAppCustomDomainServiceProxy().getAppCustomDomain(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "AppCustomDomainBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve AppCustomDomainBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("appId")) {
            return bean.getAppId();
        } else if(field.equals("siteDomain")) {
            return bean.getSiteDomain();
        } else if(field.equals("domain")) {
            return bean.getDomain();
        } else if(field.equals("verified")) {
            return bean.isVerified();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("verifiedTime")) {
            return bean.getVerifiedTime();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<AppCustomDomain> getAppCustomDomains(List<String> guids) throws BaseException
    {
        log.fine("getAppCustomDomains()");

        // TBD: Is there a better way????
        List<AppCustomDomain> appCustomDomains = getProxyFactory().getAppCustomDomainServiceProxy().getAppCustomDomains(guids);
        if(appCustomDomains == null) {
            log.log(Level.WARNING, "Failed to retrieve AppCustomDomainBean list.");
        }

        log.finer("END");
        return appCustomDomains;
    }

    @Override
    public List<AppCustomDomain> getAllAppCustomDomains() throws BaseException
    {
        return getAllAppCustomDomains(null, null, null);
    }

    @Override
    public List<AppCustomDomain> getAllAppCustomDomains(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllAppCustomDomains(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<AppCustomDomain> appCustomDomains = getProxyFactory().getAppCustomDomainServiceProxy().getAllAppCustomDomains(ordering, offset, count);
        if(appCustomDomains == null) {
            log.log(Level.WARNING, "Failed to retrieve AppCustomDomainBean list.");
        }

        log.finer("END");
        return appCustomDomains;
    }

    @Override
    public List<String> getAllAppCustomDomainKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllAppCustomDomainKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getAppCustomDomainServiceProxy().getAllAppCustomDomainKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve AppCustomDomainBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty AppCustomDomainBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<AppCustomDomain> findAppCustomDomains(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findAppCustomDomains(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<AppCustomDomain> findAppCustomDomains(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("AppCustomDomainServiceImpl.findAppCustomDomains(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<AppCustomDomain> appCustomDomains = getProxyFactory().getAppCustomDomainServiceProxy().findAppCustomDomains(filter, ordering, params, values, grouping, unique, offset, count);
        if(appCustomDomains == null) {
            log.log(Level.WARNING, "Failed to find appCustomDomains for the given criterion.");
        }

        log.finer("END");
        return appCustomDomains;
    }

    @Override
    public List<String> findAppCustomDomainKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("AppCustomDomainServiceImpl.findAppCustomDomainKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getAppCustomDomainServiceProxy().findAppCustomDomainKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find AppCustomDomain keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty AppCustomDomain key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("AppCustomDomainServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getAppCustomDomainServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createAppCustomDomain(String appId, String siteDomain, String domain, Boolean verified, String status, Long verifiedTime) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        AppCustomDomainBean bean = new AppCustomDomainBean(null, appId, siteDomain, domain, verified, status, verifiedTime);
        return createAppCustomDomain(bean);
    }

    @Override
    public String createAppCustomDomain(AppCustomDomain appCustomDomain) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //AppCustomDomain bean = constructAppCustomDomain(appCustomDomain);
        //return bean.getGuid();

        // Param appCustomDomain cannot be null.....
        if(appCustomDomain == null) {
            log.log(Level.INFO, "Param appCustomDomain is null!");
            throw new BadRequestException("Param appCustomDomain object is null!");
        }
        AppCustomDomainBean bean = null;
        if(appCustomDomain instanceof AppCustomDomainBean) {
            bean = (AppCustomDomainBean) appCustomDomain;
        } else if(appCustomDomain instanceof AppCustomDomain) {
            // bean = new AppCustomDomainBean(null, appCustomDomain.getAppId(), appCustomDomain.getSiteDomain(), appCustomDomain.getDomain(), appCustomDomain.isVerified(), appCustomDomain.getStatus(), appCustomDomain.getVerifiedTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new AppCustomDomainBean(appCustomDomain.getGuid(), appCustomDomain.getAppId(), appCustomDomain.getSiteDomain(), appCustomDomain.getDomain(), appCustomDomain.isVerified(), appCustomDomain.getStatus(), appCustomDomain.getVerifiedTime());
        } else {
            log.log(Level.WARNING, "createAppCustomDomain(): Arg appCustomDomain is of an unknown type.");
            //bean = new AppCustomDomainBean();
            bean = new AppCustomDomainBean(appCustomDomain.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getAppCustomDomainServiceProxy().createAppCustomDomain(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public AppCustomDomain constructAppCustomDomain(AppCustomDomain appCustomDomain) throws BaseException
    {
        log.finer("BEGIN");

        // Param appCustomDomain cannot be null.....
        if(appCustomDomain == null) {
            log.log(Level.INFO, "Param appCustomDomain is null!");
            throw new BadRequestException("Param appCustomDomain object is null!");
        }
        AppCustomDomainBean bean = null;
        if(appCustomDomain instanceof AppCustomDomainBean) {
            bean = (AppCustomDomainBean) appCustomDomain;
        } else if(appCustomDomain instanceof AppCustomDomain) {
            // bean = new AppCustomDomainBean(null, appCustomDomain.getAppId(), appCustomDomain.getSiteDomain(), appCustomDomain.getDomain(), appCustomDomain.isVerified(), appCustomDomain.getStatus(), appCustomDomain.getVerifiedTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new AppCustomDomainBean(appCustomDomain.getGuid(), appCustomDomain.getAppId(), appCustomDomain.getSiteDomain(), appCustomDomain.getDomain(), appCustomDomain.isVerified(), appCustomDomain.getStatus(), appCustomDomain.getVerifiedTime());
        } else {
            log.log(Level.WARNING, "createAppCustomDomain(): Arg appCustomDomain is of an unknown type.");
            //bean = new AppCustomDomainBean();
            bean = new AppCustomDomainBean(appCustomDomain.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getAppCustomDomainServiceProxy().createAppCustomDomain(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateAppCustomDomain(String guid, String appId, String siteDomain, String domain, Boolean verified, String status, Long verifiedTime) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        AppCustomDomainBean bean = new AppCustomDomainBean(guid, appId, siteDomain, domain, verified, status, verifiedTime);
        return updateAppCustomDomain(bean);
    }
        
    @Override
    public Boolean updateAppCustomDomain(AppCustomDomain appCustomDomain) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //AppCustomDomain bean = refreshAppCustomDomain(appCustomDomain);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param appCustomDomain cannot be null.....
        if(appCustomDomain == null || appCustomDomain.getGuid() == null) {
            log.log(Level.INFO, "Param appCustomDomain or its guid is null!");
            throw new BadRequestException("Param appCustomDomain object or its guid is null!");
        }
        AppCustomDomainBean bean = null;
        if(appCustomDomain instanceof AppCustomDomainBean) {
            bean = (AppCustomDomainBean) appCustomDomain;
        } else {  // if(appCustomDomain instanceof AppCustomDomain)
            bean = new AppCustomDomainBean(appCustomDomain.getGuid(), appCustomDomain.getAppId(), appCustomDomain.getSiteDomain(), appCustomDomain.getDomain(), appCustomDomain.isVerified(), appCustomDomain.getStatus(), appCustomDomain.getVerifiedTime());
        }
        Boolean suc = getProxyFactory().getAppCustomDomainServiceProxy().updateAppCustomDomain(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public AppCustomDomain refreshAppCustomDomain(AppCustomDomain appCustomDomain) throws BaseException
    {
        log.finer("BEGIN");

        // Param appCustomDomain cannot be null.....
        if(appCustomDomain == null || appCustomDomain.getGuid() == null) {
            log.log(Level.INFO, "Param appCustomDomain or its guid is null!");
            throw new BadRequestException("Param appCustomDomain object or its guid is null!");
        }
        AppCustomDomainBean bean = null;
        if(appCustomDomain instanceof AppCustomDomainBean) {
            bean = (AppCustomDomainBean) appCustomDomain;
        } else {  // if(appCustomDomain instanceof AppCustomDomain)
            bean = new AppCustomDomainBean(appCustomDomain.getGuid(), appCustomDomain.getAppId(), appCustomDomain.getSiteDomain(), appCustomDomain.getDomain(), appCustomDomain.isVerified(), appCustomDomain.getStatus(), appCustomDomain.getVerifiedTime());
        }
        Boolean suc = getProxyFactory().getAppCustomDomainServiceProxy().updateAppCustomDomain(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteAppCustomDomain(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getAppCustomDomainServiceProxy().deleteAppCustomDomain(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }
            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            AppCustomDomain appCustomDomain = null;
            try {
                appCustomDomain = getProxyFactory().getAppCustomDomainServiceProxy().getAppCustomDomain(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch appCustomDomain with a key, " + guid);
                return false;
            }
            if(appCustomDomain != null) {
                String beanGuid = appCustomDomain.getGuid();
                Boolean suc1 = getProxyFactory().getAppCustomDomainServiceProxy().deleteAppCustomDomain(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("appCustomDomain with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteAppCustomDomain(AppCustomDomain appCustomDomain) throws BaseException
    {
        log.finer("BEGIN");

        // Param appCustomDomain cannot be null.....
        if(appCustomDomain == null || appCustomDomain.getGuid() == null) {
            log.log(Level.INFO, "Param appCustomDomain or its guid is null!");
            throw new BadRequestException("Param appCustomDomain object or its guid is null!");
        }
        AppCustomDomainBean bean = null;
        if(appCustomDomain instanceof AppCustomDomainBean) {
            bean = (AppCustomDomainBean) appCustomDomain;
        } else {  // if(appCustomDomain instanceof AppCustomDomain)
            // ????
            log.warning("appCustomDomain is not an instance of AppCustomDomainBean.");
            bean = new AppCustomDomainBean(appCustomDomain.getGuid(), appCustomDomain.getAppId(), appCustomDomain.getSiteDomain(), appCustomDomain.getDomain(), appCustomDomain.isVerified(), appCustomDomain.getStatus(), appCustomDomain.getVerifiedTime());
        }
        Boolean suc = getProxyFactory().getAppCustomDomainServiceProxy().deleteAppCustomDomain(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteAppCustomDomains(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getAppCustomDomainServiceProxy().deleteAppCustomDomains(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createAppCustomDomains(List<AppCustomDomain> appCustomDomains) throws BaseException
    {
        log.finer("BEGIN");

        if(appCustomDomains == null) {
            log.log(Level.WARNING, "createAppCustomDomains() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = appCustomDomains.size();
        if(size == 0) {
            log.log(Level.WARNING, "createAppCustomDomains() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(AppCustomDomain appCustomDomain : appCustomDomains) {
            String guid = createAppCustomDomain(appCustomDomain);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createAppCustomDomains() failed for at least one appCustomDomain. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateAppCustomDomains(List<AppCustomDomain> appCustomDomains) throws BaseException
    //{
    //}

}
