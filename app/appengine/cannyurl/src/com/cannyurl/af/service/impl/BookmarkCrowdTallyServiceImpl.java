package com.cannyurl.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.BookmarkCrowdTally;
import com.cannyurl.af.config.Config;

import com.cannyurl.af.bean.HashedPasswordStructBean;
import com.cannyurl.af.bean.GeoPointStructBean;
import com.cannyurl.af.bean.WebProfileStructBean;
import com.cannyurl.af.bean.StreetAddressStructBean;
import com.cannyurl.af.bean.SiteLogBean;
import com.cannyurl.af.bean.HelpNoticeBean;
import com.cannyurl.af.bean.TwitterCardAppInfoBean;
import com.cannyurl.af.bean.ContactInfoStructBean;
import com.cannyurl.af.bean.GeoCoordinateStructBean;
import com.cannyurl.af.bean.FullNameStructBean;
import com.cannyurl.af.bean.ExternalUserIdStructBean;
import com.cannyurl.af.bean.GaeUserStructBean;
import com.cannyurl.af.bean.AppBrandStructBean;
import com.cannyurl.af.bean.CellLatitudeLongitudeBean;
import com.cannyurl.af.bean.GaeAppStructBean;
import com.cannyurl.af.bean.ShortPassageAttributeBean;
import com.cannyurl.af.bean.KeyValuePairStructBean;
import com.cannyurl.af.bean.KeyValueRelationStructBean;
import com.cannyurl.af.bean.ExternalServiceApiKeyStructBean;
import com.cannyurl.af.bean.ReferrerInfoStructBean;
import com.cannyurl.af.bean.TwitterCardProductDataBean;
import com.cannyurl.af.bean.PagerStateStructBean;

import com.cannyurl.af.bean.BookmarkCrowdTallyBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.BookmarkCrowdTallyService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class BookmarkCrowdTallyServiceImpl implements BookmarkCrowdTallyService
{
    private static final Logger log = Logger.getLogger(BookmarkCrowdTallyServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "BookmarkCrowdTally-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("BookmarkCrowdTally:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public BookmarkCrowdTallyServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // BookmarkCrowdTally related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public BookmarkCrowdTally getBookmarkCrowdTally(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getBookmarkCrowdTally(): guid = " + guid);

        BookmarkCrowdTallyBean bean = null;
        if(getCache() != null) {
            bean = (BookmarkCrowdTallyBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (BookmarkCrowdTallyBean) getProxyFactory().getBookmarkCrowdTallyServiceProxy().getBookmarkCrowdTally(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "BookmarkCrowdTallyBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve BookmarkCrowdTallyBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getBookmarkCrowdTally(String guid, String field) throws BaseException
    {
        BookmarkCrowdTallyBean bean = null;
        if(getCache() != null) {
            bean = (BookmarkCrowdTallyBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (BookmarkCrowdTallyBean) getProxyFactory().getBookmarkCrowdTallyServiceProxy().getBookmarkCrowdTally(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "BookmarkCrowdTallyBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve BookmarkCrowdTallyBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return bean.getUser();
        } else if(field.equals("shortLink")) {
            return bean.getShortLink();
        } else if(field.equals("domain")) {
            return bean.getDomain();
        } else if(field.equals("token")) {
            return bean.getToken();
        } else if(field.equals("longUrl")) {
            return bean.getLongUrl();
        } else if(field.equals("shortUrl")) {
            return bean.getShortUrl();
        } else if(field.equals("tallyDate")) {
            return bean.getTallyDate();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("note")) {
            return bean.getNote();
        } else if(field.equals("expirationTime")) {
            return bean.getExpirationTime();
        } else if(field.equals("bookmarkFolder")) {
            return bean.getBookmarkFolder();
        } else if(field.equals("contentTag")) {
            return bean.getContentTag();
        } else if(field.equals("referenceElement")) {
            return bean.getReferenceElement();
        } else if(field.equals("elementType")) {
            return bean.getElementType();
        } else if(field.equals("keywordLink")) {
            return bean.getKeywordLink();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<BookmarkCrowdTally> getBookmarkCrowdTallies(List<String> guids) throws BaseException
    {
        log.fine("getBookmarkCrowdTallies()");

        // TBD: Is there a better way????
        List<BookmarkCrowdTally> bookmarkCrowdTallies = getProxyFactory().getBookmarkCrowdTallyServiceProxy().getBookmarkCrowdTallies(guids);
        if(bookmarkCrowdTallies == null) {
            log.log(Level.WARNING, "Failed to retrieve BookmarkCrowdTallyBean list.");
        }

        log.finer("END");
        return bookmarkCrowdTallies;
    }

    @Override
    public List<BookmarkCrowdTally> getAllBookmarkCrowdTallies() throws BaseException
    {
        return getAllBookmarkCrowdTallies(null, null, null);
    }

    @Override
    public List<BookmarkCrowdTally> getAllBookmarkCrowdTallies(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllBookmarkCrowdTallies(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<BookmarkCrowdTally> bookmarkCrowdTallies = getProxyFactory().getBookmarkCrowdTallyServiceProxy().getAllBookmarkCrowdTallies(ordering, offset, count);
        if(bookmarkCrowdTallies == null) {
            log.log(Level.WARNING, "Failed to retrieve BookmarkCrowdTallyBean list.");
        }

        log.finer("END");
        return bookmarkCrowdTallies;
    }

    @Override
    public List<String> getAllBookmarkCrowdTallyKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllBookmarkCrowdTallyKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getBookmarkCrowdTallyServiceProxy().getAllBookmarkCrowdTallyKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve BookmarkCrowdTallyBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty BookmarkCrowdTallyBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<BookmarkCrowdTally> findBookmarkCrowdTallies(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findBookmarkCrowdTallies(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<BookmarkCrowdTally> findBookmarkCrowdTallies(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BookmarkCrowdTallyServiceImpl.findBookmarkCrowdTallies(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<BookmarkCrowdTally> bookmarkCrowdTallies = getProxyFactory().getBookmarkCrowdTallyServiceProxy().findBookmarkCrowdTallies(filter, ordering, params, values, grouping, unique, offset, count);
        if(bookmarkCrowdTallies == null) {
            log.log(Level.WARNING, "Failed to find bookmarkCrowdTallies for the given criterion.");
        }

        log.finer("END");
        return bookmarkCrowdTallies;
    }

    @Override
    public List<String> findBookmarkCrowdTallyKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BookmarkCrowdTallyServiceImpl.findBookmarkCrowdTallyKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getBookmarkCrowdTallyServiceProxy().findBookmarkCrowdTallyKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find BookmarkCrowdTally keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty BookmarkCrowdTally key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BookmarkCrowdTallyServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getBookmarkCrowdTallyServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createBookmarkCrowdTally(String user, String shortLink, String domain, String token, String longUrl, String shortUrl, String tallyDate, String status, String note, Long expirationTime, String bookmarkFolder, String contentTag, String referenceElement, String elementType, String keywordLink) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        BookmarkCrowdTallyBean bean = new BookmarkCrowdTallyBean(null, user, shortLink, domain, token, longUrl, shortUrl, tallyDate, status, note, expirationTime, bookmarkFolder, contentTag, referenceElement, elementType, keywordLink);
        return createBookmarkCrowdTally(bean);
    }

    @Override
    public String createBookmarkCrowdTally(BookmarkCrowdTally bookmarkCrowdTally) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //BookmarkCrowdTally bean = constructBookmarkCrowdTally(bookmarkCrowdTally);
        //return bean.getGuid();

        // Param bookmarkCrowdTally cannot be null.....
        if(bookmarkCrowdTally == null) {
            log.log(Level.INFO, "Param bookmarkCrowdTally is null!");
            throw new BadRequestException("Param bookmarkCrowdTally object is null!");
        }
        BookmarkCrowdTallyBean bean = null;
        if(bookmarkCrowdTally instanceof BookmarkCrowdTallyBean) {
            bean = (BookmarkCrowdTallyBean) bookmarkCrowdTally;
        } else if(bookmarkCrowdTally instanceof BookmarkCrowdTally) {
            // bean = new BookmarkCrowdTallyBean(null, bookmarkCrowdTally.getUser(), bookmarkCrowdTally.getShortLink(), bookmarkCrowdTally.getDomain(), bookmarkCrowdTally.getToken(), bookmarkCrowdTally.getLongUrl(), bookmarkCrowdTally.getShortUrl(), bookmarkCrowdTally.getTallyDate(), bookmarkCrowdTally.getStatus(), bookmarkCrowdTally.getNote(), bookmarkCrowdTally.getExpirationTime(), bookmarkCrowdTally.getBookmarkFolder(), bookmarkCrowdTally.getContentTag(), bookmarkCrowdTally.getReferenceElement(), bookmarkCrowdTally.getElementType(), bookmarkCrowdTally.getKeywordLink());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new BookmarkCrowdTallyBean(bookmarkCrowdTally.getGuid(), bookmarkCrowdTally.getUser(), bookmarkCrowdTally.getShortLink(), bookmarkCrowdTally.getDomain(), bookmarkCrowdTally.getToken(), bookmarkCrowdTally.getLongUrl(), bookmarkCrowdTally.getShortUrl(), bookmarkCrowdTally.getTallyDate(), bookmarkCrowdTally.getStatus(), bookmarkCrowdTally.getNote(), bookmarkCrowdTally.getExpirationTime(), bookmarkCrowdTally.getBookmarkFolder(), bookmarkCrowdTally.getContentTag(), bookmarkCrowdTally.getReferenceElement(), bookmarkCrowdTally.getElementType(), bookmarkCrowdTally.getKeywordLink());
        } else {
            log.log(Level.WARNING, "createBookmarkCrowdTally(): Arg bookmarkCrowdTally is of an unknown type.");
            //bean = new BookmarkCrowdTallyBean();
            bean = new BookmarkCrowdTallyBean(bookmarkCrowdTally.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getBookmarkCrowdTallyServiceProxy().createBookmarkCrowdTally(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public BookmarkCrowdTally constructBookmarkCrowdTally(BookmarkCrowdTally bookmarkCrowdTally) throws BaseException
    {
        log.finer("BEGIN");

        // Param bookmarkCrowdTally cannot be null.....
        if(bookmarkCrowdTally == null) {
            log.log(Level.INFO, "Param bookmarkCrowdTally is null!");
            throw new BadRequestException("Param bookmarkCrowdTally object is null!");
        }
        BookmarkCrowdTallyBean bean = null;
        if(bookmarkCrowdTally instanceof BookmarkCrowdTallyBean) {
            bean = (BookmarkCrowdTallyBean) bookmarkCrowdTally;
        } else if(bookmarkCrowdTally instanceof BookmarkCrowdTally) {
            // bean = new BookmarkCrowdTallyBean(null, bookmarkCrowdTally.getUser(), bookmarkCrowdTally.getShortLink(), bookmarkCrowdTally.getDomain(), bookmarkCrowdTally.getToken(), bookmarkCrowdTally.getLongUrl(), bookmarkCrowdTally.getShortUrl(), bookmarkCrowdTally.getTallyDate(), bookmarkCrowdTally.getStatus(), bookmarkCrowdTally.getNote(), bookmarkCrowdTally.getExpirationTime(), bookmarkCrowdTally.getBookmarkFolder(), bookmarkCrowdTally.getContentTag(), bookmarkCrowdTally.getReferenceElement(), bookmarkCrowdTally.getElementType(), bookmarkCrowdTally.getKeywordLink());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new BookmarkCrowdTallyBean(bookmarkCrowdTally.getGuid(), bookmarkCrowdTally.getUser(), bookmarkCrowdTally.getShortLink(), bookmarkCrowdTally.getDomain(), bookmarkCrowdTally.getToken(), bookmarkCrowdTally.getLongUrl(), bookmarkCrowdTally.getShortUrl(), bookmarkCrowdTally.getTallyDate(), bookmarkCrowdTally.getStatus(), bookmarkCrowdTally.getNote(), bookmarkCrowdTally.getExpirationTime(), bookmarkCrowdTally.getBookmarkFolder(), bookmarkCrowdTally.getContentTag(), bookmarkCrowdTally.getReferenceElement(), bookmarkCrowdTally.getElementType(), bookmarkCrowdTally.getKeywordLink());
        } else {
            log.log(Level.WARNING, "createBookmarkCrowdTally(): Arg bookmarkCrowdTally is of an unknown type.");
            //bean = new BookmarkCrowdTallyBean();
            bean = new BookmarkCrowdTallyBean(bookmarkCrowdTally.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getBookmarkCrowdTallyServiceProxy().createBookmarkCrowdTally(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateBookmarkCrowdTally(String guid, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, String tallyDate, String status, String note, Long expirationTime, String bookmarkFolder, String contentTag, String referenceElement, String elementType, String keywordLink) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        BookmarkCrowdTallyBean bean = new BookmarkCrowdTallyBean(guid, user, shortLink, domain, token, longUrl, shortUrl, tallyDate, status, note, expirationTime, bookmarkFolder, contentTag, referenceElement, elementType, keywordLink);
        return updateBookmarkCrowdTally(bean);
    }
        
    @Override
    public Boolean updateBookmarkCrowdTally(BookmarkCrowdTally bookmarkCrowdTally) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //BookmarkCrowdTally bean = refreshBookmarkCrowdTally(bookmarkCrowdTally);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param bookmarkCrowdTally cannot be null.....
        if(bookmarkCrowdTally == null || bookmarkCrowdTally.getGuid() == null) {
            log.log(Level.INFO, "Param bookmarkCrowdTally or its guid is null!");
            throw new BadRequestException("Param bookmarkCrowdTally object or its guid is null!");
        }
        BookmarkCrowdTallyBean bean = null;
        if(bookmarkCrowdTally instanceof BookmarkCrowdTallyBean) {
            bean = (BookmarkCrowdTallyBean) bookmarkCrowdTally;
        } else {  // if(bookmarkCrowdTally instanceof BookmarkCrowdTally)
            bean = new BookmarkCrowdTallyBean(bookmarkCrowdTally.getGuid(), bookmarkCrowdTally.getUser(), bookmarkCrowdTally.getShortLink(), bookmarkCrowdTally.getDomain(), bookmarkCrowdTally.getToken(), bookmarkCrowdTally.getLongUrl(), bookmarkCrowdTally.getShortUrl(), bookmarkCrowdTally.getTallyDate(), bookmarkCrowdTally.getStatus(), bookmarkCrowdTally.getNote(), bookmarkCrowdTally.getExpirationTime(), bookmarkCrowdTally.getBookmarkFolder(), bookmarkCrowdTally.getContentTag(), bookmarkCrowdTally.getReferenceElement(), bookmarkCrowdTally.getElementType(), bookmarkCrowdTally.getKeywordLink());
        }
        Boolean suc = getProxyFactory().getBookmarkCrowdTallyServiceProxy().updateBookmarkCrowdTally(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public BookmarkCrowdTally refreshBookmarkCrowdTally(BookmarkCrowdTally bookmarkCrowdTally) throws BaseException
    {
        log.finer("BEGIN");

        // Param bookmarkCrowdTally cannot be null.....
        if(bookmarkCrowdTally == null || bookmarkCrowdTally.getGuid() == null) {
            log.log(Level.INFO, "Param bookmarkCrowdTally or its guid is null!");
            throw new BadRequestException("Param bookmarkCrowdTally object or its guid is null!");
        }
        BookmarkCrowdTallyBean bean = null;
        if(bookmarkCrowdTally instanceof BookmarkCrowdTallyBean) {
            bean = (BookmarkCrowdTallyBean) bookmarkCrowdTally;
        } else {  // if(bookmarkCrowdTally instanceof BookmarkCrowdTally)
            bean = new BookmarkCrowdTallyBean(bookmarkCrowdTally.getGuid(), bookmarkCrowdTally.getUser(), bookmarkCrowdTally.getShortLink(), bookmarkCrowdTally.getDomain(), bookmarkCrowdTally.getToken(), bookmarkCrowdTally.getLongUrl(), bookmarkCrowdTally.getShortUrl(), bookmarkCrowdTally.getTallyDate(), bookmarkCrowdTally.getStatus(), bookmarkCrowdTally.getNote(), bookmarkCrowdTally.getExpirationTime(), bookmarkCrowdTally.getBookmarkFolder(), bookmarkCrowdTally.getContentTag(), bookmarkCrowdTally.getReferenceElement(), bookmarkCrowdTally.getElementType(), bookmarkCrowdTally.getKeywordLink());
        }
        Boolean suc = getProxyFactory().getBookmarkCrowdTallyServiceProxy().updateBookmarkCrowdTally(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteBookmarkCrowdTally(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getBookmarkCrowdTallyServiceProxy().deleteBookmarkCrowdTally(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }
            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            BookmarkCrowdTally bookmarkCrowdTally = null;
            try {
                bookmarkCrowdTally = getProxyFactory().getBookmarkCrowdTallyServiceProxy().getBookmarkCrowdTally(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch bookmarkCrowdTally with a key, " + guid);
                return false;
            }
            if(bookmarkCrowdTally != null) {
                String beanGuid = bookmarkCrowdTally.getGuid();
                Boolean suc1 = getProxyFactory().getBookmarkCrowdTallyServiceProxy().deleteBookmarkCrowdTally(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("bookmarkCrowdTally with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteBookmarkCrowdTally(BookmarkCrowdTally bookmarkCrowdTally) throws BaseException
    {
        log.finer("BEGIN");

        // Param bookmarkCrowdTally cannot be null.....
        if(bookmarkCrowdTally == null || bookmarkCrowdTally.getGuid() == null) {
            log.log(Level.INFO, "Param bookmarkCrowdTally or its guid is null!");
            throw new BadRequestException("Param bookmarkCrowdTally object or its guid is null!");
        }
        BookmarkCrowdTallyBean bean = null;
        if(bookmarkCrowdTally instanceof BookmarkCrowdTallyBean) {
            bean = (BookmarkCrowdTallyBean) bookmarkCrowdTally;
        } else {  // if(bookmarkCrowdTally instanceof BookmarkCrowdTally)
            // ????
            log.warning("bookmarkCrowdTally is not an instance of BookmarkCrowdTallyBean.");
            bean = new BookmarkCrowdTallyBean(bookmarkCrowdTally.getGuid(), bookmarkCrowdTally.getUser(), bookmarkCrowdTally.getShortLink(), bookmarkCrowdTally.getDomain(), bookmarkCrowdTally.getToken(), bookmarkCrowdTally.getLongUrl(), bookmarkCrowdTally.getShortUrl(), bookmarkCrowdTally.getTallyDate(), bookmarkCrowdTally.getStatus(), bookmarkCrowdTally.getNote(), bookmarkCrowdTally.getExpirationTime(), bookmarkCrowdTally.getBookmarkFolder(), bookmarkCrowdTally.getContentTag(), bookmarkCrowdTally.getReferenceElement(), bookmarkCrowdTally.getElementType(), bookmarkCrowdTally.getKeywordLink());
        }
        Boolean suc = getProxyFactory().getBookmarkCrowdTallyServiceProxy().deleteBookmarkCrowdTally(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteBookmarkCrowdTallies(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getBookmarkCrowdTallyServiceProxy().deleteBookmarkCrowdTallies(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createBookmarkCrowdTallies(List<BookmarkCrowdTally> bookmarkCrowdTallies) throws BaseException
    {
        log.finer("BEGIN");

        if(bookmarkCrowdTallies == null) {
            log.log(Level.WARNING, "createBookmarkCrowdTallies() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = bookmarkCrowdTallies.size();
        if(size == 0) {
            log.log(Level.WARNING, "createBookmarkCrowdTallies() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(BookmarkCrowdTally bookmarkCrowdTally : bookmarkCrowdTallies) {
            String guid = createBookmarkCrowdTally(bookmarkCrowdTally);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createBookmarkCrowdTallies() failed for at least one bookmarkCrowdTally. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateBookmarkCrowdTallies(List<BookmarkCrowdTally> bookmarkCrowdTallies) throws BaseException
    //{
    //}

}
