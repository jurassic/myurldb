package com.cannyurl.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.BookmarkFolderImport;
import com.cannyurl.af.config.Config;

import com.cannyurl.af.bean.HashedPasswordStructBean;
import com.cannyurl.af.bean.GeoPointStructBean;
import com.cannyurl.af.bean.WebProfileStructBean;
import com.cannyurl.af.bean.StreetAddressStructBean;
import com.cannyurl.af.bean.SiteLogBean;
import com.cannyurl.af.bean.HelpNoticeBean;
import com.cannyurl.af.bean.TwitterCardAppInfoBean;
import com.cannyurl.af.bean.ContactInfoStructBean;
import com.cannyurl.af.bean.GeoCoordinateStructBean;
import com.cannyurl.af.bean.FullNameStructBean;
import com.cannyurl.af.bean.ExternalUserIdStructBean;
import com.cannyurl.af.bean.GaeUserStructBean;
import com.cannyurl.af.bean.AppBrandStructBean;
import com.cannyurl.af.bean.CellLatitudeLongitudeBean;
import com.cannyurl.af.bean.GaeAppStructBean;
import com.cannyurl.af.bean.ShortPassageAttributeBean;
import com.cannyurl.af.bean.KeyValuePairStructBean;
import com.cannyurl.af.bean.KeyValueRelationStructBean;
import com.cannyurl.af.bean.ExternalServiceApiKeyStructBean;
import com.cannyurl.af.bean.ReferrerInfoStructBean;
import com.cannyurl.af.bean.TwitterCardProductDataBean;
import com.cannyurl.af.bean.PagerStateStructBean;

import com.cannyurl.af.bean.BookmarkFolderImportBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.BookmarkFolderImportService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class BookmarkFolderImportServiceImpl implements BookmarkFolderImportService
{
    private static final Logger log = Logger.getLogger(BookmarkFolderImportServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "BookmarkFolderImport-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("BookmarkFolderImport:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public BookmarkFolderImportServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // BookmarkFolderImport related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public BookmarkFolderImport getBookmarkFolderImport(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getBookmarkFolderImport(): guid = " + guid);

        BookmarkFolderImportBean bean = null;
        if(getCache() != null) {
            bean = (BookmarkFolderImportBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (BookmarkFolderImportBean) getProxyFactory().getBookmarkFolderImportServiceProxy().getBookmarkFolderImport(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "BookmarkFolderImportBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve BookmarkFolderImportBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getBookmarkFolderImport(String guid, String field) throws BaseException
    {
        BookmarkFolderImportBean bean = null;
        if(getCache() != null) {
            bean = (BookmarkFolderImportBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (BookmarkFolderImportBean) getProxyFactory().getBookmarkFolderImportServiceProxy().getBookmarkFolderImport(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "BookmarkFolderImportBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve BookmarkFolderImportBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return bean.getUser();
        } else if(field.equals("precedence")) {
            return bean.getPrecedence();
        } else if(field.equals("importType")) {
            return bean.getImportType();
        } else if(field.equals("importedFolder")) {
            return bean.getImportedFolder();
        } else if(field.equals("importedFolderUser")) {
            return bean.getImportedFolderUser();
        } else if(field.equals("importedFolderTitle")) {
            return bean.getImportedFolderTitle();
        } else if(field.equals("importedFolderPath")) {
            return bean.getImportedFolderPath();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("note")) {
            return bean.getNote();
        } else if(field.equals("bookmarkFolder")) {
            return bean.getBookmarkFolder();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<BookmarkFolderImport> getBookmarkFolderImports(List<String> guids) throws BaseException
    {
        log.fine("getBookmarkFolderImports()");

        // TBD: Is there a better way????
        List<BookmarkFolderImport> bookmarkFolderImports = getProxyFactory().getBookmarkFolderImportServiceProxy().getBookmarkFolderImports(guids);
        if(bookmarkFolderImports == null) {
            log.log(Level.WARNING, "Failed to retrieve BookmarkFolderImportBean list.");
        }

        log.finer("END");
        return bookmarkFolderImports;
    }

    @Override
    public List<BookmarkFolderImport> getAllBookmarkFolderImports() throws BaseException
    {
        return getAllBookmarkFolderImports(null, null, null);
    }

    @Override
    public List<BookmarkFolderImport> getAllBookmarkFolderImports(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllBookmarkFolderImports(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<BookmarkFolderImport> bookmarkFolderImports = getProxyFactory().getBookmarkFolderImportServiceProxy().getAllBookmarkFolderImports(ordering, offset, count);
        if(bookmarkFolderImports == null) {
            log.log(Level.WARNING, "Failed to retrieve BookmarkFolderImportBean list.");
        }

        log.finer("END");
        return bookmarkFolderImports;
    }

    @Override
    public List<String> getAllBookmarkFolderImportKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllBookmarkFolderImportKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getBookmarkFolderImportServiceProxy().getAllBookmarkFolderImportKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve BookmarkFolderImportBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty BookmarkFolderImportBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<BookmarkFolderImport> findBookmarkFolderImports(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findBookmarkFolderImports(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<BookmarkFolderImport> findBookmarkFolderImports(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BookmarkFolderImportServiceImpl.findBookmarkFolderImports(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<BookmarkFolderImport> bookmarkFolderImports = getProxyFactory().getBookmarkFolderImportServiceProxy().findBookmarkFolderImports(filter, ordering, params, values, grouping, unique, offset, count);
        if(bookmarkFolderImports == null) {
            log.log(Level.WARNING, "Failed to find bookmarkFolderImports for the given criterion.");
        }

        log.finer("END");
        return bookmarkFolderImports;
    }

    @Override
    public List<String> findBookmarkFolderImportKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BookmarkFolderImportServiceImpl.findBookmarkFolderImportKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getBookmarkFolderImportServiceProxy().findBookmarkFolderImportKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find BookmarkFolderImport keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty BookmarkFolderImport key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BookmarkFolderImportServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getBookmarkFolderImportServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createBookmarkFolderImport(String user, Integer precedence, String importType, String importedFolder, String importedFolderUser, String importedFolderTitle, String importedFolderPath, String status, String note, String bookmarkFolder) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        BookmarkFolderImportBean bean = new BookmarkFolderImportBean(null, user, precedence, importType, importedFolder, importedFolderUser, importedFolderTitle, importedFolderPath, status, note, bookmarkFolder);
        return createBookmarkFolderImport(bean);
    }

    @Override
    public String createBookmarkFolderImport(BookmarkFolderImport bookmarkFolderImport) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //BookmarkFolderImport bean = constructBookmarkFolderImport(bookmarkFolderImport);
        //return bean.getGuid();

        // Param bookmarkFolderImport cannot be null.....
        if(bookmarkFolderImport == null) {
            log.log(Level.INFO, "Param bookmarkFolderImport is null!");
            throw new BadRequestException("Param bookmarkFolderImport object is null!");
        }
        BookmarkFolderImportBean bean = null;
        if(bookmarkFolderImport instanceof BookmarkFolderImportBean) {
            bean = (BookmarkFolderImportBean) bookmarkFolderImport;
        } else if(bookmarkFolderImport instanceof BookmarkFolderImport) {
            // bean = new BookmarkFolderImportBean(null, bookmarkFolderImport.getUser(), bookmarkFolderImport.getPrecedence(), bookmarkFolderImport.getImportType(), bookmarkFolderImport.getImportedFolder(), bookmarkFolderImport.getImportedFolderUser(), bookmarkFolderImport.getImportedFolderTitle(), bookmarkFolderImport.getImportedFolderPath(), bookmarkFolderImport.getStatus(), bookmarkFolderImport.getNote(), bookmarkFolderImport.getBookmarkFolder());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new BookmarkFolderImportBean(bookmarkFolderImport.getGuid(), bookmarkFolderImport.getUser(), bookmarkFolderImport.getPrecedence(), bookmarkFolderImport.getImportType(), bookmarkFolderImport.getImportedFolder(), bookmarkFolderImport.getImportedFolderUser(), bookmarkFolderImport.getImportedFolderTitle(), bookmarkFolderImport.getImportedFolderPath(), bookmarkFolderImport.getStatus(), bookmarkFolderImport.getNote(), bookmarkFolderImport.getBookmarkFolder());
        } else {
            log.log(Level.WARNING, "createBookmarkFolderImport(): Arg bookmarkFolderImport is of an unknown type.");
            //bean = new BookmarkFolderImportBean();
            bean = new BookmarkFolderImportBean(bookmarkFolderImport.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getBookmarkFolderImportServiceProxy().createBookmarkFolderImport(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public BookmarkFolderImport constructBookmarkFolderImport(BookmarkFolderImport bookmarkFolderImport) throws BaseException
    {
        log.finer("BEGIN");

        // Param bookmarkFolderImport cannot be null.....
        if(bookmarkFolderImport == null) {
            log.log(Level.INFO, "Param bookmarkFolderImport is null!");
            throw new BadRequestException("Param bookmarkFolderImport object is null!");
        }
        BookmarkFolderImportBean bean = null;
        if(bookmarkFolderImport instanceof BookmarkFolderImportBean) {
            bean = (BookmarkFolderImportBean) bookmarkFolderImport;
        } else if(bookmarkFolderImport instanceof BookmarkFolderImport) {
            // bean = new BookmarkFolderImportBean(null, bookmarkFolderImport.getUser(), bookmarkFolderImport.getPrecedence(), bookmarkFolderImport.getImportType(), bookmarkFolderImport.getImportedFolder(), bookmarkFolderImport.getImportedFolderUser(), bookmarkFolderImport.getImportedFolderTitle(), bookmarkFolderImport.getImportedFolderPath(), bookmarkFolderImport.getStatus(), bookmarkFolderImport.getNote(), bookmarkFolderImport.getBookmarkFolder());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new BookmarkFolderImportBean(bookmarkFolderImport.getGuid(), bookmarkFolderImport.getUser(), bookmarkFolderImport.getPrecedence(), bookmarkFolderImport.getImportType(), bookmarkFolderImport.getImportedFolder(), bookmarkFolderImport.getImportedFolderUser(), bookmarkFolderImport.getImportedFolderTitle(), bookmarkFolderImport.getImportedFolderPath(), bookmarkFolderImport.getStatus(), bookmarkFolderImport.getNote(), bookmarkFolderImport.getBookmarkFolder());
        } else {
            log.log(Level.WARNING, "createBookmarkFolderImport(): Arg bookmarkFolderImport is of an unknown type.");
            //bean = new BookmarkFolderImportBean();
            bean = new BookmarkFolderImportBean(bookmarkFolderImport.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getBookmarkFolderImportServiceProxy().createBookmarkFolderImport(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateBookmarkFolderImport(String guid, String user, Integer precedence, String importType, String importedFolder, String importedFolderUser, String importedFolderTitle, String importedFolderPath, String status, String note, String bookmarkFolder) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        BookmarkFolderImportBean bean = new BookmarkFolderImportBean(guid, user, precedence, importType, importedFolder, importedFolderUser, importedFolderTitle, importedFolderPath, status, note, bookmarkFolder);
        return updateBookmarkFolderImport(bean);
    }
        
    @Override
    public Boolean updateBookmarkFolderImport(BookmarkFolderImport bookmarkFolderImport) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //BookmarkFolderImport bean = refreshBookmarkFolderImport(bookmarkFolderImport);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param bookmarkFolderImport cannot be null.....
        if(bookmarkFolderImport == null || bookmarkFolderImport.getGuid() == null) {
            log.log(Level.INFO, "Param bookmarkFolderImport or its guid is null!");
            throw new BadRequestException("Param bookmarkFolderImport object or its guid is null!");
        }
        BookmarkFolderImportBean bean = null;
        if(bookmarkFolderImport instanceof BookmarkFolderImportBean) {
            bean = (BookmarkFolderImportBean) bookmarkFolderImport;
        } else {  // if(bookmarkFolderImport instanceof BookmarkFolderImport)
            bean = new BookmarkFolderImportBean(bookmarkFolderImport.getGuid(), bookmarkFolderImport.getUser(), bookmarkFolderImport.getPrecedence(), bookmarkFolderImport.getImportType(), bookmarkFolderImport.getImportedFolder(), bookmarkFolderImport.getImportedFolderUser(), bookmarkFolderImport.getImportedFolderTitle(), bookmarkFolderImport.getImportedFolderPath(), bookmarkFolderImport.getStatus(), bookmarkFolderImport.getNote(), bookmarkFolderImport.getBookmarkFolder());
        }
        Boolean suc = getProxyFactory().getBookmarkFolderImportServiceProxy().updateBookmarkFolderImport(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public BookmarkFolderImport refreshBookmarkFolderImport(BookmarkFolderImport bookmarkFolderImport) throws BaseException
    {
        log.finer("BEGIN");

        // Param bookmarkFolderImport cannot be null.....
        if(bookmarkFolderImport == null || bookmarkFolderImport.getGuid() == null) {
            log.log(Level.INFO, "Param bookmarkFolderImport or its guid is null!");
            throw new BadRequestException("Param bookmarkFolderImport object or its guid is null!");
        }
        BookmarkFolderImportBean bean = null;
        if(bookmarkFolderImport instanceof BookmarkFolderImportBean) {
            bean = (BookmarkFolderImportBean) bookmarkFolderImport;
        } else {  // if(bookmarkFolderImport instanceof BookmarkFolderImport)
            bean = new BookmarkFolderImportBean(bookmarkFolderImport.getGuid(), bookmarkFolderImport.getUser(), bookmarkFolderImport.getPrecedence(), bookmarkFolderImport.getImportType(), bookmarkFolderImport.getImportedFolder(), bookmarkFolderImport.getImportedFolderUser(), bookmarkFolderImport.getImportedFolderTitle(), bookmarkFolderImport.getImportedFolderPath(), bookmarkFolderImport.getStatus(), bookmarkFolderImport.getNote(), bookmarkFolderImport.getBookmarkFolder());
        }
        Boolean suc = getProxyFactory().getBookmarkFolderImportServiceProxy().updateBookmarkFolderImport(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteBookmarkFolderImport(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getBookmarkFolderImportServiceProxy().deleteBookmarkFolderImport(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }
            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            BookmarkFolderImport bookmarkFolderImport = null;
            try {
                bookmarkFolderImport = getProxyFactory().getBookmarkFolderImportServiceProxy().getBookmarkFolderImport(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch bookmarkFolderImport with a key, " + guid);
                return false;
            }
            if(bookmarkFolderImport != null) {
                String beanGuid = bookmarkFolderImport.getGuid();
                Boolean suc1 = getProxyFactory().getBookmarkFolderImportServiceProxy().deleteBookmarkFolderImport(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("bookmarkFolderImport with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteBookmarkFolderImport(BookmarkFolderImport bookmarkFolderImport) throws BaseException
    {
        log.finer("BEGIN");

        // Param bookmarkFolderImport cannot be null.....
        if(bookmarkFolderImport == null || bookmarkFolderImport.getGuid() == null) {
            log.log(Level.INFO, "Param bookmarkFolderImport or its guid is null!");
            throw new BadRequestException("Param bookmarkFolderImport object or its guid is null!");
        }
        BookmarkFolderImportBean bean = null;
        if(bookmarkFolderImport instanceof BookmarkFolderImportBean) {
            bean = (BookmarkFolderImportBean) bookmarkFolderImport;
        } else {  // if(bookmarkFolderImport instanceof BookmarkFolderImport)
            // ????
            log.warning("bookmarkFolderImport is not an instance of BookmarkFolderImportBean.");
            bean = new BookmarkFolderImportBean(bookmarkFolderImport.getGuid(), bookmarkFolderImport.getUser(), bookmarkFolderImport.getPrecedence(), bookmarkFolderImport.getImportType(), bookmarkFolderImport.getImportedFolder(), bookmarkFolderImport.getImportedFolderUser(), bookmarkFolderImport.getImportedFolderTitle(), bookmarkFolderImport.getImportedFolderPath(), bookmarkFolderImport.getStatus(), bookmarkFolderImport.getNote(), bookmarkFolderImport.getBookmarkFolder());
        }
        Boolean suc = getProxyFactory().getBookmarkFolderImportServiceProxy().deleteBookmarkFolderImport(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteBookmarkFolderImports(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getBookmarkFolderImportServiceProxy().deleteBookmarkFolderImports(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createBookmarkFolderImports(List<BookmarkFolderImport> bookmarkFolderImports) throws BaseException
    {
        log.finer("BEGIN");

        if(bookmarkFolderImports == null) {
            log.log(Level.WARNING, "createBookmarkFolderImports() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = bookmarkFolderImports.size();
        if(size == 0) {
            log.log(Level.WARNING, "createBookmarkFolderImports() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(BookmarkFolderImport bookmarkFolderImport : bookmarkFolderImports) {
            String guid = createBookmarkFolderImport(bookmarkFolderImport);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createBookmarkFolderImports() failed for at least one bookmarkFolderImport. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateBookmarkFolderImports(List<BookmarkFolderImport> bookmarkFolderImports) throws BaseException
    //{
    //}

}
