package com.cannyurl.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.BookmarkFolder;
import com.myurldb.ws.search.gae.BookmarkFolderIndexBuilder;
import com.cannyurl.af.config.Config;

import com.cannyurl.af.bean.HashedPasswordStructBean;
import com.cannyurl.af.bean.GeoPointStructBean;
import com.cannyurl.af.bean.WebProfileStructBean;
import com.cannyurl.af.bean.StreetAddressStructBean;
import com.cannyurl.af.bean.SiteLogBean;
import com.cannyurl.af.bean.HelpNoticeBean;
import com.cannyurl.af.bean.TwitterCardAppInfoBean;
import com.cannyurl.af.bean.ContactInfoStructBean;
import com.cannyurl.af.bean.GeoCoordinateStructBean;
import com.cannyurl.af.bean.FullNameStructBean;
import com.cannyurl.af.bean.ExternalUserIdStructBean;
import com.cannyurl.af.bean.GaeUserStructBean;
import com.cannyurl.af.bean.AppBrandStructBean;
import com.cannyurl.af.bean.CellLatitudeLongitudeBean;
import com.cannyurl.af.bean.GaeAppStructBean;
import com.cannyurl.af.bean.ShortPassageAttributeBean;
import com.cannyurl.af.bean.KeyValuePairStructBean;
import com.cannyurl.af.bean.KeyValueRelationStructBean;
import com.cannyurl.af.bean.ExternalServiceApiKeyStructBean;
import com.cannyurl.af.bean.ReferrerInfoStructBean;
import com.cannyurl.af.bean.TwitterCardProductDataBean;
import com.cannyurl.af.bean.PagerStateStructBean;

import com.cannyurl.af.bean.BookmarkFolderBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.BookmarkFolderService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class BookmarkFolderServiceImpl implements BookmarkFolderService
{
    private static final Logger log = Logger.getLogger(BookmarkFolderServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "BookmarkFolder-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("BookmarkFolder:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public BookmarkFolderServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // BookmarkFolder related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public BookmarkFolder getBookmarkFolder(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getBookmarkFolder(): guid = " + guid);

        BookmarkFolderBean bean = null;
        if(getCache() != null) {
            bean = (BookmarkFolderBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (BookmarkFolderBean) getProxyFactory().getBookmarkFolderServiceProxy().getBookmarkFolder(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "BookmarkFolderBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve BookmarkFolderBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getBookmarkFolder(String guid, String field) throws BaseException
    {
        BookmarkFolderBean bean = null;
        if(getCache() != null) {
            bean = (BookmarkFolderBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (BookmarkFolderBean) getProxyFactory().getBookmarkFolderServiceProxy().getBookmarkFolder(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "BookmarkFolderBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve BookmarkFolderBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("appClient")) {
            return bean.getAppClient();
        } else if(field.equals("clientRootDomain")) {
            return bean.getClientRootDomain();
        } else if(field.equals("user")) {
            return bean.getUser();
        } else if(field.equals("title")) {
            return bean.getTitle();
        } else if(field.equals("description")) {
            return bean.getDescription();
        } else if(field.equals("type")) {
            return bean.getType();
        } else if(field.equals("category")) {
            return bean.getCategory();
        } else if(field.equals("parent")) {
            return bean.getParent();
        } else if(field.equals("aggregate")) {
            return bean.getAggregate();
        } else if(field.equals("acl")) {
            return bean.getAcl();
        } else if(field.equals("exportable")) {
            return bean.isExportable();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("note")) {
            return bean.getNote();
        } else if(field.equals("keywordFolder")) {
            return bean.getKeywordFolder();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<BookmarkFolder> getBookmarkFolders(List<String> guids) throws BaseException
    {
        log.fine("getBookmarkFolders()");

        // TBD: Is there a better way????
        List<BookmarkFolder> bookmarkFolders = getProxyFactory().getBookmarkFolderServiceProxy().getBookmarkFolders(guids);
        if(bookmarkFolders == null) {
            log.log(Level.WARNING, "Failed to retrieve BookmarkFolderBean list.");
        }

        log.finer("END");
        return bookmarkFolders;
    }

    @Override
    public List<BookmarkFolder> getAllBookmarkFolders() throws BaseException
    {
        return getAllBookmarkFolders(null, null, null);
    }

    @Override
    public List<BookmarkFolder> getAllBookmarkFolders(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllBookmarkFolders(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<BookmarkFolder> bookmarkFolders = getProxyFactory().getBookmarkFolderServiceProxy().getAllBookmarkFolders(ordering, offset, count);
        if(bookmarkFolders == null) {
            log.log(Level.WARNING, "Failed to retrieve BookmarkFolderBean list.");
        }

        log.finer("END");
        return bookmarkFolders;
    }

    @Override
    public List<String> getAllBookmarkFolderKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllBookmarkFolderKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getBookmarkFolderServiceProxy().getAllBookmarkFolderKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve BookmarkFolderBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty BookmarkFolderBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<BookmarkFolder> findBookmarkFolders(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findBookmarkFolders(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<BookmarkFolder> findBookmarkFolders(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BookmarkFolderServiceImpl.findBookmarkFolders(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<BookmarkFolder> bookmarkFolders = getProxyFactory().getBookmarkFolderServiceProxy().findBookmarkFolders(filter, ordering, params, values, grouping, unique, offset, count);
        if(bookmarkFolders == null) {
            log.log(Level.WARNING, "Failed to find bookmarkFolders for the given criterion.");
        }

        log.finer("END");
        return bookmarkFolders;
    }

    @Override
    public List<String> findBookmarkFolderKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BookmarkFolderServiceImpl.findBookmarkFolderKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getBookmarkFolderServiceProxy().findBookmarkFolderKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find BookmarkFolder keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty BookmarkFolder key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BookmarkFolderServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getBookmarkFolderServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createBookmarkFolder(String appClient, String clientRootDomain, String user, String title, String description, String type, String category, String parent, String aggregate, String acl, Boolean exportable, String status, String note, String keywordFolder) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        BookmarkFolderBean bean = new BookmarkFolderBean(null, appClient, clientRootDomain, user, title, description, type, category, parent, aggregate, acl, exportable, status, note, keywordFolder);
        return createBookmarkFolder(bean);
    }

    @Override
    public String createBookmarkFolder(BookmarkFolder bookmarkFolder) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //BookmarkFolder bean = constructBookmarkFolder(bookmarkFolder);
        //return bean.getGuid();

        // Param bookmarkFolder cannot be null.....
        if(bookmarkFolder == null) {
            log.log(Level.INFO, "Param bookmarkFolder is null!");
            throw new BadRequestException("Param bookmarkFolder object is null!");
        }
        BookmarkFolderBean bean = null;
        if(bookmarkFolder instanceof BookmarkFolderBean) {
            bean = (BookmarkFolderBean) bookmarkFolder;
        } else if(bookmarkFolder instanceof BookmarkFolder) {
            // bean = new BookmarkFolderBean(null, bookmarkFolder.getAppClient(), bookmarkFolder.getClientRootDomain(), bookmarkFolder.getUser(), bookmarkFolder.getTitle(), bookmarkFolder.getDescription(), bookmarkFolder.getType(), bookmarkFolder.getCategory(), bookmarkFolder.getParent(), bookmarkFolder.getAggregate(), bookmarkFolder.getAcl(), bookmarkFolder.isExportable(), bookmarkFolder.getStatus(), bookmarkFolder.getNote(), bookmarkFolder.getKeywordFolder());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new BookmarkFolderBean(bookmarkFolder.getGuid(), bookmarkFolder.getAppClient(), bookmarkFolder.getClientRootDomain(), bookmarkFolder.getUser(), bookmarkFolder.getTitle(), bookmarkFolder.getDescription(), bookmarkFolder.getType(), bookmarkFolder.getCategory(), bookmarkFolder.getParent(), bookmarkFolder.getAggregate(), bookmarkFolder.getAcl(), bookmarkFolder.isExportable(), bookmarkFolder.getStatus(), bookmarkFolder.getNote(), bookmarkFolder.getKeywordFolder());
        } else {
            log.log(Level.WARNING, "createBookmarkFolder(): Arg bookmarkFolder is of an unknown type.");
            //bean = new BookmarkFolderBean();
            bean = new BookmarkFolderBean(bookmarkFolder.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getBookmarkFolderServiceProxy().createBookmarkFolder(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

	    // TBD:
	    if(Config.getInstance().isSearchEnabled()) {
            if(guid != null && !guid.isEmpty()) {
                log.fine("Calling addDocument()... for BookmarkFolder.");
                BookmarkFolderIndexBuilder builder = new BookmarkFolderIndexBuilder();
	    	    builder.addDocument(bean);
	    	}
	    }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public BookmarkFolder constructBookmarkFolder(BookmarkFolder bookmarkFolder) throws BaseException
    {
        log.finer("BEGIN");

        // Param bookmarkFolder cannot be null.....
        if(bookmarkFolder == null) {
            log.log(Level.INFO, "Param bookmarkFolder is null!");
            throw new BadRequestException("Param bookmarkFolder object is null!");
        }
        BookmarkFolderBean bean = null;
        if(bookmarkFolder instanceof BookmarkFolderBean) {
            bean = (BookmarkFolderBean) bookmarkFolder;
        } else if(bookmarkFolder instanceof BookmarkFolder) {
            // bean = new BookmarkFolderBean(null, bookmarkFolder.getAppClient(), bookmarkFolder.getClientRootDomain(), bookmarkFolder.getUser(), bookmarkFolder.getTitle(), bookmarkFolder.getDescription(), bookmarkFolder.getType(), bookmarkFolder.getCategory(), bookmarkFolder.getParent(), bookmarkFolder.getAggregate(), bookmarkFolder.getAcl(), bookmarkFolder.isExportable(), bookmarkFolder.getStatus(), bookmarkFolder.getNote(), bookmarkFolder.getKeywordFolder());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new BookmarkFolderBean(bookmarkFolder.getGuid(), bookmarkFolder.getAppClient(), bookmarkFolder.getClientRootDomain(), bookmarkFolder.getUser(), bookmarkFolder.getTitle(), bookmarkFolder.getDescription(), bookmarkFolder.getType(), bookmarkFolder.getCategory(), bookmarkFolder.getParent(), bookmarkFolder.getAggregate(), bookmarkFolder.getAcl(), bookmarkFolder.isExportable(), bookmarkFolder.getStatus(), bookmarkFolder.getNote(), bookmarkFolder.getKeywordFolder());
        } else {
            log.log(Level.WARNING, "createBookmarkFolder(): Arg bookmarkFolder is of an unknown type.");
            //bean = new BookmarkFolderBean();
            bean = new BookmarkFolderBean(bookmarkFolder.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getBookmarkFolderServiceProxy().createBookmarkFolder(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

	    // TBD:
	    if(Config.getInstance().isSearchEnabled()) {
            if(guid != null && !guid.isEmpty()) {
                log.fine("Calling addDocument()... for BookmarkFolder.");
                BookmarkFolderIndexBuilder builder = new BookmarkFolderIndexBuilder();
	    	    builder.addDocument(bean);
	    	}
	    }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateBookmarkFolder(String guid, String appClient, String clientRootDomain, String user, String title, String description, String type, String category, String parent, String aggregate, String acl, Boolean exportable, String status, String note, String keywordFolder) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        BookmarkFolderBean bean = new BookmarkFolderBean(guid, appClient, clientRootDomain, user, title, description, type, category, parent, aggregate, acl, exportable, status, note, keywordFolder);
        return updateBookmarkFolder(bean);
    }
        
    @Override
    public Boolean updateBookmarkFolder(BookmarkFolder bookmarkFolder) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //BookmarkFolder bean = refreshBookmarkFolder(bookmarkFolder);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param bookmarkFolder cannot be null.....
        if(bookmarkFolder == null || bookmarkFolder.getGuid() == null) {
            log.log(Level.INFO, "Param bookmarkFolder or its guid is null!");
            throw new BadRequestException("Param bookmarkFolder object or its guid is null!");
        }
        BookmarkFolderBean bean = null;
        if(bookmarkFolder instanceof BookmarkFolderBean) {
            bean = (BookmarkFolderBean) bookmarkFolder;
        } else {  // if(bookmarkFolder instanceof BookmarkFolder)
            bean = new BookmarkFolderBean(bookmarkFolder.getGuid(), bookmarkFolder.getAppClient(), bookmarkFolder.getClientRootDomain(), bookmarkFolder.getUser(), bookmarkFolder.getTitle(), bookmarkFolder.getDescription(), bookmarkFolder.getType(), bookmarkFolder.getCategory(), bookmarkFolder.getParent(), bookmarkFolder.getAggregate(), bookmarkFolder.getAcl(), bookmarkFolder.isExportable(), bookmarkFolder.getStatus(), bookmarkFolder.getNote(), bookmarkFolder.getKeywordFolder());
        }
        Boolean suc = getProxyFactory().getBookmarkFolderServiceProxy().updateBookmarkFolder(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

	    // TBD:
        if(Config.getInstance().isSearchEnabled()) {
            if(suc != null && suc.equals(Boolean.TRUE)) {
                log.fine("Calling addDocument()... for BookmarkFolder.");
	    	    BookmarkFolderIndexBuilder builder = new BookmarkFolderIndexBuilder();
	    	    builder.addDocument(bean);
	        }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public BookmarkFolder refreshBookmarkFolder(BookmarkFolder bookmarkFolder) throws BaseException
    {
        log.finer("BEGIN");

        // Param bookmarkFolder cannot be null.....
        if(bookmarkFolder == null || bookmarkFolder.getGuid() == null) {
            log.log(Level.INFO, "Param bookmarkFolder or its guid is null!");
            throw new BadRequestException("Param bookmarkFolder object or its guid is null!");
        }
        BookmarkFolderBean bean = null;
        if(bookmarkFolder instanceof BookmarkFolderBean) {
            bean = (BookmarkFolderBean) bookmarkFolder;
        } else {  // if(bookmarkFolder instanceof BookmarkFolder)
            bean = new BookmarkFolderBean(bookmarkFolder.getGuid(), bookmarkFolder.getAppClient(), bookmarkFolder.getClientRootDomain(), bookmarkFolder.getUser(), bookmarkFolder.getTitle(), bookmarkFolder.getDescription(), bookmarkFolder.getType(), bookmarkFolder.getCategory(), bookmarkFolder.getParent(), bookmarkFolder.getAggregate(), bookmarkFolder.getAcl(), bookmarkFolder.isExportable(), bookmarkFolder.getStatus(), bookmarkFolder.getNote(), bookmarkFolder.getKeywordFolder());
        }
        Boolean suc = getProxyFactory().getBookmarkFolderServiceProxy().updateBookmarkFolder(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

	    // TBD:
        if(Config.getInstance().isSearchEnabled()) {
            if(suc != null && suc.equals(Boolean.TRUE)) {
                log.fine("Calling addDocument()... for BookmarkFolder.");
	    	    BookmarkFolderIndexBuilder builder = new BookmarkFolderIndexBuilder();
	    	    builder.addDocument(bean);
	        }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteBookmarkFolder(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getBookmarkFolderServiceProxy().deleteBookmarkFolder(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }

        // TBD:
        if(Config.getInstance().isSearchEnabled()) {
            if(suc != null && suc.equals(Boolean.TRUE)) {
                log.fine("Calling removeDocument()... for BookmarkFolder.");
	    	    BookmarkFolderIndexBuilder builder = new BookmarkFolderIndexBuilder();
                builder.removeDocument(guid);
	        }
        }

            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            BookmarkFolder bookmarkFolder = null;
            try {
                bookmarkFolder = getProxyFactory().getBookmarkFolderServiceProxy().getBookmarkFolder(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch bookmarkFolder with a key, " + guid);
                return false;
            }
            if(bookmarkFolder != null) {
                String beanGuid = bookmarkFolder.getGuid();
                Boolean suc1 = getProxyFactory().getBookmarkFolderServiceProxy().deleteBookmarkFolder(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("bookmarkFolder with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteBookmarkFolder(BookmarkFolder bookmarkFolder) throws BaseException
    {
        log.finer("BEGIN");

        // Param bookmarkFolder cannot be null.....
        if(bookmarkFolder == null || bookmarkFolder.getGuid() == null) {
            log.log(Level.INFO, "Param bookmarkFolder or its guid is null!");
            throw new BadRequestException("Param bookmarkFolder object or its guid is null!");
        }
        BookmarkFolderBean bean = null;
        if(bookmarkFolder instanceof BookmarkFolderBean) {
            bean = (BookmarkFolderBean) bookmarkFolder;
        } else {  // if(bookmarkFolder instanceof BookmarkFolder)
            // ????
            log.warning("bookmarkFolder is not an instance of BookmarkFolderBean.");
            bean = new BookmarkFolderBean(bookmarkFolder.getGuid(), bookmarkFolder.getAppClient(), bookmarkFolder.getClientRootDomain(), bookmarkFolder.getUser(), bookmarkFolder.getTitle(), bookmarkFolder.getDescription(), bookmarkFolder.getType(), bookmarkFolder.getCategory(), bookmarkFolder.getParent(), bookmarkFolder.getAggregate(), bookmarkFolder.getAcl(), bookmarkFolder.isExportable(), bookmarkFolder.getStatus(), bookmarkFolder.getNote(), bookmarkFolder.getKeywordFolder());
        }
        Boolean suc = getProxyFactory().getBookmarkFolderServiceProxy().deleteBookmarkFolder(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

	    // TBD:
        if(Config.getInstance().isSearchEnabled()) {
            if(suc != null && suc.equals(Boolean.TRUE)) {
                log.fine("Calling removeDocument()... for BookmarkFolder.");
	    	    BookmarkFolderIndexBuilder builder = new BookmarkFolderIndexBuilder();
                builder.removeDocument(bean.getGuid());
	        }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteBookmarkFolders(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getBookmarkFolderServiceProxy().deleteBookmarkFolders(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

	    // TBD:
        //if(Config.getInstance().isSearchEnabled()) {
        //    if(suc != null && suc.equals(Boolean.TRUE)) {
        //      log.fine("Calling removeDocument()... for BookmarkFolder.");
	    //	    BookmarkFolderIndexBuilder builder = new BookmarkFolderIndexBuilder();
        //      builder.removeDocument(guids);
	    //    }
        //}

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createBookmarkFolders(List<BookmarkFolder> bookmarkFolders) throws BaseException
    {
        log.finer("BEGIN");

        if(bookmarkFolders == null) {
            log.log(Level.WARNING, "createBookmarkFolders() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = bookmarkFolders.size();
        if(size == 0) {
            log.log(Level.WARNING, "createBookmarkFolders() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(BookmarkFolder bookmarkFolder : bookmarkFolders) {
            String guid = createBookmarkFolder(bookmarkFolder);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createBookmarkFolders() failed for at least one bookmarkFolder. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateBookmarkFolders(List<BookmarkFolder> bookmarkFolders) throws BaseException
    //{
    //}

}
