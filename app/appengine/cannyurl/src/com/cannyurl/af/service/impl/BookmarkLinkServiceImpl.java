package com.cannyurl.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.BookmarkLink;
import com.myurldb.ws.search.gae.BookmarkLinkIndexBuilder;
import com.cannyurl.af.config.Config;

import com.cannyurl.af.bean.HashedPasswordStructBean;
import com.cannyurl.af.bean.GeoPointStructBean;
import com.cannyurl.af.bean.WebProfileStructBean;
import com.cannyurl.af.bean.StreetAddressStructBean;
import com.cannyurl.af.bean.SiteLogBean;
import com.cannyurl.af.bean.HelpNoticeBean;
import com.cannyurl.af.bean.TwitterCardAppInfoBean;
import com.cannyurl.af.bean.ContactInfoStructBean;
import com.cannyurl.af.bean.GeoCoordinateStructBean;
import com.cannyurl.af.bean.FullNameStructBean;
import com.cannyurl.af.bean.ExternalUserIdStructBean;
import com.cannyurl.af.bean.GaeUserStructBean;
import com.cannyurl.af.bean.AppBrandStructBean;
import com.cannyurl.af.bean.CellLatitudeLongitudeBean;
import com.cannyurl.af.bean.GaeAppStructBean;
import com.cannyurl.af.bean.ShortPassageAttributeBean;
import com.cannyurl.af.bean.KeyValuePairStructBean;
import com.cannyurl.af.bean.KeyValueRelationStructBean;
import com.cannyurl.af.bean.ExternalServiceApiKeyStructBean;
import com.cannyurl.af.bean.ReferrerInfoStructBean;
import com.cannyurl.af.bean.TwitterCardProductDataBean;
import com.cannyurl.af.bean.PagerStateStructBean;

import com.cannyurl.af.bean.BookmarkLinkBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.BookmarkLinkService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class BookmarkLinkServiceImpl implements BookmarkLinkService
{
    private static final Logger log = Logger.getLogger(BookmarkLinkServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "BookmarkLink-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("BookmarkLink:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public BookmarkLinkServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // BookmarkLink related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public BookmarkLink getBookmarkLink(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getBookmarkLink(): guid = " + guid);

        BookmarkLinkBean bean = null;
        if(getCache() != null) {
            bean = (BookmarkLinkBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (BookmarkLinkBean) getProxyFactory().getBookmarkLinkServiceProxy().getBookmarkLink(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "BookmarkLinkBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve BookmarkLinkBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getBookmarkLink(String guid, String field) throws BaseException
    {
        BookmarkLinkBean bean = null;
        if(getCache() != null) {
            bean = (BookmarkLinkBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (BookmarkLinkBean) getProxyFactory().getBookmarkLinkServiceProxy().getBookmarkLink(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "BookmarkLinkBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve BookmarkLinkBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("appClient")) {
            return bean.getAppClient();
        } else if(field.equals("clientRootDomain")) {
            return bean.getClientRootDomain();
        } else if(field.equals("user")) {
            return bean.getUser();
        } else if(field.equals("shortLink")) {
            return bean.getShortLink();
        } else if(field.equals("domain")) {
            return bean.getDomain();
        } else if(field.equals("token")) {
            return bean.getToken();
        } else if(field.equals("longUrl")) {
            return bean.getLongUrl();
        } else if(field.equals("shortUrl")) {
            return bean.getShortUrl();
        } else if(field.equals("internal")) {
            return bean.isInternal();
        } else if(field.equals("caching")) {
            return bean.isCaching();
        } else if(field.equals("memo")) {
            return bean.getMemo();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("note")) {
            return bean.getNote();
        } else if(field.equals("expirationTime")) {
            return bean.getExpirationTime();
        } else if(field.equals("bookmarkFolder")) {
            return bean.getBookmarkFolder();
        } else if(field.equals("contentTag")) {
            return bean.getContentTag();
        } else if(field.equals("referenceElement")) {
            return bean.getReferenceElement();
        } else if(field.equals("elementType")) {
            return bean.getElementType();
        } else if(field.equals("keywordLink")) {
            return bean.getKeywordLink();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<BookmarkLink> getBookmarkLinks(List<String> guids) throws BaseException
    {
        log.fine("getBookmarkLinks()");

        // TBD: Is there a better way????
        List<BookmarkLink> bookmarkLinks = getProxyFactory().getBookmarkLinkServiceProxy().getBookmarkLinks(guids);
        if(bookmarkLinks == null) {
            log.log(Level.WARNING, "Failed to retrieve BookmarkLinkBean list.");
        }

        log.finer("END");
        return bookmarkLinks;
    }

    @Override
    public List<BookmarkLink> getAllBookmarkLinks() throws BaseException
    {
        return getAllBookmarkLinks(null, null, null);
    }

    @Override
    public List<BookmarkLink> getAllBookmarkLinks(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllBookmarkLinks(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<BookmarkLink> bookmarkLinks = getProxyFactory().getBookmarkLinkServiceProxy().getAllBookmarkLinks(ordering, offset, count);
        if(bookmarkLinks == null) {
            log.log(Level.WARNING, "Failed to retrieve BookmarkLinkBean list.");
        }

        log.finer("END");
        return bookmarkLinks;
    }

    @Override
    public List<String> getAllBookmarkLinkKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllBookmarkLinkKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getBookmarkLinkServiceProxy().getAllBookmarkLinkKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve BookmarkLinkBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty BookmarkLinkBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<BookmarkLink> findBookmarkLinks(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findBookmarkLinks(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<BookmarkLink> findBookmarkLinks(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BookmarkLinkServiceImpl.findBookmarkLinks(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<BookmarkLink> bookmarkLinks = getProxyFactory().getBookmarkLinkServiceProxy().findBookmarkLinks(filter, ordering, params, values, grouping, unique, offset, count);
        if(bookmarkLinks == null) {
            log.log(Level.WARNING, "Failed to find bookmarkLinks for the given criterion.");
        }

        log.finer("END");
        return bookmarkLinks;
    }

    @Override
    public List<String> findBookmarkLinkKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BookmarkLinkServiceImpl.findBookmarkLinkKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getBookmarkLinkServiceProxy().findBookmarkLinkKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find BookmarkLink keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty BookmarkLink key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BookmarkLinkServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getBookmarkLinkServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createBookmarkLink(String appClient, String clientRootDomain, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, Boolean internal, Boolean caching, String memo, String status, String note, Long expirationTime, String bookmarkFolder, String contentTag, String referenceElement, String elementType, String keywordLink) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        BookmarkLinkBean bean = new BookmarkLinkBean(null, appClient, clientRootDomain, user, shortLink, domain, token, longUrl, shortUrl, internal, caching, memo, status, note, expirationTime, bookmarkFolder, contentTag, referenceElement, elementType, keywordLink);
        return createBookmarkLink(bean);
    }

    @Override
    public String createBookmarkLink(BookmarkLink bookmarkLink) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //BookmarkLink bean = constructBookmarkLink(bookmarkLink);
        //return bean.getGuid();

        // Param bookmarkLink cannot be null.....
        if(bookmarkLink == null) {
            log.log(Level.INFO, "Param bookmarkLink is null!");
            throw new BadRequestException("Param bookmarkLink object is null!");
        }
        BookmarkLinkBean bean = null;
        if(bookmarkLink instanceof BookmarkLinkBean) {
            bean = (BookmarkLinkBean) bookmarkLink;
        } else if(bookmarkLink instanceof BookmarkLink) {
            // bean = new BookmarkLinkBean(null, bookmarkLink.getAppClient(), bookmarkLink.getClientRootDomain(), bookmarkLink.getUser(), bookmarkLink.getShortLink(), bookmarkLink.getDomain(), bookmarkLink.getToken(), bookmarkLink.getLongUrl(), bookmarkLink.getShortUrl(), bookmarkLink.isInternal(), bookmarkLink.isCaching(), bookmarkLink.getMemo(), bookmarkLink.getStatus(), bookmarkLink.getNote(), bookmarkLink.getExpirationTime(), bookmarkLink.getBookmarkFolder(), bookmarkLink.getContentTag(), bookmarkLink.getReferenceElement(), bookmarkLink.getElementType(), bookmarkLink.getKeywordLink());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new BookmarkLinkBean(bookmarkLink.getGuid(), bookmarkLink.getAppClient(), bookmarkLink.getClientRootDomain(), bookmarkLink.getUser(), bookmarkLink.getShortLink(), bookmarkLink.getDomain(), bookmarkLink.getToken(), bookmarkLink.getLongUrl(), bookmarkLink.getShortUrl(), bookmarkLink.isInternal(), bookmarkLink.isCaching(), bookmarkLink.getMemo(), bookmarkLink.getStatus(), bookmarkLink.getNote(), bookmarkLink.getExpirationTime(), bookmarkLink.getBookmarkFolder(), bookmarkLink.getContentTag(), bookmarkLink.getReferenceElement(), bookmarkLink.getElementType(), bookmarkLink.getKeywordLink());
        } else {
            log.log(Level.WARNING, "createBookmarkLink(): Arg bookmarkLink is of an unknown type.");
            //bean = new BookmarkLinkBean();
            bean = new BookmarkLinkBean(bookmarkLink.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getBookmarkLinkServiceProxy().createBookmarkLink(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

	    // TBD:
	    if(Config.getInstance().isSearchEnabled()) {
            if(guid != null && !guid.isEmpty()) {
                log.fine("Calling addDocument()... for BookmarkLink.");
                BookmarkLinkIndexBuilder builder = new BookmarkLinkIndexBuilder();
	    	    builder.addDocument(bean);
	    	}
	    }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public BookmarkLink constructBookmarkLink(BookmarkLink bookmarkLink) throws BaseException
    {
        log.finer("BEGIN");

        // Param bookmarkLink cannot be null.....
        if(bookmarkLink == null) {
            log.log(Level.INFO, "Param bookmarkLink is null!");
            throw new BadRequestException("Param bookmarkLink object is null!");
        }
        BookmarkLinkBean bean = null;
        if(bookmarkLink instanceof BookmarkLinkBean) {
            bean = (BookmarkLinkBean) bookmarkLink;
        } else if(bookmarkLink instanceof BookmarkLink) {
            // bean = new BookmarkLinkBean(null, bookmarkLink.getAppClient(), bookmarkLink.getClientRootDomain(), bookmarkLink.getUser(), bookmarkLink.getShortLink(), bookmarkLink.getDomain(), bookmarkLink.getToken(), bookmarkLink.getLongUrl(), bookmarkLink.getShortUrl(), bookmarkLink.isInternal(), bookmarkLink.isCaching(), bookmarkLink.getMemo(), bookmarkLink.getStatus(), bookmarkLink.getNote(), bookmarkLink.getExpirationTime(), bookmarkLink.getBookmarkFolder(), bookmarkLink.getContentTag(), bookmarkLink.getReferenceElement(), bookmarkLink.getElementType(), bookmarkLink.getKeywordLink());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new BookmarkLinkBean(bookmarkLink.getGuid(), bookmarkLink.getAppClient(), bookmarkLink.getClientRootDomain(), bookmarkLink.getUser(), bookmarkLink.getShortLink(), bookmarkLink.getDomain(), bookmarkLink.getToken(), bookmarkLink.getLongUrl(), bookmarkLink.getShortUrl(), bookmarkLink.isInternal(), bookmarkLink.isCaching(), bookmarkLink.getMemo(), bookmarkLink.getStatus(), bookmarkLink.getNote(), bookmarkLink.getExpirationTime(), bookmarkLink.getBookmarkFolder(), bookmarkLink.getContentTag(), bookmarkLink.getReferenceElement(), bookmarkLink.getElementType(), bookmarkLink.getKeywordLink());
        } else {
            log.log(Level.WARNING, "createBookmarkLink(): Arg bookmarkLink is of an unknown type.");
            //bean = new BookmarkLinkBean();
            bean = new BookmarkLinkBean(bookmarkLink.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getBookmarkLinkServiceProxy().createBookmarkLink(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

	    // TBD:
	    if(Config.getInstance().isSearchEnabled()) {
            if(guid != null && !guid.isEmpty()) {
                log.fine("Calling addDocument()... for BookmarkLink.");
                BookmarkLinkIndexBuilder builder = new BookmarkLinkIndexBuilder();
	    	    builder.addDocument(bean);
	    	}
	    }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateBookmarkLink(String guid, String appClient, String clientRootDomain, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, Boolean internal, Boolean caching, String memo, String status, String note, Long expirationTime, String bookmarkFolder, String contentTag, String referenceElement, String elementType, String keywordLink) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        BookmarkLinkBean bean = new BookmarkLinkBean(guid, appClient, clientRootDomain, user, shortLink, domain, token, longUrl, shortUrl, internal, caching, memo, status, note, expirationTime, bookmarkFolder, contentTag, referenceElement, elementType, keywordLink);
        return updateBookmarkLink(bean);
    }
        
    @Override
    public Boolean updateBookmarkLink(BookmarkLink bookmarkLink) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //BookmarkLink bean = refreshBookmarkLink(bookmarkLink);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param bookmarkLink cannot be null.....
        if(bookmarkLink == null || bookmarkLink.getGuid() == null) {
            log.log(Level.INFO, "Param bookmarkLink or its guid is null!");
            throw new BadRequestException("Param bookmarkLink object or its guid is null!");
        }
        BookmarkLinkBean bean = null;
        if(bookmarkLink instanceof BookmarkLinkBean) {
            bean = (BookmarkLinkBean) bookmarkLink;
        } else {  // if(bookmarkLink instanceof BookmarkLink)
            bean = new BookmarkLinkBean(bookmarkLink.getGuid(), bookmarkLink.getAppClient(), bookmarkLink.getClientRootDomain(), bookmarkLink.getUser(), bookmarkLink.getShortLink(), bookmarkLink.getDomain(), bookmarkLink.getToken(), bookmarkLink.getLongUrl(), bookmarkLink.getShortUrl(), bookmarkLink.isInternal(), bookmarkLink.isCaching(), bookmarkLink.getMemo(), bookmarkLink.getStatus(), bookmarkLink.getNote(), bookmarkLink.getExpirationTime(), bookmarkLink.getBookmarkFolder(), bookmarkLink.getContentTag(), bookmarkLink.getReferenceElement(), bookmarkLink.getElementType(), bookmarkLink.getKeywordLink());
        }
        Boolean suc = getProxyFactory().getBookmarkLinkServiceProxy().updateBookmarkLink(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

	    // TBD:
        if(Config.getInstance().isSearchEnabled()) {
            if(suc != null && suc.equals(Boolean.TRUE)) {
                log.fine("Calling addDocument()... for BookmarkLink.");
	    	    BookmarkLinkIndexBuilder builder = new BookmarkLinkIndexBuilder();
	    	    builder.addDocument(bean);
	        }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public BookmarkLink refreshBookmarkLink(BookmarkLink bookmarkLink) throws BaseException
    {
        log.finer("BEGIN");

        // Param bookmarkLink cannot be null.....
        if(bookmarkLink == null || bookmarkLink.getGuid() == null) {
            log.log(Level.INFO, "Param bookmarkLink or its guid is null!");
            throw new BadRequestException("Param bookmarkLink object or its guid is null!");
        }
        BookmarkLinkBean bean = null;
        if(bookmarkLink instanceof BookmarkLinkBean) {
            bean = (BookmarkLinkBean) bookmarkLink;
        } else {  // if(bookmarkLink instanceof BookmarkLink)
            bean = new BookmarkLinkBean(bookmarkLink.getGuid(), bookmarkLink.getAppClient(), bookmarkLink.getClientRootDomain(), bookmarkLink.getUser(), bookmarkLink.getShortLink(), bookmarkLink.getDomain(), bookmarkLink.getToken(), bookmarkLink.getLongUrl(), bookmarkLink.getShortUrl(), bookmarkLink.isInternal(), bookmarkLink.isCaching(), bookmarkLink.getMemo(), bookmarkLink.getStatus(), bookmarkLink.getNote(), bookmarkLink.getExpirationTime(), bookmarkLink.getBookmarkFolder(), bookmarkLink.getContentTag(), bookmarkLink.getReferenceElement(), bookmarkLink.getElementType(), bookmarkLink.getKeywordLink());
        }
        Boolean suc = getProxyFactory().getBookmarkLinkServiceProxy().updateBookmarkLink(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

	    // TBD:
        if(Config.getInstance().isSearchEnabled()) {
            if(suc != null && suc.equals(Boolean.TRUE)) {
                log.fine("Calling addDocument()... for BookmarkLink.");
	    	    BookmarkLinkIndexBuilder builder = new BookmarkLinkIndexBuilder();
	    	    builder.addDocument(bean);
	        }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteBookmarkLink(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getBookmarkLinkServiceProxy().deleteBookmarkLink(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }

        // TBD:
        if(Config.getInstance().isSearchEnabled()) {
            if(suc != null && suc.equals(Boolean.TRUE)) {
                log.fine("Calling removeDocument()... for BookmarkLink.");
	    	    BookmarkLinkIndexBuilder builder = new BookmarkLinkIndexBuilder();
                builder.removeDocument(guid);
	        }
        }

            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            BookmarkLink bookmarkLink = null;
            try {
                bookmarkLink = getProxyFactory().getBookmarkLinkServiceProxy().getBookmarkLink(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch bookmarkLink with a key, " + guid);
                return false;
            }
            if(bookmarkLink != null) {
                String beanGuid = bookmarkLink.getGuid();
                Boolean suc1 = getProxyFactory().getBookmarkLinkServiceProxy().deleteBookmarkLink(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("bookmarkLink with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteBookmarkLink(BookmarkLink bookmarkLink) throws BaseException
    {
        log.finer("BEGIN");

        // Param bookmarkLink cannot be null.....
        if(bookmarkLink == null || bookmarkLink.getGuid() == null) {
            log.log(Level.INFO, "Param bookmarkLink or its guid is null!");
            throw new BadRequestException("Param bookmarkLink object or its guid is null!");
        }
        BookmarkLinkBean bean = null;
        if(bookmarkLink instanceof BookmarkLinkBean) {
            bean = (BookmarkLinkBean) bookmarkLink;
        } else {  // if(bookmarkLink instanceof BookmarkLink)
            // ????
            log.warning("bookmarkLink is not an instance of BookmarkLinkBean.");
            bean = new BookmarkLinkBean(bookmarkLink.getGuid(), bookmarkLink.getAppClient(), bookmarkLink.getClientRootDomain(), bookmarkLink.getUser(), bookmarkLink.getShortLink(), bookmarkLink.getDomain(), bookmarkLink.getToken(), bookmarkLink.getLongUrl(), bookmarkLink.getShortUrl(), bookmarkLink.isInternal(), bookmarkLink.isCaching(), bookmarkLink.getMemo(), bookmarkLink.getStatus(), bookmarkLink.getNote(), bookmarkLink.getExpirationTime(), bookmarkLink.getBookmarkFolder(), bookmarkLink.getContentTag(), bookmarkLink.getReferenceElement(), bookmarkLink.getElementType(), bookmarkLink.getKeywordLink());
        }
        Boolean suc = getProxyFactory().getBookmarkLinkServiceProxy().deleteBookmarkLink(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

	    // TBD:
        if(Config.getInstance().isSearchEnabled()) {
            if(suc != null && suc.equals(Boolean.TRUE)) {
                log.fine("Calling removeDocument()... for BookmarkLink.");
	    	    BookmarkLinkIndexBuilder builder = new BookmarkLinkIndexBuilder();
                builder.removeDocument(bean.getGuid());
	        }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteBookmarkLinks(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getBookmarkLinkServiceProxy().deleteBookmarkLinks(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

	    // TBD:
        //if(Config.getInstance().isSearchEnabled()) {
        //    if(suc != null && suc.equals(Boolean.TRUE)) {
        //      log.fine("Calling removeDocument()... for BookmarkLink.");
	    //	    BookmarkLinkIndexBuilder builder = new BookmarkLinkIndexBuilder();
        //      builder.removeDocument(guids);
	    //    }
        //}

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createBookmarkLinks(List<BookmarkLink> bookmarkLinks) throws BaseException
    {
        log.finer("BEGIN");

        if(bookmarkLinks == null) {
            log.log(Level.WARNING, "createBookmarkLinks() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = bookmarkLinks.size();
        if(size == 0) {
            log.log(Level.WARNING, "createBookmarkLinks() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(BookmarkLink bookmarkLink : bookmarkLinks) {
            String guid = createBookmarkLink(bookmarkLink);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createBookmarkLinks() failed for at least one bookmarkLink. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateBookmarkLinks(List<BookmarkLink> bookmarkLinks) throws BaseException
    //{
    //}

}
