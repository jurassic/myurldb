package com.cannyurl.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.ClientSetting;
import com.cannyurl.af.config.Config;

import com.cannyurl.af.bean.HashedPasswordStructBean;
import com.cannyurl.af.bean.GeoPointStructBean;
import com.cannyurl.af.bean.WebProfileStructBean;
import com.cannyurl.af.bean.StreetAddressStructBean;
import com.cannyurl.af.bean.SiteLogBean;
import com.cannyurl.af.bean.HelpNoticeBean;
import com.cannyurl.af.bean.TwitterCardAppInfoBean;
import com.cannyurl.af.bean.ContactInfoStructBean;
import com.cannyurl.af.bean.GeoCoordinateStructBean;
import com.cannyurl.af.bean.FullNameStructBean;
import com.cannyurl.af.bean.ExternalUserIdStructBean;
import com.cannyurl.af.bean.GaeUserStructBean;
import com.cannyurl.af.bean.AppBrandStructBean;
import com.cannyurl.af.bean.CellLatitudeLongitudeBean;
import com.cannyurl.af.bean.GaeAppStructBean;
import com.cannyurl.af.bean.ShortPassageAttributeBean;
import com.cannyurl.af.bean.KeyValuePairStructBean;
import com.cannyurl.af.bean.KeyValueRelationStructBean;
import com.cannyurl.af.bean.ExternalServiceApiKeyStructBean;
import com.cannyurl.af.bean.ReferrerInfoStructBean;
import com.cannyurl.af.bean.TwitterCardProductDataBean;
import com.cannyurl.af.bean.PagerStateStructBean;

import com.cannyurl.af.bean.ClientSettingBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.ClientSettingService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class ClientSettingServiceImpl implements ClientSettingService
{
    private static final Logger log = Logger.getLogger(ClientSettingServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "ClientSetting-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("ClientSetting:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public ClientSettingServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // ClientSetting related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public ClientSetting getClientSetting(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getClientSetting(): guid = " + guid);

        ClientSettingBean bean = null;
        if(getCache() != null) {
            bean = (ClientSettingBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (ClientSettingBean) getProxyFactory().getClientSettingServiceProxy().getClientSetting(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "ClientSettingBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve ClientSettingBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getClientSetting(String guid, String field) throws BaseException
    {
        ClientSettingBean bean = null;
        if(getCache() != null) {
            bean = (ClientSettingBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (ClientSettingBean) getProxyFactory().getClientSettingServiceProxy().getClientSetting(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "ClientSettingBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve ClientSettingBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("appClient")) {
            return bean.getAppClient();
        } else if(field.equals("admin")) {
            return bean.getAdmin();
        } else if(field.equals("autoRedirectEnabled")) {
            return bean.isAutoRedirectEnabled();
        } else if(field.equals("viewEnabled")) {
            return bean.isViewEnabled();
        } else if(field.equals("shareEnabled")) {
            return bean.isShareEnabled();
        } else if(field.equals("defaultBaseDomain")) {
            return bean.getDefaultBaseDomain();
        } else if(field.equals("defaultDomainType")) {
            return bean.getDefaultDomainType();
        } else if(field.equals("defaultTokenType")) {
            return bean.getDefaultTokenType();
        } else if(field.equals("defaultRedirectType")) {
            return bean.getDefaultRedirectType();
        } else if(field.equals("defaultFlashDuration")) {
            return bean.getDefaultFlashDuration();
        } else if(field.equals("defaultAccessType")) {
            return bean.getDefaultAccessType();
        } else if(field.equals("defaultViewType")) {
            return bean.getDefaultViewType();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<ClientSetting> getClientSettings(List<String> guids) throws BaseException
    {
        log.fine("getClientSettings()");

        // TBD: Is there a better way????
        List<ClientSetting> clientSettings = getProxyFactory().getClientSettingServiceProxy().getClientSettings(guids);
        if(clientSettings == null) {
            log.log(Level.WARNING, "Failed to retrieve ClientSettingBean list.");
        }

        log.finer("END");
        return clientSettings;
    }

    @Override
    public List<ClientSetting> getAllClientSettings() throws BaseException
    {
        return getAllClientSettings(null, null, null);
    }

    @Override
    public List<ClientSetting> getAllClientSettings(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllClientSettings(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<ClientSetting> clientSettings = getProxyFactory().getClientSettingServiceProxy().getAllClientSettings(ordering, offset, count);
        if(clientSettings == null) {
            log.log(Level.WARNING, "Failed to retrieve ClientSettingBean list.");
        }

        log.finer("END");
        return clientSettings;
    }

    @Override
    public List<String> getAllClientSettingKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllClientSettingKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getClientSettingServiceProxy().getAllClientSettingKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve ClientSettingBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty ClientSettingBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<ClientSetting> findClientSettings(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findClientSettings(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<ClientSetting> findClientSettings(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("ClientSettingServiceImpl.findClientSettings(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<ClientSetting> clientSettings = getProxyFactory().getClientSettingServiceProxy().findClientSettings(filter, ordering, params, values, grouping, unique, offset, count);
        if(clientSettings == null) {
            log.log(Level.WARNING, "Failed to find clientSettings for the given criterion.");
        }

        log.finer("END");
        return clientSettings;
    }

    @Override
    public List<String> findClientSettingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("ClientSettingServiceImpl.findClientSettingKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getClientSettingServiceProxy().findClientSettingKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find ClientSetting keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty ClientSetting key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("ClientSettingServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getClientSettingServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createClientSetting(String appClient, String admin, Boolean autoRedirectEnabled, Boolean viewEnabled, Boolean shareEnabled, String defaultBaseDomain, String defaultDomainType, String defaultTokenType, String defaultRedirectType, Long defaultFlashDuration, String defaultAccessType, String defaultViewType) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        ClientSettingBean bean = new ClientSettingBean(null, appClient, admin, autoRedirectEnabled, viewEnabled, shareEnabled, defaultBaseDomain, defaultDomainType, defaultTokenType, defaultRedirectType, defaultFlashDuration, defaultAccessType, defaultViewType);
        return createClientSetting(bean);
    }

    @Override
    public String createClientSetting(ClientSetting clientSetting) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //ClientSetting bean = constructClientSetting(clientSetting);
        //return bean.getGuid();

        // Param clientSetting cannot be null.....
        if(clientSetting == null) {
            log.log(Level.INFO, "Param clientSetting is null!");
            throw new BadRequestException("Param clientSetting object is null!");
        }
        ClientSettingBean bean = null;
        if(clientSetting instanceof ClientSettingBean) {
            bean = (ClientSettingBean) clientSetting;
        } else if(clientSetting instanceof ClientSetting) {
            // bean = new ClientSettingBean(null, clientSetting.getAppClient(), clientSetting.getAdmin(), clientSetting.isAutoRedirectEnabled(), clientSetting.isViewEnabled(), clientSetting.isShareEnabled(), clientSetting.getDefaultBaseDomain(), clientSetting.getDefaultDomainType(), clientSetting.getDefaultTokenType(), clientSetting.getDefaultRedirectType(), clientSetting.getDefaultFlashDuration(), clientSetting.getDefaultAccessType(), clientSetting.getDefaultViewType());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new ClientSettingBean(clientSetting.getGuid(), clientSetting.getAppClient(), clientSetting.getAdmin(), clientSetting.isAutoRedirectEnabled(), clientSetting.isViewEnabled(), clientSetting.isShareEnabled(), clientSetting.getDefaultBaseDomain(), clientSetting.getDefaultDomainType(), clientSetting.getDefaultTokenType(), clientSetting.getDefaultRedirectType(), clientSetting.getDefaultFlashDuration(), clientSetting.getDefaultAccessType(), clientSetting.getDefaultViewType());
        } else {
            log.log(Level.WARNING, "createClientSetting(): Arg clientSetting is of an unknown type.");
            //bean = new ClientSettingBean();
            bean = new ClientSettingBean(clientSetting.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getClientSettingServiceProxy().createClientSetting(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public ClientSetting constructClientSetting(ClientSetting clientSetting) throws BaseException
    {
        log.finer("BEGIN");

        // Param clientSetting cannot be null.....
        if(clientSetting == null) {
            log.log(Level.INFO, "Param clientSetting is null!");
            throw new BadRequestException("Param clientSetting object is null!");
        }
        ClientSettingBean bean = null;
        if(clientSetting instanceof ClientSettingBean) {
            bean = (ClientSettingBean) clientSetting;
        } else if(clientSetting instanceof ClientSetting) {
            // bean = new ClientSettingBean(null, clientSetting.getAppClient(), clientSetting.getAdmin(), clientSetting.isAutoRedirectEnabled(), clientSetting.isViewEnabled(), clientSetting.isShareEnabled(), clientSetting.getDefaultBaseDomain(), clientSetting.getDefaultDomainType(), clientSetting.getDefaultTokenType(), clientSetting.getDefaultRedirectType(), clientSetting.getDefaultFlashDuration(), clientSetting.getDefaultAccessType(), clientSetting.getDefaultViewType());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new ClientSettingBean(clientSetting.getGuid(), clientSetting.getAppClient(), clientSetting.getAdmin(), clientSetting.isAutoRedirectEnabled(), clientSetting.isViewEnabled(), clientSetting.isShareEnabled(), clientSetting.getDefaultBaseDomain(), clientSetting.getDefaultDomainType(), clientSetting.getDefaultTokenType(), clientSetting.getDefaultRedirectType(), clientSetting.getDefaultFlashDuration(), clientSetting.getDefaultAccessType(), clientSetting.getDefaultViewType());
        } else {
            log.log(Level.WARNING, "createClientSetting(): Arg clientSetting is of an unknown type.");
            //bean = new ClientSettingBean();
            bean = new ClientSettingBean(clientSetting.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getClientSettingServiceProxy().createClientSetting(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateClientSetting(String guid, String appClient, String admin, Boolean autoRedirectEnabled, Boolean viewEnabled, Boolean shareEnabled, String defaultBaseDomain, String defaultDomainType, String defaultTokenType, String defaultRedirectType, Long defaultFlashDuration, String defaultAccessType, String defaultViewType) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        ClientSettingBean bean = new ClientSettingBean(guid, appClient, admin, autoRedirectEnabled, viewEnabled, shareEnabled, defaultBaseDomain, defaultDomainType, defaultTokenType, defaultRedirectType, defaultFlashDuration, defaultAccessType, defaultViewType);
        return updateClientSetting(bean);
    }
        
    @Override
    public Boolean updateClientSetting(ClientSetting clientSetting) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //ClientSetting bean = refreshClientSetting(clientSetting);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param clientSetting cannot be null.....
        if(clientSetting == null || clientSetting.getGuid() == null) {
            log.log(Level.INFO, "Param clientSetting or its guid is null!");
            throw new BadRequestException("Param clientSetting object or its guid is null!");
        }
        ClientSettingBean bean = null;
        if(clientSetting instanceof ClientSettingBean) {
            bean = (ClientSettingBean) clientSetting;
        } else {  // if(clientSetting instanceof ClientSetting)
            bean = new ClientSettingBean(clientSetting.getGuid(), clientSetting.getAppClient(), clientSetting.getAdmin(), clientSetting.isAutoRedirectEnabled(), clientSetting.isViewEnabled(), clientSetting.isShareEnabled(), clientSetting.getDefaultBaseDomain(), clientSetting.getDefaultDomainType(), clientSetting.getDefaultTokenType(), clientSetting.getDefaultRedirectType(), clientSetting.getDefaultFlashDuration(), clientSetting.getDefaultAccessType(), clientSetting.getDefaultViewType());
        }
        Boolean suc = getProxyFactory().getClientSettingServiceProxy().updateClientSetting(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public ClientSetting refreshClientSetting(ClientSetting clientSetting) throws BaseException
    {
        log.finer("BEGIN");

        // Param clientSetting cannot be null.....
        if(clientSetting == null || clientSetting.getGuid() == null) {
            log.log(Level.INFO, "Param clientSetting or its guid is null!");
            throw new BadRequestException("Param clientSetting object or its guid is null!");
        }
        ClientSettingBean bean = null;
        if(clientSetting instanceof ClientSettingBean) {
            bean = (ClientSettingBean) clientSetting;
        } else {  // if(clientSetting instanceof ClientSetting)
            bean = new ClientSettingBean(clientSetting.getGuid(), clientSetting.getAppClient(), clientSetting.getAdmin(), clientSetting.isAutoRedirectEnabled(), clientSetting.isViewEnabled(), clientSetting.isShareEnabled(), clientSetting.getDefaultBaseDomain(), clientSetting.getDefaultDomainType(), clientSetting.getDefaultTokenType(), clientSetting.getDefaultRedirectType(), clientSetting.getDefaultFlashDuration(), clientSetting.getDefaultAccessType(), clientSetting.getDefaultViewType());
        }
        Boolean suc = getProxyFactory().getClientSettingServiceProxy().updateClientSetting(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteClientSetting(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getClientSettingServiceProxy().deleteClientSetting(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }
            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            ClientSetting clientSetting = null;
            try {
                clientSetting = getProxyFactory().getClientSettingServiceProxy().getClientSetting(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch clientSetting with a key, " + guid);
                return false;
            }
            if(clientSetting != null) {
                String beanGuid = clientSetting.getGuid();
                Boolean suc1 = getProxyFactory().getClientSettingServiceProxy().deleteClientSetting(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("clientSetting with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteClientSetting(ClientSetting clientSetting) throws BaseException
    {
        log.finer("BEGIN");

        // Param clientSetting cannot be null.....
        if(clientSetting == null || clientSetting.getGuid() == null) {
            log.log(Level.INFO, "Param clientSetting or its guid is null!");
            throw new BadRequestException("Param clientSetting object or its guid is null!");
        }
        ClientSettingBean bean = null;
        if(clientSetting instanceof ClientSettingBean) {
            bean = (ClientSettingBean) clientSetting;
        } else {  // if(clientSetting instanceof ClientSetting)
            // ????
            log.warning("clientSetting is not an instance of ClientSettingBean.");
            bean = new ClientSettingBean(clientSetting.getGuid(), clientSetting.getAppClient(), clientSetting.getAdmin(), clientSetting.isAutoRedirectEnabled(), clientSetting.isViewEnabled(), clientSetting.isShareEnabled(), clientSetting.getDefaultBaseDomain(), clientSetting.getDefaultDomainType(), clientSetting.getDefaultTokenType(), clientSetting.getDefaultRedirectType(), clientSetting.getDefaultFlashDuration(), clientSetting.getDefaultAccessType(), clientSetting.getDefaultViewType());
        }
        Boolean suc = getProxyFactory().getClientSettingServiceProxy().deleteClientSetting(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteClientSettings(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getClientSettingServiceProxy().deleteClientSettings(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createClientSettings(List<ClientSetting> clientSettings) throws BaseException
    {
        log.finer("BEGIN");

        if(clientSettings == null) {
            log.log(Level.WARNING, "createClientSettings() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = clientSettings.size();
        if(size == 0) {
            log.log(Level.WARNING, "createClientSettings() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(ClientSetting clientSetting : clientSettings) {
            String guid = createClientSetting(clientSetting);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createClientSettings() failed for at least one clientSetting. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateClientSettings(List<ClientSetting> clientSettings) throws BaseException
    //{
    //}

}
