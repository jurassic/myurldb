package com.cannyurl.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.DomainInfo;
import com.myurldb.ws.search.gae.DomainInfoIndexBuilder;
import com.cannyurl.af.config.Config;

import com.cannyurl.af.bean.HashedPasswordStructBean;
import com.cannyurl.af.bean.GeoPointStructBean;
import com.cannyurl.af.bean.WebProfileStructBean;
import com.cannyurl.af.bean.StreetAddressStructBean;
import com.cannyurl.af.bean.SiteLogBean;
import com.cannyurl.af.bean.HelpNoticeBean;
import com.cannyurl.af.bean.TwitterCardAppInfoBean;
import com.cannyurl.af.bean.ContactInfoStructBean;
import com.cannyurl.af.bean.GeoCoordinateStructBean;
import com.cannyurl.af.bean.FullNameStructBean;
import com.cannyurl.af.bean.ExternalUserIdStructBean;
import com.cannyurl.af.bean.GaeUserStructBean;
import com.cannyurl.af.bean.AppBrandStructBean;
import com.cannyurl.af.bean.CellLatitudeLongitudeBean;
import com.cannyurl.af.bean.GaeAppStructBean;
import com.cannyurl.af.bean.ShortPassageAttributeBean;
import com.cannyurl.af.bean.KeyValuePairStructBean;
import com.cannyurl.af.bean.KeyValueRelationStructBean;
import com.cannyurl.af.bean.ExternalServiceApiKeyStructBean;
import com.cannyurl.af.bean.ReferrerInfoStructBean;
import com.cannyurl.af.bean.TwitterCardProductDataBean;
import com.cannyurl.af.bean.PagerStateStructBean;

import com.cannyurl.af.bean.DomainInfoBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.DomainInfoService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class DomainInfoServiceImpl implements DomainInfoService
{
    private static final Logger log = Logger.getLogger(DomainInfoServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "DomainInfo-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("DomainInfo:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public DomainInfoServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // DomainInfo related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public DomainInfo getDomainInfo(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getDomainInfo(): guid = " + guid);

        DomainInfoBean bean = null;
        if(getCache() != null) {
            bean = (DomainInfoBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (DomainInfoBean) getProxyFactory().getDomainInfoServiceProxy().getDomainInfo(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "DomainInfoBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve DomainInfoBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getDomainInfo(String guid, String field) throws BaseException
    {
        DomainInfoBean bean = null;
        if(getCache() != null) {
            bean = (DomainInfoBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (DomainInfoBean) getProxyFactory().getDomainInfoServiceProxy().getDomainInfo(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "DomainInfoBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve DomainInfoBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("domain")) {
            return bean.getDomain();
        } else if(field.equals("banned")) {
            return bean.isBanned();
        } else if(field.equals("urlShortener")) {
            return bean.isUrlShortener();
        } else if(field.equals("category")) {
            return bean.getCategory();
        } else if(field.equals("reputation")) {
            return bean.getReputation();
        } else if(field.equals("authority")) {
            return bean.getAuthority();
        } else if(field.equals("note")) {
            return bean.getNote();
        } else if(field.equals("verifiedTime")) {
            return bean.getVerifiedTime();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<DomainInfo> getDomainInfos(List<String> guids) throws BaseException
    {
        log.fine("getDomainInfos()");

        // TBD: Is there a better way????
        List<DomainInfo> domainInfos = getProxyFactory().getDomainInfoServiceProxy().getDomainInfos(guids);
        if(domainInfos == null) {
            log.log(Level.WARNING, "Failed to retrieve DomainInfoBean list.");
        }

        log.finer("END");
        return domainInfos;
    }

    @Override
    public List<DomainInfo> getAllDomainInfos() throws BaseException
    {
        return getAllDomainInfos(null, null, null);
    }

    @Override
    public List<DomainInfo> getAllDomainInfos(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllDomainInfos(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<DomainInfo> domainInfos = getProxyFactory().getDomainInfoServiceProxy().getAllDomainInfos(ordering, offset, count);
        if(domainInfos == null) {
            log.log(Level.WARNING, "Failed to retrieve DomainInfoBean list.");
        }

        log.finer("END");
        return domainInfos;
    }

    @Override
    public List<String> getAllDomainInfoKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllDomainInfoKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getDomainInfoServiceProxy().getAllDomainInfoKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve DomainInfoBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty DomainInfoBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<DomainInfo> findDomainInfos(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findDomainInfos(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<DomainInfo> findDomainInfos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("DomainInfoServiceImpl.findDomainInfos(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<DomainInfo> domainInfos = getProxyFactory().getDomainInfoServiceProxy().findDomainInfos(filter, ordering, params, values, grouping, unique, offset, count);
        if(domainInfos == null) {
            log.log(Level.WARNING, "Failed to find domainInfos for the given criterion.");
        }

        log.finer("END");
        return domainInfos;
    }

    @Override
    public List<String> findDomainInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("DomainInfoServiceImpl.findDomainInfoKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getDomainInfoServiceProxy().findDomainInfoKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find DomainInfo keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty DomainInfo key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("DomainInfoServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getDomainInfoServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createDomainInfo(String domain, Boolean banned, Boolean urlShortener, String category, String reputation, String authority, String note, Long verifiedTime) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        DomainInfoBean bean = new DomainInfoBean(null, domain, banned, urlShortener, category, reputation, authority, note, verifiedTime);
        return createDomainInfo(bean);
    }

    @Override
    public String createDomainInfo(DomainInfo domainInfo) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //DomainInfo bean = constructDomainInfo(domainInfo);
        //return bean.getGuid();

        // Param domainInfo cannot be null.....
        if(domainInfo == null) {
            log.log(Level.INFO, "Param domainInfo is null!");
            throw new BadRequestException("Param domainInfo object is null!");
        }
        DomainInfoBean bean = null;
        if(domainInfo instanceof DomainInfoBean) {
            bean = (DomainInfoBean) domainInfo;
        } else if(domainInfo instanceof DomainInfo) {
            // bean = new DomainInfoBean(null, domainInfo.getDomain(), domainInfo.isBanned(), domainInfo.isUrlShortener(), domainInfo.getCategory(), domainInfo.getReputation(), domainInfo.getAuthority(), domainInfo.getNote(), domainInfo.getVerifiedTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new DomainInfoBean(domainInfo.getGuid(), domainInfo.getDomain(), domainInfo.isBanned(), domainInfo.isUrlShortener(), domainInfo.getCategory(), domainInfo.getReputation(), domainInfo.getAuthority(), domainInfo.getNote(), domainInfo.getVerifiedTime());
        } else {
            log.log(Level.WARNING, "createDomainInfo(): Arg domainInfo is of an unknown type.");
            //bean = new DomainInfoBean();
            bean = new DomainInfoBean(domainInfo.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getDomainInfoServiceProxy().createDomainInfo(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

	    // TBD:
	    if(Config.getInstance().isSearchEnabled()) {
            if(guid != null && !guid.isEmpty()) {
                log.fine("Calling addDocument()... for DomainInfo.");
                DomainInfoIndexBuilder builder = new DomainInfoIndexBuilder();
	    	    builder.addDocument(bean);
	    	}
	    }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public DomainInfo constructDomainInfo(DomainInfo domainInfo) throws BaseException
    {
        log.finer("BEGIN");

        // Param domainInfo cannot be null.....
        if(domainInfo == null) {
            log.log(Level.INFO, "Param domainInfo is null!");
            throw new BadRequestException("Param domainInfo object is null!");
        }
        DomainInfoBean bean = null;
        if(domainInfo instanceof DomainInfoBean) {
            bean = (DomainInfoBean) domainInfo;
        } else if(domainInfo instanceof DomainInfo) {
            // bean = new DomainInfoBean(null, domainInfo.getDomain(), domainInfo.isBanned(), domainInfo.isUrlShortener(), domainInfo.getCategory(), domainInfo.getReputation(), domainInfo.getAuthority(), domainInfo.getNote(), domainInfo.getVerifiedTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new DomainInfoBean(domainInfo.getGuid(), domainInfo.getDomain(), domainInfo.isBanned(), domainInfo.isUrlShortener(), domainInfo.getCategory(), domainInfo.getReputation(), domainInfo.getAuthority(), domainInfo.getNote(), domainInfo.getVerifiedTime());
        } else {
            log.log(Level.WARNING, "createDomainInfo(): Arg domainInfo is of an unknown type.");
            //bean = new DomainInfoBean();
            bean = new DomainInfoBean(domainInfo.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getDomainInfoServiceProxy().createDomainInfo(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

	    // TBD:
	    if(Config.getInstance().isSearchEnabled()) {
            if(guid != null && !guid.isEmpty()) {
                log.fine("Calling addDocument()... for DomainInfo.");
                DomainInfoIndexBuilder builder = new DomainInfoIndexBuilder();
	    	    builder.addDocument(bean);
	    	}
	    }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateDomainInfo(String guid, String domain, Boolean banned, Boolean urlShortener, String category, String reputation, String authority, String note, Long verifiedTime) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        DomainInfoBean bean = new DomainInfoBean(guid, domain, banned, urlShortener, category, reputation, authority, note, verifiedTime);
        return updateDomainInfo(bean);
    }
        
    @Override
    public Boolean updateDomainInfo(DomainInfo domainInfo) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //DomainInfo bean = refreshDomainInfo(domainInfo);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param domainInfo cannot be null.....
        if(domainInfo == null || domainInfo.getGuid() == null) {
            log.log(Level.INFO, "Param domainInfo or its guid is null!");
            throw new BadRequestException("Param domainInfo object or its guid is null!");
        }
        DomainInfoBean bean = null;
        if(domainInfo instanceof DomainInfoBean) {
            bean = (DomainInfoBean) domainInfo;
        } else {  // if(domainInfo instanceof DomainInfo)
            bean = new DomainInfoBean(domainInfo.getGuid(), domainInfo.getDomain(), domainInfo.isBanned(), domainInfo.isUrlShortener(), domainInfo.getCategory(), domainInfo.getReputation(), domainInfo.getAuthority(), domainInfo.getNote(), domainInfo.getVerifiedTime());
        }
        Boolean suc = getProxyFactory().getDomainInfoServiceProxy().updateDomainInfo(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

	    // TBD:
        if(Config.getInstance().isSearchEnabled()) {
            if(suc != null && suc.equals(Boolean.TRUE)) {
                log.fine("Calling addDocument()... for DomainInfo.");
	    	    DomainInfoIndexBuilder builder = new DomainInfoIndexBuilder();
	    	    builder.addDocument(bean);
	        }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public DomainInfo refreshDomainInfo(DomainInfo domainInfo) throws BaseException
    {
        log.finer("BEGIN");

        // Param domainInfo cannot be null.....
        if(domainInfo == null || domainInfo.getGuid() == null) {
            log.log(Level.INFO, "Param domainInfo or its guid is null!");
            throw new BadRequestException("Param domainInfo object or its guid is null!");
        }
        DomainInfoBean bean = null;
        if(domainInfo instanceof DomainInfoBean) {
            bean = (DomainInfoBean) domainInfo;
        } else {  // if(domainInfo instanceof DomainInfo)
            bean = new DomainInfoBean(domainInfo.getGuid(), domainInfo.getDomain(), domainInfo.isBanned(), domainInfo.isUrlShortener(), domainInfo.getCategory(), domainInfo.getReputation(), domainInfo.getAuthority(), domainInfo.getNote(), domainInfo.getVerifiedTime());
        }
        Boolean suc = getProxyFactory().getDomainInfoServiceProxy().updateDomainInfo(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

	    // TBD:
        if(Config.getInstance().isSearchEnabled()) {
            if(suc != null && suc.equals(Boolean.TRUE)) {
                log.fine("Calling addDocument()... for DomainInfo.");
	    	    DomainInfoIndexBuilder builder = new DomainInfoIndexBuilder();
	    	    builder.addDocument(bean);
	        }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteDomainInfo(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getDomainInfoServiceProxy().deleteDomainInfo(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }

        // TBD:
        if(Config.getInstance().isSearchEnabled()) {
            if(suc != null && suc.equals(Boolean.TRUE)) {
                log.fine("Calling removeDocument()... for DomainInfo.");
	    	    DomainInfoIndexBuilder builder = new DomainInfoIndexBuilder();
                builder.removeDocument(guid);
	        }
        }

            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            DomainInfo domainInfo = null;
            try {
                domainInfo = getProxyFactory().getDomainInfoServiceProxy().getDomainInfo(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch domainInfo with a key, " + guid);
                return false;
            }
            if(domainInfo != null) {
                String beanGuid = domainInfo.getGuid();
                Boolean suc1 = getProxyFactory().getDomainInfoServiceProxy().deleteDomainInfo(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("domainInfo with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteDomainInfo(DomainInfo domainInfo) throws BaseException
    {
        log.finer("BEGIN");

        // Param domainInfo cannot be null.....
        if(domainInfo == null || domainInfo.getGuid() == null) {
            log.log(Level.INFO, "Param domainInfo or its guid is null!");
            throw new BadRequestException("Param domainInfo object or its guid is null!");
        }
        DomainInfoBean bean = null;
        if(domainInfo instanceof DomainInfoBean) {
            bean = (DomainInfoBean) domainInfo;
        } else {  // if(domainInfo instanceof DomainInfo)
            // ????
            log.warning("domainInfo is not an instance of DomainInfoBean.");
            bean = new DomainInfoBean(domainInfo.getGuid(), domainInfo.getDomain(), domainInfo.isBanned(), domainInfo.isUrlShortener(), domainInfo.getCategory(), domainInfo.getReputation(), domainInfo.getAuthority(), domainInfo.getNote(), domainInfo.getVerifiedTime());
        }
        Boolean suc = getProxyFactory().getDomainInfoServiceProxy().deleteDomainInfo(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

	    // TBD:
        if(Config.getInstance().isSearchEnabled()) {
            if(suc != null && suc.equals(Boolean.TRUE)) {
                log.fine("Calling removeDocument()... for DomainInfo.");
	    	    DomainInfoIndexBuilder builder = new DomainInfoIndexBuilder();
                builder.removeDocument(bean.getGuid());
	        }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteDomainInfos(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getDomainInfoServiceProxy().deleteDomainInfos(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

	    // TBD:
        //if(Config.getInstance().isSearchEnabled()) {
        //    if(suc != null && suc.equals(Boolean.TRUE)) {
        //      log.fine("Calling removeDocument()... for DomainInfo.");
	    //	    DomainInfoIndexBuilder builder = new DomainInfoIndexBuilder();
        //      builder.removeDocument(guids);
	    //    }
        //}

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createDomainInfos(List<DomainInfo> domainInfos) throws BaseException
    {
        log.finer("BEGIN");

        if(domainInfos == null) {
            log.log(Level.WARNING, "createDomainInfos() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = domainInfos.size();
        if(size == 0) {
            log.log(Level.WARNING, "createDomainInfos() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(DomainInfo domainInfo : domainInfos) {
            String guid = createDomainInfo(domainInfo);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createDomainInfos() failed for at least one domainInfo. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateDomainInfos(List<DomainInfo> domainInfos) throws BaseException
    //{
    //}

}
