package com.cannyurl.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.CellLatitudeLongitude;
import com.myurldb.ws.GeoCoordinateStruct;
import com.myurldb.ws.GeoLink;
import com.myurldb.ws.search.gae.GeoLinkIndexBuilder;
import com.cannyurl.af.config.Config;

import com.cannyurl.af.bean.HashedPasswordStructBean;
import com.cannyurl.af.bean.GeoPointStructBean;
import com.cannyurl.af.bean.WebProfileStructBean;
import com.cannyurl.af.bean.StreetAddressStructBean;
import com.cannyurl.af.bean.SiteLogBean;
import com.cannyurl.af.bean.HelpNoticeBean;
import com.cannyurl.af.bean.TwitterCardAppInfoBean;
import com.cannyurl.af.bean.ContactInfoStructBean;
import com.cannyurl.af.bean.GeoCoordinateStructBean;
import com.cannyurl.af.bean.FullNameStructBean;
import com.cannyurl.af.bean.ExternalUserIdStructBean;
import com.cannyurl.af.bean.GaeUserStructBean;
import com.cannyurl.af.bean.AppBrandStructBean;
import com.cannyurl.af.bean.CellLatitudeLongitudeBean;
import com.cannyurl.af.bean.GaeAppStructBean;
import com.cannyurl.af.bean.ShortPassageAttributeBean;
import com.cannyurl.af.bean.KeyValuePairStructBean;
import com.cannyurl.af.bean.KeyValueRelationStructBean;
import com.cannyurl.af.bean.ExternalServiceApiKeyStructBean;
import com.cannyurl.af.bean.ReferrerInfoStructBean;
import com.cannyurl.af.bean.TwitterCardProductDataBean;
import com.cannyurl.af.bean.PagerStateStructBean;

import com.cannyurl.af.bean.GeoLinkBean;
import com.cannyurl.af.bean.GeoCoordinateStructBean;
import com.cannyurl.af.bean.CellLatitudeLongitudeBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.GeoLinkService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class GeoLinkServiceImpl implements GeoLinkService
{
    private static final Logger log = Logger.getLogger(GeoLinkServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "GeoLink-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("GeoLink:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public GeoLinkServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // GeoLink related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public GeoLink getGeoLink(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getGeoLink(): guid = " + guid);

        GeoLinkBean bean = null;
        if(getCache() != null) {
            bean = (GeoLinkBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (GeoLinkBean) getProxyFactory().getGeoLinkServiceProxy().getGeoLink(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "GeoLinkBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve GeoLinkBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getGeoLink(String guid, String field) throws BaseException
    {
        GeoLinkBean bean = null;
        if(getCache() != null) {
            bean = (GeoLinkBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (GeoLinkBean) getProxyFactory().getGeoLinkServiceProxy().getGeoLink(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "GeoLinkBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve GeoLinkBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("shortLink")) {
            return bean.getShortLink();
        } else if(field.equals("shortUrl")) {
            return bean.getShortUrl();
        } else if(field.equals("geoCoordinate")) {
            return bean.getGeoCoordinate();
        } else if(field.equals("geoCell")) {
            return bean.getGeoCell();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<GeoLink> getGeoLinks(List<String> guids) throws BaseException
    {
        log.fine("getGeoLinks()");

        // TBD: Is there a better way????
        List<GeoLink> geoLinks = getProxyFactory().getGeoLinkServiceProxy().getGeoLinks(guids);
        if(geoLinks == null) {
            log.log(Level.WARNING, "Failed to retrieve GeoLinkBean list.");
        }

        log.finer("END");
        return geoLinks;
    }

    @Override
    public List<GeoLink> getAllGeoLinks() throws BaseException
    {
        return getAllGeoLinks(null, null, null);
    }

    @Override
    public List<GeoLink> getAllGeoLinks(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllGeoLinks(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<GeoLink> geoLinks = getProxyFactory().getGeoLinkServiceProxy().getAllGeoLinks(ordering, offset, count);
        if(geoLinks == null) {
            log.log(Level.WARNING, "Failed to retrieve GeoLinkBean list.");
        }

        log.finer("END");
        return geoLinks;
    }

    @Override
    public List<String> getAllGeoLinkKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllGeoLinkKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getGeoLinkServiceProxy().getAllGeoLinkKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve GeoLinkBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty GeoLinkBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<GeoLink> findGeoLinks(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findGeoLinks(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<GeoLink> findGeoLinks(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("GeoLinkServiceImpl.findGeoLinks(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<GeoLink> geoLinks = getProxyFactory().getGeoLinkServiceProxy().findGeoLinks(filter, ordering, params, values, grouping, unique, offset, count);
        if(geoLinks == null) {
            log.log(Level.WARNING, "Failed to find geoLinks for the given criterion.");
        }

        log.finer("END");
        return geoLinks;
    }

    @Override
    public List<String> findGeoLinkKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("GeoLinkServiceImpl.findGeoLinkKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getGeoLinkServiceProxy().findGeoLinkKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find GeoLink keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty GeoLink key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("GeoLinkServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getGeoLinkServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createGeoLink(String shortLink, String shortUrl, GeoCoordinateStruct geoCoordinate, CellLatitudeLongitude geoCell, String status) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        GeoCoordinateStructBean geoCoordinateBean = null;
        if(geoCoordinate instanceof GeoCoordinateStructBean) {
            geoCoordinateBean = (GeoCoordinateStructBean) geoCoordinate;
        } else if(geoCoordinate instanceof GeoCoordinateStruct) {
            geoCoordinateBean = new GeoCoordinateStructBean(geoCoordinate.getUuid(), geoCoordinate.getLatitude(), geoCoordinate.getLongitude(), geoCoordinate.getAltitude(), geoCoordinate.isSensorUsed(), geoCoordinate.getAccuracy(), geoCoordinate.getAltitudeAccuracy(), geoCoordinate.getHeading(), geoCoordinate.getSpeed(), geoCoordinate.getNote());
        } else {
            geoCoordinateBean = null;   // ????
        }
        CellLatitudeLongitudeBean geoCellBean = null;
        if(geoCell instanceof CellLatitudeLongitudeBean) {
            geoCellBean = (CellLatitudeLongitudeBean) geoCell;
        } else if(geoCell instanceof CellLatitudeLongitude) {
            geoCellBean = new CellLatitudeLongitudeBean(geoCell.getScale(), geoCell.getLatitude(), geoCell.getLongitude());
        } else {
            geoCellBean = null;   // ????
        }
        GeoLinkBean bean = new GeoLinkBean(null, shortLink, shortUrl, geoCoordinateBean, geoCellBean, status);
        return createGeoLink(bean);
    }

    @Override
    public String createGeoLink(GeoLink geoLink) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //GeoLink bean = constructGeoLink(geoLink);
        //return bean.getGuid();

        // Param geoLink cannot be null.....
        if(geoLink == null) {
            log.log(Level.INFO, "Param geoLink is null!");
            throw new BadRequestException("Param geoLink object is null!");
        }
        GeoLinkBean bean = null;
        if(geoLink instanceof GeoLinkBean) {
            bean = (GeoLinkBean) geoLink;
        } else if(geoLink instanceof GeoLink) {
            // bean = new GeoLinkBean(null, geoLink.getShortLink(), geoLink.getShortUrl(), (GeoCoordinateStructBean) geoLink.getGeoCoordinate(), (CellLatitudeLongitudeBean) geoLink.getGeoCell(), geoLink.getStatus());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new GeoLinkBean(geoLink.getGuid(), geoLink.getShortLink(), geoLink.getShortUrl(), (GeoCoordinateStructBean) geoLink.getGeoCoordinate(), (CellLatitudeLongitudeBean) geoLink.getGeoCell(), geoLink.getStatus());
        } else {
            log.log(Level.WARNING, "createGeoLink(): Arg geoLink is of an unknown type.");
            //bean = new GeoLinkBean();
            bean = new GeoLinkBean(geoLink.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getGeoLinkServiceProxy().createGeoLink(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

	    // TBD:
	    if(Config.getInstance().isSearchEnabled()) {
            if(guid != null && !guid.isEmpty()) {
                log.fine("Calling addDocument()... for GeoLink.");
                GeoLinkIndexBuilder builder = new GeoLinkIndexBuilder();
	    	    builder.addDocument(bean);
	    	}
	    }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public GeoLink constructGeoLink(GeoLink geoLink) throws BaseException
    {
        log.finer("BEGIN");

        // Param geoLink cannot be null.....
        if(geoLink == null) {
            log.log(Level.INFO, "Param geoLink is null!");
            throw new BadRequestException("Param geoLink object is null!");
        }
        GeoLinkBean bean = null;
        if(geoLink instanceof GeoLinkBean) {
            bean = (GeoLinkBean) geoLink;
        } else if(geoLink instanceof GeoLink) {
            // bean = new GeoLinkBean(null, geoLink.getShortLink(), geoLink.getShortUrl(), (GeoCoordinateStructBean) geoLink.getGeoCoordinate(), (CellLatitudeLongitudeBean) geoLink.getGeoCell(), geoLink.getStatus());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new GeoLinkBean(geoLink.getGuid(), geoLink.getShortLink(), geoLink.getShortUrl(), (GeoCoordinateStructBean) geoLink.getGeoCoordinate(), (CellLatitudeLongitudeBean) geoLink.getGeoCell(), geoLink.getStatus());
        } else {
            log.log(Level.WARNING, "createGeoLink(): Arg geoLink is of an unknown type.");
            //bean = new GeoLinkBean();
            bean = new GeoLinkBean(geoLink.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getGeoLinkServiceProxy().createGeoLink(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

	    // TBD:
	    if(Config.getInstance().isSearchEnabled()) {
            if(guid != null && !guid.isEmpty()) {
                log.fine("Calling addDocument()... for GeoLink.");
                GeoLinkIndexBuilder builder = new GeoLinkIndexBuilder();
	    	    builder.addDocument(bean);
	    	}
	    }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateGeoLink(String guid, String shortLink, String shortUrl, GeoCoordinateStruct geoCoordinate, CellLatitudeLongitude geoCell, String status) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        GeoCoordinateStructBean geoCoordinateBean = null;
        if(geoCoordinate instanceof GeoCoordinateStructBean) {
            geoCoordinateBean = (GeoCoordinateStructBean) geoCoordinate;
        } else if(geoCoordinate instanceof GeoCoordinateStruct) {
            geoCoordinateBean = new GeoCoordinateStructBean(geoCoordinate.getUuid(), geoCoordinate.getLatitude(), geoCoordinate.getLongitude(), geoCoordinate.getAltitude(), geoCoordinate.isSensorUsed(), geoCoordinate.getAccuracy(), geoCoordinate.getAltitudeAccuracy(), geoCoordinate.getHeading(), geoCoordinate.getSpeed(), geoCoordinate.getNote());
        } else {
            geoCoordinateBean = null;   // ????
        }
        CellLatitudeLongitudeBean geoCellBean = null;
        if(geoCell instanceof CellLatitudeLongitudeBean) {
            geoCellBean = (CellLatitudeLongitudeBean) geoCell;
        } else if(geoCell instanceof CellLatitudeLongitude) {
            geoCellBean = new CellLatitudeLongitudeBean(geoCell.getScale(), geoCell.getLatitude(), geoCell.getLongitude());
        } else {
            geoCellBean = null;   // ????
        }
        GeoLinkBean bean = new GeoLinkBean(guid, shortLink, shortUrl, geoCoordinateBean, geoCellBean, status);
        return updateGeoLink(bean);
    }
        
    @Override
    public Boolean updateGeoLink(GeoLink geoLink) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //GeoLink bean = refreshGeoLink(geoLink);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param geoLink cannot be null.....
        if(geoLink == null || geoLink.getGuid() == null) {
            log.log(Level.INFO, "Param geoLink or its guid is null!");
            throw new BadRequestException("Param geoLink object or its guid is null!");
        }
        GeoLinkBean bean = null;
        if(geoLink instanceof GeoLinkBean) {
            bean = (GeoLinkBean) geoLink;
        } else {  // if(geoLink instanceof GeoLink)
            bean = new GeoLinkBean(geoLink.getGuid(), geoLink.getShortLink(), geoLink.getShortUrl(), (GeoCoordinateStructBean) geoLink.getGeoCoordinate(), (CellLatitudeLongitudeBean) geoLink.getGeoCell(), geoLink.getStatus());
        }
        Boolean suc = getProxyFactory().getGeoLinkServiceProxy().updateGeoLink(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

	    // TBD:
        if(Config.getInstance().isSearchEnabled()) {
            if(suc != null && suc.equals(Boolean.TRUE)) {
                log.fine("Calling addDocument()... for GeoLink.");
	    	    GeoLinkIndexBuilder builder = new GeoLinkIndexBuilder();
	    	    builder.addDocument(bean);
	        }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public GeoLink refreshGeoLink(GeoLink geoLink) throws BaseException
    {
        log.finer("BEGIN");

        // Param geoLink cannot be null.....
        if(geoLink == null || geoLink.getGuid() == null) {
            log.log(Level.INFO, "Param geoLink or its guid is null!");
            throw new BadRequestException("Param geoLink object or its guid is null!");
        }
        GeoLinkBean bean = null;
        if(geoLink instanceof GeoLinkBean) {
            bean = (GeoLinkBean) geoLink;
        } else {  // if(geoLink instanceof GeoLink)
            bean = new GeoLinkBean(geoLink.getGuid(), geoLink.getShortLink(), geoLink.getShortUrl(), (GeoCoordinateStructBean) geoLink.getGeoCoordinate(), (CellLatitudeLongitudeBean) geoLink.getGeoCell(), geoLink.getStatus());
        }
        Boolean suc = getProxyFactory().getGeoLinkServiceProxy().updateGeoLink(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

	    // TBD:
        if(Config.getInstance().isSearchEnabled()) {
            if(suc != null && suc.equals(Boolean.TRUE)) {
                log.fine("Calling addDocument()... for GeoLink.");
	    	    GeoLinkIndexBuilder builder = new GeoLinkIndexBuilder();
	    	    builder.addDocument(bean);
	        }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteGeoLink(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getGeoLinkServiceProxy().deleteGeoLink(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }

        // TBD:
        if(Config.getInstance().isSearchEnabled()) {
            if(suc != null && suc.equals(Boolean.TRUE)) {
                log.fine("Calling removeDocument()... for GeoLink.");
	    	    GeoLinkIndexBuilder builder = new GeoLinkIndexBuilder();
                builder.removeDocument(guid);
	        }
        }

            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            GeoLink geoLink = null;
            try {
                geoLink = getProxyFactory().getGeoLinkServiceProxy().getGeoLink(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch geoLink with a key, " + guid);
                return false;
            }
            if(geoLink != null) {
                String beanGuid = geoLink.getGuid();
                Boolean suc1 = getProxyFactory().getGeoLinkServiceProxy().deleteGeoLink(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("geoLink with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteGeoLink(GeoLink geoLink) throws BaseException
    {
        log.finer("BEGIN");

        // Param geoLink cannot be null.....
        if(geoLink == null || geoLink.getGuid() == null) {
            log.log(Level.INFO, "Param geoLink or its guid is null!");
            throw new BadRequestException("Param geoLink object or its guid is null!");
        }
        GeoLinkBean bean = null;
        if(geoLink instanceof GeoLinkBean) {
            bean = (GeoLinkBean) geoLink;
        } else {  // if(geoLink instanceof GeoLink)
            // ????
            log.warning("geoLink is not an instance of GeoLinkBean.");
            bean = new GeoLinkBean(geoLink.getGuid(), geoLink.getShortLink(), geoLink.getShortUrl(), (GeoCoordinateStructBean) geoLink.getGeoCoordinate(), (CellLatitudeLongitudeBean) geoLink.getGeoCell(), geoLink.getStatus());
        }
        Boolean suc = getProxyFactory().getGeoLinkServiceProxy().deleteGeoLink(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

	    // TBD:
        if(Config.getInstance().isSearchEnabled()) {
            if(suc != null && suc.equals(Boolean.TRUE)) {
                log.fine("Calling removeDocument()... for GeoLink.");
	    	    GeoLinkIndexBuilder builder = new GeoLinkIndexBuilder();
                builder.removeDocument(bean.getGuid());
	        }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteGeoLinks(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getGeoLinkServiceProxy().deleteGeoLinks(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

	    // TBD:
        //if(Config.getInstance().isSearchEnabled()) {
        //    if(suc != null && suc.equals(Boolean.TRUE)) {
        //      log.fine("Calling removeDocument()... for GeoLink.");
	    //	    GeoLinkIndexBuilder builder = new GeoLinkIndexBuilder();
        //      builder.removeDocument(guids);
	    //    }
        //}

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createGeoLinks(List<GeoLink> geoLinks) throws BaseException
    {
        log.finer("BEGIN");

        if(geoLinks == null) {
            log.log(Level.WARNING, "createGeoLinks() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = geoLinks.size();
        if(size == 0) {
            log.log(Level.WARNING, "createGeoLinks() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(GeoLink geoLink : geoLinks) {
            String guid = createGeoLink(geoLink);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createGeoLinks() failed for at least one geoLink. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateGeoLinks(List<GeoLink> geoLinks) throws BaseException
    //{
    //}

}
