package com.cannyurl.af.service.impl;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.cannyurl.af.service.AbstractServiceFactory;
import com.cannyurl.af.service.ApiConsumerService;
import com.cannyurl.af.service.AppCustomDomainService;
import com.cannyurl.af.service.SiteCustomDomainService;
import com.cannyurl.af.service.UserService;
import com.cannyurl.af.service.UserUsercodeService;
import com.cannyurl.af.service.UserPasswordService;
import com.cannyurl.af.service.ExternalUserAuthService;
import com.cannyurl.af.service.UserAuthStateService;
import com.cannyurl.af.service.UserResourcePermissionService;
import com.cannyurl.af.service.UserResourceProhibitionService;
import com.cannyurl.af.service.RolePermissionService;
import com.cannyurl.af.service.UserRoleService;
import com.cannyurl.af.service.AppClientService;
import com.cannyurl.af.service.ClientUserService;
import com.cannyurl.af.service.UserCustomDomainService;
import com.cannyurl.af.service.ClientSettingService;
import com.cannyurl.af.service.UserSettingService;
import com.cannyurl.af.service.VisitorSettingService;
import com.cannyurl.af.service.TwitterSummaryCardService;
import com.cannyurl.af.service.TwitterPhotoCardService;
import com.cannyurl.af.service.TwitterGalleryCardService;
import com.cannyurl.af.service.TwitterAppCardService;
import com.cannyurl.af.service.TwitterPlayerCardService;
import com.cannyurl.af.service.TwitterProductCardService;
import com.cannyurl.af.service.ShortPassageService;
import com.cannyurl.af.service.ShortLinkService;
import com.cannyurl.af.service.GeoLinkService;
import com.cannyurl.af.service.QrCodeService;
import com.cannyurl.af.service.LinkPassphraseService;
import com.cannyurl.af.service.LinkMessageService;
import com.cannyurl.af.service.LinkAlbumService;
import com.cannyurl.af.service.AlbumShortLinkService;
import com.cannyurl.af.service.KeywordFolderService;
import com.cannyurl.af.service.BookmarkFolderService;
import com.cannyurl.af.service.KeywordLinkService;
import com.cannyurl.af.service.BookmarkLinkService;
import com.cannyurl.af.service.SpeedDialService;
import com.cannyurl.af.service.KeywordFolderImportService;
import com.cannyurl.af.service.BookmarkFolderImportService;
import com.cannyurl.af.service.KeywordCrowdTallyService;
import com.cannyurl.af.service.BookmarkCrowdTallyService;
import com.cannyurl.af.service.DomainInfoService;
import com.cannyurl.af.service.UrlRatingService;
import com.cannyurl.af.service.UserRatingService;
import com.cannyurl.af.service.AbuseTagService;
import com.cannyurl.af.service.ServiceInfoService;
import com.cannyurl.af.service.FiveTenService;

public class ImplServiceFactory extends AbstractServiceFactory
{
    private static final Logger log = Logger.getLogger(ImplServiceFactory.class.getName());

    private ImplServiceFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class ImplServiceFactoryHolder
    {
        private static final ImplServiceFactory INSTANCE = new ImplServiceFactory();
    }

    // Singleton method
    public static ImplServiceFactory getInstance()
    {
        return ImplServiceFactoryHolder.INSTANCE;
    }

    @Override
    public ApiConsumerService getApiConsumerService()
    {
        return new ApiConsumerServiceImpl();
    }

    @Override
    public AppCustomDomainService getAppCustomDomainService()
    {
        return new AppCustomDomainServiceImpl();
    }

    @Override
    public SiteCustomDomainService getSiteCustomDomainService()
    {
        return new SiteCustomDomainServiceImpl();
    }

    @Override
    public UserService getUserService()
    {
        return new UserServiceImpl();
    }

    @Override
    public UserUsercodeService getUserUsercodeService()
    {
        return new UserUsercodeServiceImpl();
    }

    @Override
    public UserPasswordService getUserPasswordService()
    {
        return new UserPasswordServiceImpl();
    }

    @Override
    public ExternalUserAuthService getExternalUserAuthService()
    {
        return new ExternalUserAuthServiceImpl();
    }

    @Override
    public UserAuthStateService getUserAuthStateService()
    {
        return new UserAuthStateServiceImpl();
    }

    @Override
    public UserResourcePermissionService getUserResourcePermissionService()
    {
        return new UserResourcePermissionServiceImpl();
    }

    @Override
    public UserResourceProhibitionService getUserResourceProhibitionService()
    {
        return new UserResourceProhibitionServiceImpl();
    }

    @Override
    public RolePermissionService getRolePermissionService()
    {
        return new RolePermissionServiceImpl();
    }

    @Override
    public UserRoleService getUserRoleService()
    {
        return new UserRoleServiceImpl();
    }

    @Override
    public AppClientService getAppClientService()
    {
        return new AppClientServiceImpl();
    }

    @Override
    public ClientUserService getClientUserService()
    {
        return new ClientUserServiceImpl();
    }

    @Override
    public UserCustomDomainService getUserCustomDomainService()
    {
        return new UserCustomDomainServiceImpl();
    }

    @Override
    public ClientSettingService getClientSettingService()
    {
        return new ClientSettingServiceImpl();
    }

    @Override
    public UserSettingService getUserSettingService()
    {
        return new UserSettingServiceImpl();
    }

    @Override
    public VisitorSettingService getVisitorSettingService()
    {
        return new VisitorSettingServiceImpl();
    }

    @Override
    public TwitterSummaryCardService getTwitterSummaryCardService()
    {
        return new TwitterSummaryCardServiceImpl();
    }

    @Override
    public TwitterPhotoCardService getTwitterPhotoCardService()
    {
        return new TwitterPhotoCardServiceImpl();
    }

    @Override
    public TwitterGalleryCardService getTwitterGalleryCardService()
    {
        return new TwitterGalleryCardServiceImpl();
    }

    @Override
    public TwitterAppCardService getTwitterAppCardService()
    {
        return new TwitterAppCardServiceImpl();
    }

    @Override
    public TwitterPlayerCardService getTwitterPlayerCardService()
    {
        return new TwitterPlayerCardServiceImpl();
    }

    @Override
    public TwitterProductCardService getTwitterProductCardService()
    {
        return new TwitterProductCardServiceImpl();
    }

    @Override
    public ShortPassageService getShortPassageService()
    {
        return new ShortPassageServiceImpl();
    }

    @Override
    public ShortLinkService getShortLinkService()
    {
        return new ShortLinkServiceImpl();
    }

    @Override
    public GeoLinkService getGeoLinkService()
    {
        return new GeoLinkServiceImpl();
    }

    @Override
    public QrCodeService getQrCodeService()
    {
        return new QrCodeServiceImpl();
    }

    @Override
    public LinkPassphraseService getLinkPassphraseService()
    {
        return new LinkPassphraseServiceImpl();
    }

    @Override
    public LinkMessageService getLinkMessageService()
    {
        return new LinkMessageServiceImpl();
    }

    @Override
    public LinkAlbumService getLinkAlbumService()
    {
        return new LinkAlbumServiceImpl();
    }

    @Override
    public AlbumShortLinkService getAlbumShortLinkService()
    {
        return new AlbumShortLinkServiceImpl();
    }

    @Override
    public KeywordFolderService getKeywordFolderService()
    {
        return new KeywordFolderServiceImpl();
    }

    @Override
    public BookmarkFolderService getBookmarkFolderService()
    {
        return new BookmarkFolderServiceImpl();
    }

    @Override
    public KeywordLinkService getKeywordLinkService()
    {
        return new KeywordLinkServiceImpl();
    }

    @Override
    public BookmarkLinkService getBookmarkLinkService()
    {
        return new BookmarkLinkServiceImpl();
    }

    @Override
    public SpeedDialService getSpeedDialService()
    {
        return new SpeedDialServiceImpl();
    }

    @Override
    public KeywordFolderImportService getKeywordFolderImportService()
    {
        return new KeywordFolderImportServiceImpl();
    }

    @Override
    public BookmarkFolderImportService getBookmarkFolderImportService()
    {
        return new BookmarkFolderImportServiceImpl();
    }

    @Override
    public KeywordCrowdTallyService getKeywordCrowdTallyService()
    {
        return new KeywordCrowdTallyServiceImpl();
    }

    @Override
    public BookmarkCrowdTallyService getBookmarkCrowdTallyService()
    {
        return new BookmarkCrowdTallyServiceImpl();
    }

    @Override
    public DomainInfoService getDomainInfoService()
    {
        return new DomainInfoServiceImpl();
    }

    @Override
    public UrlRatingService getUrlRatingService()
    {
        return new UrlRatingServiceImpl();
    }

    @Override
    public UserRatingService getUserRatingService()
    {
        return new UserRatingServiceImpl();
    }

    @Override
    public AbuseTagService getAbuseTagService()
    {
        return new AbuseTagServiceImpl();
    }

    @Override
    public ServiceInfoService getServiceInfoService()
    {
        return new ServiceInfoServiceImpl();
    }

    @Override
    public FiveTenService getFiveTenService()
    {
        return new FiveTenServiceImpl();
    }


}
