package com.cannyurl.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.KeywordCrowdTally;
import com.myurldb.ws.search.gae.KeywordCrowdTallyIndexBuilder;
import com.cannyurl.af.config.Config;

import com.cannyurl.af.bean.HashedPasswordStructBean;
import com.cannyurl.af.bean.GeoPointStructBean;
import com.cannyurl.af.bean.WebProfileStructBean;
import com.cannyurl.af.bean.StreetAddressStructBean;
import com.cannyurl.af.bean.SiteLogBean;
import com.cannyurl.af.bean.HelpNoticeBean;
import com.cannyurl.af.bean.TwitterCardAppInfoBean;
import com.cannyurl.af.bean.ContactInfoStructBean;
import com.cannyurl.af.bean.GeoCoordinateStructBean;
import com.cannyurl.af.bean.FullNameStructBean;
import com.cannyurl.af.bean.ExternalUserIdStructBean;
import com.cannyurl.af.bean.GaeUserStructBean;
import com.cannyurl.af.bean.AppBrandStructBean;
import com.cannyurl.af.bean.CellLatitudeLongitudeBean;
import com.cannyurl.af.bean.GaeAppStructBean;
import com.cannyurl.af.bean.ShortPassageAttributeBean;
import com.cannyurl.af.bean.KeyValuePairStructBean;
import com.cannyurl.af.bean.KeyValueRelationStructBean;
import com.cannyurl.af.bean.ExternalServiceApiKeyStructBean;
import com.cannyurl.af.bean.ReferrerInfoStructBean;
import com.cannyurl.af.bean.TwitterCardProductDataBean;
import com.cannyurl.af.bean.PagerStateStructBean;

import com.cannyurl.af.bean.KeywordCrowdTallyBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.KeywordCrowdTallyService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class KeywordCrowdTallyServiceImpl implements KeywordCrowdTallyService
{
    private static final Logger log = Logger.getLogger(KeywordCrowdTallyServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "KeywordCrowdTally-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("KeywordCrowdTally:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public KeywordCrowdTallyServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // KeywordCrowdTally related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public KeywordCrowdTally getKeywordCrowdTally(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getKeywordCrowdTally(): guid = " + guid);

        KeywordCrowdTallyBean bean = null;
        if(getCache() != null) {
            bean = (KeywordCrowdTallyBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (KeywordCrowdTallyBean) getProxyFactory().getKeywordCrowdTallyServiceProxy().getKeywordCrowdTally(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "KeywordCrowdTallyBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve KeywordCrowdTallyBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getKeywordCrowdTally(String guid, String field) throws BaseException
    {
        KeywordCrowdTallyBean bean = null;
        if(getCache() != null) {
            bean = (KeywordCrowdTallyBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (KeywordCrowdTallyBean) getProxyFactory().getKeywordCrowdTallyServiceProxy().getKeywordCrowdTally(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "KeywordCrowdTallyBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve KeywordCrowdTallyBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return bean.getUser();
        } else if(field.equals("shortLink")) {
            return bean.getShortLink();
        } else if(field.equals("domain")) {
            return bean.getDomain();
        } else if(field.equals("token")) {
            return bean.getToken();
        } else if(field.equals("longUrl")) {
            return bean.getLongUrl();
        } else if(field.equals("shortUrl")) {
            return bean.getShortUrl();
        } else if(field.equals("tallyDate")) {
            return bean.getTallyDate();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("note")) {
            return bean.getNote();
        } else if(field.equals("expirationTime")) {
            return bean.getExpirationTime();
        } else if(field.equals("keywordFolder")) {
            return bean.getKeywordFolder();
        } else if(field.equals("folderPath")) {
            return bean.getFolderPath();
        } else if(field.equals("keyword")) {
            return bean.getKeyword();
        } else if(field.equals("queryKey")) {
            return bean.getQueryKey();
        } else if(field.equals("scope")) {
            return bean.getScope();
        } else if(field.equals("caseSensitive")) {
            return bean.isCaseSensitive();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<KeywordCrowdTally> getKeywordCrowdTallies(List<String> guids) throws BaseException
    {
        log.fine("getKeywordCrowdTallies()");

        // TBD: Is there a better way????
        List<KeywordCrowdTally> keywordCrowdTallies = getProxyFactory().getKeywordCrowdTallyServiceProxy().getKeywordCrowdTallies(guids);
        if(keywordCrowdTallies == null) {
            log.log(Level.WARNING, "Failed to retrieve KeywordCrowdTallyBean list.");
        }

        log.finer("END");
        return keywordCrowdTallies;
    }

    @Override
    public List<KeywordCrowdTally> getAllKeywordCrowdTallies() throws BaseException
    {
        return getAllKeywordCrowdTallies(null, null, null);
    }

    @Override
    public List<KeywordCrowdTally> getAllKeywordCrowdTallies(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllKeywordCrowdTallies(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<KeywordCrowdTally> keywordCrowdTallies = getProxyFactory().getKeywordCrowdTallyServiceProxy().getAllKeywordCrowdTallies(ordering, offset, count);
        if(keywordCrowdTallies == null) {
            log.log(Level.WARNING, "Failed to retrieve KeywordCrowdTallyBean list.");
        }

        log.finer("END");
        return keywordCrowdTallies;
    }

    @Override
    public List<String> getAllKeywordCrowdTallyKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllKeywordCrowdTallyKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getKeywordCrowdTallyServiceProxy().getAllKeywordCrowdTallyKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve KeywordCrowdTallyBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty KeywordCrowdTallyBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<KeywordCrowdTally> findKeywordCrowdTallies(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findKeywordCrowdTallies(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<KeywordCrowdTally> findKeywordCrowdTallies(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("KeywordCrowdTallyServiceImpl.findKeywordCrowdTallies(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<KeywordCrowdTally> keywordCrowdTallies = getProxyFactory().getKeywordCrowdTallyServiceProxy().findKeywordCrowdTallies(filter, ordering, params, values, grouping, unique, offset, count);
        if(keywordCrowdTallies == null) {
            log.log(Level.WARNING, "Failed to find keywordCrowdTallies for the given criterion.");
        }

        log.finer("END");
        return keywordCrowdTallies;
    }

    @Override
    public List<String> findKeywordCrowdTallyKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("KeywordCrowdTallyServiceImpl.findKeywordCrowdTallyKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getKeywordCrowdTallyServiceProxy().findKeywordCrowdTallyKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find KeywordCrowdTally keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty KeywordCrowdTally key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("KeywordCrowdTallyServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getKeywordCrowdTallyServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createKeywordCrowdTally(String user, String shortLink, String domain, String token, String longUrl, String shortUrl, String tallyDate, String status, String note, Long expirationTime, String keywordFolder, String folderPath, String keyword, String queryKey, String scope, Boolean caseSensitive) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        KeywordCrowdTallyBean bean = new KeywordCrowdTallyBean(null, user, shortLink, domain, token, longUrl, shortUrl, tallyDate, status, note, expirationTime, keywordFolder, folderPath, keyword, queryKey, scope, caseSensitive);
        return createKeywordCrowdTally(bean);
    }

    @Override
    public String createKeywordCrowdTally(KeywordCrowdTally keywordCrowdTally) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //KeywordCrowdTally bean = constructKeywordCrowdTally(keywordCrowdTally);
        //return bean.getGuid();

        // Param keywordCrowdTally cannot be null.....
        if(keywordCrowdTally == null) {
            log.log(Level.INFO, "Param keywordCrowdTally is null!");
            throw new BadRequestException("Param keywordCrowdTally object is null!");
        }
        KeywordCrowdTallyBean bean = null;
        if(keywordCrowdTally instanceof KeywordCrowdTallyBean) {
            bean = (KeywordCrowdTallyBean) keywordCrowdTally;
        } else if(keywordCrowdTally instanceof KeywordCrowdTally) {
            // bean = new KeywordCrowdTallyBean(null, keywordCrowdTally.getUser(), keywordCrowdTally.getShortLink(), keywordCrowdTally.getDomain(), keywordCrowdTally.getToken(), keywordCrowdTally.getLongUrl(), keywordCrowdTally.getShortUrl(), keywordCrowdTally.getTallyDate(), keywordCrowdTally.getStatus(), keywordCrowdTally.getNote(), keywordCrowdTally.getExpirationTime(), keywordCrowdTally.getKeywordFolder(), keywordCrowdTally.getFolderPath(), keywordCrowdTally.getKeyword(), keywordCrowdTally.getQueryKey(), keywordCrowdTally.getScope(), keywordCrowdTally.isCaseSensitive());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new KeywordCrowdTallyBean(keywordCrowdTally.getGuid(), keywordCrowdTally.getUser(), keywordCrowdTally.getShortLink(), keywordCrowdTally.getDomain(), keywordCrowdTally.getToken(), keywordCrowdTally.getLongUrl(), keywordCrowdTally.getShortUrl(), keywordCrowdTally.getTallyDate(), keywordCrowdTally.getStatus(), keywordCrowdTally.getNote(), keywordCrowdTally.getExpirationTime(), keywordCrowdTally.getKeywordFolder(), keywordCrowdTally.getFolderPath(), keywordCrowdTally.getKeyword(), keywordCrowdTally.getQueryKey(), keywordCrowdTally.getScope(), keywordCrowdTally.isCaseSensitive());
        } else {
            log.log(Level.WARNING, "createKeywordCrowdTally(): Arg keywordCrowdTally is of an unknown type.");
            //bean = new KeywordCrowdTallyBean();
            bean = new KeywordCrowdTallyBean(keywordCrowdTally.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getKeywordCrowdTallyServiceProxy().createKeywordCrowdTally(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

	    // TBD:
	    if(Config.getInstance().isSearchEnabled()) {
            if(guid != null && !guid.isEmpty()) {
                log.fine("Calling addDocument()... for KeywordCrowdTally.");
                KeywordCrowdTallyIndexBuilder builder = new KeywordCrowdTallyIndexBuilder();
	    	    builder.addDocument(bean);
	    	}
	    }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public KeywordCrowdTally constructKeywordCrowdTally(KeywordCrowdTally keywordCrowdTally) throws BaseException
    {
        log.finer("BEGIN");

        // Param keywordCrowdTally cannot be null.....
        if(keywordCrowdTally == null) {
            log.log(Level.INFO, "Param keywordCrowdTally is null!");
            throw new BadRequestException("Param keywordCrowdTally object is null!");
        }
        KeywordCrowdTallyBean bean = null;
        if(keywordCrowdTally instanceof KeywordCrowdTallyBean) {
            bean = (KeywordCrowdTallyBean) keywordCrowdTally;
        } else if(keywordCrowdTally instanceof KeywordCrowdTally) {
            // bean = new KeywordCrowdTallyBean(null, keywordCrowdTally.getUser(), keywordCrowdTally.getShortLink(), keywordCrowdTally.getDomain(), keywordCrowdTally.getToken(), keywordCrowdTally.getLongUrl(), keywordCrowdTally.getShortUrl(), keywordCrowdTally.getTallyDate(), keywordCrowdTally.getStatus(), keywordCrowdTally.getNote(), keywordCrowdTally.getExpirationTime(), keywordCrowdTally.getKeywordFolder(), keywordCrowdTally.getFolderPath(), keywordCrowdTally.getKeyword(), keywordCrowdTally.getQueryKey(), keywordCrowdTally.getScope(), keywordCrowdTally.isCaseSensitive());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new KeywordCrowdTallyBean(keywordCrowdTally.getGuid(), keywordCrowdTally.getUser(), keywordCrowdTally.getShortLink(), keywordCrowdTally.getDomain(), keywordCrowdTally.getToken(), keywordCrowdTally.getLongUrl(), keywordCrowdTally.getShortUrl(), keywordCrowdTally.getTallyDate(), keywordCrowdTally.getStatus(), keywordCrowdTally.getNote(), keywordCrowdTally.getExpirationTime(), keywordCrowdTally.getKeywordFolder(), keywordCrowdTally.getFolderPath(), keywordCrowdTally.getKeyword(), keywordCrowdTally.getQueryKey(), keywordCrowdTally.getScope(), keywordCrowdTally.isCaseSensitive());
        } else {
            log.log(Level.WARNING, "createKeywordCrowdTally(): Arg keywordCrowdTally is of an unknown type.");
            //bean = new KeywordCrowdTallyBean();
            bean = new KeywordCrowdTallyBean(keywordCrowdTally.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getKeywordCrowdTallyServiceProxy().createKeywordCrowdTally(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

	    // TBD:
	    if(Config.getInstance().isSearchEnabled()) {
            if(guid != null && !guid.isEmpty()) {
                log.fine("Calling addDocument()... for KeywordCrowdTally.");
                KeywordCrowdTallyIndexBuilder builder = new KeywordCrowdTallyIndexBuilder();
	    	    builder.addDocument(bean);
	    	}
	    }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateKeywordCrowdTally(String guid, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, String tallyDate, String status, String note, Long expirationTime, String keywordFolder, String folderPath, String keyword, String queryKey, String scope, Boolean caseSensitive) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        KeywordCrowdTallyBean bean = new KeywordCrowdTallyBean(guid, user, shortLink, domain, token, longUrl, shortUrl, tallyDate, status, note, expirationTime, keywordFolder, folderPath, keyword, queryKey, scope, caseSensitive);
        return updateKeywordCrowdTally(bean);
    }
        
    @Override
    public Boolean updateKeywordCrowdTally(KeywordCrowdTally keywordCrowdTally) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //KeywordCrowdTally bean = refreshKeywordCrowdTally(keywordCrowdTally);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param keywordCrowdTally cannot be null.....
        if(keywordCrowdTally == null || keywordCrowdTally.getGuid() == null) {
            log.log(Level.INFO, "Param keywordCrowdTally or its guid is null!");
            throw new BadRequestException("Param keywordCrowdTally object or its guid is null!");
        }
        KeywordCrowdTallyBean bean = null;
        if(keywordCrowdTally instanceof KeywordCrowdTallyBean) {
            bean = (KeywordCrowdTallyBean) keywordCrowdTally;
        } else {  // if(keywordCrowdTally instanceof KeywordCrowdTally)
            bean = new KeywordCrowdTallyBean(keywordCrowdTally.getGuid(), keywordCrowdTally.getUser(), keywordCrowdTally.getShortLink(), keywordCrowdTally.getDomain(), keywordCrowdTally.getToken(), keywordCrowdTally.getLongUrl(), keywordCrowdTally.getShortUrl(), keywordCrowdTally.getTallyDate(), keywordCrowdTally.getStatus(), keywordCrowdTally.getNote(), keywordCrowdTally.getExpirationTime(), keywordCrowdTally.getKeywordFolder(), keywordCrowdTally.getFolderPath(), keywordCrowdTally.getKeyword(), keywordCrowdTally.getQueryKey(), keywordCrowdTally.getScope(), keywordCrowdTally.isCaseSensitive());
        }
        Boolean suc = getProxyFactory().getKeywordCrowdTallyServiceProxy().updateKeywordCrowdTally(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

	    // TBD:
        if(Config.getInstance().isSearchEnabled()) {
            if(suc != null && suc.equals(Boolean.TRUE)) {
                log.fine("Calling addDocument()... for KeywordCrowdTally.");
	    	    KeywordCrowdTallyIndexBuilder builder = new KeywordCrowdTallyIndexBuilder();
	    	    builder.addDocument(bean);
	        }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public KeywordCrowdTally refreshKeywordCrowdTally(KeywordCrowdTally keywordCrowdTally) throws BaseException
    {
        log.finer("BEGIN");

        // Param keywordCrowdTally cannot be null.....
        if(keywordCrowdTally == null || keywordCrowdTally.getGuid() == null) {
            log.log(Level.INFO, "Param keywordCrowdTally or its guid is null!");
            throw new BadRequestException("Param keywordCrowdTally object or its guid is null!");
        }
        KeywordCrowdTallyBean bean = null;
        if(keywordCrowdTally instanceof KeywordCrowdTallyBean) {
            bean = (KeywordCrowdTallyBean) keywordCrowdTally;
        } else {  // if(keywordCrowdTally instanceof KeywordCrowdTally)
            bean = new KeywordCrowdTallyBean(keywordCrowdTally.getGuid(), keywordCrowdTally.getUser(), keywordCrowdTally.getShortLink(), keywordCrowdTally.getDomain(), keywordCrowdTally.getToken(), keywordCrowdTally.getLongUrl(), keywordCrowdTally.getShortUrl(), keywordCrowdTally.getTallyDate(), keywordCrowdTally.getStatus(), keywordCrowdTally.getNote(), keywordCrowdTally.getExpirationTime(), keywordCrowdTally.getKeywordFolder(), keywordCrowdTally.getFolderPath(), keywordCrowdTally.getKeyword(), keywordCrowdTally.getQueryKey(), keywordCrowdTally.getScope(), keywordCrowdTally.isCaseSensitive());
        }
        Boolean suc = getProxyFactory().getKeywordCrowdTallyServiceProxy().updateKeywordCrowdTally(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

	    // TBD:
        if(Config.getInstance().isSearchEnabled()) {
            if(suc != null && suc.equals(Boolean.TRUE)) {
                log.fine("Calling addDocument()... for KeywordCrowdTally.");
	    	    KeywordCrowdTallyIndexBuilder builder = new KeywordCrowdTallyIndexBuilder();
	    	    builder.addDocument(bean);
	        }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteKeywordCrowdTally(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getKeywordCrowdTallyServiceProxy().deleteKeywordCrowdTally(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }

        // TBD:
        if(Config.getInstance().isSearchEnabled()) {
            if(suc != null && suc.equals(Boolean.TRUE)) {
                log.fine("Calling removeDocument()... for KeywordCrowdTally.");
	    	    KeywordCrowdTallyIndexBuilder builder = new KeywordCrowdTallyIndexBuilder();
                builder.removeDocument(guid);
	        }
        }

            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            KeywordCrowdTally keywordCrowdTally = null;
            try {
                keywordCrowdTally = getProxyFactory().getKeywordCrowdTallyServiceProxy().getKeywordCrowdTally(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch keywordCrowdTally with a key, " + guid);
                return false;
            }
            if(keywordCrowdTally != null) {
                String beanGuid = keywordCrowdTally.getGuid();
                Boolean suc1 = getProxyFactory().getKeywordCrowdTallyServiceProxy().deleteKeywordCrowdTally(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("keywordCrowdTally with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteKeywordCrowdTally(KeywordCrowdTally keywordCrowdTally) throws BaseException
    {
        log.finer("BEGIN");

        // Param keywordCrowdTally cannot be null.....
        if(keywordCrowdTally == null || keywordCrowdTally.getGuid() == null) {
            log.log(Level.INFO, "Param keywordCrowdTally or its guid is null!");
            throw new BadRequestException("Param keywordCrowdTally object or its guid is null!");
        }
        KeywordCrowdTallyBean bean = null;
        if(keywordCrowdTally instanceof KeywordCrowdTallyBean) {
            bean = (KeywordCrowdTallyBean) keywordCrowdTally;
        } else {  // if(keywordCrowdTally instanceof KeywordCrowdTally)
            // ????
            log.warning("keywordCrowdTally is not an instance of KeywordCrowdTallyBean.");
            bean = new KeywordCrowdTallyBean(keywordCrowdTally.getGuid(), keywordCrowdTally.getUser(), keywordCrowdTally.getShortLink(), keywordCrowdTally.getDomain(), keywordCrowdTally.getToken(), keywordCrowdTally.getLongUrl(), keywordCrowdTally.getShortUrl(), keywordCrowdTally.getTallyDate(), keywordCrowdTally.getStatus(), keywordCrowdTally.getNote(), keywordCrowdTally.getExpirationTime(), keywordCrowdTally.getKeywordFolder(), keywordCrowdTally.getFolderPath(), keywordCrowdTally.getKeyword(), keywordCrowdTally.getQueryKey(), keywordCrowdTally.getScope(), keywordCrowdTally.isCaseSensitive());
        }
        Boolean suc = getProxyFactory().getKeywordCrowdTallyServiceProxy().deleteKeywordCrowdTally(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

	    // TBD:
        if(Config.getInstance().isSearchEnabled()) {
            if(suc != null && suc.equals(Boolean.TRUE)) {
                log.fine("Calling removeDocument()... for KeywordCrowdTally.");
	    	    KeywordCrowdTallyIndexBuilder builder = new KeywordCrowdTallyIndexBuilder();
                builder.removeDocument(bean.getGuid());
	        }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteKeywordCrowdTallies(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getKeywordCrowdTallyServiceProxy().deleteKeywordCrowdTallies(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

	    // TBD:
        //if(Config.getInstance().isSearchEnabled()) {
        //    if(suc != null && suc.equals(Boolean.TRUE)) {
        //      log.fine("Calling removeDocument()... for KeywordCrowdTally.");
	    //	    KeywordCrowdTallyIndexBuilder builder = new KeywordCrowdTallyIndexBuilder();
        //      builder.removeDocument(guids);
	    //    }
        //}

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createKeywordCrowdTallies(List<KeywordCrowdTally> keywordCrowdTallies) throws BaseException
    {
        log.finer("BEGIN");

        if(keywordCrowdTallies == null) {
            log.log(Level.WARNING, "createKeywordCrowdTallies() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = keywordCrowdTallies.size();
        if(size == 0) {
            log.log(Level.WARNING, "createKeywordCrowdTallies() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(KeywordCrowdTally keywordCrowdTally : keywordCrowdTallies) {
            String guid = createKeywordCrowdTally(keywordCrowdTally);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createKeywordCrowdTallies() failed for at least one keywordCrowdTally. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateKeywordCrowdTallies(List<KeywordCrowdTally> keywordCrowdTallies) throws BaseException
    //{
    //}

}
