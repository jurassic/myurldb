package com.cannyurl.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.KeywordFolder;
import com.myurldb.ws.search.gae.KeywordFolderIndexBuilder;
import com.cannyurl.af.config.Config;

import com.cannyurl.af.bean.HashedPasswordStructBean;
import com.cannyurl.af.bean.GeoPointStructBean;
import com.cannyurl.af.bean.WebProfileStructBean;
import com.cannyurl.af.bean.StreetAddressStructBean;
import com.cannyurl.af.bean.SiteLogBean;
import com.cannyurl.af.bean.HelpNoticeBean;
import com.cannyurl.af.bean.TwitterCardAppInfoBean;
import com.cannyurl.af.bean.ContactInfoStructBean;
import com.cannyurl.af.bean.GeoCoordinateStructBean;
import com.cannyurl.af.bean.FullNameStructBean;
import com.cannyurl.af.bean.ExternalUserIdStructBean;
import com.cannyurl.af.bean.GaeUserStructBean;
import com.cannyurl.af.bean.AppBrandStructBean;
import com.cannyurl.af.bean.CellLatitudeLongitudeBean;
import com.cannyurl.af.bean.GaeAppStructBean;
import com.cannyurl.af.bean.ShortPassageAttributeBean;
import com.cannyurl.af.bean.KeyValuePairStructBean;
import com.cannyurl.af.bean.KeyValueRelationStructBean;
import com.cannyurl.af.bean.ExternalServiceApiKeyStructBean;
import com.cannyurl.af.bean.ReferrerInfoStructBean;
import com.cannyurl.af.bean.TwitterCardProductDataBean;
import com.cannyurl.af.bean.PagerStateStructBean;

import com.cannyurl.af.bean.KeywordFolderBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.KeywordFolderService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class KeywordFolderServiceImpl implements KeywordFolderService
{
    private static final Logger log = Logger.getLogger(KeywordFolderServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "KeywordFolder-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("KeywordFolder:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public KeywordFolderServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // KeywordFolder related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public KeywordFolder getKeywordFolder(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getKeywordFolder(): guid = " + guid);

        KeywordFolderBean bean = null;
        if(getCache() != null) {
            bean = (KeywordFolderBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (KeywordFolderBean) getProxyFactory().getKeywordFolderServiceProxy().getKeywordFolder(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "KeywordFolderBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve KeywordFolderBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getKeywordFolder(String guid, String field) throws BaseException
    {
        KeywordFolderBean bean = null;
        if(getCache() != null) {
            bean = (KeywordFolderBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (KeywordFolderBean) getProxyFactory().getKeywordFolderServiceProxy().getKeywordFolder(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "KeywordFolderBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve KeywordFolderBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("appClient")) {
            return bean.getAppClient();
        } else if(field.equals("clientRootDomain")) {
            return bean.getClientRootDomain();
        } else if(field.equals("user")) {
            return bean.getUser();
        } else if(field.equals("title")) {
            return bean.getTitle();
        } else if(field.equals("description")) {
            return bean.getDescription();
        } else if(field.equals("type")) {
            return bean.getType();
        } else if(field.equals("category")) {
            return bean.getCategory();
        } else if(field.equals("parent")) {
            return bean.getParent();
        } else if(field.equals("aggregate")) {
            return bean.getAggregate();
        } else if(field.equals("acl")) {
            return bean.getAcl();
        } else if(field.equals("exportable")) {
            return bean.isExportable();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("note")) {
            return bean.getNote();
        } else if(field.equals("folderPath")) {
            return bean.getFolderPath();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<KeywordFolder> getKeywordFolders(List<String> guids) throws BaseException
    {
        log.fine("getKeywordFolders()");

        // TBD: Is there a better way????
        List<KeywordFolder> keywordFolders = getProxyFactory().getKeywordFolderServiceProxy().getKeywordFolders(guids);
        if(keywordFolders == null) {
            log.log(Level.WARNING, "Failed to retrieve KeywordFolderBean list.");
        }

        log.finer("END");
        return keywordFolders;
    }

    @Override
    public List<KeywordFolder> getAllKeywordFolders() throws BaseException
    {
        return getAllKeywordFolders(null, null, null);
    }

    @Override
    public List<KeywordFolder> getAllKeywordFolders(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllKeywordFolders(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<KeywordFolder> keywordFolders = getProxyFactory().getKeywordFolderServiceProxy().getAllKeywordFolders(ordering, offset, count);
        if(keywordFolders == null) {
            log.log(Level.WARNING, "Failed to retrieve KeywordFolderBean list.");
        }

        log.finer("END");
        return keywordFolders;
    }

    @Override
    public List<String> getAllKeywordFolderKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllKeywordFolderKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getKeywordFolderServiceProxy().getAllKeywordFolderKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve KeywordFolderBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty KeywordFolderBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<KeywordFolder> findKeywordFolders(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findKeywordFolders(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<KeywordFolder> findKeywordFolders(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("KeywordFolderServiceImpl.findKeywordFolders(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<KeywordFolder> keywordFolders = getProxyFactory().getKeywordFolderServiceProxy().findKeywordFolders(filter, ordering, params, values, grouping, unique, offset, count);
        if(keywordFolders == null) {
            log.log(Level.WARNING, "Failed to find keywordFolders for the given criterion.");
        }

        log.finer("END");
        return keywordFolders;
    }

    @Override
    public List<String> findKeywordFolderKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("KeywordFolderServiceImpl.findKeywordFolderKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getKeywordFolderServiceProxy().findKeywordFolderKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find KeywordFolder keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty KeywordFolder key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("KeywordFolderServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getKeywordFolderServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createKeywordFolder(String appClient, String clientRootDomain, String user, String title, String description, String type, String category, String parent, String aggregate, String acl, Boolean exportable, String status, String note, String folderPath) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        KeywordFolderBean bean = new KeywordFolderBean(null, appClient, clientRootDomain, user, title, description, type, category, parent, aggregate, acl, exportable, status, note, folderPath);
        return createKeywordFolder(bean);
    }

    @Override
    public String createKeywordFolder(KeywordFolder keywordFolder) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //KeywordFolder bean = constructKeywordFolder(keywordFolder);
        //return bean.getGuid();

        // Param keywordFolder cannot be null.....
        if(keywordFolder == null) {
            log.log(Level.INFO, "Param keywordFolder is null!");
            throw new BadRequestException("Param keywordFolder object is null!");
        }
        KeywordFolderBean bean = null;
        if(keywordFolder instanceof KeywordFolderBean) {
            bean = (KeywordFolderBean) keywordFolder;
        } else if(keywordFolder instanceof KeywordFolder) {
            // bean = new KeywordFolderBean(null, keywordFolder.getAppClient(), keywordFolder.getClientRootDomain(), keywordFolder.getUser(), keywordFolder.getTitle(), keywordFolder.getDescription(), keywordFolder.getType(), keywordFolder.getCategory(), keywordFolder.getParent(), keywordFolder.getAggregate(), keywordFolder.getAcl(), keywordFolder.isExportable(), keywordFolder.getStatus(), keywordFolder.getNote(), keywordFolder.getFolderPath());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new KeywordFolderBean(keywordFolder.getGuid(), keywordFolder.getAppClient(), keywordFolder.getClientRootDomain(), keywordFolder.getUser(), keywordFolder.getTitle(), keywordFolder.getDescription(), keywordFolder.getType(), keywordFolder.getCategory(), keywordFolder.getParent(), keywordFolder.getAggregate(), keywordFolder.getAcl(), keywordFolder.isExportable(), keywordFolder.getStatus(), keywordFolder.getNote(), keywordFolder.getFolderPath());
        } else {
            log.log(Level.WARNING, "createKeywordFolder(): Arg keywordFolder is of an unknown type.");
            //bean = new KeywordFolderBean();
            bean = new KeywordFolderBean(keywordFolder.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getKeywordFolderServiceProxy().createKeywordFolder(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

	    // TBD:
	    if(Config.getInstance().isSearchEnabled()) {
            if(guid != null && !guid.isEmpty()) {
                log.fine("Calling addDocument()... for KeywordFolder.");
                KeywordFolderIndexBuilder builder = new KeywordFolderIndexBuilder();
	    	    builder.addDocument(bean);
	    	}
	    }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public KeywordFolder constructKeywordFolder(KeywordFolder keywordFolder) throws BaseException
    {
        log.finer("BEGIN");

        // Param keywordFolder cannot be null.....
        if(keywordFolder == null) {
            log.log(Level.INFO, "Param keywordFolder is null!");
            throw new BadRequestException("Param keywordFolder object is null!");
        }
        KeywordFolderBean bean = null;
        if(keywordFolder instanceof KeywordFolderBean) {
            bean = (KeywordFolderBean) keywordFolder;
        } else if(keywordFolder instanceof KeywordFolder) {
            // bean = new KeywordFolderBean(null, keywordFolder.getAppClient(), keywordFolder.getClientRootDomain(), keywordFolder.getUser(), keywordFolder.getTitle(), keywordFolder.getDescription(), keywordFolder.getType(), keywordFolder.getCategory(), keywordFolder.getParent(), keywordFolder.getAggregate(), keywordFolder.getAcl(), keywordFolder.isExportable(), keywordFolder.getStatus(), keywordFolder.getNote(), keywordFolder.getFolderPath());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new KeywordFolderBean(keywordFolder.getGuid(), keywordFolder.getAppClient(), keywordFolder.getClientRootDomain(), keywordFolder.getUser(), keywordFolder.getTitle(), keywordFolder.getDescription(), keywordFolder.getType(), keywordFolder.getCategory(), keywordFolder.getParent(), keywordFolder.getAggregate(), keywordFolder.getAcl(), keywordFolder.isExportable(), keywordFolder.getStatus(), keywordFolder.getNote(), keywordFolder.getFolderPath());
        } else {
            log.log(Level.WARNING, "createKeywordFolder(): Arg keywordFolder is of an unknown type.");
            //bean = new KeywordFolderBean();
            bean = new KeywordFolderBean(keywordFolder.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getKeywordFolderServiceProxy().createKeywordFolder(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

	    // TBD:
	    if(Config.getInstance().isSearchEnabled()) {
            if(guid != null && !guid.isEmpty()) {
                log.fine("Calling addDocument()... for KeywordFolder.");
                KeywordFolderIndexBuilder builder = new KeywordFolderIndexBuilder();
	    	    builder.addDocument(bean);
	    	}
	    }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateKeywordFolder(String guid, String appClient, String clientRootDomain, String user, String title, String description, String type, String category, String parent, String aggregate, String acl, Boolean exportable, String status, String note, String folderPath) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        KeywordFolderBean bean = new KeywordFolderBean(guid, appClient, clientRootDomain, user, title, description, type, category, parent, aggregate, acl, exportable, status, note, folderPath);
        return updateKeywordFolder(bean);
    }
        
    @Override
    public Boolean updateKeywordFolder(KeywordFolder keywordFolder) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //KeywordFolder bean = refreshKeywordFolder(keywordFolder);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param keywordFolder cannot be null.....
        if(keywordFolder == null || keywordFolder.getGuid() == null) {
            log.log(Level.INFO, "Param keywordFolder or its guid is null!");
            throw new BadRequestException("Param keywordFolder object or its guid is null!");
        }
        KeywordFolderBean bean = null;
        if(keywordFolder instanceof KeywordFolderBean) {
            bean = (KeywordFolderBean) keywordFolder;
        } else {  // if(keywordFolder instanceof KeywordFolder)
            bean = new KeywordFolderBean(keywordFolder.getGuid(), keywordFolder.getAppClient(), keywordFolder.getClientRootDomain(), keywordFolder.getUser(), keywordFolder.getTitle(), keywordFolder.getDescription(), keywordFolder.getType(), keywordFolder.getCategory(), keywordFolder.getParent(), keywordFolder.getAggregate(), keywordFolder.getAcl(), keywordFolder.isExportable(), keywordFolder.getStatus(), keywordFolder.getNote(), keywordFolder.getFolderPath());
        }
        Boolean suc = getProxyFactory().getKeywordFolderServiceProxy().updateKeywordFolder(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

	    // TBD:
        if(Config.getInstance().isSearchEnabled()) {
            if(suc != null && suc.equals(Boolean.TRUE)) {
                log.fine("Calling addDocument()... for KeywordFolder.");
	    	    KeywordFolderIndexBuilder builder = new KeywordFolderIndexBuilder();
	    	    builder.addDocument(bean);
	        }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public KeywordFolder refreshKeywordFolder(KeywordFolder keywordFolder) throws BaseException
    {
        log.finer("BEGIN");

        // Param keywordFolder cannot be null.....
        if(keywordFolder == null || keywordFolder.getGuid() == null) {
            log.log(Level.INFO, "Param keywordFolder or its guid is null!");
            throw new BadRequestException("Param keywordFolder object or its guid is null!");
        }
        KeywordFolderBean bean = null;
        if(keywordFolder instanceof KeywordFolderBean) {
            bean = (KeywordFolderBean) keywordFolder;
        } else {  // if(keywordFolder instanceof KeywordFolder)
            bean = new KeywordFolderBean(keywordFolder.getGuid(), keywordFolder.getAppClient(), keywordFolder.getClientRootDomain(), keywordFolder.getUser(), keywordFolder.getTitle(), keywordFolder.getDescription(), keywordFolder.getType(), keywordFolder.getCategory(), keywordFolder.getParent(), keywordFolder.getAggregate(), keywordFolder.getAcl(), keywordFolder.isExportable(), keywordFolder.getStatus(), keywordFolder.getNote(), keywordFolder.getFolderPath());
        }
        Boolean suc = getProxyFactory().getKeywordFolderServiceProxy().updateKeywordFolder(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

	    // TBD:
        if(Config.getInstance().isSearchEnabled()) {
            if(suc != null && suc.equals(Boolean.TRUE)) {
                log.fine("Calling addDocument()... for KeywordFolder.");
	    	    KeywordFolderIndexBuilder builder = new KeywordFolderIndexBuilder();
	    	    builder.addDocument(bean);
	        }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteKeywordFolder(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getKeywordFolderServiceProxy().deleteKeywordFolder(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }

        // TBD:
        if(Config.getInstance().isSearchEnabled()) {
            if(suc != null && suc.equals(Boolean.TRUE)) {
                log.fine("Calling removeDocument()... for KeywordFolder.");
	    	    KeywordFolderIndexBuilder builder = new KeywordFolderIndexBuilder();
                builder.removeDocument(guid);
	        }
        }

            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            KeywordFolder keywordFolder = null;
            try {
                keywordFolder = getProxyFactory().getKeywordFolderServiceProxy().getKeywordFolder(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch keywordFolder with a key, " + guid);
                return false;
            }
            if(keywordFolder != null) {
                String beanGuid = keywordFolder.getGuid();
                Boolean suc1 = getProxyFactory().getKeywordFolderServiceProxy().deleteKeywordFolder(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("keywordFolder with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteKeywordFolder(KeywordFolder keywordFolder) throws BaseException
    {
        log.finer("BEGIN");

        // Param keywordFolder cannot be null.....
        if(keywordFolder == null || keywordFolder.getGuid() == null) {
            log.log(Level.INFO, "Param keywordFolder or its guid is null!");
            throw new BadRequestException("Param keywordFolder object or its guid is null!");
        }
        KeywordFolderBean bean = null;
        if(keywordFolder instanceof KeywordFolderBean) {
            bean = (KeywordFolderBean) keywordFolder;
        } else {  // if(keywordFolder instanceof KeywordFolder)
            // ????
            log.warning("keywordFolder is not an instance of KeywordFolderBean.");
            bean = new KeywordFolderBean(keywordFolder.getGuid(), keywordFolder.getAppClient(), keywordFolder.getClientRootDomain(), keywordFolder.getUser(), keywordFolder.getTitle(), keywordFolder.getDescription(), keywordFolder.getType(), keywordFolder.getCategory(), keywordFolder.getParent(), keywordFolder.getAggregate(), keywordFolder.getAcl(), keywordFolder.isExportable(), keywordFolder.getStatus(), keywordFolder.getNote(), keywordFolder.getFolderPath());
        }
        Boolean suc = getProxyFactory().getKeywordFolderServiceProxy().deleteKeywordFolder(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

	    // TBD:
        if(Config.getInstance().isSearchEnabled()) {
            if(suc != null && suc.equals(Boolean.TRUE)) {
                log.fine("Calling removeDocument()... for KeywordFolder.");
	    	    KeywordFolderIndexBuilder builder = new KeywordFolderIndexBuilder();
                builder.removeDocument(bean.getGuid());
	        }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteKeywordFolders(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getKeywordFolderServiceProxy().deleteKeywordFolders(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

	    // TBD:
        //if(Config.getInstance().isSearchEnabled()) {
        //    if(suc != null && suc.equals(Boolean.TRUE)) {
        //      log.fine("Calling removeDocument()... for KeywordFolder.");
	    //	    KeywordFolderIndexBuilder builder = new KeywordFolderIndexBuilder();
        //      builder.removeDocument(guids);
	    //    }
        //}

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createKeywordFolders(List<KeywordFolder> keywordFolders) throws BaseException
    {
        log.finer("BEGIN");

        if(keywordFolders == null) {
            log.log(Level.WARNING, "createKeywordFolders() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = keywordFolders.size();
        if(size == 0) {
            log.log(Level.WARNING, "createKeywordFolders() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(KeywordFolder keywordFolder : keywordFolders) {
            String guid = createKeywordFolder(keywordFolder);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createKeywordFolders() failed for at least one keywordFolder. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateKeywordFolders(List<KeywordFolder> keywordFolders) throws BaseException
    //{
    //}

}
