package com.cannyurl.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.KeywordLink;
import com.myurldb.ws.search.gae.KeywordLinkIndexBuilder;
import com.cannyurl.af.config.Config;

import com.cannyurl.af.bean.HashedPasswordStructBean;
import com.cannyurl.af.bean.GeoPointStructBean;
import com.cannyurl.af.bean.WebProfileStructBean;
import com.cannyurl.af.bean.StreetAddressStructBean;
import com.cannyurl.af.bean.SiteLogBean;
import com.cannyurl.af.bean.HelpNoticeBean;
import com.cannyurl.af.bean.TwitterCardAppInfoBean;
import com.cannyurl.af.bean.ContactInfoStructBean;
import com.cannyurl.af.bean.GeoCoordinateStructBean;
import com.cannyurl.af.bean.FullNameStructBean;
import com.cannyurl.af.bean.ExternalUserIdStructBean;
import com.cannyurl.af.bean.GaeUserStructBean;
import com.cannyurl.af.bean.AppBrandStructBean;
import com.cannyurl.af.bean.CellLatitudeLongitudeBean;
import com.cannyurl.af.bean.GaeAppStructBean;
import com.cannyurl.af.bean.ShortPassageAttributeBean;
import com.cannyurl.af.bean.KeyValuePairStructBean;
import com.cannyurl.af.bean.KeyValueRelationStructBean;
import com.cannyurl.af.bean.ExternalServiceApiKeyStructBean;
import com.cannyurl.af.bean.ReferrerInfoStructBean;
import com.cannyurl.af.bean.TwitterCardProductDataBean;
import com.cannyurl.af.bean.PagerStateStructBean;

import com.cannyurl.af.bean.KeywordLinkBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.KeywordLinkService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class KeywordLinkServiceImpl implements KeywordLinkService
{
    private static final Logger log = Logger.getLogger(KeywordLinkServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "KeywordLink-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("KeywordLink:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public KeywordLinkServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // KeywordLink related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public KeywordLink getKeywordLink(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getKeywordLink(): guid = " + guid);

        KeywordLinkBean bean = null;
        if(getCache() != null) {
            bean = (KeywordLinkBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (KeywordLinkBean) getProxyFactory().getKeywordLinkServiceProxy().getKeywordLink(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "KeywordLinkBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve KeywordLinkBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getKeywordLink(String guid, String field) throws BaseException
    {
        KeywordLinkBean bean = null;
        if(getCache() != null) {
            bean = (KeywordLinkBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (KeywordLinkBean) getProxyFactory().getKeywordLinkServiceProxy().getKeywordLink(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "KeywordLinkBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve KeywordLinkBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("appClient")) {
            return bean.getAppClient();
        } else if(field.equals("clientRootDomain")) {
            return bean.getClientRootDomain();
        } else if(field.equals("user")) {
            return bean.getUser();
        } else if(field.equals("shortLink")) {
            return bean.getShortLink();
        } else if(field.equals("domain")) {
            return bean.getDomain();
        } else if(field.equals("token")) {
            return bean.getToken();
        } else if(field.equals("longUrl")) {
            return bean.getLongUrl();
        } else if(field.equals("shortUrl")) {
            return bean.getShortUrl();
        } else if(field.equals("internal")) {
            return bean.isInternal();
        } else if(field.equals("caching")) {
            return bean.isCaching();
        } else if(field.equals("memo")) {
            return bean.getMemo();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("note")) {
            return bean.getNote();
        } else if(field.equals("expirationTime")) {
            return bean.getExpirationTime();
        } else if(field.equals("keywordFolder")) {
            return bean.getKeywordFolder();
        } else if(field.equals("folderPath")) {
            return bean.getFolderPath();
        } else if(field.equals("keyword")) {
            return bean.getKeyword();
        } else if(field.equals("queryKey")) {
            return bean.getQueryKey();
        } else if(field.equals("scope")) {
            return bean.getScope();
        } else if(field.equals("dynamic")) {
            return bean.isDynamic();
        } else if(field.equals("caseSensitive")) {
            return bean.isCaseSensitive();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<KeywordLink> getKeywordLinks(List<String> guids) throws BaseException
    {
        log.fine("getKeywordLinks()");

        // TBD: Is there a better way????
        List<KeywordLink> keywordLinks = getProxyFactory().getKeywordLinkServiceProxy().getKeywordLinks(guids);
        if(keywordLinks == null) {
            log.log(Level.WARNING, "Failed to retrieve KeywordLinkBean list.");
        }

        log.finer("END");
        return keywordLinks;
    }

    @Override
    public List<KeywordLink> getAllKeywordLinks() throws BaseException
    {
        return getAllKeywordLinks(null, null, null);
    }

    @Override
    public List<KeywordLink> getAllKeywordLinks(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllKeywordLinks(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<KeywordLink> keywordLinks = getProxyFactory().getKeywordLinkServiceProxy().getAllKeywordLinks(ordering, offset, count);
        if(keywordLinks == null) {
            log.log(Level.WARNING, "Failed to retrieve KeywordLinkBean list.");
        }

        log.finer("END");
        return keywordLinks;
    }

    @Override
    public List<String> getAllKeywordLinkKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllKeywordLinkKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getKeywordLinkServiceProxy().getAllKeywordLinkKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve KeywordLinkBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty KeywordLinkBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<KeywordLink> findKeywordLinks(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findKeywordLinks(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<KeywordLink> findKeywordLinks(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("KeywordLinkServiceImpl.findKeywordLinks(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<KeywordLink> keywordLinks = getProxyFactory().getKeywordLinkServiceProxy().findKeywordLinks(filter, ordering, params, values, grouping, unique, offset, count);
        if(keywordLinks == null) {
            log.log(Level.WARNING, "Failed to find keywordLinks for the given criterion.");
        }

        log.finer("END");
        return keywordLinks;
    }

    @Override
    public List<String> findKeywordLinkKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("KeywordLinkServiceImpl.findKeywordLinkKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getKeywordLinkServiceProxy().findKeywordLinkKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find KeywordLink keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty KeywordLink key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("KeywordLinkServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getKeywordLinkServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createKeywordLink(String appClient, String clientRootDomain, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, Boolean internal, Boolean caching, String memo, String status, String note, Long expirationTime, String keywordFolder, String folderPath, String keyword, String queryKey, String scope, Boolean dynamic, Boolean caseSensitive) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        KeywordLinkBean bean = new KeywordLinkBean(null, appClient, clientRootDomain, user, shortLink, domain, token, longUrl, shortUrl, internal, caching, memo, status, note, expirationTime, keywordFolder, folderPath, keyword, queryKey, scope, dynamic, caseSensitive);
        return createKeywordLink(bean);
    }

    @Override
    public String createKeywordLink(KeywordLink keywordLink) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //KeywordLink bean = constructKeywordLink(keywordLink);
        //return bean.getGuid();

        // Param keywordLink cannot be null.....
        if(keywordLink == null) {
            log.log(Level.INFO, "Param keywordLink is null!");
            throw new BadRequestException("Param keywordLink object is null!");
        }
        KeywordLinkBean bean = null;
        if(keywordLink instanceof KeywordLinkBean) {
            bean = (KeywordLinkBean) keywordLink;
        } else if(keywordLink instanceof KeywordLink) {
            // bean = new KeywordLinkBean(null, keywordLink.getAppClient(), keywordLink.getClientRootDomain(), keywordLink.getUser(), keywordLink.getShortLink(), keywordLink.getDomain(), keywordLink.getToken(), keywordLink.getLongUrl(), keywordLink.getShortUrl(), keywordLink.isInternal(), keywordLink.isCaching(), keywordLink.getMemo(), keywordLink.getStatus(), keywordLink.getNote(), keywordLink.getExpirationTime(), keywordLink.getKeywordFolder(), keywordLink.getFolderPath(), keywordLink.getKeyword(), keywordLink.getQueryKey(), keywordLink.getScope(), keywordLink.isDynamic(), keywordLink.isCaseSensitive());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new KeywordLinkBean(keywordLink.getGuid(), keywordLink.getAppClient(), keywordLink.getClientRootDomain(), keywordLink.getUser(), keywordLink.getShortLink(), keywordLink.getDomain(), keywordLink.getToken(), keywordLink.getLongUrl(), keywordLink.getShortUrl(), keywordLink.isInternal(), keywordLink.isCaching(), keywordLink.getMemo(), keywordLink.getStatus(), keywordLink.getNote(), keywordLink.getExpirationTime(), keywordLink.getKeywordFolder(), keywordLink.getFolderPath(), keywordLink.getKeyword(), keywordLink.getQueryKey(), keywordLink.getScope(), keywordLink.isDynamic(), keywordLink.isCaseSensitive());
        } else {
            log.log(Level.WARNING, "createKeywordLink(): Arg keywordLink is of an unknown type.");
            //bean = new KeywordLinkBean();
            bean = new KeywordLinkBean(keywordLink.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getKeywordLinkServiceProxy().createKeywordLink(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

	    // TBD:
	    if(Config.getInstance().isSearchEnabled()) {
            if(guid != null && !guid.isEmpty()) {
                log.fine("Calling addDocument()... for KeywordLink.");
                KeywordLinkIndexBuilder builder = new KeywordLinkIndexBuilder();
	    	    builder.addDocument(bean);
	    	}
	    }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public KeywordLink constructKeywordLink(KeywordLink keywordLink) throws BaseException
    {
        log.finer("BEGIN");

        // Param keywordLink cannot be null.....
        if(keywordLink == null) {
            log.log(Level.INFO, "Param keywordLink is null!");
            throw new BadRequestException("Param keywordLink object is null!");
        }
        KeywordLinkBean bean = null;
        if(keywordLink instanceof KeywordLinkBean) {
            bean = (KeywordLinkBean) keywordLink;
        } else if(keywordLink instanceof KeywordLink) {
            // bean = new KeywordLinkBean(null, keywordLink.getAppClient(), keywordLink.getClientRootDomain(), keywordLink.getUser(), keywordLink.getShortLink(), keywordLink.getDomain(), keywordLink.getToken(), keywordLink.getLongUrl(), keywordLink.getShortUrl(), keywordLink.isInternal(), keywordLink.isCaching(), keywordLink.getMemo(), keywordLink.getStatus(), keywordLink.getNote(), keywordLink.getExpirationTime(), keywordLink.getKeywordFolder(), keywordLink.getFolderPath(), keywordLink.getKeyword(), keywordLink.getQueryKey(), keywordLink.getScope(), keywordLink.isDynamic(), keywordLink.isCaseSensitive());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new KeywordLinkBean(keywordLink.getGuid(), keywordLink.getAppClient(), keywordLink.getClientRootDomain(), keywordLink.getUser(), keywordLink.getShortLink(), keywordLink.getDomain(), keywordLink.getToken(), keywordLink.getLongUrl(), keywordLink.getShortUrl(), keywordLink.isInternal(), keywordLink.isCaching(), keywordLink.getMemo(), keywordLink.getStatus(), keywordLink.getNote(), keywordLink.getExpirationTime(), keywordLink.getKeywordFolder(), keywordLink.getFolderPath(), keywordLink.getKeyword(), keywordLink.getQueryKey(), keywordLink.getScope(), keywordLink.isDynamic(), keywordLink.isCaseSensitive());
        } else {
            log.log(Level.WARNING, "createKeywordLink(): Arg keywordLink is of an unknown type.");
            //bean = new KeywordLinkBean();
            bean = new KeywordLinkBean(keywordLink.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getKeywordLinkServiceProxy().createKeywordLink(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

	    // TBD:
	    if(Config.getInstance().isSearchEnabled()) {
            if(guid != null && !guid.isEmpty()) {
                log.fine("Calling addDocument()... for KeywordLink.");
                KeywordLinkIndexBuilder builder = new KeywordLinkIndexBuilder();
	    	    builder.addDocument(bean);
	    	}
	    }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateKeywordLink(String guid, String appClient, String clientRootDomain, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, Boolean internal, Boolean caching, String memo, String status, String note, Long expirationTime, String keywordFolder, String folderPath, String keyword, String queryKey, String scope, Boolean dynamic, Boolean caseSensitive) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        KeywordLinkBean bean = new KeywordLinkBean(guid, appClient, clientRootDomain, user, shortLink, domain, token, longUrl, shortUrl, internal, caching, memo, status, note, expirationTime, keywordFolder, folderPath, keyword, queryKey, scope, dynamic, caseSensitive);
        return updateKeywordLink(bean);
    }
        
    @Override
    public Boolean updateKeywordLink(KeywordLink keywordLink) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //KeywordLink bean = refreshKeywordLink(keywordLink);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param keywordLink cannot be null.....
        if(keywordLink == null || keywordLink.getGuid() == null) {
            log.log(Level.INFO, "Param keywordLink or its guid is null!");
            throw new BadRequestException("Param keywordLink object or its guid is null!");
        }
        KeywordLinkBean bean = null;
        if(keywordLink instanceof KeywordLinkBean) {
            bean = (KeywordLinkBean) keywordLink;
        } else {  // if(keywordLink instanceof KeywordLink)
            bean = new KeywordLinkBean(keywordLink.getGuid(), keywordLink.getAppClient(), keywordLink.getClientRootDomain(), keywordLink.getUser(), keywordLink.getShortLink(), keywordLink.getDomain(), keywordLink.getToken(), keywordLink.getLongUrl(), keywordLink.getShortUrl(), keywordLink.isInternal(), keywordLink.isCaching(), keywordLink.getMemo(), keywordLink.getStatus(), keywordLink.getNote(), keywordLink.getExpirationTime(), keywordLink.getKeywordFolder(), keywordLink.getFolderPath(), keywordLink.getKeyword(), keywordLink.getQueryKey(), keywordLink.getScope(), keywordLink.isDynamic(), keywordLink.isCaseSensitive());
        }
        Boolean suc = getProxyFactory().getKeywordLinkServiceProxy().updateKeywordLink(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

	    // TBD:
        if(Config.getInstance().isSearchEnabled()) {
            if(suc != null && suc.equals(Boolean.TRUE)) {
                log.fine("Calling addDocument()... for KeywordLink.");
	    	    KeywordLinkIndexBuilder builder = new KeywordLinkIndexBuilder();
	    	    builder.addDocument(bean);
	        }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public KeywordLink refreshKeywordLink(KeywordLink keywordLink) throws BaseException
    {
        log.finer("BEGIN");

        // Param keywordLink cannot be null.....
        if(keywordLink == null || keywordLink.getGuid() == null) {
            log.log(Level.INFO, "Param keywordLink or its guid is null!");
            throw new BadRequestException("Param keywordLink object or its guid is null!");
        }
        KeywordLinkBean bean = null;
        if(keywordLink instanceof KeywordLinkBean) {
            bean = (KeywordLinkBean) keywordLink;
        } else {  // if(keywordLink instanceof KeywordLink)
            bean = new KeywordLinkBean(keywordLink.getGuid(), keywordLink.getAppClient(), keywordLink.getClientRootDomain(), keywordLink.getUser(), keywordLink.getShortLink(), keywordLink.getDomain(), keywordLink.getToken(), keywordLink.getLongUrl(), keywordLink.getShortUrl(), keywordLink.isInternal(), keywordLink.isCaching(), keywordLink.getMemo(), keywordLink.getStatus(), keywordLink.getNote(), keywordLink.getExpirationTime(), keywordLink.getKeywordFolder(), keywordLink.getFolderPath(), keywordLink.getKeyword(), keywordLink.getQueryKey(), keywordLink.getScope(), keywordLink.isDynamic(), keywordLink.isCaseSensitive());
        }
        Boolean suc = getProxyFactory().getKeywordLinkServiceProxy().updateKeywordLink(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

	    // TBD:
        if(Config.getInstance().isSearchEnabled()) {
            if(suc != null && suc.equals(Boolean.TRUE)) {
                log.fine("Calling addDocument()... for KeywordLink.");
	    	    KeywordLinkIndexBuilder builder = new KeywordLinkIndexBuilder();
	    	    builder.addDocument(bean);
	        }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteKeywordLink(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getKeywordLinkServiceProxy().deleteKeywordLink(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }

        // TBD:
        if(Config.getInstance().isSearchEnabled()) {
            if(suc != null && suc.equals(Boolean.TRUE)) {
                log.fine("Calling removeDocument()... for KeywordLink.");
	    	    KeywordLinkIndexBuilder builder = new KeywordLinkIndexBuilder();
                builder.removeDocument(guid);
	        }
        }

            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            KeywordLink keywordLink = null;
            try {
                keywordLink = getProxyFactory().getKeywordLinkServiceProxy().getKeywordLink(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch keywordLink with a key, " + guid);
                return false;
            }
            if(keywordLink != null) {
                String beanGuid = keywordLink.getGuid();
                Boolean suc1 = getProxyFactory().getKeywordLinkServiceProxy().deleteKeywordLink(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("keywordLink with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteKeywordLink(KeywordLink keywordLink) throws BaseException
    {
        log.finer("BEGIN");

        // Param keywordLink cannot be null.....
        if(keywordLink == null || keywordLink.getGuid() == null) {
            log.log(Level.INFO, "Param keywordLink or its guid is null!");
            throw new BadRequestException("Param keywordLink object or its guid is null!");
        }
        KeywordLinkBean bean = null;
        if(keywordLink instanceof KeywordLinkBean) {
            bean = (KeywordLinkBean) keywordLink;
        } else {  // if(keywordLink instanceof KeywordLink)
            // ????
            log.warning("keywordLink is not an instance of KeywordLinkBean.");
            bean = new KeywordLinkBean(keywordLink.getGuid(), keywordLink.getAppClient(), keywordLink.getClientRootDomain(), keywordLink.getUser(), keywordLink.getShortLink(), keywordLink.getDomain(), keywordLink.getToken(), keywordLink.getLongUrl(), keywordLink.getShortUrl(), keywordLink.isInternal(), keywordLink.isCaching(), keywordLink.getMemo(), keywordLink.getStatus(), keywordLink.getNote(), keywordLink.getExpirationTime(), keywordLink.getKeywordFolder(), keywordLink.getFolderPath(), keywordLink.getKeyword(), keywordLink.getQueryKey(), keywordLink.getScope(), keywordLink.isDynamic(), keywordLink.isCaseSensitive());
        }
        Boolean suc = getProxyFactory().getKeywordLinkServiceProxy().deleteKeywordLink(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

	    // TBD:
        if(Config.getInstance().isSearchEnabled()) {
            if(suc != null && suc.equals(Boolean.TRUE)) {
                log.fine("Calling removeDocument()... for KeywordLink.");
	    	    KeywordLinkIndexBuilder builder = new KeywordLinkIndexBuilder();
                builder.removeDocument(bean.getGuid());
	        }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteKeywordLinks(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getKeywordLinkServiceProxy().deleteKeywordLinks(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

	    // TBD:
        //if(Config.getInstance().isSearchEnabled()) {
        //    if(suc != null && suc.equals(Boolean.TRUE)) {
        //      log.fine("Calling removeDocument()... for KeywordLink.");
	    //	    KeywordLinkIndexBuilder builder = new KeywordLinkIndexBuilder();
        //      builder.removeDocument(guids);
	    //    }
        //}

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createKeywordLinks(List<KeywordLink> keywordLinks) throws BaseException
    {
        log.finer("BEGIN");

        if(keywordLinks == null) {
            log.log(Level.WARNING, "createKeywordLinks() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = keywordLinks.size();
        if(size == 0) {
            log.log(Level.WARNING, "createKeywordLinks() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(KeywordLink keywordLink : keywordLinks) {
            String guid = createKeywordLink(keywordLink);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createKeywordLinks() failed for at least one keywordLink. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateKeywordLinks(List<KeywordLink> keywordLinks) throws BaseException
    //{
    //}

}
