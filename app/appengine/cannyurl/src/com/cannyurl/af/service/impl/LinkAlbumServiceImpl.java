package com.cannyurl.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.LinkAlbum;
import com.myurldb.ws.search.gae.LinkAlbumIndexBuilder;
import com.cannyurl.af.config.Config;

import com.cannyurl.af.bean.HashedPasswordStructBean;
import com.cannyurl.af.bean.GeoPointStructBean;
import com.cannyurl.af.bean.WebProfileStructBean;
import com.cannyurl.af.bean.StreetAddressStructBean;
import com.cannyurl.af.bean.SiteLogBean;
import com.cannyurl.af.bean.HelpNoticeBean;
import com.cannyurl.af.bean.TwitterCardAppInfoBean;
import com.cannyurl.af.bean.ContactInfoStructBean;
import com.cannyurl.af.bean.GeoCoordinateStructBean;
import com.cannyurl.af.bean.FullNameStructBean;
import com.cannyurl.af.bean.ExternalUserIdStructBean;
import com.cannyurl.af.bean.GaeUserStructBean;
import com.cannyurl.af.bean.AppBrandStructBean;
import com.cannyurl.af.bean.CellLatitudeLongitudeBean;
import com.cannyurl.af.bean.GaeAppStructBean;
import com.cannyurl.af.bean.ShortPassageAttributeBean;
import com.cannyurl.af.bean.KeyValuePairStructBean;
import com.cannyurl.af.bean.KeyValueRelationStructBean;
import com.cannyurl.af.bean.ExternalServiceApiKeyStructBean;
import com.cannyurl.af.bean.ReferrerInfoStructBean;
import com.cannyurl.af.bean.TwitterCardProductDataBean;
import com.cannyurl.af.bean.PagerStateStructBean;

import com.cannyurl.af.bean.LinkAlbumBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.LinkAlbumService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class LinkAlbumServiceImpl implements LinkAlbumService
{
    private static final Logger log = Logger.getLogger(LinkAlbumServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "LinkAlbum-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("LinkAlbum:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public LinkAlbumServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // LinkAlbum related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public LinkAlbum getLinkAlbum(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getLinkAlbum(): guid = " + guid);

        LinkAlbumBean bean = null;
        if(getCache() != null) {
            bean = (LinkAlbumBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (LinkAlbumBean) getProxyFactory().getLinkAlbumServiceProxy().getLinkAlbum(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "LinkAlbumBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve LinkAlbumBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getLinkAlbum(String guid, String field) throws BaseException
    {
        LinkAlbumBean bean = null;
        if(getCache() != null) {
            bean = (LinkAlbumBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (LinkAlbumBean) getProxyFactory().getLinkAlbumServiceProxy().getLinkAlbum(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "LinkAlbumBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve LinkAlbumBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("appClient")) {
            return bean.getAppClient();
        } else if(field.equals("clientRootDomain")) {
            return bean.getClientRootDomain();
        } else if(field.equals("owner")) {
            return bean.getOwner();
        } else if(field.equals("name")) {
            return bean.getName();
        } else if(field.equals("summary")) {
            return bean.getSummary();
        } else if(field.equals("tokenPrefix")) {
            return bean.getTokenPrefix();
        } else if(field.equals("permalink")) {
            return bean.getPermalink();
        } else if(field.equals("shortLink")) {
            return bean.getShortLink();
        } else if(field.equals("shortUrl")) {
            return bean.getShortUrl();
        } else if(field.equals("source")) {
            return bean.getSource();
        } else if(field.equals("referenceUrl")) {
            return bean.getReferenceUrl();
        } else if(field.equals("autoGenerated")) {
            return bean.isAutoGenerated();
        } else if(field.equals("maxLinkCount")) {
            return bean.getMaxLinkCount();
        } else if(field.equals("note")) {
            return bean.getNote();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<LinkAlbum> getLinkAlbums(List<String> guids) throws BaseException
    {
        log.fine("getLinkAlbums()");

        // TBD: Is there a better way????
        List<LinkAlbum> linkAlbums = getProxyFactory().getLinkAlbumServiceProxy().getLinkAlbums(guids);
        if(linkAlbums == null) {
            log.log(Level.WARNING, "Failed to retrieve LinkAlbumBean list.");
        }

        log.finer("END");
        return linkAlbums;
    }

    @Override
    public List<LinkAlbum> getAllLinkAlbums() throws BaseException
    {
        return getAllLinkAlbums(null, null, null);
    }

    @Override
    public List<LinkAlbum> getAllLinkAlbums(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllLinkAlbums(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<LinkAlbum> linkAlbums = getProxyFactory().getLinkAlbumServiceProxy().getAllLinkAlbums(ordering, offset, count);
        if(linkAlbums == null) {
            log.log(Level.WARNING, "Failed to retrieve LinkAlbumBean list.");
        }

        log.finer("END");
        return linkAlbums;
    }

    @Override
    public List<String> getAllLinkAlbumKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllLinkAlbumKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getLinkAlbumServiceProxy().getAllLinkAlbumKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve LinkAlbumBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty LinkAlbumBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<LinkAlbum> findLinkAlbums(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findLinkAlbums(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<LinkAlbum> findLinkAlbums(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("LinkAlbumServiceImpl.findLinkAlbums(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<LinkAlbum> linkAlbums = getProxyFactory().getLinkAlbumServiceProxy().findLinkAlbums(filter, ordering, params, values, grouping, unique, offset, count);
        if(linkAlbums == null) {
            log.log(Level.WARNING, "Failed to find linkAlbums for the given criterion.");
        }

        log.finer("END");
        return linkAlbums;
    }

    @Override
    public List<String> findLinkAlbumKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("LinkAlbumServiceImpl.findLinkAlbumKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getLinkAlbumServiceProxy().findLinkAlbumKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find LinkAlbum keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty LinkAlbum key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("LinkAlbumServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getLinkAlbumServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createLinkAlbum(String appClient, String clientRootDomain, String owner, String name, String summary, String tokenPrefix, String permalink, String shortLink, String shortUrl, String source, String referenceUrl, Boolean autoGenerated, Integer maxLinkCount, String note, String status) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        LinkAlbumBean bean = new LinkAlbumBean(null, appClient, clientRootDomain, owner, name, summary, tokenPrefix, permalink, shortLink, shortUrl, source, referenceUrl, autoGenerated, maxLinkCount, note, status);
        return createLinkAlbum(bean);
    }

    @Override
    public String createLinkAlbum(LinkAlbum linkAlbum) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //LinkAlbum bean = constructLinkAlbum(linkAlbum);
        //return bean.getGuid();

        // Param linkAlbum cannot be null.....
        if(linkAlbum == null) {
            log.log(Level.INFO, "Param linkAlbum is null!");
            throw new BadRequestException("Param linkAlbum object is null!");
        }
        LinkAlbumBean bean = null;
        if(linkAlbum instanceof LinkAlbumBean) {
            bean = (LinkAlbumBean) linkAlbum;
        } else if(linkAlbum instanceof LinkAlbum) {
            // bean = new LinkAlbumBean(null, linkAlbum.getAppClient(), linkAlbum.getClientRootDomain(), linkAlbum.getOwner(), linkAlbum.getName(), linkAlbum.getSummary(), linkAlbum.getTokenPrefix(), linkAlbum.getPermalink(), linkAlbum.getShortLink(), linkAlbum.getShortUrl(), linkAlbum.getSource(), linkAlbum.getReferenceUrl(), linkAlbum.isAutoGenerated(), linkAlbum.getMaxLinkCount(), linkAlbum.getNote(), linkAlbum.getStatus());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new LinkAlbumBean(linkAlbum.getGuid(), linkAlbum.getAppClient(), linkAlbum.getClientRootDomain(), linkAlbum.getOwner(), linkAlbum.getName(), linkAlbum.getSummary(), linkAlbum.getTokenPrefix(), linkAlbum.getPermalink(), linkAlbum.getShortLink(), linkAlbum.getShortUrl(), linkAlbum.getSource(), linkAlbum.getReferenceUrl(), linkAlbum.isAutoGenerated(), linkAlbum.getMaxLinkCount(), linkAlbum.getNote(), linkAlbum.getStatus());
        } else {
            log.log(Level.WARNING, "createLinkAlbum(): Arg linkAlbum is of an unknown type.");
            //bean = new LinkAlbumBean();
            bean = new LinkAlbumBean(linkAlbum.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getLinkAlbumServiceProxy().createLinkAlbum(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

	    // TBD:
	    if(Config.getInstance().isSearchEnabled()) {
            if(guid != null && !guid.isEmpty()) {
                log.fine("Calling addDocument()... for LinkAlbum.");
                LinkAlbumIndexBuilder builder = new LinkAlbumIndexBuilder();
	    	    builder.addDocument(bean);
	    	}
	    }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public LinkAlbum constructLinkAlbum(LinkAlbum linkAlbum) throws BaseException
    {
        log.finer("BEGIN");

        // Param linkAlbum cannot be null.....
        if(linkAlbum == null) {
            log.log(Level.INFO, "Param linkAlbum is null!");
            throw new BadRequestException("Param linkAlbum object is null!");
        }
        LinkAlbumBean bean = null;
        if(linkAlbum instanceof LinkAlbumBean) {
            bean = (LinkAlbumBean) linkAlbum;
        } else if(linkAlbum instanceof LinkAlbum) {
            // bean = new LinkAlbumBean(null, linkAlbum.getAppClient(), linkAlbum.getClientRootDomain(), linkAlbum.getOwner(), linkAlbum.getName(), linkAlbum.getSummary(), linkAlbum.getTokenPrefix(), linkAlbum.getPermalink(), linkAlbum.getShortLink(), linkAlbum.getShortUrl(), linkAlbum.getSource(), linkAlbum.getReferenceUrl(), linkAlbum.isAutoGenerated(), linkAlbum.getMaxLinkCount(), linkAlbum.getNote(), linkAlbum.getStatus());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new LinkAlbumBean(linkAlbum.getGuid(), linkAlbum.getAppClient(), linkAlbum.getClientRootDomain(), linkAlbum.getOwner(), linkAlbum.getName(), linkAlbum.getSummary(), linkAlbum.getTokenPrefix(), linkAlbum.getPermalink(), linkAlbum.getShortLink(), linkAlbum.getShortUrl(), linkAlbum.getSource(), linkAlbum.getReferenceUrl(), linkAlbum.isAutoGenerated(), linkAlbum.getMaxLinkCount(), linkAlbum.getNote(), linkAlbum.getStatus());
        } else {
            log.log(Level.WARNING, "createLinkAlbum(): Arg linkAlbum is of an unknown type.");
            //bean = new LinkAlbumBean();
            bean = new LinkAlbumBean(linkAlbum.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getLinkAlbumServiceProxy().createLinkAlbum(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

	    // TBD:
	    if(Config.getInstance().isSearchEnabled()) {
            if(guid != null && !guid.isEmpty()) {
                log.fine("Calling addDocument()... for LinkAlbum.");
                LinkAlbumIndexBuilder builder = new LinkAlbumIndexBuilder();
	    	    builder.addDocument(bean);
	    	}
	    }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateLinkAlbum(String guid, String appClient, String clientRootDomain, String owner, String name, String summary, String tokenPrefix, String permalink, String shortLink, String shortUrl, String source, String referenceUrl, Boolean autoGenerated, Integer maxLinkCount, String note, String status) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        LinkAlbumBean bean = new LinkAlbumBean(guid, appClient, clientRootDomain, owner, name, summary, tokenPrefix, permalink, shortLink, shortUrl, source, referenceUrl, autoGenerated, maxLinkCount, note, status);
        return updateLinkAlbum(bean);
    }
        
    @Override
    public Boolean updateLinkAlbum(LinkAlbum linkAlbum) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //LinkAlbum bean = refreshLinkAlbum(linkAlbum);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param linkAlbum cannot be null.....
        if(linkAlbum == null || linkAlbum.getGuid() == null) {
            log.log(Level.INFO, "Param linkAlbum or its guid is null!");
            throw new BadRequestException("Param linkAlbum object or its guid is null!");
        }
        LinkAlbumBean bean = null;
        if(linkAlbum instanceof LinkAlbumBean) {
            bean = (LinkAlbumBean) linkAlbum;
        } else {  // if(linkAlbum instanceof LinkAlbum)
            bean = new LinkAlbumBean(linkAlbum.getGuid(), linkAlbum.getAppClient(), linkAlbum.getClientRootDomain(), linkAlbum.getOwner(), linkAlbum.getName(), linkAlbum.getSummary(), linkAlbum.getTokenPrefix(), linkAlbum.getPermalink(), linkAlbum.getShortLink(), linkAlbum.getShortUrl(), linkAlbum.getSource(), linkAlbum.getReferenceUrl(), linkAlbum.isAutoGenerated(), linkAlbum.getMaxLinkCount(), linkAlbum.getNote(), linkAlbum.getStatus());
        }
        Boolean suc = getProxyFactory().getLinkAlbumServiceProxy().updateLinkAlbum(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

	    // TBD:
        if(Config.getInstance().isSearchEnabled()) {
            if(suc != null && suc.equals(Boolean.TRUE)) {
                log.fine("Calling addDocument()... for LinkAlbum.");
	    	    LinkAlbumIndexBuilder builder = new LinkAlbumIndexBuilder();
	    	    builder.addDocument(bean);
	        }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public LinkAlbum refreshLinkAlbum(LinkAlbum linkAlbum) throws BaseException
    {
        log.finer("BEGIN");

        // Param linkAlbum cannot be null.....
        if(linkAlbum == null || linkAlbum.getGuid() == null) {
            log.log(Level.INFO, "Param linkAlbum or its guid is null!");
            throw new BadRequestException("Param linkAlbum object or its guid is null!");
        }
        LinkAlbumBean bean = null;
        if(linkAlbum instanceof LinkAlbumBean) {
            bean = (LinkAlbumBean) linkAlbum;
        } else {  // if(linkAlbum instanceof LinkAlbum)
            bean = new LinkAlbumBean(linkAlbum.getGuid(), linkAlbum.getAppClient(), linkAlbum.getClientRootDomain(), linkAlbum.getOwner(), linkAlbum.getName(), linkAlbum.getSummary(), linkAlbum.getTokenPrefix(), linkAlbum.getPermalink(), linkAlbum.getShortLink(), linkAlbum.getShortUrl(), linkAlbum.getSource(), linkAlbum.getReferenceUrl(), linkAlbum.isAutoGenerated(), linkAlbum.getMaxLinkCount(), linkAlbum.getNote(), linkAlbum.getStatus());
        }
        Boolean suc = getProxyFactory().getLinkAlbumServiceProxy().updateLinkAlbum(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

	    // TBD:
        if(Config.getInstance().isSearchEnabled()) {
            if(suc != null && suc.equals(Boolean.TRUE)) {
                log.fine("Calling addDocument()... for LinkAlbum.");
	    	    LinkAlbumIndexBuilder builder = new LinkAlbumIndexBuilder();
	    	    builder.addDocument(bean);
	        }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteLinkAlbum(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getLinkAlbumServiceProxy().deleteLinkAlbum(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }

        // TBD:
        if(Config.getInstance().isSearchEnabled()) {
            if(suc != null && suc.equals(Boolean.TRUE)) {
                log.fine("Calling removeDocument()... for LinkAlbum.");
	    	    LinkAlbumIndexBuilder builder = new LinkAlbumIndexBuilder();
                builder.removeDocument(guid);
	        }
        }

            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            LinkAlbum linkAlbum = null;
            try {
                linkAlbum = getProxyFactory().getLinkAlbumServiceProxy().getLinkAlbum(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch linkAlbum with a key, " + guid);
                return false;
            }
            if(linkAlbum != null) {
                String beanGuid = linkAlbum.getGuid();
                Boolean suc1 = getProxyFactory().getLinkAlbumServiceProxy().deleteLinkAlbum(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("linkAlbum with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteLinkAlbum(LinkAlbum linkAlbum) throws BaseException
    {
        log.finer("BEGIN");

        // Param linkAlbum cannot be null.....
        if(linkAlbum == null || linkAlbum.getGuid() == null) {
            log.log(Level.INFO, "Param linkAlbum or its guid is null!");
            throw new BadRequestException("Param linkAlbum object or its guid is null!");
        }
        LinkAlbumBean bean = null;
        if(linkAlbum instanceof LinkAlbumBean) {
            bean = (LinkAlbumBean) linkAlbum;
        } else {  // if(linkAlbum instanceof LinkAlbum)
            // ????
            log.warning("linkAlbum is not an instance of LinkAlbumBean.");
            bean = new LinkAlbumBean(linkAlbum.getGuid(), linkAlbum.getAppClient(), linkAlbum.getClientRootDomain(), linkAlbum.getOwner(), linkAlbum.getName(), linkAlbum.getSummary(), linkAlbum.getTokenPrefix(), linkAlbum.getPermalink(), linkAlbum.getShortLink(), linkAlbum.getShortUrl(), linkAlbum.getSource(), linkAlbum.getReferenceUrl(), linkAlbum.isAutoGenerated(), linkAlbum.getMaxLinkCount(), linkAlbum.getNote(), linkAlbum.getStatus());
        }
        Boolean suc = getProxyFactory().getLinkAlbumServiceProxy().deleteLinkAlbum(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

	    // TBD:
        if(Config.getInstance().isSearchEnabled()) {
            if(suc != null && suc.equals(Boolean.TRUE)) {
                log.fine("Calling removeDocument()... for LinkAlbum.");
	    	    LinkAlbumIndexBuilder builder = new LinkAlbumIndexBuilder();
                builder.removeDocument(bean.getGuid());
	        }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteLinkAlbums(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getLinkAlbumServiceProxy().deleteLinkAlbums(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

	    // TBD:
        //if(Config.getInstance().isSearchEnabled()) {
        //    if(suc != null && suc.equals(Boolean.TRUE)) {
        //      log.fine("Calling removeDocument()... for LinkAlbum.");
	    //	    LinkAlbumIndexBuilder builder = new LinkAlbumIndexBuilder();
        //      builder.removeDocument(guids);
	    //    }
        //}

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createLinkAlbums(List<LinkAlbum> linkAlbums) throws BaseException
    {
        log.finer("BEGIN");

        if(linkAlbums == null) {
            log.log(Level.WARNING, "createLinkAlbums() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = linkAlbums.size();
        if(size == 0) {
            log.log(Level.WARNING, "createLinkAlbums() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(LinkAlbum linkAlbum : linkAlbums) {
            String guid = createLinkAlbum(linkAlbum);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createLinkAlbums() failed for at least one linkAlbum. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateLinkAlbums(List<LinkAlbum> linkAlbums) throws BaseException
    //{
    //}

}
