package com.cannyurl.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.LinkMessage;
import com.myurldb.ws.search.gae.LinkMessageIndexBuilder;
import com.cannyurl.af.config.Config;

import com.cannyurl.af.bean.HashedPasswordStructBean;
import com.cannyurl.af.bean.GeoPointStructBean;
import com.cannyurl.af.bean.WebProfileStructBean;
import com.cannyurl.af.bean.StreetAddressStructBean;
import com.cannyurl.af.bean.SiteLogBean;
import com.cannyurl.af.bean.HelpNoticeBean;
import com.cannyurl.af.bean.TwitterCardAppInfoBean;
import com.cannyurl.af.bean.ContactInfoStructBean;
import com.cannyurl.af.bean.GeoCoordinateStructBean;
import com.cannyurl.af.bean.FullNameStructBean;
import com.cannyurl.af.bean.ExternalUserIdStructBean;
import com.cannyurl.af.bean.GaeUserStructBean;
import com.cannyurl.af.bean.AppBrandStructBean;
import com.cannyurl.af.bean.CellLatitudeLongitudeBean;
import com.cannyurl.af.bean.GaeAppStructBean;
import com.cannyurl.af.bean.ShortPassageAttributeBean;
import com.cannyurl.af.bean.KeyValuePairStructBean;
import com.cannyurl.af.bean.KeyValueRelationStructBean;
import com.cannyurl.af.bean.ExternalServiceApiKeyStructBean;
import com.cannyurl.af.bean.ReferrerInfoStructBean;
import com.cannyurl.af.bean.TwitterCardProductDataBean;
import com.cannyurl.af.bean.PagerStateStructBean;

import com.cannyurl.af.bean.LinkMessageBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.LinkMessageService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class LinkMessageServiceImpl implements LinkMessageService
{
    private static final Logger log = Logger.getLogger(LinkMessageServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "LinkMessage-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("LinkMessage:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public LinkMessageServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // LinkMessage related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public LinkMessage getLinkMessage(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getLinkMessage(): guid = " + guid);

        LinkMessageBean bean = null;
        if(getCache() != null) {
            bean = (LinkMessageBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (LinkMessageBean) getProxyFactory().getLinkMessageServiceProxy().getLinkMessage(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "LinkMessageBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve LinkMessageBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getLinkMessage(String guid, String field) throws BaseException
    {
        LinkMessageBean bean = null;
        if(getCache() != null) {
            bean = (LinkMessageBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (LinkMessageBean) getProxyFactory().getLinkMessageServiceProxy().getLinkMessage(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "LinkMessageBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve LinkMessageBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("shortLink")) {
            return bean.getShortLink();
        } else if(field.equals("message")) {
            return bean.getMessage();
        } else if(field.equals("password")) {
            return bean.getPassword();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<LinkMessage> getLinkMessages(List<String> guids) throws BaseException
    {
        log.fine("getLinkMessages()");

        // TBD: Is there a better way????
        List<LinkMessage> linkMessages = getProxyFactory().getLinkMessageServiceProxy().getLinkMessages(guids);
        if(linkMessages == null) {
            log.log(Level.WARNING, "Failed to retrieve LinkMessageBean list.");
        }

        log.finer("END");
        return linkMessages;
    }

    @Override
    public List<LinkMessage> getAllLinkMessages() throws BaseException
    {
        return getAllLinkMessages(null, null, null);
    }

    @Override
    public List<LinkMessage> getAllLinkMessages(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllLinkMessages(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<LinkMessage> linkMessages = getProxyFactory().getLinkMessageServiceProxy().getAllLinkMessages(ordering, offset, count);
        if(linkMessages == null) {
            log.log(Level.WARNING, "Failed to retrieve LinkMessageBean list.");
        }

        log.finer("END");
        return linkMessages;
    }

    @Override
    public List<String> getAllLinkMessageKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllLinkMessageKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getLinkMessageServiceProxy().getAllLinkMessageKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve LinkMessageBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty LinkMessageBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<LinkMessage> findLinkMessages(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findLinkMessages(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<LinkMessage> findLinkMessages(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("LinkMessageServiceImpl.findLinkMessages(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<LinkMessage> linkMessages = getProxyFactory().getLinkMessageServiceProxy().findLinkMessages(filter, ordering, params, values, grouping, unique, offset, count);
        if(linkMessages == null) {
            log.log(Level.WARNING, "Failed to find linkMessages for the given criterion.");
        }

        log.finer("END");
        return linkMessages;
    }

    @Override
    public List<String> findLinkMessageKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("LinkMessageServiceImpl.findLinkMessageKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getLinkMessageServiceProxy().findLinkMessageKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find LinkMessage keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty LinkMessage key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("LinkMessageServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getLinkMessageServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createLinkMessage(String shortLink, String message, String password) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        LinkMessageBean bean = new LinkMessageBean(null, shortLink, message, password);
        return createLinkMessage(bean);
    }

    @Override
    public String createLinkMessage(LinkMessage linkMessage) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //LinkMessage bean = constructLinkMessage(linkMessage);
        //return bean.getGuid();

        // Param linkMessage cannot be null.....
        if(linkMessage == null) {
            log.log(Level.INFO, "Param linkMessage is null!");
            throw new BadRequestException("Param linkMessage object is null!");
        }
        LinkMessageBean bean = null;
        if(linkMessage instanceof LinkMessageBean) {
            bean = (LinkMessageBean) linkMessage;
        } else if(linkMessage instanceof LinkMessage) {
            // bean = new LinkMessageBean(null, linkMessage.getShortLink(), linkMessage.getMessage(), linkMessage.getPassword());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new LinkMessageBean(linkMessage.getGuid(), linkMessage.getShortLink(), linkMessage.getMessage(), linkMessage.getPassword());
        } else {
            log.log(Level.WARNING, "createLinkMessage(): Arg linkMessage is of an unknown type.");
            //bean = new LinkMessageBean();
            bean = new LinkMessageBean(linkMessage.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getLinkMessageServiceProxy().createLinkMessage(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

	    // TBD:
	    if(Config.getInstance().isSearchEnabled()) {
            if(guid != null && !guid.isEmpty()) {
                log.fine("Calling addDocument()... for LinkMessage.");
                LinkMessageIndexBuilder builder = new LinkMessageIndexBuilder();
	    	    builder.addDocument(bean);
	    	}
	    }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public LinkMessage constructLinkMessage(LinkMessage linkMessage) throws BaseException
    {
        log.finer("BEGIN");

        // Param linkMessage cannot be null.....
        if(linkMessage == null) {
            log.log(Level.INFO, "Param linkMessage is null!");
            throw new BadRequestException("Param linkMessage object is null!");
        }
        LinkMessageBean bean = null;
        if(linkMessage instanceof LinkMessageBean) {
            bean = (LinkMessageBean) linkMessage;
        } else if(linkMessage instanceof LinkMessage) {
            // bean = new LinkMessageBean(null, linkMessage.getShortLink(), linkMessage.getMessage(), linkMessage.getPassword());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new LinkMessageBean(linkMessage.getGuid(), linkMessage.getShortLink(), linkMessage.getMessage(), linkMessage.getPassword());
        } else {
            log.log(Level.WARNING, "createLinkMessage(): Arg linkMessage is of an unknown type.");
            //bean = new LinkMessageBean();
            bean = new LinkMessageBean(linkMessage.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getLinkMessageServiceProxy().createLinkMessage(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

	    // TBD:
	    if(Config.getInstance().isSearchEnabled()) {
            if(guid != null && !guid.isEmpty()) {
                log.fine("Calling addDocument()... for LinkMessage.");
                LinkMessageIndexBuilder builder = new LinkMessageIndexBuilder();
	    	    builder.addDocument(bean);
	    	}
	    }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateLinkMessage(String guid, String shortLink, String message, String password) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        LinkMessageBean bean = new LinkMessageBean(guid, shortLink, message, password);
        return updateLinkMessage(bean);
    }
        
    @Override
    public Boolean updateLinkMessage(LinkMessage linkMessage) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //LinkMessage bean = refreshLinkMessage(linkMessage);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param linkMessage cannot be null.....
        if(linkMessage == null || linkMessage.getGuid() == null) {
            log.log(Level.INFO, "Param linkMessage or its guid is null!");
            throw new BadRequestException("Param linkMessage object or its guid is null!");
        }
        LinkMessageBean bean = null;
        if(linkMessage instanceof LinkMessageBean) {
            bean = (LinkMessageBean) linkMessage;
        } else {  // if(linkMessage instanceof LinkMessage)
            bean = new LinkMessageBean(linkMessage.getGuid(), linkMessage.getShortLink(), linkMessage.getMessage(), linkMessage.getPassword());
        }
        Boolean suc = getProxyFactory().getLinkMessageServiceProxy().updateLinkMessage(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

	    // TBD:
        if(Config.getInstance().isSearchEnabled()) {
            if(suc != null && suc.equals(Boolean.TRUE)) {
                log.fine("Calling addDocument()... for LinkMessage.");
	    	    LinkMessageIndexBuilder builder = new LinkMessageIndexBuilder();
	    	    builder.addDocument(bean);
	        }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public LinkMessage refreshLinkMessage(LinkMessage linkMessage) throws BaseException
    {
        log.finer("BEGIN");

        // Param linkMessage cannot be null.....
        if(linkMessage == null || linkMessage.getGuid() == null) {
            log.log(Level.INFO, "Param linkMessage or its guid is null!");
            throw new BadRequestException("Param linkMessage object or its guid is null!");
        }
        LinkMessageBean bean = null;
        if(linkMessage instanceof LinkMessageBean) {
            bean = (LinkMessageBean) linkMessage;
        } else {  // if(linkMessage instanceof LinkMessage)
            bean = new LinkMessageBean(linkMessage.getGuid(), linkMessage.getShortLink(), linkMessage.getMessage(), linkMessage.getPassword());
        }
        Boolean suc = getProxyFactory().getLinkMessageServiceProxy().updateLinkMessage(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

	    // TBD:
        if(Config.getInstance().isSearchEnabled()) {
            if(suc != null && suc.equals(Boolean.TRUE)) {
                log.fine("Calling addDocument()... for LinkMessage.");
	    	    LinkMessageIndexBuilder builder = new LinkMessageIndexBuilder();
	    	    builder.addDocument(bean);
	        }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteLinkMessage(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getLinkMessageServiceProxy().deleteLinkMessage(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }

        // TBD:
        if(Config.getInstance().isSearchEnabled()) {
            if(suc != null && suc.equals(Boolean.TRUE)) {
                log.fine("Calling removeDocument()... for LinkMessage.");
	    	    LinkMessageIndexBuilder builder = new LinkMessageIndexBuilder();
                builder.removeDocument(guid);
	        }
        }

            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            LinkMessage linkMessage = null;
            try {
                linkMessage = getProxyFactory().getLinkMessageServiceProxy().getLinkMessage(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch linkMessage with a key, " + guid);
                return false;
            }
            if(linkMessage != null) {
                String beanGuid = linkMessage.getGuid();
                Boolean suc1 = getProxyFactory().getLinkMessageServiceProxy().deleteLinkMessage(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("linkMessage with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteLinkMessage(LinkMessage linkMessage) throws BaseException
    {
        log.finer("BEGIN");

        // Param linkMessage cannot be null.....
        if(linkMessage == null || linkMessage.getGuid() == null) {
            log.log(Level.INFO, "Param linkMessage or its guid is null!");
            throw new BadRequestException("Param linkMessage object or its guid is null!");
        }
        LinkMessageBean bean = null;
        if(linkMessage instanceof LinkMessageBean) {
            bean = (LinkMessageBean) linkMessage;
        } else {  // if(linkMessage instanceof LinkMessage)
            // ????
            log.warning("linkMessage is not an instance of LinkMessageBean.");
            bean = new LinkMessageBean(linkMessage.getGuid(), linkMessage.getShortLink(), linkMessage.getMessage(), linkMessage.getPassword());
        }
        Boolean suc = getProxyFactory().getLinkMessageServiceProxy().deleteLinkMessage(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

	    // TBD:
        if(Config.getInstance().isSearchEnabled()) {
            if(suc != null && suc.equals(Boolean.TRUE)) {
                log.fine("Calling removeDocument()... for LinkMessage.");
	    	    LinkMessageIndexBuilder builder = new LinkMessageIndexBuilder();
                builder.removeDocument(bean.getGuid());
	        }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteLinkMessages(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getLinkMessageServiceProxy().deleteLinkMessages(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

	    // TBD:
        //if(Config.getInstance().isSearchEnabled()) {
        //    if(suc != null && suc.equals(Boolean.TRUE)) {
        //      log.fine("Calling removeDocument()... for LinkMessage.");
	    //	    LinkMessageIndexBuilder builder = new LinkMessageIndexBuilder();
        //      builder.removeDocument(guids);
	    //    }
        //}

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createLinkMessages(List<LinkMessage> linkMessages) throws BaseException
    {
        log.finer("BEGIN");

        if(linkMessages == null) {
            log.log(Level.WARNING, "createLinkMessages() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = linkMessages.size();
        if(size == 0) {
            log.log(Level.WARNING, "createLinkMessages() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(LinkMessage linkMessage : linkMessages) {
            String guid = createLinkMessage(linkMessage);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createLinkMessages() failed for at least one linkMessage. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateLinkMessages(List<LinkMessage> linkMessages) throws BaseException
    //{
    //}

}
