package com.cannyurl.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.LinkPassphrase;
import com.cannyurl.af.config.Config;

import com.cannyurl.af.bean.HashedPasswordStructBean;
import com.cannyurl.af.bean.GeoPointStructBean;
import com.cannyurl.af.bean.WebProfileStructBean;
import com.cannyurl.af.bean.StreetAddressStructBean;
import com.cannyurl.af.bean.SiteLogBean;
import com.cannyurl.af.bean.HelpNoticeBean;
import com.cannyurl.af.bean.TwitterCardAppInfoBean;
import com.cannyurl.af.bean.ContactInfoStructBean;
import com.cannyurl.af.bean.GeoCoordinateStructBean;
import com.cannyurl.af.bean.FullNameStructBean;
import com.cannyurl.af.bean.ExternalUserIdStructBean;
import com.cannyurl.af.bean.GaeUserStructBean;
import com.cannyurl.af.bean.AppBrandStructBean;
import com.cannyurl.af.bean.CellLatitudeLongitudeBean;
import com.cannyurl.af.bean.GaeAppStructBean;
import com.cannyurl.af.bean.ShortPassageAttributeBean;
import com.cannyurl.af.bean.KeyValuePairStructBean;
import com.cannyurl.af.bean.KeyValueRelationStructBean;
import com.cannyurl.af.bean.ExternalServiceApiKeyStructBean;
import com.cannyurl.af.bean.ReferrerInfoStructBean;
import com.cannyurl.af.bean.TwitterCardProductDataBean;
import com.cannyurl.af.bean.PagerStateStructBean;

import com.cannyurl.af.bean.LinkPassphraseBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.LinkPassphraseService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class LinkPassphraseServiceImpl implements LinkPassphraseService
{
    private static final Logger log = Logger.getLogger(LinkPassphraseServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "LinkPassphrase-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("LinkPassphrase:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public LinkPassphraseServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // LinkPassphrase related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public LinkPassphrase getLinkPassphrase(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getLinkPassphrase(): guid = " + guid);

        LinkPassphraseBean bean = null;
        if(getCache() != null) {
            bean = (LinkPassphraseBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (LinkPassphraseBean) getProxyFactory().getLinkPassphraseServiceProxy().getLinkPassphrase(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "LinkPassphraseBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve LinkPassphraseBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getLinkPassphrase(String guid, String field) throws BaseException
    {
        LinkPassphraseBean bean = null;
        if(getCache() != null) {
            bean = (LinkPassphraseBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (LinkPassphraseBean) getProxyFactory().getLinkPassphraseServiceProxy().getLinkPassphrase(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "LinkPassphraseBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve LinkPassphraseBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("shortLink")) {
            return bean.getShortLink();
        } else if(field.equals("passphrase")) {
            return bean.getPassphrase();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<LinkPassphrase> getLinkPassphrases(List<String> guids) throws BaseException
    {
        log.fine("getLinkPassphrases()");

        // TBD: Is there a better way????
        List<LinkPassphrase> linkPassphrases = getProxyFactory().getLinkPassphraseServiceProxy().getLinkPassphrases(guids);
        if(linkPassphrases == null) {
            log.log(Level.WARNING, "Failed to retrieve LinkPassphraseBean list.");
        }

        log.finer("END");
        return linkPassphrases;
    }

    @Override
    public List<LinkPassphrase> getAllLinkPassphrases() throws BaseException
    {
        return getAllLinkPassphrases(null, null, null);
    }

    @Override
    public List<LinkPassphrase> getAllLinkPassphrases(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllLinkPassphrases(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<LinkPassphrase> linkPassphrases = getProxyFactory().getLinkPassphraseServiceProxy().getAllLinkPassphrases(ordering, offset, count);
        if(linkPassphrases == null) {
            log.log(Level.WARNING, "Failed to retrieve LinkPassphraseBean list.");
        }

        log.finer("END");
        return linkPassphrases;
    }

    @Override
    public List<String> getAllLinkPassphraseKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllLinkPassphraseKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getLinkPassphraseServiceProxy().getAllLinkPassphraseKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve LinkPassphraseBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty LinkPassphraseBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<LinkPassphrase> findLinkPassphrases(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findLinkPassphrases(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<LinkPassphrase> findLinkPassphrases(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("LinkPassphraseServiceImpl.findLinkPassphrases(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<LinkPassphrase> linkPassphrases = getProxyFactory().getLinkPassphraseServiceProxy().findLinkPassphrases(filter, ordering, params, values, grouping, unique, offset, count);
        if(linkPassphrases == null) {
            log.log(Level.WARNING, "Failed to find linkPassphrases for the given criterion.");
        }

        log.finer("END");
        return linkPassphrases;
    }

    @Override
    public List<String> findLinkPassphraseKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("LinkPassphraseServiceImpl.findLinkPassphraseKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getLinkPassphraseServiceProxy().findLinkPassphraseKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find LinkPassphrase keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty LinkPassphrase key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("LinkPassphraseServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getLinkPassphraseServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createLinkPassphrase(String shortLink, String passphrase) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        LinkPassphraseBean bean = new LinkPassphraseBean(null, shortLink, passphrase);
        return createLinkPassphrase(bean);
    }

    @Override
    public String createLinkPassphrase(LinkPassphrase linkPassphrase) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //LinkPassphrase bean = constructLinkPassphrase(linkPassphrase);
        //return bean.getGuid();

        // Param linkPassphrase cannot be null.....
        if(linkPassphrase == null) {
            log.log(Level.INFO, "Param linkPassphrase is null!");
            throw new BadRequestException("Param linkPassphrase object is null!");
        }
        LinkPassphraseBean bean = null;
        if(linkPassphrase instanceof LinkPassphraseBean) {
            bean = (LinkPassphraseBean) linkPassphrase;
        } else if(linkPassphrase instanceof LinkPassphrase) {
            // bean = new LinkPassphraseBean(null, linkPassphrase.getShortLink(), linkPassphrase.getPassphrase());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new LinkPassphraseBean(linkPassphrase.getGuid(), linkPassphrase.getShortLink(), linkPassphrase.getPassphrase());
        } else {
            log.log(Level.WARNING, "createLinkPassphrase(): Arg linkPassphrase is of an unknown type.");
            //bean = new LinkPassphraseBean();
            bean = new LinkPassphraseBean(linkPassphrase.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getLinkPassphraseServiceProxy().createLinkPassphrase(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public LinkPassphrase constructLinkPassphrase(LinkPassphrase linkPassphrase) throws BaseException
    {
        log.finer("BEGIN");

        // Param linkPassphrase cannot be null.....
        if(linkPassphrase == null) {
            log.log(Level.INFO, "Param linkPassphrase is null!");
            throw new BadRequestException("Param linkPassphrase object is null!");
        }
        LinkPassphraseBean bean = null;
        if(linkPassphrase instanceof LinkPassphraseBean) {
            bean = (LinkPassphraseBean) linkPassphrase;
        } else if(linkPassphrase instanceof LinkPassphrase) {
            // bean = new LinkPassphraseBean(null, linkPassphrase.getShortLink(), linkPassphrase.getPassphrase());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new LinkPassphraseBean(linkPassphrase.getGuid(), linkPassphrase.getShortLink(), linkPassphrase.getPassphrase());
        } else {
            log.log(Level.WARNING, "createLinkPassphrase(): Arg linkPassphrase is of an unknown type.");
            //bean = new LinkPassphraseBean();
            bean = new LinkPassphraseBean(linkPassphrase.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getLinkPassphraseServiceProxy().createLinkPassphrase(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateLinkPassphrase(String guid, String shortLink, String passphrase) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        LinkPassphraseBean bean = new LinkPassphraseBean(guid, shortLink, passphrase);
        return updateLinkPassphrase(bean);
    }
        
    @Override
    public Boolean updateLinkPassphrase(LinkPassphrase linkPassphrase) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //LinkPassphrase bean = refreshLinkPassphrase(linkPassphrase);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param linkPassphrase cannot be null.....
        if(linkPassphrase == null || linkPassphrase.getGuid() == null) {
            log.log(Level.INFO, "Param linkPassphrase or its guid is null!");
            throw new BadRequestException("Param linkPassphrase object or its guid is null!");
        }
        LinkPassphraseBean bean = null;
        if(linkPassphrase instanceof LinkPassphraseBean) {
            bean = (LinkPassphraseBean) linkPassphrase;
        } else {  // if(linkPassphrase instanceof LinkPassphrase)
            bean = new LinkPassphraseBean(linkPassphrase.getGuid(), linkPassphrase.getShortLink(), linkPassphrase.getPassphrase());
        }
        Boolean suc = getProxyFactory().getLinkPassphraseServiceProxy().updateLinkPassphrase(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public LinkPassphrase refreshLinkPassphrase(LinkPassphrase linkPassphrase) throws BaseException
    {
        log.finer("BEGIN");

        // Param linkPassphrase cannot be null.....
        if(linkPassphrase == null || linkPassphrase.getGuid() == null) {
            log.log(Level.INFO, "Param linkPassphrase or its guid is null!");
            throw new BadRequestException("Param linkPassphrase object or its guid is null!");
        }
        LinkPassphraseBean bean = null;
        if(linkPassphrase instanceof LinkPassphraseBean) {
            bean = (LinkPassphraseBean) linkPassphrase;
        } else {  // if(linkPassphrase instanceof LinkPassphrase)
            bean = new LinkPassphraseBean(linkPassphrase.getGuid(), linkPassphrase.getShortLink(), linkPassphrase.getPassphrase());
        }
        Boolean suc = getProxyFactory().getLinkPassphraseServiceProxy().updateLinkPassphrase(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteLinkPassphrase(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getLinkPassphraseServiceProxy().deleteLinkPassphrase(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }
            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            LinkPassphrase linkPassphrase = null;
            try {
                linkPassphrase = getProxyFactory().getLinkPassphraseServiceProxy().getLinkPassphrase(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch linkPassphrase with a key, " + guid);
                return false;
            }
            if(linkPassphrase != null) {
                String beanGuid = linkPassphrase.getGuid();
                Boolean suc1 = getProxyFactory().getLinkPassphraseServiceProxy().deleteLinkPassphrase(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("linkPassphrase with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteLinkPassphrase(LinkPassphrase linkPassphrase) throws BaseException
    {
        log.finer("BEGIN");

        // Param linkPassphrase cannot be null.....
        if(linkPassphrase == null || linkPassphrase.getGuid() == null) {
            log.log(Level.INFO, "Param linkPassphrase or its guid is null!");
            throw new BadRequestException("Param linkPassphrase object or its guid is null!");
        }
        LinkPassphraseBean bean = null;
        if(linkPassphrase instanceof LinkPassphraseBean) {
            bean = (LinkPassphraseBean) linkPassphrase;
        } else {  // if(linkPassphrase instanceof LinkPassphrase)
            // ????
            log.warning("linkPassphrase is not an instance of LinkPassphraseBean.");
            bean = new LinkPassphraseBean(linkPassphrase.getGuid(), linkPassphrase.getShortLink(), linkPassphrase.getPassphrase());
        }
        Boolean suc = getProxyFactory().getLinkPassphraseServiceProxy().deleteLinkPassphrase(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteLinkPassphrases(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getLinkPassphraseServiceProxy().deleteLinkPassphrases(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createLinkPassphrases(List<LinkPassphrase> linkPassphrases) throws BaseException
    {
        log.finer("BEGIN");

        if(linkPassphrases == null) {
            log.log(Level.WARNING, "createLinkPassphrases() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = linkPassphrases.size();
        if(size == 0) {
            log.log(Level.WARNING, "createLinkPassphrases() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(LinkPassphrase linkPassphrase : linkPassphrases) {
            String guid = createLinkPassphrase(linkPassphrase);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createLinkPassphrases() failed for at least one linkPassphrase. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateLinkPassphrases(List<LinkPassphrase> linkPassphrases) throws BaseException
    //{
    //}

}
