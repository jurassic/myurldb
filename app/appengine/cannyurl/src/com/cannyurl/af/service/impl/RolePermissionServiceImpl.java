package com.cannyurl.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.RolePermission;
import com.cannyurl.af.config.Config;

import com.cannyurl.af.bean.HashedPasswordStructBean;
import com.cannyurl.af.bean.GeoPointStructBean;
import com.cannyurl.af.bean.WebProfileStructBean;
import com.cannyurl.af.bean.StreetAddressStructBean;
import com.cannyurl.af.bean.SiteLogBean;
import com.cannyurl.af.bean.HelpNoticeBean;
import com.cannyurl.af.bean.TwitterCardAppInfoBean;
import com.cannyurl.af.bean.ContactInfoStructBean;
import com.cannyurl.af.bean.GeoCoordinateStructBean;
import com.cannyurl.af.bean.FullNameStructBean;
import com.cannyurl.af.bean.ExternalUserIdStructBean;
import com.cannyurl.af.bean.GaeUserStructBean;
import com.cannyurl.af.bean.AppBrandStructBean;
import com.cannyurl.af.bean.CellLatitudeLongitudeBean;
import com.cannyurl.af.bean.GaeAppStructBean;
import com.cannyurl.af.bean.ShortPassageAttributeBean;
import com.cannyurl.af.bean.KeyValuePairStructBean;
import com.cannyurl.af.bean.KeyValueRelationStructBean;
import com.cannyurl.af.bean.ExternalServiceApiKeyStructBean;
import com.cannyurl.af.bean.ReferrerInfoStructBean;
import com.cannyurl.af.bean.TwitterCardProductDataBean;
import com.cannyurl.af.bean.PagerStateStructBean;

import com.cannyurl.af.bean.RolePermissionBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.RolePermissionService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class RolePermissionServiceImpl implements RolePermissionService
{
    private static final Logger log = Logger.getLogger(RolePermissionServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "RolePermission-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("RolePermission:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public RolePermissionServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // RolePermission related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public RolePermission getRolePermission(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getRolePermission(): guid = " + guid);

        RolePermissionBean bean = null;
        if(getCache() != null) {
            bean = (RolePermissionBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (RolePermissionBean) getProxyFactory().getRolePermissionServiceProxy().getRolePermission(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "RolePermissionBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve RolePermissionBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getRolePermission(String guid, String field) throws BaseException
    {
        RolePermissionBean bean = null;
        if(getCache() != null) {
            bean = (RolePermissionBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (RolePermissionBean) getProxyFactory().getRolePermissionServiceProxy().getRolePermission(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "RolePermissionBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve RolePermissionBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("role")) {
            return bean.getRole();
        } else if(field.equals("permissionName")) {
            return bean.getPermissionName();
        } else if(field.equals("resource")) {
            return bean.getResource();
        } else if(field.equals("instance")) {
            return bean.getInstance();
        } else if(field.equals("action")) {
            return bean.getAction();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<RolePermission> getRolePermissions(List<String> guids) throws BaseException
    {
        log.fine("getRolePermissions()");

        // TBD: Is there a better way????
        List<RolePermission> rolePermissions = getProxyFactory().getRolePermissionServiceProxy().getRolePermissions(guids);
        if(rolePermissions == null) {
            log.log(Level.WARNING, "Failed to retrieve RolePermissionBean list.");
        }

        log.finer("END");
        return rolePermissions;
    }

    @Override
    public List<RolePermission> getAllRolePermissions() throws BaseException
    {
        return getAllRolePermissions(null, null, null);
    }

    @Override
    public List<RolePermission> getAllRolePermissions(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllRolePermissions(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<RolePermission> rolePermissions = getProxyFactory().getRolePermissionServiceProxy().getAllRolePermissions(ordering, offset, count);
        if(rolePermissions == null) {
            log.log(Level.WARNING, "Failed to retrieve RolePermissionBean list.");
        }

        log.finer("END");
        return rolePermissions;
    }

    @Override
    public List<String> getAllRolePermissionKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllRolePermissionKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getRolePermissionServiceProxy().getAllRolePermissionKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve RolePermissionBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty RolePermissionBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<RolePermission> findRolePermissions(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findRolePermissions(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<RolePermission> findRolePermissions(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("RolePermissionServiceImpl.findRolePermissions(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<RolePermission> rolePermissions = getProxyFactory().getRolePermissionServiceProxy().findRolePermissions(filter, ordering, params, values, grouping, unique, offset, count);
        if(rolePermissions == null) {
            log.log(Level.WARNING, "Failed to find rolePermissions for the given criterion.");
        }

        log.finer("END");
        return rolePermissions;
    }

    @Override
    public List<String> findRolePermissionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("RolePermissionServiceImpl.findRolePermissionKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getRolePermissionServiceProxy().findRolePermissionKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find RolePermission keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty RolePermission key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("RolePermissionServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getRolePermissionServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createRolePermission(String role, String permissionName, String resource, String instance, String action, String status) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        RolePermissionBean bean = new RolePermissionBean(null, role, permissionName, resource, instance, action, status);
        return createRolePermission(bean);
    }

    @Override
    public String createRolePermission(RolePermission rolePermission) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //RolePermission bean = constructRolePermission(rolePermission);
        //return bean.getGuid();

        // Param rolePermission cannot be null.....
        if(rolePermission == null) {
            log.log(Level.INFO, "Param rolePermission is null!");
            throw new BadRequestException("Param rolePermission object is null!");
        }
        RolePermissionBean bean = null;
        if(rolePermission instanceof RolePermissionBean) {
            bean = (RolePermissionBean) rolePermission;
        } else if(rolePermission instanceof RolePermission) {
            // bean = new RolePermissionBean(null, rolePermission.getRole(), rolePermission.getPermissionName(), rolePermission.getResource(), rolePermission.getInstance(), rolePermission.getAction(), rolePermission.getStatus());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new RolePermissionBean(rolePermission.getGuid(), rolePermission.getRole(), rolePermission.getPermissionName(), rolePermission.getResource(), rolePermission.getInstance(), rolePermission.getAction(), rolePermission.getStatus());
        } else {
            log.log(Level.WARNING, "createRolePermission(): Arg rolePermission is of an unknown type.");
            //bean = new RolePermissionBean();
            bean = new RolePermissionBean(rolePermission.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getRolePermissionServiceProxy().createRolePermission(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public RolePermission constructRolePermission(RolePermission rolePermission) throws BaseException
    {
        log.finer("BEGIN");

        // Param rolePermission cannot be null.....
        if(rolePermission == null) {
            log.log(Level.INFO, "Param rolePermission is null!");
            throw new BadRequestException("Param rolePermission object is null!");
        }
        RolePermissionBean bean = null;
        if(rolePermission instanceof RolePermissionBean) {
            bean = (RolePermissionBean) rolePermission;
        } else if(rolePermission instanceof RolePermission) {
            // bean = new RolePermissionBean(null, rolePermission.getRole(), rolePermission.getPermissionName(), rolePermission.getResource(), rolePermission.getInstance(), rolePermission.getAction(), rolePermission.getStatus());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new RolePermissionBean(rolePermission.getGuid(), rolePermission.getRole(), rolePermission.getPermissionName(), rolePermission.getResource(), rolePermission.getInstance(), rolePermission.getAction(), rolePermission.getStatus());
        } else {
            log.log(Level.WARNING, "createRolePermission(): Arg rolePermission is of an unknown type.");
            //bean = new RolePermissionBean();
            bean = new RolePermissionBean(rolePermission.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getRolePermissionServiceProxy().createRolePermission(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateRolePermission(String guid, String role, String permissionName, String resource, String instance, String action, String status) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        RolePermissionBean bean = new RolePermissionBean(guid, role, permissionName, resource, instance, action, status);
        return updateRolePermission(bean);
    }
        
    @Override
    public Boolean updateRolePermission(RolePermission rolePermission) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //RolePermission bean = refreshRolePermission(rolePermission);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param rolePermission cannot be null.....
        if(rolePermission == null || rolePermission.getGuid() == null) {
            log.log(Level.INFO, "Param rolePermission or its guid is null!");
            throw new BadRequestException("Param rolePermission object or its guid is null!");
        }
        RolePermissionBean bean = null;
        if(rolePermission instanceof RolePermissionBean) {
            bean = (RolePermissionBean) rolePermission;
        } else {  // if(rolePermission instanceof RolePermission)
            bean = new RolePermissionBean(rolePermission.getGuid(), rolePermission.getRole(), rolePermission.getPermissionName(), rolePermission.getResource(), rolePermission.getInstance(), rolePermission.getAction(), rolePermission.getStatus());
        }
        Boolean suc = getProxyFactory().getRolePermissionServiceProxy().updateRolePermission(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public RolePermission refreshRolePermission(RolePermission rolePermission) throws BaseException
    {
        log.finer("BEGIN");

        // Param rolePermission cannot be null.....
        if(rolePermission == null || rolePermission.getGuid() == null) {
            log.log(Level.INFO, "Param rolePermission or its guid is null!");
            throw new BadRequestException("Param rolePermission object or its guid is null!");
        }
        RolePermissionBean bean = null;
        if(rolePermission instanceof RolePermissionBean) {
            bean = (RolePermissionBean) rolePermission;
        } else {  // if(rolePermission instanceof RolePermission)
            bean = new RolePermissionBean(rolePermission.getGuid(), rolePermission.getRole(), rolePermission.getPermissionName(), rolePermission.getResource(), rolePermission.getInstance(), rolePermission.getAction(), rolePermission.getStatus());
        }
        Boolean suc = getProxyFactory().getRolePermissionServiceProxy().updateRolePermission(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteRolePermission(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getRolePermissionServiceProxy().deleteRolePermission(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }
            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            RolePermission rolePermission = null;
            try {
                rolePermission = getProxyFactory().getRolePermissionServiceProxy().getRolePermission(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch rolePermission with a key, " + guid);
                return false;
            }
            if(rolePermission != null) {
                String beanGuid = rolePermission.getGuid();
                Boolean suc1 = getProxyFactory().getRolePermissionServiceProxy().deleteRolePermission(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("rolePermission with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteRolePermission(RolePermission rolePermission) throws BaseException
    {
        log.finer("BEGIN");

        // Param rolePermission cannot be null.....
        if(rolePermission == null || rolePermission.getGuid() == null) {
            log.log(Level.INFO, "Param rolePermission or its guid is null!");
            throw new BadRequestException("Param rolePermission object or its guid is null!");
        }
        RolePermissionBean bean = null;
        if(rolePermission instanceof RolePermissionBean) {
            bean = (RolePermissionBean) rolePermission;
        } else {  // if(rolePermission instanceof RolePermission)
            // ????
            log.warning("rolePermission is not an instance of RolePermissionBean.");
            bean = new RolePermissionBean(rolePermission.getGuid(), rolePermission.getRole(), rolePermission.getPermissionName(), rolePermission.getResource(), rolePermission.getInstance(), rolePermission.getAction(), rolePermission.getStatus());
        }
        Boolean suc = getProxyFactory().getRolePermissionServiceProxy().deleteRolePermission(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteRolePermissions(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getRolePermissionServiceProxy().deleteRolePermissions(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createRolePermissions(List<RolePermission> rolePermissions) throws BaseException
    {
        log.finer("BEGIN");

        if(rolePermissions == null) {
            log.log(Level.WARNING, "createRolePermissions() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = rolePermissions.size();
        if(size == 0) {
            log.log(Level.WARNING, "createRolePermissions() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(RolePermission rolePermission : rolePermissions) {
            String guid = createRolePermission(rolePermission);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createRolePermissions() failed for at least one rolePermission. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateRolePermissions(List<RolePermission> rolePermissions) throws BaseException
    //{
    //}

}
