package com.cannyurl.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.ReferrerInfoStruct;
import com.myurldb.ws.ShortLink;
import com.myurldb.ws.search.gae.ShortLinkIndexBuilder;
import com.cannyurl.af.config.Config;

import com.cannyurl.af.bean.HashedPasswordStructBean;
import com.cannyurl.af.bean.GeoPointStructBean;
import com.cannyurl.af.bean.WebProfileStructBean;
import com.cannyurl.af.bean.StreetAddressStructBean;
import com.cannyurl.af.bean.SiteLogBean;
import com.cannyurl.af.bean.HelpNoticeBean;
import com.cannyurl.af.bean.TwitterCardAppInfoBean;
import com.cannyurl.af.bean.ContactInfoStructBean;
import com.cannyurl.af.bean.GeoCoordinateStructBean;
import com.cannyurl.af.bean.FullNameStructBean;
import com.cannyurl.af.bean.ExternalUserIdStructBean;
import com.cannyurl.af.bean.GaeUserStructBean;
import com.cannyurl.af.bean.AppBrandStructBean;
import com.cannyurl.af.bean.CellLatitudeLongitudeBean;
import com.cannyurl.af.bean.GaeAppStructBean;
import com.cannyurl.af.bean.ShortPassageAttributeBean;
import com.cannyurl.af.bean.KeyValuePairStructBean;
import com.cannyurl.af.bean.KeyValueRelationStructBean;
import com.cannyurl.af.bean.ExternalServiceApiKeyStructBean;
import com.cannyurl.af.bean.ReferrerInfoStructBean;
import com.cannyurl.af.bean.TwitterCardProductDataBean;
import com.cannyurl.af.bean.PagerStateStructBean;

import com.cannyurl.af.bean.ShortLinkBean;
import com.cannyurl.af.bean.ReferrerInfoStructBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.ShortLinkService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class ShortLinkServiceImpl implements ShortLinkService
{
    private static final Logger log = Logger.getLogger(ShortLinkServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "ShortLink-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("ShortLink:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public ShortLinkServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // ShortLink related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public ShortLink getShortLink(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getShortLink(): guid = " + guid);

        ShortLinkBean bean = null;
        if(getCache() != null) {
            bean = (ShortLinkBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (ShortLinkBean) getProxyFactory().getShortLinkServiceProxy().getShortLink(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "ShortLinkBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve ShortLinkBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getShortLink(String guid, String field) throws BaseException
    {
        ShortLinkBean bean = null;
        if(getCache() != null) {
            bean = (ShortLinkBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (ShortLinkBean) getProxyFactory().getShortLinkServiceProxy().getShortLink(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "ShortLinkBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve ShortLinkBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("appClient")) {
            return bean.getAppClient();
        } else if(field.equals("clientRootDomain")) {
            return bean.getClientRootDomain();
        } else if(field.equals("owner")) {
            return bean.getOwner();
        } else if(field.equals("longUrlDomain")) {
            return bean.getLongUrlDomain();
        } else if(field.equals("longUrl")) {
            return bean.getLongUrl();
        } else if(field.equals("longUrlFull")) {
            return bean.getLongUrlFull();
        } else if(field.equals("longUrlHash")) {
            return bean.getLongUrlHash();
        } else if(field.equals("reuseExistingShortUrl")) {
            return bean.isReuseExistingShortUrl();
        } else if(field.equals("failIfShortUrlExists")) {
            return bean.isFailIfShortUrlExists();
        } else if(field.equals("domain")) {
            return bean.getDomain();
        } else if(field.equals("domainType")) {
            return bean.getDomainType();
        } else if(field.equals("usercode")) {
            return bean.getUsercode();
        } else if(field.equals("username")) {
            return bean.getUsername();
        } else if(field.equals("tokenPrefix")) {
            return bean.getTokenPrefix();
        } else if(field.equals("token")) {
            return bean.getToken();
        } else if(field.equals("tokenType")) {
            return bean.getTokenType();
        } else if(field.equals("sassyTokenType")) {
            return bean.getSassyTokenType();
        } else if(field.equals("shortUrl")) {
            return bean.getShortUrl();
        } else if(field.equals("shortPassage")) {
            return bean.getShortPassage();
        } else if(field.equals("redirectType")) {
            return bean.getRedirectType();
        } else if(field.equals("flashDuration")) {
            return bean.getFlashDuration();
        } else if(field.equals("accessType")) {
            return bean.getAccessType();
        } else if(field.equals("viewType")) {
            return bean.getViewType();
        } else if(field.equals("shareType")) {
            return bean.getShareType();
        } else if(field.equals("readOnly")) {
            return bean.isReadOnly();
        } else if(field.equals("displayMessage")) {
            return bean.getDisplayMessage();
        } else if(field.equals("shortMessage")) {
            return bean.getShortMessage();
        } else if(field.equals("keywordEnabled")) {
            return bean.isKeywordEnabled();
        } else if(field.equals("bookmarkEnabled")) {
            return bean.isBookmarkEnabled();
        } else if(field.equals("referrerInfo")) {
            return bean.getReferrerInfo();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("note")) {
            return bean.getNote();
        } else if(field.equals("expirationTime")) {
            return bean.getExpirationTime();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<ShortLink> getShortLinks(List<String> guids) throws BaseException
    {
        log.fine("getShortLinks()");

        // TBD: Is there a better way????
        List<ShortLink> shortLinks = getProxyFactory().getShortLinkServiceProxy().getShortLinks(guids);
        if(shortLinks == null) {
            log.log(Level.WARNING, "Failed to retrieve ShortLinkBean list.");
        }

        log.finer("END");
        return shortLinks;
    }

    @Override
    public List<ShortLink> getAllShortLinks() throws BaseException
    {
        return getAllShortLinks(null, null, null);
    }

    @Override
    public List<ShortLink> getAllShortLinks(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllShortLinks(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<ShortLink> shortLinks = getProxyFactory().getShortLinkServiceProxy().getAllShortLinks(ordering, offset, count);
        if(shortLinks == null) {
            log.log(Level.WARNING, "Failed to retrieve ShortLinkBean list.");
        }

        log.finer("END");
        return shortLinks;
    }

    @Override
    public List<String> getAllShortLinkKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllShortLinkKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getShortLinkServiceProxy().getAllShortLinkKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve ShortLinkBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty ShortLinkBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<ShortLink> findShortLinks(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findShortLinks(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<ShortLink> findShortLinks(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("ShortLinkServiceImpl.findShortLinks(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<ShortLink> shortLinks = getProxyFactory().getShortLinkServiceProxy().findShortLinks(filter, ordering, params, values, grouping, unique, offset, count);
        if(shortLinks == null) {
            log.log(Level.WARNING, "Failed to find shortLinks for the given criterion.");
        }

        log.finer("END");
        return shortLinks;
    }

    @Override
    public List<String> findShortLinkKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("ShortLinkServiceImpl.findShortLinkKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getShortLinkServiceProxy().findShortLinkKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find ShortLink keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty ShortLink key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("ShortLinkServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getShortLinkServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createShortLink(String appClient, String clientRootDomain, String owner, String longUrlDomain, String longUrl, String longUrlFull, String longUrlHash, Boolean reuseExistingShortUrl, Boolean failIfShortUrlExists, String domain, String domainType, String usercode, String username, String tokenPrefix, String token, String tokenType, String sassyTokenType, String shortUrl, String shortPassage, String redirectType, Long flashDuration, String accessType, String viewType, String shareType, Boolean readOnly, String displayMessage, String shortMessage, Boolean keywordEnabled, Boolean bookmarkEnabled, ReferrerInfoStruct referrerInfo, String status, String note, Long expirationTime) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        ReferrerInfoStructBean referrerInfoBean = null;
        if(referrerInfo instanceof ReferrerInfoStructBean) {
            referrerInfoBean = (ReferrerInfoStructBean) referrerInfo;
        } else if(referrerInfo instanceof ReferrerInfoStruct) {
            referrerInfoBean = new ReferrerInfoStructBean(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            referrerInfoBean = null;   // ????
        }
        ShortLinkBean bean = new ShortLinkBean(null, appClient, clientRootDomain, owner, longUrlDomain, longUrl, longUrlFull, longUrlHash, reuseExistingShortUrl, failIfShortUrlExists, domain, domainType, usercode, username, tokenPrefix, token, tokenType, sassyTokenType, shortUrl, shortPassage, redirectType, flashDuration, accessType, viewType, shareType, readOnly, displayMessage, shortMessage, keywordEnabled, bookmarkEnabled, referrerInfoBean, status, note, expirationTime);
        return createShortLink(bean);
    }

    @Override
    public String createShortLink(ShortLink shortLink) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //ShortLink bean = constructShortLink(shortLink);
        //return bean.getGuid();

        // Param shortLink cannot be null.....
        if(shortLink == null) {
            log.log(Level.INFO, "Param shortLink is null!");
            throw new BadRequestException("Param shortLink object is null!");
        }
        ShortLinkBean bean = null;
        if(shortLink instanceof ShortLinkBean) {
            bean = (ShortLinkBean) shortLink;
        } else if(shortLink instanceof ShortLink) {
            // bean = new ShortLinkBean(null, shortLink.getAppClient(), shortLink.getClientRootDomain(), shortLink.getOwner(), shortLink.getLongUrlDomain(), shortLink.getLongUrl(), shortLink.getLongUrlFull(), shortLink.getLongUrlHash(), shortLink.isReuseExistingShortUrl(), shortLink.isFailIfShortUrlExists(), shortLink.getDomain(), shortLink.getDomainType(), shortLink.getUsercode(), shortLink.getUsername(), shortLink.getTokenPrefix(), shortLink.getToken(), shortLink.getTokenType(), shortLink.getSassyTokenType(), shortLink.getShortUrl(), shortLink.getShortPassage(), shortLink.getRedirectType(), shortLink.getFlashDuration(), shortLink.getAccessType(), shortLink.getViewType(), shortLink.getShareType(), shortLink.isReadOnly(), shortLink.getDisplayMessage(), shortLink.getShortMessage(), shortLink.isKeywordEnabled(), shortLink.isBookmarkEnabled(), (ReferrerInfoStructBean) shortLink.getReferrerInfo(), shortLink.getStatus(), shortLink.getNote(), shortLink.getExpirationTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new ShortLinkBean(shortLink.getGuid(), shortLink.getAppClient(), shortLink.getClientRootDomain(), shortLink.getOwner(), shortLink.getLongUrlDomain(), shortLink.getLongUrl(), shortLink.getLongUrlFull(), shortLink.getLongUrlHash(), shortLink.isReuseExistingShortUrl(), shortLink.isFailIfShortUrlExists(), shortLink.getDomain(), shortLink.getDomainType(), shortLink.getUsercode(), shortLink.getUsername(), shortLink.getTokenPrefix(), shortLink.getToken(), shortLink.getTokenType(), shortLink.getSassyTokenType(), shortLink.getShortUrl(), shortLink.getShortPassage(), shortLink.getRedirectType(), shortLink.getFlashDuration(), shortLink.getAccessType(), shortLink.getViewType(), shortLink.getShareType(), shortLink.isReadOnly(), shortLink.getDisplayMessage(), shortLink.getShortMessage(), shortLink.isKeywordEnabled(), shortLink.isBookmarkEnabled(), (ReferrerInfoStructBean) shortLink.getReferrerInfo(), shortLink.getStatus(), shortLink.getNote(), shortLink.getExpirationTime());
        } else {
            log.log(Level.WARNING, "createShortLink(): Arg shortLink is of an unknown type.");
            //bean = new ShortLinkBean();
            bean = new ShortLinkBean(shortLink.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getShortLinkServiceProxy().createShortLink(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

	    // TBD:
	    if(Config.getInstance().isSearchEnabled()) {
            if(guid != null && !guid.isEmpty()) {
                log.fine("Calling addDocument()... for ShortLink.");
                ShortLinkIndexBuilder builder = new ShortLinkIndexBuilder();
	    	    builder.addDocument(bean);
	    	}
	    }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public ShortLink constructShortLink(ShortLink shortLink) throws BaseException
    {
        log.finer("BEGIN");

        // Param shortLink cannot be null.....
        if(shortLink == null) {
            log.log(Level.INFO, "Param shortLink is null!");
            throw new BadRequestException("Param shortLink object is null!");
        }
        ShortLinkBean bean = null;
        if(shortLink instanceof ShortLinkBean) {
            bean = (ShortLinkBean) shortLink;
        } else if(shortLink instanceof ShortLink) {
            // bean = new ShortLinkBean(null, shortLink.getAppClient(), shortLink.getClientRootDomain(), shortLink.getOwner(), shortLink.getLongUrlDomain(), shortLink.getLongUrl(), shortLink.getLongUrlFull(), shortLink.getLongUrlHash(), shortLink.isReuseExistingShortUrl(), shortLink.isFailIfShortUrlExists(), shortLink.getDomain(), shortLink.getDomainType(), shortLink.getUsercode(), shortLink.getUsername(), shortLink.getTokenPrefix(), shortLink.getToken(), shortLink.getTokenType(), shortLink.getSassyTokenType(), shortLink.getShortUrl(), shortLink.getShortPassage(), shortLink.getRedirectType(), shortLink.getFlashDuration(), shortLink.getAccessType(), shortLink.getViewType(), shortLink.getShareType(), shortLink.isReadOnly(), shortLink.getDisplayMessage(), shortLink.getShortMessage(), shortLink.isKeywordEnabled(), shortLink.isBookmarkEnabled(), (ReferrerInfoStructBean) shortLink.getReferrerInfo(), shortLink.getStatus(), shortLink.getNote(), shortLink.getExpirationTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new ShortLinkBean(shortLink.getGuid(), shortLink.getAppClient(), shortLink.getClientRootDomain(), shortLink.getOwner(), shortLink.getLongUrlDomain(), shortLink.getLongUrl(), shortLink.getLongUrlFull(), shortLink.getLongUrlHash(), shortLink.isReuseExistingShortUrl(), shortLink.isFailIfShortUrlExists(), shortLink.getDomain(), shortLink.getDomainType(), shortLink.getUsercode(), shortLink.getUsername(), shortLink.getTokenPrefix(), shortLink.getToken(), shortLink.getTokenType(), shortLink.getSassyTokenType(), shortLink.getShortUrl(), shortLink.getShortPassage(), shortLink.getRedirectType(), shortLink.getFlashDuration(), shortLink.getAccessType(), shortLink.getViewType(), shortLink.getShareType(), shortLink.isReadOnly(), shortLink.getDisplayMessage(), shortLink.getShortMessage(), shortLink.isKeywordEnabled(), shortLink.isBookmarkEnabled(), (ReferrerInfoStructBean) shortLink.getReferrerInfo(), shortLink.getStatus(), shortLink.getNote(), shortLink.getExpirationTime());
        } else {
            log.log(Level.WARNING, "createShortLink(): Arg shortLink is of an unknown type.");
            //bean = new ShortLinkBean();
            bean = new ShortLinkBean(shortLink.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getShortLinkServiceProxy().createShortLink(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

	    // TBD:
	    if(Config.getInstance().isSearchEnabled()) {
            if(guid != null && !guid.isEmpty()) {
                log.fine("Calling addDocument()... for ShortLink.");
                ShortLinkIndexBuilder builder = new ShortLinkIndexBuilder();
	    	    builder.addDocument(bean);
	    	}
	    }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateShortLink(String guid, String appClient, String clientRootDomain, String owner, String longUrlDomain, String longUrl, String longUrlFull, String longUrlHash, Boolean reuseExistingShortUrl, Boolean failIfShortUrlExists, String domain, String domainType, String usercode, String username, String tokenPrefix, String token, String tokenType, String sassyTokenType, String shortUrl, String shortPassage, String redirectType, Long flashDuration, String accessType, String viewType, String shareType, Boolean readOnly, String displayMessage, String shortMessage, Boolean keywordEnabled, Boolean bookmarkEnabled, ReferrerInfoStruct referrerInfo, String status, String note, Long expirationTime) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        ReferrerInfoStructBean referrerInfoBean = null;
        if(referrerInfo instanceof ReferrerInfoStructBean) {
            referrerInfoBean = (ReferrerInfoStructBean) referrerInfo;
        } else if(referrerInfo instanceof ReferrerInfoStruct) {
            referrerInfoBean = new ReferrerInfoStructBean(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            referrerInfoBean = null;   // ????
        }
        ShortLinkBean bean = new ShortLinkBean(guid, appClient, clientRootDomain, owner, longUrlDomain, longUrl, longUrlFull, longUrlHash, reuseExistingShortUrl, failIfShortUrlExists, domain, domainType, usercode, username, tokenPrefix, token, tokenType, sassyTokenType, shortUrl, shortPassage, redirectType, flashDuration, accessType, viewType, shareType, readOnly, displayMessage, shortMessage, keywordEnabled, bookmarkEnabled, referrerInfoBean, status, note, expirationTime);
        return updateShortLink(bean);
    }
        
    @Override
    public Boolean updateShortLink(ShortLink shortLink) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //ShortLink bean = refreshShortLink(shortLink);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param shortLink cannot be null.....
        if(shortLink == null || shortLink.getGuid() == null) {
            log.log(Level.INFO, "Param shortLink or its guid is null!");
            throw new BadRequestException("Param shortLink object or its guid is null!");
        }
        ShortLinkBean bean = null;
        if(shortLink instanceof ShortLinkBean) {
            bean = (ShortLinkBean) shortLink;
        } else {  // if(shortLink instanceof ShortLink)
            bean = new ShortLinkBean(shortLink.getGuid(), shortLink.getAppClient(), shortLink.getClientRootDomain(), shortLink.getOwner(), shortLink.getLongUrlDomain(), shortLink.getLongUrl(), shortLink.getLongUrlFull(), shortLink.getLongUrlHash(), shortLink.isReuseExistingShortUrl(), shortLink.isFailIfShortUrlExists(), shortLink.getDomain(), shortLink.getDomainType(), shortLink.getUsercode(), shortLink.getUsername(), shortLink.getTokenPrefix(), shortLink.getToken(), shortLink.getTokenType(), shortLink.getSassyTokenType(), shortLink.getShortUrl(), shortLink.getShortPassage(), shortLink.getRedirectType(), shortLink.getFlashDuration(), shortLink.getAccessType(), shortLink.getViewType(), shortLink.getShareType(), shortLink.isReadOnly(), shortLink.getDisplayMessage(), shortLink.getShortMessage(), shortLink.isKeywordEnabled(), shortLink.isBookmarkEnabled(), (ReferrerInfoStructBean) shortLink.getReferrerInfo(), shortLink.getStatus(), shortLink.getNote(), shortLink.getExpirationTime());
        }
        Boolean suc = getProxyFactory().getShortLinkServiceProxy().updateShortLink(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

	    // TBD:
        if(Config.getInstance().isSearchEnabled()) {
            if(suc != null && suc.equals(Boolean.TRUE)) {
                log.fine("Calling addDocument()... for ShortLink.");
	    	    ShortLinkIndexBuilder builder = new ShortLinkIndexBuilder();
	    	    builder.addDocument(bean);
	        }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public ShortLink refreshShortLink(ShortLink shortLink) throws BaseException
    {
        log.finer("BEGIN");

        // Param shortLink cannot be null.....
        if(shortLink == null || shortLink.getGuid() == null) {
            log.log(Level.INFO, "Param shortLink or its guid is null!");
            throw new BadRequestException("Param shortLink object or its guid is null!");
        }
        ShortLinkBean bean = null;
        if(shortLink instanceof ShortLinkBean) {
            bean = (ShortLinkBean) shortLink;
        } else {  // if(shortLink instanceof ShortLink)
            bean = new ShortLinkBean(shortLink.getGuid(), shortLink.getAppClient(), shortLink.getClientRootDomain(), shortLink.getOwner(), shortLink.getLongUrlDomain(), shortLink.getLongUrl(), shortLink.getLongUrlFull(), shortLink.getLongUrlHash(), shortLink.isReuseExistingShortUrl(), shortLink.isFailIfShortUrlExists(), shortLink.getDomain(), shortLink.getDomainType(), shortLink.getUsercode(), shortLink.getUsername(), shortLink.getTokenPrefix(), shortLink.getToken(), shortLink.getTokenType(), shortLink.getSassyTokenType(), shortLink.getShortUrl(), shortLink.getShortPassage(), shortLink.getRedirectType(), shortLink.getFlashDuration(), shortLink.getAccessType(), shortLink.getViewType(), shortLink.getShareType(), shortLink.isReadOnly(), shortLink.getDisplayMessage(), shortLink.getShortMessage(), shortLink.isKeywordEnabled(), shortLink.isBookmarkEnabled(), (ReferrerInfoStructBean) shortLink.getReferrerInfo(), shortLink.getStatus(), shortLink.getNote(), shortLink.getExpirationTime());
        }
        Boolean suc = getProxyFactory().getShortLinkServiceProxy().updateShortLink(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

	    // TBD:
        if(Config.getInstance().isSearchEnabled()) {
            if(suc != null && suc.equals(Boolean.TRUE)) {
                log.fine("Calling addDocument()... for ShortLink.");
	    	    ShortLinkIndexBuilder builder = new ShortLinkIndexBuilder();
	    	    builder.addDocument(bean);
	        }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteShortLink(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getShortLinkServiceProxy().deleteShortLink(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }

        // TBD:
        if(Config.getInstance().isSearchEnabled()) {
            if(suc != null && suc.equals(Boolean.TRUE)) {
                log.fine("Calling removeDocument()... for ShortLink.");
	    	    ShortLinkIndexBuilder builder = new ShortLinkIndexBuilder();
                builder.removeDocument(guid);
	        }
        }

            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            ShortLink shortLink = null;
            try {
                shortLink = getProxyFactory().getShortLinkServiceProxy().getShortLink(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch shortLink with a key, " + guid);
                return false;
            }
            if(shortLink != null) {
                String beanGuid = shortLink.getGuid();
                Boolean suc1 = getProxyFactory().getShortLinkServiceProxy().deleteShortLink(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("shortLink with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteShortLink(ShortLink shortLink) throws BaseException
    {
        log.finer("BEGIN");

        // Param shortLink cannot be null.....
        if(shortLink == null || shortLink.getGuid() == null) {
            log.log(Level.INFO, "Param shortLink or its guid is null!");
            throw new BadRequestException("Param shortLink object or its guid is null!");
        }
        ShortLinkBean bean = null;
        if(shortLink instanceof ShortLinkBean) {
            bean = (ShortLinkBean) shortLink;
        } else {  // if(shortLink instanceof ShortLink)
            // ????
            log.warning("shortLink is not an instance of ShortLinkBean.");
            bean = new ShortLinkBean(shortLink.getGuid(), shortLink.getAppClient(), shortLink.getClientRootDomain(), shortLink.getOwner(), shortLink.getLongUrlDomain(), shortLink.getLongUrl(), shortLink.getLongUrlFull(), shortLink.getLongUrlHash(), shortLink.isReuseExistingShortUrl(), shortLink.isFailIfShortUrlExists(), shortLink.getDomain(), shortLink.getDomainType(), shortLink.getUsercode(), shortLink.getUsername(), shortLink.getTokenPrefix(), shortLink.getToken(), shortLink.getTokenType(), shortLink.getSassyTokenType(), shortLink.getShortUrl(), shortLink.getShortPassage(), shortLink.getRedirectType(), shortLink.getFlashDuration(), shortLink.getAccessType(), shortLink.getViewType(), shortLink.getShareType(), shortLink.isReadOnly(), shortLink.getDisplayMessage(), shortLink.getShortMessage(), shortLink.isKeywordEnabled(), shortLink.isBookmarkEnabled(), (ReferrerInfoStructBean) shortLink.getReferrerInfo(), shortLink.getStatus(), shortLink.getNote(), shortLink.getExpirationTime());
        }
        Boolean suc = getProxyFactory().getShortLinkServiceProxy().deleteShortLink(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

	    // TBD:
        if(Config.getInstance().isSearchEnabled()) {
            if(suc != null && suc.equals(Boolean.TRUE)) {
                log.fine("Calling removeDocument()... for ShortLink.");
	    	    ShortLinkIndexBuilder builder = new ShortLinkIndexBuilder();
                builder.removeDocument(bean.getGuid());
	        }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteShortLinks(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getShortLinkServiceProxy().deleteShortLinks(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

	    // TBD:
        //if(Config.getInstance().isSearchEnabled()) {
        //    if(suc != null && suc.equals(Boolean.TRUE)) {
        //      log.fine("Calling removeDocument()... for ShortLink.");
	    //	    ShortLinkIndexBuilder builder = new ShortLinkIndexBuilder();
        //      builder.removeDocument(guids);
	    //    }
        //}

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createShortLinks(List<ShortLink> shortLinks) throws BaseException
    {
        log.finer("BEGIN");

        if(shortLinks == null) {
            log.log(Level.WARNING, "createShortLinks() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = shortLinks.size();
        if(size == 0) {
            log.log(Level.WARNING, "createShortLinks() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(ShortLink shortLink : shortLinks) {
            String guid = createShortLink(shortLink);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createShortLinks() failed for at least one shortLink. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateShortLinks(List<ShortLink> shortLinks) throws BaseException
    //{
    //}

}
