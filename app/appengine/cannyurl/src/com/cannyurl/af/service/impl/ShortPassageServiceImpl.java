package com.cannyurl.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.ShortPassageAttribute;
import com.myurldb.ws.ShortPassage;
import com.myurldb.ws.search.gae.ShortPassageIndexBuilder;
import com.cannyurl.af.config.Config;

import com.cannyurl.af.bean.HashedPasswordStructBean;
import com.cannyurl.af.bean.GeoPointStructBean;
import com.cannyurl.af.bean.WebProfileStructBean;
import com.cannyurl.af.bean.StreetAddressStructBean;
import com.cannyurl.af.bean.SiteLogBean;
import com.cannyurl.af.bean.HelpNoticeBean;
import com.cannyurl.af.bean.TwitterCardAppInfoBean;
import com.cannyurl.af.bean.ContactInfoStructBean;
import com.cannyurl.af.bean.GeoCoordinateStructBean;
import com.cannyurl.af.bean.FullNameStructBean;
import com.cannyurl.af.bean.ExternalUserIdStructBean;
import com.cannyurl.af.bean.GaeUserStructBean;
import com.cannyurl.af.bean.AppBrandStructBean;
import com.cannyurl.af.bean.CellLatitudeLongitudeBean;
import com.cannyurl.af.bean.GaeAppStructBean;
import com.cannyurl.af.bean.ShortPassageAttributeBean;
import com.cannyurl.af.bean.KeyValuePairStructBean;
import com.cannyurl.af.bean.KeyValueRelationStructBean;
import com.cannyurl.af.bean.ExternalServiceApiKeyStructBean;
import com.cannyurl.af.bean.ReferrerInfoStructBean;
import com.cannyurl.af.bean.TwitterCardProductDataBean;
import com.cannyurl.af.bean.PagerStateStructBean;

import com.cannyurl.af.bean.ShortPassageBean;
import com.cannyurl.af.bean.ShortPassageAttributeBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.ShortPassageService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class ShortPassageServiceImpl implements ShortPassageService
{
    private static final Logger log = Logger.getLogger(ShortPassageServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "ShortPassage-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("ShortPassage:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public ShortPassageServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // ShortPassage related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public ShortPassage getShortPassage(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getShortPassage(): guid = " + guid);

        ShortPassageBean bean = null;
        if(getCache() != null) {
            bean = (ShortPassageBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (ShortPassageBean) getProxyFactory().getShortPassageServiceProxy().getShortPassage(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "ShortPassageBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve ShortPassageBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getShortPassage(String guid, String field) throws BaseException
    {
        ShortPassageBean bean = null;
        if(getCache() != null) {
            bean = (ShortPassageBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (ShortPassageBean) getProxyFactory().getShortPassageServiceProxy().getShortPassage(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "ShortPassageBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve ShortPassageBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("owner")) {
            return bean.getOwner();
        } else if(field.equals("longText")) {
            return bean.getLongText();
        } else if(field.equals("shortText")) {
            return bean.getShortText();
        } else if(field.equals("attribute")) {
            return bean.getAttribute();
        } else if(field.equals("readOnly")) {
            return bean.isReadOnly();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("note")) {
            return bean.getNote();
        } else if(field.equals("expirationTime")) {
            return bean.getExpirationTime();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<ShortPassage> getShortPassages(List<String> guids) throws BaseException
    {
        log.fine("getShortPassages()");

        // TBD: Is there a better way????
        List<ShortPassage> shortPassages = getProxyFactory().getShortPassageServiceProxy().getShortPassages(guids);
        if(shortPassages == null) {
            log.log(Level.WARNING, "Failed to retrieve ShortPassageBean list.");
        }

        log.finer("END");
        return shortPassages;
    }

    @Override
    public List<ShortPassage> getAllShortPassages() throws BaseException
    {
        return getAllShortPassages(null, null, null);
    }

    @Override
    public List<ShortPassage> getAllShortPassages(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllShortPassages(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<ShortPassage> shortPassages = getProxyFactory().getShortPassageServiceProxy().getAllShortPassages(ordering, offset, count);
        if(shortPassages == null) {
            log.log(Level.WARNING, "Failed to retrieve ShortPassageBean list.");
        }

        log.finer("END");
        return shortPassages;
    }

    @Override
    public List<String> getAllShortPassageKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllShortPassageKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getShortPassageServiceProxy().getAllShortPassageKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve ShortPassageBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty ShortPassageBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<ShortPassage> findShortPassages(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findShortPassages(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<ShortPassage> findShortPassages(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("ShortPassageServiceImpl.findShortPassages(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<ShortPassage> shortPassages = getProxyFactory().getShortPassageServiceProxy().findShortPassages(filter, ordering, params, values, grouping, unique, offset, count);
        if(shortPassages == null) {
            log.log(Level.WARNING, "Failed to find shortPassages for the given criterion.");
        }

        log.finer("END");
        return shortPassages;
    }

    @Override
    public List<String> findShortPassageKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("ShortPassageServiceImpl.findShortPassageKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getShortPassageServiceProxy().findShortPassageKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find ShortPassage keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty ShortPassage key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("ShortPassageServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getShortPassageServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createShortPassage(String owner, String longText, String shortText, ShortPassageAttribute attribute, Boolean readOnly, String status, String note, Long expirationTime) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        ShortPassageAttributeBean attributeBean = null;
        if(attribute instanceof ShortPassageAttributeBean) {
            attributeBean = (ShortPassageAttributeBean) attribute;
        } else if(attribute instanceof ShortPassageAttribute) {
            attributeBean = new ShortPassageAttributeBean(attribute.getDomain(), attribute.getTokenType(), attribute.getDisplayMessage(), attribute.getRedirectType(), attribute.getFlashDuration(), attribute.getAccessType(), attribute.getViewType(), attribute.getShareType());
        } else {
            attributeBean = null;   // ????
        }
        ShortPassageBean bean = new ShortPassageBean(null, owner, longText, shortText, attributeBean, readOnly, status, note, expirationTime);
        return createShortPassage(bean);
    }

    @Override
    public String createShortPassage(ShortPassage shortPassage) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //ShortPassage bean = constructShortPassage(shortPassage);
        //return bean.getGuid();

        // Param shortPassage cannot be null.....
        if(shortPassage == null) {
            log.log(Level.INFO, "Param shortPassage is null!");
            throw new BadRequestException("Param shortPassage object is null!");
        }
        ShortPassageBean bean = null;
        if(shortPassage instanceof ShortPassageBean) {
            bean = (ShortPassageBean) shortPassage;
        } else if(shortPassage instanceof ShortPassage) {
            // bean = new ShortPassageBean(null, shortPassage.getOwner(), shortPassage.getLongText(), shortPassage.getShortText(), (ShortPassageAttributeBean) shortPassage.getAttribute(), shortPassage.isReadOnly(), shortPassage.getStatus(), shortPassage.getNote(), shortPassage.getExpirationTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new ShortPassageBean(shortPassage.getGuid(), shortPassage.getOwner(), shortPassage.getLongText(), shortPassage.getShortText(), (ShortPassageAttributeBean) shortPassage.getAttribute(), shortPassage.isReadOnly(), shortPassage.getStatus(), shortPassage.getNote(), shortPassage.getExpirationTime());
        } else {
            log.log(Level.WARNING, "createShortPassage(): Arg shortPassage is of an unknown type.");
            //bean = new ShortPassageBean();
            bean = new ShortPassageBean(shortPassage.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getShortPassageServiceProxy().createShortPassage(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

	    // TBD:
	    if(Config.getInstance().isSearchEnabled()) {
            if(guid != null && !guid.isEmpty()) {
                log.fine("Calling addDocument()... for ShortPassage.");
                ShortPassageIndexBuilder builder = new ShortPassageIndexBuilder();
	    	    builder.addDocument(bean);
	    	}
	    }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public ShortPassage constructShortPassage(ShortPassage shortPassage) throws BaseException
    {
        log.finer("BEGIN");

        // Param shortPassage cannot be null.....
        if(shortPassage == null) {
            log.log(Level.INFO, "Param shortPassage is null!");
            throw new BadRequestException("Param shortPassage object is null!");
        }
        ShortPassageBean bean = null;
        if(shortPassage instanceof ShortPassageBean) {
            bean = (ShortPassageBean) shortPassage;
        } else if(shortPassage instanceof ShortPassage) {
            // bean = new ShortPassageBean(null, shortPassage.getOwner(), shortPassage.getLongText(), shortPassage.getShortText(), (ShortPassageAttributeBean) shortPassage.getAttribute(), shortPassage.isReadOnly(), shortPassage.getStatus(), shortPassage.getNote(), shortPassage.getExpirationTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new ShortPassageBean(shortPassage.getGuid(), shortPassage.getOwner(), shortPassage.getLongText(), shortPassage.getShortText(), (ShortPassageAttributeBean) shortPassage.getAttribute(), shortPassage.isReadOnly(), shortPassage.getStatus(), shortPassage.getNote(), shortPassage.getExpirationTime());
        } else {
            log.log(Level.WARNING, "createShortPassage(): Arg shortPassage is of an unknown type.");
            //bean = new ShortPassageBean();
            bean = new ShortPassageBean(shortPassage.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getShortPassageServiceProxy().createShortPassage(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

	    // TBD:
	    if(Config.getInstance().isSearchEnabled()) {
            if(guid != null && !guid.isEmpty()) {
                log.fine("Calling addDocument()... for ShortPassage.");
                ShortPassageIndexBuilder builder = new ShortPassageIndexBuilder();
	    	    builder.addDocument(bean);
	    	}
	    }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateShortPassage(String guid, String owner, String longText, String shortText, ShortPassageAttribute attribute, Boolean readOnly, String status, String note, Long expirationTime) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        ShortPassageAttributeBean attributeBean = null;
        if(attribute instanceof ShortPassageAttributeBean) {
            attributeBean = (ShortPassageAttributeBean) attribute;
        } else if(attribute instanceof ShortPassageAttribute) {
            attributeBean = new ShortPassageAttributeBean(attribute.getDomain(), attribute.getTokenType(), attribute.getDisplayMessage(), attribute.getRedirectType(), attribute.getFlashDuration(), attribute.getAccessType(), attribute.getViewType(), attribute.getShareType());
        } else {
            attributeBean = null;   // ????
        }
        ShortPassageBean bean = new ShortPassageBean(guid, owner, longText, shortText, attributeBean, readOnly, status, note, expirationTime);
        return updateShortPassage(bean);
    }
        
    @Override
    public Boolean updateShortPassage(ShortPassage shortPassage) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //ShortPassage bean = refreshShortPassage(shortPassage);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param shortPassage cannot be null.....
        if(shortPassage == null || shortPassage.getGuid() == null) {
            log.log(Level.INFO, "Param shortPassage or its guid is null!");
            throw new BadRequestException("Param shortPassage object or its guid is null!");
        }
        ShortPassageBean bean = null;
        if(shortPassage instanceof ShortPassageBean) {
            bean = (ShortPassageBean) shortPassage;
        } else {  // if(shortPassage instanceof ShortPassage)
            bean = new ShortPassageBean(shortPassage.getGuid(), shortPassage.getOwner(), shortPassage.getLongText(), shortPassage.getShortText(), (ShortPassageAttributeBean) shortPassage.getAttribute(), shortPassage.isReadOnly(), shortPassage.getStatus(), shortPassage.getNote(), shortPassage.getExpirationTime());
        }
        Boolean suc = getProxyFactory().getShortPassageServiceProxy().updateShortPassage(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

	    // TBD:
        if(Config.getInstance().isSearchEnabled()) {
            if(suc != null && suc.equals(Boolean.TRUE)) {
                log.fine("Calling addDocument()... for ShortPassage.");
	    	    ShortPassageIndexBuilder builder = new ShortPassageIndexBuilder();
	    	    builder.addDocument(bean);
	        }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public ShortPassage refreshShortPassage(ShortPassage shortPassage) throws BaseException
    {
        log.finer("BEGIN");

        // Param shortPassage cannot be null.....
        if(shortPassage == null || shortPassage.getGuid() == null) {
            log.log(Level.INFO, "Param shortPassage or its guid is null!");
            throw new BadRequestException("Param shortPassage object or its guid is null!");
        }
        ShortPassageBean bean = null;
        if(shortPassage instanceof ShortPassageBean) {
            bean = (ShortPassageBean) shortPassage;
        } else {  // if(shortPassage instanceof ShortPassage)
            bean = new ShortPassageBean(shortPassage.getGuid(), shortPassage.getOwner(), shortPassage.getLongText(), shortPassage.getShortText(), (ShortPassageAttributeBean) shortPassage.getAttribute(), shortPassage.isReadOnly(), shortPassage.getStatus(), shortPassage.getNote(), shortPassage.getExpirationTime());
        }
        Boolean suc = getProxyFactory().getShortPassageServiceProxy().updateShortPassage(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

	    // TBD:
        if(Config.getInstance().isSearchEnabled()) {
            if(suc != null && suc.equals(Boolean.TRUE)) {
                log.fine("Calling addDocument()... for ShortPassage.");
	    	    ShortPassageIndexBuilder builder = new ShortPassageIndexBuilder();
	    	    builder.addDocument(bean);
	        }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteShortPassage(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getShortPassageServiceProxy().deleteShortPassage(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }

        // TBD:
        if(Config.getInstance().isSearchEnabled()) {
            if(suc != null && suc.equals(Boolean.TRUE)) {
                log.fine("Calling removeDocument()... for ShortPassage.");
	    	    ShortPassageIndexBuilder builder = new ShortPassageIndexBuilder();
                builder.removeDocument(guid);
	        }
        }

            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            ShortPassage shortPassage = null;
            try {
                shortPassage = getProxyFactory().getShortPassageServiceProxy().getShortPassage(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch shortPassage with a key, " + guid);
                return false;
            }
            if(shortPassage != null) {
                String beanGuid = shortPassage.getGuid();
                Boolean suc1 = getProxyFactory().getShortPassageServiceProxy().deleteShortPassage(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("shortPassage with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteShortPassage(ShortPassage shortPassage) throws BaseException
    {
        log.finer("BEGIN");

        // Param shortPassage cannot be null.....
        if(shortPassage == null || shortPassage.getGuid() == null) {
            log.log(Level.INFO, "Param shortPassage or its guid is null!");
            throw new BadRequestException("Param shortPassage object or its guid is null!");
        }
        ShortPassageBean bean = null;
        if(shortPassage instanceof ShortPassageBean) {
            bean = (ShortPassageBean) shortPassage;
        } else {  // if(shortPassage instanceof ShortPassage)
            // ????
            log.warning("shortPassage is not an instance of ShortPassageBean.");
            bean = new ShortPassageBean(shortPassage.getGuid(), shortPassage.getOwner(), shortPassage.getLongText(), shortPassage.getShortText(), (ShortPassageAttributeBean) shortPassage.getAttribute(), shortPassage.isReadOnly(), shortPassage.getStatus(), shortPassage.getNote(), shortPassage.getExpirationTime());
        }
        Boolean suc = getProxyFactory().getShortPassageServiceProxy().deleteShortPassage(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

	    // TBD:
        if(Config.getInstance().isSearchEnabled()) {
            if(suc != null && suc.equals(Boolean.TRUE)) {
                log.fine("Calling removeDocument()... for ShortPassage.");
	    	    ShortPassageIndexBuilder builder = new ShortPassageIndexBuilder();
                builder.removeDocument(bean.getGuid());
	        }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteShortPassages(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getShortPassageServiceProxy().deleteShortPassages(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

	    // TBD:
        //if(Config.getInstance().isSearchEnabled()) {
        //    if(suc != null && suc.equals(Boolean.TRUE)) {
        //      log.fine("Calling removeDocument()... for ShortPassage.");
	    //	    ShortPassageIndexBuilder builder = new ShortPassageIndexBuilder();
        //      builder.removeDocument(guids);
	    //    }
        //}

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createShortPassages(List<ShortPassage> shortPassages) throws BaseException
    {
        log.finer("BEGIN");

        if(shortPassages == null) {
            log.log(Level.WARNING, "createShortPassages() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = shortPassages.size();
        if(size == 0) {
            log.log(Level.WARNING, "createShortPassages() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(ShortPassage shortPassage : shortPassages) {
            String guid = createShortPassage(shortPassage);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createShortPassages() failed for at least one shortPassage. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateShortPassages(List<ShortPassage> shortPassages) throws BaseException
    //{
    //}

}
