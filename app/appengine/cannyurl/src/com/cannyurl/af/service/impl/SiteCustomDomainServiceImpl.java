package com.cannyurl.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.SiteCustomDomain;
import com.cannyurl.af.config.Config;

import com.cannyurl.af.bean.HashedPasswordStructBean;
import com.cannyurl.af.bean.GeoPointStructBean;
import com.cannyurl.af.bean.WebProfileStructBean;
import com.cannyurl.af.bean.StreetAddressStructBean;
import com.cannyurl.af.bean.SiteLogBean;
import com.cannyurl.af.bean.HelpNoticeBean;
import com.cannyurl.af.bean.TwitterCardAppInfoBean;
import com.cannyurl.af.bean.ContactInfoStructBean;
import com.cannyurl.af.bean.GeoCoordinateStructBean;
import com.cannyurl.af.bean.FullNameStructBean;
import com.cannyurl.af.bean.ExternalUserIdStructBean;
import com.cannyurl.af.bean.GaeUserStructBean;
import com.cannyurl.af.bean.AppBrandStructBean;
import com.cannyurl.af.bean.CellLatitudeLongitudeBean;
import com.cannyurl.af.bean.GaeAppStructBean;
import com.cannyurl.af.bean.ShortPassageAttributeBean;
import com.cannyurl.af.bean.KeyValuePairStructBean;
import com.cannyurl.af.bean.KeyValueRelationStructBean;
import com.cannyurl.af.bean.ExternalServiceApiKeyStructBean;
import com.cannyurl.af.bean.ReferrerInfoStructBean;
import com.cannyurl.af.bean.TwitterCardProductDataBean;
import com.cannyurl.af.bean.PagerStateStructBean;

import com.cannyurl.af.bean.SiteCustomDomainBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.SiteCustomDomainService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class SiteCustomDomainServiceImpl implements SiteCustomDomainService
{
    private static final Logger log = Logger.getLogger(SiteCustomDomainServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "SiteCustomDomain-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("SiteCustomDomain:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public SiteCustomDomainServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // SiteCustomDomain related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public SiteCustomDomain getSiteCustomDomain(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getSiteCustomDomain(): guid = " + guid);

        SiteCustomDomainBean bean = null;
        if(getCache() != null) {
            bean = (SiteCustomDomainBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (SiteCustomDomainBean) getProxyFactory().getSiteCustomDomainServiceProxy().getSiteCustomDomain(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "SiteCustomDomainBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve SiteCustomDomainBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getSiteCustomDomain(String guid, String field) throws BaseException
    {
        SiteCustomDomainBean bean = null;
        if(getCache() != null) {
            bean = (SiteCustomDomainBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (SiteCustomDomainBean) getProxyFactory().getSiteCustomDomainServiceProxy().getSiteCustomDomain(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "SiteCustomDomainBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve SiteCustomDomainBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("siteDomain")) {
            return bean.getSiteDomain();
        } else if(field.equals("domain")) {
            return bean.getDomain();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<SiteCustomDomain> getSiteCustomDomains(List<String> guids) throws BaseException
    {
        log.fine("getSiteCustomDomains()");

        // TBD: Is there a better way????
        List<SiteCustomDomain> siteCustomDomains = getProxyFactory().getSiteCustomDomainServiceProxy().getSiteCustomDomains(guids);
        if(siteCustomDomains == null) {
            log.log(Level.WARNING, "Failed to retrieve SiteCustomDomainBean list.");
        }

        log.finer("END");
        return siteCustomDomains;
    }

    @Override
    public List<SiteCustomDomain> getAllSiteCustomDomains() throws BaseException
    {
        return getAllSiteCustomDomains(null, null, null);
    }

    @Override
    public List<SiteCustomDomain> getAllSiteCustomDomains(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllSiteCustomDomains(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<SiteCustomDomain> siteCustomDomains = getProxyFactory().getSiteCustomDomainServiceProxy().getAllSiteCustomDomains(ordering, offset, count);
        if(siteCustomDomains == null) {
            log.log(Level.WARNING, "Failed to retrieve SiteCustomDomainBean list.");
        }

        log.finer("END");
        return siteCustomDomains;
    }

    @Override
    public List<String> getAllSiteCustomDomainKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllSiteCustomDomainKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getSiteCustomDomainServiceProxy().getAllSiteCustomDomainKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve SiteCustomDomainBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty SiteCustomDomainBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<SiteCustomDomain> findSiteCustomDomains(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findSiteCustomDomains(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<SiteCustomDomain> findSiteCustomDomains(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("SiteCustomDomainServiceImpl.findSiteCustomDomains(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<SiteCustomDomain> siteCustomDomains = getProxyFactory().getSiteCustomDomainServiceProxy().findSiteCustomDomains(filter, ordering, params, values, grouping, unique, offset, count);
        if(siteCustomDomains == null) {
            log.log(Level.WARNING, "Failed to find siteCustomDomains for the given criterion.");
        }

        log.finer("END");
        return siteCustomDomains;
    }

    @Override
    public List<String> findSiteCustomDomainKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("SiteCustomDomainServiceImpl.findSiteCustomDomainKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getSiteCustomDomainServiceProxy().findSiteCustomDomainKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find SiteCustomDomain keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty SiteCustomDomain key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("SiteCustomDomainServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getSiteCustomDomainServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createSiteCustomDomain(String siteDomain, String domain, String status) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        SiteCustomDomainBean bean = new SiteCustomDomainBean(null, siteDomain, domain, status);
        return createSiteCustomDomain(bean);
    }

    @Override
    public String createSiteCustomDomain(SiteCustomDomain siteCustomDomain) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //SiteCustomDomain bean = constructSiteCustomDomain(siteCustomDomain);
        //return bean.getGuid();

        // Param siteCustomDomain cannot be null.....
        if(siteCustomDomain == null) {
            log.log(Level.INFO, "Param siteCustomDomain is null!");
            throw new BadRequestException("Param siteCustomDomain object is null!");
        }
        SiteCustomDomainBean bean = null;
        if(siteCustomDomain instanceof SiteCustomDomainBean) {
            bean = (SiteCustomDomainBean) siteCustomDomain;
        } else if(siteCustomDomain instanceof SiteCustomDomain) {
            // bean = new SiteCustomDomainBean(null, siteCustomDomain.getSiteDomain(), siteCustomDomain.getDomain(), siteCustomDomain.getStatus());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new SiteCustomDomainBean(siteCustomDomain.getGuid(), siteCustomDomain.getSiteDomain(), siteCustomDomain.getDomain(), siteCustomDomain.getStatus());
        } else {
            log.log(Level.WARNING, "createSiteCustomDomain(): Arg siteCustomDomain is of an unknown type.");
            //bean = new SiteCustomDomainBean();
            bean = new SiteCustomDomainBean(siteCustomDomain.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getSiteCustomDomainServiceProxy().createSiteCustomDomain(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public SiteCustomDomain constructSiteCustomDomain(SiteCustomDomain siteCustomDomain) throws BaseException
    {
        log.finer("BEGIN");

        // Param siteCustomDomain cannot be null.....
        if(siteCustomDomain == null) {
            log.log(Level.INFO, "Param siteCustomDomain is null!");
            throw new BadRequestException("Param siteCustomDomain object is null!");
        }
        SiteCustomDomainBean bean = null;
        if(siteCustomDomain instanceof SiteCustomDomainBean) {
            bean = (SiteCustomDomainBean) siteCustomDomain;
        } else if(siteCustomDomain instanceof SiteCustomDomain) {
            // bean = new SiteCustomDomainBean(null, siteCustomDomain.getSiteDomain(), siteCustomDomain.getDomain(), siteCustomDomain.getStatus());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new SiteCustomDomainBean(siteCustomDomain.getGuid(), siteCustomDomain.getSiteDomain(), siteCustomDomain.getDomain(), siteCustomDomain.getStatus());
        } else {
            log.log(Level.WARNING, "createSiteCustomDomain(): Arg siteCustomDomain is of an unknown type.");
            //bean = new SiteCustomDomainBean();
            bean = new SiteCustomDomainBean(siteCustomDomain.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getSiteCustomDomainServiceProxy().createSiteCustomDomain(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateSiteCustomDomain(String guid, String siteDomain, String domain, String status) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        SiteCustomDomainBean bean = new SiteCustomDomainBean(guid, siteDomain, domain, status);
        return updateSiteCustomDomain(bean);
    }
        
    @Override
    public Boolean updateSiteCustomDomain(SiteCustomDomain siteCustomDomain) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //SiteCustomDomain bean = refreshSiteCustomDomain(siteCustomDomain);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param siteCustomDomain cannot be null.....
        if(siteCustomDomain == null || siteCustomDomain.getGuid() == null) {
            log.log(Level.INFO, "Param siteCustomDomain or its guid is null!");
            throw new BadRequestException("Param siteCustomDomain object or its guid is null!");
        }
        SiteCustomDomainBean bean = null;
        if(siteCustomDomain instanceof SiteCustomDomainBean) {
            bean = (SiteCustomDomainBean) siteCustomDomain;
        } else {  // if(siteCustomDomain instanceof SiteCustomDomain)
            bean = new SiteCustomDomainBean(siteCustomDomain.getGuid(), siteCustomDomain.getSiteDomain(), siteCustomDomain.getDomain(), siteCustomDomain.getStatus());
        }
        Boolean suc = getProxyFactory().getSiteCustomDomainServiceProxy().updateSiteCustomDomain(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public SiteCustomDomain refreshSiteCustomDomain(SiteCustomDomain siteCustomDomain) throws BaseException
    {
        log.finer("BEGIN");

        // Param siteCustomDomain cannot be null.....
        if(siteCustomDomain == null || siteCustomDomain.getGuid() == null) {
            log.log(Level.INFO, "Param siteCustomDomain or its guid is null!");
            throw new BadRequestException("Param siteCustomDomain object or its guid is null!");
        }
        SiteCustomDomainBean bean = null;
        if(siteCustomDomain instanceof SiteCustomDomainBean) {
            bean = (SiteCustomDomainBean) siteCustomDomain;
        } else {  // if(siteCustomDomain instanceof SiteCustomDomain)
            bean = new SiteCustomDomainBean(siteCustomDomain.getGuid(), siteCustomDomain.getSiteDomain(), siteCustomDomain.getDomain(), siteCustomDomain.getStatus());
        }
        Boolean suc = getProxyFactory().getSiteCustomDomainServiceProxy().updateSiteCustomDomain(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteSiteCustomDomain(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getSiteCustomDomainServiceProxy().deleteSiteCustomDomain(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }
            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            SiteCustomDomain siteCustomDomain = null;
            try {
                siteCustomDomain = getProxyFactory().getSiteCustomDomainServiceProxy().getSiteCustomDomain(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch siteCustomDomain with a key, " + guid);
                return false;
            }
            if(siteCustomDomain != null) {
                String beanGuid = siteCustomDomain.getGuid();
                Boolean suc1 = getProxyFactory().getSiteCustomDomainServiceProxy().deleteSiteCustomDomain(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("siteCustomDomain with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteSiteCustomDomain(SiteCustomDomain siteCustomDomain) throws BaseException
    {
        log.finer("BEGIN");

        // Param siteCustomDomain cannot be null.....
        if(siteCustomDomain == null || siteCustomDomain.getGuid() == null) {
            log.log(Level.INFO, "Param siteCustomDomain or its guid is null!");
            throw new BadRequestException("Param siteCustomDomain object or its guid is null!");
        }
        SiteCustomDomainBean bean = null;
        if(siteCustomDomain instanceof SiteCustomDomainBean) {
            bean = (SiteCustomDomainBean) siteCustomDomain;
        } else {  // if(siteCustomDomain instanceof SiteCustomDomain)
            // ????
            log.warning("siteCustomDomain is not an instance of SiteCustomDomainBean.");
            bean = new SiteCustomDomainBean(siteCustomDomain.getGuid(), siteCustomDomain.getSiteDomain(), siteCustomDomain.getDomain(), siteCustomDomain.getStatus());
        }
        Boolean suc = getProxyFactory().getSiteCustomDomainServiceProxy().deleteSiteCustomDomain(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteSiteCustomDomains(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getSiteCustomDomainServiceProxy().deleteSiteCustomDomains(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createSiteCustomDomains(List<SiteCustomDomain> siteCustomDomains) throws BaseException
    {
        log.finer("BEGIN");

        if(siteCustomDomains == null) {
            log.log(Level.WARNING, "createSiteCustomDomains() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = siteCustomDomains.size();
        if(size == 0) {
            log.log(Level.WARNING, "createSiteCustomDomains() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(SiteCustomDomain siteCustomDomain : siteCustomDomains) {
            String guid = createSiteCustomDomain(siteCustomDomain);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createSiteCustomDomains() failed for at least one siteCustomDomain. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateSiteCustomDomains(List<SiteCustomDomain> siteCustomDomains) throws BaseException
    //{
    //}

}
