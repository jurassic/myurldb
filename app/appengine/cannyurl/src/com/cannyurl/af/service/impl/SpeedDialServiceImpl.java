package com.cannyurl.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.SpeedDial;
import com.cannyurl.af.config.Config;

import com.cannyurl.af.bean.HashedPasswordStructBean;
import com.cannyurl.af.bean.GeoPointStructBean;
import com.cannyurl.af.bean.WebProfileStructBean;
import com.cannyurl.af.bean.StreetAddressStructBean;
import com.cannyurl.af.bean.SiteLogBean;
import com.cannyurl.af.bean.HelpNoticeBean;
import com.cannyurl.af.bean.TwitterCardAppInfoBean;
import com.cannyurl.af.bean.ContactInfoStructBean;
import com.cannyurl.af.bean.GeoCoordinateStructBean;
import com.cannyurl.af.bean.FullNameStructBean;
import com.cannyurl.af.bean.ExternalUserIdStructBean;
import com.cannyurl.af.bean.GaeUserStructBean;
import com.cannyurl.af.bean.AppBrandStructBean;
import com.cannyurl.af.bean.CellLatitudeLongitudeBean;
import com.cannyurl.af.bean.GaeAppStructBean;
import com.cannyurl.af.bean.ShortPassageAttributeBean;
import com.cannyurl.af.bean.KeyValuePairStructBean;
import com.cannyurl.af.bean.KeyValueRelationStructBean;
import com.cannyurl.af.bean.ExternalServiceApiKeyStructBean;
import com.cannyurl.af.bean.ReferrerInfoStructBean;
import com.cannyurl.af.bean.TwitterCardProductDataBean;
import com.cannyurl.af.bean.PagerStateStructBean;

import com.cannyurl.af.bean.SpeedDialBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.SpeedDialService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class SpeedDialServiceImpl implements SpeedDialService
{
    private static final Logger log = Logger.getLogger(SpeedDialServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "SpeedDial-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("SpeedDial:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public SpeedDialServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // SpeedDial related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public SpeedDial getSpeedDial(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getSpeedDial(): guid = " + guid);

        SpeedDialBean bean = null;
        if(getCache() != null) {
            bean = (SpeedDialBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (SpeedDialBean) getProxyFactory().getSpeedDialServiceProxy().getSpeedDial(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "SpeedDialBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve SpeedDialBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getSpeedDial(String guid, String field) throws BaseException
    {
        SpeedDialBean bean = null;
        if(getCache() != null) {
            bean = (SpeedDialBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (SpeedDialBean) getProxyFactory().getSpeedDialServiceProxy().getSpeedDial(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "SpeedDialBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve SpeedDialBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return bean.getUser();
        } else if(field.equals("code")) {
            return bean.getCode();
        } else if(field.equals("shortcut")) {
            return bean.getShortcut();
        } else if(field.equals("shortLink")) {
            return bean.getShortLink();
        } else if(field.equals("keywordLink")) {
            return bean.getKeywordLink();
        } else if(field.equals("bookmarkLink")) {
            return bean.getBookmarkLink();
        } else if(field.equals("longUrl")) {
            return bean.getLongUrl();
        } else if(field.equals("shortUrl")) {
            return bean.getShortUrl();
        } else if(field.equals("caseSensitive")) {
            return bean.isCaseSensitive();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("note")) {
            return bean.getNote();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<SpeedDial> getSpeedDials(List<String> guids) throws BaseException
    {
        log.fine("getSpeedDials()");

        // TBD: Is there a better way????
        List<SpeedDial> speedDials = getProxyFactory().getSpeedDialServiceProxy().getSpeedDials(guids);
        if(speedDials == null) {
            log.log(Level.WARNING, "Failed to retrieve SpeedDialBean list.");
        }

        log.finer("END");
        return speedDials;
    }

    @Override
    public List<SpeedDial> getAllSpeedDials() throws BaseException
    {
        return getAllSpeedDials(null, null, null);
    }

    @Override
    public List<SpeedDial> getAllSpeedDials(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllSpeedDials(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<SpeedDial> speedDials = getProxyFactory().getSpeedDialServiceProxy().getAllSpeedDials(ordering, offset, count);
        if(speedDials == null) {
            log.log(Level.WARNING, "Failed to retrieve SpeedDialBean list.");
        }

        log.finer("END");
        return speedDials;
    }

    @Override
    public List<String> getAllSpeedDialKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllSpeedDialKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getSpeedDialServiceProxy().getAllSpeedDialKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve SpeedDialBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty SpeedDialBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<SpeedDial> findSpeedDials(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findSpeedDials(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<SpeedDial> findSpeedDials(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("SpeedDialServiceImpl.findSpeedDials(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<SpeedDial> speedDials = getProxyFactory().getSpeedDialServiceProxy().findSpeedDials(filter, ordering, params, values, grouping, unique, offset, count);
        if(speedDials == null) {
            log.log(Level.WARNING, "Failed to find speedDials for the given criterion.");
        }

        log.finer("END");
        return speedDials;
    }

    @Override
    public List<String> findSpeedDialKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("SpeedDialServiceImpl.findSpeedDialKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getSpeedDialServiceProxy().findSpeedDialKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find SpeedDial keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty SpeedDial key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("SpeedDialServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getSpeedDialServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createSpeedDial(String user, String code, String shortcut, String shortLink, String keywordLink, String bookmarkLink, String longUrl, String shortUrl, Boolean caseSensitive, String status, String note) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        SpeedDialBean bean = new SpeedDialBean(null, user, code, shortcut, shortLink, keywordLink, bookmarkLink, longUrl, shortUrl, caseSensitive, status, note);
        return createSpeedDial(bean);
    }

    @Override
    public String createSpeedDial(SpeedDial speedDial) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //SpeedDial bean = constructSpeedDial(speedDial);
        //return bean.getGuid();

        // Param speedDial cannot be null.....
        if(speedDial == null) {
            log.log(Level.INFO, "Param speedDial is null!");
            throw new BadRequestException("Param speedDial object is null!");
        }
        SpeedDialBean bean = null;
        if(speedDial instanceof SpeedDialBean) {
            bean = (SpeedDialBean) speedDial;
        } else if(speedDial instanceof SpeedDial) {
            // bean = new SpeedDialBean(null, speedDial.getUser(), speedDial.getCode(), speedDial.getShortcut(), speedDial.getShortLink(), speedDial.getKeywordLink(), speedDial.getBookmarkLink(), speedDial.getLongUrl(), speedDial.getShortUrl(), speedDial.isCaseSensitive(), speedDial.getStatus(), speedDial.getNote());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new SpeedDialBean(speedDial.getGuid(), speedDial.getUser(), speedDial.getCode(), speedDial.getShortcut(), speedDial.getShortLink(), speedDial.getKeywordLink(), speedDial.getBookmarkLink(), speedDial.getLongUrl(), speedDial.getShortUrl(), speedDial.isCaseSensitive(), speedDial.getStatus(), speedDial.getNote());
        } else {
            log.log(Level.WARNING, "createSpeedDial(): Arg speedDial is of an unknown type.");
            //bean = new SpeedDialBean();
            bean = new SpeedDialBean(speedDial.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getSpeedDialServiceProxy().createSpeedDial(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public SpeedDial constructSpeedDial(SpeedDial speedDial) throws BaseException
    {
        log.finer("BEGIN");

        // Param speedDial cannot be null.....
        if(speedDial == null) {
            log.log(Level.INFO, "Param speedDial is null!");
            throw new BadRequestException("Param speedDial object is null!");
        }
        SpeedDialBean bean = null;
        if(speedDial instanceof SpeedDialBean) {
            bean = (SpeedDialBean) speedDial;
        } else if(speedDial instanceof SpeedDial) {
            // bean = new SpeedDialBean(null, speedDial.getUser(), speedDial.getCode(), speedDial.getShortcut(), speedDial.getShortLink(), speedDial.getKeywordLink(), speedDial.getBookmarkLink(), speedDial.getLongUrl(), speedDial.getShortUrl(), speedDial.isCaseSensitive(), speedDial.getStatus(), speedDial.getNote());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new SpeedDialBean(speedDial.getGuid(), speedDial.getUser(), speedDial.getCode(), speedDial.getShortcut(), speedDial.getShortLink(), speedDial.getKeywordLink(), speedDial.getBookmarkLink(), speedDial.getLongUrl(), speedDial.getShortUrl(), speedDial.isCaseSensitive(), speedDial.getStatus(), speedDial.getNote());
        } else {
            log.log(Level.WARNING, "createSpeedDial(): Arg speedDial is of an unknown type.");
            //bean = new SpeedDialBean();
            bean = new SpeedDialBean(speedDial.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getSpeedDialServiceProxy().createSpeedDial(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateSpeedDial(String guid, String user, String code, String shortcut, String shortLink, String keywordLink, String bookmarkLink, String longUrl, String shortUrl, Boolean caseSensitive, String status, String note) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        SpeedDialBean bean = new SpeedDialBean(guid, user, code, shortcut, shortLink, keywordLink, bookmarkLink, longUrl, shortUrl, caseSensitive, status, note);
        return updateSpeedDial(bean);
    }
        
    @Override
    public Boolean updateSpeedDial(SpeedDial speedDial) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //SpeedDial bean = refreshSpeedDial(speedDial);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param speedDial cannot be null.....
        if(speedDial == null || speedDial.getGuid() == null) {
            log.log(Level.INFO, "Param speedDial or its guid is null!");
            throw new BadRequestException("Param speedDial object or its guid is null!");
        }
        SpeedDialBean bean = null;
        if(speedDial instanceof SpeedDialBean) {
            bean = (SpeedDialBean) speedDial;
        } else {  // if(speedDial instanceof SpeedDial)
            bean = new SpeedDialBean(speedDial.getGuid(), speedDial.getUser(), speedDial.getCode(), speedDial.getShortcut(), speedDial.getShortLink(), speedDial.getKeywordLink(), speedDial.getBookmarkLink(), speedDial.getLongUrl(), speedDial.getShortUrl(), speedDial.isCaseSensitive(), speedDial.getStatus(), speedDial.getNote());
        }
        Boolean suc = getProxyFactory().getSpeedDialServiceProxy().updateSpeedDial(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public SpeedDial refreshSpeedDial(SpeedDial speedDial) throws BaseException
    {
        log.finer("BEGIN");

        // Param speedDial cannot be null.....
        if(speedDial == null || speedDial.getGuid() == null) {
            log.log(Level.INFO, "Param speedDial or its guid is null!");
            throw new BadRequestException("Param speedDial object or its guid is null!");
        }
        SpeedDialBean bean = null;
        if(speedDial instanceof SpeedDialBean) {
            bean = (SpeedDialBean) speedDial;
        } else {  // if(speedDial instanceof SpeedDial)
            bean = new SpeedDialBean(speedDial.getGuid(), speedDial.getUser(), speedDial.getCode(), speedDial.getShortcut(), speedDial.getShortLink(), speedDial.getKeywordLink(), speedDial.getBookmarkLink(), speedDial.getLongUrl(), speedDial.getShortUrl(), speedDial.isCaseSensitive(), speedDial.getStatus(), speedDial.getNote());
        }
        Boolean suc = getProxyFactory().getSpeedDialServiceProxy().updateSpeedDial(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteSpeedDial(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getSpeedDialServiceProxy().deleteSpeedDial(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }
            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            SpeedDial speedDial = null;
            try {
                speedDial = getProxyFactory().getSpeedDialServiceProxy().getSpeedDial(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch speedDial with a key, " + guid);
                return false;
            }
            if(speedDial != null) {
                String beanGuid = speedDial.getGuid();
                Boolean suc1 = getProxyFactory().getSpeedDialServiceProxy().deleteSpeedDial(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("speedDial with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteSpeedDial(SpeedDial speedDial) throws BaseException
    {
        log.finer("BEGIN");

        // Param speedDial cannot be null.....
        if(speedDial == null || speedDial.getGuid() == null) {
            log.log(Level.INFO, "Param speedDial or its guid is null!");
            throw new BadRequestException("Param speedDial object or its guid is null!");
        }
        SpeedDialBean bean = null;
        if(speedDial instanceof SpeedDialBean) {
            bean = (SpeedDialBean) speedDial;
        } else {  // if(speedDial instanceof SpeedDial)
            // ????
            log.warning("speedDial is not an instance of SpeedDialBean.");
            bean = new SpeedDialBean(speedDial.getGuid(), speedDial.getUser(), speedDial.getCode(), speedDial.getShortcut(), speedDial.getShortLink(), speedDial.getKeywordLink(), speedDial.getBookmarkLink(), speedDial.getLongUrl(), speedDial.getShortUrl(), speedDial.isCaseSensitive(), speedDial.getStatus(), speedDial.getNote());
        }
        Boolean suc = getProxyFactory().getSpeedDialServiceProxy().deleteSpeedDial(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteSpeedDials(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getSpeedDialServiceProxy().deleteSpeedDials(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createSpeedDials(List<SpeedDial> speedDials) throws BaseException
    {
        log.finer("BEGIN");

        if(speedDials == null) {
            log.log(Level.WARNING, "createSpeedDials() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = speedDials.size();
        if(size == 0) {
            log.log(Level.WARNING, "createSpeedDials() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(SpeedDial speedDial : speedDials) {
            String guid = createSpeedDial(speedDial);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createSpeedDials() failed for at least one speedDial. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateSpeedDials(List<SpeedDial> speedDials) throws BaseException
    //{
    //}

}
