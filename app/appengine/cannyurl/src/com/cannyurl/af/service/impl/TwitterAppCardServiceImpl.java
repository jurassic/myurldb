package com.cannyurl.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.TwitterAppCard;
import com.cannyurl.af.config.Config;

import com.cannyurl.af.bean.HashedPasswordStructBean;
import com.cannyurl.af.bean.GeoPointStructBean;
import com.cannyurl.af.bean.WebProfileStructBean;
import com.cannyurl.af.bean.StreetAddressStructBean;
import com.cannyurl.af.bean.SiteLogBean;
import com.cannyurl.af.bean.HelpNoticeBean;
import com.cannyurl.af.bean.TwitterCardAppInfoBean;
import com.cannyurl.af.bean.ContactInfoStructBean;
import com.cannyurl.af.bean.GeoCoordinateStructBean;
import com.cannyurl.af.bean.FullNameStructBean;
import com.cannyurl.af.bean.ExternalUserIdStructBean;
import com.cannyurl.af.bean.GaeUserStructBean;
import com.cannyurl.af.bean.AppBrandStructBean;
import com.cannyurl.af.bean.CellLatitudeLongitudeBean;
import com.cannyurl.af.bean.GaeAppStructBean;
import com.cannyurl.af.bean.ShortPassageAttributeBean;
import com.cannyurl.af.bean.KeyValuePairStructBean;
import com.cannyurl.af.bean.KeyValueRelationStructBean;
import com.cannyurl.af.bean.ExternalServiceApiKeyStructBean;
import com.cannyurl.af.bean.ReferrerInfoStructBean;
import com.cannyurl.af.bean.TwitterCardProductDataBean;
import com.cannyurl.af.bean.PagerStateStructBean;

import com.cannyurl.af.bean.TwitterAppCardBean;
import com.cannyurl.af.bean.TwitterCardAppInfoBean;
import com.cannyurl.af.bean.TwitterCardProductDataBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.TwitterAppCardService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class TwitterAppCardServiceImpl implements TwitterAppCardService
{
    private static final Logger log = Logger.getLogger(TwitterAppCardServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "TwitterAppCard-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("TwitterAppCard:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public TwitterAppCardServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // TwitterAppCard related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public TwitterAppCard getTwitterAppCard(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getTwitterAppCard(): guid = " + guid);

        TwitterAppCardBean bean = null;
        if(getCache() != null) {
            bean = (TwitterAppCardBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (TwitterAppCardBean) getProxyFactory().getTwitterAppCardServiceProxy().getTwitterAppCard(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "TwitterAppCardBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve TwitterAppCardBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getTwitterAppCard(String guid, String field) throws BaseException
    {
        TwitterAppCardBean bean = null;
        if(getCache() != null) {
            bean = (TwitterAppCardBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (TwitterAppCardBean) getProxyFactory().getTwitterAppCardServiceProxy().getTwitterAppCard(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "TwitterAppCardBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve TwitterAppCardBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("card")) {
            return bean.getCard();
        } else if(field.equals("url")) {
            return bean.getUrl();
        } else if(field.equals("title")) {
            return bean.getTitle();
        } else if(field.equals("description")) {
            return bean.getDescription();
        } else if(field.equals("site")) {
            return bean.getSite();
        } else if(field.equals("siteId")) {
            return bean.getSiteId();
        } else if(field.equals("creator")) {
            return bean.getCreator();
        } else if(field.equals("creatorId")) {
            return bean.getCreatorId();
        } else if(field.equals("image")) {
            return bean.getImage();
        } else if(field.equals("imageWidth")) {
            return bean.getImageWidth();
        } else if(field.equals("imageHeight")) {
            return bean.getImageHeight();
        } else if(field.equals("iphoneApp")) {
            return bean.getIphoneApp();
        } else if(field.equals("ipadApp")) {
            return bean.getIpadApp();
        } else if(field.equals("googlePlayApp")) {
            return bean.getGooglePlayApp();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<TwitterAppCard> getTwitterAppCards(List<String> guids) throws BaseException
    {
        log.fine("getTwitterAppCards()");

        // TBD: Is there a better way????
        List<TwitterAppCard> twitterAppCards = getProxyFactory().getTwitterAppCardServiceProxy().getTwitterAppCards(guids);
        if(twitterAppCards == null) {
            log.log(Level.WARNING, "Failed to retrieve TwitterAppCardBean list.");
        }

        log.finer("END");
        return twitterAppCards;
    }

    @Override
    public List<TwitterAppCard> getAllTwitterAppCards() throws BaseException
    {
        return getAllTwitterAppCards(null, null, null);
    }

    @Override
    public List<TwitterAppCard> getAllTwitterAppCards(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllTwitterAppCards(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<TwitterAppCard> twitterAppCards = getProxyFactory().getTwitterAppCardServiceProxy().getAllTwitterAppCards(ordering, offset, count);
        if(twitterAppCards == null) {
            log.log(Level.WARNING, "Failed to retrieve TwitterAppCardBean list.");
        }

        log.finer("END");
        return twitterAppCards;
    }

    @Override
    public List<String> getAllTwitterAppCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllTwitterAppCardKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getTwitterAppCardServiceProxy().getAllTwitterAppCardKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve TwitterAppCardBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty TwitterAppCardBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<TwitterAppCard> findTwitterAppCards(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findTwitterAppCards(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<TwitterAppCard> findTwitterAppCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TwitterAppCardServiceImpl.findTwitterAppCards(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<TwitterAppCard> twitterAppCards = getProxyFactory().getTwitterAppCardServiceProxy().findTwitterAppCards(filter, ordering, params, values, grouping, unique, offset, count);
        if(twitterAppCards == null) {
            log.log(Level.WARNING, "Failed to find twitterAppCards for the given criterion.");
        }

        log.finer("END");
        return twitterAppCards;
    }

    @Override
    public List<String> findTwitterAppCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TwitterAppCardServiceImpl.findTwitterAppCardKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getTwitterAppCardServiceProxy().findTwitterAppCardKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find TwitterAppCard keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty TwitterAppCard key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TwitterAppCardServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getTwitterAppCardServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createTwitterAppCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, TwitterCardAppInfo iphoneApp, TwitterCardAppInfo ipadApp, TwitterCardAppInfo googlePlayApp) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        TwitterCardAppInfoBean iphoneAppBean = null;
        if(iphoneApp instanceof TwitterCardAppInfoBean) {
            iphoneAppBean = (TwitterCardAppInfoBean) iphoneApp;
        } else if(iphoneApp instanceof TwitterCardAppInfo) {
            iphoneAppBean = new TwitterCardAppInfoBean(iphoneApp.getName(), iphoneApp.getId(), iphoneApp.getUrl());
        } else {
            iphoneAppBean = null;   // ????
        }
        TwitterCardAppInfoBean ipadAppBean = null;
        if(ipadApp instanceof TwitterCardAppInfoBean) {
            ipadAppBean = (TwitterCardAppInfoBean) ipadApp;
        } else if(ipadApp instanceof TwitterCardAppInfo) {
            ipadAppBean = new TwitterCardAppInfoBean(ipadApp.getName(), ipadApp.getId(), ipadApp.getUrl());
        } else {
            ipadAppBean = null;   // ????
        }
        TwitterCardAppInfoBean googlePlayAppBean = null;
        if(googlePlayApp instanceof TwitterCardAppInfoBean) {
            googlePlayAppBean = (TwitterCardAppInfoBean) googlePlayApp;
        } else if(googlePlayApp instanceof TwitterCardAppInfo) {
            googlePlayAppBean = new TwitterCardAppInfoBean(googlePlayApp.getName(), googlePlayApp.getId(), googlePlayApp.getUrl());
        } else {
            googlePlayAppBean = null;   // ????
        }
        TwitterAppCardBean bean = new TwitterAppCardBean(null, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, iphoneAppBean, ipadAppBean, googlePlayAppBean);
        return createTwitterAppCard(bean);
    }

    @Override
    public String createTwitterAppCard(TwitterAppCard twitterAppCard) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //TwitterAppCard bean = constructTwitterAppCard(twitterAppCard);
        //return bean.getGuid();

        // Param twitterAppCard cannot be null.....
        if(twitterAppCard == null) {
            log.log(Level.INFO, "Param twitterAppCard is null!");
            throw new BadRequestException("Param twitterAppCard object is null!");
        }
        TwitterAppCardBean bean = null;
        if(twitterAppCard instanceof TwitterAppCardBean) {
            bean = (TwitterAppCardBean) twitterAppCard;
        } else if(twitterAppCard instanceof TwitterAppCard) {
            // bean = new TwitterAppCardBean(null, twitterAppCard.getCard(), twitterAppCard.getUrl(), twitterAppCard.getTitle(), twitterAppCard.getDescription(), twitterAppCard.getSite(), twitterAppCard.getSiteId(), twitterAppCard.getCreator(), twitterAppCard.getCreatorId(), twitterAppCard.getImage(), twitterAppCard.getImageWidth(), twitterAppCard.getImageHeight(), (TwitterCardAppInfoBean) twitterAppCard.getIphoneApp(), (TwitterCardAppInfoBean) twitterAppCard.getIpadApp(), (TwitterCardAppInfoBean) twitterAppCard.getGooglePlayApp());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new TwitterAppCardBean(twitterAppCard.getGuid(), twitterAppCard.getCard(), twitterAppCard.getUrl(), twitterAppCard.getTitle(), twitterAppCard.getDescription(), twitterAppCard.getSite(), twitterAppCard.getSiteId(), twitterAppCard.getCreator(), twitterAppCard.getCreatorId(), twitterAppCard.getImage(), twitterAppCard.getImageWidth(), twitterAppCard.getImageHeight(), (TwitterCardAppInfoBean) twitterAppCard.getIphoneApp(), (TwitterCardAppInfoBean) twitterAppCard.getIpadApp(), (TwitterCardAppInfoBean) twitterAppCard.getGooglePlayApp());
        } else {
            log.log(Level.WARNING, "createTwitterAppCard(): Arg twitterAppCard is of an unknown type.");
            //bean = new TwitterAppCardBean();
            bean = new TwitterAppCardBean(twitterAppCard.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getTwitterAppCardServiceProxy().createTwitterAppCard(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public TwitterAppCard constructTwitterAppCard(TwitterAppCard twitterAppCard) throws BaseException
    {
        log.finer("BEGIN");

        // Param twitterAppCard cannot be null.....
        if(twitterAppCard == null) {
            log.log(Level.INFO, "Param twitterAppCard is null!");
            throw new BadRequestException("Param twitterAppCard object is null!");
        }
        TwitterAppCardBean bean = null;
        if(twitterAppCard instanceof TwitterAppCardBean) {
            bean = (TwitterAppCardBean) twitterAppCard;
        } else if(twitterAppCard instanceof TwitterAppCard) {
            // bean = new TwitterAppCardBean(null, twitterAppCard.getCard(), twitterAppCard.getUrl(), twitterAppCard.getTitle(), twitterAppCard.getDescription(), twitterAppCard.getSite(), twitterAppCard.getSiteId(), twitterAppCard.getCreator(), twitterAppCard.getCreatorId(), twitterAppCard.getImage(), twitterAppCard.getImageWidth(), twitterAppCard.getImageHeight(), (TwitterCardAppInfoBean) twitterAppCard.getIphoneApp(), (TwitterCardAppInfoBean) twitterAppCard.getIpadApp(), (TwitterCardAppInfoBean) twitterAppCard.getGooglePlayApp());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new TwitterAppCardBean(twitterAppCard.getGuid(), twitterAppCard.getCard(), twitterAppCard.getUrl(), twitterAppCard.getTitle(), twitterAppCard.getDescription(), twitterAppCard.getSite(), twitterAppCard.getSiteId(), twitterAppCard.getCreator(), twitterAppCard.getCreatorId(), twitterAppCard.getImage(), twitterAppCard.getImageWidth(), twitterAppCard.getImageHeight(), (TwitterCardAppInfoBean) twitterAppCard.getIphoneApp(), (TwitterCardAppInfoBean) twitterAppCard.getIpadApp(), (TwitterCardAppInfoBean) twitterAppCard.getGooglePlayApp());
        } else {
            log.log(Level.WARNING, "createTwitterAppCard(): Arg twitterAppCard is of an unknown type.");
            //bean = new TwitterAppCardBean();
            bean = new TwitterAppCardBean(twitterAppCard.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getTwitterAppCardServiceProxy().createTwitterAppCard(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateTwitterAppCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, TwitterCardAppInfo iphoneApp, TwitterCardAppInfo ipadApp, TwitterCardAppInfo googlePlayApp) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        TwitterCardAppInfoBean iphoneAppBean = null;
        if(iphoneApp instanceof TwitterCardAppInfoBean) {
            iphoneAppBean = (TwitterCardAppInfoBean) iphoneApp;
        } else if(iphoneApp instanceof TwitterCardAppInfo) {
            iphoneAppBean = new TwitterCardAppInfoBean(iphoneApp.getName(), iphoneApp.getId(), iphoneApp.getUrl());
        } else {
            iphoneAppBean = null;   // ????
        }
        TwitterCardAppInfoBean ipadAppBean = null;
        if(ipadApp instanceof TwitterCardAppInfoBean) {
            ipadAppBean = (TwitterCardAppInfoBean) ipadApp;
        } else if(ipadApp instanceof TwitterCardAppInfo) {
            ipadAppBean = new TwitterCardAppInfoBean(ipadApp.getName(), ipadApp.getId(), ipadApp.getUrl());
        } else {
            ipadAppBean = null;   // ????
        }
        TwitterCardAppInfoBean googlePlayAppBean = null;
        if(googlePlayApp instanceof TwitterCardAppInfoBean) {
            googlePlayAppBean = (TwitterCardAppInfoBean) googlePlayApp;
        } else if(googlePlayApp instanceof TwitterCardAppInfo) {
            googlePlayAppBean = new TwitterCardAppInfoBean(googlePlayApp.getName(), googlePlayApp.getId(), googlePlayApp.getUrl());
        } else {
            googlePlayAppBean = null;   // ????
        }
        TwitterAppCardBean bean = new TwitterAppCardBean(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, iphoneAppBean, ipadAppBean, googlePlayAppBean);
        return updateTwitterAppCard(bean);
    }
        
    @Override
    public Boolean updateTwitterAppCard(TwitterAppCard twitterAppCard) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //TwitterAppCard bean = refreshTwitterAppCard(twitterAppCard);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param twitterAppCard cannot be null.....
        if(twitterAppCard == null || twitterAppCard.getGuid() == null) {
            log.log(Level.INFO, "Param twitterAppCard or its guid is null!");
            throw new BadRequestException("Param twitterAppCard object or its guid is null!");
        }
        TwitterAppCardBean bean = null;
        if(twitterAppCard instanceof TwitterAppCardBean) {
            bean = (TwitterAppCardBean) twitterAppCard;
        } else {  // if(twitterAppCard instanceof TwitterAppCard)
            bean = new TwitterAppCardBean(twitterAppCard.getGuid(), twitterAppCard.getCard(), twitterAppCard.getUrl(), twitterAppCard.getTitle(), twitterAppCard.getDescription(), twitterAppCard.getSite(), twitterAppCard.getSiteId(), twitterAppCard.getCreator(), twitterAppCard.getCreatorId(), twitterAppCard.getImage(), twitterAppCard.getImageWidth(), twitterAppCard.getImageHeight(), (TwitterCardAppInfoBean) twitterAppCard.getIphoneApp(), (TwitterCardAppInfoBean) twitterAppCard.getIpadApp(), (TwitterCardAppInfoBean) twitterAppCard.getGooglePlayApp());
        }
        Boolean suc = getProxyFactory().getTwitterAppCardServiceProxy().updateTwitterAppCard(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public TwitterAppCard refreshTwitterAppCard(TwitterAppCard twitterAppCard) throws BaseException
    {
        log.finer("BEGIN");

        // Param twitterAppCard cannot be null.....
        if(twitterAppCard == null || twitterAppCard.getGuid() == null) {
            log.log(Level.INFO, "Param twitterAppCard or its guid is null!");
            throw new BadRequestException("Param twitterAppCard object or its guid is null!");
        }
        TwitterAppCardBean bean = null;
        if(twitterAppCard instanceof TwitterAppCardBean) {
            bean = (TwitterAppCardBean) twitterAppCard;
        } else {  // if(twitterAppCard instanceof TwitterAppCard)
            bean = new TwitterAppCardBean(twitterAppCard.getGuid(), twitterAppCard.getCard(), twitterAppCard.getUrl(), twitterAppCard.getTitle(), twitterAppCard.getDescription(), twitterAppCard.getSite(), twitterAppCard.getSiteId(), twitterAppCard.getCreator(), twitterAppCard.getCreatorId(), twitterAppCard.getImage(), twitterAppCard.getImageWidth(), twitterAppCard.getImageHeight(), (TwitterCardAppInfoBean) twitterAppCard.getIphoneApp(), (TwitterCardAppInfoBean) twitterAppCard.getIpadApp(), (TwitterCardAppInfoBean) twitterAppCard.getGooglePlayApp());
        }
        Boolean suc = getProxyFactory().getTwitterAppCardServiceProxy().updateTwitterAppCard(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteTwitterAppCard(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getTwitterAppCardServiceProxy().deleteTwitterAppCard(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }
            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            TwitterAppCard twitterAppCard = null;
            try {
                twitterAppCard = getProxyFactory().getTwitterAppCardServiceProxy().getTwitterAppCard(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch twitterAppCard with a key, " + guid);
                return false;
            }
            if(twitterAppCard != null) {
                String beanGuid = twitterAppCard.getGuid();
                Boolean suc1 = getProxyFactory().getTwitterAppCardServiceProxy().deleteTwitterAppCard(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("twitterAppCard with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteTwitterAppCard(TwitterAppCard twitterAppCard) throws BaseException
    {
        log.finer("BEGIN");

        // Param twitterAppCard cannot be null.....
        if(twitterAppCard == null || twitterAppCard.getGuid() == null) {
            log.log(Level.INFO, "Param twitterAppCard or its guid is null!");
            throw new BadRequestException("Param twitterAppCard object or its guid is null!");
        }
        TwitterAppCardBean bean = null;
        if(twitterAppCard instanceof TwitterAppCardBean) {
            bean = (TwitterAppCardBean) twitterAppCard;
        } else {  // if(twitterAppCard instanceof TwitterAppCard)
            // ????
            log.warning("twitterAppCard is not an instance of TwitterAppCardBean.");
            bean = new TwitterAppCardBean(twitterAppCard.getGuid(), twitterAppCard.getCard(), twitterAppCard.getUrl(), twitterAppCard.getTitle(), twitterAppCard.getDescription(), twitterAppCard.getSite(), twitterAppCard.getSiteId(), twitterAppCard.getCreator(), twitterAppCard.getCreatorId(), twitterAppCard.getImage(), twitterAppCard.getImageWidth(), twitterAppCard.getImageHeight(), (TwitterCardAppInfoBean) twitterAppCard.getIphoneApp(), (TwitterCardAppInfoBean) twitterAppCard.getIpadApp(), (TwitterCardAppInfoBean) twitterAppCard.getGooglePlayApp());
        }
        Boolean suc = getProxyFactory().getTwitterAppCardServiceProxy().deleteTwitterAppCard(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteTwitterAppCards(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getTwitterAppCardServiceProxy().deleteTwitterAppCards(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createTwitterAppCards(List<TwitterAppCard> twitterAppCards) throws BaseException
    {
        log.finer("BEGIN");

        if(twitterAppCards == null) {
            log.log(Level.WARNING, "createTwitterAppCards() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = twitterAppCards.size();
        if(size == 0) {
            log.log(Level.WARNING, "createTwitterAppCards() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(TwitterAppCard twitterAppCard : twitterAppCards) {
            String guid = createTwitterAppCard(twitterAppCard);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createTwitterAppCards() failed for at least one twitterAppCard. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateTwitterAppCards(List<TwitterAppCard> twitterAppCards) throws BaseException
    //{
    //}

}
