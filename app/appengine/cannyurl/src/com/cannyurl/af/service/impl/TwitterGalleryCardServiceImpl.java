package com.cannyurl.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.TwitterGalleryCard;
import com.cannyurl.af.config.Config;

import com.cannyurl.af.bean.HashedPasswordStructBean;
import com.cannyurl.af.bean.GeoPointStructBean;
import com.cannyurl.af.bean.WebProfileStructBean;
import com.cannyurl.af.bean.StreetAddressStructBean;
import com.cannyurl.af.bean.SiteLogBean;
import com.cannyurl.af.bean.HelpNoticeBean;
import com.cannyurl.af.bean.TwitterCardAppInfoBean;
import com.cannyurl.af.bean.ContactInfoStructBean;
import com.cannyurl.af.bean.GeoCoordinateStructBean;
import com.cannyurl.af.bean.FullNameStructBean;
import com.cannyurl.af.bean.ExternalUserIdStructBean;
import com.cannyurl.af.bean.GaeUserStructBean;
import com.cannyurl.af.bean.AppBrandStructBean;
import com.cannyurl.af.bean.CellLatitudeLongitudeBean;
import com.cannyurl.af.bean.GaeAppStructBean;
import com.cannyurl.af.bean.ShortPassageAttributeBean;
import com.cannyurl.af.bean.KeyValuePairStructBean;
import com.cannyurl.af.bean.KeyValueRelationStructBean;
import com.cannyurl.af.bean.ExternalServiceApiKeyStructBean;
import com.cannyurl.af.bean.ReferrerInfoStructBean;
import com.cannyurl.af.bean.TwitterCardProductDataBean;
import com.cannyurl.af.bean.PagerStateStructBean;

import com.cannyurl.af.bean.TwitterGalleryCardBean;
import com.cannyurl.af.bean.TwitterCardAppInfoBean;
import com.cannyurl.af.bean.TwitterCardProductDataBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.TwitterGalleryCardService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class TwitterGalleryCardServiceImpl implements TwitterGalleryCardService
{
    private static final Logger log = Logger.getLogger(TwitterGalleryCardServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "TwitterGalleryCard-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("TwitterGalleryCard:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public TwitterGalleryCardServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // TwitterGalleryCard related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public TwitterGalleryCard getTwitterGalleryCard(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getTwitterGalleryCard(): guid = " + guid);

        TwitterGalleryCardBean bean = null;
        if(getCache() != null) {
            bean = (TwitterGalleryCardBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (TwitterGalleryCardBean) getProxyFactory().getTwitterGalleryCardServiceProxy().getTwitterGalleryCard(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "TwitterGalleryCardBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve TwitterGalleryCardBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getTwitterGalleryCard(String guid, String field) throws BaseException
    {
        TwitterGalleryCardBean bean = null;
        if(getCache() != null) {
            bean = (TwitterGalleryCardBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (TwitterGalleryCardBean) getProxyFactory().getTwitterGalleryCardServiceProxy().getTwitterGalleryCard(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "TwitterGalleryCardBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve TwitterGalleryCardBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("card")) {
            return bean.getCard();
        } else if(field.equals("url")) {
            return bean.getUrl();
        } else if(field.equals("title")) {
            return bean.getTitle();
        } else if(field.equals("description")) {
            return bean.getDescription();
        } else if(field.equals("site")) {
            return bean.getSite();
        } else if(field.equals("siteId")) {
            return bean.getSiteId();
        } else if(field.equals("creator")) {
            return bean.getCreator();
        } else if(field.equals("creatorId")) {
            return bean.getCreatorId();
        } else if(field.equals("image0")) {
            return bean.getImage0();
        } else if(field.equals("image1")) {
            return bean.getImage1();
        } else if(field.equals("image2")) {
            return bean.getImage2();
        } else if(field.equals("image3")) {
            return bean.getImage3();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<TwitterGalleryCard> getTwitterGalleryCards(List<String> guids) throws BaseException
    {
        log.fine("getTwitterGalleryCards()");

        // TBD: Is there a better way????
        List<TwitterGalleryCard> twitterGalleryCards = getProxyFactory().getTwitterGalleryCardServiceProxy().getTwitterGalleryCards(guids);
        if(twitterGalleryCards == null) {
            log.log(Level.WARNING, "Failed to retrieve TwitterGalleryCardBean list.");
        }

        log.finer("END");
        return twitterGalleryCards;
    }

    @Override
    public List<TwitterGalleryCard> getAllTwitterGalleryCards() throws BaseException
    {
        return getAllTwitterGalleryCards(null, null, null);
    }

    @Override
    public List<TwitterGalleryCard> getAllTwitterGalleryCards(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllTwitterGalleryCards(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<TwitterGalleryCard> twitterGalleryCards = getProxyFactory().getTwitterGalleryCardServiceProxy().getAllTwitterGalleryCards(ordering, offset, count);
        if(twitterGalleryCards == null) {
            log.log(Level.WARNING, "Failed to retrieve TwitterGalleryCardBean list.");
        }

        log.finer("END");
        return twitterGalleryCards;
    }

    @Override
    public List<String> getAllTwitterGalleryCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllTwitterGalleryCardKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getTwitterGalleryCardServiceProxy().getAllTwitterGalleryCardKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve TwitterGalleryCardBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty TwitterGalleryCardBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<TwitterGalleryCard> findTwitterGalleryCards(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findTwitterGalleryCards(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<TwitterGalleryCard> findTwitterGalleryCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TwitterGalleryCardServiceImpl.findTwitterGalleryCards(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<TwitterGalleryCard> twitterGalleryCards = getProxyFactory().getTwitterGalleryCardServiceProxy().findTwitterGalleryCards(filter, ordering, params, values, grouping, unique, offset, count);
        if(twitterGalleryCards == null) {
            log.log(Level.WARNING, "Failed to find twitterGalleryCards for the given criterion.");
        }

        log.finer("END");
        return twitterGalleryCards;
    }

    @Override
    public List<String> findTwitterGalleryCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TwitterGalleryCardServiceImpl.findTwitterGalleryCardKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getTwitterGalleryCardServiceProxy().findTwitterGalleryCardKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find TwitterGalleryCard keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty TwitterGalleryCard key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TwitterGalleryCardServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getTwitterGalleryCardServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createTwitterGalleryCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image0, String image1, String image2, String image3) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        TwitterGalleryCardBean bean = new TwitterGalleryCardBean(null, card, url, title, description, site, siteId, creator, creatorId, image0, image1, image2, image3);
        return createTwitterGalleryCard(bean);
    }

    @Override
    public String createTwitterGalleryCard(TwitterGalleryCard twitterGalleryCard) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //TwitterGalleryCard bean = constructTwitterGalleryCard(twitterGalleryCard);
        //return bean.getGuid();

        // Param twitterGalleryCard cannot be null.....
        if(twitterGalleryCard == null) {
            log.log(Level.INFO, "Param twitterGalleryCard is null!");
            throw new BadRequestException("Param twitterGalleryCard object is null!");
        }
        TwitterGalleryCardBean bean = null;
        if(twitterGalleryCard instanceof TwitterGalleryCardBean) {
            bean = (TwitterGalleryCardBean) twitterGalleryCard;
        } else if(twitterGalleryCard instanceof TwitterGalleryCard) {
            // bean = new TwitterGalleryCardBean(null, twitterGalleryCard.getCard(), twitterGalleryCard.getUrl(), twitterGalleryCard.getTitle(), twitterGalleryCard.getDescription(), twitterGalleryCard.getSite(), twitterGalleryCard.getSiteId(), twitterGalleryCard.getCreator(), twitterGalleryCard.getCreatorId(), twitterGalleryCard.getImage0(), twitterGalleryCard.getImage1(), twitterGalleryCard.getImage2(), twitterGalleryCard.getImage3());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new TwitterGalleryCardBean(twitterGalleryCard.getGuid(), twitterGalleryCard.getCard(), twitterGalleryCard.getUrl(), twitterGalleryCard.getTitle(), twitterGalleryCard.getDescription(), twitterGalleryCard.getSite(), twitterGalleryCard.getSiteId(), twitterGalleryCard.getCreator(), twitterGalleryCard.getCreatorId(), twitterGalleryCard.getImage0(), twitterGalleryCard.getImage1(), twitterGalleryCard.getImage2(), twitterGalleryCard.getImage3());
        } else {
            log.log(Level.WARNING, "createTwitterGalleryCard(): Arg twitterGalleryCard is of an unknown type.");
            //bean = new TwitterGalleryCardBean();
            bean = new TwitterGalleryCardBean(twitterGalleryCard.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getTwitterGalleryCardServiceProxy().createTwitterGalleryCard(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public TwitterGalleryCard constructTwitterGalleryCard(TwitterGalleryCard twitterGalleryCard) throws BaseException
    {
        log.finer("BEGIN");

        // Param twitterGalleryCard cannot be null.....
        if(twitterGalleryCard == null) {
            log.log(Level.INFO, "Param twitterGalleryCard is null!");
            throw new BadRequestException("Param twitterGalleryCard object is null!");
        }
        TwitterGalleryCardBean bean = null;
        if(twitterGalleryCard instanceof TwitterGalleryCardBean) {
            bean = (TwitterGalleryCardBean) twitterGalleryCard;
        } else if(twitterGalleryCard instanceof TwitterGalleryCard) {
            // bean = new TwitterGalleryCardBean(null, twitterGalleryCard.getCard(), twitterGalleryCard.getUrl(), twitterGalleryCard.getTitle(), twitterGalleryCard.getDescription(), twitterGalleryCard.getSite(), twitterGalleryCard.getSiteId(), twitterGalleryCard.getCreator(), twitterGalleryCard.getCreatorId(), twitterGalleryCard.getImage0(), twitterGalleryCard.getImage1(), twitterGalleryCard.getImage2(), twitterGalleryCard.getImage3());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new TwitterGalleryCardBean(twitterGalleryCard.getGuid(), twitterGalleryCard.getCard(), twitterGalleryCard.getUrl(), twitterGalleryCard.getTitle(), twitterGalleryCard.getDescription(), twitterGalleryCard.getSite(), twitterGalleryCard.getSiteId(), twitterGalleryCard.getCreator(), twitterGalleryCard.getCreatorId(), twitterGalleryCard.getImage0(), twitterGalleryCard.getImage1(), twitterGalleryCard.getImage2(), twitterGalleryCard.getImage3());
        } else {
            log.log(Level.WARNING, "createTwitterGalleryCard(): Arg twitterGalleryCard is of an unknown type.");
            //bean = new TwitterGalleryCardBean();
            bean = new TwitterGalleryCardBean(twitterGalleryCard.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getTwitterGalleryCardServiceProxy().createTwitterGalleryCard(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateTwitterGalleryCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image0, String image1, String image2, String image3) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        TwitterGalleryCardBean bean = new TwitterGalleryCardBean(guid, card, url, title, description, site, siteId, creator, creatorId, image0, image1, image2, image3);
        return updateTwitterGalleryCard(bean);
    }
        
    @Override
    public Boolean updateTwitterGalleryCard(TwitterGalleryCard twitterGalleryCard) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //TwitterGalleryCard bean = refreshTwitterGalleryCard(twitterGalleryCard);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param twitterGalleryCard cannot be null.....
        if(twitterGalleryCard == null || twitterGalleryCard.getGuid() == null) {
            log.log(Level.INFO, "Param twitterGalleryCard or its guid is null!");
            throw new BadRequestException("Param twitterGalleryCard object or its guid is null!");
        }
        TwitterGalleryCardBean bean = null;
        if(twitterGalleryCard instanceof TwitterGalleryCardBean) {
            bean = (TwitterGalleryCardBean) twitterGalleryCard;
        } else {  // if(twitterGalleryCard instanceof TwitterGalleryCard)
            bean = new TwitterGalleryCardBean(twitterGalleryCard.getGuid(), twitterGalleryCard.getCard(), twitterGalleryCard.getUrl(), twitterGalleryCard.getTitle(), twitterGalleryCard.getDescription(), twitterGalleryCard.getSite(), twitterGalleryCard.getSiteId(), twitterGalleryCard.getCreator(), twitterGalleryCard.getCreatorId(), twitterGalleryCard.getImage0(), twitterGalleryCard.getImage1(), twitterGalleryCard.getImage2(), twitterGalleryCard.getImage3());
        }
        Boolean suc = getProxyFactory().getTwitterGalleryCardServiceProxy().updateTwitterGalleryCard(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public TwitterGalleryCard refreshTwitterGalleryCard(TwitterGalleryCard twitterGalleryCard) throws BaseException
    {
        log.finer("BEGIN");

        // Param twitterGalleryCard cannot be null.....
        if(twitterGalleryCard == null || twitterGalleryCard.getGuid() == null) {
            log.log(Level.INFO, "Param twitterGalleryCard or its guid is null!");
            throw new BadRequestException("Param twitterGalleryCard object or its guid is null!");
        }
        TwitterGalleryCardBean bean = null;
        if(twitterGalleryCard instanceof TwitterGalleryCardBean) {
            bean = (TwitterGalleryCardBean) twitterGalleryCard;
        } else {  // if(twitterGalleryCard instanceof TwitterGalleryCard)
            bean = new TwitterGalleryCardBean(twitterGalleryCard.getGuid(), twitterGalleryCard.getCard(), twitterGalleryCard.getUrl(), twitterGalleryCard.getTitle(), twitterGalleryCard.getDescription(), twitterGalleryCard.getSite(), twitterGalleryCard.getSiteId(), twitterGalleryCard.getCreator(), twitterGalleryCard.getCreatorId(), twitterGalleryCard.getImage0(), twitterGalleryCard.getImage1(), twitterGalleryCard.getImage2(), twitterGalleryCard.getImage3());
        }
        Boolean suc = getProxyFactory().getTwitterGalleryCardServiceProxy().updateTwitterGalleryCard(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteTwitterGalleryCard(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getTwitterGalleryCardServiceProxy().deleteTwitterGalleryCard(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }
            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            TwitterGalleryCard twitterGalleryCard = null;
            try {
                twitterGalleryCard = getProxyFactory().getTwitterGalleryCardServiceProxy().getTwitterGalleryCard(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch twitterGalleryCard with a key, " + guid);
                return false;
            }
            if(twitterGalleryCard != null) {
                String beanGuid = twitterGalleryCard.getGuid();
                Boolean suc1 = getProxyFactory().getTwitterGalleryCardServiceProxy().deleteTwitterGalleryCard(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("twitterGalleryCard with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteTwitterGalleryCard(TwitterGalleryCard twitterGalleryCard) throws BaseException
    {
        log.finer("BEGIN");

        // Param twitterGalleryCard cannot be null.....
        if(twitterGalleryCard == null || twitterGalleryCard.getGuid() == null) {
            log.log(Level.INFO, "Param twitterGalleryCard or its guid is null!");
            throw new BadRequestException("Param twitterGalleryCard object or its guid is null!");
        }
        TwitterGalleryCardBean bean = null;
        if(twitterGalleryCard instanceof TwitterGalleryCardBean) {
            bean = (TwitterGalleryCardBean) twitterGalleryCard;
        } else {  // if(twitterGalleryCard instanceof TwitterGalleryCard)
            // ????
            log.warning("twitterGalleryCard is not an instance of TwitterGalleryCardBean.");
            bean = new TwitterGalleryCardBean(twitterGalleryCard.getGuid(), twitterGalleryCard.getCard(), twitterGalleryCard.getUrl(), twitterGalleryCard.getTitle(), twitterGalleryCard.getDescription(), twitterGalleryCard.getSite(), twitterGalleryCard.getSiteId(), twitterGalleryCard.getCreator(), twitterGalleryCard.getCreatorId(), twitterGalleryCard.getImage0(), twitterGalleryCard.getImage1(), twitterGalleryCard.getImage2(), twitterGalleryCard.getImage3());
        }
        Boolean suc = getProxyFactory().getTwitterGalleryCardServiceProxy().deleteTwitterGalleryCard(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteTwitterGalleryCards(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getTwitterGalleryCardServiceProxy().deleteTwitterGalleryCards(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createTwitterGalleryCards(List<TwitterGalleryCard> twitterGalleryCards) throws BaseException
    {
        log.finer("BEGIN");

        if(twitterGalleryCards == null) {
            log.log(Level.WARNING, "createTwitterGalleryCards() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = twitterGalleryCards.size();
        if(size == 0) {
            log.log(Level.WARNING, "createTwitterGalleryCards() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(TwitterGalleryCard twitterGalleryCard : twitterGalleryCards) {
            String guid = createTwitterGalleryCard(twitterGalleryCard);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createTwitterGalleryCards() failed for at least one twitterGalleryCard. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateTwitterGalleryCards(List<TwitterGalleryCard> twitterGalleryCards) throws BaseException
    //{
    //}

}
