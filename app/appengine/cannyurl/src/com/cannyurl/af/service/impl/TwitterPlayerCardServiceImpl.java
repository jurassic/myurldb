package com.cannyurl.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.TwitterPlayerCard;
import com.cannyurl.af.config.Config;

import com.cannyurl.af.bean.HashedPasswordStructBean;
import com.cannyurl.af.bean.GeoPointStructBean;
import com.cannyurl.af.bean.WebProfileStructBean;
import com.cannyurl.af.bean.StreetAddressStructBean;
import com.cannyurl.af.bean.SiteLogBean;
import com.cannyurl.af.bean.HelpNoticeBean;
import com.cannyurl.af.bean.TwitterCardAppInfoBean;
import com.cannyurl.af.bean.ContactInfoStructBean;
import com.cannyurl.af.bean.GeoCoordinateStructBean;
import com.cannyurl.af.bean.FullNameStructBean;
import com.cannyurl.af.bean.ExternalUserIdStructBean;
import com.cannyurl.af.bean.GaeUserStructBean;
import com.cannyurl.af.bean.AppBrandStructBean;
import com.cannyurl.af.bean.CellLatitudeLongitudeBean;
import com.cannyurl.af.bean.GaeAppStructBean;
import com.cannyurl.af.bean.ShortPassageAttributeBean;
import com.cannyurl.af.bean.KeyValuePairStructBean;
import com.cannyurl.af.bean.KeyValueRelationStructBean;
import com.cannyurl.af.bean.ExternalServiceApiKeyStructBean;
import com.cannyurl.af.bean.ReferrerInfoStructBean;
import com.cannyurl.af.bean.TwitterCardProductDataBean;
import com.cannyurl.af.bean.PagerStateStructBean;

import com.cannyurl.af.bean.TwitterPlayerCardBean;
import com.cannyurl.af.bean.TwitterCardAppInfoBean;
import com.cannyurl.af.bean.TwitterCardProductDataBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.TwitterPlayerCardService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class TwitterPlayerCardServiceImpl implements TwitterPlayerCardService
{
    private static final Logger log = Logger.getLogger(TwitterPlayerCardServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "TwitterPlayerCard-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("TwitterPlayerCard:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public TwitterPlayerCardServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // TwitterPlayerCard related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public TwitterPlayerCard getTwitterPlayerCard(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getTwitterPlayerCard(): guid = " + guid);

        TwitterPlayerCardBean bean = null;
        if(getCache() != null) {
            bean = (TwitterPlayerCardBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (TwitterPlayerCardBean) getProxyFactory().getTwitterPlayerCardServiceProxy().getTwitterPlayerCard(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "TwitterPlayerCardBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve TwitterPlayerCardBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getTwitterPlayerCard(String guid, String field) throws BaseException
    {
        TwitterPlayerCardBean bean = null;
        if(getCache() != null) {
            bean = (TwitterPlayerCardBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (TwitterPlayerCardBean) getProxyFactory().getTwitterPlayerCardServiceProxy().getTwitterPlayerCard(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "TwitterPlayerCardBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve TwitterPlayerCardBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("card")) {
            return bean.getCard();
        } else if(field.equals("url")) {
            return bean.getUrl();
        } else if(field.equals("title")) {
            return bean.getTitle();
        } else if(field.equals("description")) {
            return bean.getDescription();
        } else if(field.equals("site")) {
            return bean.getSite();
        } else if(field.equals("siteId")) {
            return bean.getSiteId();
        } else if(field.equals("creator")) {
            return bean.getCreator();
        } else if(field.equals("creatorId")) {
            return bean.getCreatorId();
        } else if(field.equals("image")) {
            return bean.getImage();
        } else if(field.equals("imageWidth")) {
            return bean.getImageWidth();
        } else if(field.equals("imageHeight")) {
            return bean.getImageHeight();
        } else if(field.equals("player")) {
            return bean.getPlayer();
        } else if(field.equals("playerWidth")) {
            return bean.getPlayerWidth();
        } else if(field.equals("playerHeight")) {
            return bean.getPlayerHeight();
        } else if(field.equals("playerStream")) {
            return bean.getPlayerStream();
        } else if(field.equals("playerStreamContentType")) {
            return bean.getPlayerStreamContentType();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<TwitterPlayerCard> getTwitterPlayerCards(List<String> guids) throws BaseException
    {
        log.fine("getTwitterPlayerCards()");

        // TBD: Is there a better way????
        List<TwitterPlayerCard> twitterPlayerCards = getProxyFactory().getTwitterPlayerCardServiceProxy().getTwitterPlayerCards(guids);
        if(twitterPlayerCards == null) {
            log.log(Level.WARNING, "Failed to retrieve TwitterPlayerCardBean list.");
        }

        log.finer("END");
        return twitterPlayerCards;
    }

    @Override
    public List<TwitterPlayerCard> getAllTwitterPlayerCards() throws BaseException
    {
        return getAllTwitterPlayerCards(null, null, null);
    }

    @Override
    public List<TwitterPlayerCard> getAllTwitterPlayerCards(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllTwitterPlayerCards(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<TwitterPlayerCard> twitterPlayerCards = getProxyFactory().getTwitterPlayerCardServiceProxy().getAllTwitterPlayerCards(ordering, offset, count);
        if(twitterPlayerCards == null) {
            log.log(Level.WARNING, "Failed to retrieve TwitterPlayerCardBean list.");
        }

        log.finer("END");
        return twitterPlayerCards;
    }

    @Override
    public List<String> getAllTwitterPlayerCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllTwitterPlayerCardKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getTwitterPlayerCardServiceProxy().getAllTwitterPlayerCardKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve TwitterPlayerCardBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty TwitterPlayerCardBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<TwitterPlayerCard> findTwitterPlayerCards(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findTwitterPlayerCards(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<TwitterPlayerCard> findTwitterPlayerCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TwitterPlayerCardServiceImpl.findTwitterPlayerCards(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<TwitterPlayerCard> twitterPlayerCards = getProxyFactory().getTwitterPlayerCardServiceProxy().findTwitterPlayerCards(filter, ordering, params, values, grouping, unique, offset, count);
        if(twitterPlayerCards == null) {
            log.log(Level.WARNING, "Failed to find twitterPlayerCards for the given criterion.");
        }

        log.finer("END");
        return twitterPlayerCards;
    }

    @Override
    public List<String> findTwitterPlayerCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TwitterPlayerCardServiceImpl.findTwitterPlayerCardKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getTwitterPlayerCardServiceProxy().findTwitterPlayerCardKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find TwitterPlayerCard keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty TwitterPlayerCard key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TwitterPlayerCardServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getTwitterPlayerCardServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createTwitterPlayerCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, String player, Integer playerWidth, Integer playerHeight, String playerStream, String playerStreamContentType) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        TwitterPlayerCardBean bean = new TwitterPlayerCardBean(null, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, player, playerWidth, playerHeight, playerStream, playerStreamContentType);
        return createTwitterPlayerCard(bean);
    }

    @Override
    public String createTwitterPlayerCard(TwitterPlayerCard twitterPlayerCard) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //TwitterPlayerCard bean = constructTwitterPlayerCard(twitterPlayerCard);
        //return bean.getGuid();

        // Param twitterPlayerCard cannot be null.....
        if(twitterPlayerCard == null) {
            log.log(Level.INFO, "Param twitterPlayerCard is null!");
            throw new BadRequestException("Param twitterPlayerCard object is null!");
        }
        TwitterPlayerCardBean bean = null;
        if(twitterPlayerCard instanceof TwitterPlayerCardBean) {
            bean = (TwitterPlayerCardBean) twitterPlayerCard;
        } else if(twitterPlayerCard instanceof TwitterPlayerCard) {
            // bean = new TwitterPlayerCardBean(null, twitterPlayerCard.getCard(), twitterPlayerCard.getUrl(), twitterPlayerCard.getTitle(), twitterPlayerCard.getDescription(), twitterPlayerCard.getSite(), twitterPlayerCard.getSiteId(), twitterPlayerCard.getCreator(), twitterPlayerCard.getCreatorId(), twitterPlayerCard.getImage(), twitterPlayerCard.getImageWidth(), twitterPlayerCard.getImageHeight(), twitterPlayerCard.getPlayer(), twitterPlayerCard.getPlayerWidth(), twitterPlayerCard.getPlayerHeight(), twitterPlayerCard.getPlayerStream(), twitterPlayerCard.getPlayerStreamContentType());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new TwitterPlayerCardBean(twitterPlayerCard.getGuid(), twitterPlayerCard.getCard(), twitterPlayerCard.getUrl(), twitterPlayerCard.getTitle(), twitterPlayerCard.getDescription(), twitterPlayerCard.getSite(), twitterPlayerCard.getSiteId(), twitterPlayerCard.getCreator(), twitterPlayerCard.getCreatorId(), twitterPlayerCard.getImage(), twitterPlayerCard.getImageWidth(), twitterPlayerCard.getImageHeight(), twitterPlayerCard.getPlayer(), twitterPlayerCard.getPlayerWidth(), twitterPlayerCard.getPlayerHeight(), twitterPlayerCard.getPlayerStream(), twitterPlayerCard.getPlayerStreamContentType());
        } else {
            log.log(Level.WARNING, "createTwitterPlayerCard(): Arg twitterPlayerCard is of an unknown type.");
            //bean = new TwitterPlayerCardBean();
            bean = new TwitterPlayerCardBean(twitterPlayerCard.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getTwitterPlayerCardServiceProxy().createTwitterPlayerCard(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public TwitterPlayerCard constructTwitterPlayerCard(TwitterPlayerCard twitterPlayerCard) throws BaseException
    {
        log.finer("BEGIN");

        // Param twitterPlayerCard cannot be null.....
        if(twitterPlayerCard == null) {
            log.log(Level.INFO, "Param twitterPlayerCard is null!");
            throw new BadRequestException("Param twitterPlayerCard object is null!");
        }
        TwitterPlayerCardBean bean = null;
        if(twitterPlayerCard instanceof TwitterPlayerCardBean) {
            bean = (TwitterPlayerCardBean) twitterPlayerCard;
        } else if(twitterPlayerCard instanceof TwitterPlayerCard) {
            // bean = new TwitterPlayerCardBean(null, twitterPlayerCard.getCard(), twitterPlayerCard.getUrl(), twitterPlayerCard.getTitle(), twitterPlayerCard.getDescription(), twitterPlayerCard.getSite(), twitterPlayerCard.getSiteId(), twitterPlayerCard.getCreator(), twitterPlayerCard.getCreatorId(), twitterPlayerCard.getImage(), twitterPlayerCard.getImageWidth(), twitterPlayerCard.getImageHeight(), twitterPlayerCard.getPlayer(), twitterPlayerCard.getPlayerWidth(), twitterPlayerCard.getPlayerHeight(), twitterPlayerCard.getPlayerStream(), twitterPlayerCard.getPlayerStreamContentType());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new TwitterPlayerCardBean(twitterPlayerCard.getGuid(), twitterPlayerCard.getCard(), twitterPlayerCard.getUrl(), twitterPlayerCard.getTitle(), twitterPlayerCard.getDescription(), twitterPlayerCard.getSite(), twitterPlayerCard.getSiteId(), twitterPlayerCard.getCreator(), twitterPlayerCard.getCreatorId(), twitterPlayerCard.getImage(), twitterPlayerCard.getImageWidth(), twitterPlayerCard.getImageHeight(), twitterPlayerCard.getPlayer(), twitterPlayerCard.getPlayerWidth(), twitterPlayerCard.getPlayerHeight(), twitterPlayerCard.getPlayerStream(), twitterPlayerCard.getPlayerStreamContentType());
        } else {
            log.log(Level.WARNING, "createTwitterPlayerCard(): Arg twitterPlayerCard is of an unknown type.");
            //bean = new TwitterPlayerCardBean();
            bean = new TwitterPlayerCardBean(twitterPlayerCard.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getTwitterPlayerCardServiceProxy().createTwitterPlayerCard(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateTwitterPlayerCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, String player, Integer playerWidth, Integer playerHeight, String playerStream, String playerStreamContentType) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        TwitterPlayerCardBean bean = new TwitterPlayerCardBean(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, player, playerWidth, playerHeight, playerStream, playerStreamContentType);
        return updateTwitterPlayerCard(bean);
    }
        
    @Override
    public Boolean updateTwitterPlayerCard(TwitterPlayerCard twitterPlayerCard) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //TwitterPlayerCard bean = refreshTwitterPlayerCard(twitterPlayerCard);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param twitterPlayerCard cannot be null.....
        if(twitterPlayerCard == null || twitterPlayerCard.getGuid() == null) {
            log.log(Level.INFO, "Param twitterPlayerCard or its guid is null!");
            throw new BadRequestException("Param twitterPlayerCard object or its guid is null!");
        }
        TwitterPlayerCardBean bean = null;
        if(twitterPlayerCard instanceof TwitterPlayerCardBean) {
            bean = (TwitterPlayerCardBean) twitterPlayerCard;
        } else {  // if(twitterPlayerCard instanceof TwitterPlayerCard)
            bean = new TwitterPlayerCardBean(twitterPlayerCard.getGuid(), twitterPlayerCard.getCard(), twitterPlayerCard.getUrl(), twitterPlayerCard.getTitle(), twitterPlayerCard.getDescription(), twitterPlayerCard.getSite(), twitterPlayerCard.getSiteId(), twitterPlayerCard.getCreator(), twitterPlayerCard.getCreatorId(), twitterPlayerCard.getImage(), twitterPlayerCard.getImageWidth(), twitterPlayerCard.getImageHeight(), twitterPlayerCard.getPlayer(), twitterPlayerCard.getPlayerWidth(), twitterPlayerCard.getPlayerHeight(), twitterPlayerCard.getPlayerStream(), twitterPlayerCard.getPlayerStreamContentType());
        }
        Boolean suc = getProxyFactory().getTwitterPlayerCardServiceProxy().updateTwitterPlayerCard(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public TwitterPlayerCard refreshTwitterPlayerCard(TwitterPlayerCard twitterPlayerCard) throws BaseException
    {
        log.finer("BEGIN");

        // Param twitterPlayerCard cannot be null.....
        if(twitterPlayerCard == null || twitterPlayerCard.getGuid() == null) {
            log.log(Level.INFO, "Param twitterPlayerCard or its guid is null!");
            throw new BadRequestException("Param twitterPlayerCard object or its guid is null!");
        }
        TwitterPlayerCardBean bean = null;
        if(twitterPlayerCard instanceof TwitterPlayerCardBean) {
            bean = (TwitterPlayerCardBean) twitterPlayerCard;
        } else {  // if(twitterPlayerCard instanceof TwitterPlayerCard)
            bean = new TwitterPlayerCardBean(twitterPlayerCard.getGuid(), twitterPlayerCard.getCard(), twitterPlayerCard.getUrl(), twitterPlayerCard.getTitle(), twitterPlayerCard.getDescription(), twitterPlayerCard.getSite(), twitterPlayerCard.getSiteId(), twitterPlayerCard.getCreator(), twitterPlayerCard.getCreatorId(), twitterPlayerCard.getImage(), twitterPlayerCard.getImageWidth(), twitterPlayerCard.getImageHeight(), twitterPlayerCard.getPlayer(), twitterPlayerCard.getPlayerWidth(), twitterPlayerCard.getPlayerHeight(), twitterPlayerCard.getPlayerStream(), twitterPlayerCard.getPlayerStreamContentType());
        }
        Boolean suc = getProxyFactory().getTwitterPlayerCardServiceProxy().updateTwitterPlayerCard(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteTwitterPlayerCard(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getTwitterPlayerCardServiceProxy().deleteTwitterPlayerCard(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }
            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            TwitterPlayerCard twitterPlayerCard = null;
            try {
                twitterPlayerCard = getProxyFactory().getTwitterPlayerCardServiceProxy().getTwitterPlayerCard(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch twitterPlayerCard with a key, " + guid);
                return false;
            }
            if(twitterPlayerCard != null) {
                String beanGuid = twitterPlayerCard.getGuid();
                Boolean suc1 = getProxyFactory().getTwitterPlayerCardServiceProxy().deleteTwitterPlayerCard(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("twitterPlayerCard with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteTwitterPlayerCard(TwitterPlayerCard twitterPlayerCard) throws BaseException
    {
        log.finer("BEGIN");

        // Param twitterPlayerCard cannot be null.....
        if(twitterPlayerCard == null || twitterPlayerCard.getGuid() == null) {
            log.log(Level.INFO, "Param twitterPlayerCard or its guid is null!");
            throw new BadRequestException("Param twitterPlayerCard object or its guid is null!");
        }
        TwitterPlayerCardBean bean = null;
        if(twitterPlayerCard instanceof TwitterPlayerCardBean) {
            bean = (TwitterPlayerCardBean) twitterPlayerCard;
        } else {  // if(twitterPlayerCard instanceof TwitterPlayerCard)
            // ????
            log.warning("twitterPlayerCard is not an instance of TwitterPlayerCardBean.");
            bean = new TwitterPlayerCardBean(twitterPlayerCard.getGuid(), twitterPlayerCard.getCard(), twitterPlayerCard.getUrl(), twitterPlayerCard.getTitle(), twitterPlayerCard.getDescription(), twitterPlayerCard.getSite(), twitterPlayerCard.getSiteId(), twitterPlayerCard.getCreator(), twitterPlayerCard.getCreatorId(), twitterPlayerCard.getImage(), twitterPlayerCard.getImageWidth(), twitterPlayerCard.getImageHeight(), twitterPlayerCard.getPlayer(), twitterPlayerCard.getPlayerWidth(), twitterPlayerCard.getPlayerHeight(), twitterPlayerCard.getPlayerStream(), twitterPlayerCard.getPlayerStreamContentType());
        }
        Boolean suc = getProxyFactory().getTwitterPlayerCardServiceProxy().deleteTwitterPlayerCard(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteTwitterPlayerCards(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getTwitterPlayerCardServiceProxy().deleteTwitterPlayerCards(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createTwitterPlayerCards(List<TwitterPlayerCard> twitterPlayerCards) throws BaseException
    {
        log.finer("BEGIN");

        if(twitterPlayerCards == null) {
            log.log(Level.WARNING, "createTwitterPlayerCards() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = twitterPlayerCards.size();
        if(size == 0) {
            log.log(Level.WARNING, "createTwitterPlayerCards() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(TwitterPlayerCard twitterPlayerCard : twitterPlayerCards) {
            String guid = createTwitterPlayerCard(twitterPlayerCard);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createTwitterPlayerCards() failed for at least one twitterPlayerCard. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateTwitterPlayerCards(List<TwitterPlayerCard> twitterPlayerCards) throws BaseException
    //{
    //}

}
