package com.cannyurl.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.ExternalUserIdStruct;
import com.myurldb.ws.UserAuthState;
import com.cannyurl.af.config.Config;

import com.cannyurl.af.bean.HashedPasswordStructBean;
import com.cannyurl.af.bean.GeoPointStructBean;
import com.cannyurl.af.bean.WebProfileStructBean;
import com.cannyurl.af.bean.StreetAddressStructBean;
import com.cannyurl.af.bean.SiteLogBean;
import com.cannyurl.af.bean.HelpNoticeBean;
import com.cannyurl.af.bean.TwitterCardAppInfoBean;
import com.cannyurl.af.bean.ContactInfoStructBean;
import com.cannyurl.af.bean.GeoCoordinateStructBean;
import com.cannyurl.af.bean.FullNameStructBean;
import com.cannyurl.af.bean.ExternalUserIdStructBean;
import com.cannyurl.af.bean.GaeUserStructBean;
import com.cannyurl.af.bean.AppBrandStructBean;
import com.cannyurl.af.bean.CellLatitudeLongitudeBean;
import com.cannyurl.af.bean.GaeAppStructBean;
import com.cannyurl.af.bean.ShortPassageAttributeBean;
import com.cannyurl.af.bean.KeyValuePairStructBean;
import com.cannyurl.af.bean.KeyValueRelationStructBean;
import com.cannyurl.af.bean.ExternalServiceApiKeyStructBean;
import com.cannyurl.af.bean.ReferrerInfoStructBean;
import com.cannyurl.af.bean.TwitterCardProductDataBean;
import com.cannyurl.af.bean.PagerStateStructBean;

import com.cannyurl.af.bean.UserAuthStateBean;
import com.cannyurl.af.bean.ExternalUserIdStructBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.UserAuthStateService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class UserAuthStateServiceImpl implements UserAuthStateService
{
    private static final Logger log = Logger.getLogger(UserAuthStateServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "UserAuthState-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("UserAuthState:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public UserAuthStateServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // UserAuthState related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public UserAuthState getUserAuthState(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getUserAuthState(): guid = " + guid);

        UserAuthStateBean bean = null;
        if(getCache() != null) {
            bean = (UserAuthStateBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (UserAuthStateBean) getProxyFactory().getUserAuthStateServiceProxy().getUserAuthState(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "UserAuthStateBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve UserAuthStateBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getUserAuthState(String guid, String field) throws BaseException
    {
        UserAuthStateBean bean = null;
        if(getCache() != null) {
            bean = (UserAuthStateBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (UserAuthStateBean) getProxyFactory().getUserAuthStateServiceProxy().getUserAuthState(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "UserAuthStateBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve UserAuthStateBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("providerId")) {
            return bean.getProviderId();
        } else if(field.equals("user")) {
            return bean.getUser();
        } else if(field.equals("username")) {
            return bean.getUsername();
        } else if(field.equals("email")) {
            return bean.getEmail();
        } else if(field.equals("openId")) {
            return bean.getOpenId();
        } else if(field.equals("deviceId")) {
            return bean.getDeviceId();
        } else if(field.equals("sessionId")) {
            return bean.getSessionId();
        } else if(field.equals("authToken")) {
            return bean.getAuthToken();
        } else if(field.equals("authStatus")) {
            return bean.getAuthStatus();
        } else if(field.equals("externalAuth")) {
            return bean.getExternalAuth();
        } else if(field.equals("externalId")) {
            return bean.getExternalId();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("firstAuthTime")) {
            return bean.getFirstAuthTime();
        } else if(field.equals("lastAuthTime")) {
            return bean.getLastAuthTime();
        } else if(field.equals("expirationTime")) {
            return bean.getExpirationTime();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<UserAuthState> getUserAuthStates(List<String> guids) throws BaseException
    {
        log.fine("getUserAuthStates()");

        // TBD: Is there a better way????
        List<UserAuthState> userAuthStates = getProxyFactory().getUserAuthStateServiceProxy().getUserAuthStates(guids);
        if(userAuthStates == null) {
            log.log(Level.WARNING, "Failed to retrieve UserAuthStateBean list.");
        }

        log.finer("END");
        return userAuthStates;
    }

    @Override
    public List<UserAuthState> getAllUserAuthStates() throws BaseException
    {
        return getAllUserAuthStates(null, null, null);
    }

    @Override
    public List<UserAuthState> getAllUserAuthStates(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllUserAuthStates(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<UserAuthState> userAuthStates = getProxyFactory().getUserAuthStateServiceProxy().getAllUserAuthStates(ordering, offset, count);
        if(userAuthStates == null) {
            log.log(Level.WARNING, "Failed to retrieve UserAuthStateBean list.");
        }

        log.finer("END");
        return userAuthStates;
    }

    @Override
    public List<String> getAllUserAuthStateKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllUserAuthStateKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getUserAuthStateServiceProxy().getAllUserAuthStateKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve UserAuthStateBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty UserAuthStateBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<UserAuthState> findUserAuthStates(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findUserAuthStates(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<UserAuthState> findUserAuthStates(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("UserAuthStateServiceImpl.findUserAuthStates(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<UserAuthState> userAuthStates = getProxyFactory().getUserAuthStateServiceProxy().findUserAuthStates(filter, ordering, params, values, grouping, unique, offset, count);
        if(userAuthStates == null) {
            log.log(Level.WARNING, "Failed to find userAuthStates for the given criterion.");
        }

        log.finer("END");
        return userAuthStates;
    }

    @Override
    public List<String> findUserAuthStateKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("UserAuthStateServiceImpl.findUserAuthStateKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getUserAuthStateServiceProxy().findUserAuthStateKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find UserAuthState keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty UserAuthState key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("UserAuthStateServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getUserAuthStateServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createUserAuthState(String providerId, String user, String username, String email, String openId, String deviceId, String sessionId, String authToken, String authStatus, String externalAuth, ExternalUserIdStruct externalId, String status, Long firstAuthTime, Long lastAuthTime, Long expirationTime) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        ExternalUserIdStructBean externalIdBean = null;
        if(externalId instanceof ExternalUserIdStructBean) {
            externalIdBean = (ExternalUserIdStructBean) externalId;
        } else if(externalId instanceof ExternalUserIdStruct) {
            externalIdBean = new ExternalUserIdStructBean(externalId.getUuid(), externalId.getId(), externalId.getName(), externalId.getEmail(), externalId.getUsername(), externalId.getOpenId(), externalId.getNote());
        } else {
            externalIdBean = null;   // ????
        }
        UserAuthStateBean bean = new UserAuthStateBean(null, providerId, user, username, email, openId, deviceId, sessionId, authToken, authStatus, externalAuth, externalIdBean, status, firstAuthTime, lastAuthTime, expirationTime);
        return createUserAuthState(bean);
    }

    @Override
    public String createUserAuthState(UserAuthState userAuthState) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //UserAuthState bean = constructUserAuthState(userAuthState);
        //return bean.getGuid();

        // Param userAuthState cannot be null.....
        if(userAuthState == null) {
            log.log(Level.INFO, "Param userAuthState is null!");
            throw new BadRequestException("Param userAuthState object is null!");
        }
        UserAuthStateBean bean = null;
        if(userAuthState instanceof UserAuthStateBean) {
            bean = (UserAuthStateBean) userAuthState;
        } else if(userAuthState instanceof UserAuthState) {
            // bean = new UserAuthStateBean(null, userAuthState.getProviderId(), userAuthState.getUser(), userAuthState.getUsername(), userAuthState.getEmail(), userAuthState.getOpenId(), userAuthState.getDeviceId(), userAuthState.getSessionId(), userAuthState.getAuthToken(), userAuthState.getAuthStatus(), userAuthState.getExternalAuth(), (ExternalUserIdStructBean) userAuthState.getExternalId(), userAuthState.getStatus(), userAuthState.getFirstAuthTime(), userAuthState.getLastAuthTime(), userAuthState.getExpirationTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new UserAuthStateBean(userAuthState.getGuid(), userAuthState.getProviderId(), userAuthState.getUser(), userAuthState.getUsername(), userAuthState.getEmail(), userAuthState.getOpenId(), userAuthState.getDeviceId(), userAuthState.getSessionId(), userAuthState.getAuthToken(), userAuthState.getAuthStatus(), userAuthState.getExternalAuth(), (ExternalUserIdStructBean) userAuthState.getExternalId(), userAuthState.getStatus(), userAuthState.getFirstAuthTime(), userAuthState.getLastAuthTime(), userAuthState.getExpirationTime());
        } else {
            log.log(Level.WARNING, "createUserAuthState(): Arg userAuthState is of an unknown type.");
            //bean = new UserAuthStateBean();
            bean = new UserAuthStateBean(userAuthState.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getUserAuthStateServiceProxy().createUserAuthState(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public UserAuthState constructUserAuthState(UserAuthState userAuthState) throws BaseException
    {
        log.finer("BEGIN");

        // Param userAuthState cannot be null.....
        if(userAuthState == null) {
            log.log(Level.INFO, "Param userAuthState is null!");
            throw new BadRequestException("Param userAuthState object is null!");
        }
        UserAuthStateBean bean = null;
        if(userAuthState instanceof UserAuthStateBean) {
            bean = (UserAuthStateBean) userAuthState;
        } else if(userAuthState instanceof UserAuthState) {
            // bean = new UserAuthStateBean(null, userAuthState.getProviderId(), userAuthState.getUser(), userAuthState.getUsername(), userAuthState.getEmail(), userAuthState.getOpenId(), userAuthState.getDeviceId(), userAuthState.getSessionId(), userAuthState.getAuthToken(), userAuthState.getAuthStatus(), userAuthState.getExternalAuth(), (ExternalUserIdStructBean) userAuthState.getExternalId(), userAuthState.getStatus(), userAuthState.getFirstAuthTime(), userAuthState.getLastAuthTime(), userAuthState.getExpirationTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new UserAuthStateBean(userAuthState.getGuid(), userAuthState.getProviderId(), userAuthState.getUser(), userAuthState.getUsername(), userAuthState.getEmail(), userAuthState.getOpenId(), userAuthState.getDeviceId(), userAuthState.getSessionId(), userAuthState.getAuthToken(), userAuthState.getAuthStatus(), userAuthState.getExternalAuth(), (ExternalUserIdStructBean) userAuthState.getExternalId(), userAuthState.getStatus(), userAuthState.getFirstAuthTime(), userAuthState.getLastAuthTime(), userAuthState.getExpirationTime());
        } else {
            log.log(Level.WARNING, "createUserAuthState(): Arg userAuthState is of an unknown type.");
            //bean = new UserAuthStateBean();
            bean = new UserAuthStateBean(userAuthState.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getUserAuthStateServiceProxy().createUserAuthState(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateUserAuthState(String guid, String providerId, String user, String username, String email, String openId, String deviceId, String sessionId, String authToken, String authStatus, String externalAuth, ExternalUserIdStruct externalId, String status, Long firstAuthTime, Long lastAuthTime, Long expirationTime) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        ExternalUserIdStructBean externalIdBean = null;
        if(externalId instanceof ExternalUserIdStructBean) {
            externalIdBean = (ExternalUserIdStructBean) externalId;
        } else if(externalId instanceof ExternalUserIdStruct) {
            externalIdBean = new ExternalUserIdStructBean(externalId.getUuid(), externalId.getId(), externalId.getName(), externalId.getEmail(), externalId.getUsername(), externalId.getOpenId(), externalId.getNote());
        } else {
            externalIdBean = null;   // ????
        }
        UserAuthStateBean bean = new UserAuthStateBean(guid, providerId, user, username, email, openId, deviceId, sessionId, authToken, authStatus, externalAuth, externalIdBean, status, firstAuthTime, lastAuthTime, expirationTime);
        return updateUserAuthState(bean);
    }
        
    @Override
    public Boolean updateUserAuthState(UserAuthState userAuthState) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //UserAuthState bean = refreshUserAuthState(userAuthState);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param userAuthState cannot be null.....
        if(userAuthState == null || userAuthState.getGuid() == null) {
            log.log(Level.INFO, "Param userAuthState or its guid is null!");
            throw new BadRequestException("Param userAuthState object or its guid is null!");
        }
        UserAuthStateBean bean = null;
        if(userAuthState instanceof UserAuthStateBean) {
            bean = (UserAuthStateBean) userAuthState;
        } else {  // if(userAuthState instanceof UserAuthState)
            bean = new UserAuthStateBean(userAuthState.getGuid(), userAuthState.getProviderId(), userAuthState.getUser(), userAuthState.getUsername(), userAuthState.getEmail(), userAuthState.getOpenId(), userAuthState.getDeviceId(), userAuthState.getSessionId(), userAuthState.getAuthToken(), userAuthState.getAuthStatus(), userAuthState.getExternalAuth(), (ExternalUserIdStructBean) userAuthState.getExternalId(), userAuthState.getStatus(), userAuthState.getFirstAuthTime(), userAuthState.getLastAuthTime(), userAuthState.getExpirationTime());
        }
        Boolean suc = getProxyFactory().getUserAuthStateServiceProxy().updateUserAuthState(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public UserAuthState refreshUserAuthState(UserAuthState userAuthState) throws BaseException
    {
        log.finer("BEGIN");

        // Param userAuthState cannot be null.....
        if(userAuthState == null || userAuthState.getGuid() == null) {
            log.log(Level.INFO, "Param userAuthState or its guid is null!");
            throw new BadRequestException("Param userAuthState object or its guid is null!");
        }
        UserAuthStateBean bean = null;
        if(userAuthState instanceof UserAuthStateBean) {
            bean = (UserAuthStateBean) userAuthState;
        } else {  // if(userAuthState instanceof UserAuthState)
            bean = new UserAuthStateBean(userAuthState.getGuid(), userAuthState.getProviderId(), userAuthState.getUser(), userAuthState.getUsername(), userAuthState.getEmail(), userAuthState.getOpenId(), userAuthState.getDeviceId(), userAuthState.getSessionId(), userAuthState.getAuthToken(), userAuthState.getAuthStatus(), userAuthState.getExternalAuth(), (ExternalUserIdStructBean) userAuthState.getExternalId(), userAuthState.getStatus(), userAuthState.getFirstAuthTime(), userAuthState.getLastAuthTime(), userAuthState.getExpirationTime());
        }
        Boolean suc = getProxyFactory().getUserAuthStateServiceProxy().updateUserAuthState(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteUserAuthState(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getUserAuthStateServiceProxy().deleteUserAuthState(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }
            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            UserAuthState userAuthState = null;
            try {
                userAuthState = getProxyFactory().getUserAuthStateServiceProxy().getUserAuthState(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch userAuthState with a key, " + guid);
                return false;
            }
            if(userAuthState != null) {
                String beanGuid = userAuthState.getGuid();
                Boolean suc1 = getProxyFactory().getUserAuthStateServiceProxy().deleteUserAuthState(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("userAuthState with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteUserAuthState(UserAuthState userAuthState) throws BaseException
    {
        log.finer("BEGIN");

        // Param userAuthState cannot be null.....
        if(userAuthState == null || userAuthState.getGuid() == null) {
            log.log(Level.INFO, "Param userAuthState or its guid is null!");
            throw new BadRequestException("Param userAuthState object or its guid is null!");
        }
        UserAuthStateBean bean = null;
        if(userAuthState instanceof UserAuthStateBean) {
            bean = (UserAuthStateBean) userAuthState;
        } else {  // if(userAuthState instanceof UserAuthState)
            // ????
            log.warning("userAuthState is not an instance of UserAuthStateBean.");
            bean = new UserAuthStateBean(userAuthState.getGuid(), userAuthState.getProviderId(), userAuthState.getUser(), userAuthState.getUsername(), userAuthState.getEmail(), userAuthState.getOpenId(), userAuthState.getDeviceId(), userAuthState.getSessionId(), userAuthState.getAuthToken(), userAuthState.getAuthStatus(), userAuthState.getExternalAuth(), (ExternalUserIdStructBean) userAuthState.getExternalId(), userAuthState.getStatus(), userAuthState.getFirstAuthTime(), userAuthState.getLastAuthTime(), userAuthState.getExpirationTime());
        }
        Boolean suc = getProxyFactory().getUserAuthStateServiceProxy().deleteUserAuthState(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteUserAuthStates(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getUserAuthStateServiceProxy().deleteUserAuthStates(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createUserAuthStates(List<UserAuthState> userAuthStates) throws BaseException
    {
        log.finer("BEGIN");

        if(userAuthStates == null) {
            log.log(Level.WARNING, "createUserAuthStates() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = userAuthStates.size();
        if(size == 0) {
            log.log(Level.WARNING, "createUserAuthStates() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(UserAuthState userAuthState : userAuthStates) {
            String guid = createUserAuthState(userAuthState);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createUserAuthStates() failed for at least one userAuthState. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateUserAuthStates(List<UserAuthState> userAuthStates) throws BaseException
    //{
    //}

}
