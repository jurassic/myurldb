package com.cannyurl.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.UserCustomDomain;
import com.cannyurl.af.config.Config;

import com.cannyurl.af.bean.HashedPasswordStructBean;
import com.cannyurl.af.bean.GeoPointStructBean;
import com.cannyurl.af.bean.WebProfileStructBean;
import com.cannyurl.af.bean.StreetAddressStructBean;
import com.cannyurl.af.bean.SiteLogBean;
import com.cannyurl.af.bean.HelpNoticeBean;
import com.cannyurl.af.bean.TwitterCardAppInfoBean;
import com.cannyurl.af.bean.ContactInfoStructBean;
import com.cannyurl.af.bean.GeoCoordinateStructBean;
import com.cannyurl.af.bean.FullNameStructBean;
import com.cannyurl.af.bean.ExternalUserIdStructBean;
import com.cannyurl.af.bean.GaeUserStructBean;
import com.cannyurl.af.bean.AppBrandStructBean;
import com.cannyurl.af.bean.CellLatitudeLongitudeBean;
import com.cannyurl.af.bean.GaeAppStructBean;
import com.cannyurl.af.bean.ShortPassageAttributeBean;
import com.cannyurl.af.bean.KeyValuePairStructBean;
import com.cannyurl.af.bean.KeyValueRelationStructBean;
import com.cannyurl.af.bean.ExternalServiceApiKeyStructBean;
import com.cannyurl.af.bean.ReferrerInfoStructBean;
import com.cannyurl.af.bean.TwitterCardProductDataBean;
import com.cannyurl.af.bean.PagerStateStructBean;

import com.cannyurl.af.bean.UserCustomDomainBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.UserCustomDomainService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class UserCustomDomainServiceImpl implements UserCustomDomainService
{
    private static final Logger log = Logger.getLogger(UserCustomDomainServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "UserCustomDomain-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("UserCustomDomain:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public UserCustomDomainServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // UserCustomDomain related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public UserCustomDomain getUserCustomDomain(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getUserCustomDomain(): guid = " + guid);

        UserCustomDomainBean bean = null;
        if(getCache() != null) {
            bean = (UserCustomDomainBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (UserCustomDomainBean) getProxyFactory().getUserCustomDomainServiceProxy().getUserCustomDomain(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "UserCustomDomainBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve UserCustomDomainBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getUserCustomDomain(String guid, String field) throws BaseException
    {
        UserCustomDomainBean bean = null;
        if(getCache() != null) {
            bean = (UserCustomDomainBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (UserCustomDomainBean) getProxyFactory().getUserCustomDomainServiceProxy().getUserCustomDomain(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "UserCustomDomainBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve UserCustomDomainBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("owner")) {
            return bean.getOwner();
        } else if(field.equals("domain")) {
            return bean.getDomain();
        } else if(field.equals("verified")) {
            return bean.isVerified();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("verifiedTime")) {
            return bean.getVerifiedTime();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<UserCustomDomain> getUserCustomDomains(List<String> guids) throws BaseException
    {
        log.fine("getUserCustomDomains()");

        // TBD: Is there a better way????
        List<UserCustomDomain> userCustomDomains = getProxyFactory().getUserCustomDomainServiceProxy().getUserCustomDomains(guids);
        if(userCustomDomains == null) {
            log.log(Level.WARNING, "Failed to retrieve UserCustomDomainBean list.");
        }

        log.finer("END");
        return userCustomDomains;
    }

    @Override
    public List<UserCustomDomain> getAllUserCustomDomains() throws BaseException
    {
        return getAllUserCustomDomains(null, null, null);
    }

    @Override
    public List<UserCustomDomain> getAllUserCustomDomains(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllUserCustomDomains(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<UserCustomDomain> userCustomDomains = getProxyFactory().getUserCustomDomainServiceProxy().getAllUserCustomDomains(ordering, offset, count);
        if(userCustomDomains == null) {
            log.log(Level.WARNING, "Failed to retrieve UserCustomDomainBean list.");
        }

        log.finer("END");
        return userCustomDomains;
    }

    @Override
    public List<String> getAllUserCustomDomainKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllUserCustomDomainKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getUserCustomDomainServiceProxy().getAllUserCustomDomainKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve UserCustomDomainBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty UserCustomDomainBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<UserCustomDomain> findUserCustomDomains(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findUserCustomDomains(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<UserCustomDomain> findUserCustomDomains(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("UserCustomDomainServiceImpl.findUserCustomDomains(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<UserCustomDomain> userCustomDomains = getProxyFactory().getUserCustomDomainServiceProxy().findUserCustomDomains(filter, ordering, params, values, grouping, unique, offset, count);
        if(userCustomDomains == null) {
            log.log(Level.WARNING, "Failed to find userCustomDomains for the given criterion.");
        }

        log.finer("END");
        return userCustomDomains;
    }

    @Override
    public List<String> findUserCustomDomainKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("UserCustomDomainServiceImpl.findUserCustomDomainKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getUserCustomDomainServiceProxy().findUserCustomDomainKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find UserCustomDomain keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty UserCustomDomain key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("UserCustomDomainServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getUserCustomDomainServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createUserCustomDomain(String owner, String domain, Boolean verified, String status, Long verifiedTime) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        UserCustomDomainBean bean = new UserCustomDomainBean(null, owner, domain, verified, status, verifiedTime);
        return createUserCustomDomain(bean);
    }

    @Override
    public String createUserCustomDomain(UserCustomDomain userCustomDomain) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //UserCustomDomain bean = constructUserCustomDomain(userCustomDomain);
        //return bean.getGuid();

        // Param userCustomDomain cannot be null.....
        if(userCustomDomain == null) {
            log.log(Level.INFO, "Param userCustomDomain is null!");
            throw new BadRequestException("Param userCustomDomain object is null!");
        }
        UserCustomDomainBean bean = null;
        if(userCustomDomain instanceof UserCustomDomainBean) {
            bean = (UserCustomDomainBean) userCustomDomain;
        } else if(userCustomDomain instanceof UserCustomDomain) {
            // bean = new UserCustomDomainBean(null, userCustomDomain.getOwner(), userCustomDomain.getDomain(), userCustomDomain.isVerified(), userCustomDomain.getStatus(), userCustomDomain.getVerifiedTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new UserCustomDomainBean(userCustomDomain.getGuid(), userCustomDomain.getOwner(), userCustomDomain.getDomain(), userCustomDomain.isVerified(), userCustomDomain.getStatus(), userCustomDomain.getVerifiedTime());
        } else {
            log.log(Level.WARNING, "createUserCustomDomain(): Arg userCustomDomain is of an unknown type.");
            //bean = new UserCustomDomainBean();
            bean = new UserCustomDomainBean(userCustomDomain.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getUserCustomDomainServiceProxy().createUserCustomDomain(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public UserCustomDomain constructUserCustomDomain(UserCustomDomain userCustomDomain) throws BaseException
    {
        log.finer("BEGIN");

        // Param userCustomDomain cannot be null.....
        if(userCustomDomain == null) {
            log.log(Level.INFO, "Param userCustomDomain is null!");
            throw new BadRequestException("Param userCustomDomain object is null!");
        }
        UserCustomDomainBean bean = null;
        if(userCustomDomain instanceof UserCustomDomainBean) {
            bean = (UserCustomDomainBean) userCustomDomain;
        } else if(userCustomDomain instanceof UserCustomDomain) {
            // bean = new UserCustomDomainBean(null, userCustomDomain.getOwner(), userCustomDomain.getDomain(), userCustomDomain.isVerified(), userCustomDomain.getStatus(), userCustomDomain.getVerifiedTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new UserCustomDomainBean(userCustomDomain.getGuid(), userCustomDomain.getOwner(), userCustomDomain.getDomain(), userCustomDomain.isVerified(), userCustomDomain.getStatus(), userCustomDomain.getVerifiedTime());
        } else {
            log.log(Level.WARNING, "createUserCustomDomain(): Arg userCustomDomain is of an unknown type.");
            //bean = new UserCustomDomainBean();
            bean = new UserCustomDomainBean(userCustomDomain.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getUserCustomDomainServiceProxy().createUserCustomDomain(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateUserCustomDomain(String guid, String owner, String domain, Boolean verified, String status, Long verifiedTime) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        UserCustomDomainBean bean = new UserCustomDomainBean(guid, owner, domain, verified, status, verifiedTime);
        return updateUserCustomDomain(bean);
    }
        
    @Override
    public Boolean updateUserCustomDomain(UserCustomDomain userCustomDomain) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //UserCustomDomain bean = refreshUserCustomDomain(userCustomDomain);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param userCustomDomain cannot be null.....
        if(userCustomDomain == null || userCustomDomain.getGuid() == null) {
            log.log(Level.INFO, "Param userCustomDomain or its guid is null!");
            throw new BadRequestException("Param userCustomDomain object or its guid is null!");
        }
        UserCustomDomainBean bean = null;
        if(userCustomDomain instanceof UserCustomDomainBean) {
            bean = (UserCustomDomainBean) userCustomDomain;
        } else {  // if(userCustomDomain instanceof UserCustomDomain)
            bean = new UserCustomDomainBean(userCustomDomain.getGuid(), userCustomDomain.getOwner(), userCustomDomain.getDomain(), userCustomDomain.isVerified(), userCustomDomain.getStatus(), userCustomDomain.getVerifiedTime());
        }
        Boolean suc = getProxyFactory().getUserCustomDomainServiceProxy().updateUserCustomDomain(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public UserCustomDomain refreshUserCustomDomain(UserCustomDomain userCustomDomain) throws BaseException
    {
        log.finer("BEGIN");

        // Param userCustomDomain cannot be null.....
        if(userCustomDomain == null || userCustomDomain.getGuid() == null) {
            log.log(Level.INFO, "Param userCustomDomain or its guid is null!");
            throw new BadRequestException("Param userCustomDomain object or its guid is null!");
        }
        UserCustomDomainBean bean = null;
        if(userCustomDomain instanceof UserCustomDomainBean) {
            bean = (UserCustomDomainBean) userCustomDomain;
        } else {  // if(userCustomDomain instanceof UserCustomDomain)
            bean = new UserCustomDomainBean(userCustomDomain.getGuid(), userCustomDomain.getOwner(), userCustomDomain.getDomain(), userCustomDomain.isVerified(), userCustomDomain.getStatus(), userCustomDomain.getVerifiedTime());
        }
        Boolean suc = getProxyFactory().getUserCustomDomainServiceProxy().updateUserCustomDomain(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteUserCustomDomain(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getUserCustomDomainServiceProxy().deleteUserCustomDomain(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }
            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            UserCustomDomain userCustomDomain = null;
            try {
                userCustomDomain = getProxyFactory().getUserCustomDomainServiceProxy().getUserCustomDomain(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch userCustomDomain with a key, " + guid);
                return false;
            }
            if(userCustomDomain != null) {
                String beanGuid = userCustomDomain.getGuid();
                Boolean suc1 = getProxyFactory().getUserCustomDomainServiceProxy().deleteUserCustomDomain(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("userCustomDomain with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteUserCustomDomain(UserCustomDomain userCustomDomain) throws BaseException
    {
        log.finer("BEGIN");

        // Param userCustomDomain cannot be null.....
        if(userCustomDomain == null || userCustomDomain.getGuid() == null) {
            log.log(Level.INFO, "Param userCustomDomain or its guid is null!");
            throw new BadRequestException("Param userCustomDomain object or its guid is null!");
        }
        UserCustomDomainBean bean = null;
        if(userCustomDomain instanceof UserCustomDomainBean) {
            bean = (UserCustomDomainBean) userCustomDomain;
        } else {  // if(userCustomDomain instanceof UserCustomDomain)
            // ????
            log.warning("userCustomDomain is not an instance of UserCustomDomainBean.");
            bean = new UserCustomDomainBean(userCustomDomain.getGuid(), userCustomDomain.getOwner(), userCustomDomain.getDomain(), userCustomDomain.isVerified(), userCustomDomain.getStatus(), userCustomDomain.getVerifiedTime());
        }
        Boolean suc = getProxyFactory().getUserCustomDomainServiceProxy().deleteUserCustomDomain(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteUserCustomDomains(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getUserCustomDomainServiceProxy().deleteUserCustomDomains(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createUserCustomDomains(List<UserCustomDomain> userCustomDomains) throws BaseException
    {
        log.finer("BEGIN");

        if(userCustomDomains == null) {
            log.log(Level.WARNING, "createUserCustomDomains() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = userCustomDomains.size();
        if(size == 0) {
            log.log(Level.WARNING, "createUserCustomDomains() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(UserCustomDomain userCustomDomain : userCustomDomains) {
            String guid = createUserCustomDomain(userCustomDomain);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createUserCustomDomains() failed for at least one userCustomDomain. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateUserCustomDomains(List<UserCustomDomain> userCustomDomains) throws BaseException
    //{
    //}

}
