package com.cannyurl.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.HashedPasswordStruct;
import com.myurldb.ws.UserPassword;
import com.cannyurl.af.config.Config;

import com.cannyurl.af.bean.HashedPasswordStructBean;
import com.cannyurl.af.bean.GeoPointStructBean;
import com.cannyurl.af.bean.WebProfileStructBean;
import com.cannyurl.af.bean.StreetAddressStructBean;
import com.cannyurl.af.bean.SiteLogBean;
import com.cannyurl.af.bean.HelpNoticeBean;
import com.cannyurl.af.bean.TwitterCardAppInfoBean;
import com.cannyurl.af.bean.ContactInfoStructBean;
import com.cannyurl.af.bean.GeoCoordinateStructBean;
import com.cannyurl.af.bean.FullNameStructBean;
import com.cannyurl.af.bean.ExternalUserIdStructBean;
import com.cannyurl.af.bean.GaeUserStructBean;
import com.cannyurl.af.bean.AppBrandStructBean;
import com.cannyurl.af.bean.CellLatitudeLongitudeBean;
import com.cannyurl.af.bean.GaeAppStructBean;
import com.cannyurl.af.bean.ShortPassageAttributeBean;
import com.cannyurl.af.bean.KeyValuePairStructBean;
import com.cannyurl.af.bean.KeyValueRelationStructBean;
import com.cannyurl.af.bean.ExternalServiceApiKeyStructBean;
import com.cannyurl.af.bean.ReferrerInfoStructBean;
import com.cannyurl.af.bean.TwitterCardProductDataBean;
import com.cannyurl.af.bean.PagerStateStructBean;

import com.cannyurl.af.bean.UserPasswordBean;
import com.cannyurl.af.bean.HashedPasswordStructBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.UserPasswordService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class UserPasswordServiceImpl implements UserPasswordService
{
    private static final Logger log = Logger.getLogger(UserPasswordServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "UserPassword-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("UserPassword:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public UserPasswordServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // UserPassword related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public UserPassword getUserPassword(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getUserPassword(): guid = " + guid);

        UserPasswordBean bean = null;
        if(getCache() != null) {
            bean = (UserPasswordBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (UserPasswordBean) getProxyFactory().getUserPasswordServiceProxy().getUserPassword(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "UserPasswordBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve UserPasswordBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getUserPassword(String guid, String field) throws BaseException
    {
        UserPasswordBean bean = null;
        if(getCache() != null) {
            bean = (UserPasswordBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (UserPasswordBean) getProxyFactory().getUserPasswordServiceProxy().getUserPassword(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "UserPasswordBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve UserPasswordBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("admin")) {
            return bean.getAdmin();
        } else if(field.equals("user")) {
            return bean.getUser();
        } else if(field.equals("username")) {
            return bean.getUsername();
        } else if(field.equals("email")) {
            return bean.getEmail();
        } else if(field.equals("openId")) {
            return bean.getOpenId();
        } else if(field.equals("password")) {
            return bean.getPassword();
        } else if(field.equals("resetRequired")) {
            return bean.isResetRequired();
        } else if(field.equals("challengeQuestion")) {
            return bean.getChallengeQuestion();
        } else if(field.equals("challengeAnswer")) {
            return bean.getChallengeAnswer();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("lastResetTime")) {
            return bean.getLastResetTime();
        } else if(field.equals("expirationTime")) {
            return bean.getExpirationTime();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<UserPassword> getUserPasswords(List<String> guids) throws BaseException
    {
        log.fine("getUserPasswords()");

        // TBD: Is there a better way????
        List<UserPassword> userPasswords = getProxyFactory().getUserPasswordServiceProxy().getUserPasswords(guids);
        if(userPasswords == null) {
            log.log(Level.WARNING, "Failed to retrieve UserPasswordBean list.");
        }

        log.finer("END");
        return userPasswords;
    }

    @Override
    public List<UserPassword> getAllUserPasswords() throws BaseException
    {
        return getAllUserPasswords(null, null, null);
    }

    @Override
    public List<UserPassword> getAllUserPasswords(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllUserPasswords(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<UserPassword> userPasswords = getProxyFactory().getUserPasswordServiceProxy().getAllUserPasswords(ordering, offset, count);
        if(userPasswords == null) {
            log.log(Level.WARNING, "Failed to retrieve UserPasswordBean list.");
        }

        log.finer("END");
        return userPasswords;
    }

    @Override
    public List<String> getAllUserPasswordKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllUserPasswordKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getUserPasswordServiceProxy().getAllUserPasswordKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve UserPasswordBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty UserPasswordBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<UserPassword> findUserPasswords(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findUserPasswords(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<UserPassword> findUserPasswords(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("UserPasswordServiceImpl.findUserPasswords(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<UserPassword> userPasswords = getProxyFactory().getUserPasswordServiceProxy().findUserPasswords(filter, ordering, params, values, grouping, unique, offset, count);
        if(userPasswords == null) {
            log.log(Level.WARNING, "Failed to find userPasswords for the given criterion.");
        }

        log.finer("END");
        return userPasswords;
    }

    @Override
    public List<String> findUserPasswordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("UserPasswordServiceImpl.findUserPasswordKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getUserPasswordServiceProxy().findUserPasswordKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find UserPassword keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty UserPassword key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("UserPasswordServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getUserPasswordServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createUserPassword(String admin, String user, String username, String email, String openId, HashedPasswordStruct password, Boolean resetRequired, String challengeQuestion, String challengeAnswer, String status, Long lastResetTime, Long expirationTime) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        HashedPasswordStructBean passwordBean = null;
        if(password instanceof HashedPasswordStructBean) {
            passwordBean = (HashedPasswordStructBean) password;
        } else if(password instanceof HashedPasswordStruct) {
            passwordBean = new HashedPasswordStructBean(password.getUuid(), password.getPlainText(), password.getHashedText(), password.getSalt(), password.getAlgorithm());
        } else {
            passwordBean = null;   // ????
        }
        UserPasswordBean bean = new UserPasswordBean(null, admin, user, username, email, openId, passwordBean, resetRequired, challengeQuestion, challengeAnswer, status, lastResetTime, expirationTime);
        return createUserPassword(bean);
    }

    @Override
    public String createUserPassword(UserPassword userPassword) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //UserPassword bean = constructUserPassword(userPassword);
        //return bean.getGuid();

        // Param userPassword cannot be null.....
        if(userPassword == null) {
            log.log(Level.INFO, "Param userPassword is null!");
            throw new BadRequestException("Param userPassword object is null!");
        }
        UserPasswordBean bean = null;
        if(userPassword instanceof UserPasswordBean) {
            bean = (UserPasswordBean) userPassword;
        } else if(userPassword instanceof UserPassword) {
            // bean = new UserPasswordBean(null, userPassword.getAdmin(), userPassword.getUser(), userPassword.getUsername(), userPassword.getEmail(), userPassword.getOpenId(), (HashedPasswordStructBean) userPassword.getPassword(), userPassword.isResetRequired(), userPassword.getChallengeQuestion(), userPassword.getChallengeAnswer(), userPassword.getStatus(), userPassword.getLastResetTime(), userPassword.getExpirationTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new UserPasswordBean(userPassword.getGuid(), userPassword.getAdmin(), userPassword.getUser(), userPassword.getUsername(), userPassword.getEmail(), userPassword.getOpenId(), (HashedPasswordStructBean) userPassword.getPassword(), userPassword.isResetRequired(), userPassword.getChallengeQuestion(), userPassword.getChallengeAnswer(), userPassword.getStatus(), userPassword.getLastResetTime(), userPassword.getExpirationTime());
        } else {
            log.log(Level.WARNING, "createUserPassword(): Arg userPassword is of an unknown type.");
            //bean = new UserPasswordBean();
            bean = new UserPasswordBean(userPassword.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getUserPasswordServiceProxy().createUserPassword(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public UserPassword constructUserPassword(UserPassword userPassword) throws BaseException
    {
        log.finer("BEGIN");

        // Param userPassword cannot be null.....
        if(userPassword == null) {
            log.log(Level.INFO, "Param userPassword is null!");
            throw new BadRequestException("Param userPassword object is null!");
        }
        UserPasswordBean bean = null;
        if(userPassword instanceof UserPasswordBean) {
            bean = (UserPasswordBean) userPassword;
        } else if(userPassword instanceof UserPassword) {
            // bean = new UserPasswordBean(null, userPassword.getAdmin(), userPassword.getUser(), userPassword.getUsername(), userPassword.getEmail(), userPassword.getOpenId(), (HashedPasswordStructBean) userPassword.getPassword(), userPassword.isResetRequired(), userPassword.getChallengeQuestion(), userPassword.getChallengeAnswer(), userPassword.getStatus(), userPassword.getLastResetTime(), userPassword.getExpirationTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new UserPasswordBean(userPassword.getGuid(), userPassword.getAdmin(), userPassword.getUser(), userPassword.getUsername(), userPassword.getEmail(), userPassword.getOpenId(), (HashedPasswordStructBean) userPassword.getPassword(), userPassword.isResetRequired(), userPassword.getChallengeQuestion(), userPassword.getChallengeAnswer(), userPassword.getStatus(), userPassword.getLastResetTime(), userPassword.getExpirationTime());
        } else {
            log.log(Level.WARNING, "createUserPassword(): Arg userPassword is of an unknown type.");
            //bean = new UserPasswordBean();
            bean = new UserPasswordBean(userPassword.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getUserPasswordServiceProxy().createUserPassword(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateUserPassword(String guid, String admin, String user, String username, String email, String openId, HashedPasswordStruct password, Boolean resetRequired, String challengeQuestion, String challengeAnswer, String status, Long lastResetTime, Long expirationTime) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        HashedPasswordStructBean passwordBean = null;
        if(password instanceof HashedPasswordStructBean) {
            passwordBean = (HashedPasswordStructBean) password;
        } else if(password instanceof HashedPasswordStruct) {
            passwordBean = new HashedPasswordStructBean(password.getUuid(), password.getPlainText(), password.getHashedText(), password.getSalt(), password.getAlgorithm());
        } else {
            passwordBean = null;   // ????
        }
        UserPasswordBean bean = new UserPasswordBean(guid, admin, user, username, email, openId, passwordBean, resetRequired, challengeQuestion, challengeAnswer, status, lastResetTime, expirationTime);
        return updateUserPassword(bean);
    }
        
    @Override
    public Boolean updateUserPassword(UserPassword userPassword) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //UserPassword bean = refreshUserPassword(userPassword);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param userPassword cannot be null.....
        if(userPassword == null || userPassword.getGuid() == null) {
            log.log(Level.INFO, "Param userPassword or its guid is null!");
            throw new BadRequestException("Param userPassword object or its guid is null!");
        }
        UserPasswordBean bean = null;
        if(userPassword instanceof UserPasswordBean) {
            bean = (UserPasswordBean) userPassword;
        } else {  // if(userPassword instanceof UserPassword)
            bean = new UserPasswordBean(userPassword.getGuid(), userPassword.getAdmin(), userPassword.getUser(), userPassword.getUsername(), userPassword.getEmail(), userPassword.getOpenId(), (HashedPasswordStructBean) userPassword.getPassword(), userPassword.isResetRequired(), userPassword.getChallengeQuestion(), userPassword.getChallengeAnswer(), userPassword.getStatus(), userPassword.getLastResetTime(), userPassword.getExpirationTime());
        }
        Boolean suc = getProxyFactory().getUserPasswordServiceProxy().updateUserPassword(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public UserPassword refreshUserPassword(UserPassword userPassword) throws BaseException
    {
        log.finer("BEGIN");

        // Param userPassword cannot be null.....
        if(userPassword == null || userPassword.getGuid() == null) {
            log.log(Level.INFO, "Param userPassword or its guid is null!");
            throw new BadRequestException("Param userPassword object or its guid is null!");
        }
        UserPasswordBean bean = null;
        if(userPassword instanceof UserPasswordBean) {
            bean = (UserPasswordBean) userPassword;
        } else {  // if(userPassword instanceof UserPassword)
            bean = new UserPasswordBean(userPassword.getGuid(), userPassword.getAdmin(), userPassword.getUser(), userPassword.getUsername(), userPassword.getEmail(), userPassword.getOpenId(), (HashedPasswordStructBean) userPassword.getPassword(), userPassword.isResetRequired(), userPassword.getChallengeQuestion(), userPassword.getChallengeAnswer(), userPassword.getStatus(), userPassword.getLastResetTime(), userPassword.getExpirationTime());
        }
        Boolean suc = getProxyFactory().getUserPasswordServiceProxy().updateUserPassword(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteUserPassword(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getUserPasswordServiceProxy().deleteUserPassword(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }
            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            UserPassword userPassword = null;
            try {
                userPassword = getProxyFactory().getUserPasswordServiceProxy().getUserPassword(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch userPassword with a key, " + guid);
                return false;
            }
            if(userPassword != null) {
                String beanGuid = userPassword.getGuid();
                Boolean suc1 = getProxyFactory().getUserPasswordServiceProxy().deleteUserPassword(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("userPassword with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteUserPassword(UserPassword userPassword) throws BaseException
    {
        log.finer("BEGIN");

        // Param userPassword cannot be null.....
        if(userPassword == null || userPassword.getGuid() == null) {
            log.log(Level.INFO, "Param userPassword or its guid is null!");
            throw new BadRequestException("Param userPassword object or its guid is null!");
        }
        UserPasswordBean bean = null;
        if(userPassword instanceof UserPasswordBean) {
            bean = (UserPasswordBean) userPassword;
        } else {  // if(userPassword instanceof UserPassword)
            // ????
            log.warning("userPassword is not an instance of UserPasswordBean.");
            bean = new UserPasswordBean(userPassword.getGuid(), userPassword.getAdmin(), userPassword.getUser(), userPassword.getUsername(), userPassword.getEmail(), userPassword.getOpenId(), (HashedPasswordStructBean) userPassword.getPassword(), userPassword.isResetRequired(), userPassword.getChallengeQuestion(), userPassword.getChallengeAnswer(), userPassword.getStatus(), userPassword.getLastResetTime(), userPassword.getExpirationTime());
        }
        Boolean suc = getProxyFactory().getUserPasswordServiceProxy().deleteUserPassword(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteUserPasswords(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getUserPasswordServiceProxy().deleteUserPasswords(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createUserPasswords(List<UserPassword> userPasswords) throws BaseException
    {
        log.finer("BEGIN");

        if(userPasswords == null) {
            log.log(Level.WARNING, "createUserPasswords() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = userPasswords.size();
        if(size == 0) {
            log.log(Level.WARNING, "createUserPasswords() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(UserPassword userPassword : userPasswords) {
            String guid = createUserPassword(userPassword);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createUserPasswords() failed for at least one userPassword. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateUserPasswords(List<UserPassword> userPasswords) throws BaseException
    //{
    //}

}
