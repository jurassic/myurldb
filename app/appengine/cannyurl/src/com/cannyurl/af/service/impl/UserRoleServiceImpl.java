package com.cannyurl.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.UserRole;
import com.cannyurl.af.config.Config;

import com.cannyurl.af.bean.HashedPasswordStructBean;
import com.cannyurl.af.bean.GeoPointStructBean;
import com.cannyurl.af.bean.WebProfileStructBean;
import com.cannyurl.af.bean.StreetAddressStructBean;
import com.cannyurl.af.bean.SiteLogBean;
import com.cannyurl.af.bean.HelpNoticeBean;
import com.cannyurl.af.bean.TwitterCardAppInfoBean;
import com.cannyurl.af.bean.ContactInfoStructBean;
import com.cannyurl.af.bean.GeoCoordinateStructBean;
import com.cannyurl.af.bean.FullNameStructBean;
import com.cannyurl.af.bean.ExternalUserIdStructBean;
import com.cannyurl.af.bean.GaeUserStructBean;
import com.cannyurl.af.bean.AppBrandStructBean;
import com.cannyurl.af.bean.CellLatitudeLongitudeBean;
import com.cannyurl.af.bean.GaeAppStructBean;
import com.cannyurl.af.bean.ShortPassageAttributeBean;
import com.cannyurl.af.bean.KeyValuePairStructBean;
import com.cannyurl.af.bean.KeyValueRelationStructBean;
import com.cannyurl.af.bean.ExternalServiceApiKeyStructBean;
import com.cannyurl.af.bean.ReferrerInfoStructBean;
import com.cannyurl.af.bean.TwitterCardProductDataBean;
import com.cannyurl.af.bean.PagerStateStructBean;

import com.cannyurl.af.bean.UserRoleBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.UserRoleService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class UserRoleServiceImpl implements UserRoleService
{
    private static final Logger log = Logger.getLogger(UserRoleServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "UserRole-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("UserRole:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public UserRoleServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // UserRole related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public UserRole getUserRole(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getUserRole(): guid = " + guid);

        UserRoleBean bean = null;
        if(getCache() != null) {
            bean = (UserRoleBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (UserRoleBean) getProxyFactory().getUserRoleServiceProxy().getUserRole(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "UserRoleBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve UserRoleBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getUserRole(String guid, String field) throws BaseException
    {
        UserRoleBean bean = null;
        if(getCache() != null) {
            bean = (UserRoleBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (UserRoleBean) getProxyFactory().getUserRoleServiceProxy().getUserRole(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "UserRoleBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve UserRoleBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return bean.getUser();
        } else if(field.equals("role")) {
            return bean.getRole();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<UserRole> getUserRoles(List<String> guids) throws BaseException
    {
        log.fine("getUserRoles()");

        // TBD: Is there a better way????
        List<UserRole> userRoles = getProxyFactory().getUserRoleServiceProxy().getUserRoles(guids);
        if(userRoles == null) {
            log.log(Level.WARNING, "Failed to retrieve UserRoleBean list.");
        }

        log.finer("END");
        return userRoles;
    }

    @Override
    public List<UserRole> getAllUserRoles() throws BaseException
    {
        return getAllUserRoles(null, null, null);
    }

    @Override
    public List<UserRole> getAllUserRoles(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllUserRoles(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<UserRole> userRoles = getProxyFactory().getUserRoleServiceProxy().getAllUserRoles(ordering, offset, count);
        if(userRoles == null) {
            log.log(Level.WARNING, "Failed to retrieve UserRoleBean list.");
        }

        log.finer("END");
        return userRoles;
    }

    @Override
    public List<String> getAllUserRoleKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllUserRoleKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getUserRoleServiceProxy().getAllUserRoleKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve UserRoleBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty UserRoleBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<UserRole> findUserRoles(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findUserRoles(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<UserRole> findUserRoles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("UserRoleServiceImpl.findUserRoles(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<UserRole> userRoles = getProxyFactory().getUserRoleServiceProxy().findUserRoles(filter, ordering, params, values, grouping, unique, offset, count);
        if(userRoles == null) {
            log.log(Level.WARNING, "Failed to find userRoles for the given criterion.");
        }

        log.finer("END");
        return userRoles;
    }

    @Override
    public List<String> findUserRoleKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("UserRoleServiceImpl.findUserRoleKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getUserRoleServiceProxy().findUserRoleKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find UserRole keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty UserRole key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("UserRoleServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getUserRoleServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createUserRole(String user, String role, String status) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        UserRoleBean bean = new UserRoleBean(null, user, role, status);
        return createUserRole(bean);
    }

    @Override
    public String createUserRole(UserRole userRole) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //UserRole bean = constructUserRole(userRole);
        //return bean.getGuid();

        // Param userRole cannot be null.....
        if(userRole == null) {
            log.log(Level.INFO, "Param userRole is null!");
            throw new BadRequestException("Param userRole object is null!");
        }
        UserRoleBean bean = null;
        if(userRole instanceof UserRoleBean) {
            bean = (UserRoleBean) userRole;
        } else if(userRole instanceof UserRole) {
            // bean = new UserRoleBean(null, userRole.getUser(), userRole.getRole(), userRole.getStatus());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new UserRoleBean(userRole.getGuid(), userRole.getUser(), userRole.getRole(), userRole.getStatus());
        } else {
            log.log(Level.WARNING, "createUserRole(): Arg userRole is of an unknown type.");
            //bean = new UserRoleBean();
            bean = new UserRoleBean(userRole.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getUserRoleServiceProxy().createUserRole(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public UserRole constructUserRole(UserRole userRole) throws BaseException
    {
        log.finer("BEGIN");

        // Param userRole cannot be null.....
        if(userRole == null) {
            log.log(Level.INFO, "Param userRole is null!");
            throw new BadRequestException("Param userRole object is null!");
        }
        UserRoleBean bean = null;
        if(userRole instanceof UserRoleBean) {
            bean = (UserRoleBean) userRole;
        } else if(userRole instanceof UserRole) {
            // bean = new UserRoleBean(null, userRole.getUser(), userRole.getRole(), userRole.getStatus());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new UserRoleBean(userRole.getGuid(), userRole.getUser(), userRole.getRole(), userRole.getStatus());
        } else {
            log.log(Level.WARNING, "createUserRole(): Arg userRole is of an unknown type.");
            //bean = new UserRoleBean();
            bean = new UserRoleBean(userRole.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getUserRoleServiceProxy().createUserRole(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateUserRole(String guid, String user, String role, String status) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        UserRoleBean bean = new UserRoleBean(guid, user, role, status);
        return updateUserRole(bean);
    }
        
    @Override
    public Boolean updateUserRole(UserRole userRole) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //UserRole bean = refreshUserRole(userRole);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param userRole cannot be null.....
        if(userRole == null || userRole.getGuid() == null) {
            log.log(Level.INFO, "Param userRole or its guid is null!");
            throw new BadRequestException("Param userRole object or its guid is null!");
        }
        UserRoleBean bean = null;
        if(userRole instanceof UserRoleBean) {
            bean = (UserRoleBean) userRole;
        } else {  // if(userRole instanceof UserRole)
            bean = new UserRoleBean(userRole.getGuid(), userRole.getUser(), userRole.getRole(), userRole.getStatus());
        }
        Boolean suc = getProxyFactory().getUserRoleServiceProxy().updateUserRole(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public UserRole refreshUserRole(UserRole userRole) throws BaseException
    {
        log.finer("BEGIN");

        // Param userRole cannot be null.....
        if(userRole == null || userRole.getGuid() == null) {
            log.log(Level.INFO, "Param userRole or its guid is null!");
            throw new BadRequestException("Param userRole object or its guid is null!");
        }
        UserRoleBean bean = null;
        if(userRole instanceof UserRoleBean) {
            bean = (UserRoleBean) userRole;
        } else {  // if(userRole instanceof UserRole)
            bean = new UserRoleBean(userRole.getGuid(), userRole.getUser(), userRole.getRole(), userRole.getStatus());
        }
        Boolean suc = getProxyFactory().getUserRoleServiceProxy().updateUserRole(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteUserRole(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getUserRoleServiceProxy().deleteUserRole(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }
            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            UserRole userRole = null;
            try {
                userRole = getProxyFactory().getUserRoleServiceProxy().getUserRole(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch userRole with a key, " + guid);
                return false;
            }
            if(userRole != null) {
                String beanGuid = userRole.getGuid();
                Boolean suc1 = getProxyFactory().getUserRoleServiceProxy().deleteUserRole(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("userRole with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteUserRole(UserRole userRole) throws BaseException
    {
        log.finer("BEGIN");

        // Param userRole cannot be null.....
        if(userRole == null || userRole.getGuid() == null) {
            log.log(Level.INFO, "Param userRole or its guid is null!");
            throw new BadRequestException("Param userRole object or its guid is null!");
        }
        UserRoleBean bean = null;
        if(userRole instanceof UserRoleBean) {
            bean = (UserRoleBean) userRole;
        } else {  // if(userRole instanceof UserRole)
            // ????
            log.warning("userRole is not an instance of UserRoleBean.");
            bean = new UserRoleBean(userRole.getGuid(), userRole.getUser(), userRole.getRole(), userRole.getStatus());
        }
        Boolean suc = getProxyFactory().getUserRoleServiceProxy().deleteUserRole(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteUserRoles(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getUserRoleServiceProxy().deleteUserRoles(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createUserRoles(List<UserRole> userRoles) throws BaseException
    {
        log.finer("BEGIN");

        if(userRoles == null) {
            log.log(Level.WARNING, "createUserRoles() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = userRoles.size();
        if(size == 0) {
            log.log(Level.WARNING, "createUserRoles() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(UserRole userRole : userRoles) {
            String guid = createUserRole(userRole);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createUserRoles() failed for at least one userRole. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateUserRoles(List<UserRole> userRoles) throws BaseException
    //{
    //}

}
