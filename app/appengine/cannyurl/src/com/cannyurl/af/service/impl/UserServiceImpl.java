package com.cannyurl.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.GeoPointStruct;
import com.myurldb.ws.StreetAddressStruct;
import com.myurldb.ws.GaeAppStruct;
import com.myurldb.ws.FullNameStruct;
import com.myurldb.ws.GaeUserStruct;
import com.myurldb.ws.User;
import com.myurldb.ws.search.gae.UserIndexBuilder;
import com.cannyurl.af.config.Config;

import com.cannyurl.af.bean.HashedPasswordStructBean;
import com.cannyurl.af.bean.GeoPointStructBean;
import com.cannyurl.af.bean.WebProfileStructBean;
import com.cannyurl.af.bean.StreetAddressStructBean;
import com.cannyurl.af.bean.SiteLogBean;
import com.cannyurl.af.bean.HelpNoticeBean;
import com.cannyurl.af.bean.TwitterCardAppInfoBean;
import com.cannyurl.af.bean.ContactInfoStructBean;
import com.cannyurl.af.bean.GeoCoordinateStructBean;
import com.cannyurl.af.bean.FullNameStructBean;
import com.cannyurl.af.bean.ExternalUserIdStructBean;
import com.cannyurl.af.bean.GaeUserStructBean;
import com.cannyurl.af.bean.AppBrandStructBean;
import com.cannyurl.af.bean.CellLatitudeLongitudeBean;
import com.cannyurl.af.bean.GaeAppStructBean;
import com.cannyurl.af.bean.ShortPassageAttributeBean;
import com.cannyurl.af.bean.KeyValuePairStructBean;
import com.cannyurl.af.bean.KeyValueRelationStructBean;
import com.cannyurl.af.bean.ExternalServiceApiKeyStructBean;
import com.cannyurl.af.bean.ReferrerInfoStructBean;
import com.cannyurl.af.bean.TwitterCardProductDataBean;
import com.cannyurl.af.bean.PagerStateStructBean;

import com.cannyurl.af.bean.UserBean;
import com.cannyurl.af.bean.GeoPointStructBean;
import com.cannyurl.af.bean.StreetAddressStructBean;
import com.cannyurl.af.bean.FullNameStructBean;
import com.cannyurl.af.bean.GaeAppStructBean;
import com.cannyurl.af.bean.GaeUserStructBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.UserService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class UserServiceImpl implements UserService
{
    private static final Logger log = Logger.getLogger(UserServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "User-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("User:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public UserServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // User related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public User getUser(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getUser(): guid = " + guid);

        UserBean bean = null;
        if(getCache() != null) {
            bean = (UserBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (UserBean) getProxyFactory().getUserServiceProxy().getUser(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "UserBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve UserBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getUser(String guid, String field) throws BaseException
    {
        UserBean bean = null;
        if(getCache() != null) {
            bean = (UserBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (UserBean) getProxyFactory().getUserServiceProxy().getUser(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "UserBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve UserBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("managerApp")) {
            return bean.getManagerApp();
        } else if(field.equals("appAcl")) {
            return bean.getAppAcl();
        } else if(field.equals("gaeApp")) {
            return bean.getGaeApp();
        } else if(field.equals("aeryId")) {
            return bean.getAeryId();
        } else if(field.equals("sessionId")) {
            return bean.getSessionId();
        } else if(field.equals("ancestorGuid")) {
            return bean.getAncestorGuid();
        } else if(field.equals("name")) {
            return bean.getName();
        } else if(field.equals("usercode")) {
            return bean.getUsercode();
        } else if(field.equals("username")) {
            return bean.getUsername();
        } else if(field.equals("nickname")) {
            return bean.getNickname();
        } else if(field.equals("avatar")) {
            return bean.getAvatar();
        } else if(field.equals("email")) {
            return bean.getEmail();
        } else if(field.equals("openId")) {
            return bean.getOpenId();
        } else if(field.equals("gaeUser")) {
            return bean.getGaeUser();
        } else if(field.equals("entityType")) {
            return bean.getEntityType();
        } else if(field.equals("surrogate")) {
            return bean.isSurrogate();
        } else if(field.equals("obsolete")) {
            return bean.isObsolete();
        } else if(field.equals("timeZone")) {
            return bean.getTimeZone();
        } else if(field.equals("location")) {
            return bean.getLocation();
        } else if(field.equals("streetAddress")) {
            return bean.getStreetAddress();
        } else if(field.equals("geoPoint")) {
            return bean.getGeoPoint();
        } else if(field.equals("ipAddress")) {
            return bean.getIpAddress();
        } else if(field.equals("referer")) {
            return bean.getReferer();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("emailVerifiedTime")) {
            return bean.getEmailVerifiedTime();
        } else if(field.equals("openIdVerifiedTime")) {
            return bean.getOpenIdVerifiedTime();
        } else if(field.equals("authenticatedTime")) {
            return bean.getAuthenticatedTime();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<User> getUsers(List<String> guids) throws BaseException
    {
        log.fine("getUsers()");

        // TBD: Is there a better way????
        List<User> users = getProxyFactory().getUserServiceProxy().getUsers(guids);
        if(users == null) {
            log.log(Level.WARNING, "Failed to retrieve UserBean list.");
        }

        log.finer("END");
        return users;
    }

    @Override
    public List<User> getAllUsers() throws BaseException
    {
        return getAllUsers(null, null, null);
    }

    @Override
    public List<User> getAllUsers(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllUsers(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<User> users = getProxyFactory().getUserServiceProxy().getAllUsers(ordering, offset, count);
        if(users == null) {
            log.log(Level.WARNING, "Failed to retrieve UserBean list.");
        }

        log.finer("END");
        return users;
    }

    @Override
    public List<String> getAllUserKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllUserKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getUserServiceProxy().getAllUserKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve UserBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty UserBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<User> findUsers(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findUsers(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<User> findUsers(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("UserServiceImpl.findUsers(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<User> users = getProxyFactory().getUserServiceProxy().findUsers(filter, ordering, params, values, grouping, unique, offset, count);
        if(users == null) {
            log.log(Level.WARNING, "Failed to find users for the given criterion.");
        }

        log.finer("END");
        return users;
    }

    @Override
    public List<String> findUserKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("UserServiceImpl.findUserKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getUserServiceProxy().findUserKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find User keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty User key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("UserServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getUserServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createUser(String managerApp, Long appAcl, GaeAppStruct gaeApp, String aeryId, String sessionId, String ancestorGuid, FullNameStruct name, String usercode, String username, String nickname, String avatar, String email, String openId, GaeUserStruct gaeUser, String entityType, Boolean surrogate, Boolean obsolete, String timeZone, String location, StreetAddressStruct streetAddress, GeoPointStruct geoPoint, String ipAddress, String referer, String status, Long emailVerifiedTime, Long openIdVerifiedTime, Long authenticatedTime) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        GaeAppStructBean gaeAppBean = null;
        if(gaeApp instanceof GaeAppStructBean) {
            gaeAppBean = (GaeAppStructBean) gaeApp;
        } else if(gaeApp instanceof GaeAppStruct) {
            gaeAppBean = new GaeAppStructBean(gaeApp.getGroupId(), gaeApp.getAppId(), gaeApp.getAppDomain(), gaeApp.getNamespace(), gaeApp.getAcl(), gaeApp.getNote());
        } else {
            gaeAppBean = null;   // ????
        }
        FullNameStructBean nameBean = null;
        if(name instanceof FullNameStructBean) {
            nameBean = (FullNameStructBean) name;
        } else if(name instanceof FullNameStruct) {
            nameBean = new FullNameStructBean(name.getUuid(), name.getDisplayName(), name.getLastName(), name.getFirstName(), name.getMiddleName1(), name.getMiddleName2(), name.getMiddleInitial(), name.getSalutation(), name.getSuffix(), name.getNote());
        } else {
            nameBean = null;   // ????
        }
        GaeUserStructBean gaeUserBean = null;
        if(gaeUser instanceof GaeUserStructBean) {
            gaeUserBean = (GaeUserStructBean) gaeUser;
        } else if(gaeUser instanceof GaeUserStruct) {
            gaeUserBean = new GaeUserStructBean(gaeUser.getAuthDomain(), gaeUser.getFederatedIdentity(), gaeUser.getNickname(), gaeUser.getUserId(), gaeUser.getEmail(), gaeUser.getNote());
        } else {
            gaeUserBean = null;   // ????
        }
        StreetAddressStructBean streetAddressBean = null;
        if(streetAddress instanceof StreetAddressStructBean) {
            streetAddressBean = (StreetAddressStructBean) streetAddress;
        } else if(streetAddress instanceof StreetAddressStruct) {
            streetAddressBean = new StreetAddressStructBean(streetAddress.getUuid(), streetAddress.getStreet1(), streetAddress.getStreet2(), streetAddress.getCity(), streetAddress.getCounty(), streetAddress.getPostalCode(), streetAddress.getState(), streetAddress.getProvince(), streetAddress.getCountry(), streetAddress.getCountryName(), streetAddress.getNote());
        } else {
            streetAddressBean = null;   // ????
        }
        GeoPointStructBean geoPointBean = null;
        if(geoPoint instanceof GeoPointStructBean) {
            geoPointBean = (GeoPointStructBean) geoPoint;
        } else if(geoPoint instanceof GeoPointStruct) {
            geoPointBean = new GeoPointStructBean(geoPoint.getUuid(), geoPoint.getLatitude(), geoPoint.getLongitude(), geoPoint.getAltitude(), geoPoint.isSensorUsed());
        } else {
            geoPointBean = null;   // ????
        }
        UserBean bean = new UserBean(null, managerApp, appAcl, gaeAppBean, aeryId, sessionId, ancestorGuid, nameBean, usercode, username, nickname, avatar, email, openId, gaeUserBean, entityType, surrogate, obsolete, timeZone, location, streetAddressBean, geoPointBean, ipAddress, referer, status, emailVerifiedTime, openIdVerifiedTime, authenticatedTime);
        return createUser(bean);
    }

    @Override
    public String createUser(User user) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //User bean = constructUser(user);
        //return bean.getGuid();

        // Param user cannot be null.....
        if(user == null) {
            log.log(Level.INFO, "Param user is null!");
            throw new BadRequestException("Param user object is null!");
        }
        UserBean bean = null;
        if(user instanceof UserBean) {
            bean = (UserBean) user;
        } else if(user instanceof User) {
            // bean = new UserBean(null, user.getManagerApp(), user.getAppAcl(), (GaeAppStructBean) user.getGaeApp(), user.getAeryId(), user.getSessionId(), user.getAncestorGuid(), (FullNameStructBean) user.getName(), user.getUsercode(), user.getUsername(), user.getNickname(), user.getAvatar(), user.getEmail(), user.getOpenId(), (GaeUserStructBean) user.getGaeUser(), user.getEntityType(), user.isSurrogate(), user.isObsolete(), user.getTimeZone(), user.getLocation(), (StreetAddressStructBean) user.getStreetAddress(), (GeoPointStructBean) user.getGeoPoint(), user.getIpAddress(), user.getReferer(), user.getStatus(), user.getEmailVerifiedTime(), user.getOpenIdVerifiedTime(), user.getAuthenticatedTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new UserBean(user.getGuid(), user.getManagerApp(), user.getAppAcl(), (GaeAppStructBean) user.getGaeApp(), user.getAeryId(), user.getSessionId(), user.getAncestorGuid(), (FullNameStructBean) user.getName(), user.getUsercode(), user.getUsername(), user.getNickname(), user.getAvatar(), user.getEmail(), user.getOpenId(), (GaeUserStructBean) user.getGaeUser(), user.getEntityType(), user.isSurrogate(), user.isObsolete(), user.getTimeZone(), user.getLocation(), (StreetAddressStructBean) user.getStreetAddress(), (GeoPointStructBean) user.getGeoPoint(), user.getIpAddress(), user.getReferer(), user.getStatus(), user.getEmailVerifiedTime(), user.getOpenIdVerifiedTime(), user.getAuthenticatedTime());
        } else {
            log.log(Level.WARNING, "createUser(): Arg user is of an unknown type.");
            //bean = new UserBean();
            bean = new UserBean(user.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getUserServiceProxy().createUser(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

	    // TBD:
	    if(Config.getInstance().isSearchEnabled()) {
            if(guid != null && !guid.isEmpty()) {
                log.fine("Calling addDocument()... for User.");
                UserIndexBuilder builder = new UserIndexBuilder();
	    	    builder.addDocument(bean);
	    	}
	    }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public User constructUser(User user) throws BaseException
    {
        log.finer("BEGIN");

        // Param user cannot be null.....
        if(user == null) {
            log.log(Level.INFO, "Param user is null!");
            throw new BadRequestException("Param user object is null!");
        }
        UserBean bean = null;
        if(user instanceof UserBean) {
            bean = (UserBean) user;
        } else if(user instanceof User) {
            // bean = new UserBean(null, user.getManagerApp(), user.getAppAcl(), (GaeAppStructBean) user.getGaeApp(), user.getAeryId(), user.getSessionId(), user.getAncestorGuid(), (FullNameStructBean) user.getName(), user.getUsercode(), user.getUsername(), user.getNickname(), user.getAvatar(), user.getEmail(), user.getOpenId(), (GaeUserStructBean) user.getGaeUser(), user.getEntityType(), user.isSurrogate(), user.isObsolete(), user.getTimeZone(), user.getLocation(), (StreetAddressStructBean) user.getStreetAddress(), (GeoPointStructBean) user.getGeoPoint(), user.getIpAddress(), user.getReferer(), user.getStatus(), user.getEmailVerifiedTime(), user.getOpenIdVerifiedTime(), user.getAuthenticatedTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new UserBean(user.getGuid(), user.getManagerApp(), user.getAppAcl(), (GaeAppStructBean) user.getGaeApp(), user.getAeryId(), user.getSessionId(), user.getAncestorGuid(), (FullNameStructBean) user.getName(), user.getUsercode(), user.getUsername(), user.getNickname(), user.getAvatar(), user.getEmail(), user.getOpenId(), (GaeUserStructBean) user.getGaeUser(), user.getEntityType(), user.isSurrogate(), user.isObsolete(), user.getTimeZone(), user.getLocation(), (StreetAddressStructBean) user.getStreetAddress(), (GeoPointStructBean) user.getGeoPoint(), user.getIpAddress(), user.getReferer(), user.getStatus(), user.getEmailVerifiedTime(), user.getOpenIdVerifiedTime(), user.getAuthenticatedTime());
        } else {
            log.log(Level.WARNING, "createUser(): Arg user is of an unknown type.");
            //bean = new UserBean();
            bean = new UserBean(user.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getUserServiceProxy().createUser(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

	    // TBD:
	    if(Config.getInstance().isSearchEnabled()) {
            if(guid != null && !guid.isEmpty()) {
                log.fine("Calling addDocument()... for User.");
                UserIndexBuilder builder = new UserIndexBuilder();
	    	    builder.addDocument(bean);
	    	}
	    }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateUser(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String aeryId, String sessionId, String ancestorGuid, FullNameStruct name, String usercode, String username, String nickname, String avatar, String email, String openId, GaeUserStruct gaeUser, String entityType, Boolean surrogate, Boolean obsolete, String timeZone, String location, StreetAddressStruct streetAddress, GeoPointStruct geoPoint, String ipAddress, String referer, String status, Long emailVerifiedTime, Long openIdVerifiedTime, Long authenticatedTime) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        GaeAppStructBean gaeAppBean = null;
        if(gaeApp instanceof GaeAppStructBean) {
            gaeAppBean = (GaeAppStructBean) gaeApp;
        } else if(gaeApp instanceof GaeAppStruct) {
            gaeAppBean = new GaeAppStructBean(gaeApp.getGroupId(), gaeApp.getAppId(), gaeApp.getAppDomain(), gaeApp.getNamespace(), gaeApp.getAcl(), gaeApp.getNote());
        } else {
            gaeAppBean = null;   // ????
        }
        FullNameStructBean nameBean = null;
        if(name instanceof FullNameStructBean) {
            nameBean = (FullNameStructBean) name;
        } else if(name instanceof FullNameStruct) {
            nameBean = new FullNameStructBean(name.getUuid(), name.getDisplayName(), name.getLastName(), name.getFirstName(), name.getMiddleName1(), name.getMiddleName2(), name.getMiddleInitial(), name.getSalutation(), name.getSuffix(), name.getNote());
        } else {
            nameBean = null;   // ????
        }
        GaeUserStructBean gaeUserBean = null;
        if(gaeUser instanceof GaeUserStructBean) {
            gaeUserBean = (GaeUserStructBean) gaeUser;
        } else if(gaeUser instanceof GaeUserStruct) {
            gaeUserBean = new GaeUserStructBean(gaeUser.getAuthDomain(), gaeUser.getFederatedIdentity(), gaeUser.getNickname(), gaeUser.getUserId(), gaeUser.getEmail(), gaeUser.getNote());
        } else {
            gaeUserBean = null;   // ????
        }
        StreetAddressStructBean streetAddressBean = null;
        if(streetAddress instanceof StreetAddressStructBean) {
            streetAddressBean = (StreetAddressStructBean) streetAddress;
        } else if(streetAddress instanceof StreetAddressStruct) {
            streetAddressBean = new StreetAddressStructBean(streetAddress.getUuid(), streetAddress.getStreet1(), streetAddress.getStreet2(), streetAddress.getCity(), streetAddress.getCounty(), streetAddress.getPostalCode(), streetAddress.getState(), streetAddress.getProvince(), streetAddress.getCountry(), streetAddress.getCountryName(), streetAddress.getNote());
        } else {
            streetAddressBean = null;   // ????
        }
        GeoPointStructBean geoPointBean = null;
        if(geoPoint instanceof GeoPointStructBean) {
            geoPointBean = (GeoPointStructBean) geoPoint;
        } else if(geoPoint instanceof GeoPointStruct) {
            geoPointBean = new GeoPointStructBean(geoPoint.getUuid(), geoPoint.getLatitude(), geoPoint.getLongitude(), geoPoint.getAltitude(), geoPoint.isSensorUsed());
        } else {
            geoPointBean = null;   // ????
        }
        UserBean bean = new UserBean(guid, managerApp, appAcl, gaeAppBean, aeryId, sessionId, ancestorGuid, nameBean, usercode, username, nickname, avatar, email, openId, gaeUserBean, entityType, surrogate, obsolete, timeZone, location, streetAddressBean, geoPointBean, ipAddress, referer, status, emailVerifiedTime, openIdVerifiedTime, authenticatedTime);
        return updateUser(bean);
    }
        
    @Override
    public Boolean updateUser(User user) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //User bean = refreshUser(user);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param user cannot be null.....
        if(user == null || user.getGuid() == null) {
            log.log(Level.INFO, "Param user or its guid is null!");
            throw new BadRequestException("Param user object or its guid is null!");
        }
        UserBean bean = null;
        if(user instanceof UserBean) {
            bean = (UserBean) user;
        } else {  // if(user instanceof User)
            bean = new UserBean(user.getGuid(), user.getManagerApp(), user.getAppAcl(), (GaeAppStructBean) user.getGaeApp(), user.getAeryId(), user.getSessionId(), user.getAncestorGuid(), (FullNameStructBean) user.getName(), user.getUsercode(), user.getUsername(), user.getNickname(), user.getAvatar(), user.getEmail(), user.getOpenId(), (GaeUserStructBean) user.getGaeUser(), user.getEntityType(), user.isSurrogate(), user.isObsolete(), user.getTimeZone(), user.getLocation(), (StreetAddressStructBean) user.getStreetAddress(), (GeoPointStructBean) user.getGeoPoint(), user.getIpAddress(), user.getReferer(), user.getStatus(), user.getEmailVerifiedTime(), user.getOpenIdVerifiedTime(), user.getAuthenticatedTime());
        }
        Boolean suc = getProxyFactory().getUserServiceProxy().updateUser(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

	    // TBD:
        if(Config.getInstance().isSearchEnabled()) {
            if(suc != null && suc.equals(Boolean.TRUE)) {
                log.fine("Calling addDocument()... for User.");
	    	    UserIndexBuilder builder = new UserIndexBuilder();
	    	    builder.addDocument(bean);
	        }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public User refreshUser(User user) throws BaseException
    {
        log.finer("BEGIN");

        // Param user cannot be null.....
        if(user == null || user.getGuid() == null) {
            log.log(Level.INFO, "Param user or its guid is null!");
            throw new BadRequestException("Param user object or its guid is null!");
        }
        UserBean bean = null;
        if(user instanceof UserBean) {
            bean = (UserBean) user;
        } else {  // if(user instanceof User)
            bean = new UserBean(user.getGuid(), user.getManagerApp(), user.getAppAcl(), (GaeAppStructBean) user.getGaeApp(), user.getAeryId(), user.getSessionId(), user.getAncestorGuid(), (FullNameStructBean) user.getName(), user.getUsercode(), user.getUsername(), user.getNickname(), user.getAvatar(), user.getEmail(), user.getOpenId(), (GaeUserStructBean) user.getGaeUser(), user.getEntityType(), user.isSurrogate(), user.isObsolete(), user.getTimeZone(), user.getLocation(), (StreetAddressStructBean) user.getStreetAddress(), (GeoPointStructBean) user.getGeoPoint(), user.getIpAddress(), user.getReferer(), user.getStatus(), user.getEmailVerifiedTime(), user.getOpenIdVerifiedTime(), user.getAuthenticatedTime());
        }
        Boolean suc = getProxyFactory().getUserServiceProxy().updateUser(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

	    // TBD:
        if(Config.getInstance().isSearchEnabled()) {
            if(suc != null && suc.equals(Boolean.TRUE)) {
                log.fine("Calling addDocument()... for User.");
	    	    UserIndexBuilder builder = new UserIndexBuilder();
	    	    builder.addDocument(bean);
	        }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteUser(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getUserServiceProxy().deleteUser(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }

        // TBD:
        if(Config.getInstance().isSearchEnabled()) {
            if(suc != null && suc.equals(Boolean.TRUE)) {
                log.fine("Calling removeDocument()... for User.");
	    	    UserIndexBuilder builder = new UserIndexBuilder();
                builder.removeDocument(guid);
	        }
        }

            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            User user = null;
            try {
                user = getProxyFactory().getUserServiceProxy().getUser(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch user with a key, " + guid);
                return false;
            }
            if(user != null) {
                String beanGuid = user.getGuid();
                Boolean suc1 = getProxyFactory().getUserServiceProxy().deleteUser(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("user with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteUser(User user) throws BaseException
    {
        log.finer("BEGIN");

        // Param user cannot be null.....
        if(user == null || user.getGuid() == null) {
            log.log(Level.INFO, "Param user or its guid is null!");
            throw new BadRequestException("Param user object or its guid is null!");
        }
        UserBean bean = null;
        if(user instanceof UserBean) {
            bean = (UserBean) user;
        } else {  // if(user instanceof User)
            // ????
            log.warning("user is not an instance of UserBean.");
            bean = new UserBean(user.getGuid(), user.getManagerApp(), user.getAppAcl(), (GaeAppStructBean) user.getGaeApp(), user.getAeryId(), user.getSessionId(), user.getAncestorGuid(), (FullNameStructBean) user.getName(), user.getUsercode(), user.getUsername(), user.getNickname(), user.getAvatar(), user.getEmail(), user.getOpenId(), (GaeUserStructBean) user.getGaeUser(), user.getEntityType(), user.isSurrogate(), user.isObsolete(), user.getTimeZone(), user.getLocation(), (StreetAddressStructBean) user.getStreetAddress(), (GeoPointStructBean) user.getGeoPoint(), user.getIpAddress(), user.getReferer(), user.getStatus(), user.getEmailVerifiedTime(), user.getOpenIdVerifiedTime(), user.getAuthenticatedTime());
        }
        Boolean suc = getProxyFactory().getUserServiceProxy().deleteUser(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

	    // TBD:
        if(Config.getInstance().isSearchEnabled()) {
            if(suc != null && suc.equals(Boolean.TRUE)) {
                log.fine("Calling removeDocument()... for User.");
	    	    UserIndexBuilder builder = new UserIndexBuilder();
                builder.removeDocument(bean.getGuid());
	        }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteUsers(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getUserServiceProxy().deleteUsers(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

	    // TBD:
        //if(Config.getInstance().isSearchEnabled()) {
        //    if(suc != null && suc.equals(Boolean.TRUE)) {
        //      log.fine("Calling removeDocument()... for User.");
	    //	    UserIndexBuilder builder = new UserIndexBuilder();
        //      builder.removeDocument(guids);
	    //    }
        //}

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createUsers(List<User> users) throws BaseException
    {
        log.finer("BEGIN");

        if(users == null) {
            log.log(Level.WARNING, "createUsers() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = users.size();
        if(size == 0) {
            log.log(Level.WARNING, "createUsers() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(User user : users) {
            String guid = createUser(user);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createUsers() failed for at least one user. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateUsers(List<User> users) throws BaseException
    //{
    //}

}
