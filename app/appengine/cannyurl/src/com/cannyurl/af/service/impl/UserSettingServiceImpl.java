package com.cannyurl.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.UserSetting;
import com.cannyurl.af.config.Config;

import com.cannyurl.af.bean.HashedPasswordStructBean;
import com.cannyurl.af.bean.GeoPointStructBean;
import com.cannyurl.af.bean.WebProfileStructBean;
import com.cannyurl.af.bean.StreetAddressStructBean;
import com.cannyurl.af.bean.SiteLogBean;
import com.cannyurl.af.bean.HelpNoticeBean;
import com.cannyurl.af.bean.TwitterCardAppInfoBean;
import com.cannyurl.af.bean.ContactInfoStructBean;
import com.cannyurl.af.bean.GeoCoordinateStructBean;
import com.cannyurl.af.bean.FullNameStructBean;
import com.cannyurl.af.bean.ExternalUserIdStructBean;
import com.cannyurl.af.bean.GaeUserStructBean;
import com.cannyurl.af.bean.AppBrandStructBean;
import com.cannyurl.af.bean.CellLatitudeLongitudeBean;
import com.cannyurl.af.bean.GaeAppStructBean;
import com.cannyurl.af.bean.ShortPassageAttributeBean;
import com.cannyurl.af.bean.KeyValuePairStructBean;
import com.cannyurl.af.bean.KeyValueRelationStructBean;
import com.cannyurl.af.bean.ExternalServiceApiKeyStructBean;
import com.cannyurl.af.bean.ReferrerInfoStructBean;
import com.cannyurl.af.bean.TwitterCardProductDataBean;
import com.cannyurl.af.bean.PagerStateStructBean;

import com.cannyurl.af.bean.UserSettingBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.UserSettingService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class UserSettingServiceImpl implements UserSettingService
{
    private static final Logger log = Logger.getLogger(UserSettingServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "UserSetting-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("UserSetting:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public UserSettingServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // UserSetting related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public UserSetting getUserSetting(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getUserSetting(): guid = " + guid);

        UserSettingBean bean = null;
        if(getCache() != null) {
            bean = (UserSettingBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (UserSettingBean) getProxyFactory().getUserSettingServiceProxy().getUserSetting(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "UserSettingBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve UserSettingBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getUserSetting(String guid, String field) throws BaseException
    {
        UserSettingBean bean = null;
        if(getCache() != null) {
            bean = (UserSettingBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (UserSettingBean) getProxyFactory().getUserSettingServiceProxy().getUserSetting(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "UserSettingBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve UserSettingBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return bean.getUser();
        } else if(field.equals("homePage")) {
            return bean.getHomePage();
        } else if(field.equals("selfBio")) {
            return bean.getSelfBio();
        } else if(field.equals("userUrlDomain")) {
            return bean.getUserUrlDomain();
        } else if(field.equals("userUrlDomainNormalized")) {
            return bean.getUserUrlDomainNormalized();
        } else if(field.equals("autoRedirectEnabled")) {
            return bean.isAutoRedirectEnabled();
        } else if(field.equals("viewEnabled")) {
            return bean.isViewEnabled();
        } else if(field.equals("shareEnabled")) {
            return bean.isShareEnabled();
        } else if(field.equals("defaultDomain")) {
            return bean.getDefaultDomain();
        } else if(field.equals("defaultTokenType")) {
            return bean.getDefaultTokenType();
        } else if(field.equals("defaultRedirectType")) {
            return bean.getDefaultRedirectType();
        } else if(field.equals("defaultFlashDuration")) {
            return bean.getDefaultFlashDuration();
        } else if(field.equals("defaultAccessType")) {
            return bean.getDefaultAccessType();
        } else if(field.equals("defaultViewType")) {
            return bean.getDefaultViewType();
        } else if(field.equals("defaultShareType")) {
            return bean.getDefaultShareType();
        } else if(field.equals("defaultPassphrase")) {
            return bean.getDefaultPassphrase();
        } else if(field.equals("defaultSignature")) {
            return bean.getDefaultSignature();
        } else if(field.equals("useSignature")) {
            return bean.isUseSignature();
        } else if(field.equals("defaultEmblem")) {
            return bean.getDefaultEmblem();
        } else if(field.equals("useEmblem")) {
            return bean.isUseEmblem();
        } else if(field.equals("defaultBackground")) {
            return bean.getDefaultBackground();
        } else if(field.equals("useBackground")) {
            return bean.isUseBackground();
        } else if(field.equals("sponsorUrl")) {
            return bean.getSponsorUrl();
        } else if(field.equals("sponsorBanner")) {
            return bean.getSponsorBanner();
        } else if(field.equals("sponsorNote")) {
            return bean.getSponsorNote();
        } else if(field.equals("useSponsor")) {
            return bean.isUseSponsor();
        } else if(field.equals("extraParams")) {
            return bean.getExtraParams();
        } else if(field.equals("expirationDuration")) {
            return bean.getExpirationDuration();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<UserSetting> getUserSettings(List<String> guids) throws BaseException
    {
        log.fine("getUserSettings()");

        // TBD: Is there a better way????
        List<UserSetting> userSettings = getProxyFactory().getUserSettingServiceProxy().getUserSettings(guids);
        if(userSettings == null) {
            log.log(Level.WARNING, "Failed to retrieve UserSettingBean list.");
        }

        log.finer("END");
        return userSettings;
    }

    @Override
    public List<UserSetting> getAllUserSettings() throws BaseException
    {
        return getAllUserSettings(null, null, null);
    }

    @Override
    public List<UserSetting> getAllUserSettings(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllUserSettings(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<UserSetting> userSettings = getProxyFactory().getUserSettingServiceProxy().getAllUserSettings(ordering, offset, count);
        if(userSettings == null) {
            log.log(Level.WARNING, "Failed to retrieve UserSettingBean list.");
        }

        log.finer("END");
        return userSettings;
    }

    @Override
    public List<String> getAllUserSettingKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllUserSettingKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getUserSettingServiceProxy().getAllUserSettingKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve UserSettingBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty UserSettingBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<UserSetting> findUserSettings(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findUserSettings(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<UserSetting> findUserSettings(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("UserSettingServiceImpl.findUserSettings(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<UserSetting> userSettings = getProxyFactory().getUserSettingServiceProxy().findUserSettings(filter, ordering, params, values, grouping, unique, offset, count);
        if(userSettings == null) {
            log.log(Level.WARNING, "Failed to find userSettings for the given criterion.");
        }

        log.finer("END");
        return userSettings;
    }

    @Override
    public List<String> findUserSettingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("UserSettingServiceImpl.findUserSettingKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getUserSettingServiceProxy().findUserSettingKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find UserSetting keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty UserSetting key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("UserSettingServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getUserSettingServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createUserSetting(String user, String homePage, String selfBio, String userUrlDomain, String userUrlDomainNormalized, Boolean autoRedirectEnabled, Boolean viewEnabled, Boolean shareEnabled, String defaultDomain, String defaultTokenType, String defaultRedirectType, Long defaultFlashDuration, String defaultAccessType, String defaultViewType, String defaultShareType, String defaultPassphrase, String defaultSignature, Boolean useSignature, String defaultEmblem, Boolean useEmblem, String defaultBackground, Boolean useBackground, String sponsorUrl, String sponsorBanner, String sponsorNote, Boolean useSponsor, String extraParams, Long expirationDuration) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        UserSettingBean bean = new UserSettingBean(null, user, homePage, selfBio, userUrlDomain, userUrlDomainNormalized, autoRedirectEnabled, viewEnabled, shareEnabled, defaultDomain, defaultTokenType, defaultRedirectType, defaultFlashDuration, defaultAccessType, defaultViewType, defaultShareType, defaultPassphrase, defaultSignature, useSignature, defaultEmblem, useEmblem, defaultBackground, useBackground, sponsorUrl, sponsorBanner, sponsorNote, useSponsor, extraParams, expirationDuration);
        return createUserSetting(bean);
    }

    @Override
    public String createUserSetting(UserSetting userSetting) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //UserSetting bean = constructUserSetting(userSetting);
        //return bean.getGuid();

        // Param userSetting cannot be null.....
        if(userSetting == null) {
            log.log(Level.INFO, "Param userSetting is null!");
            throw new BadRequestException("Param userSetting object is null!");
        }
        UserSettingBean bean = null;
        if(userSetting instanceof UserSettingBean) {
            bean = (UserSettingBean) userSetting;
        } else if(userSetting instanceof UserSetting) {
            // bean = new UserSettingBean(null, userSetting.getUser(), userSetting.getHomePage(), userSetting.getSelfBio(), userSetting.getUserUrlDomain(), userSetting.getUserUrlDomainNormalized(), userSetting.isAutoRedirectEnabled(), userSetting.isViewEnabled(), userSetting.isShareEnabled(), userSetting.getDefaultDomain(), userSetting.getDefaultTokenType(), userSetting.getDefaultRedirectType(), userSetting.getDefaultFlashDuration(), userSetting.getDefaultAccessType(), userSetting.getDefaultViewType(), userSetting.getDefaultShareType(), userSetting.getDefaultPassphrase(), userSetting.getDefaultSignature(), userSetting.isUseSignature(), userSetting.getDefaultEmblem(), userSetting.isUseEmblem(), userSetting.getDefaultBackground(), userSetting.isUseBackground(), userSetting.getSponsorUrl(), userSetting.getSponsorBanner(), userSetting.getSponsorNote(), userSetting.isUseSponsor(), userSetting.getExtraParams(), userSetting.getExpirationDuration());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new UserSettingBean(userSetting.getGuid(), userSetting.getUser(), userSetting.getHomePage(), userSetting.getSelfBio(), userSetting.getUserUrlDomain(), userSetting.getUserUrlDomainNormalized(), userSetting.isAutoRedirectEnabled(), userSetting.isViewEnabled(), userSetting.isShareEnabled(), userSetting.getDefaultDomain(), userSetting.getDefaultTokenType(), userSetting.getDefaultRedirectType(), userSetting.getDefaultFlashDuration(), userSetting.getDefaultAccessType(), userSetting.getDefaultViewType(), userSetting.getDefaultShareType(), userSetting.getDefaultPassphrase(), userSetting.getDefaultSignature(), userSetting.isUseSignature(), userSetting.getDefaultEmblem(), userSetting.isUseEmblem(), userSetting.getDefaultBackground(), userSetting.isUseBackground(), userSetting.getSponsorUrl(), userSetting.getSponsorBanner(), userSetting.getSponsorNote(), userSetting.isUseSponsor(), userSetting.getExtraParams(), userSetting.getExpirationDuration());
        } else {
            log.log(Level.WARNING, "createUserSetting(): Arg userSetting is of an unknown type.");
            //bean = new UserSettingBean();
            bean = new UserSettingBean(userSetting.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getUserSettingServiceProxy().createUserSetting(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public UserSetting constructUserSetting(UserSetting userSetting) throws BaseException
    {
        log.finer("BEGIN");

        // Param userSetting cannot be null.....
        if(userSetting == null) {
            log.log(Level.INFO, "Param userSetting is null!");
            throw new BadRequestException("Param userSetting object is null!");
        }
        UserSettingBean bean = null;
        if(userSetting instanceof UserSettingBean) {
            bean = (UserSettingBean) userSetting;
        } else if(userSetting instanceof UserSetting) {
            // bean = new UserSettingBean(null, userSetting.getUser(), userSetting.getHomePage(), userSetting.getSelfBio(), userSetting.getUserUrlDomain(), userSetting.getUserUrlDomainNormalized(), userSetting.isAutoRedirectEnabled(), userSetting.isViewEnabled(), userSetting.isShareEnabled(), userSetting.getDefaultDomain(), userSetting.getDefaultTokenType(), userSetting.getDefaultRedirectType(), userSetting.getDefaultFlashDuration(), userSetting.getDefaultAccessType(), userSetting.getDefaultViewType(), userSetting.getDefaultShareType(), userSetting.getDefaultPassphrase(), userSetting.getDefaultSignature(), userSetting.isUseSignature(), userSetting.getDefaultEmblem(), userSetting.isUseEmblem(), userSetting.getDefaultBackground(), userSetting.isUseBackground(), userSetting.getSponsorUrl(), userSetting.getSponsorBanner(), userSetting.getSponsorNote(), userSetting.isUseSponsor(), userSetting.getExtraParams(), userSetting.getExpirationDuration());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new UserSettingBean(userSetting.getGuid(), userSetting.getUser(), userSetting.getHomePage(), userSetting.getSelfBio(), userSetting.getUserUrlDomain(), userSetting.getUserUrlDomainNormalized(), userSetting.isAutoRedirectEnabled(), userSetting.isViewEnabled(), userSetting.isShareEnabled(), userSetting.getDefaultDomain(), userSetting.getDefaultTokenType(), userSetting.getDefaultRedirectType(), userSetting.getDefaultFlashDuration(), userSetting.getDefaultAccessType(), userSetting.getDefaultViewType(), userSetting.getDefaultShareType(), userSetting.getDefaultPassphrase(), userSetting.getDefaultSignature(), userSetting.isUseSignature(), userSetting.getDefaultEmblem(), userSetting.isUseEmblem(), userSetting.getDefaultBackground(), userSetting.isUseBackground(), userSetting.getSponsorUrl(), userSetting.getSponsorBanner(), userSetting.getSponsorNote(), userSetting.isUseSponsor(), userSetting.getExtraParams(), userSetting.getExpirationDuration());
        } else {
            log.log(Level.WARNING, "createUserSetting(): Arg userSetting is of an unknown type.");
            //bean = new UserSettingBean();
            bean = new UserSettingBean(userSetting.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getUserSettingServiceProxy().createUserSetting(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateUserSetting(String guid, String user, String homePage, String selfBio, String userUrlDomain, String userUrlDomainNormalized, Boolean autoRedirectEnabled, Boolean viewEnabled, Boolean shareEnabled, String defaultDomain, String defaultTokenType, String defaultRedirectType, Long defaultFlashDuration, String defaultAccessType, String defaultViewType, String defaultShareType, String defaultPassphrase, String defaultSignature, Boolean useSignature, String defaultEmblem, Boolean useEmblem, String defaultBackground, Boolean useBackground, String sponsorUrl, String sponsorBanner, String sponsorNote, Boolean useSponsor, String extraParams, Long expirationDuration) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        UserSettingBean bean = new UserSettingBean(guid, user, homePage, selfBio, userUrlDomain, userUrlDomainNormalized, autoRedirectEnabled, viewEnabled, shareEnabled, defaultDomain, defaultTokenType, defaultRedirectType, defaultFlashDuration, defaultAccessType, defaultViewType, defaultShareType, defaultPassphrase, defaultSignature, useSignature, defaultEmblem, useEmblem, defaultBackground, useBackground, sponsorUrl, sponsorBanner, sponsorNote, useSponsor, extraParams, expirationDuration);
        return updateUserSetting(bean);
    }
        
    @Override
    public Boolean updateUserSetting(UserSetting userSetting) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //UserSetting bean = refreshUserSetting(userSetting);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param userSetting cannot be null.....
        if(userSetting == null || userSetting.getGuid() == null) {
            log.log(Level.INFO, "Param userSetting or its guid is null!");
            throw new BadRequestException("Param userSetting object or its guid is null!");
        }
        UserSettingBean bean = null;
        if(userSetting instanceof UserSettingBean) {
            bean = (UserSettingBean) userSetting;
        } else {  // if(userSetting instanceof UserSetting)
            bean = new UserSettingBean(userSetting.getGuid(), userSetting.getUser(), userSetting.getHomePage(), userSetting.getSelfBio(), userSetting.getUserUrlDomain(), userSetting.getUserUrlDomainNormalized(), userSetting.isAutoRedirectEnabled(), userSetting.isViewEnabled(), userSetting.isShareEnabled(), userSetting.getDefaultDomain(), userSetting.getDefaultTokenType(), userSetting.getDefaultRedirectType(), userSetting.getDefaultFlashDuration(), userSetting.getDefaultAccessType(), userSetting.getDefaultViewType(), userSetting.getDefaultShareType(), userSetting.getDefaultPassphrase(), userSetting.getDefaultSignature(), userSetting.isUseSignature(), userSetting.getDefaultEmblem(), userSetting.isUseEmblem(), userSetting.getDefaultBackground(), userSetting.isUseBackground(), userSetting.getSponsorUrl(), userSetting.getSponsorBanner(), userSetting.getSponsorNote(), userSetting.isUseSponsor(), userSetting.getExtraParams(), userSetting.getExpirationDuration());
        }
        Boolean suc = getProxyFactory().getUserSettingServiceProxy().updateUserSetting(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public UserSetting refreshUserSetting(UserSetting userSetting) throws BaseException
    {
        log.finer("BEGIN");

        // Param userSetting cannot be null.....
        if(userSetting == null || userSetting.getGuid() == null) {
            log.log(Level.INFO, "Param userSetting or its guid is null!");
            throw new BadRequestException("Param userSetting object or its guid is null!");
        }
        UserSettingBean bean = null;
        if(userSetting instanceof UserSettingBean) {
            bean = (UserSettingBean) userSetting;
        } else {  // if(userSetting instanceof UserSetting)
            bean = new UserSettingBean(userSetting.getGuid(), userSetting.getUser(), userSetting.getHomePage(), userSetting.getSelfBio(), userSetting.getUserUrlDomain(), userSetting.getUserUrlDomainNormalized(), userSetting.isAutoRedirectEnabled(), userSetting.isViewEnabled(), userSetting.isShareEnabled(), userSetting.getDefaultDomain(), userSetting.getDefaultTokenType(), userSetting.getDefaultRedirectType(), userSetting.getDefaultFlashDuration(), userSetting.getDefaultAccessType(), userSetting.getDefaultViewType(), userSetting.getDefaultShareType(), userSetting.getDefaultPassphrase(), userSetting.getDefaultSignature(), userSetting.isUseSignature(), userSetting.getDefaultEmblem(), userSetting.isUseEmblem(), userSetting.getDefaultBackground(), userSetting.isUseBackground(), userSetting.getSponsorUrl(), userSetting.getSponsorBanner(), userSetting.getSponsorNote(), userSetting.isUseSponsor(), userSetting.getExtraParams(), userSetting.getExpirationDuration());
        }
        Boolean suc = getProxyFactory().getUserSettingServiceProxy().updateUserSetting(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteUserSetting(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getUserSettingServiceProxy().deleteUserSetting(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }
            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            UserSetting userSetting = null;
            try {
                userSetting = getProxyFactory().getUserSettingServiceProxy().getUserSetting(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch userSetting with a key, " + guid);
                return false;
            }
            if(userSetting != null) {
                String beanGuid = userSetting.getGuid();
                Boolean suc1 = getProxyFactory().getUserSettingServiceProxy().deleteUserSetting(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("userSetting with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteUserSetting(UserSetting userSetting) throws BaseException
    {
        log.finer("BEGIN");

        // Param userSetting cannot be null.....
        if(userSetting == null || userSetting.getGuid() == null) {
            log.log(Level.INFO, "Param userSetting or its guid is null!");
            throw new BadRequestException("Param userSetting object or its guid is null!");
        }
        UserSettingBean bean = null;
        if(userSetting instanceof UserSettingBean) {
            bean = (UserSettingBean) userSetting;
        } else {  // if(userSetting instanceof UserSetting)
            // ????
            log.warning("userSetting is not an instance of UserSettingBean.");
            bean = new UserSettingBean(userSetting.getGuid(), userSetting.getUser(), userSetting.getHomePage(), userSetting.getSelfBio(), userSetting.getUserUrlDomain(), userSetting.getUserUrlDomainNormalized(), userSetting.isAutoRedirectEnabled(), userSetting.isViewEnabled(), userSetting.isShareEnabled(), userSetting.getDefaultDomain(), userSetting.getDefaultTokenType(), userSetting.getDefaultRedirectType(), userSetting.getDefaultFlashDuration(), userSetting.getDefaultAccessType(), userSetting.getDefaultViewType(), userSetting.getDefaultShareType(), userSetting.getDefaultPassphrase(), userSetting.getDefaultSignature(), userSetting.isUseSignature(), userSetting.getDefaultEmblem(), userSetting.isUseEmblem(), userSetting.getDefaultBackground(), userSetting.isUseBackground(), userSetting.getSponsorUrl(), userSetting.getSponsorBanner(), userSetting.getSponsorNote(), userSetting.isUseSponsor(), userSetting.getExtraParams(), userSetting.getExpirationDuration());
        }
        Boolean suc = getProxyFactory().getUserSettingServiceProxy().deleteUserSetting(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteUserSettings(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getUserSettingServiceProxy().deleteUserSettings(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createUserSettings(List<UserSetting> userSettings) throws BaseException
    {
        log.finer("BEGIN");

        if(userSettings == null) {
            log.log(Level.WARNING, "createUserSettings() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = userSettings.size();
        if(size == 0) {
            log.log(Level.WARNING, "createUserSettings() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(UserSetting userSetting : userSettings) {
            String guid = createUserSetting(userSetting);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createUserSettings() failed for at least one userSetting. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateUserSettings(List<UserSetting> userSettings) throws BaseException
    //{
    //}

}
