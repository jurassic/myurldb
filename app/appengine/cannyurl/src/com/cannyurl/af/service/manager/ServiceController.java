package com.cannyurl.af.service.manager;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.cannyurl.af.service.ApiConsumerService;
import com.cannyurl.af.service.AppCustomDomainService;
import com.cannyurl.af.service.SiteCustomDomainService;
import com.cannyurl.af.service.UserService;
import com.cannyurl.af.service.UserUsercodeService;
import com.cannyurl.af.service.UserPasswordService;
import com.cannyurl.af.service.ExternalUserAuthService;
import com.cannyurl.af.service.UserAuthStateService;
import com.cannyurl.af.service.UserResourcePermissionService;
import com.cannyurl.af.service.UserResourceProhibitionService;
import com.cannyurl.af.service.RolePermissionService;
import com.cannyurl.af.service.UserRoleService;
import com.cannyurl.af.service.AppClientService;
import com.cannyurl.af.service.ClientUserService;
import com.cannyurl.af.service.UserCustomDomainService;
import com.cannyurl.af.service.ClientSettingService;
import com.cannyurl.af.service.UserSettingService;
import com.cannyurl.af.service.VisitorSettingService;
import com.cannyurl.af.service.TwitterSummaryCardService;
import com.cannyurl.af.service.TwitterPhotoCardService;
import com.cannyurl.af.service.TwitterGalleryCardService;
import com.cannyurl.af.service.TwitterAppCardService;
import com.cannyurl.af.service.TwitterPlayerCardService;
import com.cannyurl.af.service.TwitterProductCardService;
import com.cannyurl.af.service.ShortPassageService;
import com.cannyurl.af.service.ShortLinkService;
import com.cannyurl.af.service.GeoLinkService;
import com.cannyurl.af.service.QrCodeService;
import com.cannyurl.af.service.LinkPassphraseService;
import com.cannyurl.af.service.LinkMessageService;
import com.cannyurl.af.service.LinkAlbumService;
import com.cannyurl.af.service.AlbumShortLinkService;
import com.cannyurl.af.service.KeywordFolderService;
import com.cannyurl.af.service.BookmarkFolderService;
import com.cannyurl.af.service.KeywordLinkService;
import com.cannyurl.af.service.BookmarkLinkService;
import com.cannyurl.af.service.SpeedDialService;
import com.cannyurl.af.service.KeywordFolderImportService;
import com.cannyurl.af.service.BookmarkFolderImportService;
import com.cannyurl.af.service.KeywordCrowdTallyService;
import com.cannyurl.af.service.BookmarkCrowdTallyService;
import com.cannyurl.af.service.DomainInfoService;
import com.cannyurl.af.service.UrlRatingService;
import com.cannyurl.af.service.UserRatingService;
import com.cannyurl.af.service.AbuseTagService;
import com.cannyurl.af.service.ServiceInfoService;
import com.cannyurl.af.service.FiveTenService;

// TBD: DI
// (For now, this class is used to make ServiceManager "DI-capable".)
public class ServiceController
{
    private static final Logger log = Logger.getLogger(ServiceController.class.getName());

    // "Injectable" services.
    private ApiConsumerService apiConsumerService = null;
    private AppCustomDomainService appCustomDomainService = null;
    private SiteCustomDomainService siteCustomDomainService = null;
    private UserService userService = null;
    private UserUsercodeService userUsercodeService = null;
    private UserPasswordService userPasswordService = null;
    private ExternalUserAuthService externalUserAuthService = null;
    private UserAuthStateService userAuthStateService = null;
    private UserResourcePermissionService userResourcePermissionService = null;
    private UserResourceProhibitionService userResourceProhibitionService = null;
    private RolePermissionService rolePermissionService = null;
    private UserRoleService userRoleService = null;
    private AppClientService appClientService = null;
    private ClientUserService clientUserService = null;
    private UserCustomDomainService userCustomDomainService = null;
    private ClientSettingService clientSettingService = null;
    private UserSettingService userSettingService = null;
    private VisitorSettingService visitorSettingService = null;
    private TwitterSummaryCardService twitterSummaryCardService = null;
    private TwitterPhotoCardService twitterPhotoCardService = null;
    private TwitterGalleryCardService twitterGalleryCardService = null;
    private TwitterAppCardService twitterAppCardService = null;
    private TwitterPlayerCardService twitterPlayerCardService = null;
    private TwitterProductCardService twitterProductCardService = null;
    private ShortPassageService shortPassageService = null;
    private ShortLinkService shortLinkService = null;
    private GeoLinkService geoLinkService = null;
    private QrCodeService qrCodeService = null;
    private LinkPassphraseService linkPassphraseService = null;
    private LinkMessageService linkMessageService = null;
    private LinkAlbumService linkAlbumService = null;
    private AlbumShortLinkService albumShortLinkService = null;
    private KeywordFolderService keywordFolderService = null;
    private BookmarkFolderService bookmarkFolderService = null;
    private KeywordLinkService keywordLinkService = null;
    private BookmarkLinkService bookmarkLinkService = null;
    private SpeedDialService speedDialService = null;
    private KeywordFolderImportService keywordFolderImportService = null;
    private BookmarkFolderImportService bookmarkFolderImportService = null;
    private KeywordCrowdTallyService keywordCrowdTallyService = null;
    private BookmarkCrowdTallyService bookmarkCrowdTallyService = null;
    private DomainInfoService domainInfoService = null;
    private UrlRatingService urlRatingService = null;
    private UserRatingService userRatingService = null;
    private AbuseTagService abuseTagService = null;
    private ServiceInfoService serviceInfoService = null;
    private FiveTenService fiveTenService = null;

    // Ctor injection.
    public ServiceController(ApiConsumerService apiConsumerService, AppCustomDomainService appCustomDomainService, SiteCustomDomainService siteCustomDomainService, UserService userService, UserUsercodeService userUsercodeService, UserPasswordService userPasswordService, ExternalUserAuthService externalUserAuthService, UserAuthStateService userAuthStateService, UserResourcePermissionService userResourcePermissionService, UserResourceProhibitionService userResourceProhibitionService, RolePermissionService rolePermissionService, UserRoleService userRoleService, AppClientService appClientService, ClientUserService clientUserService, UserCustomDomainService userCustomDomainService, ClientSettingService clientSettingService, UserSettingService userSettingService, VisitorSettingService visitorSettingService, TwitterSummaryCardService twitterSummaryCardService, TwitterPhotoCardService twitterPhotoCardService, TwitterGalleryCardService twitterGalleryCardService, TwitterAppCardService twitterAppCardService, TwitterPlayerCardService twitterPlayerCardService, TwitterProductCardService twitterProductCardService, ShortPassageService shortPassageService, ShortLinkService shortLinkService, GeoLinkService geoLinkService, QrCodeService qrCodeService, LinkPassphraseService linkPassphraseService, LinkMessageService linkMessageService, LinkAlbumService linkAlbumService, AlbumShortLinkService albumShortLinkService, KeywordFolderService keywordFolderService, BookmarkFolderService bookmarkFolderService, KeywordLinkService keywordLinkService, BookmarkLinkService bookmarkLinkService, SpeedDialService speedDialService, KeywordFolderImportService keywordFolderImportService, BookmarkFolderImportService bookmarkFolderImportService, KeywordCrowdTallyService keywordCrowdTallyService, BookmarkCrowdTallyService bookmarkCrowdTallyService, DomainInfoService domainInfoService, UrlRatingService urlRatingService, UserRatingService userRatingService, AbuseTagService abuseTagService, ServiceInfoService serviceInfoService, FiveTenService fiveTenService)
    {
        this.apiConsumerService = apiConsumerService;
        this.appCustomDomainService = appCustomDomainService;
        this.siteCustomDomainService = siteCustomDomainService;
        this.userService = userService;
        this.userUsercodeService = userUsercodeService;
        this.userPasswordService = userPasswordService;
        this.externalUserAuthService = externalUserAuthService;
        this.userAuthStateService = userAuthStateService;
        this.userResourcePermissionService = userResourcePermissionService;
        this.userResourceProhibitionService = userResourceProhibitionService;
        this.rolePermissionService = rolePermissionService;
        this.userRoleService = userRoleService;
        this.appClientService = appClientService;
        this.clientUserService = clientUserService;
        this.userCustomDomainService = userCustomDomainService;
        this.clientSettingService = clientSettingService;
        this.userSettingService = userSettingService;
        this.visitorSettingService = visitorSettingService;
        this.twitterSummaryCardService = twitterSummaryCardService;
        this.twitterPhotoCardService = twitterPhotoCardService;
        this.twitterGalleryCardService = twitterGalleryCardService;
        this.twitterAppCardService = twitterAppCardService;
        this.twitterPlayerCardService = twitterPlayerCardService;
        this.twitterProductCardService = twitterProductCardService;
        this.shortPassageService = shortPassageService;
        this.shortLinkService = shortLinkService;
        this.geoLinkService = geoLinkService;
        this.qrCodeService = qrCodeService;
        this.linkPassphraseService = linkPassphraseService;
        this.linkMessageService = linkMessageService;
        this.linkAlbumService = linkAlbumService;
        this.albumShortLinkService = albumShortLinkService;
        this.keywordFolderService = keywordFolderService;
        this.bookmarkFolderService = bookmarkFolderService;
        this.keywordLinkService = keywordLinkService;
        this.bookmarkLinkService = bookmarkLinkService;
        this.speedDialService = speedDialService;
        this.keywordFolderImportService = keywordFolderImportService;
        this.bookmarkFolderImportService = bookmarkFolderImportService;
        this.keywordCrowdTallyService = keywordCrowdTallyService;
        this.bookmarkCrowdTallyService = bookmarkCrowdTallyService;
        this.domainInfoService = domainInfoService;
        this.urlRatingService = urlRatingService;
        this.userRatingService = userRatingService;
        this.abuseTagService = abuseTagService;
        this.serviceInfoService = serviceInfoService;
        this.fiveTenService = fiveTenService;
    }

    // Returns a ApiConsumerService instance.
	public ApiConsumerService getApiConsumerService() 
    {
        return this.apiConsumerService;
    }
    // Setter injection for ApiConsumerService.
	public void setApiConsumerService(ApiConsumerService apiConsumerService) 
    {
        this.apiConsumerService = apiConsumerService;
    }

    // Returns a AppCustomDomainService instance.
	public AppCustomDomainService getAppCustomDomainService() 
    {
        return this.appCustomDomainService;
    }
    // Setter injection for AppCustomDomainService.
	public void setAppCustomDomainService(AppCustomDomainService appCustomDomainService) 
    {
        this.appCustomDomainService = appCustomDomainService;
    }

    // Returns a SiteCustomDomainService instance.
	public SiteCustomDomainService getSiteCustomDomainService() 
    {
        return this.siteCustomDomainService;
    }
    // Setter injection for SiteCustomDomainService.
	public void setSiteCustomDomainService(SiteCustomDomainService siteCustomDomainService) 
    {
        this.siteCustomDomainService = siteCustomDomainService;
    }

    // Returns a UserService instance.
	public UserService getUserService() 
    {
        return this.userService;
    }
    // Setter injection for UserService.
	public void setUserService(UserService userService) 
    {
        this.userService = userService;
    }

    // Returns a UserUsercodeService instance.
	public UserUsercodeService getUserUsercodeService() 
    {
        return this.userUsercodeService;
    }
    // Setter injection for UserUsercodeService.
	public void setUserUsercodeService(UserUsercodeService userUsercodeService) 
    {
        this.userUsercodeService = userUsercodeService;
    }

    // Returns a UserPasswordService instance.
	public UserPasswordService getUserPasswordService() 
    {
        return this.userPasswordService;
    }
    // Setter injection for UserPasswordService.
	public void setUserPasswordService(UserPasswordService userPasswordService) 
    {
        this.userPasswordService = userPasswordService;
    }

    // Returns a ExternalUserAuthService instance.
	public ExternalUserAuthService getExternalUserAuthService() 
    {
        return this.externalUserAuthService;
    }
    // Setter injection for ExternalUserAuthService.
	public void setExternalUserAuthService(ExternalUserAuthService externalUserAuthService) 
    {
        this.externalUserAuthService = externalUserAuthService;
    }

    // Returns a UserAuthStateService instance.
	public UserAuthStateService getUserAuthStateService() 
    {
        return this.userAuthStateService;
    }
    // Setter injection for UserAuthStateService.
	public void setUserAuthStateService(UserAuthStateService userAuthStateService) 
    {
        this.userAuthStateService = userAuthStateService;
    }

    // Returns a UserResourcePermissionService instance.
	public UserResourcePermissionService getUserResourcePermissionService() 
    {
        return this.userResourcePermissionService;
    }
    // Setter injection for UserResourcePermissionService.
	public void setUserResourcePermissionService(UserResourcePermissionService userResourcePermissionService) 
    {
        this.userResourcePermissionService = userResourcePermissionService;
    }

    // Returns a UserResourceProhibitionService instance.
	public UserResourceProhibitionService getUserResourceProhibitionService() 
    {
        return this.userResourceProhibitionService;
    }
    // Setter injection for UserResourceProhibitionService.
	public void setUserResourceProhibitionService(UserResourceProhibitionService userResourceProhibitionService) 
    {
        this.userResourceProhibitionService = userResourceProhibitionService;
    }

    // Returns a RolePermissionService instance.
	public RolePermissionService getRolePermissionService() 
    {
        return this.rolePermissionService;
    }
    // Setter injection for RolePermissionService.
	public void setRolePermissionService(RolePermissionService rolePermissionService) 
    {
        this.rolePermissionService = rolePermissionService;
    }

    // Returns a UserRoleService instance.
	public UserRoleService getUserRoleService() 
    {
        return this.userRoleService;
    }
    // Setter injection for UserRoleService.
	public void setUserRoleService(UserRoleService userRoleService) 
    {
        this.userRoleService = userRoleService;
    }

    // Returns a AppClientService instance.
	public AppClientService getAppClientService() 
    {
        return this.appClientService;
    }
    // Setter injection for AppClientService.
	public void setAppClientService(AppClientService appClientService) 
    {
        this.appClientService = appClientService;
    }

    // Returns a ClientUserService instance.
	public ClientUserService getClientUserService() 
    {
        return this.clientUserService;
    }
    // Setter injection for ClientUserService.
	public void setClientUserService(ClientUserService clientUserService) 
    {
        this.clientUserService = clientUserService;
    }

    // Returns a UserCustomDomainService instance.
	public UserCustomDomainService getUserCustomDomainService() 
    {
        return this.userCustomDomainService;
    }
    // Setter injection for UserCustomDomainService.
	public void setUserCustomDomainService(UserCustomDomainService userCustomDomainService) 
    {
        this.userCustomDomainService = userCustomDomainService;
    }

    // Returns a ClientSettingService instance.
	public ClientSettingService getClientSettingService() 
    {
        return this.clientSettingService;
    }
    // Setter injection for ClientSettingService.
	public void setClientSettingService(ClientSettingService clientSettingService) 
    {
        this.clientSettingService = clientSettingService;
    }

    // Returns a UserSettingService instance.
	public UserSettingService getUserSettingService() 
    {
        return this.userSettingService;
    }
    // Setter injection for UserSettingService.
	public void setUserSettingService(UserSettingService userSettingService) 
    {
        this.userSettingService = userSettingService;
    }

    // Returns a VisitorSettingService instance.
	public VisitorSettingService getVisitorSettingService() 
    {
        return this.visitorSettingService;
    }
    // Setter injection for VisitorSettingService.
	public void setVisitorSettingService(VisitorSettingService visitorSettingService) 
    {
        this.visitorSettingService = visitorSettingService;
    }

    // Returns a TwitterSummaryCardService instance.
	public TwitterSummaryCardService getTwitterSummaryCardService() 
    {
        return this.twitterSummaryCardService;
    }
    // Setter injection for TwitterSummaryCardService.
	public void setTwitterSummaryCardService(TwitterSummaryCardService twitterSummaryCardService) 
    {
        this.twitterSummaryCardService = twitterSummaryCardService;
    }

    // Returns a TwitterPhotoCardService instance.
	public TwitterPhotoCardService getTwitterPhotoCardService() 
    {
        return this.twitterPhotoCardService;
    }
    // Setter injection for TwitterPhotoCardService.
	public void setTwitterPhotoCardService(TwitterPhotoCardService twitterPhotoCardService) 
    {
        this.twitterPhotoCardService = twitterPhotoCardService;
    }

    // Returns a TwitterGalleryCardService instance.
	public TwitterGalleryCardService getTwitterGalleryCardService() 
    {
        return this.twitterGalleryCardService;
    }
    // Setter injection for TwitterGalleryCardService.
	public void setTwitterGalleryCardService(TwitterGalleryCardService twitterGalleryCardService) 
    {
        this.twitterGalleryCardService = twitterGalleryCardService;
    }

    // Returns a TwitterAppCardService instance.
	public TwitterAppCardService getTwitterAppCardService() 
    {
        return this.twitterAppCardService;
    }
    // Setter injection for TwitterAppCardService.
	public void setTwitterAppCardService(TwitterAppCardService twitterAppCardService) 
    {
        this.twitterAppCardService = twitterAppCardService;
    }

    // Returns a TwitterPlayerCardService instance.
	public TwitterPlayerCardService getTwitterPlayerCardService() 
    {
        return this.twitterPlayerCardService;
    }
    // Setter injection for TwitterPlayerCardService.
	public void setTwitterPlayerCardService(TwitterPlayerCardService twitterPlayerCardService) 
    {
        this.twitterPlayerCardService = twitterPlayerCardService;
    }

    // Returns a TwitterProductCardService instance.
	public TwitterProductCardService getTwitterProductCardService() 
    {
        return this.twitterProductCardService;
    }
    // Setter injection for TwitterProductCardService.
	public void setTwitterProductCardService(TwitterProductCardService twitterProductCardService) 
    {
        this.twitterProductCardService = twitterProductCardService;
    }

    // Returns a ShortPassageService instance.
	public ShortPassageService getShortPassageService() 
    {
        return this.shortPassageService;
    }
    // Setter injection for ShortPassageService.
	public void setShortPassageService(ShortPassageService shortPassageService) 
    {
        this.shortPassageService = shortPassageService;
    }

    // Returns a ShortLinkService instance.
	public ShortLinkService getShortLinkService() 
    {
        return this.shortLinkService;
    }
    // Setter injection for ShortLinkService.
	public void setShortLinkService(ShortLinkService shortLinkService) 
    {
        this.shortLinkService = shortLinkService;
    }

    // Returns a GeoLinkService instance.
	public GeoLinkService getGeoLinkService() 
    {
        return this.geoLinkService;
    }
    // Setter injection for GeoLinkService.
	public void setGeoLinkService(GeoLinkService geoLinkService) 
    {
        this.geoLinkService = geoLinkService;
    }

    // Returns a QrCodeService instance.
	public QrCodeService getQrCodeService() 
    {
        return this.qrCodeService;
    }
    // Setter injection for QrCodeService.
	public void setQrCodeService(QrCodeService qrCodeService) 
    {
        this.qrCodeService = qrCodeService;
    }

    // Returns a LinkPassphraseService instance.
	public LinkPassphraseService getLinkPassphraseService() 
    {
        return this.linkPassphraseService;
    }
    // Setter injection for LinkPassphraseService.
	public void setLinkPassphraseService(LinkPassphraseService linkPassphraseService) 
    {
        this.linkPassphraseService = linkPassphraseService;
    }

    // Returns a LinkMessageService instance.
	public LinkMessageService getLinkMessageService() 
    {
        return this.linkMessageService;
    }
    // Setter injection for LinkMessageService.
	public void setLinkMessageService(LinkMessageService linkMessageService) 
    {
        this.linkMessageService = linkMessageService;
    }

    // Returns a LinkAlbumService instance.
	public LinkAlbumService getLinkAlbumService() 
    {
        return this.linkAlbumService;
    }
    // Setter injection for LinkAlbumService.
	public void setLinkAlbumService(LinkAlbumService linkAlbumService) 
    {
        this.linkAlbumService = linkAlbumService;
    }

    // Returns a AlbumShortLinkService instance.
	public AlbumShortLinkService getAlbumShortLinkService() 
    {
        return this.albumShortLinkService;
    }
    // Setter injection for AlbumShortLinkService.
	public void setAlbumShortLinkService(AlbumShortLinkService albumShortLinkService) 
    {
        this.albumShortLinkService = albumShortLinkService;
    }

    // Returns a KeywordFolderService instance.
	public KeywordFolderService getKeywordFolderService() 
    {
        return this.keywordFolderService;
    }
    // Setter injection for KeywordFolderService.
	public void setKeywordFolderService(KeywordFolderService keywordFolderService) 
    {
        this.keywordFolderService = keywordFolderService;
    }

    // Returns a BookmarkFolderService instance.
	public BookmarkFolderService getBookmarkFolderService() 
    {
        return this.bookmarkFolderService;
    }
    // Setter injection for BookmarkFolderService.
	public void setBookmarkFolderService(BookmarkFolderService bookmarkFolderService) 
    {
        this.bookmarkFolderService = bookmarkFolderService;
    }

    // Returns a KeywordLinkService instance.
	public KeywordLinkService getKeywordLinkService() 
    {
        return this.keywordLinkService;
    }
    // Setter injection for KeywordLinkService.
	public void setKeywordLinkService(KeywordLinkService keywordLinkService) 
    {
        this.keywordLinkService = keywordLinkService;
    }

    // Returns a BookmarkLinkService instance.
	public BookmarkLinkService getBookmarkLinkService() 
    {
        return this.bookmarkLinkService;
    }
    // Setter injection for BookmarkLinkService.
	public void setBookmarkLinkService(BookmarkLinkService bookmarkLinkService) 
    {
        this.bookmarkLinkService = bookmarkLinkService;
    }

    // Returns a SpeedDialService instance.
	public SpeedDialService getSpeedDialService() 
    {
        return this.speedDialService;
    }
    // Setter injection for SpeedDialService.
	public void setSpeedDialService(SpeedDialService speedDialService) 
    {
        this.speedDialService = speedDialService;
    }

    // Returns a KeywordFolderImportService instance.
	public KeywordFolderImportService getKeywordFolderImportService() 
    {
        return this.keywordFolderImportService;
    }
    // Setter injection for KeywordFolderImportService.
	public void setKeywordFolderImportService(KeywordFolderImportService keywordFolderImportService) 
    {
        this.keywordFolderImportService = keywordFolderImportService;
    }

    // Returns a BookmarkFolderImportService instance.
	public BookmarkFolderImportService getBookmarkFolderImportService() 
    {
        return this.bookmarkFolderImportService;
    }
    // Setter injection for BookmarkFolderImportService.
	public void setBookmarkFolderImportService(BookmarkFolderImportService bookmarkFolderImportService) 
    {
        this.bookmarkFolderImportService = bookmarkFolderImportService;
    }

    // Returns a KeywordCrowdTallyService instance.
	public KeywordCrowdTallyService getKeywordCrowdTallyService() 
    {
        return this.keywordCrowdTallyService;
    }
    // Setter injection for KeywordCrowdTallyService.
	public void setKeywordCrowdTallyService(KeywordCrowdTallyService keywordCrowdTallyService) 
    {
        this.keywordCrowdTallyService = keywordCrowdTallyService;
    }

    // Returns a BookmarkCrowdTallyService instance.
	public BookmarkCrowdTallyService getBookmarkCrowdTallyService() 
    {
        return this.bookmarkCrowdTallyService;
    }
    // Setter injection for BookmarkCrowdTallyService.
	public void setBookmarkCrowdTallyService(BookmarkCrowdTallyService bookmarkCrowdTallyService) 
    {
        this.bookmarkCrowdTallyService = bookmarkCrowdTallyService;
    }

    // Returns a DomainInfoService instance.
	public DomainInfoService getDomainInfoService() 
    {
        return this.domainInfoService;
    }
    // Setter injection for DomainInfoService.
	public void setDomainInfoService(DomainInfoService domainInfoService) 
    {
        this.domainInfoService = domainInfoService;
    }

    // Returns a UrlRatingService instance.
	public UrlRatingService getUrlRatingService() 
    {
        return this.urlRatingService;
    }
    // Setter injection for UrlRatingService.
	public void setUrlRatingService(UrlRatingService urlRatingService) 
    {
        this.urlRatingService = urlRatingService;
    }

    // Returns a UserRatingService instance.
	public UserRatingService getUserRatingService() 
    {
        return this.userRatingService;
    }
    // Setter injection for UserRatingService.
	public void setUserRatingService(UserRatingService userRatingService) 
    {
        this.userRatingService = userRatingService;
    }

    // Returns a AbuseTagService instance.
	public AbuseTagService getAbuseTagService() 
    {
        return this.abuseTagService;
    }
    // Setter injection for AbuseTagService.
	public void setAbuseTagService(AbuseTagService abuseTagService) 
    {
        this.abuseTagService = abuseTagService;
    }

    // Returns a ServiceInfoService instance.
	public ServiceInfoService getServiceInfoService() 
    {
        return this.serviceInfoService;
    }
    // Setter injection for ServiceInfoService.
	public void setServiceInfoService(ServiceInfoService serviceInfoService) 
    {
        this.serviceInfoService = serviceInfoService;
    }

    // Returns a FiveTenService instance.
	public FiveTenService getFiveTenService() 
    {
        return this.fiveTenService;
    }
    // Setter injection for FiveTenService.
	public void setFiveTenService(FiveTenService fiveTenService) 
    {
        this.fiveTenService = fiveTenService;
    }

}
