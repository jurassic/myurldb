package com.cannyurl.af.service.manager;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.cannyurl.af.service.AbstractServiceFactory;
import com.cannyurl.af.service.ApiConsumerService;
import com.cannyurl.af.service.AppCustomDomainService;
import com.cannyurl.af.service.SiteCustomDomainService;
import com.cannyurl.af.service.UserService;
import com.cannyurl.af.service.UserUsercodeService;
import com.cannyurl.af.service.UserPasswordService;
import com.cannyurl.af.service.ExternalUserAuthService;
import com.cannyurl.af.service.UserAuthStateService;
import com.cannyurl.af.service.UserResourcePermissionService;
import com.cannyurl.af.service.UserResourceProhibitionService;
import com.cannyurl.af.service.RolePermissionService;
import com.cannyurl.af.service.UserRoleService;
import com.cannyurl.af.service.AppClientService;
import com.cannyurl.af.service.ClientUserService;
import com.cannyurl.af.service.UserCustomDomainService;
import com.cannyurl.af.service.ClientSettingService;
import com.cannyurl.af.service.UserSettingService;
import com.cannyurl.af.service.VisitorSettingService;
import com.cannyurl.af.service.TwitterSummaryCardService;
import com.cannyurl.af.service.TwitterPhotoCardService;
import com.cannyurl.af.service.TwitterGalleryCardService;
import com.cannyurl.af.service.TwitterAppCardService;
import com.cannyurl.af.service.TwitterPlayerCardService;
import com.cannyurl.af.service.TwitterProductCardService;
import com.cannyurl.af.service.ShortPassageService;
import com.cannyurl.af.service.ShortLinkService;
import com.cannyurl.af.service.GeoLinkService;
import com.cannyurl.af.service.QrCodeService;
import com.cannyurl.af.service.LinkPassphraseService;
import com.cannyurl.af.service.LinkMessageService;
import com.cannyurl.af.service.LinkAlbumService;
import com.cannyurl.af.service.AlbumShortLinkService;
import com.cannyurl.af.service.KeywordFolderService;
import com.cannyurl.af.service.BookmarkFolderService;
import com.cannyurl.af.service.KeywordLinkService;
import com.cannyurl.af.service.BookmarkLinkService;
import com.cannyurl.af.service.SpeedDialService;
import com.cannyurl.af.service.KeywordFolderImportService;
import com.cannyurl.af.service.BookmarkFolderImportService;
import com.cannyurl.af.service.KeywordCrowdTallyService;
import com.cannyurl.af.service.BookmarkCrowdTallyService;
import com.cannyurl.af.service.DomainInfoService;
import com.cannyurl.af.service.UrlRatingService;
import com.cannyurl.af.service.UserRatingService;
import com.cannyurl.af.service.AbuseTagService;
import com.cannyurl.af.service.ServiceInfoService;
import com.cannyurl.af.service.FiveTenService;

// TBD:
// Factory? DI? (Does the current "dual" approach make sense?)
// Make it a singleton? Implement pooling, etc. ????
public final class ServiceManager
{
    private static final Logger log = Logger.getLogger(ServiceManager.class.getName());

    // Reference to the Abstract factory.
    private static AbstractServiceFactory serviceFactory = ServiceFactoryManager.getServiceFactory();

    // All service getters/setters are delegated to ServiceController.
    // TBD: Use a DI framework such as Spring or Guice....
    private static ServiceController serviceController = new ServiceController(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);

    // Static methods only.
    private ServiceManager() {}

    // Returns a ApiConsumerService instance.
	public static ApiConsumerService getApiConsumerService() 
    {
        if(serviceController.getApiConsumerService() == null) {
            serviceController.setApiConsumerService(serviceFactory.getApiConsumerService());
        }
        return serviceController.getApiConsumerService();
    }
    // Injects a ApiConsumerService instance.
	public static void setApiConsumerService(ApiConsumerService apiConsumerService) 
    {
        serviceController.setApiConsumerService(apiConsumerService);
    }

    // Returns a AppCustomDomainService instance.
	public static AppCustomDomainService getAppCustomDomainService() 
    {
        if(serviceController.getAppCustomDomainService() == null) {
            serviceController.setAppCustomDomainService(serviceFactory.getAppCustomDomainService());
        }
        return serviceController.getAppCustomDomainService();
    }
    // Injects a AppCustomDomainService instance.
	public static void setAppCustomDomainService(AppCustomDomainService appCustomDomainService) 
    {
        serviceController.setAppCustomDomainService(appCustomDomainService);
    }

    // Returns a SiteCustomDomainService instance.
	public static SiteCustomDomainService getSiteCustomDomainService() 
    {
        if(serviceController.getSiteCustomDomainService() == null) {
            serviceController.setSiteCustomDomainService(serviceFactory.getSiteCustomDomainService());
        }
        return serviceController.getSiteCustomDomainService();
    }
    // Injects a SiteCustomDomainService instance.
	public static void setSiteCustomDomainService(SiteCustomDomainService siteCustomDomainService) 
    {
        serviceController.setSiteCustomDomainService(siteCustomDomainService);
    }

    // Returns a UserService instance.
	public static UserService getUserService() 
    {
        if(serviceController.getUserService() == null) {
            serviceController.setUserService(serviceFactory.getUserService());
        }
        return serviceController.getUserService();
    }
    // Injects a UserService instance.
	public static void setUserService(UserService userService) 
    {
        serviceController.setUserService(userService);
    }

    // Returns a UserUsercodeService instance.
	public static UserUsercodeService getUserUsercodeService() 
    {
        if(serviceController.getUserUsercodeService() == null) {
            serviceController.setUserUsercodeService(serviceFactory.getUserUsercodeService());
        }
        return serviceController.getUserUsercodeService();
    }
    // Injects a UserUsercodeService instance.
	public static void setUserUsercodeService(UserUsercodeService userUsercodeService) 
    {
        serviceController.setUserUsercodeService(userUsercodeService);
    }

    // Returns a UserPasswordService instance.
	public static UserPasswordService getUserPasswordService() 
    {
        if(serviceController.getUserPasswordService() == null) {
            serviceController.setUserPasswordService(serviceFactory.getUserPasswordService());
        }
        return serviceController.getUserPasswordService();
    }
    // Injects a UserPasswordService instance.
	public static void setUserPasswordService(UserPasswordService userPasswordService) 
    {
        serviceController.setUserPasswordService(userPasswordService);
    }

    // Returns a ExternalUserAuthService instance.
	public static ExternalUserAuthService getExternalUserAuthService() 
    {
        if(serviceController.getExternalUserAuthService() == null) {
            serviceController.setExternalUserAuthService(serviceFactory.getExternalUserAuthService());
        }
        return serviceController.getExternalUserAuthService();
    }
    // Injects a ExternalUserAuthService instance.
	public static void setExternalUserAuthService(ExternalUserAuthService externalUserAuthService) 
    {
        serviceController.setExternalUserAuthService(externalUserAuthService);
    }

    // Returns a UserAuthStateService instance.
	public static UserAuthStateService getUserAuthStateService() 
    {
        if(serviceController.getUserAuthStateService() == null) {
            serviceController.setUserAuthStateService(serviceFactory.getUserAuthStateService());
        }
        return serviceController.getUserAuthStateService();
    }
    // Injects a UserAuthStateService instance.
	public static void setUserAuthStateService(UserAuthStateService userAuthStateService) 
    {
        serviceController.setUserAuthStateService(userAuthStateService);
    }

    // Returns a UserResourcePermissionService instance.
	public static UserResourcePermissionService getUserResourcePermissionService() 
    {
        if(serviceController.getUserResourcePermissionService() == null) {
            serviceController.setUserResourcePermissionService(serviceFactory.getUserResourcePermissionService());
        }
        return serviceController.getUserResourcePermissionService();
    }
    // Injects a UserResourcePermissionService instance.
	public static void setUserResourcePermissionService(UserResourcePermissionService userResourcePermissionService) 
    {
        serviceController.setUserResourcePermissionService(userResourcePermissionService);
    }

    // Returns a UserResourceProhibitionService instance.
	public static UserResourceProhibitionService getUserResourceProhibitionService() 
    {
        if(serviceController.getUserResourceProhibitionService() == null) {
            serviceController.setUserResourceProhibitionService(serviceFactory.getUserResourceProhibitionService());
        }
        return serviceController.getUserResourceProhibitionService();
    }
    // Injects a UserResourceProhibitionService instance.
	public static void setUserResourceProhibitionService(UserResourceProhibitionService userResourceProhibitionService) 
    {
        serviceController.setUserResourceProhibitionService(userResourceProhibitionService);
    }

    // Returns a RolePermissionService instance.
	public static RolePermissionService getRolePermissionService() 
    {
        if(serviceController.getRolePermissionService() == null) {
            serviceController.setRolePermissionService(serviceFactory.getRolePermissionService());
        }
        return serviceController.getRolePermissionService();
    }
    // Injects a RolePermissionService instance.
	public static void setRolePermissionService(RolePermissionService rolePermissionService) 
    {
        serviceController.setRolePermissionService(rolePermissionService);
    }

    // Returns a UserRoleService instance.
	public static UserRoleService getUserRoleService() 
    {
        if(serviceController.getUserRoleService() == null) {
            serviceController.setUserRoleService(serviceFactory.getUserRoleService());
        }
        return serviceController.getUserRoleService();
    }
    // Injects a UserRoleService instance.
	public static void setUserRoleService(UserRoleService userRoleService) 
    {
        serviceController.setUserRoleService(userRoleService);
    }

    // Returns a AppClientService instance.
	public static AppClientService getAppClientService() 
    {
        if(serviceController.getAppClientService() == null) {
            serviceController.setAppClientService(serviceFactory.getAppClientService());
        }
        return serviceController.getAppClientService();
    }
    // Injects a AppClientService instance.
	public static void setAppClientService(AppClientService appClientService) 
    {
        serviceController.setAppClientService(appClientService);
    }

    // Returns a ClientUserService instance.
	public static ClientUserService getClientUserService() 
    {
        if(serviceController.getClientUserService() == null) {
            serviceController.setClientUserService(serviceFactory.getClientUserService());
        }
        return serviceController.getClientUserService();
    }
    // Injects a ClientUserService instance.
	public static void setClientUserService(ClientUserService clientUserService) 
    {
        serviceController.setClientUserService(clientUserService);
    }

    // Returns a UserCustomDomainService instance.
	public static UserCustomDomainService getUserCustomDomainService() 
    {
        if(serviceController.getUserCustomDomainService() == null) {
            serviceController.setUserCustomDomainService(serviceFactory.getUserCustomDomainService());
        }
        return serviceController.getUserCustomDomainService();
    }
    // Injects a UserCustomDomainService instance.
	public static void setUserCustomDomainService(UserCustomDomainService userCustomDomainService) 
    {
        serviceController.setUserCustomDomainService(userCustomDomainService);
    }

    // Returns a ClientSettingService instance.
	public static ClientSettingService getClientSettingService() 
    {
        if(serviceController.getClientSettingService() == null) {
            serviceController.setClientSettingService(serviceFactory.getClientSettingService());
        }
        return serviceController.getClientSettingService();
    }
    // Injects a ClientSettingService instance.
	public static void setClientSettingService(ClientSettingService clientSettingService) 
    {
        serviceController.setClientSettingService(clientSettingService);
    }

    // Returns a UserSettingService instance.
	public static UserSettingService getUserSettingService() 
    {
        if(serviceController.getUserSettingService() == null) {
            serviceController.setUserSettingService(serviceFactory.getUserSettingService());
        }
        return serviceController.getUserSettingService();
    }
    // Injects a UserSettingService instance.
	public static void setUserSettingService(UserSettingService userSettingService) 
    {
        serviceController.setUserSettingService(userSettingService);
    }

    // Returns a VisitorSettingService instance.
	public static VisitorSettingService getVisitorSettingService() 
    {
        if(serviceController.getVisitorSettingService() == null) {
            serviceController.setVisitorSettingService(serviceFactory.getVisitorSettingService());
        }
        return serviceController.getVisitorSettingService();
    }
    // Injects a VisitorSettingService instance.
	public static void setVisitorSettingService(VisitorSettingService visitorSettingService) 
    {
        serviceController.setVisitorSettingService(visitorSettingService);
    }

    // Returns a TwitterSummaryCardService instance.
	public static TwitterSummaryCardService getTwitterSummaryCardService() 
    {
        if(serviceController.getTwitterSummaryCardService() == null) {
            serviceController.setTwitterSummaryCardService(serviceFactory.getTwitterSummaryCardService());
        }
        return serviceController.getTwitterSummaryCardService();
    }
    // Injects a TwitterSummaryCardService instance.
	public static void setTwitterSummaryCardService(TwitterSummaryCardService twitterSummaryCardService) 
    {
        serviceController.setTwitterSummaryCardService(twitterSummaryCardService);
    }

    // Returns a TwitterPhotoCardService instance.
	public static TwitterPhotoCardService getTwitterPhotoCardService() 
    {
        if(serviceController.getTwitterPhotoCardService() == null) {
            serviceController.setTwitterPhotoCardService(serviceFactory.getTwitterPhotoCardService());
        }
        return serviceController.getTwitterPhotoCardService();
    }
    // Injects a TwitterPhotoCardService instance.
	public static void setTwitterPhotoCardService(TwitterPhotoCardService twitterPhotoCardService) 
    {
        serviceController.setTwitterPhotoCardService(twitterPhotoCardService);
    }

    // Returns a TwitterGalleryCardService instance.
	public static TwitterGalleryCardService getTwitterGalleryCardService() 
    {
        if(serviceController.getTwitterGalleryCardService() == null) {
            serviceController.setTwitterGalleryCardService(serviceFactory.getTwitterGalleryCardService());
        }
        return serviceController.getTwitterGalleryCardService();
    }
    // Injects a TwitterGalleryCardService instance.
	public static void setTwitterGalleryCardService(TwitterGalleryCardService twitterGalleryCardService) 
    {
        serviceController.setTwitterGalleryCardService(twitterGalleryCardService);
    }

    // Returns a TwitterAppCardService instance.
	public static TwitterAppCardService getTwitterAppCardService() 
    {
        if(serviceController.getTwitterAppCardService() == null) {
            serviceController.setTwitterAppCardService(serviceFactory.getTwitterAppCardService());
        }
        return serviceController.getTwitterAppCardService();
    }
    // Injects a TwitterAppCardService instance.
	public static void setTwitterAppCardService(TwitterAppCardService twitterAppCardService) 
    {
        serviceController.setTwitterAppCardService(twitterAppCardService);
    }

    // Returns a TwitterPlayerCardService instance.
	public static TwitterPlayerCardService getTwitterPlayerCardService() 
    {
        if(serviceController.getTwitterPlayerCardService() == null) {
            serviceController.setTwitterPlayerCardService(serviceFactory.getTwitterPlayerCardService());
        }
        return serviceController.getTwitterPlayerCardService();
    }
    // Injects a TwitterPlayerCardService instance.
	public static void setTwitterPlayerCardService(TwitterPlayerCardService twitterPlayerCardService) 
    {
        serviceController.setTwitterPlayerCardService(twitterPlayerCardService);
    }

    // Returns a TwitterProductCardService instance.
	public static TwitterProductCardService getTwitterProductCardService() 
    {
        if(serviceController.getTwitterProductCardService() == null) {
            serviceController.setTwitterProductCardService(serviceFactory.getTwitterProductCardService());
        }
        return serviceController.getTwitterProductCardService();
    }
    // Injects a TwitterProductCardService instance.
	public static void setTwitterProductCardService(TwitterProductCardService twitterProductCardService) 
    {
        serviceController.setTwitterProductCardService(twitterProductCardService);
    }

    // Returns a ShortPassageService instance.
	public static ShortPassageService getShortPassageService() 
    {
        if(serviceController.getShortPassageService() == null) {
            serviceController.setShortPassageService(serviceFactory.getShortPassageService());
        }
        return serviceController.getShortPassageService();
    }
    // Injects a ShortPassageService instance.
	public static void setShortPassageService(ShortPassageService shortPassageService) 
    {
        serviceController.setShortPassageService(shortPassageService);
    }

    // Returns a ShortLinkService instance.
	public static ShortLinkService getShortLinkService() 
    {
        if(serviceController.getShortLinkService() == null) {
            serviceController.setShortLinkService(serviceFactory.getShortLinkService());
        }
        return serviceController.getShortLinkService();
    }
    // Injects a ShortLinkService instance.
	public static void setShortLinkService(ShortLinkService shortLinkService) 
    {
        serviceController.setShortLinkService(shortLinkService);
    }

    // Returns a GeoLinkService instance.
	public static GeoLinkService getGeoLinkService() 
    {
        if(serviceController.getGeoLinkService() == null) {
            serviceController.setGeoLinkService(serviceFactory.getGeoLinkService());
        }
        return serviceController.getGeoLinkService();
    }
    // Injects a GeoLinkService instance.
	public static void setGeoLinkService(GeoLinkService geoLinkService) 
    {
        serviceController.setGeoLinkService(geoLinkService);
    }

    // Returns a QrCodeService instance.
	public static QrCodeService getQrCodeService() 
    {
        if(serviceController.getQrCodeService() == null) {
            serviceController.setQrCodeService(serviceFactory.getQrCodeService());
        }
        return serviceController.getQrCodeService();
    }
    // Injects a QrCodeService instance.
	public static void setQrCodeService(QrCodeService qrCodeService) 
    {
        serviceController.setQrCodeService(qrCodeService);
    }

    // Returns a LinkPassphraseService instance.
	public static LinkPassphraseService getLinkPassphraseService() 
    {
        if(serviceController.getLinkPassphraseService() == null) {
            serviceController.setLinkPassphraseService(serviceFactory.getLinkPassphraseService());
        }
        return serviceController.getLinkPassphraseService();
    }
    // Injects a LinkPassphraseService instance.
	public static void setLinkPassphraseService(LinkPassphraseService linkPassphraseService) 
    {
        serviceController.setLinkPassphraseService(linkPassphraseService);
    }

    // Returns a LinkMessageService instance.
	public static LinkMessageService getLinkMessageService() 
    {
        if(serviceController.getLinkMessageService() == null) {
            serviceController.setLinkMessageService(serviceFactory.getLinkMessageService());
        }
        return serviceController.getLinkMessageService();
    }
    // Injects a LinkMessageService instance.
	public static void setLinkMessageService(LinkMessageService linkMessageService) 
    {
        serviceController.setLinkMessageService(linkMessageService);
    }

    // Returns a LinkAlbumService instance.
	public static LinkAlbumService getLinkAlbumService() 
    {
        if(serviceController.getLinkAlbumService() == null) {
            serviceController.setLinkAlbumService(serviceFactory.getLinkAlbumService());
        }
        return serviceController.getLinkAlbumService();
    }
    // Injects a LinkAlbumService instance.
	public static void setLinkAlbumService(LinkAlbumService linkAlbumService) 
    {
        serviceController.setLinkAlbumService(linkAlbumService);
    }

    // Returns a AlbumShortLinkService instance.
	public static AlbumShortLinkService getAlbumShortLinkService() 
    {
        if(serviceController.getAlbumShortLinkService() == null) {
            serviceController.setAlbumShortLinkService(serviceFactory.getAlbumShortLinkService());
        }
        return serviceController.getAlbumShortLinkService();
    }
    // Injects a AlbumShortLinkService instance.
	public static void setAlbumShortLinkService(AlbumShortLinkService albumShortLinkService) 
    {
        serviceController.setAlbumShortLinkService(albumShortLinkService);
    }

    // Returns a KeywordFolderService instance.
	public static KeywordFolderService getKeywordFolderService() 
    {
        if(serviceController.getKeywordFolderService() == null) {
            serviceController.setKeywordFolderService(serviceFactory.getKeywordFolderService());
        }
        return serviceController.getKeywordFolderService();
    }
    // Injects a KeywordFolderService instance.
	public static void setKeywordFolderService(KeywordFolderService keywordFolderService) 
    {
        serviceController.setKeywordFolderService(keywordFolderService);
    }

    // Returns a BookmarkFolderService instance.
	public static BookmarkFolderService getBookmarkFolderService() 
    {
        if(serviceController.getBookmarkFolderService() == null) {
            serviceController.setBookmarkFolderService(serviceFactory.getBookmarkFolderService());
        }
        return serviceController.getBookmarkFolderService();
    }
    // Injects a BookmarkFolderService instance.
	public static void setBookmarkFolderService(BookmarkFolderService bookmarkFolderService) 
    {
        serviceController.setBookmarkFolderService(bookmarkFolderService);
    }

    // Returns a KeywordLinkService instance.
	public static KeywordLinkService getKeywordLinkService() 
    {
        if(serviceController.getKeywordLinkService() == null) {
            serviceController.setKeywordLinkService(serviceFactory.getKeywordLinkService());
        }
        return serviceController.getKeywordLinkService();
    }
    // Injects a KeywordLinkService instance.
	public static void setKeywordLinkService(KeywordLinkService keywordLinkService) 
    {
        serviceController.setKeywordLinkService(keywordLinkService);
    }

    // Returns a BookmarkLinkService instance.
	public static BookmarkLinkService getBookmarkLinkService() 
    {
        if(serviceController.getBookmarkLinkService() == null) {
            serviceController.setBookmarkLinkService(serviceFactory.getBookmarkLinkService());
        }
        return serviceController.getBookmarkLinkService();
    }
    // Injects a BookmarkLinkService instance.
	public static void setBookmarkLinkService(BookmarkLinkService bookmarkLinkService) 
    {
        serviceController.setBookmarkLinkService(bookmarkLinkService);
    }

    // Returns a SpeedDialService instance.
	public static SpeedDialService getSpeedDialService() 
    {
        if(serviceController.getSpeedDialService() == null) {
            serviceController.setSpeedDialService(serviceFactory.getSpeedDialService());
        }
        return serviceController.getSpeedDialService();
    }
    // Injects a SpeedDialService instance.
	public static void setSpeedDialService(SpeedDialService speedDialService) 
    {
        serviceController.setSpeedDialService(speedDialService);
    }

    // Returns a KeywordFolderImportService instance.
	public static KeywordFolderImportService getKeywordFolderImportService() 
    {
        if(serviceController.getKeywordFolderImportService() == null) {
            serviceController.setKeywordFolderImportService(serviceFactory.getKeywordFolderImportService());
        }
        return serviceController.getKeywordFolderImportService();
    }
    // Injects a KeywordFolderImportService instance.
	public static void setKeywordFolderImportService(KeywordFolderImportService keywordFolderImportService) 
    {
        serviceController.setKeywordFolderImportService(keywordFolderImportService);
    }

    // Returns a BookmarkFolderImportService instance.
	public static BookmarkFolderImportService getBookmarkFolderImportService() 
    {
        if(serviceController.getBookmarkFolderImportService() == null) {
            serviceController.setBookmarkFolderImportService(serviceFactory.getBookmarkFolderImportService());
        }
        return serviceController.getBookmarkFolderImportService();
    }
    // Injects a BookmarkFolderImportService instance.
	public static void setBookmarkFolderImportService(BookmarkFolderImportService bookmarkFolderImportService) 
    {
        serviceController.setBookmarkFolderImportService(bookmarkFolderImportService);
    }

    // Returns a KeywordCrowdTallyService instance.
	public static KeywordCrowdTallyService getKeywordCrowdTallyService() 
    {
        if(serviceController.getKeywordCrowdTallyService() == null) {
            serviceController.setKeywordCrowdTallyService(serviceFactory.getKeywordCrowdTallyService());
        }
        return serviceController.getKeywordCrowdTallyService();
    }
    // Injects a KeywordCrowdTallyService instance.
	public static void setKeywordCrowdTallyService(KeywordCrowdTallyService keywordCrowdTallyService) 
    {
        serviceController.setKeywordCrowdTallyService(keywordCrowdTallyService);
    }

    // Returns a BookmarkCrowdTallyService instance.
	public static BookmarkCrowdTallyService getBookmarkCrowdTallyService() 
    {
        if(serviceController.getBookmarkCrowdTallyService() == null) {
            serviceController.setBookmarkCrowdTallyService(serviceFactory.getBookmarkCrowdTallyService());
        }
        return serviceController.getBookmarkCrowdTallyService();
    }
    // Injects a BookmarkCrowdTallyService instance.
	public static void setBookmarkCrowdTallyService(BookmarkCrowdTallyService bookmarkCrowdTallyService) 
    {
        serviceController.setBookmarkCrowdTallyService(bookmarkCrowdTallyService);
    }

    // Returns a DomainInfoService instance.
	public static DomainInfoService getDomainInfoService() 
    {
        if(serviceController.getDomainInfoService() == null) {
            serviceController.setDomainInfoService(serviceFactory.getDomainInfoService());
        }
        return serviceController.getDomainInfoService();
    }
    // Injects a DomainInfoService instance.
	public static void setDomainInfoService(DomainInfoService domainInfoService) 
    {
        serviceController.setDomainInfoService(domainInfoService);
    }

    // Returns a UrlRatingService instance.
	public static UrlRatingService getUrlRatingService() 
    {
        if(serviceController.getUrlRatingService() == null) {
            serviceController.setUrlRatingService(serviceFactory.getUrlRatingService());
        }
        return serviceController.getUrlRatingService();
    }
    // Injects a UrlRatingService instance.
	public static void setUrlRatingService(UrlRatingService urlRatingService) 
    {
        serviceController.setUrlRatingService(urlRatingService);
    }

    // Returns a UserRatingService instance.
	public static UserRatingService getUserRatingService() 
    {
        if(serviceController.getUserRatingService() == null) {
            serviceController.setUserRatingService(serviceFactory.getUserRatingService());
        }
        return serviceController.getUserRatingService();
    }
    // Injects a UserRatingService instance.
	public static void setUserRatingService(UserRatingService userRatingService) 
    {
        serviceController.setUserRatingService(userRatingService);
    }

    // Returns a AbuseTagService instance.
	public static AbuseTagService getAbuseTagService() 
    {
        if(serviceController.getAbuseTagService() == null) {
            serviceController.setAbuseTagService(serviceFactory.getAbuseTagService());
        }
        return serviceController.getAbuseTagService();
    }
    // Injects a AbuseTagService instance.
	public static void setAbuseTagService(AbuseTagService abuseTagService) 
    {
        serviceController.setAbuseTagService(abuseTagService);
    }

    // Returns a ServiceInfoService instance.
	public static ServiceInfoService getServiceInfoService() 
    {
        if(serviceController.getServiceInfoService() == null) {
            serviceController.setServiceInfoService(serviceFactory.getServiceInfoService());
        }
        return serviceController.getServiceInfoService();
    }
    // Injects a ServiceInfoService instance.
	public static void setServiceInfoService(ServiceInfoService serviceInfoService) 
    {
        serviceController.setServiceInfoService(serviceInfoService);
    }

    // Returns a FiveTenService instance.
	public static FiveTenService getFiveTenService() 
    {
        if(serviceController.getFiveTenService() == null) {
            serviceController.setFiveTenService(serviceFactory.getFiveTenService());
        }
        return serviceController.getFiveTenService();
    }
    // Injects a FiveTenService instance.
	public static void setFiveTenService(FiveTenService fiveTenService) 
    {
        serviceController.setFiveTenService(fiveTenService);
    }

}
