package com.cannyurl.af.service.proto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.AlbumShortLink;
import com.cannyurl.af.bean.AlbumShortLinkBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.AlbumShortLinkService;
import com.cannyurl.af.service.impl.AlbumShortLinkServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class AlbumShortLinkProtoService extends AlbumShortLinkServiceImpl implements AlbumShortLinkService
{
    private static final Logger log = Logger.getLogger(AlbumShortLinkProtoService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public AlbumShortLinkProtoService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // AlbumShortLink related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public AlbumShortLink getAlbumShortLink(String guid) throws BaseException
    {
        return super.getAlbumShortLink(guid);
    }

    @Override
    public Object getAlbumShortLink(String guid, String field) throws BaseException
    {
        return super.getAlbumShortLink(guid, field);
    }

    @Override
    public List<AlbumShortLink> getAlbumShortLinks(List<String> guids) throws BaseException
    {
        return super.getAlbumShortLinks(guids);
    }

    @Override
    public List<AlbumShortLink> getAllAlbumShortLinks() throws BaseException
    {
        return super.getAllAlbumShortLinks();
    }

    @Override
    public List<String> getAllAlbumShortLinkKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllAlbumShortLinkKeys(ordering, offset, count);
    }

    @Override
    public List<AlbumShortLink> findAlbumShortLinks(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findAlbumShortLinks(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findAlbumShortLinkKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findAlbumShortLinkKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createAlbumShortLink(AlbumShortLink albumShortLink) throws BaseException
    {
        return super.createAlbumShortLink(albumShortLink);
    }

    @Override
    public AlbumShortLink constructAlbumShortLink(AlbumShortLink albumShortLink) throws BaseException
    {
        return super.constructAlbumShortLink(albumShortLink);
    }


    @Override
    public Boolean updateAlbumShortLink(AlbumShortLink albumShortLink) throws BaseException
    {
        return super.updateAlbumShortLink(albumShortLink);
    }
        
    @Override
    public AlbumShortLink refreshAlbumShortLink(AlbumShortLink albumShortLink) throws BaseException
    {
        return super.refreshAlbumShortLink(albumShortLink);
    }

    @Override
    public Boolean deleteAlbumShortLink(String guid) throws BaseException
    {
        return super.deleteAlbumShortLink(guid);
    }

    @Override
    public Boolean deleteAlbumShortLink(AlbumShortLink albumShortLink) throws BaseException
    {
        return super.deleteAlbumShortLink(albumShortLink);
    }

    @Override
    public Integer createAlbumShortLinks(List<AlbumShortLink> albumShortLinks) throws BaseException
    {
        return super.createAlbumShortLinks(albumShortLinks);
    }

    // TBD
    //@Override
    //public Boolean updateAlbumShortLinks(List<AlbumShortLink> albumShortLinks) throws BaseException
    //{
    //}

}
