package com.cannyurl.af.service.proto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.AppClient;
import com.cannyurl.af.bean.AppClientBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.AppClientService;
import com.cannyurl.af.service.impl.AppClientServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class AppClientProtoService extends AppClientServiceImpl implements AppClientService
{
    private static final Logger log = Logger.getLogger(AppClientProtoService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public AppClientProtoService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // AppClient related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public AppClient getAppClient(String guid) throws BaseException
    {
        return super.getAppClient(guid);
    }

    @Override
    public Object getAppClient(String guid, String field) throws BaseException
    {
        return super.getAppClient(guid, field);
    }

    @Override
    public List<AppClient> getAppClients(List<String> guids) throws BaseException
    {
        return super.getAppClients(guids);
    }

    @Override
    public List<AppClient> getAllAppClients() throws BaseException
    {
        return super.getAllAppClients();
    }

    @Override
    public List<String> getAllAppClientKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllAppClientKeys(ordering, offset, count);
    }

    @Override
    public List<AppClient> findAppClients(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findAppClients(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findAppClientKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findAppClientKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createAppClient(AppClient appClient) throws BaseException
    {
        return super.createAppClient(appClient);
    }

    @Override
    public AppClient constructAppClient(AppClient appClient) throws BaseException
    {
        return super.constructAppClient(appClient);
    }


    @Override
    public Boolean updateAppClient(AppClient appClient) throws BaseException
    {
        return super.updateAppClient(appClient);
    }
        
    @Override
    public AppClient refreshAppClient(AppClient appClient) throws BaseException
    {
        return super.refreshAppClient(appClient);
    }

    @Override
    public Boolean deleteAppClient(String guid) throws BaseException
    {
        return super.deleteAppClient(guid);
    }

    @Override
    public Boolean deleteAppClient(AppClient appClient) throws BaseException
    {
        return super.deleteAppClient(appClient);
    }

    @Override
    public Integer createAppClients(List<AppClient> appClients) throws BaseException
    {
        return super.createAppClients(appClients);
    }

    // TBD
    //@Override
    //public Boolean updateAppClients(List<AppClient> appClients) throws BaseException
    //{
    //}

}
