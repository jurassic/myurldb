package com.cannyurl.af.service.proto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.BookmarkCrowdTally;
import com.cannyurl.af.bean.BookmarkCrowdTallyBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.BookmarkCrowdTallyService;
import com.cannyurl.af.service.impl.BookmarkCrowdTallyServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class BookmarkCrowdTallyProtoService extends BookmarkCrowdTallyServiceImpl implements BookmarkCrowdTallyService
{
    private static final Logger log = Logger.getLogger(BookmarkCrowdTallyProtoService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public BookmarkCrowdTallyProtoService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // BookmarkCrowdTally related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public BookmarkCrowdTally getBookmarkCrowdTally(String guid) throws BaseException
    {
        return super.getBookmarkCrowdTally(guid);
    }

    @Override
    public Object getBookmarkCrowdTally(String guid, String field) throws BaseException
    {
        return super.getBookmarkCrowdTally(guid, field);
    }

    @Override
    public List<BookmarkCrowdTally> getBookmarkCrowdTallies(List<String> guids) throws BaseException
    {
        return super.getBookmarkCrowdTallies(guids);
    }

    @Override
    public List<BookmarkCrowdTally> getAllBookmarkCrowdTallies() throws BaseException
    {
        return super.getAllBookmarkCrowdTallies();
    }

    @Override
    public List<String> getAllBookmarkCrowdTallyKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllBookmarkCrowdTallyKeys(ordering, offset, count);
    }

    @Override
    public List<BookmarkCrowdTally> findBookmarkCrowdTallies(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findBookmarkCrowdTallies(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findBookmarkCrowdTallyKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findBookmarkCrowdTallyKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createBookmarkCrowdTally(BookmarkCrowdTally bookmarkCrowdTally) throws BaseException
    {
        return super.createBookmarkCrowdTally(bookmarkCrowdTally);
    }

    @Override
    public BookmarkCrowdTally constructBookmarkCrowdTally(BookmarkCrowdTally bookmarkCrowdTally) throws BaseException
    {
        return super.constructBookmarkCrowdTally(bookmarkCrowdTally);
    }


    @Override
    public Boolean updateBookmarkCrowdTally(BookmarkCrowdTally bookmarkCrowdTally) throws BaseException
    {
        return super.updateBookmarkCrowdTally(bookmarkCrowdTally);
    }
        
    @Override
    public BookmarkCrowdTally refreshBookmarkCrowdTally(BookmarkCrowdTally bookmarkCrowdTally) throws BaseException
    {
        return super.refreshBookmarkCrowdTally(bookmarkCrowdTally);
    }

    @Override
    public Boolean deleteBookmarkCrowdTally(String guid) throws BaseException
    {
        return super.deleteBookmarkCrowdTally(guid);
    }

    @Override
    public Boolean deleteBookmarkCrowdTally(BookmarkCrowdTally bookmarkCrowdTally) throws BaseException
    {
        return super.deleteBookmarkCrowdTally(bookmarkCrowdTally);
    }

    @Override
    public Integer createBookmarkCrowdTallies(List<BookmarkCrowdTally> bookmarkCrowdTallies) throws BaseException
    {
        return super.createBookmarkCrowdTallies(bookmarkCrowdTallies);
    }

    // TBD
    //@Override
    //public Boolean updateBookmarkCrowdTallies(List<BookmarkCrowdTally> bookmarkCrowdTallies) throws BaseException
    //{
    //}

}
