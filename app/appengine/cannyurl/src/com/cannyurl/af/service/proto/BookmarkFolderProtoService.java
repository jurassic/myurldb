package com.cannyurl.af.service.proto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.BookmarkFolder;
import com.cannyurl.af.bean.BookmarkFolderBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.BookmarkFolderService;
import com.cannyurl.af.service.impl.BookmarkFolderServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class BookmarkFolderProtoService extends BookmarkFolderServiceImpl implements BookmarkFolderService
{
    private static final Logger log = Logger.getLogger(BookmarkFolderProtoService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public BookmarkFolderProtoService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // BookmarkFolder related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public BookmarkFolder getBookmarkFolder(String guid) throws BaseException
    {
        return super.getBookmarkFolder(guid);
    }

    @Override
    public Object getBookmarkFolder(String guid, String field) throws BaseException
    {
        return super.getBookmarkFolder(guid, field);
    }

    @Override
    public List<BookmarkFolder> getBookmarkFolders(List<String> guids) throws BaseException
    {
        return super.getBookmarkFolders(guids);
    }

    @Override
    public List<BookmarkFolder> getAllBookmarkFolders() throws BaseException
    {
        return super.getAllBookmarkFolders();
    }

    @Override
    public List<String> getAllBookmarkFolderKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllBookmarkFolderKeys(ordering, offset, count);
    }

    @Override
    public List<BookmarkFolder> findBookmarkFolders(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findBookmarkFolders(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findBookmarkFolderKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findBookmarkFolderKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createBookmarkFolder(BookmarkFolder bookmarkFolder) throws BaseException
    {
        return super.createBookmarkFolder(bookmarkFolder);
    }

    @Override
    public BookmarkFolder constructBookmarkFolder(BookmarkFolder bookmarkFolder) throws BaseException
    {
        return super.constructBookmarkFolder(bookmarkFolder);
    }


    @Override
    public Boolean updateBookmarkFolder(BookmarkFolder bookmarkFolder) throws BaseException
    {
        return super.updateBookmarkFolder(bookmarkFolder);
    }
        
    @Override
    public BookmarkFolder refreshBookmarkFolder(BookmarkFolder bookmarkFolder) throws BaseException
    {
        return super.refreshBookmarkFolder(bookmarkFolder);
    }

    @Override
    public Boolean deleteBookmarkFolder(String guid) throws BaseException
    {
        return super.deleteBookmarkFolder(guid);
    }

    @Override
    public Boolean deleteBookmarkFolder(BookmarkFolder bookmarkFolder) throws BaseException
    {
        return super.deleteBookmarkFolder(bookmarkFolder);
    }

    @Override
    public Integer createBookmarkFolders(List<BookmarkFolder> bookmarkFolders) throws BaseException
    {
        return super.createBookmarkFolders(bookmarkFolders);
    }

    // TBD
    //@Override
    //public Boolean updateBookmarkFolders(List<BookmarkFolder> bookmarkFolders) throws BaseException
    //{
    //}

}
