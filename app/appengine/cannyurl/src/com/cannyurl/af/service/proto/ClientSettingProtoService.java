package com.cannyurl.af.service.proto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.ClientSetting;
import com.cannyurl.af.bean.ClientSettingBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.ClientSettingService;
import com.cannyurl.af.service.impl.ClientSettingServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class ClientSettingProtoService extends ClientSettingServiceImpl implements ClientSettingService
{
    private static final Logger log = Logger.getLogger(ClientSettingProtoService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public ClientSettingProtoService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // ClientSetting related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public ClientSetting getClientSetting(String guid) throws BaseException
    {
        return super.getClientSetting(guid);
    }

    @Override
    public Object getClientSetting(String guid, String field) throws BaseException
    {
        return super.getClientSetting(guid, field);
    }

    @Override
    public List<ClientSetting> getClientSettings(List<String> guids) throws BaseException
    {
        return super.getClientSettings(guids);
    }

    @Override
    public List<ClientSetting> getAllClientSettings() throws BaseException
    {
        return super.getAllClientSettings();
    }

    @Override
    public List<String> getAllClientSettingKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllClientSettingKeys(ordering, offset, count);
    }

    @Override
    public List<ClientSetting> findClientSettings(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findClientSettings(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findClientSettingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findClientSettingKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createClientSetting(ClientSetting clientSetting) throws BaseException
    {
        return super.createClientSetting(clientSetting);
    }

    @Override
    public ClientSetting constructClientSetting(ClientSetting clientSetting) throws BaseException
    {
        return super.constructClientSetting(clientSetting);
    }


    @Override
    public Boolean updateClientSetting(ClientSetting clientSetting) throws BaseException
    {
        return super.updateClientSetting(clientSetting);
    }
        
    @Override
    public ClientSetting refreshClientSetting(ClientSetting clientSetting) throws BaseException
    {
        return super.refreshClientSetting(clientSetting);
    }

    @Override
    public Boolean deleteClientSetting(String guid) throws BaseException
    {
        return super.deleteClientSetting(guid);
    }

    @Override
    public Boolean deleteClientSetting(ClientSetting clientSetting) throws BaseException
    {
        return super.deleteClientSetting(clientSetting);
    }

    @Override
    public Integer createClientSettings(List<ClientSetting> clientSettings) throws BaseException
    {
        return super.createClientSettings(clientSettings);
    }

    // TBD
    //@Override
    //public Boolean updateClientSettings(List<ClientSetting> clientSettings) throws BaseException
    //{
    //}

}
