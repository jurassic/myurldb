package com.cannyurl.af.service.proto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.DomainInfo;
import com.cannyurl.af.bean.DomainInfoBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.DomainInfoService;
import com.cannyurl.af.service.impl.DomainInfoServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class DomainInfoProtoService extends DomainInfoServiceImpl implements DomainInfoService
{
    private static final Logger log = Logger.getLogger(DomainInfoProtoService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public DomainInfoProtoService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // DomainInfo related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public DomainInfo getDomainInfo(String guid) throws BaseException
    {
        return super.getDomainInfo(guid);
    }

    @Override
    public Object getDomainInfo(String guid, String field) throws BaseException
    {
        return super.getDomainInfo(guid, field);
    }

    @Override
    public List<DomainInfo> getDomainInfos(List<String> guids) throws BaseException
    {
        return super.getDomainInfos(guids);
    }

    @Override
    public List<DomainInfo> getAllDomainInfos() throws BaseException
    {
        return super.getAllDomainInfos();
    }

    @Override
    public List<String> getAllDomainInfoKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllDomainInfoKeys(ordering, offset, count);
    }

    @Override
    public List<DomainInfo> findDomainInfos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findDomainInfos(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findDomainInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findDomainInfoKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createDomainInfo(DomainInfo domainInfo) throws BaseException
    {
        return super.createDomainInfo(domainInfo);
    }

    @Override
    public DomainInfo constructDomainInfo(DomainInfo domainInfo) throws BaseException
    {
        return super.constructDomainInfo(domainInfo);
    }


    @Override
    public Boolean updateDomainInfo(DomainInfo domainInfo) throws BaseException
    {
        return super.updateDomainInfo(domainInfo);
    }
        
    @Override
    public DomainInfo refreshDomainInfo(DomainInfo domainInfo) throws BaseException
    {
        return super.refreshDomainInfo(domainInfo);
    }

    @Override
    public Boolean deleteDomainInfo(String guid) throws BaseException
    {
        return super.deleteDomainInfo(guid);
    }

    @Override
    public Boolean deleteDomainInfo(DomainInfo domainInfo) throws BaseException
    {
        return super.deleteDomainInfo(domainInfo);
    }

    @Override
    public Integer createDomainInfos(List<DomainInfo> domainInfos) throws BaseException
    {
        return super.createDomainInfos(domainInfos);
    }

    // TBD
    //@Override
    //public Boolean updateDomainInfos(List<DomainInfo> domainInfos) throws BaseException
    //{
    //}

}
