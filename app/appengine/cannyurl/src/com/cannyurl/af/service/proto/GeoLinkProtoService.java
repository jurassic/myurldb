package com.cannyurl.af.service.proto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.CellLatitudeLongitude;
import com.myurldb.ws.GeoCoordinateStruct;
import com.myurldb.ws.GeoLink;
import com.cannyurl.af.bean.CellLatitudeLongitudeBean;
import com.cannyurl.af.bean.GeoCoordinateStructBean;
import com.cannyurl.af.bean.GeoLinkBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.GeoLinkService;
import com.cannyurl.af.service.impl.GeoLinkServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class GeoLinkProtoService extends GeoLinkServiceImpl implements GeoLinkService
{
    private static final Logger log = Logger.getLogger(GeoLinkProtoService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public GeoLinkProtoService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // GeoLink related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public GeoLink getGeoLink(String guid) throws BaseException
    {
        return super.getGeoLink(guid);
    }

    @Override
    public Object getGeoLink(String guid, String field) throws BaseException
    {
        return super.getGeoLink(guid, field);
    }

    @Override
    public List<GeoLink> getGeoLinks(List<String> guids) throws BaseException
    {
        return super.getGeoLinks(guids);
    }

    @Override
    public List<GeoLink> getAllGeoLinks() throws BaseException
    {
        return super.getAllGeoLinks();
    }

    @Override
    public List<String> getAllGeoLinkKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllGeoLinkKeys(ordering, offset, count);
    }

    @Override
    public List<GeoLink> findGeoLinks(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findGeoLinks(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findGeoLinkKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findGeoLinkKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createGeoLink(GeoLink geoLink) throws BaseException
    {
        return super.createGeoLink(geoLink);
    }

    @Override
    public GeoLink constructGeoLink(GeoLink geoLink) throws BaseException
    {
        return super.constructGeoLink(geoLink);
    }


    @Override
    public Boolean updateGeoLink(GeoLink geoLink) throws BaseException
    {
        return super.updateGeoLink(geoLink);
    }
        
    @Override
    public GeoLink refreshGeoLink(GeoLink geoLink) throws BaseException
    {
        return super.refreshGeoLink(geoLink);
    }

    @Override
    public Boolean deleteGeoLink(String guid) throws BaseException
    {
        return super.deleteGeoLink(guid);
    }

    @Override
    public Boolean deleteGeoLink(GeoLink geoLink) throws BaseException
    {
        return super.deleteGeoLink(geoLink);
    }

    @Override
    public Integer createGeoLinks(List<GeoLink> geoLinks) throws BaseException
    {
        return super.createGeoLinks(geoLinks);
    }

    // TBD
    //@Override
    //public Boolean updateGeoLinks(List<GeoLink> geoLinks) throws BaseException
    //{
    //}

}
