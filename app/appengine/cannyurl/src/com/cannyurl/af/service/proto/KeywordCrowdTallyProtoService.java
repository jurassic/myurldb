package com.cannyurl.af.service.proto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.KeywordCrowdTally;
import com.cannyurl.af.bean.KeywordCrowdTallyBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.KeywordCrowdTallyService;
import com.cannyurl.af.service.impl.KeywordCrowdTallyServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class KeywordCrowdTallyProtoService extends KeywordCrowdTallyServiceImpl implements KeywordCrowdTallyService
{
    private static final Logger log = Logger.getLogger(KeywordCrowdTallyProtoService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public KeywordCrowdTallyProtoService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // KeywordCrowdTally related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public KeywordCrowdTally getKeywordCrowdTally(String guid) throws BaseException
    {
        return super.getKeywordCrowdTally(guid);
    }

    @Override
    public Object getKeywordCrowdTally(String guid, String field) throws BaseException
    {
        return super.getKeywordCrowdTally(guid, field);
    }

    @Override
    public List<KeywordCrowdTally> getKeywordCrowdTallies(List<String> guids) throws BaseException
    {
        return super.getKeywordCrowdTallies(guids);
    }

    @Override
    public List<KeywordCrowdTally> getAllKeywordCrowdTallies() throws BaseException
    {
        return super.getAllKeywordCrowdTallies();
    }

    @Override
    public List<String> getAllKeywordCrowdTallyKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllKeywordCrowdTallyKeys(ordering, offset, count);
    }

    @Override
    public List<KeywordCrowdTally> findKeywordCrowdTallies(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findKeywordCrowdTallies(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findKeywordCrowdTallyKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findKeywordCrowdTallyKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createKeywordCrowdTally(KeywordCrowdTally keywordCrowdTally) throws BaseException
    {
        return super.createKeywordCrowdTally(keywordCrowdTally);
    }

    @Override
    public KeywordCrowdTally constructKeywordCrowdTally(KeywordCrowdTally keywordCrowdTally) throws BaseException
    {
        return super.constructKeywordCrowdTally(keywordCrowdTally);
    }


    @Override
    public Boolean updateKeywordCrowdTally(KeywordCrowdTally keywordCrowdTally) throws BaseException
    {
        return super.updateKeywordCrowdTally(keywordCrowdTally);
    }
        
    @Override
    public KeywordCrowdTally refreshKeywordCrowdTally(KeywordCrowdTally keywordCrowdTally) throws BaseException
    {
        return super.refreshKeywordCrowdTally(keywordCrowdTally);
    }

    @Override
    public Boolean deleteKeywordCrowdTally(String guid) throws BaseException
    {
        return super.deleteKeywordCrowdTally(guid);
    }

    @Override
    public Boolean deleteKeywordCrowdTally(KeywordCrowdTally keywordCrowdTally) throws BaseException
    {
        return super.deleteKeywordCrowdTally(keywordCrowdTally);
    }

    @Override
    public Integer createKeywordCrowdTallies(List<KeywordCrowdTally> keywordCrowdTallies) throws BaseException
    {
        return super.createKeywordCrowdTallies(keywordCrowdTallies);
    }

    // TBD
    //@Override
    //public Boolean updateKeywordCrowdTallies(List<KeywordCrowdTally> keywordCrowdTallies) throws BaseException
    //{
    //}

}
