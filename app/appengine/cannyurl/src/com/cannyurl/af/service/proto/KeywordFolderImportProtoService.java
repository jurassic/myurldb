package com.cannyurl.af.service.proto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.KeywordFolderImport;
import com.cannyurl.af.bean.KeywordFolderImportBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.KeywordFolderImportService;
import com.cannyurl.af.service.impl.KeywordFolderImportServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class KeywordFolderImportProtoService extends KeywordFolderImportServiceImpl implements KeywordFolderImportService
{
    private static final Logger log = Logger.getLogger(KeywordFolderImportProtoService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public KeywordFolderImportProtoService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // KeywordFolderImport related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public KeywordFolderImport getKeywordFolderImport(String guid) throws BaseException
    {
        return super.getKeywordFolderImport(guid);
    }

    @Override
    public Object getKeywordFolderImport(String guid, String field) throws BaseException
    {
        return super.getKeywordFolderImport(guid, field);
    }

    @Override
    public List<KeywordFolderImport> getKeywordFolderImports(List<String> guids) throws BaseException
    {
        return super.getKeywordFolderImports(guids);
    }

    @Override
    public List<KeywordFolderImport> getAllKeywordFolderImports() throws BaseException
    {
        return super.getAllKeywordFolderImports();
    }

    @Override
    public List<String> getAllKeywordFolderImportKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllKeywordFolderImportKeys(ordering, offset, count);
    }

    @Override
    public List<KeywordFolderImport> findKeywordFolderImports(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findKeywordFolderImports(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findKeywordFolderImportKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findKeywordFolderImportKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createKeywordFolderImport(KeywordFolderImport keywordFolderImport) throws BaseException
    {
        return super.createKeywordFolderImport(keywordFolderImport);
    }

    @Override
    public KeywordFolderImport constructKeywordFolderImport(KeywordFolderImport keywordFolderImport) throws BaseException
    {
        return super.constructKeywordFolderImport(keywordFolderImport);
    }


    @Override
    public Boolean updateKeywordFolderImport(KeywordFolderImport keywordFolderImport) throws BaseException
    {
        return super.updateKeywordFolderImport(keywordFolderImport);
    }
        
    @Override
    public KeywordFolderImport refreshKeywordFolderImport(KeywordFolderImport keywordFolderImport) throws BaseException
    {
        return super.refreshKeywordFolderImport(keywordFolderImport);
    }

    @Override
    public Boolean deleteKeywordFolderImport(String guid) throws BaseException
    {
        return super.deleteKeywordFolderImport(guid);
    }

    @Override
    public Boolean deleteKeywordFolderImport(KeywordFolderImport keywordFolderImport) throws BaseException
    {
        return super.deleteKeywordFolderImport(keywordFolderImport);
    }

    @Override
    public Integer createKeywordFolderImports(List<KeywordFolderImport> keywordFolderImports) throws BaseException
    {
        return super.createKeywordFolderImports(keywordFolderImports);
    }

    // TBD
    //@Override
    //public Boolean updateKeywordFolderImports(List<KeywordFolderImport> keywordFolderImports) throws BaseException
    //{
    //}

}
