package com.cannyurl.af.service.proto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.KeywordFolder;
import com.cannyurl.af.bean.KeywordFolderBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.KeywordFolderService;
import com.cannyurl.af.service.impl.KeywordFolderServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class KeywordFolderProtoService extends KeywordFolderServiceImpl implements KeywordFolderService
{
    private static final Logger log = Logger.getLogger(KeywordFolderProtoService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public KeywordFolderProtoService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // KeywordFolder related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public KeywordFolder getKeywordFolder(String guid) throws BaseException
    {
        return super.getKeywordFolder(guid);
    }

    @Override
    public Object getKeywordFolder(String guid, String field) throws BaseException
    {
        return super.getKeywordFolder(guid, field);
    }

    @Override
    public List<KeywordFolder> getKeywordFolders(List<String> guids) throws BaseException
    {
        return super.getKeywordFolders(guids);
    }

    @Override
    public List<KeywordFolder> getAllKeywordFolders() throws BaseException
    {
        return super.getAllKeywordFolders();
    }

    @Override
    public List<String> getAllKeywordFolderKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllKeywordFolderKeys(ordering, offset, count);
    }

    @Override
    public List<KeywordFolder> findKeywordFolders(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findKeywordFolders(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findKeywordFolderKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findKeywordFolderKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createKeywordFolder(KeywordFolder keywordFolder) throws BaseException
    {
        return super.createKeywordFolder(keywordFolder);
    }

    @Override
    public KeywordFolder constructKeywordFolder(KeywordFolder keywordFolder) throws BaseException
    {
        return super.constructKeywordFolder(keywordFolder);
    }


    @Override
    public Boolean updateKeywordFolder(KeywordFolder keywordFolder) throws BaseException
    {
        return super.updateKeywordFolder(keywordFolder);
    }
        
    @Override
    public KeywordFolder refreshKeywordFolder(KeywordFolder keywordFolder) throws BaseException
    {
        return super.refreshKeywordFolder(keywordFolder);
    }

    @Override
    public Boolean deleteKeywordFolder(String guid) throws BaseException
    {
        return super.deleteKeywordFolder(guid);
    }

    @Override
    public Boolean deleteKeywordFolder(KeywordFolder keywordFolder) throws BaseException
    {
        return super.deleteKeywordFolder(keywordFolder);
    }

    @Override
    public Integer createKeywordFolders(List<KeywordFolder> keywordFolders) throws BaseException
    {
        return super.createKeywordFolders(keywordFolders);
    }

    // TBD
    //@Override
    //public Boolean updateKeywordFolders(List<KeywordFolder> keywordFolders) throws BaseException
    //{
    //}

}
