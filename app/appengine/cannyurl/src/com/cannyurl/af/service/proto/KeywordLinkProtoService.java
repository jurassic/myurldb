package com.cannyurl.af.service.proto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.KeywordLink;
import com.cannyurl.af.bean.KeywordLinkBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.KeywordLinkService;
import com.cannyurl.af.service.impl.KeywordLinkServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class KeywordLinkProtoService extends KeywordLinkServiceImpl implements KeywordLinkService
{
    private static final Logger log = Logger.getLogger(KeywordLinkProtoService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public KeywordLinkProtoService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // KeywordLink related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public KeywordLink getKeywordLink(String guid) throws BaseException
    {
        return super.getKeywordLink(guid);
    }

    @Override
    public Object getKeywordLink(String guid, String field) throws BaseException
    {
        return super.getKeywordLink(guid, field);
    }

    @Override
    public List<KeywordLink> getKeywordLinks(List<String> guids) throws BaseException
    {
        return super.getKeywordLinks(guids);
    }

    @Override
    public List<KeywordLink> getAllKeywordLinks() throws BaseException
    {
        return super.getAllKeywordLinks();
    }

    @Override
    public List<String> getAllKeywordLinkKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllKeywordLinkKeys(ordering, offset, count);
    }

    @Override
    public List<KeywordLink> findKeywordLinks(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findKeywordLinks(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findKeywordLinkKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findKeywordLinkKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createKeywordLink(KeywordLink keywordLink) throws BaseException
    {
        return super.createKeywordLink(keywordLink);
    }

    @Override
    public KeywordLink constructKeywordLink(KeywordLink keywordLink) throws BaseException
    {
        return super.constructKeywordLink(keywordLink);
    }


    @Override
    public Boolean updateKeywordLink(KeywordLink keywordLink) throws BaseException
    {
        return super.updateKeywordLink(keywordLink);
    }
        
    @Override
    public KeywordLink refreshKeywordLink(KeywordLink keywordLink) throws BaseException
    {
        return super.refreshKeywordLink(keywordLink);
    }

    @Override
    public Boolean deleteKeywordLink(String guid) throws BaseException
    {
        return super.deleteKeywordLink(guid);
    }

    @Override
    public Boolean deleteKeywordLink(KeywordLink keywordLink) throws BaseException
    {
        return super.deleteKeywordLink(keywordLink);
    }

    @Override
    public Integer createKeywordLinks(List<KeywordLink> keywordLinks) throws BaseException
    {
        return super.createKeywordLinks(keywordLinks);
    }

    // TBD
    //@Override
    //public Boolean updateKeywordLinks(List<KeywordLink> keywordLinks) throws BaseException
    //{
    //}

}
