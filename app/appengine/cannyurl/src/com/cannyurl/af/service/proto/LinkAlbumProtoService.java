package com.cannyurl.af.service.proto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.LinkAlbum;
import com.cannyurl.af.bean.LinkAlbumBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.LinkAlbumService;
import com.cannyurl.af.service.impl.LinkAlbumServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class LinkAlbumProtoService extends LinkAlbumServiceImpl implements LinkAlbumService
{
    private static final Logger log = Logger.getLogger(LinkAlbumProtoService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public LinkAlbumProtoService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // LinkAlbum related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public LinkAlbum getLinkAlbum(String guid) throws BaseException
    {
        return super.getLinkAlbum(guid);
    }

    @Override
    public Object getLinkAlbum(String guid, String field) throws BaseException
    {
        return super.getLinkAlbum(guid, field);
    }

    @Override
    public List<LinkAlbum> getLinkAlbums(List<String> guids) throws BaseException
    {
        return super.getLinkAlbums(guids);
    }

    @Override
    public List<LinkAlbum> getAllLinkAlbums() throws BaseException
    {
        return super.getAllLinkAlbums();
    }

    @Override
    public List<String> getAllLinkAlbumKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllLinkAlbumKeys(ordering, offset, count);
    }

    @Override
    public List<LinkAlbum> findLinkAlbums(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findLinkAlbums(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findLinkAlbumKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findLinkAlbumKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createLinkAlbum(LinkAlbum linkAlbum) throws BaseException
    {
        return super.createLinkAlbum(linkAlbum);
    }

    @Override
    public LinkAlbum constructLinkAlbum(LinkAlbum linkAlbum) throws BaseException
    {
        return super.constructLinkAlbum(linkAlbum);
    }


    @Override
    public Boolean updateLinkAlbum(LinkAlbum linkAlbum) throws BaseException
    {
        return super.updateLinkAlbum(linkAlbum);
    }
        
    @Override
    public LinkAlbum refreshLinkAlbum(LinkAlbum linkAlbum) throws BaseException
    {
        return super.refreshLinkAlbum(linkAlbum);
    }

    @Override
    public Boolean deleteLinkAlbum(String guid) throws BaseException
    {
        return super.deleteLinkAlbum(guid);
    }

    @Override
    public Boolean deleteLinkAlbum(LinkAlbum linkAlbum) throws BaseException
    {
        return super.deleteLinkAlbum(linkAlbum);
    }

    @Override
    public Integer createLinkAlbums(List<LinkAlbum> linkAlbums) throws BaseException
    {
        return super.createLinkAlbums(linkAlbums);
    }

    // TBD
    //@Override
    //public Boolean updateLinkAlbums(List<LinkAlbum> linkAlbums) throws BaseException
    //{
    //}

}
