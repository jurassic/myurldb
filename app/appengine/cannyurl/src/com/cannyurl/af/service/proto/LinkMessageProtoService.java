package com.cannyurl.af.service.proto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.LinkMessage;
import com.cannyurl.af.bean.LinkMessageBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.LinkMessageService;
import com.cannyurl.af.service.impl.LinkMessageServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class LinkMessageProtoService extends LinkMessageServiceImpl implements LinkMessageService
{
    private static final Logger log = Logger.getLogger(LinkMessageProtoService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public LinkMessageProtoService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // LinkMessage related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public LinkMessage getLinkMessage(String guid) throws BaseException
    {
        return super.getLinkMessage(guid);
    }

    @Override
    public Object getLinkMessage(String guid, String field) throws BaseException
    {
        return super.getLinkMessage(guid, field);
    }

    @Override
    public List<LinkMessage> getLinkMessages(List<String> guids) throws BaseException
    {
        return super.getLinkMessages(guids);
    }

    @Override
    public List<LinkMessage> getAllLinkMessages() throws BaseException
    {
        return super.getAllLinkMessages();
    }

    @Override
    public List<String> getAllLinkMessageKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllLinkMessageKeys(ordering, offset, count);
    }

    @Override
    public List<LinkMessage> findLinkMessages(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findLinkMessages(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findLinkMessageKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findLinkMessageKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createLinkMessage(LinkMessage linkMessage) throws BaseException
    {
        return super.createLinkMessage(linkMessage);
    }

    @Override
    public LinkMessage constructLinkMessage(LinkMessage linkMessage) throws BaseException
    {
        return super.constructLinkMessage(linkMessage);
    }


    @Override
    public Boolean updateLinkMessage(LinkMessage linkMessage) throws BaseException
    {
        return super.updateLinkMessage(linkMessage);
    }
        
    @Override
    public LinkMessage refreshLinkMessage(LinkMessage linkMessage) throws BaseException
    {
        return super.refreshLinkMessage(linkMessage);
    }

    @Override
    public Boolean deleteLinkMessage(String guid) throws BaseException
    {
        return super.deleteLinkMessage(guid);
    }

    @Override
    public Boolean deleteLinkMessage(LinkMessage linkMessage) throws BaseException
    {
        return super.deleteLinkMessage(linkMessage);
    }

    @Override
    public Integer createLinkMessages(List<LinkMessage> linkMessages) throws BaseException
    {
        return super.createLinkMessages(linkMessages);
    }

    // TBD
    //@Override
    //public Boolean updateLinkMessages(List<LinkMessage> linkMessages) throws BaseException
    //{
    //}

}
