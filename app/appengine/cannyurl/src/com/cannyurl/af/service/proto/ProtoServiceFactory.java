package com.cannyurl.af.service.proto;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.cannyurl.af.service.AbstractServiceFactory;
import com.cannyurl.af.service.ApiConsumerService;
import com.cannyurl.af.service.AppCustomDomainService;
import com.cannyurl.af.service.SiteCustomDomainService;
import com.cannyurl.af.service.UserService;
import com.cannyurl.af.service.UserUsercodeService;
import com.cannyurl.af.service.UserPasswordService;
import com.cannyurl.af.service.ExternalUserAuthService;
import com.cannyurl.af.service.UserAuthStateService;
import com.cannyurl.af.service.UserResourcePermissionService;
import com.cannyurl.af.service.UserResourceProhibitionService;
import com.cannyurl.af.service.RolePermissionService;
import com.cannyurl.af.service.UserRoleService;
import com.cannyurl.af.service.AppClientService;
import com.cannyurl.af.service.ClientUserService;
import com.cannyurl.af.service.UserCustomDomainService;
import com.cannyurl.af.service.ClientSettingService;
import com.cannyurl.af.service.UserSettingService;
import com.cannyurl.af.service.VisitorSettingService;
import com.cannyurl.af.service.TwitterSummaryCardService;
import com.cannyurl.af.service.TwitterPhotoCardService;
import com.cannyurl.af.service.TwitterGalleryCardService;
import com.cannyurl.af.service.TwitterAppCardService;
import com.cannyurl.af.service.TwitterPlayerCardService;
import com.cannyurl.af.service.TwitterProductCardService;
import com.cannyurl.af.service.ShortPassageService;
import com.cannyurl.af.service.ShortLinkService;
import com.cannyurl.af.service.GeoLinkService;
import com.cannyurl.af.service.QrCodeService;
import com.cannyurl.af.service.LinkPassphraseService;
import com.cannyurl.af.service.LinkMessageService;
import com.cannyurl.af.service.LinkAlbumService;
import com.cannyurl.af.service.AlbumShortLinkService;
import com.cannyurl.af.service.KeywordFolderService;
import com.cannyurl.af.service.BookmarkFolderService;
import com.cannyurl.af.service.KeywordLinkService;
import com.cannyurl.af.service.BookmarkLinkService;
import com.cannyurl.af.service.SpeedDialService;
import com.cannyurl.af.service.KeywordFolderImportService;
import com.cannyurl.af.service.BookmarkFolderImportService;
import com.cannyurl.af.service.KeywordCrowdTallyService;
import com.cannyurl.af.service.BookmarkCrowdTallyService;
import com.cannyurl.af.service.DomainInfoService;
import com.cannyurl.af.service.UrlRatingService;
import com.cannyurl.af.service.UserRatingService;
import com.cannyurl.af.service.AbuseTagService;
import com.cannyurl.af.service.ServiceInfoService;
import com.cannyurl.af.service.FiveTenService;

public class ProtoServiceFactory extends AbstractServiceFactory
{
    private static final Logger log = Logger.getLogger(ProtoServiceFactory.class.getName());

    private ProtoServiceFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class ProtoServiceFactoryHolder
    {
        private static final ProtoServiceFactory INSTANCE = new ProtoServiceFactory();
    }

    // Singleton method
    public static ProtoServiceFactory getInstance()
    {
        return ProtoServiceFactoryHolder.INSTANCE;
    }

    @Override
    public ApiConsumerService getApiConsumerService()
    {
        return new ApiConsumerProtoService();
    }

    @Override
    public AppCustomDomainService getAppCustomDomainService()
    {
        return new AppCustomDomainProtoService();
    }

    @Override
    public SiteCustomDomainService getSiteCustomDomainService()
    {
        return new SiteCustomDomainProtoService();
    }

    @Override
    public UserService getUserService()
    {
        return new UserProtoService();
    }

    @Override
    public UserUsercodeService getUserUsercodeService()
    {
        return new UserUsercodeProtoService();
    }

    @Override
    public UserPasswordService getUserPasswordService()
    {
        return new UserPasswordProtoService();
    }

    @Override
    public ExternalUserAuthService getExternalUserAuthService()
    {
        return new ExternalUserAuthProtoService();
    }

    @Override
    public UserAuthStateService getUserAuthStateService()
    {
        return new UserAuthStateProtoService();
    }

    @Override
    public UserResourcePermissionService getUserResourcePermissionService()
    {
        return new UserResourcePermissionProtoService();
    }

    @Override
    public UserResourceProhibitionService getUserResourceProhibitionService()
    {
        return new UserResourceProhibitionProtoService();
    }

    @Override
    public RolePermissionService getRolePermissionService()
    {
        return new RolePermissionProtoService();
    }

    @Override
    public UserRoleService getUserRoleService()
    {
        return new UserRoleProtoService();
    }

    @Override
    public AppClientService getAppClientService()
    {
        return new AppClientProtoService();
    }

    @Override
    public ClientUserService getClientUserService()
    {
        return new ClientUserProtoService();
    }

    @Override
    public UserCustomDomainService getUserCustomDomainService()
    {
        return new UserCustomDomainProtoService();
    }

    @Override
    public ClientSettingService getClientSettingService()
    {
        return new ClientSettingProtoService();
    }

    @Override
    public UserSettingService getUserSettingService()
    {
        return new UserSettingProtoService();
    }

    @Override
    public VisitorSettingService getVisitorSettingService()
    {
        return new VisitorSettingProtoService();
    }

    @Override
    public TwitterSummaryCardService getTwitterSummaryCardService()
    {
        return new TwitterSummaryCardProtoService();
    }

    @Override
    public TwitterPhotoCardService getTwitterPhotoCardService()
    {
        return new TwitterPhotoCardProtoService();
    }

    @Override
    public TwitterGalleryCardService getTwitterGalleryCardService()
    {
        return new TwitterGalleryCardProtoService();
    }

    @Override
    public TwitterAppCardService getTwitterAppCardService()
    {
        return new TwitterAppCardProtoService();
    }

    @Override
    public TwitterPlayerCardService getTwitterPlayerCardService()
    {
        return new TwitterPlayerCardProtoService();
    }

    @Override
    public TwitterProductCardService getTwitterProductCardService()
    {
        return new TwitterProductCardProtoService();
    }

    @Override
    public ShortPassageService getShortPassageService()
    {
        return new ShortPassageProtoService();
    }

    @Override
    public ShortLinkService getShortLinkService()
    {
        return new ShortLinkProtoService();
    }

    @Override
    public GeoLinkService getGeoLinkService()
    {
        return new GeoLinkProtoService();
    }

    @Override
    public QrCodeService getQrCodeService()
    {
        return new QrCodeProtoService();
    }

    @Override
    public LinkPassphraseService getLinkPassphraseService()
    {
        return new LinkPassphraseProtoService();
    }

    @Override
    public LinkMessageService getLinkMessageService()
    {
        return new LinkMessageProtoService();
    }

    @Override
    public LinkAlbumService getLinkAlbumService()
    {
        return new LinkAlbumProtoService();
    }

    @Override
    public AlbumShortLinkService getAlbumShortLinkService()
    {
        return new AlbumShortLinkProtoService();
    }

    @Override
    public KeywordFolderService getKeywordFolderService()
    {
        return new KeywordFolderProtoService();
    }

    @Override
    public BookmarkFolderService getBookmarkFolderService()
    {
        return new BookmarkFolderProtoService();
    }

    @Override
    public KeywordLinkService getKeywordLinkService()
    {
        return new KeywordLinkProtoService();
    }

    @Override
    public BookmarkLinkService getBookmarkLinkService()
    {
        return new BookmarkLinkProtoService();
    }

    @Override
    public SpeedDialService getSpeedDialService()
    {
        return new SpeedDialProtoService();
    }

    @Override
    public KeywordFolderImportService getKeywordFolderImportService()
    {
        return new KeywordFolderImportProtoService();
    }

    @Override
    public BookmarkFolderImportService getBookmarkFolderImportService()
    {
        return new BookmarkFolderImportProtoService();
    }

    @Override
    public KeywordCrowdTallyService getKeywordCrowdTallyService()
    {
        return new KeywordCrowdTallyProtoService();
    }

    @Override
    public BookmarkCrowdTallyService getBookmarkCrowdTallyService()
    {
        return new BookmarkCrowdTallyProtoService();
    }

    @Override
    public DomainInfoService getDomainInfoService()
    {
        return new DomainInfoProtoService();
    }

    @Override
    public UrlRatingService getUrlRatingService()
    {
        return new UrlRatingProtoService();
    }

    @Override
    public UserRatingService getUserRatingService()
    {
        return new UserRatingProtoService();
    }

    @Override
    public AbuseTagService getAbuseTagService()
    {
        return new AbuseTagProtoService();
    }

    @Override
    public ServiceInfoService getServiceInfoService()
    {
        return new ServiceInfoProtoService();
    }

    @Override
    public FiveTenService getFiveTenService()
    {
        return new FiveTenProtoService();
    }


}
