package com.cannyurl.af.service.proto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.QrCode;
import com.cannyurl.af.bean.QrCodeBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.QrCodeService;
import com.cannyurl.af.service.impl.QrCodeServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class QrCodeProtoService extends QrCodeServiceImpl implements QrCodeService
{
    private static final Logger log = Logger.getLogger(QrCodeProtoService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public QrCodeProtoService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // QrCode related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public QrCode getQrCode(String guid) throws BaseException
    {
        return super.getQrCode(guid);
    }

    @Override
    public Object getQrCode(String guid, String field) throws BaseException
    {
        return super.getQrCode(guid, field);
    }

    @Override
    public List<QrCode> getQrCodes(List<String> guids) throws BaseException
    {
        return super.getQrCodes(guids);
    }

    @Override
    public List<QrCode> getAllQrCodes() throws BaseException
    {
        return super.getAllQrCodes();
    }

    @Override
    public List<String> getAllQrCodeKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllQrCodeKeys(ordering, offset, count);
    }

    @Override
    public List<QrCode> findQrCodes(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findQrCodes(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findQrCodeKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findQrCodeKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createQrCode(QrCode qrCode) throws BaseException
    {
        return super.createQrCode(qrCode);
    }

    @Override
    public QrCode constructQrCode(QrCode qrCode) throws BaseException
    {
        return super.constructQrCode(qrCode);
    }


    @Override
    public Boolean updateQrCode(QrCode qrCode) throws BaseException
    {
        return super.updateQrCode(qrCode);
    }
        
    @Override
    public QrCode refreshQrCode(QrCode qrCode) throws BaseException
    {
        return super.refreshQrCode(qrCode);
    }

    @Override
    public Boolean deleteQrCode(String guid) throws BaseException
    {
        return super.deleteQrCode(guid);
    }

    @Override
    public Boolean deleteQrCode(QrCode qrCode) throws BaseException
    {
        return super.deleteQrCode(qrCode);
    }

    @Override
    public Integer createQrCodes(List<QrCode> qrCodes) throws BaseException
    {
        return super.createQrCodes(qrCodes);
    }

    // TBD
    //@Override
    //public Boolean updateQrCodes(List<QrCode> qrCodes) throws BaseException
    //{
    //}

}
