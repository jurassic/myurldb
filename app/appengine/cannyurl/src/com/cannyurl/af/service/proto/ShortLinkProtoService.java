package com.cannyurl.af.service.proto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.ReferrerInfoStruct;
import com.myurldb.ws.ShortLink;
import com.cannyurl.af.bean.ReferrerInfoStructBean;
import com.cannyurl.af.bean.ShortLinkBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.ShortLinkService;
import com.cannyurl.af.service.impl.ShortLinkServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class ShortLinkProtoService extends ShortLinkServiceImpl implements ShortLinkService
{
    private static final Logger log = Logger.getLogger(ShortLinkProtoService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public ShortLinkProtoService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // ShortLink related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public ShortLink getShortLink(String guid) throws BaseException
    {
        return super.getShortLink(guid);
    }

    @Override
    public Object getShortLink(String guid, String field) throws BaseException
    {
        return super.getShortLink(guid, field);
    }

    @Override
    public List<ShortLink> getShortLinks(List<String> guids) throws BaseException
    {
        return super.getShortLinks(guids);
    }

    @Override
    public List<ShortLink> getAllShortLinks() throws BaseException
    {
        return super.getAllShortLinks();
    }

    @Override
    public List<String> getAllShortLinkKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllShortLinkKeys(ordering, offset, count);
    }

    @Override
    public List<ShortLink> findShortLinks(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findShortLinks(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findShortLinkKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findShortLinkKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createShortLink(ShortLink shortLink) throws BaseException
    {
        return super.createShortLink(shortLink);
    }

    @Override
    public ShortLink constructShortLink(ShortLink shortLink) throws BaseException
    {
        return super.constructShortLink(shortLink);
    }


    @Override
    public Boolean updateShortLink(ShortLink shortLink) throws BaseException
    {
        return super.updateShortLink(shortLink);
    }
        
    @Override
    public ShortLink refreshShortLink(ShortLink shortLink) throws BaseException
    {
        return super.refreshShortLink(shortLink);
    }

    @Override
    public Boolean deleteShortLink(String guid) throws BaseException
    {
        return super.deleteShortLink(guid);
    }

    @Override
    public Boolean deleteShortLink(ShortLink shortLink) throws BaseException
    {
        return super.deleteShortLink(shortLink);
    }

    @Override
    public Integer createShortLinks(List<ShortLink> shortLinks) throws BaseException
    {
        return super.createShortLinks(shortLinks);
    }

    // TBD
    //@Override
    //public Boolean updateShortLinks(List<ShortLink> shortLinks) throws BaseException
    //{
    //}

}
