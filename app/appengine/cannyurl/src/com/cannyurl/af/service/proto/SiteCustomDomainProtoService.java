package com.cannyurl.af.service.proto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.SiteCustomDomain;
import com.cannyurl.af.bean.SiteCustomDomainBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.SiteCustomDomainService;
import com.cannyurl.af.service.impl.SiteCustomDomainServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class SiteCustomDomainProtoService extends SiteCustomDomainServiceImpl implements SiteCustomDomainService
{
    private static final Logger log = Logger.getLogger(SiteCustomDomainProtoService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public SiteCustomDomainProtoService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // SiteCustomDomain related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public SiteCustomDomain getSiteCustomDomain(String guid) throws BaseException
    {
        return super.getSiteCustomDomain(guid);
    }

    @Override
    public Object getSiteCustomDomain(String guid, String field) throws BaseException
    {
        return super.getSiteCustomDomain(guid, field);
    }

    @Override
    public List<SiteCustomDomain> getSiteCustomDomains(List<String> guids) throws BaseException
    {
        return super.getSiteCustomDomains(guids);
    }

    @Override
    public List<SiteCustomDomain> getAllSiteCustomDomains() throws BaseException
    {
        return super.getAllSiteCustomDomains();
    }

    @Override
    public List<String> getAllSiteCustomDomainKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllSiteCustomDomainKeys(ordering, offset, count);
    }

    @Override
    public List<SiteCustomDomain> findSiteCustomDomains(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findSiteCustomDomains(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findSiteCustomDomainKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findSiteCustomDomainKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createSiteCustomDomain(SiteCustomDomain siteCustomDomain) throws BaseException
    {
        return super.createSiteCustomDomain(siteCustomDomain);
    }

    @Override
    public SiteCustomDomain constructSiteCustomDomain(SiteCustomDomain siteCustomDomain) throws BaseException
    {
        return super.constructSiteCustomDomain(siteCustomDomain);
    }


    @Override
    public Boolean updateSiteCustomDomain(SiteCustomDomain siteCustomDomain) throws BaseException
    {
        return super.updateSiteCustomDomain(siteCustomDomain);
    }
        
    @Override
    public SiteCustomDomain refreshSiteCustomDomain(SiteCustomDomain siteCustomDomain) throws BaseException
    {
        return super.refreshSiteCustomDomain(siteCustomDomain);
    }

    @Override
    public Boolean deleteSiteCustomDomain(String guid) throws BaseException
    {
        return super.deleteSiteCustomDomain(guid);
    }

    @Override
    public Boolean deleteSiteCustomDomain(SiteCustomDomain siteCustomDomain) throws BaseException
    {
        return super.deleteSiteCustomDomain(siteCustomDomain);
    }

    @Override
    public Integer createSiteCustomDomains(List<SiteCustomDomain> siteCustomDomains) throws BaseException
    {
        return super.createSiteCustomDomains(siteCustomDomains);
    }

    // TBD
    //@Override
    //public Boolean updateSiteCustomDomains(List<SiteCustomDomain> siteCustomDomains) throws BaseException
    //{
    //}

}
