package com.cannyurl.af.service.proto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.TwitterAppCard;
import com.cannyurl.af.bean.TwitterCardAppInfoBean;
import com.cannyurl.af.bean.TwitterCardProductDataBean;
import com.cannyurl.af.bean.TwitterAppCardBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.TwitterAppCardService;
import com.cannyurl.af.service.impl.TwitterAppCardServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class TwitterAppCardProtoService extends TwitterAppCardServiceImpl implements TwitterAppCardService
{
    private static final Logger log = Logger.getLogger(TwitterAppCardProtoService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public TwitterAppCardProtoService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // TwitterAppCard related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public TwitterAppCard getTwitterAppCard(String guid) throws BaseException
    {
        return super.getTwitterAppCard(guid);
    }

    @Override
    public Object getTwitterAppCard(String guid, String field) throws BaseException
    {
        return super.getTwitterAppCard(guid, field);
    }

    @Override
    public List<TwitterAppCard> getTwitterAppCards(List<String> guids) throws BaseException
    {
        return super.getTwitterAppCards(guids);
    }

    @Override
    public List<TwitterAppCard> getAllTwitterAppCards() throws BaseException
    {
        return super.getAllTwitterAppCards();
    }

    @Override
    public List<String> getAllTwitterAppCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllTwitterAppCardKeys(ordering, offset, count);
    }

    @Override
    public List<TwitterAppCard> findTwitterAppCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findTwitterAppCards(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findTwitterAppCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findTwitterAppCardKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createTwitterAppCard(TwitterAppCard twitterAppCard) throws BaseException
    {
        return super.createTwitterAppCard(twitterAppCard);
    }

    @Override
    public TwitterAppCard constructTwitterAppCard(TwitterAppCard twitterAppCard) throws BaseException
    {
        return super.constructTwitterAppCard(twitterAppCard);
    }


    @Override
    public Boolean updateTwitterAppCard(TwitterAppCard twitterAppCard) throws BaseException
    {
        return super.updateTwitterAppCard(twitterAppCard);
    }
        
    @Override
    public TwitterAppCard refreshTwitterAppCard(TwitterAppCard twitterAppCard) throws BaseException
    {
        return super.refreshTwitterAppCard(twitterAppCard);
    }

    @Override
    public Boolean deleteTwitterAppCard(String guid) throws BaseException
    {
        return super.deleteTwitterAppCard(guid);
    }

    @Override
    public Boolean deleteTwitterAppCard(TwitterAppCard twitterAppCard) throws BaseException
    {
        return super.deleteTwitterAppCard(twitterAppCard);
    }

    @Override
    public Integer createTwitterAppCards(List<TwitterAppCard> twitterAppCards) throws BaseException
    {
        return super.createTwitterAppCards(twitterAppCards);
    }

    // TBD
    //@Override
    //public Boolean updateTwitterAppCards(List<TwitterAppCard> twitterAppCards) throws BaseException
    //{
    //}

}
