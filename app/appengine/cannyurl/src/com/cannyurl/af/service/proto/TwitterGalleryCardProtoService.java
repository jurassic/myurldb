package com.cannyurl.af.service.proto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.TwitterGalleryCard;
import com.cannyurl.af.bean.TwitterCardAppInfoBean;
import com.cannyurl.af.bean.TwitterCardProductDataBean;
import com.cannyurl.af.bean.TwitterGalleryCardBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.TwitterGalleryCardService;
import com.cannyurl.af.service.impl.TwitterGalleryCardServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class TwitterGalleryCardProtoService extends TwitterGalleryCardServiceImpl implements TwitterGalleryCardService
{
    private static final Logger log = Logger.getLogger(TwitterGalleryCardProtoService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public TwitterGalleryCardProtoService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // TwitterGalleryCard related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public TwitterGalleryCard getTwitterGalleryCard(String guid) throws BaseException
    {
        return super.getTwitterGalleryCard(guid);
    }

    @Override
    public Object getTwitterGalleryCard(String guid, String field) throws BaseException
    {
        return super.getTwitterGalleryCard(guid, field);
    }

    @Override
    public List<TwitterGalleryCard> getTwitterGalleryCards(List<String> guids) throws BaseException
    {
        return super.getTwitterGalleryCards(guids);
    }

    @Override
    public List<TwitterGalleryCard> getAllTwitterGalleryCards() throws BaseException
    {
        return super.getAllTwitterGalleryCards();
    }

    @Override
    public List<String> getAllTwitterGalleryCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllTwitterGalleryCardKeys(ordering, offset, count);
    }

    @Override
    public List<TwitterGalleryCard> findTwitterGalleryCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findTwitterGalleryCards(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findTwitterGalleryCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findTwitterGalleryCardKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createTwitterGalleryCard(TwitterGalleryCard twitterGalleryCard) throws BaseException
    {
        return super.createTwitterGalleryCard(twitterGalleryCard);
    }

    @Override
    public TwitterGalleryCard constructTwitterGalleryCard(TwitterGalleryCard twitterGalleryCard) throws BaseException
    {
        return super.constructTwitterGalleryCard(twitterGalleryCard);
    }


    @Override
    public Boolean updateTwitterGalleryCard(TwitterGalleryCard twitterGalleryCard) throws BaseException
    {
        return super.updateTwitterGalleryCard(twitterGalleryCard);
    }
        
    @Override
    public TwitterGalleryCard refreshTwitterGalleryCard(TwitterGalleryCard twitterGalleryCard) throws BaseException
    {
        return super.refreshTwitterGalleryCard(twitterGalleryCard);
    }

    @Override
    public Boolean deleteTwitterGalleryCard(String guid) throws BaseException
    {
        return super.deleteTwitterGalleryCard(guid);
    }

    @Override
    public Boolean deleteTwitterGalleryCard(TwitterGalleryCard twitterGalleryCard) throws BaseException
    {
        return super.deleteTwitterGalleryCard(twitterGalleryCard);
    }

    @Override
    public Integer createTwitterGalleryCards(List<TwitterGalleryCard> twitterGalleryCards) throws BaseException
    {
        return super.createTwitterGalleryCards(twitterGalleryCards);
    }

    // TBD
    //@Override
    //public Boolean updateTwitterGalleryCards(List<TwitterGalleryCard> twitterGalleryCards) throws BaseException
    //{
    //}

}
