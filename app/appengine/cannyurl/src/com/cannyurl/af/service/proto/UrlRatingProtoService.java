package com.cannyurl.af.service.proto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.UrlRating;
import com.cannyurl.af.bean.UrlRatingBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.UrlRatingService;
import com.cannyurl.af.service.impl.UrlRatingServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class UrlRatingProtoService extends UrlRatingServiceImpl implements UrlRatingService
{
    private static final Logger log = Logger.getLogger(UrlRatingProtoService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public UrlRatingProtoService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // UrlRating related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public UrlRating getUrlRating(String guid) throws BaseException
    {
        return super.getUrlRating(guid);
    }

    @Override
    public Object getUrlRating(String guid, String field) throws BaseException
    {
        return super.getUrlRating(guid, field);
    }

    @Override
    public List<UrlRating> getUrlRatings(List<String> guids) throws BaseException
    {
        return super.getUrlRatings(guids);
    }

    @Override
    public List<UrlRating> getAllUrlRatings() throws BaseException
    {
        return super.getAllUrlRatings();
    }

    @Override
    public List<String> getAllUrlRatingKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllUrlRatingKeys(ordering, offset, count);
    }

    @Override
    public List<UrlRating> findUrlRatings(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findUrlRatings(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findUrlRatingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findUrlRatingKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createUrlRating(UrlRating urlRating) throws BaseException
    {
        return super.createUrlRating(urlRating);
    }

    @Override
    public UrlRating constructUrlRating(UrlRating urlRating) throws BaseException
    {
        return super.constructUrlRating(urlRating);
    }


    @Override
    public Boolean updateUrlRating(UrlRating urlRating) throws BaseException
    {
        return super.updateUrlRating(urlRating);
    }
        
    @Override
    public UrlRating refreshUrlRating(UrlRating urlRating) throws BaseException
    {
        return super.refreshUrlRating(urlRating);
    }

    @Override
    public Boolean deleteUrlRating(String guid) throws BaseException
    {
        return super.deleteUrlRating(guid);
    }

    @Override
    public Boolean deleteUrlRating(UrlRating urlRating) throws BaseException
    {
        return super.deleteUrlRating(urlRating);
    }

    @Override
    public Integer createUrlRatings(List<UrlRating> urlRatings) throws BaseException
    {
        return super.createUrlRatings(urlRatings);
    }

    // TBD
    //@Override
    //public Boolean updateUrlRatings(List<UrlRating> urlRatings) throws BaseException
    //{
    //}

}
