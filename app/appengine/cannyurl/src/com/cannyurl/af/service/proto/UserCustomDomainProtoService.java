package com.cannyurl.af.service.proto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.UserCustomDomain;
import com.cannyurl.af.bean.UserCustomDomainBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.UserCustomDomainService;
import com.cannyurl.af.service.impl.UserCustomDomainServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class UserCustomDomainProtoService extends UserCustomDomainServiceImpl implements UserCustomDomainService
{
    private static final Logger log = Logger.getLogger(UserCustomDomainProtoService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public UserCustomDomainProtoService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // UserCustomDomain related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public UserCustomDomain getUserCustomDomain(String guid) throws BaseException
    {
        return super.getUserCustomDomain(guid);
    }

    @Override
    public Object getUserCustomDomain(String guid, String field) throws BaseException
    {
        return super.getUserCustomDomain(guid, field);
    }

    @Override
    public List<UserCustomDomain> getUserCustomDomains(List<String> guids) throws BaseException
    {
        return super.getUserCustomDomains(guids);
    }

    @Override
    public List<UserCustomDomain> getAllUserCustomDomains() throws BaseException
    {
        return super.getAllUserCustomDomains();
    }

    @Override
    public List<String> getAllUserCustomDomainKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllUserCustomDomainKeys(ordering, offset, count);
    }

    @Override
    public List<UserCustomDomain> findUserCustomDomains(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findUserCustomDomains(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findUserCustomDomainKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findUserCustomDomainKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createUserCustomDomain(UserCustomDomain userCustomDomain) throws BaseException
    {
        return super.createUserCustomDomain(userCustomDomain);
    }

    @Override
    public UserCustomDomain constructUserCustomDomain(UserCustomDomain userCustomDomain) throws BaseException
    {
        return super.constructUserCustomDomain(userCustomDomain);
    }


    @Override
    public Boolean updateUserCustomDomain(UserCustomDomain userCustomDomain) throws BaseException
    {
        return super.updateUserCustomDomain(userCustomDomain);
    }
        
    @Override
    public UserCustomDomain refreshUserCustomDomain(UserCustomDomain userCustomDomain) throws BaseException
    {
        return super.refreshUserCustomDomain(userCustomDomain);
    }

    @Override
    public Boolean deleteUserCustomDomain(String guid) throws BaseException
    {
        return super.deleteUserCustomDomain(guid);
    }

    @Override
    public Boolean deleteUserCustomDomain(UserCustomDomain userCustomDomain) throws BaseException
    {
        return super.deleteUserCustomDomain(userCustomDomain);
    }

    @Override
    public Integer createUserCustomDomains(List<UserCustomDomain> userCustomDomains) throws BaseException
    {
        return super.createUserCustomDomains(userCustomDomains);
    }

    // TBD
    //@Override
    //public Boolean updateUserCustomDomains(List<UserCustomDomain> userCustomDomains) throws BaseException
    //{
    //}

}
