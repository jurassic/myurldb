package com.cannyurl.af.service.proto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.UserResourcePermission;
import com.cannyurl.af.bean.UserResourcePermissionBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.UserResourcePermissionService;
import com.cannyurl.af.service.impl.UserResourcePermissionServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class UserResourcePermissionProtoService extends UserResourcePermissionServiceImpl implements UserResourcePermissionService
{
    private static final Logger log = Logger.getLogger(UserResourcePermissionProtoService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public UserResourcePermissionProtoService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // UserResourcePermission related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public UserResourcePermission getUserResourcePermission(String guid) throws BaseException
    {
        return super.getUserResourcePermission(guid);
    }

    @Override
    public Object getUserResourcePermission(String guid, String field) throws BaseException
    {
        return super.getUserResourcePermission(guid, field);
    }

    @Override
    public List<UserResourcePermission> getUserResourcePermissions(List<String> guids) throws BaseException
    {
        return super.getUserResourcePermissions(guids);
    }

    @Override
    public List<UserResourcePermission> getAllUserResourcePermissions() throws BaseException
    {
        return super.getAllUserResourcePermissions();
    }

    @Override
    public List<String> getAllUserResourcePermissionKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllUserResourcePermissionKeys(ordering, offset, count);
    }

    @Override
    public List<UserResourcePermission> findUserResourcePermissions(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findUserResourcePermissions(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findUserResourcePermissionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findUserResourcePermissionKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createUserResourcePermission(UserResourcePermission userResourcePermission) throws BaseException
    {
        return super.createUserResourcePermission(userResourcePermission);
    }

    @Override
    public UserResourcePermission constructUserResourcePermission(UserResourcePermission userResourcePermission) throws BaseException
    {
        return super.constructUserResourcePermission(userResourcePermission);
    }


    @Override
    public Boolean updateUserResourcePermission(UserResourcePermission userResourcePermission) throws BaseException
    {
        return super.updateUserResourcePermission(userResourcePermission);
    }
        
    @Override
    public UserResourcePermission refreshUserResourcePermission(UserResourcePermission userResourcePermission) throws BaseException
    {
        return super.refreshUserResourcePermission(userResourcePermission);
    }

    @Override
    public Boolean deleteUserResourcePermission(String guid) throws BaseException
    {
        return super.deleteUserResourcePermission(guid);
    }

    @Override
    public Boolean deleteUserResourcePermission(UserResourcePermission userResourcePermission) throws BaseException
    {
        return super.deleteUserResourcePermission(userResourcePermission);
    }

    @Override
    public Integer createUserResourcePermissions(List<UserResourcePermission> userResourcePermissions) throws BaseException
    {
        return super.createUserResourcePermissions(userResourcePermissions);
    }

    // TBD
    //@Override
    //public Boolean updateUserResourcePermissions(List<UserResourcePermission> userResourcePermissions) throws BaseException
    //{
    //}

}
