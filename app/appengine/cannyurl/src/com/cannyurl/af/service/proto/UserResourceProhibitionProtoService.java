package com.cannyurl.af.service.proto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.UserResourceProhibition;
import com.cannyurl.af.bean.UserResourceProhibitionBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.UserResourceProhibitionService;
import com.cannyurl.af.service.impl.UserResourceProhibitionServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class UserResourceProhibitionProtoService extends UserResourceProhibitionServiceImpl implements UserResourceProhibitionService
{
    private static final Logger log = Logger.getLogger(UserResourceProhibitionProtoService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public UserResourceProhibitionProtoService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // UserResourceProhibition related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public UserResourceProhibition getUserResourceProhibition(String guid) throws BaseException
    {
        return super.getUserResourceProhibition(guid);
    }

    @Override
    public Object getUserResourceProhibition(String guid, String field) throws BaseException
    {
        return super.getUserResourceProhibition(guid, field);
    }

    @Override
    public List<UserResourceProhibition> getUserResourceProhibitions(List<String> guids) throws BaseException
    {
        return super.getUserResourceProhibitions(guids);
    }

    @Override
    public List<UserResourceProhibition> getAllUserResourceProhibitions() throws BaseException
    {
        return super.getAllUserResourceProhibitions();
    }

    @Override
    public List<String> getAllUserResourceProhibitionKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllUserResourceProhibitionKeys(ordering, offset, count);
    }

    @Override
    public List<UserResourceProhibition> findUserResourceProhibitions(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findUserResourceProhibitions(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findUserResourceProhibitionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findUserResourceProhibitionKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createUserResourceProhibition(UserResourceProhibition userResourceProhibition) throws BaseException
    {
        return super.createUserResourceProhibition(userResourceProhibition);
    }

    @Override
    public UserResourceProhibition constructUserResourceProhibition(UserResourceProhibition userResourceProhibition) throws BaseException
    {
        return super.constructUserResourceProhibition(userResourceProhibition);
    }


    @Override
    public Boolean updateUserResourceProhibition(UserResourceProhibition userResourceProhibition) throws BaseException
    {
        return super.updateUserResourceProhibition(userResourceProhibition);
    }
        
    @Override
    public UserResourceProhibition refreshUserResourceProhibition(UserResourceProhibition userResourceProhibition) throws BaseException
    {
        return super.refreshUserResourceProhibition(userResourceProhibition);
    }

    @Override
    public Boolean deleteUserResourceProhibition(String guid) throws BaseException
    {
        return super.deleteUserResourceProhibition(guid);
    }

    @Override
    public Boolean deleteUserResourceProhibition(UserResourceProhibition userResourceProhibition) throws BaseException
    {
        return super.deleteUserResourceProhibition(userResourceProhibition);
    }

    @Override
    public Integer createUserResourceProhibitions(List<UserResourceProhibition> userResourceProhibitions) throws BaseException
    {
        return super.createUserResourceProhibitions(userResourceProhibitions);
    }

    // TBD
    //@Override
    //public Boolean updateUserResourceProhibitions(List<UserResourceProhibition> userResourceProhibitions) throws BaseException
    //{
    //}

}
