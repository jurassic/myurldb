package com.cannyurl.af.service.proto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.UserRole;
import com.cannyurl.af.bean.UserRoleBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.UserRoleService;
import com.cannyurl.af.service.impl.UserRoleServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class UserRoleProtoService extends UserRoleServiceImpl implements UserRoleService
{
    private static final Logger log = Logger.getLogger(UserRoleProtoService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public UserRoleProtoService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // UserRole related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public UserRole getUserRole(String guid) throws BaseException
    {
        return super.getUserRole(guid);
    }

    @Override
    public Object getUserRole(String guid, String field) throws BaseException
    {
        return super.getUserRole(guid, field);
    }

    @Override
    public List<UserRole> getUserRoles(List<String> guids) throws BaseException
    {
        return super.getUserRoles(guids);
    }

    @Override
    public List<UserRole> getAllUserRoles() throws BaseException
    {
        return super.getAllUserRoles();
    }

    @Override
    public List<String> getAllUserRoleKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllUserRoleKeys(ordering, offset, count);
    }

    @Override
    public List<UserRole> findUserRoles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findUserRoles(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findUserRoleKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findUserRoleKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createUserRole(UserRole userRole) throws BaseException
    {
        return super.createUserRole(userRole);
    }

    @Override
    public UserRole constructUserRole(UserRole userRole) throws BaseException
    {
        return super.constructUserRole(userRole);
    }


    @Override
    public Boolean updateUserRole(UserRole userRole) throws BaseException
    {
        return super.updateUserRole(userRole);
    }
        
    @Override
    public UserRole refreshUserRole(UserRole userRole) throws BaseException
    {
        return super.refreshUserRole(userRole);
    }

    @Override
    public Boolean deleteUserRole(String guid) throws BaseException
    {
        return super.deleteUserRole(guid);
    }

    @Override
    public Boolean deleteUserRole(UserRole userRole) throws BaseException
    {
        return super.deleteUserRole(userRole);
    }

    @Override
    public Integer createUserRoles(List<UserRole> userRoles) throws BaseException
    {
        return super.createUserRoles(userRoles);
    }

    // TBD
    //@Override
    //public Boolean updateUserRoles(List<UserRole> userRoles) throws BaseException
    //{
    //}

}
