package com.cannyurl.af.service.proto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.UserUsercode;
import com.cannyurl.af.bean.UserUsercodeBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.UserUsercodeService;
import com.cannyurl.af.service.impl.UserUsercodeServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class UserUsercodeProtoService extends UserUsercodeServiceImpl implements UserUsercodeService
{
    private static final Logger log = Logger.getLogger(UserUsercodeProtoService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public UserUsercodeProtoService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // UserUsercode related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public UserUsercode getUserUsercode(String guid) throws BaseException
    {
        return super.getUserUsercode(guid);
    }

    @Override
    public Object getUserUsercode(String guid, String field) throws BaseException
    {
        return super.getUserUsercode(guid, field);
    }

    @Override
    public List<UserUsercode> getUserUsercodes(List<String> guids) throws BaseException
    {
        return super.getUserUsercodes(guids);
    }

    @Override
    public List<UserUsercode> getAllUserUsercodes() throws BaseException
    {
        return super.getAllUserUsercodes();
    }

    @Override
    public List<String> getAllUserUsercodeKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllUserUsercodeKeys(ordering, offset, count);
    }

    @Override
    public List<UserUsercode> findUserUsercodes(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findUserUsercodes(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findUserUsercodeKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findUserUsercodeKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createUserUsercode(UserUsercode userUsercode) throws BaseException
    {
        return super.createUserUsercode(userUsercode);
    }

    @Override
    public UserUsercode constructUserUsercode(UserUsercode userUsercode) throws BaseException
    {
        return super.constructUserUsercode(userUsercode);
    }


    @Override
    public Boolean updateUserUsercode(UserUsercode userUsercode) throws BaseException
    {
        return super.updateUserUsercode(userUsercode);
    }
        
    @Override
    public UserUsercode refreshUserUsercode(UserUsercode userUsercode) throws BaseException
    {
        return super.refreshUserUsercode(userUsercode);
    }

    @Override
    public Boolean deleteUserUsercode(String guid) throws BaseException
    {
        return super.deleteUserUsercode(guid);
    }

    @Override
    public Boolean deleteUserUsercode(UserUsercode userUsercode) throws BaseException
    {
        return super.deleteUserUsercode(userUsercode);
    }

    @Override
    public Integer createUserUsercodes(List<UserUsercode> userUsercodes) throws BaseException
    {
        return super.createUserUsercodes(userUsercodes);
    }

    // TBD
    //@Override
    //public Boolean updateUserUsercodes(List<UserUsercode> userUsercodes) throws BaseException
    //{
    //}

}
