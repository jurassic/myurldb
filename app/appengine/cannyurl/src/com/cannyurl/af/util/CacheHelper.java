package com.cannyurl.af.util;

import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;

import com.cannyurl.af.config.Config;


// Temporary
public final class CacheHelper
{
    private static final Logger log = Logger.getLogger(CacheHelper.class.getName());

    // TBD:
    // Arbitrary numbers:
    private static final int EXPIRATION_DELTA_FLASH = 30;           // 30 seconds. Primarily to be used as a "global lock"...
    private static final int EXPIRATION_DELTA_QUICK = 600;          // 10 minutes
    private static final int EXPIRATION_DELTA_SHORT = 1800;         // 30 minutes
    private static final int EXPIRATION_DELTA_HOUR = 3600;          // 1 hour
    private static final int EXPIRATION_DELTA_MEDIUM = 2 * 3600;    // 2 hours
    private static final int EXPIRATION_DELTA_QUARTER = 6 * 3600;   // 1/4 a day
    private static final int EXPIRATION_DELTA_LONG = 12 * 3600;     // half a day
    private static final int EXPIRATION_DELTA_DAY = 24 * 3600;      // 1 day
    private static final int EXPIRATION_DELTA_TWODAYS = 2 * 24 * 3600;   // 2 days
    private static final int EXPIRATION_DELTA_WEEK = 7 * 24 * 3600; // 1 week
    // Note: 
    // Longer than several hours, or a few days, is really equivalent to "non expiring" cache...
    // ...
    
    private final Integer expirationDelta;
    private final Cache cache;
    private CacheHelper() 
    {
        this(null);
    }
    private CacheHelper(Integer expirationDelta) 
    {
        this.expirationDelta = expirationDelta;
        this.cache = initCache(this.expirationDelta);
    }

    public Cache getCache()
    {
        return this.cache;
    }

    // To support "multi-tons"...
    private static Map<Integer, CacheHelper> sInstances = null;
    static {
        sInstances = new HashMap<Integer, CacheHelper>();
    }
    public static CacheHelper getInstance(Integer expirationDelta)
    {
        if(expirationDelta == null || expirationDelta == 0) {
            return getNonExpiringInstance();
        }
        // TBD: Should this be in the synchronized block as well????
        if(sInstances.containsKey(expirationDelta)) {
            return sInstances.get(expirationDelta);
        }
        CacheHelper INSTANCE = null;
        synchronized(sInstances) {
            INSTANCE = new CacheHelper(expirationDelta);
            sInstances.put(expirationDelta, INSTANCE);
        }
        return INSTANCE;
    }


    // "Default" == based on config...
    private static final class DefaultCacheHelperHolder
    {
        private static final CacheHelper INSTANCE = new CacheHelper(Config.getInstance().getCacheExpirationDelta());
    }
    public static CacheHelper getDefaultInstance()
    {
        return DefaultCacheHelperHolder.INSTANCE;
    }
    
    // No expiration delta
    private static final class NonExpiringCacheHelperHolder
    {
        private static final CacheHelper INSTANCE = new CacheHelper();
    }
    public static CacheHelper getNonExpiringInstance()
    {
        return NonExpiringCacheHelperHolder.INSTANCE;
    }

    private static final class FlashCacheHelperHolder
    {
        private static final CacheHelper INSTANCE = new CacheHelper(EXPIRATION_DELTA_FLASH);
    }
    public static CacheHelper getFlashInstance()
    {
        return FlashCacheHelperHolder.INSTANCE;
    }

    private static final class QuickCacheHelperHolder
    {
        private static final CacheHelper INSTANCE = new CacheHelper(EXPIRATION_DELTA_QUICK);
    }
    public static CacheHelper getQuickInstance()
    {
        return QuickCacheHelperHolder.INSTANCE;
    }

    private static final class ShortCacheHelperHolder
    {
        private static final CacheHelper INSTANCE = new CacheHelper(EXPIRATION_DELTA_SHORT);
    }
    public static CacheHelper getShortInstance()
    {
        return ShortCacheHelperHolder.INSTANCE;
    }

    private static final class HourLongCacheHelperHolder
    {
        private static final CacheHelper INSTANCE = new CacheHelper(EXPIRATION_DELTA_HOUR);
    }
    public static CacheHelper getHourLongInstance()
    {
        return HourLongCacheHelperHolder.INSTANCE;
    }

    private static final class MediumCacheHelperHolder
    {
        private static final CacheHelper INSTANCE = new CacheHelper(EXPIRATION_DELTA_MEDIUM);
    }
    public static CacheHelper getMediumInstance()
    {
        return MediumCacheHelperHolder.INSTANCE;
    }

    private static final class QuarterDayCacheHelperHolder
    {
        private static final CacheHelper INSTANCE = new CacheHelper(EXPIRATION_DELTA_QUARTER);
    }
    public static CacheHelper getQuarterDayInstance()
    {
        return QuarterDayCacheHelperHolder.INSTANCE;
    }

    private static final class LongCacheHelperHolder
    {
        private static final CacheHelper INSTANCE = new CacheHelper(EXPIRATION_DELTA_LONG);
    }
    public static CacheHelper getLongInstance()
    {
        return LongCacheHelperHolder.INSTANCE;
    }

    private static final class DayLongCacheHelperHolder
    {
        private static final CacheHelper INSTANCE = new CacheHelper(EXPIRATION_DELTA_DAY);
    }
    public static CacheHelper getDayLongInstance()
    {
        return DayLongCacheHelperHolder.INSTANCE;
    }

    private static final class TwoDayCacheHelperHolder
    {
        private static final CacheHelper INSTANCE = new CacheHelper(EXPIRATION_DELTA_TWODAYS);
    }
    public static CacheHelper getTwoDayInstance()
    {
        return TwoDayCacheHelperHolder.INSTANCE;
    }

    private static final class WeekLongCacheHelperHolder
    {
        private static final CacheHelper INSTANCE = new CacheHelper(EXPIRATION_DELTA_WEEK);
    }
    public static CacheHelper getWeekLongInstance()
    {
        return WeekLongCacheHelperHolder.INSTANCE;
    }

    
    // Init. called by ctor...
    private Cache initCache(Integer cacheExpirationDelta)
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                return cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
        return null;
    }

 
}
