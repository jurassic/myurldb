package com.cannyurl.af.util;

import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cannyurl.af.config.Config;


// Cross-Site Response Settings.
// http://www.w3.org/TR/cors/
// https://developer.mozilla.org/en-US/docs/HTTP_access_control
public final class CorsHelper
{
    private static final Logger log = Logger.getLogger(CorsHelper.class.getName());

    // TBD:
    public static final String AC_ALLOW_ORIGIN_NONE = "none";
    public static final String AC_ALLOW_ORIGIN_SAME = "same";
    public static final String AC_ALLOW_ORIGIN_ALL = "all";
    // ...

    private CorsHelper() 
    {
    }

    private static final class CorsHelperHolder
    {
        private static final CorsHelper INSTANCE = new CorsHelper();
    }
    public static CorsHelper getInstance()
    {
        return CorsHelperHolder.INSTANCE;
    }
    
    // For now,
    // Only for GET and POST....
    // ....

    // TBD: Just use whether getDefaultAllowOrigin() == AC_ALLOW_ORIGIN_NONE ??
    public Boolean useAllowOrigin()
    {
        return Config.getInstance().isCorsUseAllowOrigin();
    }
    public String getDefaultAllowOrigin()
    {
    	return Config.getInstance().getCorsAllowOrigin();
    }
    
    public static boolean isValid(String originType)
    {
        if(AC_ALLOW_ORIGIN_SAME.equals(originType) || AC_ALLOW_ORIGIN_ALL.equals(originType)) {
            return true;
        } else {
            return false;
        }
    }
 
}
