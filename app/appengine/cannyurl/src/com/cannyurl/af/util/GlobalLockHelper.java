package com.cannyurl.af.util;

import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;

import com.myurldb.ws.ApiConsumer;
import com.myurldb.ws.AppCustomDomain;
import com.myurldb.ws.SiteCustomDomain;
import com.myurldb.ws.User;
import com.myurldb.ws.UserUsercode;
import com.myurldb.ws.UserPassword;
import com.myurldb.ws.ExternalUserAuth;
import com.myurldb.ws.UserAuthState;
import com.myurldb.ws.UserResourcePermission;
import com.myurldb.ws.UserResourceProhibition;
import com.myurldb.ws.RolePermission;
import com.myurldb.ws.UserRole;
import com.myurldb.ws.AppClient;
import com.myurldb.ws.ClientUser;
import com.myurldb.ws.UserCustomDomain;
import com.myurldb.ws.ClientSetting;
import com.myurldb.ws.UserSetting;
import com.myurldb.ws.VisitorSetting;
import com.myurldb.ws.TwitterSummaryCard;
import com.myurldb.ws.TwitterPhotoCard;
import com.myurldb.ws.TwitterGalleryCard;
import com.myurldb.ws.TwitterAppCard;
import com.myurldb.ws.TwitterPlayerCard;
import com.myurldb.ws.TwitterProductCard;
import com.myurldb.ws.ShortPassage;
import com.myurldb.ws.ShortLink;
import com.myurldb.ws.GeoLink;
import com.myurldb.ws.QrCode;
import com.myurldb.ws.LinkPassphrase;
import com.myurldb.ws.LinkMessage;
import com.myurldb.ws.LinkAlbum;
import com.myurldb.ws.AlbumShortLink;
import com.myurldb.ws.KeywordFolder;
import com.myurldb.ws.BookmarkFolder;
import com.myurldb.ws.KeywordLink;
import com.myurldb.ws.BookmarkLink;
import com.myurldb.ws.SpeedDial;
import com.myurldb.ws.KeywordFolderImport;
import com.myurldb.ws.BookmarkFolderImport;
import com.myurldb.ws.KeywordCrowdTally;
import com.myurldb.ws.BookmarkCrowdTally;
import com.myurldb.ws.DomainInfo;
import com.myurldb.ws.UrlRating;
import com.myurldb.ws.UserRating;
import com.myurldb.ws.AbuseTag;
import com.myurldb.ws.ServiceInfo;
import com.myurldb.ws.FiveTen;


// Temporary
public final class GlobalLockHelper
{
    private static final Logger log = Logger.getLogger(GlobalLockHelper.class.getName());


    // Singleton
    private GlobalLockHelper() {}

    private static final class GlobalLockHelperHolder
    {
        private static final GlobalLockHelper INSTANCE = new GlobalLockHelper();
    }
    public static GlobalLockHelper getInstance()
    {
        return GlobalLockHelperHolder.INSTANCE;
    }


    // temporary
    // Poorman's "global locking" using a distributed cache...
    private static Cache getCache()
    {
        // 30 seconds
        return CacheHelper.getFlashInstance().getCache();
    }


    private static String getApiConsumerLockKey(ApiConsumer apiConsumer)
    {
        // Note: We assume that the arg apiConsumer is not null.
        return "ApiConsumer-Processing-Lock-" + apiConsumer.getGuid();
    }
    public String lockApiConsumer(ApiConsumer apiConsumer)
    {
        if(getCache() != null && apiConsumer != null) {
            // The stored value is not important.
            String val = apiConsumer.getGuid();
            getCache().put(getApiConsumerLockKey(apiConsumer), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockApiConsumer(ApiConsumer apiConsumer)
    {
        if(getCache() != null && apiConsumer != null) {
            String val = apiConsumer.getGuid();
            getCache().remove(getApiConsumerLockKey(apiConsumer));
            return val;
        } else {
            return null;
        }
    }
    public boolean isApiConsumerLocked(ApiConsumer apiConsumer)
    {
        if(getCache() != null && apiConsumer != null) {
            return getCache().containsKey(getApiConsumerLockKey(apiConsumer)); 
        } else {
            return false;
        }
    }

    private static String getAppCustomDomainLockKey(AppCustomDomain appCustomDomain)
    {
        // Note: We assume that the arg appCustomDomain is not null.
        return "AppCustomDomain-Processing-Lock-" + appCustomDomain.getGuid();
    }
    public String lockAppCustomDomain(AppCustomDomain appCustomDomain)
    {
        if(getCache() != null && appCustomDomain != null) {
            // The stored value is not important.
            String val = appCustomDomain.getGuid();
            getCache().put(getAppCustomDomainLockKey(appCustomDomain), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockAppCustomDomain(AppCustomDomain appCustomDomain)
    {
        if(getCache() != null && appCustomDomain != null) {
            String val = appCustomDomain.getGuid();
            getCache().remove(getAppCustomDomainLockKey(appCustomDomain));
            return val;
        } else {
            return null;
        }
    }
    public boolean isAppCustomDomainLocked(AppCustomDomain appCustomDomain)
    {
        if(getCache() != null && appCustomDomain != null) {
            return getCache().containsKey(getAppCustomDomainLockKey(appCustomDomain)); 
        } else {
            return false;
        }
    }

    private static String getSiteCustomDomainLockKey(SiteCustomDomain siteCustomDomain)
    {
        // Note: We assume that the arg siteCustomDomain is not null.
        return "SiteCustomDomain-Processing-Lock-" + siteCustomDomain.getGuid();
    }
    public String lockSiteCustomDomain(SiteCustomDomain siteCustomDomain)
    {
        if(getCache() != null && siteCustomDomain != null) {
            // The stored value is not important.
            String val = siteCustomDomain.getGuid();
            getCache().put(getSiteCustomDomainLockKey(siteCustomDomain), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockSiteCustomDomain(SiteCustomDomain siteCustomDomain)
    {
        if(getCache() != null && siteCustomDomain != null) {
            String val = siteCustomDomain.getGuid();
            getCache().remove(getSiteCustomDomainLockKey(siteCustomDomain));
            return val;
        } else {
            return null;
        }
    }
    public boolean isSiteCustomDomainLocked(SiteCustomDomain siteCustomDomain)
    {
        if(getCache() != null && siteCustomDomain != null) {
            return getCache().containsKey(getSiteCustomDomainLockKey(siteCustomDomain)); 
        } else {
            return false;
        }
    }

    private static String getUserLockKey(User user)
    {
        // Note: We assume that the arg user is not null.
        return "User-Processing-Lock-" + user.getGuid();
    }
    public String lockUser(User user)
    {
        if(getCache() != null && user != null) {
            // The stored value is not important.
            String val = user.getGuid();
            getCache().put(getUserLockKey(user), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockUser(User user)
    {
        if(getCache() != null && user != null) {
            String val = user.getGuid();
            getCache().remove(getUserLockKey(user));
            return val;
        } else {
            return null;
        }
    }
    public boolean isUserLocked(User user)
    {
        if(getCache() != null && user != null) {
            return getCache().containsKey(getUserLockKey(user)); 
        } else {
            return false;
        }
    }

    private static String getUserUsercodeLockKey(UserUsercode userUsercode)
    {
        // Note: We assume that the arg userUsercode is not null.
        return "UserUsercode-Processing-Lock-" + userUsercode.getGuid();
    }
    public String lockUserUsercode(UserUsercode userUsercode)
    {
        if(getCache() != null && userUsercode != null) {
            // The stored value is not important.
            String val = userUsercode.getGuid();
            getCache().put(getUserUsercodeLockKey(userUsercode), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockUserUsercode(UserUsercode userUsercode)
    {
        if(getCache() != null && userUsercode != null) {
            String val = userUsercode.getGuid();
            getCache().remove(getUserUsercodeLockKey(userUsercode));
            return val;
        } else {
            return null;
        }
    }
    public boolean isUserUsercodeLocked(UserUsercode userUsercode)
    {
        if(getCache() != null && userUsercode != null) {
            return getCache().containsKey(getUserUsercodeLockKey(userUsercode)); 
        } else {
            return false;
        }
    }

    private static String getUserPasswordLockKey(UserPassword userPassword)
    {
        // Note: We assume that the arg userPassword is not null.
        return "UserPassword-Processing-Lock-" + userPassword.getGuid();
    }
    public String lockUserPassword(UserPassword userPassword)
    {
        if(getCache() != null && userPassword != null) {
            // The stored value is not important.
            String val = userPassword.getGuid();
            getCache().put(getUserPasswordLockKey(userPassword), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockUserPassword(UserPassword userPassword)
    {
        if(getCache() != null && userPassword != null) {
            String val = userPassword.getGuid();
            getCache().remove(getUserPasswordLockKey(userPassword));
            return val;
        } else {
            return null;
        }
    }
    public boolean isUserPasswordLocked(UserPassword userPassword)
    {
        if(getCache() != null && userPassword != null) {
            return getCache().containsKey(getUserPasswordLockKey(userPassword)); 
        } else {
            return false;
        }
    }

    private static String getExternalUserAuthLockKey(ExternalUserAuth externalUserAuth)
    {
        // Note: We assume that the arg externalUserAuth is not null.
        return "ExternalUserAuth-Processing-Lock-" + externalUserAuth.getGuid();
    }
    public String lockExternalUserAuth(ExternalUserAuth externalUserAuth)
    {
        if(getCache() != null && externalUserAuth != null) {
            // The stored value is not important.
            String val = externalUserAuth.getGuid();
            getCache().put(getExternalUserAuthLockKey(externalUserAuth), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockExternalUserAuth(ExternalUserAuth externalUserAuth)
    {
        if(getCache() != null && externalUserAuth != null) {
            String val = externalUserAuth.getGuid();
            getCache().remove(getExternalUserAuthLockKey(externalUserAuth));
            return val;
        } else {
            return null;
        }
    }
    public boolean isExternalUserAuthLocked(ExternalUserAuth externalUserAuth)
    {
        if(getCache() != null && externalUserAuth != null) {
            return getCache().containsKey(getExternalUserAuthLockKey(externalUserAuth)); 
        } else {
            return false;
        }
    }

    private static String getUserAuthStateLockKey(UserAuthState userAuthState)
    {
        // Note: We assume that the arg userAuthState is not null.
        return "UserAuthState-Processing-Lock-" + userAuthState.getGuid();
    }
    public String lockUserAuthState(UserAuthState userAuthState)
    {
        if(getCache() != null && userAuthState != null) {
            // The stored value is not important.
            String val = userAuthState.getGuid();
            getCache().put(getUserAuthStateLockKey(userAuthState), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockUserAuthState(UserAuthState userAuthState)
    {
        if(getCache() != null && userAuthState != null) {
            String val = userAuthState.getGuid();
            getCache().remove(getUserAuthStateLockKey(userAuthState));
            return val;
        } else {
            return null;
        }
    }
    public boolean isUserAuthStateLocked(UserAuthState userAuthState)
    {
        if(getCache() != null && userAuthState != null) {
            return getCache().containsKey(getUserAuthStateLockKey(userAuthState)); 
        } else {
            return false;
        }
    }

    private static String getUserResourcePermissionLockKey(UserResourcePermission userResourcePermission)
    {
        // Note: We assume that the arg userResourcePermission is not null.
        return "UserResourcePermission-Processing-Lock-" + userResourcePermission.getGuid();
    }
    public String lockUserResourcePermission(UserResourcePermission userResourcePermission)
    {
        if(getCache() != null && userResourcePermission != null) {
            // The stored value is not important.
            String val = userResourcePermission.getGuid();
            getCache().put(getUserResourcePermissionLockKey(userResourcePermission), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockUserResourcePermission(UserResourcePermission userResourcePermission)
    {
        if(getCache() != null && userResourcePermission != null) {
            String val = userResourcePermission.getGuid();
            getCache().remove(getUserResourcePermissionLockKey(userResourcePermission));
            return val;
        } else {
            return null;
        }
    }
    public boolean isUserResourcePermissionLocked(UserResourcePermission userResourcePermission)
    {
        if(getCache() != null && userResourcePermission != null) {
            return getCache().containsKey(getUserResourcePermissionLockKey(userResourcePermission)); 
        } else {
            return false;
        }
    }

    private static String getUserResourceProhibitionLockKey(UserResourceProhibition userResourceProhibition)
    {
        // Note: We assume that the arg userResourceProhibition is not null.
        return "UserResourceProhibition-Processing-Lock-" + userResourceProhibition.getGuid();
    }
    public String lockUserResourceProhibition(UserResourceProhibition userResourceProhibition)
    {
        if(getCache() != null && userResourceProhibition != null) {
            // The stored value is not important.
            String val = userResourceProhibition.getGuid();
            getCache().put(getUserResourceProhibitionLockKey(userResourceProhibition), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockUserResourceProhibition(UserResourceProhibition userResourceProhibition)
    {
        if(getCache() != null && userResourceProhibition != null) {
            String val = userResourceProhibition.getGuid();
            getCache().remove(getUserResourceProhibitionLockKey(userResourceProhibition));
            return val;
        } else {
            return null;
        }
    }
    public boolean isUserResourceProhibitionLocked(UserResourceProhibition userResourceProhibition)
    {
        if(getCache() != null && userResourceProhibition != null) {
            return getCache().containsKey(getUserResourceProhibitionLockKey(userResourceProhibition)); 
        } else {
            return false;
        }
    }

    private static String getRolePermissionLockKey(RolePermission rolePermission)
    {
        // Note: We assume that the arg rolePermission is not null.
        return "RolePermission-Processing-Lock-" + rolePermission.getGuid();
    }
    public String lockRolePermission(RolePermission rolePermission)
    {
        if(getCache() != null && rolePermission != null) {
            // The stored value is not important.
            String val = rolePermission.getGuid();
            getCache().put(getRolePermissionLockKey(rolePermission), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockRolePermission(RolePermission rolePermission)
    {
        if(getCache() != null && rolePermission != null) {
            String val = rolePermission.getGuid();
            getCache().remove(getRolePermissionLockKey(rolePermission));
            return val;
        } else {
            return null;
        }
    }
    public boolean isRolePermissionLocked(RolePermission rolePermission)
    {
        if(getCache() != null && rolePermission != null) {
            return getCache().containsKey(getRolePermissionLockKey(rolePermission)); 
        } else {
            return false;
        }
    }

    private static String getUserRoleLockKey(UserRole userRole)
    {
        // Note: We assume that the arg userRole is not null.
        return "UserRole-Processing-Lock-" + userRole.getGuid();
    }
    public String lockUserRole(UserRole userRole)
    {
        if(getCache() != null && userRole != null) {
            // The stored value is not important.
            String val = userRole.getGuid();
            getCache().put(getUserRoleLockKey(userRole), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockUserRole(UserRole userRole)
    {
        if(getCache() != null && userRole != null) {
            String val = userRole.getGuid();
            getCache().remove(getUserRoleLockKey(userRole));
            return val;
        } else {
            return null;
        }
    }
    public boolean isUserRoleLocked(UserRole userRole)
    {
        if(getCache() != null && userRole != null) {
            return getCache().containsKey(getUserRoleLockKey(userRole)); 
        } else {
            return false;
        }
    }

    private static String getAppClientLockKey(AppClient appClient)
    {
        // Note: We assume that the arg appClient is not null.
        return "AppClient-Processing-Lock-" + appClient.getGuid();
    }
    public String lockAppClient(AppClient appClient)
    {
        if(getCache() != null && appClient != null) {
            // The stored value is not important.
            String val = appClient.getGuid();
            getCache().put(getAppClientLockKey(appClient), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockAppClient(AppClient appClient)
    {
        if(getCache() != null && appClient != null) {
            String val = appClient.getGuid();
            getCache().remove(getAppClientLockKey(appClient));
            return val;
        } else {
            return null;
        }
    }
    public boolean isAppClientLocked(AppClient appClient)
    {
        if(getCache() != null && appClient != null) {
            return getCache().containsKey(getAppClientLockKey(appClient)); 
        } else {
            return false;
        }
    }

    private static String getClientUserLockKey(ClientUser clientUser)
    {
        // Note: We assume that the arg clientUser is not null.
        return "ClientUser-Processing-Lock-" + clientUser.getGuid();
    }
    public String lockClientUser(ClientUser clientUser)
    {
        if(getCache() != null && clientUser != null) {
            // The stored value is not important.
            String val = clientUser.getGuid();
            getCache().put(getClientUserLockKey(clientUser), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockClientUser(ClientUser clientUser)
    {
        if(getCache() != null && clientUser != null) {
            String val = clientUser.getGuid();
            getCache().remove(getClientUserLockKey(clientUser));
            return val;
        } else {
            return null;
        }
    }
    public boolean isClientUserLocked(ClientUser clientUser)
    {
        if(getCache() != null && clientUser != null) {
            return getCache().containsKey(getClientUserLockKey(clientUser)); 
        } else {
            return false;
        }
    }

    private static String getUserCustomDomainLockKey(UserCustomDomain userCustomDomain)
    {
        // Note: We assume that the arg userCustomDomain is not null.
        return "UserCustomDomain-Processing-Lock-" + userCustomDomain.getGuid();
    }
    public String lockUserCustomDomain(UserCustomDomain userCustomDomain)
    {
        if(getCache() != null && userCustomDomain != null) {
            // The stored value is not important.
            String val = userCustomDomain.getGuid();
            getCache().put(getUserCustomDomainLockKey(userCustomDomain), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockUserCustomDomain(UserCustomDomain userCustomDomain)
    {
        if(getCache() != null && userCustomDomain != null) {
            String val = userCustomDomain.getGuid();
            getCache().remove(getUserCustomDomainLockKey(userCustomDomain));
            return val;
        } else {
            return null;
        }
    }
    public boolean isUserCustomDomainLocked(UserCustomDomain userCustomDomain)
    {
        if(getCache() != null && userCustomDomain != null) {
            return getCache().containsKey(getUserCustomDomainLockKey(userCustomDomain)); 
        } else {
            return false;
        }
    }

    private static String getClientSettingLockKey(ClientSetting clientSetting)
    {
        // Note: We assume that the arg clientSetting is not null.
        return "ClientSetting-Processing-Lock-" + clientSetting.getGuid();
    }
    public String lockClientSetting(ClientSetting clientSetting)
    {
        if(getCache() != null && clientSetting != null) {
            // The stored value is not important.
            String val = clientSetting.getGuid();
            getCache().put(getClientSettingLockKey(clientSetting), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockClientSetting(ClientSetting clientSetting)
    {
        if(getCache() != null && clientSetting != null) {
            String val = clientSetting.getGuid();
            getCache().remove(getClientSettingLockKey(clientSetting));
            return val;
        } else {
            return null;
        }
    }
    public boolean isClientSettingLocked(ClientSetting clientSetting)
    {
        if(getCache() != null && clientSetting != null) {
            return getCache().containsKey(getClientSettingLockKey(clientSetting)); 
        } else {
            return false;
        }
    }

    private static String getUserSettingLockKey(UserSetting userSetting)
    {
        // Note: We assume that the arg userSetting is not null.
        return "UserSetting-Processing-Lock-" + userSetting.getGuid();
    }
    public String lockUserSetting(UserSetting userSetting)
    {
        if(getCache() != null && userSetting != null) {
            // The stored value is not important.
            String val = userSetting.getGuid();
            getCache().put(getUserSettingLockKey(userSetting), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockUserSetting(UserSetting userSetting)
    {
        if(getCache() != null && userSetting != null) {
            String val = userSetting.getGuid();
            getCache().remove(getUserSettingLockKey(userSetting));
            return val;
        } else {
            return null;
        }
    }
    public boolean isUserSettingLocked(UserSetting userSetting)
    {
        if(getCache() != null && userSetting != null) {
            return getCache().containsKey(getUserSettingLockKey(userSetting)); 
        } else {
            return false;
        }
    }

    private static String getVisitorSettingLockKey(VisitorSetting visitorSetting)
    {
        // Note: We assume that the arg visitorSetting is not null.
        return "VisitorSetting-Processing-Lock-" + visitorSetting.getGuid();
    }
    public String lockVisitorSetting(VisitorSetting visitorSetting)
    {
        if(getCache() != null && visitorSetting != null) {
            // The stored value is not important.
            String val = visitorSetting.getGuid();
            getCache().put(getVisitorSettingLockKey(visitorSetting), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockVisitorSetting(VisitorSetting visitorSetting)
    {
        if(getCache() != null && visitorSetting != null) {
            String val = visitorSetting.getGuid();
            getCache().remove(getVisitorSettingLockKey(visitorSetting));
            return val;
        } else {
            return null;
        }
    }
    public boolean isVisitorSettingLocked(VisitorSetting visitorSetting)
    {
        if(getCache() != null && visitorSetting != null) {
            return getCache().containsKey(getVisitorSettingLockKey(visitorSetting)); 
        } else {
            return false;
        }
    }

    private static String getTwitterSummaryCardLockKey(TwitterSummaryCard twitterSummaryCard)
    {
        // Note: We assume that the arg twitterSummaryCard is not null.
        return "TwitterSummaryCard-Processing-Lock-" + twitterSummaryCard.getGuid();
    }
    public String lockTwitterSummaryCard(TwitterSummaryCard twitterSummaryCard)
    {
        if(getCache() != null && twitterSummaryCard != null) {
            // The stored value is not important.
            String val = twitterSummaryCard.getGuid();
            getCache().put(getTwitterSummaryCardLockKey(twitterSummaryCard), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockTwitterSummaryCard(TwitterSummaryCard twitterSummaryCard)
    {
        if(getCache() != null && twitterSummaryCard != null) {
            String val = twitterSummaryCard.getGuid();
            getCache().remove(getTwitterSummaryCardLockKey(twitterSummaryCard));
            return val;
        } else {
            return null;
        }
    }
    public boolean isTwitterSummaryCardLocked(TwitterSummaryCard twitterSummaryCard)
    {
        if(getCache() != null && twitterSummaryCard != null) {
            return getCache().containsKey(getTwitterSummaryCardLockKey(twitterSummaryCard)); 
        } else {
            return false;
        }
    }

    private static String getTwitterPhotoCardLockKey(TwitterPhotoCard twitterPhotoCard)
    {
        // Note: We assume that the arg twitterPhotoCard is not null.
        return "TwitterPhotoCard-Processing-Lock-" + twitterPhotoCard.getGuid();
    }
    public String lockTwitterPhotoCard(TwitterPhotoCard twitterPhotoCard)
    {
        if(getCache() != null && twitterPhotoCard != null) {
            // The stored value is not important.
            String val = twitterPhotoCard.getGuid();
            getCache().put(getTwitterPhotoCardLockKey(twitterPhotoCard), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockTwitterPhotoCard(TwitterPhotoCard twitterPhotoCard)
    {
        if(getCache() != null && twitterPhotoCard != null) {
            String val = twitterPhotoCard.getGuid();
            getCache().remove(getTwitterPhotoCardLockKey(twitterPhotoCard));
            return val;
        } else {
            return null;
        }
    }
    public boolean isTwitterPhotoCardLocked(TwitterPhotoCard twitterPhotoCard)
    {
        if(getCache() != null && twitterPhotoCard != null) {
            return getCache().containsKey(getTwitterPhotoCardLockKey(twitterPhotoCard)); 
        } else {
            return false;
        }
    }

    private static String getTwitterGalleryCardLockKey(TwitterGalleryCard twitterGalleryCard)
    {
        // Note: We assume that the arg twitterGalleryCard is not null.
        return "TwitterGalleryCard-Processing-Lock-" + twitterGalleryCard.getGuid();
    }
    public String lockTwitterGalleryCard(TwitterGalleryCard twitterGalleryCard)
    {
        if(getCache() != null && twitterGalleryCard != null) {
            // The stored value is not important.
            String val = twitterGalleryCard.getGuid();
            getCache().put(getTwitterGalleryCardLockKey(twitterGalleryCard), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockTwitterGalleryCard(TwitterGalleryCard twitterGalleryCard)
    {
        if(getCache() != null && twitterGalleryCard != null) {
            String val = twitterGalleryCard.getGuid();
            getCache().remove(getTwitterGalleryCardLockKey(twitterGalleryCard));
            return val;
        } else {
            return null;
        }
    }
    public boolean isTwitterGalleryCardLocked(TwitterGalleryCard twitterGalleryCard)
    {
        if(getCache() != null && twitterGalleryCard != null) {
            return getCache().containsKey(getTwitterGalleryCardLockKey(twitterGalleryCard)); 
        } else {
            return false;
        }
    }

    private static String getTwitterAppCardLockKey(TwitterAppCard twitterAppCard)
    {
        // Note: We assume that the arg twitterAppCard is not null.
        return "TwitterAppCard-Processing-Lock-" + twitterAppCard.getGuid();
    }
    public String lockTwitterAppCard(TwitterAppCard twitterAppCard)
    {
        if(getCache() != null && twitterAppCard != null) {
            // The stored value is not important.
            String val = twitterAppCard.getGuid();
            getCache().put(getTwitterAppCardLockKey(twitterAppCard), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockTwitterAppCard(TwitterAppCard twitterAppCard)
    {
        if(getCache() != null && twitterAppCard != null) {
            String val = twitterAppCard.getGuid();
            getCache().remove(getTwitterAppCardLockKey(twitterAppCard));
            return val;
        } else {
            return null;
        }
    }
    public boolean isTwitterAppCardLocked(TwitterAppCard twitterAppCard)
    {
        if(getCache() != null && twitterAppCard != null) {
            return getCache().containsKey(getTwitterAppCardLockKey(twitterAppCard)); 
        } else {
            return false;
        }
    }

    private static String getTwitterPlayerCardLockKey(TwitterPlayerCard twitterPlayerCard)
    {
        // Note: We assume that the arg twitterPlayerCard is not null.
        return "TwitterPlayerCard-Processing-Lock-" + twitterPlayerCard.getGuid();
    }
    public String lockTwitterPlayerCard(TwitterPlayerCard twitterPlayerCard)
    {
        if(getCache() != null && twitterPlayerCard != null) {
            // The stored value is not important.
            String val = twitterPlayerCard.getGuid();
            getCache().put(getTwitterPlayerCardLockKey(twitterPlayerCard), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockTwitterPlayerCard(TwitterPlayerCard twitterPlayerCard)
    {
        if(getCache() != null && twitterPlayerCard != null) {
            String val = twitterPlayerCard.getGuid();
            getCache().remove(getTwitterPlayerCardLockKey(twitterPlayerCard));
            return val;
        } else {
            return null;
        }
    }
    public boolean isTwitterPlayerCardLocked(TwitterPlayerCard twitterPlayerCard)
    {
        if(getCache() != null && twitterPlayerCard != null) {
            return getCache().containsKey(getTwitterPlayerCardLockKey(twitterPlayerCard)); 
        } else {
            return false;
        }
    }

    private static String getTwitterProductCardLockKey(TwitterProductCard twitterProductCard)
    {
        // Note: We assume that the arg twitterProductCard is not null.
        return "TwitterProductCard-Processing-Lock-" + twitterProductCard.getGuid();
    }
    public String lockTwitterProductCard(TwitterProductCard twitterProductCard)
    {
        if(getCache() != null && twitterProductCard != null) {
            // The stored value is not important.
            String val = twitterProductCard.getGuid();
            getCache().put(getTwitterProductCardLockKey(twitterProductCard), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockTwitterProductCard(TwitterProductCard twitterProductCard)
    {
        if(getCache() != null && twitterProductCard != null) {
            String val = twitterProductCard.getGuid();
            getCache().remove(getTwitterProductCardLockKey(twitterProductCard));
            return val;
        } else {
            return null;
        }
    }
    public boolean isTwitterProductCardLocked(TwitterProductCard twitterProductCard)
    {
        if(getCache() != null && twitterProductCard != null) {
            return getCache().containsKey(getTwitterProductCardLockKey(twitterProductCard)); 
        } else {
            return false;
        }
    }

    private static String getShortPassageLockKey(ShortPassage shortPassage)
    {
        // Note: We assume that the arg shortPassage is not null.
        return "ShortPassage-Processing-Lock-" + shortPassage.getGuid();
    }
    public String lockShortPassage(ShortPassage shortPassage)
    {
        if(getCache() != null && shortPassage != null) {
            // The stored value is not important.
            String val = shortPassage.getGuid();
            getCache().put(getShortPassageLockKey(shortPassage), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockShortPassage(ShortPassage shortPassage)
    {
        if(getCache() != null && shortPassage != null) {
            String val = shortPassage.getGuid();
            getCache().remove(getShortPassageLockKey(shortPassage));
            return val;
        } else {
            return null;
        }
    }
    public boolean isShortPassageLocked(ShortPassage shortPassage)
    {
        if(getCache() != null && shortPassage != null) {
            return getCache().containsKey(getShortPassageLockKey(shortPassage)); 
        } else {
            return false;
        }
    }

    private static String getShortLinkLockKey(ShortLink shortLink)
    {
        // Note: We assume that the arg shortLink is not null.
        return "ShortLink-Processing-Lock-" + shortLink.getGuid();
    }
    public String lockShortLink(ShortLink shortLink)
    {
        if(getCache() != null && shortLink != null) {
            // The stored value is not important.
            String val = shortLink.getGuid();
            getCache().put(getShortLinkLockKey(shortLink), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockShortLink(ShortLink shortLink)
    {
        if(getCache() != null && shortLink != null) {
            String val = shortLink.getGuid();
            getCache().remove(getShortLinkLockKey(shortLink));
            return val;
        } else {
            return null;
        }
    }
    public boolean isShortLinkLocked(ShortLink shortLink)
    {
        if(getCache() != null && shortLink != null) {
            return getCache().containsKey(getShortLinkLockKey(shortLink)); 
        } else {
            return false;
        }
    }

    private static String getGeoLinkLockKey(GeoLink geoLink)
    {
        // Note: We assume that the arg geoLink is not null.
        return "GeoLink-Processing-Lock-" + geoLink.getGuid();
    }
    public String lockGeoLink(GeoLink geoLink)
    {
        if(getCache() != null && geoLink != null) {
            // The stored value is not important.
            String val = geoLink.getGuid();
            getCache().put(getGeoLinkLockKey(geoLink), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockGeoLink(GeoLink geoLink)
    {
        if(getCache() != null && geoLink != null) {
            String val = geoLink.getGuid();
            getCache().remove(getGeoLinkLockKey(geoLink));
            return val;
        } else {
            return null;
        }
    }
    public boolean isGeoLinkLocked(GeoLink geoLink)
    {
        if(getCache() != null && geoLink != null) {
            return getCache().containsKey(getGeoLinkLockKey(geoLink)); 
        } else {
            return false;
        }
    }

    private static String getQrCodeLockKey(QrCode qrCode)
    {
        // Note: We assume that the arg qrCode is not null.
        return "QrCode-Processing-Lock-" + qrCode.getGuid();
    }
    public String lockQrCode(QrCode qrCode)
    {
        if(getCache() != null && qrCode != null) {
            // The stored value is not important.
            String val = qrCode.getGuid();
            getCache().put(getQrCodeLockKey(qrCode), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockQrCode(QrCode qrCode)
    {
        if(getCache() != null && qrCode != null) {
            String val = qrCode.getGuid();
            getCache().remove(getQrCodeLockKey(qrCode));
            return val;
        } else {
            return null;
        }
    }
    public boolean isQrCodeLocked(QrCode qrCode)
    {
        if(getCache() != null && qrCode != null) {
            return getCache().containsKey(getQrCodeLockKey(qrCode)); 
        } else {
            return false;
        }
    }

    private static String getLinkPassphraseLockKey(LinkPassphrase linkPassphrase)
    {
        // Note: We assume that the arg linkPassphrase is not null.
        return "LinkPassphrase-Processing-Lock-" + linkPassphrase.getGuid();
    }
    public String lockLinkPassphrase(LinkPassphrase linkPassphrase)
    {
        if(getCache() != null && linkPassphrase != null) {
            // The stored value is not important.
            String val = linkPassphrase.getGuid();
            getCache().put(getLinkPassphraseLockKey(linkPassphrase), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockLinkPassphrase(LinkPassphrase linkPassphrase)
    {
        if(getCache() != null && linkPassphrase != null) {
            String val = linkPassphrase.getGuid();
            getCache().remove(getLinkPassphraseLockKey(linkPassphrase));
            return val;
        } else {
            return null;
        }
    }
    public boolean isLinkPassphraseLocked(LinkPassphrase linkPassphrase)
    {
        if(getCache() != null && linkPassphrase != null) {
            return getCache().containsKey(getLinkPassphraseLockKey(linkPassphrase)); 
        } else {
            return false;
        }
    }

    private static String getLinkMessageLockKey(LinkMessage linkMessage)
    {
        // Note: We assume that the arg linkMessage is not null.
        return "LinkMessage-Processing-Lock-" + linkMessage.getGuid();
    }
    public String lockLinkMessage(LinkMessage linkMessage)
    {
        if(getCache() != null && linkMessage != null) {
            // The stored value is not important.
            String val = linkMessage.getGuid();
            getCache().put(getLinkMessageLockKey(linkMessage), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockLinkMessage(LinkMessage linkMessage)
    {
        if(getCache() != null && linkMessage != null) {
            String val = linkMessage.getGuid();
            getCache().remove(getLinkMessageLockKey(linkMessage));
            return val;
        } else {
            return null;
        }
    }
    public boolean isLinkMessageLocked(LinkMessage linkMessage)
    {
        if(getCache() != null && linkMessage != null) {
            return getCache().containsKey(getLinkMessageLockKey(linkMessage)); 
        } else {
            return false;
        }
    }

    private static String getLinkAlbumLockKey(LinkAlbum linkAlbum)
    {
        // Note: We assume that the arg linkAlbum is not null.
        return "LinkAlbum-Processing-Lock-" + linkAlbum.getGuid();
    }
    public String lockLinkAlbum(LinkAlbum linkAlbum)
    {
        if(getCache() != null && linkAlbum != null) {
            // The stored value is not important.
            String val = linkAlbum.getGuid();
            getCache().put(getLinkAlbumLockKey(linkAlbum), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockLinkAlbum(LinkAlbum linkAlbum)
    {
        if(getCache() != null && linkAlbum != null) {
            String val = linkAlbum.getGuid();
            getCache().remove(getLinkAlbumLockKey(linkAlbum));
            return val;
        } else {
            return null;
        }
    }
    public boolean isLinkAlbumLocked(LinkAlbum linkAlbum)
    {
        if(getCache() != null && linkAlbum != null) {
            return getCache().containsKey(getLinkAlbumLockKey(linkAlbum)); 
        } else {
            return false;
        }
    }

    private static String getAlbumShortLinkLockKey(AlbumShortLink albumShortLink)
    {
        // Note: We assume that the arg albumShortLink is not null.
        return "AlbumShortLink-Processing-Lock-" + albumShortLink.getGuid();
    }
    public String lockAlbumShortLink(AlbumShortLink albumShortLink)
    {
        if(getCache() != null && albumShortLink != null) {
            // The stored value is not important.
            String val = albumShortLink.getGuid();
            getCache().put(getAlbumShortLinkLockKey(albumShortLink), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockAlbumShortLink(AlbumShortLink albumShortLink)
    {
        if(getCache() != null && albumShortLink != null) {
            String val = albumShortLink.getGuid();
            getCache().remove(getAlbumShortLinkLockKey(albumShortLink));
            return val;
        } else {
            return null;
        }
    }
    public boolean isAlbumShortLinkLocked(AlbumShortLink albumShortLink)
    {
        if(getCache() != null && albumShortLink != null) {
            return getCache().containsKey(getAlbumShortLinkLockKey(albumShortLink)); 
        } else {
            return false;
        }
    }

    private static String getKeywordFolderLockKey(KeywordFolder keywordFolder)
    {
        // Note: We assume that the arg keywordFolder is not null.
        return "KeywordFolder-Processing-Lock-" + keywordFolder.getGuid();
    }
    public String lockKeywordFolder(KeywordFolder keywordFolder)
    {
        if(getCache() != null && keywordFolder != null) {
            // The stored value is not important.
            String val = keywordFolder.getGuid();
            getCache().put(getKeywordFolderLockKey(keywordFolder), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockKeywordFolder(KeywordFolder keywordFolder)
    {
        if(getCache() != null && keywordFolder != null) {
            String val = keywordFolder.getGuid();
            getCache().remove(getKeywordFolderLockKey(keywordFolder));
            return val;
        } else {
            return null;
        }
    }
    public boolean isKeywordFolderLocked(KeywordFolder keywordFolder)
    {
        if(getCache() != null && keywordFolder != null) {
            return getCache().containsKey(getKeywordFolderLockKey(keywordFolder)); 
        } else {
            return false;
        }
    }

    private static String getBookmarkFolderLockKey(BookmarkFolder bookmarkFolder)
    {
        // Note: We assume that the arg bookmarkFolder is not null.
        return "BookmarkFolder-Processing-Lock-" + bookmarkFolder.getGuid();
    }
    public String lockBookmarkFolder(BookmarkFolder bookmarkFolder)
    {
        if(getCache() != null && bookmarkFolder != null) {
            // The stored value is not important.
            String val = bookmarkFolder.getGuid();
            getCache().put(getBookmarkFolderLockKey(bookmarkFolder), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockBookmarkFolder(BookmarkFolder bookmarkFolder)
    {
        if(getCache() != null && bookmarkFolder != null) {
            String val = bookmarkFolder.getGuid();
            getCache().remove(getBookmarkFolderLockKey(bookmarkFolder));
            return val;
        } else {
            return null;
        }
    }
    public boolean isBookmarkFolderLocked(BookmarkFolder bookmarkFolder)
    {
        if(getCache() != null && bookmarkFolder != null) {
            return getCache().containsKey(getBookmarkFolderLockKey(bookmarkFolder)); 
        } else {
            return false;
        }
    }

    private static String getKeywordLinkLockKey(KeywordLink keywordLink)
    {
        // Note: We assume that the arg keywordLink is not null.
        return "KeywordLink-Processing-Lock-" + keywordLink.getGuid();
    }
    public String lockKeywordLink(KeywordLink keywordLink)
    {
        if(getCache() != null && keywordLink != null) {
            // The stored value is not important.
            String val = keywordLink.getGuid();
            getCache().put(getKeywordLinkLockKey(keywordLink), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockKeywordLink(KeywordLink keywordLink)
    {
        if(getCache() != null && keywordLink != null) {
            String val = keywordLink.getGuid();
            getCache().remove(getKeywordLinkLockKey(keywordLink));
            return val;
        } else {
            return null;
        }
    }
    public boolean isKeywordLinkLocked(KeywordLink keywordLink)
    {
        if(getCache() != null && keywordLink != null) {
            return getCache().containsKey(getKeywordLinkLockKey(keywordLink)); 
        } else {
            return false;
        }
    }

    private static String getBookmarkLinkLockKey(BookmarkLink bookmarkLink)
    {
        // Note: We assume that the arg bookmarkLink is not null.
        return "BookmarkLink-Processing-Lock-" + bookmarkLink.getGuid();
    }
    public String lockBookmarkLink(BookmarkLink bookmarkLink)
    {
        if(getCache() != null && bookmarkLink != null) {
            // The stored value is not important.
            String val = bookmarkLink.getGuid();
            getCache().put(getBookmarkLinkLockKey(bookmarkLink), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockBookmarkLink(BookmarkLink bookmarkLink)
    {
        if(getCache() != null && bookmarkLink != null) {
            String val = bookmarkLink.getGuid();
            getCache().remove(getBookmarkLinkLockKey(bookmarkLink));
            return val;
        } else {
            return null;
        }
    }
    public boolean isBookmarkLinkLocked(BookmarkLink bookmarkLink)
    {
        if(getCache() != null && bookmarkLink != null) {
            return getCache().containsKey(getBookmarkLinkLockKey(bookmarkLink)); 
        } else {
            return false;
        }
    }

    private static String getSpeedDialLockKey(SpeedDial speedDial)
    {
        // Note: We assume that the arg speedDial is not null.
        return "SpeedDial-Processing-Lock-" + speedDial.getGuid();
    }
    public String lockSpeedDial(SpeedDial speedDial)
    {
        if(getCache() != null && speedDial != null) {
            // The stored value is not important.
            String val = speedDial.getGuid();
            getCache().put(getSpeedDialLockKey(speedDial), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockSpeedDial(SpeedDial speedDial)
    {
        if(getCache() != null && speedDial != null) {
            String val = speedDial.getGuid();
            getCache().remove(getSpeedDialLockKey(speedDial));
            return val;
        } else {
            return null;
        }
    }
    public boolean isSpeedDialLocked(SpeedDial speedDial)
    {
        if(getCache() != null && speedDial != null) {
            return getCache().containsKey(getSpeedDialLockKey(speedDial)); 
        } else {
            return false;
        }
    }

    private static String getKeywordFolderImportLockKey(KeywordFolderImport keywordFolderImport)
    {
        // Note: We assume that the arg keywordFolderImport is not null.
        return "KeywordFolderImport-Processing-Lock-" + keywordFolderImport.getGuid();
    }
    public String lockKeywordFolderImport(KeywordFolderImport keywordFolderImport)
    {
        if(getCache() != null && keywordFolderImport != null) {
            // The stored value is not important.
            String val = keywordFolderImport.getGuid();
            getCache().put(getKeywordFolderImportLockKey(keywordFolderImport), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockKeywordFolderImport(KeywordFolderImport keywordFolderImport)
    {
        if(getCache() != null && keywordFolderImport != null) {
            String val = keywordFolderImport.getGuid();
            getCache().remove(getKeywordFolderImportLockKey(keywordFolderImport));
            return val;
        } else {
            return null;
        }
    }
    public boolean isKeywordFolderImportLocked(KeywordFolderImport keywordFolderImport)
    {
        if(getCache() != null && keywordFolderImport != null) {
            return getCache().containsKey(getKeywordFolderImportLockKey(keywordFolderImport)); 
        } else {
            return false;
        }
    }

    private static String getBookmarkFolderImportLockKey(BookmarkFolderImport bookmarkFolderImport)
    {
        // Note: We assume that the arg bookmarkFolderImport is not null.
        return "BookmarkFolderImport-Processing-Lock-" + bookmarkFolderImport.getGuid();
    }
    public String lockBookmarkFolderImport(BookmarkFolderImport bookmarkFolderImport)
    {
        if(getCache() != null && bookmarkFolderImport != null) {
            // The stored value is not important.
            String val = bookmarkFolderImport.getGuid();
            getCache().put(getBookmarkFolderImportLockKey(bookmarkFolderImport), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockBookmarkFolderImport(BookmarkFolderImport bookmarkFolderImport)
    {
        if(getCache() != null && bookmarkFolderImport != null) {
            String val = bookmarkFolderImport.getGuid();
            getCache().remove(getBookmarkFolderImportLockKey(bookmarkFolderImport));
            return val;
        } else {
            return null;
        }
    }
    public boolean isBookmarkFolderImportLocked(BookmarkFolderImport bookmarkFolderImport)
    {
        if(getCache() != null && bookmarkFolderImport != null) {
            return getCache().containsKey(getBookmarkFolderImportLockKey(bookmarkFolderImport)); 
        } else {
            return false;
        }
    }

    private static String getKeywordCrowdTallyLockKey(KeywordCrowdTally keywordCrowdTally)
    {
        // Note: We assume that the arg keywordCrowdTally is not null.
        return "KeywordCrowdTally-Processing-Lock-" + keywordCrowdTally.getGuid();
    }
    public String lockKeywordCrowdTally(KeywordCrowdTally keywordCrowdTally)
    {
        if(getCache() != null && keywordCrowdTally != null) {
            // The stored value is not important.
            String val = keywordCrowdTally.getGuid();
            getCache().put(getKeywordCrowdTallyLockKey(keywordCrowdTally), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockKeywordCrowdTally(KeywordCrowdTally keywordCrowdTally)
    {
        if(getCache() != null && keywordCrowdTally != null) {
            String val = keywordCrowdTally.getGuid();
            getCache().remove(getKeywordCrowdTallyLockKey(keywordCrowdTally));
            return val;
        } else {
            return null;
        }
    }
    public boolean isKeywordCrowdTallyLocked(KeywordCrowdTally keywordCrowdTally)
    {
        if(getCache() != null && keywordCrowdTally != null) {
            return getCache().containsKey(getKeywordCrowdTallyLockKey(keywordCrowdTally)); 
        } else {
            return false;
        }
    }

    private static String getBookmarkCrowdTallyLockKey(BookmarkCrowdTally bookmarkCrowdTally)
    {
        // Note: We assume that the arg bookmarkCrowdTally is not null.
        return "BookmarkCrowdTally-Processing-Lock-" + bookmarkCrowdTally.getGuid();
    }
    public String lockBookmarkCrowdTally(BookmarkCrowdTally bookmarkCrowdTally)
    {
        if(getCache() != null && bookmarkCrowdTally != null) {
            // The stored value is not important.
            String val = bookmarkCrowdTally.getGuid();
            getCache().put(getBookmarkCrowdTallyLockKey(bookmarkCrowdTally), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockBookmarkCrowdTally(BookmarkCrowdTally bookmarkCrowdTally)
    {
        if(getCache() != null && bookmarkCrowdTally != null) {
            String val = bookmarkCrowdTally.getGuid();
            getCache().remove(getBookmarkCrowdTallyLockKey(bookmarkCrowdTally));
            return val;
        } else {
            return null;
        }
    }
    public boolean isBookmarkCrowdTallyLocked(BookmarkCrowdTally bookmarkCrowdTally)
    {
        if(getCache() != null && bookmarkCrowdTally != null) {
            return getCache().containsKey(getBookmarkCrowdTallyLockKey(bookmarkCrowdTally)); 
        } else {
            return false;
        }
    }

    private static String getDomainInfoLockKey(DomainInfo domainInfo)
    {
        // Note: We assume that the arg domainInfo is not null.
        return "DomainInfo-Processing-Lock-" + domainInfo.getGuid();
    }
    public String lockDomainInfo(DomainInfo domainInfo)
    {
        if(getCache() != null && domainInfo != null) {
            // The stored value is not important.
            String val = domainInfo.getGuid();
            getCache().put(getDomainInfoLockKey(domainInfo), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockDomainInfo(DomainInfo domainInfo)
    {
        if(getCache() != null && domainInfo != null) {
            String val = domainInfo.getGuid();
            getCache().remove(getDomainInfoLockKey(domainInfo));
            return val;
        } else {
            return null;
        }
    }
    public boolean isDomainInfoLocked(DomainInfo domainInfo)
    {
        if(getCache() != null && domainInfo != null) {
            return getCache().containsKey(getDomainInfoLockKey(domainInfo)); 
        } else {
            return false;
        }
    }

    private static String getUrlRatingLockKey(UrlRating urlRating)
    {
        // Note: We assume that the arg urlRating is not null.
        return "UrlRating-Processing-Lock-" + urlRating.getGuid();
    }
    public String lockUrlRating(UrlRating urlRating)
    {
        if(getCache() != null && urlRating != null) {
            // The stored value is not important.
            String val = urlRating.getGuid();
            getCache().put(getUrlRatingLockKey(urlRating), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockUrlRating(UrlRating urlRating)
    {
        if(getCache() != null && urlRating != null) {
            String val = urlRating.getGuid();
            getCache().remove(getUrlRatingLockKey(urlRating));
            return val;
        } else {
            return null;
        }
    }
    public boolean isUrlRatingLocked(UrlRating urlRating)
    {
        if(getCache() != null && urlRating != null) {
            return getCache().containsKey(getUrlRatingLockKey(urlRating)); 
        } else {
            return false;
        }
    }

    private static String getUserRatingLockKey(UserRating userRating)
    {
        // Note: We assume that the arg userRating is not null.
        return "UserRating-Processing-Lock-" + userRating.getGuid();
    }
    public String lockUserRating(UserRating userRating)
    {
        if(getCache() != null && userRating != null) {
            // The stored value is not important.
            String val = userRating.getGuid();
            getCache().put(getUserRatingLockKey(userRating), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockUserRating(UserRating userRating)
    {
        if(getCache() != null && userRating != null) {
            String val = userRating.getGuid();
            getCache().remove(getUserRatingLockKey(userRating));
            return val;
        } else {
            return null;
        }
    }
    public boolean isUserRatingLocked(UserRating userRating)
    {
        if(getCache() != null && userRating != null) {
            return getCache().containsKey(getUserRatingLockKey(userRating)); 
        } else {
            return false;
        }
    }

    private static String getAbuseTagLockKey(AbuseTag abuseTag)
    {
        // Note: We assume that the arg abuseTag is not null.
        return "AbuseTag-Processing-Lock-" + abuseTag.getGuid();
    }
    public String lockAbuseTag(AbuseTag abuseTag)
    {
        if(getCache() != null && abuseTag != null) {
            // The stored value is not important.
            String val = abuseTag.getGuid();
            getCache().put(getAbuseTagLockKey(abuseTag), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockAbuseTag(AbuseTag abuseTag)
    {
        if(getCache() != null && abuseTag != null) {
            String val = abuseTag.getGuid();
            getCache().remove(getAbuseTagLockKey(abuseTag));
            return val;
        } else {
            return null;
        }
    }
    public boolean isAbuseTagLocked(AbuseTag abuseTag)
    {
        if(getCache() != null && abuseTag != null) {
            return getCache().containsKey(getAbuseTagLockKey(abuseTag)); 
        } else {
            return false;
        }
    }

    private static String getServiceInfoLockKey(ServiceInfo serviceInfo)
    {
        // Note: We assume that the arg serviceInfo is not null.
        return "ServiceInfo-Processing-Lock-" + serviceInfo.getGuid();
    }
    public String lockServiceInfo(ServiceInfo serviceInfo)
    {
        if(getCache() != null && serviceInfo != null) {
            // The stored value is not important.
            String val = serviceInfo.getGuid();
            getCache().put(getServiceInfoLockKey(serviceInfo), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockServiceInfo(ServiceInfo serviceInfo)
    {
        if(getCache() != null && serviceInfo != null) {
            String val = serviceInfo.getGuid();
            getCache().remove(getServiceInfoLockKey(serviceInfo));
            return val;
        } else {
            return null;
        }
    }
    public boolean isServiceInfoLocked(ServiceInfo serviceInfo)
    {
        if(getCache() != null && serviceInfo != null) {
            return getCache().containsKey(getServiceInfoLockKey(serviceInfo)); 
        } else {
            return false;
        }
    }

    private static String getFiveTenLockKey(FiveTen fiveTen)
    {
        // Note: We assume that the arg fiveTen is not null.
        return "FiveTen-Processing-Lock-" + fiveTen.getGuid();
    }
    public String lockFiveTen(FiveTen fiveTen)
    {
        if(getCache() != null && fiveTen != null) {
            // The stored value is not important.
            String val = fiveTen.getGuid();
            getCache().put(getFiveTenLockKey(fiveTen), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockFiveTen(FiveTen fiveTen)
    {
        if(getCache() != null && fiveTen != null) {
            String val = fiveTen.getGuid();
            getCache().remove(getFiveTenLockKey(fiveTen));
            return val;
        } else {
            return null;
        }
    }
    public boolean isFiveTenLocked(FiveTen fiveTen)
    {
        if(getCache() != null && fiveTen != null) {
            return getCache().containsKey(getFiveTenLockKey(fiveTen)); 
        } else {
            return false;
        }
    }

 
}
