package com.cannyurl.af.util;

import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.util.Locale;
import java.util.logging.Logger;
import java.util.logging.Level;


public final class HtmlTextUtil
{
    private static final Logger log = Logger.getLogger(HtmlTextUtil.class.getName());

    private HtmlTextUtil() {}


    public static String escapeForHtml(String str)
    {
        return escapeForHtml(str, false);
    }

    public static String escapeForHtml(String str, boolean convertWhiteSpace)
    {
        return escapeForHtml(str, convertWhiteSpace, false);
    }

    // Escape text to be safe for use in html.
    public static String escapeForHtml(String str, boolean convertWhiteSpace, boolean useHtmlEntities)
    {
        if (str == null) {
            return null;
        }

        final StringBuilder result = new StringBuilder();
        final StringCharacterIterator iterator = new StringCharacterIterator(str);
        char character =  iterator.current();
        while (character != CharacterIterator.DONE ){
            if (character == '<') {
                result.append("&lt;");
            } else if (character == '>') {
                result.append("&gt;");
            } else if (character == '&') {
                result.append("&amp;");
            } else if (character == '\"') {
                result.append("&quot;");
            } else if (character == '\n') {
                if(convertWhiteSpace == true) {
                    result.append("<br/>");
                } else {
                    result.append(" ");   // TBD: Just add a space... ????
                }
            }  else if (character == '\t') {
                if(convertWhiteSpace == true) {
                    result.append("&nbsp; &nbsp; ");  // Hack. Include 3~4 spaces, for now... TBD
                } else {
                    result.append(" ");   // TBD: Just add a space... ????
                    //addCharEntity(9, result);
                }
            } else {
                if(useHtmlEntities) {
                    if (character == '!') {
                        addCharEntity(33, result);
                    } else if (character == '#') {
                        addCharEntity(35, result);
                    } else if (character == '$') {
                        addCharEntity(36, result);
                    } else if (character == '%') {
                        addCharEntity(37, result);
                    } else if (character == '\'') {
                        addCharEntity(39, result);
                    } else if (character == '(') {
                        addCharEntity(40, result);
                    } else if (character == ')') {
                        addCharEntity(41, result);
                    } else if (character == '*') {
                        addCharEntity(42, result);
                    } else if (character == '+') {
                        addCharEntity(43, result);
                    } else if (character == ',') {
                        addCharEntity(44, result);
                    } else if (character == '-') {
                        addCharEntity(45, result);
                    } else if (character == '.') {
                        addCharEntity(46, result);
                    } else if (character == '/') {
                        addCharEntity(47, result);
                    } else if (character == ':') {
                        addCharEntity(58, result);
                    } else if (character == ';') {
                        addCharEntity(59, result);
                    } else if (character == '=') {
                        addCharEntity(61, result);
                    } else if (character == '?') {
                        addCharEntity(63, result);
                    } else if (character == '@') {
                        addCharEntity(64, result);
                    } else if (character == '[') {
                        addCharEntity(91, result);
                    } else if (character == '\\') {
                        addCharEntity(92, result);
                    } else if (character == ']') {
                        addCharEntity(93, result);
                    } else if (character == '^') {
                        addCharEntity(94, result);
                    } else if (character == '_') {
                        addCharEntity(95, result);
                    } else if (character == '`') {
                        addCharEntity(96, result);
                    } else if (character == '{') {
                        addCharEntity(123, result);
                    } else if (character == '|') {
                        addCharEntity(124, result);
                    } else if (character == '}') {
                        addCharEntity(125, result);
                    } else if (character == '~') {
                          addCharEntity(126, result);
                    } else {
                        result.append(character);
                    }
                } else {
                    //the char is not a special one
                    //add it to the result as is
                    result.append(character);                    
                }
            }
            character = iterator.next();
        }
        return result.toString();
    }

    private static void addCharEntity(Integer aIdx, StringBuilder strBuilder)
    {
        String padding = "";
        if( aIdx <= 9 ){
           padding = "00";
        }
        else if( aIdx <= 99 ){
          padding = "0";
        }
        else {
          //no prefix
        }
        String number = padding + aIdx.toString();
        strBuilder.append("&#" + number + ";");
    }


    public static String escapeForJavascript(String str)
    {
        return escapeForJavaStyle(str, true, true);
    }
 
    public static String escapeForJava(String str)
    {
        return escapeForJavaStyle(str, false, false);
    }
 
    private static String escapeForJavaStyle(String str, boolean escapeSingleQuote, boolean escapeForwardSlash)
    {
        if (str == null) {
            return null;
        }

        final StringBuilder result = new StringBuilder();
        int sz = str.length();
        for (int i = 0; i < sz; i++) {
            char ch = str.charAt(i);

            // handle unicode
            if (ch > 0xfff) {
                result.append("\\u" + hex(ch));
            } else if (ch > 0xff) {
                result.append("\\u0" + hex(ch));
            } else if (ch > 0x7f) {
                result.append("\\u00" + hex(ch));
            } else if (ch < 32) {
                switch (ch) {
                    case '\b' :
                        result.append('\\');
                        result.append('b');
                        break;
                    case '\n' :
                        result.append('\\');
                        result.append('n');
                        break;
                    case '\t' :
                        result.append('\\');
                        result.append('t');
                        break;
                    case '\f' :
                        result.append('\\');
                        result.append('f');
                        break;
                    case '\r' :
                        result.append('\\');
                        result.append('r');
                        break;
                    default :
                        if (ch > 0xf) {
                            result.append("\\u00" + hex(ch));
                        } else {
                            result.append("\\u000" + hex(ch));
                        }
                        break;
                }
            } else {
                switch (ch) {
                    case '\'' :
                        if (escapeSingleQuote) {
                            result.append('\\');
                        }
                        result.append('\'');
                        break;
                    case '"' :
                        result.append('\\');
                        result.append('"');
                        break;
                    case '\\' :
                        result.append('\\');
                        result.append('\\');
                        break;
                    case '/' :
                        if (escapeForwardSlash) {
                            result.append('\\');
                        }
                        result.append('/');
                        break;
                    default :
                        result.append(ch);
                        break;
                }
            }
        }
        return result.toString();
    }

    private static String hex(char ch)
    {
        return Integer.toHexString(ch).toUpperCase(Locale.ENGLISH);
    }
    
}
