package com.cannyurl.af.util;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.KeyValuePairStruct;
import com.myurldb.ws.KeyValueRelationStruct;
import com.myurldb.ws.ExternalServiceApiKeyStruct;
import com.myurldb.ws.GeoPointStruct;
import com.myurldb.ws.GeoCoordinateStruct;
import com.myurldb.ws.CellLatitudeLongitude;
import com.myurldb.ws.StreetAddressStruct;
import com.myurldb.ws.WebProfileStruct;
import com.myurldb.ws.FullNameStruct;
import com.myurldb.ws.ContactInfoStruct;
import com.myurldb.ws.ReferrerInfoStruct;
import com.myurldb.ws.GaeAppStruct;
import com.myurldb.ws.GaeUserStruct;
import com.myurldb.ws.PagerStateStruct;
import com.myurldb.ws.AppBrandStruct;
import com.myurldb.ws.ApiConsumer;
import com.myurldb.ws.AppCustomDomain;
import com.myurldb.ws.SiteCustomDomain;
import com.myurldb.ws.User;
import com.myurldb.ws.UserUsercode;
import com.myurldb.ws.HashedPasswordStruct;
import com.myurldb.ws.UserPassword;
import com.myurldb.ws.ExternalUserIdStruct;
import com.myurldb.ws.ExternalUserAuth;
import com.myurldb.ws.UserAuthState;
import com.myurldb.ws.UserResourcePermission;
import com.myurldb.ws.UserResourceProhibition;
import com.myurldb.ws.RolePermission;
import com.myurldb.ws.UserRole;
import com.myurldb.ws.AppClient;
import com.myurldb.ws.ClientUser;
import com.myurldb.ws.UserCustomDomain;
import com.myurldb.ws.PersonalSetting;
import com.myurldb.ws.ClientSetting;
import com.myurldb.ws.UserSetting;
import com.myurldb.ws.VisitorSetting;
import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.TwitterCardBase;
import com.myurldb.ws.TwitterSummaryCard;
import com.myurldb.ws.TwitterPhotoCard;
import com.myurldb.ws.TwitterGalleryCard;
import com.myurldb.ws.TwitterAppCard;
import com.myurldb.ws.TwitterPlayerCard;
import com.myurldb.ws.TwitterProductCard;
import com.myurldb.ws.ShortPassageAttribute;
import com.myurldb.ws.ShortPassage;
import com.myurldb.ws.ShortLink;
import com.myurldb.ws.GeoLink;
import com.myurldb.ws.QrCode;
import com.myurldb.ws.LinkPassphrase;
import com.myurldb.ws.LinkMessage;
import com.myurldb.ws.LinkAlbum;
import com.myurldb.ws.AlbumShortLink;
import com.myurldb.ws.FolderBase;
import com.myurldb.ws.KeywordFolder;
import com.myurldb.ws.BookmarkFolder;
import com.myurldb.ws.NavLinkBase;
import com.myurldb.ws.KeywordLink;
import com.myurldb.ws.BookmarkLink;
import com.myurldb.ws.SpeedDial;
import com.myurldb.ws.FolderImportBase;
import com.myurldb.ws.KeywordFolderImport;
import com.myurldb.ws.BookmarkFolderImport;
import com.myurldb.ws.CrowdTallyBase;
import com.myurldb.ws.KeywordCrowdTally;
import com.myurldb.ws.BookmarkCrowdTally;
import com.myurldb.ws.DomainInfo;
import com.myurldb.ws.UrlRating;
import com.myurldb.ws.UserRating;
import com.myurldb.ws.AbuseTag;
import com.myurldb.ws.HelpNotice;
import com.myurldb.ws.SiteLog;
import com.myurldb.ws.ServiceInfo;
import com.myurldb.ws.FiveTen;
import com.myurldb.ws.stub.KeyValuePairStructStub;
import com.myurldb.ws.stub.KeyValuePairStructListStub;
import com.myurldb.ws.stub.KeyValueRelationStructStub;
import com.myurldb.ws.stub.KeyValueRelationStructListStub;
import com.myurldb.ws.stub.ExternalServiceApiKeyStructStub;
import com.myurldb.ws.stub.ExternalServiceApiKeyStructListStub;
import com.myurldb.ws.stub.GeoPointStructStub;
import com.myurldb.ws.stub.GeoPointStructListStub;
import com.myurldb.ws.stub.GeoCoordinateStructStub;
import com.myurldb.ws.stub.GeoCoordinateStructListStub;
import com.myurldb.ws.stub.CellLatitudeLongitudeStub;
import com.myurldb.ws.stub.CellLatitudeLongitudeListStub;
import com.myurldb.ws.stub.StreetAddressStructStub;
import com.myurldb.ws.stub.StreetAddressStructListStub;
import com.myurldb.ws.stub.WebProfileStructStub;
import com.myurldb.ws.stub.WebProfileStructListStub;
import com.myurldb.ws.stub.FullNameStructStub;
import com.myurldb.ws.stub.FullNameStructListStub;
import com.myurldb.ws.stub.ContactInfoStructStub;
import com.myurldb.ws.stub.ContactInfoStructListStub;
import com.myurldb.ws.stub.ReferrerInfoStructStub;
import com.myurldb.ws.stub.ReferrerInfoStructListStub;
import com.myurldb.ws.stub.GaeAppStructStub;
import com.myurldb.ws.stub.GaeAppStructListStub;
import com.myurldb.ws.stub.GaeUserStructStub;
import com.myurldb.ws.stub.GaeUserStructListStub;
import com.myurldb.ws.stub.PagerStateStructStub;
import com.myurldb.ws.stub.PagerStateStructListStub;
import com.myurldb.ws.stub.AppBrandStructStub;
import com.myurldb.ws.stub.AppBrandStructListStub;
import com.myurldb.ws.stub.ApiConsumerStub;
import com.myurldb.ws.stub.ApiConsumerListStub;
import com.myurldb.ws.stub.AppCustomDomainStub;
import com.myurldb.ws.stub.AppCustomDomainListStub;
import com.myurldb.ws.stub.SiteCustomDomainStub;
import com.myurldb.ws.stub.SiteCustomDomainListStub;
import com.myurldb.ws.stub.UserStub;
import com.myurldb.ws.stub.UserListStub;
import com.myurldb.ws.stub.UserUsercodeStub;
import com.myurldb.ws.stub.UserUsercodeListStub;
import com.myurldb.ws.stub.HashedPasswordStructStub;
import com.myurldb.ws.stub.HashedPasswordStructListStub;
import com.myurldb.ws.stub.UserPasswordStub;
import com.myurldb.ws.stub.UserPasswordListStub;
import com.myurldb.ws.stub.ExternalUserIdStructStub;
import com.myurldb.ws.stub.ExternalUserIdStructListStub;
import com.myurldb.ws.stub.ExternalUserAuthStub;
import com.myurldb.ws.stub.ExternalUserAuthListStub;
import com.myurldb.ws.stub.UserAuthStateStub;
import com.myurldb.ws.stub.UserAuthStateListStub;
import com.myurldb.ws.stub.UserResourcePermissionStub;
import com.myurldb.ws.stub.UserResourcePermissionListStub;
import com.myurldb.ws.stub.UserResourceProhibitionStub;
import com.myurldb.ws.stub.UserResourceProhibitionListStub;
import com.myurldb.ws.stub.RolePermissionStub;
import com.myurldb.ws.stub.RolePermissionListStub;
import com.myurldb.ws.stub.UserRoleStub;
import com.myurldb.ws.stub.UserRoleListStub;
import com.myurldb.ws.stub.AppClientStub;
import com.myurldb.ws.stub.AppClientListStub;
import com.myurldb.ws.stub.ClientUserStub;
import com.myurldb.ws.stub.ClientUserListStub;
import com.myurldb.ws.stub.UserCustomDomainStub;
import com.myurldb.ws.stub.UserCustomDomainListStub;
import com.myurldb.ws.stub.ClientSettingStub;
import com.myurldb.ws.stub.ClientSettingListStub;
import com.myurldb.ws.stub.UserSettingStub;
import com.myurldb.ws.stub.UserSettingListStub;
import com.myurldb.ws.stub.VisitorSettingStub;
import com.myurldb.ws.stub.VisitorSettingListStub;
import com.myurldb.ws.stub.TwitterCardAppInfoStub;
import com.myurldb.ws.stub.TwitterCardAppInfoListStub;
import com.myurldb.ws.stub.TwitterCardProductDataStub;
import com.myurldb.ws.stub.TwitterCardProductDataListStub;
import com.myurldb.ws.stub.TwitterSummaryCardStub;
import com.myurldb.ws.stub.TwitterSummaryCardListStub;
import com.myurldb.ws.stub.TwitterPhotoCardStub;
import com.myurldb.ws.stub.TwitterPhotoCardListStub;
import com.myurldb.ws.stub.TwitterGalleryCardStub;
import com.myurldb.ws.stub.TwitterGalleryCardListStub;
import com.myurldb.ws.stub.TwitterAppCardStub;
import com.myurldb.ws.stub.TwitterAppCardListStub;
import com.myurldb.ws.stub.TwitterPlayerCardStub;
import com.myurldb.ws.stub.TwitterPlayerCardListStub;
import com.myurldb.ws.stub.TwitterProductCardStub;
import com.myurldb.ws.stub.TwitterProductCardListStub;
import com.myurldb.ws.stub.ShortPassageAttributeStub;
import com.myurldb.ws.stub.ShortPassageAttributeListStub;
import com.myurldb.ws.stub.ShortPassageStub;
import com.myurldb.ws.stub.ShortPassageListStub;
import com.myurldb.ws.stub.ShortLinkStub;
import com.myurldb.ws.stub.ShortLinkListStub;
import com.myurldb.ws.stub.GeoLinkStub;
import com.myurldb.ws.stub.GeoLinkListStub;
import com.myurldb.ws.stub.QrCodeStub;
import com.myurldb.ws.stub.QrCodeListStub;
import com.myurldb.ws.stub.LinkPassphraseStub;
import com.myurldb.ws.stub.LinkPassphraseListStub;
import com.myurldb.ws.stub.LinkMessageStub;
import com.myurldb.ws.stub.LinkMessageListStub;
import com.myurldb.ws.stub.LinkAlbumStub;
import com.myurldb.ws.stub.LinkAlbumListStub;
import com.myurldb.ws.stub.AlbumShortLinkStub;
import com.myurldb.ws.stub.AlbumShortLinkListStub;
import com.myurldb.ws.stub.KeywordFolderStub;
import com.myurldb.ws.stub.KeywordFolderListStub;
import com.myurldb.ws.stub.BookmarkFolderStub;
import com.myurldb.ws.stub.BookmarkFolderListStub;
import com.myurldb.ws.stub.KeywordLinkStub;
import com.myurldb.ws.stub.KeywordLinkListStub;
import com.myurldb.ws.stub.BookmarkLinkStub;
import com.myurldb.ws.stub.BookmarkLinkListStub;
import com.myurldb.ws.stub.SpeedDialStub;
import com.myurldb.ws.stub.SpeedDialListStub;
import com.myurldb.ws.stub.KeywordFolderImportStub;
import com.myurldb.ws.stub.KeywordFolderImportListStub;
import com.myurldb.ws.stub.BookmarkFolderImportStub;
import com.myurldb.ws.stub.BookmarkFolderImportListStub;
import com.myurldb.ws.stub.KeywordCrowdTallyStub;
import com.myurldb.ws.stub.KeywordCrowdTallyListStub;
import com.myurldb.ws.stub.BookmarkCrowdTallyStub;
import com.myurldb.ws.stub.BookmarkCrowdTallyListStub;
import com.myurldb.ws.stub.DomainInfoStub;
import com.myurldb.ws.stub.DomainInfoListStub;
import com.myurldb.ws.stub.UrlRatingStub;
import com.myurldb.ws.stub.UrlRatingListStub;
import com.myurldb.ws.stub.UserRatingStub;
import com.myurldb.ws.stub.UserRatingListStub;
import com.myurldb.ws.stub.AbuseTagStub;
import com.myurldb.ws.stub.AbuseTagListStub;
import com.myurldb.ws.stub.HelpNoticeStub;
import com.myurldb.ws.stub.HelpNoticeListStub;
import com.myurldb.ws.stub.SiteLogStub;
import com.myurldb.ws.stub.SiteLogListStub;
import com.myurldb.ws.stub.ServiceInfoStub;
import com.myurldb.ws.stub.ServiceInfoListStub;
import com.myurldb.ws.stub.FiveTenStub;
import com.myurldb.ws.stub.FiveTenListStub;
import com.cannyurl.af.bean.KeyValuePairStructBean;
import com.cannyurl.af.bean.KeyValueRelationStructBean;
import com.cannyurl.af.bean.ExternalServiceApiKeyStructBean;
import com.cannyurl.af.bean.GeoPointStructBean;
import com.cannyurl.af.bean.GeoCoordinateStructBean;
import com.cannyurl.af.bean.CellLatitudeLongitudeBean;
import com.cannyurl.af.bean.StreetAddressStructBean;
import com.cannyurl.af.bean.WebProfileStructBean;
import com.cannyurl.af.bean.FullNameStructBean;
import com.cannyurl.af.bean.ContactInfoStructBean;
import com.cannyurl.af.bean.ReferrerInfoStructBean;
import com.cannyurl.af.bean.GaeAppStructBean;
import com.cannyurl.af.bean.GaeUserStructBean;
import com.cannyurl.af.bean.PagerStateStructBean;
import com.cannyurl.af.bean.AppBrandStructBean;
import com.cannyurl.af.bean.ApiConsumerBean;
import com.cannyurl.af.bean.AppCustomDomainBean;
import com.cannyurl.af.bean.SiteCustomDomainBean;
import com.cannyurl.af.bean.UserBean;
import com.cannyurl.af.bean.UserUsercodeBean;
import com.cannyurl.af.bean.HashedPasswordStructBean;
import com.cannyurl.af.bean.UserPasswordBean;
import com.cannyurl.af.bean.ExternalUserIdStructBean;
import com.cannyurl.af.bean.ExternalUserAuthBean;
import com.cannyurl.af.bean.UserAuthStateBean;
import com.cannyurl.af.bean.UserResourcePermissionBean;
import com.cannyurl.af.bean.UserResourceProhibitionBean;
import com.cannyurl.af.bean.RolePermissionBean;
import com.cannyurl.af.bean.UserRoleBean;
import com.cannyurl.af.bean.AppClientBean;
import com.cannyurl.af.bean.ClientUserBean;
import com.cannyurl.af.bean.UserCustomDomainBean;
import com.cannyurl.af.bean.PersonalSettingBean;
import com.cannyurl.af.bean.ClientSettingBean;
import com.cannyurl.af.bean.UserSettingBean;
import com.cannyurl.af.bean.VisitorSettingBean;
import com.cannyurl.af.bean.TwitterCardAppInfoBean;
import com.cannyurl.af.bean.TwitterCardProductDataBean;
import com.cannyurl.af.bean.TwitterCardBaseBean;
import com.cannyurl.af.bean.TwitterSummaryCardBean;
import com.cannyurl.af.bean.TwitterPhotoCardBean;
import com.cannyurl.af.bean.TwitterGalleryCardBean;
import com.cannyurl.af.bean.TwitterAppCardBean;
import com.cannyurl.af.bean.TwitterPlayerCardBean;
import com.cannyurl.af.bean.TwitterProductCardBean;
import com.cannyurl.af.bean.ShortPassageAttributeBean;
import com.cannyurl.af.bean.ShortPassageBean;
import com.cannyurl.af.bean.ShortLinkBean;
import com.cannyurl.af.bean.GeoLinkBean;
import com.cannyurl.af.bean.QrCodeBean;
import com.cannyurl.af.bean.LinkPassphraseBean;
import com.cannyurl.af.bean.LinkMessageBean;
import com.cannyurl.af.bean.LinkAlbumBean;
import com.cannyurl.af.bean.AlbumShortLinkBean;
import com.cannyurl.af.bean.FolderBaseBean;
import com.cannyurl.af.bean.KeywordFolderBean;
import com.cannyurl.af.bean.BookmarkFolderBean;
import com.cannyurl.af.bean.NavLinkBaseBean;
import com.cannyurl.af.bean.KeywordLinkBean;
import com.cannyurl.af.bean.BookmarkLinkBean;
import com.cannyurl.af.bean.SpeedDialBean;
import com.cannyurl.af.bean.FolderImportBaseBean;
import com.cannyurl.af.bean.KeywordFolderImportBean;
import com.cannyurl.af.bean.BookmarkFolderImportBean;
import com.cannyurl.af.bean.CrowdTallyBaseBean;
import com.cannyurl.af.bean.KeywordCrowdTallyBean;
import com.cannyurl.af.bean.BookmarkCrowdTallyBean;
import com.cannyurl.af.bean.DomainInfoBean;
import com.cannyurl.af.bean.UrlRatingBean;
import com.cannyurl.af.bean.UserRatingBean;
import com.cannyurl.af.bean.AbuseTagBean;
import com.cannyurl.af.bean.HelpNoticeBean;
import com.cannyurl.af.bean.SiteLogBean;
import com.cannyurl.af.bean.ServiceInfoBean;
import com.cannyurl.af.bean.FiveTenBean;


public final class MarshalHelper
{
    private static final Logger log = Logger.getLogger(MarshalHelper.class.getName());

    // Static methods only.
    private MarshalHelper() {}

    // temporary
    public static KeyValuePairStructBean convertKeyValuePairStructToBean(KeyValuePairStruct stub)
    {
        KeyValuePairStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new KeyValuePairStructBean();
        } else if(stub instanceof KeyValuePairStructBean) {
        	bean = (KeyValuePairStructBean) stub;
        } else {
        	bean = new KeyValuePairStructBean(stub);
        }
        return bean;
    }
    public static List<KeyValuePairStructBean> convertKeyValuePairStructListToBeanList(List<KeyValuePairStruct> stubList)
    {
        List<KeyValuePairStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<KeyValuePairStruct>();
        } else {
            beanList = new ArrayList<KeyValuePairStructBean>();
            for(KeyValuePairStruct stub : stubList) {
                beanList.add(convertKeyValuePairStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<KeyValuePairStruct> convertKeyValuePairStructListStubToBeanList(KeyValuePairStructListStub listStub)
    {
        List<KeyValuePairStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<KeyValuePairStruct>();
        } else {
            beanList = new ArrayList<KeyValuePairStruct>();
            for(KeyValuePairStructStub stub : listStub.getList()) {
            	beanList.add(convertKeyValuePairStructToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static KeyValuePairStructStub convertKeyValuePairStructToStub(KeyValuePairStruct bean)
    {
        KeyValuePairStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new KeyValuePairStructStub();
        } else if(bean instanceof KeyValuePairStructStub) {
            stub = (KeyValuePairStructStub) bean;
        } else if(bean instanceof KeyValuePairStructBean && ((KeyValuePairStructBean) bean).isWrapper()) {
            stub = ((KeyValuePairStructBean) bean).getStub();
        } else {
            stub = KeyValuePairStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static KeyValueRelationStructBean convertKeyValueRelationStructToBean(KeyValueRelationStruct stub)
    {
        KeyValueRelationStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new KeyValueRelationStructBean();
        } else if(stub instanceof KeyValueRelationStructBean) {
        	bean = (KeyValueRelationStructBean) stub;
        } else {
        	bean = new KeyValueRelationStructBean(stub);
        }
        return bean;
    }
    public static List<KeyValueRelationStructBean> convertKeyValueRelationStructListToBeanList(List<KeyValueRelationStruct> stubList)
    {
        List<KeyValueRelationStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<KeyValueRelationStruct>();
        } else {
            beanList = new ArrayList<KeyValueRelationStructBean>();
            for(KeyValueRelationStruct stub : stubList) {
                beanList.add(convertKeyValueRelationStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<KeyValueRelationStruct> convertKeyValueRelationStructListStubToBeanList(KeyValueRelationStructListStub listStub)
    {
        List<KeyValueRelationStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<KeyValueRelationStruct>();
        } else {
            beanList = new ArrayList<KeyValueRelationStruct>();
            for(KeyValueRelationStructStub stub : listStub.getList()) {
            	beanList.add(convertKeyValueRelationStructToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static KeyValueRelationStructStub convertKeyValueRelationStructToStub(KeyValueRelationStruct bean)
    {
        KeyValueRelationStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new KeyValueRelationStructStub();
        } else if(bean instanceof KeyValueRelationStructStub) {
            stub = (KeyValueRelationStructStub) bean;
        } else if(bean instanceof KeyValueRelationStructBean && ((KeyValueRelationStructBean) bean).isWrapper()) {
            stub = ((KeyValueRelationStructBean) bean).getStub();
        } else {
            stub = KeyValueRelationStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static ExternalServiceApiKeyStructBean convertExternalServiceApiKeyStructToBean(ExternalServiceApiKeyStruct stub)
    {
        ExternalServiceApiKeyStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new ExternalServiceApiKeyStructBean();
        } else if(stub instanceof ExternalServiceApiKeyStructBean) {
        	bean = (ExternalServiceApiKeyStructBean) stub;
        } else {
        	bean = new ExternalServiceApiKeyStructBean(stub);
        }
        return bean;
    }
    public static List<ExternalServiceApiKeyStructBean> convertExternalServiceApiKeyStructListToBeanList(List<ExternalServiceApiKeyStruct> stubList)
    {
        List<ExternalServiceApiKeyStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<ExternalServiceApiKeyStruct>();
        } else {
            beanList = new ArrayList<ExternalServiceApiKeyStructBean>();
            for(ExternalServiceApiKeyStruct stub : stubList) {
                beanList.add(convertExternalServiceApiKeyStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<ExternalServiceApiKeyStruct> convertExternalServiceApiKeyStructListStubToBeanList(ExternalServiceApiKeyStructListStub listStub)
    {
        List<ExternalServiceApiKeyStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<ExternalServiceApiKeyStruct>();
        } else {
            beanList = new ArrayList<ExternalServiceApiKeyStruct>();
            for(ExternalServiceApiKeyStructStub stub : listStub.getList()) {
            	beanList.add(convertExternalServiceApiKeyStructToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static ExternalServiceApiKeyStructStub convertExternalServiceApiKeyStructToStub(ExternalServiceApiKeyStruct bean)
    {
        ExternalServiceApiKeyStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new ExternalServiceApiKeyStructStub();
        } else if(bean instanceof ExternalServiceApiKeyStructStub) {
            stub = (ExternalServiceApiKeyStructStub) bean;
        } else if(bean instanceof ExternalServiceApiKeyStructBean && ((ExternalServiceApiKeyStructBean) bean).isWrapper()) {
            stub = ((ExternalServiceApiKeyStructBean) bean).getStub();
        } else {
            stub = ExternalServiceApiKeyStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static GeoPointStructBean convertGeoPointStructToBean(GeoPointStruct stub)
    {
        GeoPointStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new GeoPointStructBean();
        } else if(stub instanceof GeoPointStructBean) {
        	bean = (GeoPointStructBean) stub;
        } else {
        	bean = new GeoPointStructBean(stub);
        }
        return bean;
    }
    public static List<GeoPointStructBean> convertGeoPointStructListToBeanList(List<GeoPointStruct> stubList)
    {
        List<GeoPointStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<GeoPointStruct>();
        } else {
            beanList = new ArrayList<GeoPointStructBean>();
            for(GeoPointStruct stub : stubList) {
                beanList.add(convertGeoPointStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<GeoPointStruct> convertGeoPointStructListStubToBeanList(GeoPointStructListStub listStub)
    {
        List<GeoPointStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<GeoPointStruct>();
        } else {
            beanList = new ArrayList<GeoPointStruct>();
            for(GeoPointStructStub stub : listStub.getList()) {
            	beanList.add(convertGeoPointStructToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static GeoPointStructStub convertGeoPointStructToStub(GeoPointStruct bean)
    {
        GeoPointStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new GeoPointStructStub();
        } else if(bean instanceof GeoPointStructStub) {
            stub = (GeoPointStructStub) bean;
        } else if(bean instanceof GeoPointStructBean && ((GeoPointStructBean) bean).isWrapper()) {
            stub = ((GeoPointStructBean) bean).getStub();
        } else {
            stub = GeoPointStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static GeoCoordinateStructBean convertGeoCoordinateStructToBean(GeoCoordinateStruct stub)
    {
        GeoCoordinateStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new GeoCoordinateStructBean();
        } else if(stub instanceof GeoCoordinateStructBean) {
        	bean = (GeoCoordinateStructBean) stub;
        } else {
        	bean = new GeoCoordinateStructBean(stub);
        }
        return bean;
    }
    public static List<GeoCoordinateStructBean> convertGeoCoordinateStructListToBeanList(List<GeoCoordinateStruct> stubList)
    {
        List<GeoCoordinateStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<GeoCoordinateStruct>();
        } else {
            beanList = new ArrayList<GeoCoordinateStructBean>();
            for(GeoCoordinateStruct stub : stubList) {
                beanList.add(convertGeoCoordinateStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<GeoCoordinateStruct> convertGeoCoordinateStructListStubToBeanList(GeoCoordinateStructListStub listStub)
    {
        List<GeoCoordinateStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<GeoCoordinateStruct>();
        } else {
            beanList = new ArrayList<GeoCoordinateStruct>();
            for(GeoCoordinateStructStub stub : listStub.getList()) {
            	beanList.add(convertGeoCoordinateStructToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static GeoCoordinateStructStub convertGeoCoordinateStructToStub(GeoCoordinateStruct bean)
    {
        GeoCoordinateStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new GeoCoordinateStructStub();
        } else if(bean instanceof GeoCoordinateStructStub) {
            stub = (GeoCoordinateStructStub) bean;
        } else if(bean instanceof GeoCoordinateStructBean && ((GeoCoordinateStructBean) bean).isWrapper()) {
            stub = ((GeoCoordinateStructBean) bean).getStub();
        } else {
            stub = GeoCoordinateStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static CellLatitudeLongitudeBean convertCellLatitudeLongitudeToBean(CellLatitudeLongitude stub)
    {
        CellLatitudeLongitudeBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new CellLatitudeLongitudeBean();
        } else if(stub instanceof CellLatitudeLongitudeBean) {
        	bean = (CellLatitudeLongitudeBean) stub;
        } else {
        	bean = new CellLatitudeLongitudeBean(stub);
        }
        return bean;
    }
    public static List<CellLatitudeLongitudeBean> convertCellLatitudeLongitudeListToBeanList(List<CellLatitudeLongitude> stubList)
    {
        List<CellLatitudeLongitudeBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<CellLatitudeLongitude>();
        } else {
            beanList = new ArrayList<CellLatitudeLongitudeBean>();
            for(CellLatitudeLongitude stub : stubList) {
                beanList.add(convertCellLatitudeLongitudeToBean(stub));
            }
        }
        return beanList;
    }
    public static List<CellLatitudeLongitude> convertCellLatitudeLongitudeListStubToBeanList(CellLatitudeLongitudeListStub listStub)
    {
        List<CellLatitudeLongitude> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<CellLatitudeLongitude>();
        } else {
            beanList = new ArrayList<CellLatitudeLongitude>();
            for(CellLatitudeLongitudeStub stub : listStub.getList()) {
            	beanList.add(convertCellLatitudeLongitudeToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static CellLatitudeLongitudeStub convertCellLatitudeLongitudeToStub(CellLatitudeLongitude bean)
    {
        CellLatitudeLongitudeStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new CellLatitudeLongitudeStub();
        } else if(bean instanceof CellLatitudeLongitudeStub) {
            stub = (CellLatitudeLongitudeStub) bean;
        } else if(bean instanceof CellLatitudeLongitudeBean && ((CellLatitudeLongitudeBean) bean).isWrapper()) {
            stub = ((CellLatitudeLongitudeBean) bean).getStub();
        } else {
            stub = CellLatitudeLongitudeStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static StreetAddressStructBean convertStreetAddressStructToBean(StreetAddressStruct stub)
    {
        StreetAddressStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new StreetAddressStructBean();
        } else if(stub instanceof StreetAddressStructBean) {
        	bean = (StreetAddressStructBean) stub;
        } else {
        	bean = new StreetAddressStructBean(stub);
        }
        return bean;
    }
    public static List<StreetAddressStructBean> convertStreetAddressStructListToBeanList(List<StreetAddressStruct> stubList)
    {
        List<StreetAddressStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<StreetAddressStruct>();
        } else {
            beanList = new ArrayList<StreetAddressStructBean>();
            for(StreetAddressStruct stub : stubList) {
                beanList.add(convertStreetAddressStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<StreetAddressStruct> convertStreetAddressStructListStubToBeanList(StreetAddressStructListStub listStub)
    {
        List<StreetAddressStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<StreetAddressStruct>();
        } else {
            beanList = new ArrayList<StreetAddressStruct>();
            for(StreetAddressStructStub stub : listStub.getList()) {
            	beanList.add(convertStreetAddressStructToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static StreetAddressStructStub convertStreetAddressStructToStub(StreetAddressStruct bean)
    {
        StreetAddressStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new StreetAddressStructStub();
        } else if(bean instanceof StreetAddressStructStub) {
            stub = (StreetAddressStructStub) bean;
        } else if(bean instanceof StreetAddressStructBean && ((StreetAddressStructBean) bean).isWrapper()) {
            stub = ((StreetAddressStructBean) bean).getStub();
        } else {
            stub = StreetAddressStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static WebProfileStructBean convertWebProfileStructToBean(WebProfileStruct stub)
    {
        WebProfileStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new WebProfileStructBean();
        } else if(stub instanceof WebProfileStructBean) {
        	bean = (WebProfileStructBean) stub;
        } else {
        	bean = new WebProfileStructBean(stub);
        }
        return bean;
    }
    public static List<WebProfileStructBean> convertWebProfileStructListToBeanList(List<WebProfileStruct> stubList)
    {
        List<WebProfileStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<WebProfileStruct>();
        } else {
            beanList = new ArrayList<WebProfileStructBean>();
            for(WebProfileStruct stub : stubList) {
                beanList.add(convertWebProfileStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<WebProfileStruct> convertWebProfileStructListStubToBeanList(WebProfileStructListStub listStub)
    {
        List<WebProfileStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<WebProfileStruct>();
        } else {
            beanList = new ArrayList<WebProfileStruct>();
            for(WebProfileStructStub stub : listStub.getList()) {
            	beanList.add(convertWebProfileStructToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static WebProfileStructStub convertWebProfileStructToStub(WebProfileStruct bean)
    {
        WebProfileStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new WebProfileStructStub();
        } else if(bean instanceof WebProfileStructStub) {
            stub = (WebProfileStructStub) bean;
        } else if(bean instanceof WebProfileStructBean && ((WebProfileStructBean) bean).isWrapper()) {
            stub = ((WebProfileStructBean) bean).getStub();
        } else {
            stub = WebProfileStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static FullNameStructBean convertFullNameStructToBean(FullNameStruct stub)
    {
        FullNameStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new FullNameStructBean();
        } else if(stub instanceof FullNameStructBean) {
        	bean = (FullNameStructBean) stub;
        } else {
        	bean = new FullNameStructBean(stub);
        }
        return bean;
    }
    public static List<FullNameStructBean> convertFullNameStructListToBeanList(List<FullNameStruct> stubList)
    {
        List<FullNameStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<FullNameStruct>();
        } else {
            beanList = new ArrayList<FullNameStructBean>();
            for(FullNameStruct stub : stubList) {
                beanList.add(convertFullNameStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<FullNameStruct> convertFullNameStructListStubToBeanList(FullNameStructListStub listStub)
    {
        List<FullNameStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<FullNameStruct>();
        } else {
            beanList = new ArrayList<FullNameStruct>();
            for(FullNameStructStub stub : listStub.getList()) {
            	beanList.add(convertFullNameStructToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static FullNameStructStub convertFullNameStructToStub(FullNameStruct bean)
    {
        FullNameStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new FullNameStructStub();
        } else if(bean instanceof FullNameStructStub) {
            stub = (FullNameStructStub) bean;
        } else if(bean instanceof FullNameStructBean && ((FullNameStructBean) bean).isWrapper()) {
            stub = ((FullNameStructBean) bean).getStub();
        } else {
            stub = FullNameStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static ContactInfoStructBean convertContactInfoStructToBean(ContactInfoStruct stub)
    {
        ContactInfoStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new ContactInfoStructBean();
        } else if(stub instanceof ContactInfoStructBean) {
        	bean = (ContactInfoStructBean) stub;
        } else {
        	bean = new ContactInfoStructBean(stub);
        }
        return bean;
    }
    public static List<ContactInfoStructBean> convertContactInfoStructListToBeanList(List<ContactInfoStruct> stubList)
    {
        List<ContactInfoStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<ContactInfoStruct>();
        } else {
            beanList = new ArrayList<ContactInfoStructBean>();
            for(ContactInfoStruct stub : stubList) {
                beanList.add(convertContactInfoStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<ContactInfoStruct> convertContactInfoStructListStubToBeanList(ContactInfoStructListStub listStub)
    {
        List<ContactInfoStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<ContactInfoStruct>();
        } else {
            beanList = new ArrayList<ContactInfoStruct>();
            for(ContactInfoStructStub stub : listStub.getList()) {
            	beanList.add(convertContactInfoStructToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static ContactInfoStructStub convertContactInfoStructToStub(ContactInfoStruct bean)
    {
        ContactInfoStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new ContactInfoStructStub();
        } else if(bean instanceof ContactInfoStructStub) {
            stub = (ContactInfoStructStub) bean;
        } else if(bean instanceof ContactInfoStructBean && ((ContactInfoStructBean) bean).isWrapper()) {
            stub = ((ContactInfoStructBean) bean).getStub();
        } else {
            stub = ContactInfoStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static ReferrerInfoStructBean convertReferrerInfoStructToBean(ReferrerInfoStruct stub)
    {
        ReferrerInfoStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new ReferrerInfoStructBean();
        } else if(stub instanceof ReferrerInfoStructBean) {
        	bean = (ReferrerInfoStructBean) stub;
        } else {
        	bean = new ReferrerInfoStructBean(stub);
        }
        return bean;
    }
    public static List<ReferrerInfoStructBean> convertReferrerInfoStructListToBeanList(List<ReferrerInfoStruct> stubList)
    {
        List<ReferrerInfoStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<ReferrerInfoStruct>();
        } else {
            beanList = new ArrayList<ReferrerInfoStructBean>();
            for(ReferrerInfoStruct stub : stubList) {
                beanList.add(convertReferrerInfoStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<ReferrerInfoStruct> convertReferrerInfoStructListStubToBeanList(ReferrerInfoStructListStub listStub)
    {
        List<ReferrerInfoStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<ReferrerInfoStruct>();
        } else {
            beanList = new ArrayList<ReferrerInfoStruct>();
            for(ReferrerInfoStructStub stub : listStub.getList()) {
            	beanList.add(convertReferrerInfoStructToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static ReferrerInfoStructStub convertReferrerInfoStructToStub(ReferrerInfoStruct bean)
    {
        ReferrerInfoStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new ReferrerInfoStructStub();
        } else if(bean instanceof ReferrerInfoStructStub) {
            stub = (ReferrerInfoStructStub) bean;
        } else if(bean instanceof ReferrerInfoStructBean && ((ReferrerInfoStructBean) bean).isWrapper()) {
            stub = ((ReferrerInfoStructBean) bean).getStub();
        } else {
            stub = ReferrerInfoStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static GaeAppStructBean convertGaeAppStructToBean(GaeAppStruct stub)
    {
        GaeAppStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new GaeAppStructBean();
        } else if(stub instanceof GaeAppStructBean) {
        	bean = (GaeAppStructBean) stub;
        } else {
        	bean = new GaeAppStructBean(stub);
        }
        return bean;
    }
    public static List<GaeAppStructBean> convertGaeAppStructListToBeanList(List<GaeAppStruct> stubList)
    {
        List<GaeAppStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<GaeAppStruct>();
        } else {
            beanList = new ArrayList<GaeAppStructBean>();
            for(GaeAppStruct stub : stubList) {
                beanList.add(convertGaeAppStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<GaeAppStruct> convertGaeAppStructListStubToBeanList(GaeAppStructListStub listStub)
    {
        List<GaeAppStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<GaeAppStruct>();
        } else {
            beanList = new ArrayList<GaeAppStruct>();
            for(GaeAppStructStub stub : listStub.getList()) {
            	beanList.add(convertGaeAppStructToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static GaeAppStructStub convertGaeAppStructToStub(GaeAppStruct bean)
    {
        GaeAppStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new GaeAppStructStub();
        } else if(bean instanceof GaeAppStructStub) {
            stub = (GaeAppStructStub) bean;
        } else if(bean instanceof GaeAppStructBean && ((GaeAppStructBean) bean).isWrapper()) {
            stub = ((GaeAppStructBean) bean).getStub();
        } else {
            stub = GaeAppStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static GaeUserStructBean convertGaeUserStructToBean(GaeUserStruct stub)
    {
        GaeUserStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new GaeUserStructBean();
        } else if(stub instanceof GaeUserStructBean) {
        	bean = (GaeUserStructBean) stub;
        } else {
        	bean = new GaeUserStructBean(stub);
        }
        return bean;
    }
    public static List<GaeUserStructBean> convertGaeUserStructListToBeanList(List<GaeUserStruct> stubList)
    {
        List<GaeUserStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<GaeUserStruct>();
        } else {
            beanList = new ArrayList<GaeUserStructBean>();
            for(GaeUserStruct stub : stubList) {
                beanList.add(convertGaeUserStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<GaeUserStruct> convertGaeUserStructListStubToBeanList(GaeUserStructListStub listStub)
    {
        List<GaeUserStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<GaeUserStruct>();
        } else {
            beanList = new ArrayList<GaeUserStruct>();
            for(GaeUserStructStub stub : listStub.getList()) {
            	beanList.add(convertGaeUserStructToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static GaeUserStructStub convertGaeUserStructToStub(GaeUserStruct bean)
    {
        GaeUserStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new GaeUserStructStub();
        } else if(bean instanceof GaeUserStructStub) {
            stub = (GaeUserStructStub) bean;
        } else if(bean instanceof GaeUserStructBean && ((GaeUserStructBean) bean).isWrapper()) {
            stub = ((GaeUserStructBean) bean).getStub();
        } else {
            stub = GaeUserStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static PagerStateStructBean convertPagerStateStructToBean(PagerStateStruct stub)
    {
        PagerStateStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new PagerStateStructBean();
        } else if(stub instanceof PagerStateStructBean) {
        	bean = (PagerStateStructBean) stub;
        } else {
        	bean = new PagerStateStructBean(stub);
        }
        return bean;
    }
    public static List<PagerStateStructBean> convertPagerStateStructListToBeanList(List<PagerStateStruct> stubList)
    {
        List<PagerStateStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<PagerStateStruct>();
        } else {
            beanList = new ArrayList<PagerStateStructBean>();
            for(PagerStateStruct stub : stubList) {
                beanList.add(convertPagerStateStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<PagerStateStruct> convertPagerStateStructListStubToBeanList(PagerStateStructListStub listStub)
    {
        List<PagerStateStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<PagerStateStruct>();
        } else {
            beanList = new ArrayList<PagerStateStruct>();
            for(PagerStateStructStub stub : listStub.getList()) {
            	beanList.add(convertPagerStateStructToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static PagerStateStructStub convertPagerStateStructToStub(PagerStateStruct bean)
    {
        PagerStateStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new PagerStateStructStub();
        } else if(bean instanceof PagerStateStructStub) {
            stub = (PagerStateStructStub) bean;
        } else if(bean instanceof PagerStateStructBean && ((PagerStateStructBean) bean).isWrapper()) {
            stub = ((PagerStateStructBean) bean).getStub();
        } else {
            stub = PagerStateStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static AppBrandStructBean convertAppBrandStructToBean(AppBrandStruct stub)
    {
        AppBrandStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new AppBrandStructBean();
        } else if(stub instanceof AppBrandStructBean) {
        	bean = (AppBrandStructBean) stub;
        } else {
        	bean = new AppBrandStructBean(stub);
        }
        return bean;
    }
    public static List<AppBrandStructBean> convertAppBrandStructListToBeanList(List<AppBrandStruct> stubList)
    {
        List<AppBrandStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<AppBrandStruct>();
        } else {
            beanList = new ArrayList<AppBrandStructBean>();
            for(AppBrandStruct stub : stubList) {
                beanList.add(convertAppBrandStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<AppBrandStruct> convertAppBrandStructListStubToBeanList(AppBrandStructListStub listStub)
    {
        List<AppBrandStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<AppBrandStruct>();
        } else {
            beanList = new ArrayList<AppBrandStruct>();
            for(AppBrandStructStub stub : listStub.getList()) {
            	beanList.add(convertAppBrandStructToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static AppBrandStructStub convertAppBrandStructToStub(AppBrandStruct bean)
    {
        AppBrandStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new AppBrandStructStub();
        } else if(bean instanceof AppBrandStructStub) {
            stub = (AppBrandStructStub) bean;
        } else if(bean instanceof AppBrandStructBean && ((AppBrandStructBean) bean).isWrapper()) {
            stub = ((AppBrandStructBean) bean).getStub();
        } else {
            stub = AppBrandStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static ApiConsumerBean convertApiConsumerToBean(ApiConsumer stub)
    {
        ApiConsumerBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new ApiConsumerBean();
        } else if(stub instanceof ApiConsumerBean) {
        	bean = (ApiConsumerBean) stub;
        } else {
        	bean = new ApiConsumerBean(stub);
        }
        return bean;
    }
    public static List<ApiConsumerBean> convertApiConsumerListToBeanList(List<ApiConsumer> stubList)
    {
        List<ApiConsumerBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<ApiConsumer>();
        } else {
            beanList = new ArrayList<ApiConsumerBean>();
            for(ApiConsumer stub : stubList) {
                beanList.add(convertApiConsumerToBean(stub));
            }
        }
        return beanList;
    }
    public static List<ApiConsumer> convertApiConsumerListStubToBeanList(ApiConsumerListStub listStub)
    {
        List<ApiConsumer> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<ApiConsumer>();
        } else {
            beanList = new ArrayList<ApiConsumer>();
            for(ApiConsumerStub stub : listStub.getList()) {
            	beanList.add(convertApiConsumerToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static ApiConsumerStub convertApiConsumerToStub(ApiConsumer bean)
    {
        ApiConsumerStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new ApiConsumerStub();
        } else if(bean instanceof ApiConsumerStub) {
            stub = (ApiConsumerStub) bean;
        } else if(bean instanceof ApiConsumerBean && ((ApiConsumerBean) bean).isWrapper()) {
            stub = ((ApiConsumerBean) bean).getStub();
        } else {
            stub = ApiConsumerStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static AppCustomDomainBean convertAppCustomDomainToBean(AppCustomDomain stub)
    {
        AppCustomDomainBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new AppCustomDomainBean();
        } else if(stub instanceof AppCustomDomainBean) {
        	bean = (AppCustomDomainBean) stub;
        } else {
        	bean = new AppCustomDomainBean(stub);
        }
        return bean;
    }
    public static List<AppCustomDomainBean> convertAppCustomDomainListToBeanList(List<AppCustomDomain> stubList)
    {
        List<AppCustomDomainBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<AppCustomDomain>();
        } else {
            beanList = new ArrayList<AppCustomDomainBean>();
            for(AppCustomDomain stub : stubList) {
                beanList.add(convertAppCustomDomainToBean(stub));
            }
        }
        return beanList;
    }
    public static List<AppCustomDomain> convertAppCustomDomainListStubToBeanList(AppCustomDomainListStub listStub)
    {
        List<AppCustomDomain> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<AppCustomDomain>();
        } else {
            beanList = new ArrayList<AppCustomDomain>();
            for(AppCustomDomainStub stub : listStub.getList()) {
            	beanList.add(convertAppCustomDomainToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static AppCustomDomainStub convertAppCustomDomainToStub(AppCustomDomain bean)
    {
        AppCustomDomainStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new AppCustomDomainStub();
        } else if(bean instanceof AppCustomDomainStub) {
            stub = (AppCustomDomainStub) bean;
        } else if(bean instanceof AppCustomDomainBean && ((AppCustomDomainBean) bean).isWrapper()) {
            stub = ((AppCustomDomainBean) bean).getStub();
        } else {
            stub = AppCustomDomainStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static SiteCustomDomainBean convertSiteCustomDomainToBean(SiteCustomDomain stub)
    {
        SiteCustomDomainBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new SiteCustomDomainBean();
        } else if(stub instanceof SiteCustomDomainBean) {
        	bean = (SiteCustomDomainBean) stub;
        } else {
        	bean = new SiteCustomDomainBean(stub);
        }
        return bean;
    }
    public static List<SiteCustomDomainBean> convertSiteCustomDomainListToBeanList(List<SiteCustomDomain> stubList)
    {
        List<SiteCustomDomainBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<SiteCustomDomain>();
        } else {
            beanList = new ArrayList<SiteCustomDomainBean>();
            for(SiteCustomDomain stub : stubList) {
                beanList.add(convertSiteCustomDomainToBean(stub));
            }
        }
        return beanList;
    }
    public static List<SiteCustomDomain> convertSiteCustomDomainListStubToBeanList(SiteCustomDomainListStub listStub)
    {
        List<SiteCustomDomain> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<SiteCustomDomain>();
        } else {
            beanList = new ArrayList<SiteCustomDomain>();
            for(SiteCustomDomainStub stub : listStub.getList()) {
            	beanList.add(convertSiteCustomDomainToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static SiteCustomDomainStub convertSiteCustomDomainToStub(SiteCustomDomain bean)
    {
        SiteCustomDomainStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new SiteCustomDomainStub();
        } else if(bean instanceof SiteCustomDomainStub) {
            stub = (SiteCustomDomainStub) bean;
        } else if(bean instanceof SiteCustomDomainBean && ((SiteCustomDomainBean) bean).isWrapper()) {
            stub = ((SiteCustomDomainBean) bean).getStub();
        } else {
            stub = SiteCustomDomainStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static UserBean convertUserToBean(User stub)
    {
        UserBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new UserBean();
        } else if(stub instanceof UserBean) {
        	bean = (UserBean) stub;
        } else {
        	bean = new UserBean(stub);
        }
        return bean;
    }
    public static List<UserBean> convertUserListToBeanList(List<User> stubList)
    {
        List<UserBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<User>();
        } else {
            beanList = new ArrayList<UserBean>();
            for(User stub : stubList) {
                beanList.add(convertUserToBean(stub));
            }
        }
        return beanList;
    }
    public static List<User> convertUserListStubToBeanList(UserListStub listStub)
    {
        List<User> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<User>();
        } else {
            beanList = new ArrayList<User>();
            for(UserStub stub : listStub.getList()) {
            	beanList.add(convertUserToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static UserStub convertUserToStub(User bean)
    {
        UserStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new UserStub();
        } else if(bean instanceof UserStub) {
            stub = (UserStub) bean;
        } else if(bean instanceof UserBean && ((UserBean) bean).isWrapper()) {
            stub = ((UserBean) bean).getStub();
        } else {
            stub = UserStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static UserUsercodeBean convertUserUsercodeToBean(UserUsercode stub)
    {
        UserUsercodeBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new UserUsercodeBean();
        } else if(stub instanceof UserUsercodeBean) {
        	bean = (UserUsercodeBean) stub;
        } else {
        	bean = new UserUsercodeBean(stub);
        }
        return bean;
    }
    public static List<UserUsercodeBean> convertUserUsercodeListToBeanList(List<UserUsercode> stubList)
    {
        List<UserUsercodeBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<UserUsercode>();
        } else {
            beanList = new ArrayList<UserUsercodeBean>();
            for(UserUsercode stub : stubList) {
                beanList.add(convertUserUsercodeToBean(stub));
            }
        }
        return beanList;
    }
    public static List<UserUsercode> convertUserUsercodeListStubToBeanList(UserUsercodeListStub listStub)
    {
        List<UserUsercode> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<UserUsercode>();
        } else {
            beanList = new ArrayList<UserUsercode>();
            for(UserUsercodeStub stub : listStub.getList()) {
            	beanList.add(convertUserUsercodeToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static UserUsercodeStub convertUserUsercodeToStub(UserUsercode bean)
    {
        UserUsercodeStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new UserUsercodeStub();
        } else if(bean instanceof UserUsercodeStub) {
            stub = (UserUsercodeStub) bean;
        } else if(bean instanceof UserUsercodeBean && ((UserUsercodeBean) bean).isWrapper()) {
            stub = ((UserUsercodeBean) bean).getStub();
        } else {
            stub = UserUsercodeStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static HashedPasswordStructBean convertHashedPasswordStructToBean(HashedPasswordStruct stub)
    {
        HashedPasswordStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new HashedPasswordStructBean();
        } else if(stub instanceof HashedPasswordStructBean) {
        	bean = (HashedPasswordStructBean) stub;
        } else {
        	bean = new HashedPasswordStructBean(stub);
        }
        return bean;
    }
    public static List<HashedPasswordStructBean> convertHashedPasswordStructListToBeanList(List<HashedPasswordStruct> stubList)
    {
        List<HashedPasswordStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<HashedPasswordStruct>();
        } else {
            beanList = new ArrayList<HashedPasswordStructBean>();
            for(HashedPasswordStruct stub : stubList) {
                beanList.add(convertHashedPasswordStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<HashedPasswordStruct> convertHashedPasswordStructListStubToBeanList(HashedPasswordStructListStub listStub)
    {
        List<HashedPasswordStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<HashedPasswordStruct>();
        } else {
            beanList = new ArrayList<HashedPasswordStruct>();
            for(HashedPasswordStructStub stub : listStub.getList()) {
            	beanList.add(convertHashedPasswordStructToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static HashedPasswordStructStub convertHashedPasswordStructToStub(HashedPasswordStruct bean)
    {
        HashedPasswordStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new HashedPasswordStructStub();
        } else if(bean instanceof HashedPasswordStructStub) {
            stub = (HashedPasswordStructStub) bean;
        } else if(bean instanceof HashedPasswordStructBean && ((HashedPasswordStructBean) bean).isWrapper()) {
            stub = ((HashedPasswordStructBean) bean).getStub();
        } else {
            stub = HashedPasswordStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static UserPasswordBean convertUserPasswordToBean(UserPassword stub)
    {
        UserPasswordBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new UserPasswordBean();
        } else if(stub instanceof UserPasswordBean) {
        	bean = (UserPasswordBean) stub;
        } else {
        	bean = new UserPasswordBean(stub);
        }
        return bean;
    }
    public static List<UserPasswordBean> convertUserPasswordListToBeanList(List<UserPassword> stubList)
    {
        List<UserPasswordBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<UserPassword>();
        } else {
            beanList = new ArrayList<UserPasswordBean>();
            for(UserPassword stub : stubList) {
                beanList.add(convertUserPasswordToBean(stub));
            }
        }
        return beanList;
    }
    public static List<UserPassword> convertUserPasswordListStubToBeanList(UserPasswordListStub listStub)
    {
        List<UserPassword> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<UserPassword>();
        } else {
            beanList = new ArrayList<UserPassword>();
            for(UserPasswordStub stub : listStub.getList()) {
            	beanList.add(convertUserPasswordToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static UserPasswordStub convertUserPasswordToStub(UserPassword bean)
    {
        UserPasswordStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new UserPasswordStub();
        } else if(bean instanceof UserPasswordStub) {
            stub = (UserPasswordStub) bean;
        } else if(bean instanceof UserPasswordBean && ((UserPasswordBean) bean).isWrapper()) {
            stub = ((UserPasswordBean) bean).getStub();
        } else {
            stub = UserPasswordStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static ExternalUserIdStructBean convertExternalUserIdStructToBean(ExternalUserIdStruct stub)
    {
        ExternalUserIdStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new ExternalUserIdStructBean();
        } else if(stub instanceof ExternalUserIdStructBean) {
        	bean = (ExternalUserIdStructBean) stub;
        } else {
        	bean = new ExternalUserIdStructBean(stub);
        }
        return bean;
    }
    public static List<ExternalUserIdStructBean> convertExternalUserIdStructListToBeanList(List<ExternalUserIdStruct> stubList)
    {
        List<ExternalUserIdStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<ExternalUserIdStruct>();
        } else {
            beanList = new ArrayList<ExternalUserIdStructBean>();
            for(ExternalUserIdStruct stub : stubList) {
                beanList.add(convertExternalUserIdStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<ExternalUserIdStruct> convertExternalUserIdStructListStubToBeanList(ExternalUserIdStructListStub listStub)
    {
        List<ExternalUserIdStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<ExternalUserIdStruct>();
        } else {
            beanList = new ArrayList<ExternalUserIdStruct>();
            for(ExternalUserIdStructStub stub : listStub.getList()) {
            	beanList.add(convertExternalUserIdStructToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static ExternalUserIdStructStub convertExternalUserIdStructToStub(ExternalUserIdStruct bean)
    {
        ExternalUserIdStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new ExternalUserIdStructStub();
        } else if(bean instanceof ExternalUserIdStructStub) {
            stub = (ExternalUserIdStructStub) bean;
        } else if(bean instanceof ExternalUserIdStructBean && ((ExternalUserIdStructBean) bean).isWrapper()) {
            stub = ((ExternalUserIdStructBean) bean).getStub();
        } else {
            stub = ExternalUserIdStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static ExternalUserAuthBean convertExternalUserAuthToBean(ExternalUserAuth stub)
    {
        ExternalUserAuthBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new ExternalUserAuthBean();
        } else if(stub instanceof ExternalUserAuthBean) {
        	bean = (ExternalUserAuthBean) stub;
        } else {
        	bean = new ExternalUserAuthBean(stub);
        }
        return bean;
    }
    public static List<ExternalUserAuthBean> convertExternalUserAuthListToBeanList(List<ExternalUserAuth> stubList)
    {
        List<ExternalUserAuthBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<ExternalUserAuth>();
        } else {
            beanList = new ArrayList<ExternalUserAuthBean>();
            for(ExternalUserAuth stub : stubList) {
                beanList.add(convertExternalUserAuthToBean(stub));
            }
        }
        return beanList;
    }
    public static List<ExternalUserAuth> convertExternalUserAuthListStubToBeanList(ExternalUserAuthListStub listStub)
    {
        List<ExternalUserAuth> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<ExternalUserAuth>();
        } else {
            beanList = new ArrayList<ExternalUserAuth>();
            for(ExternalUserAuthStub stub : listStub.getList()) {
            	beanList.add(convertExternalUserAuthToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static ExternalUserAuthStub convertExternalUserAuthToStub(ExternalUserAuth bean)
    {
        ExternalUserAuthStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new ExternalUserAuthStub();
        } else if(bean instanceof ExternalUserAuthStub) {
            stub = (ExternalUserAuthStub) bean;
        } else if(bean instanceof ExternalUserAuthBean && ((ExternalUserAuthBean) bean).isWrapper()) {
            stub = ((ExternalUserAuthBean) bean).getStub();
        } else {
            stub = ExternalUserAuthStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static UserAuthStateBean convertUserAuthStateToBean(UserAuthState stub)
    {
        UserAuthStateBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new UserAuthStateBean();
        } else if(stub instanceof UserAuthStateBean) {
        	bean = (UserAuthStateBean) stub;
        } else {
        	bean = new UserAuthStateBean(stub);
        }
        return bean;
    }
    public static List<UserAuthStateBean> convertUserAuthStateListToBeanList(List<UserAuthState> stubList)
    {
        List<UserAuthStateBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<UserAuthState>();
        } else {
            beanList = new ArrayList<UserAuthStateBean>();
            for(UserAuthState stub : stubList) {
                beanList.add(convertUserAuthStateToBean(stub));
            }
        }
        return beanList;
    }
    public static List<UserAuthState> convertUserAuthStateListStubToBeanList(UserAuthStateListStub listStub)
    {
        List<UserAuthState> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<UserAuthState>();
        } else {
            beanList = new ArrayList<UserAuthState>();
            for(UserAuthStateStub stub : listStub.getList()) {
            	beanList.add(convertUserAuthStateToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static UserAuthStateStub convertUserAuthStateToStub(UserAuthState bean)
    {
        UserAuthStateStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new UserAuthStateStub();
        } else if(bean instanceof UserAuthStateStub) {
            stub = (UserAuthStateStub) bean;
        } else if(bean instanceof UserAuthStateBean && ((UserAuthStateBean) bean).isWrapper()) {
            stub = ((UserAuthStateBean) bean).getStub();
        } else {
            stub = UserAuthStateStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static UserResourcePermissionBean convertUserResourcePermissionToBean(UserResourcePermission stub)
    {
        UserResourcePermissionBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new UserResourcePermissionBean();
        } else if(stub instanceof UserResourcePermissionBean) {
        	bean = (UserResourcePermissionBean) stub;
        } else {
        	bean = new UserResourcePermissionBean(stub);
        }
        return bean;
    }
    public static List<UserResourcePermissionBean> convertUserResourcePermissionListToBeanList(List<UserResourcePermission> stubList)
    {
        List<UserResourcePermissionBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<UserResourcePermission>();
        } else {
            beanList = new ArrayList<UserResourcePermissionBean>();
            for(UserResourcePermission stub : stubList) {
                beanList.add(convertUserResourcePermissionToBean(stub));
            }
        }
        return beanList;
    }
    public static List<UserResourcePermission> convertUserResourcePermissionListStubToBeanList(UserResourcePermissionListStub listStub)
    {
        List<UserResourcePermission> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<UserResourcePermission>();
        } else {
            beanList = new ArrayList<UserResourcePermission>();
            for(UserResourcePermissionStub stub : listStub.getList()) {
            	beanList.add(convertUserResourcePermissionToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static UserResourcePermissionStub convertUserResourcePermissionToStub(UserResourcePermission bean)
    {
        UserResourcePermissionStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new UserResourcePermissionStub();
        } else if(bean instanceof UserResourcePermissionStub) {
            stub = (UserResourcePermissionStub) bean;
        } else if(bean instanceof UserResourcePermissionBean && ((UserResourcePermissionBean) bean).isWrapper()) {
            stub = ((UserResourcePermissionBean) bean).getStub();
        } else {
            stub = UserResourcePermissionStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static UserResourceProhibitionBean convertUserResourceProhibitionToBean(UserResourceProhibition stub)
    {
        UserResourceProhibitionBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new UserResourceProhibitionBean();
        } else if(stub instanceof UserResourceProhibitionBean) {
        	bean = (UserResourceProhibitionBean) stub;
        } else {
        	bean = new UserResourceProhibitionBean(stub);
        }
        return bean;
    }
    public static List<UserResourceProhibitionBean> convertUserResourceProhibitionListToBeanList(List<UserResourceProhibition> stubList)
    {
        List<UserResourceProhibitionBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<UserResourceProhibition>();
        } else {
            beanList = new ArrayList<UserResourceProhibitionBean>();
            for(UserResourceProhibition stub : stubList) {
                beanList.add(convertUserResourceProhibitionToBean(stub));
            }
        }
        return beanList;
    }
    public static List<UserResourceProhibition> convertUserResourceProhibitionListStubToBeanList(UserResourceProhibitionListStub listStub)
    {
        List<UserResourceProhibition> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<UserResourceProhibition>();
        } else {
            beanList = new ArrayList<UserResourceProhibition>();
            for(UserResourceProhibitionStub stub : listStub.getList()) {
            	beanList.add(convertUserResourceProhibitionToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static UserResourceProhibitionStub convertUserResourceProhibitionToStub(UserResourceProhibition bean)
    {
        UserResourceProhibitionStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new UserResourceProhibitionStub();
        } else if(bean instanceof UserResourceProhibitionStub) {
            stub = (UserResourceProhibitionStub) bean;
        } else if(bean instanceof UserResourceProhibitionBean && ((UserResourceProhibitionBean) bean).isWrapper()) {
            stub = ((UserResourceProhibitionBean) bean).getStub();
        } else {
            stub = UserResourceProhibitionStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static RolePermissionBean convertRolePermissionToBean(RolePermission stub)
    {
        RolePermissionBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new RolePermissionBean();
        } else if(stub instanceof RolePermissionBean) {
        	bean = (RolePermissionBean) stub;
        } else {
        	bean = new RolePermissionBean(stub);
        }
        return bean;
    }
    public static List<RolePermissionBean> convertRolePermissionListToBeanList(List<RolePermission> stubList)
    {
        List<RolePermissionBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<RolePermission>();
        } else {
            beanList = new ArrayList<RolePermissionBean>();
            for(RolePermission stub : stubList) {
                beanList.add(convertRolePermissionToBean(stub));
            }
        }
        return beanList;
    }
    public static List<RolePermission> convertRolePermissionListStubToBeanList(RolePermissionListStub listStub)
    {
        List<RolePermission> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<RolePermission>();
        } else {
            beanList = new ArrayList<RolePermission>();
            for(RolePermissionStub stub : listStub.getList()) {
            	beanList.add(convertRolePermissionToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static RolePermissionStub convertRolePermissionToStub(RolePermission bean)
    {
        RolePermissionStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new RolePermissionStub();
        } else if(bean instanceof RolePermissionStub) {
            stub = (RolePermissionStub) bean;
        } else if(bean instanceof RolePermissionBean && ((RolePermissionBean) bean).isWrapper()) {
            stub = ((RolePermissionBean) bean).getStub();
        } else {
            stub = RolePermissionStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static UserRoleBean convertUserRoleToBean(UserRole stub)
    {
        UserRoleBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new UserRoleBean();
        } else if(stub instanceof UserRoleBean) {
        	bean = (UserRoleBean) stub;
        } else {
        	bean = new UserRoleBean(stub);
        }
        return bean;
    }
    public static List<UserRoleBean> convertUserRoleListToBeanList(List<UserRole> stubList)
    {
        List<UserRoleBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<UserRole>();
        } else {
            beanList = new ArrayList<UserRoleBean>();
            for(UserRole stub : stubList) {
                beanList.add(convertUserRoleToBean(stub));
            }
        }
        return beanList;
    }
    public static List<UserRole> convertUserRoleListStubToBeanList(UserRoleListStub listStub)
    {
        List<UserRole> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<UserRole>();
        } else {
            beanList = new ArrayList<UserRole>();
            for(UserRoleStub stub : listStub.getList()) {
            	beanList.add(convertUserRoleToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static UserRoleStub convertUserRoleToStub(UserRole bean)
    {
        UserRoleStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new UserRoleStub();
        } else if(bean instanceof UserRoleStub) {
            stub = (UserRoleStub) bean;
        } else if(bean instanceof UserRoleBean && ((UserRoleBean) bean).isWrapper()) {
            stub = ((UserRoleBean) bean).getStub();
        } else {
            stub = UserRoleStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static AppClientBean convertAppClientToBean(AppClient stub)
    {
        AppClientBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new AppClientBean();
        } else if(stub instanceof AppClientBean) {
        	bean = (AppClientBean) stub;
        } else {
        	bean = new AppClientBean(stub);
        }
        return bean;
    }
    public static List<AppClientBean> convertAppClientListToBeanList(List<AppClient> stubList)
    {
        List<AppClientBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<AppClient>();
        } else {
            beanList = new ArrayList<AppClientBean>();
            for(AppClient stub : stubList) {
                beanList.add(convertAppClientToBean(stub));
            }
        }
        return beanList;
    }
    public static List<AppClient> convertAppClientListStubToBeanList(AppClientListStub listStub)
    {
        List<AppClient> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<AppClient>();
        } else {
            beanList = new ArrayList<AppClient>();
            for(AppClientStub stub : listStub.getList()) {
            	beanList.add(convertAppClientToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static AppClientStub convertAppClientToStub(AppClient bean)
    {
        AppClientStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new AppClientStub();
        } else if(bean instanceof AppClientStub) {
            stub = (AppClientStub) bean;
        } else if(bean instanceof AppClientBean && ((AppClientBean) bean).isWrapper()) {
            stub = ((AppClientBean) bean).getStub();
        } else {
            stub = AppClientStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static ClientUserBean convertClientUserToBean(ClientUser stub)
    {
        ClientUserBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new ClientUserBean();
        } else if(stub instanceof ClientUserBean) {
        	bean = (ClientUserBean) stub;
        } else {
        	bean = new ClientUserBean(stub);
        }
        return bean;
    }
    public static List<ClientUserBean> convertClientUserListToBeanList(List<ClientUser> stubList)
    {
        List<ClientUserBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<ClientUser>();
        } else {
            beanList = new ArrayList<ClientUserBean>();
            for(ClientUser stub : stubList) {
                beanList.add(convertClientUserToBean(stub));
            }
        }
        return beanList;
    }
    public static List<ClientUser> convertClientUserListStubToBeanList(ClientUserListStub listStub)
    {
        List<ClientUser> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<ClientUser>();
        } else {
            beanList = new ArrayList<ClientUser>();
            for(ClientUserStub stub : listStub.getList()) {
            	beanList.add(convertClientUserToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static ClientUserStub convertClientUserToStub(ClientUser bean)
    {
        ClientUserStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new ClientUserStub();
        } else if(bean instanceof ClientUserStub) {
            stub = (ClientUserStub) bean;
        } else if(bean instanceof ClientUserBean && ((ClientUserBean) bean).isWrapper()) {
            stub = ((ClientUserBean) bean).getStub();
        } else {
            stub = ClientUserStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static UserCustomDomainBean convertUserCustomDomainToBean(UserCustomDomain stub)
    {
        UserCustomDomainBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new UserCustomDomainBean();
        } else if(stub instanceof UserCustomDomainBean) {
        	bean = (UserCustomDomainBean) stub;
        } else {
        	bean = new UserCustomDomainBean(stub);
        }
        return bean;
    }
    public static List<UserCustomDomainBean> convertUserCustomDomainListToBeanList(List<UserCustomDomain> stubList)
    {
        List<UserCustomDomainBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<UserCustomDomain>();
        } else {
            beanList = new ArrayList<UserCustomDomainBean>();
            for(UserCustomDomain stub : stubList) {
                beanList.add(convertUserCustomDomainToBean(stub));
            }
        }
        return beanList;
    }
    public static List<UserCustomDomain> convertUserCustomDomainListStubToBeanList(UserCustomDomainListStub listStub)
    {
        List<UserCustomDomain> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<UserCustomDomain>();
        } else {
            beanList = new ArrayList<UserCustomDomain>();
            for(UserCustomDomainStub stub : listStub.getList()) {
            	beanList.add(convertUserCustomDomainToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static UserCustomDomainStub convertUserCustomDomainToStub(UserCustomDomain bean)
    {
        UserCustomDomainStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new UserCustomDomainStub();
        } else if(bean instanceof UserCustomDomainStub) {
            stub = (UserCustomDomainStub) bean;
        } else if(bean instanceof UserCustomDomainBean && ((UserCustomDomainBean) bean).isWrapper()) {
            stub = ((UserCustomDomainBean) bean).getStub();
        } else {
            stub = UserCustomDomainStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static ClientSettingBean convertClientSettingToBean(ClientSetting stub)
    {
        ClientSettingBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new ClientSettingBean();
        } else if(stub instanceof ClientSettingBean) {
        	bean = (ClientSettingBean) stub;
        } else {
        	bean = new ClientSettingBean(stub);
        }
        return bean;
    }
    public static List<ClientSettingBean> convertClientSettingListToBeanList(List<ClientSetting> stubList)
    {
        List<ClientSettingBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<ClientSetting>();
        } else {
            beanList = new ArrayList<ClientSettingBean>();
            for(ClientSetting stub : stubList) {
                beanList.add(convertClientSettingToBean(stub));
            }
        }
        return beanList;
    }
    public static List<ClientSetting> convertClientSettingListStubToBeanList(ClientSettingListStub listStub)
    {
        List<ClientSetting> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<ClientSetting>();
        } else {
            beanList = new ArrayList<ClientSetting>();
            for(ClientSettingStub stub : listStub.getList()) {
            	beanList.add(convertClientSettingToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static ClientSettingStub convertClientSettingToStub(ClientSetting bean)
    {
        ClientSettingStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new ClientSettingStub();
        } else if(bean instanceof ClientSettingStub) {
            stub = (ClientSettingStub) bean;
        } else if(bean instanceof ClientSettingBean && ((ClientSettingBean) bean).isWrapper()) {
            stub = ((ClientSettingBean) bean).getStub();
        } else {
            stub = ClientSettingStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static UserSettingBean convertUserSettingToBean(UserSetting stub)
    {
        UserSettingBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new UserSettingBean();
        } else if(stub instanceof UserSettingBean) {
        	bean = (UserSettingBean) stub;
        } else {
        	bean = new UserSettingBean(stub);
        }
        return bean;
    }
    public static List<UserSettingBean> convertUserSettingListToBeanList(List<UserSetting> stubList)
    {
        List<UserSettingBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<UserSetting>();
        } else {
            beanList = new ArrayList<UserSettingBean>();
            for(UserSetting stub : stubList) {
                beanList.add(convertUserSettingToBean(stub));
            }
        }
        return beanList;
    }
    public static List<UserSetting> convertUserSettingListStubToBeanList(UserSettingListStub listStub)
    {
        List<UserSetting> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<UserSetting>();
        } else {
            beanList = new ArrayList<UserSetting>();
            for(UserSettingStub stub : listStub.getList()) {
            	beanList.add(convertUserSettingToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static UserSettingStub convertUserSettingToStub(UserSetting bean)
    {
        UserSettingStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new UserSettingStub();
        } else if(bean instanceof UserSettingStub) {
            stub = (UserSettingStub) bean;
        } else if(bean instanceof UserSettingBean && ((UserSettingBean) bean).isWrapper()) {
            stub = ((UserSettingBean) bean).getStub();
        } else {
            stub = UserSettingStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static VisitorSettingBean convertVisitorSettingToBean(VisitorSetting stub)
    {
        VisitorSettingBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new VisitorSettingBean();
        } else if(stub instanceof VisitorSettingBean) {
        	bean = (VisitorSettingBean) stub;
        } else {
        	bean = new VisitorSettingBean(stub);
        }
        return bean;
    }
    public static List<VisitorSettingBean> convertVisitorSettingListToBeanList(List<VisitorSetting> stubList)
    {
        List<VisitorSettingBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<VisitorSetting>();
        } else {
            beanList = new ArrayList<VisitorSettingBean>();
            for(VisitorSetting stub : stubList) {
                beanList.add(convertVisitorSettingToBean(stub));
            }
        }
        return beanList;
    }
    public static List<VisitorSetting> convertVisitorSettingListStubToBeanList(VisitorSettingListStub listStub)
    {
        List<VisitorSetting> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<VisitorSetting>();
        } else {
            beanList = new ArrayList<VisitorSetting>();
            for(VisitorSettingStub stub : listStub.getList()) {
            	beanList.add(convertVisitorSettingToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static VisitorSettingStub convertVisitorSettingToStub(VisitorSetting bean)
    {
        VisitorSettingStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new VisitorSettingStub();
        } else if(bean instanceof VisitorSettingStub) {
            stub = (VisitorSettingStub) bean;
        } else if(bean instanceof VisitorSettingBean && ((VisitorSettingBean) bean).isWrapper()) {
            stub = ((VisitorSettingBean) bean).getStub();
        } else {
            stub = VisitorSettingStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static TwitterCardAppInfoBean convertTwitterCardAppInfoToBean(TwitterCardAppInfo stub)
    {
        TwitterCardAppInfoBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new TwitterCardAppInfoBean();
        } else if(stub instanceof TwitterCardAppInfoBean) {
        	bean = (TwitterCardAppInfoBean) stub;
        } else {
        	bean = new TwitterCardAppInfoBean(stub);
        }
        return bean;
    }
    public static List<TwitterCardAppInfoBean> convertTwitterCardAppInfoListToBeanList(List<TwitterCardAppInfo> stubList)
    {
        List<TwitterCardAppInfoBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<TwitterCardAppInfo>();
        } else {
            beanList = new ArrayList<TwitterCardAppInfoBean>();
            for(TwitterCardAppInfo stub : stubList) {
                beanList.add(convertTwitterCardAppInfoToBean(stub));
            }
        }
        return beanList;
    }
    public static List<TwitterCardAppInfo> convertTwitterCardAppInfoListStubToBeanList(TwitterCardAppInfoListStub listStub)
    {
        List<TwitterCardAppInfo> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<TwitterCardAppInfo>();
        } else {
            beanList = new ArrayList<TwitterCardAppInfo>();
            for(TwitterCardAppInfoStub stub : listStub.getList()) {
            	beanList.add(convertTwitterCardAppInfoToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static TwitterCardAppInfoStub convertTwitterCardAppInfoToStub(TwitterCardAppInfo bean)
    {
        TwitterCardAppInfoStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new TwitterCardAppInfoStub();
        } else if(bean instanceof TwitterCardAppInfoStub) {
            stub = (TwitterCardAppInfoStub) bean;
        } else if(bean instanceof TwitterCardAppInfoBean && ((TwitterCardAppInfoBean) bean).isWrapper()) {
            stub = ((TwitterCardAppInfoBean) bean).getStub();
        } else {
            stub = TwitterCardAppInfoStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static TwitterCardProductDataBean convertTwitterCardProductDataToBean(TwitterCardProductData stub)
    {
        TwitterCardProductDataBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new TwitterCardProductDataBean();
        } else if(stub instanceof TwitterCardProductDataBean) {
        	bean = (TwitterCardProductDataBean) stub;
        } else {
        	bean = new TwitterCardProductDataBean(stub);
        }
        return bean;
    }
    public static List<TwitterCardProductDataBean> convertTwitterCardProductDataListToBeanList(List<TwitterCardProductData> stubList)
    {
        List<TwitterCardProductDataBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<TwitterCardProductData>();
        } else {
            beanList = new ArrayList<TwitterCardProductDataBean>();
            for(TwitterCardProductData stub : stubList) {
                beanList.add(convertTwitterCardProductDataToBean(stub));
            }
        }
        return beanList;
    }
    public static List<TwitterCardProductData> convertTwitterCardProductDataListStubToBeanList(TwitterCardProductDataListStub listStub)
    {
        List<TwitterCardProductData> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<TwitterCardProductData>();
        } else {
            beanList = new ArrayList<TwitterCardProductData>();
            for(TwitterCardProductDataStub stub : listStub.getList()) {
            	beanList.add(convertTwitterCardProductDataToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static TwitterCardProductDataStub convertTwitterCardProductDataToStub(TwitterCardProductData bean)
    {
        TwitterCardProductDataStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new TwitterCardProductDataStub();
        } else if(bean instanceof TwitterCardProductDataStub) {
            stub = (TwitterCardProductDataStub) bean;
        } else if(bean instanceof TwitterCardProductDataBean && ((TwitterCardProductDataBean) bean).isWrapper()) {
            stub = ((TwitterCardProductDataBean) bean).getStub();
        } else {
            stub = TwitterCardProductDataStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static TwitterSummaryCardBean convertTwitterSummaryCardToBean(TwitterSummaryCard stub)
    {
        TwitterSummaryCardBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new TwitterSummaryCardBean();
        } else if(stub instanceof TwitterSummaryCardBean) {
        	bean = (TwitterSummaryCardBean) stub;
        } else {
        	bean = new TwitterSummaryCardBean(stub);
        }
        return bean;
    }
    public static List<TwitterSummaryCardBean> convertTwitterSummaryCardListToBeanList(List<TwitterSummaryCard> stubList)
    {
        List<TwitterSummaryCardBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<TwitterSummaryCard>();
        } else {
            beanList = new ArrayList<TwitterSummaryCardBean>();
            for(TwitterSummaryCard stub : stubList) {
                beanList.add(convertTwitterSummaryCardToBean(stub));
            }
        }
        return beanList;
    }
    public static List<TwitterSummaryCard> convertTwitterSummaryCardListStubToBeanList(TwitterSummaryCardListStub listStub)
    {
        List<TwitterSummaryCard> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<TwitterSummaryCard>();
        } else {
            beanList = new ArrayList<TwitterSummaryCard>();
            for(TwitterSummaryCardStub stub : listStub.getList()) {
            	beanList.add(convertTwitterSummaryCardToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static TwitterSummaryCardStub convertTwitterSummaryCardToStub(TwitterSummaryCard bean)
    {
        TwitterSummaryCardStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new TwitterSummaryCardStub();
        } else if(bean instanceof TwitterSummaryCardStub) {
            stub = (TwitterSummaryCardStub) bean;
        } else if(bean instanceof TwitterSummaryCardBean && ((TwitterSummaryCardBean) bean).isWrapper()) {
            stub = ((TwitterSummaryCardBean) bean).getStub();
        } else {
            stub = TwitterSummaryCardStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static TwitterPhotoCardBean convertTwitterPhotoCardToBean(TwitterPhotoCard stub)
    {
        TwitterPhotoCardBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new TwitterPhotoCardBean();
        } else if(stub instanceof TwitterPhotoCardBean) {
        	bean = (TwitterPhotoCardBean) stub;
        } else {
        	bean = new TwitterPhotoCardBean(stub);
        }
        return bean;
    }
    public static List<TwitterPhotoCardBean> convertTwitterPhotoCardListToBeanList(List<TwitterPhotoCard> stubList)
    {
        List<TwitterPhotoCardBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<TwitterPhotoCard>();
        } else {
            beanList = new ArrayList<TwitterPhotoCardBean>();
            for(TwitterPhotoCard stub : stubList) {
                beanList.add(convertTwitterPhotoCardToBean(stub));
            }
        }
        return beanList;
    }
    public static List<TwitterPhotoCard> convertTwitterPhotoCardListStubToBeanList(TwitterPhotoCardListStub listStub)
    {
        List<TwitterPhotoCard> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<TwitterPhotoCard>();
        } else {
            beanList = new ArrayList<TwitterPhotoCard>();
            for(TwitterPhotoCardStub stub : listStub.getList()) {
            	beanList.add(convertTwitterPhotoCardToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static TwitterPhotoCardStub convertTwitterPhotoCardToStub(TwitterPhotoCard bean)
    {
        TwitterPhotoCardStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new TwitterPhotoCardStub();
        } else if(bean instanceof TwitterPhotoCardStub) {
            stub = (TwitterPhotoCardStub) bean;
        } else if(bean instanceof TwitterPhotoCardBean && ((TwitterPhotoCardBean) bean).isWrapper()) {
            stub = ((TwitterPhotoCardBean) bean).getStub();
        } else {
            stub = TwitterPhotoCardStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static TwitterGalleryCardBean convertTwitterGalleryCardToBean(TwitterGalleryCard stub)
    {
        TwitterGalleryCardBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new TwitterGalleryCardBean();
        } else if(stub instanceof TwitterGalleryCardBean) {
        	bean = (TwitterGalleryCardBean) stub;
        } else {
        	bean = new TwitterGalleryCardBean(stub);
        }
        return bean;
    }
    public static List<TwitterGalleryCardBean> convertTwitterGalleryCardListToBeanList(List<TwitterGalleryCard> stubList)
    {
        List<TwitterGalleryCardBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<TwitterGalleryCard>();
        } else {
            beanList = new ArrayList<TwitterGalleryCardBean>();
            for(TwitterGalleryCard stub : stubList) {
                beanList.add(convertTwitterGalleryCardToBean(stub));
            }
        }
        return beanList;
    }
    public static List<TwitterGalleryCard> convertTwitterGalleryCardListStubToBeanList(TwitterGalleryCardListStub listStub)
    {
        List<TwitterGalleryCard> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<TwitterGalleryCard>();
        } else {
            beanList = new ArrayList<TwitterGalleryCard>();
            for(TwitterGalleryCardStub stub : listStub.getList()) {
            	beanList.add(convertTwitterGalleryCardToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static TwitterGalleryCardStub convertTwitterGalleryCardToStub(TwitterGalleryCard bean)
    {
        TwitterGalleryCardStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new TwitterGalleryCardStub();
        } else if(bean instanceof TwitterGalleryCardStub) {
            stub = (TwitterGalleryCardStub) bean;
        } else if(bean instanceof TwitterGalleryCardBean && ((TwitterGalleryCardBean) bean).isWrapper()) {
            stub = ((TwitterGalleryCardBean) bean).getStub();
        } else {
            stub = TwitterGalleryCardStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static TwitterAppCardBean convertTwitterAppCardToBean(TwitterAppCard stub)
    {
        TwitterAppCardBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new TwitterAppCardBean();
        } else if(stub instanceof TwitterAppCardBean) {
        	bean = (TwitterAppCardBean) stub;
        } else {
        	bean = new TwitterAppCardBean(stub);
        }
        return bean;
    }
    public static List<TwitterAppCardBean> convertTwitterAppCardListToBeanList(List<TwitterAppCard> stubList)
    {
        List<TwitterAppCardBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<TwitterAppCard>();
        } else {
            beanList = new ArrayList<TwitterAppCardBean>();
            for(TwitterAppCard stub : stubList) {
                beanList.add(convertTwitterAppCardToBean(stub));
            }
        }
        return beanList;
    }
    public static List<TwitterAppCard> convertTwitterAppCardListStubToBeanList(TwitterAppCardListStub listStub)
    {
        List<TwitterAppCard> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<TwitterAppCard>();
        } else {
            beanList = new ArrayList<TwitterAppCard>();
            for(TwitterAppCardStub stub : listStub.getList()) {
            	beanList.add(convertTwitterAppCardToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static TwitterAppCardStub convertTwitterAppCardToStub(TwitterAppCard bean)
    {
        TwitterAppCardStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new TwitterAppCardStub();
        } else if(bean instanceof TwitterAppCardStub) {
            stub = (TwitterAppCardStub) bean;
        } else if(bean instanceof TwitterAppCardBean && ((TwitterAppCardBean) bean).isWrapper()) {
            stub = ((TwitterAppCardBean) bean).getStub();
        } else {
            stub = TwitterAppCardStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static TwitterPlayerCardBean convertTwitterPlayerCardToBean(TwitterPlayerCard stub)
    {
        TwitterPlayerCardBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new TwitterPlayerCardBean();
        } else if(stub instanceof TwitterPlayerCardBean) {
        	bean = (TwitterPlayerCardBean) stub;
        } else {
        	bean = new TwitterPlayerCardBean(stub);
        }
        return bean;
    }
    public static List<TwitterPlayerCardBean> convertTwitterPlayerCardListToBeanList(List<TwitterPlayerCard> stubList)
    {
        List<TwitterPlayerCardBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<TwitterPlayerCard>();
        } else {
            beanList = new ArrayList<TwitterPlayerCardBean>();
            for(TwitterPlayerCard stub : stubList) {
                beanList.add(convertTwitterPlayerCardToBean(stub));
            }
        }
        return beanList;
    }
    public static List<TwitterPlayerCard> convertTwitterPlayerCardListStubToBeanList(TwitterPlayerCardListStub listStub)
    {
        List<TwitterPlayerCard> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<TwitterPlayerCard>();
        } else {
            beanList = new ArrayList<TwitterPlayerCard>();
            for(TwitterPlayerCardStub stub : listStub.getList()) {
            	beanList.add(convertTwitterPlayerCardToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static TwitterPlayerCardStub convertTwitterPlayerCardToStub(TwitterPlayerCard bean)
    {
        TwitterPlayerCardStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new TwitterPlayerCardStub();
        } else if(bean instanceof TwitterPlayerCardStub) {
            stub = (TwitterPlayerCardStub) bean;
        } else if(bean instanceof TwitterPlayerCardBean && ((TwitterPlayerCardBean) bean).isWrapper()) {
            stub = ((TwitterPlayerCardBean) bean).getStub();
        } else {
            stub = TwitterPlayerCardStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static TwitterProductCardBean convertTwitterProductCardToBean(TwitterProductCard stub)
    {
        TwitterProductCardBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new TwitterProductCardBean();
        } else if(stub instanceof TwitterProductCardBean) {
        	bean = (TwitterProductCardBean) stub;
        } else {
        	bean = new TwitterProductCardBean(stub);
        }
        return bean;
    }
    public static List<TwitterProductCardBean> convertTwitterProductCardListToBeanList(List<TwitterProductCard> stubList)
    {
        List<TwitterProductCardBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<TwitterProductCard>();
        } else {
            beanList = new ArrayList<TwitterProductCardBean>();
            for(TwitterProductCard stub : stubList) {
                beanList.add(convertTwitterProductCardToBean(stub));
            }
        }
        return beanList;
    }
    public static List<TwitterProductCard> convertTwitterProductCardListStubToBeanList(TwitterProductCardListStub listStub)
    {
        List<TwitterProductCard> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<TwitterProductCard>();
        } else {
            beanList = new ArrayList<TwitterProductCard>();
            for(TwitterProductCardStub stub : listStub.getList()) {
            	beanList.add(convertTwitterProductCardToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static TwitterProductCardStub convertTwitterProductCardToStub(TwitterProductCard bean)
    {
        TwitterProductCardStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new TwitterProductCardStub();
        } else if(bean instanceof TwitterProductCardStub) {
            stub = (TwitterProductCardStub) bean;
        } else if(bean instanceof TwitterProductCardBean && ((TwitterProductCardBean) bean).isWrapper()) {
            stub = ((TwitterProductCardBean) bean).getStub();
        } else {
            stub = TwitterProductCardStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static ShortPassageAttributeBean convertShortPassageAttributeToBean(ShortPassageAttribute stub)
    {
        ShortPassageAttributeBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new ShortPassageAttributeBean();
        } else if(stub instanceof ShortPassageAttributeBean) {
        	bean = (ShortPassageAttributeBean) stub;
        } else {
        	bean = new ShortPassageAttributeBean(stub);
        }
        return bean;
    }
    public static List<ShortPassageAttributeBean> convertShortPassageAttributeListToBeanList(List<ShortPassageAttribute> stubList)
    {
        List<ShortPassageAttributeBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<ShortPassageAttribute>();
        } else {
            beanList = new ArrayList<ShortPassageAttributeBean>();
            for(ShortPassageAttribute stub : stubList) {
                beanList.add(convertShortPassageAttributeToBean(stub));
            }
        }
        return beanList;
    }
    public static List<ShortPassageAttribute> convertShortPassageAttributeListStubToBeanList(ShortPassageAttributeListStub listStub)
    {
        List<ShortPassageAttribute> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<ShortPassageAttribute>();
        } else {
            beanList = new ArrayList<ShortPassageAttribute>();
            for(ShortPassageAttributeStub stub : listStub.getList()) {
            	beanList.add(convertShortPassageAttributeToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static ShortPassageAttributeStub convertShortPassageAttributeToStub(ShortPassageAttribute bean)
    {
        ShortPassageAttributeStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new ShortPassageAttributeStub();
        } else if(bean instanceof ShortPassageAttributeStub) {
            stub = (ShortPassageAttributeStub) bean;
        } else if(bean instanceof ShortPassageAttributeBean && ((ShortPassageAttributeBean) bean).isWrapper()) {
            stub = ((ShortPassageAttributeBean) bean).getStub();
        } else {
            stub = ShortPassageAttributeStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static ShortPassageBean convertShortPassageToBean(ShortPassage stub)
    {
        ShortPassageBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new ShortPassageBean();
        } else if(stub instanceof ShortPassageBean) {
        	bean = (ShortPassageBean) stub;
        } else {
        	bean = new ShortPassageBean(stub);
        }
        return bean;
    }
    public static List<ShortPassageBean> convertShortPassageListToBeanList(List<ShortPassage> stubList)
    {
        List<ShortPassageBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<ShortPassage>();
        } else {
            beanList = new ArrayList<ShortPassageBean>();
            for(ShortPassage stub : stubList) {
                beanList.add(convertShortPassageToBean(stub));
            }
        }
        return beanList;
    }
    public static List<ShortPassage> convertShortPassageListStubToBeanList(ShortPassageListStub listStub)
    {
        List<ShortPassage> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<ShortPassage>();
        } else {
            beanList = new ArrayList<ShortPassage>();
            for(ShortPassageStub stub : listStub.getList()) {
            	beanList.add(convertShortPassageToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static ShortPassageStub convertShortPassageToStub(ShortPassage bean)
    {
        ShortPassageStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new ShortPassageStub();
        } else if(bean instanceof ShortPassageStub) {
            stub = (ShortPassageStub) bean;
        } else if(bean instanceof ShortPassageBean && ((ShortPassageBean) bean).isWrapper()) {
            stub = ((ShortPassageBean) bean).getStub();
        } else {
            stub = ShortPassageStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static ShortLinkBean convertShortLinkToBean(ShortLink stub)
    {
        ShortLinkBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new ShortLinkBean();
        } else if(stub instanceof ShortLinkBean) {
        	bean = (ShortLinkBean) stub;
        } else {
        	bean = new ShortLinkBean(stub);
        }
        return bean;
    }
    public static List<ShortLinkBean> convertShortLinkListToBeanList(List<ShortLink> stubList)
    {
        List<ShortLinkBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<ShortLink>();
        } else {
            beanList = new ArrayList<ShortLinkBean>();
            for(ShortLink stub : stubList) {
                beanList.add(convertShortLinkToBean(stub));
            }
        }
        return beanList;
    }
    public static List<ShortLink> convertShortLinkListStubToBeanList(ShortLinkListStub listStub)
    {
        List<ShortLink> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<ShortLink>();
        } else {
            beanList = new ArrayList<ShortLink>();
            for(ShortLinkStub stub : listStub.getList()) {
            	beanList.add(convertShortLinkToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static ShortLinkStub convertShortLinkToStub(ShortLink bean)
    {
        ShortLinkStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new ShortLinkStub();
        } else if(bean instanceof ShortLinkStub) {
            stub = (ShortLinkStub) bean;
        } else if(bean instanceof ShortLinkBean && ((ShortLinkBean) bean).isWrapper()) {
            stub = ((ShortLinkBean) bean).getStub();
        } else {
            stub = ShortLinkStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static GeoLinkBean convertGeoLinkToBean(GeoLink stub)
    {
        GeoLinkBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new GeoLinkBean();
        } else if(stub instanceof GeoLinkBean) {
        	bean = (GeoLinkBean) stub;
        } else {
        	bean = new GeoLinkBean(stub);
        }
        return bean;
    }
    public static List<GeoLinkBean> convertGeoLinkListToBeanList(List<GeoLink> stubList)
    {
        List<GeoLinkBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<GeoLink>();
        } else {
            beanList = new ArrayList<GeoLinkBean>();
            for(GeoLink stub : stubList) {
                beanList.add(convertGeoLinkToBean(stub));
            }
        }
        return beanList;
    }
    public static List<GeoLink> convertGeoLinkListStubToBeanList(GeoLinkListStub listStub)
    {
        List<GeoLink> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<GeoLink>();
        } else {
            beanList = new ArrayList<GeoLink>();
            for(GeoLinkStub stub : listStub.getList()) {
            	beanList.add(convertGeoLinkToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static GeoLinkStub convertGeoLinkToStub(GeoLink bean)
    {
        GeoLinkStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new GeoLinkStub();
        } else if(bean instanceof GeoLinkStub) {
            stub = (GeoLinkStub) bean;
        } else if(bean instanceof GeoLinkBean && ((GeoLinkBean) bean).isWrapper()) {
            stub = ((GeoLinkBean) bean).getStub();
        } else {
            stub = GeoLinkStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static QrCodeBean convertQrCodeToBean(QrCode stub)
    {
        QrCodeBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new QrCodeBean();
        } else if(stub instanceof QrCodeBean) {
        	bean = (QrCodeBean) stub;
        } else {
        	bean = new QrCodeBean(stub);
        }
        return bean;
    }
    public static List<QrCodeBean> convertQrCodeListToBeanList(List<QrCode> stubList)
    {
        List<QrCodeBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<QrCode>();
        } else {
            beanList = new ArrayList<QrCodeBean>();
            for(QrCode stub : stubList) {
                beanList.add(convertQrCodeToBean(stub));
            }
        }
        return beanList;
    }
    public static List<QrCode> convertQrCodeListStubToBeanList(QrCodeListStub listStub)
    {
        List<QrCode> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<QrCode>();
        } else {
            beanList = new ArrayList<QrCode>();
            for(QrCodeStub stub : listStub.getList()) {
            	beanList.add(convertQrCodeToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static QrCodeStub convertQrCodeToStub(QrCode bean)
    {
        QrCodeStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new QrCodeStub();
        } else if(bean instanceof QrCodeStub) {
            stub = (QrCodeStub) bean;
        } else if(bean instanceof QrCodeBean && ((QrCodeBean) bean).isWrapper()) {
            stub = ((QrCodeBean) bean).getStub();
        } else {
            stub = QrCodeStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static LinkPassphraseBean convertLinkPassphraseToBean(LinkPassphrase stub)
    {
        LinkPassphraseBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new LinkPassphraseBean();
        } else if(stub instanceof LinkPassphraseBean) {
        	bean = (LinkPassphraseBean) stub;
        } else {
        	bean = new LinkPassphraseBean(stub);
        }
        return bean;
    }
    public static List<LinkPassphraseBean> convertLinkPassphraseListToBeanList(List<LinkPassphrase> stubList)
    {
        List<LinkPassphraseBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<LinkPassphrase>();
        } else {
            beanList = new ArrayList<LinkPassphraseBean>();
            for(LinkPassphrase stub : stubList) {
                beanList.add(convertLinkPassphraseToBean(stub));
            }
        }
        return beanList;
    }
    public static List<LinkPassphrase> convertLinkPassphraseListStubToBeanList(LinkPassphraseListStub listStub)
    {
        List<LinkPassphrase> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<LinkPassphrase>();
        } else {
            beanList = new ArrayList<LinkPassphrase>();
            for(LinkPassphraseStub stub : listStub.getList()) {
            	beanList.add(convertLinkPassphraseToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static LinkPassphraseStub convertLinkPassphraseToStub(LinkPassphrase bean)
    {
        LinkPassphraseStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new LinkPassphraseStub();
        } else if(bean instanceof LinkPassphraseStub) {
            stub = (LinkPassphraseStub) bean;
        } else if(bean instanceof LinkPassphraseBean && ((LinkPassphraseBean) bean).isWrapper()) {
            stub = ((LinkPassphraseBean) bean).getStub();
        } else {
            stub = LinkPassphraseStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static LinkMessageBean convertLinkMessageToBean(LinkMessage stub)
    {
        LinkMessageBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new LinkMessageBean();
        } else if(stub instanceof LinkMessageBean) {
        	bean = (LinkMessageBean) stub;
        } else {
        	bean = new LinkMessageBean(stub);
        }
        return bean;
    }
    public static List<LinkMessageBean> convertLinkMessageListToBeanList(List<LinkMessage> stubList)
    {
        List<LinkMessageBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<LinkMessage>();
        } else {
            beanList = new ArrayList<LinkMessageBean>();
            for(LinkMessage stub : stubList) {
                beanList.add(convertLinkMessageToBean(stub));
            }
        }
        return beanList;
    }
    public static List<LinkMessage> convertLinkMessageListStubToBeanList(LinkMessageListStub listStub)
    {
        List<LinkMessage> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<LinkMessage>();
        } else {
            beanList = new ArrayList<LinkMessage>();
            for(LinkMessageStub stub : listStub.getList()) {
            	beanList.add(convertLinkMessageToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static LinkMessageStub convertLinkMessageToStub(LinkMessage bean)
    {
        LinkMessageStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new LinkMessageStub();
        } else if(bean instanceof LinkMessageStub) {
            stub = (LinkMessageStub) bean;
        } else if(bean instanceof LinkMessageBean && ((LinkMessageBean) bean).isWrapper()) {
            stub = ((LinkMessageBean) bean).getStub();
        } else {
            stub = LinkMessageStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static LinkAlbumBean convertLinkAlbumToBean(LinkAlbum stub)
    {
        LinkAlbumBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new LinkAlbumBean();
        } else if(stub instanceof LinkAlbumBean) {
        	bean = (LinkAlbumBean) stub;
        } else {
        	bean = new LinkAlbumBean(stub);
        }
        return bean;
    }
    public static List<LinkAlbumBean> convertLinkAlbumListToBeanList(List<LinkAlbum> stubList)
    {
        List<LinkAlbumBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<LinkAlbum>();
        } else {
            beanList = new ArrayList<LinkAlbumBean>();
            for(LinkAlbum stub : stubList) {
                beanList.add(convertLinkAlbumToBean(stub));
            }
        }
        return beanList;
    }
    public static List<LinkAlbum> convertLinkAlbumListStubToBeanList(LinkAlbumListStub listStub)
    {
        List<LinkAlbum> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<LinkAlbum>();
        } else {
            beanList = new ArrayList<LinkAlbum>();
            for(LinkAlbumStub stub : listStub.getList()) {
            	beanList.add(convertLinkAlbumToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static LinkAlbumStub convertLinkAlbumToStub(LinkAlbum bean)
    {
        LinkAlbumStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new LinkAlbumStub();
        } else if(bean instanceof LinkAlbumStub) {
            stub = (LinkAlbumStub) bean;
        } else if(bean instanceof LinkAlbumBean && ((LinkAlbumBean) bean).isWrapper()) {
            stub = ((LinkAlbumBean) bean).getStub();
        } else {
            stub = LinkAlbumStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static AlbumShortLinkBean convertAlbumShortLinkToBean(AlbumShortLink stub)
    {
        AlbumShortLinkBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new AlbumShortLinkBean();
        } else if(stub instanceof AlbumShortLinkBean) {
        	bean = (AlbumShortLinkBean) stub;
        } else {
        	bean = new AlbumShortLinkBean(stub);
        }
        return bean;
    }
    public static List<AlbumShortLinkBean> convertAlbumShortLinkListToBeanList(List<AlbumShortLink> stubList)
    {
        List<AlbumShortLinkBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<AlbumShortLink>();
        } else {
            beanList = new ArrayList<AlbumShortLinkBean>();
            for(AlbumShortLink stub : stubList) {
                beanList.add(convertAlbumShortLinkToBean(stub));
            }
        }
        return beanList;
    }
    public static List<AlbumShortLink> convertAlbumShortLinkListStubToBeanList(AlbumShortLinkListStub listStub)
    {
        List<AlbumShortLink> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<AlbumShortLink>();
        } else {
            beanList = new ArrayList<AlbumShortLink>();
            for(AlbumShortLinkStub stub : listStub.getList()) {
            	beanList.add(convertAlbumShortLinkToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static AlbumShortLinkStub convertAlbumShortLinkToStub(AlbumShortLink bean)
    {
        AlbumShortLinkStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new AlbumShortLinkStub();
        } else if(bean instanceof AlbumShortLinkStub) {
            stub = (AlbumShortLinkStub) bean;
        } else if(bean instanceof AlbumShortLinkBean && ((AlbumShortLinkBean) bean).isWrapper()) {
            stub = ((AlbumShortLinkBean) bean).getStub();
        } else {
            stub = AlbumShortLinkStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static KeywordFolderBean convertKeywordFolderToBean(KeywordFolder stub)
    {
        KeywordFolderBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new KeywordFolderBean();
        } else if(stub instanceof KeywordFolderBean) {
        	bean = (KeywordFolderBean) stub;
        } else {
        	bean = new KeywordFolderBean(stub);
        }
        return bean;
    }
    public static List<KeywordFolderBean> convertKeywordFolderListToBeanList(List<KeywordFolder> stubList)
    {
        List<KeywordFolderBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<KeywordFolder>();
        } else {
            beanList = new ArrayList<KeywordFolderBean>();
            for(KeywordFolder stub : stubList) {
                beanList.add(convertKeywordFolderToBean(stub));
            }
        }
        return beanList;
    }
    public static List<KeywordFolder> convertKeywordFolderListStubToBeanList(KeywordFolderListStub listStub)
    {
        List<KeywordFolder> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<KeywordFolder>();
        } else {
            beanList = new ArrayList<KeywordFolder>();
            for(KeywordFolderStub stub : listStub.getList()) {
            	beanList.add(convertKeywordFolderToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static KeywordFolderStub convertKeywordFolderToStub(KeywordFolder bean)
    {
        KeywordFolderStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new KeywordFolderStub();
        } else if(bean instanceof KeywordFolderStub) {
            stub = (KeywordFolderStub) bean;
        } else if(bean instanceof KeywordFolderBean && ((KeywordFolderBean) bean).isWrapper()) {
            stub = ((KeywordFolderBean) bean).getStub();
        } else {
            stub = KeywordFolderStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static BookmarkFolderBean convertBookmarkFolderToBean(BookmarkFolder stub)
    {
        BookmarkFolderBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new BookmarkFolderBean();
        } else if(stub instanceof BookmarkFolderBean) {
        	bean = (BookmarkFolderBean) stub;
        } else {
        	bean = new BookmarkFolderBean(stub);
        }
        return bean;
    }
    public static List<BookmarkFolderBean> convertBookmarkFolderListToBeanList(List<BookmarkFolder> stubList)
    {
        List<BookmarkFolderBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<BookmarkFolder>();
        } else {
            beanList = new ArrayList<BookmarkFolderBean>();
            for(BookmarkFolder stub : stubList) {
                beanList.add(convertBookmarkFolderToBean(stub));
            }
        }
        return beanList;
    }
    public static List<BookmarkFolder> convertBookmarkFolderListStubToBeanList(BookmarkFolderListStub listStub)
    {
        List<BookmarkFolder> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<BookmarkFolder>();
        } else {
            beanList = new ArrayList<BookmarkFolder>();
            for(BookmarkFolderStub stub : listStub.getList()) {
            	beanList.add(convertBookmarkFolderToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static BookmarkFolderStub convertBookmarkFolderToStub(BookmarkFolder bean)
    {
        BookmarkFolderStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new BookmarkFolderStub();
        } else if(bean instanceof BookmarkFolderStub) {
            stub = (BookmarkFolderStub) bean;
        } else if(bean instanceof BookmarkFolderBean && ((BookmarkFolderBean) bean).isWrapper()) {
            stub = ((BookmarkFolderBean) bean).getStub();
        } else {
            stub = BookmarkFolderStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static KeywordLinkBean convertKeywordLinkToBean(KeywordLink stub)
    {
        KeywordLinkBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new KeywordLinkBean();
        } else if(stub instanceof KeywordLinkBean) {
        	bean = (KeywordLinkBean) stub;
        } else {
        	bean = new KeywordLinkBean(stub);
        }
        return bean;
    }
    public static List<KeywordLinkBean> convertKeywordLinkListToBeanList(List<KeywordLink> stubList)
    {
        List<KeywordLinkBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<KeywordLink>();
        } else {
            beanList = new ArrayList<KeywordLinkBean>();
            for(KeywordLink stub : stubList) {
                beanList.add(convertKeywordLinkToBean(stub));
            }
        }
        return beanList;
    }
    public static List<KeywordLink> convertKeywordLinkListStubToBeanList(KeywordLinkListStub listStub)
    {
        List<KeywordLink> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<KeywordLink>();
        } else {
            beanList = new ArrayList<KeywordLink>();
            for(KeywordLinkStub stub : listStub.getList()) {
            	beanList.add(convertKeywordLinkToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static KeywordLinkStub convertKeywordLinkToStub(KeywordLink bean)
    {
        KeywordLinkStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new KeywordLinkStub();
        } else if(bean instanceof KeywordLinkStub) {
            stub = (KeywordLinkStub) bean;
        } else if(bean instanceof KeywordLinkBean && ((KeywordLinkBean) bean).isWrapper()) {
            stub = ((KeywordLinkBean) bean).getStub();
        } else {
            stub = KeywordLinkStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static BookmarkLinkBean convertBookmarkLinkToBean(BookmarkLink stub)
    {
        BookmarkLinkBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new BookmarkLinkBean();
        } else if(stub instanceof BookmarkLinkBean) {
        	bean = (BookmarkLinkBean) stub;
        } else {
        	bean = new BookmarkLinkBean(stub);
        }
        return bean;
    }
    public static List<BookmarkLinkBean> convertBookmarkLinkListToBeanList(List<BookmarkLink> stubList)
    {
        List<BookmarkLinkBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<BookmarkLink>();
        } else {
            beanList = new ArrayList<BookmarkLinkBean>();
            for(BookmarkLink stub : stubList) {
                beanList.add(convertBookmarkLinkToBean(stub));
            }
        }
        return beanList;
    }
    public static List<BookmarkLink> convertBookmarkLinkListStubToBeanList(BookmarkLinkListStub listStub)
    {
        List<BookmarkLink> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<BookmarkLink>();
        } else {
            beanList = new ArrayList<BookmarkLink>();
            for(BookmarkLinkStub stub : listStub.getList()) {
            	beanList.add(convertBookmarkLinkToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static BookmarkLinkStub convertBookmarkLinkToStub(BookmarkLink bean)
    {
        BookmarkLinkStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new BookmarkLinkStub();
        } else if(bean instanceof BookmarkLinkStub) {
            stub = (BookmarkLinkStub) bean;
        } else if(bean instanceof BookmarkLinkBean && ((BookmarkLinkBean) bean).isWrapper()) {
            stub = ((BookmarkLinkBean) bean).getStub();
        } else {
            stub = BookmarkLinkStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static SpeedDialBean convertSpeedDialToBean(SpeedDial stub)
    {
        SpeedDialBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new SpeedDialBean();
        } else if(stub instanceof SpeedDialBean) {
        	bean = (SpeedDialBean) stub;
        } else {
        	bean = new SpeedDialBean(stub);
        }
        return bean;
    }
    public static List<SpeedDialBean> convertSpeedDialListToBeanList(List<SpeedDial> stubList)
    {
        List<SpeedDialBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<SpeedDial>();
        } else {
            beanList = new ArrayList<SpeedDialBean>();
            for(SpeedDial stub : stubList) {
                beanList.add(convertSpeedDialToBean(stub));
            }
        }
        return beanList;
    }
    public static List<SpeedDial> convertSpeedDialListStubToBeanList(SpeedDialListStub listStub)
    {
        List<SpeedDial> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<SpeedDial>();
        } else {
            beanList = new ArrayList<SpeedDial>();
            for(SpeedDialStub stub : listStub.getList()) {
            	beanList.add(convertSpeedDialToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static SpeedDialStub convertSpeedDialToStub(SpeedDial bean)
    {
        SpeedDialStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new SpeedDialStub();
        } else if(bean instanceof SpeedDialStub) {
            stub = (SpeedDialStub) bean;
        } else if(bean instanceof SpeedDialBean && ((SpeedDialBean) bean).isWrapper()) {
            stub = ((SpeedDialBean) bean).getStub();
        } else {
            stub = SpeedDialStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static KeywordFolderImportBean convertKeywordFolderImportToBean(KeywordFolderImport stub)
    {
        KeywordFolderImportBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new KeywordFolderImportBean();
        } else if(stub instanceof KeywordFolderImportBean) {
        	bean = (KeywordFolderImportBean) stub;
        } else {
        	bean = new KeywordFolderImportBean(stub);
        }
        return bean;
    }
    public static List<KeywordFolderImportBean> convertKeywordFolderImportListToBeanList(List<KeywordFolderImport> stubList)
    {
        List<KeywordFolderImportBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<KeywordFolderImport>();
        } else {
            beanList = new ArrayList<KeywordFolderImportBean>();
            for(KeywordFolderImport stub : stubList) {
                beanList.add(convertKeywordFolderImportToBean(stub));
            }
        }
        return beanList;
    }
    public static List<KeywordFolderImport> convertKeywordFolderImportListStubToBeanList(KeywordFolderImportListStub listStub)
    {
        List<KeywordFolderImport> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<KeywordFolderImport>();
        } else {
            beanList = new ArrayList<KeywordFolderImport>();
            for(KeywordFolderImportStub stub : listStub.getList()) {
            	beanList.add(convertKeywordFolderImportToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static KeywordFolderImportStub convertKeywordFolderImportToStub(KeywordFolderImport bean)
    {
        KeywordFolderImportStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new KeywordFolderImportStub();
        } else if(bean instanceof KeywordFolderImportStub) {
            stub = (KeywordFolderImportStub) bean;
        } else if(bean instanceof KeywordFolderImportBean && ((KeywordFolderImportBean) bean).isWrapper()) {
            stub = ((KeywordFolderImportBean) bean).getStub();
        } else {
            stub = KeywordFolderImportStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static BookmarkFolderImportBean convertBookmarkFolderImportToBean(BookmarkFolderImport stub)
    {
        BookmarkFolderImportBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new BookmarkFolderImportBean();
        } else if(stub instanceof BookmarkFolderImportBean) {
        	bean = (BookmarkFolderImportBean) stub;
        } else {
        	bean = new BookmarkFolderImportBean(stub);
        }
        return bean;
    }
    public static List<BookmarkFolderImportBean> convertBookmarkFolderImportListToBeanList(List<BookmarkFolderImport> stubList)
    {
        List<BookmarkFolderImportBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<BookmarkFolderImport>();
        } else {
            beanList = new ArrayList<BookmarkFolderImportBean>();
            for(BookmarkFolderImport stub : stubList) {
                beanList.add(convertBookmarkFolderImportToBean(stub));
            }
        }
        return beanList;
    }
    public static List<BookmarkFolderImport> convertBookmarkFolderImportListStubToBeanList(BookmarkFolderImportListStub listStub)
    {
        List<BookmarkFolderImport> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<BookmarkFolderImport>();
        } else {
            beanList = new ArrayList<BookmarkFolderImport>();
            for(BookmarkFolderImportStub stub : listStub.getList()) {
            	beanList.add(convertBookmarkFolderImportToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static BookmarkFolderImportStub convertBookmarkFolderImportToStub(BookmarkFolderImport bean)
    {
        BookmarkFolderImportStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new BookmarkFolderImportStub();
        } else if(bean instanceof BookmarkFolderImportStub) {
            stub = (BookmarkFolderImportStub) bean;
        } else if(bean instanceof BookmarkFolderImportBean && ((BookmarkFolderImportBean) bean).isWrapper()) {
            stub = ((BookmarkFolderImportBean) bean).getStub();
        } else {
            stub = BookmarkFolderImportStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static KeywordCrowdTallyBean convertKeywordCrowdTallyToBean(KeywordCrowdTally stub)
    {
        KeywordCrowdTallyBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new KeywordCrowdTallyBean();
        } else if(stub instanceof KeywordCrowdTallyBean) {
        	bean = (KeywordCrowdTallyBean) stub;
        } else {
        	bean = new KeywordCrowdTallyBean(stub);
        }
        return bean;
    }
    public static List<KeywordCrowdTallyBean> convertKeywordCrowdTallyListToBeanList(List<KeywordCrowdTally> stubList)
    {
        List<KeywordCrowdTallyBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<KeywordCrowdTally>();
        } else {
            beanList = new ArrayList<KeywordCrowdTallyBean>();
            for(KeywordCrowdTally stub : stubList) {
                beanList.add(convertKeywordCrowdTallyToBean(stub));
            }
        }
        return beanList;
    }
    public static List<KeywordCrowdTally> convertKeywordCrowdTallyListStubToBeanList(KeywordCrowdTallyListStub listStub)
    {
        List<KeywordCrowdTally> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<KeywordCrowdTally>();
        } else {
            beanList = new ArrayList<KeywordCrowdTally>();
            for(KeywordCrowdTallyStub stub : listStub.getList()) {
            	beanList.add(convertKeywordCrowdTallyToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static KeywordCrowdTallyStub convertKeywordCrowdTallyToStub(KeywordCrowdTally bean)
    {
        KeywordCrowdTallyStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new KeywordCrowdTallyStub();
        } else if(bean instanceof KeywordCrowdTallyStub) {
            stub = (KeywordCrowdTallyStub) bean;
        } else if(bean instanceof KeywordCrowdTallyBean && ((KeywordCrowdTallyBean) bean).isWrapper()) {
            stub = ((KeywordCrowdTallyBean) bean).getStub();
        } else {
            stub = KeywordCrowdTallyStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static BookmarkCrowdTallyBean convertBookmarkCrowdTallyToBean(BookmarkCrowdTally stub)
    {
        BookmarkCrowdTallyBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new BookmarkCrowdTallyBean();
        } else if(stub instanceof BookmarkCrowdTallyBean) {
        	bean = (BookmarkCrowdTallyBean) stub;
        } else {
        	bean = new BookmarkCrowdTallyBean(stub);
        }
        return bean;
    }
    public static List<BookmarkCrowdTallyBean> convertBookmarkCrowdTallyListToBeanList(List<BookmarkCrowdTally> stubList)
    {
        List<BookmarkCrowdTallyBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<BookmarkCrowdTally>();
        } else {
            beanList = new ArrayList<BookmarkCrowdTallyBean>();
            for(BookmarkCrowdTally stub : stubList) {
                beanList.add(convertBookmarkCrowdTallyToBean(stub));
            }
        }
        return beanList;
    }
    public static List<BookmarkCrowdTally> convertBookmarkCrowdTallyListStubToBeanList(BookmarkCrowdTallyListStub listStub)
    {
        List<BookmarkCrowdTally> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<BookmarkCrowdTally>();
        } else {
            beanList = new ArrayList<BookmarkCrowdTally>();
            for(BookmarkCrowdTallyStub stub : listStub.getList()) {
            	beanList.add(convertBookmarkCrowdTallyToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static BookmarkCrowdTallyStub convertBookmarkCrowdTallyToStub(BookmarkCrowdTally bean)
    {
        BookmarkCrowdTallyStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new BookmarkCrowdTallyStub();
        } else if(bean instanceof BookmarkCrowdTallyStub) {
            stub = (BookmarkCrowdTallyStub) bean;
        } else if(bean instanceof BookmarkCrowdTallyBean && ((BookmarkCrowdTallyBean) bean).isWrapper()) {
            stub = ((BookmarkCrowdTallyBean) bean).getStub();
        } else {
            stub = BookmarkCrowdTallyStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static DomainInfoBean convertDomainInfoToBean(DomainInfo stub)
    {
        DomainInfoBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new DomainInfoBean();
        } else if(stub instanceof DomainInfoBean) {
        	bean = (DomainInfoBean) stub;
        } else {
        	bean = new DomainInfoBean(stub);
        }
        return bean;
    }
    public static List<DomainInfoBean> convertDomainInfoListToBeanList(List<DomainInfo> stubList)
    {
        List<DomainInfoBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<DomainInfo>();
        } else {
            beanList = new ArrayList<DomainInfoBean>();
            for(DomainInfo stub : stubList) {
                beanList.add(convertDomainInfoToBean(stub));
            }
        }
        return beanList;
    }
    public static List<DomainInfo> convertDomainInfoListStubToBeanList(DomainInfoListStub listStub)
    {
        List<DomainInfo> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<DomainInfo>();
        } else {
            beanList = new ArrayList<DomainInfo>();
            for(DomainInfoStub stub : listStub.getList()) {
            	beanList.add(convertDomainInfoToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static DomainInfoStub convertDomainInfoToStub(DomainInfo bean)
    {
        DomainInfoStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new DomainInfoStub();
        } else if(bean instanceof DomainInfoStub) {
            stub = (DomainInfoStub) bean;
        } else if(bean instanceof DomainInfoBean && ((DomainInfoBean) bean).isWrapper()) {
            stub = ((DomainInfoBean) bean).getStub();
        } else {
            stub = DomainInfoStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static UrlRatingBean convertUrlRatingToBean(UrlRating stub)
    {
        UrlRatingBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new UrlRatingBean();
        } else if(stub instanceof UrlRatingBean) {
        	bean = (UrlRatingBean) stub;
        } else {
        	bean = new UrlRatingBean(stub);
        }
        return bean;
    }
    public static List<UrlRatingBean> convertUrlRatingListToBeanList(List<UrlRating> stubList)
    {
        List<UrlRatingBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<UrlRating>();
        } else {
            beanList = new ArrayList<UrlRatingBean>();
            for(UrlRating stub : stubList) {
                beanList.add(convertUrlRatingToBean(stub));
            }
        }
        return beanList;
    }
    public static List<UrlRating> convertUrlRatingListStubToBeanList(UrlRatingListStub listStub)
    {
        List<UrlRating> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<UrlRating>();
        } else {
            beanList = new ArrayList<UrlRating>();
            for(UrlRatingStub stub : listStub.getList()) {
            	beanList.add(convertUrlRatingToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static UrlRatingStub convertUrlRatingToStub(UrlRating bean)
    {
        UrlRatingStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new UrlRatingStub();
        } else if(bean instanceof UrlRatingStub) {
            stub = (UrlRatingStub) bean;
        } else if(bean instanceof UrlRatingBean && ((UrlRatingBean) bean).isWrapper()) {
            stub = ((UrlRatingBean) bean).getStub();
        } else {
            stub = UrlRatingStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static UserRatingBean convertUserRatingToBean(UserRating stub)
    {
        UserRatingBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new UserRatingBean();
        } else if(stub instanceof UserRatingBean) {
        	bean = (UserRatingBean) stub;
        } else {
        	bean = new UserRatingBean(stub);
        }
        return bean;
    }
    public static List<UserRatingBean> convertUserRatingListToBeanList(List<UserRating> stubList)
    {
        List<UserRatingBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<UserRating>();
        } else {
            beanList = new ArrayList<UserRatingBean>();
            for(UserRating stub : stubList) {
                beanList.add(convertUserRatingToBean(stub));
            }
        }
        return beanList;
    }
    public static List<UserRating> convertUserRatingListStubToBeanList(UserRatingListStub listStub)
    {
        List<UserRating> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<UserRating>();
        } else {
            beanList = new ArrayList<UserRating>();
            for(UserRatingStub stub : listStub.getList()) {
            	beanList.add(convertUserRatingToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static UserRatingStub convertUserRatingToStub(UserRating bean)
    {
        UserRatingStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new UserRatingStub();
        } else if(bean instanceof UserRatingStub) {
            stub = (UserRatingStub) bean;
        } else if(bean instanceof UserRatingBean && ((UserRatingBean) bean).isWrapper()) {
            stub = ((UserRatingBean) bean).getStub();
        } else {
            stub = UserRatingStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static AbuseTagBean convertAbuseTagToBean(AbuseTag stub)
    {
        AbuseTagBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new AbuseTagBean();
        } else if(stub instanceof AbuseTagBean) {
        	bean = (AbuseTagBean) stub;
        } else {
        	bean = new AbuseTagBean(stub);
        }
        return bean;
    }
    public static List<AbuseTagBean> convertAbuseTagListToBeanList(List<AbuseTag> stubList)
    {
        List<AbuseTagBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<AbuseTag>();
        } else {
            beanList = new ArrayList<AbuseTagBean>();
            for(AbuseTag stub : stubList) {
                beanList.add(convertAbuseTagToBean(stub));
            }
        }
        return beanList;
    }
    public static List<AbuseTag> convertAbuseTagListStubToBeanList(AbuseTagListStub listStub)
    {
        List<AbuseTag> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<AbuseTag>();
        } else {
            beanList = new ArrayList<AbuseTag>();
            for(AbuseTagStub stub : listStub.getList()) {
            	beanList.add(convertAbuseTagToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static AbuseTagStub convertAbuseTagToStub(AbuseTag bean)
    {
        AbuseTagStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new AbuseTagStub();
        } else if(bean instanceof AbuseTagStub) {
            stub = (AbuseTagStub) bean;
        } else if(bean instanceof AbuseTagBean && ((AbuseTagBean) bean).isWrapper()) {
            stub = ((AbuseTagBean) bean).getStub();
        } else {
            stub = AbuseTagStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static HelpNoticeBean convertHelpNoticeToBean(HelpNotice stub)
    {
        HelpNoticeBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new HelpNoticeBean();
        } else if(stub instanceof HelpNoticeBean) {
        	bean = (HelpNoticeBean) stub;
        } else {
        	bean = new HelpNoticeBean(stub);
        }
        return bean;
    }
    public static List<HelpNoticeBean> convertHelpNoticeListToBeanList(List<HelpNotice> stubList)
    {
        List<HelpNoticeBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<HelpNotice>();
        } else {
            beanList = new ArrayList<HelpNoticeBean>();
            for(HelpNotice stub : stubList) {
                beanList.add(convertHelpNoticeToBean(stub));
            }
        }
        return beanList;
    }
    public static List<HelpNotice> convertHelpNoticeListStubToBeanList(HelpNoticeListStub listStub)
    {
        List<HelpNotice> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<HelpNotice>();
        } else {
            beanList = new ArrayList<HelpNotice>();
            for(HelpNoticeStub stub : listStub.getList()) {
            	beanList.add(convertHelpNoticeToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static HelpNoticeStub convertHelpNoticeToStub(HelpNotice bean)
    {
        HelpNoticeStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new HelpNoticeStub();
        } else if(bean instanceof HelpNoticeStub) {
            stub = (HelpNoticeStub) bean;
        } else if(bean instanceof HelpNoticeBean && ((HelpNoticeBean) bean).isWrapper()) {
            stub = ((HelpNoticeBean) bean).getStub();
        } else {
            stub = HelpNoticeStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static SiteLogBean convertSiteLogToBean(SiteLog stub)
    {
        SiteLogBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new SiteLogBean();
        } else if(stub instanceof SiteLogBean) {
        	bean = (SiteLogBean) stub;
        } else {
        	bean = new SiteLogBean(stub);
        }
        return bean;
    }
    public static List<SiteLogBean> convertSiteLogListToBeanList(List<SiteLog> stubList)
    {
        List<SiteLogBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<SiteLog>();
        } else {
            beanList = new ArrayList<SiteLogBean>();
            for(SiteLog stub : stubList) {
                beanList.add(convertSiteLogToBean(stub));
            }
        }
        return beanList;
    }
    public static List<SiteLog> convertSiteLogListStubToBeanList(SiteLogListStub listStub)
    {
        List<SiteLog> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<SiteLog>();
        } else {
            beanList = new ArrayList<SiteLog>();
            for(SiteLogStub stub : listStub.getList()) {
            	beanList.add(convertSiteLogToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static SiteLogStub convertSiteLogToStub(SiteLog bean)
    {
        SiteLogStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new SiteLogStub();
        } else if(bean instanceof SiteLogStub) {
            stub = (SiteLogStub) bean;
        } else if(bean instanceof SiteLogBean && ((SiteLogBean) bean).isWrapper()) {
            stub = ((SiteLogBean) bean).getStub();
        } else {
            stub = SiteLogStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static ServiceInfoBean convertServiceInfoToBean(ServiceInfo stub)
    {
        ServiceInfoBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new ServiceInfoBean();
        } else if(stub instanceof ServiceInfoBean) {
        	bean = (ServiceInfoBean) stub;
        } else {
        	bean = new ServiceInfoBean(stub);
        }
        return bean;
    }
    public static List<ServiceInfoBean> convertServiceInfoListToBeanList(List<ServiceInfo> stubList)
    {
        List<ServiceInfoBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<ServiceInfo>();
        } else {
            beanList = new ArrayList<ServiceInfoBean>();
            for(ServiceInfo stub : stubList) {
                beanList.add(convertServiceInfoToBean(stub));
            }
        }
        return beanList;
    }
    public static List<ServiceInfo> convertServiceInfoListStubToBeanList(ServiceInfoListStub listStub)
    {
        List<ServiceInfo> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<ServiceInfo>();
        } else {
            beanList = new ArrayList<ServiceInfo>();
            for(ServiceInfoStub stub : listStub.getList()) {
            	beanList.add(convertServiceInfoToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static ServiceInfoStub convertServiceInfoToStub(ServiceInfo bean)
    {
        ServiceInfoStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new ServiceInfoStub();
        } else if(bean instanceof ServiceInfoStub) {
            stub = (ServiceInfoStub) bean;
        } else if(bean instanceof ServiceInfoBean && ((ServiceInfoBean) bean).isWrapper()) {
            stub = ((ServiceInfoBean) bean).getStub();
        } else {
            stub = ServiceInfoStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static FiveTenBean convertFiveTenToBean(FiveTen stub)
    {
        FiveTenBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new FiveTenBean();
        } else if(stub instanceof FiveTenBean) {
        	bean = (FiveTenBean) stub;
        } else {
        	bean = new FiveTenBean(stub);
        }
        return bean;
    }
    public static List<FiveTenBean> convertFiveTenListToBeanList(List<FiveTen> stubList)
    {
        List<FiveTenBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<FiveTen>();
        } else {
            beanList = new ArrayList<FiveTenBean>();
            for(FiveTen stub : stubList) {
                beanList.add(convertFiveTenToBean(stub));
            }
        }
        return beanList;
    }
    public static List<FiveTen> convertFiveTenListStubToBeanList(FiveTenListStub listStub)
    {
        List<FiveTen> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<FiveTen>();
        } else {
            beanList = new ArrayList<FiveTen>();
            for(FiveTenStub stub : listStub.getList()) {
            	beanList.add(convertFiveTenToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static FiveTenStub convertFiveTenToStub(FiveTen bean)
    {
        FiveTenStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new FiveTenStub();
        } else if(bean instanceof FiveTenStub) {
            stub = (FiveTenStub) bean;
        } else if(bean instanceof FiveTenBean && ((FiveTenBean) bean).isWrapper()) {
            stub = ((FiveTenBean) bean).getStub();
        } else {
            stub = FiveTenStub.convertBeanToStub(bean);
        }
        return stub;
    }

    
}
