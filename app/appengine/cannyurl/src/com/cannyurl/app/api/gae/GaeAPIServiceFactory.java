package com.cannyurl.app.api.gae;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.cannyurl.app.api.APIServiceFactory;
import com.cannyurl.app.api.UrlShortenerAPIService;
import com.cannyurl.app.api.TwitterAPIService;


public class GaeAPIServiceFactory extends APIServiceFactory
{
    private static final Logger log = Logger.getLogger(GaeAPIServiceFactory.class.getName());

    private GaeAPIServiceFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class GaeAPIServiceFactoryHolder
    {
        private static final GaeAPIServiceFactory INSTANCE = new GaeAPIServiceFactory();
    }

    // Singleton method
    public static GaeAPIServiceFactory getInstance()
    {
        return GaeAPIServiceFactoryHolder.INSTANCE;
    }


    // API Services

    public UrlShortenerAPIService getUrlShortenerAPIService()
    {
        return GaeUrlShortenerAPIService.getInstance();
    }

    public TwitterAPIService getTwitterAPIService()
    {
        return new GaeTwitterAPIService();
    }


}
