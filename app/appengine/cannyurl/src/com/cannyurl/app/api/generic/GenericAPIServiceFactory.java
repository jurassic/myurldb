package com.cannyurl.app.api.generic;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.cannyurl.app.api.APIServiceFactory;
import com.cannyurl.app.api.UrlShortenerAPIService;
import com.cannyurl.app.api.TwitterAPIService;


public class GenericAPIServiceFactory extends APIServiceFactory
{
    private static final Logger log = Logger.getLogger(GenericAPIServiceFactory.class.getName());

    private GenericAPIServiceFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class GenericAPIServiceFactoryHolder
    {
        private static final GenericAPIServiceFactory INSTANCE = new GenericAPIServiceFactory();
    }

    // Singleton method
    public static GenericAPIServiceFactory getInstance()
    {
        return GenericAPIServiceFactoryHolder.INSTANCE;
    }


    // API Services

    public UrlShortenerAPIService getUrlShortenerAPIService()
    {
        return GenericUrlShortenerAPIService.getInstance();
    }

    public TwitterAPIService getTwitterAPIService()
    {
        return new GenericTwitterAPIService();
    }


}
