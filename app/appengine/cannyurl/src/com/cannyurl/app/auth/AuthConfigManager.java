package com.cannyurl.app.auth;

import java.util.logging.Logger;

import com.aeryid.ws.AeryUser;
import com.cannyurl.af.auth.user.AuthUser;
import com.cannyurl.af.config.Config;
import com.cannyurl.app.service.UserAppService;
import com.myurldb.ws.User;


// 3/03/13:
//...


public class AuthConfigManager
{
    private static final Logger log = Logger.getLogger(AuthConfigManager.class.getName());

    // TBD
    private static final String CONFIG_KEY_AUTH_USE_AERYID = "cannyurlapp.auth.useaeryid";
    // Whether to enable or not each of the SSO options...
    // TBD: For now, if one of SSO option is selected, it's exclusive, 
    //               that is, no other auth providers (including aeryId providers) will not be checked... 
    private static final String CONFIG_KEY_AUTH_SSO_GOOGLEAPPS = "cannyurlapp.auth.sso.googleapps";
    private static final String CONFIG_KEY_AUTH_SSO_WEBUSERDB = "cannyurlapp.auth.sso.webuserdb";
    // ...
    // --> Use af.auth.googleapps.GoogleAppsUtil.CONFIG_KEY_LOGINREALM....
    //private static final String CONFIG_KEY_AUTH_GOOGLEAPPS_LOGINREALM = "cannyurlapp.auth.googleapps.loginrealm";
    // ...

    // TBD
    private UserAppService userAppService = null;
//    private AeryUserService mAeryUserService = null;
    // ...

    // Default value is false...
    private boolean useAeryId = false;
    private boolean ssoGoogleApps = false;
    private boolean sssoWebUserDB = false;
    // ...
    //private String googleAppsLoginRealm = null;
    // ...
    
    private AuthConfigManager() 
    {
        useAeryId = Config.getInstance().getBoolean(CONFIG_KEY_AUTH_USE_AERYID, useAeryId);
        ssoGoogleApps = Config.getInstance().getBoolean(CONFIG_KEY_AUTH_SSO_GOOGLEAPPS, ssoGoogleApps);
        sssoWebUserDB = Config.getInstance().getBoolean(CONFIG_KEY_AUTH_SSO_WEBUSERDB, sssoWebUserDB);
        //googleAppsLoginRealm = Config.getInstance().getString(CONFIG_KEY_AUTH_GOOGLEAPPS_LOGINREALM, null);
    }

    // Initialization-on-demand holder.
    private static final class AuthConfigManagerHolder
    {
        private static final AuthConfigManager INSTANCE = new AuthConfigManager();
    }

    // Singleton method
    public static AuthConfigManager getInstance()
    {
        return AuthConfigManagerHolder.INSTANCE;
    }
    
    private UserAppService getUserAppService()
    {
        if(userAppService == null) {
            userAppService = new UserAppService();
        }
        return userAppService;
    }
//    private AeryUserService getAeryUserService()
//    {
//        if(mAeryUserService == null) {
//            mAeryUserService = new AeryUserServiceProxy();
//        }
//        return mAeryUserService;
//    }


    public boolean useAeryId()
    {
        return useAeryId;
    }
    public boolean ssoGoogleApps()
    {
        return ssoGoogleApps;
    }
    public boolean sssoWebUserDB()
    {
        return sssoWebUserDB;
    }
    //public String getGoogleAppsLoginRealm()
    //{
    //    return googleAppsLoginRealm;
    //}
    
    
    
    
    
    // TBD:
    // What are the following functions?
    // Why are they here???
    // ...
    

    public String createUser(String sessionGuid, String userGuid)
    {
        if(useAeryId()) {
            return AeryUserHelper.getInstance().createAeryUser(sessionGuid, userGuid);
        } else {
            return AuthUserHelper.getInstance().createUser(sessionGuid, userGuid);
        }
    }
    
    public Boolean createOrUpdateUser(String currentUserGuid, String currentSessionId, AuthUser authUser)
    {
        Boolean created = null;
        if(useAeryId()) {
            created = AeryUserHelper.getInstance().createOrUpdateAeryUser(currentUserGuid, currentSessionId, authUser);
        } else {
            created = AuthUserHelper.getInstance().createOrUpdateUserBean(currentUserGuid, currentSessionId, authUser);
        }
        return created;
    }

    public Boolean updateUserIfNecessary(String currentUserGuid, String currentSessionId)
    {
        Boolean updated = null;
        if(useAeryId()) {
            updated = AeryUserHelper.getInstance().updateAeryUserIfNecessary(currentUserGuid, currentSessionId);
        } else {
            updated = AuthUserHelper.getInstance().updateUserBeanIfNecessary(currentUserGuid, currentSessionId);
        }
        return updated;
    }

    public String getUserGuid(String authUserGuid)
    {
        String userGuid = null;
        if(useAeryId()) {
            AeryUser aeryUser = AeryUserHelper.getInstance().getAeryUser(authUserGuid);
            if(aeryUser != null) {
                userGuid = aeryUser.getGuid();
            }
        } else {
            User appUser = AuthUserHelper.getInstance().getUser(authUserGuid);
            if(appUser != null) {
                userGuid = appUser.getGuid();
            }
        }
        return userGuid;
    }

    // Note:
    // authDomain is not being used.... ????
    public String findUserGuidByGaeNicknameAndAuthDomain(String gaeOpenId, String authDomain)
    {
        String userGuid = null;
        if(useAeryId()) {
            AeryUser aeryUser = AeryUserHelper.getInstance().findAeryUserByGaeNicknameAndAuthDomain(gaeOpenId, authDomain);
            if(aeryUser != null) {
                userGuid = aeryUser.getGuid();
            }
        } else {
            User appUser = AuthUserHelper.getInstance().findUserByGaeNicknameAndAuthDomain(gaeOpenId, authDomain);
            if(appUser != null) {
                userGuid = appUser.getGuid();
            }
        }        
        return userGuid;
    }
    

    public String findUserGuidByGaeNicknameAndFederatedIdentity(String gaeOpenId, String federatedIdentity)
    {
        String userGuid = null;
        if(useAeryId()) {
            AeryUser aeryUser = AeryUserHelper.getInstance().findAeryUserByGaeNicknameAndFederatedIdentity(gaeOpenId, federatedIdentity);
            if(aeryUser != null) {
                userGuid = aeryUser.getGuid();
            }
        } else {
            User appUser = AuthUserHelper.getInstance().findUserByGaeNicknameAndFederatedIdentity(gaeOpenId, federatedIdentity);
            if(appUser != null) {
                userGuid = appUser.getGuid();
            }
        }        
        return userGuid;
    }
    

    
}
