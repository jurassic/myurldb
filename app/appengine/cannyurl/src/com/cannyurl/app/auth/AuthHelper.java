package com.cannyurl.app.auth;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import com.cannyurl.af.auth.common.CommonAuthUtil;
import com.cannyurl.af.auth.facebook.FacebookAuthHelper;
import com.cannyurl.af.auth.twitter.TwitterAuthHelper;
import com.cannyurl.af.bean.ExternalUserAuthBean;
import com.cannyurl.af.bean.ExternalUserIdStructBean;
import com.cannyurl.af.config.Config;
import com.cannyurl.af.util.URLUtil;


// 3/03/13:
//...


public class AuthHelper
{
    private static final Logger log = Logger.getLogger(AuthHelper.class.getName());

    // TBD: These need to match settings in web.xml
    private static final String AUTH_MANAGER_URLPATH = "/auth/manager";
    private static final String AUTH_HANDLER_URLPATH = "/auth/handler";
    // ...

    // "Lazy initialization"
    private Boolean mUseTwitter4j = null;
    private Boolean mUseSocialAuth = null;
    // ...

    public AuthHelper() {}

    // Initialization-on-demand holder.
    private static final class AuthHelperHolder
    {
        private static final AuthHelper INSTANCE = new AuthHelper();
    }

    // Singleton method
    public static AuthHelper getInstance()
    {
        return AuthHelperHolder.INSTANCE;
    }

    
    // Note:
    // The following abstract URLs are used for redirect, and redirect only...
    // For forward, relative urlpath, and path alone, needs to be used...
    // ....
    
    
    public String getAuthManagerUrlPath()
    {
        return AUTH_MANAGER_URLPATH;
    }
    public String getAuthManagerUrl(String topLevelUrl)
    {
        return getAuthManagerUrl(topLevelUrl, null);
    }
    public String getAuthManagerUrl(String topLevelUrl, String comebackUrl)
    {
        return getAuthManagerUrl(topLevelUrl, comebackUrl, null);
    }
    public String getAuthManagerUrl(String topLevelUrl, String comebackUrl, String fallbackUrl)
    {
        String authPath = AUTH_MANAGER_URLPATH;
        Map<String, Object> params = new HashMap<String, Object>();
        if(comebackUrl != null) {
            params.put(CommonAuthUtil.PARAM_COMEBACKURL, comebackUrl);
        }
        if(fallbackUrl != null) {
            params.put(CommonAuthUtil.PARAM_FALLBACKURL, fallbackUrl);
        }
        String authUrl = CommonAuthUtil.constructUrl(topLevelUrl, authPath, params);
        return authUrl;
    }

    public String getAuthHandlerUrlPath()
    {
        return AUTH_HANDLER_URLPATH;
    }
    public String getAuthHandlerUrl(String topLevelUrl)
    {
        return getAuthHandlerUrl(topLevelUrl, null);
    }
    public String getAuthHandlerUrl(String topLevelUrl, String comebackUrl)
    {
        return getAuthHandlerUrl(topLevelUrl, comebackUrl, null);
    }
    public String getAuthHandlerUrl(String topLevelUrl, String comebackUrl, String fallbackUrl)
    {
        String authPath = AUTH_HANDLER_URLPATH;
        Map<String, Object> params = new HashMap<String, Object>();
        if(comebackUrl != null) {
            params.put(CommonAuthUtil.PARAM_COMEBACKURL, comebackUrl);
        }
        if(fallbackUrl != null) {
            params.put(CommonAuthUtil.PARAM_FALLBACKURL, fallbackUrl);
        }
        String authUrl = CommonAuthUtil.constructUrl(topLevelUrl, authPath, params);
        return authUrl;
    }

    public String getFacebookCallbackUrl(String topLevelUrl)
    {
        return getFacebookCallbackUrl(topLevelUrl, null);  // ???
    }
    public String getFacebookCallbackUrl(String topLevelUrl, String comebackUrl)
    {
        return getFacebookCallbackUrl(topLevelUrl, comebackUrl, null);  // ???
    }
    public String getFacebookCallbackUrl(String topLevelUrl, String comebackUrl, String fallbackUrl)
    {
        String baseUrl = FacebookAuthHelper.getInstance().getDefaultOAuthRedirectUri(topLevelUrl);
        Map<String, Object> params = new HashMap<String, Object>();
        if(comebackUrl != null) {
            params.put(CommonAuthUtil.PARAM_COMEBACKURL, comebackUrl);
        }
        if(fallbackUrl != null) {
            params.put(CommonAuthUtil.PARAM_FALLBACKURL, fallbackUrl);
        }
        String authUrl = URLUtil.buildUrl(baseUrl, params);
        return authUrl;
    }

    public String getTwitterCallbackUrl(String topLevelUrl)
    {
        return getTwitterCallbackUrl(topLevelUrl, null);
    }
    public String getTwitterCallbackUrl(String topLevelUrl, String comebackUrl)
    {
        return getTwitterCallbackUrl(topLevelUrl, comebackUrl, null);
    }
    public String getTwitterCallbackUrl(String topLevelUrl, String comebackUrl, String fallbackUrl)
    {
        String baseUrl = TwitterAuthHelper.getInstance().getDefaultOAuthRedirectUri(topLevelUrl);
        Map<String, Object> params = new HashMap<String, Object>();
        if(comebackUrl != null) {
            params.put(CommonAuthUtil.PARAM_COMEBACKURL, comebackUrl);
        }
        if(fallbackUrl != null) {
            params.put(CommonAuthUtil.PARAM_FALLBACKURL, fallbackUrl);
        }
        String authUrl = URLUtil.buildUrl(baseUrl, params);
        return authUrl;
    }


    public boolean useTwitter4jLibrary()
    {
        if(mUseTwitter4j == null) {
            mUseTwitter4j = Config.getInstance().getBoolean(AuthUtil.CONFIG_KEY_USELIBRARY_TWITTER4J, false);
        }
        return mUseTwitter4j;
    }

    // Applicable only if providerId == twitter.
    public boolean useTwitter4j(String identifier)
    {
        if(CommonAuthUtil.AUTH_SERVICE_PROVIDERID_TWITTER.equals(identifier)) {
            if(useTwitter4jLibrary() == true) {
                return true;
            }
        }
        return false;
    }

    // SocialAuth library is to be used only for non-openId service providers even if the config is set to true.
    public boolean useSocialAuthLibrary()
    {
        if(mUseSocialAuth == null) {
            mUseSocialAuth = Config.getInstance().getBoolean(AuthUtil.CONFIG_KEY_USELIBRARY_SOCIALAUTH, false);
        }
        return mUseSocialAuth;
    }

    // Applicable only if providerId == twitter or facebook...
    public boolean useSocialAuth(String identifier)
    {
        if(useSocialAuthLibrary() == true) {
            if(CommonAuthUtil.isNonOpenId(identifier)) {
                return true;
            }
        }
        return false;
    }

    
    // Cf. Twitter4jHelper.saveTwitterAccessToken()...
    public String saveAcessToken(String providerId, String user, String accessToken, Integer expires)
    {
        String guid = null;
       
        ExternalUserAuthBean bean = new ExternalUserAuthBean();
        bean.setProviderId(providerId);
        bean.setUser(user);
        //ExternalUserIdStructBean externaUserId = new ExternalUserIdStructBean();
        //externaUserId.setUserId(userId);
        //bean.setExternalUserId(externaUserId);
        bean.setAccessToken(accessToken);
        //bean.setAccessTokenSecret(accessTokenSecret);
        long now = System.currentTimeMillis();
        bean.setAuthTime(now);
        if(expires != null && expires > 0) {
            long expirationTime = now + expires * 1000L;
            bean.setExpirationTime(expirationTime);
        }
        
        // TBD: save or update.... ???
        bean = (ExternalUserAuthBean) ExternalUserAuthHelper.getInstance().saveExternalUserAuth(bean, true);
        if(bean != null) {
            guid = bean.getGuid();
        } else {
            guid = null;   // ???
        }

        return guid;        
    }
    
    
    
}
