package com.cannyurl.app.auth;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpSession;

import com.cannyurl.af.auth.AuthConstants;
import com.cannyurl.af.auth.SessionBean;
import com.cannyurl.af.auth.common.AuthToken;
import com.cannyurl.af.auth.common.CommonAuthUtil;
import com.cannyurl.af.auth.user.AuthUserService;
import com.cannyurl.af.auth.user.AuthUserServiceFactory;
import com.cannyurl.af.bean.UserAuthStateBean;
import com.cannyurl.common.AuthStatus;
import com.myurldb.ws.ExternalUserIdStruct;
import com.myurldb.ws.UserAuthState;


// 3/03/13:
// 1/13/13: Support for "Logout"
//...


// TBD:
// The problem with the current implementation (querying UserAuthState based on user/providerId) is
//   If a user logs on to the same service (using the same account/username) multiple times from different browsers,
//   then the same UserAuthState will be shared across different browsers...
//   This has many implications..
//   for example, the user logs out from one browser, the UserAuthState will be deleted (in the current implementation),
//   and all the rest login states will be kind of invalidated.....
// --> This is really a bug...
public final class AuthManager
{
    private static final Logger log = Logger.getLogger(AuthManager.class.getName());


    public AuthManager() {}

    // Initialization-on-demand holder.
    private static final class AuthManagerHolder
    {
        private static final AuthManager INSTANCE = new AuthManager();
    }

    // Singleton method
    public static AuthManager getInstance()
    {
        return AuthManagerHolder.INSTANCE;
    }

    
    
    // 1/13/13
    // At this point, I'm not sure what I was trying to do initially...
    // Looks like
    // we use three layers
    // (1) authToken session var
    // (2) User auth state DB
    // (3) GAE Auth service....
    // ....

    
    
    // TBD: Is there a better way???
    //      This whole design of using GAE UserService and other authentication options is so messy....
    // ....
    // Check the session first, and if no auth info is found,
    // then check the GAE auth service.....
    public boolean isUserLoggedIn(HttpSession session)
    {
        return isUserLoggedIn(session, null);
    }
    public boolean isUserLoggedIn(HttpSession session, String providerId)
    {
        if(session == null) {
            // ????
            if(log.isLoggable(Level.WARNING)) log.warning("Session is null. providerId = " + providerId);
            return false;
        }
        // For debugging...
        String sessionId = session.getId();

        // Always err on the safer side.
        // We may return false when in fact the user is authenticated.
        // but we do not want to return true when the user is NOT authenticated.
        boolean isAuthenticated = false;

        // [1]
        AuthToken authToken = (AuthToken) session.getAttribute(CommonAuthUtil.SESSION_ATTR_AUTHTOKEN);
        if(authToken != null) {
            isAuthenticated = authToken.isAuthenticated(providerId);
        } else {
            if(log.isLoggable(Level.FINE)) log.fine("authToken not found in the session. sessionId = " + sessionId + "; providerId = " + providerId);
        }
        
        // [2]
        // ??? 
        // what about when isAuthenticated == true and the DB does not have record ????
        // ????
        if(isAuthenticated == false) {

            // TBD:
            // Check extAuth table???
//            ExternalUserAuth extAuth = ExternalUserAuthHelper.getInstance().findExternalUserAuthByUser(user, providerId);   // TBD: include authStatus in the query ????
//            if(extAuth != null) {
//                // ????
//                isAuthenticated = true;
//            }

            // Check session first for userAuthState ????
            UserAuthState userAuthState = (UserAuthState) session.getAttribute(AuthUtil.getSessionAttrKeyUserAuthState(providerId));
            if(userAuthState == null) {
                SessionBean sessionBean = (SessionBean) session.getAttribute( AuthConstants.SESSION_ATTR_SESSION_BEAN );
                if(sessionBean != null) {
                    String user = sessionBean.getUserId();   // User.guid.
                    if(user != null) {                        
                        userAuthState = UserAuthStateHelper.getInstance().findUserAuthStateForUser(user, providerId);
                        if(userAuthState != null) {
                            session.setAttribute(AuthUtil.getSessionAttrKeyUserAuthState(providerId), userAuthState);
                        } else {
                            if(log.isLoggable(Level.FINE)) log.fine("Failed to find userAuthState. sessionId = " + sessionId + "; user = " + user + "; providerId = " + providerId);
                        }
                    } else {
                        if(log.isLoggable(Level.WARNING)) log.warning("SessionBean user not found. sessionId = " + sessionId + "; providerId = " + providerId);
                    }
                } else {
                    if(log.isLoggable(Level.INFO)) log.info("SessionBean not found. sessionId = " + sessionId + "; providerId = " + providerId);
                }
            } else {
                if(log.isLoggable(Level.FINE)) log.fine("userAuthState found in the session. sessionId = " + sessionId + "; providerId = " + providerId);
            }

            if(userAuthState != null) {
                String authStatus = userAuthState.getAuthStatus();
                if(AuthStatus.STATUS_AUTHENTICATED.equals(authStatus)) {
                    isAuthenticated = true;
                    // TBD:
                    // Create/Update authToken session var ???
                    // ....
                    // It seems to do this in "action" type methods (e.g., login, etc.) than in "checking" method such as this (IS_loggedin).
                    // But, if don't do this here, this method will end up repeatedly querying DB again and again...
                    // hence...
                    ExternalUserIdStruct extUserId = userAuthState.getExternalId();
                    if(extUserId != null) {
                        String username = extUserId.getUsername();
                        if(username != null && !username.isEmpty()) {   // ????
                            if(authToken == null) {
                                authToken = new AuthToken();
                            }
                            authToken.addAuthState(providerId, username);
                            session.setAttribute(CommonAuthUtil.SESSION_ATTR_AUTHTOKEN, authToken);
                        } else {
                            if(log.isLoggable(Level.INFO)) log.info("ExternalUserIdStruct.username not found. sessionId = " + sessionId + "; providerId = " + providerId);
                        }
                    } else {
                        if(log.isLoggable(Level.WARNING)) log.warning("ExternalUserIdStruct not found. sessionId = " + sessionId + "; providerId = " + providerId);
                    }
                    // .....
                } else {
                    // Auth_failed, etc. ?
                    if(log.isLoggable(Level.INFO)) log.info("User not authenticated. sessionId = " + sessionId + "; authStatus = " + authStatus + "; providerId = " + providerId);
                }
            } else {
                // ????
                // User, not authenticated. ignore...
                if(log.isLoggable(Level.FINE)) log.fine("Session user, not authenticated. sessionId = " + sessionId + "; providerId = " + providerId);
            }
    
            
            // [3] 
            // ??? Does this make sense???
            // TBD: ...
            if(isAuthenticated == false) {
                if(providerId == null || providerId.isEmpty()) {  // Or, when providerId == GAE openId ???? TBD: How to do this ??? 
                    AuthUserService authUserService = AuthUserServiceFactory.getAuthUserService();
                    isAuthenticated = authUserService.isUserLoggedIn();
                    // TBD
                    // if isAuthenticated==true, update authToken and/or DB ???
                    // ??????
                }
            }

        }

        if(log.isLoggable(Level.INFO)) log.info("isUserLoggedIn() sessionId = " + sessionId + "; isAuthenticated = " + isAuthenticated);
        return isAuthenticated;
    }
    
    
    
    
    // Supports:
    // Login/logout through "external auth services" (including our own custom userdb)
    // but, not login/logout through GAE auth service....
    // How to integrate it ???
    
    // TBD:
    // Do this in Auth JSPs ????
    // ....

    
    // TBD:
    public String getUserUsername(HttpSession session, String providerId)
    {
        if(session == null) {
            // ????
            if(log.isLoggable(Level.WARNING)) log.warning("Session is null. providerId = " + providerId);
            return null;
        }
        // For debugging...
        String sessionId = session.getId();

        // Twitter username, etc...
        String username = null;

        // [1] quick shortcut.
        AuthToken authToken = (AuthToken) session.getAttribute(CommonAuthUtil.SESSION_ATTR_AUTHTOKEN);
        if(authToken != null) {
            username = authToken.getUsername(providerId);
            if(username != null) {
                if(log.isLoggable(Level.INFO)) log.info("username found in authToken: sessionId = " + sessionId + "; username = " + username);
            }
        } else {
            if(log.isLoggable(Level.FINE)) log.fine("authToken not found in the session. sessionId = " + sessionId + "; providerId = " + providerId);
        }
        
        // [2] get ExternalUserIdStruct
        if(username == null) {
            ExternalUserIdStruct extId = getExternalUserIdStruct(session, providerId);
            if(extId != null) {
                username = extId.getUsername();
            } else {
                if(log.isLoggable(Level.FINE)) log.fine("ExternalUserIdStruct not found. sessionId = " + sessionId + "; providerId = " + providerId);
            }
        }

        if(log.isLoggable(Level.FINE)) log.fine("getUserUsername() sessionId = " + sessionId + "; username = " + username);
        return username;
    }

    // TBD:
    // ...
    // It returns null if the use is not logged on.....
    // ...
    // TBD:
    // Duplicate code: Instead of calling isUserLoggedIn(), which will end up checking the sames things twice...
    //    We just implement the same thing here....
    // Due to this,
    // this method can be used instead of isUserLoggedIn().
    // Return value null is (sort of) equivalent to isUserLoggedIn() == false...
    // (except for the GAE auth manager part....)
    // (Also, there are some minor differences. Compare the implementations.)
    // --> After some thought...
    // Actually, this is not necessarily true...
    // ExternalUserIdStruct is really an optional field of userAuthStruct...
    // We should really look at authStatus field, and that's it....
    // ....
    // Unfortunately, some use cases may require having to call both methods (with potential DB lookup)
    // (At least, however, we can say, 
    //         if getExternalUserIdStruct() != null, then isUserLoggedIn() == true...)
    // ????
    public ExternalUserIdStruct getExternalUserIdStruct(HttpSession session, String providerId)
    {
        if(session == null) {
            // ????
            if(log.isLoggable(Level.WARNING)) log.warning("Session is null. providerId = " + providerId);
            return null;
        }
        // For debugging...
        String sessionId = session.getId();

        // Return value
        ExternalUserIdStruct extUserId = null;

        boolean isAuthenticatedInAuthToken = false;
        AuthToken authToken = (AuthToken) session.getAttribute(CommonAuthUtil.SESSION_ATTR_AUTHTOKEN);
        if(authToken != null) {
            isAuthenticatedInAuthToken = authToken.isAuthenticated(providerId);
        } else {
            if(log.isLoggable(Level.FINE)) log.fine("authToken not found in the session. sessionId = " + sessionId + "; providerId = " + providerId);
        }

        UserAuthState userAuthState = (UserAuthState) session.getAttribute(AuthUtil.getSessionAttrKeyUserAuthState(providerId));
        if(userAuthState == null) {
            SessionBean sessionBean = (SessionBean) session.getAttribute( AuthConstants.SESSION_ATTR_SESSION_BEAN );
            if(sessionBean != null) {
                String user = sessionBean.getUserId();   // User.guid.
                if(user != null) {                        
                    userAuthState = UserAuthStateHelper.getInstance().findUserAuthStateForUser(user, providerId);
                    if(userAuthState != null) {
                        session.setAttribute(AuthUtil.getSessionAttrKeyUserAuthState(providerId), userAuthState);
                    } else {
                        if(log.isLoggable(Level.FINE)) log.fine("Failed to find userAuthState. sessionId = " + sessionId + "; user = " + user + "; providerId = " + providerId);
                    }
                } else {
                    if(log.isLoggable(Level.WARNING)) log.warning("SessionBean user not found. sessionId = " + sessionId + "; providerId = " + providerId);
                }
            } else {
                if(log.isLoggable(Level.INFO)) log.info("SessionBean not found. sessionId = " + sessionId + "; providerId = " + providerId);
            }
        } else {
            if(log.isLoggable(Level.FINE)) log.fine("userAuthState found in the session. sessionId = " + sessionId + "; providerId = " + providerId);
        }
        if(userAuthState != null) {
            String authStatus = userAuthState.getAuthStatus();
            if(AuthStatus.STATUS_AUTHENTICATED.equals(authStatus)) {
                // boolean isAuthenticated = true;
                extUserId = userAuthState.getExternalId();
                if(extUserId != null) {
                    if(isAuthenticatedInAuthToken == false) {
                        // TBD:
                        String username = extUserId.getUsername();
                        if(username == null || username.isEmpty()) {
                            // TBD:
//                            Long userId = extUserId.getUserId(); 
//                            if(userId != null && userId > 0L) {
//                                username = userId.toString();   // ???
//                                if(log.isLoggable(Level.INFO)) log.info("ExternalUserIdStruct.username not found. userId is used instread. userId = " + userId + "; sessionId = " + sessionId + "; providerId = " + providerId);
//                            }
                            String userId = extUserId.getId(); 
                            if(userId != null && !userId.isEmpty()) {
                                username = userId;   // ???
                                if(log.isLoggable(Level.INFO)) log.info("ExternalUserIdStruct.username not found. userId is used instread. userId = " + userId + "; sessionId = " + sessionId + "; providerId = " + providerId);
                            }
                            // ....
                        }
                        if(username != null && !username.isEmpty()) {   // ????
                            if(log.isLoggable(Level.FINE)) log.fine("username from extUserId: sessionId = " + sessionId + "; username = " + username);
                            if(authToken == null) {
                                authToken = new AuthToken();
                            }
                            authToken.addAuthState(providerId, username);
                            session.setAttribute(CommonAuthUtil.SESSION_ATTR_AUTHTOKEN, authToken);
                        } else {
                            if(log.isLoggable(Level.INFO)) log.info("ExternalUserIdStruct.username not found. sessionId = " + sessionId + "; providerId = " + providerId);
                        }
                    } else {
                        // Nothing to do...
                    }
                } else {
                    if(log.isLoggable(Level.WARNING)) log.warning("ExternalUserIdStruct not found. sessionId = " + sessionId + "; providerId = " + providerId);
                }
                // .....
            } else {
                // Auth_failed, etc. ?
                if(log.isLoggable(Level.INFO)) log.info("User not authenticated. sessionId = " + sessionId + "; authStatus = " + authStatus + "; providerId = " + providerId);
            }
        } else {
            // ????
            if(isAuthenticatedInAuthToken == true) {
                // This is an error....
                // --> See the comment above.... this is not necessarily an error since externalUserId is optional...
                if(log.isLoggable(Level.WARNING)) log.warning("isAuthenticatedInAuthToken == true but userAuthState is not found. sessionId = " + sessionId + "; providerId = " + providerId);
            } else {
                // Simply, user not authenticated....
                // Ignore...
                if(log.isLoggable(Level.FINE)) log.fine("Session user, not authenticated. sessionId = " + sessionId + "; providerId = " + providerId);
            }
        }

        if(log.isLoggable(Level.FINE)) log.fine("getExternalUserId() sessionId = " + sessionId);
        return extUserId;
    }
    
    
    
    // session is really an "in-out" param.
    public boolean processUserLogIn(HttpSession session, String providerId, UserAuthState userAuthState, boolean createNew)
    {
        if(session == null) {
            // ????
            if(log.isLoggable(Level.WARNING)) log.warning("Session is null. providerId = " + providerId);
            return false;
        }
        // For debugging...
        String sessionId = session.getId();
        if(log.isLoggable(Level.FINE)) log.fine("processUserLogIn(): sessionId = " + sessionId + "; providerId = " + providerId + "; createNew = " + createNew);

        // TBD:
        boolean processed = false;
        userAuthState = (UserAuthStateBean) UserAuthStateHelper.getInstance().saveUserAuthState(userAuthState, createNew);
        if(userAuthState != null) {
            session.setAttribute(AuthUtil.getSessionAttrKeyUserAuthState(providerId), userAuthState);
            // ...
            AuthToken authToken = (AuthToken) session.getAttribute(CommonAuthUtil.SESSION_ATTR_AUTHTOKEN);
            // ...
            String authStatus = userAuthState.getAuthStatus();
            if(AuthStatus.STATUS_AUTHENTICATED.equals(authStatus)) {
                // Long userId = null;
                ExternalUserIdStruct extUserId = userAuthState.getExternalId();
                if(extUserId != null) {
                    // TBD:
                    String username = extUserId.getUsername();
                    if(username == null || username.isEmpty()) {
                        // TBD:
//                        Long userId = extUserId.getUserId(); 
//                        if(userId != null && userId > 0L) {
//                            username = userId.toString();   // ???
//                            if(log.isLoggable(Level.INFO)) log.info("ExternalUserIdStruct.username not found. userId is used instread. userId = " + userId + "; sessionId = " + sessionId + "; providerId = " + providerId);
//                        }
                        String userId = extUserId.getId(); 
                        if(userId != null && !userId.isEmpty()) {
                            username = userId;   // ???
                            if(log.isLoggable(Level.INFO)) log.info("ExternalUserIdStruct.username not found. userId is used instread. userId = " + userId + "; sessionId = " + sessionId + "; providerId = " + providerId);
                        }
                        // ....
                    }
                    // etc.... ???
                    if(username != null && !username.isEmpty()) {   // ????
                        if(authToken == null) {
                            authToken = new AuthToken();
                        }
                        authToken.addAuthState(providerId, username);
                        session.setAttribute(CommonAuthUtil.SESSION_ATTR_AUTHTOKEN, authToken);
                    } else {
                        if(log.isLoggable(Level.WARNING)) log.warning("ExternalUserIdStruct.username not found. AuthToken session attribute cannot be set. sessionId = " + sessionId + "; providerId = " + providerId);
                    }
                } else {
                    if(log.isLoggable(Level.WARNING)) log.warning("ExternalUserIdStruct not found. sessionId = " + sessionId + "; providerId = " + providerId);
                }
                processed = true;
            } else {
                if(authToken != null) {
                    authToken.removeAuthState(providerId);
                    session.setAttribute(CommonAuthUtil.SESSION_ATTR_AUTHTOKEN, authToken);
                }
            }
        } else {
            // ???
            session.removeAttribute(AuthUtil.getSessionAttrKeyUserAuthState(providerId));
            if(log.isLoggable(Level.FINE)) log.fine("Failed to save userAuthState. Session userAuthState removed. sessionId = " + sessionId);
        }

        if(log.isLoggable(Level.INFO)) log.info("processUserLogIn(): sessionId = " + sessionId + "; providerId = " + providerId + "; processed = " + processed);
        return processed;
    }


    // session is an "in-out" param.
    // Note that returning false does not mean the user has not been logged out.
    // The user may not have been logged in in the first place...
    public boolean processUserLogOut(HttpSession session, String providerId)
    {
        if(session == null) {
            // ????
            if(log.isLoggable(Level.WARNING)) log.warning("Session is null. providerId = " + providerId);
            return false;
        }
        // For debugging...
        String sessionId = session.getId();
        if(log.isLoggable(Level.FINE)) log.fine("processUserLogOut(): sessionId = " + sessionId + "; providerId = " + providerId);

        // TBD:
        boolean processed = false;
        AuthToken authToken = (AuthToken) session.getAttribute(CommonAuthUtil.SESSION_ATTR_AUTHTOKEN);
        if(authToken != null) {
            authToken.removeAuthState(providerId);
            if(authToken.isAuthenticated()) {    // true means the user is authenticated through at least one provider...
                session.setAttribute(CommonAuthUtil.SESSION_ATTR_AUTHTOKEN, authToken);
            } else {
                session.removeAttribute(CommonAuthUtil.SESSION_ATTR_AUTHTOKEN);
            }
            processed = true;
            if(log.isLoggable(Level.FINE)) log.fine("Session authToken removed/updated. sessionId = " + sessionId + "; providerId = " + providerId);
        } else {
            // When can this happen ????
            if(log.isLoggable(Level.INFO)) log.info("authToken not found in the session. sessionId = " + sessionId + "; providerId = " + providerId);
        }

        UserAuthState userAuthState = (UserAuthState) session.getAttribute(AuthUtil.getSessionAttrKeyUserAuthState(providerId));
        if(userAuthState != null) {
            session.removeAttribute(AuthUtil.getSessionAttrKeyUserAuthState(providerId));
            processed = true;
            if(log.isLoggable(Level.FINE)) log.fine("Session userAuthState removed. sessionId = " + sessionId + "; providerId = " + providerId);
        } else {
            // ????
            if(log.isLoggable(Level.INFO)) log.info("userAuthState not found in the session. sessionId = " + sessionId + "; providerId = " + providerId);
        }

        SessionBean sessionBean = (SessionBean) session.getAttribute( AuthConstants.SESSION_ATTR_SESSION_BEAN );
        if(sessionBean != null) {
            String user = sessionBean.getUserId();   // User.guid.
            if(user != null) {
                // TBD:
                // (a) Just update the auth status ???
                // userAuthState = UserAuthStateHelper.getInstance().findUserAuthStateForUser(user, providerId);
                // if(userAuthState != null) {
                //     ((UserAuthStateBean) userAuthState).setAuthStatus(AuthStatus.STATUS_LOGGEDOUT);
                //     userAuthState = UserAuthStateHelper.getInstance().refreshUserAuthState(userAuthState);
                // }
                // (b) Or, delete the record ???
                boolean suc = UserAuthStateHelper.getInstance().deleteUserAuthState(user, providerId);
                processed = suc;
                if(log.isLoggable(Level.FINE)) log.fine("Deleting userAuthState. sessionId = " + sessionId + "; providerId = " + providerId + "; suc = " + suc);
            } else {
                if(log.isLoggable(Level.WARNING)) log.warning("SessionBean user not found. sessionId = " + sessionId + "; providerId = " + providerId);
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.info("sessionBean not found in the session. sessionId = " + sessionId + "; providerId = " + providerId);
        }

        // TBD:
        if(processed == true) {
            // TBD:
            // Log out from the external service (e.g., Twitter) ???
            // ????
            // On second thought, logging out from the external server may not be necessary ???
            // Some services may not even support "log out" ???? (Twitter ???)
            // ???
            // Maybe, just clear the cookie ???
            // ....
        }

        if(log.isLoggable(Level.INFO)) log.info("processUserLogOut() sessionId = " + sessionId + "; providerId = " + providerId + "; processed = " + processed);
        return processed;
    }

    
    
}
