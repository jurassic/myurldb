package com.cannyurl.app.auth;

import java.util.logging.Logger;

import javax.servlet.ServletRequest;

import com.cannyurl.af.auth.common.CommonAuthUtil;
import com.cannyurl.af.auth.googleapps.GoogleAppsAuthUtil;
import com.cannyurl.af.auth.googleplus.GooglePlusAuthUtil;


// 3/03/13
//...


public class AuthUtil
{
    private static final Logger log = Logger.getLogger(AuthUtil.class.getName());

    // TBD... These libs are, if enabled, to be used for authentication purposes only..
    public static final String CONFIG_KEY_USELIBRARY_TWITTER4J = "cannyurlapp.uselibrary.twitter4j";
    public static final String CONFIG_KEY_USELIBRARY_SOCIALAUTH = "cannyurlapp.uselibrary.socialauth";
    // ...

    //public static final String PARAM_CODE = "code";
    // ...
    
    // TBD: For google apps sso support
    // public static final String PARAM_GOOGLEAPPS_DOMAIN = "hd";    // ????
    // .....

    // This is used to indicate, on authHandler page, which callback page the request comes from...
    public static final String REQUEST_ATTR_CALLBACKPAGE  = "com.cannyurl.app.auth.callbackpage";
    public static final String SESSION_ATTR_AUTH_MODE  = "com.cannyurl.app.auth.authmode";
    public static final String SESSION_ATTR_AUTH_PROVIDERID  = "com.cannyurl.app.auth.providerid";
    public static final String SESSION_ATTR_AUTH_FEDERATEDIDENTITY  = "com.cannyurl.app.auth.federated_identity";
    // ....
    // The session attr for each provider will be prefix + providerid.
    // (this means, we can support only one auth (e.g., username/login) per provider...)
    public static final String SESSION_ATTR_PREFIX_USERAUTHSTATE  = "com.cannyurl.app.auth.userauthstate.";
    // ....
    public static final String REQUEST_ATTR_AUTHSTATEBEAN = "com.cannyurl.app.auth.authstatebean";
    // ....
    

    // TBD:
    public static final String DEFAULT_FORM_ID_TOPNAVBAR_AUTH_FORM = "_topnavbar_auth_form";         // ???
    // ....

    
    // Static methods only.
    private AuthUtil() {}
    
    

    // TBD
    public static String getSessionAttrKeyUserAuthState()
    {
        return getSessionAttrKeyUserAuthState(CommonAuthUtil.AUTH_SERVICE_PROVIDERID_CUSTOM);
    }
    public static String getSessionAttrKeyUserAuthState(String providerId)
    {
        // TBD: Validation?
        if(providerId != null) {
            return SESSION_ATTR_PREFIX_USERAUTHSTATE + providerId;
        } else {
            // ????
            return SESSION_ATTR_PREFIX_USERAUTHSTATE;
        }
    }

    
    // TBD...
    public static String getGoogleAppsSsoAppsDomain(ServletRequest request)
    {
        String appsDomain = null;
        if(request != null) {
            appsDomain = request.getParameter(GoogleAppsAuthUtil.PARAM_HD);
        }
        return appsDomain;
    }


    public static String getDefaultTopNavBarAuthFormId()
    {
        return DEFAULT_FORM_ID_TOPNAVBAR_AUTH_FORM;
    }


}
