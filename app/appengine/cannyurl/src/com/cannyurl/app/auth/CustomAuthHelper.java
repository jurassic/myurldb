package com.cannyurl.app.auth;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cannyurl.af.bean.HashedPasswordStructBean;
import com.cannyurl.af.bean.UserPasswordBean;
import com.cannyurl.app.service.UserAppService;
import com.cannyurl.app.service.UserPasswordAppService;
import com.cannyurl.common.HashType;
import com.myurldb.ws.BaseException;
import com.myurldb.ws.HashedPasswordStruct;
import com.myurldb.ws.UserPassword;
import com.myurldb.ws.core.GUID;


// 3/03/13
// 2/17/13: Password hashing support
//...


public class CustomAuthHelper
{
    private static final Logger log = Logger.getLogger(CustomAuthHelper.class.getName());

    // TBD: These need to match settings in web.xml
    //private static final String AUTH_HANDLER_URLPATH = "/auth/handler";
    // ...

    // "Lazy initialization"
    //private Boolean mUseSocialAuth = null;
    // ...
    private UserAppService userAppService = null;
    private UserPasswordAppService userPasswordAppService = null;
    // ...

    private CustomAuthHelper() {}

    // Initialization-on-demand holder.
    private static final class CustomAuthHelperHolder
    {
        private static final CustomAuthHelper INSTANCE = new CustomAuthHelper();
    }

    // Singleton method
    public static CustomAuthHelper getInstance()
    {
        return CustomAuthHelperHolder.INSTANCE;
    }

    private UserAppService getUserService()
    {
        if(userAppService == null) {
            userAppService = new UserAppService();
        }
        return userAppService;
    }
    private UserPasswordAppService getUserPasswordService()
    {
        if(userPasswordAppService == null) {
            userPasswordAppService = new UserPasswordAppService();
        }
        return userPasswordAppService;
    }

    
    
    
    // TBD:
    // Check username availability.
    // Same with email and openId, etc....
    // ...
    
    public boolean isUsernameAvailable(String username)
    {
        
        
        // TBD
        return false;
    }
    
    
    
    
    

    public boolean saveUsername(String user, String username)
    {
        boolean suc = false;
        // ....
        return suc;        
    }
    public boolean saveUsername(String user, String username, String email, String openId)
    {
        boolean suc = false;
        // ....
        return suc;        
    }


    public boolean saveUserPassword(String user, String passwd)
    {
        boolean suc = false;
        // ....
        return suc;        
    }
    public boolean saveUserPassword(String user, String username, String email, String openId, String passwd)
    {
        boolean suc = false;
        // ....
        return suc;        
    }

    
    // Use any "id" - password combination...
    public boolean registerUser(String user, String username, String email, String openId, String passwd)
    {
        boolean suc = false;
        // ....
        return suc;        
    }
    
    // Use any "id" - password combination...
    public boolean authenticateUser(String user, String username, String email, String openId, String passwd)
    {
        boolean suc = false;
        // ....
        return suc;        
    }

    
    // TBD:
    private UserPasswordBean hashAndSetPassword(UserPasswordBean userPassword, String passwd)
    {
        if(userPassword == null) {
            // What to do???
            log.warning("Input userPassword bean is null.");
            return null;
        }

        // temporary
        String hashMethod = null;
        String salt = null; 
        HashedPasswordStruct oldPasswordStruct = userPassword.getPassword();
        // Note:
        // Because we don't do full fetch in the find() methods,
        // the existing HashedPasswordStruct will not be returned....
        // Therefore, HashedPasswordStruct will always be overwritten with new passwd/salt/hash aglo, etc...
        if(oldPasswordStruct != null) {   // This is always false, in the current design (defaultFetch==false)
            // Always try to reuse the existing value, if exits....
            hashMethod = oldPasswordStruct.getAlgorithm();
            salt = oldPasswordStruct.getSalt();
            // TBD: Does this make sense???
            // In any case, in the normal use cases, the plain text field should be null....
//            if(passwd == null) {    // Is an emtpy string a valid password????
//                passwd = oldPasswordStruct.getPlainText();  // ???
//            }
        }

        if(passwd == null) {    // Do we allow an empty password????
            // What to do???
            log.warning("Input passwd is null.");
            return null;
        }

        if(! HashType.isSupportedAlgorithm(hashMethod)) {
            // TBD: Get default hash algo from config???
            hashMethod = HashType.TYPE_SHA256;
        }
        if(salt == null || salt.isEmpty()) {
            salt = CustomAuthUtil.generateRandomSalt();
        }
        
        // New hashed password..
        String hashedPwd = CustomAuthUtil.hashPassword(passwd, salt, hashMethod);

        HashedPasswordStructBean passwordStruct = null;
        if(oldPasswordStruct != null) {
            // Reuse???
            passwordStruct = (HashedPasswordStructBean) oldPasswordStruct;
        } else {
            passwordStruct = new HashedPasswordStructBean();
            passwordStruct.setUuid(GUID.generate());
        }
        // TBD: Plain password should be cleared when saved??? Or, clear it now?????
        // passwordStruct.setPlainText(passwd); 
        passwordStruct.setPlainText(null);        // ????? 
        // ....
        passwordStruct.setHashedText(hashedPwd);
        passwordStruct.setSalt(salt);
        passwordStruct.setAlgorithm(hashMethod);
        
        userPassword.setPassword(passwordStruct);

        userPassword.setLastResetTime(System.currentTimeMillis());
        return userPassword;
    }


    public UserPassword constructUserPassword(String user, String username, String email, String openId, String passwd)
    {
        if(passwd == null) {   // Do we allow empty passwd???
            return null;  // ???
        }
        UserPasswordBean userPassword = new UserPasswordBean();
        userPassword.setUser(user);
        if(username != null) {
            userPassword.setUsername(username);
        }
        if(email != null) {
            userPassword.setEmail(email);
        }
        if(openId != null) {
            userPassword.setOpenId(openId);
        }
        userPassword = hashAndSetPassword(userPassword, passwd);
        try {
            userPassword = (UserPasswordBean) getUserPasswordService().constructUserPassword(userPassword);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to saved userPassword.", e);
        }        
        return userPassword;
    }


    public UserPassword updateUserPassword(String user, String passwd)
    {
        UserPassword userPassword = findPasswordForUser(user);
        if(userPassword == null) {
            // What to do???
            //userPassword = constructUserPassword(user, null, null, null, passwd);
            return null;
        } else {
            userPassword = hashAndSetPassword((UserPasswordBean) userPassword, passwd);
            try {
                userPassword = getUserPasswordService().refreshUserPassword(userPassword);
            } catch (BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to update the passwd for user = " + user, e);
                return null;  // ???
            }
        }
        return userPassword;
    }

    public UserPassword updateUserPasswordByUsername(String username, String passwd)
    {
        UserPassword userPassword = findPasswordByUsername(username);
        if(userPassword == null) {
            // What to do???
            //userPassword = constructUserPassword(user, null, null, null, passwd);
            return null;
        } else {
            userPassword = hashAndSetPassword((UserPasswordBean) userPassword, passwd);
            try {
                userPassword = getUserPasswordService().refreshUserPassword(userPassword);
            } catch (BaseException e) {
                log.log(Level.WARNING, "Failed to update the passwd for username = " + username, e);
                return null;  // ???
            }
        }
        return userPassword;
    }

    public UserPassword updateUserPasswordByEmail(String email, String passwd)
    {
        UserPassword userPassword = findPasswordByEmail(email);
        if(userPassword == null) {
            // What to do???
            //userPassword = constructUserPassword(user, null, null, null, passwd);
            return null;
        } else {
            userPassword = hashAndSetPassword((UserPasswordBean) userPassword, passwd);
            try {
                userPassword = getUserPasswordService().refreshUserPassword(userPassword);
            } catch (BaseException e) {
                log.log(Level.WARNING, "Failed to update the passwd for email = " + email, e);
                return null;  // ???
            }
        }
        return userPassword;
    }

    public UserPassword updateUserPasswordByOpenId(String openId, String passwd)
    {
        UserPassword userPassword = findPasswordByOpenId(openId);
        if(userPassword == null) {
            // What to do???
            //userPassword = constructUserPassword(user, null, null, null, passwd);
            return null;
        } else {
            userPassword = hashAndSetPassword((UserPasswordBean) userPassword, passwd);
            try {
                userPassword = getUserPasswordService().refreshUserPassword(userPassword);
            } catch (BaseException e) {
                log.log(Level.WARNING, "Failed to update the passwd for openId = " + openId, e);
                return null;  // ???
            }
        }
        return userPassword;
    }

    

    // TBD:
    // Note that the following find() methods do not do full fetch.
    // However, in order to be able to authenticate the user
    //      (e.g., by comparing the hashed value of the user-provided password and the stored hashedPsssword),
    // we will need to do full fetch....
    // TBD.....

    
    public UserPassword findPasswordForUser(String userGuid)
    {
        UserPassword userPassword = null;
        try {
            List<UserPassword> beans = null;
            // TBD:
            // Return only the beans which were created while the user was logged on ????
            // ... (e.g., use Memo.loggedOn field???)
            String filter = "user=='" + userGuid + "'";
            String ordering = "createdTime desc";
            beans = getUserPasswordService().findUserPasswords(filter, ordering, null, null, null, null, null, null);   
            if(beans != null && beans.size() > 0) {
                int size = beans.size();
                userPassword = beans.get(0);                
                if(size > 1) {
                    log.log(Level.WARNING, size + " beans found for userGuid = " + userGuid);
                }
                // Note:
                // No full fetch...
                // This means that HashedPasswordStruct will not be returned....
                // If we update it, HashedPasswordStruct will always be overwritten with new passwd/salt/hash aglo, etc...
                // ...
            } else {
                log.log(Level.WARNING, "No beans for given userGuid = " + userGuid);
            }
        } catch (BaseException e) {
            log.log(Level.SEVERE, "Failed to find beans for given userGuid = " + userGuid, e);
            return null;
        }
        return userPassword;
    }

    public UserPassword findPasswordByUsername(String username)
    {
        UserPassword userPassword = null;
        try {
            List<UserPassword> beans = null;
            String filter = "username=='" + username + "'";
            String ordering = "createdTime desc";
            beans = getUserPasswordService().findUserPasswords(filter, ordering, null, null, null, null, null, null);   
            if(beans != null && beans.size() > 0) {
                int size = beans.size();
                userPassword = beans.get(0);                
                if(size > 1) {
                    log.log(Level.WARNING, size + " beans found for username = " + username);
                }
                // Note:
                // No full fetch...
                // This means that HashedPasswordStruct will not be returned....
                // If we update it, HashedPasswordStruct will always be overwritten with new passwd/salt/hash aglo, etc...
                // ...
            } else {
                log.log(Level.WARNING, "No beans for given username = " + username);
            }
        } catch (BaseException e) {
            log.log(Level.SEVERE, "Failed to find beans for given username = " + username, e);
            return null;
        }
        return userPassword;
    }

    public UserPassword findPasswordByEmail(String email)
    {
        UserPassword userPassword = null;
        try {
            List<UserPassword> beans = null;
            String filter = "email=='" + email + "'";
            String ordering = "createdTime desc";
            beans = getUserPasswordService().findUserPasswords(filter, ordering, null, null, null, null, null, null);   
            if(beans != null && beans.size() > 0) {
                int size = beans.size();
                userPassword = beans.get(0);                
                if(size > 1) {
                    log.log(Level.WARNING, size + " beans found for email = " + email);
                }
                // Note:
                // No full fetch...
                // This means that HashedPasswordStruct will not be returned....
                // If we update it, HashedPasswordStruct will always be overwritten with new passwd/salt/hash aglo, etc...
                // ...
            } else {
                log.log(Level.WARNING, "No beans for given email = " + email);
            }
        } catch (BaseException e) {
            log.log(Level.SEVERE, "Failed to find beans for given email = " + email, e);
            return null;
        }
        return userPassword;
    }

    public UserPassword findPasswordByOpenId(String openId)
    {
        UserPassword userPassword = null;
        try {
            List<UserPassword> beans = null;
            String filter = "openId=='" + openId + "'";
            String ordering = "createdTime desc";
            beans = getUserPasswordService().findUserPasswords(filter, ordering, null, null, null, null, null, null);   
            if(beans != null && beans.size() > 0) {
                int size = beans.size();
                userPassword = beans.get(0);                
                if(size > 1) {
                    log.log(Level.WARNING, size + " beans found for openId = " + openId);
                }
                // Note:
                // No full fetch...
                // This means that HashedPasswordStruct will not be returned....
                // If we update it, HashedPasswordStruct will always be overwritten with new passwd/salt/hash aglo, etc...
                // ...
            } else {
                log.log(Level.WARNING, "No beans for given openId = " + openId);
            }
        } catch (BaseException e) {
            log.log(Level.SEVERE, "Failed to find beans for given openId = " + openId, e);
            return null;
        }
        return userPassword;
    }

    
}
