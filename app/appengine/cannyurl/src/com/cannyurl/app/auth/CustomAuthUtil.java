package com.cannyurl.app.auth;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.cannyurl.common.HashType;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.util.HashUtil;


// 3/03/13
// 2/17/13: Password hashing support
//...


public class CustomAuthUtil
{
    private static final Logger log = Logger.getLogger(CustomAuthUtil.class.getName());

    // temporary
    // Get default hash algo from config???
    private static final String DEFAULT_HASH_ALGORITHM = HashType.TYPE_SHA256;
    // ...

    // TBD:
    public static final String PARAM_ACTION = "action";
    // ...
    
    
    // Static methods only.
    private CustomAuthUtil() {}
    
    public static String generateRandomSalt()
    {
        final int defaultLen = 5;     // 5 chars should suffice, normally, in password hashing...
        return generateRandomSalt(defaultLen);
    }
    public static String generateRandomSalt(final int len)
    {
        // temporary
        String salt = GUID.generateShortString();   // This returns 22 char string.
        if(len > 0 && len < 22) {
            salt = salt.substring(0, len);          // Just use the first len chars...
        }
        // ...
        if(log.isLoggable(Level.FINER)) log.finer("New salt generated: " + salt);
        return salt;
    }

    // TBD:
    public static String hashPassword(String passwd, String salt)
    {
        // TBD: Get default hash algo from config???
        return hashPassword(passwd, salt, DEFAULT_HASH_ALGORITHM);
    }
    public static String hashPassword(String passwd, String salt, String hashMethod)
    {
        // TBD:
        String hashed = null;
        if(HashType.TYPE_MD2.equals(hashMethod)) {
            hashed = HashUtil.generateMd2Hash(passwd, salt);
        } else if(HashType.TYPE_MD5.equals(hashMethod)) {
            hashed = HashUtil.generateMd5Hash(passwd, salt);
        } else if(HashType.TYPE_SHA1.equals(hashMethod)) {
            hashed = HashUtil.generateSha1Hash(passwd, salt);
        } else if(HashType.TYPE_SHA256.equals(hashMethod)) {
            hashed = HashUtil.generateSha256Hash(passwd, salt);
        } else if(HashType.TYPE_SHA384.equals(hashMethod)) {
            hashed = HashUtil.generateSha384Hash(passwd, salt);
        } else if(HashType.TYPE_SHA512.equals(hashMethod)) {
            hashed = HashUtil.generateSha512Hash(passwd, salt);
        } else {
            // Throw not-implemented exception...
            // Or, just use the system default hash algorithm ????
        }
        return hashed;
    }


}
