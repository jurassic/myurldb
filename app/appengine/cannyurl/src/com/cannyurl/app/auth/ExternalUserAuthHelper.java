package com.cannyurl.app.auth;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cannyurl.af.bean.ExternalUserAuthBean;
import com.cannyurl.app.service.ExternalUserAuthAppService;
import com.cannyurl.app.service.UserAppService;
import com.myurldb.ws.BaseException;
import com.myurldb.ws.ExternalUserAuth;
import com.myurldb.ws.core.GUID;


// 3/03/13
// 2/16/13: Fixed a missing-quote bug around externalUserId.id value.
//...


public class ExternalUserAuthHelper
{
    private static final Logger log = Logger.getLogger(ExternalUserAuthHelper.class.getName());

    private ExternalUserAuthHelper() {}

    // TBD: Is this safe for concurrent calls??
    private UserAppService userAppService = null;
    private ExternalUserAuthAppService userAuthAppService = null;
    // etc...


    private UserAppService getUserService()
    {
        if(userAppService == null) {
            userAppService = new UserAppService();
        }
        return userAppService;
    }   
    private ExternalUserAuthAppService getExternalUserAuthService()
    {
        if(userAuthAppService == null) {
            userAuthAppService = new ExternalUserAuthAppService();
        }
        return userAuthAppService;
    }
    // etc. ...


    // Initialization-on-demand holder.
    private static final class ExternalUserAuthHelperHolder
    {
        private static final ExternalUserAuthHelper INSTANCE = new ExternalUserAuthHelper();
    }

    // Singleton method
    public static ExternalUserAuthHelper getInstance()
    {
        return ExternalUserAuthHelperHolder.INSTANCE;
    }

    
    public ExternalUserAuth saveExternalUserAuth(ExternalUserAuth userAuth, boolean createNew)
    {
        if(userAuth == null) {
            log.log(Level.WARNING, "userAuth param is null.");
            return null;
        }
        try {
            if(createNew) {
                String guid = userAuth.getGuid();
                if(guid == null || guid.isEmpty()) {
                    guid = GUID.generate();
                    ((ExternalUserAuthBean) userAuth).setGuid(guid); 
                }
                userAuth = getExternalUserAuthService().constructExternalUserAuth(userAuth);
            } else {
                userAuth = getExternalUserAuthService().refreshExternalUserAuth(userAuth);
            }
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to save ExternalUserAuth.", e);
            // ???
        }
        return userAuth;
    }
    
    
    public ExternalUserAuth getExternalUserAuth(String guid) 
    {
        ExternalUserAuth userAuth = null;
        try {
            userAuth = getExternalUserAuthService().getExternalUserAuth(guid);
        } catch (BaseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve the userAuth for guid = " + guid, e);
            // ???
        }
        return userAuth;
    }
    
    
    // TBD:
    public ExternalUserAuth findExternalUserAuthByUser(String user, String providerId, String extUserAuthGuid)
    {
        ExternalUserAuth userAuth = null;

        if(extUserAuthGuid != null) {
            userAuth = (ExternalUserAuthBean) ExternalUserAuthHelper.getInstance().getExternalUserAuth(extUserAuthGuid);
            if(userAuth != null) {
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "ExternalUserAuth found for extUserAuthGuid = " + extUserAuthGuid + "; providerId = " + providerId + "; user guid = " + user);
            }
        }
        if(userAuth == null) {
            // Reuse any existing one in DB, if any... ????
            userAuth = (ExternalUserAuthBean) ExternalUserAuthHelper.getInstance().findExternalUserAuthByUser(user, providerId);
            if(userAuth != null) {
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "ExternalUserAuth found for providerId = " + providerId + "; user guid = " + user);
            }
        }

        return userAuth;
    }
    
    // TBD:
    public ExternalUserAuth findExternalUserAuthByUser(String user, String providerId)
    {
        ExternalUserAuth userAuth = null;

        String filter = "user=='" + user + "'";   // authStatus ???
        if(providerId != null) {
            filter += " && providerId=='" + providerId + "'";
        }
        String ordering = "createdTime desc";
        try {
            List<ExternalUserAuth> list = getExternalUserAuthService().findExternalUserAuths(filter, ordering, null, null, null, null, 0L, 2);  // ???
            if(list == null || list.isEmpty()) {
                // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "ExternalUserAuth list is empty for providerId = " + providerId + "; user guid = " + user);
            } else {
                // What if there are more than one????
                userAuth = list.get(0);
                // Do full fetch.
                userAuth = getExternalUserAuth(userAuth.getGuid());
            }
        } catch (BaseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve the userAuth list for providerId = " + providerId + "; user guid = " + user, e);
            // ???
        }

        return userAuth;
    }

    // TBD:
    public ExternalUserAuth findExternalUserAuthByUserId(long userId, String providerId)
    {
        return findExternalUserAuthByUserId(String.valueOf(userId), providerId);
    }
    public ExternalUserAuth findExternalUserAuthByUserId(String userId, String providerId)
    {
        ExternalUserAuth userAuth = null;

        String filter = "externalUserId.id=='" + userId + "'";   // authStatus ???
        if(providerId != null) {
            filter += " && providerId=='" + providerId + "'";
        }
        String ordering = "createdTime desc";
        try {
            List<ExternalUserAuth> list = getExternalUserAuthService().findExternalUserAuths(filter, ordering, null, null, null, null, 0L, 2);  // ???
            if(list == null || list.isEmpty()) {
                // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "ExternalUserAuth list is empty for providerId = " + providerId + "; userId = " + userId);
            } else {
                // What if there are more than one????
                userAuth = list.get(0);
                // Do full fetch.
                userAuth = getExternalUserAuth(userAuth.getGuid());
            }
        } catch (BaseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve the userAuth list for providerId = " + providerId + "; userId = " + userId, e);
            // ???
        }

        return userAuth;
    }
    
    // TBD:
    public ExternalUserAuth findExternalUserAuthByUsername(String username, String providerId)
    {
        ExternalUserAuth userAuth = null;

        String filter = "externalUserId.username=='" + username + "'";   // authStatus ???
        if(providerId != null) {
            filter += " && providerId=='" + providerId + "'";
        }
        String ordering = "createdTime desc";
        try {
            List<ExternalUserAuth> list = getExternalUserAuthService().findExternalUserAuths(filter, ordering, null, null, null, null, 0L, 2);  // ???
            if(list == null || list.isEmpty()) {
                // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "ExternalUserAuth list is empty for providerId = " + providerId + "; username = " + username);
            } else {
                // What if there are more than one????
                userAuth = list.get(0);
                // Do full fetch.
                userAuth = getExternalUserAuth(userAuth.getGuid());
            }
        } catch (BaseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve the userAuth list for providerId = " + providerId + "; username = " + username, e);
            // ???
        }

        return userAuth;
    }
    
    // TBD:
    public ExternalUserAuth findExternalUserAuthByEmail(String email, String providerId)
    {
        ExternalUserAuth userAuth = null;

        String filter = "externalUserId.email=='" + email + "'";   // authStatus ???
        if(providerId != null) {
            filter += " && providerId=='" + providerId + "'";
        }
        String ordering = "createdTime desc";
        try {
            List<ExternalUserAuth> list = getExternalUserAuthService().findExternalUserAuths(filter, ordering, null, null, null, null, 0L, 2);  // ???
            if(list == null || list.isEmpty()) {
                // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "ExternalUserAuth list is empty for providerId = " + providerId + "; email = " + email);
            } else {
                // What if there are more than one????
                userAuth = list.get(0);
                // Do full fetch.
                userAuth = getExternalUserAuth(userAuth.getGuid());
            }
        } catch (BaseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve the userAuth list for providerId = " + providerId + "; email = " + email, e);
            // ???
        }

        return userAuth;
    }

    
    
    
    // TBD:
    public List<String> getExternalUserAuthKeysForUser(String user)
    {
        return getExternalUserAuthKeysForUser(user, (Integer) null);
    }
    public List<String> getExternalUserAuthKeysForUser(String user, Integer maxCount)
    {
        return getExternalUserAuthKeysForUser(user, null, maxCount);
    }
    public List<String> getExternalUserAuthKeysForUser(String user, String providerId, Integer maxCount)
    {
        List<String> keys = null;

        String filter = "user=='" + user + "'";   // authStatus ???
        if(providerId != null) {
            filter += " && providerId=='" + providerId + "'";
        }
        String ordering = "createdTime desc";
        try {
            keys = getExternalUserAuthService().findExternalUserAuthKeys(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve userAuthState key list.", e);
        }

        return keys;
    }

    public List<ExternalUserAuth> getExternalUserAuthsForUser(String user)
    {
        return getExternalUserAuthsForUser(user, (Integer) null);
    }
    public List<ExternalUserAuth> getExternalUserAuthsForUser(String user, Integer maxCount)
    {
        return getExternalUserAuthsForUser(user, null, maxCount);
    }
    public List<ExternalUserAuth> getExternalUserAuthsForUser(String user, String providerId, Integer maxCount)
    {
        List<ExternalUserAuth> beans = null;
        
        String filter = "user=='" + user + "'";   // authStatus ???
        if(providerId != null) {
            filter += " && providerId=='" + providerId + "'";
        }
        String ordering = "createdTime desc";
        try {
            beans = getExternalUserAuthService().findExternalUserAuths(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve userAuthState list.", e);
        }

        return beans;
    }
    
    
    
    
    // TBD:
    public List<String> getExternalUserAuthKeysByUsername(String username, String providerId)
    {
        return getExternalUserAuthKeysByUsername(username, providerId, null);
    }
    public List<String> getExternalUserAuthKeysByUsername(String username, String providerId, Integer maxCount)
    {
        List<String> keys = null;

        String filter = "externalUserId.username=='" + username + "'";
        if(providerId != null) {
            filter += " && providerId=='" + providerId + "'";   // authStatus ???
        }
        String ordering = "createdTime desc";
        try {
            keys = getExternalUserAuthService().findExternalUserAuthKeys(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve userAuthState key list.", e);
        }

        return keys;
    }

    public List<ExternalUserAuth> getExternalUserAuthByUsername(String username, String providerId)
    {
        return getExternalUserAuthByUsername(username, providerId, null);
    }
    public List<ExternalUserAuth> getExternalUserAuthByUsername(String username, String providerId, Integer maxCount)
    {
        List<ExternalUserAuth> beans = null;
        
        String filter = "externalUserId.username=='" + username + "'";
        if(providerId != null) {
            filter += " && providerId=='" + providerId + "'";   // authStatus ???
        }
        String ordering = "createdTime desc";
        try {
            beans = getExternalUserAuthService().findExternalUserAuths(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve userAuthState list.", e);
        }

        return beans;
    }

    
    // TBD:
    public List<String> getExternalUserAuthKeysByUserId(Long userId, String providerId)
    {
        return getExternalUserAuthKeysByUserId(String.valueOf(userId), providerId);
    }
    public List<String> getExternalUserAuthKeysByUserId(String userId, String providerId)
    {
        return getExternalUserAuthKeysByUserId(userId, providerId, null);
    }
    public List<String> getExternalUserAuthKeysByUserId(Long userId, String providerId, Integer maxCount)
    {
        return getExternalUserAuthKeysByUserId(String.valueOf(userId), providerId, maxCount);
    }
    public List<String> getExternalUserAuthKeysByUserId(String userId, String providerId, Integer maxCount)
    {
        List<String> keys = null;

        String filter = "externalUserId.id=='" + userId + "'";
        if(providerId != null) {
            filter += " && providerId=='" + providerId + "'";   // authStatus ???
        }
        String ordering = "createdTime desc";
        try {
            keys = getExternalUserAuthService().findExternalUserAuthKeys(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve userAuthState key list.", e);
        }

        return keys;
    }

    public List<ExternalUserAuth> getExternalUserAuthByUserId(Long userId, String providerId)
    {
        return getExternalUserAuthByUserId(String.valueOf(userId), providerId);
    }
    public List<ExternalUserAuth> getExternalUserAuthByUserId(String userId, String providerId)
    {
        return getExternalUserAuthByUserId(userId, providerId, null);
    }
    public List<ExternalUserAuth> getExternalUserAuthByUserId(Long userId, String providerId, Integer maxCount)
    {
        return getExternalUserAuthByUserId(String.valueOf(userId), providerId, maxCount);
    }
    public List<ExternalUserAuth> getExternalUserAuthByUserId(String userId, String providerId, Integer maxCount)
    {
        List<ExternalUserAuth> beans = null;
        
        String filter = "externalUserId.id=='" + userId + "'";
        if(providerId != null) {
            filter += " && providerId=='" + providerId + "'";   // authStatus ???
        }
        String ordering = "createdTime desc";
        try {
            beans = getExternalUserAuthService().findExternalUserAuths(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve userAuthState list.", e);
        }

        return beans;
    }

    
    // TBD:
    public List<String> getExternalUserAuthKeysByEmail(String email, String providerId)
    {
        return getExternalUserAuthKeysByEmail(email, providerId, null);
    }
    public List<String> getExternalUserAuthKeysByEmail(String email, String providerId, Integer maxCount)
    {
        List<String> keys = null;

        String filter = "externalUserId.email=='" + email + "'";
        if(providerId != null) {
            filter += " && providerId=='" + providerId + "'";   // authStatus ???
        }
        String ordering = "createdTime desc";
        try {
            keys = getExternalUserAuthService().findExternalUserAuthKeys(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve userAuthState key list.", e);
        }

        return keys;
    }

    public List<ExternalUserAuth> getExternalUserAuthByEmail(String email, String providerId)
    {
        return getExternalUserAuthByEmail(email, providerId, null);
    }
    public List<ExternalUserAuth> getExternalUserAuthByEmail(String email, String providerId, Integer maxCount)
    {
        List<ExternalUserAuth> beans = null;
        
        String filter = "externalUserId.email=='" + email + "'";
        if(providerId != null) {
            filter += " && providerId=='" + providerId + "'";   // authStatus ???
        }
        String ordering = "createdTime desc";
        try {
            beans = getExternalUserAuthService().findExternalUserAuths(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve userAuthState list.", e);
        }

        return beans;
    }


    // TBD:
    public List<String> getExternalUserAuthKeysByOpenId(String openId, String providerId)
    {
        return getExternalUserAuthKeysByOpenId(openId, providerId, null);
    }
    public List<String> getExternalUserAuthKeysByOpenId(String openId, String providerId, Integer maxCount)
    {
        List<String> keys = null;

        String filter = "externalUserId.openId=='" + openId + "'";
        if(providerId != null) {
            filter += " && providerId=='" + providerId + "'";   // authStatus ???
        }
        String ordering = "createdTime desc";
        try {
            keys = getExternalUserAuthService().findExternalUserAuthKeys(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve userAuthState key list.", e);
        }

        return keys;
    }

    public List<ExternalUserAuth> getExternalUserAuthByOpenId(String openId, String providerId)
    {
        return getExternalUserAuthByOpenId(openId, providerId, null);
    }
    public List<ExternalUserAuth> getExternalUserAuthByOpenId(String openId, String providerId, Integer maxCount)
    {
        List<ExternalUserAuth> beans = null;
        
        String filter = "externalUserId.openId=='" + openId + "'";
        if(providerId != null) {
            filter += " && providerId=='" + providerId + "'";   // authStatus ???
        }
        String ordering = "createdTime desc";
        try {
            beans = getExternalUserAuthService().findExternalUserAuths(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve userAuthState list.", e);
        }

        return beans;
    }


    
    
}
