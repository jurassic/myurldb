package com.cannyurl.app.auth;

import java.util.logging.Logger;


// 3/03/13: 


// temporary
public class GoogleAuthHelper
{
    private static final Logger log = Logger.getLogger(GoogleAuthHelper.class.getName());

    // ...
    public static final String SESSION_ATTR_GOOGLEAUTH_LOGOUTREQUETED = "com.cannyurl.app.auth.googleauth.logoutrequested";
    // public static final String SESSION_ATTR_GOOGLEAUTH_REQUESTTOKEN = "com.cannyurl.app.auth.googleauth.requesttoken";
    // public static final String REQUEST_ATTR_GOOGLEAUTH_ACCESSTOKEN = "com.cannyurl.app.auth.googleauth.accesstoken";
    // ...
    
    private GoogleAuthHelper()
    {
    }

    // Initialization-on-demand holder.
    private static class GoogleAuthHelperHolder
    {
        private static final GoogleAuthHelper INSTANCE = new GoogleAuthHelper();
    }

    // Singleton method
    public static GoogleAuthHelper getInstance()
    {
        return GoogleAuthHelperHolder.INSTANCE;
    }

    
    

}
