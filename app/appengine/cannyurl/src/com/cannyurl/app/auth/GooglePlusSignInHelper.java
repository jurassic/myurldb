package com.cannyurl.app.auth;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpSession;

import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeTokenRequest;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson.JacksonFactory;
import com.google.gson.Gson;
import com.cannyurl.af.auth.common.CommonAuthUtil;
import com.cannyurl.af.auth.googleplus.GooglePlusAccessToken;
import com.cannyurl.af.auth.googleplus.GooglePlusAuthHelper;
import com.cannyurl.af.auth.googleplus.GooglePlusAuthUtil;
import com.cannyurl.af.auth.googleplus.GooglePlusSignInStruct;
import com.cannyurl.af.bean.ExternalUserAuthBean;
import com.cannyurl.af.bean.ExternalUserIdStructBean;
import com.myurldb.ws.core.GUID;


// 3/03/13: Google+ SignIn (Work in progress)


// temporary
public class GooglePlusSignInHelper
{
    private static final Logger log = Logger.getLogger(GooglePlusSignInHelper.class.getName());

    // ...
    public static final String SESSION_ATTR_GOOGLEPLUS_LOGOUTREQUETED = "com.cannyurl.app.auth.googleplus.logoutrequested";
    // ...

    private GooglePlusSignInHelper()
    {
    }

    // Initialization-on-demand holder.
    private static class GooglePlusSignInHelperHolder
    {
        private static final GooglePlusSignInHelper INSTANCE = new GooglePlusSignInHelper();
    }

    // Singleton method
    public static GooglePlusSignInHelper getInstance()
    {
        return GooglePlusSignInHelperHolder.INSTANCE;
    }

    
    // temporary
    public GooglePlusSignInStruct buildGooglePlusSignInStruct()
    {
        return buildGooglePlusSignInStruct(null, null); 
    }
    public GooglePlusSignInStruct buildGooglePlusSignInStruct(String dataCallback, String dataApprovalPrompt)
    {
        GooglePlusSignInStruct struct = new GooglePlusSignInStruct();
        
        String dataScope = GooglePlusAuthUtil.DEFAULT_DATA_SCOPE;
        struct.setDataScope(dataScope);
        String dataClientId = GooglePlusAuthHelper.getInstance().getClientId();
        if(dataClientId != null && !dataClientId.isEmpty()) {
            struct.setDataClientId(dataClientId);
        } else {
            // Error...
            // What to do ???
        }
        String dataRedirectUri = GooglePlusAuthUtil.DEFAULT_DATA_REDIRECT_URL;           // ????
        struct.setDataRedirectUri(dataRedirectUri);
        String dataAccessType = GooglePlusAuthUtil.DEFAULT_DATA_ACCESS_TYPE;             // ????
        struct.setDataAccessType(dataAccessType);
        String dataCookiePolicy = GooglePlusAuthUtil.DEFAULT_DATA_COOKIE_POLICY;         // ????
        struct.setDataCookiePolicy(dataCookiePolicy);
        if(dataCallback == null || dataCallback.isEmpty()) {
            dataCallback = GooglePlusAuthHelper.getInstance().getJavascriptSignInCallback();
        }
        if(dataCallback != null && !dataCallback.isEmpty()) {
            struct.setDataCallback(dataCallback);
        } else {
            // Error...
            // What to do ???
        }
        if(dataApprovalPrompt != null && !dataApprovalPrompt.isEmpty()) {
            struct.setDataApprovalPrompt(dataApprovalPrompt);
        } else {
            // Ignore...
        }
        
        return struct;
    }
    
    

    // temporary
    private static HttpTransport TRANSPORT = new NetHttpTransport();
    private static JacksonFactory JSON_FACTORY = new JacksonFactory();
    private static Gson GSON = new Gson();

    
    // temporary
    public GoogleTokenResponse convertToGooglePlusTokenResponse(String code)
    {
        String clientId = GooglePlusAuthHelper.getInstance().getClientId();
        String clientSecret = GooglePlusAuthHelper.getInstance().getClientSecret();
        String redirectUrl = GooglePlusAuthUtil.DEFAULT_DATA_REDIRECT_URL;
        
        GoogleTokenResponse tokenResponse = null;
        try {
            tokenResponse =
                    new GoogleAuthorizationCodeTokenRequest(TRANSPORT, JSON_FACTORY,
                            clientId, clientSecret, code, redirectUrl).execute();
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to convert code to GoogleTokenResponse", e);
        }
        
        return tokenResponse;
    }
    // temporary
    public GooglePlusAccessToken convertToGooglePlusAccessToken(String code)
    {
        String clientId = GooglePlusAuthHelper.getInstance().getClientId();
        String clientSecret = GooglePlusAuthHelper.getInstance().getClientSecret();
        String redirectUrl = GooglePlusAuthUtil.DEFAULT_DATA_REDIRECT_URL;
        
        GooglePlusAccessToken token = null;
        try {
            GoogleTokenResponse tokenResponse =
                    new GoogleAuthorizationCodeTokenRequest(TRANSPORT, JSON_FACTORY,
                            clientId, clientSecret, code, redirectUrl).execute();

            String accessToken = tokenResponse.getAccessToken();
            String refreshToken = tokenResponse.getRefreshToken();
            long expiresInSeconds = tokenResponse.getExpiresInSeconds();
            long expirationTime = System.currentTimeMillis() + expiresInSeconds * 1000L;  // ???
            token = new GooglePlusAccessToken(accessToken, refreshToken, expirationTime);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to convert code to GooglePlusAccessToken", e);
        }
        
        return token;
    }
    // temporary
    public GoogleCredential getGooglePlusCredential(GoogleTokenResponse tokenResponse)
    {
        String clientId = GooglePlusAuthHelper.getInstance().getClientId();
        String clientSecret = GooglePlusAuthHelper.getInstance().getClientSecret();
        String redirectUrl = GooglePlusAuthUtil.DEFAULT_DATA_REDIRECT_URL;
        
        GoogleCredential credential = null;
        try {
            credential = new GoogleCredential.Builder()
            .setJsonFactory(JSON_FACTORY)
            .setTransport(TRANSPORT)
            .setClientSecrets(clientId, clientSecret).build()
            .setFromTokenResponse(tokenResponse);
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to find GoogleCredential", e);
        }
        
        return credential;
    }

    
    
    
    
    // TBD:::
    public void connectGooglePlus(HttpSession session, GoogleTokenResponse tokenResponse)
    {
        GoogleCredential credential = getGooglePlusCredential(tokenResponse);
        // tbd...
                
        
        // TBD...
        
        session.setAttribute(GooglePlusAuthUtil.SESSION_ATTR_TOKENRESPONSE, tokenResponse);
        // ...
        
    }

    // TBD:::
    public void disconnectGooglePlus(HttpSession session, GoogleTokenResponse tokenResponse)
    {
        GoogleCredential credential = getGooglePlusCredential(tokenResponse);
        // tbd...
                
        try {
            HttpResponse revokeResponse = TRANSPORT.createRequestFactory()
                    .buildGetRequest(new GenericUrl(
                        String.format(
                            "https://accounts.google.com/o/oauth2/revoke?token=%s",
                            credential.getAccessToken()))).execute();
        } catch (IOException e) {

        }

        // Reset the user's session.
        session.removeAttribute(GooglePlusAuthUtil.SESSION_ATTR_TOKENRESPONSE);

        // etc...
        
    }
    
    
    
    
    
    
    // temporary
    private boolean validateAccessCode(HttpSession session, String user, String code)
    {
        String clientId = GooglePlusAuthHelper.getInstance().getClientId();
        String clientSecret = GooglePlusAuthHelper.getInstance().getClientSecret();
        String redirectUrl = GooglePlusAuthUtil.DEFAULT_DATA_REDIRECT_URL;
    
        
        
        try {
            GoogleTokenResponse tokenResponse =
                    new GoogleAuthorizationCodeTokenRequest(TRANSPORT, JSON_FACTORY,
                            clientId, clientSecret, code, redirectUrl).execute();

            String accessToken = tokenResponse.getAccessToken();
            String refreshToken = tokenResponse.getRefreshToken();
            long expiresInSeconds = tokenResponse.getExpiresInSeconds();
            long expirationTime = System.currentTimeMillis() + expiresInSeconds * 1000L;  // ???
            GooglePlusAccessToken token = new GooglePlusAccessToken(accessToken, refreshToken, expirationTime);
            

            GoogleCredential credential = new GoogleCredential.Builder()
            .setJsonFactory(JSON_FACTORY)
            .setTransport(TRANSPORT)
            .setClientSecrets(clientId, clientSecret).build()
            .setFromTokenResponse(tokenResponse);

            // String accessToken = credential.getAccessToken();
            // String refreshToken = credential.getRefreshToken();
            
            // credential.getExpirationTimeMilliseconds();
            // ...
            
            
            
            // TBD:
            // Verify that auth succeeded...
            // ....

            

        } catch (IOException e) {
            log.log(Level.WARNING, "", e);
        }
        
        return false;
    }
    

    
    

    // temporary
    public ExternalUserAuthBean saveGooglePlusTokenResponse(HttpSession session, String user, GoogleTokenResponse tokenResponse, ExternalUserAuthBean externalUserAuth)
    {

        if(tokenResponse != null) {
       
            GoogleCredential credential = getGooglePlusCredential(tokenResponse);
            if(credential != null) {
                
                boolean createNew = false;
                if(externalUserAuth == null) {
                    createNew = true;
                    externalUserAuth = new ExternalUserAuthBean();
                }

                String guid = externalUserAuth.getGuid();
                if(guid == null || guid.isEmpty()) {   // Validate?
                    guid = GUID.generate();
                    externalUserAuth.setGuid(guid);
                }
                // TBD: Reuse any of the fields if externalUserAuth != null ???
                // Or, just overwrite everything fresh ???
                externalUserAuth.setProviderId(CommonAuthUtil.AUTH_SERVICE_PROVIDERID_GOOGLEPLUS);
                externalUserAuth.setUser(user);

                // TBD:
                // How to get user's google+ name, etc. ?????
                // ....
                
                
                
                ExternalUserIdStructBean externalUserId = new ExternalUserIdStructBean();
                externalUserId.setUuid(GUID.generate());
                // .....
                
                externalUserAuth.setExternalUserId(externalUserId);


                
                String accessToken = tokenResponse.getAccessToken();
                String refreshToken = tokenResponse.getRefreshToken();
                externalUserAuth.setAccessToken(accessToken);
                externalUserAuth.setAccessTokenSecret(refreshToken);       // ???????????
                // ...


                
                
                
                long now = System.currentTimeMillis();
                Long createdTime = externalUserAuth.getCreatedTime();
                if(createdTime == null || createdTime == 0L) {
                    externalUserAuth.setCreatedTime(now);
                }
                externalUserAuth.setModifiedTime(now);
                externalUserAuth.setAuthTime(now);
                // ...
                
                // TBD: save or update.... ???
                externalUserAuth = (ExternalUserAuthBean) ExternalUserAuthHelper.getInstance().saveExternalUserAuth(externalUserAuth, createNew);
//                if(externalUserAuth != null) {
//                    guid = externalUserAuth.getGuid();
//                } else {
//                    guid = null;   // ???
//                }
            } else {
                // ???
                log.log(Level.WARNING, "Could not save the tokenResponse because failed to find credential.");
            }
        } else {
            // ???
            log.log(Level.WARNING, "Could not save the tokenResponse because it is null.");
        }
                
        return externalUserAuth;
    }
    

    // temporary
    private ExternalUserAuthBean saveGooglePlusAccessToken(HttpSession session, String user, GooglePlusAccessToken token, ExternalUserAuthBean externalUserAuth)
    {

        // .....
        
        boolean createNew = false;
        if(externalUserAuth == null) {
            createNew = true;
            externalUserAuth = new ExternalUserAuthBean();
        }

        // .....
                
        return externalUserAuth;
    }
    
    
}
