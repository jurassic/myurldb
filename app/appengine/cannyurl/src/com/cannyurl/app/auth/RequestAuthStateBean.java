package com.cannyurl.app.auth;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.cannyurl.af.util.SimpleEncryptUtil;



// 3/03/13:
// 2/08/13: Added toJsonString()
// 2/07/13: Google OpenID support....
// 1/16/13: Created.
// ...


// Primarily to be used as a request attr.
public class RequestAuthStateBean implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(RequestAuthStateBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String user;
    private String providerId;
    private boolean authenticated;
    private String loginUrl;
    private String logoutUrl;
    // private ExternalUserIdStruct externalUserId;
    private String name;     // Nickname, display name.
    private String email;
    private String username;
    // TBD:
//    private Long   userId;
    private String   userId;
    // ...
    private String openId;
    private String note;    // ??? comment, etc. ???

    
    public RequestAuthStateBean()
    {
        this(null);
    }
    public RequestAuthStateBean(String user)
    {
        this(user, null);
    }
    public RequestAuthStateBean(String user, String providerId)
    {
        this(user, providerId, false);
    }
    public RequestAuthStateBean(String user, String providerId, boolean authenticated)
    {
        this(user, providerId, authenticated, null, null);
    }
    public RequestAuthStateBean(String user, String providerId, boolean authenticated,
            String loginUrl, String logoutUrl)
    {
        this(user, providerId, authenticated, loginUrl, logoutUrl, null, null, null, null, null);
    }
    public RequestAuthStateBean(String user, String providerId, boolean authenticated,
            String loginUrl, String logoutUrl, String name, String email,
            String username, String userId, String openId)
    {
        this(user, providerId, authenticated, loginUrl, logoutUrl, name, email, username, userId, openId, null);
    }
    public RequestAuthStateBean(String user, String providerId, boolean authenticated,
            String loginUrl, String logoutUrl, String name, String email,
            String username, String userId, String openId, String note)
    {
        super();
        this.user = user;
        this.providerId = providerId;
        this.authenticated = authenticated;
        this.loginUrl = loginUrl;
        this.logoutUrl = logoutUrl;
        this.name = name;
        this.email = email;
        this.username = username;
        this.userId = userId;
        this.openId = openId;
        this.note = note;
    }

    public String getUser()
    {
        return user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }
    public String getProviderId()
    {
        return providerId;
    }
    public void setProviderId(String providerId)
    {
        this.providerId = providerId;
    }
    public boolean isAuthenticated()
    {
        return authenticated;
    }
    public void setAuthenticated(boolean authenticated)
    {
        this.authenticated = authenticated;
    }
    public String getLoginUrl()
    {
        return loginUrl;
    }
    public void setLoginUrl(String loginUrl)
    {
        this.loginUrl = loginUrl;
    }
    public String getLogoutUrl()
    {
        return logoutUrl;
    }
    public void setLogoutUrl(String logoutUrl)
    {
        this.logoutUrl = logoutUrl;
    }
    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = name;
    }
    public String getEmail()
    {
        return email;
    }
    public void setEmail(String email)
    {
        this.email = email;
    }
    public String getUsername()
    {
        return username;
    }
    public void setUsername(String username)
    {
        this.username = username;
    }

    // TBD:
//    public Long getUserId()
//    {
//        return userId;
//    }
//    public void setUserId(Long userId)
//    {
//        this.userId = userId;
//    }
    public String getUserId()
    {
        return userId;
    }
    public void setUserId(String userId)
    {
        this.userId = userId;
    }
    // ....

    public String getOpenId()
    {
        return openId;
    }
    public void setOpenId(String openId)
    {
        this.openId = openId;
    }
    public String getNote()
    {
        return note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }    
    
    
    public String toJsonString()
    {
        String jsonStr = null;
        try {
            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;
    }

    public String toEncryptedJsonString()
    {
        String jsonStr = toJsonString();
        return SimpleEncryptUtil.encrypt(jsonStr);
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("user = " + this.user).append(";");
        sb.append("providerId = " + this.providerId).append(";");
        sb.append("authenticated = " + this.authenticated).append(";");
        sb.append("loginUrl = " + this.loginUrl).append(";");
        sb.append("logoutUrl = " + this.logoutUrl).append(";");
        sb.append("name = " + this.name).append(";");
        sb.append("email = " + this.email).append(";");
        sb.append("username = " + this.username).append(";");
        sb.append("userId = " + this.userId).append(";");
        sb.append("openId = " + this.openId).append(";");
        sb.append("note = " + this.note).append(";");
        return sb.toString();
    }
    
}
