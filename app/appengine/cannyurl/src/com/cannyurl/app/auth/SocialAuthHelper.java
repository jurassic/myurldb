package com.cannyurl.app.auth;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.brickred.socialauth.AuthProvider;
import org.brickred.socialauth.Permission;
import org.brickred.socialauth.Profile;
import org.brickred.socialauth.SocialAuthConfig;
import org.brickred.socialauth.SocialAuthManager;
import org.brickred.socialauth.util.AccessGrant;
import org.brickred.socialauth.util.SocialAuthUtil;

import com.cannyurl.af.auth.common.CommonAuthUtil;
import com.cannyurl.af.bean.ExternalUserAuthBean;
import com.cannyurl.af.bean.ExternalUserIdStructBean;
import com.cannyurl.af.config.Config;
import com.cannyurl.af.util.URLUtil;
import com.myurldb.ws.core.GUID;


// 3/03/13:
// 2/13/13: Facebook login support...
//...


public class SocialAuthHelper
{
    private static final Logger log = Logger.getLogger(SocialAuthHelper.class.getName());

    public static final String CONFIG_KEY_PROPERTIES_FILE = "cannyurlapp.socialauth.oauthconsumer.properties";
    public static final String CONFIG_KEY_SUCCESS_REDIRECTURLPATH = "cannyurlapp.socialauth.success.redirecturlpath";
    // ...
    //public static final String PARAM_CODE = "code";
    // ...
    public static final String SESSION_ATTR_SOCIALAUTH_AUTHMANAGER = "com.cannyurl.app.auth.socialauth.authmanager";
    public static final String REQUEST_ATTR_SOCIALAUTH_ACCESSGRANT = "com.cannyurl.app.auth.socialauth.accessgrant";
    public static final String REQUEST_ATTR_SOCIALAUTH_USERPROFILE = "com.cannyurl.app.auth.socialauth.userprofile";
    // ...

    // "Lazy initialization"
    private String mPropertiesFile = null;
    private String mSuccessRedirectUrlPath = null;
    // ...
    
    private SocialAuthConfig mSocialAuthConfig = null;
    private boolean configLoaded = false;

    
    private SocialAuthHelper() 
    {
        if(mSocialAuthConfig == null) {
            mSocialAuthConfig = SocialAuthConfig.getDefault();
            try {
                mSocialAuthConfig.load(getPropertiesFile());
                configLoaded = true;
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to load SocialAuth config file.", e);
            }
        }
    }

    // Initialization-on-demand holder.
    private static final class SocialAuthHelperHolder
    {
        private static final SocialAuthHelper INSTANCE = new SocialAuthHelper();
    }

    // Singleton method
    public static SocialAuthHelper getInstance()
    {
        return SocialAuthHelperHolder.INSTANCE;
    }
    
    
    public String getPropertiesFile()
    {
        if(mPropertiesFile == null) {
            mPropertiesFile = Config.getInstance().getString(CONFIG_KEY_PROPERTIES_FILE);
            if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "SocialAuthHelper: mPropertiesFile = " + mPropertiesFile);
        }
        return mPropertiesFile;
    }
    private String getSuccessRedirectUrlPath()
    {
        if(mSuccessRedirectUrlPath == null) {
            mSuccessRedirectUrlPath = Config.getInstance().getString(CONFIG_KEY_SUCCESS_REDIRECTURLPATH);
            if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "SocialAuthHelper: mSuccessRedirectUrlPath = " + mSuccessRedirectUrlPath);
        }
        return mSuccessRedirectUrlPath;
    }
    public String getDefaultSuccessRedirectUri(String topLevelUrl)
    {
        return CommonAuthUtil.constructUrl(topLevelUrl, getSuccessRedirectUrlPath());
    }
    public String getSuccessRedirectUri(String topLevelUrl, String comebackUrl)
    {
        return getSuccessRedirectUri(topLevelUrl, comebackUrl, null);
    }
    public String getSuccessRedirectUri(String topLevelUrl, String comebackUrl, String fallbackUrl)
    {
        String baseUrl = getDefaultSuccessRedirectUri(topLevelUrl);
        Map<String, Object> params = new HashMap<String, Object>();
        if(comebackUrl != null) {
            params.put(CommonAuthUtil.PARAM_COMEBACKURL, comebackUrl);
        }
        if(fallbackUrl != null) {
            params.put(CommonAuthUtil.PARAM_FALLBACKURL, fallbackUrl);
        }
        String authUrl = URLUtil.buildUrl(baseUrl, params);
        return authUrl;
    }


    public SocialAuthManager createSocialAuthManager()
    {
        SocialAuthManager authManager = null;
        if(configLoaded == true) {  // ???
            try {
                authManager =  new SocialAuthManager();
                authManager.setSocialAuthConfig(mSocialAuthConfig);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create a SocialAuthManager.", e);
                authManager = null;   // ???
            }
        }
        return authManager;
    }

    public SocialAuthManager getSocialAuthManager(HttpSession session)
    {
        return getSocialAuthManager(session, true);
    }

    public SocialAuthManager getSocialAuthManager(HttpSession session, boolean createIfNotInSession)
    {
        SocialAuthManager authManager = (SocialAuthManager) session.getAttribute(SESSION_ATTR_SOCIALAUTH_AUTHMANAGER);
        if(authManager == null && createIfNotInSession==true) {
            authManager = createSocialAuthManager();
            if(authManager != null) {
                session.setAttribute(SESSION_ATTR_SOCIALAUTH_AUTHMANAGER, authManager);
            }
        }
        return authManager;
    }

    public String getAuthenticationUrl(HttpSession session, String providerId, String successUrl)
    {
        String authenticationUrl = null;
        SocialAuthManager authManager = getSocialAuthManager(session);
        if(authManager != null) {
            try {
                // TBD: Permission ????
                authenticationUrl = authManager.getAuthenticationUrl(providerId, successUrl, Permission.AUTHENTICATE_ONLY);  // TBD:::
                //authenticationUrl = authManager.getAuthenticationUrl(providerId, successUrl, Permission.DEFAULT);               // TBD:::
                // ...
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "SocialAuthHelper: authenticationUrl = " + authenticationUrl);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to get the authenticationUrl for providerId = " + providerId, e);
            }
        }        
        return authenticationUrl;
    }
    public String getAuthenticationUrl(HttpSession session, String providerId, String topLevelUrl, String comebackUrl)
    {
        String successUrl = getSuccessRedirectUri(topLevelUrl, comebackUrl);
        return getAuthenticationUrl(session, providerId, successUrl);
    }
    public String getAuthenticationUrl(HttpSession session, String providerId, String topLevelUrl, String comebackUrl, String fallbackUrl)
    {
        String successUrl = getSuccessRedirectUri(topLevelUrl, comebackUrl, fallbackUrl);
        return getAuthenticationUrl(session, providerId, successUrl);
    }

    
    // ??????
    public AuthProvider getAuthProvider(HttpServletRequest request)
    {
        AuthProvider authProvider = null;
        HttpSession session = request.getSession();
        if(session != null) {
            SocialAuthManager authManager = getSocialAuthManager(session);
            if(authManager != null) {
                try {
                    java.util.Map<String, String> paramsMap = org.brickred.socialauth.util.SocialAuthUtil.getRequestParametersMap(request);
                    if(paramsMap != null) {
                        authProvider = authManager.connect(paramsMap);
                    } else {
                        // ????
                        // authProvider = authManager.connect(accessGrant);
                    }
                } catch (Exception e) {
                    log.log(Level.WARNING, "Failed to get the auth provider.", e);
                }
            }
        }
        return authProvider;
    }

    
    
    
    // ????
    // Returns the updated ExternalUserAuthBean (after create/update)
    public ExternalUserAuthBean saveSocialAuthAccessGrant(HttpSession session, String user, String providerId, org.brickred.socialauth.util.AccessGrant accessGrant, org.brickred.socialauth.Profile userProfile, ExternalUserAuthBean externalUserAuth)
    {
        String guid = null;
        if(accessGrant != null) {

            // TBD:
            // Check if the account already exists in the table....
            // If so, update it rather than create a new one.
            // ...
                
            boolean createNew = false;
            if(externalUserAuth == null) {
                createNew = true;
                externalUserAuth = new ExternalUserAuthBean();
            }
            guid = externalUserAuth.getGuid();
            if(guid == null || guid.isEmpty()) {   // Validate?
                guid = GUID.generate();
                externalUserAuth.setGuid(guid);
            }

            // TBD: Reuse any of the fields if externalUserAuth != null ???
            // Or, just overwrite everything fresh ???
            
            // TBD:
            // Note that the providerId used in SocialAuth library may not coincide the providerId string we use...
            // We need a conversion table.....
            if(providerId == null || providerId.isEmpty()) {
                providerId = accessGrant.getProviderId();     // ???
            }
            externalUserAuth.setProviderId(providerId);
            externalUserAuth.setUser(user);

            String accessGrantKey = accessGrant.getKey();
            String accessGrantSecret = accessGrant.getSecret();
            externalUserAuth.setAccessToken(accessGrantKey);
            externalUserAuth.setAccessTokenSecret(accessGrantSecret);

            String fullName = null;      // ????
            String displayName = null;   // ????
            String email = null;         // ????
            String username = null;      // ????
            Long userId = null;          // ????
            String profileImageUrl = null;
            String language = null;
            String country = null;
            String location = null;
            if(userProfile != null) {                    
                fullName = userProfile.getFullName();
                displayName = userProfile.getDisplayName();
                email = userProfile.getEmail();
                
                // TBD:
                // Does this make sense???
                // (this works for Twitter and Faceboo....)
                // ...
                String uid = userProfile.getValidatedId();   // ????
                if(uid != null && !uid.isEmpty()) {
                    // ????
                    Long longId = null;
                    try {
                        longId = Long.valueOf(uid);
                    } catch(Exception e) {
                        // ignore...
                    }
                    if(longId != null) {
                        userId = longId; 
                    } else {
                        username = uid;
                    }
                }
                // ....

                profileImageUrl = userProfile.getProfileImageURL();
                language = userProfile.getLanguage();
                country = userProfile.getCountry();
                location = userProfile.getLocation();
                // ...
            } else {
                // ???
            }
            
            if(displayName != null && !displayName.isEmpty()) {
                externalUserAuth.setDisplayName(displayName);
            }

            ExternalUserIdStructBean externalUserId = new ExternalUserIdStructBean();
            externalUserId.setUuid(GUID.generate());
            externalUserId.setName(fullName);
            // TBD:
//            if(userId != null && userId > 0L) {
//                externalUserId.setUserId(userId);
//            }
            if(userId != null && userId > 0L) {
                externalUserId.setId(String.valueOf(userId));
            }
            // ....
            if(username != null) {
                externalUserId.setUsername(username);
            }
            if(fullName != null) {
                externalUserAuth.setFullName(fullName);
                externalUserId.setName(fullName);
            }
            if(email != null) {
                externalUserAuth.setEmail(email);
                externalUserId.setEmail(email);
            }
            externalUserAuth.setExternalUserId(externalUserId);

            if(profileImageUrl != null) {
                externalUserAuth.setProfileImageUrl(profileImageUrl);
            }
            if(language != null) {
                externalUserAuth.setLanguage(language);
            }
            if(country != null) {
                externalUserAuth.setCountry(country);
            }
            if(location != null) {
                externalUserAuth.setLocation(location);
            }
            // timezone, etc.. ???
            // ...
            
            long now = System.currentTimeMillis();
            Long createdTime = externalUserAuth.getCreatedTime();
            if(createdTime == null || createdTime == 0L) {
                externalUserAuth.setCreatedTime(now);
            }
            externalUserAuth.setModifiedTime(now);
            externalUserAuth.setAuthTime(now);
            // ...
            
            // TBD: save or update.... ???
            externalUserAuth = (ExternalUserAuthBean) ExternalUserAuthHelper.getInstance().saveExternalUserAuth(externalUserAuth, createNew);
//                if(externalUserAuth != null) {
//                    guid = externalUserAuth.getGuid();
//                } else {
//                    guid = null;   // ???
//                }
        } else {
            // ???
            log.log(Level.WARNING, "Could not save the accessGrant because it is null.");
        }
        return externalUserAuth;
    }


    
}
