package com.cannyurl.app.auth;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpSession;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.ConfigurationBuilder;

import com.cannyurl.af.auth.common.CommonAuthUtil;
import com.cannyurl.af.auth.common.OAuthRequestToken;
import com.cannyurl.af.auth.twitter.TwitterAuthHelper;
import com.cannyurl.af.bean.ExternalUserAuthBean;
import com.cannyurl.af.bean.ExternalUserIdStructBean;
import com.cannyurl.af.config.Config;
import com.cannyurl.af.util.URLUtil;
import com.myurldb.ws.ExternalUserAuth;
import com.myurldb.ws.core.GUID;


// 3/03/13:
// 2/15/13: Some minor changes to support a workaround implementation (for ExternalUserId.userId -> ExternalUserId.id change). 
// 2/07/13: Google OpenID support....
// 1/13/13: Support for "Logout"
// 1/02/13: A bug fixed ???
// 1/06/13: Updated for Twitter4j 3.0.3 ?????
// 1/09/13: Added some comments (and place-holder for storing request token in a session). 
// ....

// Twitter OAuth Manager
// https://dev.twitter.com/docs/auth
// http://twitter4j.org/en/index.html
public class Twitter4jHelper
{
    private static final Logger log = Logger.getLogger(Twitter4jHelper.class.getName());

    public static final String CONFIG_KEY_CALLBACKURLPATH = "cannyurlapp.twitter4j.callbackurlpath";
    // ...
    public static final String SESSION_ATTR_TWITTER4J_TWITTER = "com.cannyurl.app.auth.twitter4j.twitter";
    public static final String SESSION_ATTR_TWITTER4J_REQUESTTOKEN = "com.cannyurl.app.auth.twitter4j.requesttoken";
    public static final String REQUEST_ATTR_TWITTER4J_ACCESSTOKEN = "com.cannyurl.app.auth.twitter4j.accesstoken";
    // ...
    
    // Temporary
    private static final long REQUESTTOKEN_LIFETIME = 2 * 3600 * 1000L;   // 2 hours, for now...
    // ...
    

    
    private TwitterFactory mTwitterFactory = null;
    private String mCallbackUrlPath = null;
    // ...

    private Twitter4jHelper()
    {
        // TBD:
        //initTwitterFactory();
    }

    // Initialization-on-demand holder.
    private static class Twitter4jHelperHolder
    {
        private static final Twitter4jHelper INSTANCE = new Twitter4jHelper();
    }

    // Singleton method
    public static Twitter4jHelper getInstance()
    {
        return Twitter4jHelperHolder.INSTANCE;
    }
    
    private void initTwitterFactory()
    {
        // TBD:
        String consumerKey = TwitterAuthHelper.getInstance().getConsumerKey();
        String consumerSecret = TwitterAuthHelper.getInstance().getConsumerSecret();
    
        // temporary
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true)
          .setOAuthConsumerKey(consumerKey)
          .setOAuthConsumerSecret(consumerSecret);
        mTwitterFactory = new TwitterFactory(cb.build());      
    }


    // ????
    public Twitter getSessionTwitterInstance(HttpSession session)
    {
        Twitter twitter = null;
        if(session != null) {
            twitter = (Twitter) session.getAttribute(SESSION_ATTR_TWITTER4J_TWITTER);
        }
        return twitter;
    }
    public void saveSessionTwitterInstance(HttpSession session, Twitter twitter)
    {
        if(session != null) {
            session.setAttribute(SESSION_ATTR_TWITTER4J_TWITTER, twitter);
        }
    }
    public void clearSessionTwitterInstance(HttpSession session)
    {
        if(session != null) {
            session.removeAttribute(SESSION_ATTR_TWITTER4J_TWITTER);
        }
    }
    
    
    
    // TBD:
    // Does this scheme of using multiple versions of Twitter instance make sense???
    // Regardless of which method was called before, we use the last instance stored in the session, if availabble...
    // This can be a problem...
    // Need to modify this whole messy logic here...
    // ....
    
    
    
    // Returns the unauthenticated twitter instance.
    public Twitter getBaseTwitterInstance()
    {
        if(mTwitterFactory == null) {
            initTwitterFactory();
        }
        Twitter twitter = mTwitterFactory.getInstance();
        return twitter;
    }
    public Twitter getBaseTwitterInstance(HttpSession session)
    {
        Twitter twitter = null;
        if(session != null) {
            twitter = getSessionTwitterInstance(session);
        }
        if(twitter == null) {
            twitter = getBaseTwitterInstance();
            if(twitter != null) {
                saveSessionTwitterInstance(session, twitter);
            }
        }
        return twitter;
    }


    // Returns the twitter instance with the system default AccessToken.
    public Twitter getDefaultTwitterInstance()
    {
        AccessToken accessToken = getDefaultOAuthAccessToken();
        return getTwitterInstance(accessToken);
    }
    public Twitter getDefaultTwitterInstance(HttpSession session)
    {
        Twitter twitter = null;
        if(session != null) {
            twitter = getSessionTwitterInstance(session);
        }
        if(twitter == null) {
            twitter = getDefaultTwitterInstance();
            if(twitter != null) {
                saveSessionTwitterInstance(session, twitter);
            }
        }
        return twitter;
    }

    public Twitter getTwitterInstance()
    {
        return getDefaultTwitterInstance();
    }
    public Twitter getTwitterInstance(HttpSession session)
    {
        Twitter twitter = null;
        if(session != null) {
            twitter = getSessionTwitterInstance(session);
        }
        if(twitter == null) {
            twitter = getTwitterInstance();
            if(twitter != null) {
                saveSessionTwitterInstance(session, twitter);
            }
        }
        return twitter;
    }

    public Twitter getTwitterInstance(long userId)
    {
        AccessToken accessToken = getOAuthAccessTokenByUserId(userId);
        if(accessToken != null) {
            return getTwitterInstance(accessToken);
        } else {
            // ???
            return null;
        }
    }
    public Twitter getTwitterInstance(HttpSession session, long userId)
    {
        Twitter twitter = null;
        if(session != null) {
            twitter = getSessionTwitterInstance(session);
        }
        if(twitter == null) {
            twitter = getTwitterInstance(userId);
            if(twitter != null) {
                saveSessionTwitterInstance(session, twitter);
            }
        }
        return twitter;
    }

    public Twitter getTwitterInstanceByUsername(String username)
    {
        AccessToken accessToken = getOAuthAccessTokenByUsername(username);
        return getTwitterInstance(accessToken);
    }
    public Twitter getTwitterInstanceByUsername(HttpSession session, String username)
    {
        Twitter twitter = null;
        if(session != null) {
            twitter = getSessionTwitterInstance(session);
        }
        if(twitter == null) {
            twitter = getTwitterInstanceByUsername(username);
            if(twitter != null) {
                saveSessionTwitterInstance(session, twitter);
            }
        }
        return twitter;
    }

    public Twitter getTwitterInstance(String user)
    {
        AccessToken accessToken = getOAuthAccessToken(user);
        return getTwitterInstance(accessToken);
    }
    public Twitter getTwitterInstance(HttpSession session, String user)
    {
        Twitter twitter = null;
        if(session != null) {
            twitter = getSessionTwitterInstance(session);
        }
        if(twitter == null) {
            twitter = getTwitterInstance(user);
            if(twitter != null) {
                saveSessionTwitterInstance(session, twitter);
            }
        }
        return twitter;
    }

    public Twitter getTwitterInstance(AccessToken accessToken)
    {
        if(accessToken == null) {
            return null;   // ????
        }
        if(mTwitterFactory == null) {
            initTwitterFactory();
        }        
        Twitter twitter = mTwitterFactory.getInstance(accessToken);
        return twitter;
    }
    public Twitter getTwitterInstance(HttpSession session, AccessToken accessToken)
    {
        Twitter twitter = null;
        if(session != null) {
            twitter = getSessionTwitterInstance(session);
        }
        if(twitter == null) {
            twitter = getTwitterInstance(accessToken);
            if(twitter != null) {
                saveSessionTwitterInstance(session, twitter);
            }
        }
        return twitter;
    }


    public String getCallbackUrlPath()
    {
        if(mCallbackUrlPath == null) {
            mCallbackUrlPath = Config.getInstance().getString(CONFIG_KEY_CALLBACKURLPATH);
        }
        return mCallbackUrlPath;
    }
    public String getDefaultCallbackUri(String topLevelUrl)
    {
        return CommonAuthUtil.constructUrl(topLevelUrl, getCallbackUrlPath());
    }
    public String getCallbackUri(String topLevelUrl, String comebackUrl)
    {
        return getCallbackUri(topLevelUrl, comebackUrl, null);
    }
    public String getCallbackUri(String topLevelUrl, String comebackUrl, String fallbackUrl)
    {
        String baseUrl = getDefaultCallbackUri(topLevelUrl);
        Map<String, Object> params = new HashMap<String, Object>();
        if(comebackUrl != null) {
            params.put(CommonAuthUtil.PARAM_COMEBACKURL, comebackUrl);
        }
        if(fallbackUrl != null) {
            params.put(CommonAuthUtil.PARAM_FALLBACKURL, fallbackUrl);
        }
        String authUrl = URLUtil.buildUrl(baseUrl, params);
        return authUrl;
    }
    


    // TBD:
    // If the user is already authenticated, then calling getRequestToken...() will throw illegalStateException...
    // How to handle this???

    public RequestToken getRequestToken()
    {
        return getRequestToken((String) null);
    }
    public RequestToken getRequestToken(HttpSession session)
    {
        return getRequestToken(session, null);
    }
    public RequestToken getRequestToken(String callbackUrl)
    {
        RequestToken requestToken = null;
        try {
            if(callbackUrl == null) {
                requestToken = getBaseTwitterInstance().getOAuthRequestToken();
            } else {
                requestToken = getBaseTwitterInstance().getOAuthRequestToken(callbackUrl);
            }
        } catch (TwitterException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to get Twitter RequestToken for callbackUrl = " + callbackUrl, e);
            // What to do???
        } catch (Exception e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unkown excpetion while trying to get Twitter RequestToken for callbackUrl = " + callbackUrl, e);
            // What to do???
        }
        if(requestToken == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to get requestToken for callbackUrl = " + callbackUrl);
        } else {
            if(log.isLoggable(Level.FINE)) {
                String token = requestToken.getToken();
                String tokenSecret = requestToken.getTokenSecret();
                log.fine("RequestToken: token = " + token + "; tokenSecret = " + tokenSecret);
            }
        }
        return requestToken;
    }
    public RequestToken getRequestToken(HttpSession session, String callbackUrl)
    {
        RequestToken requestToken = null;
        try {
            if(callbackUrl == null) {
                requestToken = getBaseTwitterInstance(session).getOAuthRequestToken();
            } else {
                requestToken = getBaseTwitterInstance(session).getOAuthRequestToken(callbackUrl);
            }
        } catch (TwitterException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to get Twitter RequestToken for callbackUrl = " + callbackUrl, e);
            // What to do???
        } catch (Exception e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unkown excpetion while trying to get Twitter RequestToken for callbackUrl = " + callbackUrl, e);
            // What to do???
        }
        if(requestToken == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to get requestToken for callbackUrl = " + callbackUrl);
            // Check if it is in a session???
            // [1] Either ....  (Not being used.)
//            OAuthRequestToken oaReqToken = (OAuthRequestToken) session.getAttribute(CommonAuthUtil.getSessionKeyForRequestToken(CommonAuthUtil.AUTH_SERVICE_PROVIDERID_TWITTER));
//            if(oaReqToken != null) {
//                long now = System.currentTimeMillis();
//                long timestamp = oaReqToken.getTimestamp();
//                if(now - timestamp < REQUESTTOKEN_LIFETIME) {
//                    String tok = oaReqToken.getToken();
//                    String sec = oaReqToken.getTokenSecret();
//                    requestToken = new RequestToken(tok, sec);
//                } else {
//                    // ignore
//                }
//            }
            // [2] Or .... (Current implementation.)
//            requestToken = (RequestToken) session.getAttribute(SESSION_ATTR_TWITTER4J_REQUESTTOKEN);
            // Note:
            // This has been moved to JSP
            // ( --> Auth2Manager.jsp)
        }
        if(requestToken != null) {
            if(log.isLoggable(Level.FINE)) {
                String token = requestToken.getToken();
                String tokenSecret = requestToken.getTokenSecret();
                log.fine("RequestToken: token = " + token + "; tokenSecret = " + tokenSecret);
                // TBD: Store requestToken in the session?
                // --> This is done in Auth JSPs....
            }
        }
        return requestToken;
    }


    
    public AccessToken getDefaultOAuthAccessToken()
    {
        String token = TwitterAuthHelper.getInstance().getDefaultAccessToken();
        String secret = TwitterAuthHelper.getInstance().getDefaultAccessTokenSecret();
        AccessToken accessToken = new AccessToken(token, secret);
        return accessToken;
    }

    public AccessToken getOAuthAccessToken()
    {
        return getDefaultOAuthAccessToken();
    }

    private AccessToken getOAuthAccessToken(ExternalUserAuth userAuth)
    {
        // TBD: cache accessToken???
        AccessToken accessToken = null;
        if(userAuth != null) {
            String token = userAuth.getAccessToken();
            String secret = userAuth.getAccessTokenSecret();
            accessToken = new AccessToken(token, secret);
        }
        return accessToken;
    }

    // TBD:
    // Error fixed ???
    // (Need to update this across different projects ???)
    public AccessToken getOAuthAccessToken(String user)
    {
        // Wrong. The arg of getExternalUserAuth() is actually a guid of externalAuth record....
        // ExternalUserAuth userAuth = ExternalUserAuthHelper.getInstance().getExternalUserAuth(user);
        // Correct.
        ExternalUserAuth userAuth = ExternalUserAuthHelper.getInstance().findExternalUserAuthByUser(user, CommonAuthUtil.AUTH_SERVICE_PROVIDERID_TWITTER);
        AccessToken accessToken = getOAuthAccessToken(userAuth);
        return accessToken;
    }
    public AccessToken getOAuthAccessTokenByUserId(long userId)
    {
        ExternalUserAuth userAuth = ExternalUserAuthHelper.getInstance().findExternalUserAuthByUserId(String.valueOf(userId), CommonAuthUtil.AUTH_SERVICE_PROVIDERID_TWITTER);
        AccessToken accessToken = getOAuthAccessToken(userAuth);
        return accessToken;
    }
    public AccessToken getOAuthAccessTokenByUsername(String username)
    {
        ExternalUserAuth userAuth = ExternalUserAuthHelper.getInstance().findExternalUserAuthByUsername(username, CommonAuthUtil.AUTH_SERVICE_PROVIDERID_TWITTER);
        AccessToken accessToken = getOAuthAccessToken(userAuth);
        return accessToken;
    }

    public AccessToken getOAuthAccessToken(RequestToken requestToken)
    {
        return getOAuthAccessToken(requestToken, (String) null);
    }
    public AccessToken getOAuthAccessToken(HttpSession session, RequestToken requestToken)
    {
        return getOAuthAccessToken(session, requestToken, null);
    }
    public AccessToken getOAuthAccessToken(RequestToken requestToken, String verifier)
    {
        if(requestToken == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to get Twitter AccessToken because requestToken is null. verifier = " + verifier);
            return null;
        }
        AccessToken accessToken = null;
        try {
            if(verifier == null) {
                accessToken = getBaseTwitterInstance().getOAuthAccessToken(requestToken);
            } else {
                accessToken = getBaseTwitterInstance().getOAuthAccessToken(requestToken, verifier);
            }
        } catch (TwitterException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to get Twitter AccessToken. verifier = " + verifier, e);
            // What to do???
        } catch (Exception e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unknown error while trying to get AccessToken. verifier = " + verifier, e);
            // What to do???
        }
        return accessToken;
    }
    public AccessToken getOAuthAccessToken(HttpSession session, RequestToken requestToken, String verifier)
    {
        if(requestToken == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to get Twitter AccessToken because requestToken is null. verifier = " + verifier);
            return null;
        }
        AccessToken accessToken = null;
        try {
            if(verifier == null) {
                accessToken = getBaseTwitterInstance(session).getOAuthAccessToken(requestToken);
            } else {
                accessToken = getBaseTwitterInstance(session).getOAuthAccessToken(requestToken, verifier);
            }
        } catch (TwitterException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to get Twitter AccessToken. verifier = " + verifier, e);
            // What to do???
        } catch (Exception e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unknown error while trying to get AccessToken. verifier = " + verifier, e);
            // What to do???
        }
        return accessToken;
    }



    // ????
    public long getTwitterUserId()
    {
        long userId = 0L;  // ???
        
        try {
            // TBD:
            twitter4j.User user = getBaseTwitterInstance().verifyCredentials();
            //twitter4j.User user = getDefaultTwitterInstance().verifyCredentials();  // ?????
            if(user != null) {
                userId = user.getId();
            } else {
                // ???
            }
        } catch (TwitterException e) {
            log.log(Level.WARNING, "Failed to verify Twitter User.", e);
            // What to do???
        } catch (Exception e) {
            log.log(Level.WARNING, "Unknown error occurred while verifying Twitter User.", e);
            // What to do???
        }
        
        return userId;
    }
    public long getTwitterUserId(HttpSession session)
    {
        long userId = 0L;  // ???
        
        try {
            // TBD:
            twitter4j.User user = getBaseTwitterInstance(session).verifyCredentials();
            //twitter4j.User user = getDefaultTwitterInstance(session).verifyCredentials();  // ?????
            if(user != null) {
                userId = user.getId();
            } else {
                // ???
            }
        } catch (TwitterException e) {
            log.log(Level.WARNING, "Failed to verify Twitter User.", e);
            // What to do???
        } catch (Exception e) {
            log.log(Level.WARNING, "Unknown error occurred while verifying Twitter User.", e);
            // What to do???
        }
        
        return userId;
    }

    // ????
    public twitter4j.User getTwitterUser()
    {
        twitter4j.User twitterUser = null;
        try {
            // TBD:
            twitterUser = getBaseTwitterInstance().verifyCredentials();
            //twitterUser = getDefaultTwitterInstance(session).verifyCredentials();  // ?????
        } catch (TwitterException e) {
            log.log(Level.WARNING, "Failed to verify Twitter User.", e);
            // What to do???
        } catch (Exception e) {
            log.log(Level.WARNING, "Unknown error occurred while verifying Twitter User.", e);
            // What to do???
        }
        return twitterUser;
    }
    public twitter4j.User getTwitterUser(HttpSession session)
    {
        twitter4j.User twitterUser = null;
        try {
            // TBD:
            twitterUser = getBaseTwitterInstance(session).verifyCredentials();
            //twitterUser = getDefaultTwitterInstance(session).verifyCredentials();  // ?????
        } catch (TwitterException e) {
            log.log(Level.WARNING, "Failed to verify Twitter User.", e);
            // What to do???
        } catch (Exception e) {
            log.log(Level.WARNING, "Unknown error occurred while verifying Twitter User.", e);
            // What to do???
        }
        return twitterUser;
    }

 
    
    // ????
    // Returns the updated ExternalUserAuthBean (after create/update)
    public ExternalUserAuthBean saveTwitterAccessToken(HttpSession session, String user, AccessToken accessToken, ExternalUserAuthBean externalUserAuth)
    {
        String guid = null;
        if(accessToken != null) {
            twitter4j.User twitterUser = getTwitterUser(session);
            if(twitterUser != null) {

                // TBD:
                // Check if the account already exists in the table....
                // If so, update it rather than create a new one.
                // ...
                    
                boolean createNew = false;
                if(externalUserAuth == null) {
                    createNew = true;
                    externalUserAuth = new ExternalUserAuthBean();
                }
                guid = externalUserAuth.getGuid();
                if(guid == null || guid.isEmpty()) {   // Validate?
                    guid = GUID.generate();
                    externalUserAuth.setGuid(guid);
                }
                // TBD: Reuse any of the fields if externalUserAuth != null ???
                // Or, just overwrite everything fresh ???
                externalUserAuth.setProviderId(CommonAuthUtil.AUTH_SERVICE_PROVIDERID_TWITTER);
                externalUserAuth.setUser(user);
                String name = twitterUser.getName();
                if(name != null && !name.isEmpty()) {
                    externalUserAuth.setDisplayName(name);
                }

                ExternalUserIdStructBean externalUserId = new ExternalUserIdStructBean();
                externalUserId.setUuid(GUID.generate());
                externalUserId.setName(name);
                long twitterUserId = twitterUser.getId();
                // TBD:
//                externalUserId.setUserId(twitterUserId);
                externalUserId.setId(String.valueOf(twitterUserId));
                // ....
                String twitterUsername = twitterUser.getScreenName();
                externalUserId.setUsername(twitterUsername);
                
                // String email = twitterUser.getEmail();   // ???
                // externalUserId.setEmail(email);
                // etc... 
                
                externalUserAuth.setExternalUserId(externalUserId);

                String token = accessToken.getToken();
                String secret = accessToken.getTokenSecret();
                externalUserAuth.setAccessToken(token);
                externalUserAuth.setAccessTokenSecret(secret);

                String description = twitterUser.getDescription();
                if(description != null && !description.isEmpty()) {
                    if(description.length() > 500) {
                        description = description.substring(0, 500);   // ???
                    }
                    externalUserAuth.setDescription(description);
                }
                // URL profileURL = twitterUser.getProfileImageURL();   // v 2.2.6 ???
                String profileURL = twitterUser.getProfileImageURL();   // v 3.0.3 ???
                if(profileURL != null) {
                    // externalUserAuth.setProfileImageUrl(profileURL.toString());
                    externalUserAuth.setProfileImageUrl(profileURL);
                }
                String timeZone = twitterUser.getTimeZone();
                if(timeZone != null && !timeZone.isEmpty()) {
                    externalUserAuth.setTimeZone(timeZone);
                }
                String location = twitterUser.getLocation();
                if(location != null && !location.isEmpty()) {
                    externalUserAuth.setLocation(location);
                }
                String language = twitterUser.getLang();
                if(language != null && !language.isEmpty()) {
                    externalUserAuth.setLanguage(language);
                }
                
                long now = System.currentTimeMillis();
                Long createdTime = externalUserAuth.getCreatedTime();
                if(createdTime == null || createdTime == 0L) {
                    externalUserAuth.setCreatedTime(now);
                }
                externalUserAuth.setModifiedTime(now);
                externalUserAuth.setAuthTime(now);
                // ...
                
                // TBD: save or update.... ???
                externalUserAuth = (ExternalUserAuthBean) ExternalUserAuthHelper.getInstance().saveExternalUserAuth(externalUserAuth, createNew);
//                if(externalUserAuth != null) {
//                    guid = externalUserAuth.getGuid();
//                } else {
//                    guid = null;   // ???
//                }
            } else {
                // ???
                log.log(Level.WARNING, "Could not save the accessToken because failed to find the twitter user.");
            }
        } else {
            // ???
            log.log(Level.WARNING, "Could not save the accessToken because it is null.");
        }
        return externalUserAuth;
    }

    
    // TBD...
    
    public ExternalUserAuth getExternalUserAuth(long userId)
    {
        // TBD: ...
        ExternalUserAuth userAuth = null;
        //ExternalUserAuth userAuth = ExternalUserAuthHelper.getInstance().findExternalUserAuthByUserId(userId);
        return userAuth;
    }

    
    
    
}
