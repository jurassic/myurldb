package com.cannyurl.app.auth;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpSession;

import com.cannyurl.af.bean.UserAuthStateBean;
import com.cannyurl.app.service.UserAppService;
import com.cannyurl.app.service.UserAuthStateAppService;
import com.myurldb.ws.BaseException;
import com.myurldb.ws.UserAuthState;


// 3/03/13:
// 2/16/13: Fixed a missing-quote bug around externalId.id value.
//...


public class UserAuthStateHelper
{
    private static final Logger log = Logger.getLogger(UserAuthStateHelper.class.getName());

    private UserAuthStateHelper() {}

    // TBD: Is this safe for concurrent calls??
    private UserAppService userAppService = null;
    private UserAuthStateAppService userAuthAppService = null;
    // etc...


    private UserAppService getUserService()
    {
        if(userAppService == null) {
            userAppService = new UserAppService();
        }
        return userAppService;
    }   
    private UserAuthStateAppService getUserAuthStateService()
    {
        if(userAuthAppService == null) {
            userAuthAppService = new UserAuthStateAppService();
        }
        return userAuthAppService;
    }
    // etc. ...


    // Initialization-on-demand holder.
    private static final class UserAuthStateHelperHolder
    {
        private static final UserAuthStateHelper INSTANCE = new UserAuthStateHelper();
    }

    // Singleton method
    public static UserAuthStateHelper getInstance()
    {
        return UserAuthStateHelperHolder.INSTANCE;
    }

    
    
    
    public UserAuthState getUserAuthState(String guid) 
    {
        UserAuthState userAuth = null;
        try {
            userAuth = getUserAuthStateService().getUserAuthState(guid);
        } catch (BaseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve the userAuth for guid = " + guid, e);
            // ???
        }
        return userAuth;
    }
    

    
    public UserAuthState constructUserAuthState(UserAuthState userAuthState)
    {
        return saveUserAuthState(userAuthState, true);
    }    
    public UserAuthState refreshUserAuthState(UserAuthState userAuthState)
    {
        return saveUserAuthState(userAuthState, false);
    }    
    public UserAuthState saveUserAuthState(UserAuthState userAuthState, boolean createNew)
    {
        try {
            if(createNew) {
                userAuthState = getUserAuthStateService().constructUserAuthState(userAuthState);
            } else {
                userAuthState = getUserAuthStateService().refreshUserAuthState(userAuthState);                
            }
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to save UserAuthState.", e);
            // ???
        }
        return userAuthState;
    }
    
    
    
    public boolean deleteUserAuthState(UserAuthState userAuthState)
    {
        Boolean suc = false;
        try {
            suc = getUserAuthStateService().deleteUserAuthState(userAuthState);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to delete UserAuthState.", e);
            // ???
        }
        if(suc == null) {
            suc = false;    // ????
        }
        return suc;
    }

    // Note:
    // Deletes the most recently created userAuthState, if any.
    // TBD: Should we allow null providerId ????
    public boolean deleteUserAuthState(String user, String providerId)
    {
        Boolean suc = false;
        try {
            // This method catches the exception,
            // which makes it impossible to distinguish error vs. not-found....
            // TBD: All these methods shoudl be changed to throw exceptions???
            // UserAuthState userAuthState = findUserAuthStateForUser(user, providerId);
            // Instead....
            UserAuthState userAuthState = null;
            String filter = "user=='" + user + "'";   // authStatus ???
            if(providerId != null) {
                filter += " && providerId=='" + providerId + "'";
            }
            String ordering = "createdTime desc";
            try {
                List<UserAuthState> list = getUserAuthStateService().findUserAuthStates(filter, ordering, null, null, null, null, 0L, 2);  // ???
                if(list == null || list.isEmpty()) {
                    // ???
                    if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "UserAuthState list is empty for providerId = " + providerId + "; user guid = " + user);
                    // Nothing to delete.
                    // mark it as success ??? (this is more reasonable, for processing downward stream....)
                    suc = true;
                } else {
                    // What if there are more than one????
                    userAuthState = list.get(0);
                }
            } catch (BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve the userAuth list for providerId = " + providerId + "; user guid = " + user, e);
                // ???
            }
            
            if(userAuthState != null) {
                suc = getUserAuthStateService().deleteUserAuthState(userAuthState);
            }
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to delete UserAuthState.", e);
            // ???
        }
        if(suc == null) {
            suc = false;    // ????
        }
        return suc;
    }

    
    
    // TBD
    public boolean setUserAuthStatus(String user, String providerId, String authStatus)
    {
        // TBD: Validate providerId ????
        // TBD: Validate authStatus ????

        Boolean suc = false;
        try {
            // This method catches the exception,
            // which makes it impossible to distinguish error vs. not-found....
            // TBD: All these methods shoudl be changed to throw exceptions???
            // UserAuthState userAuthState = findUserAuthStateForUser(user, providerId);
            // Instead....
            UserAuthState userAuthState = null;
            String filter = "user=='" + user + "'";   // authStatus ???
            if(providerId != null) {
                filter += " && providerId=='" + providerId + "'";
            }
            String ordering = "createdTime desc";
            try {
                List<UserAuthState> list = getUserAuthStateService().findUserAuthStates(filter, ordering, null, null, null, null, 0L, 2);  // ???
                if(list == null || list.isEmpty()) {
                    // ???
                    if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "UserAuthState list is empty for providerId = " + providerId + "; user guid = " + user);
                } else {
                    // What if there are more than one????
                    userAuthState = list.get(0);
                }
            } catch (BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve the userAuth list for providerId = " + providerId + "; user guid = " + user, e);
                // ???
            }
            
            if(userAuthState != null) {
                String oldStatus = userAuthState.getAuthStatus();
                if(oldStatus != null && oldStatus.equals(authStatus)) {
                    if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "authStatus not changed: authStatus = " + authStatus);
                    suc = true;     // ???
                } else {
                    ((UserAuthStateBean) userAuthState).setAuthStatus(authStatus);
                    suc = getUserAuthStateService().updateUserAuthState(userAuthState);
                }
            } else {
                // ????
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "authStatus not updated: providerId = " + providerId + "; user guid = " + user);
            }
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to update UserAuthState.", e);
            // ???
        }
        if(suc == null) {
            suc = false;    // ????
        }
        return suc;
    }

    
    
    
    
    // TBD: ...
    public UserAuthState findUserAuthState(HttpSession session, String providerId, String user, String username, String email, String openId)
    {
        if(session == null) {
            return null;   // ????
        }
        UserAuthStateBean userAuthState = (UserAuthStateBean) session.getAttribute(AuthUtil.getSessionAttrKeyUserAuthState(providerId));
        if(userAuthState == null) {
            // query db ???
            // ...
        }
        return userAuthState;
    }

    
    
    // TBD:
    public UserAuthState findUserAuthStateForUser(String user, String providerId)
    {
        UserAuthState userAuth = null;

        String filter = "user=='" + user + "'";   // authStatus ???
        if(providerId != null) {
            filter += " && providerId=='" + providerId + "'";
        }
        String ordering = "createdTime desc";
        try {
            List<UserAuthState> list = getUserAuthStateService().findUserAuthStates(filter, ordering, null, null, null, null, 0L, 2);  // ???
            if(list == null || list.isEmpty()) {
                // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "UserAuthState list is empty for providerId = " + providerId + "; user guid = " + user);
            } else {
                // What if there are more than one????
                userAuth = list.get(0);
                // Do full fetch.
                userAuth = getUserAuthState(userAuth.getGuid());
            }
        } catch (BaseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve the userAuth list for providerId = " + providerId + "; user guid = " + user, e);
            // ???
        }

        return userAuth;
    }

    // TBD:
    public UserAuthState findUserAuthStateByUsername(String username, String providerId)
    {
        UserAuthState userAuth = null;

        String filter = "externalId.username=='" + username + "'";   // authStatus ???
        if(providerId != null) {
            filter += " && providerId=='" + providerId + "'";
        }
        String ordering = "createdTime desc";
        try {
            List<UserAuthState> list = getUserAuthStateService().findUserAuthStates(filter, ordering, null, null, null, null, 0L, 2);  // ???
            if(list == null || list.isEmpty()) {
                // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "UserAuthState list is empty for providerId = " + providerId + "; username = " + username);
            } else {
                // What if there are more than one????
                userAuth = list.get(0);
                // Do full fetch.
                userAuth = getUserAuthState(userAuth.getGuid());
            }
        } catch (BaseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve the userAuth list for providerId = " + providerId + "; username = " + username, e);
            // ???
        }

        return userAuth;
    }

    // TBD:
    public UserAuthState findUserAuthStateByUserId(Long userId, String providerId)
    {
        return findUserAuthStateByUserId(String.valueOf(userId), providerId);
    }
    public UserAuthState findUserAuthStateByUserId(String userId, String providerId)
    {
        UserAuthState userAuth = null;

        String filter = "externalId.id=='" + userId + "'";   // authStatus ???
        if(providerId != null) {
            filter += " && providerId=='" + providerId + "'";
        }
        String ordering = "createdTime desc";
        try {
            List<UserAuthState> list = getUserAuthStateService().findUserAuthStates(filter, ordering, null, null, null, null, 0L, 2);  // ???
            if(list == null || list.isEmpty()) {
                // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "UserAuthState list is empty for providerId = " + providerId + "; userId = " + userId);
            } else {
                // What if there are more than one????
                userAuth = list.get(0);
                // Do full fetch.
                userAuth = getUserAuthState(userAuth.getGuid());
            }
        } catch (BaseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve the userAuth list for providerId = " + providerId + "; userId = " + userId, e);
            // ???
        }

        return userAuth;
    }


    // TBD:
    public UserAuthState findUserAuthStateByEmail(String email, String providerId)
    {
        UserAuthState userAuth = null;

        String filter = "externalId.email=='" + email + "'";   // authStatus ???
        if(providerId != null) {
            filter += " && providerId=='" + providerId + "'";
        }
        String ordering = "createdTime desc";
        try {
            List<UserAuthState> list = getUserAuthStateService().findUserAuthStates(filter, ordering, null, null, null, null, 0L, 2);  // ???
            if(list == null || list.isEmpty()) {
                // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "UserAuthState list is empty for providerId = " + providerId + "; email = " + email);
            } else {
                // What if there are more than one????
                userAuth = list.get(0);
                // Do full fetch.
                userAuth = getUserAuthState(userAuth.getGuid());
            }
        } catch (BaseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve the userAuth list for providerId = " + providerId + "; email = " + email, e);
            // ???
        }

        return userAuth;
    }

    // TBD:
    public UserAuthState findUserAuthStateByOpenId(String openId, String providerId)
    {
        UserAuthState userAuth = null;

        String filter = "externalId.openId=='" + openId + "'";   // authStatus ???
        if(providerId != null) {
            filter += " && providerId=='" + providerId + "'";
        }
        String ordering = "createdTime desc";
        try {
            List<UserAuthState> list = getUserAuthStateService().findUserAuthStates(filter, ordering, null, null, null, null, 0L, 2);  // ???
            if(list == null || list.isEmpty()) {
                // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "UserAuthState list is empty for providerId = " + providerId + "; openId = " + openId);
            } else {
                // What if there are more than one????
                userAuth = list.get(0);
                // Do full fetch.
                userAuth = getUserAuthState(userAuth.getGuid());
            }
        } catch (BaseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve the userAuth list for providerId = " + providerId + "; openId = " + openId, e);
            // ???
        }

        return userAuth;
    }

    
    
    // TBD:
    public List<String> getUserAuthStateKeysForUser(String user)
    {
        return getUserAuthStateKeysForUser(user, (Integer) null);
    }
    public List<String> getUserAuthStateKeysForUser(String user, Integer maxCount)
    {
        return getUserAuthStateKeysForUser(user, null, maxCount);
    }
    public List<String> getUserAuthStateKeysForUser(String user, String providerId, Integer maxCount)
    {
        List<String> keys = null;

        String filter = "user=='" + user + "'";   // authStatus ???
        if(providerId != null) {
            filter += " && providerId=='" + providerId + "'";
        }
        String ordering = "createdTime desc";
        try {
            keys = getUserAuthStateService().findUserAuthStateKeys(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve userAuthState key list.", e);
        }

        return keys;
    }

    public List<UserAuthState> getUserAuthStatesForUser(String user)
    {
        return getUserAuthStatesForUser(user, (Integer) null);
    }
    public List<UserAuthState> getUserAuthStatesForUser(String user, Integer maxCount)
    {
        return getUserAuthStatesForUser(user, null, maxCount);
    }
    public List<UserAuthState> getUserAuthStatesForUser(String user, String providerId, Integer maxCount)
    {
        List<UserAuthState> beans = null;
        
        String filter = "user=='" + user + "'";   // authStatus ???
        if(providerId != null) {
            filter += " && providerId=='" + providerId + "'";
        }
        String ordering = "createdTime desc";
        try {
            beans = getUserAuthStateService().findUserAuthStates(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve userAuthState list.", e);
        }

        return beans;
    }

    
    
    
    
    
    
}
