package com.cannyurl.app.auth;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cannyurl.af.auth.SessionBean;
import com.cannyurl.af.auth.UserSessionManager;
import com.cannyurl.af.auth.user.AuthUser;
import com.cannyurl.af.auth.user.AuthUserService;
import com.cannyurl.af.auth.user.AuthUserServiceFactory;
import com.cannyurl.af.bean.GaeUserStructBean;
import com.cannyurl.af.bean.UserBean;
import com.cannyurl.app.service.UserAppService;
import com.myurldb.ws.BaseException;
import com.myurldb.ws.User;
import com.myurldb.ws.core.GUID;


// 3/03/13:
//...


public class UserSessionFilter implements Filter
{
    private static final Logger log = Logger.getLogger(UserSessionFilter.class.getName());

    // TBD
    private FilterConfig config = null;
    
    public UserSessionFilter()
    {
    }

    @Override
    public void init(FilterConfig config) throws ServletException
    {
        this.config = config;
        // TBD: 
    }

    @Override
    public void destroy()
    {
        this.config = null;
    }

    
    // This is very messy....
    // We are dealing with three objects:
    // (1) Session bean  (replaces the GAE session object)
    // (2) User bean (app-defined user object or AeryUser bean)
    // (3) AuthUser (wrapper around the user object of GAE UserService. Note that gae user is read only object...)
    // ...
    
    
    @Override
    public void doFilter(ServletRequest req, ServletResponse res,
            FilterChain chain) throws IOException, ServletException
    {
        // Add a sessionBean to the user session.
        SessionBean sessionBean = null;
        if(req != null && req instanceof HttpServletRequest) {
            sessionBean = UserSessionManager.getInstance().setSessionBean((HttpServletRequest) req, (HttpServletResponse) res);
            if(sessionBean != null) {
                String sessionId = sessionBean.getGuid();
                String sessionToken = sessionBean.getToken();
                if(log.isLoggable(Level.INFO)) log.info("Session: sessionId = " + sessionId + "; sessionToken = " + sessionToken);
            } else {
                log.warning("Failed to create/retrieve a sessionBean from the current session.");
            }
        } else {
            // This cannot happen.
            log.warning("Failed to create a session due to unknown reasons.");
            // TBD: Just bail out?
        }
        
        // TBD: 
        if(sessionBean != null) {
            String currentSessionId = sessionBean.getGuid();

            // ??? Set to true if sessionBean requires updating...
            boolean needToUpdateSession = false;
            // ....

            // Could be a new user....
            String currentUserGuid = sessionBean.getUserId();
            // currentUserGuid can be null at this point.... 

            String authUserGuid = null;
            AuthUserService authUserService = AuthUserServiceFactory.getAuthUserService();
            AuthUser authUser = authUserService.getCurrentUser();
            if(authUser != null) {
                // User is logged on.
                // We need to make sure that the logged on user is the same as the session user...
                // ....
                authUserGuid = authUser.getUserGuid();
                if(authUserGuid == null) {
                    // TBD: Is there a better way?
                    // This is clearly very inefficient to access DB every time!!!!!
                    String gaeOpenId = authUser.getNickname();
                    String authDomain = authUser.getAuthDomain();   // ??? Is this being used????
                    authUserGuid = AuthConfigManager.getInstance().findUserGuidByGaeNicknameAndAuthDomain(gaeOpenId, authDomain);
                    if(authUserGuid != null) {
                        authUser.setUserGuid(authUserGuid);     // For caching... Does this work???
                        if(log.isLoggable(Level.FINE)) log.fine("authUser set with authUserGuid = " + authUserGuid);
                    } else {
                        // ????
                        if(log.isLoggable(Level.INFO)) log.info("Failed to find a user for gaeOpenId = " + gaeOpenId + "; authDomain = " + authDomain);
                    }
                } else {
                    if(log.isLoggable(Level.FINER)) log.finer("Cached authUserGuid returned: " + authUserGuid);
                }
            } else {
                // User not logged on.
                // Just ignore and continue...
            }

            
            // OK,
            // This part is extremely complicated.
            // We have to check three variables:
            // currentUserGuid, authUserGuid, and authUser
            // ....
            
            // Auth Domain
            // Note: This filter does not really handle "logging on"....
            // the auth domain needs to be used when the user logs on, based on the identify provider service...
            // --> Cf. Auth1Selector.jsp
//            String gaAppsDomain = null;    // To be used as "federatedIdentity" arg...
//            if(AuthConfigManager.getInstance().ssoGoogleApps()) {
//                gaAppsDomain = AuthUtil.getGoogleAppsSsoAppsDomain(req);
//            }
            
            // Session user.
            if(currentUserGuid == null) {
                if(authUser == null) {
                    // [1] First time (no session), Not logged on.
                    log.finer("UserSessionFilter - Branch [1]:");
                    
                    // Create a user object
                    //userBean = constructUser(currentSessionId, null);
                    //currentUserGuid = userBean.getGuid();
                    currentUserGuid = AuthConfigManager.getInstance().createUser(currentSessionId, null);
//                    currentUserGuid = AuthConfigManager.getInstance().createUser(currentSessionId, null, gaAppsDomain);
                    needToUpdateSession = true;
                } else {
                    if(authUserGuid == null) {
                        // [3] First time (no session), Logged on, first time.
                        log.finer("UserSessionFilter - Branch [3]:");

                        // Create a user object
                        //userBean = constructUser(currentSessionId, null);
                        //currentUserGuid = userBean.getGuid();
                        currentUserGuid = AuthConfigManager.getInstance().createUser(currentSessionId, null);
//                        currentUserGuid = AuthConfigManager.getInstance().createUser(currentSessionId, null, gaAppsDomain);
                        needToUpdateSession = true;
                    } else {
                        // [4] First time (no session), Logged on, existing user.  ?????
                        if(log.isLoggable(Level.FINER)) log.finer("UserSessionFilter - Branch [4]: authUserGuid = " + authUserGuid);

                        // Find the user object for the given authUserGuid.
                        currentUserGuid = AuthConfigManager.getInstance().getUserGuid(authUserGuid);   // currentUserGuid == authUserGuid
                        needToUpdateSession = true;                    
                    }
                }                
            } else {
                if(authUser == null) {
                    // [5] Existing session, Not logged on....
                    if(log.isLoggable(Level.FINER)) log.finer("UserSessionFilter - Branch [5]: currentUserGuid = " + currentUserGuid);
                    
                    // Nothing to do...
                    
                    // TBD:
                    // Force the user to log on (based on some config/param to this session filter) ????
                    // ???
                    // Probably, it's better to put the logon/protected resource logic outside this session filter...
                    // ...
                } else {
                    if(authUserGuid == null) {
                        // [7] Existing session, Logged on, user not found....
                        if(log.isLoggable(Level.FINER)) log.finer("UserSessionFilter - Branch [7]: currentUserGuid = " + currentUserGuid);

                        // Update the user...
                        Boolean created = AuthConfigManager.getInstance().createOrUpdateUser(currentUserGuid, currentSessionId, authUser);
                        if(created == null) {
                            // If connection to User DB (or, AeryId service) is slow, etc....
                            // then we may end up trying to create a new user (rather than updating the user),
                            // which will likely fail...
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to create or update the user with currentUserGuid = " + currentUserGuid);
                        } else if(Boolean.TRUE.equals(created)) {
                            // Can this happen??? When can this happen???
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to find the user with currentUserGuid = " + currentUserGuid + ". A new user has been created.");                            
                        } else {
                            if(log.isLoggable(Level.INFO)) log.info("Successfully updated the user with currentUserGuid = " + currentUserGuid);
                        }
                    } else {
                        // [8] Existing session, Logged on, user found....
                        if(log.isLoggable(Level.FINER)) log.finer("UserSessionFilter - Branch [8]: currentUserGuid = " + currentUserGuid + "; authUserGuid = " + authUserGuid);

                        if(currentUserGuid.equals(authUserGuid)) {
                            log.finer("UserSessionFilter - Branch [8a]: currentUserGuid == authUserGuid");

                            // Nothing to do..
                            // TBD: Every time, update gaeUser just in case????
                        } else {
                            log.finer("UserSessionFilter - Branch [8b]: currentUserGuid != authUserGuid");

                            // TBD: Migrate all data from old user to new user ?????
                            // ...

                            // ???
                            if(log.isLoggable(Level.WARNING)) log.warning("The user guid in the current session, " + currentUserGuid + ", is different from authUserGuid, " + authUserGuid);
                            currentUserGuid = authUserGuid;
                            // TBD: Update LoggedOnUser.gaeUser????
                            needToUpdateSession = true;
                            // ...
                        }

                        // TBD:
                        // Still need to update sessionID of the userBean ???
                        Boolean updated = AuthConfigManager.getInstance().updateUserIfNecessary(currentUserGuid, currentSessionId);
                        if(updated == null) {
                            // This cannot happen!!!
                            if(log.isLoggable(Level.SEVERE)) log.severe("User not found for currentUserGuid = " + currentUserGuid);                            
                        } else {
                            if(updated == true) {
                                needToUpdateSession = true;
                            } else {
                                // ??
                                if(log.isLoggable(Level.INFO)) log.info("User not updated: currentUserGuid = " + currentUserGuid);                            
                            }
                        }

                    }
                }
            }

            if(needToUpdateSession == true) {
                if(currentUserGuid != null) {   // Can this happen????
                    sessionBean.setUserId(currentUserGuid);
                    UserSessionManager.getInstance().refreshSessionBean((HttpServletRequest) req, (HttpServletResponse) res, sessionBean);
                    if(log.isLoggable(Level.INFO)) log.info("Session has bean updated. currentUserGuid = " + currentUserGuid);
                } else {
                    // ???
                    log.severe("currentUserGuid is null.");
                }
            }
        } else {
            // ????
            // SessionBean cannot be null!!!
            log.severe("session is null.");
        }

        // Continue through filter chain.
        if(chain != null) {        // ???
            chain.doFilter(req, res);
        }
    }

    
}
