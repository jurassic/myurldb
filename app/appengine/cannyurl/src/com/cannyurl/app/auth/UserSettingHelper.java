package com.cannyurl.app.auth;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cannyurl.app.service.UserAppService;
import com.cannyurl.app.service.UserPasswordAppService;
import com.cannyurl.app.service.UserSettingAppService;
import com.myurldb.ws.BaseException;
import com.myurldb.ws.UserSetting;


// 3/03/13:
//...


public class UserSettingHelper
{
    private static final Logger log = Logger.getLogger(UserSettingHelper.class.getName());

    // "Lazy initialization"
    private UserAppService userAppService = null;
    private UserPasswordAppService userPasswordAppService = null;
    private UserSettingAppService userSettingAppService = null;
    // ...

    public UserSettingHelper() {}

    // Initialization-on-demand holder.
    private static final class UserSettingHelperHolder
    {
        private static final UserSettingHelper INSTANCE = new UserSettingHelper();
    }

    // Singleton method
    public static UserSettingHelper getInstance()
    {
        return UserSettingHelperHolder.INSTANCE;
    }

    private UserAppService getUserService()
    {
        if(userAppService == null) {
            userAppService = new UserAppService();
        }
        return userAppService;
    }
    private UserPasswordAppService getUserPasswordService()
    {
        if(userPasswordAppService == null) {
            userPasswordAppService = new UserPasswordAppService();
        }
        return userPasswordAppService;
    }
    private UserSettingAppService getUserSettingService()
    {
        if(userSettingAppService == null) {
            userSettingAppService = new UserSettingAppService();
        }
        return userSettingAppService;
    }

    


    public UserSetting findSettingForUser(String userGuid)
    {
        UserSetting userSetting = null;
        try {
            List<UserSetting> beans = null;
            // TBD:
            // Return only the beans which were created while the user was logged on ????
            // ... (e.g., use Memo.loggedOn field???)
            String filter = "user=='" + userGuid + "'";
            String ordering = "createdTime desc";
            beans = getUserSettingService().findUserSettings(filter, ordering, null, null, null, null, null, null);   
            if(beans != null && beans.size() > 0) {
                int size = beans.size();
                userSetting = beans.get(0);                
                if(size > 1) {
                    log.log(Level.WARNING, size + " beans found for userGuid = " + userGuid);
                }
            } else {
                log.log(Level.WARNING, "No beans for given userGuid = " + userGuid);
            }
        } catch (BaseException e) {
            log.log(Level.SEVERE, "Failed to find beans for given userGuid = " + userGuid, e);
            return null;
        }
        return userSetting;
    }

    public UserSetting findSettingByUsername(String username)
    {
        UserSetting userSetting = null;
        try {
            List<UserSetting> beans = null;
            String filter = "username=='" + username + "'";
            String ordering = "createdTime desc";
            beans = getUserSettingService().findUserSettings(filter, ordering, null, null, null, null, null, null);   
            if(beans != null && beans.size() > 0) {
                int size = beans.size();
                userSetting = beans.get(0);                
                if(size > 1) {
                    log.log(Level.WARNING, size + " beans found for username = " + username);
                }
            } else {
                log.log(Level.WARNING, "No beans for given username = " + username);
            }
        } catch (BaseException e) {
            log.log(Level.SEVERE, "Failed to find beans for given username = " + username, e);
            return null;
        }
        return userSetting;
    }

    public UserSetting findSettingByEmail(String email)
    {
        UserSetting userSetting = null;
        try {
            List<UserSetting> beans = null;
            String filter = "email=='" + email + "'";
            String ordering = "createdTime desc";
            beans = getUserSettingService().findUserSettings(filter, ordering, null, null, null, null, null, null);   
            if(beans != null && beans.size() > 0) {
                int size = beans.size();
                userSetting = beans.get(0);                
                if(size > 1) {
                    log.log(Level.WARNING, size + " beans found for email = " + email);
                }
            } else {
                log.log(Level.WARNING, "No beans for given email = " + email);
            }
        } catch (BaseException e) {
            log.log(Level.SEVERE, "Failed to find beans for given email = " + email, e);
            return null;
        }
        return userSetting;
    }

    public UserSetting findSettingByOpenId(String openId)
    {
        UserSetting userSetting = null;
        try {
            List<UserSetting> beans = null;
            String filter = "openId=='" + openId + "'";
            String ordering = "createdTime desc";
            beans = getUserSettingService().findUserSettings(filter, ordering, null, null, null, null, null, null);   
            if(beans != null && beans.size() > 0) {
                int size = beans.size();
                userSetting = beans.get(0);                
                if(size > 1) {
                    log.log(Level.WARNING, size + " beans found for openId = " + openId);
                }
            } else {
                log.log(Level.WARNING, "No beans for given openId = " + openId);
            }
        } catch (BaseException e) {
            log.log(Level.SEVERE, "Failed to find beans for given openId = " + openId, e);
            return null;
        }
        return userSetting;
    }

    
}
