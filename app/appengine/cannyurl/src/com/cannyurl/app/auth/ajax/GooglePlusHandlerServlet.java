package com.cannyurl.app.auth.ajax;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.cannyurl.af.auth.SessionBean;
import com.cannyurl.af.auth.UserSessionManager;
import com.cannyurl.af.auth.googleplus.GooglePlusAuthHelper;
import com.cannyurl.af.auth.googleplus.GooglePlusAuthUtil;
import com.cannyurl.af.bean.ExternalUserAuthBean;
import com.cannyurl.af.util.CsrfHelper;
import com.cannyurl.app.auth.GooglePlusSignInHelper;
import com.cannyurl.fe.util.JsonJsBeanUtil;
import com.myurldb.ws.core.StatusCode;


// 3/03/13: Google+ SignIn (Work in progress)


// Mainly, for ajax calls....
public class GooglePlusHandlerServlet extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(GooglePlusHandlerServlet.class.getName());

    // Query params...
    // format == xml, json, jsonp, scrpt, text, html, ....  Currently, only "json" is allowed.
    // Note that the default format is either text of json depending on the method....
    // format=json overwrites the output format of the method which would have otherwise produced text content.  
    private static final String QUERY_PARAM_FORMAT = "format";
    private static final String QUERY_PARAM_JSONP_CALLBACK1 = "jsonp";
    private static final String QUERY_PARAM_JSONP_CALLBACK2 = "callback";
    // ...
    private static final String PATH_COMMAND_AUTHSTRUCT = "authstruct";
    private static final String PATH_COMMAND_AUTHENTICATED = "authenticated";
    private static final String PATH_COMMAND_LOGINURL = "loginurl";
    private static final String PATH_COMMAND_LOGOUTURL = "logouturl";
    // ...

    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        // throw new ServletException("Not implemented.");
        resp.setStatus(StatusCode.METHOD_NOT_ALLOWED);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        log.info("doPost() called.");


        // TBD:
        // Based on pathInfo + query params
        // Process different requests...  ???
        // ...
        String requestUrl = req.getRequestURL().toString();
        String contextPath = req.getContextPath();
        String servletPath = req.getServletPath();
        String pathInfo = req.getPathInfo();
        String queryString = req.getQueryString();
        if(log.isLoggable(Level.FINE)) log.fine("requestUrl = " + requestUrl);
        if(log.isLoggable(Level.FINE)) log.fine("contextPath = " + contextPath);
        if(log.isLoggable(Level.FINE)) log.fine("servletPath = " + servletPath);
        if(log.isLoggable(Level.FINE)) log.fine("pathInfo = " + pathInfo);
        if(log.isLoggable(Level.FINE)) log.fine("queryString = " + queryString);
        
        
        // TBD:
        String command = null;
        if(pathInfo != null) {
            if(pathInfo.equals("/connect")) {
                command = "connect";
            } else if(pathInfo.equals("/disconnect")) {
                command = "disconnect";
            }
        }
        
        if(command == null) {
            log.warning("Invalid command: pathInfo = " + pathInfo);
            resp.setStatus(StatusCode.BAD_REQUEST);   // Or, forbidden?
            return;            
        }


        // TBD:
        // Use a filter....
        String formId = GooglePlusAuthHelper.getInstance().getDefaultSignInFormId();   // ???
        boolean isVerified = CsrfHelper.getInstance().verifyCsrfState(req, formId);
        if(isVerified == false) {
            // TBD:
            // Bail out....
            log.warning("CSRF State variable verification failed.");
            resp.setStatus(StatusCode.BAD_REQUEST);   // Or, forbidden?
            return;
        }

        // Session....
        // Can this be null ???
        HttpSession session = req.getSession();

        // Is this necessary????
        // When this servlet is called, the user should not have been authenticated... ?????
        GoogleTokenResponse tokenResponse = null;
        if(session != null) {
            tokenResponse = (GoogleTokenResponse) session.getAttribute(GooglePlusAuthUtil.SESSION_ATTR_TOKENRESPONSE);
        }
        if(tokenResponse != null) {
            // ????
            log.info("tokenResponse already exists in the session.");
            // what to do???
//            resp.setStatus(StatusCode.NO_CONTENT);   // ???
//            return;
        }
       
        String user = null;
        if(session != null) {
            SessionBean sessionBean = UserSessionManager.getInstance().setSessionBean(req, resp);
            String sessionToken = sessionBean.getToken();
            if(sessionToken != null) {
                user = sessionBean.getUserId();
            }
        }
        
        
        if(command.equals("connect")) {
            // [1] Connect
            
            try {
                BufferedReader reader = req.getReader();
                String inJsonStr = reader.readLine();
                if(log.isLoggable(Level.INFO)) log.info("inJsonStr = " + inJsonStr);
    
                String code = null;
                Map<String,String> jsonObjectMap = JsonJsBeanUtil.parseJsonObjectMapString(inJsonStr);
                for(String key : jsonObjectMap.keySet()) {
                    String json = jsonObjectMap.get(key);
                    if(log.isLoggable(Level.INFO)) log.info("key = " + key + "; json = " + json);
                    
                    if(key.equals("code")) {
                        code = json;
                    } else {
                        if(log.isLoggable(Level.WARNING)) log.warning("Unregconized key in the json input string. key = " + key);
                    }
                }
    
                if(code != null && !code.isEmpty()) {
                    String outJsonStr = ""; ///....
                    
                    
                    tokenResponse = GooglePlusSignInHelper.getInstance().convertToGooglePlusTokenResponse(code);
                    if(tokenResponse != null) {
                        if(session != null) {
                            session.setAttribute(GooglePlusAuthUtil.SESSION_ATTR_TOKENRESPONSE, tokenResponse);
                        }
                        // GoogleCredential credential = GooglePlusSignInHelper.getInstance().getGooglePlusCredential(tokenResponse);
                         
                        ExternalUserAuthBean externalUserAuth = new ExternalUserAuthBean();
                        externalUserAuth = GooglePlusSignInHelper.getInstance().saveGooglePlusTokenResponse(session, user, tokenResponse, externalUserAuth);
                        
                        // TBD:
                        // ...
    
                    } else {
                        // ?????
                        log.log(Level.WARNING, "Failed to convert the input code to tokenResponse.");
                        resp.setStatus(StatusCode.INTERNAL_SERVER_ERROR);  // ????
                        return;
                    }
                    
                    String contentType = "application/json;charset=UTF-8";
                    resp.setContentType(contentType);            
        
                    PrintWriter writer = resp.getWriter();
                    writer.print(outJsonStr);
                    resp.setStatus(StatusCode.OK);  
                } else {
                    // ????
                    log.log(Level.WARNING, "Google plus authentication request processing failed because the param, code, is missing.");
                    resp.setStatus(StatusCode.BAD_REQUEST);
                    return;
                }
    
            } catch(Exception e) {
                log.log(Level.WARNING, "Exception while processing google plus authentication request.", e);
                resp.setStatus(StatusCode.INTERNAL_SERVER_ERROR);  // ????
                return;
            }
        } else {
            // [2] Disconnect...

            // TBD:
            
            
            
            
            
            resp.setStatus(StatusCode.NOT_IMPLEMENTED);
            return;
            
        }
        
        log.info("doPost(): BOTTOM");
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }

}
