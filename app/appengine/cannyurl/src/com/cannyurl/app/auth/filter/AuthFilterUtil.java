package com.cannyurl.app.auth.filter;

import java.net.URLEncoder;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.cannyurl.af.auth.SessionBean;
import com.cannyurl.af.auth.UserSessionManager;
import com.cannyurl.af.auth.common.CommonAuthUtil;
import com.cannyurl.af.auth.user.AuthUser;
import com.cannyurl.af.auth.user.AuthUserService;
import com.cannyurl.af.auth.user.AuthUserServiceFactory;
import com.cannyurl.af.bean.ExternalUserIdStructBean;
import com.cannyurl.af.bean.UserAuthStateBean;
import com.cannyurl.app.auth.AuthManager;
import com.cannyurl.app.auth.RequestAuthStateBean;
import com.cannyurl.app.util.ConfigUtil;
import com.cannyurl.common.AppAuthMode;
import com.myurldb.ws.ExternalUserIdStruct;
import com.myurldb.ws.core.GUID;


// 3/03/13: Google+ SignIn (Work in progress)
// 2/13/13: Facebook connect support.
// 2/07/13: Google OpenID support....
//...


public class AuthFilterUtil
{
    private static final Logger log = Logger.getLogger(AuthFilterUtil.class.getName());

    private AuthFilterUtil() {}

    private static boolean isApplicationAuthDisabled = false;
    private static String applicationAuthMode = null;
    static {
        isApplicationAuthDisabled = ConfigUtil.isApplicationAuthDisabled();
        if(isApplicationAuthDisabled) {
            log.warning("isApplicationAuthDisabled == true");
        } else {
            log.info("isApplicationAuthDisabled == false");
        }
        applicationAuthMode = ConfigUtil.getApplicationAuthMode();
        // TBD:
        // Validate applicationAuthMode ????
        // ...
        if(applicationAuthMode == null || applicationAuthMode.isEmpty()) {
            applicationAuthMode = AppAuthMode.getDefaultValue();
            if(log.isLoggable(Level.WARNING)) log.warning("applicationAuthMode set to " + applicationAuthMode);
        } else {
            if(log.isLoggable(Level.INFO)) log.info("applicationAuthMode = " + applicationAuthMode);
        }
    }

    
    public static RequestAuthStateBean createAuthStateBean(ServletRequest req, ServletResponse res)
    {
        HttpSession session = ((HttpServletRequest) req).getSession();
        String referer = ((HttpServletRequest) req).getHeader("referer");

        String providerId = null; 
        boolean isAuthenticated = false;
        String name = null;
        String email = null;
        String username = null;
        // TBD:
//        Long userId = null;
        String userId = null;
        // ....
        String openId = null;
        if(applicationAuthMode.equals(AppAuthMode.MODE_OPENID)) {
            AuthUserService authUserService = AuthUserServiceFactory.getAuthUserService();
            boolean isLoggedIn = authUserService.isUserLoggedIn();
            if(isLoggedIn) {
                AuthUser authUser = authUserService.getCurrentUser();
                if(authUser != null) {
                    isAuthenticated = true;
                    name = authUser.getNickname();
                    email = authUser.getEmail();
                    // TBD:
//                    username = authUser.getUserId();             // ???
                    // username = authUser.getNickname();        // ????
                    userId = authUser.getUserId();             // ???
                    // ...
                    openId = authUser.getFederatedIdentity();    // ???
                    // ....
                }
            }
        } else if(applicationAuthMode.equals(AppAuthMode.MODE_GOOGLE)) {
            // OpenID w/ Google preselected....
            
            // TBD:
            // Use AuthService or ExternalUserId ????
            // ...

            // [1] ???
//            AuthUserService authUserService = AuthUserServiceFactory.getAuthUserService();
//            boolean isLoggedIn = authUserService.isUserLoggedIn();
//            if(isLoggedIn) {
//                AuthUser authUser = authUserService.getCurrentUser();
//                if(authUser != null) {
//                    isAuthenticated = true;
//                    name = authUser.getNickname();
//                    email = authUser.getEmail();
//                    username = authUser.getUserId();             // ???
//                    // userId ????
//                    openId = authUser.getFederatedIdentity();    // ???
//                    // ....
//                }
//            }

            // [2] ????
            providerId = CommonAuthUtil.AUTH_SERVICE_PROVIDERID_GOOGLE;
            // isAuthenticated = AuthManager.getInstance().isUserLoggedIn(session, providerId);
            ExternalUserIdStruct externalId = AuthManager.getInstance().getExternalUserIdStruct(session, providerId);
            if(externalId != null) {
                isAuthenticated = true;      // ??? In the current implementation, once the user logs out, we delete the externalId... This might change in the future....
                name = externalId.getName();
                email = externalId.getEmail();
                username = externalId.getUsername();
                userId = externalId.getId();
                openId = externalId.getOpenId();
                // ....
            }
            
            // TBD:
            AuthUserService authUserService = AuthUserServiceFactory.getAuthUserService();
            boolean isLoggedIn = authUserService.isUserLoggedIn();
            if(isLoggedIn != isAuthenticated) {
                // Something's wrong....
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "AuthStateFilter.createAuthStateBean(): Inconsistent auth state: isAuthenticated = " + isAuthenticated + "; isLoggedIn = " + isLoggedIn);

                // What to do???
                if(isLoggedIn) {
                    AuthUser authUser = authUserService.getCurrentUser();
                    if(authUser != null) {
                        isAuthenticated = true;
                        name = authUser.getNickname();
                        email = authUser.getEmail();
                        username = authUser.getUserId();             // ???
                        // userId ????
                        openId = authUser.getFederatedIdentity();    // ???
                        // ....

                        // ????
                        UserAuthStateBean userAuthState = new UserAuthStateBean();
                        userAuthState.setGuid(GUID.generate());
                        userAuthState.setProviderId(providerId);
                        // userAuthState.setFirstAuthTime(now);   // ????
                        // etc...
                        ExternalUserIdStructBean externalUserIdStruct = new ExternalUserIdStructBean();
                        externalUserIdStruct.setUuid(GUID.generate());
                        externalUserIdStruct.setName(name);
                        externalUserIdStruct.setEmail(email);
                        externalUserIdStruct.setUsername(username);
                        // externalUserIdStruct.setUserId(userId);
                        externalUserIdStruct.setOpenId(openId);
                        // ...
                        userAuthState.setExternalId(externalUserIdStruct);
                        boolean suc = AuthManager.getInstance().processUserLogIn(session, providerId, userAuthState, true);
                        if(suc) {
                            isAuthenticated = true;
                        } else {
                            // ?????
                            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process user login.");
                        }
                    } else {
                        // ????
                        if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to find the authUser.");
                    }
                } else {
                    boolean suc = AuthManager.getInstance().processUserLogOut(session, providerId);
                    if(suc) {
                        isAuthenticated = false;
                    } else {
                        // ????
                        if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process user logout.");
                    }
                }
            }

        } else if(applicationAuthMode.equals(AppAuthMode.MODE_GOOGLEPLUS)) {
            providerId = CommonAuthUtil.AUTH_SERVICE_PROVIDERID_GOOGLEPLUS;
            // isAuthenticated = AuthManager.getInstance().isUserLoggedIn(session, providerId);
            ExternalUserIdStruct externalId = AuthManager.getInstance().getExternalUserIdStruct(session, providerId);
            if(externalId != null) {
                isAuthenticated = true;      // ??? In the current implementation, once the user logs out, we delete the externalId... This might change in the future....
                name = externalId.getName();
                email = externalId.getEmail();
                username = externalId.getUsername();
                userId = externalId.getId();
                openId = externalId.getOpenId();
                // ....
            }
            // ...            
        } else if(applicationAuthMode.equals(AppAuthMode.MODE_TWITTER)) {
            providerId = CommonAuthUtil.AUTH_SERVICE_PROVIDERID_TWITTER;
            // isAuthenticated = AuthManager.getInstance().isUserLoggedIn(session, providerId);
            ExternalUserIdStruct externalId = AuthManager.getInstance().getExternalUserIdStruct(session, providerId);
            if(externalId != null) {
                isAuthenticated = true;      // ??? In the current implementation, once the user logs out, we delete the externalId... This might change in the future....
                name = externalId.getName();
                email = externalId.getEmail();
                username = externalId.getUsername();
                userId = externalId.getId();
                openId = externalId.getOpenId();
                // ....
            }
        } else if(applicationAuthMode.equals(AppAuthMode.MODE_FACEBOOK)) {
            providerId = CommonAuthUtil.AUTH_SERVICE_PROVIDERID_FACEBOOK;
            // isAuthenticated = AuthManager.getInstance().isUserLoggedIn(session, providerId);
            ExternalUserIdStruct externalId = AuthManager.getInstance().getExternalUserIdStruct(session, providerId);
            if(externalId != null) {
                isAuthenticated = true;      // ??? In the current implementation, once the user logs out, we delete the externalId... This might change in the future....
                name = externalId.getName();
                email = externalId.getEmail();
                username = externalId.getUsername();
                userId = externalId.getId();
                openId = externalId.getOpenId();
                // ....
            }
        } else {
            // ???
        }
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "AuthFilterUtil.createAuthStateBean(): providerId = " + providerId);
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "AuthFilterUtil.createAuthStateBean(): isAuthenticated = " + isAuthenticated);

        // Session is required...
        SessionBean sessionBean = UserSessionManager.getInstance().setSessionBean((HttpServletRequest) req, (HttpServletResponse) res);
        String user = null;
        if(sessionBean != null) {
            user = sessionBean.getUserId();
        }
        RequestAuthStateBean authStateBean = new RequestAuthStateBean(user);

        // ...
        if(providerId != null) {
            authStateBean.setProviderId(providerId);
        } else {
            // ????
            if(applicationAuthMode.equals(AppAuthMode.MODE_OPENID)) {
                // authStateBean.setProviderId(null);   // ????
                // ...
            } else if(applicationAuthMode.equals(AppAuthMode.MODE_GOOGLE)) {
                // ???
                authStateBean.setProviderId(CommonAuthUtil.AUTH_SERVICE_PROVIDERID_GOOGLE);
            } else if(applicationAuthMode.equals(AppAuthMode.MODE_GOOGLEPLUS)) {
                authStateBean.setProviderId(CommonAuthUtil.AUTH_SERVICE_PROVIDERID_GOOGLEPLUS);
            } else if(applicationAuthMode.equals(AppAuthMode.MODE_TWITTER)) {
                authStateBean.setProviderId(CommonAuthUtil.AUTH_SERVICE_PROVIDERID_TWITTER);
            } else if(applicationAuthMode.equals(AppAuthMode.MODE_FACEBOOK)) {
                authStateBean.setProviderId(CommonAuthUtil.AUTH_SERVICE_PROVIDERID_FACEBOOK);
            } else {
                // ????
            }
        }
        authStateBean.setAuthenticated(isAuthenticated);

        authStateBean.setName(name);
        authStateBean.setEmail(email);
        authStateBean.setUsername(username);
        authStateBean.setUserId(userId);
        authStateBean.setOpenId(openId);
        // etc...

        
        String loginUrl = null;
        String logoutUrl = null;
        if(isAuthenticated) {
            // Note:
            // Some page requires/implements automatic login.
            // If we use the current page as the logout comeback url, 
            // we end up automatically being logged in right after logging out....
            // TBD: this is a problem. Need to look into the automatic login logic....
            // For now, just use the home page (or, any page that does not require login...)
//            String comebackUrl = req.getParameter(CommonAuthUtil.PARAM_COMEBACKURL);
//            if(comebackUrl == null || comebackUrl.isEmpty()) {
//                // ???
//                // if(referer != null && !referer.isEmpty()) {   // ????
//                //     comebackUrl = referer;  // ???
//                // } else {
//                    comebackUrl = "/home";
//                // }
//            }
            // ???
            String comebackUrl = "/home";
            // ????

            String encodedComebackUrl = null;
            if(comebackUrl != null && !comebackUrl.isEmpty()) {
                try {
                    encodedComebackUrl = URLEncoder.encode(comebackUrl, "UTF-8");
                } catch(Exception e) {
                    // ignore
                    if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Failed to encode comebackUrl", e);
                }
            }
//            String fallbackUrl = req.getParameter(CommonAuthUtil.PARAM_FALLBACKURL);
//            if(fallbackUrl == null || fallbackUrl.isEmpty()) {
//                // if(referer != null && !referer.isEmpty()) {   // ????
//                //     fallbackUrl = referer;  // ???
//                // } else {
//                     fallbackUrl = comebackUrl;    // ????
//                // }
//            }
            // ????
            String fallbackUrl = comebackUrl;    // ????
            // ????

            String encodedFallbackUrl = null;
            if(fallbackUrl != null && !fallbackUrl.isEmpty()) {
                try {
                    encodedFallbackUrl = URLEncoder.encode(fallbackUrl, "UTF-8");
                } catch(Exception e) {
                    // ignore
                    if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Failed to encode fallbackUrl", e);
                }
            }

            String logoutPage = null; 
            if(applicationAuthMode.equals(AppAuthMode.MODE_OPENID)) {
                AuthUserService authUserService = AuthUserServiceFactory.getAuthUserService();
                // TBD:
                // This is a bit of a problem...
                // If the current page (getRequestURI()) requires login,
                // logout will automatically redirect to login page, if we use the current page as the destination url....
                // ....
                String destinationUrl = null;
                if(comebackUrl != null && !comebackUrl.isEmpty()) {
                    destinationUrl = comebackUrl;
                } else {
                    if(referer != null && !referer.isEmpty()) {
                        destinationUrl = referer;   // ???
                    } else {
                        // Just use a universal page which does not require login??? 
                        // destinationUrl = "/home"; // ???
                        destinationUrl = ((HttpServletRequest) req).getRequestURI();
                    }
                }
                logoutUrl = authUserService.createLogoutURL(destinationUrl);
            } else if(applicationAuthMode.equals(AppAuthMode.MODE_GOOGLE)) {
                // ???
                // AuthUserService authUserService = AuthUserServiceFactory.getAuthUserService();
                // logoutUrl = authUserService.createLogoutURL(((HttpServletRequest) req).getRequestURI());
                // ???

                logoutPage = "/google/logout";
                // ....

            } else if(applicationAuthMode.equals(AppAuthMode.MODE_GOOGLEPLUS)) {
                logoutPage = "/google+/logout";
            } else if(applicationAuthMode.equals(AppAuthMode.MODE_TWITTER)) {
                logoutPage = "/twitter/logout";
            } else if(applicationAuthMode.equals(AppAuthMode.MODE_FACEBOOK)) {
                logoutPage = "/facebook/logout";
            } else {
                // ???
                if(log.isLoggable(Level.WARNING)) log.warning("Unsupported/Unimplemented applicationAuthMode = " + applicationAuthMode);
                // ???
            }
            if(logoutPage != null) {
                if(encodedComebackUrl != null) {
                    logoutUrl = logoutPage + "?comebackUrl=" + encodedComebackUrl;
                    if(encodedFallbackUrl != null) {
                        logoutUrl += "&fallbackUrl=" + encodedFallbackUrl;
                    }
                } else {
                    if(encodedFallbackUrl != null) {
                        logoutUrl = logoutPage + "?fallbackUrl=" + encodedFallbackUrl;
                    } else {
                        logoutUrl = logoutPage;
                    }
                }
            }

        } else {
            // TBD: Does this make sense???
            String comebackUrl = req.getParameter(CommonAuthUtil.PARAM_COMEBACKURL);
            if(comebackUrl == null || comebackUrl.isEmpty()) {
                comebackUrl = ((HttpServletRequest) req).getRequestURI();
                if(comebackUrl == null || comebackUrl.isEmpty()) {
                    if(referer != null && !referer.isEmpty()) {   // ????
                        comebackUrl = referer;  // ???
                    } else {
                       comebackUrl = "/home"; // ????
                    }
                }
            }
            // ????
            // String comebackUrl = ((HttpServletRequest) req).getRequestURI();;
            // ????
            
            String encodedComebackUrl = null;
            try {
                encodedComebackUrl = URLEncoder.encode(comebackUrl, "UTF-8");
            } catch(Exception e) {
                // ignore
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Failed to encode comebackUrl", e);
            }
            String fallbackUrl = req.getParameter(CommonAuthUtil.PARAM_FALLBACKURL);
            if(fallbackUrl == null || fallbackUrl.isEmpty()) {
                // if(referer != null && !referer.isEmpty()) {   // ????
                //     fallbackUrl = referer;  // ???
                // } else {
                     fallbackUrl = "/error/Unauthorized";
                     // fallbackUrl = "/error/AuthFailure";
                // }
            }
            // ?????
            // String fallbackUrl = "/error/Unauthorized";
            // ?????

            String encodedFallbackUrl = null;
            try {
                encodedFallbackUrl = URLEncoder.encode(fallbackUrl, "UTF-8");
            } catch(Exception e) {
                // ignore
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Failed to encode fallbackUrl", e);
            }

            String loginPage = null;
            if(applicationAuthMode.equals(AppAuthMode.MODE_OPENID)) {
                loginPage = "/auth";
            } else if(applicationAuthMode.equals(AppAuthMode.MODE_GOOGLE)) {
                // ????
                loginPage = "/google/login";
                // ...
            } else if(applicationAuthMode.equals(AppAuthMode.MODE_GOOGLEPLUS)) {
                loginPage = "/google+/login";
            } else if(applicationAuthMode.equals(AppAuthMode.MODE_TWITTER)) {
                loginPage = "/twitter/login";
            } else if(applicationAuthMode.equals(AppAuthMode.MODE_FACEBOOK)) {
                loginPage = "/facebook/login";
            } else {
                // ???
                if(log.isLoggable(Level.WARNING)) log.warning("Unsupported/Unimplemented applicationAuthMode = " + applicationAuthMode);
                // ???
            }
            if(loginPage != null) {
                if(encodedComebackUrl != null) {
                    loginUrl = loginPage + "?comebackUrl=" + encodedComebackUrl;
                    if(encodedFallbackUrl != null) {
                        loginUrl += "&fallbackUrl=" + encodedFallbackUrl;
                    }
                } else {
                    if(encodedFallbackUrl != null) {
                        loginUrl = loginPage + "?fallbackUrl=" + encodedFallbackUrl;
                    } else {
                        loginUrl = loginPage;
                    }
                }
            }
        }
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "AuthFilterUtil.createAuthStateBean(): loginUrl = " + loginUrl);
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "AuthFilterUtil.createAuthStateBean(): logoutUrl = " + logoutUrl);

        authStateBean.setLoginUrl(loginUrl);
        authStateBean.setLogoutUrl(logoutUrl);
        // ...

        return authStateBean;
    }

}
