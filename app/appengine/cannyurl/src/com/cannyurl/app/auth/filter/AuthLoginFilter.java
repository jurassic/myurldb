package com.cannyurl.app.auth.filter;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.cannyurl.af.auth.common.CommonAuthUtil;
import com.cannyurl.af.auth.user.AuthUserService;
import com.cannyurl.af.auth.user.AuthUserServiceFactory;
import com.cannyurl.app.auth.AuthManager;
import com.cannyurl.app.auth.AuthUtil;
import com.cannyurl.app.auth.RequestAuthStateBean;
import com.cannyurl.app.util.ConfigUtil;
import com.cannyurl.common.AppAuthMode;


// 3/03/13:
// ...


// For pages for which login is required.
// If the user is not authenticated, redirects to a login page.
public class AuthLoginFilter implements Filter
{
    private static final Logger log = Logger.getLogger(AuthLoginFilter.class.getName());

    // TBD
    private FilterConfig config = null;
    private boolean isApplicationAuthDisabled = false;
    private String applicationAuthMode = null;

    public AuthLoginFilter()
    {
    }

    @Override
    public void destroy()
    {
        this.config = null;
    }

    @Override
    public void init(FilterConfig config) throws ServletException
    {
        this.config = config;
        this.isApplicationAuthDisabled = ConfigUtil.isApplicationAuthDisabled();
        if(this.isApplicationAuthDisabled) {
            log.warning("isApplicationAuthDisabled == true");
        } else {
            log.info("isApplicationAuthDisabled == false");
        }
        this.applicationAuthMode = ConfigUtil.getApplicationAuthMode();
        // TBD:
        // Validate applicationAuthMode ????
        // ...
        if(this.applicationAuthMode == null || this.applicationAuthMode.isEmpty()) {
            this.applicationAuthMode = AppAuthMode.getDefaultValue();
            if(log.isLoggable(Level.WARNING)) log.warning("applicationAuthMode set to " + this.applicationAuthMode);
        } else {
            if(log.isLoggable(Level.INFO)) log.info("applicationAuthMode = " + this.applicationAuthMode);
        }
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res,
            FilterChain chain) throws IOException, ServletException
    {
        if(isApplicationAuthDisabled) {
            chain.doFilter(req, res);
            return;
        }

    
        RequestAuthStateBean authStateBean = null;
        // ???? Reuse authStateBean, if one is found in the current request????
        authStateBean = (RequestAuthStateBean) req.getAttribute(AuthUtil.REQUEST_ATTR_AUTHSTATEBEAN);
        if(authStateBean == null) {
            authStateBean = AuthFilterUtil.createAuthStateBean(req, res);
            if(authStateBean != null) {
                // Add it to the current request object....
                req.setAttribute(AuthUtil.REQUEST_ATTR_AUTHSTATEBEAN, authStateBean);
            }
        }

//        // Add it to the current request object....
//        if(authStateBean != null) {
//            req.setAttribute(AuthUtil.REQUEST_ATTR_AUTHSTATEBEAN, authStateBean);
//        }

        boolean isAuthenticated = false;
        if(authStateBean != null) {
            isAuthenticated = authStateBean.isAuthenticated();
        }
        
        if(isAuthenticated) {
            // Continue through filter chain.
            chain.doFilter(req, res);
            return;
        } else {
            // Redirect to the login page. ???
            
            String loginUrl = null;
            if(authStateBean != null) {
                loginUrl = authStateBean.getLoginUrl();
            }

            if(loginUrl != null) {
                // ????
                ((HttpServletResponse) res).sendRedirect(loginUrl);
            } else {
                config.getServletContext().getRequestDispatcher("/error/Unauthorized").forward(req, res);
            }
        }

    }
    
}
