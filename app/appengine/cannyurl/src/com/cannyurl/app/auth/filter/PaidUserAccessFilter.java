package com.cannyurl.app.auth.filter;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

// import com.cannyurl.util.UserUsercodeUtil;


// 3/03/13:
//...


public class PaidUserAccessFilter implements Filter
{
    private static final Logger log = Logger.getLogger(PaidUserAccessFilter.class.getName());

    // TBD
    private FilterConfig config = null;

    public PaidUserAccessFilter()
    {
    }

    @Override
    public void destroy()
    {
        this.config = null;
    }

    @Override
    public void init(FilterConfig config) throws ServletException
    {
        this.config = config;
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res,
            FilterChain chain) throws IOException, ServletException
    {
        // No access
        boolean accessAllowed = false;
        
        // TBD:
        // This work only for the request: /post/user/<usercode>/edit...
        // String usercode = UserUsercodeUtil.getUsercodeFromPathInfo(((HttpServletRequest) req).getPathInfo());
        String usercode = null;
        // ....
        boolean isUsercodeValid = PaidUserRegistry.getInstance().isUsercodeValid(usercode);
        if(isUsercodeValid) {
        	accessAllowed = true;
        }
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "PaidUserAccessFilter.doFilter(): accessAllowed = " + accessAllowed);
        
        if(accessAllowed) {
            // Continue through filter chain.
            chain.doFilter(req, res);        	
        } else {
            // TBD
            // Redirect to the error or login page. ???
            //config.getServletContext().getRequestDispatcher("/").forward(req, res);        	
            //config.getServletContext().getRequestDispatcher("/error/Default404").forward(req, res);
            config.getServletContext().getRequestDispatcher("/error/Unauthorized").forward(req, res);
            // ????
        }
    }
    
}
