package com.cannyurl.app.auth.filter;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import com.cannyurl.wa.service.UserWebService;


// 3/03/13:
//...


// Temporary
public class PaidUserRegistry
{
    private static final Logger log = Logger.getLogger(PaidUserRegistry.class.getName());

    private UserWebService userWebService = null;
//    private RecordedMessageWebService recordedMessageWebService = null;
    // ...

    
    // temporary
    private Map<String,String> mUsers = null;
    // ...
    
    
    private PaidUserRegistry() 
    {
    	// temporary
    	mUsers = new HashMap<String,String>();
    	// mUsers.put("anonymous", "Anonymous User");
    	// mUsers.put("guest", "Guest User");
    	// mUsers.put("harrywye", "Harry Yoon");
    	// mUsers.put("", "");
    	// ...
    }

    
    // Initialization-on-demand holder.
    private static final class PaidUserRegistryHolder
    {
        private static final PaidUserRegistry INSTANCE = new PaidUserRegistry();
    }

    // Singleton method
    public static PaidUserRegistry getInstance()
    {
        return PaidUserRegistryHolder.INSTANCE;
    }


    private UserWebService getUserWebService()
    {
        if(userWebService == null) {
            userWebService = new UserWebService();
        }
        return userWebService;
    }
//    private RecordedMessageWebService getRecordedMessageWebService()
//    {
//        if(recordedMessageWebService == null) {
//            recordedMessageWebService = new RecordedMessageWebService();
//        }
//        return recordedMessageWebService;
//    }
    
    
    public boolean isUsercodeValid(String usercode)
    {
    	if(usercode == null || usercode.isEmpty()) {
    		return false;
    	}
    	if(mUsers.keySet().contains(usercode.toLowerCase())) {
    		return true;
    	} else {
    		return false;
    	}    	
    }

    
}
