package com.cannyurl.app.core;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;


// Note: App.core package comes *before* app.service package.
//       Therefore, classes in core package should directly use af.service.impl.
public class CustomDomainMap
{
    private static final Logger log = Logger.getLogger(CustomDomainMap.class.getName());

    // Three types of mappings...
    // System-level:  long url/domain -> custom domain
    // App-level:     appId -> (long url/domain) -> custom domain
    // User-level:    userId -> custom domain.
    
    private final Map<String, String> systemDomainMap;
    private final Map<String, Map<String, String>> appDomainMap;
    private final Map<String, String> userDomainMap;
    
    
    private CustomDomainMap()
    {
        // ...
        systemDomainMap = new HashMap<String, String>();
        appDomainMap = new HashMap<String, Map<String, String>>();
        userDomainMap = new HashMap<String, String>();
        
        // TBD:
        // Just use memcache ???
        // ...
        
        
    }

    // Initialization-on-demand holder.
    private static class CustomDomainMapHolder
    {
        private static final CustomDomainMap INSTANCE = new CustomDomainMap();
    }

    // Singleton method
    public static CustomDomainMap getInstance()
    {
        return CustomDomainMapHolder.INSTANCE;
    }

    
    
    // ...
    public String getCustomDomain(String siteDomain)
    {
        // TBD
        return null;
    }
    
    // ...
    
    
}
