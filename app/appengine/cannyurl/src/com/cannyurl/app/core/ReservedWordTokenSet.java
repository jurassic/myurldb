package com.cannyurl.app.core;

import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;


// TBD:
// Use DB or flat file ????
// Note:
// Moved to ReservedWorkRegistry....
// ...
@Deprecated
public class ReservedWordTokenSet
{
    private static final Logger log = Logger.getLogger(ReservedWordTokenSet.class.getName());

    private final Set<String> sReservedWords;
    
    private ReservedWordTokenSet()
    {
        sReservedWords = new HashSet<String>();
        // ...
        // TBD:
        // Use memcache ???
        // ...
        
        // temporary
        sReservedWords.add("home");
        sReservedWords.add("about");
        sReservedWords.add("contact");
        sReservedWords.add("blog");
        sReservedWords.add("help");
        sReservedWords.add("faq");
        sReservedWords.add("option");
        sReservedWords.add("feedback");
        sReservedWords.add("comment");
        sReservedWords.add("auth");
        sReservedWords.add("login");
        sReservedWords.add("logout");
        sReservedWords.add("new");
        sReservedWords.add("edit");
        sReservedWords.add("shorten");
        sReservedWords.add("info");
        sReservedWords.add("user");
        sReservedWords.add("list");
        sReservedWords.add("verify");
        sReservedWords.add("view");
        sReservedWords.add("confirm");
        sReservedWords.add("flash");
        sReservedWords.add("permanent");
        sReservedWords.add("temporary");
        sReservedWords.add("domain");
        sReservedWords.add("keyword");
        sReservedWords.add("bookmark");
        sReservedWords.add("share");
        sReservedWords.add("tweet");
        sReservedWords.add("twitter");
        sReservedWords.add("facebook");
        sReservedWords.add("google");
        sReservedWords.add("linkedin");
        // etc..
        // ...
        
    }

    // Initialization-on-demand holder.
    private static class ReservedWordTokenSetHolder
    {
        private static final ReservedWordTokenSet INSTANCE = new ReservedWordTokenSet();
    }

    // Singleton method
    public static ReservedWordTokenSet getInstance()
    {
        return ReservedWordTokenSetHolder.INSTANCE;
    }
    
    // ...
    public boolean isRservedWord(String token)
    {
        return sReservedWords.contains(token); 
    }

    // TBD:
    // Allow adding words while the instance is running ???
    // Note that there is no persistent storage, and hence this is only temporary...
    // Should not be really used, in general.
    // The only (semi-legit) use case is, populate it whenever the instance is started...
    public boolean addReservedWord(String token)
    {
        return sReservedWords.add(token);
    }
    public boolean addReservedWords(Set<String> tokens)
    {
        return sReservedWords.addAll(tokens);
    }
    
    
}
