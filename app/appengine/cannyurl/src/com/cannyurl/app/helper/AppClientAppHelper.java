package com.cannyurl.app.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.sf.jsr107cache.Cache;

import com.cannyurl.af.util.CacheHelper;
import com.cannyurl.app.service.AppClientAppService;
import com.cannyurl.app.service.UserAppService;
import com.myurldb.ws.AppClient;
import com.myurldb.ws.BaseException;



// TBD::::
// Need a way to look up shortUrl
//    first look up appClient
//    if not found, then look up shortLink
// ....

public class AppClientAppHelper
{
    private static final Logger log = Logger.getLogger(AppClientAppHelper.class.getName());

    private UserAppService userAppService = null;
    private AppClientAppService appClientAppService = null;
    // ...

    private AppClientAppHelper() {}

    // Initialization-on-demand holder.
    private static final class AppClientAppHelperHolder
    {
        private static final AppClientAppHelper INSTANCE = new AppClientAppHelper();
    }

    // Singleton method
    public static AppClientAppHelper getInstance()
    {
        return AppClientAppHelperHolder.INSTANCE;
    }


    // temporary
    private static Cache getCache()
    {
        return CacheHelper.getLongInstance().getCache();
    }


    private UserAppService getUserAppService()
    {
        if(userAppService == null) {
            userAppService = new UserAppService();
        }
        return userAppService;
    }
    private AppClientAppService getAppClientAppService()
    {
        if(appClientAppService == null) {
            appClientAppService = new AppClientAppService();
        }
        return appClientAppService;
    }

    
    public AppClient getAppClient(String guid)
    {
        AppClient appClientBean = null;
        try {
            if(getCache() != null) {
                appClientBean = (AppClient) getCache().get(guid);
            }
            if(appClientBean == null) {
                appClientBean = getAppClientAppService().getAppClient(guid);
                if(appClientBean != null) {
                    if(getCache() != null) {
                        getCache().put(guid, appClientBean);
                    }
                }
            }
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to find an appClient for given guid = " + guid, e);
            return null;
        } catch (Exception e) {
            log.log(Level.WARNING, "Unexpected error: Failed to find a appClient for given guid = " + guid, e);
            return null;
        }

        return appClientBean;
    }


    public String findAppClientKeyByClientId(String clientId)
    {
        // TBD
        String key = null;
        try {
            String filter = "clientId=='" + clientId + "'";
            // String ordering = "createdTime desc";    // This is only useful when we have more than one appClient for given clientId, but it's very unlikely...
            String ordering = null;
            // TBD: Since we are doing full fetch, we should really fetch keys only...
            List<String> keys = getAppClientAppService().findAppClientKeys(filter, ordering, null, null, null, null, 0L, 2);
            if(keys != null && !keys.isEmpty()) {
                if(keys.size() > 1) {
                    log.severe("More than one appClient found for clientId = " + clientId);
                    // What to do ????
                    // Continue or throw error....
                    // We do NOT want to use an incorrect appClient... so, just bail out....
                    return null;
                }
                // Just the first one.... if size > 1...
                key = keys.get(0);
            } else {
                if(log.isLoggable(Level.INFO)) log.info("Failed to find appClient for clientId = " + clientId);
            }
        } catch (BaseException e) {
            log.log(Level.WARNING, "findAppClientKeys() failed for clientId = " + clientId, e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Unexpected error: findAppClientKeys() failed for clientId = " + clientId, e);
        }
        return key;
    }
    public AppClient findAppClientByClientId(String clientId)
    {
        // TBD
        AppClient appClient = null;
        try {
            String filter = "clientId=='" + clientId + "'";
            // String ordering = "createdTime desc";    // This is only useful when we have more than one appClient for given clientId, but it's very unlikely...
            String ordering = null;
            // TBD: Since we are doing full fetch, we should really fetch keys only...
            List<AppClient> beans = getAppClientAppService().findAppClients(filter, ordering, null, null, null, null, 0L, 2);
            if(beans != null && !beans.isEmpty()) {
                if(beans.size() > 1) {
                    log.severe("More than one appClient found for clientId = " + clientId);
                    // What to do ????
                    // Continue or throw error....
                    // We do NOT want to use an incorrect appClient... so, just bail out....
                    return null;
                }
                // Just the first one.... if size > 1...
                String guid = beans.get(0).getGuid();
                // Full fetch...
                appClient = getAppClient(guid);
            } else {
                if(log.isLoggable(Level.INFO)) log.info("Failed to find appClient for clientId = " + clientId);
            }
        } catch (BaseException e) {
            log.log(Level.WARNING, "findAppClients() failed for clientId = " + clientId, e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Unexpected error: findAppClients() failed for clientId = " + clientId, e);
        }
        return appClient;
    }

    public String findAppClientKeyByClientCode(String clientCode)
    {
        // TBD
        String key = null;
        try {
            String filter = "clientCode=='" + clientCode + "'";
            // String ordering = "createdTime desc";    // This is only useful when we have more than one appClient for given clientCode, but it's very unlikely...
            String ordering = null;
            // TBD: Since we are doing full fetch, we should really fetch keys only...
            List<String> keys = getAppClientAppService().findAppClientKeys(filter, ordering, null, null, null, null, 0L, 2);
            if(keys != null && !keys.isEmpty()) {
                if(keys.size() > 1) {
                    log.severe("More than one appClient found for clientCode = " + clientCode);
                    // What to do ????
                    // Continue or throw error....
                    // We do NOT want to use an incorrect appClient... so, just bail out....
                    return null;
                }
                // Just the first one.... if size > 1...
                key = keys.get(0);
            } else {
                if(log.isLoggable(Level.INFO)) log.info("Failed to find appClient for clientCode = " + clientCode);
            }
        } catch (BaseException e) {
            log.log(Level.WARNING, "findAppClientKeys() failed for clientCode = " + clientCode, e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Unexpected error: findAppClientKeys() failed for clientCode = " + clientCode, e);
        }
        return key;
    }
    public AppClient findAppClientByClientCode(String clientCode)
    {
        // TBD
        AppClient appClient = null;
        try {
            String filter = "clientCode=='" + clientCode + "'";
            // String ordering = "createdTime desc";    // This is only useful when we have more than one appClient for given clientCode, but it's very unlikely...
            String ordering = null;
            // TBD: Since we are doing full fetch, we should really fetch keys only...
            List<AppClient> beans = getAppClientAppService().findAppClients(filter, ordering, null, null, null, null, 0L, 2);
            if(beans != null && !beans.isEmpty()) {
                if(beans.size() > 1) {
                    log.severe("More than one appClient found for clientCode = " + clientCode);
                    // What to do ????
                    // Continue or throw error....
                    // We do NOT want to use an incorrect appClient... so, just bail out....
                    return null;
                }
                // Just the first one.... if size > 1...
                String guid = beans.get(0).getGuid();
                // Full fetch...
                appClient = getAppClient(guid);
            } else {
                if(log.isLoggable(Level.INFO)) log.info("Failed to find appClient for clientCode = " + clientCode);
            }
        } catch (BaseException e) {
            log.log(Level.WARNING, "findAppClients() failed for clientCode = " + clientCode, e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Unexpected error: findAppClients() failed for clientCode = " + clientCode, e);
        }
        return appClient;
    }


    public String findAppClientKeyByRootDomain(String rootDomain)
    {
        // TBD
        String key = null;
        try {
            String filter = "rootDomain=='" + rootDomain + "'";
            // String ordering = "createdTime desc";    // This is only useful when we have more than one appClient for given rootDomain, but it's very unlikely...
            String ordering = null;
            // TBD: Since we are doing full fetch, we should really fetch keys only...
            List<String> keys = getAppClientAppService().findAppClientKeys(filter, ordering, null, null, null, null, 0L, 2);
            if(keys != null && !keys.isEmpty()) {
                if(keys.size() > 1) {
                    log.severe("More than one appClient found for rootDomain = " + rootDomain);
                    // What to do ????
                    // Continue or throw error....
                    // We do NOT want to use an incorrect appClient... so, just bail out....
                    return null;
                }
                // Just the first one.... if size > 1...
                key = keys.get(0);
            } else {
                if(log.isLoggable(Level.INFO)) log.info("Failed to find appClient for rootDomain = " + rootDomain);
            }
        } catch (BaseException e) {
            log.log(Level.WARNING, "findAppClientKeys() failed for rootDomain = " + rootDomain, e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Unexpected error: findAppClientKeys() failed for rootDomain = " + rootDomain, e);
        }
        return key;
    }
    public AppClient findAppClientByRootDomain(String rootDomain)
    {
        // TBD
        AppClient appClient = null;
        try {
            String filter = "rootDomain=='" + rootDomain + "'";
            // String ordering = "createdTime desc";    // This is only useful when we have more than one appClient for given rootDomain, but it's very unlikely...
            String ordering = null;
            // TBD: Since we are doing full fetch, we should really fetch keys only...
            List<AppClient> beans = getAppClientAppService().findAppClients(filter, ordering, null, null, null, null, 0L, 2);
            if(beans != null && !beans.isEmpty()) {
                if(beans.size() > 1) {
                    log.severe("More than one appClient found for rootDomain = " + rootDomain);
                    // What to do ????
                    // Continue or throw error....
                    // We do NOT want to use an incorrect appClient... so, just bail out....
                    return null;
                }
                // Just the first one.... if size > 1...
                String guid = beans.get(0).getGuid();
                // Full fetch...
                appClient = getAppClient(guid);
            } else {
                if(log.isLoggable(Level.INFO)) log.info("Failed to find appClient for rootDomain = " + rootDomain);
            }
        } catch (BaseException e) {
            log.log(Level.WARNING, "findAppClients() failed for rootDomain = " + rootDomain, e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Unexpected error: findAppClients() failed for rootDomain = " + rootDomain, e);
        }
        return appClient;
    }


    public String findAppClientKeyByAppDomain(String appDomain)
    {
        // TBD
        String key = null;
        try {
            String filter = "appDomain=='" + appDomain + "'";
            // String ordering = "createdTime desc";    // This is only useful when we have more than one appClient for given appDomain, but it's very unlikely...
            String ordering = null;
            // TBD: Since we are doing full fetch, we should really fetch keys only...
            List<String> keys = getAppClientAppService().findAppClientKeys(filter, ordering, null, null, null, null, 0L, 2);
            if(keys != null && !keys.isEmpty()) {
                if(keys.size() > 1) {
                    log.severe("More than one appClient found for appDomain = " + appDomain);
                    // What to do ????
                    // Continue or throw error....
                    // We do NOT want to use an incorrect appClient... so, just bail out....
                    return null;
                }
                // Just the first one.... if size > 1...
                key = keys.get(0);
            } else {
                if(log.isLoggable(Level.INFO)) log.info("Failed to find appClient for appDomain = " + appDomain);
            }
        } catch (BaseException e) {
            log.log(Level.WARNING, "findAppClientKeys() failed for appDomain = " + appDomain, e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Unexpected error: findAppClientKeys() failed for appDomain = " + appDomain, e);
        }
        return key;
    }
    public AppClient findAppClientByAppDomain(String appDomain)
    {
        // TBD
        AppClient appClient = null;
        try {
            String filter = "appDomain=='" + appDomain + "'";
            // String ordering = "createdTime desc";    // This is only useful when we have more than one appClient for given appDomain, but it's very unlikely...
            String ordering = null;
            // TBD: Since we are doing full fetch, we should really fetch keys only...
            List<AppClient> beans = getAppClientAppService().findAppClients(filter, ordering, null, null, null, null, 0L, 2);
            if(beans != null && !beans.isEmpty()) {
                if(beans.size() > 1) {
                    log.severe("More than one appClient found for appDomain = " + appDomain);
                    // What to do ????
                    // Continue or throw error....
                    // We do NOT want to use an incorrect appClient... so, just bail out....
                    return null;
                }
                // Just the first one.... if size > 1...
                String guid = beans.get(0).getGuid();
                // Full fetch...
                appClient = getAppClient(guid);
            } else {
                if(log.isLoggable(Level.INFO)) log.info("Failed to find appClient for appDomain = " + appDomain);
            }
        } catch (BaseException e) {
            log.log(Level.WARNING, "findAppClients() failed for appDomain = " + appDomain, e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Unexpected error: findAppClients() failed for appDomain = " + appDomain, e);
        }
        return appClient;
    }

    
}
