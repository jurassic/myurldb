package com.cannyurl.app.helper;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cannyurl.af.bean.ShortLinkBean;
import com.cannyurl.af.service.KeywordLinkService;
import com.cannyurl.af.service.manager.ServiceManager;
import com.cannyurl.app.service.KeywordLinkAppService;
import com.cannyurl.app.service.ShortLinkAppService;
import com.cannyurl.app.service.UserAppService;
import com.cannyurl.app.util.ShortLinkUtil;
import com.myurldb.ws.BaseException;
import com.myurldb.ws.KeywordLink;



// TBD::::
// Need a way to look up shortUrl
//    first look up keywordLink
//    if not found, then look up shortLink
// ....

public class KeywordLinkAppHelper
{
    private static final Logger log = Logger.getLogger(KeywordLinkAppHelper.class.getName());

    private UserAppService userAppService = null;
    private KeywordLinkAppService keywordLinkAppService = null;
    private ShortLinkAppService shortLinkAppService = null;
    // ...

    private KeywordLinkAppHelper() {}

    // Initialization-on-demand holder.
    private static final class KeywordLinkHelperHolder
    {
        private static final KeywordLinkAppHelper INSTANCE = new KeywordLinkAppHelper();
    }

    // Singleton method
    public static KeywordLinkAppHelper getInstance()
    {
        return KeywordLinkHelperHolder.INSTANCE;
    }
    
    
    private UserAppService getUserAppService()
    {
        if(userAppService == null) {
            userAppService = new UserAppService();
        }
        return userAppService;
    }
    private KeywordLinkAppService getKeywordLinkAppService()
    {
        if(keywordLinkAppService == null) {
            keywordLinkAppService = new KeywordLinkAppService();
        }
        return keywordLinkAppService;
    }
    private ShortLinkAppService getShortLinkAppService()
    {
        if(shortLinkAppService == null) {
            shortLinkAppService = new ShortLinkAppService();
        }
        return shortLinkAppService;
    }
    
    
    // TBD
    public boolean createShortLink(ShortLinkBean shortLinkBean)
    {
        // TBD
        try {
            getShortLinkAppService().createShortLink(shortLinkBean);
            return true;
        } catch (BaseException e) {
            // ignore.
            log.log(Level.WARNING, "Failed to create ShortLink.", e);
        }

        // TBD
        return false;
    }

    
    public KeywordLink getKeywordLink(String guid)
    {
        KeywordLink keywordLinkBean = null;
        try {
            keywordLinkBean = getKeywordLinkAppService().getKeywordLink(guid);
        } catch (BaseException e) {
            log.log(Level.SEVERE, "Failed to find a keywordLink for given guid = " + guid, e);
            return null;
        } catch (Exception e) {
            log.log(Level.SEVERE, "Failed to find a keywordLink for given guid = " + guid, e);
            return null;
        }

        return keywordLinkBean;
    }
    
    public String getRedirectUrl(String longUrl, String queryString)
    {
        if(longUrl == null || queryString == null || queryString.trim().isEmpty()) {
            return longUrl;
        }

        // temporary
        String fullRedirectUrl = longUrl;
        // if the longUrl already includes "?", use "&"....
        if(longUrl.indexOf("?") > 0) {
            fullRedirectUrl = longUrl + "&" + queryString.trim();
        } else {
            fullRedirectUrl = longUrl + "?" + queryString.trim();
        }

        log.log(Level.INFO, "getRedirectUrl(): fullRedirectUrl = " + fullRedirectUrl);
        return fullRedirectUrl;
    }


    // TBD
    // (What happens if IP address is used ????)
    public KeywordLink findKeywordLink(String shortUrl)
    {
        String[] urlParts = ShortLinkUtil.parseFullUrl(shortUrl);
        if(urlParts == null) {
            log.log(Level.WARNING, "Url arg is null or invalid."); 
            return null;
        }

        KeywordLink keywordLinkBean = null;
        try {
            String domain = urlParts[0];
            String path = urlParts[1];
            String token = urlParts[2];

            List<KeywordLink> keywordLinks = null;
            String filter = "domain=='" + domain + "' && token=='" + token + "'";
            // TBD: Path should not be used in finding keywordLink...
            //      Path can be used to modify access/view type in certain cases....
            //if(path != null && path.length() > 0) {
            //    filter += " && path=='" + path + "'";
            //}
            String ordering = "createdTime desc";

            keywordLinks = getKeywordLinkAppService().findKeywordLinks(filter, ordering, null, null);    
            if(keywordLinks != null && keywordLinks.size() > 0) {
                keywordLinkBean = keywordLinks.get(0);
                // For debugging/diagnostic purposes.
                if(keywordLinks.size() > 1) { 
                    // This should not happen.
                    log.log(Level.SEVERE, "There are more than one keywordLink with the same url = " + shortUrl + ". Needs further investigation!");
                }
            } else {
                log.log(Level.WARNING, "KeywordLink does not exist with given url = " + shortUrl);                
            }
        } catch (BaseException e) {
            log.log(Level.SEVERE, "Failed to find a keywordLink for given url = " + shortUrl, e);
            return null;
        } catch (Exception e) {
            log.log(Level.SEVERE, "Failed to find a keywordLink for given url = " + shortUrl, e);
            return null;
        }

        log.log(Level.INFO, "findKeywordLink(): keywordLinkBean = " + keywordLinkBean);
        return keywordLinkBean;
    }

    
    
    // TBD:
    //  addPassphrase(String keywordLink, String passphrase)
    //  deletePassphrase(String keywordLink, String passphrase)
    // ...
    
    
    
    
    // TBD:
    // This vs. helper.ShortUrlAccessHelper
    // ...

    
    
    // ...
    public List<KeywordLink> findKeywordLinksByLongUrl(String longUrl, String owner)
    {
        return findKeywordLinksByLongUrl(longUrl, owner, null);
    }
    public List<KeywordLink> findKeywordLinksByLongUrl(String longUrl, String owner, Integer count)
    {
        // TBD
        String filter = "longUrl == longUrlParam";
        if(owner != null) {
            filter += " && owner == ownerParam";
        }
        String ordering = "createdTime desc";
        String params = "String longUrlParam";
        if(owner != null) {
            params += ", String ownerParam";
        }
        List<String> values = new ArrayList<String>();
        values.add(longUrl);
        if(owner != null) {
            values.add(owner);
        }
        
        KeywordLinkService service = ServiceManager.getKeywordLinkService();

        List<KeywordLink> beans = null;
        try {
            beans = service.findKeywordLinks(filter, ordering, params, values, null, null, 0L, count);
        } catch (BaseException e) {
            log.log(Level.WARNING, "findKeywordLinks() failed.", e);
        }
        return beans;
    }

    public List<KeywordLink> findKeywordLinksByToken(String token, String domain)
    {
        return findKeywordLinksByToken(token, domain, null);
    }
    public List<KeywordLink> findKeywordLinksByToken(String token, String domain, Integer count)
    {
        // TBD
        String filter = "token == tokenParam";
        if(domain != null) {
            filter += " && domain == domainParam";
        }
        String ordering = "createdTime desc";
        String params = "String tokenParam";
        if(domain != null) {
            params += ", String domainParam";
        }
        List<String> values = new ArrayList<String>();
        values.add(token);
        if(domain != null) {
            values.add(domain);
        }
        
        KeywordLinkService service = ServiceManager.getKeywordLinkService();

        List<KeywordLink> beans = null;
        try {
            beans = service.findKeywordLinks(filter, ordering, params, values, null, null, 0L, count);
        } catch (BaseException e) {
            log.log(Level.WARNING, "findKeywordLinks() failed.", e);
        }
        return beans;
    }

    
    
}
