package com.cannyurl.app.helper;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cannyurl.af.bean.ShortLinkBean;
import com.cannyurl.af.service.ShortLinkService;
import com.cannyurl.af.service.manager.ServiceManager;
import com.cannyurl.app.service.ShortLinkAppService;
import com.cannyurl.app.service.UserAppService;
import com.cannyurl.app.util.ConfigUtil;
import com.cannyurl.app.util.ShortLinkUtil;
import com.cannyurl.common.RedirectType;
import com.cannyurl.common.TokenType;
import com.myurldb.ws.BaseException;
import com.myurldb.ws.CellLatitudeLongitude;
import com.myurldb.ws.ShortLink;



// TBD::::
public class ShortLinkAppHelper
{
    private static final Logger log = Logger.getLogger(ShortLinkAppHelper.class.getName());

    private UserAppService userAppService = null;
    private ShortLinkAppService shortLinkAppService = null;
//    private AccessRecordAppService accessRecordAppService = null;
    // ...

    private ShortLinkAppHelper() {}

    // Initialization-on-demand holder.
    private static final class ShortLinkHelperHolder
    {
        private static final ShortLinkAppHelper INSTANCE = new ShortLinkAppHelper();
    }

    // Singleton method
    public static ShortLinkAppHelper getInstance()
    {
        return ShortLinkHelperHolder.INSTANCE;
    }
    
    
    private UserAppService getUserAppService()
    {
        if(userAppService == null) {
            userAppService = new UserAppService();
        }
        return userAppService;
    }
    private ShortLinkAppService getShortLinkAppService()
    {
        if(shortLinkAppService == null) {
            shortLinkAppService = new ShortLinkAppService();
        }
        return shortLinkAppService;
    }
//    private AccessRecordAppService getAccessRecordAppService()
//    {
//        if(accessRecordAppService == null) {
//            accessRecordAppService = new AccessRecordAppService();
//        }
//        return accessRecordAppService;
//    }
    
    
//    // TBD
//    public boolean createAccessRecord(AccessRecordBean accessRecordBean)
//    {
//        // TBD
//        try {
//            getAccessRecordAppService().createAccessRecord(accessRecordBean);
//            return true;
//        } catch (BaseException e) {
//            // ignore.
//            log.log(Level.WARNING, "Failed to create AccessRecord.", e);
//        }
//
//        // TBD
//        return false;
//    }

    
    public ShortLink getShortLink(String guid)
    {
        ShortLink shortLinkBean = null;
        
        if(getShortLinkAppService().getCache() != null) {
            shortLinkBean = (ShortLink) getShortLinkAppService().getCache().get(guid);
            if(shortLinkBean != null) {
                if(log.isLoggable(Level.INFO)) log.info("shortLinkBean retreived from cache: guid = " + guid);
            }
        }

        if(shortLinkBean == null) {
            try {
                shortLinkBean = getShortLinkAppService().getShortLink(guid);
                if(getShortLinkAppService().getCache() != null) {
                    if(shortLinkBean != null) {
                        getShortLinkAppService().getCache().put(guid, shortLinkBean);
                    }
                }           
            } catch (BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to find a shortLink for given guid = " + guid, e);
                return null;
            } catch (Exception e) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to find a shortLink due to unknown error: guid = " + guid, e);
                return null;
            }
        }

        return shortLinkBean;
    }


    public String getRedirectUrl(String longUrl, String queryString)
    {
        if(longUrl == null || queryString == null || queryString.trim().isEmpty()) {
            return longUrl;
        }

        // temporary
        String fullRedirectUrl = longUrl;
        // if the longUrl already includes "?", use "&"....
        if(longUrl.indexOf("?") > 0) {
            fullRedirectUrl = longUrl + "&" + queryString.trim();
        } else {
            fullRedirectUrl = longUrl + "?" + queryString.trim();
        }

        log.log(Level.INFO, "getRedirectUrl(): fullRedirectUrl = " + fullRedirectUrl);
        return fullRedirectUrl;
    }

    public String getRedirectType(ShortLinkBean linkBean, String shortUrl)
    {
        // TBD
        String redirectType = linkBean.getRedirectType();
        if(redirectType == null || redirectType.isEmpty()) {
            // TBD: Use user settings if available.
            redirectType = ConfigUtil.getSystemDefaultRedirectType();
        }
        if(shortUrl != null) {
            String[] urlParts = ShortLinkUtil.parseFullUrl(shortUrl);
            if(urlParts != null) {
                String path = urlParts[1];
                if(path != null && !path.isEmpty()) {
                    // temporary
//                    if(redirectType.equals(RedirectType.TYPE_PERMANENT) || redirectType.equals(RedirectType.TYPE_TEMPORARY)) {
//                        if(path.equals("c")) {
//                            redirectType = RedirectType.TYPE_CONFIRM;
//                        } else if(path.equals("f")) {
//                            redirectType = RedirectType.TYPE_FLASH;
//                        } else if(path.equals("a")) {
//                            redirectType = RedirectType.TYPE_PERMANENT;
//                        } else {
//                            // ignore
//                        }
//                    } else if(redirectType.equals(RedirectType.TYPE_CONFIRM) || redirectType.equals(RedirectType.TYPE_FLASH)) {
//                        if(path.equals("c")) {
//                            redirectType = RedirectType.TYPE_CONFIRM;
//                        } else if(path.equals("f")) {
//                            redirectType = RedirectType.TYPE_FLASH;
//                        } else {
//                            // ignore
//                        }
//                    } else {
//                        // Do nothing.
//                    }

                    // temporary
                    // For now, a valid path always overwrites the default behavior. 
                    if(path.equals("c")) {
                        redirectType = RedirectType.TYPE_CONFIRM;
                    } else if(path.equals("f")) {
                        redirectType = RedirectType.TYPE_FLASH;
                    } else if(path.equals("a")) {
                        redirectType = RedirectType.TYPE_PERMANENT;
                    } else if(path.equals("o")) {
                        redirectType = RedirectType.TYPE_TEMPORARY;
                    } else {
                        // ignore
                    }
                    // temporary
                    
                }                
            }
        }

        log.log(Level.INFO, "getRedirectType(): redirectType = " + redirectType);
        return redirectType;
    }

    // TBD
    // (What happens if IP address is used ????)
    public ShortLink findShortLink(String shortUrl)
    {
        String[] urlParts = ShortLinkUtil.parseFullUrl(shortUrl);
        if(urlParts == null) {
            log.log(Level.WARNING, "Url arg is null or invalid."); 
            return null;
        }

        ShortLink shortLinkBean = null;
        try {
            String domain = urlParts[0];
            String path = urlParts[1];
            String token = urlParts[2];

            List<ShortLink> shortLinks = null;
            String filter = "domain=='" + domain + "' && token=='" + token + "'";
            // TBD: Path should not be used in finding shortLink...
            //      Path can be used to modify access/view type in certain cases....
            //if(path != null && path.length() > 0) {
            //    filter += " && path=='" + path + "'";
            //}
            String ordering = "createdTime desc";

            shortLinks = getShortLinkAppService().findShortLinks(filter, ordering, null, null);    
            if(shortLinks != null && shortLinks.size() > 0) {
                shortLinkBean = shortLinks.get(0);
                // For debugging/diagnostic purposes.
                if(shortLinks.size() > 1) { 
                    // This should not happen.
                    log.log(Level.SEVERE, "There are more than one shortLink with the same url = " + shortUrl + ". Needs further investigation!");
                }
            } else {
                log.log(Level.WARNING, "ShortLink does not exist with given url = " + shortUrl);                
            }
        } catch (BaseException e) {
            log.log(Level.SEVERE, "Failed to find a shortLink for given url = " + shortUrl, e);
            return null;
        } catch (Exception e) {
            log.log(Level.SEVERE, "Failed to find a shortLink for given url = " + shortUrl, e);
            return null;
        }

        log.log(Level.INFO, "findShortLink(): shortLinkBean = " + shortLinkBean);
        return shortLinkBean;
    }

    
    
    // TBD:
    //  addPassphrase(String shortLink, String passphrase)
    //  deletePassphrase(String shortLink, String passphrase)
    // ...
    
    
    
    
    // TBD:
    // This vs. helper.ShortUrlAccessHelper
    // ...

    
    
    // ...
    public List<ShortLink> findShortLinksByLongUrl(String longUrl, String owner)
    {
        return findShortLinksByLongUrl(longUrl, owner, null);
    }
    public List<ShortLink> findShortLinksByLongUrl(String longUrl, String owner, Integer count)
    {
        // TBD
        String filter = "longUrl == longUrlParam";
        if(owner != null) {
            filter += " && owner == ownerParam";
        }
        String ordering = "createdTime desc";
        String params = "String longUrlParam";
        if(owner != null) {
            params += ", String ownerParam";
        }
        List<String> values = new ArrayList<String>();
        values.add(longUrl);
        if(owner != null) {
            values.add(owner);
        }
        
        ShortLinkService service = ServiceManager.getShortLinkService();

        List<ShortLink> beans = null;
        try {
            beans = service.findShortLinks(filter, ordering, params, values, null, null, 0L, count);
        } catch (BaseException e) {
            log.log(Level.WARNING, "findShortLinks() failed.", e);
        }
        return beans;
    }
    
    
    
    

    public List<ShortLink> findShortLinksByToken(String token, String domain)
    {
        return findShortLinksByToken(token, domain, null);
    }
    public List<ShortLink> findShortLinksByToken(String token, String domain, Integer count)
    {
        // TBD
        String filter = "token == tokenParam";
        if(domain != null) {
            filter += " && domain == domainParam";
        }
        String ordering = "createdTime desc";
        String params = "String tokenParam";
        if(domain != null) {
            params += ", String domainParam";
        }
        List<String> values = new ArrayList<String>();
        values.add(token);
        if(domain != null) {
            values.add(domain);
        }
        
        ShortLinkService service = ServiceManager.getShortLinkService();

        List<ShortLink> beans = null;
        try {
            beans = service.findShortLinks(filter, ordering, params, values, null, null, 0L, count);
        } catch (BaseException e) {
            log.log(Level.WARNING, "findShortLinks() failed.", e);
        }
        return beans;
    }

    
    
    // TBD:
    // Get shortLink list by user, appClient, etc.. ???
    // Cf. helper.ShortLinkListHelper...
    // ...
    
    

    // Note:
    // the following 3~4 methods throw BaseException unlike other methods in this class...
    // ...

    // Returns one most recently created shortlink, if any....
    public ShortLink findShortLinkByToken(String token, String domain) throws BaseException
    {
        // TBD
        String filter = "token == tokenParam";
        if(domain != null) {
            filter += " && domain == domainParam";
        }
        String ordering = "createdTime desc";
        String params = "String tokenParam";
        if(domain != null) {
            params += ", String domainParam";
        }
        List<String> values = new ArrayList<String>();
        values.add(token);
        if(domain != null) {
            values.add(domain);
        }
        
        ShortLinkService service = ServiceManager.getShortLinkService();

        ShortLink bean = null;
        List<ShortLink> beans = service.findShortLinks(filter, ordering, params, values, null, null, 0L, 2);
        if(beans != null && beans.size() > 0) {
            if(beans.size() > 1) {
                // BAD!!!!!  (note: we should take into account status/expiration time, etc....)
                log.severe("More than one ShortLinks have been found for token = " + token + " and domain = " + domain);
            }
            bean = beans.get(0);
            // TBD: Full fetch ???
        }
        return bean;
    }
    public String findShortLinkKeyByToken(String token, String domain) throws BaseException
    {
        // TBD
        String filter = "token == tokenParam";
        if(domain != null) {
            filter += " && domain == domainParam";
        }
        String ordering = "createdTime desc";
        String params = "String tokenParam";
        if(domain != null) {
            params += ", String domainParam";
        }
        List<String> values = new ArrayList<String>();
        values.add(token);
        if(domain != null) {
            values.add(domain);
        }
        
        ShortLinkService service = ServiceManager.getShortLinkService();

        String key = null;
        List<String> keys = service.findShortLinkKeys(filter, ordering, params, values, null, null, 0L, 2);
        if(keys != null && keys.size() > 0) {
            if(keys.size() > 1) {
                // BAD!!!!!  (note: we should take into account status/expiration time, etc....)
                log.severe("More than one ShortLinks have been found for token = " + token + " and domain = " + domain);
            }
            key = keys.get(0);
        }
        return key;
    }


    // Returns the "last" token in a sort order (alphabetical?)
    // This is to be used for sequential token generation only...
    public String findTheLastTokenForDomain(String domain) throws BaseException
//    {
//        return findTheLastTokenForDomain(domain, null);
//    }
//    public String findTheLastTokenForDomain(String domain, String tokenType) throws BaseException
    {
        String token = null;
        
        // try {
            String filter = "domain=='" + domain + "'";
//            if(tokenType != null && !tokenType.isEmpty()) {
//                filter += " && tokenType=='" + tokenType + "'";
//            }

            // Note:
            // We need to remove custom tokens when we generate sequential tokens....
            // filter += " && tokenType != '" + TokenType.TYPE_CUSTOM + "'";
            filter += " && tokenType=='" + TokenType.TYPE_SEQUENTIAL + "'";
            // ....

            // Unfortunately, "token desc" does not work...
            // (1) DB sort order/collation is different from the way we generate tokens...
            // (2) If user/system changes the tokenType, we may end up being stuck with the same "next token"...
            // String ordering = "token desc";   // What does this mean????
            // Instead, just use the last generated token....
            String ordering = "createdTime desc";
            List<ShortLink> beans = getShortLinkAppService().findShortLinks(filter, ordering, null, null, null, null, 0L, 1);
            if(beans != null && !beans.isEmpty()) {
                ShortLink shortLink = beans.get(0);   // No need for full fetch...
                token = shortLink.getToken();
            }
        // } catch (BaseException e) {
        //     // What to do ????
        //     if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to find a token for domain = " + domain + "; tokenType = " + tokenType);
        // } 
        
        return token;
    }

    
    

    public List<String> findShortLinkKeys(Long startTime)
    {
        return findShortLinkKeys(startTime, (Integer) null);
    }
    public List<String> findShortLinkKeys(Long startTime, Integer maxCount)
    {
        return findShortLinkKeys(null, startTime, null, maxCount);
    }
    public List<String> findShortLinkKeys(CellLatitudeLongitude geoCell, Long startTime, Integer maxCount)
    {
        return findShortLinkKeys(geoCell, startTime, null, maxCount);
    }
    public List<String> findShortLinkKeys(CellLatitudeLongitude geoCell, Long startTime, Long offset, Integer maxCount)
    {
        if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "BEGIN: startTime = " + startTime + "; offset = " + offset + "; maxCount = " + maxCount);
        List<String> shortLinkKeys = null;
        try {
            String filter = "createdTime>=" + startTime;  // Note ">=" not ">"...
            if(geoCell != null) {
                Integer scale = geoCell.getScale();
                Integer latitude = geoCell.getLatitude();
                Integer longitude = geoCell.getLongitude();
                if(scale != null && latitude != null && longitude != null) {
                    filter += " && geoCell.scale==" + scale;
                    filter += " && geoCell.latitude==" + latitude;
                    filter += " && geoCell.longitude==" + longitude;
                } else {
                    // ???
                    if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Input geoCell object is incomplete: geoCell = " + geoCell);
                }
            }
            String ordering = "createdTime asc";
            if(offset == null) {
                offset = 0L;
            }
            if(maxCount == null || maxCount == 0) {
                maxCount = 500;    // ????   Arbitrary cutoff just to be safe...
            }
            shortLinkKeys = getShortLinkAppService().findShortLinkKeys(filter, ordering, null, null, null, null, offset, maxCount);    
        } catch (BaseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to find shortLink keys for given startTime = " + startTime, e);
            return null;
        } catch (Exception e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to find shortLink keys due to unknown error: startTime = " + startTime, e);
            return null;
        }
        if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "END");
        return shortLinkKeys;
    }

    public List<ShortLink> findShortLinks(Long startTime)
    {
        return findShortLinks(startTime, (Integer) null);
    }
    public List<ShortLink> findShortLinks(Long startTime, Integer maxCount)
    {
        return findShortLinks(null, startTime, null, maxCount);
    }
    public List<ShortLink> findShortLinks(CellLatitudeLongitude geoCell, Long startTime, Integer maxCount)
    {
        return findShortLinks(geoCell, startTime, null, maxCount);
    }
    private List<ShortLink> findShortLinks(CellLatitudeLongitude geoCell, Long startTime, Long offset, Integer maxCount)
    {
        if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "BEGIN: startTime = " + startTime + "; offset = " + offset + "; maxCount = " + maxCount);
        List<ShortLink> shortLinks = null;
        try {
            String filter = "createdTime>=" + startTime;  // Note ">=" not ">"...
            if(geoCell != null) {
                Integer scale = geoCell.getScale();
                Integer latitude = geoCell.getLatitude();
                Integer longitude = geoCell.getLongitude();
                if(scale != null && latitude != null && longitude != null) {
                    filter += " && geoCell.scale==" + scale;
                    filter += " && geoCell.latitude==" + latitude;
                    filter += " && geoCell.longitude==" + longitude;
                } else {
                    // ???
                    if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Input geoCell object is incomplete: geoCell = " + geoCell);
                }
            }
            String ordering = "createdTime asc";
            if(offset == null) {
                offset = 0L;
            }
            if(maxCount == null || maxCount == 0) {
                maxCount = 500;    // ????   Arbitrary cutoff just to be safe...
            }
            shortLinks = getShortLinkAppService().findShortLinks(filter, ordering, null, null, null, null, offset, maxCount);    
        } catch (BaseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to find shortLinks for given startTime = " + startTime, e);
            return null;
        } catch (Exception e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to find shortLinks due to unknown error: startTime = " + startTime, e);
            return null;
        }
        if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "END");
        return shortLinks;
    }
    
    
    public List<String> findShortLinkKeys(Long startTime, Long endTime)
    {
        return findShortLinkKeys(null, startTime, endTime);
    }
    public List<String> findShortLinkKeys(CellLatitudeLongitude geoCell, Long startTime, Long endTime)
    {
        return findShortLinkKeys(geoCell, startTime, endTime, null, null);
    }
    public List<String> findShortLinkKeys(CellLatitudeLongitude geoCell, Long startTime, Long endTime, Long offset, Integer maxCount)
    {
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "BEGIN: startTime = " + startTime + "; endTime = " + endTime + "; offset = " + offset + "; maxCount = " + maxCount);
        List<String> shortLinkKeys = null;
        try {
            String filter = "createdTime>=" + startTime;  // Note ">=" not ">"...
            if(endTime != null && endTime > 0L) {
                filter += " && createdTime<" + endTime;   // Note "<" not "<="...
            }
            if(geoCell != null) {
                Integer scale = geoCell.getScale();
                Integer latitude = geoCell.getLatitude();
                Integer longitude = geoCell.getLongitude();
                if(scale != null && latitude != null && longitude != null) {
                    filter += " && geoCell.scale==" + scale;
                    filter += " && geoCell.latitude==" + latitude;
                    filter += " && geoCell.longitude==" + longitude;
                } else {
                    // ???
                    if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Input geoCell object is incomplete: geoCell = " + geoCell);
                }
            }
            String ordering = "createdTime asc";
            if(offset == null) {
                offset = 0L;
            }
            if(maxCount == null || maxCount == 0) {
                maxCount = 500;    // ????   Arbitrary cutoff just to be safe...
            }
            shortLinkKeys = getShortLinkAppService().findShortLinkKeys(filter, ordering, null, null, null, null, offset, maxCount);    
        } catch (BaseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to find shortLink keys for given startTime = " + startTime, e);
            return null;
        } catch (Exception e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to find shortLink keys due to unknown error: startTime = " + startTime, e);
            return null;
        }
        if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "END");
        return shortLinkKeys;
    }

    public List<ShortLink> findShortLinks(Long startTime, Long endTime)
    {
        return findShortLinks(null, startTime, endTime);
    }
    public List<ShortLink> findShortLinks(CellLatitudeLongitude geoCell, Long startTime, Long endTime)
    {
        return findShortLinks(geoCell, startTime, endTime, null, null);        
    }
    public List<ShortLink> findShortLinks(CellLatitudeLongitude geoCell, Long startTime, Long endTime, Long offset, Integer maxCount)
    {
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "BEGIN: startTime = " + startTime + "; endTime = " + endTime + "; offset = " + offset + "; maxCount = " + maxCount);
        List<ShortLink> shortLinks = null;
        try {
            String filter = "createdTime>=" + startTime;  // Note ">=" not ">"...
            if(endTime != null && endTime > 0L) {
                filter += " && createdTime<" + endTime;   // Note "<" not "<="...
            }
            if(geoCell != null) {
                Integer scale = geoCell.getScale();
                Integer latitude = geoCell.getLatitude();
                Integer longitude = geoCell.getLongitude();
                if(scale != null && latitude != null && longitude != null) {
                    filter += " && geoCell.scale==" + scale;
                    filter += " && geoCell.latitude==" + latitude;
                    filter += " && geoCell.longitude==" + longitude;
                } else {
                    // ???
                    if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Input geoCell object is incomplete: geoCell = " + geoCell);
                }
            }
            String ordering = "createdTime asc";
            if(offset == null) {
                offset = 0L;
            }
            if(maxCount == null || maxCount == 0) {
                maxCount = 500;    // ????   Arbitrary cutoff just to be safe...
            }
            shortLinks = getShortLinkAppService().findShortLinks(filter, ordering, null, null, null, null, offset, maxCount);    
        } catch (BaseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to find shortLinks for given startTime = " + startTime, e);
            return null;
        } catch (Exception e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to find shortLinks due to unknown error: startTime = " + startTime, e);
            return null;
        }
        if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "END");
        return shortLinks;
    }

    
    
    
    
}
