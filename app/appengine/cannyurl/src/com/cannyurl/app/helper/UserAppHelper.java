package com.cannyurl.app.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cannyurl.app.service.UserAppService;
import com.myurldb.ws.BaseException;
import com.myurldb.ws.CellLatitudeLongitude;
import com.myurldb.ws.User;
import com.myurldb.ws.core.GUID;



// TBD::::
public class UserAppHelper
{
    private static final Logger log = Logger.getLogger(UserAppHelper.class.getName());

    private UserAppService userAppService = null;
//    private AccessRecordAppService accessRecordAppService = null;
    // ...

    private UserAppHelper() {}

    // Initialization-on-demand holder.
    private static final class UserHelperHolder
    {
        private static final UserAppHelper INSTANCE = new UserAppHelper();
    }

    // Singleton method
    public static UserAppHelper getInstance()
    {
        return UserHelperHolder.INSTANCE;
    }
    
    
    private UserAppService getUserAppService()
    {
        if(userAppService == null) {
            userAppService = new UserAppService();
        }
        return userAppService;
    }

    
    public User getUser(String guid)
    {
        User userBean = null;
        
        // if(getUserAppService().getCache() != null && GUID.isValid(guid)) {   // For guid only, not key.... ????
        if(getUserAppService().getCache() != null) {
            userBean = (User) getUserAppService().getCache().get(guid);
            if(userBean != null) {
                if(log.isLoggable(Level.INFO)) log.info("userBean retreived from cache: guid = " + guid);
            }
        }

        if(userBean == null) {
            try {
                userBean = getUserAppService().getUser(guid);
                if(userBean != null) {
                    // if(getUserAppService().getCache() != null && GUID.isValid(guid)) {
                    if(getUserAppService().getCache() != null) {
                        getUserAppService().getCache().put(guid, userBean);
                    }
                }           
            } catch (BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to find a user for given guid = " + guid, e);
                return null;
            } catch (Exception e) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to find a user due to unknown error: guid = " + guid, e);
                return null;
            }
        }

        return userBean;
    }



    // TBD:


    
    
    
    

    public List<String> findUserKeys(Long startTime)
    {
        return findUserKeys(startTime, (Integer) null);
    }
    public List<String> findUserKeys(Long startTime, Integer maxCount)
    {
        return findUserKeys(null, startTime, null, maxCount);
    }
    public List<String> findUserKeys(CellLatitudeLongitude geoCell, Long startTime, Integer maxCount)
    {
        return findUserKeys(geoCell, startTime, null, maxCount);
    }
    public List<String> findUserKeys(CellLatitudeLongitude geoCell, Long startTime, Long offset, Integer maxCount)
    {
        if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "BEGIN: startTime = " + startTime + "; offset = " + offset + "; maxCount = " + maxCount);
        List<String> userKeys = null;
        try {
            String filter = "createdTime>=" + startTime;  // Note ">=" not ">"...
            if(geoCell != null) {
                Integer scale = geoCell.getScale();
                Integer latitude = geoCell.getLatitude();
                Integer longitude = geoCell.getLongitude();
                if(scale != null && latitude != null && longitude != null) {
                    filter += " && geoCell.scale==" + scale;
                    filter += " && geoCell.latitude==" + latitude;
                    filter += " && geoCell.longitude==" + longitude;
                } else {
                    // ???
                    if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Input geoCell object is incomplete: geoCell = " + geoCell);
                }
            }
            String ordering = "createdTime asc";
            if(offset == null) {
                offset = 0L;
            }
            if(maxCount == null || maxCount == 0) {
                maxCount = 500;    // ????   Arbitrary cutoff just to be safe...
            }
            userKeys = getUserAppService().findUserKeys(filter, ordering, null, null, null, null, offset, maxCount);    
        } catch (BaseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to find user keys for given startTime = " + startTime, e);
            return null;
        } catch (Exception e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to find user keys due to unknown error: startTime = " + startTime, e);
            return null;
        }
        if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "END");
        return userKeys;
    }

    public List<User> findUsers(Long startTime)
    {
        return findUsers(startTime, (Integer) null);
    }
    public List<User> findUsers(Long startTime, Integer maxCount)
    {
        return findUsers(null, startTime, null, maxCount);
    }
    public List<User> findUsers(CellLatitudeLongitude geoCell, Long startTime, Integer maxCount)
    {
        return findUsers(geoCell, startTime, null, maxCount);
    }
    private List<User> findUsers(CellLatitudeLongitude geoCell, Long startTime, Long offset, Integer maxCount)
    {
        if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "BEGIN: startTime = " + startTime + "; offset = " + offset + "; maxCount = " + maxCount);
        List<User> users = null;
        try {
            String filter = "createdTime>=" + startTime;  // Note ">=" not ">"...
            if(geoCell != null) {
                Integer scale = geoCell.getScale();
                Integer latitude = geoCell.getLatitude();
                Integer longitude = geoCell.getLongitude();
                if(scale != null && latitude != null && longitude != null) {
                    filter += " && geoCell.scale==" + scale;
                    filter += " && geoCell.latitude==" + latitude;
                    filter += " && geoCell.longitude==" + longitude;
                } else {
                    // ???
                    if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Input geoCell object is incomplete: geoCell = " + geoCell);
                }
            }
            String ordering = "createdTime asc";
            if(offset == null) {
                offset = 0L;
            }
            if(maxCount == null || maxCount == 0) {
                maxCount = 500;    // ????   Arbitrary cutoff just to be safe...
            }
            users = getUserAppService().findUsers(filter, ordering, null, null, null, null, offset, maxCount);    
        } catch (BaseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to find users for given startTime = " + startTime, e);
            return null;
        } catch (Exception e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to find users due to unknown error: startTime = " + startTime, e);
            return null;
        }
        if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "END");
        return users;
    }
    
    
    public List<String> findUserKeys(Long startTime, Long endTime)
    {
        return findUserKeys(null, startTime, endTime);
    }
    public List<String> findUserKeys(CellLatitudeLongitude geoCell, Long startTime, Long endTime)
    {
        return findUserKeys(geoCell, startTime, endTime, null, null);
    }
    public List<String> findUserKeys(CellLatitudeLongitude geoCell, Long startTime, Long endTime, Long offset, Integer maxCount)
    {
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "BEGIN: startTime = " + startTime + "; endTime = " + endTime + "; offset = " + offset + "; maxCount = " + maxCount);
        List<String> userKeys = null;
        try {
            String filter = "createdTime>=" + startTime;  // Note ">=" not ">"...
            if(endTime != null && endTime > 0L) {
                filter += " && createdTime<" + endTime;   // Note "<" not "<="...
            }
            if(geoCell != null) {
                Integer scale = geoCell.getScale();
                Integer latitude = geoCell.getLatitude();
                Integer longitude = geoCell.getLongitude();
                if(scale != null && latitude != null && longitude != null) {
                    filter += " && geoCell.scale==" + scale;
                    filter += " && geoCell.latitude==" + latitude;
                    filter += " && geoCell.longitude==" + longitude;
                } else {
                    // ???
                    if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Input geoCell object is incomplete: geoCell = " + geoCell);
                }
            }
            String ordering = "createdTime asc";
            if(offset == null) {
                offset = 0L;
            }
            if(maxCount == null || maxCount == 0) {
                maxCount = 500;    // ????   Arbitrary cutoff just to be safe...
            }
            userKeys = getUserAppService().findUserKeys(filter, ordering, null, null, null, null, offset, maxCount);    
        } catch (BaseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to find user keys for given startTime = " + startTime, e);
            return null;
        } catch (Exception e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to find user keys due to unknown error: startTime = " + startTime, e);
            return null;
        }
        if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "END");
        return userKeys;
    }

    public List<User> findUsers(Long startTime, Long endTime)
    {
        return findUsers(null, startTime, endTime);
    }
    public List<User> findUsers(CellLatitudeLongitude geoCell, Long startTime, Long endTime)
    {
        return findUsers(geoCell, startTime, endTime, null, null);        
    }
    public List<User> findUsers(CellLatitudeLongitude geoCell, Long startTime, Long endTime, Long offset, Integer maxCount)
    {
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "BEGIN: startTime = " + startTime + "; endTime = " + endTime + "; offset = " + offset + "; maxCount = " + maxCount);
        List<User> users = null;
        try {
            String filter = "createdTime>=" + startTime;  // Note ">=" not ">"...
            if(endTime != null && endTime > 0L) {
                filter += " && createdTime<" + endTime;   // Note "<" not "<="...
            }
            if(geoCell != null) {
                Integer scale = geoCell.getScale();
                Integer latitude = geoCell.getLatitude();
                Integer longitude = geoCell.getLongitude();
                if(scale != null && latitude != null && longitude != null) {
                    filter += " && geoCell.scale==" + scale;
                    filter += " && geoCell.latitude==" + latitude;
                    filter += " && geoCell.longitude==" + longitude;
                } else {
                    // ???
                    if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Input geoCell object is incomplete: geoCell = " + geoCell);
                }
            }
            String ordering = "createdTime asc";
            if(offset == null) {
                offset = 0L;
            }
            if(maxCount == null || maxCount == 0) {
                maxCount = 500;    // ????   Arbitrary cutoff just to be safe...
            }
            users = getUserAppService().findUsers(filter, ordering, null, null, null, null, offset, maxCount);    
        } catch (BaseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to find users for given startTime = " + startTime, e);
            return null;
        } catch (Exception e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to find users due to unknown error: startTime = " + startTime, e);
            return null;
        }
        if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "END");
        return users;
    }

    
    
    
    
}
