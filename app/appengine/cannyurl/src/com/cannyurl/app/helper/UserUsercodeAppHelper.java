package com.cannyurl.app.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cannyurl.app.service.UserUsercodeAppService;
import com.myurldb.ws.BaseException;
import com.myurldb.ws.UserUsercode;



// TBD::::
public class UserUsercodeAppHelper
{
    private static final Logger log = Logger.getLogger(UserUsercodeAppHelper.class.getName());

    private UserUsercodeAppService userUsercodeAppService = null;
//    private AccessRecordAppService accessRecordAppService = null;
    // ...

    private UserUsercodeAppHelper() {}

    // Initialization-on-demand holder.
    private static final class UserHelperHolder
    {
        private static final UserUsercodeAppHelper INSTANCE = new UserUsercodeAppHelper();
    }

    // Singleton method
    public static UserUsercodeAppHelper getInstance()
    {
        return UserHelperHolder.INSTANCE;
    }
    
    
    private UserUsercodeAppService getUserUsercodeAppService()
    {
        if(userUsercodeAppService == null) {
            userUsercodeAppService = new UserUsercodeAppService();
        }
        return userUsercodeAppService;
    }

    
    public UserUsercode getUserUsercode(String guid)
    {
        UserUsercode userUsercodeBean = null;
        
        if(getUserUsercodeAppService().getCache() != null) {
            userUsercodeBean = (UserUsercode) getUserUsercodeAppService().getCache().get(guid);
            if(userUsercodeBean != null) {
                if(log.isLoggable(Level.INFO)) log.info("userUsercodeBean retreived from cache: guid = " + guid);
            }
        }

        if(userUsercodeBean == null) {
            try {
                userUsercodeBean = getUserUsercodeAppService().getUserUsercode(guid);
                if(getUserUsercodeAppService().getCache() != null) {
                    if(userUsercodeBean != null) {
                        getUserUsercodeAppService().getCache().put(guid, userUsercodeBean);
                    }
                }           
            } catch (BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to find a userUsercode for given guid = " + guid, e);
                return null;
            } catch (Exception e) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to find a userUsercode due to unknown error: guid = " + guid, e);
                return null;
            }
        }

        return userUsercodeBean;
    }


    public String findUserUsercodeKeyByUsercode(String usercode)
    {
        // TBD
        String key = null;
        try {
            String filter = "usercode=='" + usercode + "'";
            // String ordering = "createdTime desc";    // This is only useful when we have more than one userUsercode for given usercode, but it's very unlikely...
            String ordering = null;
            // TBD: Since we are doing full fetch, we should really fetch keys only...
            List<String> keys = getUserUsercodeAppService().findUserUsercodeKeys(filter, ordering, null, null, null, null, 0L, 2);
            if(keys != null && !keys.isEmpty()) {
                if(keys.size() > 1) {
                    log.severe("More than one userUsercode found for usercode = " + usercode);
                    // What to do ????
                    // Continue or throw error....
                    // We do NOT want to use an incorrect userUsercode... so, just bail out....
                    return null;
                }
                // Just the first one.... if size > 1...
                key = keys.get(0);
            } else {
                if(log.isLoggable(Level.INFO)) log.info("Failed to find userUsercode for usercode = " + usercode);
            }
        } catch (BaseException e) {
            log.log(Level.WARNING, "findUserUsercodeKeys() failed for usercode = " + usercode, e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Unexpected error: findUserUsercodeKeys() failed for usercode = " + usercode, e);
        }
        return key;
    }
    public UserUsercode findUserUsercodeByUsercode(String usercode)
    {
        // TBD
        UserUsercode userUsercode = null;
        try {
            String filter = "usercode=='" + usercode + "'";
            // String ordering = "createdTime desc";    // This is only useful when we have more than one userUsercode for given usercode, but it's very unlikely...
            String ordering = null;
            // TBD: Since we are doing full fetch, we should really fetch keys only...
            List<UserUsercode> beans = getUserUsercodeAppService().findUserUsercodes(filter, ordering, null, null, null, null, 0L, 2);
            if(beans != null && !beans.isEmpty()) {
                if(beans.size() > 1) {
                    log.severe("More than one userUsercode found for usercode = " + usercode);
                    // What to do ????
                    // Continue or throw error....
                    // We do NOT want to use an incorrect userUsercode... so, just bail out....
                    return null;
                }
                // Just the first one.... if size > 1...
                String guid = beans.get(0).getGuid();
                // Full fetch...
                userUsercode = getUserUsercode(guid);
            } else {
                if(log.isLoggable(Level.INFO)) log.info("Failed to find userUsercode for usercode = " + usercode);
            }
        } catch (BaseException e) {
            log.log(Level.WARNING, "findUserUsercodes() failed for usercode = " + usercode, e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Unexpected error: findUserUsercodes() failed for usercode = " + usercode, e);
        }
        return userUsercode;
    }


    // TBD:
    // ....
    
    

}
