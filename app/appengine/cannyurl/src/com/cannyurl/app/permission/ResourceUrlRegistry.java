package com.cannyurl.app.permission;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


// Hack...
// Map of 
//        (JSP) Page URL -> Resource/Action
// This is needed to be able to use a generic permission filter.
// TBD: Is there a better way?
// .....
public class ResourceUrlRegistry
{
    private static final Logger log = Logger.getLogger(ResourceUrlRegistry.class.getName());

    // Static methods only..
    private ResourceUrlRegistry() {}

    // TBD:
    private static Map<String, String[]> sUrlResourceMap = null;
    static {
        // Note that we check the key from the top,
        // it's important to keep the entries in an order....
        // Also, put longer urls first (e.g., "/shorten/" vs. "/shorten")...
        sUrlResourceMap = new LinkedHashMap<String, String[]>();
        
        // TBD:
        // We need to be able to support "startsWith" not just exact URL match....
        // How to do this efficiently ????
        // TBD:
        // We need to keep this in sync with the definitions in web.xml
        // ....
        sUrlResourceMap.put("/shorten/", new String[] {"ShortLink", "update"});
        sUrlResourceMap.put("/shorten", new String[] {"ShortLink", "create"});
        sUrlResourceMap.put("/view/", new String[] {"ShortLink", "read"});      // What about "/*" ??????
        // ....
    }


    // temporary
    public static String[] getResourceAndAction(String url)
    {
        if(url == null) {
            if(log.isLoggable(Level.INFO)) log.info("Failed to find resource/action because url is null");
            return null;
        }
        if(sUrlResourceMap.containsKey(url)) {
            return sUrlResourceMap.get(url);
        } else {
            for(String u : sUrlResourceMap.keySet()) {
               if(url.startsWith(u)) {
                   return sUrlResourceMap.get(u);
               }
            }
        }
        if(log.isLoggable(Level.INFO)) log.info("Failed to find resource/action for the given url = " + url);
        return null;  // ????
    }
    
    

}
