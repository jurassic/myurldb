package com.cannyurl.app.permission;

import java.util.logging.Logger;

import com.cannyurl.app.util.ConfigUtil;
import com.myurldb.ws.permission.ShortLinkBasePermission;


public class ShortLinkPermission extends ShortLinkBasePermission
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ShortLinkPermission.class.getName());

    public ShortLinkPermission()
    {
    }

    
    @Override
    public boolean isCreatePermissionRequired()
    {
        // TBD: ...
        return ConfigUtil.isPermissionRequiredForShortLinkCreate();
    }

//    @Override
//    public boolean isReadPermissionRequired()
//    {
//        return super.isReadPermissionRequired();
//    }
//
//    @Override
//    public boolean isUpdatePermissionRequired()
//    {
//        return super.isUpdatePermissionRequired();
//    }
//
//    @Override
//    public boolean isDeletePermissionRequired()
//    {
//        return super.isDeletePermissionRequired();
//    }

    
}
