package com.cannyurl.app.permission;

import java.util.logging.Level;
import java.util.logging.Logger;

import net.sf.jsr107cache.Cache;

import com.cannyurl.af.permission.BasePermissionManager;
import com.cannyurl.af.permission.PermissionManagerFactory;
import com.cannyurl.af.util.CacheHelper;
import com.cannyurl.app.helper.UserAppHelper;
import com.cannyurl.app.permission.helper.UserResourcePermissionAppHelper;
import com.cannyurl.app.permission.helper.UserResourceProhibitionAppHelper;
import com.cannyurl.app.permission.helper.UserRolePermissionAppHelper;
import com.cannyurl.app.permission.util.PermissionNameUtil;
import com.cannyurl.app.util.ConfigUtil;
import com.myurldb.ws.BaseException;
import com.myurldb.ws.User;
import com.myurldb.ws.UserResourcePermission;
import com.myurldb.ws.UserResourceProhibition;
import com.myurldb.ws.permission.ShortLinkBasePermission;


public class UserPermissionManager
{
    private static final Logger log = Logger.getLogger(UserPermissionManager.class.getName());


    // TBD:
    // Map:  user -> {resource, action, permitted}
    // ....
    
    
    private UserPermissionManager()
    {
        // TBD:
        BasePermissionManager resourcePermissionManager = new ResourcePermissionManager();
        ShortLinkBasePermission shortLinkPermission = new ShortLinkPermission();
        resourcePermissionManager.setShortLinkPermission(shortLinkPermission);
        // etc...
        // ....
        PermissionManagerFactory permissionManagerFactory = PermissionManagerFactory.getInstance();
        permissionManagerFactory.setPermissionManager(resourcePermissionManager);        
    }


    // Initialization-on-demand holder.
    private static final class UserPermissionManagerHolder
    {
        private static final UserPermissionManager INSTANCE = new UserPermissionManager();
    }

    // Singleton method
    public static UserPermissionManager getInstance()
    {
        return UserPermissionManagerHolder.INSTANCE;
    }


    private Cache getCache()
    {
        return CacheHelper.getLongInstance().getCache();
        // return CacheHelper.getHourLongInstance().getCache();
    }
    // Note: We are implicitly using the permissioName - resource::action equivalency....
    private static String getCacheKey(String user, String permissionName)
    {
        StringBuilder sb = new StringBuilder();
        sb.append("UserPermission-").append(user).append("-").append(permissionName);
        return sb.toString();
    }
    private static String getCacheKey(String user, String resource, String action)
    {
        return getCacheKey(user, PermissionNameUtil.getPermissioName(resource, action));
//        StringBuilder sb = new StringBuilder();
//        sb.append("UserPermission-").append(user).append("-").append(PermissionNameUtil.getPermissioName(resource, action));
//        return sb.toString();
    }


    // TBD:
    // We need to check user -> role -> permission mapping as well....
    // ....
    // Note that, as a general rule,
    // we check direct user - permission mapping first....
    // the settings in UserResourcePermission and UserResourceProhibition take precedence
    // If none is found,
    // then check user - role - permission mapping...
    // ....
    // .....
    public boolean isPermitted(String user, String resource, String action)
    {
        String permissionName = PermissionNameUtil.getPermissioName(resource, action);
        if(log.isLoggable(Level.FINE)) log.fine(">>>>> permissionName = " + permissionName);
        return isPermitted(user, permissionName);
    }
    public boolean isPermitted(String user, String permissionName)
    {
        if(getCache() != null) {
            Boolean permitted = (Boolean) getCache().get(getCacheKey(user, permissionName));
            if(permitted != null) {
                return permitted;
            }
        }

        // tbd
        boolean isPermitted = false;
        boolean saveInCache = true;
        if(PermissionManagerFactory.getInstance().getPermissionManager().isPermissionRequired(permissionName) == false) {
            // Check blacklist ???
            try {
                UserResourceProhibition userResourceProhibition = UserResourceProhibitionAppHelper.getInstance().findUserResourceProhibitionByUserAndPermissionName(user, permissionName);
                if(userResourceProhibition == null) {
                    isPermitted = true;
                } else {
                    Boolean prohibited = userResourceProhibition.isProhibited();
                    if(prohibited != null && prohibited == true) {   // This should always be true.... ???
                        isPermitted = false;
                    } else {
                        isPermitted = true;
                    }
                }
            } catch (BaseException e) {
                log.log(Level.WARNING, "Error while fetching UserResourceProhibition for user = " + user + "; permissionName = " + permissionName, e);
                // What to do????
                isPermitted = false;   // ?????
                saveInCache = false;
            }
        } else {
            // [1] TBD:
            //     Check in the local file first ???
            // ...
            // [2] Then, look up in the permission table...
            try {
                UserResourcePermission userResourcePermission = UserResourcePermissionAppHelper.getInstance().findUserResourcePermissionByUserAndPermissionName(user, permissionName);
                if(userResourcePermission == null) {
                    
                    // TBD:
                    // Need to check user - role - permission mapping
                    // This seems a bit tricky...
                    // How to do this (efficiently) ????
                    // For given resource/action, find all roles that allow those resources/actions
                    // then for each role in the list,
                    // check if the user has the role...
                    // .....
                    
                    String perm = UserRolePermissionAppHelper.getInstance().findPermissionByUserAndPermissionName(user, permissionName);
                    if(perm != null) {
                        isPermitted = true;
                    } else {
                        // ???
                        // temporary
                        isPermitted = false;
                        // ....
                    }
                } else {
                    Boolean permitted = userResourcePermission.isPermitted();
                    if(permitted != null && permitted == true) {   // This should always be true.... ???
                        isPermitted = true;
                    } else {
                        isPermitted = false;
                    }
                }
            } catch (BaseException e) {
                log.log(Level.WARNING, "Error while fetching UserResourcePermission for user = " + user + "; permissionName = " + permissionName, e);
                // What to do????
                isPermitted = false;
                saveInCache = false;
            }
            
            
            // TBD:
            // We need to check user -> role -> permission mapping as well....
            // .....

            
            
            
            // TBD:
            // Username length check, etc. ???
            // Note: This is probably too expensive to do it every time (even with caching)......
            // TBD: if isPermitted == false due to length constraint, etc...
            //      Save it into UserResourcePermission, etc... ????
            // ?????
            // ....
            if(isPermitted == true) {   // No need to do this when isPermitted == false.
                // TBD: Check the UserUsercode table for usercode... ????
                // ....
                User userBean = UserAppHelper.getInstance().getUser(user);
                if(userBean != null) {
                    String username = userBean.getUsername();
                    if(username != null && !username.isEmpty()) {
                        int usernameLength = username.length();
                        int usernameMinLength = ConfigUtil.getUsernameMinLength();
                        int usernameMaxLength = ConfigUtil.getUsernameMaxLength();
                        if(usernameLength < usernameMinLength || usernameLength > usernameMaxLength) {
                            isPermitted = false;
                        }
                    } else {
                        // ???
                        // TBD:
                        // If username is required for the given domainType
                        // we need to set isPermitted to false...
                        // ...
                    }
                    String usercode = userBean.getUsercode();
                    if(usercode != null && !usercode.isEmpty()) {
                        int usercodeLength = usercode.length();
                        int usercodeMinLength = ConfigUtil.getUsercodeMinLength();
                        int usercodeMaxLength = ConfigUtil.getUsercodeMaxLength();
                        if(usercodeLength < usercodeMinLength || usercodeLength > usercodeMaxLength) {
                            isPermitted = false;
                        }
                    } else {
                        // ???
                        // TBD:
                        // If usercode is required for the given domainType
                        // we need to set isPermitted to false...
                        // ...
                    }
                    
                    // TBD:
                    // if isPermitted == false
                    // Save it to UserResourcePermission ???
                    // or delete UserResourcePermission ????
                    // -->
                    // the problem with this approach is that
                    // we need to refresh back UserResourcePermission if user's username/usercode change, etc...
                    // ....
                    // ....

                } else {
                    // ????
                    log.log(Level.WARNING, "Failed to find UserBean for user = " + user);
                    // What to do????
                    isPermitted = false;
                    saveInCache = false;
                }
            }
            // .....
        }

        if(getCache() != null && saveInCache == true) {
            getCache().put(getCacheKey(user, permissionName), isPermitted);
        }
        
        // temporary
        return isPermitted;
    }

    // ???
    public boolean hasRole(String user, String roleName)
    {
        // tbd
        return false;
    }
    
    
}
