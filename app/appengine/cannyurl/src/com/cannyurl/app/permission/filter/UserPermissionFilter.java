package com.cannyurl.app.permission.filter;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cannyurl.af.auth.SessionBean;
import com.cannyurl.af.auth.UserSessionManager;
import com.cannyurl.app.permission.ResourceUrlRegistry;
import com.cannyurl.app.permission.UserPermissionManager;
import com.cannyurl.app.util.ConfigUtil;
import com.cannyurl.common.AppAuthMode;
import com.cannyurl.helper.UrlHelper;


// 3/18/13:
//...


// For user/resource permission check...
// If the user is not authorized, forwards to an error page.
public class UserPermissionFilter implements Filter
{
    private static final Logger log = Logger.getLogger(UserPermissionFilter.class.getName());

    // TBD
    private FilterConfig config = null;
    private boolean isApplicationAuthDisabled = false;
//    private String applicationAuthMode = null;

    public UserPermissionFilter()
    {
    }

    @Override
    public void destroy()
    {
        this.config = null;
    }

    @Override
    public void init(FilterConfig config) throws ServletException
    {
        this.config = config;
        this.isApplicationAuthDisabled = ConfigUtil.isApplicationAuthDisabled();
        if(this.isApplicationAuthDisabled) {
            log.warning("isApplicationAuthDisabled == true");
        } else {
            log.info("isApplicationAuthDisabled == false");
        }
//        this.applicationAuthMode = ConfigUtil.getApplicationAuthMode();
//        // TBD:
//        // Validate applicationAuthMode ????
//        // ...
//        if(this.applicationAuthMode == null || this.applicationAuthMode.isEmpty()) {
//            this.applicationAuthMode = AppAuthMode.getDefaultValue();
//            if(log.isLoggable(Level.WARNING)) log.warning("applicationAuthMode set to " + this.applicationAuthMode);
//        } else {
//            if(log.isLoggable(Level.INFO)) log.info("applicationAuthMode = " + this.applicationAuthMode);
//        }
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res,
            FilterChain chain) throws IOException, ServletException
    {
        // ????
        if(isApplicationAuthDisabled) {
            chain.doFilter(req, res);
            return;
        } 

        // TBD:....
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;

        String requestURI = request.getRequestURI();
        String requestUrl = request.getRequestURL().toString();
        String topLevelUrl = UrlHelper.getInstance().getTopLevelURLFromRequestURL(requestUrl);
        String contextPath = request.getContextPath();
        String servletPath = request.getServletPath();
        String pathInfo = request.getPathInfo();
        String queryString = request.getQueryString();
        //...

        // TBD:
        SessionBean sessionBean = UserSessionManager.getInstance().setSessionBean(request, response);
        String user = sessionBean.getUserId();
        // ....
        
        // TBD:
        boolean isPermitted = false;

        // TBD:
        // How to find out which "resource - action" is being accessed???
        // .....
        
        if(user != null && !user.isEmpty()) {
            String[] resourceAction = ResourceUrlRegistry.getResourceAndAction(servletPath);
            if(resourceAction == null || resourceAction.length == 0) {
                // What to do ???
                // ....
                if(log.isLoggable(Level.INFO)) log.info("resourceAction not found for servletPath = " + servletPath);
            } else {
                String resource = null;
                String action = null;
                int len = resourceAction.length;
                // if(len > 0) {
                    resource = resourceAction[0];
                    if(len > 1) {
                        action = resourceAction[1];
                    }
                    isPermitted = UserPermissionManager.getInstance().isPermitted(user, resource, action);
                // } else {
                //     // ???
                // }
            }
        } else {
            // Can this happen????
            // What to do???
            log.warning("User is not set.");
            // ...
        }

        if(log.isLoggable(Level.INFO)) log.info("UserSessionFilter::isPermitted = " + isPermitted);
        if(isPermitted) {
            // Continue through filter chain.
            chain.doFilter(req, res);
            return;
        } else {
            // TBD
            // Redirect to the error page. ????
            config.getServletContext().getRequestDispatcher("/error/Unauthorized").forward(req, res);
            return;
        }
    }
    
}
