package com.cannyurl.app.permission.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.sf.jsr107cache.Cache;

import com.cannyurl.af.util.CacheHelper;
import com.cannyurl.app.permission.util.PermissionNameUtil;
import com.cannyurl.app.service.RolePermissionAppService;
import com.cannyurl.app.service.UserAppService;
import com.myurldb.ws.BaseException;
import com.myurldb.ws.RolePermission;



// TBD::::
// Need a way to look up shortUrl
//    first look up rolePermission
//    if not found, then look up shortLink
// ....

public class RolePermissionAppHelper
{
    private static final Logger log = Logger.getLogger(RolePermissionAppHelper.class.getName());

    private UserAppService userAppService = null;
    private RolePermissionAppService rolePermissionAppService = null;
    // ...

    private RolePermissionAppHelper() {}

    // Initialization-on-demand holder.
    private static final class RolePermissionAppHelperHolder
    {
        private static final RolePermissionAppHelper INSTANCE = new RolePermissionAppHelper();
    }

    // Singleton method
    public static RolePermissionAppHelper getInstance()
    {
        return RolePermissionAppHelperHolder.INSTANCE;
    }


    // temporary
    private static Cache getCache()
    {
        return CacheHelper.getLongInstance().getCache();
    }


    private UserAppService getUserAppService()
    {
        if(userAppService == null) {
            userAppService = new UserAppService();
        }
        return userAppService;
    }
    private RolePermissionAppService getRolePermissionAppService()
    {
        if(rolePermissionAppService == null) {
            rolePermissionAppService = new RolePermissionAppService();
        }
        return rolePermissionAppService;
    }

    
    public RolePermission getRolePermission(String guid)
    {
        RolePermission rolePermissionBean = null;
        try {
            if(getCache() != null) {
                rolePermissionBean = (RolePermission) getCache().get(guid);
            }
            if(rolePermissionBean == null) {
                rolePermissionBean = getRolePermissionAppService().getRolePermission(guid);
                if(rolePermissionBean != null) {
                    if(getCache() != null) {
                        getCache().put(guid, rolePermissionBean);
                    }
                }
            }
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to find an rolePermission for given guid = " + guid, e);
            return null;
        } catch (Exception e) {
            log.log(Level.WARNING, "Unexpected error: Failed to find a rolePermission for given guid = " + guid, e);
            return null;
        }

        return rolePermissionBean;
    }


    public List<RolePermission> findRolePermissionsByRole(String role)
    {
        // TBD
        List<RolePermission> beans = null;
        try {
            String filter = "role=='" + role + "'";
            // String ordering = "createdTime desc";    // This is only useful when we have more than one rolePermission for given permissionName, but it's very unlikely...
            String ordering = null;
            beans = getRolePermissionAppService().findRolePermissions(filter, ordering, null, null, null, null, null, null);
        } catch (BaseException e) {
            log.log(Level.WARNING, "findRolePermissions() failed for role = " + role, e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Unexpected error: findRolePermissions() failed for role = " + role, e);
        }
        return beans;
    }


    public RolePermission findRolePermissionByRoleAndPermissionName(String role, String permissionName) throws BaseException
    {
        // TBD
        RolePermission rolePermission = null;
        String filter = "role=='" + role + "'";
        filter += " && permissionName=='" + permissionName + "'";
        // String ordering = "createdTime desc";    // This is only useful when we have more than one rolePermission for given permissionName, but it's very unlikely...
        String ordering = null;
        List<RolePermission> beans = getRolePermissionAppService().findRolePermissions(filter, ordering, null, null, null, null, 0L, 2);
        if(beans != null && !beans.isEmpty()) {
            if(beans.size() > 1) {
                // Something's wrong...
                log.severe("More than one RolePermission found for role = " + role + "; permissionName = " + permissionName);
            }
            // full fetch.
            String guid = beans.get(0).getGuid();
            rolePermission = getRolePermission(guid);
        } else {
            if(log.isLoggable(Level.INFO)) log.info("No RolePermission found for role = " + role + "; permissionName = " + permissionName);
        }
        return rolePermission;
    }

    public RolePermission findRolePermissionByRoleAndResource(String role, String resource, String action) throws BaseException
    {
        String permissionName = PermissionNameUtil.getPermissioName(resource, action);
        return findRolePermissionByRoleAndPermissionName(role, permissionName);

//        // TBD
//        RolePermission rolePermission = null;
//        String filter = "role=='" + role + "'";
//        filter += " && resource=='" + resource + "'";
//        if(action != null && !action.isEmpty()) {
//            filter += " && action=='" + action + "'";
//        }
//        // String ordering = "createdTime desc";    // This is only useful when we have more than one rolePermission for given action, but it's very unlikely...
//        String ordering = null;
//        List<RolePermission> beans = getRolePermissionAppService().findRolePermissions(filter, ordering, null, null, null, null, 0L, 2);
//        if(beans != null && !beans.isEmpty()) {
//            if(beans.size() > 1) {
//                // Something's wrong...
//                log.severe("More than one RolePermission found for role = " + role + "; resource = " + resource + "; action" + action);
//            }
//            // full fetch.
//            String guid = beans.get(0).getGuid();
//            rolePermission = getRolePermission(guid);
//        } else {
//            if(log.isLoggable(Level.INFO)) log.info("No RolePermission found for role = " + role + "; resource = " + resource + "; action" + action);
//        }
//        return rolePermission;
    }

    
    
    
    // Functions below,
    // Not being used
    // No indexes set up... ???
    
    public List<RolePermission> findRolePermissionsByPermissionName(String permissionName)
    {
        // TBD
        List<RolePermission> beans = null;
        try {
            String filter = "permissionName=='" + permissionName + "'";
            // String ordering = "createdTime desc";    // This is only useful when we have more than one rolePermission for given permissionName, but it's very unlikely...
            String ordering = null;
            beans = getRolePermissionAppService().findRolePermissions(filter, ordering, null, null, null, null, null, null);
        } catch (BaseException e) {
            log.log(Level.WARNING, "findRolePermissions() failed for permissionName = " + permissionName, e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Unexpected error: findRolePermissions() failed for permissionName = " + permissionName, e);
        }
        return beans;
    }


    public List<RolePermission> findRolePermissionsByResource(String resource)
    {
        return findRolePermissionsByResource(resource, null);
    }
    public List<RolePermission> findRolePermissionsByResource(String resource, String action)
    {
        String permissionName = PermissionNameUtil.getPermissioName(resource, action);
        return findRolePermissionsByPermissionName(permissionName);

//        // TBD
//        List<RolePermission> beans = null;
//        try {
//            String filter = "resource=='" + resource + "'";
//            if(action != null && !action.isEmpty()) {
//                filter += " && action=='" + action + "'";
//            }
//            // String ordering = "createdTime desc";    // This is only useful when we have more than one rolePermission for given action, but it's very unlikely...
//            String ordering = null;
//            beans = getRolePermissionAppService().findRolePermissions(filter, ordering, null, null, null, null, null, null);
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "findRolePermissions() failed for action = " + action, e);
//        } catch (Exception e) {
//            log.log(Level.WARNING, "Unexpected error: findRolePermissions() failed for action = " + action, e);
//        }
//        return beans;
    }

    
}
