package com.cannyurl.app.permission.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.sf.jsr107cache.Cache;

import com.cannyurl.af.util.CacheHelper;
import com.cannyurl.app.permission.util.PermissionNameUtil;
import com.cannyurl.app.service.UserResourcePermissionAppService;
import com.cannyurl.app.service.UserAppService;
import com.myurldb.ws.UserResourcePermission;
import com.myurldb.ws.BaseException;



// TBD::::
// Need a way to look up shortUrl
//    first look up userResourcePermission
//    if not found, then look up shortLink
// ....

public class UserResourcePermissionAppHelper
{
    private static final Logger log = Logger.getLogger(UserResourcePermissionAppHelper.class.getName());

    private UserAppService userAppService = null;
    private UserResourcePermissionAppService userResourcePermissionAppService = null;
    // ...

    private UserResourcePermissionAppHelper() {}

    // Initialization-on-demand holder.
    private static final class UserResourcePermissionAppHelperHolder
    {
        private static final UserResourcePermissionAppHelper INSTANCE = new UserResourcePermissionAppHelper();
    }

    // Singleton method
    public static UserResourcePermissionAppHelper getInstance()
    {
        return UserResourcePermissionAppHelperHolder.INSTANCE;
    }


    // temporary
    private static Cache getCache()
    {
        return CacheHelper.getLongInstance().getCache();
    }


    private UserAppService getUserAppService()
    {
        if(userAppService == null) {
            userAppService = new UserAppService();
        }
        return userAppService;
    }
    private UserResourcePermissionAppService getUserResourcePermissionAppService()
    {
        if(userResourcePermissionAppService == null) {
            userResourcePermissionAppService = new UserResourcePermissionAppService();
        }
        return userResourcePermissionAppService;
    }

    
    public UserResourcePermission getUserResourcePermission(String guid)
    {
        UserResourcePermission userResourcePermissionBean = null;
        try {
            if(getCache() != null) {
                userResourcePermissionBean = (UserResourcePermission) getCache().get(guid);
            }
            if(userResourcePermissionBean == null) {
                userResourcePermissionBean = getUserResourcePermissionAppService().getUserResourcePermission(guid);
                if(userResourcePermissionBean != null) {
                    if(getCache() != null) {
                        getCache().put(guid, userResourcePermissionBean);
                    }
                }
            }
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to find an userResourcePermission for given guid = " + guid, e);
            return null;
        } catch (Exception e) {
            log.log(Level.WARNING, "Unexpected error: Failed to find a userResourcePermission for given guid = " + guid, e);
            return null;
        }

        return userResourcePermissionBean;
    }


    public List<UserResourcePermission> findUserResourcePermissionsByUser(String user)
    {
        // TBD
        List<UserResourcePermission> beans = null;
        try {
            String filter = "user=='" + user + "'";
            // String ordering = "createdTime desc";    // This is only useful when we have more than one userResourcePermission for given permissionName, but it's very unlikely...
            String ordering = null;
            beans = getUserResourcePermissionAppService().findUserResourcePermissions(filter, ordering, null, null, null, null, null, null);
        } catch (BaseException e) {
            log.log(Level.WARNING, "findUserResourcePermissions() failed for user = " + user, e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Unexpected error: findUserResourcePermissions() failed for user = " + user, e);
        }
        return beans;
    }

    public List<UserResourcePermission> findUserResourcePermissionsByPermissionName(String permissionName)
    {
        // TBD
        List<UserResourcePermission> beans = null;
        try {
            String filter = "permissionName=='" + permissionName + "'";
            // String ordering = "createdTime desc";    // This is only useful when we have more than one userResourcePermission for given permissionName, but it's very unlikely...
            String ordering = null;
            beans = getUserResourcePermissionAppService().findUserResourcePermissions(filter, ordering, null, null, null, null, null, null);
        } catch (BaseException e) {
            log.log(Level.WARNING, "findUserResourcePermissions() failed for permissionName = " + permissionName, e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Unexpected error: findUserResourcePermissions() failed for permissionName = " + permissionName, e);
        }
        return beans;
    }


    public UserResourcePermission findUserResourcePermissionByUserAndPermissionName(String user, String permissionName) throws BaseException
    {
        // TBD
        UserResourcePermission userResourcePermission = null;
        String filter = "user=='" + user + "'";
        filter += " && permissionName=='" + permissionName + "'";
        // String ordering = "createdTime desc";    // This is only useful when we have more than one userResourcePermission for given permissionName, but it's very unlikely...
        String ordering = null;
        List<UserResourcePermission> beans = getUserResourcePermissionAppService().findUserResourcePermissions(filter, ordering, null, null, null, null, 0L, 2);
        if(beans != null && !beans.isEmpty()) {
            if(beans.size() > 1) {
                // Something's wrong...
                log.severe("More than one UserResourcePermission found for user = " + user + "; permissionName = " + permissionName);
            }
            // full fetch.
            String guid = beans.get(0).getGuid();
            userResourcePermission = getUserResourcePermission(guid);
        } else {
            if(log.isLoggable(Level.INFO)) log.info("No UserResourcePermission found for user = " + user + "; permissionName = " + permissionName);
        }
        return userResourcePermission;
    }

    public List<UserResourcePermission> findUserResourcePermissionsByResource(String resource)
    {
        return findUserResourcePermissionsByResource(resource, null);
    }
    public List<UserResourcePermission> findUserResourcePermissionsByResource(String resource, String action)
    {
        String permissionName = PermissionNameUtil.getPermissioName(resource, action);
        return findUserResourcePermissionsByPermissionName(permissionName);

//        // TBD
//        List<UserResourcePermission> beans = null;
//        try {
//            String filter = "resource=='" + resource + "'";
//            if(action != null && !action.isEmpty()) {
//                filter += " && action=='" + action + "'";
//            }
//            // String ordering = "createdTime desc";    // This is only useful when we have more than one userResourcePermission for given action, but it's very unlikely...
//            String ordering = null;
//            beans = getUserResourcePermissionAppService().findUserResourcePermissions(filter, ordering, null, null, null, null, null, null);
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "findUserResourcePermissions() failed for action = " + action, e);
//        } catch (Exception e) {
//            log.log(Level.WARNING, "Unexpected error: findUserResourcePermissions() failed for action = " + action, e);
//        }
//        return beans;
    }


    public List<UserResourcePermission> findUserResourcePermissionsByUserAndResource(String user, String resource)
    {
//        String permissionName = PermissionNameUtil.getPermissioName(resource);
//        return findUserResourcePermissionsByUserAndPermissionName(user, permissionName);

        // TBD
        List<UserResourcePermission> beans = null;
        try {
            String filter = "user=='" + user + "'";
            filter += " && resource=='" + resource + "'";
            // String ordering = "createdTime desc";    // This is only useful when we have more than one userResourcePermission for given action, but it's very unlikely...
            String ordering = null;
            beans = getUserResourcePermissionAppService().findUserResourcePermissions(filter, ordering, null, null, null, null, null, null);
        } catch (BaseException e) {
            log.log(Level.WARNING, "findUserResourcePermissions() failed for user = " + user + "; resource = " + resource, e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Unexpected error: findUserResourcePermissions() failed for user = " + user + "; resource = " + resource, e);
        }
        return beans;
    }

    public UserResourcePermission findUserResourcePermissionByUserAndResource(String user, String resource, String action) throws BaseException
    {
        String permissionName = PermissionNameUtil.getPermissioName(resource, action);
        return findUserResourcePermissionByUserAndPermissionName(user, permissionName);

//        // TBD
//        UserResourcePermission userResourcePermission = null;
//        String filter = "user=='" + user + "'";
//        filter += " && resource=='" + resource + "'";
//        if(action != null && !action.isEmpty()) {
//            filter += " && action=='" + action + "'";
//        }
//        // String ordering = "createdTime desc";    // This is only useful when we have more than one userResourcePermission for given action, but it's very unlikely...
//        String ordering = null;
//        List<UserResourcePermission> beans = getUserResourcePermissionAppService().findUserResourcePermissions(filter, ordering, null, null, null, null, 0L, 2);
//        if(beans != null && !beans.isEmpty()) {
//            if(beans.size() > 1) {
//                // Something's wrong...
//                log.severe("More than one UserResourcePermission found for user = " + user + "; resource = " + resource + "; action" + action);
//            }
//            // full fetch.
//            String guid = beans.get(0).getGuid();
//            userResourcePermission = getUserResourcePermission(guid);
//        } else {
//            if(log.isLoggable(Level.INFO)) log.info("No UserResourcePermission found for user = " + user + "; resource = " + resource + "; action" + action);
//        }
//        return userResourcePermission;
    }

    
}
