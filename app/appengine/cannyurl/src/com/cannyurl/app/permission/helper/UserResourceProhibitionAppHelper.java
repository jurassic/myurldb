package com.cannyurl.app.permission.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.sf.jsr107cache.Cache;

import com.cannyurl.af.util.CacheHelper;
import com.cannyurl.app.permission.util.PermissionNameUtil;
import com.cannyurl.app.service.UserResourceProhibitionAppService;
import com.cannyurl.app.service.UserAppService;
import com.myurldb.ws.UserResourceProhibition;
import com.myurldb.ws.BaseException;



// TBD::::
// Need a way to look up shortUrl
//    first look up userResourceProhibition
//    if not found, then look up shortLink
// ....

public class UserResourceProhibitionAppHelper
{
    private static final Logger log = Logger.getLogger(UserResourceProhibitionAppHelper.class.getName());

    private UserAppService userAppService = null;
    private UserResourceProhibitionAppService userResourceProhibitionAppService = null;
    // ...

    private UserResourceProhibitionAppHelper() {}

    // Initialization-on-demand holder.
    private static final class UserResourceProhibitionAppHelperHolder
    {
        private static final UserResourceProhibitionAppHelper INSTANCE = new UserResourceProhibitionAppHelper();
    }

    // Singleton method
    public static UserResourceProhibitionAppHelper getInstance()
    {
        return UserResourceProhibitionAppHelperHolder.INSTANCE;
    }


    // temporary
    private static Cache getCache()
    {
        return CacheHelper.getLongInstance().getCache();
    }


    private UserAppService getUserAppService()
    {
        if(userAppService == null) {
            userAppService = new UserAppService();
        }
        return userAppService;
    }
    private UserResourceProhibitionAppService getUserResourceProhibitionAppService()
    {
        if(userResourceProhibitionAppService == null) {
            userResourceProhibitionAppService = new UserResourceProhibitionAppService();
        }
        return userResourceProhibitionAppService;
    }

    
    public UserResourceProhibition getUserResourceProhibition(String guid)
    {
        UserResourceProhibition userResourceProhibitionBean = null;
        try {
            if(getCache() != null) {
                userResourceProhibitionBean = (UserResourceProhibition) getCache().get(guid);
            }
            if(userResourceProhibitionBean == null) {
                userResourceProhibitionBean = getUserResourceProhibitionAppService().getUserResourceProhibition(guid);
                if(userResourceProhibitionBean != null) {
                    if(getCache() != null) {
                        getCache().put(guid, userResourceProhibitionBean);
                    }
                }
            }
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to find an userResourceProhibition for given guid = " + guid, e);
            return null;
        } catch (Exception e) {
            log.log(Level.WARNING, "Unexpected error: Failed to find a userResourceProhibition for given guid = " + guid, e);
            return null;
        }

        return userResourceProhibitionBean;
    }


    public List<UserResourceProhibition> findUserResourceProhibitionsByUser(String user)
    {
        // TBD
        List<UserResourceProhibition> beans = null;
        try {
            String filter = "user=='" + user + "'";
            // String ordering = "createdTime desc";    // This is only useful when we have more than one userResourceProhibition for given permissionName, but it's very unlikely...
            String ordering = null;
            beans = getUserResourceProhibitionAppService().findUserResourceProhibitions(filter, ordering, null, null, null, null, null, null);
        } catch (BaseException e) {
            log.log(Level.WARNING, "findUserResourceProhibitions() failed for user = " + user, e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Unexpected error: findUserResourceProhibitions() failed for user = " + user, e);
        }
        return beans;
    }

    public List<UserResourceProhibition> findUserResourceProhibitionsByProhibitionName(String permissionName)
    {
        // TBD
        List<UserResourceProhibition> beans = null;
        try {
            String filter = "permissionName=='" + permissionName + "'";
            // String ordering = "createdTime desc";    // This is only useful when we have more than one userResourceProhibition for given permissionName, but it's very unlikely...
            String ordering = null;
            beans = getUserResourceProhibitionAppService().findUserResourceProhibitions(filter, ordering, null, null, null, null, null, null);
        } catch (BaseException e) {
            log.log(Level.WARNING, "findUserResourceProhibitions() failed for permissionName = " + permissionName, e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Unexpected error: findUserResourceProhibitions() failed for permissionName = " + permissionName, e);
        }
        return beans;
    }


    public UserResourceProhibition findUserResourceProhibitionByUserAndPermissionName(String user, String permissionName) throws BaseException
    {
        // TBD
        UserResourceProhibition userResourceProhibition = null;
        String filter = "user=='" + user + "'";
        filter += " && permissionName=='" + permissionName + "'";
        // String ordering = "createdTime desc";    // This is only useful when we have more than one userResourceProhibition for given permissionName, but it's very unlikely...
        String ordering = null;
        List<UserResourceProhibition> beans = getUserResourceProhibitionAppService().findUserResourceProhibitions(filter, ordering, null, null, null, null, 0L, 2);
        if(beans != null && !beans.isEmpty()) {
            if(beans.size() > 1) {
                // Something's wrong...
                log.severe("More than one UserResourceProhibition found for user = " + user + "; permissionName = " + permissionName);
            }
            // full fetch.
            String guid = beans.get(0).getGuid();
            userResourceProhibition = getUserResourceProhibition(guid);
        } else {
            if(log.isLoggable(Level.INFO)) log.info("No UserResourceProhibition found for user = " + user + "; permissionName = " + permissionName);
        }
        return userResourceProhibition;
    }

    public List<UserResourceProhibition> findUserResourceProhibitionsByResource(String resource)
    {
        return findUserResourceProhibitionsByResource(resource, null);
    }
    public List<UserResourceProhibition> findUserResourceProhibitionsByResource(String resource, String action)
    {
        String permissionName = PermissionNameUtil.getPermissioName(resource, action);
        return findUserResourceProhibitionsByProhibitionName(permissionName);

//        // TBD
//        List<UserResourceProhibition> beans = null;
//        try {
//            String filter = "resource=='" + resource + "'";
//            if(action != null && !action.isEmpty()) {
//                filter += " && action=='" + action + "'";
//            }
//            // String ordering = "createdTime desc";    // This is only useful when we have more than one userResourceProhibition for given action, but it's very unlikely...
//            String ordering = null;
//            beans = getUserResourceProhibitionAppService().findUserResourceProhibitions(filter, ordering, null, null, null, null, null, null);
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "findUserResourceProhibitions() failed for action = " + action, e);
//        } catch (Exception e) {
//            log.log(Level.WARNING, "Unexpected error: findUserResourceProhibitions() failed for action = " + action, e);
//        }
//        return beans;
    }


    public List<UserResourceProhibition> findUserResourceProhibitionsByUserAndResource(String user, String resource)
    {
//      String permissionName = PermissionNameUtil.getPermissioName(resource);
//      return findUserResourcePerohibitionsByUserAndPermissionName(user, permissionName);

        // TBD
        List<UserResourceProhibition> beans = null;
        try {
            String filter = "user=='" + user + "'";
            filter += " && resource=='" + resource + "'";
            // String ordering = "createdTime desc";    // This is only useful when we have more than one userResourceProhibition for given action, but it's very unlikely...
            String ordering = null;
            beans = getUserResourceProhibitionAppService().findUserResourceProhibitions(filter, ordering, null, null, null, null, null, null);
        } catch (BaseException e) {
            log.log(Level.WARNING, "findUserResourceProhibitions() failed for user = " + user + "; resource = " + resource, e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Unexpected error: findUserResourceProhibitions() failed for user = " + user + "; resource = " + resource, e);
        }
        return beans;
    }

    public UserResourceProhibition findUserResourceProhibitionByUserAndResource(String user, String resource, String action) throws BaseException
    {
        String permissionName = PermissionNameUtil.getPermissioName(resource, action);
        return findUserResourceProhibitionByUserAndPermissionName(user, permissionName);

//        // TBD
//        UserResourceProhibition userResourceProhibition = null;
//        String filter = "user=='" + user + "'";
//        filter += " && resource=='" + resource + "'";
//        if(action != null && !action.isEmpty()) {
//            filter += " && action=='" + action + "'";
//        }
//        // String ordering = "createdTime desc";    // This is only useful when we have more than one userResourceProhibition for given action, but it's very unlikely...
//        String ordering = null;
//        List<UserResourceProhibition> beans = getUserResourceProhibitionAppService().findUserResourceProhibitions(filter, ordering, null, null, null, null, 0L, 2);
//        if(beans != null && !beans.isEmpty()) {
//            if(beans.size() > 1) {
//                // Something's wrong...
//                log.severe("More than one UserResourceProhibition found for user = " + user + "; resource = " + resource + "; action" + action);
//            }
//            // full fetch.
//            String guid = beans.get(0).getGuid();
//            userResourceProhibition = getUserResourceProhibition(guid);
//        } else {
//            if(log.isLoggable(Level.INFO)) log.info("No UserResourceProhibition found for user = " + user + "; resource = " + resource + "; action" + action);
//        }
//        return userResourceProhibition;
    }

    
}
