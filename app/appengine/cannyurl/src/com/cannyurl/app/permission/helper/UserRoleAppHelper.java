package com.cannyurl.app.permission.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.sf.jsr107cache.Cache;

import com.cannyurl.af.util.CacheHelper;
import com.cannyurl.app.service.UserAppService;
import com.cannyurl.app.service.UserRoleAppService;
import com.myurldb.ws.BaseException;
import com.myurldb.ws.UserRole;



// TBD::::
// Need a way to look up shortUrl
//    first look up userRole
//    if not found, then look up shortLink
// ....

public class UserRoleAppHelper
{
    private static final Logger log = Logger.getLogger(UserRoleAppHelper.class.getName());

    private UserAppService userAppService = null;
    private UserRoleAppService userRoleAppService = null;
    // ...

    private UserRoleAppHelper() {}

    // Initialization-on-demand holder.
    private static final class UserRoleAppHelperHolder
    {
        private static final UserRoleAppHelper INSTANCE = new UserRoleAppHelper();
    }

    // Singleton method
    public static UserRoleAppHelper getInstance()
    {
        return UserRoleAppHelperHolder.INSTANCE;
    }


    // temporary
    private static Cache getCache()
    {
        return CacheHelper.getLongInstance().getCache();
    }


    private UserAppService getUserAppService()
    {
        if(userAppService == null) {
            userAppService = new UserAppService();
        }
        return userAppService;
    }
    private UserRoleAppService getUserRoleAppService()
    {
        if(userRoleAppService == null) {
            userRoleAppService = new UserRoleAppService();
        }
        return userRoleAppService;
    }

    
    public UserRole getUserRole(String guid)
    {
        UserRole userRoleBean = null;
        try {
            if(getCache() != null) {
                userRoleBean = (UserRole) getCache().get(guid);
            }
            if(userRoleBean == null) {
                userRoleBean = getUserRoleAppService().getUserRole(guid);
                if(userRoleBean != null) {
                    if(getCache() != null) {
                        getCache().put(guid, userRoleBean);
                    }
                }
            }
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to find an userRole for given guid = " + guid, e);
            return null;
        } catch (Exception e) {
            log.log(Level.WARNING, "Unexpected error: Failed to find a userRole for given guid = " + guid, e);
            return null;
        }

        return userRoleBean;
    }

    
    // TBD:
    // Implicit role for All users???
    // e.g., "user", etc.. ???
    // ....

    public List<UserRole> findUserRolesByUser(String user)
    {
        // TBD
        List<UserRole> beans = null;
        try {
            String filter = "user=='" + user + "'";
            // String ordering = "createdTime desc";    // This is only useful when we have more than one userRole for given permissionName, but it's very unlikely...
            String ordering = null;
            beans = getUserRoleAppService().findUserRoles(filter, ordering, null, null, null, null, null, null);
        } catch (BaseException e) {
            log.log(Level.WARNING, "findUserRoles() failed for user = " + user, e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Unexpected error: findUserRoles() failed for user = " + user, e);
        }
        return beans;
    }


    
    // TBD:
    // This method requires offset/count args... unlike findUserRolesByUser()
    // There can be a large number of users for the givne role....
    public List<UserRole> findUserRolesByRole(String role)
    {
        return findUserRolesByRole(role, null, null);
    }
    public List<UserRole> findUserRolesByRole(String role, Long offset, Integer count)
    {
        // TBD
        List<UserRole> beans = null;
        try {
            String filter = "role=='" + role + "'";
            // String ordering = "createdTime desc";    // This is only useful when we have more than one roleRole for given permissionName, but it's very unlikely...
            String ordering = null;
            beans = getUserRoleAppService().findUserRoles(filter, ordering, null, null, null, null, offset, count);
        } catch (BaseException e) {
            log.log(Level.WARNING, "findUserRoles() failed for role = " + role, e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Unexpected error: findUserRoles() failed for role = " + role, e);
        }
        return beans;
    }

}
