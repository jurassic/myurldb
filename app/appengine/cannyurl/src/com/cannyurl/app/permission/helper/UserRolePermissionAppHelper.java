package com.cannyurl.app.permission.helper;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import net.sf.jsr107cache.Cache;

import com.cannyurl.af.util.CacheHelper;
import com.cannyurl.app.permission.util.PermissionNameUtil;
import com.cannyurl.app.service.RolePermissionAppService;
import com.cannyurl.app.service.UserAppService;
import com.cannyurl.app.service.UserRoleAppService;
import com.myurldb.ws.BaseException;
import com.myurldb.ws.RolePermission;
import com.myurldb.ws.UserRole;



// TBD::::
// Need a way to look up shortUrl
//    first look up rolePermission
//    if not found, then look up shortLink
// ....

public class UserRolePermissionAppHelper
{
    private static final Logger log = Logger.getLogger(UserRolePermissionAppHelper.class.getName());

    private UserAppService userAppService = null;
    private UserRoleAppService userRoleAppService = null;
    private RolePermissionAppService rolePermissionAppService = null;
    // ...

    private UserRolePermissionAppHelper() {}

    // Initialization-on-demand holder.
    private static final class UserRolePermissionAppHelperHolder
    {
        private static final UserRolePermissionAppHelper INSTANCE = new UserRolePermissionAppHelper();
    }

    // Singleton method
    public static UserRolePermissionAppHelper getInstance()
    {
        return UserRolePermissionAppHelperHolder.INSTANCE;
    }



    private Cache getCache()
    {
        return CacheHelper.getLongInstance().getCache();
        // return CacheHelper.getHourLongInstance().getCache();
    }
    // Note: We are implicitly using the permissioName - resource::action equivalency....
    private static String getCacheKey(String user, String permissionName)
    {
        StringBuilder sb = new StringBuilder();
        sb.append("UserRolePermission-").append(user).append("-").append(permissionName);
        return sb.toString();
    }
    private static String getCacheKey(String user, String resource, String action)
    {
        return getCacheKey(user, PermissionNameUtil.getPermissioName(resource, action));
//        StringBuilder sb = new StringBuilder();
//        sb.append("UserRolePermission-").append(user).append("-").append(PermissionNameUtil.getPermissioName(resource, action));
//        return sb.toString();
    }



    private UserAppService getUserAppService()
    {
        if(userAppService == null) {
            userAppService = new UserAppService();
        }
        return userAppService;
    }
    private UserRoleAppService getUserRoleAppService()
    {
        if(userRoleAppService == null) {
            userRoleAppService = new UserRoleAppService();
        }
        return userRoleAppService;
    }
    private RolePermissionAppService getRolePermissionAppService()
    {
        if(rolePermissionAppService == null) {
            rolePermissionAppService = new RolePermissionAppService();
        }
        return rolePermissionAppService;
    }



    // The method in this class are all sort of "joins".
    // Expensive queries.
    // TBD: Is there a better way????
    
    
    
    // TBD: To be deleted???
    private List<RolePermission> findRolePermissionsByUser(String user)
    {
        List<UserRole> userRoles = UserRoleAppHelper.getInstance().findUserRolesByUser(user);
        if(userRoles == null || userRoles.isEmpty()) {
            return null;   // ???
        }

        // Using set for de-dup'ing... ???
        // Set<RolePermission> beans = new HashSet<RolePermission>();
        // --> This does not work since RolePermission might be different entities even though they point to the same permissions...
        List<RolePermission> beans = new ArrayList<RolePermission>();
        for(UserRole ur : userRoles) {
            String role = ur.getRole();
            List<RolePermission> perms = RolePermissionAppHelper.getInstance().findRolePermissionsByRole(role);
            if(perms != null && !perms.isEmpty()) {
                beans.addAll(perms);
            }
        }
        // TBD:
        // De'duping here????
        return beans;
    }

    // Returns a set of permissionNames...
    public Set<String> findPermissionsByUser(String user)
    {
        List<UserRole> userRoles = UserRoleAppHelper.getInstance().findUserRolesByUser(user);
        if(userRoles == null || userRoles.isEmpty()) {
            return null;   // ???
        }

        // Using set for de-dup'ing... 
        Set<String> beans = new HashSet<String>();
        for(UserRole ur : userRoles) {
            String role = ur.getRole();
            List<RolePermission> perms = RolePermissionAppHelper.getInstance().findRolePermissionsByRole(role);
            if(perms != null && !perms.isEmpty()) {
                for(RolePermission rp : perms) {
                    String pName = rp.getPermissionName();
                    beans.add(pName);
                }
            }
        }
        // TBD:
        // De'duping here????
        return beans;
    }

    // Returns the arg permissionName, if any is found for the given user. 
    // Returns null, otherwise.
    public String findPermissionByUserAndPermissionName(String user, String permissionName) throws BaseException
    {
        String permName = null;
        if(getCache() != null) {
            if(getCache().containsKey(getCacheKey(user, permissionName))) {
                permName = (String) getCache().get(getCacheKey(user, permissionName));
                return permName;  // == permissionName if permName!= null. Note tha permNaem can be null....
            }
        }

        List<UserRole> userRoles = UserRoleAppHelper.getInstance().findUserRolesByUser(user);
        if(userRoles == null || userRoles.isEmpty()) {
            // return null;   // ???
        } else {
            for(UserRole ur : userRoles) {
                String role = ur.getRole();
                RolePermission perm = RolePermissionAppHelper.getInstance().findRolePermissionByRoleAndPermissionName(role, permissionName);
                if(perm != null) {
                    // permissionName = perm.getPermissionName();
                    permName = permissionName;
                    break;
                }
            }
        }

        if(getCache() != null) {
            getCache().put(getCacheKey(user, permissionName), permName);   // Note that permName can be null...
        }

        return permName;
    }

    public String findPermissionByUserAndResource(String user, String resource, String action) throws BaseException
    {
        String permissionName = PermissionNameUtil.getPermissioName(resource, action);
        return findPermissionByUserAndPermissionName(user, permissionName);
        
//        String permName = null;
//        if(getCache() != null) {
//            if(getCache().containsKey(getCacheKey(user, resource, action))) {
//                permName = (String) getCache().get(getCacheKey(user, resource, action));
//                return permName;  // == permissionName if permName!= null. Note tha permNaem can be null....
//            }
//        }
//
//        List<UserRole> userRoles = UserRoleAppHelper.getInstance().findUserRolesByUser(user);
//        if(userRoles == null || userRoles.isEmpty()) {
//            // return null;   // ???
//        } else {
//            for(UserRole ur : userRoles) {
//                String role = ur.getRole();
//                RolePermission perm = RolePermissionAppHelper.getInstance().findRolePermissionByRoleAndResource(role, resource, action);
//                if(perm != null) {
//                    permName = perm.getPermissionName();
//                    break;
//                }
//            }
//        }
//
//        if(getCache() != null) {
//            getCache().put(getCacheKey(user, resource, action), permName);  // Note that permName can be null...
//        }
//
//        return permName;
    }

    
}
