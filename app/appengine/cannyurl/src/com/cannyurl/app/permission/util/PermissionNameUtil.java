package com.cannyurl.app.permission.util;

import java.util.logging.Level;
import java.util.logging.Logger;


public class PermissionNameUtil
{
    private static final Logger log = Logger.getLogger(PermissionNameUtil.class.getName());

    // Static methods only...
    private PermissionNameUtil() {}


    public static String getPermissioName(String resource)
    {
        return getPermissioName(resource, null);
    }
    public static String getPermissioName(String resource, String action)
    {
        return getPermissioName(resource, null, action);
    }
    // Note the (unusual) arg order... 
    public static String getPermissioName(String resource, String instance, String action)
    {
        return getPermissioName(resource, instance, action, null);
    }
    public static String getPermissioName(String resource, String instance, String action, String field)
    {
        if(resource == null || resource.isEmpty()) {
            return resource;   // ????
        }
        StringBuilder sb = new StringBuilder();
        sb.append(resource).append(":");
        if(instance != null) {
            sb.append(instance);
        }
        sb.append(":");
        if(action != null) {
            sb.append(action);
        }
        if(field != null) {
            sb.append(":").append(field);
        }
        String permissionName = sb.toString();
        if(log.isLoggable(Level.FINER)) log.finer("permissionName = " + permissionName + " for resource = " + resource + "; instance = " + instance + "; action = " + action);
        return permissionName;
    }

    
    // Return String[] {resource, action, ...} from permissionName ???
    // Note that this method ignores instance and field. 
    public static String[] parsePermissionNameForResourceAndAction(String permissionName)
    {
        if(permissionName == null || permissionName.isEmpty()) {
            // ???
            log.warning("Invalid permissionName = " + permissionName);
            return null;
        }
        int idx = permissionName.indexOf("::");   // TBD: "instance" portion ????
        if(idx > 0) {
            String resource = permissionName.substring(0, idx);
            String action = permissionName.substring(idx + 2);
            return new String[]{resource, action};
        } else {
            // ???
            return new String[]{permissionName, null};
        }

    }
    
}
