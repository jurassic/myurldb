package com.cannyurl.app.resource;

import java.net.URI;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.cannyurl.af.auth.TwoLeggedOAuthProvider;
import com.cannyurl.af.bean.ShortLinkBean;
import com.cannyurl.af.resource.impl.BaseResourceImpl;
import com.cannyurl.app.service.ShortLinkAppService;
import com.cannyurl.app.util.ShortLinkUtil;
import com.myurldb.ws.BaseException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.exception.InternalServerErrorException;
import com.myurldb.ws.exception.RequestConflictException;
import com.myurldb.ws.exception.RequestForbiddenException;
import com.myurldb.ws.exception.ServiceUnavailableException;
import com.myurldb.ws.exception.resource.BaseResourceException;
import com.myurldb.ws.resource.exception.BadRequestRsException;
import com.myurldb.ws.resource.exception.InternalServerErrorRsException;
import com.myurldb.ws.resource.exception.RequestConflictRsException;
import com.myurldb.ws.resource.exception.RequestForbiddenRsException;
import com.myurldb.ws.resource.exception.ServiceUnavailableRsException;
import com.myurldb.ws.resource.exception.UnauthorizedRsException;
import com.myurldb.ws.stub.ShortLinkStub;
import com.sun.jersey.api.core.HttpContext;


// TBD:
// Old implementation
// Need update...



@Path("/shortlink/")
public class ShortLinkAppResource extends BaseResourceImpl
{
    private static final Logger log = Logger.getLogger(ShortLinkAppResource.class.getName());

    private UriInfo uriInfo;
    private HttpHeaders httpHeaders;
    private Request request;
    private String resourceUri;
    private boolean isRunningOnDevel;
    private HttpContext httpContext;

    private ShortLinkAppService shortLinkService = null;

    public ShortLinkAppResource(@Context javax.ws.rs.core.UriInfo uriInfo, @Context javax.ws.rs.core.HttpHeaders httpHeaders, @Context javax.ws.rs.core.Request request, @Context ServletContext servletContext, @Context HttpContext httpContext)
    {
        this.uriInfo = uriInfo;
        this.httpHeaders = httpHeaders;
        this.request = request;
        this.resourceUri = uriInfo.getAbsolutePath().toString();

        String serverInfo = servletContext.getServerInfo();
        log.info("ServerInfo = " + serverInfo);
        if(serverInfo != null && serverInfo.contains("Development")) {
            isRunningOnDevel = true;
        } else {
            isRunningOnDevel = false;
        }

        this.httpContext = httpContext;
    }

    private ShortLinkAppService getShortLinkAppService()
    {
        if(shortLinkService == null) {
            shortLinkService = new ShortLinkAppService();
        }
        return shortLinkService;
    }

    
    // TBD: Need updates. See the new function (method signatures, etc.), constructShortUrl() below...
    // Non-REST api for convenience. Should have been POST.
    @Deprecated
    @GET
    @Produces({ MediaType.TEXT_PLAIN })
    public Response createShortUrl(@QueryParam("owner") String owner, 
            @QueryParam("longUrl") String longUrl, 
            @QueryParam("domain") String domain, 
            @QueryParam("tokenType") String tokenType, 
            @QueryParam("displayMessage") String displayMessage) throws BaseResourceException
    {
        try {
            if(longUrl == null || longUrl.isEmpty()) {
                throw new BadRequestRsException("Arg, longUrl, is missing.", resourceUri);
            }

            ShortLinkBean shortLink = new ShortLinkBean();   // ????
            String guid = GUID.generate();
            shortLink.setGuid(guid);
            if(owner != null && !owner.isEmpty()) {
                // TBD: Check if the owner is a valid user guid (eg from User table)???
                shortLink.setOwner(owner);
            }
            // TBD: Url decode longUrl??? (or, is it already decoded??  (--> Looks like it's decoded by Jersey)
            shortLink.setLongUrl(longUrl);
            if(domain != null && !domain.isEmpty()) {
                shortLink.setDomain(domain);
            }
            if(tokenType != null && !tokenType.isEmpty()) {
                shortLink.setTokenType(tokenType);
            }
            if(displayMessage != null && !displayMessage.isEmpty()) {
                // TBD: Url decode here??? (or, is it already decoded??  (--> Looks like it's decoded by Jersey)
                shortLink.setDisplayMessage(displayMessage);
            }

            shortLink = (ShortLinkBean) getShortLinkAppService().constructShortLink(shortLink);
            if(shortLink == null) {
                throw new InternalServerErrorRsException("Failed to create a short URL due to unknown error: longUrl = " + longUrl, resourceUri);                        
            }
            //guid = shortLink.getGuid();
            URI createdUri = URI.create(resourceUri + "/" + guid);
            domain = shortLink.getDomain();
            String token = shortLink.getToken();
            String shortUrl = ShortLinkUtil.buildShortUrl(domain, null, token);
            log.info("A new shortUrl, " + shortUrl + ", successfully created for longUrl," + longUrl);

            return Response.created(createdUri).entity(shortUrl).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    
    @POST
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
    public Response constructShortUrl(
            @QueryParam("longUrl") String longUrl, 
            @QueryParam("user") String user,
            @QueryParam("username") String username,
            @QueryParam("usercode") String usercode,
            @QueryParam("domain") String domain, 
            @QueryParam("domainType") String domainType, 
            @QueryParam("token") String token, 
            @QueryParam("tokenType") String tokenType, 
            @QueryParam("sassyTokenType") String sassyTokenType, 
            @QueryParam("redirectType") String redirectType, 
            @QueryParam("flashDuration") Long flashDuration,
            @QueryParam("shortMessage") String shortMessage) throws BaseResourceException
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            if(isRunningOnDevel && isIgnoreAuth()) {
                log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
            } else {
                // Verify the signature first.
                try {
                    if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                        log.warning("Two-legged OAuth verification failed.");
                        throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                    }
                } catch (BadRequestException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                    throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
                } catch (BaseException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                    throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
                } catch (Exception e) {
                    log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                    throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
                }
            }
        }

        try {
            if(longUrl == null || longUrl.isEmpty()) {
                throw new BadRequestRsException("Arg, longUrl, is missing.", resourceUri);
            }

            ShortLinkBean shortLink = new ShortLinkBean();   // ????
            String guid = GUID.generate();
            shortLink.setGuid(guid);
            shortLink.setLongUrl(longUrl);
            if(user != null && !user.isEmpty()) {
                // TBD: Check if the owner is a valid user guid (eg from User table)???
                shortLink.setOwner(user);
            }
            if(username != null && !username.isEmpty()) {
                shortLink.setUsername(username);
            }
            if(usercode != null && !usercode.isEmpty()) {
                shortLink.setUsercode(usercode);
            }
            if(domain != null && !domain.isEmpty()) {
                shortLink.setDomain(domain);
            }
            if(domainType != null && !domainType.isEmpty()) {
                shortLink.setDomainType(domainType);
            }
            if(token != null && !token.isEmpty()) {
                shortLink.setToken(token);
            }
            if(tokenType != null && !tokenType.isEmpty()) {
                shortLink.setTokenType(tokenType);
            }
            if(sassyTokenType != null && !sassyTokenType.isEmpty()) {
                shortLink.setSassyTokenType(sassyTokenType);
            }
            if(redirectType != null && !redirectType.isEmpty()) {
                shortLink.setRedirectType(redirectType);
            }
            if(flashDuration != null && flashDuration >= 0L) {  // ???
                shortLink.setFlashDuration(flashDuration);
            }
            if(shortMessage != null && !shortMessage.isEmpty()) {
                shortLink.setShortMessage(shortMessage);
            }
            // ...

            shortLink = (ShortLinkBean) getShortLinkAppService().constructShortLink(shortLink);
            if(shortLink == null) {
                throw new InternalServerErrorRsException("Failed to create a short URL due to unknown error: longUrl = " + longUrl, resourceUri);                        
            }
            URI createdUri = URI.create(resourceUri + "/" + guid);
            ShortLinkStub stub = ShortLinkStub.convertBeanToStub(shortLink);
            return Response.created(createdUri).entity(stub).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }


}
