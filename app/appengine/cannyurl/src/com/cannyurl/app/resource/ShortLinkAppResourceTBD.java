package com.cannyurl.app.resource;

import java.net.URI;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.ShortLink;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.exception.InternalServerErrorException;
import com.myurldb.ws.exception.RequestConflictException;
import com.myurldb.ws.exception.RequestForbiddenException;
import com.myurldb.ws.exception.ResourceGoneException;
import com.myurldb.ws.exception.ResourceNotFoundException;
import com.myurldb.ws.exception.ServiceUnavailableException;
import com.myurldb.ws.exception.resource.BaseResourceException;
import com.myurldb.ws.resource.exception.BadRequestRsException;
import com.myurldb.ws.resource.exception.InternalServerErrorRsException;
import com.myurldb.ws.resource.exception.RequestConflictRsException;
import com.myurldb.ws.resource.exception.RequestForbiddenRsException;
import com.myurldb.ws.resource.exception.ResourceGoneRsException;
import com.myurldb.ws.resource.exception.ResourceNotFoundRsException;
import com.myurldb.ws.resource.exception.ServiceUnavailableRsException;
import com.myurldb.ws.stub.ShortLinkStub;
import com.cannyurl.af.bean.ShortLinkBean;
import com.cannyurl.af.resource.impl.ShortLinkResourceImpl;
import com.cannyurl.af.service.manager.ServiceManager;
import com.cannyurl.app.service.ShortLinkAppService;
import com.sun.jersey.api.core.HttpContext;


// TBD:
// Old implementation
// Need update...
// Or, remove this
// ....


// Note: No authentication/authorization for this WS endpoint.
@Path("/shortlink_tbd/")
public class ShortLinkAppResourceTBD
{
    private static final Logger log = Logger.getLogger(ShortLinkAppResourceTBD.class.getName());

    private UriInfo uriInfo;
    private HttpHeaders httpHeaders;
    private Request request;
    private String resourceUri;
    private boolean isRunningOnDevel;
    private HttpContext httpContext;

    private ShortLinkAppService shortLinkService = null;

    public ShortLinkAppResourceTBD(@Context javax.ws.rs.core.UriInfo uriInfo, @Context javax.ws.rs.core.HttpHeaders httpHeaders, @Context javax.ws.rs.core.Request request, @Context ServletContext servletContext, @Context HttpContext httpContext)
    {
        this.uriInfo = uriInfo;
        this.httpHeaders = httpHeaders;
        this.request = request;
        this.resourceUri = uriInfo.getAbsolutePath().toString();

        String serverInfo = servletContext.getServerInfo();
        log.info("ServerInfo = " + serverInfo);
        if(serverInfo != null && serverInfo.contains("Development")) {
            isRunningOnDevel = true;
        } else {
            isRunningOnDevel = false;
        }        

        this.httpContext = httpContext;
    }

    private ShortLinkAppService getShortLinkAppService()
    {
        if(shortLinkService == null) {
            shortLinkService = new ShortLinkAppService();
        }
        return shortLinkService;
    }

    // Returns one of the shortLinks, if any.
    // TBD: Actually, this use case does not make sense when user == null. The returned ShortLink might have non-empty displayMessage which the user may not like...
    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    public Response findShortLinkByLongUrl(@QueryParam("longUrl") String longUrl, @QueryParam("owner") String owner, @QueryParam("create") Boolean create) throws BaseResourceException
    {
        try {
            if(longUrl == null || longUrl.isEmpty()) {
                throw new BadRequestRsException("Arg, longUrl, is missing.", resourceUri);
            }

            String filter = "longUrl=='" + longUrl + "'";
            if(owner != null && !owner.isEmpty()) {
                filter += " && owner=='" + owner + "'";
            }
            // else "owner == null" ???
            String ordering = "createdTime desc";
            Long offset = 0L;
            Integer count = 1;
            List<ShortLink> beans = getShortLinkAppService().findShortLinks(filter, ordering, null, null, null, null, offset, count);
            
            ShortLinkStub stub = null;
            if(beans != null && beans.size() > 0) {
                stub = ShortLinkStub.convertBeanToStub(beans.get(0));
                return Response.ok(stub).build();
            } else {
                if(create != null && create == true) {
                    stub = new ShortLinkStub();   // ????
                    String guid = GUID.generate();
                    stub.setGuid(guid);
                    stub.setLongUrl(longUrl);
                    if(owner != null && !owner.isEmpty()) {
                        stub.setOwner(owner);
                    }
                    return constructShortLink(stub);
                } else {
                    // TBD: Throw 404 ??? Or, just return en empty/null object???
                    throw new ResourceNotFoundRsException("ShortLink not found for the given longUrl, " + longUrl, resourceUri);
                }
            }
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }
    
    @POST
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
    public Response constructShortLink(ShortLinkStub shortLink) throws BaseResourceException
    {
        try {
            String longUrl = shortLink.getLongUrl();
            if(longUrl == null || longUrl.isEmpty()) {
                throw new BadRequestRsException("Arg, longUrl, is missing.", resourceUri);
            }

            ShortLinkBean bean = ShortLinkResourceImpl.convertShortLinkStubToBean(shortLink);
            bean = (ShortLinkBean) getShortLinkAppService().constructShortLink(bean);
            if(bean == null) {
                throw new InternalServerErrorRsException("Failed to create a short URL due to unknown error: longUrl = " + longUrl, resourceUri);                        
            }
            String guid = bean.getGuid();
            URI createdUri = URI.create(resourceUri + "/" + guid);
            shortLink = ShortLinkStub.convertBeanToStub(bean);
            return Response.created(createdUri).entity(shortLink).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    // TBD: Does update make sense, in the current use cases (eg, for essentially anonymous users)????
    @PUT
    @Path("{guid : [0-9a-fA-F\\-]+}")
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
    public Response refreshShortLink(@PathParam("guid") String guid, ShortLinkStub shortLink) throws BaseResourceException
    {
        try {
            if(shortLink == null || !guid.equals(shortLink.getGuid())) {
                log.log(Level.WARNING, "Path param guid = " + guid + " is different from shortLink guid = " + shortLink.getGuid());
                throw new RequestForbiddenException("Failed to refresh the shortLink with guid = " + guid);
            }
            ShortLinkBean bean = ShortLinkResourceImpl.convertShortLinkStubToBean(shortLink);
            bean = (ShortLinkBean) getShortLinkAppService().refreshShortLink(bean);
            if(bean == null) {
                log.log(Level.WARNING, "Failed to refresh the shortLink with guid = " + guid);
                throw new InternalServerErrorException("Failed to refresh the shortLink with guid = " + guid);
            }
            shortLink = ShortLinkStub.convertBeanToStub(bean);
            return Response.ok(shortLink).build();  // ???
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

}
