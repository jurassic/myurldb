package com.cannyurl.app.sassy;

import java.util.logging.Logger;

import com.cannyurl.app.util.ConfigUtil;
import com.cannyurl.common.SassyTokenType;
import com.myurldb.ws.core.GUID;


///////////////////////////////////////
//
// (Note that we might be overcounting...)
//
// [1] Mixed case
// length = 4 --> (4x128) x (4x128) + (16x5120) == 344 064 
// length = 5 --> (4x128) x (8x1024) x 2 == 8 388 608
// length = 6 --> (4x128) x (4x128) x (4x128) + (8x1024) x (8x1024) + (4x128) x (16x5120) x 2 == 285 212 672
// length = 7 --> (4x128) x (4x128) x (8x1024) x 3 + (8x1024) x (16x5120) x 2 == 7 784 628 224
// length = 8 --> (4x128)^4 + (4x128) x (8x1024)^2 x 3 + (4x128)^2 x (16x5120) x 3 ==
// length = 9 --> (4x128)^3 x (8x1024) x 4 + (8x1024)^3 + (4x128) x (8x1024) x (16x5120) x 6 == 
//
// [2] Lower case + capitalization
// length = 4 --> (2x128) x (2x128) + (2x5120) == 75 776
// length = 5 --> (2x128) x (2x1024) x 2 == 1 048 576
// length = 6 --> (2x128) x (2x128) x (2x128) + (2x1024) x (2x1024) + (2x128) x (2x5120) x 2 == 26 214 400
// length = 7 --> (2x128) x (2x128) x (2x1024) x 3 + (2x1024) x (2x5120) x 2 == 465 567 744
// length = 8 --> (2x128)^4 + (2x128) x (2x1024)^2 x 3 + (2x128)^2 x (2x5120) x 3 ==
// length = 9 --> (2x128)^3 x (2x1024) x 4 + (2x1024)^3 + (2x128) x (2x1024) x (2x5120) x 6 == 
//
// [3] Lower case only 
// length = 4 --> (128) x (128) + (5120) == 21 504
// length = 5 --> (128) x (1024) x 2 == 262 144
// length = 6 --> (128) x (128) x (128) + (1024) x (1024) + (128) x (5120) x 2 == 4 456 448
// length = 7 --> (128) x (128) x (1024) x 3 + (1024) x (5120) x 2 == 60 817 408
// length = 8 --> (128)^4 + (128) x (1024)^2 x 3 + (128)^2 x (5120) x 3 ==
// length = 9 --> (128)^3 x (1024) x 4 + (1024)^3 + (128) x (1024) x (5120) x 6 == 
//
// (4x128==512 2x128==256)
// (8x1024==8192 2x1024==2048)
// (16x5120==81920 2x5120==10240)
///////////////////////////////////////



// TBD: Lower case only... ???
public final class SassyUrlUtil
{
    private static final Logger log = Logger.getLogger(SassyUrlUtil.class.getName());

    private SassyUrlUtil() {}

    
    // Returns the default length for a given sassy token type.
    public static int getDefaultSassyTokenLength(String sassyTokenType)
    {
        int length;
        if(SassyTokenType.TYPE_MIXEDCASE.equals(sassyTokenType)) {
            length = ConfigUtil.getSassyTokenMixedcaseLength();
        } else if(SassyTokenType.TYPE_LOWERCAPS.equals(sassyTokenType)) {
            length = ConfigUtil.getSassyTokenLowercapsLength();
        } else if(SassyTokenType.TYPE_LOWERCASE.equals(sassyTokenType)) {
            length = ConfigUtil.getSassyTokenLowercaseLength();
        } else if(SassyTokenType.TYPE_LONGTOKEN.equals(sassyTokenType)) {
            length = ConfigUtil.getSassyTokenLongtokenLength();
        } else {
            length = ConfigUtil.getSassyTokenMixedcaseLength();
        }
        return length;
    }

    // TBD:
    // Length == 3 ~ 10
    public static String generateSassyUrlToken(String sassyTokenType)
    {
        int length = getDefaultSassyTokenLength(sassyTokenType);
        return generateSassyUrlToken(sassyTokenType, length);
    }
    public static String generateSassyUrlToken(String sassyTokenType, int length)
    {
        if(length <= 3) {
            return generateSassyUrlToken3(sassyTokenType);
        } else if(length == 4) {
            return generateSassyUrlToken4(sassyTokenType);
        } else if(length == 5) {
            return generateSassyUrlToken5(sassyTokenType);
        } else if(length == 6) {
            return generateSassyUrlToken6(sassyTokenType);
        } else if(length == 7) {
            return generateSassyUrlToken7(sassyTokenType);
        } else if(length == 8) {
            return generateSassyUrlToken8(sassyTokenType);
        } else if(length == 9) {
            return generateSassyUrlToken9(sassyTokenType);
        } else {  // 10 or longer
            return generateSassyUrlToken10(sassyTokenType);
        }
    }


    public static String generateSassyUrlToken3(String sassyTokenType)
    {
        log.finer("generateSassyUrlToken3() called");

        // Guid format: 8-4-4-4-12.
        String guid = GUID.generate();
//        String firstDigit = guid.substring(0, 1);   // First 4 bits.
//        int firstHex = Integer.parseInt(firstDigit, 16);
//        String secondDigit = guid.substring(1, 2);  // Second 4 bits.
//        int secondHex = Integer.parseInt(secondDigit, 16);
        String thirdDigit = guid.substring(2, 3);   // Thrid 4 bits.
        int thirdHex = Integer.parseInt(thirdDigit, 16);
//        String fourthDigit = guid.substring(3, 4);  // Fourth 4 bits.
//        int fourthHex = Integer.parseInt(fourthDigit, 16);
//        String fifthDigit = guid.substring(4, 5);   // Fifth 4 bits.
//        int fifthHex = Integer.parseInt(fifthDigit, 16);
//        String sixthDigit = guid.substring(5, 6);   // Sixth 4 bits.
//        int sixthHex = Integer.parseInt(sixthDigit, 16);
//        String seventhDigit = guid.substring(6, 7); // Seventh 4 bits.
//        int seventhHex = Integer.parseInt(seventhDigit, 16);
//        String eightthDigit = guid.substring(7, 8); // Eighth 4 bits.
//        int eightthHex = Integer.parseInt(eightthDigit, 16);
//        String ninthDigit = guid.substring(9, 10);  // Ninth 4 bits.
//        int ninthHex = Integer.parseInt(ninthDigit, 16);
//        String tenthDigit = guid.substring(10, 11); // Tenth 4 bits.
//        int tenthHex = Integer.parseInt(tenthDigit, 16);
//        String eleventhDigit = guid.substring(11, 12);  // Eleventh 4 bits.
//        int eleventhHex = Integer.parseInt(eleventhDigit, 16);
//        String twelfthDigit = guid.substring(12, 13);   // Twelfth 4 bits.
//        int twelfthHex = Integer.parseInt(twelfthDigit, 16);
        
//        String twoLetterWord1 = guid.substring(16, 18);            // 2 chars
//        int twoLetterHex1 = Integer.parseInt(twoLetterWord1, 16);  // 
//        String twoLetterWord2 = guid.substring(19, 21);            // 2 chars
//        int twoLetterHex2 = Integer.parseInt(twoLetterWord2, 16);  // 
//        String twoLetterWord3 = guid.substring(21, 23);            // 2 chars
//        int twoLetterHex3 = Integer.parseInt(twoLetterWord3, 16);  // 
//        String twoLetterWord4 = guid.substring(24, 26);            // 2 chars
//        int twoLetterHex4 = Integer.parseInt(twoLetterWord4, 16);  // 
        String threeLetterWord1 = guid.substring(27, 30);          // 3 chars
        int threeLetterHex1 = Integer.parseInt(threeLetterWord1, 16); 
//        String threeLetterWord2 = guid.substring(30, 33);          // 3 chars
//        int threeLetterHex2 = Integer.parseInt(threeLetterWord2, 16); 
//        String threeLetterWord3 = guid.substring(33, 36);          // 3 chars
//        int threeLetterHex3 = Integer.parseInt(threeLetterWord3, 16); 
        
//        String fourLetterWord1 = guid.substring(0, 4);               // 4 chars
//        int fourLetterHex1 = Integer.parseInt(fourLetterWord1, 16);  // 
//        String fourLetterWord2 = guid.substring(4, 8);               // 4 chars
//        int fourLetterHex2 = Integer.parseInt(fourLetterWord2, 16);  // 

        String threeLetters1 = ThreeLetterWords.getInstance(sassyTokenType).getWord(threeLetterHex1, thirdHex);
        
        StringBuilder sb = new StringBuilder();
        sb.append(threeLetters1);
        String token = sb.toString();
        log.finer("token = " + token);
        return token;
    }

    public static String generateSassyUrlToken4(String sassyTokenType)
    {
        log.finer("generateSassyUrlToken4() called");

        // Guid format: 8-4-4-4-12.
        String guid = GUID.generate();
        String firstDigit = guid.substring(0, 1);   // First 4 bits.
        int firstHex = Integer.parseInt(firstDigit, 16);
        String secondDigit = guid.substring(1, 2);  // Second 4 bits.
        int secondHex = Integer.parseInt(secondDigit, 16);
        String thirdDigit = guid.substring(2, 3);   // Thrid 4 bits.
        int thirdHex = Integer.parseInt(thirdDigit, 16);
//        String fourthDigit = guid.substring(3, 4);  // Fourth 4 bits.
//        int fourthHex = Integer.parseInt(fourthDigit, 16);
//        String fifthDigit = guid.substring(4, 5);   // Fifth 4 bits.
//        int fifthHex = Integer.parseInt(fifthDigit, 16);
//        String sixthDigit = guid.substring(5, 6);   // Sixth 4 bits.
//        int sixthHex = Integer.parseInt(sixthDigit, 16);
//        String seventhDigit = guid.substring(6, 7); // Seventh 4 bits.
//        int seventhHex = Integer.parseInt(seventhDigit, 16);
//        String eightthDigit = guid.substring(7, 8); // Eighth 4 bits.
//        int eightthHex = Integer.parseInt(eightthDigit, 16);
        String ninthDigit = guid.substring(9, 10);  // Ninth 4 bits.
        int ninthHex = Integer.parseInt(ninthDigit, 16);
//        String tenthDigit = guid.substring(10, 11); // Tenth 4 bits.
//        int tenthHex = Integer.parseInt(tenthDigit, 16);
//        String eleventhDigit = guid.substring(11, 12);  // Eleventh 4 bits.
//        int eleventhHex = Integer.parseInt(eleventhDigit, 16);
//        String twelfthDigit = guid.substring(12, 13);   // Twelfth 4 bits.
//        int twelfthHex = Integer.parseInt(twelfthDigit, 16);

        String twoLetterWord1 = guid.substring(16, 18);            // 2 chars
        int twoLetterHex1 = Integer.parseInt(twoLetterWord1, 16);  // 
        String twoLetterWord2 = guid.substring(19, 21);            // 2 chars
        int twoLetterHex2 = Integer.parseInt(twoLetterWord2, 16);  // 
//        String twoLetterWord3 = guid.substring(21, 23);            // 2 chars
//        int twoLetterHex3 = Integer.parseInt(twoLetterWord3, 16);  // 
//        String twoLetterWord4 = guid.substring(24, 26);            // 2 chars
//        int twoLetterHex4 = Integer.parseInt(twoLetterWord4, 16);  // 
//        String threeLetterWord1 = guid.substring(27, 30);          // 3 chars
//        int threeLetterHex1 = Integer.parseInt(threeLetterWord1, 16); 
//        String threeLetterWord2 = guid.substring(30, 33);          // 3 chars
//        int threeLetterHex2 = Integer.parseInt(threeLetterWord2, 16); 
//        String threeLetterWord3 = guid.substring(33, 36);          // 3 chars
//        int threeLetterHex3 = Integer.parseInt(threeLetterWord3, 16); 

        String fourLetterWord1 = guid.substring(0, 4);               // 4 chars
        int fourLetterHex1 = Integer.parseInt(fourLetterWord1, 16);  // 
//        String fourLetterWord2 = guid.substring(4, 8);               // 4 chars
//        int fourLetterHex2 = Integer.parseInt(fourLetterWord2, 16);  // 
        
        int mod = firstHex % 2;
        StringBuilder sb = new StringBuilder();
        if(mod == 0) {
            String twoLetters1 = TwoLetterWords.getInstance(sassyTokenType).getWord(twoLetterHex1, secondHex);
            String twoLetters2 = TwoLetterWords.getInstance(sassyTokenType).getWord(twoLetterHex2, thirdHex);
            sb.append(twoLetters1);
            sb.append(twoLetters2);
        } else {
            String fourLetters1 = FourLetterWords.getInstance(sassyTokenType).getWord(fourLetterHex1, ninthHex);            
            sb.append(fourLetters1);
        }        
        String token = sb.toString();
        log.finer("token = " + token);
        return token;
    }

    public static String generateSassyUrlToken5(String sassyTokenType)
    {
        log.finer("generateSassyUrlToken5() called");

        // Guid format: 8-4-4-4-12.
        String guid = GUID.generate();
        String firstDigit = guid.substring(0, 1);   // First 4 bits.
        int firstHex = Integer.parseInt(firstDigit, 16);
        String secondDigit = guid.substring(1, 2);  // Second 4 bits.
        int secondHex = Integer.parseInt(secondDigit, 16);
        String thirdDigit = guid.substring(2, 3);   // Thrid 4 bits.
        int thirdHex = Integer.parseInt(thirdDigit, 16);
//        String fourthDigit = guid.substring(3, 4);  // Fourth 4 bits.
//        int fourthHex = Integer.parseInt(fourthDigit, 16);
//        String fifthDigit = guid.substring(4, 5);   // Fifth 4 bits.
//        int fifthHex = Integer.parseInt(fifthDigit, 16);
//        String sixthDigit = guid.substring(5, 6);   // Sixth 4 bits.
//        int sixthHex = Integer.parseInt(sixthDigit, 16);
//        String seventhDigit = guid.substring(6, 7); // Seventh 4 bits.
//        int seventhHex = Integer.parseInt(seventhDigit, 16);
//        String eightthDigit = guid.substring(7, 8); // Eighth 4 bits.
//        int eightthHex = Integer.parseInt(eightthDigit, 16);
//        String ninthDigit = guid.substring(9, 10);  // Ninth 4 bits.
//        int ninthHex = Integer.parseInt(ninthDigit, 16);
//        String tenthDigit = guid.substring(10, 11); // Tenth 4 bits.
//        int tenthHex = Integer.parseInt(tenthDigit, 16);
//        String eleventhDigit = guid.substring(11, 12);  // Eleventh 4 bits.
//        int eleventhHex = Integer.parseInt(eleventhDigit, 16);
//        String twelfthDigit = guid.substring(12, 13);   // Twelfth 4 bits.
//        int twelfthHex = Integer.parseInt(twelfthDigit, 16);
        
        String twoLetterWord1 = guid.substring(16, 18);            // 2 chars
        int twoLetterHex1 = Integer.parseInt(twoLetterWord1, 16);  // 
//        String twoLetterWord2 = guid.substring(19, 21);            // 2 chars
//        int twoLetterHex2 = Integer.parseInt(twoLetterWord2, 16);  // 
//        String twoLetterWord3 = guid.substring(21, 23);            // 2 chars
//        int twoLetterHex3 = Integer.parseInt(twoLetterWord3, 16);  // 
//        String twoLetterWord4 = guid.substring(24, 26);            // 2 chars
//        int twoLetterHex4 = Integer.parseInt(twoLetterWord4, 16);  // 
        String threeLetterWord1 = guid.substring(27, 30);          // 3 chars
        int threeLetterHex1 = Integer.parseInt(threeLetterWord1, 16); 
//        String threeLetterWord2 = guid.substring(30, 33);          // 3 chars
//        int threeLetterHex2 = Integer.parseInt(threeLetterWord2, 16); 
//        String threeLetterWord3 = guid.substring(33, 36);          // 3 chars
//        int threeLetterHex3 = Integer.parseInt(threeLetterWord3, 16); 
        
//        String fourLetterWord1 = guid.substring(0, 4);               // 4 chars
//        int fourLetterHex1 = Integer.parseInt(fourLetterWord1, 16);  // 
//        String fourLetterWord2 = guid.substring(4, 8);               // 4 chars
//        int fourLetterHex2 = Integer.parseInt(fourLetterWord2, 16);  // 

        String twoLetters1 = TwoLetterWords.getInstance(sassyTokenType).getWord(twoLetterHex1, secondHex);
        String threeLetters1 = ThreeLetterWords.getInstance(sassyTokenType).getWord(threeLetterHex1, thirdHex);
        
        int mod = firstHex % 2;
        StringBuilder sb = new StringBuilder();
        if(mod == 0) {
            sb.append(twoLetters1);
            sb.append(threeLetters1);
        } else {
            sb.append(threeLetters1);
            sb.append(twoLetters1);
        }        
        String token = sb.toString();
        log.finer("token = " + token);
        return token;
    }

    public static String generateSassyUrlToken6(String sassyTokenType)
    {
        log.finer("generateSassyUrlToken6() called");

        // Guid format: 8-4-4-4-12.
        String guid = GUID.generate();
        String firstDigit = guid.substring(0, 1);   // First 4 bits.
        int firstHex = Integer.parseInt(firstDigit, 16);
        String secondDigit = guid.substring(1, 2);  // Second 4 bits.
        int secondHex = Integer.parseInt(secondDigit, 16);
        String thirdDigit = guid.substring(2, 3);   // Thrid 4 bits.
        int thirdHex = Integer.parseInt(thirdDigit, 16);
        String fourthDigit = guid.substring(3, 4);  // Fourth 4 bits.
        int fourthHex = Integer.parseInt(fourthDigit, 16);
        String fifthDigit = guid.substring(4, 5);   // Fifth 4 bits.
        int fifthHex = Integer.parseInt(fifthDigit, 16);
        String sixthDigit = guid.substring(5, 6);   // Sixth 4 bits.
        int sixthHex = Integer.parseInt(sixthDigit, 16);
//        String seventhDigit = guid.substring(6, 7); // Seventh 4 bits.
//        int seventhHex = Integer.parseInt(seventhDigit, 16);
//        String eightthDigit = guid.substring(7, 8); // Eighth 4 bits.
//        int eightthHex = Integer.parseInt(eightthDigit, 16);
        String ninthDigit = guid.substring(9, 10);  // Ninth 4 bits.
        int ninthHex = Integer.parseInt(ninthDigit, 16);
//        String tenthDigit = guid.substring(10, 11); // Tenth 4 bits.
//        int tenthHex = Integer.parseInt(tenthDigit, 16);
//        String eleventhDigit = guid.substring(11, 12);  // Eleventh 4 bits.
//        int eleventhHex = Integer.parseInt(eleventhDigit, 16);
//        String twelfthDigit = guid.substring(12, 13);   // Twelfth 4 bits.
//        int twelfthHex = Integer.parseInt(twelfthDigit, 16);
        
        String twoLetterWord1 = guid.substring(16, 18);            // 2 chars
        int twoLetterHex1 = Integer.parseInt(twoLetterWord1, 16);  // 
        String twoLetterWord2 = guid.substring(19, 21);            // 2 chars
        int twoLetterHex2 = Integer.parseInt(twoLetterWord2, 16);  // 
        String twoLetterWord3 = guid.substring(21, 23);            // 2 chars
        int twoLetterHex3 = Integer.parseInt(twoLetterWord3, 16);  // 
//        String twoLetterWord4 = guid.substring(24, 26);            // 2 chars
//        int twoLetterHex4 = Integer.parseInt(twoLetterWord4, 16);  // 
        String threeLetterWord1 = guid.substring(27, 30);          // 3 chars
        int threeLetterHex1 = Integer.parseInt(threeLetterWord1, 16); 
        String threeLetterWord2 = guid.substring(30, 33);          // 3 chars
        int threeLetterHex2 = Integer.parseInt(threeLetterWord2, 16); 
//        String threeLetterWord3 = guid.substring(33, 36);          // 3 chars
//        int threeLetterHex3 = Integer.parseInt(threeLetterWord3, 16); 

        String fourLetterWord1 = guid.substring(0, 4);               // 4 chars
        int fourLetterHex1 = Integer.parseInt(fourLetterWord1, 16);  // 
//        String fourLetterWord2 = guid.substring(4, 8);               // 4 chars
//        int fourLetterHex2 = Integer.parseInt(fourLetterWord2, 16);  // 

        int mod = firstHex % 4;
        StringBuilder sb = new StringBuilder();
        if(mod == 0) {
            String twoLetters1 = TwoLetterWords.getInstance(sassyTokenType).getWord(twoLetterHex1, secondHex);
            String twoLetters2 = TwoLetterWords.getInstance(sassyTokenType).getWord(twoLetterHex2, thirdHex);
            String twoLetters3 = TwoLetterWords.getInstance(sassyTokenType).getWord(twoLetterHex3, fourthHex);
            sb.append(twoLetters1);
            sb.append(twoLetters2);
            sb.append(twoLetters3);
        } else if(mod == 1) {
            String threeLetters1 = ThreeLetterWords.getInstance(sassyTokenType).getWord(threeLetterHex1, fifthHex);
            String threeLetters2 = ThreeLetterWords.getInstance(sassyTokenType).getWord(threeLetterHex2, sixthHex);
            sb.append(threeLetters1);
            sb.append(threeLetters2);
        } else {
            String twoLetters1 = TwoLetterWords.getInstance(sassyTokenType).getWord(twoLetterHex1, secondHex);
            String fourLetters1 = FourLetterWords.getInstance(sassyTokenType).getWord(fourLetterHex1, ninthHex);
            if(mod == 2) {
                sb.append(twoLetters1);
                sb.append(fourLetters1);
            } else {
                sb.append(fourLetters1);
                sb.append(twoLetters1);
            }
        }
        String token = sb.toString();
        log.finer("token = " + token);
        return token;
    }

    public static String generateSassyUrlToken7(String sassyTokenType)
    {
        log.finer("generateSassyUrlToken7() called");

        // Guid format: 8-4-4-4-12.
        String guid = GUID.generate();
        String firstDigit = guid.substring(0, 1);   // First 4 bits.
        int firstHex = Integer.parseInt(firstDigit, 16);
        String secondDigit = guid.substring(1, 2);  // Second 4 bits.
        int secondHex = Integer.parseInt(secondDigit, 16);
        String thirdDigit = guid.substring(2, 3);   // Thrid 4 bits.
        int thirdHex = Integer.parseInt(thirdDigit, 16);
        String fourthDigit = guid.substring(3, 4);  // Fourth 4 bits.
        int fourthHex = Integer.parseInt(fourthDigit, 16);
//        String fifthDigit = guid.substring(4, 5);   // Fifth 4 bits.
//        int fifthHex = Integer.parseInt(fifthDigit, 16);
//        String sixthDigit = guid.substring(5, 6);   // Sixth 4 bits.
//        int sixthHex = Integer.parseInt(sixthDigit, 16);
//        String seventhDigit = guid.substring(6, 7); // Seventh 4 bits.
//        int seventhHex = Integer.parseInt(seventhDigit, 16);
//        String eightthDigit = guid.substring(7, 8); // Eighth 4 bits.
//        int eightthHex = Integer.parseInt(eightthDigit, 16);
        String ninthDigit = guid.substring(9, 10);  // Ninth 4 bits.
        int ninthHex = Integer.parseInt(ninthDigit, 16);
//        String tenthDigit = guid.substring(10, 11); // Tenth 4 bits.
//        int tenthHex = Integer.parseInt(tenthDigit, 16);
//        String eleventhDigit = guid.substring(11, 12);  // Eleventh 4 bits.
//        int eleventhHex = Integer.parseInt(eleventhDigit, 16);
//        String twelfthDigit = guid.substring(12, 13);   // Twelfth 4 bits.
//        int twelfthHex = Integer.parseInt(twelfthDigit, 16);
        
        String twoLetterWord1 = guid.substring(16, 18);            // 2 chars
        int twoLetterHex1 = Integer.parseInt(twoLetterWord1, 16);  // 
        String twoLetterWord2 = guid.substring(19, 21);            // 2 chars
        int twoLetterHex2 = Integer.parseInt(twoLetterWord2, 16);  // 
//        String twoLetterWord3 = guid.substring(21, 23);            // 2 chars
//        int twoLetterHex3 = Integer.parseInt(twoLetterWord3, 16);  // 
//        String twoLetterWord4 = guid.substring(24, 26);            // 2 chars
//        int twoLetterHex4 = Integer.parseInt(twoLetterWord4, 16);  // 
        String threeLetterWord1 = guid.substring(27, 30);          // 3 chars
        int threeLetterHex1 = Integer.parseInt(threeLetterWord1, 16); 
//        String threeLetterWord2 = guid.substring(30, 33);          // 3 chars
//        int threeLetterHex2 = Integer.parseInt(threeLetterWord2, 16); 
//        String threeLetterWord3 = guid.substring(33, 36);          // 3 chars
//        int threeLetterHex3 = Integer.parseInt(threeLetterWord3, 16); 
        
        String fourLetterWord1 = guid.substring(0, 4);               // 4 chars
        int fourLetterHex1 = Integer.parseInt(fourLetterWord1, 16);  // 
//        String fourLetterWord2 = guid.substring(4, 8);               // 4 chars
//        int fourLetterHex2 = Integer.parseInt(fourLetterWord2, 16);  // 

        int mod = firstHex % 5;
        StringBuilder sb = new StringBuilder();
        if(mod < 3) {
            String twoLetters1 = TwoLetterWords.getInstance(sassyTokenType).getWord(twoLetterHex1, secondHex);
            String twoLetters2 = TwoLetterWords.getInstance(sassyTokenType).getWord(twoLetterHex2, thirdHex);
            String threeLetters1 = ThreeLetterWords.getInstance(sassyTokenType).getWord(threeLetterHex1, fourthHex);            
            if(mod == 0) {  // Note: 0~15  --> 0 has a higher weight...
                sb.append(twoLetters1);
                sb.append(threeLetters1);
                sb.append(twoLetters2);
            } else if(mod == 1) {
                sb.append(twoLetters1);
                sb.append(twoLetters2);
                sb.append(threeLetters1);
            } else {
                sb.append(threeLetters1);
                sb.append(twoLetters1);
                sb.append(twoLetters2);
            }
        } else {
            String threeLetters1 = ThreeLetterWords.getInstance(sassyTokenType).getWord(threeLetterHex1, fourthHex);            
            String fourLetters1 = FourLetterWords.getInstance(sassyTokenType).getWord(fourLetterHex1, ninthHex);
            if(mod == 3) {
                sb.append(fourLetters1);
                sb.append(threeLetters1);
            } else {
                sb.append(threeLetters1);
                sb.append(fourLetters1);
            }
        }
        String token = sb.toString();
        log.finer("token = " + token);
        return token;
    }    

    public static String generateSassyUrlToken8(String sassyTokenType)
    {
        log.finer("generateSassyUrlToken8() called");

        // Guid format: 8-4-4-4-12.
        String guid = GUID.generate();
        String firstDigit = guid.substring(0, 1);   // First 4 bits.
        int firstHex = Integer.parseInt(firstDigit, 16);
        String secondDigit = guid.substring(1, 2);  // Second 4 bits.
        int secondHex = Integer.parseInt(secondDigit, 16);
        String thirdDigit = guid.substring(2, 3);   // Thrid 4 bits.
        int thirdHex = Integer.parseInt(thirdDigit, 16);
        String fourthDigit = guid.substring(3, 4);  // Fourth 4 bits.
        int fourthHex = Integer.parseInt(fourthDigit, 16);
        String fifthDigit = guid.substring(4, 5);   // Fifth 4 bits.
        int fifthHex = Integer.parseInt(fifthDigit, 16);
        String sixthDigit = guid.substring(5, 6);   // Sixth 4 bits.
        int sixthHex = Integer.parseInt(sixthDigit, 16);
        String seventhDigit = guid.substring(6, 7); // Seventh 4 bits.
        int seventhHex = Integer.parseInt(seventhDigit, 16);
//        String eightthDigit = guid.substring(7, 8); // Eighth 4 bits.
//        int eightthHex = Integer.parseInt(eightthDigit, 16);
        String ninthDigit = guid.substring(9, 10);  // Ninth 4 bits.
        int ninthHex = Integer.parseInt(ninthDigit, 16);
        String tenthDigit = guid.substring(10, 11); // Tenth 4 bits.
        int tenthHex = Integer.parseInt(tenthDigit, 16);
//        String eleventhDigit = guid.substring(11, 12);  // Eleventh 4 bits.
//        int eleventhHex = Integer.parseInt(eleventhDigit, 16);
//        String twelfthDigit = guid.substring(12, 13);   // Twelfth 4 bits.
//        int twelfthHex = Integer.parseInt(twelfthDigit, 16);
        
        String twoLetterWord1 = guid.substring(16, 18);            // 2 chars
        int twoLetterHex1 = Integer.parseInt(twoLetterWord1, 16);  // 
        String twoLetterWord2 = guid.substring(19, 21);            // 2 chars
        int twoLetterHex2 = Integer.parseInt(twoLetterWord2, 16);  // 
        String twoLetterWord3 = guid.substring(21, 23);            // 2 chars
        int twoLetterHex3 = Integer.parseInt(twoLetterWord3, 16);  // 
        String twoLetterWord4 = guid.substring(24, 26);            // 2 chars
        int twoLetterHex4 = Integer.parseInt(twoLetterWord4, 16);  // 
        String threeLetterWord1 = guid.substring(27, 30);          // 3 chars
        int threeLetterHex1 = Integer.parseInt(threeLetterWord1, 16); 
        String threeLetterWord2 = guid.substring(30, 33);          // 3 chars
        int threeLetterHex2 = Integer.parseInt(threeLetterWord2, 16); 
//        String threeLetterWord3 = guid.substring(33, 36);          // 3 chars
//        int threeLetterHex3 = Integer.parseInt(threeLetterWord3, 16); 
        
        String fourLetterWord1 = guid.substring(0, 4);               // 4 chars
        int fourLetterHex1 = Integer.parseInt(fourLetterWord1, 16);  // 
        String fourLetterWord2 = guid.substring(4, 8);               // 4 chars
        int fourLetterHex2 = Integer.parseInt(fourLetterWord2, 16);  // 
        
        int mod = firstHex % 8;
        StringBuilder sb = new StringBuilder();
        if(mod < 4) {
            String twoLetters1 = TwoLetterWords.getInstance(sassyTokenType).getWord(twoLetterHex1, secondHex);
            String threeLetters1 = ThreeLetterWords.getInstance(sassyTokenType).getWord(threeLetterHex1, thirdHex);
            String threeLetters2 = ThreeLetterWords.getInstance(sassyTokenType).getWord(threeLetterHex2, fourthHex);
            if(mod == 0) {
                sb.append(twoLetters1);
                sb.append(threeLetters1);
                sb.append(threeLetters2);
            } else if(mod == 1) {
                sb.append(threeLetters1);
                sb.append(twoLetters1);
                sb.append(threeLetters2);
            } else if(mod == 2) {
                sb.append(threeLetters1);
                sb.append(threeLetters2);
                sb.append(twoLetters1);
            } else {
                String twoLetters2 = TwoLetterWords.getInstance(sassyTokenType).getWord(twoLetterHex2, fifthHex);
                String twoLetters3 = TwoLetterWords.getInstance(sassyTokenType).getWord(twoLetterHex3, sixthHex);
                String twoLetters4 = TwoLetterWords.getInstance(sassyTokenType).getWord(twoLetterHex4, seventhHex);
                sb.append(twoLetters1);
                sb.append(twoLetters2);
                sb.append(twoLetters3);
                sb.append(twoLetters4);
            }
        } else {
            String twoLetters1 = TwoLetterWords.getInstance(sassyTokenType).getWord(twoLetterHex1, secondHex);
            String twoLetters2 = TwoLetterWords.getInstance(sassyTokenType).getWord(twoLetterHex2, thirdHex);
            String fourLetters1 = FourLetterWords.getInstance(sassyTokenType).getWord(fourLetterHex1, ninthHex);
            if(mod == 4) {
                sb.append(twoLetters1);
                sb.append(twoLetters2);
                sb.append(fourLetters1);
            } else if(mod == 5) {
                sb.append(twoLetters1);
                sb.append(fourLetters1);
                sb.append(twoLetters2);
            } else if(mod == 6) {
                sb.append(fourLetters1);
                sb.append(twoLetters1);
                sb.append(twoLetters2);
            } else {
                String fourLetters2 = FourLetterWords.getInstance(sassyTokenType).getWord(fourLetterHex2, tenthHex);
                sb.append(fourLetters1);
                sb.append(fourLetters2);                
            }
        }
        String token = sb.toString();
        log.finer("token = " + token);
        return token;
    }

    public static String generateSassyUrlToken9(String sassyTokenType)
    {
        log.finer("generateSassyUrlToken9() called");

        // Guid format: 8-4-4-4-12.
        String guid = GUID.generate();
        String firstDigit = guid.substring(0, 1);   // First 4 bits.
        int firstHex = Integer.parseInt(firstDigit, 16);
        String secondDigit = guid.substring(1, 2);  // Second 4 bits.
        int secondHex = Integer.parseInt(secondDigit, 16);
        String thirdDigit = guid.substring(2, 3);   // Thrid 4 bits.
        int thirdHex = Integer.parseInt(thirdDigit, 16);
        String fourthDigit = guid.substring(3, 4);  // Fourth 4 bits.
        int fourthHex = Integer.parseInt(fourthDigit, 16);
        String fifthDigit = guid.substring(4, 5);   // Fifth 4 bits.
        int fifthHex = Integer.parseInt(fifthDigit, 16);
        String sixthDigit = guid.substring(5, 6);   // Sixth 4 bits.
        int sixthHex = Integer.parseInt(sixthDigit, 16);
        String seventhDigit = guid.substring(6, 7); // Seventh 4 bits.
        int seventhHex = Integer.parseInt(seventhDigit, 16);
//        String eightthDigit = guid.substring(7, 8); // Eighth 4 bits.
//        int eightthHex = Integer.parseInt(eightthDigit, 16);
        String ninthDigit = guid.substring(9, 10);  // Ninth 4 bits.
        int ninthHex = Integer.parseInt(ninthDigit, 16);
//        String tenthDigit = guid.substring(10, 11); // Tenth 4 bits.
//        int tenthHex = Integer.parseInt(tenthDigit, 16);
//        String eleventhDigit = guid.substring(11, 12);  // Eleventh 4 bits.
//        int eleventhHex = Integer.parseInt(eleventhDigit, 16);
//        String twelfthDigit = guid.substring(12, 13);   // Twelfth 4 bits.
//        int twelfthHex = Integer.parseInt(twelfthDigit, 16);
        
        String twoLetterWord1 = guid.substring(16, 18);            // 2 chars
        int twoLetterHex1 = Integer.parseInt(twoLetterWord1, 16);  // 
        String twoLetterWord2 = guid.substring(19, 21);            // 2 chars
        int twoLetterHex2 = Integer.parseInt(twoLetterWord2, 16);  // 
        String twoLetterWord3 = guid.substring(21, 23);            // 2 chars
        int twoLetterHex3 = Integer.parseInt(twoLetterWord3, 16);  // 
//        String twoLetterWord4 = guid.substring(24, 26);            // 2 chars
//        int twoLetterHex4 = Integer.parseInt(twoLetterWord4, 16);  // 
        String threeLetterWord1 = guid.substring(27, 30);          // 3 chars
        int threeLetterHex1 = Integer.parseInt(threeLetterWord1, 16); 
        String threeLetterWord2 = guid.substring(30, 33);          // 3 chars
        int threeLetterHex2 = Integer.parseInt(threeLetterWord2, 16); 
        String threeLetterWord3 = guid.substring(33, 36);          // 3 chars
        int threeLetterHex3 = Integer.parseInt(threeLetterWord3, 16); 
        
        String fourLetterWord1 = guid.substring(0, 4);               // 4 chars
        int fourLetterHex1 = Integer.parseInt(fourLetterWord1, 16);  // 
//        String fourLetterWord2 = guid.substring(4, 8);               // 4 chars
//        int fourLetterHex2 = Integer.parseInt(fourLetterWord2, 16);  // 

        String twoLetters1 = TwoLetterWords.getInstance(sassyTokenType).getWord(twoLetterHex1, secondHex);
        String threeLetters1 = ThreeLetterWords.getInstance(sassyTokenType).getWord(threeLetterHex1, fifthHex);
        
        int mod = firstHex % 11;
        StringBuilder sb = new StringBuilder();
        if(mod < 5) {   // Note: 0~15 -> 0~10 + 11~15 : First five have twice the weight of the last six.  
            String twoLetters2 = TwoLetterWords.getInstance(sassyTokenType).getWord(twoLetterHex2, thirdHex);
            String twoLetters3 = TwoLetterWords.getInstance(sassyTokenType).getWord(twoLetterHex3, fourthHex);
            if(mod == 0) {
                sb.append(twoLetters1);
                sb.append(twoLetters2);
                sb.append(twoLetters3);
                sb.append(threeLetters1);
            } else if(mod == 1) {
                sb.append(twoLetters1);
                sb.append(twoLetters2);
                sb.append(threeLetters1);
                sb.append(twoLetters3);
            } else if(mod == 2) {
                sb.append(twoLetters1);
                sb.append(threeLetters1);
                sb.append(twoLetters2);
                sb.append(twoLetters3);
            } else if(mod == 3) {
                sb.append(threeLetters1);
                sb.append(twoLetters1);
                sb.append(twoLetters2);
                sb.append(twoLetters3);
            } else {
                String threeLetters2 = ThreeLetterWords.getInstance(sassyTokenType).getWord(threeLetterHex2, sixthHex);
                String threeLetters3 = ThreeLetterWords.getInstance(sassyTokenType).getWord(threeLetterHex3, seventhHex);
                sb.append(threeLetters1);
                sb.append(threeLetters2);
                sb.append(threeLetters3);
            }        
        } else {
            String fourLetters1 = FourLetterWords.getInstance(sassyTokenType).getWord(fourLetterHex1, ninthHex);
            if(mod == 5) {
                sb.append(twoLetters1);
                sb.append(threeLetters1);
                sb.append(fourLetters1);
            } else if(mod == 6) {
                sb.append(twoLetters1);
                sb.append(fourLetters1);
                sb.append(threeLetters1);
            } else if(mod == 7) {
                sb.append(threeLetters1);
                sb.append(twoLetters1);
                sb.append(fourLetters1);
            } else if(mod == 8) {
                sb.append(threeLetters1);
                sb.append(fourLetters1);
                sb.append(twoLetters1);
            } else if(mod == 9) {
                sb.append(fourLetters1);
                sb.append(twoLetters1);
                sb.append(threeLetters1);
            } else {
                sb.append(fourLetters1);
                sb.append(threeLetters1);
                sb.append(twoLetters1);
            }            
        }
        String token = sb.toString();
        log.finer("token = " + token);
        return token;
    }

    public static String generateSassyUrlToken10(String sassyTokenType)
    {
        log.finer("generateSassyUrlToken10() called");

        // Guid format: 8-4-4-4-12.
        String guid = GUID.generate();
        String firstDigit = guid.substring(0, 1);   // First 4 bits.
        int firstHex = Integer.parseInt(firstDigit, 16);
        String secondDigit = guid.substring(1, 2);  // Second 4 bits.
        int secondHex = Integer.parseInt(secondDigit, 16);
        String thirdDigit = guid.substring(2, 3);   // Thrid 4 bits.
        int thirdHex = Integer.parseInt(thirdDigit, 16);
        String fourthDigit = guid.substring(3, 4);  // Fourth 4 bits.
        int fourthHex = Integer.parseInt(fourthDigit, 16);
        String fifthDigit = guid.substring(4, 5);   // Fifth 4 bits.
        int fifthHex = Integer.parseInt(fifthDigit, 16);
        String sixthDigit = guid.substring(5, 6);   // Sixth 4 bits.
        int sixthHex = Integer.parseInt(sixthDigit, 16);
//        String seventhDigit = guid.substring(6, 7); // Seventh 4 bits.
//        int seventhHex = Integer.parseInt(seventhDigit, 16);
//        String eightthDigit = guid.substring(7, 8); // Eighth 4 bits.
//        int eightthHex = Integer.parseInt(eightthDigit, 16);
        String ninthDigit = guid.substring(9, 10);  // Ninth 4 bits.
        int ninthHex = Integer.parseInt(ninthDigit, 16);
        String tenthDigit = guid.substring(10, 11); // Tenth 4 bits.
        int tenthHex = Integer.parseInt(tenthDigit, 16);
//        String eleventhDigit = guid.substring(11, 12);  // Eleventh 4 bits.
//        int eleventhHex = Integer.parseInt(eleventhDigit, 16);
//        String twelfthDigit = guid.substring(12, 13);   // Twelfth 4 bits.
//        int twelfthHex = Integer.parseInt(twelfthDigit, 16);
        
        String twoLetterWord1 = guid.substring(16, 18);            // 2 chars
        int twoLetterHex1 = Integer.parseInt(twoLetterWord1, 16);  // 
        String twoLetterWord2 = guid.substring(19, 21);            // 2 chars
        int twoLetterHex2 = Integer.parseInt(twoLetterWord2, 16);  // 
        String twoLetterWord3 = guid.substring(21, 23);            // 2 chars
        int twoLetterHex3 = Integer.parseInt(twoLetterWord3, 16);  // 
//        String twoLetterWord4 = guid.substring(24, 26);            // 2 chars
//        int twoLetterHex4 = Integer.parseInt(twoLetterWord4, 16);  // 
        String threeLetterWord1 = guid.substring(27, 30);          // 3 chars
        int threeLetterHex1 = Integer.parseInt(threeLetterWord1, 16); 
        String threeLetterWord2 = guid.substring(30, 33);          // 3 chars
        int threeLetterHex2 = Integer.parseInt(threeLetterWord2, 16); 
//        String threeLetterWord3 = guid.substring(33, 36);          // 3 chars
//        int threeLetterHex3 = Integer.parseInt(threeLetterWord3, 16); 
        
        String fourLetterWord1 = guid.substring(0, 4);               // 4 chars
        int fourLetterHex1 = Integer.parseInt(fourLetterWord1, 16);  // 
        String fourLetterWord2 = guid.substring(4, 8);               // 4 chars
        int fourLetterHex2 = Integer.parseInt(fourLetterWord2, 16);  // 

        String twoLetters1 = TwoLetterWords.getInstance(sassyTokenType).getWord(twoLetterHex1, secondHex);
        String twoLetters2 = TwoLetterWords.getInstance(sassyTokenType).getWord(twoLetterHex2, thirdHex);
        String twoLetters3 = TwoLetterWords.getInstance(sassyTokenType).getWord(twoLetterHex3, fourthHex);
        String threeLetters1 = ThreeLetterWords.getInstance(sassyTokenType).getWord(threeLetterHex1, fifthHex);
        String threeLetters2 = ThreeLetterWords.getInstance(sassyTokenType).getWord(threeLetterHex2, sixthHex);
        String fourLetters1 = FourLetterWords.getInstance(sassyTokenType).getWord(fourLetterHex1, ninthHex);
        String fourLetters2 = FourLetterWords.getInstance(sassyTokenType).getWord(fourLetterHex2, tenthHex);
        
        int mod = firstHex;
        StringBuilder sb = new StringBuilder();
        if(mod == 0) {
            sb.append(twoLetters1);
            sb.append(twoLetters2);
            sb.append(twoLetters3);
            sb.append(fourLetters1);
        } else if(mod == 1) {
            sb.append(twoLetters1);
            sb.append(twoLetters2);
            sb.append(fourLetters1);
            sb.append(twoLetters3);
        } else if(mod == 2) {
            sb.append(twoLetters1);
            sb.append(fourLetters1);
            sb.append(twoLetters2);
            sb.append(twoLetters3);
        } else if(mod == 3) {
            sb.append(fourLetters1);
            sb.append(twoLetters1);
            sb.append(twoLetters2);
            sb.append(twoLetters3);
        } else if(mod == 4) {
            sb.append(twoLetters1);
            sb.append(twoLetters2);
            sb.append(threeLetters1);
            sb.append(threeLetters2);
        } else if(mod == 5) {
            sb.append(twoLetters1);
            sb.append(threeLetters1);
            sb.append(twoLetters2);
            sb.append(threeLetters2);
        } else if(mod == 6) {
            sb.append(threeLetters1);
            sb.append(twoLetters1);
            sb.append(twoLetters2);
            sb.append(threeLetters2);
        } else if(mod == 7) {
            sb.append(twoLetters1);
            sb.append(threeLetters1);
            sb.append(threeLetters2);
            sb.append(twoLetters2);
        } else if(mod == 8) {
            sb.append(threeLetters1);
            sb.append(twoLetters1);
            sb.append(threeLetters2);
            sb.append(twoLetters2);
        } else if(mod == 9) {
            sb.append(threeLetters1);
            sb.append(threeLetters2);
            sb.append(twoLetters1);
            sb.append(twoLetters2);
        } else if(mod == 10) {
            sb.append(threeLetters1);
            sb.append(threeLetters2);
            sb.append(fourLetters1);
        } else if(mod == 11) {
            sb.append(threeLetters1);
            sb.append(fourLetters1);
            sb.append(threeLetters2);
        } else if(mod == 12) {
            sb.append(fourLetters1);
            sb.append(threeLetters1);
            sb.append(threeLetters2);
        } else if(mod == 13) {
            sb.append(twoLetters1);
            sb.append(fourLetters1);
            sb.append(fourLetters2);
        } else if(mod == 14) {
            sb.append(fourLetters1);
            sb.append(twoLetters1);
            sb.append(fourLetters2);
        } else {
            sb.append(fourLetters1);
            sb.append(fourLetters2);
            sb.append(twoLetters1);
        }            
        String token = sb.toString();
        log.finer("token = " + token);
        return token;
    }

    
}
