package com.cannyurl.app.sassy;

import java.util.Arrays;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.cannyurl.af.analytics.AnalyticsManager;
import com.cannyurl.app.util.ConfigUtil;
import com.cannyurl.common.SassyTokenType;


public class TwoLetterWords
{
    private static final Logger log = Logger.getLogger(TwoLetterWords.class.getName());

    private static final int WORD_SIZE = 2;
    private static final int LIST_SIZE = 128;

    private char[][] mWords = null;
    private final int mSassyTokenTypeFlag;

    private TwoLetterWords(String sassyTokenType)
    {
        if(SassyTokenType.TYPE_MIXEDCASE.equals(sassyTokenType)) {
            mSassyTokenTypeFlag = 2;
        } else if(SassyTokenType.TYPE_LOWERCAPS.equals(sassyTokenType)) {
            mSassyTokenTypeFlag = 1;
        } else if(SassyTokenType.TYPE_LOWERCASE.equals(sassyTokenType)) {
            mSassyTokenTypeFlag = 0;
        } else if(SassyTokenType.TYPE_LONGTOKEN.equals(sassyTokenType)) {
            mSassyTokenTypeFlag = 0;
        } else {
            mSassyTokenTypeFlag = 2;
        }
        initWordArray();
    }
    
    // Initialization-on-demand holder.
    private static final class TwoLetterWordsMixedcaseHolder
    {
        private static final TwoLetterWords INSTANCE = new TwoLetterWords(SassyTokenType.TYPE_MIXEDCASE);
    }
    private static final class TwoLetterWordsLowercapsHolder
    {
        private static final TwoLetterWords INSTANCE = new TwoLetterWords(SassyTokenType.TYPE_LOWERCAPS);
    }
    private static final class TwoLetterWordsLowercaseHolder
    {
        private static final TwoLetterWords INSTANCE = new TwoLetterWords(SassyTokenType.TYPE_LOWERCASE);
    }
    private static final class TwoLetterWordsLongertokenHolder
    {
        private static final TwoLetterWords INSTANCE = new TwoLetterWords(SassyTokenType.TYPE_LONGTOKEN);
    }

    // Singleton method
    public static TwoLetterWords getInstance(String sassyTokenType)
    {
        if(SassyTokenType.TYPE_MIXEDCASE.equals(sassyTokenType)) {
            return TwoLetterWordsMixedcaseHolder.INSTANCE;
        } else if(SassyTokenType.TYPE_LOWERCAPS.equals(sassyTokenType)) {
            return TwoLetterWordsLowercapsHolder.INSTANCE;
        } else if(SassyTokenType.TYPE_LOWERCASE.equals(sassyTokenType)) {
            return TwoLetterWordsLowercaseHolder.INSTANCE;
        } else if(SassyTokenType.TYPE_LONGTOKEN.equals(sassyTokenType)) {
            return TwoLetterWordsLongertokenHolder.INSTANCE;
        } else {
            return TwoLetterWordsMixedcaseHolder.INSTANCE;
        }
    }

    
    // TBD:
    public char[] getWordChars(int idx)
    {
        // idx validation?
        idx = idx % LIST_SIZE;
        return mWords[idx];
    }
    public String getWord(int idx)
    {
        StringBuilder sb = new StringBuilder();
        char[] w = getWordChars(idx);
        sb.append(w[0]).append(w[1]);
        return sb.toString();
    }

    // TBD:
    // Note: A == 65 & a == 97
    public char[] getWordChars(int idx, int mask)
    {
        // idx/mask validation?
        idx = idx % LIST_SIZE;
        // char[] w = mWords[idx];
        char[] w = Arrays.copyOf(mWords[idx], WORD_SIZE);
        if(mSassyTokenTypeFlag > 0) {
            if((mask & 1) != 0) {
                w[0] = (char) (w[0] - 32);
            }
            if(mSassyTokenTypeFlag > 1) {
                if((mask & 2) != 0) {
                    w[1] = (char) (w[1] - 32);
                }
            }
        }
        return w;
    }
    public String getWord(int idx, int mask)
    {
        StringBuilder sb = new StringBuilder();
        char[] w = getWordChars(idx, mask);
        sb.append(w[0]).append(w[1]);
        return sb.toString();
    }

    
    private void initWordArray()
    {
        //mWords = new char[LIST_SIZE][WORD_SIZE];
        //mWords = new char[LIST_SIZE][];
        mWords = new char[][] {
{'a','a'},
{'a','b'},
{'a','d'},
{'a','e'},
{'a','g'},
{'a','h'},
{'a','i'},
{'a','l'},
{'a','m'},
{'a','n'},
{'a','r'},
{'a','s'},
{'a','t'},
{'a','w'},
{'a','x'},
{'a','y'},
{'b','a'},
{'b','e'},
{'b','i'},
{'b','o'},
{'b','y'},
{'c','a'},
{'c','o'},
{'d','e'},
{'d','o'},
{'e','d'},
{'e','e'},
{'e','f'},
{'e','h'},
{'e','l'},
{'e','m'},
{'e','n'},
{'e','r'},
{'e','s'},
{'e','t'},
{'e','x'},
{'f','a'},
{'f','e'},
{'f','y'},
{'g','a'},
{'g','o'},
{'g','u'},
{'h','a'},
{'h','e'},
{'h','i'},
{'h','m'},
{'h','o'},
{'i','d'},
{'i','e'},
{'i','f'},
{'i','n'},
{'i','o'},
{'i','s'},
{'i','t'},
{'i','x'},
{'j','j'},
{'j','o'},
{'k','a'},
{'k','i'},
{'l','a'},
{'l','i'},
{'l','o'},
{'l','y'},
{'m','a'},
{'m','e'},
{'m','i'},
{'m','m'},
{'m','o'},
{'m','u'},
{'m','y'},
{'n','a'},
{'n','e'},
{'n','i'},
{'n','o'},
{'n','u'},
{'o','d'},
{'o','e'},
{'o','f'},
{'o','h'},
{'o','i'},
{'o','k'},
{'o','m'},
{'o','n'},
{'o','o'},
{'o','p'},
{'o','r'},
{'o','s'},
{'o','w'},
{'o','x'},
{'o','y'},
{'p','a'},
{'p','e'},
{'p','i'},
{'q','i'},
{'q','u'},
{'r','e'},
{'r','i'},
{'s','a'},
{'s','h'},
{'s','i'},
{'s','o'},
{'s','t'},
{'t','a'},
{'t','i'},
{'t','o'},
{'t','v'},
{'u','g'},
{'u','h'},
{'u','m'},
{'u','n'},
{'u','p'},
{'u','r'},
{'u','s'},
{'u','t'},
{'v','a'},
{'v','o'},
{'w','a'},
{'w','e'},
{'w','o'},
{'x','i'},
{'x','u'},
{'y','a'},
{'y','e'},
{'y','i'},
{'y','o'},
{'y','u'},
{'z','a'},
{'z','e'}
        };
    }


}
