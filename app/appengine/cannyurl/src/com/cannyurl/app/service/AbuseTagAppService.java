package com.cannyurl.app.service;

import java.util.List;
import java.util.logging.Logger;

import com.myurldb.ws.AbuseTag;
import com.myurldb.ws.BaseException;
import com.cannyurl.af.bean.AbuseTagBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.AbuseTagService;
import com.cannyurl.af.service.impl.AbuseTagServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class AbuseTagAppService extends AbuseTagServiceImpl implements AbuseTagService
{
    private static final Logger log = Logger.getLogger(AbuseTagAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public AbuseTagAppService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // AbuseTag related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public AbuseTag getAbuseTag(String guid) throws BaseException
    {
        return super.getAbuseTag(guid);
    }

    @Override
    public Object getAbuseTag(String guid, String field) throws BaseException
    {
        return super.getAbuseTag(guid, field);
    }
    
    @Override
    public List<AbuseTag> getAllAbuseTags() throws BaseException
    {
        return super.getAllAbuseTags();
    }

    @Override
    public List<AbuseTag> findAbuseTags(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findAbuseTags(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createAbuseTag(AbuseTag abuseTag) throws BaseException
    {
        return super.createAbuseTag(abuseTag);
    }

    @Override
    public AbuseTag constructAbuseTag(AbuseTag abuseTag) throws BaseException
    {
        return super.constructAbuseTag(abuseTag);
    }

    @Override
    public Boolean updateAbuseTag(AbuseTag abuseTag) throws BaseException
    {
        return super.updateAbuseTag(abuseTag);
    }
        
    @Override
    public AbuseTag refreshAbuseTag(AbuseTag abuseTag) throws BaseException
    {
        return super.refreshAbuseTag(abuseTag);
    }

    @Override
    public Boolean deleteAbuseTag(String guid) throws BaseException
    {
        return super.deleteAbuseTag(guid);
    }

    @Override
    public Boolean deleteAbuseTag(AbuseTag abuseTag) throws BaseException
    {
        return super.deleteAbuseTag(abuseTag);
    }

}
