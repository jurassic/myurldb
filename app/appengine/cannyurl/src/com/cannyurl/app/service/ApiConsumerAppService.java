package com.cannyurl.app.service;

import java.util.List;
import java.util.logging.Logger;

import com.myurldb.ws.ApiConsumer;
import com.myurldb.ws.BaseException;
import com.cannyurl.af.bean.ApiConsumerBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ApiConsumerService;
import com.cannyurl.af.service.impl.ApiConsumerServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class ApiConsumerAppService extends ApiConsumerServiceImpl implements ApiConsumerService
{
    private static final Logger log = Logger.getLogger(ApiConsumerAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public ApiConsumerAppService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // ApiConsumer related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public ApiConsumer getApiConsumer(String guid) throws BaseException
    {
        return super.getApiConsumer(guid);
    }

    @Override
    public Object getApiConsumer(String guid, String field) throws BaseException
    {
        return super.getApiConsumer(guid, field);
    }
    
    @Override
    public List<ApiConsumer> getAllApiConsumers() throws BaseException
    {
        return super.getAllApiConsumers();
    }

    @Override
    public List<ApiConsumer> findApiConsumers(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findApiConsumers(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createApiConsumer(ApiConsumer apiConsumer) throws BaseException
    {
        return super.createApiConsumer(apiConsumer);
    }

    @Override
    public ApiConsumer constructApiConsumer(ApiConsumer apiConsumer) throws BaseException
    {
        return super.constructApiConsumer(apiConsumer);
    }
        
    @Override
    public Boolean updateApiConsumer(ApiConsumer apiConsumer) throws BaseException
    {
        return super.updateApiConsumer(apiConsumer);
    }
        
    @Override
    public ApiConsumer refreshApiConsumer(ApiConsumer apiConsumer) throws BaseException
    {
        return super.refreshApiConsumer(apiConsumer);
    }

    @Override
    public Boolean deleteApiConsumer(String guid) throws BaseException
    {
        return super.deleteApiConsumer(guid);
    }

    @Override
    public Boolean deleteApiConsumer(ApiConsumer apiConsumer) throws BaseException
    {
        return super.deleteApiConsumer(apiConsumer);
    }

}
