package com.cannyurl.app.service;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cannyurl.af.bean.AppClientBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.AppClientService;
import com.cannyurl.af.service.impl.AppClientServiceImpl;
import com.cannyurl.app.util.ConfigUtil;
import com.myurldb.ws.AppClient;
import com.myurldb.ws.BaseException;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.exception.BadRequestException;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;


// Updated.
public class AppClientAppService extends AppClientServiceImpl implements AppClientService
{
    private static final Logger log = Logger.getLogger(AppClientAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }

    public AppClientAppService()
    {
         super();
    }

//    private static UserService userService = null;
//    private static UserService getUserService()
//    {
//        if(userService == null) {
//            userService = new UserServiceImpl();
//        }
//        return userService;
//    }


    
    //////////////////////////////////////////////////////////////////////////
    // AppClient related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public AppClient getAppClient(String guid) throws BaseException
    {
        return super.getAppClient(guid);
    }

    @Override
    public Object getAppClient(String guid, String field) throws BaseException
    {
        return super.getAppClient(guid, field);
    }

    @Override
    public List<AppClient> getAppClients(List<String> guids) throws BaseException
    {
        return super.getAppClients(guids);
    }

    @Override
    public List<AppClient> getAllAppClients() throws BaseException
    {
        return super.getAllAppClients();
    }

    @Override
    public List<String> getAllAppClientKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllAppClientKeys(ordering, offset, count);
    }

    @Override
    public List<AppClient> findAppClients(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findAppClients(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findAppClientKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findAppClientKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }



    @Override
    public String createAppClient(AppClient appClient) throws BaseException
    {
        log.finer("TOP: createAppClient()");

        // TBD:
        appClient = validateAppClient(appClient);
        appClient = enhanceAppClient(appClient);
        appClient = processAppClient(appClient, false);
        String guid = super.createAppClient(appClient);
        
        // TBD: This is somewhat questionable
//        if(getCache() != null) {
//        	if(guid != null) {
//	        	String permalink = appClient.getPermalink();
//	        	if(permalink != null && !permalink.isEmpty()) {
//	        		getCache().put(permalink, appClient);
//	        	}
//        	}
//        }

        if(log.isLoggable(Level.FINER)) log.finer("BOTTOM: createAppClient(): guid = " + guid);
        return guid;
    }

    @Override
    public AppClient constructAppClient(AppClient appClient) throws BaseException
    {
        log.finer("TOP: constructAppClient()");

        // TBD:

        // log.warning("111111 appClient = " + appClient);
        appClient = validateAppClient(appClient);
        // log.warning("222222 appClient = " + appClient);
        appClient = enhanceAppClient(appClient);
        // log.warning("333333 appClient = " + appClient);
        appClient = processAppClient(appClient, false);
        // log.warning("444444 appClient = " + appClient);
        appClient = super.constructAppClient(appClient);
        // log.warning("555555 appClient = " + appClient);

        // TBD: This is somewhat questionable
//        if(getCache() != null) {
//        	if(appClient != null) {
//	        	String permalink = appClient.getPermalink();
//	        	if(permalink != null && !permalink.isEmpty()) {
//	        		getCache().put(permalink, appClient);
//	        	}
//        	}
//        }

        if(log.isLoggable(Level.FINER)) log.finer("BOTTOM: constructAppClient(): appClient = " + appClient);
        return appClient;
    }


    @Override
    public Boolean updateAppClient(AppClient appClient) throws BaseException
    {
        log.finer("TOP: updateAppClient()");

        // TBD:
        appClient = validateAppClient(appClient);
        appClient = enhanceAppClient(appClient);
        appClient = processAppClient(appClient, true);
        Boolean suc = super.updateAppClient(appClient);

        // TBD: This is somewhat questionable
        // Old cache need to be removed when the object is updated/deleted...
//        if(getCache() != null) {
//        	if(Boolean.TRUE.equals(suc)) {
//	        	String permalink = appClient.getPermalink();
//	        	if(permalink != null && !permalink.isEmpty()) {
//	        		getCache().put(permalink, appClient);
//	        	}
//        	}
//        }

        if(log.isLoggable(Level.FINER)) log.finer("BOTTOM: createAppClient(): suc = " + suc);
        return suc;
    }
        
    @Override
    public AppClient refreshAppClient(AppClient appClient) throws BaseException
    {
        log.finer("TOP: refreshAppClient()");

        // TBD:
        appClient = validateAppClient(appClient);
        appClient = enhanceAppClient(appClient);
        appClient = processAppClient(appClient, true);
        appClient = super.refreshAppClient(appClient);

        // TBD: This is somewhat questionable
        // Old cache need to be removed when the object is updated/deleted...
//        if(getCache() != null) {
//        	if(appClient != null) {
//	        	String permalink = appClient.getPermalink();
//	        	if(permalink != null && !permalink.isEmpty()) {
//	        		getCache().put(permalink, appClient);
//	        	}
//        	}
//        }

        if(log.isLoggable(Level.FINER)) log.finer("BOTTOM: refreshAppClient(): appClient = " + appClient);
        return appClient;
    }


    @Override
    public Boolean deleteAppClient(String guid) throws BaseException
    {
        return super.deleteAppClient(guid);
    }

    @Override
    public Boolean deleteAppClient(AppClient appClient) throws BaseException
    {
        return super.deleteAppClient(appClient);
    }

    @Override
    public Integer createAppClients(List<AppClient> appClients) throws BaseException
    {
        return super.createAppClients(appClients);
    }

    // TBD
    //@Override
    //public Boolean updateAppClients(List<AppClient> appClients) throws BaseException
    //{
    //}

    



    // TBD...
    private AppClientBean validateAppClient(AppClient appClient) throws BaseException
    {
        log.finer("TOP: validateAppClient().");
        AppClientBean bean = null;
        if(appClient instanceof AppClientBean) {
            bean = (AppClientBean) appClient;
        } else {
            // ????
            throw new BadRequestException("ClientCode is of a wrong type. Cannot proceed...");
        }

        String owner =  bean.getOwner();
        if( owner == null || owner.isEmpty() ) {
            // ???  --> Note that the bean will not even be saved ....
            throw new BadRequestException("owner is not set. Cannot proceed...");
        }
        
        // TBD:
        // admin, name, clientId, etc. ???
        // ....

        String clientCode =  bean.getClientCode();
        if( clientCode == null || clientCode.isEmpty() ) {
            // ???  --> Note that the bean will not even be saved ....
            throw new BadRequestException("ClientCode is not set. Cannot proceed...");
        }

        int minLength = ConfigUtil.getClientCodeMinLength();
        int maxLength = ConfigUtil.getClientCodeMaxLength();
        int clientCodeLength = clientCode.length();
        if(clientCodeLength < minLength) {
            throw new BadRequestException("ClientCode is too short. Cannot proceed...");            
        }
        if(clientCodeLength > maxLength) {
            throw new BadRequestException("ClientCode is too long. Cannot proceed...");
        }
        
        // TBD:
        // ClientCode is case-insensitive...
        // We need to change it to lower-case.. (Or, we need an extra column to save the lower-case version, for querying...)
        clientCode = clientCode.toLowerCase();
        bean.setClientCode(clientCode);
        // ....


        // TBD:
        // rootDomain
        // appDomain
        // ????
        // ....
        
        
        // Guid cannot be null
        String guid = bean.getGuid();
        if(guid == null || guid.isEmpty()) {
            guid = GUID.generate();
            bean.setGuid(guid);
        }

        Long createdTime = bean.getCreatedTime();
        if(createdTime == null || createdTime == 0L) {
            createdTime = System.currentTimeMillis();  // ???
            bean.setCreatedTime(createdTime);
        }
        
        
        // TBD:
        // ...
        

        log.finer("BOTTOM: validateAppClient()");
        return bean;
    }
    
    private AppClient enhanceAppClient(AppClient appClient) throws BaseException
    {
        log.finer("TOP: enhanceAppClient().");
        AppClientBean bean = null;
        if(appClient instanceof AppClientBean) {
            bean = (AppClientBean) appClient;
        } else {
            // ????
            log.log(Level.WARNING, "appClient is of a wrong type.");
            return appClient;   // ???
        }
        
        // TBD...
        // ...

        log.finer("BOTTOM: enhanceAppClient()");
        return bean;
    }        

    private AppClient processAppClient(AppClient appClient) throws BaseException
    {
        return processAppClient(appClient, false);
    }
    private AppClient processAppClient(AppClient appClient, boolean update) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TOP: processAppClient(). update = " + update);
        AppClientBean bean = null;
        if(appClient instanceof AppClientBean) {
            bean = (AppClientBean) appClient;
        } else {
            // ????
            log.log(Level.WARNING, "appClient is of a wrong type.");
            return appClient;   // ???
        }
        // ...        


        // TBD:
        // ....

        log.finer("BOTTOM: processAppClient()");
        return bean;
    }
    

}
