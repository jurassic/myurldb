package com.cannyurl.app.service;

import java.util.List;
import java.util.logging.Logger;

import com.myurldb.ws.AppCustomDomain;
import com.myurldb.ws.BaseException;
import com.cannyurl.af.bean.AppCustomDomainBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.AppCustomDomainService;
import com.cannyurl.af.service.impl.AppCustomDomainServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class AppCustomDomainAppService extends AppCustomDomainServiceImpl implements AppCustomDomainService
{
    private static final Logger log = Logger.getLogger(AppCustomDomainAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public AppCustomDomainAppService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // AppCustomDomain related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public AppCustomDomain getAppCustomDomain(String guid) throws BaseException
    {
        return super.getAppCustomDomain(guid);
    }

    @Override
    public Object getAppCustomDomain(String guid, String field) throws BaseException
    {
        return super.getAppCustomDomain(guid, field);
    }
    
    @Override
    public List<AppCustomDomain> getAllAppCustomDomains() throws BaseException
    {
        return super.getAllAppCustomDomains();
    }

    @Override
    public List<AppCustomDomain> findAppCustomDomains(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findAppCustomDomains(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createAppCustomDomain(AppCustomDomain appCustomDomain) throws BaseException
    {
        return super.createAppCustomDomain(appCustomDomain);
    }

    @Override
    public AppCustomDomain constructAppCustomDomain(AppCustomDomain appCustomDomain) throws BaseException
    {
        return super.constructAppCustomDomain(appCustomDomain);
    }
        
    @Override
    public Boolean updateAppCustomDomain(AppCustomDomain appCustomDomain) throws BaseException
    {
        return super.updateAppCustomDomain(appCustomDomain);
    }
        
    @Override
    public AppCustomDomain refreshAppCustomDomain(AppCustomDomain appCustomDomain) throws BaseException
    {
        return super.refreshAppCustomDomain(appCustomDomain);
    }

    @Override
    public Boolean deleteAppCustomDomain(String guid) throws BaseException
    {
        return super.deleteAppCustomDomain(guid);
    }

    @Override
    public Boolean deleteAppCustomDomain(AppCustomDomain appCustomDomain) throws BaseException
    {
        return super.deleteAppCustomDomain(appCustomDomain);
    }

}
