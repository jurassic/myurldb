package com.cannyurl.app.service;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.cannyurl.af.service.AbstractServiceFactory;
import com.cannyurl.af.service.ApiConsumerService;
import com.cannyurl.af.service.AppCustomDomainService;
import com.cannyurl.af.service.SiteCustomDomainService;
import com.cannyurl.af.service.UserService;
import com.cannyurl.af.service.UserUsercodeService;
import com.cannyurl.af.service.UserPasswordService;
import com.cannyurl.af.service.ExternalUserAuthService;
import com.cannyurl.af.service.UserAuthStateService;
import com.cannyurl.af.service.UserResourcePermissionService;
import com.cannyurl.af.service.UserResourceProhibitionService;
import com.cannyurl.af.service.RolePermissionService;
import com.cannyurl.af.service.UserRoleService;
import com.cannyurl.af.service.AppClientService;
import com.cannyurl.af.service.ClientUserService;
import com.cannyurl.af.service.UserCustomDomainService;
import com.cannyurl.af.service.ClientSettingService;
import com.cannyurl.af.service.UserSettingService;
import com.cannyurl.af.service.VisitorSettingService;
import com.cannyurl.af.service.TwitterSummaryCardService;
import com.cannyurl.af.service.TwitterPhotoCardService;
import com.cannyurl.af.service.TwitterGalleryCardService;
import com.cannyurl.af.service.TwitterAppCardService;
import com.cannyurl.af.service.TwitterPlayerCardService;
import com.cannyurl.af.service.TwitterProductCardService;
import com.cannyurl.af.service.ShortPassageService;
import com.cannyurl.af.service.ShortLinkService;
import com.cannyurl.af.service.GeoLinkService;
import com.cannyurl.af.service.QrCodeService;
import com.cannyurl.af.service.LinkPassphraseService;
import com.cannyurl.af.service.LinkMessageService;
import com.cannyurl.af.service.LinkAlbumService;
import com.cannyurl.af.service.AlbumShortLinkService;
import com.cannyurl.af.service.KeywordFolderService;
import com.cannyurl.af.service.BookmarkFolderService;
import com.cannyurl.af.service.KeywordLinkService;
import com.cannyurl.af.service.BookmarkLinkService;
import com.cannyurl.af.service.SpeedDialService;
import com.cannyurl.af.service.KeywordFolderImportService;
import com.cannyurl.af.service.BookmarkFolderImportService;
import com.cannyurl.af.service.KeywordCrowdTallyService;
import com.cannyurl.af.service.BookmarkCrowdTallyService;
import com.cannyurl.af.service.DomainInfoService;
import com.cannyurl.af.service.UrlRatingService;
import com.cannyurl.af.service.UserRatingService;
import com.cannyurl.af.service.AbuseTagService;
import com.cannyurl.af.service.ServiceInfoService;
import com.cannyurl.af.service.FiveTenService;

public class AppServiceFactory extends AbstractServiceFactory
{
    private static final Logger log = Logger.getLogger(AppServiceFactory.class.getName());

    private AppServiceFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class AppServiceFactoryHolder
    {
        private static final AppServiceFactory INSTANCE = new AppServiceFactory();
    }

    // Singleton method
    public static AppServiceFactory getInstance()
    {
        return AppServiceFactoryHolder.INSTANCE;
    }

    @Override
    public ApiConsumerService getApiConsumerService()
    {
        return new ApiConsumerAppService();
    }

    @Override
    public AppCustomDomainService getAppCustomDomainService()
    {
        return new AppCustomDomainAppService();
    }

    @Override
    public SiteCustomDomainService getSiteCustomDomainService()
    {
        return new SiteCustomDomainAppService();
    }

    @Override
    public UserService getUserService()
    {
        return new UserAppService();
    }

    @Override
    public UserUsercodeService getUserUsercodeService()
    {
        return new UserUsercodeAppService();
    }

    @Override
    public UserPasswordService getUserPasswordService()
    {
        return new UserPasswordAppService();
    }

    @Override
    public ExternalUserAuthService getExternalUserAuthService()
    {
        return new ExternalUserAuthAppService();
    }

    @Override
    public UserAuthStateService getUserAuthStateService()
    {
        return new UserAuthStateAppService();
    }

    @Override
    public UserResourcePermissionService getUserResourcePermissionService()
    {
        return new UserResourcePermissionAppService();
    }

    @Override
    public UserResourceProhibitionService getUserResourceProhibitionService()
    {
        return new UserResourceProhibitionAppService();
    }

    @Override
    public RolePermissionService getRolePermissionService()
    {
        return new RolePermissionAppService();
    }

    @Override
    public UserRoleService getUserRoleService()
    {
        return new UserRoleAppService();
    }

    @Override
    public AppClientService getAppClientService()
    {
        return new AppClientAppService();
    }

    @Override
    public ClientUserService getClientUserService()
    {
        return new ClientUserAppService();
    }

    @Override
    public UserCustomDomainService getUserCustomDomainService()
    {
        return new UserCustomDomainAppService();
    }

    @Override
    public ClientSettingService getClientSettingService()
    {
        return new ClientSettingAppService();
    }

    @Override
    public UserSettingService getUserSettingService()
    {
        return new UserSettingAppService();
    }

    @Override
    public VisitorSettingService getVisitorSettingService()
    {
        return new VisitorSettingAppService();
    }

    @Override
    public TwitterSummaryCardService getTwitterSummaryCardService()
    {
        return new TwitterSummaryCardAppService();
    }

    @Override
    public TwitterPhotoCardService getTwitterPhotoCardService()
    {
        return new TwitterPhotoCardAppService();
    }

    @Override
    public TwitterGalleryCardService getTwitterGalleryCardService()
    {
        return new TwitterGalleryCardAppService();
    }

    @Override
    public TwitterAppCardService getTwitterAppCardService()
    {
        return new TwitterAppCardAppService();
    }

    @Override
    public TwitterPlayerCardService getTwitterPlayerCardService()
    {
        return new TwitterPlayerCardAppService();
    }

    @Override
    public TwitterProductCardService getTwitterProductCardService()
    {
        return new TwitterProductCardAppService();
    }

    @Override
    public ShortPassageService getShortPassageService()
    {
        return new ShortPassageAppService();
    }

    @Override
    public ShortLinkService getShortLinkService()
    {
        return new ShortLinkAppService();
    }

    @Override
    public GeoLinkService getGeoLinkService()
    {
        return new GeoLinkAppService();
    }

    @Override
    public QrCodeService getQrCodeService()
    {
        return new QrCodeAppService();
    }

    @Override
    public LinkPassphraseService getLinkPassphraseService()
    {
        return new LinkPassphraseAppService();
    }

    @Override
    public LinkMessageService getLinkMessageService()
    {
        return new LinkMessageAppService();
    }

    @Override
    public LinkAlbumService getLinkAlbumService()
    {
        return new LinkAlbumAppService();
    }

    @Override
    public AlbumShortLinkService getAlbumShortLinkService()
    {
        return new AlbumShortLinkAppService();
    }

    @Override
    public KeywordFolderService getKeywordFolderService()
    {
        return new KeywordFolderAppService();
    }

    @Override
    public BookmarkFolderService getBookmarkFolderService()
    {
        return new BookmarkFolderAppService();
    }

    @Override
    public KeywordLinkService getKeywordLinkService()
    {
        return new KeywordLinkAppService();
    }

    @Override
    public BookmarkLinkService getBookmarkLinkService()
    {
        return new BookmarkLinkAppService();
    }

    @Override
    public SpeedDialService getSpeedDialService()
    {
        return new SpeedDialAppService();
    }

    @Override
    public KeywordFolderImportService getKeywordFolderImportService()
    {
        return new KeywordFolderImportAppService();
    }

    @Override
    public BookmarkFolderImportService getBookmarkFolderImportService()
    {
        return new BookmarkFolderImportAppService();
    }

    @Override
    public KeywordCrowdTallyService getKeywordCrowdTallyService()
    {
        return new KeywordCrowdTallyAppService();
    }

    @Override
    public BookmarkCrowdTallyService getBookmarkCrowdTallyService()
    {
        return new BookmarkCrowdTallyAppService();
    }

    @Override
    public DomainInfoService getDomainInfoService()
    {
        return new DomainInfoAppService();
    }

    @Override
    public UrlRatingService getUrlRatingService()
    {
        return new UrlRatingAppService();
    }

    @Override
    public UserRatingService getUserRatingService()
    {
        return new UserRatingAppService();
    }

    @Override
    public AbuseTagService getAbuseTagService()
    {
        return new AbuseTagAppService();
    }

    @Override
    public ServiceInfoService getServiceInfoService()
    {
        return new ServiceInfoAppService();
    }

    @Override
    public FiveTenService getFiveTenService()
    {
        return new FiveTenAppService();
    }


}
