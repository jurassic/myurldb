package com.cannyurl.app.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.BookmarkFolderImport;
import com.cannyurl.af.bean.BookmarkFolderImportBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.BookmarkFolderImportService;
import com.cannyurl.af.service.impl.BookmarkFolderImportServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class BookmarkFolderImportAppService extends BookmarkFolderImportServiceImpl implements BookmarkFolderImportService
{
    private static final Logger log = Logger.getLogger(BookmarkFolderImportAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public BookmarkFolderImportAppService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // BookmarkFolderImport related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public BookmarkFolderImport getBookmarkFolderImport(String guid) throws BaseException
    {
        return super.getBookmarkFolderImport(guid);
    }

    @Override
    public Object getBookmarkFolderImport(String guid, String field) throws BaseException
    {
        return super.getBookmarkFolderImport(guid, field);
    }

    @Override
    public List<BookmarkFolderImport> getBookmarkFolderImports(List<String> guids) throws BaseException
    {
        return super.getBookmarkFolderImports(guids);
    }

    @Override
    public List<BookmarkFolderImport> getAllBookmarkFolderImports() throws BaseException
    {
        return super.getAllBookmarkFolderImports();
    }

    @Override
    public List<String> getAllBookmarkFolderImportKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllBookmarkFolderImportKeys(ordering, offset, count);
    }

    @Override
    public List<BookmarkFolderImport> findBookmarkFolderImports(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findBookmarkFolderImports(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findBookmarkFolderImportKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findBookmarkFolderImportKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createBookmarkFolderImport(BookmarkFolderImport bookmarkFolderImport) throws BaseException
    {
        return super.createBookmarkFolderImport(bookmarkFolderImport);
    }

    @Override
    public BookmarkFolderImport constructBookmarkFolderImport(BookmarkFolderImport bookmarkFolderImport) throws BaseException
    {
        return super.constructBookmarkFolderImport(bookmarkFolderImport);
    }


    @Override
    public Boolean updateBookmarkFolderImport(BookmarkFolderImport bookmarkFolderImport) throws BaseException
    {
        return super.updateBookmarkFolderImport(bookmarkFolderImport);
    }
        
    @Override
    public BookmarkFolderImport refreshBookmarkFolderImport(BookmarkFolderImport bookmarkFolderImport) throws BaseException
    {
        return super.refreshBookmarkFolderImport(bookmarkFolderImport);
    }

    @Override
    public Boolean deleteBookmarkFolderImport(String guid) throws BaseException
    {
        return super.deleteBookmarkFolderImport(guid);
    }

    @Override
    public Boolean deleteBookmarkFolderImport(BookmarkFolderImport bookmarkFolderImport) throws BaseException
    {
        return super.deleteBookmarkFolderImport(bookmarkFolderImport);
    }

    @Override
    public Integer createBookmarkFolderImports(List<BookmarkFolderImport> bookmarkFolderImports) throws BaseException
    {
        return super.createBookmarkFolderImports(bookmarkFolderImports);
    }

    // TBD
    //@Override
    //public Boolean updateBookmarkFolderImports(List<BookmarkFolderImport> bookmarkFolderImports) throws BaseException
    //{
    //}

}
