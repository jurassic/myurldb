package com.cannyurl.app.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.BookmarkLink;
import com.cannyurl.af.bean.BookmarkLinkBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.BookmarkLinkService;
import com.cannyurl.af.service.impl.BookmarkLinkServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class BookmarkLinkAppService extends BookmarkLinkServiceImpl implements BookmarkLinkService
{
    private static final Logger log = Logger.getLogger(BookmarkLinkAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public BookmarkLinkAppService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // BookmarkLink related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public BookmarkLink getBookmarkLink(String guid) throws BaseException
    {
        return super.getBookmarkLink(guid);
    }

    @Override
    public Object getBookmarkLink(String guid, String field) throws BaseException
    {
        return super.getBookmarkLink(guid, field);
    }

    @Override
    public List<BookmarkLink> getBookmarkLinks(List<String> guids) throws BaseException
    {
        return super.getBookmarkLinks(guids);
    }

    @Override
    public List<BookmarkLink> getAllBookmarkLinks() throws BaseException
    {
        return super.getAllBookmarkLinks();
    }

    @Override
    public List<String> getAllBookmarkLinkKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllBookmarkLinkKeys(ordering, offset, count);
    }

    @Override
    public List<BookmarkLink> findBookmarkLinks(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findBookmarkLinks(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findBookmarkLinkKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findBookmarkLinkKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createBookmarkLink(BookmarkLink bookmarkLink) throws BaseException
    {
        return super.createBookmarkLink(bookmarkLink);
    }

    @Override
    public BookmarkLink constructBookmarkLink(BookmarkLink bookmarkLink) throws BaseException
    {
        return super.constructBookmarkLink(bookmarkLink);
    }


    @Override
    public Boolean updateBookmarkLink(BookmarkLink bookmarkLink) throws BaseException
    {
        return super.updateBookmarkLink(bookmarkLink);
    }
        
    @Override
    public BookmarkLink refreshBookmarkLink(BookmarkLink bookmarkLink) throws BaseException
    {
        return super.refreshBookmarkLink(bookmarkLink);
    }

    @Override
    public Boolean deleteBookmarkLink(String guid) throws BaseException
    {
        return super.deleteBookmarkLink(guid);
    }

    @Override
    public Boolean deleteBookmarkLink(BookmarkLink bookmarkLink) throws BaseException
    {
        return super.deleteBookmarkLink(bookmarkLink);
    }

    @Override
    public Integer createBookmarkLinks(List<BookmarkLink> bookmarkLinks) throws BaseException
    {
        return super.createBookmarkLinks(bookmarkLinks);
    }

    // TBD
    //@Override
    //public Boolean updateBookmarkLinks(List<BookmarkLink> bookmarkLinks) throws BaseException
    //{
    //}

}
