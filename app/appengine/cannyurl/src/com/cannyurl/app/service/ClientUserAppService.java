package com.cannyurl.app.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.ClientUser;
import com.cannyurl.af.bean.ClientUserBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.ClientUserService;
import com.cannyurl.af.service.impl.ClientUserServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class ClientUserAppService extends ClientUserServiceImpl implements ClientUserService
{
    private static final Logger log = Logger.getLogger(ClientUserAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public ClientUserAppService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // ClientUser related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public ClientUser getClientUser(String guid) throws BaseException
    {
        return super.getClientUser(guid);
    }

    @Override
    public Object getClientUser(String guid, String field) throws BaseException
    {
        return super.getClientUser(guid, field);
    }

    @Override
    public List<ClientUser> getClientUsers(List<String> guids) throws BaseException
    {
        return super.getClientUsers(guids);
    }

    @Override
    public List<ClientUser> getAllClientUsers() throws BaseException
    {
        return super.getAllClientUsers();
    }

    @Override
    public List<String> getAllClientUserKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllClientUserKeys(ordering, offset, count);
    }

    @Override
    public List<ClientUser> findClientUsers(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findClientUsers(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findClientUserKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findClientUserKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createClientUser(ClientUser clientUser) throws BaseException
    {
        return super.createClientUser(clientUser);
    }

    @Override
    public ClientUser constructClientUser(ClientUser clientUser) throws BaseException
    {
        return super.constructClientUser(clientUser);
    }


    @Override
    public Boolean updateClientUser(ClientUser clientUser) throws BaseException
    {
        return super.updateClientUser(clientUser);
    }
        
    @Override
    public ClientUser refreshClientUser(ClientUser clientUser) throws BaseException
    {
        return super.refreshClientUser(clientUser);
    }

    @Override
    public Boolean deleteClientUser(String guid) throws BaseException
    {
        return super.deleteClientUser(guid);
    }

    @Override
    public Boolean deleteClientUser(ClientUser clientUser) throws BaseException
    {
        return super.deleteClientUser(clientUser);
    }

    @Override
    public Integer createClientUsers(List<ClientUser> clientUsers) throws BaseException
    {
        return super.createClientUsers(clientUsers);
    }

    // TBD
    //@Override
    //public Boolean updateClientUsers(List<ClientUser> clientUsers) throws BaseException
    //{
    //}

}
