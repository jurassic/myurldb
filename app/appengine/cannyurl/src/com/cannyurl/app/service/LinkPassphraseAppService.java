package com.cannyurl.app.service;

import java.util.List;
import java.util.logging.Logger;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.LinkPassphrase;
import com.cannyurl.af.bean.LinkPassphraseBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.LinkPassphraseService;
import com.cannyurl.af.service.impl.LinkPassphraseServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class LinkPassphraseAppService extends LinkPassphraseServiceImpl implements LinkPassphraseService
{
    private static final Logger log = Logger.getLogger(LinkPassphraseAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public LinkPassphraseAppService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // LinkPassphrase related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public LinkPassphrase getLinkPassphrase(String guid) throws BaseException
    {
        return super.getLinkPassphrase(guid);
    }

    @Override
    public Object getLinkPassphrase(String guid, String field) throws BaseException
    {
        return super.getLinkPassphrase(guid, field);
    }
    
    @Override
    public List<LinkPassphrase> getAllLinkPassphrases() throws BaseException
    {
        return super.getAllLinkPassphrases();
    }

    @Override
    public List<LinkPassphrase> findLinkPassphrases(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findLinkPassphrases(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createLinkPassphrase(LinkPassphrase linkPassphrase) throws BaseException
    {
        return super.createLinkPassphrase(linkPassphrase);
    }

    @Override
    public LinkPassphrase constructLinkPassphrase(LinkPassphrase linkPassphrase) throws BaseException
    {
        return super.constructLinkPassphrase(linkPassphrase);
    }
        
    @Override
    public Boolean updateLinkPassphrase(LinkPassphrase linkPassphrase) throws BaseException
    {
        return super.updateLinkPassphrase(linkPassphrase);
    }
        
    @Override
    public LinkPassphrase refreshLinkPassphrase(LinkPassphrase linkPassphrase) throws BaseException
    {
        return super.refreshLinkPassphrase(linkPassphrase);
    }

    @Override
    public Boolean deleteLinkPassphrase(String guid) throws BaseException
    {
        return super.deleteLinkPassphrase(guid);
    }

    @Override
    public Boolean deleteLinkPassphrase(LinkPassphrase linkPassphrase) throws BaseException
    {
        return super.deleteLinkPassphrase(linkPassphrase);
    }

}
