package com.cannyurl.app.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.RolePermission;
import com.cannyurl.af.bean.RolePermissionBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.RolePermissionService;
import com.cannyurl.af.service.impl.RolePermissionServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class RolePermissionAppService extends RolePermissionServiceImpl implements RolePermissionService
{
    private static final Logger log = Logger.getLogger(RolePermissionAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public RolePermissionAppService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // RolePermission related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public RolePermission getRolePermission(String guid) throws BaseException
    {
        return super.getRolePermission(guid);
    }

    @Override
    public Object getRolePermission(String guid, String field) throws BaseException
    {
        return super.getRolePermission(guid, field);
    }

    @Override
    public List<RolePermission> getRolePermissions(List<String> guids) throws BaseException
    {
        return super.getRolePermissions(guids);
    }

    @Override
    public List<RolePermission> getAllRolePermissions() throws BaseException
    {
        return super.getAllRolePermissions();
    }

    @Override
    public List<String> getAllRolePermissionKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllRolePermissionKeys(ordering, offset, count);
    }

    @Override
    public List<RolePermission> findRolePermissions(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findRolePermissions(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findRolePermissionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findRolePermissionKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createRolePermission(RolePermission rolePermission) throws BaseException
    {
        return super.createRolePermission(rolePermission);
    }

    @Override
    public RolePermission constructRolePermission(RolePermission rolePermission) throws BaseException
    {
        return super.constructRolePermission(rolePermission);
    }


    @Override
    public Boolean updateRolePermission(RolePermission rolePermission) throws BaseException
    {
        return super.updateRolePermission(rolePermission);
    }
        
    @Override
    public RolePermission refreshRolePermission(RolePermission rolePermission) throws BaseException
    {
        return super.refreshRolePermission(rolePermission);
    }

    @Override
    public Boolean deleteRolePermission(String guid) throws BaseException
    {
        return super.deleteRolePermission(guid);
    }

    @Override
    public Boolean deleteRolePermission(RolePermission rolePermission) throws BaseException
    {
        return super.deleteRolePermission(rolePermission);
    }

    @Override
    public Integer createRolePermissions(List<RolePermission> rolePermissions) throws BaseException
    {
        return super.createRolePermissions(rolePermissions);
    }

    // TBD
    //@Override
    //public Boolean updateRolePermissions(List<RolePermission> rolePermissions) throws BaseException
    //{
    //}

}
