package com.cannyurl.app.service;

import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.sf.jsr107cache.Cache;

import com.cannyurl.af.bean.BookmarkLinkBean;
import com.cannyurl.af.bean.KeywordLinkBean;
import com.cannyurl.af.bean.ShortLinkBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.BookmarkLinkService;
import com.cannyurl.af.service.KeywordLinkService;
import com.cannyurl.af.service.ShortLinkService;
import com.cannyurl.af.service.impl.BookmarkLinkServiceImpl;
import com.cannyurl.af.service.impl.KeywordLinkServiceImpl;
import com.cannyurl.af.service.impl.ShortLinkServiceImpl;
import com.cannyurl.af.util.CacheHelper;
import com.cannyurl.af.util.URLUtil;
import com.cannyurl.app.core.ReservedWordTokenSet;
import com.cannyurl.app.helper.ShortLinkAppHelper;
import com.cannyurl.app.helper.UserAppHelper;
import com.cannyurl.app.util.BadWordRegistry;
import com.cannyurl.app.util.ConfigUtil;
import com.cannyurl.app.util.DomainUtil;
import com.cannyurl.app.util.ShortLinkUtil;
import com.cannyurl.app.util.TextUtil;
import com.cannyurl.app.util.TokenGeneratorUtil;
import com.cannyurl.app.util.TokenUtil;
import com.cannyurl.common.DomainType;
import com.cannyurl.common.RedirectType;
import com.cannyurl.common.TokenGenerationMethod;
import com.cannyurl.common.TokenType;
import com.cannyurl.helper.UrlHelper;
import com.google.appengine.api.taskqueue.DeferredTask;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.RetryOptions;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.myurldb.ws.BaseException;
import com.myurldb.ws.ShortLink;
import com.myurldb.ws.User;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.exception.InternalServerErrorException;
import com.myurldb.ws.exception.ResourceAlreadyPresentException;
import com.myurldb.ws.util.HashUtil;


// Updated.
public class ShortLinkAppService extends ShortLinkServiceImpl implements ShortLinkService
{
    private static final Logger log = Logger.getLogger(ShortLinkAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public ShortLinkAppService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // ShortLink related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public ShortLink getShortLink(String guid) throws BaseException
    {
        return super.getShortLink(guid);
    }

    @Override
    public Object getShortLink(String guid, String field) throws BaseException
    {
        return super.getShortLink(guid, field);
    }
    
    @Override
    public List<ShortLink> getAllShortLinks() throws BaseException
    {
        return super.getAllShortLinks();
    }

    @Override
    public List<ShortLink> findShortLinks(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findShortLinks(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createShortLink(ShortLink shortLink) throws BaseException
    {
        if(!(shortLink instanceof ShortLinkBean)) {
            log.log(Level.WARNING, "Param shortLink is of an invalid type.");
            throw new BadRequestException("Param shortLink is of an invalid type.");
        }

        // TBD: Any better way to do this???
        // This is a heck....
        String exstingBeanGuid = null;
        try {
            shortLink = validateShortLink((ShortLinkBean) shortLink, true);
        } catch(ResourceAlreadyPresentException rapex) {
            exstingBeanGuid = rapex.getMessage();
        }
        // This is a heck....

//        String guid = null;
//        if(exstingBeanGuid != null) {
//            // "Full fetch" and just return it....
//            ShortLink existingShortLink = ShortLinkAppHelper.getInstance().getShortLink(exstingBeanGuid);
//            if(existingShortLink != null) {
//                guid = existingShortLink.getGuid();   // guid should be the same as exstingBeanGuid ....
//            } else {
//                // ???
//                if(log.isLoggable(Level.WARNING)) log.warning("Failed to find shortLink for given exstingBeanGuid = " + exstingBeanGuid);
//            }
////        } else {    // ?????
////            // shortLink = enhanceShortLink((ShortLinkBean) shortLink, true);
////            guid = super.createShortLink(shortLink);
//        }
//        if(guid == null) {   // ????   Note the difference betweent the above (commented-out) else block vs this...
//            // shortLink = enhanceShortLink((ShortLinkBean) shortLink, true);
//            guid = super.createShortLink(shortLink);
//        }

        String guid = null;
        if(exstingBeanGuid != null) {
            guid = exstingBeanGuid;
        } else {
            // shortLink = enhanceShortLink((ShortLinkBean) shortLink, true);
            guid = super.createShortLink(shortLink);            
        }
        
        // TBD: PageInfo
        // Update MimeType, pageTitle, etc....
        // ????

        return guid;
    }

    @Override
    public ShortLink constructShortLink(ShortLink shortLink) throws BaseException
    {
        if(!(shortLink instanceof ShortLinkBean)) {
            log.log(Level.WARNING, "Param shortLink is of an invalid type.");
            throw new BadRequestException("Param shortLink is of an invalid type.");
        }

        // TBD: Any better way to do this???
        // This is a heck....
        String exstingBeanGuid = null;
        try {
            shortLink = validateShortLink((ShortLinkBean) shortLink, true);
        } catch(ResourceAlreadyPresentException rapex) {
            exstingBeanGuid = rapex.getMessage();
        }
        // This is a heck....

        ShortLink existingShortLink = null;
        if(exstingBeanGuid != null) {
            // "Full fetch" and just return it....
            existingShortLink = ShortLinkAppHelper.getInstance().getShortLink(exstingBeanGuid);
            if(existingShortLink != null) {
                shortLink = existingShortLink;
            } else {
                // ??? 
                // Note that we cannot just return input shortLink without saving it... Or, we can return null....
                if(log.isLoggable(Level.WARNING)) log.warning("Failed to find shortLink for given exstingBeanGuid = " + exstingBeanGuid);
            }
//        } else {    // ?????   See the comment just above AND just below.... the current implementation (not using this else block) is better/safer...
//            // shortLink = enhanceShortLink((ShortLinkBean) shortLink, true);
//            shortLink = super.constructShortLink(shortLink);
        }
        if(existingShortLink == null) {   // ????  Note the difference between the above (commented-out) else block vs this...
            // shortLink = enhanceShortLink((ShortLinkBean) shortLink, true);
            shortLink = super.constructShortLink(shortLink);
        }

        // TBD: PageInfo
        // Update MimeType, pageTitle, etc....
        // ????

        return shortLink;
    }
        
    @Override
    public Boolean updateShortLink(ShortLink shortLink) throws BaseException
    {
        if(!(shortLink instanceof ShortLinkBean)) {
            log.log(Level.WARNING, "Param shortLink is of an invalid type.");
            throw new BadRequestException("Param shortLink is of an invalid type.");
        }

        // TBD: If longUrl or tokenType has changed, regenerate the token
        //      Otherwise, do NOT update the token.
        // ??? How to do this???
        // At this point, we do not know whether longUrl or tokenType has changed.
        // Therefore, it should be caller's responsibility.
        // If either of these attrs has changed, remove the "token" value from shortLink.
        // then, the token will be regenerated.
        // Note:
        // Normally we should not allow changing the core attrs, longUri, domain, and token,
        // once the ShortLink object has been created and persisted.
        shortLink = validateShortLink((ShortLinkBean) shortLink, false);
        // shortLink = enhanceShortLink((ShortLinkBean) shortLink, false);

        return super.updateShortLink(shortLink);
    }
        
    @Override
    public ShortLink refreshShortLink(ShortLink shortLink) throws BaseException
    {
        if(!(shortLink instanceof ShortLinkBean)) {
            log.log(Level.WARNING, "Param shortLink is of an invalid type.");
            throw new BadRequestException("Param shortLink is of an invalid type.");
        }

        // TBD: If longUrl or tokenType has changed, regenerate the token
        //      Otherwise, do NOT update the token.
        // ??? How to do this???
        // At this point, we do not know whether longUrl or tokenType has changed.
        // Therefore, it should be caller's responsibility.
        // If either of these attrs has changed, remove the "token" value from shortLink.
        // then, the token will be regenerated.
        // Note:
        // Normally we should not allow changing the core attrs, longUri, domain, and token,
        // once the ShortLink object has been created and persisted.
        shortLink = validateShortLink((ShortLinkBean) shortLink, false);
        // shortLink = enhanceShortLink((ShortLinkBean) shortLink, false);

        return super.refreshShortLink(shortLink);
    }

    @Override
    public Boolean deleteShortLink(String guid) throws BaseException
    {
        return super.deleteShortLink(guid);
    }

    @Override
    public Boolean deleteShortLink(ShortLink shortLink) throws BaseException
    {
        return super.deleteShortLink(shortLink);
    }

    
    
    ///////////////////////////////////////
    // Utility methods
    ///////////////////////////////////////
    
    // Use ReservedWordTokenSet ....
    // temporary
//    private static final Set<String> sReservedWords = new HashSet<String>();
//    static {
//        sReservedWords.add("edit");
//        sReservedWords.add("info");
//        sReservedWords.add("user");
//        sReservedWords.add("list");
//        sReservedWords.add("tweet");
//        sReservedWords.add("verify");
//        sReservedWords.add("confirm");
//        sReservedWords.add("flash");
//        sReservedWords.add("permanent");
//        sReservedWords.add("temporary");
//        sReservedWords.add("twitter");
//        // ...
//    }
    // temporary

    
    // Move this to a separate "validater" class???
    // TBD: Use isCreating arg....
    // For POST, isCreating==true,
    // For PUT, isCreating can be, in theory, true or false...
    // But, for now, we will assume isCreating is always false for PUT.
    private ShortLinkBean validateShortLink(ShortLinkBean shortLink, boolean isCreating) throws BaseException
    {
        String longUrl = shortLink.getLongUrl();
        if(longUrl == null || longUrl.trim().length() == 0) {
            // No long url, no ShortLink.
            log.log(Level.WARNING, "shortLink object does not contain the long Url field.");
            throw new BadRequestException("shortLink object does not contain the long Url field.");
        } else {
            // TBD: This should be removed...
            // Use ShortLink.longUrlFull and just save a truncated version of longUrl to ShortLink.longUrl....
            // ....
            int longUrlLen = longUrl.length();
            if(longUrlLen > 500) {
                log.log(Level.WARNING, "The input long Url is too long: longUrl = " + longUrl);
                // throw new BadRequestException("The input long Url is too long: longUrl = " + longUrl);
            }
            // ...
        }
        
        // TBD: temporary.
        // If the user just inputs the url without the scheme, add http:// ?????
        // TBD: This is not entirely correct. What if the url starts with a scheme which we don't support???
        if(!longUrl.startsWith("http://") && !longUrl.startsWith("https://") && !longUrl.startsWith("ftp://")) {   // etc. etc.
            // temporary
            if(log.isLoggable(Level.INFO)) log.info("Input longUrl missing the scheme: " + longUrl);
            if(longUrl.indexOf("://") == -1) {
                longUrl = "http://" + longUrl;
                shortLink.setLongUrl(longUrl);
                if(log.isLoggable(Level.INFO)) log.info("longUrl has been modified to " + longUrl);
            } else {
                // ????
                if(log.isLoggable(Level.INFO)) log.info("Unsupported scheme: longUrl = " + longUrl);
                throw new BadRequestException("Unsupported scheme: longUrl = " + longUrl);
            }
        }

        // Validate the long URL before proceeding further....
        if(! URLUtil.isValidUrl(longUrl)) {
            // Invalid long url. Bail out.            
            log.log(Level.WARNING, "shortLink object contains an invalid long Url.");
            throw new BadRequestException("shortLink object contains an invalid long Url.");
        }
        
        

        // TBD:
        // Check if the original url belongs to spam site (blacklisted)
        // ... if so, bail out at this point with error!!!
        // ...

        
        
        
        // TBD:
        // Check if the url is too long (e.g., > 400 or 500 at max)
        // ...
        // Add longUrlHash
        // Add longUrlFull
        // etc...
        // ....
        // Note that if we start using longUrlFull, we will have to update url lookup algorithm (e.g., ShrotLinkView.jsp)
        // etc...
        // ...
        
        
        // Note that longUrl might have changed, hence always regenerate these "derivative" values such as domain, fullUrl, hash, etc....
        String longUrlDomain = shortLink.getLongUrlDomain();
        // if(longUrlDomain == null || longUrlDomain.isEmpty()) {
            // ???
            longUrlDomain = UrlHelper.getInstance().getTopLevelURLFromRequestURL(longUrl);
            shortLink.setLongUrlDomain(longUrlDomain);
            // ...
        //}
        
        // Always overwrite longUrlFull;
        String longUrlFull = null;
        // boolean isLongUrlTruncated = false;
        int longUrlLen = longUrl.length();
        if(longUrlLen > 500) {
            longUrlFull = longUrl;
            // isLongUrlTruncated = true;
            longUrl = longUrl.substring(0, 500);
            shortLink.setLongUrl(longUrl);
        } else {
            longUrlFull = longUrl;                
        }
        shortLink.setLongUrlFull(longUrlFull);
        // Always overwrite hash value (even if the longUrl value has not changed... since we have no way of knowing it....)
        String longUrlHash = HashUtil.generateSha256Hash(longUrlFull);   // Note that we use SHA-256, and this cannot be changed.... (because the client/reader should use the same hashing algo..)
        shortLink.setLongUrlHash(longUrlHash);
        // ...

        
        
        // Is this necessary???
        String guid = shortLink.getGuid();
        if(guid == null || guid.isEmpty()) {  // TBD: validation?
            guid = GUID.generate();
            shortLink.setGuid(guid);
        }

        Long createdTime = shortLink.getCreatedTime();
        if(createdTime == null || createdTime == 0L) {
            createdTime = System.currentTimeMillis();
            shortLink.setCreatedTime(createdTime);
        }

        
        String owner = shortLink.getOwner();
        if(owner == null || owner.isEmpty()) {
            // ???
            // bail out ???
            // ...
        }

        
        
        // TBD:
        // AppClient, etc...
        // ...
        
        

        // TBD
        // Check if the original url already exists in the user's table????
        // If so, just return it, or an error, with an option "create new". ????
        // ...

        // Only when creating...
        if(isCreating == true) {
            
            // See the note written in the data model, myurldb.yaml...
            Boolean failIfShortUrlExists = shortLink.isFailIfShortUrlExists();
            if(failIfShortUrlExists != null && failIfShortUrlExists == true) {

                // TBD: Check longUrlFull or longUrlHash in case longUrl.length > 500.....
                List<ShortLink> beans = ShortLinkAppHelper.getInstance().findShortLinksByLongUrl(longUrl, owner);
                if(beans != null && beans.size() > 0) {
                    if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "ShortLinks with longUrl, " + longUrl + ", already exist for owner = " + owner);
                    // TBD
                    // Display then on a dialog box?
                    // What to do???
                    // ...
                    // ShortLink bean = beans.get(0);  // ???
                    // TBD: reuse bean???
                    // ...
                    // The problem is,
                    // even for the same longUrl,
                    // the ShortLink object may be associated with different data/meta data  (e.g., message, expiration time, etc...)
                    // ....
                    // so, it's not easy to reuse the existing shortLink
                    // unless we compare all fields of an existing shortLink and the input values
                    // ....
                    // TBD
                    // ...
                    
                    
                    // TBD:
                    // Just return error if failIfShortUrlExists == true, for now...
                    // But, how the client is supposed to deal with the error???
                    // ????
                    // A better option is,
                    // the client checks if the short url already exists first,
                    // and if so reuse it, etc...
                    
                    ShortLink bean = beans.get(0);  // ???
                    String existingBeanGuid = bean.getGuid();
                    String existingShortUrl = bean.getShortUrl();
                    throw new BadRequestException("ShortLink, " + existingShortUrl + " (" + existingBeanGuid + "), already exists for longUrl, " + longUrl + " and owner = " + owner);
                } else {
                    // nothing to do...
                }        
            } else {
                // TBD:
                // Check the flag, reuseExistingShortUrl, next ???
                // ....
                Boolean reuseExistingShortUrl = shortLink.isReuseExistingShortUrl();
                if(reuseExistingShortUrl != null && reuseExistingShortUrl == true) {
                    List<ShortLink> beans = ShortLinkAppHelper.getInstance().findShortLinksByLongUrl(longUrl, owner);
                    if(beans != null && beans.size() > 0) {
                        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "ShortLinks with longUrl, " + longUrl + ", already exist for owner = " + owner);

                        // This is a heck....
                        // We have no easy way to return the bean object..... 
                        ShortLink bean = beans.get(0);  // ???
                        String existingBeanGuid = bean.getGuid();
                        String existingShortUrl = bean.getShortUrl();
                        log.warning("ShortLink, " + existingShortUrl + " (" + existingBeanGuid + "), already exists for longUrl, " + longUrl + " and owner = " + owner);
                        throw new ResourceAlreadyPresentException(existingBeanGuid);   // Piggybacking on the "message" field of an exception...
                        // This is a heck...
                    } else {
                        // nothing to do...
                    }
                } else {
                    // ignore ...
                }                
            }
        }

        

        // temporary
        String username = shortLink.getUsername();
        String usercode = shortLink.getUsercode();
        // TBD:
        // Validate username/usercode, if set, based on user auth status
        // ...
        boolean usercodeCheckedAndNotFound = false;
        if(username != null && !username.isEmpty()) {
            // 
        } else {
            // Look up username from DB???
            // .....
            if(owner != null && !owner.isEmpty()) {
                User userBean = UserAppHelper.getInstance().getUser(owner);
                if(userBean != null) {
                    username = userBean.getUsername();
                    if(username != null && !username.isEmpty()) {
                        shortLink.setUsername(username);
                        if(log.isLoggable(Level.FINE)) log.fine("Username set to " + username + " for shortLink: owner = " + owner);                        
                    } else {
                        if(log.isLoggable(Level.INFO)) log.info("Username not found in the user object: owner = " + owner);                        
                    }
                    usercode = userBean.getUsercode();
                    if(usercode != null && !usercode.isEmpty()) {
                        shortLink.setUsercode(usercode);
                        if(log.isLoggable(Level.FINE)) log.fine("Usercode set to " + usercode + " for shortLink: owner = " + owner);                        
                    } else {
                        usercodeCheckedAndNotFound = true;
                        if(log.isLoggable(Level.INFO)) log.info("Usercode not found in the user object: owner = " + owner);                        
                    }
                } else {
                    // ???
                    // Can this happen???
                    log.warning("User not found for owner = " + owner);
                }
            } else {
                // ????
                // What to do????
            }            
        }
        
        
        
        
        
        // TBD:
        // This whole logic of generating domain/token (==shortUrl) should be moved/encapsulated
        // in a utility method, so that it can be re-usable....
        // ....
        
        
        
        
        // Note: This part is an updated algorithm on 12/03/11.
        // If the usedomain.userurl config var is set to true, then domain is always in the form http://a.com/xyz/.
        //      and, other forms such as http://a.com/!xyz/ or http://a.com/@xyz/ are not allowed.
        // boolean useDomainUserURL = ConfigUtil.isUseDomainUserURL();
        // TBD ....

        
        
        // temporary
        String domain = shortLink.getDomain();                
        String domainType = shortLink.getDomainType();
        if( domain == null || domain.isEmpty() ) {
            domainType = DomainUtil.validateInitialDomainType(domainType);
            shortLink.setDomainType(domainType);
        } else {
            // If domain is set, we cannot really change the domainType without knowing what domainType the domain belongs to....
            // TBD: Always use custom when domain is already set???  Probably not.... (Domain may have been set by the frontend, not necessarily by the user)
            // ?????
            if(!DomainType.isValidType(domainType)) {
                // TBD: Create a new domainType for frontend (e.g., Chrome extension) generated domains ?????
                if(log.isLoggable(Level.INFO)) log.info("Input domainType = " + domainType + " will be ignored. Using the custom domain type.");
                domainType = DomainType.TYPE_CUSTOM;
                shortLink.setDomainType(domainType);   // ????
            }            
        }

        // temporary
        if(domain == null || domain.isEmpty()) {   // This is generally true when creating...
            // "Select" a domain
            if(domainType.equals(DomainType.TYPE_USERCODE)) {
                // TBD:
                if(usercode == null || usercode.isEmpty()) {
                    if(usercodeCheckedAndNotFound == true) {
                        // already checked above while checking the username, don't try again...
                    } else {
                        // Get usercode from User table
                        // ...
                        if(owner != null && !owner.isEmpty()) {
                            User userBean = UserAppHelper.getInstance().getUser(owner);
                            if(userBean != null) {
                                usercode = userBean.getUsercode();
                                if(usercode != null && !usercode.isEmpty()) {
                                    shortLink.setUsercode(usercode);
                                    if(log.isLoggable(Level.FINE)) log.fine("Usercode set to " + usercode + " for shortLink: owner = " + owner);                        
                                } else {
                                    if(log.isLoggable(Level.INFO)) log.info("Usercode not found in the user object: owner = " + owner);                        
                                }
                            } else {
                                // ???
                                // Can this happen???
                                log.warning("User not found for owner = " + owner);
                            }
                        } else {
                            // ????
                        }
                    }
                }
                // ....
                domain = DomainUtil.getDomain(domainType, owner, username, usercode);   // ???
            } else {
                
//                if(!DomainType.isValidType(domainType)) {
//                    domainType = ConfigUtil.getSystemDefaultDomainType();  // ????
//                    shortLink.setDomainType(domainType);   // ????
//                }
                
                domain = DomainUtil.getDomain(domainType, owner, username);   // ???
            }
            if(domain == null || domain.isEmpty()) {
                // Error. This cannot happen
                throw new BaseException("Domain is null. ShortLink cannot be created.");
            }
            shortLink.setDomain(domain);
        } else {
            // TBD: Validate domain???
            // (E.g., check against a predefined list/database, etc. ???)
            // etc....
        }

        
        
        
        // TBD:
        // This whole else block is a bit flakey.
        // Will probably need refactoring...
        
        // TBD:
        // Sequential token type is somewhat dubious...
        // it cannot be used as an input tokenType, but it may be used in saving..
        // it will be used as queyr filter...
        // this is really messy...
        
        
        // TBD:
        String tokenGenerationMethod = ConfigUtil.getTokenGenerationMethod();
        boolean isUniqueToken = ConfigUtil.isTokenGenerationUniqueToken();
        // ....

        // temporary
        String token = shortLink.getToken();
        String tokenType = shortLink.getTokenType();
        if(token == null || token.isEmpty()) {
            // TBD: Note that tokenType cannot be custom at this point...
            //      Throw exception??? Or, just use system default????
            if(!TokenType.isValidType(tokenType) 
                    || tokenType.equals(TokenType.TYPE_CUSTOM)
                    || (! TokenGenerationMethod.METHOD_SEQUENTIAL.equals(tokenGenerationMethod) && tokenType.equals(TokenType.TYPE_SEQUENTIAL) )) {
                // ???
                if(log.isLoggable(Level.INFO)) log.info("Invalid input tokenType = " + tokenType);
                tokenType = ConfigUtil.getSystemDefaultTokenType();   // This shouldn't return custom...
                shortLink.setTokenType(tokenType);
                if(log.isLoggable(Level.INFO)) log.info("tokenType is now set to " + tokenType);
            }
            
            // Only used if tokenType.equals(TokenType.TYPE_SASSY)
            String sassyTokenType = shortLink.getSassyTokenType();
            
            
            // Note:
            // See the comment in the TokenGenerationMethod class...
            // ...
            
            
            if(isUniqueToken == true) {
                // TBD:
                // Note that
                // If we cannot find the valid token for the next max_queries,
                // then we will PERMANENTLY get stuck. The user cannot get any new token for following subsequent tries...
                // This is cearly a serious problem.....
                final int max_queries = 20;   // temporary
                boolean isUnique = false;
                int query_count = 0;
                while(isUnique == false && ++query_count <= max_queries) {
                    if(TokenGenerationMethod.METHOD_SEQUENTIAL.equals(tokenGenerationMethod)) {
                        // Note: The input tokenType is used to generate the next sequential token...
                        // but when in saving use the sequential type.....
                        // If input tokenType == sequential, then the system default token type is used....
                        // TBD: Does user-provided tokenType really make sense?
                        //      Or more generally, does changing token type over time for the sequential generation make sense???  
                        token = TokenGeneratorUtil.generateNextSequentialToken(domain, tokenType);
                        tokenType = TokenType.TYPE_SEQUENTIAL;
                    } else {  // if(TokenGenerationMethod.METHOD_RANDOM.equals(tokenGenerationMethod)) {
                        token = TokenGeneratorUtil.generateRandomToken(tokenType, sassyTokenType);    
                    }
                    try {
                        String existingShortUrlKey = ShortLinkAppHelper.getInstance().findShortLinkKeyByToken(token, domain);
                        if(existingShortUrlKey == null) {
                            if(log.isLoggable(Level.FINE)) log.fine("No existing shortUrl found for token = " + token + "; domain = " + domain);
                            isUnique = true;
                        } else {
                            if(log.isLoggable(Level.FINE)) log.fine("shortUlr already exists for token = " + token + "; domain = " + domain + ". Trying a new token... query_count = " + query_count);
                        }
                    } catch(BaseException bex) {
                        // Error occurred while querying...
                        // In such a case, existingShortUrlKey == null does not mean the shortUrl with given token/domain does not exist.
                        // What to do???
                        log.log(Level.WARNING, "Short URL query failed for token = " + token + "; domain = " + domain + "; query_count = " + query_count);
                    }
                }
                if(isUnique == false) {
                    // Couldn't find the unique token ....
                    log.severe("Failed to find a unique token for tokenType = " + tokenType + "; domain = " + domain);
                    throw new InternalServerErrorException("Failed to find a unique token for tokenType = " + tokenType + " and domain = " + domain);
                } else {
                    if(log.isLoggable(Level.INFO)) log.info("Unique token found: token = " + token + "; domain = " + domain);
                }

            } else {
                if(TokenGenerationMethod.METHOD_SEQUENTIAL.equals(tokenGenerationMethod)) {
                    // Note: The input tokenType is used to generate the next sequential token...
                    // but when saving use the sequential type.....
                    // If input tokenType == sequential, then the system default token type is used....
                    token = TokenGeneratorUtil.generateNextSequentialToken(domain, tokenType);
                    tokenType = TokenType.TYPE_SEQUENTIAL;
                    if(log.isLoggable(Level.INFO)) log.info("Next sequential token generated: token = " + token + "; domain = " + domain);
                    // if(log.isLoggable(Level.INFO)) log.info("tokenType is now set to " + tokenType);
                } else {  // if(TokenGenerationMethod.METHOD_RANDOM.equals(tokenGenerationMethod)) {
                    token = TokenGeneratorUtil.generateRandomToken(tokenType, sassyTokenType);
                    if(log.isLoggable(Level.INFO)) log.info("Random token generated: token = " + token + "; domain = " + domain);
                }
            }

            if(token == null) {
                // Error. This cannot happen
                throw new InternalServerErrorException("Token is null. ShortLink cannot be created.");
            }
            shortLink.setToken(token);
            shortLink.setTokenType(tokenType);
            if(log.isLoggable(Level.INFO)) log.info("tokenType is now set to " + tokenType);
        } else {
            // TBD: This needs to be done even for the above if() block ???
            // ...

            // Reset tokenType????  
            // (Note: This validateShortLink() function is used even during update/refresh...)
            // TBD: Always use custom when token is already set???  Probably not.... (Token may have been set by the frontend, not necessarily by the user)
            //      But, in such a case, it's frontend's responsibility to set proper tokenType...
            if(!TokenType.isValidType(tokenType) || (! TokenGenerationMethod.METHOD_SEQUENTIAL.equals(tokenGenerationMethod) && tokenType.equals(TokenType.TYPE_SEQUENTIAL))) {  
                tokenType = TokenType.TYPE_CUSTOM;   // ???
                shortLink.setTokenType(tokenType);
            }

            
            // TBD: Move this to "validater" class...

            // TBD: Do this only if tokenType == custom, or do it in general  ?????
            if(TokenType.TYPE_CUSTOM.equals(tokenType)) {
                // Validate token???
                // for now, just check min/max lengths, and do some basic sanitization...
                int min = ConfigUtil.getCustomTokenMinLength();
                int max = ConfigUtil.getCustomTokenMaxLength();
                int len = token.length();
                if(len < min || len > max) {
                    log.log(Level.WARNING, "Token length is invalid. token = " + token);
                    throw new BadRequestException("Token length is invalid. token = " + token);
                }
                
                // temporary
                // TBD: Have to go through each reservedWords and see if token.startsWith(word) or token.contains(word), etc....  ????
                if(ReservedWordTokenSet.getInstance().isRservedWord(token)) {
                    log.log(Level.WARNING, "Token is invalid. token = " + token);
                    throw new BadRequestException("Token is invalid. token = " + token);
                }
                // temporary                
                
                String sanitized = TokenUtil.sanitizeToken(token);
                if(!sanitized.equals(token)) {
                    log.warning("(Custom) token has been sanitized: old token = " + token + "; new token = " + sanitized);
                    token = sanitized;
                    shortLink.setToken(token);
                }
                // etc....
                // ....
                
                // TBD:
                // Need really have to check data store to see if the domain+token combo doea not already exist...
                // This should be done in general, but it is more critical for custom tokens due to high probability of collision
                // ....
                
                // In case of custom token, the best algorithm should be
                // (1) check first if the custom token + longurl + user (without domain) already exists.
                // (2) if found, return it. reuse the same short url for the same user and for the same longurl.
                //              (Note: the complication is, the existing bean might be associated with different data/metadata
                //                     such as message and expiration time, etc....)   ???????
                //     if not, continue
                // (3) check if the domain + custom token exists  (or, get all ShorkLinks for the given token)
                // (4) if found, see if domain can be changed (e.g., in case of random domain)
                //               if so, try a different domain, so that domain+token is not already in the DB.
                //               if not, return error.
                //     if not, use domain+token and continue...
                // ....
                
                // Temporary
                // The easiest thing is, if domain+token is already taken, then just bail out (even though we could try a different domain, etc.)
                if(isCreating == true) {   // Check this only for POST.... ???? --> Does this make sense???? what if token has changed in the update???? See the comment below...

                    int tokLen = token.length();
                    Set<String> badWordSet = BadWordRegistry.getBadWordSet(tokLen);
                    if(badWordSet != null && badWordSet.contains(token)) {
                        log.log(Level.WARNING, "Short URL cannot be created for token = " + token + "; domain = " + domain);
                        throw new BadRequestException("Short URL cannot be created for token = " + token + "; domain = " + domain);
                    }
                    
                    String existingShortUrlKey = null;
                    try {
                        if(getCacheInstance() != null) {
                            // getCacheInstance().containsKey() suffices, but just to use the "if(existingShortUrlKey != null)" condition below...
                            existingShortUrlKey = (String) getCacheInstance().get(getCacheKeyForCustomTokenAndDomain(token, domain));
                        }
                        if(existingShortUrlKey == null) {
                            existingShortUrlKey = ShortLinkAppHelper.getInstance().findShortLinkKeyByToken(token, domain);
                        }
                    } catch(BaseException bex) {
                        // Error occurred while querying...
                        // In such a case, existingShortUrlKey == null does not mean the shortUrl with given token/domain does not exist.
                        // What to do???
                        log.log(Level.WARNING, "Short URL query failed for token = " + token + "; domain = " + domain);
                        throw new InternalServerErrorException("Short URL query failed for token = " + token + "; domain = " + domain, bex);
                    }
                    if(existingShortUrlKey != null) {
                        // TBD: 
                        // Note that we really need to get beanGuid not key...
                        // Note: if(guid == beanGuid), then we are probably updating...  hence this may not really be an error.
                        //    But, this cannot happen since we already checked isCreating == true...
                        // ...
                        // TBD:
                        // In case domainType == random, try different domains ???
                        // ....
                        log.warning("Short URL already exists. existingShortUrlKey = " + existingShortUrlKey + "; token = " + token + "; domain = " + domain);
                        throw new BadRequestException("Short URL already exists for token = " + token + " and domain = " + domain);                    
                    } else {
                        // Note that we put this in the cache BEFORE it is actually saved in the DB...
                        // which means that even if the value might be in the cache, the token might not exist in the DB...
                        // The implication of this is that,
                        // the next time the user tries the same token (without saving it previously), it will fail...
                        // --> TBD: Need to take care of this use case.....
                        // See below, re: cache.remove....
                        if(getCacheInstance() != null) {
                            // Note that the actual value is not important, for our purposes (we are only concerned with existence, not the value)...
                            existingShortUrlKey = "---";
                            getCacheInstance().put(getCacheKeyForCustomTokenAndDomain(token, domain), existingShortUrlKey);
                        }
                    }
                }
                // Temporary
                
            } else {
                // ???
                // Keep whatever token is currently set???
                // Or, at least validate ???
                // ....
                // Note that the client (e.g., javascript) may have generated the token... without checking whether the shortUrl already exists, etc...
                // that means, only the TokenGenerationMethod.METHOD_RANDOM makes sense in such use cases...
                // .....
            }
        }
        
        // TBD:
        // The "path" part???
        // etc. ???
        // ...
        
        
        // temporary
        // TBD: Always regenerate shortUrl regardless of whether it has been set in the input????
        // shortUrl is really a "cache" (based on domaina and token).
        // Either use dirty flag (is it possible???) or always update shortUrl to be safe....
        String shortUrl = shortLink.getShortUrl();
        //if(shortUrl == null || shortUrl.isEmpty() || !URLUtil.isValidUrl(shortUrl)) {
            shortUrl = ShortLinkUtil.buildShortUrl(domain, null, token);   // Default Short URL does not include path....
            shortLink.setShortUrl(shortUrl);
            if(log.isLoggable(Level.INFO)) log.info("shortUrl is set to " + shortUrl);
        //} else {
            // TBD:
            // Make sure that shortUrl is "consistent" with domain+token???
            // ....
            //log.info("Input shortUrl = " + shortUrl);
        //}
        

        // temporary
        String redirectType = shortLink.getRedirectType();
        if(!RedirectType.isValidType(redirectType)) {
            // ???
            if(log.isLoggable(Level.INFO)) log.info("Invalid input redirectType = " + redirectType);
            redirectType = ConfigUtil.getSystemDefaultRedirectType();
            shortLink.setRedirectType(redirectType);
            if(log.isLoggable(Level.INFO)) log.info("redirectType is now set to " + redirectType);
        }
        if(redirectType.equals(RedirectType.TYPE_FLASH)) {
            Long flashDuration = shortLink.getFlashDuration();
            if(flashDuration == null || flashDuration <= 0L) {    // ????
                log.info("Invalid input flashDuration = " + flashDuration);
                flashDuration = ConfigUtil.getSystemDefaultFlashDuration();
                // TBD: Check if flashDuration value is valid at this point????
                shortLink.setFlashDuration(flashDuration);
                if(log.isLoggable(Level.INFO)) log.info("flashDuration is now set to " + flashDuration);
            }
        } else {
            // ignore...
        }
        
        
        // TBD:
        // Maximum length for displayMessage???
        // etc. ???
        // ...
        
        // temporary
        // we are deprecating displayMessage and using shortMessage (or, longMessage, possibly in the future)
        // ....
        String shortMessage = shortLink.getShortMessage();
        if(shortMessage != null && !shortMessage.isEmpty()) {    // ???? empty string is valid input ????            
            int shortMsgLen = shortMessage.length();
            if(shortMsgLen > 500) {
                log.warning("shortMessage is longer than 500 chars: shortMsgLen = " + shortMsgLen);
                // shortMessage = shortMessage.substring(0, 500);
                shortMessage = TextUtil.truncateAtWordBoundary(shortMessage, 500, 480, true);
                shortLink.setShortMessage(shortMessage);
            }
        } else {
            // This is done because we are transitioning from displayMessage to shortMessage...
            // Do this only if shortMessage is null (or, empty??)
            String displayMessage = shortLink.getDisplayMessage();
            if(displayMessage != null && !displayMessage.isEmpty()) {
                shortMessage = displayMessage;
                shortLink.setShortMessage(shortMessage);
            }            
        }
        
        
        
        // TBD: Check if the url is "new"
        //       especially when tokenType == custom or generic
        // Throw exception when a record exists for the given domain+token
        // Or, try creating a new domain+token pair...
        // ....
        // TBD: Also, validate tokens
        //      for example, length of a token, characters, ...
        //      some "reserved" words (eg. servlet mapped paths) cannot be used as token either.
        //      Also, token cannot start with "@" or "!", etc.
        // ....
        
        
        
        // TBD:
        // Use deferred task to save bookmarks/keywords
        
        // TBD:
        // The config variables, bookmark/keyword, etc., should really be more than boolean flags.
        // e.g.., "always create bookmark (regardless of bean value)", "create bookmark if bean value is true", and "never create bookmark", etc...
        // ...
        
        // For now,
        // Just use flags.
        // If config flag is true, then use the shortlink bean flag.
        // If config flag is false, never create bookmark/keywords.
        // TBD:
        // We will also need to include user pref (and, appclient setting) into this logic...
        // ....

        boolean isAppNavKeywordEanbled = ConfigUtil.isAppModeNavKeywordEnabled();
        if(log.isLoggable(Level.FINE)) log.fine("isAppNavKeywordEanbled = " + isAppNavKeywordEanbled);
        if(isAppNavKeywordEanbled) {
            Boolean keywordEnabled = shortLink.isKeywordEnabled();
            if(log.isLoggable(Level.INFO)) log.info("keywordEnabled = " + keywordEnabled);
            if(keywordEnabled != null && keywordEnabled == true) {
                try {
                    DeferredTask task = new KeywordLinkTask(shortLink, isCreating);
                    RetryOptions retryOptions = RetryOptions.Builder.withTaskRetryLimit(3);   // Max 3 retries...
                    TaskOptions taskOptions = TaskOptions.Builder.withPayload(task).retryOptions(retryOptions).countdownMillis(2211L);  // 2 second delay...
                    Queue queue = QueueFactory.getDefaultQueue();
                    queue.add(taskOptions);
                } catch(Exception e) {
                    log.log(Level.WARNING, "Task to create keyword link failed.", e);
                }
            } else {
                // ...
            }
        } else {
            // ....
        }

        boolean isAppBookmarkingEnabled = ConfigUtil.isAppModeBookmarkingEnabled();
        if(log.isLoggable(Level.FINE)) log.fine("isAppBookmarkingEnabled = " + isAppBookmarkingEnabled);
        if(isAppBookmarkingEnabled) {
            Boolean bookmarkEnabled = shortLink.isBookmarkEnabled();
            if(log.isLoggable(Level.INFO)) log.info("bookmarkEnabled = " + bookmarkEnabled);
            if(bookmarkEnabled != null && bookmarkEnabled == true) {
                DeferredTask task = new KeywordLinkTask(shortLink, isCreating);
                RetryOptions retryOptions = RetryOptions.Builder.withTaskRetryLimit(3);   // Max 3 retries...
                TaskOptions taskOptions = TaskOptions.Builder.withPayload(task).retryOptions(retryOptions).countdownMillis(2211L);  // 2 second delay...
                Queue queue = QueueFactory.getDefaultQueue();
                queue.add(taskOptions);
            } else {
                // ... 
            }
        } else {
            // ...
        }
        
        
        
        // etc...
        // .....

        
        
        return shortLink;
    }
    
    
    // TBD:
    private ShortLinkBean enhanceShortLink(ShortLinkBean shortLink, boolean isCreating) throws BaseException
    {
        // TBD:L
        // Look location, fill in geoCell, etc....
        // ????

        return shortLink;
    }

    
    
    // temporary
    private static Cache getCacheInstance()
    {
        return CacheHelper.getDayLongInstance().getCache();
    }
   
    private static String getCacheKeyForCustomTokenAndDomain(String token, String domain)
    {
        String key = "ShortLinkAppService-CustomToken-" + token + "-" + domain;
        return key;
    }

    
    
    
    
    private static final class KeywordLinkTask implements DeferredTask 
    {
        private static final long serialVersionUID = 1L;

        private static KeywordLinkService keywordLinkService = null;
        private static KeywordLinkService getKeywordLinkService()
        {
            if(keywordLinkService == null) {
                keywordLinkService = new KeywordLinkServiceImpl();
            }
            return keywordLinkService;
        }

        private ShortLink mShortLink = null;
        private KeywordLinkBean mKeywordLink = null;
        private boolean mIsCreating;
        public KeywordLinkTask(ShortLink shortLink, boolean isCreating) 
        {
            mShortLink = shortLink;
            mIsCreating = isCreating;
            // TBD: Build mKeywordLink
            // --> Use lazy initialization????
            // ...
        }
        
        // TBD:
        private void buildKeywordLink()
        {
            if(mIsCreating) {
                mKeywordLink = new KeywordLinkBean();
                // ...
            } else {
                try {
                    String shortLinkGuid = mShortLink.getGuid();
                    String filter = "shortLink=='" + shortLinkGuid + "'";
                    // String ordering = "createdTime desc";
                    String ordering = null;
                    List<String> keys = getKeywordLinkService().findKeywordLinkKeys(filter, ordering, null, null, null, null, 0L, 2);
                    if(keys != null && !keys.isEmpty()) {
                        if(keys.size() > 1) {
                            // Can this happen???
                            log.log(Level.WARNING, "More than one KeywordLink found for shortLinkGuid = " + shortLinkGuid);
                        }
                        // Full fetch...
                        String key = keys.get(0);
                        mKeywordLink = (KeywordLinkBean) getKeywordLinkService().getKeywordLink(key);
                        // ....
                    } else {
                        // ????
                        if(log.isLoggable(Level.INFO)) log.info("No KeywordLink found for shortLinkGuid = " + shortLinkGuid);
                        mKeywordLink = new KeywordLinkBean();
                    }
                } catch (BaseException e) {
                    log.log(Level.WARNING, "KeywordLink fetch failed.", e);
                }
            }
            
            if(mKeywordLink != null) {
                // Update
                // Is there a better way to do this???

                String appClient = mShortLink.getAppClient();
                if(appClient != null) {
                    mKeywordLink.setAppClient(appClient);
                }
                String clientRootDomain = mShortLink.getClientRootDomain();
                if(clientRootDomain != null) {
                    mKeywordLink.setClientRootDomain(clientRootDomain);
                }
                String user = mShortLink.getOwner();
                if(user != null) {
                    mKeywordLink.setUser(user);
                }
                String domain = mShortLink.getDomain();
                if(domain != null) {
                    mKeywordLink.setDomain(domain);
                }
                String token = mShortLink.getToken();
                if(token != null) {
                    mKeywordLink.setToken(token);
                }
                String longUrl = mShortLink.getLongUrl();
                if(longUrl != null) {
                    mKeywordLink.setLongUrl(longUrl);
                }
                String shortUrl = mShortLink.getShortUrl();
                if(shortUrl != null) {
                    mKeywordLink.setShortUrl(shortUrl);
                }
                // ????
                String keyword = token;
                if(keyword != null) {
                    mKeywordLink.setKeyword(keyword);
                }
                // ????

                // Etc...
                // ...
                
                
            } else {
                // What to do???
            }
        }

        @Override
        public void run()
        {
            log.fine("KeywordLinkTask.run().");
            if(mKeywordLink == null) {
                buildKeywordLink();
            }
            if(mKeywordLink != null) {
                if(mIsCreating) {
                    try {
                        String guid = getKeywordLinkService().createKeywordLink(mKeywordLink);
                        if(log.isLoggable(Level.INFO)) log.info("createKeywordLink() called: guid = " + guid);
                        // ...
                    } catch (BaseException e) {
                        log.log(Level.WARNING, "KeywordLink create failed.", e);
                    }
                } else {
                    try {
                        Boolean suc = getKeywordLinkService().updateKeywordLink(mKeywordLink);
                        if(log.isLoggable(Level.INFO)) log.info("updateKeywordLink() called: suc = " + suc);
                        // ....
                    } catch (BaseException e) {
                        log.log(Level.WARNING, "KeywordLink update failed.", e);
                    }
                }
            } else {
                // ????
            }
        }                
    }

    private static final class BookmarkLinkTask implements DeferredTask 
    {
        private static final long serialVersionUID = 1L;

        private static BookmarkLinkService bookmarkLinkService = null;
        private static BookmarkLinkService getBookmarkLinkService()
        {
            if(bookmarkLinkService == null) {
                bookmarkLinkService = new BookmarkLinkServiceImpl();
            }
            return bookmarkLinkService;
        }

        private ShortLink mShortLink = null;
        private BookmarkLinkBean mBookmarkLink = null;
        private boolean mIsCreating;
        public BookmarkLinkTask(ShortLink shortLink, boolean isCreating) 
        {
            mShortLink = shortLink;
            mIsCreating = isCreating;
            // TBD: Build mBookmarkLink
            // --> Use lazy initialization????
            // ...
        }
        
        // TBD:
        private void buildBookmarkLink()
        {
            if(mIsCreating) {
                mBookmarkLink = new BookmarkLinkBean();
                // ...
            } else {
                try {
                    String shortLinkGuid = mShortLink.getGuid();
                    String filter = "shortLink=='" + shortLinkGuid + "'";
                    // String ordering = "createdTime desc";
                    String ordering = null;
                    List<String> keys = getBookmarkLinkService().findBookmarkLinkKeys(filter, ordering, null, null, null, null, 0L, 2);
                    if(keys != null && !keys.isEmpty()) {
                        if(keys.size() > 1) {
                            // Can this happen???
                            log.log(Level.WARNING, "More than one BookmarkLink found for shortLinkGuid = " + shortLinkGuid);
                        }
                        // Full fetch...
                        String key = keys.get(0);
                        mBookmarkLink = (BookmarkLinkBean) getBookmarkLinkService().getBookmarkLink(key);
                        // ....
                    } else {
                        // ????
                        if(log.isLoggable(Level.INFO)) log.info("No BookmarkLink found for shortLinkGuid = " + shortLinkGuid);
                        mBookmarkLink = new BookmarkLinkBean();
                    }
                } catch (BaseException e) {
                    log.log(Level.WARNING, "BookmarkLink fetch failed.", e);
                }
            }
            
            if(mBookmarkLink != null) {
                // Update
                // Is there a better way to do this???

                String appClient = mShortLink.getAppClient();
                if(appClient != null) {
                    mBookmarkLink.setAppClient(appClient);
                }
                String clientRootDomain = mShortLink.getClientRootDomain();
                if(clientRootDomain != null) {
                    mBookmarkLink.setClientRootDomain(clientRootDomain);
                }
                String user = mShortLink.getOwner();
                if(user != null) {
                    mBookmarkLink.setUser(user);
                }
                String domain = mShortLink.getDomain();
                if(domain != null) {
                    mBookmarkLink.setDomain(domain);
                }
                String token = mShortLink.getToken();
                if(token != null) {
                    mBookmarkLink.setToken(token);
                }
                String longUrl = mShortLink.getLongUrl();
                if(longUrl != null) {
                    mBookmarkLink.setLongUrl(longUrl);
                }
                String shortUrl = mShortLink.getShortUrl();
                if(shortUrl != null) {
                    mBookmarkLink.setShortUrl(shortUrl);
                }
                // ????
                // KeywordLink ????
                // ????

                // Etc...
                // ...
                
                
            } else {
                // What to do???
            }
        }

        @Override
        public void run()
        {
            log.fine("BookmarkLinkTask.run().");
            if(mBookmarkLink == null) {
                buildBookmarkLink();
            }
            if(mBookmarkLink != null) {
                if(mIsCreating) {
                    try {
                        String guid = getBookmarkLinkService().createBookmarkLink(mBookmarkLink);
                        if(log.isLoggable(Level.INFO)) log.info("createBookmarkLink() called: guid = " + guid);
                        // ...
                    } catch (BaseException e) {
                        log.log(Level.WARNING, "BookmarkLink create failed.", e);
                    }
                } else {
                    try {
                        Boolean suc = getBookmarkLinkService().updateBookmarkLink(mBookmarkLink);
                        if(log.isLoggable(Level.INFO)) log.info("updateBookmarkLink() called: suc = " + suc);
                        // ....
                    } catch (BaseException e) {
                        log.log(Level.WARNING, "BookmarkLink update failed.", e);
                    }
                }
            } else {
                // ????
            }
        }                
    }

    
}
