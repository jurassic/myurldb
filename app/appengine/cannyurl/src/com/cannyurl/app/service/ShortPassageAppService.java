package com.cannyurl.app.service;

import java.util.List;
import java.util.logging.Logger;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.ShortPassage;
import com.myurldb.ws.ShortPassageAttribute;
import com.cannyurl.af.bean.ShortPassageAttributeBean;
import com.cannyurl.af.bean.ShortPassageBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ShortPassageService;
import com.cannyurl.af.service.impl.ShortPassageServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class ShortPassageAppService extends ShortPassageServiceImpl implements ShortPassageService
{
    private static final Logger log = Logger.getLogger(ShortPassageAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public ShortPassageAppService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // ShortPassage related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public ShortPassage getShortPassage(String guid) throws BaseException
    {
        return super.getShortPassage(guid);
    }

    @Override
    public Object getShortPassage(String guid, String field) throws BaseException
    {
        return super.getShortPassage(guid, field);
    }
    
    @Override
    public List<ShortPassage> getAllShortPassages() throws BaseException
    {
        return super.getAllShortPassages();
    }

    @Override
    public List<ShortPassage> findShortPassages(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findShortPassages(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createShortPassage(ShortPassage shortPassage) throws BaseException
    {
        return super.createShortPassage(shortPassage);
    }

    @Override
    public ShortPassage constructShortPassage(ShortPassage shortPassage) throws BaseException
    {
        return super.constructShortPassage(shortPassage);
    }
        
    @Override
    public Boolean updateShortPassage(ShortPassage shortPassage) throws BaseException
    {
        return super.updateShortPassage(shortPassage);
    }
        
    @Override
    public ShortPassage refreshShortPassage(ShortPassage shortPassage) throws BaseException
    {
        return super.refreshShortPassage(shortPassage);
    }

    @Override
    public Boolean deleteShortPassage(String guid) throws BaseException
    {
        return super.deleteShortPassage(guid);
    }

    @Override
    public Boolean deleteShortPassage(ShortPassage shortPassage) throws BaseException
    {
        return super.deleteShortPassage(shortPassage);
    }

}
