package com.cannyurl.app.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.SpeedDial;
import com.cannyurl.af.bean.SpeedDialBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.SpeedDialService;
import com.cannyurl.af.service.impl.SpeedDialServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class SpeedDialAppService extends SpeedDialServiceImpl implements SpeedDialService
{
    private static final Logger log = Logger.getLogger(SpeedDialAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public SpeedDialAppService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // SpeedDial related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public SpeedDial getSpeedDial(String guid) throws BaseException
    {
        return super.getSpeedDial(guid);
    }

    @Override
    public Object getSpeedDial(String guid, String field) throws BaseException
    {
        return super.getSpeedDial(guid, field);
    }

    @Override
    public List<SpeedDial> getSpeedDials(List<String> guids) throws BaseException
    {
        return super.getSpeedDials(guids);
    }

    @Override
    public List<SpeedDial> getAllSpeedDials() throws BaseException
    {
        return super.getAllSpeedDials();
    }

    @Override
    public List<String> getAllSpeedDialKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllSpeedDialKeys(ordering, offset, count);
    }

    @Override
    public List<SpeedDial> findSpeedDials(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findSpeedDials(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findSpeedDialKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findSpeedDialKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createSpeedDial(SpeedDial speedDial) throws BaseException
    {
        return super.createSpeedDial(speedDial);
    }

    @Override
    public SpeedDial constructSpeedDial(SpeedDial speedDial) throws BaseException
    {
        return super.constructSpeedDial(speedDial);
    }


    @Override
    public Boolean updateSpeedDial(SpeedDial speedDial) throws BaseException
    {
        return super.updateSpeedDial(speedDial);
    }
        
    @Override
    public SpeedDial refreshSpeedDial(SpeedDial speedDial) throws BaseException
    {
        return super.refreshSpeedDial(speedDial);
    }

    @Override
    public Boolean deleteSpeedDial(String guid) throws BaseException
    {
        return super.deleteSpeedDial(guid);
    }

    @Override
    public Boolean deleteSpeedDial(SpeedDial speedDial) throws BaseException
    {
        return super.deleteSpeedDial(speedDial);
    }

    @Override
    public Integer createSpeedDials(List<SpeedDial> speedDials) throws BaseException
    {
        return super.createSpeedDials(speedDials);
    }

    // TBD
    //@Override
    //public Boolean updateSpeedDials(List<SpeedDial> speedDials) throws BaseException
    //{
    //}

}
