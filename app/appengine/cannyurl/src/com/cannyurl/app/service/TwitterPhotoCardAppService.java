package com.cannyurl.app.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.TwitterPhotoCard;
import com.cannyurl.af.bean.TwitterPhotoCardBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.TwitterPhotoCardService;
import com.cannyurl.af.service.impl.TwitterPhotoCardServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class TwitterPhotoCardAppService extends TwitterPhotoCardServiceImpl implements TwitterPhotoCardService
{
    private static final Logger log = Logger.getLogger(TwitterPhotoCardAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public TwitterPhotoCardAppService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // TwitterPhotoCard related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public TwitterPhotoCard getTwitterPhotoCard(String guid) throws BaseException
    {
        return super.getTwitterPhotoCard(guid);
    }

    @Override
    public Object getTwitterPhotoCard(String guid, String field) throws BaseException
    {
        return super.getTwitterPhotoCard(guid, field);
    }

    @Override
    public List<TwitterPhotoCard> getTwitterPhotoCards(List<String> guids) throws BaseException
    {
        return super.getTwitterPhotoCards(guids);
    }

    @Override
    public List<TwitterPhotoCard> getAllTwitterPhotoCards() throws BaseException
    {
        return super.getAllTwitterPhotoCards();
    }

    @Override
    public List<String> getAllTwitterPhotoCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllTwitterPhotoCardKeys(ordering, offset, count);
    }

    @Override
    public List<TwitterPhotoCard> findTwitterPhotoCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findTwitterPhotoCards(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findTwitterPhotoCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findTwitterPhotoCardKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createTwitterPhotoCard(TwitterPhotoCard twitterPhotoCard) throws BaseException
    {
        return super.createTwitterPhotoCard(twitterPhotoCard);
    }

    @Override
    public TwitterPhotoCard constructTwitterPhotoCard(TwitterPhotoCard twitterPhotoCard) throws BaseException
    {
        return super.constructTwitterPhotoCard(twitterPhotoCard);
    }


    @Override
    public Boolean updateTwitterPhotoCard(TwitterPhotoCard twitterPhotoCard) throws BaseException
    {
        return super.updateTwitterPhotoCard(twitterPhotoCard);
    }
        
    @Override
    public TwitterPhotoCard refreshTwitterPhotoCard(TwitterPhotoCard twitterPhotoCard) throws BaseException
    {
        return super.refreshTwitterPhotoCard(twitterPhotoCard);
    }

    @Override
    public Boolean deleteTwitterPhotoCard(String guid) throws BaseException
    {
        return super.deleteTwitterPhotoCard(guid);
    }

    @Override
    public Boolean deleteTwitterPhotoCard(TwitterPhotoCard twitterPhotoCard) throws BaseException
    {
        return super.deleteTwitterPhotoCard(twitterPhotoCard);
    }

    @Override
    public Integer createTwitterPhotoCards(List<TwitterPhotoCard> twitterPhotoCards) throws BaseException
    {
        return super.createTwitterPhotoCards(twitterPhotoCards);
    }

    // TBD
    //@Override
    //public Boolean updateTwitterPhotoCards(List<TwitterPhotoCard> twitterPhotoCards) throws BaseException
    //{
    //}

}
