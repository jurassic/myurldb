package com.cannyurl.app.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.TwitterSummaryCard;
import com.cannyurl.af.bean.TwitterSummaryCardBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.TwitterSummaryCardService;
import com.cannyurl.af.service.impl.TwitterSummaryCardServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class TwitterSummaryCardAppService extends TwitterSummaryCardServiceImpl implements TwitterSummaryCardService
{
    private static final Logger log = Logger.getLogger(TwitterSummaryCardAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public TwitterSummaryCardAppService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // TwitterSummaryCard related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public TwitterSummaryCard getTwitterSummaryCard(String guid) throws BaseException
    {
        return super.getTwitterSummaryCard(guid);
    }

    @Override
    public Object getTwitterSummaryCard(String guid, String field) throws BaseException
    {
        return super.getTwitterSummaryCard(guid, field);
    }

    @Override
    public List<TwitterSummaryCard> getTwitterSummaryCards(List<String> guids) throws BaseException
    {
        return super.getTwitterSummaryCards(guids);
    }

    @Override
    public List<TwitterSummaryCard> getAllTwitterSummaryCards() throws BaseException
    {
        return super.getAllTwitterSummaryCards();
    }

    @Override
    public List<String> getAllTwitterSummaryCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllTwitterSummaryCardKeys(ordering, offset, count);
    }

    @Override
    public List<TwitterSummaryCard> findTwitterSummaryCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findTwitterSummaryCards(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findTwitterSummaryCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findTwitterSummaryCardKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createTwitterSummaryCard(TwitterSummaryCard twitterSummaryCard) throws BaseException
    {
        return super.createTwitterSummaryCard(twitterSummaryCard);
    }

    @Override
    public TwitterSummaryCard constructTwitterSummaryCard(TwitterSummaryCard twitterSummaryCard) throws BaseException
    {
        return super.constructTwitterSummaryCard(twitterSummaryCard);
    }


    @Override
    public Boolean updateTwitterSummaryCard(TwitterSummaryCard twitterSummaryCard) throws BaseException
    {
        return super.updateTwitterSummaryCard(twitterSummaryCard);
    }
        
    @Override
    public TwitterSummaryCard refreshTwitterSummaryCard(TwitterSummaryCard twitterSummaryCard) throws BaseException
    {
        return super.refreshTwitterSummaryCard(twitterSummaryCard);
    }

    @Override
    public Boolean deleteTwitterSummaryCard(String guid) throws BaseException
    {
        return super.deleteTwitterSummaryCard(guid);
    }

    @Override
    public Boolean deleteTwitterSummaryCard(TwitterSummaryCard twitterSummaryCard) throws BaseException
    {
        return super.deleteTwitterSummaryCard(twitterSummaryCard);
    }

    @Override
    public Integer createTwitterSummaryCards(List<TwitterSummaryCard> twitterSummaryCards) throws BaseException
    {
        return super.createTwitterSummaryCards(twitterSummaryCards);
    }

    // TBD
    //@Override
    //public Boolean updateTwitterSummaryCards(List<TwitterSummaryCard> twitterSummaryCards) throws BaseException
    //{
    //}

}
