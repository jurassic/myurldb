package com.cannyurl.app.service;

import java.util.List;
import java.util.logging.Logger;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.UrlRating;
import com.cannyurl.af.bean.UrlRatingBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.UrlRatingService;
import com.cannyurl.af.service.impl.UrlRatingServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class UrlRatingAppService extends UrlRatingServiceImpl implements UrlRatingService
{
    private static final Logger log = Logger.getLogger(UrlRatingAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public UrlRatingAppService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // UrlRating related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public UrlRating getUrlRating(String guid) throws BaseException
    {
        return super.getUrlRating(guid);
    }

    @Override
    public Object getUrlRating(String guid, String field) throws BaseException
    {
        return super.getUrlRating(guid, field);
    }
    
    @Override
    public List<UrlRating> getAllUrlRatings() throws BaseException
    {
        return super.getAllUrlRatings();
    }

    @Override
    public List<UrlRating> findUrlRatings(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findUrlRatings(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createUrlRating(UrlRating urlRating) throws BaseException
    {
        return super.createUrlRating(urlRating);
    }

    @Override
    public UrlRating constructUrlRating(UrlRating urlRating) throws BaseException
    {
        return super.constructUrlRating(urlRating);
    }
        
    @Override
    public Boolean updateUrlRating(UrlRating urlRating) throws BaseException
    {
        return super.updateUrlRating(urlRating);
    }
        
    @Override
    public UrlRating refreshUrlRating(UrlRating urlRating) throws BaseException
    {
        return super.refreshUrlRating(urlRating);
    }

    @Override
    public Boolean deleteUrlRating(String guid) throws BaseException
    {
        return super.deleteUrlRating(guid);
    }

    @Override
    public Boolean deleteUrlRating(UrlRating urlRating) throws BaseException
    {
        return super.deleteUrlRating(urlRating);
    }

}
