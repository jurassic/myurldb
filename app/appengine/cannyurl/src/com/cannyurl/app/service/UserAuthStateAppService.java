package com.cannyurl.app.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.GaeAppStruct;
import com.myurldb.ws.ExternalUserIdStruct;
import com.myurldb.ws.UserAuthState;
import com.cannyurl.af.bean.GaeAppStructBean;
import com.cannyurl.af.bean.ExternalUserIdStructBean;
import com.cannyurl.af.bean.UserAuthStateBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.UserAuthStateService;
import com.cannyurl.af.service.impl.UserAuthStateServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class UserAuthStateAppService extends UserAuthStateServiceImpl implements UserAuthStateService
{
    private static final Logger log = Logger.getLogger(UserAuthStateAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public UserAuthStateAppService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // UserAuthState related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public UserAuthState getUserAuthState(String guid) throws BaseException
    {
        return super.getUserAuthState(guid);
    }

    @Override
    public Object getUserAuthState(String guid, String field) throws BaseException
    {
        return super.getUserAuthState(guid, field);
    }

    @Override
    public List<UserAuthState> getUserAuthStates(List<String> guids) throws BaseException
    {
        return super.getUserAuthStates(guids);
    }

    @Override
    public List<UserAuthState> getAllUserAuthStates() throws BaseException
    {
        return super.getAllUserAuthStates();
    }

    @Override
    public List<String> getAllUserAuthStateKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllUserAuthStateKeys(ordering, offset, count);
    }

    @Override
    public List<UserAuthState> findUserAuthStates(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findUserAuthStates(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findUserAuthStateKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findUserAuthStateKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createUserAuthState(UserAuthState userAuthState) throws BaseException
    {
        return super.createUserAuthState(userAuthState);
    }

    @Override
    public UserAuthState constructUserAuthState(UserAuthState userAuthState) throws BaseException
    {
        return super.constructUserAuthState(userAuthState);
    }


    @Override
    public Boolean updateUserAuthState(UserAuthState userAuthState) throws BaseException
    {
        return super.updateUserAuthState(userAuthState);
    }
        
    @Override
    public UserAuthState refreshUserAuthState(UserAuthState userAuthState) throws BaseException
    {
        return super.refreshUserAuthState(userAuthState);
    }

    @Override
    public Boolean deleteUserAuthState(String guid) throws BaseException
    {
        return super.deleteUserAuthState(guid);
    }

    @Override
    public Boolean deleteUserAuthState(UserAuthState userAuthState) throws BaseException
    {
        return super.deleteUserAuthState(userAuthState);
    }

    @Override
    public Integer createUserAuthStates(List<UserAuthState> userAuthStates) throws BaseException
    {
        return super.createUserAuthStates(userAuthStates);
    }

    // TBD
    //@Override
    //public Boolean updateUserAuthStates(List<UserAuthState> userAuthStates) throws BaseException
    //{
    //}

}
