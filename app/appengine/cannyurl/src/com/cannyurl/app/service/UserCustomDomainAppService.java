package com.cannyurl.app.service;

import java.util.List;
import java.util.logging.Logger;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.UserCustomDomain;
import com.cannyurl.af.bean.UserCustomDomainBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.UserCustomDomainService;
import com.cannyurl.af.service.impl.UserCustomDomainServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class UserCustomDomainAppService extends UserCustomDomainServiceImpl implements UserCustomDomainService
{
    private static final Logger log = Logger.getLogger(UserCustomDomainAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public UserCustomDomainAppService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // UserCustomDomain related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public UserCustomDomain getUserCustomDomain(String guid) throws BaseException
    {
        return super.getUserCustomDomain(guid);
    }

    @Override
    public Object getUserCustomDomain(String guid, String field) throws BaseException
    {
        return super.getUserCustomDomain(guid, field);
    }
    
    @Override
    public List<UserCustomDomain> getAllUserCustomDomains() throws BaseException
    {
        return super.getAllUserCustomDomains();
    }

    @Override
    public List<UserCustomDomain> findUserCustomDomains(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findUserCustomDomains(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createUserCustomDomain(UserCustomDomain userCustomDomain) throws BaseException
    {
        return super.createUserCustomDomain(userCustomDomain);
    }

    @Override
    public UserCustomDomain constructUserCustomDomain(UserCustomDomain userCustomDomain) throws BaseException
    {
        return super.constructUserCustomDomain(userCustomDomain);
    }
        
    @Override
    public Boolean updateUserCustomDomain(UserCustomDomain userCustomDomain) throws BaseException
    {
        return super.updateUserCustomDomain(userCustomDomain);
    }
        
    @Override
    public UserCustomDomain refreshUserCustomDomain(UserCustomDomain userCustomDomain) throws BaseException
    {
        return super.refreshUserCustomDomain(userCustomDomain);
    }

    @Override
    public Boolean deleteUserCustomDomain(String guid) throws BaseException
    {
        return super.deleteUserCustomDomain(guid);
    }

    @Override
    public Boolean deleteUserCustomDomain(UserCustomDomain userCustomDomain) throws BaseException
    {
        return super.deleteUserCustomDomain(userCustomDomain);
    }

}
