package com.cannyurl.app.service;

import java.util.List;
import java.util.logging.Logger;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.UserRating;
import com.cannyurl.af.bean.UserRatingBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.UserRatingService;
import com.cannyurl.af.service.impl.UserRatingServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class UserRatingAppService extends UserRatingServiceImpl implements UserRatingService
{
    private static final Logger log = Logger.getLogger(UserRatingAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public UserRatingAppService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // UserRating related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public UserRating getUserRating(String guid) throws BaseException
    {
        return super.getUserRating(guid);
    }

    @Override
    public Object getUserRating(String guid, String field) throws BaseException
    {
        return super.getUserRating(guid, field);
    }
    
    @Override
    public List<UserRating> getAllUserRatings() throws BaseException
    {
        return super.getAllUserRatings();
    }

    @Override
    public List<UserRating> findUserRatings(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findUserRatings(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createUserRating(UserRating userRating) throws BaseException
    {
        return super.createUserRating(userRating);
    }

    @Override
    public UserRating constructUserRating(UserRating userRating) throws BaseException
    {
        return super.constructUserRating(userRating);
    }
        
    @Override
    public Boolean updateUserRating(UserRating userRating) throws BaseException
    {
        return super.updateUserRating(userRating);
    }
        
    @Override
    public UserRating refreshUserRating(UserRating userRating) throws BaseException
    {
        return super.refreshUserRating(userRating);
    }

    @Override
    public Boolean deleteUserRating(String guid) throws BaseException
    {
        return super.deleteUserRating(guid);
    }

    @Override
    public Boolean deleteUserRating(UserRating userRating) throws BaseException
    {
        return super.deleteUserRating(userRating);
    }

}
