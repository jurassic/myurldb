package com.cannyurl.app.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.UserSetting;
import com.cannyurl.af.bean.UserSettingBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.UserSettingService;
import com.cannyurl.af.service.impl.UserSettingServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class UserSettingAppService extends UserSettingServiceImpl implements UserSettingService
{
    private static final Logger log = Logger.getLogger(UserSettingAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public UserSettingAppService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // UserSetting related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public UserSetting getUserSetting(String guid) throws BaseException
    {
        return super.getUserSetting(guid);
    }

    @Override
    public Object getUserSetting(String guid, String field) throws BaseException
    {
        return super.getUserSetting(guid, field);
    }

    @Override
    public List<UserSetting> getUserSettings(List<String> guids) throws BaseException
    {
        return super.getUserSettings(guids);
    }

    @Override
    public List<UserSetting> getAllUserSettings() throws BaseException
    {
        return super.getAllUserSettings();
    }

    @Override
    public List<String> getAllUserSettingKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllUserSettingKeys(ordering, offset, count);
    }

    @Override
    public List<UserSetting> findUserSettings(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findUserSettings(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findUserSettingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findUserSettingKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createUserSetting(UserSetting userSetting) throws BaseException
    {
        return super.createUserSetting(userSetting);
    }

    @Override
    public UserSetting constructUserSetting(UserSetting userSetting) throws BaseException
    {
        return super.constructUserSetting(userSetting);
    }


    @Override
    public Boolean updateUserSetting(UserSetting userSetting) throws BaseException
    {
        return super.updateUserSetting(userSetting);
    }
        
    @Override
    public UserSetting refreshUserSetting(UserSetting userSetting) throws BaseException
    {
        return super.refreshUserSetting(userSetting);
    }

    @Override
    public Boolean deleteUserSetting(String guid) throws BaseException
    {
        return super.deleteUserSetting(guid);
    }

    @Override
    public Boolean deleteUserSetting(UserSetting userSetting) throws BaseException
    {
        return super.deleteUserSetting(userSetting);
    }

    @Override
    public Integer createUserSettings(List<UserSetting> userSettings) throws BaseException
    {
        return super.createUserSettings(userSettings);
    }

    // TBD
    //@Override
    //public Boolean updateUserSettings(List<UserSetting> userSettings) throws BaseException
    //{
    //}

}
