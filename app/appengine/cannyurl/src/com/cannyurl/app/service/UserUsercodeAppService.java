package com.cannyurl.app.service;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cannyurl.af.bean.UserBean;
import com.cannyurl.af.bean.UserUsercodeBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.UserService;
import com.cannyurl.af.service.UserUsercodeService;
import com.cannyurl.af.service.impl.UserServiceImpl;
import com.cannyurl.af.service.impl.UserUsercodeServiceImpl;
import com.cannyurl.app.util.ConfigUtil;
import com.google.appengine.api.taskqueue.DeferredTask;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.RetryOptions;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.myurldb.ws.BaseException;
import com.myurldb.ws.User;
import com.myurldb.ws.UserUsercode;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.exception.BadRequestException;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;


// Updated.
public class UserUsercodeAppService extends UserUsercodeServiceImpl implements UserUsercodeService
{
    private static final Logger log = Logger.getLogger(UserUsercodeAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }

    public UserUsercodeAppService()
    {
         super();
    }

//    private static UserService userService = null;
//    private static UserService getUserService()
//    {
//        if(userService == null) {
//            userService = new UserServiceImpl();
//        }
//        return userService;
//    }


    
    //////////////////////////////////////////////////////////////////////////
    // UserUsercode related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public UserUsercode getUserUsercode(String guid) throws BaseException
    {
        return super.getUserUsercode(guid);
    }

    @Override
    public Object getUserUsercode(String guid, String field) throws BaseException
    {
        return super.getUserUsercode(guid, field);
    }

    @Override
    public List<UserUsercode> getUserUsercodes(List<String> guids) throws BaseException
    {
        return super.getUserUsercodes(guids);
    }

    @Override
    public List<UserUsercode> getAllUserUsercodes() throws BaseException
    {
        return super.getAllUserUsercodes();
    }

    @Override
    public List<String> getAllUserUsercodeKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllUserUsercodeKeys(ordering, offset, count);
    }

    @Override
    public List<UserUsercode> findUserUsercodes(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findUserUsercodes(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findUserUsercodeKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findUserUsercodeKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }



    @Override
    public String createUserUsercode(UserUsercode userUsercode) throws BaseException
    {
        log.finer("TOP: createUserUsercode()");

        // TBD:
        userUsercode = validateUserUsercode(userUsercode);
        userUsercode = enhanceUserUsercode(userUsercode);
        userUsercode = processUserUsercode(userUsercode, false);
        String guid = super.createUserUsercode(userUsercode);
        
        // TBD: This is somewhat questionable
//        if(getCache() != null) {
//        	if(guid != null) {
//	        	String permalink = userUsercode.getPermalink();
//	        	if(permalink != null && !permalink.isEmpty()) {
//	        		getCache().put(permalink, userUsercode);
//	        	}
//        	}
//        }

        if(log.isLoggable(Level.FINER)) log.finer("BOTTOM: createUserUsercode(): guid = " + guid);
        return guid;
    }

    @Override
    public UserUsercode constructUserUsercode(UserUsercode userUsercode) throws BaseException
    {
        log.finer("TOP: constructUserUsercode()");

        // TBD:

        // log.warning("111111 userUsercode = " + userUsercode);
        userUsercode = validateUserUsercode(userUsercode);
        // log.warning("222222 userUsercode = " + userUsercode);
        userUsercode = enhanceUserUsercode(userUsercode);
        // log.warning("333333 userUsercode = " + userUsercode);
        userUsercode = processUserUsercode(userUsercode, false);
        // log.warning("444444 userUsercode = " + userUsercode);
        userUsercode = super.constructUserUsercode(userUsercode);
        // log.warning("555555 userUsercode = " + userUsercode);

        // TBD: This is somewhat questionable
//        if(getCache() != null) {
//        	if(userUsercode != null) {
//	        	String permalink = userUsercode.getPermalink();
//	        	if(permalink != null && !permalink.isEmpty()) {
//	        		getCache().put(permalink, userUsercode);
//	        	}
//        	}
//        }

        if(log.isLoggable(Level.FINER)) log.finer("BOTTOM: constructUserUsercode(): userUsercode = " + userUsercode);
        return userUsercode;
    }


    @Override
    public Boolean updateUserUsercode(UserUsercode userUsercode) throws BaseException
    {
        log.finer("TOP: updateUserUsercode()");

        // TBD:
        userUsercode = validateUserUsercode(userUsercode);
        userUsercode = enhanceUserUsercode(userUsercode);
        userUsercode = processUserUsercode(userUsercode, true);
        Boolean suc = super.updateUserUsercode(userUsercode);

        // TBD: This is somewhat questionable
        // Old cache need to be removed when the object is updated/deleted...
//        if(getCache() != null) {
//        	if(Boolean.TRUE.equals(suc)) {
//	        	String permalink = userUsercode.getPermalink();
//	        	if(permalink != null && !permalink.isEmpty()) {
//	        		getCache().put(permalink, userUsercode);
//	        	}
//        	}
//        }

        if(log.isLoggable(Level.FINER)) log.finer("BOTTOM: createUserUsercode(): suc = " + suc);
        return suc;
    }
        
    @Override
    public UserUsercode refreshUserUsercode(UserUsercode userUsercode) throws BaseException
    {
        log.finer("TOP: refreshUserUsercode()");

        // TBD:
        userUsercode = validateUserUsercode(userUsercode);
        userUsercode = enhanceUserUsercode(userUsercode);
        userUsercode = processUserUsercode(userUsercode, true);
        userUsercode = super.refreshUserUsercode(userUsercode);

        // TBD: This is somewhat questionable
        // Old cache need to be removed when the object is updated/deleted...
//        if(getCache() != null) {
//        	if(userUsercode != null) {
//	        	String permalink = userUsercode.getPermalink();
//	        	if(permalink != null && !permalink.isEmpty()) {
//	        		getCache().put(permalink, userUsercode);
//	        	}
//        	}
//        }

        if(log.isLoggable(Level.FINER)) log.finer("BOTTOM: refreshUserUsercode(): userUsercode = " + userUsercode);
        return userUsercode;
    }


    @Override
    public Boolean deleteUserUsercode(String guid) throws BaseException
    {
        return super.deleteUserUsercode(guid);
    }

    @Override
    public Boolean deleteUserUsercode(UserUsercode userUsercode) throws BaseException
    {
        return super.deleteUserUsercode(userUsercode);
    }

    @Override
    public Integer createUserUsercodes(List<UserUsercode> userUsercodes) throws BaseException
    {
        return super.createUserUsercodes(userUsercodes);
    }

    // TBD
    //@Override
    //public Boolean updateUserUsercodes(List<UserUsercode> userUsercodes) throws BaseException
    //{
    //}

    



    // TBD...
    private UserUsercodeBean validateUserUsercode(UserUsercode userUsercode) throws BaseException
    {
        log.finer("TOP: validateUserUsercode().");
        UserUsercodeBean bean = null;
        if(userUsercode instanceof UserUsercodeBean) {
            bean = (UserUsercodeBean) userUsercode;
        } else {
            // ????
            throw new BadRequestException("Usercode is of a wrong type. Cannot proceed...");
        }

        String user =  bean.getUser();
        if( user == null || user.isEmpty() ) {
            // ???  --> Note that the bean will not even be saved ....
            throw new BadRequestException("User.guid is not set. Cannot proceed...");
        }

        String usercode =  bean.getUsercode();
        if( usercode == null || usercode.isEmpty() ) {
            // ???  --> Note that the bean will not even be saved ....
            throw new BadRequestException("Usercode is not set. Cannot proceed...");
        }

        int minLength = ConfigUtil.getUsercodeMinLength();
        int maxLength = ConfigUtil.getUsercodeMaxLength();
        int usercodeLength = usercode.length();
        if(usercodeLength < minLength) {
            throw new BadRequestException("Usercode is too short. Cannot proceed...");            
        }
        if(usercodeLength > maxLength) {
            throw new BadRequestException("Usercode is too long. Cannot proceed...");
        }
        
        // TBD:
        // usercode is case-insensitive...
        // We need to change it to lower-case.. (Or, we need an extra column to save the lower-case version, for querying...)
        usercode = usercode.toLowerCase();
        bean.setUsercode(usercode);
        // ....
        
        // TBD:
        // This should be done in UserService as well...
        // Always make usercode lower-case when saving...
        // ...
        

        
        // Guid cannot be null
        String guid = bean.getGuid();
        if(guid == null || guid.isEmpty()) {
            guid = GUID.generate();
            bean.setGuid(guid);
        }

        Long createdTime = bean.getCreatedTime();
        if(createdTime == null || createdTime == 0L) {
            createdTime = System.currentTimeMillis();  // ???
            bean.setCreatedTime(createdTime);
        }
        
        
        // TBD:
        // ...
        

        log.finer("BOTTOM: validateUserUsercode()");
        return bean;
    }
    
    private UserUsercode enhanceUserUsercode(UserUsercode userUsercode) throws BaseException
    {
        log.finer("TOP: enhanceUserUsercode().");
        UserUsercodeBean bean = null;
        if(userUsercode instanceof UserUsercodeBean) {
            bean = (UserUsercodeBean) userUsercode;
        } else {
            // ????
            log.log(Level.WARNING, "userUsercode is of a wrong type.");
            return userUsercode;   // ???
        }
        
        // TBD...
        // ...

        log.finer("BOTTOM: enhanceUserUsercode()");
        return bean;
    }        

    private UserUsercode processUserUsercode(UserUsercode userUsercode) throws BaseException
    {
        return processUserUsercode(userUsercode, false);
    }
    private UserUsercode processUserUsercode(UserUsercode userUsercode, boolean update) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TOP: processUserUsercode(). update = " + update);
        UserUsercodeBean bean = null;
        if(userUsercode instanceof UserUsercodeBean) {
            bean = (UserUsercodeBean) userUsercode;
        } else {
            // ????
            log.log(Level.WARNING, "userUsercode is of a wrong type.");
            return userUsercode;   // ???
        }
        // ...        


        // Do this only when creating ???? No... When UserUsercode.usercode changes, refresh User.usercode as well....
        // if(update == false) {
            try {
                String user = bean.getUser();
                String usercode = bean.getUsercode();
                DeferredTask task = new UserUsercodeRefreshTask(user, usercode, update);
                RetryOptions retryOptions = RetryOptions.Builder.withTaskRetryLimit(3);   // Max 3 retries...
                TaskOptions taskOptions = TaskOptions.Builder.withPayload(task).retryOptions(retryOptions).countdownMillis(1211L);  // 1 second delay...
                Queue queue = QueueFactory.getDefaultQueue();
                queue.add(taskOptions);
            } catch(Exception e) {
                log.log(Level.WARNING, "Usercode recording failed.", e);
            }
        // }


        // TBD:
        // ....

        log.finer("BOTTOM: processUserUsercode()");
        return bean;
    }
    
    // Using TaskQueue...
    private static final class UserUsercodeRefreshTask implements DeferredTask 
    {
        private static final long serialVersionUID = 1L;

        private static UserService userService = null;
        private static UserService getUserService()
        {
            if(userService == null) {
                userService = new UserServiceImpl();
            }
            return userService;
        }

        private String mUser;
        private String mUsercode;
        private boolean mUpdate;    // Not being used....
        public UserUsercodeRefreshTask(String user, String usercode, boolean update) 
        {
            mUser = user;
            mUsercode = usercode;
            mUpdate = update;
        }

        @Override
        public void run()
        {
            log.fine("UserUsercodeRefreshTask.run().");
            try {
                User userBean = getUserService().getUser(mUser);
                if(userBean == null) {
                    // Can this happen????
                    log.log(Level.WARNING, "Failed to get User for guid = " + mUser);
                } else {
                    String oldUsercode = userBean.getUsercode();
                    if(oldUsercode == null || !oldUsercode.equals(mUsercode)) {
                        // Need update
                        ((UserBean) userBean).setUsercode(mUsercode);
                        getUserService().createUser(userBean);
                    }
                }
            } catch (BaseException e) {
                log.log(Level.WARNING, "User Usercode Refresh failed.", e);
            }            
        }                
    }

}
