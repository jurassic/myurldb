package com.cannyurl.app.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.VisitorSetting;
import com.cannyurl.af.bean.VisitorSettingBean;
import com.cannyurl.af.proxy.AbstractProxyFactory;
import com.cannyurl.af.proxy.manager.ProxyFactoryManager;
import com.cannyurl.af.service.ServiceConstants;
import com.cannyurl.af.service.VisitorSettingService;
import com.cannyurl.af.service.impl.VisitorSettingServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class VisitorSettingAppService extends VisitorSettingServiceImpl implements VisitorSettingService
{
    private static final Logger log = Logger.getLogger(VisitorSettingAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public VisitorSettingAppService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // VisitorSetting related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public VisitorSetting getVisitorSetting(String guid) throws BaseException
    {
        return super.getVisitorSetting(guid);
    }

    @Override
    public Object getVisitorSetting(String guid, String field) throws BaseException
    {
        return super.getVisitorSetting(guid, field);
    }

    @Override
    public List<VisitorSetting> getVisitorSettings(List<String> guids) throws BaseException
    {
        return super.getVisitorSettings(guids);
    }

    @Override
    public List<VisitorSetting> getAllVisitorSettings() throws BaseException
    {
        return super.getAllVisitorSettings();
    }

    @Override
    public List<String> getAllVisitorSettingKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllVisitorSettingKeys(ordering, offset, count);
    }

    @Override
    public List<VisitorSetting> findVisitorSettings(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findVisitorSettings(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findVisitorSettingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findVisitorSettingKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createVisitorSetting(VisitorSetting visitorSetting) throws BaseException
    {
        return super.createVisitorSetting(visitorSetting);
    }

    @Override
    public VisitorSetting constructVisitorSetting(VisitorSetting visitorSetting) throws BaseException
    {
        return super.constructVisitorSetting(visitorSetting);
    }


    @Override
    public Boolean updateVisitorSetting(VisitorSetting visitorSetting) throws BaseException
    {
        return super.updateVisitorSetting(visitorSetting);
    }
        
    @Override
    public VisitorSetting refreshVisitorSetting(VisitorSetting visitorSetting) throws BaseException
    {
        return super.refreshVisitorSetting(visitorSetting);
    }

    @Override
    public Boolean deleteVisitorSetting(String guid) throws BaseException
    {
        return super.deleteVisitorSetting(guid);
    }

    @Override
    public Boolean deleteVisitorSetting(VisitorSetting visitorSetting) throws BaseException
    {
        return super.deleteVisitorSetting(visitorSetting);
    }

    @Override
    public Integer createVisitorSettings(List<VisitorSetting> visitorSettings) throws BaseException
    {
        return super.createVisitorSettings(visitorSettings);
    }

    // TBD
    //@Override
    //public Boolean updateVisitorSettings(List<VisitorSetting> visitorSettings) throws BaseException
    //{
    //}

}
