package com.cannyurl.app.stream;

import java.util.logging.Logger;

import com.cannyurl.af.service.ShortLinkService;
import com.cannyurl.app.service.ShortLinkAppService;


// ...
public class ShortUrlStreamManager
{
    private static final Logger log = Logger.getLogger(ShortUrlStreamManager.class.getName());   
    
    // temporary
    private static final long REFRESH_INTERVAL = 60 * 1000L;   // One minute

    
    // TBD:
    private ShortLinkService shortLinkService = null;
    // ...

    
    private ShortUrlStreamManager()
    {
        init();
    }

    // Initialization-on-demand holder.
    private static class ShortUrlStreamManagerHolder
    {
        private static final ShortUrlStreamManager INSTANCE = new ShortUrlStreamManager();
    }

    // Singleton method
    public static ShortUrlStreamManager getInstance()
    {
        return ShortUrlStreamManagerHolder.INSTANCE;
    }

    private void init()
    {
        log.info("BEGIN: init()");

        // TBD:
        // ...
    }
    
    
    // ...
    private ShortLinkService getShortLinkService()
    {
        if(shortLinkService == null) {
            shortLinkService = new ShortLinkAppService();
        }
        return shortLinkService;
    }

    
    
    // TBD:
    // get shortLink list filtered by the ShortLinkType, etc...
    // ....
    
    
    

   
}
