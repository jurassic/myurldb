package com.cannyurl.app.stream.helper;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.sf.jsr107cache.Cache;

import com.cannyurl.af.service.ShortLinkService;
import com.cannyurl.af.util.CacheHelper;
import com.cannyurl.app.helper.ShortLinkAppHelper;
import com.cannyurl.app.service.ShortLinkAppService;
import com.cannyurl.app.util.TimeRangeUtil;
import com.myurldb.ws.CellLatitudeLongitude;
import com.myurldb.ws.ShortLink;


public class ShortUrlStreamServiceHelper
{
    private static final Logger log = Logger.getLogger(ShortUrlStreamServiceHelper.class.getName());

    // TBD:
    private ShortLinkService shortLinkService = null;
    // ...

    // Cache service
//    private Cache mCache = null;
    public Cache getCache()
    {
        // Is this necessary???
//        if(mCache == null) {
//            mCache = CacheHelper.getMediumInstance().getCache();
//        }
//        return mCache;
        //return CacheHelper.getMediumInstance().getCache();      // 2 hours
        return CacheHelper.getQuarterDayInstance().getCache();    // 6 hours
    }


    private ShortUrlStreamServiceHelper() 
    {
//        try {
//            Map<String, Object> props = new HashMap<String, Object>();
//            //props.put(GCacheFactory.EXPIRATION_DELTA, ServiceConstants.CACHE_EXPIRATION_DURATION);
//            props.put(GCacheFactory.EXPIRATION_DELTA, 1800);     // TBD: Get this from Config...
//            CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
//            mCache = cacheFactory.createCache(props);
//        } catch (CacheException e) {
//            log.log(Level.WARNING, "Failed to create Cache service.", e);
//        } catch (Exception e) {
//            log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
//        }
    }

    // Initialization-on-demand holder.
    private static final class ShortUrlStreamHelperHolder
    {
        private static final ShortUrlStreamServiceHelper INSTANCE = new ShortUrlStreamServiceHelper();
    }

    // Singleton method
    public static ShortUrlStreamServiceHelper getInstance()
    {
        return ShortUrlStreamHelperHolder.INSTANCE;
    }
    
    
    // ...
    private ShortLinkService getShortLinkService()
    {
        if(shortLinkService == null) {
            shortLinkService = new ShortLinkAppService();
        }
        return shortLinkService;
    }


    // temporary
    private static final String PREFIX = "ARTICLE-LIST-";
//    @Deprecated
//    private static String getCacheKey(String termType, String dayHour)
//    {
//        String cacheKey = PREFIX + termType + ":" + dayHour;
//        return cacheKey;
//    }
    private static String getCacheKey(CellLatitudeLongitude geoCell, String termType, String dayHour)
    {
        String prefix = PREFIX;
        if(geoCell != null) {
            prefix += geoCell + "-"; 
        } else {
            prefix += "feedcategory-";   // ???? 
        }
        String cacheKey = prefix + termType + ":" + dayHour;
        return cacheKey;
    }
    
        
    
    // Note:
    // fetched lists are sorted by "createdTime ASC"..
    // ....
    
    
    
    // shortLinkKeys/List is sorted by createdTime asc...
    public static Long getMostRecentTimeFromShortLinkList(List<ShortLink> shortLinkList)
    {
        if(shortLinkList == null || shortLinkList.isEmpty()) {
            return null;  // return 0L?
        }
        Long mostRecentTime = null;
        ShortLink struct = shortLinkList.get(shortLinkList.size()-1);
        if(struct != null) {  // struct cannot be null...
            mostRecentTime = struct.getCreatedTime();
        }
        return mostRecentTime;
    }
    

    
    // Place-holder for now...
    public ShortLink getShortLink(String guid)
    {
        return ShortLinkAppHelper.getInstance().getShortLink(guid);
    }

    
    public int getShortLinkCount(CellLatitudeLongitude geoCell, String termType, String dayHour)
    {
        // TBD:
        // Is there any better/more efficient way????
        List<String> shortLinkKeys = fetchShortLinkKeys(geoCell, termType, dayHour);
        if(shortLinkKeys == null) {
            return 0;
        } else {
            return shortLinkKeys.size();
        }        
    }

    
    // TBD: Allow multiple geoCell input....
    @SuppressWarnings("unchecked")
    public List<String> fetchShortLinkKeys(CellLatitudeLongitude geoCell, String termType, String dayHour)
    {
        List<String> keys = null;
        
        // TBD: Need to check this...
        if(getCache() != null) {
            keys = (List<String>) getCache().get(getCacheKey(geoCell, termType, dayHour));
            if(keys != null) {
                if(log.isLoggable(Level.INFO)) log.info("Key list retreived from cache: geoCell = " + geoCell + "; termType = " + termType + "; dayHour = " + dayHour);
            }
        }

        if(keys == null) {
            long[] times = TimeRangeUtil.getMilliRange(termType, dayHour);
            if(times != null && times.length == 2) {
                keys = fetchShortLinkKeys(geoCell, times[0], times[1]);
                // (1) Do this only if times[1] < now - delta.... To allow time for all records within the time range to be saved... ???
                // (2) Do not cache empty list ????  just in case it was due to an error ????
                if(getCache() != null && (times[1] < System.currentTimeMillis() - 120*1000L) && (keys != null && keys.size() > 0)) {  // ????
                    getCache().put(getCacheKey(geoCell, termType, dayHour), keys);
                    if(log.isLoggable(Level.INFO)) log.info("Key list added to cache: geoCell = " + geoCell + "; termType = " + termType + "; dayHour = " + dayHour);
                } else {
                    // ????
                    if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Skipping adding the keys to cache... for geoCell = " + geoCell + "; termType = " + termType + "; dayHour = " + dayHour);
                }
                if(log.isLoggable(Level.FINER)) {
                    if(keys != null) {
                        for(String b : keys) {
                            log.finer("String = " + b);
                        }
                    }
                }
            } else {
                // ???
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Failed to find the start/endTimes for geoCell = " + geoCell + "; termType = " + termType + "; dayHour = " + dayHour);
            }
        }
        return keys;
    }
    public List<ShortLink> fetchShortLinks(CellLatitudeLongitude geoCell, String termType, String dayHour)
    {
        return fetchShortLinks(geoCell, termType, dayHour, false);
    }
    public List<ShortLink> fetchShortLinks(CellLatitudeLongitude geoCell, String termType, String dayHour, boolean fullFetch)
    {
        List<ShortLink> shortLinks = null;
        long[] times = TimeRangeUtil.getMilliRange(termType, dayHour);
        if(times != null && times.length == 2) {
            shortLinks = fetchShortLinks(geoCell, times[0], times[1], fullFetch);
        }
        return shortLinks;
    }



    public int getShortLinkCount(CellLatitudeLongitude geoCell, Long startTime)
    {
        return getShortLinkCount(geoCell, startTime, null);  // null == no limit
    }
    public int getShortLinkCount(CellLatitudeLongitude geoCell, Long startTime, Integer maxCount)
    {
        // TBD:
        // Is there any better/more efficient way????
        List<String> shortLinkKeys = fetchShortLinkKeys(geoCell, startTime, maxCount);
        if(shortLinkKeys == null) {
            return 0;
        } else {
            return shortLinkKeys.size();
        }        
    }

    
    // TBD: Allow multiple geoCell input....
    public List<String> fetchShortLinkKeys(CellLatitudeLongitude geoCell, Long startTime)
    {
        return fetchShortLinkKeys(geoCell, startTime, (Integer) null);  // null == no limit
    }
    public List<String> fetchShortLinkKeys(CellLatitudeLongitude geoCell, Long startTime, Integer maxCount)
    {
        SortedMap<Long, String> shortLinkMap = new TreeMap<Long, String>();
        
        // Unfortunately, fetching only keys would not work... because we need the createdTime of each bean....
//        List<String> shortLinkKeys = ShortLinkHelper.getInstance().findShortLinkKeys(startTime, maxCount);
        List<ShortLink> shortLinks = ShortLinkAppHelper.getInstance().findShortLinks(geoCell, startTime, maxCount);

        if(shortLinks != null && !shortLinks.isEmpty()) {
            for(ShortLink b : shortLinks) {
                String guid = b.getGuid();
                Long createdTime = b.getCreatedTime();
                shortLinkMap.put(createdTime, guid);
            }
        }

        List<String> shortLinkKeyList = new ArrayList<String>(shortLinkMap.values());
        if(maxCount != null && maxCount > 0) {
            if(shortLinkKeyList.size() > maxCount) {
                shortLinkKeyList = shortLinkKeyList.subList(0, maxCount);
            }
        }

        return shortLinkKeyList;
    }

    public List<String> fetchShortLinkKeys(CellLatitudeLongitude geoCell, Long startTime, Long endTime)
    {
        SortedMap<Long, String> shortLinkMap = new TreeMap<Long, String>();
        
        // Unfortunately, fetching only keys would not work... because we need the createdTime of each bean....
//        List<String> shortLinkKeys = ShortLinkHelper.getInstance().findShortLinkKeys(startTime, endTime);
        List<ShortLink> shortLinks = ShortLinkAppHelper.getInstance().findShortLinks(geoCell, startTime, endTime);

        // TBD:
        // Note that if there are more than one shortLinks created at exactly same time,
        // only one will be saved....
        // ( <-- this is entirely possible, because we might be using the same "now" timestamp during shortLink creation... )
        // Need to check....
        // (First of all, need to check if we can have multiple entries with the same value in TreeMap....)
        // ....

        if(shortLinks != null && !shortLinks.isEmpty()) {
            for(ShortLink b : shortLinks) {
                String guid = b.getGuid();
                Long createdTime = b.getCreatedTime();
                shortLinkMap.put(createdTime, guid);
            }
            if(log.isLoggable(Level.FINE)) {
                int size = shortLinks.size();
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "ShortLinks found, size = " + size + ", for geoCell = " + geoCell + "; startTime = " + startTime + "; endTime = " + endTime);
            }
        } else {
            if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "no ShortLinks found for geoCell = " + geoCell + "; startTime = " + startTime + "; endTime = " + endTime);
        }

        List<String> shortLinkKeyList = new ArrayList<String>(shortLinkMap.values());

        return shortLinkKeyList;
    }



    // TBD: Allow multiple geoCell input....
    public List<ShortLink> fetchShortLinks(CellLatitudeLongitude geoCell, Long startTime)
    {
        return fetchShortLinks(geoCell, startTime, (Integer) null);  // null == no limit
    }
    public List<ShortLink> fetchShortLinks(CellLatitudeLongitude geoCell, Long startTime, Integer maxCount)
    {
        return fetchShortLinks(geoCell, startTime, maxCount, false);
    }
    public List<ShortLink> fetchShortLinks(CellLatitudeLongitude geoCell, Long startTime, Integer maxCount, boolean fullFetch)
    {
        SortedMap<Long, ShortLink> shortLinkMap = new TreeMap<Long, ShortLink>();
        
        List<ShortLink> shortLinks = ShortLinkAppHelper.getInstance().findShortLinks(geoCell, startTime, maxCount);

        if(shortLinks != null && !shortLinks.isEmpty()) {
            for(ShortLink b : shortLinks) {
                Long createdTime = b.getCreatedTime();
                shortLinkMap.put(createdTime, b);
            }
        }

        List<ShortLink> shortLinkList = new ArrayList<ShortLink>(shortLinkMap.values());
        if(maxCount != null && maxCount > 0) {
            if(shortLinkList.size() > maxCount) {
                shortLinkList = shortLinkList.subList(0, maxCount);
            }
        }
        
        // TBD:
        // Fetch full objects ????
        if(fullFetch) {
            List<ShortLink> fullShortLinkList = new ArrayList<ShortLink>();
            for(ShortLink a : shortLinkList) {
                String guid = a.getGuid();
                if(a instanceof ShortLink) {
                    ShortLink bean = ShortLinkAppHelper.getInstance().getShortLink(guid);
                    fullShortLinkList.add(bean);
                }
            }
            shortLinkList = fullShortLinkList;
        }
        // ....

        return shortLinkList;
    }

    public List<ShortLink> fetchShortLinks(CellLatitudeLongitude geoCell, Long startTime, Long endTime)
    {
        return fetchShortLinks(geoCell, startTime, endTime, false);
    }
    public List<ShortLink> fetchShortLinks(CellLatitudeLongitude geoCell, Long startTime, Long endTime, boolean fullFetch)
    {
        SortedMap<Long, ShortLink> shortLinkMap = new TreeMap<Long, ShortLink>();
        
        // These lists are chronologically sorted, with older ones front...
        List<ShortLink> shortLinks = ShortLinkAppHelper.getInstance().findShortLinks(geoCell, startTime, endTime);

        if(shortLinks != null && !shortLinks.isEmpty()) {
            for(ShortLink b : shortLinks) {
                Long createdTime = b.getCreatedTime();
                shortLinkMap.put(createdTime, b);
            }
        }

        List<ShortLink> shortLinkList = new ArrayList<ShortLink>(shortLinkMap.values());

        // TBD:
        // Fetch full objects ????
        if(fullFetch) {
            List<ShortLink> fullShortLinkList = new ArrayList<ShortLink>();
            for(ShortLink a : shortLinkList) {
                String guid = a.getGuid();
                if(a instanceof ShortLink) {
                    ShortLink bean = ShortLinkAppHelper.getInstance().getShortLink(guid);
                    fullShortLinkList.add(bean);
                }
            }
            shortLinkList = fullShortLinkList;
        }
        // ....

        return shortLinkList;
    }
    
    
    
}
