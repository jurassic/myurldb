package com.cannyurl.app.stream.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;


// Primarily to shield third party Feed libraries (such as Rome)
public class StreamUtil
{
    private static final Logger log = Logger.getLogger(StreamUtil.class.getName());

    // Static methods only.
    private StreamUtil() {}
    
    
    // TBD
    // ....
    

}
