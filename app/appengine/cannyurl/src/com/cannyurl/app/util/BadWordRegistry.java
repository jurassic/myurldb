package com.cannyurl.app.util;

import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;


public class BadWordRegistry
{
    private static final Logger log = Logger.getLogger(BadWordRegistry.class.getName());

    private BadWordRegistry() {}


    public static Set<String> TwoLetterWords = new HashSet<String>();
    static {
        TwoLetterWords.add("ho");
        // ...
    }

    public static Set<String> ThreeLetterWords = new HashSet<String>();
    static {
        ThreeLetterWords.add("sex");
        ThreeLetterWords.add("ass");
        ThreeLetterWords.add("cum");
        ThreeLetterWords.add("fag");
        ThreeLetterWords.add("gay");
        ThreeLetterWords.add("hoe");
        ThreeLetterWords.add("jap");
        ThreeLetterWords.add("tit");
        ThreeLetterWords.add("vag");
        // ...
    }

    public static Set<String> FourLetterWords = new HashSet<String>();
    static {
        FourLetterWords.add("arse");
        FourLetterWords.add("clit");
        FourLetterWords.add("cock");
        FourLetterWords.add("coon");
        FourLetterWords.add("cunt");
        FourLetterWords.add("damn");
        FourLetterWords.add("dick");
        FourLetterWords.add("dike");
        FourLetterWords.add("poop");
        FourLetterWords.add("dyke");
        FourLetterWords.add("fuck");
        FourLetterWords.add("hell");
        FourLetterWords.add("homo");
        FourLetterWords.add("jizz");
        FourLetterWords.add("kunt");
        FourLetterWords.add("piss");
        FourLetterWords.add("poon");
        FourLetterWords.add("shit");
        FourLetterWords.add("shiz");
        FourLetterWords.add("slut");
        FourLetterWords.add("spic");
        FourLetterWords.add("tard");
        FourLetterWords.add("tits");
        FourLetterWords.add("twat");
        FourLetterWords.add("wank");
        // ...
    }

    public static Set<String> FiveLetterWords = new HashSet<String>();
    static {
        FiveLetterWords.add("bitch");
        FiveLetterWords.add("boner");
        FiveLetterWords.add("dildo");
        FiveLetterWords.add("fucks");
        FiveLetterWords.add("gooch");
        FiveLetterWords.add("kooch");
        FiveLetterWords.add("lesbo");
        FiveLetterWords.add("negro");
        FiveLetterWords.add("nigga");
        FiveLetterWords.add("penis");
        FiveLetterWords.add("prick");
        FiveLetterWords.add("pussy");
        FiveLetterWords.add("queer");
        FiveLetterWords.add("skeet");
        FiveLetterWords.add("twats");
        FiveLetterWords.add("whore");
        // ...
    }

    public static Set<String> SixLetterWords = new HashSet<String>();
    static {
        SixLetterWords.add("faggot");
        SixLetterWords.add("fucker");
        SixLetterWords.add("hardon");
        SixLetterWords.add("nigger");
        SixLetterWords.add("pecker");
        SixLetterWords.add("rimjob");
        SixLetterWords.add("shitty");
        SixLetterWords.add("vagina");
        // ...
    }
   
    
    public static Set<String> getBadWordSet(int len)
    {
        switch(len) {
        case 2:
            return TwoLetterWords;
        case 3:
            return ThreeLetterWords;
        case 4:
            return FourLetterWords;
        case 5:
            return FiveLetterWords;
        case 6:
            return SixLetterWords;
        default:
            return null;
        }
    }


}
