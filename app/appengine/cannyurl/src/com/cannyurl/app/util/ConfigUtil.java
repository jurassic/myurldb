package com.cannyurl.app.util;

import java.util.logging.Logger;

import com.cannyurl.af.config.Config;
import com.cannyurl.common.AppAuthMode;
import com.cannyurl.common.AppBrand;
import com.cannyurl.common.AppMode;
import com.cannyurl.common.DomainType;
import com.cannyurl.common.RedirectType;
import com.cannyurl.common.SassyTokenType;
import com.cannyurl.common.TokenGenerationMethod;
import com.cannyurl.common.TokenType;
import com.cannyurl.common.UserUrlUsernameType;
import com.myurldb.ws.permission.ShortLinkBasePermission;


// For fetching system default values.
// User-specific default values/settings should be read from somewhere else...
public class ConfigUtil
{
    private static final Logger log = Logger.getLogger(ConfigUtil.class.getName());
    private static Config sConfig = null;

    // temporary
    private static final int DEFAULT_BLOG_PAGE_SIZE = 3;
    private static final int DEFAULT_PAGER_PAGE_SIZE = 10;
    private static final String DEFAULT_PAGER_ORDERING_PRIMARY = "createdTime desc";
    private static final int DEFAULT_AUTOPLAY_INTERVAL = 30;   // in seconds

    // Config keys... 
    
    // Note: if devel==true, then always beta==true.
    private static final String KEY_DEVELFEATURE_ENABLED = "cannyurlapp.develfeature.enabled";
    private static final String KEY_BETAFEATURE_ENABLED = "cannyurlapp.betafeature.enabled";

    // Config keys... 
    private static final String KEY_APPLICATION_BRAND = "cannyurlapp.application.brand";
    // ...
    // TBD:
    // The variables, bookmark/keyword-enabled, etc., should really be more than boolean flags.
    // e.g.., "always create bookmark (regardless of bean value)", "create bookmark if bean value is true", and "never create bookmark", etc...
    private static final String KEY_APPLICATION_PRIMARY_APPMODE = "cannyurlapp.application.primary.appmode";
    private static final String KEY_APPMODE_URLSHORTENER_ENABLED = "cannyurlapp.appmode.urlshortener.enabled";
    private static final String KEY_APPMODE_BOOKMARKING_ENABLED = "cannyurlapp.appmode.bookmarking.enabled";
    private static final String KEY_APPMODE_NAVKEYWORD_ENABLED = "cannyurlapp.appmode.navkeyword.enabled";
    private static final String KEY_APPMODE_INVITEURL_ENABLED = "cannyurlapp.appmode.inviteurl.enabled";
    // ...
    // GAE openid vs. Twitter vs. etc....
    private static final String KEY_APPLICATION_AUTHMODE = "cannyurlapp.application.authmode";
    // Global flags..
    // If true, no page will require user logins.
    // The default value == false ??????
    private static final String KEY_APPLICATION_AUTH_DISABLED = "cannyurlapp.application.auth.disabled";
    // If true, login is not required for this app/brand (for pages which normally display different content based on use auth states), e.g., filter -> AuthStateFilter
    //          That is, these pages will display content which normally would have displayed for logged on users only.
    //          It has no impact for pages which DO require login. e.g., filter -> AuthLoginFilter or AuthRequiredFilter.
    // If KEY_APPLICATION_AUTH_DISABLED == true, this flag is ignored (since all pages will bypass auth).
    // TBD: We are doing this because we can use only one web.xml regardless of brands.
    //      We should really do this for each or a set of pages.
    //      But, that can be an overkill.
    //      We, for now, just use three sets of pages based on their auth filter use......
    private static final String KEY_APPLICATION_AUTH_OPTIONAL = "cannyurlapp.application.auth.optional";
    // TBD:  paypal, etc. ????
    private static final String KEY_APPLICATION_PAYMENT_SERVICE = "cannyurlapp.application.payment.service";
    // If true, payment is enabled. (Not just subscription, includes one time payment, in-app payment, etc.)
    private static final String KEY_APPLICATION_SUBSCRIPTION_ENABLED = "cannyurlapp.application.subscription.enabled";
    // ...

    // TBD:
    private static final String KEY_APPLICATION_CLIENTCODE_MINLENGTH = "cannyurlapp.application.clientcode.minlength";
    private static final String KEY_APPLICATION_CLIENTCODE_MAXLENGTH = "cannyurlapp.application.clientcode.maxlength";

    // TBD:
    private static final String KEY_APPLICATION_USERNAME_MINLENGTH = "cannyurlapp.application.username.minlength";
    private static final String KEY_APPLICATION_USERNAME_MAXLENGTH = "cannyurlapp.application.username.maxlength";
    private static final String KEY_APPLICATION_USERCODE_MINLENGTH = "cannyurlapp.application.usercode.minlength";
    private static final String KEY_APPLICATION_USERCODE_MAXLENGTH = "cannyurlapp.application.usercode.maxlength";

    // TBD:
    // Enable ingressDB / URL tally...
    private static final String KEY_APPLICATION_INGRESSDB_ENABLED = "cannyurlapp.application.ingressdb.enabled";
    private static final String KEY_APPLICATION_URLTALLY_ENABLED = "cannyurlapp.application.urltally.enabled";
    // ...

    
    // TBD:
    // Most of these are copied from BlogStoaApp...
    // Need to clean up....
    // ...
    private static final String KEY_PAGER_PAGESIZE = "cannyurlapp.pager.pagesize";    // Default page size
    private static final String KEY_PAGER_HEADLINES_PAGESIZE = "cannyurlapp.pager.headlines.pagesize"; 
    private static final String KEY_PAGER_SUMMARIES_PAGESIZE = "cannyurlapp.pager.summaries.pagesize"; 
    private static final String KEY_PAGER_SUMMARYLIST_PAGESIZE = "cannyurlapp.pager.summarylist.pagesize";
    // ...
    private static final String KEY_PAGER_LONGURLLIST_PAGESIZE = "cannyurlapp.pager.longurllist.pagesize";
    private static final String KEY_PAGER_SHORTURLLIST_PAGESIZE = "cannyurlapp.pager.shorturllist.pagesize";
    private static final String KEY_PAGER_SHORTURL_STREAM_PAGESIZE = "cannyurlapp.pager.shorturl.stream.pagesize";
    // ...
    private static final String KEY_PAGER_FEATUREDLIST_PAGESIZE = "cannyurlapp.pager.featuredlist.pagesize";
    private static final String KEY_PAGER_FEATUREDGRID_PAGESIZE = "cannyurlapp.pager.featuredgrid.pagesize";
    // ...
    private static final String KEY_PAGER_ORDERING_PRIMARY = "cannyurlapp.pager.ordering.primary";      // "field asc/desc"
    // private static final String KEY_PAGER_ORDERING_SECONDARY = "cannyurlapp.pager.ordering.secondary";  // "field asc/desc"
    private static final String KEY_PAGER_HEADLINES_ORDERING_PRIMARY = "cannyurlapp.pager.headlines.ordering.primary";
    private static final String KEY_PAGER_SUMMARIES_ORDERING_PRIMARY = "cannyurlapp.pager.summaries.ordering.primary";
    private static final String KEY_PAGER_SUMMARYLIST_ORDERING_PRIMARY = "cannyurlapp.pager.summarylist.ordering.primary";
    // ...
    private static final String KEY_PAGER_LONGURLLIST_ORDERING_PRIMARY = "cannyurlapp.pager.longurllist.ordering.primary";
    private static final String KEY_PAGER_SHORTURLLIST_ORDERING_PRIMARY = "cannyurlapp.pager.shorturllist.ordering.primary";
    private static final String KEY_PAGER_SHORTURL_STREAM_ORDERING_PRIMARY = "cannyurlapp.pager.shorturl.stream.ordering.primary";
    // ...
    private static final String KEY_PAGER_FEATUREDLIST_ORDERING_PRIMARY = "cannyurlapp.pager.featuredlist.ordering.primary";
    private static final String KEY_PAGER_FEATUREDGRID_ORDERING_PRIMARY = "cannyurlapp.pager.featuredgrid.ordering.primary";
    // ...
    
    // domain vs path...
    // ....
    
    
    // If this is set to true, it displays all /c /f etc. variations to the user in addition to the main short URL. 
    private static final String KEY_GENERAL_SHOW_URLVARIATIONS = "cannyurlapp.general.show.urlvariations";
    // If this is set to true, use sassytoken.
    private static final String KEY_GENERAL_USE_SASSYTOKEN = "cannyurlapp.general.use.sassytoken";
    // If this is set to true, use navkeyword.
    private static final String KEY_GENERAL_USE_NAVKEYWORD = "cannyurlapp.general.use.navkeyword";
    // If this is set to true, include a flash duration input box. 
    private static final String KEY_GENERAL_INCLUDE_FLASHDURATION = "cannyurlapp.general.include.flashduration";
    // If this is set to true, include a message dialog in the edit page (regardless of the redirect type). False does not mean the message box is not to be shown.
    private static final String KEY_GENERAL_INCLUDE_MESSAGEBOX = "cannyurlapp.general.include.messagebox";
    // If this is set to true, exclude a message dialog in the edit page (regardless of the redirect type or the include.messagebox flag). 
    private static final String KEY_GENERAL_EXCLUDE_MESSAGEBOX = "cannyurlapp.general.exclude.messagebox";
    // If this is set to true, always include a tweet button (regardless of the Twitter oauth setup). Does this make sense? If oauth is not set up, tweet button will not function properly.
    private static final String KEY_GENERAL_INCLUDE_TWEETBUTTON = "cannyurlapp.general.include.tweetbutton";
    // If this is set to true, exclude a tweet button (regardless of the Twitter oauth setup or the include.tweetbutton flag). 
    private static final String KEY_GENERAL_EXCLUDE_TWEETBUTTON = "cannyurlapp.general.exclude.tweetbutton";
    // TBD: Whether to include social sharing buttons, etc....
    // ....
    private static final String KEY_LOCATION_LOOKUP_ENABLED = "cannyurlapp.location.lookup.enabled";
    private static final String KEY_LOCATION_SERIVCE_BASEURI = "cannyurlapp.location.service.baseuri";
    private static final String KEY_LOCATION_GEOCELL_SCALE = "cannyurlapp.location.geocell.scale";
    // ...

    // If this is set to true, this overwrites KEY_DEFAULT_DOMAIN_TYPE setting.....
    private static final String KEY_USEDOMAIN_USERURL = "cannyurlapp.usedomain.userurl";
    private static final String KEY_USERURL_USERNAME_TYPE = "cannyurlapp.userurl.usernametype";
    private static final String KEY_USERURL_USERNAME_USEPREFIX = "cannyurlapp.userurl.username.useprefix";
    private static final String KEY_USERURL_USERNAME_PREFIXCHAR = "cannyurlapp.userurl.username.prefixchar";
    // ...
    private static final String KEY_ALGORITHM_TOKENGENERATION_METHOD = "cannyurlapp.algorithm.tokengeneration.method";
    private static final String KEY_ALGORITHM_TOKENGENERATION_UNIQUETOKEN = "cannyurlapp.algorithm.tokengeneration.uniquetoken";
    // ...
    private static final String KEY_DOMAIN_TYPE_OVERRIDE_ALLOWED = "cannyurlapp.domaintype.override.allowed";
    private static final String KEY_DOMAIN_TYPE_SUBDOMAIN_OR_PATH = "cannyurlapp.domaintype.subdomain.or.path";   // ????
    private static final String KEY_DEFAULT_DOMAIN_TYPE = "cannyurlapp.default.domaintype";
    private static final String KEY_DEFAULT_TOKEN_TYPE = "cannyurlapp.default.tokentype";
    private static final String KEY_DEFAULT_SASSYTOKEN_TYPE = "cannyurlapp.default.sassytokentype";
    private static final String KEY_DEFAULT_REDIRECT_TYPE = "cannyurlapp.default.redirecttype";
    private static final String KEY_DEFAULT_FLASH_DURATION = "cannyurlapp.default.flashduration";
    private static final String KEY_DEFAULT_MINIMUM_FLASH = "cannyurlapp.default.minimumflash";
    // ...
    // TBD: When tokenPrefix != "", set the length of the remaining part to length-1 (or, length-2)????
    private static final String KEY_SHORTURL_TINYTOKEN_LENGTH = "cannyurlapp.shorturl.tinytoken.length";
    private static final String KEY_SHORTURL_SHORTTOKEN_LENGTH = "cannyurlapp.shorturl.shorttoken.length";
    private static final String KEY_SHORTURL_MEDIUMTOKEN_LENGTH = "cannyurlapp.shorturl.mediumtoken.length";
    private static final String KEY_SHORTURL_LONGTOKEN_LENGTH = "cannyurlapp.shorturl.longtoken.length";
    private static final String KEY_SHORTURL_DIGITTOKEN_LENGTH = "cannyurlapp.shorturl.digittoken.length";
    private static final String KEY_SHORTURL_VOWELTOKEN_LENGTH = "cannyurlapp.shorturl.voweltoken.length";
    private static final String KEY_SHORTURL_ABCTOKEN_LENGTH = "cannyurlapp.shorturl.abctoken.length";
    private static final String KEY_SHORTURL_BINARYTOKEN_LENGTH = "cannyurlapp.shorturl.binarytoken.length";
    private static final String KEY_SHORTURL_CUSTOMTOKEN_MINLENGTH = "cannyurlapp.shorturl.customtoken.minlength";
    private static final String KEY_SHORTURL_CUSTOMTOKEN_MAXLENGTH = "cannyurlapp.shorturl.customtoken.maxlength";
    private static final String KEY_SEQUENTIAL_TOKENGENENRATION_MINLENGTH = "cannyurlapp.sequential.tokengeneration.minlength";
    // ...
    private static final String KEY_SASSYTOKEN_MIXEDCASE_LENGTH = "cannyurlapp.sassytoken.mixedcase.length";
    private static final String KEY_SASSYTOKEN_LOWERCAPS_LENGTH = "cannyurlapp.sassytoken.lowercaps.length";
    private static final String KEY_SASSYTOKEN_LOWERCASE_LENGTH = "cannyurlapp.sassytoken.lowercase.length";
    private static final String KEY_SASSYTOKEN_LONGTOKEN_LENGTH = "cannyurlapp.sassytoken.longtoken.length";
    // ...
    private static final String KEY_SHORTURL_EXPIRATION_MAXAGE = "cannyurlapp.shorturl.expiration.maxage";
    // ...
    private static final String KEY_TOKENFORMAT_ALLOWSPACE = "cannyurlapp.tokenformat.allowspace";
    private static final String KEY_TOKENFORMAT_ALLOWSLASH = "cannyurlapp.tokenformat.allowslash";
    // ...
    // ????
    private static final String KEY_NAVKEYWORD_MINLENGTH = "cannyurlapp.navkeyword.minlength";
    private static final String KEY_NAVKEYWORD_MAXLENGTH = "cannyurlapp.navkeyword.maxlength";
    // ...
    private static final String KEY_URLVERIFY_FOLLOW_VERIFYPAGE = "cannyurlapp.urlverify.follow.verifypage";
    // ...
    // etc...
    
    // TBD:
    // keywordCaseSensitive?
    // speedDialCaseSensitive?
    // allowMultiCharSpeedDial?
    // ....


    // TBD: Resource permissions
    private static final String KEY_PERMISSION_REQUIRED_SHORTLINK_CREATE = "cannyurlapp.permission.required.shortlink.create";
    // ....

    // TBD: UI customization. (but, not feature enable/disable...)
    private static final String KEY_UI_WEBAPP_FRONTPAGE = "cannyurlapp.ui.webapp.frontpage";
    private static final String KEY_UI_WEBAPP_USERSETTING_ENABLED = "cannyurlapp.ui.webapp.usersetting.enabled";
    private static final String KEY_UI_WEBAPP_CLIENTSETTING_ENABLED = "cannyurlapp.ui.webapp.clientsetting.enabled";
    private static final String KEY_UI_SHORTLINKEDIT_TOKENTYPE_SELECTION_DISPLAYED = "cannyurlapp.ui.shortlinkedit.tokentype.selection.displayed";
    private static final String KEY_UI_SHORTLINKEDIT_DOMAIN_PREFILLED = "cannyurlapp.ui.shortlinkedit.domain.prefilled";
    private static final String KEY_UI_SHORTLINKEDIT_BOOKMARKING_INCLUDED = "cannyurlapp.ui.shortlinkedit.bookmarking.included";
    private static final String KEY_UI_SHORTLINKEDIT_NAVKEYWORD_ENABLED = "cannyurlapp.ui.shortlinkedit.navkeyword.enabled";
    // etc...
    // ...


    // For Twitter Card support...
    private static final String KEY_TWITTERCARD_DEFAULT_SITE = "cannyurlapp.twittercard.default.site";
    private static final String KEY_TWITTERCARD_DEFAULT_SITEID = "cannyurlapp.twittercard.default.siteid";
    private static final String KEY_TWITTERCARD_DEFAULT_CREATOR = "cannyurlapp.twittercard.default.creator";
    private static final String KEY_TWITTERCARD_DEFAULT_CREATORID = "cannyurlapp.twittercard.default.creatorid";
    // ...

    
    
    // "Cache" values.  (TBD: Convert types to enums????)
    private static Boolean sDevelFeatureEnabled = null;
    private static Boolean sBetaFeatureEnabled = null;

    private static String sApplicationBrand = null;
    // ...
    private static String sApplicationPrimaryAppMode = null;
    private static Boolean sAppModeUrlShortenerEnabled = null;
    private static Boolean sAppModeBookmarkingEnabled = null;
    private static Boolean sAppModeNavKeywordEnabled = null;
    private static Boolean sAppModeInviteUrlEnabled = null;

    private static String sApplicationAuthMode = null;
    private static Boolean sApplicationAuthDisabled = null;
    private static Boolean sApplicationAuthOptional = null;
    private static String sApplicationPaymentService = null;
    private static Boolean sApplicationSubscriptionEnabled = null;

    // ...
    private static int sDefaultClientCodeMinLength = -1;  // Should be > 0. 
    private static int sDefaultClientCodeMaxLength = -1;  // Should be > 0. 
    // ...
    private static int sDefaultUsernameMinLength = -1;  // Should be > 0. 
    private static int sDefaultUsernameMaxLength = -1;  // Should be > 0. 
    private static int sDefaultUsercodeMinLength = -1;  // Should be > 0. 
    private static int sDefaultUsercodeMaxLength = -1;  // Should be > 0. 

    private static Boolean sApplicationIngressDBEnabled = null;
    private static Boolean sApplicationUrlTallyEnabled = null;

    private static Integer sPagerPageSize = null;
    private static Integer sPagerHeadlinesPageSize = null;
    private static Integer sPagerSummariesPageSize = null;
    private static Integer sPagerSummaryListPageSize = null;
    private static Integer sPagerLongUrlListPageSize = null;
    private static Integer sPagerShortUrlListPageSize = null;
    private static Integer sPagerShortUrlStreamPageSize = null;
    private static Integer sPagerFeaturedListPageSize = null;
    private static Integer sPagerFeaturedGridPageSize = null;

    private static String sPagerOrderingPrimary = null;
    private static String sPagerHeadlinesOrderingPrimary = null;
    private static String sPagerSummariesOrderingPrimary = null;
    private static String sPagerSummaryListOrderingPrimary = null;
    private static String sPagerLongUrlListOrderingPrimary = null;
    private static String sPagerShortUrlListOrderingPrimary = null;
    private static String sPagerShortUrlStreamOrderingPrimary = null;
    private static String sPagerFeaturedListOrderingPrimary = null;
    private static String sPagerFeaturedGridOrderingPrimary = null;
    // ...

    private static Boolean sGeneralShowUrlVariations = null;
    private static Boolean sGeneralUseSassyToken = null;
    private static Boolean sGeneralUseNavKeyword = null;
    private static Boolean sGeneralIncludeFlashDuration = null;
    private static Boolean sGeneralIncludeMessageBox = null;
    private static Boolean sGeneralExcludeMessageBox = null;
    private static Boolean sGeneralIncludeTweetButton = null;
    private static Boolean sGeneralExcludeTweetButton = null;
    // ...

    private static Boolean sLocationLookupEnabled = null;
    private static String sLocationServiceBaseUri = null;
    private static Integer sLocationGeoCellScale = null;      // >= 1 ~ 1000000
    // ...

    private static Boolean sUseDomainUserURL = null;
    private static String sUserURLUsernameType = null;
    private static Boolean sUserURLUsernameUsePrefix = null;
    private static String sUserURLUsernamePrefixChar = null;
    // ...
    private static String sTokenGenerationMethod = null;
    private static Boolean sTokenGenerationUniqueToken = null;
    // ...
    private static Boolean sDomainTypeOverrideAllowed = null;
    private static String sDomainTypeSubdomainOrPath = null;
    private static String sDefaultDomainType = null;
    private static String sDefaultTokenType = null;
    private static String sDefaultSassyTokenType = null;
    private static String sDefaultRedirectType = null;
    private static long sDefaultFlashDuration = -1L;  // Should be > 0L.  milliseconds...
    private static long sDefaultMinimumFlash = -1L;   // Should be > 0L.  milliseconds...
    // ...
    private static int sDefaultTinyTokenLength = -1;  // Should be > 0. 
    private static int sDefaultShortTokenLength = -1;  // Should be > 0. 
    private static int sDefaultMediumTokenLength = -1;  // Should be > 0. 
    private static int sDefaultLongTokenLength = -1;  // Should be > 0. 
    private static int sDefaultDigitTokenLength = -1;  // Should be > 0. 
    private static int sDefaultVowelTokenLength = -1;  // Should be > 0. 
    private static int sDefaultAbcTokenLength = -1;  // Should be > 0. 
    private static int sDefaultBinaryTokenLength = -1;  // Should be > 0. 
    private static int sDefaultCustomTokenMinLength = -1;  // Should be > 0. 
    private static int sDefaultCustomTokenMaxLength = -1;  // Should be > 0. 
    private static int sSequentialTokenGenerationMinLength = -1;  // Should be > 0. 
    // ...
    private static int sDefaultSassyTokenMixedcaseLength = -1;  // Should be > 0. 
    private static int sDefaultSassyTokenLowercapsLength = -1;  // Should be > 0. 
    private static int sDefaultSassyTokenLowercaseLength = -1;  // Should be > 0. 
    private static int sDefaultSassyTokenLongtokenLength = -1;  // Should be > 0. 
    // ...
    private static int sDefaultShortUrlMaxAge = -1;  // Should be >= 0.  seconds...
    //
    private static Boolean sTokenFormatAllowSpace = null;
    private static Boolean sTokenFormatAllowSlash = null;
    // ...
    private static int sDefaultKeywordMinLength = -1;  // Should be > 0. 
    private static int sDefaultKeywordMaxLength = -1;  // Should be > 0. 
    // ...
    // This applies to ..???
    private static Boolean sUrlVerifyFollowVerifyPage = null;
    // ...
    
    // For resource permissions
    private static Boolean sPermissionRequiredForShortLinkCreate = null;
    // ...

    // UI related...
    private static String sUiWebAppFrontPage = null;    // Home page URL
    private static Boolean sUiWebAppUserSettingEnabled = null;
    private static Boolean sUiWebAppClientSettingEnabled = null;
    private static Boolean sUiShortLinkEditTokenTypeSelectionDisplayed = null;
    private static Boolean sUiShortLinkEditDomainPrefilled = null;
    private static Boolean sUiShortLinkEditBookmarkingIncluded = null;
    private static Boolean sUiShortLinkEditNavKeywordEnabled = null;
    // ...
    
    private static String sTwitterCardDefaultSite = null;
    private static String sTwitterCardDefaultSiteId = null;
    private static String sTwitterCardDefaultCreator = null;
    private static String sTwitterCardDefaultCreatorId = null;
    // ...


    // Static methods only.
    private ConfigUtil() {}

    
    public static int getDefaultBlogPageSize()
    {
        return DEFAULT_BLOG_PAGE_SIZE;
    }
    public static int getDefaultPagerPageSize()
    {
        return DEFAULT_PAGER_PAGE_SIZE;
    }
    public static String getDefaultPagerOrderingPrimary()
    {
        return DEFAULT_PAGER_ORDERING_PRIMARY;
    }

    
    public static Boolean isDevelFeatureEnabled()
    {
        if(sDevelFeatureEnabled == null) {
            sDevelFeatureEnabled = Config.getInstance().getBoolean(KEY_DEVELFEATURE_ENABLED, false);
        }
        return sDevelFeatureEnabled;
    }
    public static Boolean isBetaFeatureEnabled()
    {
        if(sBetaFeatureEnabled == null) {
            // Do this in a higher level in the stack (e.g, DebugUtil)...
            //if(isDevelFeatureEnabled()) {
            //    sBetaFeatureEnabled = true;
            //} else {
               // sBetaFeatureEnabled = Config.getInstance().getBoolean(KEY_BETAFEATURE_ENABLED, false);
               sBetaFeatureEnabled = Config.getInstance().getBoolean(KEY_BETAFEATURE_ENABLED, isDevelFeatureEnabled());  // ???
            //}
        }
        return sBetaFeatureEnabled;
    }


    // App "brand" 
    public static String getApplicationBrand()
    {
        if(sApplicationBrand == null) {
            sApplicationBrand = Config.getInstance().getString(KEY_APPLICATION_BRAND, AppBrand.getDefaultBrand());
        }
        return sApplicationBrand;
    }


    public static String getApplicationPrimaryAppMode()
    {
        if(sApplicationPrimaryAppMode == null) {
            sApplicationPrimaryAppMode = Config.getInstance().getString(KEY_APPLICATION_PRIMARY_APPMODE, AppMode.getDefaultMode());
        }
        return sApplicationPrimaryAppMode;
    }

    public static boolean isAppModeUrlShortenerEnabled()
    {
        if(sAppModeUrlShortenerEnabled == null) {
            sAppModeUrlShortenerEnabled = Config.getInstance().getBoolean(KEY_APPMODE_URLSHORTENER_ENABLED, false);
            if(sAppModeUrlShortenerEnabled == null) {   // ????
                sAppModeUrlShortenerEnabled = false;
            }
        }
        return sAppModeUrlShortenerEnabled;
    }

    public static boolean isAppModeBookmarkingEnabled()
    {
        if(sAppModeBookmarkingEnabled == null) {
            sAppModeBookmarkingEnabled = Config.getInstance().getBoolean(KEY_APPMODE_BOOKMARKING_ENABLED, false);
            if(sAppModeBookmarkingEnabled == null) {   // ????
                sAppModeBookmarkingEnabled = false;
            }
        }
        return sAppModeBookmarkingEnabled;
    }

    public static boolean isAppModeNavKeywordEnabled()
    {
        if(sAppModeNavKeywordEnabled == null) {
            sAppModeNavKeywordEnabled = Config.getInstance().getBoolean(KEY_APPMODE_NAVKEYWORD_ENABLED, false);
            if(sAppModeNavKeywordEnabled == null) {   // ????
                sAppModeNavKeywordEnabled = false;
            }
        }
        return sAppModeNavKeywordEnabled;
    }

    public static boolean isAppModeInviteUrlEnabled()
    {
        if(sAppModeInviteUrlEnabled == null) {
            sAppModeInviteUrlEnabled = Config.getInstance().getBoolean(KEY_APPMODE_INVITEURL_ENABLED, false);
            if(sAppModeInviteUrlEnabled == null) {   // ????
                sAppModeInviteUrlEnabled = false;
            }
        }
        return sAppModeInviteUrlEnabled;
    }


    // System authmode: openid, twitter, etc...
    public static String getApplicationAuthMode()
    {
        if(sApplicationAuthMode == null) {
            sApplicationAuthMode = Config.getInstance().getString(KEY_APPLICATION_AUTHMODE, AppAuthMode.getDefaultValue());   // ????
        }
        return sApplicationAuthMode;
    }

    public static boolean isApplicationAuthDisabled()
    {
        if(sApplicationAuthDisabled == null) {
            sApplicationAuthDisabled = Config.getInstance().getBoolean(KEY_APPLICATION_AUTH_DISABLED, false);   // System auth is enabled by default...
            if(sApplicationAuthDisabled == null) {   // ????
                sApplicationAuthDisabled = false;
            }
        }
        return sApplicationAuthDisabled;
    }

    public static boolean isApplicationAuthOptional()
    {
        if(sApplicationAuthOptional == null) {
            sApplicationAuthOptional = Config.getInstance().getBoolean(KEY_APPLICATION_AUTH_OPTIONAL, false);
            if(sApplicationAuthOptional == null) {   // ????
                sApplicationAuthOptional = false;
            }
        }
        return sApplicationAuthOptional;
    }

    public static String getApplicationPaymentService()
    {
        if(sApplicationPaymentService == null) {
            sApplicationPaymentService = Config.getInstance().getString(KEY_APPLICATION_PAYMENT_SERVICE, "");   // ???
        }
        return sApplicationPaymentService;
    }

    public static boolean isApplicationSubscriptionEnabled()
    {
        if(sApplicationSubscriptionEnabled == null) {
            sApplicationSubscriptionEnabled = Config.getInstance().getBoolean(KEY_APPLICATION_SUBSCRIPTION_ENABLED, false);   // ????
            if(sApplicationSubscriptionEnabled == null) {   // ????
                sApplicationSubscriptionEnabled = false;
            }
        }
        return sApplicationSubscriptionEnabled;
    }
    
    

    // Inclusive
    public static int getClientCodeMinLength()
    {
        if(sDefaultClientCodeMinLength < 0) {
            sDefaultClientCodeMinLength = Config.getInstance().getInteger(KEY_APPLICATION_CLIENTCODE_MINLENGTH, 2);    // ???
        }
        return sDefaultClientCodeMinLength;
    }
    
    // Inclusive
    public static int getClientCodeMaxLength()
    {
        if(sDefaultClientCodeMaxLength < 0) {
            sDefaultClientCodeMaxLength = Config.getInstance().getInteger(KEY_APPLICATION_CLIENTCODE_MAXLENGTH, 8);   // ???
        }
        return sDefaultClientCodeMaxLength;
    }

    
    // Inclusive
    public static int getUsernameMinLength()
    {
        if(sDefaultUsernameMinLength < 0) {
            sDefaultUsernameMinLength = Config.getInstance().getInteger(KEY_APPLICATION_USERNAME_MINLENGTH, 5);    // ???
        }
        return sDefaultUsernameMinLength;
    }
    
    // Inclusive
    public static int getUsernameMaxLength()
    {
        if(sDefaultUsernameMaxLength < 0) {
            sDefaultUsernameMaxLength = Config.getInstance().getInteger(KEY_APPLICATION_USERNAME_MAXLENGTH, 15);   // ???
        }
        return sDefaultUsernameMaxLength;
    }

    // Inclusive
    public static int getUsercodeMinLength()
    {
        if(sDefaultUsercodeMinLength < 0) {
            sDefaultUsercodeMinLength = Config.getInstance().getInteger(KEY_APPLICATION_USERCODE_MINLENGTH, 3);    // ???
        }
        return sDefaultUsercodeMinLength;
    }
    
    // Inclusive
    public static int getUsercodeMaxLength()
    {
        if(sDefaultUsercodeMaxLength < 0) {
            sDefaultUsercodeMaxLength = Config.getInstance().getInteger(KEY_APPLICATION_USERCODE_MAXLENGTH, 10);   // ???
        }
        return sDefaultUsercodeMaxLength;
    }

    
    // TBD:
    // IngressDB/UrlTally flags are really intertwined
    // although they are processed in two different servers (three if including cannyurl server)...

    // If set to false,
    // No access records will be created, and hence no stats will be generated.
    // That means URL tally will not have any data to process....
    public static boolean isApplicationIngressDBEnabled()
    {
        if(sApplicationIngressDBEnabled == null) {
            sApplicationIngressDBEnabled = Config.getInstance().getBoolean(KEY_APPLICATION_INGRESSDB_ENABLED, false);   // ????
            if(sApplicationIngressDBEnabled == null) {   // ????
                sApplicationIngressDBEnabled = false;
            }
        }
        return sApplicationIngressDBEnabled;
    }

    // Note that since the "url tally" processing is done in a different server,
    // this flag has no real relevance.
    // We can use this flag to show/hide "info" page, perhaps ????
    public static boolean isApplicationUrlTallyEnabled()
    {
        if(sApplicationUrlTallyEnabled == null) {
            sApplicationUrlTallyEnabled = Config.getInstance().getBoolean(KEY_APPLICATION_URLTALLY_ENABLED, false);   // ????
            if(sApplicationUrlTallyEnabled == null) {   // ????
                sApplicationUrlTallyEnabled = false;
            }
        }
        return sApplicationUrlTallyEnabled;
    }


    // Default List page size.
    public static Integer getPagerPageSize()
    {
        if(sPagerPageSize == null) {
            sPagerPageSize = Config.getInstance().getInteger(KEY_PAGER_PAGESIZE, getDefaultPagerPageSize());
        }
        return sPagerPageSize;
    }

    // "Headlines" list page size.
    public static Integer getPagerHeadlinesPageSize()
    {
        if(sPagerHeadlinesPageSize == null) {
            sPagerHeadlinesPageSize = Config.getInstance().getInteger(KEY_PAGER_HEADLINES_PAGESIZE, getDefaultPagerPageSize());
        }
        return sPagerHeadlinesPageSize;
    }

    // "Summaries" list page size.
    public static Integer getPagerSummariesPageSize()
    {
        if(sPagerSummariesPageSize == null) {
            sPagerSummariesPageSize = Config.getInstance().getInteger(KEY_PAGER_SUMMARIES_PAGESIZE, getDefaultPagerPageSize());
        }
        return sPagerSummariesPageSize;
    }

    // "SummaryList" list page size.
    public static Integer getPagerSummaryListPageSize()
    {
        if(sPagerSummaryListPageSize == null) {
            sPagerSummaryListPageSize = Config.getInstance().getInteger(KEY_PAGER_SUMMARYLIST_PAGESIZE, getDefaultPagerPageSize());
        }
        return sPagerSummaryListPageSize;
    }

    public static Integer getPagerLongUrlListPageSize()
    {
        if(sPagerLongUrlListPageSize == null) {
            sPagerLongUrlListPageSize = Config.getInstance().getInteger(KEY_PAGER_LONGURLLIST_PAGESIZE, getDefaultPagerPageSize());
        }
        return sPagerLongUrlListPageSize;
    }

    public static Integer getPagerShortUrlListPageSize()
    {
        if(sPagerShortUrlListPageSize == null) {
            sPagerShortUrlListPageSize = Config.getInstance().getInteger(KEY_PAGER_SHORTURLLIST_PAGESIZE, getDefaultPagerPageSize());
        }
        return sPagerShortUrlListPageSize;
    }
    
    public static Integer getPagerShortUrlStreamPageSize()
    {
        if(sPagerShortUrlStreamPageSize == null) {
            sPagerShortUrlStreamPageSize = Config.getInstance().getInteger(KEY_PAGER_SHORTURL_STREAM_PAGESIZE, getDefaultPagerPageSize());
        }
        return sPagerShortUrlStreamPageSize;
    }

    public static Integer getPagerFeaturedListPageSize()
    {
        if(sPagerFeaturedListPageSize == null) {
            sPagerFeaturedListPageSize = Config.getInstance().getInteger(KEY_PAGER_FEATUREDLIST_PAGESIZE, getDefaultPagerPageSize());
        }
        return sPagerFeaturedListPageSize;
    }

    public static Integer getPagerFeaturedGridPageSize()
    {
        if(sPagerFeaturedGridPageSize == null) {
            sPagerFeaturedGridPageSize = Config.getInstance().getInteger(KEY_PAGER_FEATUREDGRID_PAGESIZE, getDefaultPagerPageSize());
        }
        return sPagerFeaturedGridPageSize;
    }

    
    public static String getPagerOrderingPrimary()
    {
        if(sPagerOrderingPrimary == null) {
            sPagerOrderingPrimary = Config.getInstance().getString(KEY_PAGER_ORDERING_PRIMARY, DEFAULT_PAGER_ORDERING_PRIMARY);
        }
        return sPagerOrderingPrimary;
    }

    public static String getPagerHeadlinesOrderingPrimary()
    {
        if(sPagerHeadlinesOrderingPrimary == null) {
            sPagerHeadlinesOrderingPrimary = Config.getInstance().getString(KEY_PAGER_HEADLINES_ORDERING_PRIMARY, DEFAULT_PAGER_ORDERING_PRIMARY);
        }
        return sPagerHeadlinesOrderingPrimary;
    }

    public static String getPagerSummariesOrderingPrimary()
    {
        if(sPagerSummariesOrderingPrimary == null) {
            sPagerSummariesOrderingPrimary = Config.getInstance().getString(KEY_PAGER_SUMMARIES_ORDERING_PRIMARY, DEFAULT_PAGER_ORDERING_PRIMARY);
        }
        return sPagerSummariesOrderingPrimary;
    }

    public static String getPagerSummaryListOrderingPrimary()
    {
        if(sPagerSummaryListOrderingPrimary == null) {
            sPagerSummaryListOrderingPrimary = Config.getInstance().getString(KEY_PAGER_SUMMARYLIST_ORDERING_PRIMARY, DEFAULT_PAGER_ORDERING_PRIMARY);
        }
        return sPagerSummaryListOrderingPrimary;
    }    

    public static String getPagerLongUrlListOrderingPrimary()
    {
        if(sPagerLongUrlListOrderingPrimary == null) {
            sPagerLongUrlListOrderingPrimary = Config.getInstance().getString(KEY_PAGER_LONGURLLIST_ORDERING_PRIMARY, DEFAULT_PAGER_ORDERING_PRIMARY);
        }
        return sPagerLongUrlListOrderingPrimary;
    }    

    public static String getPagerShortUrlListOrderingPrimary()
    {
        if(sPagerShortUrlListOrderingPrimary == null) {
            sPagerShortUrlListOrderingPrimary = Config.getInstance().getString(KEY_PAGER_SHORTURLLIST_ORDERING_PRIMARY, DEFAULT_PAGER_ORDERING_PRIMARY);
        }
        return sPagerShortUrlListOrderingPrimary;
    }    

    public static String getPagerShortUrlStreamOrderingPrimary()
    {
        if(sPagerShortUrlStreamOrderingPrimary == null) {
            sPagerShortUrlStreamOrderingPrimary = Config.getInstance().getString(KEY_PAGER_SHORTURL_STREAM_ORDERING_PRIMARY, DEFAULT_PAGER_ORDERING_PRIMARY);
        }
        return sPagerShortUrlStreamOrderingPrimary;
    }    

    public static String getPagerFeaturedListOrderingPrimary()
    {
        if(sPagerFeaturedListOrderingPrimary == null) {
            sPagerFeaturedListOrderingPrimary = Config.getInstance().getString(KEY_PAGER_FEATUREDLIST_ORDERING_PRIMARY, DEFAULT_PAGER_ORDERING_PRIMARY);
        }
        return sPagerFeaturedListOrderingPrimary;
    }    

    public static String getPagerFeaturedGridOrderingPrimary()
    {
        if(sPagerFeaturedGridOrderingPrimary == null) {
            sPagerFeaturedGridOrderingPrimary = Config.getInstance().getString(KEY_PAGER_FEATUREDGRID_ORDERING_PRIMARY, DEFAULT_PAGER_ORDERING_PRIMARY);
        }
        return sPagerFeaturedGridOrderingPrimary;
    }

    
    // General Settings
    ///////////////////////////////////////////////////////////////

    public static Boolean isGeneralShowUrlVariations()
    {
        if(sGeneralShowUrlVariations == null) {
            sGeneralShowUrlVariations = Config.getInstance().getBoolean(KEY_GENERAL_SHOW_URLVARIATIONS, false);   // False by default.
        }
        return sGeneralShowUrlVariations;
    }
    
    public static Boolean isGeneralUseSassyToken()
    {
        if(sGeneralUseSassyToken == null) {
            sGeneralUseSassyToken = Config.getInstance().getBoolean(KEY_GENERAL_USE_SASSYTOKEN, false);   // False by default.
        }
        return sGeneralUseSassyToken;
    }
    
    public static Boolean isGeneralUseNavKeyword()
    {
        if(sGeneralUseNavKeyword == null) {
            sGeneralUseNavKeyword = Config.getInstance().getBoolean(KEY_GENERAL_USE_NAVKEYWORD, false);   // False by default.
        }
        return sGeneralUseNavKeyword;
    }
    
    public static Boolean isGeneralIncludeFlashDuration()
    {
        if(sGeneralIncludeFlashDuration == null) {
            sGeneralIncludeFlashDuration = Config.getInstance().getBoolean(KEY_GENERAL_INCLUDE_FLASHDURATION, false);   // False by default.
        }
        return sGeneralIncludeFlashDuration;
    }

    public static Boolean isGeneralIncludeMessageBox()
    {
        if(sGeneralIncludeMessageBox == null) {
            sGeneralIncludeMessageBox = Config.getInstance().getBoolean(KEY_GENERAL_INCLUDE_MESSAGEBOX, false);   // False by default.
        }
        return sGeneralIncludeMessageBox;
    }

    public static Boolean isGeneralExcludeMessageBox()
    {
        if(sGeneralExcludeMessageBox == null) {
            sGeneralExcludeMessageBox = Config.getInstance().getBoolean(KEY_GENERAL_EXCLUDE_MESSAGEBOX, false);   // False by default.
        }
        return sGeneralExcludeMessageBox;
    }

    public static Boolean isGeneralIncludeTweetButton()
    {
        if(sGeneralIncludeTweetButton == null) {
            sGeneralIncludeTweetButton = Config.getInstance().getBoolean(KEY_GENERAL_INCLUDE_TWEETBUTTON, false);   // False by default.
        }
        return sGeneralIncludeTweetButton;
    }

    public static Boolean isGeneralExcludeTweetButton()
    {
        if(sGeneralExcludeTweetButton == null) {
            sGeneralExcludeTweetButton = Config.getInstance().getBoolean(KEY_GENERAL_EXCLUDE_TWEETBUTTON, false);   // False by default.
        }
        return sGeneralExcludeTweetButton;
    }

    
    public static Boolean isLocationLookupEnabled()
    {
        if(sLocationLookupEnabled == null) {
            sLocationLookupEnabled = Config.getInstance().getBoolean(KEY_LOCATION_LOOKUP_ENABLED, false);
        }
        return sLocationLookupEnabled;
    }
    public static String getLocationServiceBaseUri()
    {
        if(sLocationServiceBaseUri == null) {
            sLocationServiceBaseUri = Config.getInstance().getString(KEY_LOCATION_SERIVCE_BASEURI, "");   // ???
        }
        return sLocationServiceBaseUri;
    }
    public static Integer getLocationGeoCellScale()
    {
        if(sLocationGeoCellScale == null) {
            sLocationGeoCellScale = Config.getInstance().getInteger(KEY_LOCATION_GEOCELL_SCALE, 1);   // ???  1 == no zoom.
        }
        return sLocationGeoCellScale;
    }



    // If the usedomain.userurl config var is set to true, then domain is always in the form http://a.com/xyz/.
    //      and, other forms such as http://a.com/~xyz/ or http://a.com/@xyz/ are not allowed.
    // TBD:   Use getUserUrlUsernamePrefix() config ???
    // ???
    public static Boolean isUseDomainUserURL()
    {
        if(sUseDomainUserURL == null) {
            sUseDomainUserURL = Config.getInstance().getBoolean(KEY_USEDOMAIN_USERURL, false);   // False by default.
        }
        return sUseDomainUserURL;
    }

    /////////////////////////////////////////////////////////////////
    // These three methods are currently not being used... (BELOW)

    /* public */ static String getUserUrlUsernameType()
    {
        if(sUserURLUsernameType == null) {
            sUserURLUsernameType = Config.getInstance().getString(KEY_USERURL_USERNAME_TYPE, UserUrlUsernameType.getDefaultType());
        }
        return sUserURLUsernameType;
    }

    // If true, "prefix char" will be added before the username in the short url.
    /* public */ static Boolean isUserUrlUsernameUsePrefix()
    {
        if(sUserURLUsernameUsePrefix == null) {
            sUserURLUsernameUsePrefix = Config.getInstance().getBoolean(KEY_USERURL_USERNAME_USEPREFIX, false);
        }
        return sUserURLUsernameUsePrefix;
    }

    // Only relevant if getUserUrlUsernameUsePrefix() == true.
    // "" means use the "default" value?
    // Default value can be a single value or a different value for each username type....
    // Note: Despite the method name, the prefix can be a multi-char string, in theory.
    //      (In the current implementation, we only support '~' or '@')
    /* public */ static String getUserUrlUsernamePrefix()
    {
        if(sUserURLUsernamePrefixChar == null) {
            sUserURLUsernamePrefixChar = Config.getInstance().getString(KEY_USERURL_USERNAME_PREFIXCHAR, "");
        }
        return sUserURLUsernamePrefixChar;
    }

    /////////////////////////////////////////////////////////////////
    // These three methods are currently not being used... (ABOVE)

 

    public static String getTokenGenerationMethod()
    {
        if(sTokenGenerationMethod == null) {
            sTokenGenerationMethod = Config.getInstance().getString(KEY_ALGORITHM_TOKENGENERATION_METHOD, TokenGenerationMethod.getDefaultMethod());
        }
        return sTokenGenerationMethod;
    }

    // If true, check existing tokens to make sure a new token is used (for a given domain).
    // (Note that, in the way we implement it, we cannot guarantee uniqueness.
    //  In some cases, we will throw an execption rather than keep trying to fina a novel token...)
    public static Boolean isTokenGenerationUniqueToken()
    {
        if(sTokenGenerationUniqueToken == null) {
            sTokenGenerationUniqueToken = Config.getInstance().getBoolean(KEY_ALGORITHM_TOKENGENERATION_UNIQUETOKEN, false);   // False by default.
        }
        if(sTokenGenerationUniqueToken == null) {   // Can this happen???
            sTokenGenerationUniqueToken = false;
        }
        return sTokenGenerationUniqueToken;
    }


    // If false, the input, ShortLink.domainType, is ignored, and always the system default value is used.
    // If true, the client input domainType, if provided, takes precedence over the system default value.
    // Note that "custom" domain type is always allowed regardless of this flag...   ???????
    public static Boolean isDomainTypeOverrideAllowed()
    {
        if(sDomainTypeOverrideAllowed == null) {
            sDomainTypeOverrideAllowed = Config.getInstance().getBoolean(KEY_DOMAIN_TYPE_OVERRIDE_ALLOWED, false);   // False by default.
        }
        if(sDomainTypeOverrideAllowed == null) {   // Can this happen???
            sDomainTypeOverrideAllowed = false;
        }
        return sDomainTypeOverrideAllowed;
    }

    // 3/13/2013
    // This is only applicable to certain domainTypes, such as username, usercode, twitter, etc...
    // Instead of using a single var, domainType, we now use this "flag" to indicate 
    // whether to include username, etc., in subdomain part or in a path.
    // Note that this is not a boolean, for future extension.
    // Currently allowed values: subdomain, path.
    public static String getDomainTypeSubdomainOrPath()
    {
        if(sDomainTypeSubdomainOrPath == null) {
            sDomainTypeSubdomainOrPath = Config.getInstance().getString(KEY_DOMAIN_TYPE_SUBDOMAIN_OR_PATH, "path");    // ????
        }
        return sDomainTypeSubdomainOrPath;
    }

    public static String getSystemDefaultDomainType()
    {
        if(sDefaultDomainType == null) {
            sDefaultDomainType = Config.getInstance().getString(KEY_DEFAULT_DOMAIN_TYPE, DomainType.getDefaultType());
        }
        return sDefaultDomainType;
    }

    public static String getSystemDefaultTokenType()
    {
        if(sDefaultTokenType == null) {
            sDefaultTokenType = Config.getInstance().getString(KEY_DEFAULT_TOKEN_TYPE, TokenType.getDefaultType());
        }
        return sDefaultTokenType;
    }

    public static String getSystemDefaultSassyTokenType()
    {
        if(sDefaultSassyTokenType == null) {
            sDefaultSassyTokenType = Config.getInstance().getString(KEY_DEFAULT_SASSYTOKEN_TYPE, SassyTokenType.getDefaultType());
        }
        return sDefaultSassyTokenType;
    }

    public static String getSystemDefaultRedirectType()
    {
        if(sDefaultRedirectType == null) {
            sDefaultRedirectType = Config.getInstance().getString(KEY_DEFAULT_REDIRECT_TYPE, RedirectType.getDefaultType());
        }
        return sDefaultRedirectType;
    }

    public static long getSystemDefaultFlashDuration()
    {
        if(sDefaultFlashDuration < 0L) {
            sDefaultFlashDuration = Config.getInstance().getLong(KEY_DEFAULT_FLASH_DURATION, 10000L);   // ???
        }
        return sDefaultFlashDuration;
    }

    public static long getSystemDefaultMinimumFlash()
    {
        if(sDefaultMinimumFlash < 0L) {
            sDefaultMinimumFlash = Config.getInstance().getLong(KEY_DEFAULT_MINIMUM_FLASH, 5000L);      // ???
        }
        return sDefaultMinimumFlash;
    }
    
    
    public static int getTinyTokenLength()
    {
        if(sDefaultTinyTokenLength < 0L) {
            sDefaultTinyTokenLength = Config.getInstance().getInteger(KEY_SHORTURL_TINYTOKEN_LENGTH, TokenUtil.DEFAULT_TINYTOKEN_LENGTH);
        }
        return sDefaultTinyTokenLength;
    }
    
    public static int getShortTokenLength()
    {
        if(sDefaultShortTokenLength < 0L) {
            sDefaultShortTokenLength = Config.getInstance().getInteger(KEY_SHORTURL_SHORTTOKEN_LENGTH, TokenUtil.DEFAULT_SHORTTOKEN_LENGTH);
        }
        return sDefaultShortTokenLength;
    }

    public static int getMediumTokenLength()
    {
        if(sDefaultMediumTokenLength < 0L) {
            sDefaultMediumTokenLength = Config.getInstance().getInteger(KEY_SHORTURL_MEDIUMTOKEN_LENGTH, TokenUtil.DEFAULT_MEDIUMTOKEN_LENGTH);
        }
        return sDefaultMediumTokenLength;
    }

    public static int getLongTokenLength()
    {
        if(sDefaultLongTokenLength < 0L) {
            sDefaultLongTokenLength = Config.getInstance().getInteger(KEY_SHORTURL_LONGTOKEN_LENGTH, TokenUtil.DEFAULT_LONGTOKEN_LENGTH);
        }
        return sDefaultLongTokenLength;
    }

    public static int getDigitTokenLength()
    {
        if(sDefaultDigitTokenLength < 0L) {
            sDefaultDigitTokenLength = Config.getInstance().getInteger(KEY_SHORTURL_DIGITTOKEN_LENGTH, TokenUtil.DEFAULT_DIGITTOKEN_LENGTH);
        }
        return sDefaultDigitTokenLength;
    }

    public static int getVowelTokenLength()
    {
        if(sDefaultVowelTokenLength < 0L) {
            sDefaultVowelTokenLength = Config.getInstance().getInteger(KEY_SHORTURL_VOWELTOKEN_LENGTH, TokenUtil.DEFAULT_VOWELTOKEN_LENGTH);
        }
        return sDefaultVowelTokenLength;
    }

    public static int getAbcTokenLength()
    {
        if(sDefaultAbcTokenLength < 0L) {
            sDefaultAbcTokenLength = Config.getInstance().getInteger(KEY_SHORTURL_ABCTOKEN_LENGTH, TokenUtil.DEFAULT_ABCTOKEN_LENGTH);
        }
        return sDefaultAbcTokenLength;
    }

    public static int getBinaryTokenLength()
    {
        if(sDefaultBinaryTokenLength < 0L) {
            sDefaultBinaryTokenLength = Config.getInstance().getInteger(KEY_SHORTURL_BINARYTOKEN_LENGTH, TokenUtil.DEFAULT_BINARYTOKEN_LENGTH);
        }
        return sDefaultBinaryTokenLength;
    }


    public static int getSassyTokenMixedcaseLength()
    {
        if(sDefaultSassyTokenMixedcaseLength < 0L) {
            sDefaultSassyTokenMixedcaseLength = Config.getInstance().getInteger(KEY_SASSYTOKEN_MIXEDCASE_LENGTH, TokenUtil.DEFAULT_SASSYTOKEN_MIXEDCASE_LENGTH);
        }
        return sDefaultSassyTokenMixedcaseLength;
    }

    public static int getSassyTokenLowercapsLength()
    {
        if(sDefaultSassyTokenLowercapsLength < 0L) {
            sDefaultSassyTokenLowercapsLength = Config.getInstance().getInteger(KEY_SASSYTOKEN_LOWERCAPS_LENGTH, TokenUtil.DEFAULT_SASSYTOKEN_LOWERCAPS_LENGTH);
        }
        return sDefaultSassyTokenLowercapsLength;
    }

    public static int getSassyTokenLowercaseLength()
    {
        if(sDefaultSassyTokenLowercaseLength < 0L) {
            sDefaultSassyTokenLowercaseLength = Config.getInstance().getInteger(KEY_SASSYTOKEN_LOWERCASE_LENGTH, TokenUtil.DEFAULT_SASSYTOKEN_LOWERCASE_LENGTH);
        }
        return sDefaultSassyTokenLowercaseLength;
    }

    public static int getSassyTokenLongtokenLength()
    {
        if(sDefaultSassyTokenLongtokenLength < 0L) {
            sDefaultSassyTokenLongtokenLength = Config.getInstance().getInteger(KEY_SASSYTOKEN_LONGTOKEN_LENGTH, TokenUtil.DEFAULT_SASSYTOKEN_LONGTOKEN_LENGTH);
        }
        return sDefaultSassyTokenLongtokenLength;
    }


    // Inclusive
    public static int getCustomTokenMinLength()
    {
        if(sDefaultCustomTokenMinLength < 0) {
            sDefaultCustomTokenMinLength = Config.getInstance().getInteger(KEY_SHORTURL_CUSTOMTOKEN_MINLENGTH, 5);    // ???
        }
        return sDefaultCustomTokenMinLength;
    }
    
    // Inclusive
    public static int getCustomTokenMaxLength()
    {
        if(sDefaultCustomTokenMaxLength < 0) {
            sDefaultCustomTokenMaxLength = Config.getInstance().getInteger(KEY_SHORTURL_CUSTOMTOKEN_MAXLENGTH, 200);   // ???
        }
        return sDefaultCustomTokenMaxLength;
    }


    // Inclusive
    public static int getSequentialTokenGenerationMinLength()
    {
        if(sSequentialTokenGenerationMinLength < 0) {
            sSequentialTokenGenerationMinLength = Config.getInstance().getInteger(KEY_SEQUENTIAL_TOKENGENENRATION_MINLENGTH, 2);    // ???
        }
        return sSequentialTokenGenerationMinLength;
    }
    
    
    public static int getSystemDefaultShortUrlMaxAge()
    {
        if(sDefaultShortUrlMaxAge < 0) {
            sDefaultShortUrlMaxAge = Config.getInstance().getInteger(KEY_SHORTURL_EXPIRATION_MAXAGE, 0);   // 0 means no expiration ????
        }
        return sDefaultShortUrlMaxAge;
    }

    public static boolean isTokenFormatAllowSpace()
    {
        if(sTokenFormatAllowSpace == null) {
            sTokenFormatAllowSpace = Config.getInstance().getBoolean(KEY_TOKENFORMAT_ALLOWSPACE, false);   // False by default.
            if(sTokenFormatAllowSpace == null) {
                sTokenFormatAllowSpace = false;
            }
        }
        return sTokenFormatAllowSpace;
    }

    public static boolean isTokenFormatAllowSlash()
    {
        if(sTokenFormatAllowSlash == null) {
            sTokenFormatAllowSlash = Config.getInstance().getBoolean(KEY_TOKENFORMAT_ALLOWSLASH, false);   // False by default.
            if(sTokenFormatAllowSlash == null) {
                sTokenFormatAllowSlash = false;
            }
        }
        return sTokenFormatAllowSlash;
    }


    // Inclusive
    public static int getKeywordMinLength()
    {
        if(sDefaultKeywordMinLength < 0) {
            sDefaultKeywordMinLength = Config.getInstance().getInteger(KEY_NAVKEYWORD_MINLENGTH, 5);    // ???
        }
        return sDefaultKeywordMinLength;
    }
    
    // Inclusive
    public static int getKeywordMaxLength()
    {
        if(sDefaultKeywordMaxLength < 0) {
            sDefaultKeywordMaxLength = Config.getInstance().getInteger(KEY_NAVKEYWORD_MAXLENGTH, 200);   // ???
        }
        return sDefaultKeywordMaxLength;
    }


    
    // If set to true,
    // then the verify page is refreshed to show the http://xxx/v/yyy page.
    // This is applicable only to internal/myurldb short urls.
    // For external short urls (e.g., goog.gl), this is ignored.
    public static boolean isUrlVerifyFollowVerifyPage()
    {
        if(sUrlVerifyFollowVerifyPage == null) {
            sUrlVerifyFollowVerifyPage = Config.getInstance().getBoolean(KEY_URLVERIFY_FOLLOW_VERIFYPAGE, false);   // ???
            if(sUrlVerifyFollowVerifyPage == null) {
                sUrlVerifyFollowVerifyPage = false;
            }
        }
        return sUrlVerifyFollowVerifyPage;
    }


    // TBD: Default value???
    public static boolean isPermissionRequiredForShortLinkCreate()
    {
        if(sPermissionRequiredForShortLinkCreate == null) {
            // sPermissionRequiredForShortLinkCreate = Config.getInstance().getBoolean(KEY_PERMISSION_REQUIRED_SHORTLINK_CREATE, true);   // ???
            boolean defaultCreatePermissionRequired = new ShortLinkBasePermission().isCreatePermissionRequired();    // ?????
            sPermissionRequiredForShortLinkCreate = Config.getInstance().getBoolean(KEY_PERMISSION_REQUIRED_SHORTLINK_CREATE, defaultCreatePermissionRequired);
            if(sPermissionRequiredForShortLinkCreate == null) {
                sPermissionRequiredForShortLinkCreate = defaultCreatePermissionRequired;       // ????
            }
        }
        return sPermissionRequiredForShortLinkCreate;
    }


    // Returns the redirected page URL... (redirected from /home)
    // If it's "/home", no redirection....
    public static String getUiWebAppFrontPage()
    {
        if(sUiWebAppFrontPage == null) {
            sUiWebAppFrontPage = Config.getInstance().getString(KEY_UI_WEBAPP_FRONTPAGE, "/home");   // ???
        }
        return sUiWebAppFrontPage;
    }

    public static boolean isUiWebAppUserSettingEnabled()
    {
        if(sUiWebAppUserSettingEnabled == null) {
            sUiWebAppUserSettingEnabled = Config.getInstance().getBoolean(KEY_UI_WEBAPP_USERSETTING_ENABLED, false);   // ???
            if(sUiWebAppUserSettingEnabled == null) {
                sUiWebAppUserSettingEnabled = false;
            }
        }
        return sUiWebAppUserSettingEnabled;
    }

    // "Client" is an organization/smallbiz (e.g., on Google Apps platform)...
    public static boolean isUiWebAppClientSettingEnabled()
    {
        if(sUiWebAppClientSettingEnabled == null) {
            sUiWebAppClientSettingEnabled = Config.getInstance().getBoolean(KEY_UI_WEBAPP_CLIENTSETTING_ENABLED, false);   // ???
            if(sUiWebAppClientSettingEnabled == null) {
                sUiWebAppClientSettingEnabled = false;
            }
        }
        return sUiWebAppClientSettingEnabled;
    }

    public static boolean isUiShortLinkEditTokenTypeSelectionDisplayed()
    {
        if(sUiShortLinkEditTokenTypeSelectionDisplayed == null) {
            sUiShortLinkEditTokenTypeSelectionDisplayed = Config.getInstance().getBoolean(KEY_UI_SHORTLINKEDIT_TOKENTYPE_SELECTION_DISPLAYED, false);   // ???
            if(sUiShortLinkEditTokenTypeSelectionDisplayed == null) {
                sUiShortLinkEditTokenTypeSelectionDisplayed = false;
            }
        }
        return sUiShortLinkEditTokenTypeSelectionDisplayed;
    }

    public static boolean isUiShortLinkEditDomainPrefilled()
    {
        if(sUiShortLinkEditDomainPrefilled == null) {
            sUiShortLinkEditDomainPrefilled = Config.getInstance().getBoolean(KEY_UI_SHORTLINKEDIT_DOMAIN_PREFILLED, false);   // ???
            if(sUiShortLinkEditDomainPrefilled == null) {
                sUiShortLinkEditDomainPrefilled = false;
            }
        }
        return sUiShortLinkEditDomainPrefilled;
    }
    
    public static boolean isUiShortLinkEditBookmarkingIncluded()
    {
        if(sUiShortLinkEditBookmarkingIncluded == null) {
            sUiShortLinkEditBookmarkingIncluded = Config.getInstance().getBoolean(KEY_UI_SHORTLINKEDIT_BOOKMARKING_INCLUDED, false);   // ???
            if(sUiShortLinkEditBookmarkingIncluded == null) {
                sUiShortLinkEditBookmarkingIncluded = false;
            }
        }
        return sUiShortLinkEditBookmarkingIncluded;
    }
    
    public static boolean isUiShortLinkEditNavKeywordEnabled()
    {
        if(sUiShortLinkEditNavKeywordEnabled == null) {
            sUiShortLinkEditNavKeywordEnabled = Config.getInstance().getBoolean(KEY_UI_SHORTLINKEDIT_NAVKEYWORD_ENABLED, false);   // ???
            if(sUiShortLinkEditNavKeywordEnabled == null) {
                sUiShortLinkEditNavKeywordEnabled = false;
            }
        }
        return sUiShortLinkEditNavKeywordEnabled;
    }
    

    // For TwitterCard support...
    public static String getTwitterCardDefaultSite()
    {
        if(sTwitterCardDefaultSite == null) {
            sTwitterCardDefaultSite = Config.getInstance().getString(KEY_TWITTERCARD_DEFAULT_SITE, "");  // ???
        }
        return sTwitterCardDefaultSite;
    }
    public static String getTwitterCardDefaultSiteId()
    {
        if(sTwitterCardDefaultSiteId == null) {
            sTwitterCardDefaultSiteId = Config.getInstance().getString(KEY_TWITTERCARD_DEFAULT_SITEID, "");  // ???
        }
        return sTwitterCardDefaultSiteId;
    }

    public static String getTwitterCardDefaultCreator()
    {
        if(sTwitterCardDefaultCreator == null) {
            sTwitterCardDefaultCreator = Config.getInstance().getString(KEY_TWITTERCARD_DEFAULT_CREATOR, "");  // ???
        }
        return sTwitterCardDefaultCreator;
    }
    public static String getTwitterCardDefaultCreatorId()
    {
        if(sTwitterCardDefaultCreatorId == null) {
            sTwitterCardDefaultCreatorId = Config.getInstance().getString(KEY_TWITTERCARD_DEFAULT_CREATORID, "");  // ???
        }
        return sTwitterCardDefaultCreatorId;
    }
    

}
