package com.cannyurl.app.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import com.cannyurl.common.TermType;


// Note: We use this instead of TimeRangeUtil for now...
// ...
// Note: All time periods (day, week, month, etc.) are [begin, end) (e.g., begin is included, but end is excluded)...
public class DateRangeUtil
{
    private static final Logger log = Logger.getLogger(DateRangeUtil.class.getName());

    private DateRangeUtil() {}


    // UTC based.... for now... ???
    // TBD: Make it timezone dependent ????
    // Howt to handle daylight saving time????
    private static final String TZNAME_USPACIFIC = "US/Pacific";   // ??
    private static final TimeZone TIMEZONE_USPACIFIC;
    static {
        TIMEZONE_USPACIFIC = TimeZone.getTimeZone(TZNAME_USPACIFIC);
    }
    

    // ????
    private static Pattern sPattern;
    static {
    	//sPattern = Pattern.compile("^\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}");
        sPattern = Pattern.compile("^\\d{4}-\\d{2}-\\d{2}");
    }
    
    // temporary
    // Need testing...
    public static boolean isValid(String dayHour)
    {
        if(dayHour == null || dayHour.length() < 10) {
            return false;
        }
        return sPattern.matcher(dayHour).matches();
    }
   
    // TBD...
    //public static boolean isValid(String termType, String dayHour)
    // ...
    // ...
    
    
    public static String getTermTime(String termType)
    {
        if(TermType.TERM_HOURLY.equals(termType)) {
            return getDayHour();
        } else if(TermType.TERM_DAILY.equals(termType)) {
            return getDay();
        } else if(TermType.TERM_WEEKLY.equals(termType)) {
            return getWeek();
        } else if(TermType.TERM_MONTHLY.equals(termType)) {
            return getMonth();
        } else {
            return null;   // ????
        }
    }

    public static String getTermTime(String termType, long time)
    {
        if(TermType.TERM_HOURLY.equals(termType)) {
            return getDayHour(time);
        } else if(TermType.TERM_DAILY.equals(termType)) {
            return getDay(time);
        } else if(TermType.TERM_WEEKLY.equals(termType)) {
            return getWeek(time);
        } else if(TermType.TERM_MONTHLY.equals(termType)) {
            return getMonth(time);
        } else {
            return null;   // ????
        }
    }

    
    public static String getMonth()
    {
        return getMonth(System.currentTimeMillis());
    }
    public static String getMonth(long time)
    {
    	//SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-01 00:00");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-01");
        //dateFormat.setTimeZone(TIMEZONE_USPACIFIC);
        String date = dateFormat.format(new Date(time));
        return date;
    }

    public static String getWeek()
    {
        return getWeek(System.currentTimeMillis());
    }
    public static String getWeek(long time)
    {
        long t = 0L;
        int day = getDayOfWeek(time);
        t = time - day * 3600 * 24 * 1000L;
        return getDay(t);
    }
    // temporary
    private static int getDayOfWeek(long time)
    {
        SimpleDateFormat df = new SimpleDateFormat("E");
        //df.setTimeZone(TIMEZONE_USPACIFIC);
        String day = df.format(new Date(time));
        if(log.isLoggable(Level.FINER)) log.finer("getDayOfWeek(): day = " + day + "; time = " + time);

        int dayOfWeek = 0;
        if(day.startsWith("Su")) {
            dayOfWeek = 0;
        } else if(day.startsWith("Mo")) {
            dayOfWeek = 1;
        } else if(day.startsWith("Tu")) {
            dayOfWeek = 2;
        } else if(day.startsWith("We")) {
            dayOfWeek = 3;
        } else if(day.startsWith("Th")) {
            dayOfWeek = 4;
        } else if(day.startsWith("Fr")) {
            dayOfWeek = 5;
        } else if(day.startsWith("Sa")) {
            dayOfWeek = 6;
        }        
        return dayOfWeek;
    }

    public static String getDay()
    {
        return getDay(System.currentTimeMillis());
    }
    public static String getDay(long time)
    {
    	//SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd 00:00");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        //dateFormat.setTimeZone(TIMEZONE_USPACIFIC);
        String date = dateFormat.format(new Date(time));
        return date;
    }

    public static String getDayHour()
    {
        return getDayHour(System.currentTimeMillis());
    }
    public static String getDayHour(long time)
    {
    	//SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:00");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        //dateFormat.setTimeZone(TIMEZONE_USPACIFIC);
        String date = dateFormat.format(new Date(time));
        return date;
    }

    
    // ???
    public static String getMonthFromDayHour(String dayHour)
    {
        if(dayHour == null || dayHour.length() < 10) {
            if(log.isLoggable(Level.WARNING)) log.warning("Invalid input: dayHour = " + dayHour);
            return dayHour;  // ???
        }
        //String month = dayHour.substring(0, 7) + "-01 00:00";  // ???
        String month = dayHour.substring(0, 7) + "-01";  // ???
        return month;
    }
    public static String getWeekFromDayHour(String dayHour)
    {
        if(dayHour == null || dayHour.length() < 10) {
            if(log.isLoggable(Level.WARNING)) log.warning("Invalid input: dayHour = " + dayHour);
            return dayHour;  // ???
        }
        return getWeek(getMilli(dayHour));  // ????
    }
    public static String getDayFromDayHour(String dayHour)
    {
        if(dayHour == null || dayHour.length() < 10) {
            if(log.isLoggable(Level.WARNING)) log.warning("Invalid input: dayHour = " + dayHour);
            return dayHour;  // ???
        }
        //String day = dayHour.substring(0, 10) + " 00:00";  // ???
        String day = dayHour.substring(0, 10);  // ???
        return day;
    }
    
    
    public static String[] getRange(String termType, String startDayHour, String endDayHour)
    {
        if(TermType.TERM_HOURLY.equals(termType)) {
            return getDayHourRange(startDayHour, endDayHour);
        } else if(TermType.TERM_DAILY.equals(termType)) {
            return getDayRange(startDayHour, endDayHour);
        } else if(TermType.TERM_WEEKLY.equals(termType)) {
            return getWeekRange(startDayHour, endDayHour);
        } else if(TermType.TERM_MONTHLY.equals(termType)) {
            return getMonthRange(startDayHour, endDayHour);
        } else {
            return null;   // ????
        }
    }

    public static String[] getMonthRange(String startDayHour, String endDayHour)
    {
        if(! isValid(startDayHour)) {
            return null;
        }
        if(endDayHour == null || ! isValid(endDayHour)) {
            endDayHour = getMonth();
            if(log.isLoggable(Level.INFO)) log.info("Invalid endDayHour: New value = " + endDayHour);
        }
        
        // temporary.
        // Not the most efficient algo.
        List<String> vals = new ArrayList<String>();
        String pointer = startDayHour;
        while(pointer.compareTo(endDayHour) <= 0) {
            vals.add(pointer);
            pointer = getNextMonth(pointer);            
        }
        
        String[] range = vals.toArray(new String[0]);
        return range;
    }

    public static String[] getWeekRange(String startDayHour, String endDayHour)
    {
        if(! isValid(startDayHour)) {
            return null;
        }
        if(endDayHour == null || ! isValid(endDayHour)) {
            endDayHour = getWeek();
            if(log.isLoggable(Level.INFO)) log.info("Invalid endDayHour: New value = " + endDayHour);
        }
        
        // temporary.
        // Not the most efficient algo.
        List<String> vals = new ArrayList<String>();
        String pointer = startDayHour;
        while(pointer.compareTo(endDayHour) <= 0) {
            vals.add(pointer);
            pointer = getNextWeek(pointer);            
        }
        
        String[] range = vals.toArray(new String[0]);
        return range;
    }
    
    public static String[] getDayRange(String startDayHour, String endDayHour)
    {
        if(! isValid(startDayHour)) {
            return null;
        }
        if(endDayHour == null || ! isValid(endDayHour)) {
            endDayHour = getDay();
            if(log.isLoggable(Level.INFO)) log.info("Invalid endDayHour: New value = " + endDayHour);
        }
        
        // temporary.
        // Not the most efficient algo.
        List<String> vals = new ArrayList<String>();
        String pointer = startDayHour;
        while(pointer.compareTo(endDayHour) <= 0) {
            vals.add(pointer);
            pointer = getNextDay(pointer);            
        }
        
        String[] range = vals.toArray(new String[0]);
        return range;
    }
    
    public static String[] getDayHourRange(String startDayHour, String endDayHour)
    {
        if(! isValid(startDayHour)) {
            return null;
        }
        if(endDayHour == null || ! isValid(endDayHour)) {
            endDayHour = getDayHour();
            if(log.isLoggable(Level.INFO)) log.info("Invalid endDayHour: New value = " + endDayHour);
        }
        
        // temporary.
        // Not the most efficient algo.
        List<String> vals = new ArrayList<String>();
        String pointer = startDayHour;
        while(pointer.compareTo(endDayHour) <= 0) {
            vals.add(pointer);
            pointer = getNextDayHour(pointer);            
        }
        
        String[] range = vals.toArray(new String[0]);
        return range;
    }

    
    public static long getMilli(String day)
    {
    	//SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        //dateFormat.setTimeZone(TIMEZONE_USPACIFIC);
        long milli = 0L;
        try {
            Date date = dateFormat.parse(day);
            milli = date.getTime();
        } catch (ParseException e) {
            log.log(Level.WARNING, "Failed to parse the input string: day = " + day, e);
        }
        return milli;
    }
    
    public static long[] getMilliRange(String termType, String dayHour)
    {
        long[] range = new long[2];
        range[0] = getMilli(dayHour);
        range[1] = getMilli(getNext(termType, dayHour));
        return range;
    }

    
    
    // TBD:
    // Methods to convert an arbitrary date string into a valid date/time based on termType...
    // ...

    
    
    
    public static String getNext(String termType, String dayHour)
    {
        if(TermType.TERM_HOURLY.equals(termType)) {
            return getNextDayHour(dayHour);
        } else if(TermType.TERM_DAILY.equals(termType)) {
            return getNextDay(dayHour);
        } else if(TermType.TERM_WEEKLY.equals(termType)) {
            return getNextWeek(dayHour);
        } else if(TermType.TERM_MONTHLY.equals(termType)) {
            return getNextMonth(dayHour);
        } else {
            return null;   // ????
        }
    }
    public static String getPrevious(String termType, String dayHour)
    {
        if(TermType.TERM_HOURLY.equals(termType)) {
            return getPreviousDayHour(dayHour);
        } else if(TermType.TERM_DAILY.equals(termType)) {
            return getPreviousDay(dayHour);
        } else if(TermType.TERM_WEEKLY.equals(termType)) {
            return getPreviousWeek(dayHour);
        } else if(TermType.TERM_MONTHLY.equals(termType)) {
            return getPreviousMonth(dayHour);
        } else {
            return null;   // ????
        }
    }
    
    
    public static String getNextMonth(String dayHour)
    {
        if(dayHour == null || dayHour.length() < 10) {
            if(log.isLoggable(Level.WARNING)) log.warning("Invalid input: dayHour = " + dayHour);
            return null;  // ???
        }
        String currentMonth = getMonthFromDayHour(dayHour);
        int year = Integer.parseInt(currentMonth.substring(0, 4));
        int month = Integer.parseInt(currentMonth.substring(5, 7)) + 1;
        if(month > 12) {
            month = 1;
            year += 1;
        }
        String mo = "";
        if(month < 10) {
            mo = "0" + month;
        } else {
            mo = "" + month;
        }
        //String nextMonth = year + "-" + mo + "-01 00:00"; 
        String nextMonth = year + "-" + mo + "-01"; 
        return nextMonth;
    }
    public static String getPreviousMonth(String dayHour)
    {
        if(dayHour == null || dayHour.length() < 10) {
            if(log.isLoggable(Level.WARNING)) log.warning("Invalid input: dayHour = " + dayHour);
            return null;  // ???
        }
        String currentMonth = getMonthFromDayHour(dayHour);
        int year = Integer.parseInt(currentMonth.substring(0, 4));
        int month = Integer.parseInt(currentMonth.substring(5, 7)) - 1;
        if(month < 1) {
            month = 12;
            year -= 1;
        }
        String mo = "";
        if(month < 10) {
            mo = "0" + month;
        } else {
            mo = "" + month;
        }
        //String prevMonth = year + "-" + mo + "-01 00:00"; 
        String prevMonth = year + "-" + mo + "-01"; 
        return prevMonth;
    }
    
    public static String getNextWeek(String dayHour)
    {
        if(dayHour == null || dayHour.length() < 10) {
            if(log.isLoggable(Level.WARNING)) log.warning("Invalid input: dayHour = " + dayHour);
            return null;  // ???
        }
        long milli = getMilli(dayHour) + 7 * 3600 * 24 * 1000L;
        return getWeek(milli);
    }
    public static String getPreviousWeek(String dayHour)
    {
        if(dayHour == null || dayHour.length() < 10) {
            if(log.isLoggable(Level.WARNING)) log.warning("Invalid input: dayHour = " + dayHour);
            return null;  // ???
        }
        long milli = getMilli(dayHour) - 7 * 3600 * 24 * 1000L;
        return getWeek(milli);
    }
    
    public static String getNextDay(String dayHour)
    {
        if(dayHour == null || dayHour.length() < 10) {
            if(log.isLoggable(Level.WARNING)) log.warning("Invalid input: dayHour = " + dayHour);
            return null;  // ???
        }
        long milli = getMilli(dayHour) + 3600 * 24 * 1000L;
        return getDay(milli);
    }
    public static String getPreviousDay(String dayHour)
    {
        if(dayHour == null || dayHour.length() < 10) {
            if(log.isLoggable(Level.WARNING)) log.warning("Invalid input: dayHour = " + dayHour);
            return null;  // ???
        }
        long milli = getMilli(dayHour) - 3600 * 24 * 1000L;
        return getDay(milli);
    }
    
    public static String getNextDayHour(String dayHour)
    {
        if(dayHour == null || dayHour.length() < 10) {
            if(log.isLoggable(Level.WARNING)) log.warning("Invalid input: dayHour = " + dayHour);
            return null;  // ???
        }
        long milli = getMilli(dayHour) + 3600 * 1000L;
        return getDayHour(milli);
    }
    public static String getPreviousDayHour(String dayHour)
    {
        if(dayHour == null || dayHour.length() < 10) {
            if(log.isLoggable(Level.WARNING)) log.warning("Invalid input: dayHour = " + dayHour);
            return null;  // ???
        }
        long milli = getMilli(dayHour) - 3600 * 1000L;
        return getDayHour(milli);
    }

    
    public static int compareMonths(String dayL, String dayR)
    {
        return compare(dayL, dayR);
    }    
    public static int compareWeeks(String dayL, String dayR)
    {
        return compare(dayL, dayR);
    }    
    public static int compareDays(String dayL, String dayR)
    {
        return compare(dayL, dayR);
    } 
    public static int compareHours(String dayL, String dayR)
    {
        return compare(dayL, dayR);
    }    

    
    // ???
    public static int compareHourAndDay(String hourL, String dayR)
    {
        String dayL = getDayFromDayHour(hourL);
        return compare(dayL, dayR);
    }
    public static int compareHourAndWeek(String hourL, String dayR)
    {
        String weekL = getWeekFromDayHour(hourL);
        return compare(weekL, dayR);
    }
    public static int compareHourAndMonth(String hourL, String dayR)
    {
        String monthL = getMonthFromDayHour(hourL);
        return compare(monthL, dayR);
    }
    public static int compareDayAndWeek(String hourL, String dayR)
    {
        String weekL = getWeekFromDayHour(hourL);
        return compare(weekL, dayR);
    }
    public static int compareDayAndMonth(String hourL, String dayR)
    {
        String monthL = getMonthFromDayHour(hourL);
        return compare(monthL, dayR);
    }
    public static int compareWeekAndMonth(String hourL, String dayR)
    {
        String monthL = getMonthFromDayHour(hourL);
        return compare(monthL, dayR);
    }

    
    public static int compareTime(String termType, long time, String dayR)
    {
        if(TermType.TERM_HOURLY.equals(termType)) {
            return compareTimeAndHour(time, dayR);
        } else if(TermType.TERM_DAILY.equals(termType)) {
            return compareTimeAndDay(time, dayR);
        } else if(TermType.TERM_WEEKLY.equals(termType)) {
            return compareTimeAndWeek(time, dayR);
        } else if(TermType.TERM_MONTHLY.equals(termType)) {
            return compareTimeAndMonth(time, dayR);
        } else {
            return 0;   // ????
        }
    }

    public static int compareTimeAndHour(long time, String dayR)
    {
        String dayL = getDayHour(time);
        return compare(dayL, dayR);        
    }
    public static int compareTimeAndDay(long time, String dayR)
    {
        String dayL = getDay(time);
        return compare(dayL, dayR);        
    }
    public static int compareTimeAndWeek(long time, String dayR)
    {
        String dayL = getWeek(time);
        return compare(dayL, dayR);        
    }
    public static int compareTimeAndMonth(long time, String dayR)
    {
        String dayL = getMonth(time);
        return compare(dayL, dayR);        
    }
    

    // This has somewhat ambiguous semantics. Use the above, more specialized methods...
    private static int compare(String dayL, String dayR)
    {
        if(dayL == null && dayR == null) {
            return 0;
        }
        if(dayL == null) {
            return -1;
        }
        if(dayR == null) {
            return 1;
        }
        return dayL.compareTo(dayR);
    }
    
}
