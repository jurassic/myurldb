package com.cannyurl.app.util;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletContext;

import com.cannyurl.af.util.DevelUtil;


// TBD: Make it a singleton???
public class DebugUtil
{
    private static final Logger log = Logger.getLogger(DebugUtil.class.getName());

    private DebugUtil() {}

    // Lazy initialization...
    // TBD: DEVEL_ENABLED and BETA_ENABLED should be really combined into one "integer flag".
    // See comment below...
    private static Boolean DEVEL_ENABLED = null;
    private static Boolean BETA_ENABLED = null;


    public static boolean isRunningOnDevel()
    {
        return DevelUtil.isRunningOnDevel();
    }


    /////////////////////////////////////////////////////////
    // Note: if devel==true, then always beta==true.
    //      (Or, inversely, we could have "if beta==false, then always devel==false.)
    // [A] In the current implementation, the conditions should be checked as follows...
    //       (1) isBetaFeatureEnabled() { /* Do something */ isDevelFeatureEnabled() { /* Do something */ } else {} } else {}
    //    or,
    //       (2) isDevelFeatureEnabled() { /* Beta always true. Do something */ } else { isBetaFeatureEnabled() { /* Do something */ } else {} }
    // Option (1) is preferred since, in many use cases, the "else" block might be missing...
    // [B] TBD:
    //     Some devel features may rely on other devel/beta features, and some beta features may rely on other beta features..
    //     How to enforce this constraint across the application????
    //     .....
    /////////////////////////////////////////////////////////

    public static boolean isDevelFeatureEnabled()
    {
        if(DEVEL_ENABLED == null) {
            // No caching....
            if(isRunningOnDevel() == false) {
                return false;
            } else {    // if(isRunningOnDevel() == true) {
                return ConfigUtil.isDevelFeatureEnabled();
            }
        }
        return DEVEL_ENABLED;
    }

    public static boolean isBetaFeatureEnabled()
    {
    	if(BETA_ENABLED == null) {
    		// No "caching"...
	    	if(isDevelFeatureEnabled() == true) {
	    		return true;
	        } else {    // if(isRunningOnDevel() == true) {
	            return ConfigUtil.isBetaFeatureEnabled();
	        }
    	}
    	return BETA_ENABLED;
    }


}
