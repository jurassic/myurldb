package com.cannyurl.app.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cannyurl.af.config.Config;
import com.cannyurl.app.core.CustomDomainMap;
import com.cannyurl.common.DomainType;
import com.cannyurl.helper.ShortUrlAccessHelper;


// TBD: Get user's "domain" from DB. ??? --> Probably not...
// ...
// TBD: Option to use a domain based on the request hostname.
//    e.g., if a user is creating a shortlink from http://a.mq.ms/edit then the short url should start with a.mq.ms...
// ...
// ....
// TBD: We need to be consistent with authmode and domaintype....
//      for example, if domaintype==twitter, then authmode should be twitter... Otherwise, it won't work...
//      etc...
// How to enforce this????
// TBD:
//      We probably need "default username" config var
//      so that if the API request comes in (without username), we can still serve without failing ???
///     (in case the domaintype requires username)....
// ...
// TBD: Combinations???
// e..g., subdoamin for "organization" and username/usercode for user  ( http://org1.smb.bz/user1/, etc.)
// ????
public class DomainUtil
{
    private static final Logger log = Logger.getLogger(DomainUtil.class.getName());
    private static Random sRandom = new Random(new Date().getTime());
    
    private DomainUtil() {}

    // Initialization-on-demand holder.
    private static final class DefaultDomainListHolder
    {
        private static final String KEY_SHORTURI_DEFAULT_BASEDOMAIN = "cannyurlapp.shorturi.default.basedomain";
        private static final String KEY_SHORTURI_DOMAIN_LIST = "cannyurlapp.shorturi.domainlist";

        private static String sDefaultBaseDomain = null;
        private static List<String> sDomains = new ArrayList<String>();
        static {
            sDefaultBaseDomain =  Config.getInstance().getString(KEY_SHORTURI_DEFAULT_BASEDOMAIN, "");
            
            // temporary
//            sDomains.add("http://ui0.us/");
//            sDomains.add("http://ui1.us/");
//            sDomains.add("http://ui2.us/");
//            sDomains.add("http://ui3.us/");
//            sDomains.add("http://ui4.us/");
//            sDomains.add("http://ui5.us/");
//            sDomains.add("http://ui6.us/");
//            sDomains.add("http://ui7.us/");
//            sDomains.add("http://ui8.us/");
//            sDomains.add("http://ui9.us/");
            
            // temporary. For debugging...
            //sDomains.add("http://localhost:8899/");
            
            String domainList = Config.getInstance().getString(KEY_SHORTURI_DOMAIN_LIST, "");
            String[] domainArr = domainList.split(",\\s*");
            sDomains.addAll(Arrays.asList(domainArr));            
            if(DomainUtil.log.isLoggable(Level.INFO)) {
                DomainUtil.log.log(Level.INFO, "Domain list read from the config: ");
                for(String d : sDomains) {
                    DomainUtil.log.log(Level.INFO, "-- domain: " + d);
                }
            }
            if(sDefaultBaseDomain == null || sDefaultBaseDomain.isEmpty()) {
                if(! sDomains.isEmpty() ) {
                    sDefaultBaseDomain = sDomains.get(0);   // Use the first element....
                }
            }
        }

        static String getDefaultBaseDomain()
        {
            return sDefaultBaseDomain;
        }
        static List<String> getDomainList()
        {
            return sDomains;
        }
    }

    

    
    // temporary
    // This should be used only when input domain is not set/not inputted....
    // Returns a new/modified domainType, if necessary...
    public static String validateInitialDomainType(String domainType)
    {
        String systemDefaultDomainType = ConfigUtil.getSystemDefaultDomainType();
        boolean domainTypeOverrideAllowed = ConfigUtil.isDomainTypeOverrideAllowed();

        if(domainTypeOverrideAllowed) {
            if(! DomainType.isValidType(domainType)) {
                if(log.isLoggable(Level.INFO)) log.info("Invalid input domainType = " + domainType + ". systemDefaultDomainType will be used: " + systemDefaultDomainType);
                domainType = systemDefaultDomainType;
            } else {
                // Use the input domainType...
            }
        } else {
            // TBD: Note that the frontend may not have honored the system config, domainTypeOverrideAllowed....
            // What to do in such a case????
            if(domainType == null || ! domainType.equals(systemDefaultDomainType)) {
                // if(DomainType.isValidType(systemDefaultDomainType)) {
                    if(log.isLoggable(Level.INFO)) log.info("Input domainType = " + domainType + " will be ignored: systemDefaultDomainType = " + systemDefaultDomainType);
                    domainType = systemDefaultDomainType;
                // } else {
                //     // ????
                // }
            } else {
                // Nothing to do...
            }
        }

        return domainType;
    }
    
    
    
    
    // TBD:
    // (Part of) This functionality may need to be moved "after" app.auth package. ???
    // ???
    // (Note: package order.  app.util -> app.auth  -> app.helper ....)
    // ....
    // --> Note tha comment below..
    // For now, we do NOT query user service or auth related services......
    //   (so, we can keep this here in util pacckaage...)
    // ...
    
    // TBD:
    // however, domain should really depend on client/group (e.g., google apps),
    // in such a case we should query database...
    // ....
    
    
    // "Root" or top part of the domain for domainType==userename, twitter, userurl, etc..... 
    public static String getDefaultBaseDomain()
    {
        return DefaultDomainListHolder.getDefaultBaseDomain();
    }

    
    
    // Note:
    // We do not require user (User.guid) even when username/usercode is needed...
    // This makes, on the one hand, makes it usable for un-autnenticated users (e.g., by API calls)
    // On the other hand, it increases the risk of spoofing/errors....
    // ...
    // Currently, user is required only when username/usercode is needed and they are not supplied...
    // ....
    

    // user: User.guid.
    public static final String getDomain(String domainType, String user)
    {
        return getDomain(domainType, user, null);
    }
    public static final String getDomain(String domainType, String user, String username)
    {
        return getDomain(domainType, user, username, null);
    }
    public static final String getDomain(String domainType, String user, String username, String usercode)
    {
        if(!DomainType.isValidType(domainType)) {
            // TBD: Just bail out?
            if(log.isLoggable(Level.INFO)) log.info("Invalid domainType = " + domainType);
            domainType = ConfigUtil.getSystemDefaultDomainType();
            if(log.isLoggable(Level.FINE)) log.fine("domainType to be used instead = " + domainType);
        }
        
        // Note that
        // domainType==custom does not make sense in this context.
        // domainType==custom means that the domain param has been explicitly set.
        // Same with domainType==branded ???
        // ...
        

        
        // Note: This part is an updated algorithm on 12/03/11.
        // If the usedomain.userurl config var is set to true, then domain is always in the form http://a.com/xyz/.
        //      and, other forms such as http://a.com/!xyz/ or http://a.com/@xyz/ are not allowed.
        // Note that UserDomain type does not require useDomainUserURL = true ... ???
        boolean useDomainUserURL = ConfigUtil.isUseDomainUserURL();
        // TBD ....
        if(domainType.equals(DomainType.TYPE_USERURL)) {
            useDomainUserURL = true;   // ?????
        }
        // ....
        

        // Base domain url (including http:// )...
        String baseDomain = getDefaultBaseDomain();
        // TBD: Allow different values for different users??? for brands??? etc.???
        // Or, even a random/round-robin domain from a set ????
        /// ....

        
        if(domainType.equals(DomainType.TYPE_DEFAULT) 
                || domainType.equals(DomainType.TYPE_USERNAME) 
                || domainType.equals(DomainType.TYPE_USERCODE) 
                || domainType.equals(DomainType.TYPE_TWITTER) 
                || domainType.equals(DomainType.TYPE_GOOGLE) 
                || domainType.equals(DomainType.TYPE_FACEBOOK) 
                || domainType.equals(DomainType.TYPE_EMAIL) 
                // || domainType.equals(DomainType.TYPE_SUBDOMAIN) 
                || domainType.equals(DomainType.TYPE_USERURL)) {
            if(baseDomain == null || baseDomain.isEmpty()) {
                log.warning("baseDomain is not set. Cannot proceed.");
                return null;   // ???  
            }
        }
        if(domainType.equals(DomainType.TYPE_USERCODE) ) {
            if(usercode == null || usercode.isEmpty()) {
                if(user == null || user.isEmpty()) {
                    log.warning("Both user and usercode are null/empty. Cannot proceed.");
                    return null;   // ???
                }
                
                // TBD:
                // Find the user in the DB... ???
                // No. This should have been done by the caller....
                // Don't do it again...
                // ....
                
            }
            
            
            // ....
            if(usercode == null || usercode.isEmpty()) {
                // ???
                log.warning("usercode is null/empty. Cannot proceed.");
                return null;   // ???

                // log.warning("usercode is null/empty. This should not happen.");
                // TBD: Use a default usercode from the system config ?????
                // Or at least based on domainType ????
                // usercode = "anonymous";   // ?????
                // usercode = "_anon_";     // ?????
                // usercode = ConfigUtil.getApplicationBrand();   // ????
                // usercode = "";            // ?????
                // ????
                // ....
            }
        } else if(domainType.equals(DomainType.TYPE_USERNAME) 
                || domainType.equals(DomainType.TYPE_TWITTER) 
                || domainType.equals(DomainType.TYPE_GOOGLE) 
                || domainType.equals(DomainType.TYPE_FACEBOOK) 
                || domainType.equals(DomainType.TYPE_EMAIL) 
                // || domainType.equals(DomainType.TYPE_SUBDOMAIN) 
                || domainType.equals(DomainType.TYPE_USERURL)) {
            if(username == null || username.isEmpty()) {
                if(user == null || user.isEmpty()) {
                    log.warning("Both user and username are null/empty. Cannot proceed.");
                    return null;   // ???
                }
                
                // TBD:
                // Find the user in the DB... ??
                // Note: if we do this, this class really should be moved after app.auth package.....
                // ....
                
            }
                       
            
            // ....
            if(username == null || username.isEmpty()) {
                // temporarily commented out.
                // log.warning("username is null/empty. Cannot proceed.");
                // return null;   // ???

                log.warning("username is null/empty. This should not happen.");
                // TBD: Use a default username from the system config ?????
                // Or at least based on domainType ????
                // username = "anonymous";   // ?????
                username = "_anonuser_";     // ?????
                // username = ConfigUtil.getApplicationBrand();   // ????
                // username = "";            // ?????
                // ????
                // ....
            }
        } 
       
        

        // ???
        if(! baseDomain.endsWith("/")) {
            baseDomain += "/";
        }


        // TBD:
        String subdomainOrPath = ConfigUtil.getDomainTypeSubdomainOrPath();
        int usernameMinLength = ConfigUtil.getUsernameMinLength();
        int usernameMaxLength = ConfigUtil.getUsernameMaxLength();
        int usercodeMinLength = ConfigUtil.getUsercodeMinLength();
        int usercodeMaxLength = ConfigUtil.getUsercodeMaxLength();
        // ...
        
        // TBD:
        // username/usercode length constraints ????
        // ????
        // ...


        String domain = null;
        if(domainType.equals(DomainType.TYPE_DEFAULT)) {
            // ????
            domain = baseDomain;
        } else if(domainType.equals(DomainType.TYPE_USERCODE)) {
            if(usercode != null && !usercode.isEmpty()) {
                int usercodeLen = usercode.length();
                if(usercodeLen >= usercodeMinLength && usercodeLen <= usercodeMaxLength) {
                    if("subdomain".equals(subdomainOrPath)) {
                        // TBD:
                        domain = buildUsernameSubdomain(baseDomain, usercode);
                        // ...
                    } else {    // if("path".equals(subdomainOrPath))
                        if(useDomainUserURL == true) {
                            domain = baseDomain + usercode;
                        } else {
                            domain = baseDomain + "~" + usercode;    // ????
                        }
                    }
                    // To be consistent, always add the trailing "/".
                    if(! domain.endsWith("/")) {   // This should always be true, at this point, because username cannot add with "/".
                        domain += "/";
                    }
                } else {
                    // ????
                    if(log.isLoggable(Level.INFO)) log.info("usercode length is invalid for domainType = " + domainType + "; usercodeLen = " + usercodeLen);
                    // what to do????
                }
            } else {
                // ????
                if(log.isLoggable(Level.INFO)) log.info("usercode is not set for domainType = " + domainType);
                // // domain = baseDomain;
                // domain = getRandomDomain();
                // ????
                
                // TBD:
                // If usercode==null/"",
                // fallback on username ????
                // .....
                // Use config var as to whether to do this or not????
                // .....
                
            }
        } else if(domainType.equals(DomainType.TYPE_USERNAME) 
                || domainType.equals(DomainType.TYPE_TWITTER) 
                || domainType.equals(DomainType.TYPE_GOOGLE) 
                || domainType.equals(DomainType.TYPE_FACEBOOK) 
                || domainType.equals(DomainType.TYPE_EMAIL) 
                // || domainType.equals(DomainType.TYPE_SUBDOMAIN) 
                || domainType.equals(DomainType.TYPE_USERURL)) {
            if(username != null && !username.isEmpty()) {           // TBD: Usercode???
                int usernameLen = username.length();
                if(usernameLen >= usernameMinLength && usernameLen <= usernameMaxLength) {
                    if("subdomain".equals(subdomainOrPath)) {
                        // TBD:
                        domain = buildUsernameSubdomain(baseDomain, username);
                        // ...
                    } else {    // if("path".equals(subdomainOrPath))
                        if(useDomainUserURL == true) {
                            domain = baseDomain + username;
                        } else {
                            if( domainType.equals(DomainType.TYPE_USERNAME) ) {
                                domain = baseDomain + "~" + username;
                            } else if(domainType.equals(DomainType.TYPE_TWITTER)) {
                                // ????
                                domain = baseDomain + "@" + username.toLowerCase();
                                // domain = baseDomain + "@" + username;
                            } else if(domainType.equals(DomainType.TYPE_GOOGLE)) {
                                domain = baseDomain + "+" + username;
                            } else if(domainType.equals(DomainType.TYPE_FACEBOOK)) {
                                domain = baseDomain + "-" + username;
                            } else if(domainType.equals(DomainType.TYPE_EMAIL)) {
                                domain = baseDomain + "!" + username;
//                          } else if(domainType.equals(DomainType.TYPE_SUBDOMAIN)) {
//                              // TBD:
//                              domain = buidUsernameSubdomain(baseDomain, username);
//                              // ....
                            } else if(domainType.equals(DomainType.TYPE_USERURL)) {
                                domain = baseDomain + username;
                            }
                        }
                    }
                    // To be consistent, always add the trailing "/".
                    if(! domain.endsWith("/")) {   // This should always be true, at this point, because username cannot add with "/".
                        domain += "/";
                    }
                } else {
                    // ????
                    if(log.isLoggable(Level.INFO)) log.info("username length is invalid for domainType = " + domainType + "; usernameLen = " + usernameLen);
                    // what to do????
                }
            } else {
                // ????
                if(log.isLoggable(Level.INFO)) log.info("username is not set for domainType = " + domainType);
                // // domain = baseDomain;
                // domain = getRandomDomain();
                // ????
            }
        } else if(domainType.equals(DomainType.TYPE_RANDOM)) {
            domain = getRandomDomain();
        }
        
        if(domain == null) {
            // even when domainType is user-related, use the following as a fallback...
            // temporary
            domain = getRandomDomain();
            if(log.isLoggable(Level.WARNING)) log.warning("Invalid domainType = " + domainType + ". Or invalid username/usercode. Random domain is used: domain = " + domain);
        }

        
        // TBD: validate/sanitize domain????
        // (eg should be a valid url (http://xxx/) and preferably ends with "/", etc...)
        
//        // ???
//        if(! domain.endsWith("/")) {
//            domain += "/";
//        }

        
        // TBD: Check...
        // Does the user have the right to use this particular domain???
        // e.g., http://su2.us/@userx/ can only be used by userx, etc.
        // Same check for managerApp - domain, .....
        // ....
        
        return domain;
    }
    
    
    
    // TBD
    public static final String getRandomDomain()
    {
        List<String> domains = DefaultDomainListHolder.getDomainList();
        int idx = sRandom.nextInt(domains.size());
        return domains.get(idx);
    }

    
    // temporary
    public static String getDomainFromUrl(String url)
    {
        int slashslash = url.indexOf("://") + 3;
        String domain = url.substring(0, url.indexOf('/', slashslash));
        return domain;
    }
 
 
    // temporary
    public static String buildDefaultUsernameBasedDomain(String username)
    {
        String baseDomain = getDefaultBaseDomain();
        return buildUsernameSubdomain(baseDomain, username);
    }
    public static String buildUsernameSubdomain(String baseDomain, String username)
    {
        return buildSubDomainUrl(baseDomain, username);
    }
    public static String buildDefaultUsercodeBasedDomain(String usercode)
    {
        String baseDomain = getDefaultBaseDomain();
        return buildUsercodeSubdomain(baseDomain, usercode);
    }
    public static String buildUsercodeSubdomain(String baseDomain, String usercode)
    {
        return buildSubDomainUrl(baseDomain, usercode);
    }
    private static String buildSubDomainUrl(String baseDomain, String subDomain)
    {
        // temporary
        int slashslash = baseDomain.indexOf("://") + 3;
        String prefix = baseDomain.substring(0, slashslash);
        // TBD: Remove the subdomain, if any.??? (e.g., a.mq.ms --> mq.ms ???)
        // Probably not. The config var should contain "mq.ms" if that was the intention. 
        // If the baseDomain happens to be "a.mq.ms", then the return domain will be "<subDomain>.a.mq.ms"...
        String theRest = baseDomain.substring(slashslash);
        // .....
        String domain = prefix + subDomain + "." + theRest;
        return domain;
    }

    
    // temporary...
    public static String buildAppClientHostedDomain(String clientCode)
    {
        String baseDomain = getDefaultBaseDomain();
        return buildClientCodeSubdomain(baseDomain, clientCode);
    }
    public static String buildClientCodeSubdomain(String baseDomain, String clientCode)
    {
        return buildSubDomainUrl(baseDomain, clientCode);
    }

    
    ///////////////////////////////
    // To be deleted???
    
    // TBD
    private static final String getDefaultDomain(String siteDomain)
    {
        // temporary
        CustomDomainMap map = CustomDomainMap.getInstance();
        // ...
        
        // temporary
        return null;
    }
    // TBD
    private static final String getDefaultDomain(String user, String app)
    {
        // temporary
        return null;
    }
    // TBD
    private static final String getDefaultUserDomain(String user)
    {
        // temporary
        return null;
    }
    // TBD
    private static final String getDefaultAppDomain(String app)
    {
        // temporary
        return null;
    }

}
