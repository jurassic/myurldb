package com.cannyurl.app.util;

import java.util.logging.Logger;

import com.cannyurl.af.bean.CellLatitudeLongitudeBean;
import com.myurldb.ws.CellLatitudeLongitude;
import com.myurldb.ws.GeoPointStruct;


public class GeoCellUtil
{
    private static final Logger log = Logger.getLogger(GeoCellUtil.class.getName());

    private GeoCellUtil() {}

    
    public static CellLatitudeLongitude convertToGeoCell(GeoPointStruct geoPoint)
    {
        return convertToGeoCell(geoPoint, null);
    }
    public static CellLatitudeLongitude convertToGeoCell(GeoPointStruct geoPoint, Integer scale)
    {
        if(geoPoint == null) {
            log.warning("geoPoint is null.");
            return null;
        }

        Double lat = geoPoint.getLatitude();
        Double lng = geoPoint.getLongitude();
        if(lat == null || lng == null) {
            log.warning("Invalid arg, geoPoint. latitude/longitude cannot be null.");
            return null;
        }

        CellLatitudeLongitudeBean geoCell = new CellLatitudeLongitudeBean();

        if(scale == null) {
            scale = ConfigUtil.getLocationGeoCellScale();
        }
        geoCell.setScale(scale);
        
        // Round or floor???
        int latScaled = (int) Math.floor(lat * scale);
        int lngScaled = (int) Math.floor(lng * scale);
        geoCell.setLatitude(latScaled);
        geoCell.setLongitude(lngScaled);
        
        return geoCell;
    }

}
