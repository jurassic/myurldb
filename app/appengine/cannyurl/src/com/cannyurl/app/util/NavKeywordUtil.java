package com.cannyurl.app.util;

import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.logging.Logger;


public class NavKeywordUtil
{
    private static final Logger log = Logger.getLogger(NavKeywordUtil.class.getName());

    // temporary
    private static final int DEFAULT_KEYWORD_MINLENGTH = 5;     // ??? Arbitrary...
    private static final int DEFAULT_KEYWORD_MAXLENGTH = 100;   // ??? Arbitrary...
    private static final int DEFAULT_KEYWORD_UPPERBOUND = 120;  // ??? Arbitrary...  DEFAULT_KEYWORD_MAXLENGTH <= DEFAULT_KEYWORD_UPPERBOUND
    
    
    // Static methods only.
    private NavKeywordUtil() {}

    
    public static String sanitizeKeyword(String phrase)
    {
        return sanitizeKeyword(phrase, DEFAULT_KEYWORD_MAXLENGTH);
    }
    
    
    // TBD:
    // In case of using 72 chars: a-z, A-Z, 0-9, and - $ _ . + ! * , ( )
    // also   ; / ? : @ = &   except maybe "?" ?????  --> 78 chars...
    // ????
    public static String sanitizeKeyword(String phrase, int cutoffLen)
    {
        if(phrase == null) {
            return "";
        }
        //phrase = phrase.toLowerCase();  // Use lower case only. ???
        phrase = phrase.replaceAll("[^a-zA-Z0-9/_\\-]", " ");    // Cf. ShortUrlUtil.getBase64Char().... Also "/" is allowed in a custom token...
        phrase = phrase.trim();
        if(phrase.length() == 0) {
            return "";
        }

        // Slash is allowed in a custom token, but not as a first or second char.
        while(phrase.startsWith("/")) {
            phrase = phrase.substring(1);
            phrase = phrase.trim();
        }
        if(phrase.length() > 1 && phrase.charAt(1) == '/') {
            phrase = phrase.replaceFirst("/", " ");
        }
        
        String[] words = phrase.split("\\s+");
        int totLen = 0;
        StringBuffer sb = new StringBuffer();
        for(int i=0; i<words.length && totLen<=cutoffLen; i++) {
            String word = words[i];
            sb.append(word);
            totLen += word.length();
            if(i<words.length-1 && totLen<=cutoffLen-1) {
                sb.append("-");
            }
        }
        
        String sanitized = sb.toString();
        int len = sanitized.length();
        if(len > DEFAULT_KEYWORD_UPPERBOUND) {
            sanitized = sanitized.substring(0, DEFAULT_KEYWORD_UPPERBOUND);
        }

        return sanitized;
    }


}
