package com.cannyurl.app.util;

import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;


// "Reserved" words cannot be used for usercode (or, as a custom token, TBD), etc.
// TBD: Add all trademarked names? (e.g., cocacola, mac, etc... ???)
// ....
public class ReservedWordRegistry
{
    private static final Logger log = Logger.getLogger(ReservedWordRegistry.class.getName());

    private ReservedWordRegistry() {}


    public static Set<String> TwoLetterWords = new HashSet<String>();
    static {
        TwoLetterWords.add("wa");
        TwoLetterWords.add("wp");
        TwoLetterWords.add("lp");
        TwoLetterWords.add("im");
        // ...
    }

    public static Set<String> ThreeLetterWords = new HashSet<String>();
    static {
        ThreeLetterWords.add("url");
        ThreeLetterWords.add("api");
        ThreeLetterWords.add("web");
        ThreeLetterWords.add("www");
        ThreeLetterWords.add("nav");
        ThreeLetterWords.add("app");
        ThreeLetterWords.add("all");
        ThreeLetterWords.add("new");
        ThreeLetterWords.add("faq");
        ThreeLetterWords.add("doc");
        ThreeLetterWords.add("mac");
        ThreeLetterWords.add("who");
        ThreeLetterWords.add("cam");
        ThreeLetterWords.add("abc");
        ThreeLetterWords.add("nbc");
        ThreeLetterWords.add("cbs");
        ThreeLetterWords.add("fox");
        ThreeLetterWords.add("npr");
        ThreeLetterWords.add("hbo");
        // ...
    }

    public static Set<String> FourLetterWords = new HashSet<String>();
    static {
        FourLetterWords.add("beta");
        FourLetterWords.add("test");
        FourLetterWords.add("home");
        FourLetterWords.add("join");
        FourLetterWords.add("news");
        FourLetterWords.add("mail");
        FourLetterWords.add("docs");
        FourLetterWords.add("site");
        FourLetterWords.add("wiki");
        FourLetterWords.add("live");
        FourLetterWords.add("note");
        FourLetterWords.add("more");
        FourLetterWords.add("ajax");
        FourLetterWords.add("auth");
        FourLetterWords.add("blog");
        FourLetterWords.add("post");
        FourLetterWords.add("user");
        FourLetterWords.add("edit");
        FourLetterWords.add("view");
        FourLetterWords.add("info");
        FourLetterWords.add("help");
        FourLetterWords.add("list");
        FourLetterWords.add("apps");
        FourLetterWords.add("stat");
        FourLetterWords.add("like");
        FourLetterWords.add("link");
        FourLetterWords.add("talk");
        FourLetterWords.add("chat");
        FourLetterWords.add("call");
        FourLetterWords.add("apps");
        FourLetterWords.add("tool");
        FourLetterWords.add("show");
        FourLetterWords.add("ipad");
        FourLetterWords.add("what");
        FourLetterWords.add("when");
        // ...
    }

    public static Set<String> FiveLetterWords = new HashSet<String>();
    static {
        FiveLetterWords.add("alpha");
        FiveLetterWords.add("email");
        FiveLetterWords.add("gmail");
        FiveLetterWords.add("drive");
        FiveLetterWords.add("oauth");
        FiveLetterWords.add("error");
        FiveLetterWords.add("about");
        FiveLetterWords.add("setup");
        FiveLetterWords.add("login");
        FiveLetterWords.add("admin");
        FiveLetterWords.add("token");
        FiveLetterWords.add("flash");
        FiveLetterWords.add("stats");
        FiveLetterWords.add("tweet");
        FiveLetterWords.add("share");
        FiveLetterWords.add("photo");
        FiveLetterWords.add("video");
        FiveLetterWords.add("users");
        FiveLetterWords.add("group");
        FiveLetterWords.add("tools");
        FiveLetterWords.add("watch");
        FiveLetterWords.add("phone");
        FiveLetterWords.add("linux");
        FiveLetterWords.add("which");
        FiveLetterWords.add("where");
        // ...
    }

    public static Set<String> SixLetterWords = new HashSet<String>();
    static {
        SixLetterWords.add("search");
        SixLetterWords.add("mobile");
        SixLetterWords.add("openid");
        SixLetterWords.add("google");
        SixLetterWords.add("signin");
        SixLetterWords.add("logout");
        SixLetterWords.add("verify");
        SixLetterWords.add("manage");
        SixLetterWords.add("client");
        SixLetterWords.add("domain");
        SixLetterWords.add("option");
        SixLetterWords.add("stream");
        SixLetterWords.add("robots");
        SixLetterWords.add("domain");
        SixLetterWords.add("report");
        SixLetterWords.add("groups");
        SixLetterWords.add("iphone");
        SixLetterWords.add("tablet");
        SixLetterWords.add("amazon");
        // ...
    }

    public static Set<String> SevenLetterWords = new HashSet<String>();
    static {
        SevenLetterWords.add("longurl");
        SevenLetterWords.add("tinyurl");
        SevenLetterWords.add("website");
        SevenLetterWords.add("manager");
        SevenLetterWords.add("reports");
        SevenLetterWords.add("shorten");
        SevenLetterWords.add("connect");
        SevenLetterWords.add("signout");
        SevenLetterWords.add("confirm");
        SevenLetterWords.add("twitter");
        SevenLetterWords.add("google+");
        SevenLetterWords.add("contact");
        SevenLetterWords.add("support");
        SevenLetterWords.add("options");
        SevenLetterWords.add("setting");
        SevenLetterWords.add("privacy");
        SevenLetterWords.add("mission");
        SevenLetterWords.add("account");
        SevenLetterWords.add("sitemap");
        SevenLetterWords.add("comment");
        SevenLetterWords.add("message");
        SevenLetterWords.add("keyword");
        SevenLetterWords.add("windows");
        SevenLetterWords.add("android");
        // ...
    }

    public static Set<String> EightLetterWords = new HashSet<String>();
    static {
        EightLetterWords.add("facebook");
        EightLetterWords.add("linkedin");
        EightLetterWords.add("shorturl");
        EightLetterWords.add("callback");
        EightLetterWords.add("authajax");
        EightLetterWords.add("fragment");
        EightLetterWords.add("usercode");
        EightLetterWords.add("username");
        EightLetterWords.add("sitehelp");
        EightLetterWords.add("feedback");
        EightLetterWords.add("favorite");
        EightLetterWords.add("bookmark");
        EightLetterWords.add("shopping");
        EightLetterWords.add("calendar");
        EightLetterWords.add("contacts");
        EightLetterWords.add("settings");
        // ...
    }

    public static Set<String> NineLetterWords = new HashSet<String>();
    static {
        NineLetterWords.add("permanent");
        NineLetterWords.add("temporary");
        NineLetterWords.add("appclient");
        NineLetterWords.add("dashboard");
        // ...
    }

    public static Set<String> TenLetterWords = new HashSet<String>();
    static {
        TenLetterWords.add("googleplus");
        TenLetterWords.add("appsdomain");
        TenLetterWords.add("preference");
        // ...
    }

    public static Set<String> ElevenLetterWords = new HashSet<String>();
    static {
        ElevenLetterWords.add("usersetting");
        // ...
    }

    public static Set<String> TwelveLetterWords = new HashSet<String>();
    static {
        TwelveLetterWords.add("usersettings");
        // ...
    }

    
    public static Set<String> getReservedWordSet(int len)
    {
        switch(len) {
        case 2:
            return TwoLetterWords;
        case 3:
            return ThreeLetterWords;
        case 4:
            return FourLetterWords;
        case 5:
            return FiveLetterWords;
        case 6:
            return SixLetterWords;
        case 7:
            return SevenLetterWords;
        case 8:
            return EightLetterWords;
        case 9:
            return NineLetterWords;
        case 10:
            return TenLetterWords;
        case 11:
            return ElevenLetterWords;
        case 12:
            return TwelveLetterWords;
        default:
            return null;
        }
    }


}
