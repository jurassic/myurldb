package com.cannyurl.app.util;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cannyurl.af.config.Config;
import com.cannyurl.af.service.ShortLinkService;
import com.cannyurl.af.service.manager.ServiceManager;
import com.myurldb.ws.BaseException;
import com.myurldb.ws.ShortLink;


// This class include "critical" algorithm 
// for parsing the request URL.
// ...
// TBD:
// Need to be updated....
// ...
// TBD: Need to match schemes.. (eg. http (long url) -> http (short url), https -> https, etc.)
// ...
public final class ShortLinkUtil
{
    private static final Logger log = Logger.getLogger(ShortLinkUtil.class.getName());
    
    // These should match the settings in web.xml
    public static final String REDIRECT_PATH_VERIFY = "v";
    public static final String REDIRECT_PATH_INFO = "i";
    // ...
    public static final String REDIRECT_PATH_CONFIRM = "c";
    public static final String REDIRECT_PATH_FLASH = "f";
    public static final String REDIRECT_PATH_PERMANENT = "p";
    public static final String REDIRECT_PATH_TEMPORARY = "t";

    // TBD: To be deleted.... ??? (except for domain/path?)
    // Or, at least move these to ConfigUtil ????? why are these here?????
    // In the current design, "domain" really comprises a url including scheme, internet domain, optional port, trailing path segment, etc.... 
    public static final String KEY_CONFIG_SHORTLINK_SCHEME = "cannyurlapp.shortlink.scheme";
    public static final String KEY_CONFIG_SHORTLINK_DOMAIN = "cannyurlapp.shortlink.domain";
    //public static final String KEY_CONFIG_SHORTLINK_PORT = "cannyurlapp.shortlink.port";
    public static final String KEY_CONFIG_SHORTLINK_PATH = "cannyurlapp.shortlink.path";

    private ShortLinkUtil() {}


    
    ////////////////////////////////////////////////////////
    // Temporary. These functions need updates... (below)
    ////////////////////////////////////////////////////////
    
    private static String getInternalDefaultScheme()
    {
        // temporary
        return "http";
    }

    // ????
    private static String getInternalDefaultDomain()
    {
        // temporary
        String hostname = "http://localhost/";  // ???
        try {
            InetAddress addr = InetAddress.getLocalHost();
            //byte[] ipAddr = addr.getAddress();
            hostname = addr.getHostName();
        } catch (UnknownHostException e) {
            log.log(Level.INFO, "Failed to get hostname.");
        }
        return hostname;
    }

//    private static Integer getInternalDefaultPort()
//    {
//        // temporary
//        return null;
//    }

    private static String getInternalDefaultPath()
    {
        // temporary
        return "";
    }
    

    // TBD: Need to "cache" it....
    public static String getSystemDefaultScheme()
    {
        String scheme = Config.getInstance().getString(KEY_CONFIG_SHORTLINK_SCHEME, getInternalDefaultScheme());
        log.log(Level.INFO, "System default scheme = " + scheme);
        return scheme;
    }

    // TBD: Need to "cache" it....
    public static String getSystemDefaultDomain()
    {
        String domain = Config.getInstance().getString(KEY_CONFIG_SHORTLINK_DOMAIN, getInternalDefaultDomain());
        log.log(Level.INFO, "System default domain = " + domain);
        return domain;
    }

//    // TBD: Need to "cache" it....
//    public static Integer getSystemDefaultPort()
//    {
//        Integer port = Config.getInstance().getInteger(KEY_CONFIG_SHORTLINK_PORT, getInternalDefaultPort());
//        log.log(Level.INFO, "System default port = " + port);
//        return port;
//    }

    // TBD: Need to "cache" it....
    public static String getSystemDefaultPath()
    {
        String path = Config.getInstance().getString(KEY_CONFIG_SHORTLINK_PATH, getInternalDefaultPath());
        log.log(Level.INFO, "System default path = " + path);
        return path;
    }


    // TBD: Need to "cache" it....
    public static String getUserDefaultScheme(String user)
    {
        String scheme = null;
        if(user != null && user.trim().length() > 0) {  // TBD: Validate guid?
            // TBD: Get scheme from UserSetting.
            // ...
        }
        return scheme;
    }

    // TBD: Need to "cache" it....
    public static String getUserDefaultDomain(String user)
    {
        String domain = null;
        if(user != null && user.trim().length() > 0) {  // TBD: Validate guid?
            // TBD: Get domain from UserSetting.
            // ...
        }
        return domain;
    }

//    // TBD: Need to "cache" it....
//    public static Integer getUserDefaultPort(String user)
//    {
//        Integer port = null;
//        if(user != null && user.trim().length() > 0) {  // TBD: Validate guid?
//            // TBD: Get Domain/port from UserSetting.
//            // Note: If the user does not have a particular domain value in his Setting,
//            //       do not use the port.
//            // Hack! Domain and port should always be used together.
//        }
//        return port;
//    }

    // TBD: Need to "cache" it....
    public static String getUserDefaultPath(String user)
    {
        String path = null;
        if(user != null && user.trim().length() > 0) {  // TBD: Validate guid?
            // TBD: Get path from UserSetting.
            // ...
        }
        return path;
    }


    // TBD: Need to "cache" it....
    public static String getDefaultScheme(String user)
    {
        String scheme = getUserDefaultScheme(user);
        if(scheme == null || scheme.isEmpty()) {
            scheme = getSystemDefaultScheme();
        }
        return scheme;
    }

    // TBD: Need to "cache" it....
    public static String getDefaultDomain(String user)
    {
        String domain = getUserDefaultDomain(user);
        if(domain == null || domain.isEmpty()) {
            domain = getSystemDefaultDomain();
        }
        return domain;
    }

//    // TBD: Need to "cache" it....
//    public static Integer getDefaultPort(String user)
//    {
//        String port = getUserDefaultPort(user);
//        if(port == null || port.isEmpty()) {
//            port = getSystemDefaultPort();
//        }
//        return port;
//    }

    // TBD: Need to "cache" it....
    public static String getDefaultPath(String user)
    {
        String path = getUserDefaultPath(user);
        if(path == null || path.isEmpty()) {
            path = getSystemDefaultPath();
        }
        return path;
    }

    ////////////////////////////////////////////////////////
    // Temporary. These functions need updates... (above)
    ////////////////////////////////////////////////////////


    
    // "domain" includes scheme, hostname, and possibly certain trailing path. eg "http://safu.co/~username/", etc.
    // "path" is an (optional) single-char path eg, "a", "f", "c", etc...
    // "token", in the current design, cannot include the path separator "/".
    //     (  --> this might have changed. probably, config dependent now.... )
    public static String buildShortUrl(String domain, String path, String token)
    {
        if(domain == null || token == null) {
            log.log(Level.INFO, "Input args are invalid.");
            return null;
        }
        StringBuffer sb = new StringBuffer();
        sb.append(domain.trim());
        if(!domain.trim().endsWith("/")) {
            sb.append("/");
        }
        if(path != null && path.trim().length() > 0) {
            sb.append(path.trim());
            if(!path.trim().endsWith("/")) {
                sb.append("/");
            }
        }
        sb.append(token.trim());
        return sb.toString();
    }


    // Deprecated ????
    private static String buildShortUri(String scheme, String domain, Integer port, String path, String token)
    {
        // TBD: validate?
        //  scheme, domain, and token cannot be null or empty!
        
        StringBuilder sb = new StringBuilder();
        sb.append(scheme);
        sb.append("://");
        sb.append(domain);
        if(port != null && port > 0) {
            if(("http".equals(scheme) && port == 80) || ("https".equals(scheme) && port == 443)) {
                // no need to add the port
            } else {
                sb.append(":");
                sb.append(port);
            }
        }
        sb.append("/");
        if(path != null && path.trim().length() > 0) {
            sb.append(path.trim());
            sb.append("/");
        }
        sb.append(token);
        
        return sb.toString();
    }

    
    
    // TBD:
    // Should we allow a URL like http://a.com/c/~username/xyz, not just http://a.com/~username/c/xyz, etc. ?????
    // ?????
    
    // This method parses the url from the beginning.
    // It recognizes a few predefined patterns as "domain", and then (optional) one-char "path".
    // The rest is defined to be a token.
    // Note that we do support a single letter token (e.g., for custom tokens used with userURL type domains...)
    // but, we do not support a token comprising a single letter + "/" + the rest (e.g., "a/xyz")
    // because this can be parsed as path=a + token=xyz....
    // ...
    // Url arg includes the scheme part, eg "http://"
    // Url does not include query string.
    // Examples: http://a.com/xyz, -> "http://a.com/" + "xyz"
    // Examples: http://a.com/a/xyz, -> "http://a.com/" + "a" + "xyz"
    // Examples: http://a.com/~ret/a/xyz, -> "http://a.com/~ret/" + "a" + "xyz"
    // Examples: http://a.com/@ret/a/xyz/pqr, -> "http://a.com/@ret/" + "a" + "xyz/pqr"
    // Examples: http://a.com/@ret/xyz/pqr, -> "http://a.com/@ret/" + "xyz/pqr"
    // Examples: http://a.com/xyz/pqr/abc, -> "http://a.com/" + "xyz/pqr/abc"
    // [Update: 12/03/11]
    // If the usedomain.userurl config var is set to true, domain is always in the form http://a.com/xyz/.
    // Note username in this case should be longer than 1 char (to avoid parsing ambiguity wrt path)....
    // (Whereas the "@username" format allows a single char username, if necessary...)
    // Examples: http://a.com/xyz, -> Invalid (TBD: Note below)
    // Examples: http://a.com/a/xyz, -> Invalid (ditto)
    // Examples: http://a.com/xyz/pqr, -> "http://a.com/xyz/" + "pqr"
    // Examples: http://a.com/xyz/pqr/abc, -> "http://a.com/xyz/" + "pqr/abc"
    // etc. ...
    // [Update: 1/21/13]
    // A few more new prefixes have been added.
    // Now, we support ~, @, +, -, and !.
    // ...
    public static String[] parseFullUrl(String shortUrl)
    {
        if(log.isLoggable(Level.FINE)) log.fine("parseFullUrl(): shortUrl = " + shortUrl);

        // Need to cache  { shortUrl -> String[] }
        // ...
        
        // TBD: Use a better algorithm???
        // for example, what if custom token contains "/" ???
        // ...
        
        shortUrl = validateUrl(shortUrl);
        if(shortUrl == null) {
            return null;
        }
        
        // TBD:
        // Use regex???
        // Using regex is more efficient???
        // ....
        
        // 0: domain, 1: path, 2: token
        String[] parts = new String[3];

        int length = shortUrl.length();
        int idx1 = shortUrl.indexOf("/", 8);   // "http://x..." -> 8 (more like, 10)
        if(idx1 < 0 || idx1 >= length - 2) {   // Token length >= 2
            log.log(Level.WARNING, "Input url is invalid. No short-url token found.");
            return null;
        }

        // Note: This part is an updated algorithm on 12/03/11.
        // If the usedomain.userurl config var is set to true, then domain is always in the form http://a.com/xyz/.
        //      and, other forms such as http://a.com/~xyz/ or http://a.com/@xyz/ are not allowed.
        boolean useDomainUserURL = ConfigUtil.isUseDomainUserURL();
        if(useDomainUserURL) {
            int idxD = shortUrl.indexOf("/", idx1+3);   //  No single letter username  !!!!!!!!! 
            if(idxD < 0) {
                log.log(Level.WARNING, "Input url is invalid. Username-based domain is not set. Or, token is missing.");
                // TBD: Maybe just continue???? For now, NO. (This can lead to confusion to the users... also, what about http://a.com/a/xyz? <- this will work because we use "idx1+3" rather than "idx1+2")
                // TBD: remove this return statement (without setting idx1 = idxD), if you want some more flexibility. ???
                // TBD: need to revisit this later, when necessary.
                return null;
            } else if(idxD >= length - 1) {   // Token length >= 1
                log.log(Level.WARNING, "Input url is invalid. No short-url token found.");
                return null;                
            }
            // Reset the index idx1
            idx1 = idxD;            
        } else {
            //String username = null;
            //String prefix = shortUrl.substring(idx1+1, idx1+2);
            char prefix = shortUrl.charAt(idx1+1);
            //if("~".equals(prefix) || "@".equals(prefix)) {
            if(prefix == '~' || prefix == '@' || prefix == '+' || prefix == '-' || prefix == '!') {
                int idxU = shortUrl.indexOf("/", idx1+3);   // e.g, /@x/, /@xy/, etc. but not /@/... 
                if(idxU < 0) {
                    log.log(Level.WARNING, "Input url is invalid. Short-url token is invalid.");
                    return null;
                //} else if(idxU >= length - 2) {   // Token length >= 2
                } else if(idxU >= length - 1) {   // Token length >= 1
                    log.log(Level.WARNING, "Input url is invalid. No short-url token found.");
                    return null;                
                }
                //username = shortUrl.substring(idx1+1, idxU);  // Including the prefix...
                // Reset the index idx1
                idx1 = idxU;            
            }
        }
        
        // [0] Domain
        parts[0] = shortUrl.substring(0, idx1+1);  // Including the trailing "/"    // TBD: .toLowerCase() ???   No. the userurl username part can be (potentially) case sensitive.
        
        // [1] "path"  (One letter path segment)
        int idx2 = shortUrl.indexOf("/", idx1+1);
        if(idx2 == idx1+1) {
            // This should not happen..
            log.log(Level.WARNING, "Input url is invalid. Empty path");
            return null;            
        } else if(idx2 == idx1+2) {
            if(idx2 < length - 2) {   // Token length >= 2
                parts[1] = shortUrl.substring(idx1+1, idx1+2);
                // Reset the idx1
                idx1 = idx2;
            } else {
                log.log(Level.WARNING, "Input url is invalid. No short-url token found.");
                return null;                
            }
        } else {
            // No "path" segment.
            parts[1] = null;
        }
        
        // [2] Token.
        parts[2] = shortUrl.substring(idx1+1);  // The rest.
        
        if(log.isLoggable(Level.FINE)) {
            log.fine("parseFullUrl(): shortUrl parsed = " + shortUrl);
            if(parts != null) {
                for(int i=0; i<parts.length; i++) {
                    log.fine("i:" + i + " - " + parts[i]);
                }
            }
        }

        return parts;
    }

    // "path" arg is the divider between the domain part and the rest.
    // Note that this is a convenience method with somewhat limited value...
    // When a request comes in, most of the time, the "path" is absent,
    // or even if it's present, it may not be easy to parse from the UI...
    public static String[] parseFullUrl(String shortUrl, String path)
    {
        if(path == null || path.isEmpty()) {
            return parseFullUrl(shortUrl);
        }
        if(path.length() != 1) {
            log.log(Level.WARNING, "Path should be one letter character: path = " + path);
            return null;
        }
        
        shortUrl = validateUrl(shortUrl);
        if(shortUrl == null) {
            return null;
        }

        String divider = "/" + path + "/";
        int idx = shortUrl.indexOf(divider);
        if(idx < 0) {
            // This cannot happen.
            //return parseFullUrl(shortUrl);  // ????
            return null;
        }
        
        // 0: domain, 1: path, 2: token
        String[] parts = new String[3];

        parts[0] = shortUrl.substring(0, idx+1);   // +1 == Include the trailing "/".    // TBD: .toLowerCase() ???   No.
        parts[1] = path;
        parts[2] = shortUrl.substring(idx+3);
        
        return parts;
    }
    
    private static String validateUrl(String shortUrl)
    {
        if(shortUrl == null || shortUrl.trim().length() < 10) {    // "http://a.b/xy" == 13
            log.log(Level.WARNING, "Input url is invalid.");
            return null;
        }
        shortUrl = shortUrl.trim();
        if(!shortUrl.startsWith("http://")) {   // || "https://" ????
            // TBD:
            log.log(Level.WARNING, "Input url is invalid. Url scheme is incorrect");
            return null;            
        }
        if(shortUrl.endsWith("/")) {
            // TBD:
            // Just remove the traling / and continue ???            
            log.log(Level.WARNING, "Input url is invalid. Url should end with token.");
            return null;            
        }
        return shortUrl;
    }
    
    
    // To be deleted.....
    // This method parses the url from the end.
    // This cannot support a token containing "/"...
    // ...
    // Url arg includes the scheme part, eg "http://"
    // Url does not include query string.
    // Examples: http://a.com/ret/a/xyz, -> "http://a.com/ret/" + "a" + "xyz"
    private static String[] parseFullUrl_Old(String shortUrl)
    {
        // TBD:
        // Need to cache  { shortUrl -> String[] }
        // ...
        
        // TBD: Use a better algorithm???
        // for example, what if custom token contains "/" ???
        // ...
        
        if(shortUrl == null || shortUrl.trim().length() < 10) {    // "http://a.b/xy" == 13
            log.log(Level.WARNING, "Input url is invalid.");
            return null;
        }
        shortUrl = shortUrl.trim();
        if(shortUrl.endsWith("/")) {
            // TBD:
            // Just remove the traling / and continue ???
            
            log.log(Level.WARNING, "Input url is invalid. Url should end with token.");
            return null;            
        }

        // 0: domain, 1: path, 2: token
        String[] parts = new String[3];

        // [2] Token
        int idx2 = shortUrl.lastIndexOf("/");
        if(idx2 > 0 && idx2 < shortUrl.length() - 2) {   // Minimum token length == 2
            parts[2] = shortUrl.substring(idx2 + 1);
        } else {
            log.log(Level.WARNING, "Input url is invalid. Url should end with token.");
            return null;            
        }

        // [1] Path
        shortUrl = shortUrl.substring(0, idx2 + 1);  // Includes the trailing "/".
        int len = shortUrl.length();
        if(shortUrl.charAt(len - 3) == '/') {
            parts[1] = shortUrl.substring(len - 2, len - 1);
            shortUrl = shortUrl.substring(0, len - 2);  // Includes the trailing "/".
        } else {
            parts[1] = null;
        }

        // [0] Domain
        parts[0] = shortUrl;   // The rest

        return parts;
    }

    
    
    
    
}
