package com.cannyurl.app.util;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.cannyurl.app.sassy.SassyUrlUtil;
import com.cannyurl.common.TokenType;
import com.myurldb.ws.BaseException;
import com.myurldb.ws.core.GUID;


///////////////////////////////////////
//
// 78^3 =       474 552
// 78^4 =    37 015 056
// 78^5 = 2 887 174 368
//
// 74^3 =       405 224  --> tiny/3
// 74^4 =    29 986 576
// 74^5 = 2 219 006 624
// 
// 72^3 =       373 248
// 72^4 =    26 873 856
// 72^5 = 1 934 917 632
//
// ------------------------------------
//
// 64^4 =    16 777 216
// 64^5 = 1 073 741 824
//
// 62^4 =    14 775 336  --> short/4
// 62^5 =   916 132 832
// 52^4 =     7 311 616
// 52^5 =   380 204 032
//
//------------------------------------
//
// 36^4 =     1 679 616
// 36^5 =    60 466 176  --> medium/5
// 36^6 = 2 176 782 336
//
// 34^4 =     1 336 336
// 34^5 =    45 435 424
// 34^6 = 1 544 804 416
// 32^4 =     1 048 576
// 32^5 =    33 554 432
// 32^6 = 1 073 742 824
//
//------------------------------------
//
// 26^5 =    11 881 376
// 26^6 =   308 915 776  --> long/6
// 26^7 = 8 031 810 176
//
//------------------------------------
//
// 12^6 =     2 985 984
// 12^7 =    35 831 808
// 12^8 =   429 981 696
//
// 10^7 =    10 000 000  --> digit/7
// 10^8 =   100 000 000
// 10^9 = 1 000 000 000
//
//------------------------------------
//
//
///////////////////////////////////////


// Methods for generating short url tokens.
// More like "TokenUtil".
// Methods are divided into ShortUrlUtil and TokenUtil due to the dependencies on ConfigUti.
// Note:
// http://www.ietf.org/rfc/rfc3986.txt
public final class ShortUrlUtil
{
    private static final Logger log = Logger.getLogger(ShortUrlUtil.class.getName());

    // Static methods only.
    private ShortUrlUtil() {}


    // TBD:
    // Better algorithm in case the base is an exponent of 2, e.g., 64, 32, etc. ???
    // ????
    
    
    // TBD:
    // 72 chars: a-z, A-Z, 0-9, and - $ _ . + ! * , ( )
    // also   ; / ? : @ = &   in certain cases  --> up to 79 chars...
    // ...
    
    
    // 12 chars in addition to the 62 alphanumeric characters.
    // Note: '@' and '~' cannot be the first char of a token (in our design),
    //       and '/' cannot be the first or second char (or, the last char?).
    // ';', '#', and '?' may not be allowed due to URL parsing....
    private static final char[] SpecialChars = new char[]{'-', '_', '.', '$', '+', '!', '*', ',', '(', ')', ':', '=', '&'};
    private static char getBase75Char(int i)
    {
        char c;
        if(i >= 0 && i < 26) {
            c = (char) (i + 65);        // A == 65
        } else if(i >= 26 && i < 52) {
            c = (char) (i - 26 + 97);   // a == 97
        } else if(i >= 52 && i < 62) {
            c = (char) (i - 52 + 48);   // 0 == 48
        } else {  // if(i >= 62 && i < 75) {
            c = SpecialChars[i - 62];   // Note if i>=75 error!, but this should not happen....
        }
        return c;
    }
    private static int getBase75Int(char c)
    {
        int i;
        for(int k=0; k<SpecialChars.length; k++) {
            if(SpecialChars[k] == c) {
                return k + 62;
            }
        }
        int chi = (int) c;
        if(chi >= 97) {
            i = chi - 97 + 26;
        } else if(chi >= 65) {
            i = chi - 65;
        } else if(chi >= 48) {
            i = chi - 48 + 52;
        } else {
            // ????
            i = 0;
        }
        return i;
    }
    private static char getBase74Char(int i)
    {
        char c;
        if(i >= 0 && i < 26) {
            c = (char) (i + 65);        // A == 65
        } else if(i >= 26 && i < 52) {
            c = (char) (i - 26 + 97);   // a == 97
        } else if(i >= 52 && i < 62) {
            c = (char) (i - 52 + 48);   // 0 == 48
        } else {  // if(i >= 62 && i < 74) {
            c = SpecialChars[i - 62];   // Note if i>=74 error!, but this should not happen....
        }
        return c;
    }
    private static int getBase74Int(char c)
    {
        int i;
        for(int k=0; k<SpecialChars.length; k++) {
            if(SpecialChars[k] == c) {
                return k + 62;
            }
        }
        int chi = (int) c;
        if(chi >= 97) {
            i = chi - 97 + 26;
        } else if(chi >= 65) {
            i = chi - 65;
        } else if(chi >= 48) {
            i = chi - 48 + 52;
        } else {
            // ????
            i = 0;
        }
        return i;
    }
    private static char getBase72Char(int i)
    {
        char c;
        if(i >= 0 && i < 26) {
            c = (char) (i + 65);        // A == 65
        } else if(i >= 26 && i < 52) {
            c = (char) (i - 26 + 97);   // a == 97
        } else if(i >= 52 && i < 62) {
            c = (char) (i - 52 + 48);   // 0 == 48
        } else {  // if(i >= 62 && i < 72) {
            c = SpecialChars[i - 62];   // Note if i>=72 error!, but this should not happen....
        }
        return c;
    }
    private static int getBase72Int(char c)
    {
        int i;
        for(int k=0; k<SpecialChars.length; k++) {
            if(SpecialChars[k] == c) {
                return k + 62;
            }
        }
        int chi = (int) c;
        if(chi >= 97) {
            i = chi - 97 + 26;
        } else if(chi >= 65) {
            i = chi - 65;
        } else if(chi >= 48) {
            i = chi - 48 + 52;
        } else {
            // ????
            i = 0;
        }
        return i;
    }

    
    // Alphabets + digits + "_" + "-"
    private static char getBase64Char(int i)
    {
        char c;
        if(i >= 0 && i < 26) {
            c = (char) (i + 65);        // A == 65
        } else if(i >= 26 && i < 52) {
            c = (char) (i - 26 + 97);   // a == 97
        } else if(i >= 52 && i < 62) {
            c = (char) (i - 52 + 48);   // 0 == 48
        } else { // 62 <= i < 64
            if(i == 62) {
                c = '-';
            } else {
                c = '_';
            }
        }
        return c;
    }
    private static int getBase64Int(char c)
    {
        int i;
        for(int k=0; k<SpecialChars.length; k++) {
            if(SpecialChars[k] == c) {
                return k + 62;
            }
        }
        int chi = (int) c;
        if(chi >= 97) {
            i = chi - 97 + 26;
        } else if(chi >= 65) {
            i = chi - 65;
        } else if(chi >= 48) {
            i = chi - 48 + 52;
        } else {
            // ????
            i = 0;
        }
        return i;
    }

    // Alphabets + digits
    private static char getBase62Char(int i)
    {
        char c;
        if(i >= 0 && i < 26) {
            c = (char) (i + 65);        // A == 65
        } else if(i >= 26 && i < 52) {
            c = (char) (i - 26 + 97);   // a == 97
        } else {  // if(i >= 52 && i < 62) {
            c = (char) (i - 52 + 48);   // 0 == 48
        }
        return c;
    }
    private static int getBase62Int(char c)
    {
        int i;
        int chi = (int) c;
        if(chi >= 97) {
            i = chi - 97 + 26;
        } else if(chi >= 65) {
            i = chi - 65;
        } else if(chi >= 48) {
            i = chi - 48 + 52;
        } else {
            // ????
            i = 0;
        }
        return i;
    }

    // alphabets only.
    private static char getBase52Char(int i)
    {
        char c;
        if(i >= 0 && i < 26) {
            c = (char) (i + 65);        // A == 65
        } else {  // if(i >= 26 && i < 52) {
            c = (char) (i - 26 + 97);   // a == 97
        }
        return c;
    }
    private static int getBase52Int(char c)
    {
        int i;
        int chi = (int) c;
        if(chi >= 97) {
            i = chi - 97 + 26;
        } else if(chi >= 65) {
            i = chi - 65;
        } else {
            // ????
            i = 0;
        }
        return i;
    }

    // lower-case alphabets + digits
    private static char getBase36Char(int i)
    {
        char c;
        if(i >= 0 && i < 26) {
            c = (char) (i + 97);   // a == 97
        } else {  // if(i >= 26 && i < 36) {
            c = (char) (i - 26 + 48);   // 0 == 48
        }
        return c;
    }
    private static int getBase36Int(char c)
    {
        int i;
        int chi = (int) c;
        if(chi >= 97) {
            i = chi - 97;
        } else if(chi >= 48) {
            i = chi - 48 + 26;
        } else {
            // ????
            i = 0;
        }
        return i;
    }

    // lower-case alphabets + digits (excluding l/1 and o/0)
    private static char getBase34Char(int i)
    {
        char c;
        
//        // [1] Exclude alphabets 1 and o
//        if(i >= 0 && i < 10) {
//            c = (char) (i + 48);        // 0 == 48
//        } else if(i >= 10 && i < 21) {
//            c = (char) (i - 10 + 97);   // a == 97
//        } else if(i >= 21 && i < 23) {  // exclude L.
//            c = (char) (i - 21 + 109);  // m == 109
//        } else {                        // exclude O
//            c = (char) (i - 23 + 112);  // p == 112
//        }

        // [2] Exclude digits 0 and 1
        if(i >= 0 && i < 8) {
            c = (char) (i + 50);        // 2 == 50
        } else {  // if(i >= 8 && i < 34) {
            c = (char) (i - 8 + 97);    // a == 97
        }

        return c;
    }
    private static int getBase34Int(char c)
    {
        int i;
        int chi = (int) c;
        if(chi >= 97) {
            i = chi - 97 + 8;
        } else if(chi >= 50) {
            i = chi - 50;
        } else {
            // ????
            i = 0;
        }
        return i;
    }

    // lower-case alphabets + digits excluding alphabets l, o, y, and z
    private static char getBase32Char(int i)
    {
        char c;
        if(i >= 0 && i < 10) {
            c = (char) (i + 48);        // 0 == 48
        } else if(i >= 10 && i < 21) {
            c = (char) (i - 10 + 97);   // a == 97
        } else if(i >= 21 && i < 23) {  // exclude L.
            c = (char) (i - 21 + 109);  // m == 109
        } else {                        // exclude O, Y, Z.
            c = (char) (i - 23 + 112);  // p == 112
        }
        return c;
    }
    private static int getBase32Int(char c)
    {
        int i;
        int chi = (int) c;
        if(chi >= 112) {
            i = chi - 112 + 23;
        } else if(chi >= 109) {
            i = chi - 109 + 21;
        } else if(chi >= 97) {
            i = chi - 97 + 10;
        } else if(chi >= 48) {
            i = chi - 48;
        } else {
            // ????
            i = 0;
        }
        return i;
    }

    // lower-case alphabets only
    private static char getBase26Char(int i)
    {
        char c;
        // c = (char) (i + 97);  // a == 97
        c = (char) ((i % 26) + 97);  // a == 97   // ????
        return c;
    }
    private static int getBase26Int(char c)
    {
        int i;
        int chi = (int) c;
        // if(chi >= 97) {
        if(chi >= 97 && chi < 123) {   // ???
            i = chi - 97;
        } else {
            // ????
            i = 0;
        }
        return i;
    }

    // Digits + "_" + "-"
    private static char getBase12Char(int i)
    {
        char c;
        
        if(i >= 0 && i < 10) {
            c = (char) (i + 48);        // 0 == 48
        } else {
            if(i == 10) {
                c = '-';
            } else {
                c = '_';
            }
        }

        return c;
    }
    private static int getBase12Int(char c)
    {
        int i;
        for(int k=0; k<SpecialChars.length; k++) {
            if(SpecialChars[k] == c) {
                return k + 10;
            }
        }
        int chi = (int) c;
        // if(chi >= 48) {
        if(chi >= 48 && chi < 58) {   // ???
            i = chi - 48;
        } else {
            // ????
            i = 0;
        }
        return i;
    }

    // digits only
    private static char getBase10Char(int i)
    {
        char c;
        // c = (char) (i + 48);  // 0 == 48
        c = (char) ((i % 10) + 48);  // 0 == 48   // ????
        return c;
    }
    private static int getBase10Int(char c)
    {
        int i;
        int chi = (int) c;
        // if(chi >= 48) {
        if(chi >= 48 && chi < 58) {   // ???
            i = chi - 48;
        } else {
            // ????
            i = 0;
        }
        return i;
    }



    // vowels only.
    private static char getBase5Char(int i)
    {
        char c;
        // switch(i) {
        switch(i % 5) {    // ???
        case 0:
        default:  // ???
            c = 'a';
            break;
        case 1:
            c = 'e';
            break;
        case 2:
            c = 'i';
            break;
        case 3:
            c = 'o';
            break;
        case 4:
            c = 'u';
            break;
        }
        return c;
    }
    private static int getBase5Int(char c)
    {
        int i;
        switch(c) {
        case 'a':
        default:  // ???
            i = 0;
            break;
        case 'e':
            i = 1;
            break;
        case 'i':
            i = 2;
            break;
        case 'o':
            i = 3;
            break;
        case 'u':
            i = 4;
            break;
        }
        return i;
    }

    // abc only.
    private static char getBase3Char(int i)
    {
        char c;
        // switch(i) {
        switch(i % 3) {   // ????
        case 0:
        default:  // ???
            c = 'a';
            break;
        case 1:
            c = 'b';
            break;
        case 2:
            c = 'c';
            break;
        }
        return c;
    }
    private static int getBase3Int(char c)
    {
        int i;
        switch(c) {
        case 'a':
        default:  // ???
            i = 0;
            break;
        case 'b':
            i = 1;
            break;
        case 'c':
            i = 2;
            break;
        }
        return i;
    }

    // 0/1 only
    private static char getBase2Char(int i)
    {
        char c;
        // switch(i) {
        switch(i % 2) {    // ???
        case 0:
        default:  // ???
            c = '0';
            break;
        case 1:
            c = '1';
            break;
        }
        return c;
    }
    private static int getBase2Int(char c)
    {
        int i;
        switch(c) {
        case '0':
        default:  // ???
            i = 0;
            break;
        case '1':
            i = 1;
            break;
        }
        return i;
    }
    
    
    
    
    public static String generateToken(String type)
    {
        if(TokenType.TYPE_TINY.equals(type)) {
            return generateTinyUriToken();
        } else if(TokenType.TYPE_SHORT.equals(type)) {
            return generateShortUriToken();
        } else if(TokenType.TYPE_MEDIUM.equals(type)) {
            return generateMediumUriToken();
        } else if(TokenType.TYPE_LONG.equals(type)) {
            return generateLongUriToken();
        } else if(TokenType.TYPE_DIGITS.equals(type)) {
            return generateDigitUriToken();
        } else if(TokenType.TYPE_VOWELS.equals(type)) {
            return generateVowelUriToken();
        } else if(TokenType.TYPE_ABC.equals(type)) {
            return generateAbcUriToken();
        } else if(TokenType.TYPE_BINARY.equals(type)) {
            return generateBinaryUriToken();
        } else if(TokenType.TYPE_SASSY.equals(type)) {
            String sassyTokenType = ConfigUtil.getSystemDefaultSassyTokenType();
            return SassyUrlUtil.generateSassyUrlToken(sassyTokenType);
        } else {
            log.log(Level.WARNING, "ShortUri token cannot be automatically generated for type = " + type);
            return null;
        }
    }

    public static String generateToken(String type, int length)
    {
        if(TokenType.TYPE_TINY.equals(type)) {
            return generateTinyUriToken(length);
        } else if(TokenType.TYPE_SHORT.equals(type)) {
            return generateShortUriToken(length);
        } else if(TokenType.TYPE_MEDIUM.equals(type)) {
            return generateMediumUriToken(length);
        } else if(TokenType.TYPE_LONG.equals(type)) {
            return generateLongUriToken(length);
        } else if(TokenType.TYPE_DIGITS.equals(type)) {
            return generateDigitUriToken(length);
        } else if(TokenType.TYPE_VOWELS.equals(type)) {
            return generateVowelUriToken(length);
        } else if(TokenType.TYPE_ABC.equals(type)) {
            return generateAbcUriToken(length);
        } else if(TokenType.TYPE_BINARY.equals(type)) {
            return generateBinaryUriToken(length);
        } else if(TokenType.TYPE_SASSY.equals(type)) {
            String sassyTokenType = ConfigUtil.getSystemDefaultSassyTokenType();
            return SassyUrlUtil.generateSassyUrlToken(sassyTokenType, length);
        } else {
            log.log(Level.WARNING, "ShortUri token cannot be automatically generated for type = " + type);
            return null;
        }
    }

    
    // Note that we different "parts" of a guid in generating tokens for short-medium-long-digit types.
    // This is because a single/same guid may be used to generate multiple tokens, in certain cases.
    // This makes the four tokens "unrelated" ...
    // ...
    
    
    public static String generateTinyUriToken()
    {
        String guid = GUID.generate();
        return generateTinyUriToken(guid);
    }
    public static String generateTinyUriToken(int length)
    {
        String guid = GUID.generate();
        return generateTinyUriToken(guid, length);
    }
    public static String generateTinyUriToken(String guid)
    {
        return generateTinyUriToken(guid, ConfigUtil.getTinyTokenLength());
    }
    public static String generateTinyUriToken(String guid, int length)
    {
        // Guid format: 8-4-4-4-12.
        String firstpart = guid.substring(3, 8) + guid.substring(9, 13) + guid.substring(15, 18);  // First twelve chars from the fourth pos. Excludes the 14th char.
        long num48 = Long.parseLong(firstpart, 16);                       // 48 bits.
        long base = num48;

        StringBuilder sb = new StringBuilder();
//        for(int j=0; j<length; j++) {
//            long tmp = (long) (base / 72L);
//            int rem = (int) (base - tmp * 72L);
//            base = tmp;
//            sb.append(getBase72Char(rem));
//        }
//        for(int j=0; j<length; j++) {
//            long tmp = (long) (base / 74L);
//            int rem = (int) (base - tmp * 74L);
//            base = tmp;
//            sb.append(getBase74Char(rem));
//        }
        for(int j=0; j<length; j++) {
            long tmp = (long) (base / 75L);
            int rem = (int) (base - tmp * 75L);
            base = tmp;
            sb.append(getBase75Char(rem));
        }

        String tinyUriToken = sb.reverse().toString();
        return tinyUriToken;
    }

    private static String generateTinyUriToken(long base, int length)
    {
        StringBuilder sb = new StringBuilder();
        for(int j=0; j<length; j++) {
            long tmp = (long) (base / 75L);
            int rem = (int) (base - tmp * 75L);
            base = tmp;
            sb.append(getBase75Char(rem));
        }
        String tinyUriToken = sb.reverse().toString();
        return tinyUriToken;
    }
    public static String generateFirstTinyUriToken(int length)
    {
        return generateTinyUriToken(0L, length);
    }
    public static String generateNextTinyUriToken(String referenceToken, int length)
    {
        long base = 0L;
        int isAllLastChars = 1;
        int tokLen = referenceToken.length();
        for(int k=0; k<tokLen; k++) {
            char c = referenceToken.charAt(k);
            int i = getBase75Int(c);
            base = i + (base * 75);
            isAllLastChars *= (c == '&' ? 1 : 0);
        }
        long nextBase = base + 1;
        if(isAllLastChars == 1) {
            length += 1;
            nextBase = 0L;   // ???
        }
        return generateTinyUriToken(nextBase, length);
    }

    
    public static String generateShortUriToken()
    {
        String guid = GUID.generate();
        return generateShortUriToken(guid);
    }
    public static String generateShortUriToken(int length)
    {
        String guid = GUID.generate();
        return generateShortUriToken(guid, length);
    }
    public static String generateShortUriToken(String guid)
    {
        return generateShortUriToken(guid, ConfigUtil.getShortTokenLength());
    }
    public static String generateShortUriToken(String guid, int length)
    {
        // Guid format: 8-4-4-4-12.
        String firstpart = guid.substring(0, 8) + guid.substring(9, 13);  // First twelve chars.
        long num48 = Long.parseLong(firstpart, 16);                       // 48 bits.
        long base = num48;

        StringBuilder sb = new StringBuilder();
//        for(int j=0; j<6; j++) {
//            long tmp = (long) (base / 64L);
//            int rem = (int) (base - tmp * 64L);
//            base = tmp;
//            sb.append(getBase64Char(rem));
//        }
        for(int j=0; j<length; j++) {
            long tmp = (long) (base / 62L);
            int rem = (int) (base - tmp * 62L);
            base = tmp;
            sb.append(getBase62Char(rem));
        }

        String shortUriToken = sb.reverse().toString();
        return shortUriToken;
    }

    private static String generateShortUriToken(long base, int length)
    {
        StringBuilder sb = new StringBuilder();
        for(int j=0; j<length; j++) {
            long tmp = (long) (base / 62L);
            int rem = (int) (base - tmp * 62L);
            base = tmp;
            sb.append(getBase62Char(rem));
        }
        String shortUriToken = sb.reverse().toString();
        return shortUriToken;
    }
    public static String generateFirstShortUriToken(int length)
    {
        return generateShortUriToken(0L, length);
    }
    public static String generateNextShortUriToken(String referenceToken, int length)
    {
        long base = 0L;
        int isAllLastChars = 1;
        int tokLen = referenceToken.length();
        for(int k=0; k<tokLen; k++) {
            char c = referenceToken.charAt(k);
            int i = getBase62Int(c);
            base = i + (base * 62);
            isAllLastChars *= (c == '9' ? 1 : 0);
        }
        long nextBase = base + 1;
        if(isAllLastChars == 1) {
            length += 1;
            nextBase = 0L;   // ???
        }
        return generateShortUriToken(nextBase, length);
    }

    
    public static String generateMediumUriToken()
    {
        String guid = GUID.generate();
        return generateMediumUriToken(guid);
    }
    public static String generateMediumUriToken(int length)
    {
        String guid = GUID.generate();
        return generateMediumUriToken(guid, length);
    }
    public static String generateMediumUriToken(String guid)
    {
        return generateMediumUriToken(guid, ConfigUtil.getMediumTokenLength());
    }
    public static String generateMediumUriToken(String guid, int length)
    {
        int len = guid.length();
        String lastpart = guid.substring(len-12, len);  // Last twelve chars.
        long num48 = Long.parseLong(lastpart, 16);      // 48 bits.
        long base = num48;

        StringBuilder sb = new StringBuilder();
//        for(int j=0; j<9; j++) {
//            long tmp = (long) (base / 32L);
//            int rem = (int) (base - tmp * 32L);
//            base = tmp;
//            sb.append(getBase32Char(rem));
//        }
        for(int j=0; j<length; j++) {
            long tmp = (long) (base / 36L);
            int rem = (int) (base - tmp * 36L);
            base = tmp;
            sb.append(getBase36Char(rem));
        }
       
        String mediumUriToken = sb.reverse().toString();
        return mediumUriToken;
    }

    private static String generateMediumUriToken(long base, int length)
    {
        StringBuilder sb = new StringBuilder();
        for(int j=0; j<length; j++) {
            long tmp = (long) (base / 36L);
            int rem = (int) (base - tmp * 36L);
            base = tmp;
            sb.append(getBase36Char(rem));
        }
        String mediumUriToken = sb.reverse().toString();
        return mediumUriToken;
    }
    public static String generateFirstMediumUriToken(int length)
    {
        return generateMediumUriToken(0L, length);
    }
    public static String generateNextMediumUriToken(String referenceToken, int length)
    {
        long base = 0L;
        int isAllLastChars = 1;
        int tokLen = referenceToken.length();
        for(int k=0; k<tokLen; k++) {
            char c = referenceToken.charAt(k);
            int i = getBase36Int(c);
            base = i + (base * 36);
            isAllLastChars *= (c == '9' ? 1 : 0);
        }
        long nextBase = base + 1;
        if(isAllLastChars == 1) {
            length += 1;
            nextBase = 0L;   // ???
        }
        return generateMediumUriToken(nextBase, length);
    }


    public static String generateLongUriToken()
    {
        String guid = GUID.generate();
        return generateLongUriToken(guid);
    }
    public static String generateLongUriToken(int length)
    {
        String guid = GUID.generate();
        return generateLongUriToken(guid, length);
    }
    public static String generateLongUriToken(String guid)
    {
        return generateLongUriToken(guid, ConfigUtil.getLongTokenLength());
    }
    public static String generateLongUriToken(String guid, int length)
    {
        String midpart = guid.substring(19, 23) + guid.substring(24, 32);  // Middle twelve chars 
        long num48 = Long.parseLong(midpart, 16);       // 48 bits.
        long base = num48;

        StringBuilder sb = new StringBuilder();
        for(int j=0; j<length; j++) {
            long tmp = (long) (base / 26L);
            int rem = (int) (base - tmp * 26L);
            base = tmp;
            sb.append(getBase26Char(rem));
        }

        String longUriToken = sb.reverse().toString();
        return longUriToken;
    }

    private static String generateLongUriToken(long base, int length)
    {
        StringBuilder sb = new StringBuilder();
        for(int j=0; j<length; j++) {
            long tmp = (long) (base / 26L);
            int rem = (int) (base - tmp * 26L);
            base = tmp;
            sb.append(getBase26Char(rem));
        }
        String longUriToken = sb.reverse().toString();
        return longUriToken;
    }
    public static String generateFirstLongUriToken(int length)
    {
        return generateLongUriToken(0L, length);
    }
    public static String generateNextLongUriToken(String referenceToken, int length)
    {
        long base = 0L;
        int isAllLastChars = 1;
        int tokLen = referenceToken.length();
        for(int k=0; k<tokLen; k++) {
            char c = referenceToken.charAt(k);
            int i = getBase26Int(c);
            base = i + (base * 26);
            isAllLastChars *= (c == 'z' ? 1 : 0);
        }
        long nextBase = base + 1;
        if(isAllLastChars == 1) {
            length += 1;
            nextBase = 0L;   // ???
        }
        return generateLongUriToken(nextBase, length);
    }


    public static String generateDigitUriToken()
    {
        String guid = GUID.generate();
        return generateDigitUriToken(guid);
    }
    public static String generateDigitUriToken(int length)
    {
        String guid = GUID.generate();
        return generateDigitUriToken(guid, length);
    }
    public static String generateDigitUriToken(String guid)
    {
        return generateDigitUriToken(guid, ConfigUtil.getDigitTokenLength());
    }
    public static String generateDigitUriToken(String guid, int length)
    {
        String midpart = guid.substring(15, 18) + guid.substring(19, 23) + guid.substring(24, 29);  // Middle twelve chars excluding the 14th char.
        long num48 = Long.parseLong(midpart, 16);       // 48 bits.
        long base = num48;

        StringBuilder sb = new StringBuilder();
        for(int j=0; j<length; j++) {
//            long tmp = (long) (base / 12L);
//            int rem = (int) (base - tmp * 12L);
//            base = tmp;
//            sb.append(getBase12Char(rem));
            long tmp = (long) (base / 10L);
            int rem = (int) (base - tmp * 10L);
            base = tmp;
            sb.append(getBase10Char(rem));
        }
       
        String digitUriToken = sb.reverse().toString();
        return digitUriToken;
    }

    private static String generateDigitUriToken(long base, int length)
    {
        StringBuilder sb = new StringBuilder();
        for(int j=0; j<length; j++) {
            long tmp = (long) (base / 10L);
            int rem = (int) (base - tmp * 10L);
            base = tmp;
            sb.append(getBase10Char(rem));
        }
        String digitUriToken = sb.reverse().toString();
        return digitUriToken;
    }
    public static String generateFirstDigitUriToken(int length)
    {
        return generateDigitUriToken(0L, length);
    }
    public static String generateNextDigitUriToken(String referenceToken, int length)
    {
        long base = 0L;
        int isAllLastChars = 1;
        int tokLen = referenceToken.length();
        for(int k=0; k<tokLen; k++) {
            char c = referenceToken.charAt(k);
            int i = getBase10Int(c);
            base = i + (base * 10);
            isAllLastChars *= (c == '9' ? 1 : 0);
        }
        long nextBase = base + 1;
        if(isAllLastChars == 1) {
            length += 1;
            nextBase = 0L;   // ???
        }
        return generateDigitUriToken(nextBase, length);
    }
    


    public static String generateVowelUriToken()
    {
        String guid = GUID.generate();
        return generateVowelUriToken(guid);
    }
    public static String generateVowelUriToken(int length)
    {
        String guid = GUID.generate();
        return generateVowelUriToken(guid, length);
    }
    public static String generateVowelUriToken(String guid)
    {
        return generateVowelUriToken(guid, ConfigUtil.getVowelTokenLength());
    }
    public static String generateVowelUriToken(String guid, int length)
    {
        String midpart = guid.substring(15, 18) + guid.substring(19, 23) + guid.substring(24, 29);  // Middle twelve chars excluding the 14th char.
        long num48 = Long.parseLong(midpart, 16);       // 48 bits.
        long base = num48;

        StringBuilder sb = new StringBuilder();
        for(int j=0; j<length; j++) {
            long tmp = (long) (base / 5L);
            int rem = (int) (base - tmp * 5L);
            base = tmp;
            sb.append(getBase5Char(rem));
        }
       
        String digitUriToken = sb.reverse().toString();
        return digitUriToken;
    }

    private static String generateVowelUriToken(long base, int length)
    {
        StringBuilder sb = new StringBuilder();
        for(int j=0; j<length; j++) {
            long tmp = (long) (base / 5L);
            int rem = (int) (base - tmp * 5L);
            base = tmp;
            sb.append(getBase5Char(rem));
        }
        String digitUriToken = sb.reverse().toString();
        return digitUriToken;
    }
    public static String generateFirstVowelUriToken(int length)
    {
        return generateVowelUriToken(0L, length);
    }
    public static String generateNextVowelUriToken(String referenceToken, int length)
    {
        long base = 0L;
        int isAllLastChars = 1;
        int tokLen = referenceToken.length();
        for(int k=0; k<tokLen; k++) {
            char c = referenceToken.charAt(k);
            int i = getBase5Int(c);
            base = i + (base * 5);
            isAllLastChars *= (c == 'u' ? 1 : 0);
        }
        long nextBase = base + 1;
        if(isAllLastChars == 1) {
            length += 1;
            nextBase = 0L;   // ???
        }
        return generateVowelUriToken(nextBase, length);
    }


    public static String generateAbcUriToken()
    {
        String guid = GUID.generate();
        return generateAbcUriToken(guid);
    }
    public static String generateAbcUriToken(int length)
    {
        String guid = GUID.generate();
        return generateAbcUriToken(guid, length);
    }
    public static String generateAbcUriToken(String guid)
    {
        return generateAbcUriToken(guid, ConfigUtil.getAbcTokenLength());
    }
    public static String generateAbcUriToken(String guid, int length)
    {
        String midpart = guid.substring(15, 18) + guid.substring(19, 23) + guid.substring(24, 29);  // Middle twelve chars excluding the 14th char.
        long num48 = Long.parseLong(midpart, 16);       // 48 bits.
        long base = num48;

        StringBuilder sb = new StringBuilder();
        for(int j=0; j<length; j++) {
            long tmp = (long) (base / 3L);
            int rem = (int) (base - tmp * 3L);
            base = tmp;
            sb.append(getBase3Char(rem));
        }
       
        String digitUriToken = sb.reverse().toString();
        return digitUriToken;
    }

    private static String generateAbcUriToken(long base, int length)
    {
        StringBuilder sb = new StringBuilder();
        for(int j=0; j<length; j++) {
            long tmp = (long) (base / 3L);
            int rem = (int) (base - tmp * 3L);
            base = tmp;
            sb.append(getBase3Char(rem));
        }
        String digitUriToken = sb.reverse().toString();
        return digitUriToken;
    }
    public static String generateFirstAbcUriToken(int length)
    {
        return generateAbcUriToken(0L, length);
    }
    public static String generateNextAbcUriToken(String referenceToken, int length)
    {
        long base = 0L;
        int isAllLastChars = 1;
        int tokLen = referenceToken.length();
        for(int k=0; k<tokLen; k++) {
            char c = referenceToken.charAt(k);
            int i = getBase3Int(c);
            base = i + (base * 3);
            isAllLastChars *= (c == 'c' ? 1 : 0);
        }
        long nextBase = base + 1;
        if(isAllLastChars == 1) {
            length += 1;
            nextBase = 0L;   // ???
        }
        return generateAbcUriToken(nextBase, length);
    }


    public static String generateBinaryUriToken()
    {
        String guid = GUID.generate();
        return generateBinaryUriToken(guid);
    }
    public static String generateBinaryUriToken(int length)
    {
        String guid = GUID.generate();
        return generateBinaryUriToken(guid, length);
    }
    public static String generateBinaryUriToken(String guid)
    {
        return generateBinaryUriToken(guid, ConfigUtil.getBinaryTokenLength());
    }
    public static String generateBinaryUriToken(String guid, int length)
    {
        String midpart = guid.substring(15, 18) + guid.substring(19, 23) + guid.substring(24, 29);  // Middle twelve chars excluding the 14th char.
        long num48 = Long.parseLong(midpart, 16);       // 48 bits.
        long base = num48;

        StringBuilder sb = new StringBuilder();
        for(int j=0; j<length; j++) {
            long tmp = (long) (base / 2L);
            int rem = (int) (base - tmp * 2L);
            base = tmp;
            sb.append(getBase2Char(rem));
        }
       
        String digitUriToken = sb.reverse().toString();
        return digitUriToken;
    }

    private static String generateBinaryUriToken(long base, int length)
    {
        StringBuilder sb = new StringBuilder();
        for(int j=0; j<length; j++) {
            long tmp = (long) (base / 2L);
            int rem = (int) (base - tmp * 2L);
            base = tmp;
            sb.append(getBase2Char(rem));
        }
        String digitUriToken = sb.reverse().toString();
        return digitUriToken;
    }
    public static String generateFirstBinaryUriToken(int length)
    {
        return generateBinaryUriToken(0L, length);
    }
    public static String generateNextBinaryUriToken(String referenceToken, int length)
    {
        long base = 0L;
        int isAllLastChars = 1;
        int tokLen = referenceToken.length();
        for(int k=0; k<tokLen; k++) {
            char c = referenceToken.charAt(k);
            int i = getBase2Int(c);
            base = i + (base * 2);
            isAllLastChars *= (c == '1' ? 1 : 0);
        }
        long nextBase = base + 1;
        if(isAllLastChars == 1) {
            length += 1;
            nextBase = 0L;   // ???
        }
        return generateBinaryUriToken(nextBase, length);
    }


    
    
    

//    // TBD: Due to the overlapping ranges (which will change over time),
//    //      it's not possible to deduce the shortUri type based on their length alone.
//    // This function should be deleted.
//    private static String getShortUriType(String uri)
//    {
//        if(uri == null) {
//            return null;  // ????
//        }
//        int len = uri.length();
//        if(len < ConfigUtil.getMediumTokenLength()) {
//            return "short";                // TBD: Use constant....
//        } else if(len < ConfigUtil.getLongTokenLength()) {
//            return "medium";
//        } else if(len < ConfigUtil.getDigitTokenLength()) {
//            return "medium";
//        } else {
//            return "long";
//        }
//    }
    
    
    
    // Note:
    // If the sequential generation algorithm is used...
    // tokenType should be generally fixed for each user (or, domain)...
    // Otherwise, the "sequence" may be constantly changing (and, may end up generating the same/previously used tokens)
    // whenever we change the token type....
    // (or, "monotonically increase"...)

    
    // Returns the "next" token in an alphabetical sequence...
    public static String getNextSequentialToken(String referenceToken, String type)
    {
        // temporary
        int min_length = 2;  // ????
//        if(TokenType.TYPE_TINY.equals(type)) {
//            min_length = ConfigUtil.getTinyTokenLength();
//        } else if(TokenType.TYPE_SHORT.equals(type)) {
//            min_length = ConfigUtil.getShortTokenLength();
//        } else if(TokenType.TYPE_MEDIUM.equals(type)) {
//            min_length = ConfigUtil.getMediumTokenLength();
//        } else if(TokenType.TYPE_LONG.equals(type)) {
//            min_length = ConfigUtil.getLongTokenLength();
//        } else if(TokenType.TYPE_DIGITS.equals(type)) {
//            min_length = ConfigUtil.getDigitTokenLength();
//        } else {
//            log.log(Level.WARNING, "Next ShortUri token cannot be generated for type = " + type + "; referenceToken = " + referenceToken);
//            return null;
//        }
        // ....
        min_length = ConfigUtil.getSequentialTokenGenerationMinLength();
        // ....
        
        // TBD: Use a separate default tokenType for sequential generation ????
        // if(! TokenType.isValidType(type) || TokenType.TYPE_SEQUENTIAL.equals(type)) {
        if(TokenType.TYPE_SEQUENTIAL.equals(type)) {   // Just fail for invalid type.....  ????
            type = ConfigUtil.getSystemDefaultTokenType();
        }
        // ?????

        String nextToken = null;
        boolean isFirstToken = false;
        if(referenceToken == null || referenceToken.length() < min_length) {
            isFirstToken = true;
        }
        if(isFirstToken == true) {
            if(TokenType.TYPE_TINY.equals(type)) {
                nextToken = generateFirstTinyUriToken(min_length);
            } else if(TokenType.TYPE_SHORT.equals(type)) {
                nextToken = generateFirstShortUriToken(min_length);
            } else if(TokenType.TYPE_MEDIUM.equals(type)) {
                nextToken = generateFirstMediumUriToken(min_length);
            } else if(TokenType.TYPE_LONG.equals(type)) {
                nextToken = generateFirstLongUriToken(min_length);
            } else if(TokenType.TYPE_DIGITS.equals(type)) {
                nextToken = generateFirstDigitUriToken(min_length);
            } else if(TokenType.TYPE_VOWELS.equals(type)) {
                nextToken = generateFirstVowelUriToken(min_length);
            } else if(TokenType.TYPE_ABC.equals(type)) {
                nextToken = generateFirstAbcUriToken(min_length);
            } else if(TokenType.TYPE_BINARY.equals(type)) {
                nextToken = generateFirstBinaryUriToken(min_length);
            } else {
                log.log(Level.WARNING, "Next ShortUri token cannot be generated for type = " + type + "; referenceToken = " + referenceToken);
                return null;
            }
        } else {
            int length = referenceToken.length();
            if(TokenType.TYPE_TINY.equals(type)) {
                nextToken = generateNextTinyUriToken(referenceToken, length);
            } else if(TokenType.TYPE_SHORT.equals(type)) {
                nextToken = generateNextShortUriToken(referenceToken, length);
            } else if(TokenType.TYPE_MEDIUM.equals(type)) {
                nextToken = generateNextMediumUriToken(referenceToken, length);
            } else if(TokenType.TYPE_LONG.equals(type)) {
                nextToken = generateNextLongUriToken(referenceToken, length);
            } else if(TokenType.TYPE_DIGITS.equals(type)) {
                nextToken = generateNextDigitUriToken(referenceToken, length);
            } else if(TokenType.TYPE_VOWELS.equals(type)) {
                nextToken = generateNextVowelUriToken(referenceToken, length);
            } else if(TokenType.TYPE_ABC.equals(type)) {
                nextToken = generateNextAbcUriToken(referenceToken, length);
            } else if(TokenType.TYPE_BINARY.equals(type)) {
                nextToken = generateNextBinaryUriToken(referenceToken, length);
            } else {
                log.log(Level.WARNING, "Next ShortUri token cannot be generated for type = " + type + "; referenceToken = " + referenceToken);
                return null;
            }            
        }
        
        if(log.isLoggable(Level.FINE)) log.fine("nextToken generated: nextToken = " + nextToken + " for referenceToken = " + referenceToken);
        return nextToken;
    }
    


}
