package com.cannyurl.app.util;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.jsoup.Jsoup;


public class TextUtil
{
    private static final Logger log = Logger.getLogger(TextUtil.class.getName());

    // Static methods only.
    private TextUtil() {}

    
    // temporary
    public static String removeHtmlTags(String htmlContent)
    {
        if(htmlContent == null || htmlContent.isEmpty()) {
            return htmlContent;
        }
        
        String textContent = Jsoup.parse(htmlContent).text();
        return textContent;
    }
    
    
    
    // To be deleted
    // ...
    // This not only "truncates" it also "sanitizes"
    // (e.g., More than one consecutive spaces are collapsed into one.)
    // The problem with this method is that it replaces newlines with spaces, etc...
    private static String truncateSimple(String text, int cutoffLen)
    {
        if(text == null || text.isEmpty() || cutoffLen <= 0) {
            return text;
        }
        // If the string is short, no "sanitization"....
        if(text.length() <= cutoffLen) {
            return text;
        }
        
        // No need to do this for the entire text.
        // (In theory, we should add some padding in case spaces contracted... But no big deal.)
        String phrase = text.substring(0, cutoffLen);

        // Split over white spaces, not just spaces...
        String[] words = phrase.split("\\s+");

        int totLen = 0;
        StringBuffer sb = new StringBuffer();
        for(int i=0; i<words.length && totLen<cutoffLen; i++) {
            String word = words[i];
            if(i > 0) {
                totLen += 1;
            }
            totLen += word.length();
            if(totLen > cutoffLen) {
                break;
            }
            if(i > 0) {
                sb.append(" ");
            }
            sb.append(word);
            
//            if(i > 0) {
//                totLen += 1;
//                if(totLen >= cutoffLen) {
//                    break;
//                }
//                sb.append(" ");
//            }
//            totLen += word.length();
//            if(totLen > cutoffLen) {
//                break;
//            }
//            sb.append(word);
        }

        return sb.toString();
    }

    // Better method
    public static String truncateAtWordBoundary(String text, int cutoffLen)
    {
        return truncateAtWordBoundary(text, cutoffLen, 0);
    }
    public static String truncateAtWordBoundary(String text, int cutoffLen, int minLen)
    {
        return truncateAtWordBoundary(text, cutoffLen, minLen, false);
    }
    public static String truncateAtWordBoundary(String text, int cutoffLen, int minLen, boolean addEllipsis)
    {
        if(text == null || text.isEmpty() || cutoffLen <= 0) {
            return text;
        }
        if(text.length() <= cutoffLen) {
            return text;
        }
        // Assert minLen <= cutoffLen ...
        
        if(cutoffLen < 3) {        // ????
            addEllipsis = false;
        }
        
        if(addEllipsis) {
            cutoffLen -= 3;   // Room for "..."...
            // minLen -= 3;  // ????   (Note the implications of not changing minLen....)
        }

        // Base string.
        String truncatedStr = null;

        // Truncate first
        String phrase = text.substring(0, cutoffLen+1);  // trim() ????
        // Note the +1 at the end...
        // We need to include a case where text.charAt(cutoffLen)==" "|"\n"....

        // TBD:
        // Is there a better way using regex ???
        int idx1 = phrase.lastIndexOf(" ");
        int idx2 = phrase.lastIndexOf("\n");
        int idx3 = phrase.lastIndexOf("\t");
        // TBD: \r\n, \r, etc. ????
        // TBD: check for punctuation symbols (?, !, ...) as well???
        int idx = Math.max(idx1, idx2);
        idx = Math.max(idx, idx3);
        if(idx < minLen) {
            // ????
            // return phrase;
            //return phrase.substring(0, minLen);
            truncatedStr = phrase.substring(0, cutoffLen);   // Return the maximum possible...
        } else {
            truncatedStr = phrase.substring(0, idx);  // .trim() ???
        }
        
        if(addEllipsis) {
            truncatedStr = truncatedStr + "...";
        }
        
        return truncatedStr;
    }

    
    
}
