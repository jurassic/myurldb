package com.cannyurl.app.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import com.cannyurl.common.TermStruct;
import com.cannyurl.common.TermType;


// Note: We use DateRangeUtil instead of this for now...
//...
// Note: All time periods (day, week, month, etc.) are [begin, end) (e.g., begin is included, but end is excluded)...
public class TimeRangeUtil
{
    private static final Logger log = Logger.getLogger(TimeRangeUtil.class.getName());

    private TimeRangeUtil() {}


    // UTC based.... for now... ???
    // TBD: Make it timezone dependent ????
    // Howt to handle daylight saving time????
    private static final String TZNAME_USPACIFIC = "US/Pacific";   // ??
    private static final TimeZone TIMEZONE_USPACIFIC;
    static {
        TIMEZONE_USPACIFIC = TimeZone.getTimeZone(TZNAME_USPACIFIC);
    }
    

    // ????
    private static Pattern sPattern;
    static {
        sPattern = Pattern.compile("^\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}");
    }
    
    // temporary
    // Need testing...
    public static boolean isValid(String dayHour)
    {
        if(dayHour == null || dayHour.length() < 10) {
            return false;
        }
        return sPattern.matcher(dayHour).matches();
    }
   
    // TBD...
    //public static boolean isValid(String termType, String dayHour)
    // ...
    // ...
    
    
    public static String getTermTime(String termType)
    {
        if(TermType.TERM_MINUTE.equals(termType)) {
            return getDayHourMinute();
        } else if(TermType.TERM_TENMINS.equals(termType)) {
            return getDayHourTenMins();
        } else if(TermType.TERM_HOURLY.equals(termType)) {
            return getDayHour();
        } else if(TermType.TERM_SIXHOURS.equals(termType)) {
            return getDaySixHours();
        } else if(TermType.TERM_DAILY.equals(termType)) {
            return getDay();
        } else if(TermType.TERM_WEEKLY.equals(termType)) {
            return getWeek();
        } else if(TermType.TERM_MONTHLY.equals(termType)) {
            return getMonth();
        } else {
            return null;   // ????
        }
    }

    public static String getTermTime(String termType, long time)
    {
        if(TermType.TERM_MINUTE.equals(termType)) {
            return getDayHourMinute(time);
        } else if(TermType.TERM_TENMINS.equals(termType)) {
            return getDayHourTenMins(time);
        } else if(TermType.TERM_HOURLY.equals(termType)) {
            return getDayHour(time);
        } else if(TermType.TERM_SIXHOURS.equals(termType)) {
            return getDaySixHours(time);
        } else if(TermType.TERM_DAILY.equals(termType)) {
            return getDay(time);
        } else if(TermType.TERM_WEEKLY.equals(termType)) {
            return getWeek(time);
        } else if(TermType.TERM_MONTHLY.equals(termType)) {
            return getMonth(time);
        } else {
            return null;   // ????
        }
    }

    
    public static String getMonth()
    {
        return getMonth(System.currentTimeMillis());
    }
    public static String getMonth(long time)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-01 00:00");
        dateFormat.setTimeZone(TIMEZONE_USPACIFIC);
        String date = dateFormat.format(new Date(time));
        return date;
    }

    public static String getWeek()
    {
        return getWeek(System.currentTimeMillis());
    }
    public static String getWeek(long time)
    {
        long t = 0L;
        int day = getDayOfWeek(time);
        t = time - day * 3600 * 24 * 1000L;
        return getDay(t);
    }
    // temporary
    private static int getDayOfWeek(long time)
    {
        SimpleDateFormat df = new SimpleDateFormat("E");
        df.setTimeZone(TIMEZONE_USPACIFIC);
        String day = df.format(new Date(time));
        if(log.isLoggable(Level.FINER)) log.finer("getDayOfWeek(): day = " + day + "; time = " + time);

        int dayOfWeek = 0;
        if(day.startsWith("Su")) {
            dayOfWeek = 0;
        } else if(day.startsWith("Mo")) {
            dayOfWeek = 1;
        } else if(day.startsWith("Tu")) {
            dayOfWeek = 2;
        } else if(day.startsWith("We")) {
            dayOfWeek = 3;
        } else if(day.startsWith("Th")) {
            dayOfWeek = 4;
        } else if(day.startsWith("Fr")) {
            dayOfWeek = 5;
        } else if(day.startsWith("Sa")) {
            dayOfWeek = 6;
        }        
        return dayOfWeek;
    }

    public static String getDay()
    {
        return getDay(System.currentTimeMillis());
    }
    public static String getDay(long time)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd 00:00");
        dateFormat.setTimeZone(TIMEZONE_USPACIFIC);
        String date = dateFormat.format(new Date(time));
        return date;
    }

    public static String getDaySixHours()
    {
        return getDaySixHours(System.currentTimeMillis());
    }
    public static String getDaySixHours(long time)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:00");
        dateFormat.setTimeZone(TIMEZONE_USPACIFIC);
        String date = dateFormat.format(new Date(time));
        // TBD: Convert to int? Or, not?
        String hour = date.substring(11, 13);
        if(hour.compareTo("18") >= 0) {
            hour = "18";
        } else if(hour.compareTo("12") >= 0) {
            hour = "12";
        } else if(hour.compareTo("06") >= 0) {
            hour = "06";
        } else {
            hour = "00";
        }
        date = date.substring(0, 11) + hour + ":00";
        return date;
    }

    public static String getDayHour()
    {
        return getDayHour(System.currentTimeMillis());
    }
    public static String getDayHour(long time)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:00");
        dateFormat.setTimeZone(TIMEZONE_USPACIFIC);
        String date = dateFormat.format(new Date(time));
        return date;
    }

    public static String getDayHourTenMins()
    {
        return getDayHourTenMins(System.currentTimeMillis());
    }
    public static String getDayHourTenMins(long time)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        dateFormat.setTimeZone(TIMEZONE_USPACIFIC);
        String date = dateFormat.format(new Date(time));
        // ????
        date = date.substring(0, date.length()-1) + "0";
        // ....
        return date;
    }

    public static String getDayHourMinute()
    {
        return getDayHourMinute(System.currentTimeMillis());
    }
    public static String getDayHourMinute(long time)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        dateFormat.setTimeZone(TIMEZONE_USPACIFIC);
        String date = dateFormat.format(new Date(time));
        return date;
    }
    
    

    
    public static String getTermDayHourFromDayHour(String termType, String dayHour)
    {
        if(TermType.TERM_MINUTE.equals(termType)) {
            return getMinuteFromDayHour(dayHour);
        } else if(TermType.TERM_TENMINS.equals(termType)) {
            return getTenMinsFromDayHour(dayHour);
        } else if(TermType.TERM_HOURLY.equals(termType)) {
            return getHourFromDayHour(dayHour);
        } else if(TermType.TERM_SIXHOURS.equals(termType)) {
            return getSixHoursFromDayHour(dayHour);
        } else if(TermType.TERM_DAILY.equals(termType)) {
            return getDayFromDayHour(dayHour);
        } else if(TermType.TERM_WEEKLY.equals(termType)) {
            return getWeekFromDayHour(dayHour);
        } else if(TermType.TERM_MONTHLY.equals(termType)) {
            return getMonthFromDayHour(dayHour);
        } else {
            return null;   // ????
        }
    }
    
    
    
    // ???
    public static String getMonthFromDayHour(String dayHour)
    {
        if(dayHour == null || dayHour.length() < 10) {
            if(log.isLoggable(Level.WARNING)) log.warning("Invalid input: dayHour = " + dayHour);
            return dayHour;  // ???
        }
        String month = dayHour.substring(0, 7) + "-01 00:00";  // ???
        return month;
    }
    public static String getWeekFromDayHour(String dayHour)
    {
        if(dayHour == null || dayHour.length() < 10) {
            if(log.isLoggable(Level.WARNING)) log.warning("Invalid input: dayHour = " + dayHour);
            return dayHour;  // ???
        }
        return getWeek(getMilli(dayHour));  // ????
    }
    public static String getDayFromDayHour(String dayHour)
    {
        if(dayHour == null || dayHour.length() < 10) {
            if(log.isLoggable(Level.WARNING)) log.warning("Invalid input: dayHour = " + dayHour);
            return dayHour;  // ???
        }
        String day = dayHour.substring(0, 10) + " 00:00";  // ???
        return day;
    }
    public static String getSixHoursFromDayHour(String dayHour)
    {
        if(dayHour == null || dayHour.length() < 10) {
            if(log.isLoggable(Level.WARNING)) log.warning("Invalid input: dayHour = " + dayHour);
            return dayHour;  // ???
        }
        String hour = dayHour.substring(11, 13);
        if(hour.compareTo("18") >= 0) {
            hour = "18";
        } else if(hour.compareTo("12") >= 0) {
            hour = "12";
        } else if(hour.compareTo("06") >= 0) {
            hour = "06";
        } else {
            hour = "00";
        }
        dayHour = dayHour.substring(0, 11) + hour + ":00";
        return dayHour;
    }
    public static String getHourFromDayHour(String dayHour)
    {
        if(dayHour == null || dayHour.length() < 10) {
            if(log.isLoggable(Level.WARNING)) log.warning("Invalid input: dayHour = " + dayHour);
            return dayHour;  // ???
        }
        String hour = dayHour.substring(0, 13) + ":00";  // ???
        return hour;
    }
    public static String getTenMinsFromDayHour(String dayHour)
    {
        if(dayHour == null || dayHour.length() < 10) {
            if(log.isLoggable(Level.WARNING)) log.warning("Invalid input: dayHour = " + dayHour);
            return dayHour;  // ???
        }
        String hour = dayHour.substring(0, 15) + "0";  // ???
        return hour;
    }
    public static String getMinuteFromDayHour(String dayHour)
    {
        if(dayHour == null || dayHour.length() < 10) {
            if(log.isLoggable(Level.WARNING)) log.warning("Invalid input: dayHour = " + dayHour);
            return dayHour;  // ???
        }
        return dayHour;
    }
    
    
    public static String[] getRange(String termType, String startDayHour, String endDayHour)
    {
        if(TermType.TERM_MINUTE.equals(termType)) {
            return getDayHourMinuteRange(startDayHour, endDayHour);
        } else if(TermType.TERM_TENMINS.equals(termType)) {
            return getDayHourTenMinsRange(startDayHour, endDayHour);
        } else if(TermType.TERM_HOURLY.equals(termType)) {
            return getDayHourRange(startDayHour, endDayHour);
        } else if(TermType.TERM_SIXHOURS.equals(termType)) {
            return getDaySixHoursRange(startDayHour, endDayHour);
        } else if(TermType.TERM_DAILY.equals(termType)) {
            return getDayRange(startDayHour, endDayHour);
        } else if(TermType.TERM_WEEKLY.equals(termType)) {
            return getWeekRange(startDayHour, endDayHour);
        } else if(TermType.TERM_MONTHLY.equals(termType)) {
            return getMonthRange(startDayHour, endDayHour);
        } else {
            return null;   // ????
        }
    }

    public static String[] getMonthRange(String startDayHour, String endDayHour)
    {
        if(! isValid(startDayHour)) {
            return null;
        }
        if(endDayHour == null || ! isValid(endDayHour)) {
            endDayHour = getMonth();
            log.info("Invalid endDayHour: New value = " + endDayHour);
        }
        
        // temporary.
        // Not the most efficient algo.
        List<String> vals = new ArrayList<String>();
        String pointer = startDayHour;
        while(pointer.compareTo(endDayHour) <= 0) {
            vals.add(pointer);
            pointer = getNextMonth(pointer);            
        }
        
        String[] range = vals.toArray(new String[0]);
        return range;
    }

    public static String[] getWeekRange(String startDayHour, String endDayHour)
    {
        if(! isValid(startDayHour)) {
            return null;
        }
        if(endDayHour == null || ! isValid(endDayHour)) {
            endDayHour = getWeek();
            log.info("Invalid endDayHour: New value = " + endDayHour);
        }
        
        // temporary.
        // Not the most efficient algo.
        List<String> vals = new ArrayList<String>();
        String pointer = startDayHour;
        while(pointer.compareTo(endDayHour) <= 0) {
            vals.add(pointer);
            pointer = getNextWeek(pointer);            
        }
        
        String[] range = vals.toArray(new String[0]);
        return range;
    }

    public static String[] getDayRange(String startDayHour, String endDayHour)
    {
        if(! isValid(startDayHour)) {
            return null;
        }
        if(endDayHour == null || ! isValid(endDayHour)) {
            endDayHour = getDayHour();
            if(log.isLoggable(Level.INFO)) log.info("Invalid endDayHour: New value = " + endDayHour);
        }
        
        // temporary.
        // Not the most efficient algo.
        List<String> vals = new ArrayList<String>();
        String pointer = startDayHour;
        while(pointer.compareTo(endDayHour) <= 0) {
            vals.add(pointer);
            pointer = getNextHour(pointer);            
        }
        
        String[] range = vals.toArray(new String[0]);
        return range;
    }
    
    public static String[] getDaySixHoursRange(String startDayHour, String endDayHour)
    {
        if(! isValid(startDayHour)) {
            return null;
        }
        if(endDayHour == null || ! isValid(endDayHour)) {
            endDayHour = getDaySixHours();
            if(log.isLoggable(Level.INFO)) log.info("Invalid endDayHour: New value = " + endDayHour);
        }
        
        // temporary.
        // Not the most efficient algo.
        List<String> vals = new ArrayList<String>();
        String pointer = startDayHour;
        while(pointer.compareTo(endDayHour) <= 0) {
            vals.add(pointer);
            pointer = getNextSixHours(pointer);            
        }
        
        String[] range = vals.toArray(new String[0]);
        return range;
    }

    public static String[] getDayHourRange(String startDayHour, String endDayHour)
    {
        if(! isValid(startDayHour)) {
            return null;
        }
        if(endDayHour == null || ! isValid(endDayHour)) {
            endDayHour = getDayHour();
            if(log.isLoggable(Level.INFO)) log.info("Invalid endDayHour: New value = " + endDayHour);
        }
        
        // temporary.
        // Not the most efficient algo.
        List<String> vals = new ArrayList<String>();
        String pointer = startDayHour;
        while(pointer.compareTo(endDayHour) <= 0) {
            vals.add(pointer);
            pointer = getNextHour(pointer);            
        }
        
        String[] range = vals.toArray(new String[0]);
        return range;
    }
    
    public static String[] getDayHourTenMinsRange(String startDayHour, String endDayHour)
    {
        if(! isValid(startDayHour)) {
            return null;
        }
        if(endDayHour == null || ! isValid(endDayHour)) {
            endDayHour = getDayHourTenMins();
            if(log.isLoggable(Level.INFO)) log.info("Invalid endDayHour: New value = " + endDayHour);
        }
        
        // temporary.
        // Not the most efficient algo.
        List<String> vals = new ArrayList<String>();
        String pointer = startDayHour;
        while(pointer.compareTo(endDayHour) <= 0) {
            vals.add(pointer);
            pointer = getNextTenMins(pointer);            
        }
        
        String[] range = vals.toArray(new String[0]);
        return range;
    }
    
    public static String[] getDayHourMinuteRange(String startDayHour, String endDayHour)
    {
        if(! isValid(startDayHour)) {
            return null;
        }
        if(endDayHour == null || ! isValid(endDayHour)) {
            endDayHour = getDayHourMinute();
            if(log.isLoggable(Level.INFO)) log.info("Invalid endDayHour: New value = " + endDayHour);
        }
        
        // temporary.
        // Not the most efficient algo.
        List<String> vals = new ArrayList<String>();
        String pointer = startDayHour;
        while(pointer.compareTo(endDayHour) <= 0) {
            vals.add(pointer);
            pointer = getNextMinute(pointer);            
        }
        
        String[] range = vals.toArray(new String[0]);
        return range;
    }

    
    public static long getMilli(String day)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        dateFormat.setTimeZone(TIMEZONE_USPACIFIC);
        long milli = 0L;
        try {
            Date date = dateFormat.parse(day);
            milli = date.getTime();
        } catch (ParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse the input string: day = " + day, e);
        }
        return milli;
    }
    
    public static long[] getMilliRange(String termType, String dayHour)
    {
        long[] range = new long[2];
        range[0] = getMilli(dayHour);
        range[1] = getMilli(getNext(termType, dayHour));
        
        // for debugging...
        if(log.isLoggable(Level.FINE)) {
            log.fine("termType = " + termType + "; dayHour = " + dayHour);
            log.fine("range: " + range[0] + " - " + range[1]);
        }

        return range;
    }

    
    
    // This returns an array of TermStructs, with the "smallest" number of TermStructs...
    // A termStruct before another termStruct cannot be "narrower/shorter".
    // Note that this returns the term which includes originTime....
    public static List<TermStruct> getPreviousTermList(long startingTime, long originTime, String originTermType)
    {
        return getPreviousTermList(startingTime, originTime, originTermType, null);
    }
    public static List<TermStruct> getPreviousTermList(long startingTime, long originTime, String originTermType, String widestAllowedTermType)
    {
        if(log.isLoggable(Level.FINE)) log.fine("getPreviousTermList(): startingTime = " + startingTime + "; originTime = " + originTime + "; originTermType = " + originTermType + "; widestAllowedTermType = " + widestAllowedTermType);
        if(startingTime > originTime) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Error: startingTime should be no later than originTime. startingTime = " + startingTime + "; originTime" + originTime);
            return null;
        }
        List<TermStruct> termList = new ArrayList<TermStruct>();
        
        String originDayHour = getTermTime(originTermType, originTime);
        TermStruct originTerm = new TermStruct(originTermType, originDayHour);
        if(compareTime(originTermType, startingTime, originDayHour) == 0) {
            // The only term...
            // termList.add(originTerm);
        } else {
            String oneWiderWidestAllowedTermType = null;
            if(TermType.isValid(widestAllowedTermType)) {
                oneWiderWidestAllowedTermType = TermType.getWiderTerm(widestAllowedTermType);
            }
            String startingTermType = originTermType;
            String startingDayHour = getTermTime(startingTermType, startingTime);
            String widestStartingTerm = startingTermType;
            while(true) {
                widestStartingTerm = TermType.getWiderTerm(widestStartingTerm);
                if(widestStartingTerm != null && (oneWiderWidestAllowedTermType == null || ! oneWiderWidestAllowedTermType.equals(widestStartingTerm) ) && compareDayHours(widestStartingTerm, startingDayHour, originTermType, originDayHour) < 0 && !isOverlapping(widestStartingTerm, startingDayHour, originTermType, originDayHour)) {
                    startingTermType = widestStartingTerm;
                    startingDayHour = getTermTime(startingTermType, startingTime);
                } else {
                    break;
                }
            }
            log.info(">>>>>> startingTermType = " + startingTermType);
            log.info(">>>>>> startingDayHour = " + startingDayHour);
            
            String oneNarrowerOriginTermType = TermType.getNarrowerTerm(originTermType);
            if(startingTermType != null) {                
                String tt = startingTermType;
                String dh = startingDayHour;
                while(tt != null &&  (oneNarrowerOriginTermType==null || ! tt.equals(oneNarrowerOriginTermType))) {
                    while(dh != null && compareDayHours(tt, dh, originTermType, originDayHour) < 0 && !isOverlapping(tt, dh, originTermType, originDayHour)) {
                        TermStruct ts = new TermStruct(tt, dh);
                        termList.add(ts);
                        dh = getNext(tt, dh);
                    }
                    
                    tt = TermType.getNarrowerTerm(tt);
                    dh = getTermDayHourFromDayHour(tt, dh);   // current dh is 1 + the last valid dh in the previous tt...
                }
                
                // The last term...
                // termList.add(originTerm);
            } else {
                // This cannot happen....
            }
        }
        
        // The last term that includes "now" (originTime)
        termList.add(originTerm);
        
        
        if(log.isLoggable(Level.INFO)) {
            if(termList != null) {
                if(termList.isEmpty()) {
                    log.info("termList is empty");
                } else {
                    for(TermStruct struct : termList) {
                        String _tt = struct.getTermType();
                        String _dh = struct.getDayHour();
                        log.info("TermStruct: _tt = " + _tt + "; _dh = " + _dh);
                    }
                }
            }
        }
        
        return termList;
    }
    
    
    
    // TBD:
    // getNextTermList() ????
    //     the mirror image of getPreviousTermList() ????? 
    // ...
    
    
    
    
    // TBD:
    // Methods to convert an arbitrary date string into a valid date/time based on termType... ???
    // ...

    
    
    
    public static String getNext(String termType, long time)
    {
        if(TermType.TERM_MINUTE.equals(termType)) {
            return getNextMinute(time);
        } else if(TermType.TERM_TENMINS.equals(termType)) {
            return getNextTenMins(time);
        } else if(TermType.TERM_HOURLY.equals(termType)) {
            return getNextHour(time);
        } else if(TermType.TERM_SIXHOURS.equals(termType)) {
            return getNextSixHours(time);
        } else if(TermType.TERM_DAILY.equals(termType)) {
            return getNextDay(time);
        } else if(TermType.TERM_WEEKLY.equals(termType)) {
            return getNextWeek(time);
        } else if(TermType.TERM_MONTHLY.equals(termType)) {
            return getNextMonth(time);
        } else {
            return null;   // ????
        }
    }
    public static String getPrevious(String termType, long time)
    {
        if(TermType.TERM_MINUTE.equals(termType)) {
            return getPreviousMinute(time);
        } else if(TermType.TERM_TENMINS.equals(termType)) {
            return getPreviousTenMins(time);
        } else if(TermType.TERM_HOURLY.equals(termType)) {
            return getPreviousHour(time);
        } else if(TermType.TERM_SIXHOURS.equals(termType)) {
            return getPreviousSixHours(time);
        } else if(TermType.TERM_DAILY.equals(termType)) {
            return getPreviousDay(time);
        } else if(TermType.TERM_WEEKLY.equals(termType)) {
            return getPreviousWeek(time);
        } else if(TermType.TERM_MONTHLY.equals(termType)) {
            return getPreviousMonth(time);
        } else {
            return null;   // ????
        }
    }
    

    public static String getNext(String termType, String dayHour)
    {
        if(TermType.TERM_MINUTE.equals(termType)) {
            return getNextMinute(dayHour);
        } else if(TermType.TERM_TENMINS.equals(termType)) {
            return getNextTenMins(dayHour);
        } else if(TermType.TERM_HOURLY.equals(termType)) {
            return getNextHour(dayHour);
        } else if(TermType.TERM_SIXHOURS.equals(termType)) {
            return getNextSixHours(dayHour);
        } else if(TermType.TERM_DAILY.equals(termType)) {
            return getNextDay(dayHour);
        } else if(TermType.TERM_WEEKLY.equals(termType)) {
            return getNextWeek(dayHour);
        } else if(TermType.TERM_MONTHLY.equals(termType)) {
            return getNextMonth(dayHour);
        } else {
            return null;   // ????
        }
    }
    public static String getPrevious(String termType, String dayHour)
    {
        if(TermType.TERM_MINUTE.equals(termType)) {
            return getPreviousMinute(dayHour);
        } else if(TermType.TERM_TENMINS.equals(termType)) {
            return getPreviousTenMins(dayHour);
        } else if(TermType.TERM_HOURLY.equals(termType)) {
            return getPreviousHour(dayHour);
        } else if(TermType.TERM_SIXHOURS.equals(termType)) {
            return getPreviousSixHours(dayHour);
        } else if(TermType.TERM_DAILY.equals(termType)) {
            return getPreviousDay(dayHour);
        } else if(TermType.TERM_WEEKLY.equals(termType)) {
            return getPreviousWeek(dayHour);
        } else if(TermType.TERM_MONTHLY.equals(termType)) {
            return getPreviousMonth(dayHour);
        } else {
            return null;   // ????
        }
    }
    
    
    
    
    public static String getNextMonth()
    {
        return getNextMonth(System.currentTimeMillis());
    }
    public static String getNextMonth(long time)
    {
        String now = getMonth(time);
        return getNextMonth(now);
    }

    public static String getNextWeek()
    {
        return getNextWeek(System.currentTimeMillis());
    }
    public static String getNextWeek(long time)
    {
        String now = getWeek(time);
        return getNextWeek(now);
    }

    public static String getNextDay()
    {
        return getNextDay(System.currentTimeMillis());
    }
    public static String getNextDay(long time)
    {
        String now = getDay(time);
        return getNextDay(now);
    }

    public static String getNextSixHours()
    {
        return getNextSixHours(System.currentTimeMillis());
    }
    public static String getNextSixHours(long time)
    {
        String now = getDaySixHours(time);
        return getNextSixHours(now);
    }

    public static String getNextHour()
    {
        return getNextHour(System.currentTimeMillis());
    }
    public static String getNextHour(long time)
    {
        String now = getDayHour(time);
        return getNextHour(now);
    }

    public static String getNextTenMins()
    {
        return getNextTenMins(System.currentTimeMillis());
    }
    public static String getNextTenMins(long time)
    {
        String now = getDayHourTenMins(time);
        return getNextTenMins(now);
    }

    public static String getNextMinute()
    {
        return getNextMinute(System.currentTimeMillis());
    }
    public static String getNextMinute(long time)
    {
        String now = getDayHourMinute(time);
        return getNextMinute(now);
    }
    
    
    
    public static String getPreviousMonth()
    {
        return getPreviousMonth(System.currentTimeMillis());
    }
    public static String getPreviousMonth(long time)
    {
        String now = getMonth(time);
        return getPreviousMonth(now);
    }

    public static String getPreviousWeek()
    {
        return getPreviousWeek(System.currentTimeMillis());
    }
    public static String getPreviousWeek(long time)
    {
        String now = getWeek(time);
        return getPreviousWeek(now);
    }

    public static String getPreviousDay()
    {
        return getPreviousDay(System.currentTimeMillis());
    }
    public static String getPreviousDay(long time)
    {
        String now = getDay(time);
        return getPreviousDay(now);
    }

    public static String getPreviousSixHours()
    {
        return getPreviousSixHours(System.currentTimeMillis());
    }
    public static String getPreviousSixHours(long time)
    {
        String now = getDaySixHours(time);
        return getPreviousSixHours(now);
    }

    public static String getPreviousHour()
    {
        return getPreviousHour(System.currentTimeMillis());
    }
    public static String getPreviousHour(long time)
    {
        String now = getDayHour(time);
        return getPreviousHour(now);
    }

    public static String getPreviousTenMins()
    {
        return getPreviousTenMins(System.currentTimeMillis());
    }
    public static String getPreviousTenMins(long time)
    {
        String now = getDayHourTenMins(time);
        return getPreviousTenMins(now);
    }

    public static String getPreviousMinute()
    {
        return getPreviousMinute(System.currentTimeMillis());
    }
    public static String getPreviousMinute(long time)
    {
        String now = getDayHourMinute(time);
        return getPreviousMinute(now);
    }
    

    
    public static String getNextMonth(String dayHour)
    {
        if(dayHour == null || dayHour.length() < 10) {
            if(log.isLoggable(Level.WARNING)) log.warning("Invalid input: dayHour = " + dayHour);
            return null;  // ???
        }
        String currentMonth = getMonthFromDayHour(dayHour);
        int year = Integer.parseInt(currentMonth.substring(0, 4));
        int month = Integer.parseInt(currentMonth.substring(5, 7)) + 1;
        if(month > 12) {
            month = 1;
            year += 1;
        }
        String mo = "";
        if(month < 10) {
            mo = "0" + month;
        } else {
            mo = "" + month;
        }
        String nextMonth = year + "-" + mo + "-01 00:00"; 
        return nextMonth;
    }
    public static String getPreviousMonth(String dayHour)
    {
        if(dayHour == null || dayHour.length() < 10) {
            if(log.isLoggable(Level.WARNING)) log.warning("Invalid input: dayHour = " + dayHour);
            return null;  // ???
        }
        String currentMonth = getMonthFromDayHour(dayHour);
        int year = Integer.parseInt(currentMonth.substring(0, 4));
        int month = Integer.parseInt(currentMonth.substring(5, 7)) - 1;
        if(month < 1) {
            month = 12;
            year -= 1;
        }
        String mo = "";
        if(month < 10) {
            mo = "0" + month;
        } else {
            mo = "" + month;
        }
        String prevMonth = year + "-" + mo + "-01 00:00"; 
        return prevMonth;
    }
    
    public static String getNextWeek(String dayHour)
    {
        if(dayHour == null || dayHour.length() < 10) {
            if(log.isLoggable(Level.WARNING)) log.warning("Invalid input: dayHour = " + dayHour);
            return null;  // ???
        }
        long milli = getMilli(dayHour) + 7 * 3600 * 24 * 1000L;
        return getWeek(milli);
    }
    public static String getPreviousWeek(String dayHour)
    {
        if(dayHour == null || dayHour.length() < 10) {
            if(log.isLoggable(Level.WARNING)) log.warning("Invalid input: dayHour = " + dayHour);
            return null;  // ???
        }
        long milli = getMilli(dayHour) - 7 * 3600 * 24 * 1000L;
        return getWeek(milli);
    }
    
    public static String getNextDay(String dayHour)
    {
        if(dayHour == null || dayHour.length() < 10) {
            if(log.isLoggable(Level.WARNING)) log.warning("Invalid input: dayHour = " + dayHour);
            return null;  // ???
        }
        long milli = getMilli(dayHour) + 3600 * 24 * 1000L;
        return getDay(milli);
    }
    public static String getPreviousDay(String dayHour)
    {
        if(dayHour == null || dayHour.length() < 10) {
            if(log.isLoggable(Level.WARNING)) log.warning("Invalid input: dayHour = " + dayHour);
            return null;  // ???
        }
        long milli = getMilli(dayHour) - 3600 * 24 * 1000L;
        return getDay(milli);
    }

    public static String getNextSixHours(String dayHour)
    {
        if(dayHour == null || dayHour.length() < 10) {
            if(log.isLoggable(Level.WARNING)) log.warning("Invalid input: dayHour = " + dayHour);
            return null;  // ???
        }
        long milli = getMilli(dayHour) + 6 * 3600 * 1000L;
        return getDaySixHours(milli);
    }
    public static String getPreviousSixHours(String dayHour)
    {
        if(dayHour == null || dayHour.length() < 10) {
            if(log.isLoggable(Level.WARNING)) log.warning("Invalid input: dayHour = " + dayHour);
            return null;  // ???
        }
        long milli = getMilli(dayHour) - 6 * 3600 * 1000L;
        return getDaySixHours(milli);
    }

    public static String getNextHour(String dayHour)
    {
        if(dayHour == null || dayHour.length() < 10) {
            if(log.isLoggable(Level.WARNING)) log.warning("Invalid input: dayHour = " + dayHour);
            return null;  // ???
        }
        long milli = getMilli(dayHour) + 3600 * 1000L;
        return getDayHour(milli);
    }
    public static String getPreviousHour(String dayHour)
    {
        if(dayHour == null || dayHour.length() < 10) {
            if(log.isLoggable(Level.WARNING)) log.warning("Invalid input: dayHour = " + dayHour);
            return null;  // ???
        }
        long milli = getMilli(dayHour) - 3600 * 1000L;
        return getDayHour(milli);
    }
    
    public static String getNextTenMins(String dayHour)
    {
        if(dayHour == null || dayHour.length() < 10) {
            if(log.isLoggable(Level.WARNING)) log.warning("Invalid input: dayHour = " + dayHour);
            return null;  // ???
        }
        long milli = getMilli(dayHour) + 600 * 1000L;
        return getDayHourTenMins(milli);
    }
    public static String getPreviousTenMins(String dayHour)
    {
        if(dayHour == null || dayHour.length() < 10) {
            if(log.isLoggable(Level.WARNING)) log.warning("Invalid input: dayHour = " + dayHour);
            return null;  // ???
        }
        long milli = getMilli(dayHour) - 600 * 1000L;
        return getDayHourTenMins(milli);
    }

    public static String getNextMinute(String dayHour)
    {
        if(dayHour == null || dayHour.length() < 10) {
            if(log.isLoggable(Level.WARNING)) log.warning("Invalid input: dayHour = " + dayHour);
            return null;  // ???
        }
        long milli = getMilli(dayHour) + 60 * 1000L;
        return getDayHourMinute(milli);
    }
    public static String getPreviousMinute(String dayHour)
    {
        if(dayHour == null || dayHour.length() < 10) {
            if(log.isLoggable(Level.WARNING)) log.warning("Invalid input: dayHour = " + dayHour);
            return null;  // ???
        }
        long milli = getMilli(dayHour) - 60 * 1000L;
        return getDayHourMinute(milli);
    }

    
    
    
    
    
    // Note
    // Currently, all compareXXX() implementations compare the starting times of two periods (possibly, with different spans)
    // ...
    // TBD: It might be useful to return 0 if the two periods overlap...
    // Or, just implement another set of functions to check overlaps.. ????
    // ...
    
    
    // Returns true if the two ranges overlap
    // (Note: having the same end time and starting is not considered "overlap")
    public static boolean isOverlapping(String termL, String dayL, String termR, String dayR)
    {
        // TBD: Validation?
        long[] lls = TimeRangeUtil.getMilliRange(termL, dayL);
        long[] rrs = TimeRangeUtil.getMilliRange(termR, dayR);
        if((lls[0] <= rrs[0] && lls[1] > rrs[0]) || (lls[0] <= rrs[1] && lls[1] > rrs[1]) || (lls[0] >= rrs[0] && lls[0] < rrs[1])) {
            return true;
        } else {
            return false;
        }
    }
    
    
    
    
    
    // TBD: Does this work???
    // If so, 
    // update all hundred "compare" functions below... ???
    // ....
    public static int compareDayHours(String termL, String dayL, String termR, String dayR)
    {
        String termDayL = getTermDayHourFromDayHour(termL, dayL);
        String termDayR = getTermDayHourFromDayHour(termR, dayR);
        return compare(termDayL, termDayR);
    }

    
    
    public static int compareMonths(String dayL, String dayR)
    {
        return compare(dayL, dayR);
    }    
    public static int compareWeeks(String dayL, String dayR)
    {
        return compare(dayL, dayR);
    }    
    public static int compareDays(String dayL, String dayR)
    {
        return compare(dayL, dayR);
    } 
    public static int compareSixHours(String dayL, String dayR)
    {
        return compare(dayL, dayR);
    }    
    public static int compareHours(String dayL, String dayR)
    {
        return compare(dayL, dayR);
    }    
    public static int compareTenMins(String dayL, String dayR)
    {
        return compare(dayL, dayR);
    }    
    public static int compareMinutes(String dayL, String dayR)
    {
        return compare(dayL, dayR);
    }    
 
    // ???
    
    public static int compareMinuteAndTenMins(String hourL, String dayR)
    {
        String dayL = getTenMinsFromDayHour(hourL);
        return compare(dayL, dayR);
    }
    public static int compareMinuteAndHour(String hourL, String dayR)
    {
        String dayL = getHourFromDayHour(hourL);
        return compare(dayL, dayR);
    }
    public static int compareMinuteAndSixHours(String hourL, String dayR)
    {
        String dayL = getSixHoursFromDayHour(hourL);
        return compare(dayL, dayR);
    }
    public static int compareMinuteAndDay(String hourL, String dayR)
    {
        String dayL = getDayFromDayHour(hourL);
        return compare(dayL, dayR);
    }
    public static int compareMinuteAndWeek(String hourL, String dayR)
    {
        String weekL = getWeekFromDayHour(hourL);
        return compare(weekL, dayR);
    }
    public static int compareMinuteAndMonth(String hourL, String dayR)
    {
        String monthL = getMonthFromDayHour(hourL);
        return compare(monthL, dayR);
    }

    public static int compareTenMinsAndHour(String hourL, String dayR)
    {
        String dayL = getHourFromDayHour(hourL);
        return compare(dayL, dayR);
    }
    public static int compareTenMinsAndSixHours(String hourL, String dayR)
    {
        String dayL = getSixHoursFromDayHour(hourL);
        return compare(dayL, dayR);
    }
    public static int compareTenMinsAndDay(String hourL, String dayR)
    {
        String dayL = getDayFromDayHour(hourL);
        return compare(dayL, dayR);
    }
    public static int compareTenMinsAndWeek(String hourL, String dayR)
    {
        String weekL = getWeekFromDayHour(hourL);
        return compare(weekL, dayR);
    }
    public static int compareTenMinsAndMonth(String hourL, String dayR)
    {
        String monthL = getMonthFromDayHour(hourL);
        return compare(monthL, dayR);
    }
    
    public static int compareHourAndSixHours(String hourL, String dayR)
    {
        String dayL = getSixHoursFromDayHour(hourL);
        return compare(dayL, dayR);
    }
    public static int compareHourAndDay(String hourL, String dayR)
    {
        String dayL = getDayFromDayHour(hourL);
        return compare(dayL, dayR);
    }
    public static int compareHourAndWeek(String hourL, String dayR)
    {
        String weekL = getWeekFromDayHour(hourL);
        return compare(weekL, dayR);
    }
    public static int compareHourAndMonth(String hourL, String dayR)
    {
        String monthL = getMonthFromDayHour(hourL);
        return compare(monthL, dayR);
    }

    public static int compareSixHoursAndDay(String hourL, String dayR)
    {
        String dayL = getDayFromDayHour(hourL);
        return compare(dayL, dayR);
    }
    public static int compareSixHoursAndWeek(String hourL, String dayR)
    {
        String weekL = getWeekFromDayHour(hourL);
        return compare(weekL, dayR);
    }
    public static int compareSixHoursAndMonth(String hourL, String dayR)
    {
        String monthL = getMonthFromDayHour(hourL);
        return compare(monthL, dayR);
    }
    
    public static int compareDayAndWeek(String hourL, String dayR)
    {
        String weekL = getWeekFromDayHour(hourL);
        return compare(weekL, dayR);
    }
    public static int compareDayAndMonth(String hourL, String dayR)
    {
        String monthL = getMonthFromDayHour(hourL);
        return compare(monthL, dayR);
    }

    public static int compareWeekAndMonth(String hourL, String dayR)
    {
        String monthL = getMonthFromDayHour(hourL);
        return compare(monthL, dayR);
    }

    
    public static int compareTime(String termType, long time, String dayR)
    {
        if(TermType.TERM_MINUTE.equals(termType)) {
            return compareTimeAndMinute(time, dayR);
        } else if(TermType.TERM_TENMINS.equals(termType)) {
            return compareTimeAndTenMins(time, dayR);
        } else if(TermType.TERM_HOURLY.equals(termType)) {
            return compareTimeAndHour(time, dayR);
        } else if(TermType.TERM_SIXHOURS.equals(termType)) {
            return compareTimeAndSixHours(time, dayR);
        } else if(TermType.TERM_DAILY.equals(termType)) {
            return compareTimeAndDay(time, dayR);
        } else if(TermType.TERM_WEEKLY.equals(termType)) {
            return compareTimeAndWeek(time, dayR);
        } else if(TermType.TERM_MONTHLY.equals(termType)) {
            return compareTimeAndMonth(time, dayR);
        } else {
            return 0;   // ????
        }
    }

    public static int compareTimeAndMinute(long time, String dayR)
    {
        String dayL = getDayHourMinute(time);
        return compare(dayL, dayR);        
    }
    public static int compareTimeAndTenMins(long time, String dayR)
    {
        String dayL = getDayHourTenMins(time);
        return compare(dayL, dayR);        
    }
    public static int compareTimeAndHour(long time, String dayR)
    {
        String dayL = getDayHour(time);
        return compare(dayL, dayR);        
    }
    public static int compareTimeAndSixHours(long time, String dayR)
    {
        String dayL = getDaySixHours(time);
        return compare(dayL, dayR);        
    }
    public static int compareTimeAndDay(long time, String dayR)
    {
        String dayL = getDay(time);
        return compare(dayL, dayR);        
    }
    public static int compareTimeAndWeek(long time, String dayR)
    {
        String dayL = getWeek(time);
        return compare(dayL, dayR);        
    }
    public static int compareTimeAndMonth(long time, String dayR)
    {
        String dayL = getMonth(time);
        return compare(dayL, dayR);        
    }
    

    // This has somewhat ambiguous semantics. Use the above, more specialized methods...
    private static int compare(String dayL, String dayR)
    {
        if(dayL == null && dayR == null) {
            return 0;
        }
        if(dayL == null) {
            return -1;
        }
        if(dayR == null) {
            return 1;
        }
        return dayL.compareTo(dayR);
    }
    
}
