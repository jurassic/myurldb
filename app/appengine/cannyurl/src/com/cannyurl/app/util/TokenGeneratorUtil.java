package com.cannyurl.app.util;

import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.sf.jsr107cache.Cache;

import com.cannyurl.af.util.CacheHelper;
import com.cannyurl.app.core.ReservedWordTokenSet;
import com.cannyurl.app.helper.ShortLinkAppHelper;
import com.cannyurl.app.sassy.SassyUrlUtil;
import com.cannyurl.common.TokenType;
import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.InternalServerErrorException;


public class TokenGeneratorUtil
{
    private static final Logger log = Logger.getLogger(TokenGeneratorUtil.class.getName());

    private TokenGeneratorUtil() {}

    
    
    // temporary
    private static Cache getCacheInstance()
    {
        return CacheHelper.getDayLongInstance().getCache();
    }

    private static String getCacheKeyForLastGeneratedSequentialToken(String domain)
    {
        String key = "ShortLinkAppService-LastGeneratedSequentialToken-" + domain;
        return key;
    }
    
    
    public static String generateRandomToken(String tokenType, String sassyTokenType)
    {
        String token = null;

        int maxTries = 5;
        int counter = 0;
        while(++counter <= maxTries) {
            // Generate a token.
            if(tokenType.equals(TokenType.TYPE_SASSY)) {
                // TBD: random vs unique.   ( sequential ???)
                token = SassyUrlUtil.generateSassyUrlToken(sassyTokenType);
            } else {
                // TBD: random vs unique vs sequential.
                token = ShortUrlUtil.generateToken(tokenType);
            }
            if(token != null) {   // Can token be null at this point????
                int tokLen = token.length();
                // TBD: Combine ReservedWordRegistry and ReservedWordTokenSet...
                Set<String> badWordSet = BadWordRegistry.getBadWordSet(tokLen);
                Set<String> reservedWordSet = ReservedWordRegistry.getReservedWordSet(tokLen);
                if((badWordSet == null || ! badWordSet.contains(token)) 
                        && (reservedWordSet == null || ! reservedWordSet.contains(token))
                        // && ! ReservedWordTokenSet.getInstance().isRservedWord(token)
                        ) {
                    break;
                }
            }
        }

        return token;
    }
    
    
    // Note:
    // If the sequential generation algorithm is used...
    // tokenType should be generally fixed for each user (or, domain)...
    // Otherwise, the "sequence" may be constantly changing (and, may end up generating the same/previously used tokens)
    // whenever we change the token type....
    

    // Returns the "next" token for the given domain.
    // Applies only for sequential tokenGenerationMethod...
    // Also, it makes most sense for user-specific domains...
    // In general, getting "sequential/next" token in a distributed/multi-user system 
    //   does not really make sense without some kind of locking, etc...  
    public static String generateNextSequentialToken(String domain, String tokenType) throws BaseException
    {
        return generateNextSequentialToken(domain, tokenType, null);
    }
    public static String generateNextSequentialToken(String domain, String tokenType, String currentToken) throws BaseException
    {
        String nextToken = null;

        if(currentToken == null || currentToken.isEmpty()) {
            try {
                // TBD:
                //   Because we use async saving, 
                //   the returned currentToken may not really be the last one created...
                // --> we'll end up creating the same (next) token again...
                // How to fix this????
                //   --> Use memcache...
                if(getCacheInstance() != null) {
                    currentToken = (String) getCacheInstance().get(getCacheKeyForLastGeneratedSequentialToken(domain));
                }
                if(currentToken == null) {
                    currentToken = ShortLinkAppHelper.getInstance().findTheLastTokenForDomain(domain);
                }
            } catch (BaseException bex) {
                // What to do ???
                log.log(Level.WARNING, "Failed get the currentToken for domain = " + domain);
                throw new InternalServerErrorException("Failed get the currentToken for domain = " + domain, bex);
            }
        }

        // currentToken == null means that
        // Maybe the "first" time (no token exists in the DB for the given domain)
        // so, do not filter out....
        // if(currentToken != null) {
            int maxTries = 20;
            int counter = 0;
            String referenceToken = currentToken;
            while(++counter <= maxTries) {
                nextToken = ShortUrlUtil.getNextSequentialToken(referenceToken, tokenType);
                if(nextToken != null) {   // Can token be null at this point????
                    int tokLen = nextToken.length();
                    // TBD: Combine ReservedWordRegistry and ReservedWordTokenSet...
                    Set<String> badWordSet = BadWordRegistry.getBadWordSet(tokLen);
                    Set<String> reservedWordSet = ReservedWordRegistry.getReservedWordSet(tokLen);
                    if((badWordSet != null && badWordSet.contains(nextToken)) 
                            || (reservedWordSet != null && reservedWordSet.contains(nextToken)) 
                            // || ReservedWordTokenSet.getInstance().isRservedWord(nextToken)
                            ) {
                        referenceToken = nextToken;
                    } else {
                        break;
                    }
                }
            }
            if(getCacheInstance() != null) {
                if(nextToken != null) {
                    getCacheInstance().put(getCacheKeyForLastGeneratedSequentialToken(domain), nextToken);
                } else {
                    getCacheInstance().remove(getCacheKeyForLastGeneratedSequentialToken(domain));
                }
            }
        // }

        if(log.isLoggable(Level.INFO)) log.info("nextToken generated: nextToken = " + nextToken + "; currentToken = " + currentToken + " for domain = " + domain);
        return nextToken;
    }
    


}
