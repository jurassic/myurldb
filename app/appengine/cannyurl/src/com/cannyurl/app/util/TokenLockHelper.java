package com.cannyurl.app.util;

import java.util.logging.Logger;

import net.sf.jsr107cache.Cache;

import com.cannyurl.af.util.CacheHelper;


// Temporary
public final class TokenLockHelper
{
    private static final Logger log = Logger.getLogger(TokenLockHelper.class.getName());


    // Singleton
    private TokenLockHelper() {}

    private static final class TokenLockHelperHolder
    {
        private static final TokenLockHelper INSTANCE = new TokenLockHelper();
    }
    public static TokenLockHelper getInstance()
    {
        return TokenLockHelperHolder.INSTANCE;
    }


    // temporary
    // Poorman's "global locking" using a distributed cache...
    private static Cache getCache()
    {
        // 30 seconds
        return CacheHelper.getFlashInstance().getCache();
    }


    private static String getShortUrlTokenLockKey(String domain, String token)
    {
        // Note: We assume that the arg domain/token is not null.
        return "ShortURL-Token-Lock-" + domain + "-" + token;
    }
    public String lockShortUrlToken(String domain, String token)
    {
        if(getCache() != null && token != null) {
            // The stored value is not important.
            getCache().put(getShortUrlTokenLockKey(domain, token), token);
            return token;
        } else {
            return null;
        }
    }
    public String unlockShortUrlToken(String domain, String token)
    {
        if(getCache() != null && token != null) {
            getCache().remove(getShortUrlTokenLockKey(domain, token));
            return token;
        } else {
            return null;
        }
    }
    public boolean isShortUrlTokenLocked(String domain, String token)
    {
        if(getCache() != null && token != null) {
            return getCache().containsKey(getShortUrlTokenLockKey(domain, token)); 
        } else {
            return false;
        }
    }

 
}
