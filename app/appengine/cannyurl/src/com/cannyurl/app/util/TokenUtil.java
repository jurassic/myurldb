package com.cannyurl.app.util;

import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cannyurl.common.TokenType;


public class TokenUtil
{
    private static final Logger log = Logger.getLogger(TokenUtil.class.getName());

    // temporary
    // TBD: Increase the length as more and more short urls are created....
    // Tentative ranges: tiny: 3-5, short: 4~6, medium: 5~8, long: 6~10,  digits: 7~12
    // Note: Use shorter length token for user-specific Short URLs, for each type. Eg, 3,4,5,6, etc. ????
    public static final int DEFAULT_TINYTOKEN_LENGTH = 3;
    public static final int DEFAULT_SHORTTOKEN_LENGTH = 4;
    public static final int DEFAULT_MEDIUMTOKEN_LENGTH = 5;
    public static final int DEFAULT_LONGTOKEN_LENGTH = 6;
    public static final int DEFAULT_DIGITTOKEN_LENGTH = 7;
    public static final int DEFAULT_VOWELTOKEN_LENGTH = 8;
    public static final int DEFAULT_ABCTOKEN_LENGTH = 9;
    public static final int DEFAULT_BINARYTOKEN_LENGTH = 10;
    // ....
    public static final int DEFAULT_SASSYTOKEN_MIXEDCASE_LENGTH = 5;
    public static final int DEFAULT_SASSYTOKEN_LOWERCAPS_LENGTH = 6;
    public static final int DEFAULT_SASSYTOKEN_LOWERCASE_LENGTH = 7;
    public static final int DEFAULT_SASSYTOKEN_LONGTOKEN_LENGTH = 8;

    // temporary
    //private static final int DEFAULT_CUSTOMTOKEN_MINLENGTH = 5;     // ??? Arbitrary...
    private static final int DEFAULT_CUSTOMTOKEN_MAXLENGTH = 100;   // ??? Arbitrary...
    private static final int DEFAULT_CUSTOMTOKEN_UPPERBOUND = 120;  // ??? Arbitrary...  DEFAULT_CUSTOMTOKEN_MAXLENGTH <= DEFAULT_CUSTOMTOKEN_UPPERBOUND
    
    
    // Static methods only.
    private TokenUtil() {}

    
    public static String sanitizeToken(String phrase)
    {
        return sanitizeToken(phrase, DEFAULT_CUSTOMTOKEN_MAXLENGTH);
    }
    
    
    // TBD:
    // In case of using 72 chars: a-z, A-Z, 0-9, and - $ _ . + ! * , ( )
    // also   ; / ? : @ = &   except maybe "?" ?????  --> 78 chars...
    // ????
    public static String sanitizeToken(String phrase, int cutoffLen)
    {
        if(phrase == null) {
            return "";
        }
        //phrase = phrase.toLowerCase();  // Use lower case only. ???
        phrase = phrase.replaceAll("[^a-zA-Z0-9/_\\-]", " ");    // Cf. ShortUrlUtil.getBase64Char().... Also "/" is allowed in a custom token...
        phrase = phrase.trim();
        if(phrase.length() == 0) {
            return "";
        }

        // Slash is allowed in a custom token, but not as a first or second char.
        while(phrase.startsWith("/")) {
            phrase = phrase.substring(1);
            phrase = phrase.trim();
        }
        if(phrase.length() > 1 && phrase.charAt(1) == '/') {
            phrase = phrase.replaceFirst("/", " ");
        }
        
        String[] words = phrase.split("\\s+");
        int totLen = 0;
        StringBuffer sb = new StringBuffer();
        for(int i=0; i<words.length && totLen<=cutoffLen; i++) {
            String word = words[i];
            sb.append(word);
            totLen += word.length();
            if(i<words.length-1 && totLen<=cutoffLen-1) {
                sb.append("-");
            }
        }
        
        String sanitized = sb.toString();
        int len = sanitized.length();
        if(len > DEFAULT_CUSTOMTOKEN_UPPERBOUND) {
            sanitized = sanitized.substring(0, DEFAULT_CUSTOMTOKEN_UPPERBOUND);
        }

        return sanitized;
    }

    
    // Returns the default length for a given token type.
    public static int getDefaultTokenLength(String tokenType)
    {
        int length;
        if(TokenType.TYPE_TINY.equals(tokenType)) {
            length = ConfigUtil.getTinyTokenLength();
        } else if(TokenType.TYPE_SHORT.equals(tokenType)) {
            length = ConfigUtil.getShortTokenLength();
        } else if(TokenType.TYPE_MEDIUM.equals(tokenType)) {
            length = ConfigUtil.getMediumTokenLength();
        } else if(TokenType.TYPE_LONG.equals(tokenType)) {
            length = ConfigUtil.getLongTokenLength();
        } else if(TokenType.TYPE_DIGITS.equals(tokenType)) {
            length = ConfigUtil.getDigitTokenLength();
        } else if(TokenType.TYPE_VOWELS.equals(tokenType)) {
            length = ConfigUtil.getVowelTokenLength();
        } else if(TokenType.TYPE_ABC.equals(tokenType)) {
            length = ConfigUtil.getAbcTokenLength();
        } else if(TokenType.TYPE_BINARY.equals(tokenType)) {
            length = ConfigUtil.getBinaryTokenLength();
        } else {
            // ????
            if(log.isLoggable(Level.FINE)) log.fine("Failed to get default token length for tokenType = " + tokenType + ". Retrying...");
            tokenType = ConfigUtil.getSystemDefaultTokenType();
            if(TokenType.TYPE_TINY.equals(tokenType)
                    || TokenType.TYPE_SHORT.equals(tokenType)
                    || TokenType.TYPE_MEDIUM.equals(tokenType)
                    || TokenType.TYPE_LONG.equals(tokenType)
                    || TokenType.TYPE_DIGITS.equals(tokenType)
                    || TokenType.TYPE_VOWELS.equals(tokenType)
                    || TokenType.TYPE_ABC.equals(tokenType)
                    || TokenType.TYPE_BINARY.equals(tokenType)
                    ) {
                length = getDefaultTokenLength(tokenType);   // ???
            } else {
                log.warning("Failed to get default token length for tokenType = " + tokenType);
                length = 0;
            }
        }
        return length;
    }


}
