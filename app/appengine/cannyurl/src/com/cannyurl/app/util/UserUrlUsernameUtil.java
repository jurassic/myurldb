package com.cannyurl.app.util;

import java.util.logging.Logger;


public class UserUrlUsernameUtil
{
    private static final Logger log = Logger.getLogger(DomainUtil.class.getName());

    // temporary
    private static final int DEFAULT_LENGTH = 5;  // ??? 6 or longer to avoid collision ????

    private UserUrlUsernameUtil() {}

    
    // To be used for a base username for UserUrlDomain....
    // Note that this is only temporary. To be used only when the user first registers on the site (without picking his/her own username).
    // User will be given a chance to select his/her own username from Options page.
    public static String generateRandomUsername()
    {
        // temporary
        //String username = ShortUrlUtil.generateLongUriToken(6);
        String username = ShortUrlUtil.generateMediumUriToken(DEFAULT_LENGTH);
        log.info("New random username has been created. username = " + username);
        return username;
    }
    
    
}
