package com.cannyurl.cert.af;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;


// OAuth consumer key registry.
public abstract class BaseOAuthConsumerRegistry
{
    private static final Logger log = Logger.getLogger(BaseOAuthConsumerRegistry.class.getName());

    // Consumer key-secret map.
    private Map<String, String> consumerSecretMap;

    protected Map<String, String> getBaseConsumerSecretMap()
    {
        consumerSecretMap = new HashMap<String, String>();

        // TBD:
        consumerSecretMap.put("3b935a44-2242-4b54-bdbb-c2f9be0582d1", "1af1fa8e-21c8-4adf-9d44-3fd163a81b3a");  // CannyUrlApp
        consumerSecretMap.put("c25fe05a-e860-42c9-8863-dd5fe08320dd", "fa9d8666-a2bb-47b3-b0f5-4120d814198f");  // CannyUrlApp + CannyUrlWeb
        consumerSecretMap.put("fdeadb18-a946-4c73-87ac-b3923c25c570", "d63f30e0-e7d3-44fe-9c5d-001690a976fa");  // CannyUrlApp + QueryClient
        // ...

        return consumerSecretMap;
    }
    protected Map<String, String> getConsumerSecretMap()
    {
        return consumerSecretMap;
    }

}
