package com.cannyurl.common;

import java.util.logging.Logger;


// AuthMode is a global system setting.
// Allowed values..
public class AppAuthMode
{
    private static final Logger log = Logger.getLogger(AppAuthMode.class.getName());

    private AppAuthMode() {}

    // TBD:
    public static final String MODE_UNKNOWN = "unknown";    // ???
    public static final String MODE_OPENID = "openid";      // GAE openId support (user needs to select open id provider
    public static final String MODE_GOOGLE = "google";      // Single auth provider is Google/gmail OpenID. ????
    public static final String MODE_GOOGLEPLUS = "googleplus";   // Single auth provider is Google+ SignIn (and, OAuth????)
    public static final String MODE_GOOGLEAPPS = "googleapps";   // Using Google Apps domain as an OpenID provider... ?????? Different from "google" ???
    public static final String MODE_TWITTER = "twitter";    // Single auth provider is Twitter OAuth.
    public static final String MODE_FACEBOOK = "facebook";  // Single auth provider is facebook connect...
    // Google apps
    // LinkedIn
    // etc...
    
    
    public static String getDefaultValue()
    {
        // ???
        return MODE_OPENID;
    }

}
