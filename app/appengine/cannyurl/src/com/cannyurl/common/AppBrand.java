package com.cannyurl.common;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import com.cannyurl.fe.bean.AppBrandStructJsBean;


// Poor man's branding...
// CannyURL: Use safe URLs by default
// FlashURL: Use flash URLs by default
// ShellURL: Use shell URLs by default
// FemtoURL: "Standard" URL shortener with multiple options.
// SassyURL: Sassy URL is a url comprising English word tokens...
// BrandURL: Company/user branded URLs ????
// UserURL: Username-based URLs.... (Include Twitter handle as well???)
// HostedURL: Company + company user URLs ????
// QuadURL: ??? Shortens multiple URLs into one set (single short URL for the set and/or a short URL for each long URL).
public class AppBrand
{
    private static final Logger log = Logger.getLogger(AppBrand.class.getName());

    private AppBrand() {}


    // TBD:
    public static final String BRAND_UNKNOWN = "unknown";  // ???
    public static final String BRAND_CANNYURL = "cannyurl";
    public static final String BRAND_FLASHURL = "flashurl";
    public static final String BRAND_SHELLURL = "shellurl";
    public static final String BRAND_FEMTOURL = "femtourl";
    public static final String BRAND_SASSYURL = "sassyurl";
    public static final String BRAND_BRANDURL = "brandurl";  // ???
    public static final String BRAND_USERURL = "userurl";
    public static final String BRAND_HOSTEDURL = "hostedurl";
    public static final String BRAND_QUADURL = "quadurl";   // ???
    // ...
    public static final String BRAND_KEYWORDNAV = "keywordnav";
    public static final String BRAND_SPEEDKEYWORD = "speedkeyword";
    public static final String BRAND_QUICKNAVURL = "quicknavurl";
    public static final String BRAND_QUICKNAVBAR = "quicknavbar";
    // ...
    public static final String BRAND_TWRIM = "twrim";    // Deprecated...
    public static final String BRAND_TWEETUSERURL = "tweetuserurl";
    public static final String BRAND_MYPLUSURL = "myplusurl";   // Google
    public static final String BRAND_PEERURL = "peerurl";       // Facebook?
    public static final String BRAND_SIGNEDURL = "signedurl";   // Email-based ??  signatureurl.com ? signedurl.com ?
    public static final String BRAND_TASKURL = "taskurl";
    public static final String BRAND_CUBBYURL = "cubbyurl";
    // ...
    public static final String BRAND_SMALLBIZURL = "smallbizurl";
    public static final String BRAND_ENTERPRISEURL = "enterpriseurl";
    // ...

    
    
    // Just use JsBean here (rahter than app bean)....
    private static final Map<String, AppBrandStructJsBean> sBrandStructMap = new HashMap<String, AppBrandStructJsBean>();
    static {
        sBrandStructMap.put(BRAND_CANNYURL, new AppBrandStructJsBean(
                BRAND_CANNYURL, 
                "Canny URL", 
                "Safe URL shortener service"
            ));
        sBrandStructMap.put(BRAND_FLASHURL, new AppBrandStructJsBean(
                BRAND_FLASHURL, 
                "Flash URL", 
                "A flash URL shortener service"
            ));
        sBrandStructMap.put(BRAND_SHELLURL, new AppBrandStructJsBean(
                BRAND_SHELLURL, 
                "Shell URL", 
                "A shell URL shortener service"
            ));
        sBrandStructMap.put(BRAND_FEMTOURL, new AppBrandStructJsBean(
                BRAND_FEMTOURL, 
                "Femto URL", 
                "A new kind of URL redirection service"
            ));
        sBrandStructMap.put(BRAND_SASSYURL, new AppBrandStructJsBean(
                BRAND_SASSYURL, 
                "Sassy URL", 
                "A new kind of URL shortener service"
            ));
        sBrandStructMap.put(BRAND_BRANDURL, new AppBrandStructJsBean(
                BRAND_BRANDURL, 
                "Brand URL", 
                "A branded URL shortener service"
            ));
        sBrandStructMap.put(BRAND_USERURL, new AppBrandStructJsBean(
                BRAND_USERURL, 
                "User URL", 
                "A vanity URL shortener service"
            ));
        sBrandStructMap.put(BRAND_HOSTEDURL, new AppBrandStructJsBean(
                BRAND_HOSTEDURL, 
                "Hosted URL", 
                "A hosted URL shortener service"
            ));
        sBrandStructMap.put(BRAND_QUADURL, new AppBrandStructJsBean(
                BRAND_QUADURL, 
                "Quad URL", 
                "A new URL shortener and redirect service"
            ));
        sBrandStructMap.put(BRAND_KEYWORDNAV, new AppBrandStructJsBean(
                BRAND_KEYWORDNAV, 
                "Keyword Nav", 
                "A keyword navigation service"
            ));
        sBrandStructMap.put(BRAND_SPEEDKEYWORD, new AppBrandStructJsBean(
                BRAND_SPEEDKEYWORD, 
                "Speed Keyword", 
                "A quick keyword navigation service"
            ));
        sBrandStructMap.put(BRAND_QUICKNAVURL, new AppBrandStructJsBean(
                BRAND_QUICKNAVURL, 
                "QuickNav URL", 
                "A quick navigation service"
            ));
        sBrandStructMap.put(BRAND_QUICKNAVBAR, new AppBrandStructJsBean(
                BRAND_QUICKNAVBAR, 
                "QuickNav Bar", 
                "A quick Web navigation bar"
            ));
        sBrandStructMap.put(BRAND_TWRIM, new AppBrandStructJsBean(
                BRAND_TWRIM, 
                "Twr.im", 
                "Personalized Twitter URL shortener service"
            ));
        sBrandStructMap.put(BRAND_TWEETUSERURL, new AppBrandStructJsBean(
                BRAND_TWEETUSERURL, 
                "Tweet User URL", 
                "Twitter Username URL shortener service"
            ));
        sBrandStructMap.put(BRAND_MYPLUSURL, new AppBrandStructJsBean(
                BRAND_MYPLUSURL, 
                "My Plus URL", 
                "My Plus URL shortener service"
            ));
        sBrandStructMap.put(BRAND_PEERURL, new AppBrandStructJsBean(
                BRAND_PEERURL, 
                "Peer URL", 
                "Personal Facebook URL shortener service"
            ));
        sBrandStructMap.put(BRAND_SIGNEDURL, new AppBrandStructJsBean(
                BRAND_SIGNEDURL, 
                "Signature URL", 
                "Signed URL shortener service"
            ));
        sBrandStructMap.put(BRAND_TASKURL, new AppBrandStructJsBean(
                BRAND_TASKURL, 
                "Task URL", 
                "A new kind of task URL service"
            ));
        sBrandStructMap.put(BRAND_CUBBYURL, new AppBrandStructJsBean(
                BRAND_CUBBYURL, 
                "Cubby URL", 
                "A personal URL service"
            ));
        sBrandStructMap.put(BRAND_SMALLBIZURL, new AppBrandStructJsBean(
                BRAND_SMALLBIZURL, 
                "Small Biz URL", 
                "Small business URL service"
            ));
        sBrandStructMap.put(BRAND_ENTERPRISEURL, new AppBrandStructJsBean(
                BRAND_ENTERPRISEURL, 
                "Enterprise URL", 
                "An enterprise URL service"
            ));

        // etc...
        
    }
    
    
    // temporary
    public static String getDefaultBrand()
    {
        return BRAND_CANNYURL;
    }
    
    // temporary
    public static boolean isValidBrand(String brand)
    {
        return sBrandStructMap.containsKey(brand);
    }
    
    // temporary
    public static String getDisplayName(String brand)
    {
        if(isValidBrand(brand)) {
            return sBrandStructMap.get(brand).getName();
        } else {
            // ????
            return "";
        }
    }

    // temporary
    public static String getBrandDescription(String brand)
    {
        if(isValidBrand(brand)) {
            return sBrandStructMap.get(brand).getDescription();
        } else {
            // ????
            return "";
        }
    }

    // temporary
    public static AppBrandStructJsBean getBrandStruct(String brand)
    {
        if(isValidBrand(brand)) {
            return sBrandStructMap.get(brand);
        } else {
            // ????
            return null;
        }
    }


}
