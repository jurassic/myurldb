package com.cannyurl.common;

import java.util.logging.Logger;


// CannyUrl is really a combination of many different kinds of apps.
// These constants are used to enable/disable certain apps 
//      and/or to choose the "primary" app for a given brand, etc...
public class AppMode
{
    private static final Logger log = Logger.getLogger(AppMode.class.getName());

    private AppMode() {}

    // TBD:
    public static final String MODE_URLSHORTENER = "urlshortener";
    public static final String MODE_BOOKMARKING = "bookmarking";
    public static final String MODE_NAVKEYWORD = "navkeyword";
    public static final String MODE_INVITEURL = "inviteurl";
    // etc... 
    // ....

    
    public static String getDefaultMode()
    {
        // temporary
        return MODE_URLSHORTENER;
    }
    
}
