package com.cannyurl.common;

import java.util.logging.Logger;


// Primarily to be used as values for UserAuthState.authStatus....
public class AuthStatus
{
    private static final Logger log = Logger.getLogger(AuthStatus.class.getName());

    private AuthStatus() {}

    
    // Temporary
    public static final String STATUS_AUTHENTICATED = "authenticated";
    public static final String STATUS_AUTH_ABORTED = "auth_aborted";
    public static final String STATUS_AUTH_FAILED = "auth_failed";
    public static final String STATUS_EXPIRED = "expired";
    // etc...
    // ...

    
}
