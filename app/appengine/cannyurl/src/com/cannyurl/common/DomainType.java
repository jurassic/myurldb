package com.cannyurl.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;


// TBD: Use enum???
// Note: "Domain" in CannyUrl includes everything up to, and excluding, "path" and "token".
// "Domain type" is mainly used for input param (in case Domain param hasn't been explicitly set).
// Note that domainType==custom does not make sense when domain param is not set.
//      that domainType==default or username does not make sense when the user is not authenticated, etc....
// Note also that custom domain should be validated in some way (e.g., against DB), and it cannot be arbitrary...
// Note:
// "Userurl" is the form "http://a.bc/username/", and it CANNOT coexist with other types in a single deployed app.
//    (Note: ConfigUtil.isUseDomainUserURL() == true )
// In theory, the "/username/" part need not be a user's username (e.g. company name),
// and can be shared among multiple users.... sort of like a "branded" domain, in a way...
// (But, we do not need to support such use cases at this point....)
// ...
// Note that "custom" makes sense only as an input when short URL is created.
// During parsing, domain should conform to a certain particular form....
// (So, maybe custom should not be used at all?????) 
public class DomainType
{
    private static final Logger log = Logger.getLogger(DomainType.class.getName());

    private DomainType() {}

    public static final String TYPE_GENERIC = "generic";  // == type unknown  --> Invalid type....
    // ...
    public static final String TYPE_DEFAULT = "default";     // ??? what is "default" ??? single value instead of random ????
    public static final String TYPE_RANDOM = "random";       // random domain from the set of domains in the config...
    public static final String TYPE_USERNAME = "username";   // "http://a.bc/~username/"
    public static final String TYPE_USERCODE = "usercode";   // "http://a.bc/~usercode/"
    public static final String TYPE_TWITTER = "twitter";     // "http://a.bc/@twitterhandle/"
    public static final String TYPE_GOOGLE = "google";       // "http://a.bc/+gmailid/"
    public static final String TYPE_FACEBOOK = "facebook";   // "http://a.bc/-facebookusername/"    // facebook profile/email/page name. ???
    public static final String TYPE_EMAIL = "email";         // "http://a.bc/!emailaddress/"        // e.g., abc@xyz.com
    // TBD: Deprecated???
    // Use ConfigUtil.getDomainTypeSubdomainOrPath()
    // public static final String TYPE_SUBDOMAIN = "subdomain"; // Username based subdomain... e.g., "http://username.a.bc/"  ????  
    // ....
    public static final String TYPE_USERURL = "userurl";     // "http://a.bc/username|usercode/"  
    public static final String TYPE_BRANDED = "branded";     // "http://a.brand.co/"   personal branding vs app/client branding ???  
    public static final String TYPE_CUSTOM = "custom";       // ????
    // ...
    // TBD: Google apps domain ?????? It is branded????
    // Or, domain part is determined by the config/client setting, and the domain type is actually one of username, etc...
    // TBD:
    // ????  branded (subdomain) +  username/usercode (path) ????
    public static final String TYPE_APPCLIENT = "appclient";     // branded for app client ????
    // public static final String TYPE_APPUSER = "appuser";
    public static final String TYPE_APPUSERNAME = "appusername";
    public static final String TYPE_APPUSERCODE = "appusercode";
    // public static final String TYPE_APPUSEREMAIL = "appuseremail";
    // ????
    // ...
    public static final String TYPE_DEFAULT_LABEL = "User default domain";
    public static final String TYPE_RANDOM_LABEL = "System-assigned domain";
    public static final String TYPE_USERNAME_LABEL = "Username-based domain";
    public static final String TYPE_USERCODE_LABEL = "Usercode-based domain";
    public static final String TYPE_TWITTER_LABEL = "Twitter username based domain";
    public static final String TYPE_GOOGLE_LABEL = "Google gmail based domain";
    public static final String TYPE_FACEBOOK_LABEL = "Facebook username based domain";
    public static final String TYPE_EMAIL_LABEL = "Email based domain";
    public static final String TYPE_SUBDOMAIN_LABEL = "Username-based subdomain";
    public static final String TYPE_USERURL_LABEL = "Username-based branded domain";
    public static final String TYPE_BRANDED_LABEL = "Branded custom domain";
    public static final String TYPE_CUSTOM_LABEL = "User provided custom domain";

    // Type map.
    private static final Map<String, String> sTypeMap = new HashMap<String, String>();
    static {
        sTypeMap.put(TYPE_DEFAULT, TYPE_DEFAULT_LABEL);
        sTypeMap.put(TYPE_RANDOM, TYPE_RANDOM_LABEL);
        sTypeMap.put(TYPE_USERNAME, TYPE_USERNAME_LABEL);
        sTypeMap.put(TYPE_USERCODE, TYPE_USERCODE_LABEL);
        sTypeMap.put(TYPE_TWITTER, TYPE_TWITTER_LABEL);
        sTypeMap.put(TYPE_GOOGLE, TYPE_GOOGLE_LABEL);
        sTypeMap.put(TYPE_FACEBOOK, TYPE_FACEBOOK_LABEL);
        sTypeMap.put(TYPE_EMAIL, TYPE_EMAIL_LABEL);
        // sTypeMap.put(TYPE_SUBDOMAIN, TYPE_SUBDOMAIN_LABEL);
        sTypeMap.put(TYPE_USERURL, TYPE_USERURL_LABEL);
        sTypeMap.put(TYPE_BRANDED, TYPE_BRANDED_LABEL);
        sTypeMap.put(TYPE_CUSTOM, TYPE_CUSTOM_LABEL);
    }

    // Primarily for HTML Select element...
    public static final List<String[]> TYPES = new ArrayList<String[]>();
    static {
        TYPES.add(new String[]{TYPE_DEFAULT, TYPE_DEFAULT_LABEL});
        TYPES.add(new String[]{TYPE_RANDOM, TYPE_RANDOM_LABEL});
        TYPES.add(new String[]{TYPE_USERNAME, TYPE_USERNAME_LABEL});
        TYPES.add(new String[]{TYPE_USERCODE, TYPE_USERCODE_LABEL});
        TYPES.add(new String[]{TYPE_TWITTER, TYPE_TWITTER_LABEL});
        TYPES.add(new String[]{TYPE_GOOGLE, TYPE_GOOGLE_LABEL});
        TYPES.add(new String[]{TYPE_FACEBOOK, TYPE_FACEBOOK_LABEL});
        TYPES.add(new String[]{TYPE_EMAIL, TYPE_EMAIL_LABEL});
        // TYPES.add(new String[]{TYPE_SUBDOMAIN, TYPE_SUBDOMAIN_LABEL});
        TYPES.add(new String[]{TYPE_USERURL, TYPE_USERURL_LABEL});
        TYPES.add(new String[]{TYPE_BRANDED, TYPE_BRANDED_LABEL});
        TYPES.add(new String[]{TYPE_CUSTOM, TYPE_CUSTOM_LABEL});
        // ..
    }

    // TBD
    public static String getDefaultType()
    {
        // temporary
        return TYPE_RANDOM;
    }

    public static boolean isValidType(String type)
    {
        if(sTypeMap.containsKey(type)) {
            return true;
        } else {
            return false;
        }
    }

    public static String getLabel(String type)
    {
        return sTypeMap.get(type);
    }

}
