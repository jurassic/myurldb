package com.cannyurl.common;

import java.util.logging.Logger;


public class HashType
{
    private static final Logger log = Logger.getLogger(HashType.class.getName());

    private HashType() {}


    // Temporary
    public static final String TYPE_MD2 = "MD2";
    public static final String TYPE_MD5 = "MD5";
    public static final String TYPE_SHA1 = "SHA-1";
    public static final String TYPE_SHA256 = "SHA-256";
    public static final String TYPE_SHA384 = "SHA-384";
    public static final String TYPE_SHA512 = "SHA-512";
    // ...


    // TBD:
    public static boolean isSupportedAlgorithm(String hashType)
    {
        if(hashType == null) {
            return false;
        }
        if(hashType.equals(TYPE_MD2) 
                || hashType.equals(TYPE_MD5)
                || hashType.equals(TYPE_SHA1)
                || hashType.equals(TYPE_SHA256)
                || hashType.equals(TYPE_SHA384)
                || hashType.equals(TYPE_SHA512)
            ) {
            return true;
        } else {
            return false;
        }
    }

}
