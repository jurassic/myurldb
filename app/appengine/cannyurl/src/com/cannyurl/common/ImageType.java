package com.cannyurl.common;

import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;


// Image Mime types.
public class ImageType
{
    private static final Logger log = Logger.getLogger(ImageType.class.getName());

    private ImageType() {}

    // TBD
    public static final String TYPE_PNG = "image/png";
    public static final String TYPE_JPEG = "image/jpeg";
    public static final String TYPE_GIF = "image/gif";
    public static final String TYPE_BMP = "image/bmp";
    // Etc...
    // ...
    
    private static final Set<String> sTypes = new HashSet<String>();
    static {
        sTypes.add(TYPE_PNG);
        sTypes.add(TYPE_JPEG);
        sTypes.add(TYPE_GIF);
        sTypes.add(TYPE_BMP);
    }
    
    public static boolean isValid(String type)
    {
        if(sTypes.contains(type)) {
            return true;
        } else {
            return false;
        }
    }
    
    public static String getDefaultType()
    {
        // temporary
        return TYPE_PNG;
    }


}
