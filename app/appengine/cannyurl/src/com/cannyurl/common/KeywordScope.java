package com.cannyurl.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;


// TBD: Use enum???
public class KeywordScope
{
    private static final Logger log = Logger.getLogger(KeywordScope.class.getName());

    private KeywordScope() {}

    public static final String SCOPE_GENERIC = "generic";  // == type unknown  --> Invalid type....
    // ...
    public static final String SCOPE_USER = "user";
    public static final String SCOPE_GROUP = "group";
    public static final String SCOPE_CROWD = "crowd";
    public static final String SCOPE_SYSTEM = "system";   // Predefined keyword set ... ?????
    // ...
    public static final String SCOPE_USER_NAME = "Personal keywords";
    public static final String SCOPE_GROUP_NAME = "Organization keywords";
    public static final String SCOPE_CROWD_NAME = "Global keywords";
    public static final String SCOPE_SYSTEM_NAME = "System keywords";  // ????
    // ...
    public static final String SCOPE_USER_LABEL = "Personal scope";
    public static final String SCOPE_GROUP_LABEL = "Organization scope";
    public static final String SCOPE_CROWD_LABEL = "Global scope";
    public static final String SCOPE_SYSTEM_LABEL = "System scope";  // ????

    // Type map.
    private static final Map<String, String> sTypeMap = new HashMap<String, String>();
    static {
        sTypeMap.put(SCOPE_USER, SCOPE_USER_LABEL);
        sTypeMap.put(SCOPE_GROUP, SCOPE_GROUP_LABEL);
        sTypeMap.put(SCOPE_CROWD, SCOPE_CROWD_LABEL);
        sTypeMap.put(SCOPE_SYSTEM, SCOPE_SYSTEM_LABEL);
    }

    // Primarily for HTML Select element...
    public static final List<String[]> TYPES = new ArrayList<String[]>();
    static {
        TYPES.add(new String[]{SCOPE_USER, SCOPE_USER_NAME});
        TYPES.add(new String[]{SCOPE_GROUP, SCOPE_GROUP_NAME});
        TYPES.add(new String[]{SCOPE_CROWD, SCOPE_CROWD_NAME});
        TYPES.add(new String[]{SCOPE_SYSTEM, SCOPE_SYSTEM_NAME});
        // ..
    }
    // Primarily for HTML Select element...
    public static final List<String[]> SCOPES = new ArrayList<String[]>();
    static {
        SCOPES.add(new String[]{SCOPE_USER, SCOPE_USER_LABEL});
        SCOPES.add(new String[]{SCOPE_GROUP, SCOPE_GROUP_LABEL});
        SCOPES.add(new String[]{SCOPE_CROWD, SCOPE_CROWD_LABEL});
        SCOPES.add(new String[]{SCOPE_SYSTEM, SCOPE_SYSTEM_LABEL});
        // ..
    }

    // TBD
    public static String getDefaultType()
    {
        // temporary
        return SCOPE_CROWD;
    }

    public static boolean isValidType(String type)
    {
        if(sTypeMap.containsKey(type)) {
            return true;
        } else {
            return false;
        }
    }

    public static String getLabel(String type)
    {
        return sTypeMap.get(type);
    }

}
