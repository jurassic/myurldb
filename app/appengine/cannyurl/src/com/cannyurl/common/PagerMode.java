package com.cannyurl.common;

import java.util.logging.Logger;


// TBD: Use enum ???
public class PagerMode
{
    private static final Logger log = Logger.getLogger(PagerMode.class.getName());

    private PagerMode() {}

    // TBD:
    public static final String MODE_UNKNOWN = "unknown";  // ???

    public static final String MODE_PAGE = "page";        // page + page size
    public static final String MODE_OFFSET = "offset";    // offset + count
    // ...
    
}
