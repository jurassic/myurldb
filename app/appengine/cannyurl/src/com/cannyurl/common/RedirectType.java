package com.cannyurl.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;


//TBD: Use enum???
public class RedirectType
{
    private static final Logger log = Logger.getLogger(RedirectType.class.getName());

    private RedirectType() {}

    // TBD
    public static final String TYPE_GENERIC = "generic";  // == type unknown  --> Invalid type....
    // ...
    public static final String TYPE_PERMANENT = "permanent";  // 301
    public static final String TYPE_TEMPORARY = "temporary";  // 302
    public static final String TYPE_FLASH = "flash";          // Delayed redirect.
    public static final String TYPE_CONFIRM = "confirm";      // User confirm
    // ...
    public static final String TYPE_VERIFY = "verify";        // This is not exactly a redirect type. The page merely displays the short url information. Similar to Confirm (without follow/cancel buttons??)
    public static final String TYPE_INFO = "info";            // This is not exactly a redirect type either. 
    // One letter "token"...
    public static final String TYPE_PERMANENT_TOKEN = "P";
    public static final String TYPE_TEMPORARY_TOKEN = "T";
    public static final String TYPE_FLASH_TOKEN = "F";
    public static final String TYPE_CONFIRM_TOKEN = "C";
    // ....
    public static final String TYPE_PERMANENT_LABEL = "Redirect automatically (301)";
    public static final String TYPE_TEMPORARY_LABEL = "Redirect automatically (302)";
    public static final String TYPE_FLASH_LABEL = "Delayed redirect (Flash)";
    public static final String TYPE_CONFIRM_LABEL = "User confirmation required";
    // ....


    // Token map.
    private static final Map<String, String> sTypeTokenMap = new HashMap<String, String>();
    static {
        sTypeTokenMap.put(TYPE_PERMANENT, TYPE_PERMANENT_TOKEN);
        sTypeTokenMap.put(TYPE_TEMPORARY, TYPE_TEMPORARY_TOKEN);
        sTypeTokenMap.put(TYPE_FLASH, TYPE_FLASH_TOKEN);
        sTypeTokenMap.put(TYPE_CONFIRM, TYPE_CONFIRM_TOKEN);
    }

    // Type map.
    private static final Map<String, String> sTypeMap = new HashMap<String, String>();
    static {
        sTypeMap.put(TYPE_PERMANENT, TYPE_PERMANENT_LABEL);
        sTypeMap.put(TYPE_TEMPORARY, TYPE_TEMPORARY_LABEL);
        sTypeMap.put(TYPE_FLASH, TYPE_FLASH_LABEL);
        sTypeMap.put(TYPE_CONFIRM, TYPE_CONFIRM_LABEL);
    }

    // Primarily for HTML Select element...
    public static final List<String[]> TYPES = new ArrayList<String[]>();
    static {
        TYPES.add(new String[]{TYPE_PERMANENT, TYPE_PERMANENT_LABEL});
        TYPES.add(new String[]{TYPE_TEMPORARY, TYPE_TEMPORARY_LABEL});
        TYPES.add(new String[]{TYPE_FLASH, TYPE_FLASH_LABEL});
        TYPES.add(new String[]{TYPE_CONFIRM, TYPE_CONFIRM_LABEL});
        // ..
    }

    // temporary
    public static String getDefaultType()
    {
        // ????
        return TYPE_CONFIRM;
        //return TYPE_PERMANENT;
    }

    public static String getToken(String type)
    {
        return sTypeTokenMap.get(type);
    }

    public static String getLabel(String type)
    {
        return sTypeMap.get(type);
    }

    public static boolean isValidType(String type)
    {
        if(sTypeMap.containsKey(type)) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean canIncludeMessage(String type)
    {
        if( TYPE_FLASH.equals(type) || TYPE_CONFIRM.equals(type) ) {
            return true;
        } else {
            return false;
        }
    }

}
