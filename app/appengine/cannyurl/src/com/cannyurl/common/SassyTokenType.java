package com.cannyurl.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;


// TBD: Use enum???
public class SassyTokenType
{
    private static final Logger log = Logger.getLogger(SassyTokenType.class.getName());

    private SassyTokenType() {}

    public static final String TYPE_GENERIC = "generic";  // == type unknown  --> Invalid type....
    // ...
    public static final String TYPE_MIXEDCASE = "mixedcase";
    public static final String TYPE_LOWERCAPS = "lowercaps";
    public static final String TYPE_LOWERCASE = "lowercase";
    public static final String TYPE_LONGTOKEN = "longtoken";
    // ...
    public static final String TYPE_MIXEDCASE_LABEL = "Mixed Case";
    public static final String TYPE_LOWERCAPS_LABEL = "Allow Caps";
    public static final String TYPE_LOWERCASE_LABEL = "Lowercases Only";
    public static final String TYPE_LONGTOKEN_LABEL = "Longer Token";

    // Type map.
    private static final Map<String, String> sTypeMap = new HashMap<String, String>();
    static {
        sTypeMap.put(TYPE_MIXEDCASE, TYPE_MIXEDCASE_LABEL);
        sTypeMap.put(TYPE_LOWERCAPS, TYPE_LOWERCAPS_LABEL);
        sTypeMap.put(TYPE_LOWERCASE, TYPE_LOWERCASE_LABEL);
        sTypeMap.put(TYPE_LONGTOKEN, TYPE_LONGTOKEN_LABEL);
    }

    // Primarily for HTML Select element...
    public static final List<String[]> TYPES = new ArrayList<String[]>();
    static {
        TYPES.add(new String[]{TYPE_MIXEDCASE, TYPE_MIXEDCASE_LABEL});
        TYPES.add(new String[]{TYPE_LOWERCAPS, TYPE_LOWERCAPS_LABEL});
        TYPES.add(new String[]{TYPE_LOWERCASE, TYPE_LOWERCASE_LABEL});
        TYPES.add(new String[]{TYPE_LONGTOKEN, TYPE_LONGTOKEN_LABEL});
        // ..
    }

    // TBD
    public static String getDefaultType()
    {
        // temporary
        return TYPE_MIXEDCASE;
    }

    public static boolean isValidType(String type)
    {
        if(sTypeMap.containsKey(type)) {
            return true;
        } else {
            return false;
        }
    }

    public static String getLabel(String type)
    {
        return sTypeMap.get(type);
    }

}
