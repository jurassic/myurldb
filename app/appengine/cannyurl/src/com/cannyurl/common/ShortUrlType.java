package com.cannyurl.common;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;


// To be deleted.....


// vs. token type ????
// TBD: Use token type ????
/* public */ class ShortUrlType
{
    private static final Logger log = Logger.getLogger(ShortUrlType.class.getName());

    private ShortUrlType() {}

    public static final String TYPE_GENERIC = "generic";  // == type unknown
    // ...
    public static final String TYPE_SHORT = "short";
    public static final String TYPE_MEDIUM = "medium";
    public static final String TYPE_LONG = "long";
    public static final String TYPE_CUSTOM = "custom";
    // ...
    public static final String TYPE_SHORT_LABEL = "As Short As Possible";
    public static final String TYPE_MEDIUM_LABEL = "Use Alphabets Only";
    public static final String TYPE_LONG_LABEL = "Use Numbers Only";
    public static final String TYPE_CUSTOM_LABEL = "Specify Custom Path";  // custom token.

    // TBD
    public static final String getDefaultType()
    {
        // temporary
        return TYPE_SHORT;
    }

    // TBD
    private static final List<String[]> TYPES = new ArrayList<String[]>();
    static {
        TYPES.add(new String[]{TYPE_SHORT_LABEL, TYPE_SHORT});
        TYPES.add(new String[]{TYPE_MEDIUM_LABEL, TYPE_MEDIUM});
        TYPES.add(new String[]{TYPE_LONG_LABEL, TYPE_LONG});
        TYPES.add(new String[]{TYPE_CUSTOM_LABEL, TYPE_CUSTOM});
        // ..
    }

}
