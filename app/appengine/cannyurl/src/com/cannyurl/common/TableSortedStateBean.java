package com.cannyurl.common;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;


// To keep track of the sorted order for each column in a table. 
// We maintain no more than one bean per session/user. 
// Supports one table per "page" (same requestUrl), 
//    or, all column names/ids should be different across all tables in a page. 
public class TableSortedStateBean implements Serializable
{
	// requestUrl -> { column -> order }
	// Order: +1: asc
	//        -1: desc
	//        null: undefined
	private Map<String, Map<String, Integer>> mSortOrderMap = null;


	public TableSortedStateBean()
	{
		mSortOrderMap = new HashMap<String, Map<String, Integer>>();
	}

	
	public Map<String, Map<String, Integer>> getSortOrderMap()
	{
		return mSortOrderMap;
	}

	public Map<String, Integer> getColumnOrderMap(String requestUrl)
	{
		Map<String, Integer> map = mSortOrderMap.get(requestUrl);
		if(map == null) {
			map = new HashMap<String, Integer>();
			mSortOrderMap.put(requestUrl, map);
		}
		return map;
	}	

}
