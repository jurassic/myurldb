package com.cannyurl.common;

import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;


public class TermStruct
{
    private static final Logger log = Logger.getLogger(TermStruct.class.getName());

    private String termType;
    private String dayHour;

    
    public TermStruct(String termType, String dayHour)
    {
        super();
        this.termType = termType;
        this.dayHour = dayHour;
    }

    
    public String getTermType()
    {
        return termType;
    }
    public void setTermType(String termType)
    {
        this.termType = termType;
    }

    public String getDayHour()
    {
        return dayHour;
    }
    public void setDayHour(String dayHour)
    {
        this.dayHour = dayHour;
    }


}
