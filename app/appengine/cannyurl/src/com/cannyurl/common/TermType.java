package com.cannyurl.common;

import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

public class TermType
{
    private static final Logger log = Logger.getLogger(TermType.class.getName());

    private TermType() {}

    // TBD
    // public static final String TERM_YEARLY = "yearly";
    public static final String TERM_MONTHLY = "monthly";
    public static final String TERM_WEEKLY = "weekly";
    public static final String TERM_DAILY = "daily";
    // public static final String TERM_HALFDAY = "halfday";
    public static final String TERM_SIXHOURS = "sixhours";
    public static final String TERM_HOURLY = "hourly";
    public static final String TERM_TENMINS = "tenmins";
    public static final String TERM_MINUTE = "minute";
    public static final String TERM_CUMULATIVE = "cumulative";
    public static final String TERM_CURRENT = "current";
    
    private static final Set<String> sTypes = new HashSet<String>();
    static {
        // sTypes.add(TERM_YEARLY);
        sTypes.add(TERM_MONTHLY);
        sTypes.add(TERM_WEEKLY);
        sTypes.add(TERM_DAILY);
        // sTypes.add(TERM_HALFDAY);
        sTypes.add(TERM_SIXHOURS);
        sTypes.add(TERM_HOURLY);
        sTypes.add(TERM_TENMINS);
        sTypes.add(TERM_MINUTE);
        sTypes.add(TERM_CUMULATIVE);
        sTypes.add(TERM_CURRENT);
    }
    
    public static boolean isValid(String type)
    {
        if(sTypes.contains(type)) {
            return true;
        } else {
            return false;
        }
    }
    
    public static String getDefaultType()
    {
        // temporary
        //return TERM_CURRENT;
        return TERM_HOURLY;
    }

    
    public static String getWiderTerm(String type)
    {
        if(TERM_MINUTE.equals(type)) {
            return TERM_TENMINS;
        } else if(TERM_TENMINS.equals(type)) {
            return TERM_HOURLY;
        } else if(TERM_HOURLY.equals(type)) {
            return TERM_SIXHOURS;
        } else if(TERM_SIXHOURS.equals(type)) {
            return TERM_DAILY;
        } else if(TERM_DAILY.equals(type)) {
            return TERM_WEEKLY;
        } else if(TERM_WEEKLY.equals(type)) {
            return TERM_MONTHLY;
//        } else if(TERM_MONTHLY.equals(type)) {
//            return TERM_YEARLY;
        }
        return null;
    }
    
    public static String getNarrowerTerm(String type)
    {
//        if(TERM_YEARLY.equals(type)) {
//            return TERM_MONTHLY; } else 
        if(TERM_MONTHLY.equals(type)) {
            return TERM_WEEKLY;
        } else if(TERM_WEEKLY.equals(type)) {
            return TERM_DAILY;
        } else if(TERM_DAILY.equals(type)) {
            return TERM_SIXHOURS;
        } else if(TERM_SIXHOURS.equals(type)) {
            return TERM_HOURLY;
        } else if(TERM_HOURLY.equals(type)) {
            return TERM_TENMINS;
        } else if(TERM_TENMINS.equals(type)) {
            return TERM_MINUTE;
        }
        return null;
    }
    
    
    public static long getTimeSpanMillis(String type)
    {
//        if(TERM_YEARLY.equals(type)) {
//            return 365 * 24 * 3600 * 1000L;      // ????
        if(TERM_MONTHLY.equals(type)) {
            return 30 * 24 * 3600 * 1000L;      // ????
        } else if(TERM_WEEKLY.equals(type)) {
            return 7 * 24 * 3600 * 1000L;
        } else if(TERM_DAILY.equals(type)) {
            return 24 * 3600 * 1000L;
        } else if(TERM_SIXHOURS.equals(type)) {
            return 6 * 3600 * 1000L;
        } else if(TERM_HOURLY.equals(type)) {
            return 3600 * 1000L;
        } else if(TERM_TENMINS.equals(type)) {
            return 600 * 1000L;
        } else if(TERM_MINUTE.equals(type)) {
            return 60 * 1000L;
        }
        return 0L;
    }

}
