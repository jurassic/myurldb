package com.cannyurl.common;

import java.util.logging.Logger;


// TBD:
// Note that TokenGenerationMethod cannot really be changed once the system is set up/deployed.
// Mixing them together may not make sense (at least, in the current implementation)....
// ....
// Random: Generates a "random" token without checking if the token is already in the DB.
//         for custom type, neither random nor sequential is supported. It's always "unique".
// Note: Unique has been made a separate flag. Now we can use Random+unique and Sequential+unique...
// Unique: Generates a random token, but if the token has been used (that is, it is in the DB), try different tokens until it finds a new one.
//         We can think of two subtypes: token being unique across all domains, or token+domain being unique... We use the latter option....
// Sequential: Generates a token according to a certain order (e.g., alphabetical, etc..)
//             Sequential is generally useful for username based/use-specific domains.
// TBD: Note that the sequence is affected by token type (alpha, alpha-number, etc...)...
//      How to determine the "next" token ??????
//     (also, what about sassy token type ????)
public class TokenGenerationMethod
{
    private static final Logger log = Logger.getLogger(TokenGenerationMethod.class.getName());

    private TokenGenerationMethod() {}

    // TBD:
    public static final String METHOD_RANDOM = "random";
    // public static final String METHOD_UNIQUE = "unique";
    public static final String METHOD_SEQUENTIAL = "sequential";
    // TBD: Sassy/Phrase, etc.. Are these considered algorithms????
    // etc...
    // ....

    
    
    public static String getDefaultMethod()
    {
        // temporary
        return METHOD_RANDOM;
    }

    
}
