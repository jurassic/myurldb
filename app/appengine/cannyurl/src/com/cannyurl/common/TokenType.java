package com.cannyurl.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;


// TBD: Use enum???
public class TokenType
{
    private static final Logger log = Logger.getLogger(TokenType.class.getName());

    private TokenType() {}

    public static final String TYPE_GENERIC = "generic";  // == type unknown  --> Invalid type....
    // ...
    public static final String TYPE_TINY = "tiny";
    public static final String TYPE_SHORT = "short";
    public static final String TYPE_MEDIUM = "medium";
    public static final String TYPE_LONG = "long";
    public static final String TYPE_DIGITS = "digits";
    public static final String TYPE_VOWELS = "vowels";    // For testing only
    public static final String TYPE_ABC = "abc";          // For testing only
    public static final String TYPE_BINARY = "binary";    // For testing only
    public static final String TYPE_SASSY = "sassy";
    public static final String TYPE_PHRASE = "phrase"; // "keyword/phrase" from a long url or the target web page...
    public static final String TYPE_HASH = "hash";     // Note the "hash" type token cannot be used with shared domains.. because it is the same for a given long URL...
    public static final String TYPE_SEQUENTIAL = "sequential";
    public static final String TYPE_CUSTOM = "custom";
    public static final String TYPE_KEYWORD = "keyword";
    // ...
    public static final String TYPE_TINY_LABEL = "Make It Even Shorter";
    public static final String TYPE_SHORT_LABEL = "As Short As Possible";
    public static final String TYPE_MEDIUM_LABEL = "Alphabets and Numbers";
    public static final String TYPE_LONG_LABEL = "Use Alphabets Only";
    public static final String TYPE_DIGITS_LABEL = "Use Numbers Only";
    public static final String TYPE_VOWELS_LABEL = "Use Vowels Only";
    public static final String TYPE_ABC_LABEL = "Use ABC Only";
    public static final String TYPE_BINARY_LABEL = "Use 0/1 Only";
    public static final String TYPE_SASSY_LABEL = "Use English Word Combos"; 
    public static final String TYPE_PHRASE_LABEL = "Use Keyword Phrase";    // "Use Keyword Phrase from the Target URL/Page"
    public static final String TYPE_HASH_LABEL = "Use Hash Value of Long URL"; 
    public static final String TYPE_SEQUENTIAL_LABEL = "Sequentially generated token"; 
    public static final String TYPE_CUSTOM_LABEL = "Specify Custom Token";  // custom token.
    public static final String TYPE_KEYWORD_LABEL = "Navigation Keyword";   // Same as custom token????

    // Type map.
    private static final Map<String, String> sTypeMap = new HashMap<String, String>();
    static {
        sTypeMap.put(TYPE_TINY, TYPE_TINY_LABEL);
        sTypeMap.put(TYPE_SHORT, TYPE_SHORT_LABEL);
        sTypeMap.put(TYPE_MEDIUM, TYPE_MEDIUM_LABEL);
        sTypeMap.put(TYPE_LONG, TYPE_LONG_LABEL);
        sTypeMap.put(TYPE_DIGITS, TYPE_DIGITS_LABEL);
        sTypeMap.put(TYPE_VOWELS, TYPE_VOWELS_LABEL);
        sTypeMap.put(TYPE_ABC, TYPE_ABC_LABEL);
        sTypeMap.put(TYPE_BINARY, TYPE_BINARY_LABEL);
        sTypeMap.put(TYPE_SASSY, TYPE_SASSY_LABEL);
        // sTypeMap.put(TYPE_PHRASE, TYPE_PHRASE_LABEL);    // ???
        // sTypeMap.put(TYPE_HASH, TYPE_HASH_LABEL);        // ???
        sTypeMap.put(TYPE_SEQUENTIAL, TYPE_SEQUENTIAL_LABEL);
        sTypeMap.put(TYPE_CUSTOM, TYPE_CUSTOM_LABEL);
        sTypeMap.put(TYPE_KEYWORD, TYPE_KEYWORD_LABEL);
    }

    // Primarily for HTML Select element...
    public static final List<String[]> TYPES = new ArrayList<String[]>();
    static {
        TYPES.add(new String[]{TYPE_TINY, TYPE_TINY_LABEL});
        TYPES.add(new String[]{TYPE_SHORT, TYPE_SHORT_LABEL});
        TYPES.add(new String[]{TYPE_MEDIUM, TYPE_MEDIUM_LABEL});
        TYPES.add(new String[]{TYPE_LONG, TYPE_LONG_LABEL});
        TYPES.add(new String[]{TYPE_DIGITS, TYPE_DIGITS_LABEL});
        TYPES.add(new String[]{TYPE_VOWELS, TYPE_VOWELS_LABEL});
        TYPES.add(new String[]{TYPE_ABC, TYPE_ABC_LABEL});
        TYPES.add(new String[]{TYPE_BINARY, TYPE_BINARY_LABEL});
        TYPES.add(new String[]{TYPE_SASSY, TYPE_SASSY_LABEL});
        // TYPES.add(new String[]{TYPE_PHRASE, TYPE_PHRASE_LABEL});    // ???
        // TYPES.add(new String[]{TYPE_HASH, TYPE_HASH_LABEL});    // ???
        TYPES.add(new String[]{TYPE_SEQUENTIAL, TYPE_SEQUENTIAL_LABEL});
        TYPES.add(new String[]{TYPE_CUSTOM, TYPE_CUSTOM_LABEL});
        TYPES.add(new String[]{TYPE_KEYWORD, TYPE_KEYWORD_LABEL});
       // ..
    }

    // TBD
    public static String getDefaultType()
    {
        // temporary
        return TYPE_MEDIUM;
    }

    public static boolean isValidType(String type)
    {
        if(sTypeMap.containsKey(type)) {
            return true;
        } else {
            return false;
        }
    }

    public static String getLabel(String type)
    {
        return sTypeMap.get(type);
    }

}
