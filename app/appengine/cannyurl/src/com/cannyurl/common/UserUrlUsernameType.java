package com.cannyurl.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;


//TBD: Use enum???
public class UserUrlUsernameType
{
    private static final Logger log = Logger.getLogger(UserUrlUsernameType.class.getName());

    private UserUrlUsernameType() {}

    public static final String TYPE_GENERIC = "generic";  // == type unknown  --> Invalid type....
    // ...
    public static final String TYPE_SYSTEM = "system";
    public static final String TYPE_USERPICK = "userpick";
    public static final String TYPE_TWITTER = "twitter";
    public static final String TYPE_CUSTOM = "custom";  // ???
    // ...
    public static final String TYPE_SYSTEM_LABEL = "Use application username";
    public static final String TYPE_USERPICK_LABEL = "Use user-picked string";
    public static final String TYPE_TWITTER_LABEL = "Use Twitter handle";
    public static final String TYPE_CUSTOM_LABEL = "Use custom user id";

    // Type map.
    private static final Map<String, String> sTypeMap = new HashMap<String, String>();
    static {
        sTypeMap.put(TYPE_SYSTEM, TYPE_SYSTEM_LABEL);
        sTypeMap.put(TYPE_USERPICK, TYPE_USERPICK_LABEL);
        sTypeMap.put(TYPE_TWITTER, TYPE_TWITTER_LABEL);
        sTypeMap.put(TYPE_CUSTOM, TYPE_CUSTOM_LABEL);
    }

    // Primarily for HTML Select element...
    public static final List<String[]> TYPES = new ArrayList<String[]>();
    static {
        TYPES.add(new String[]{TYPE_SYSTEM, TYPE_SYSTEM_LABEL});
        TYPES.add(new String[]{TYPE_USERPICK, TYPE_USERPICK_LABEL});
        TYPES.add(new String[]{TYPE_TWITTER, TYPE_TWITTER_LABEL});
        TYPES.add(new String[]{TYPE_CUSTOM, TYPE_CUSTOM_LABEL});
        // ..
    }

    // TBD
    public static String getDefaultType()
    {
        // temporary
        return TYPE_SYSTEM;
    }

    public static boolean isValidType(String type)
    {
        if(sTypeMap.containsKey(type)) {
            return true;
        } else {
            return false;
        }
    }

    public static String getLabel(String type)
    {
        return sTypeMap.get(type);
    }

}
