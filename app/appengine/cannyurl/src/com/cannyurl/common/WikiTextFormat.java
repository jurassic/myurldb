package com.cannyurl.common;

import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;


public class WikiTextFormat
{
    private static final Logger log = Logger.getLogger(WikiTextFormat.class.getName());

    private WikiTextFormat() {}


    // TBD
    public static final String FORMAT_MEDIAWIKI = "MediaWiki";    // http://www.mediawiki.org/wiki/Help:Formatting
    public static final String FORMAT_TEXTILE = "Textile";        // http://en.wikipedia.org/wiki/Textile_(markup_language)
    public static final String FORMAT_TWIKI = "TWiki";            // http://twiki.org/cgi-bin/view/TWiki/TextFormattingRules
    public static final String FORMAT_TRACWIKI = "TracWiki";      // http://trac.edgewall.org/wiki/WikiFormatting
    public static final String FORMAT_GOOGLECODE = "GoogleCode";  // http://code.google.com/p/support/wiki/WikiSyntax
    public static final String FORMAT_MARKDOWN = "MarkDown";      // http://en.wikipedia.org/wiki/Markdown
    public static final String FORMAT_CREOLE = "Creole";          // http://en.wikipedia.org/wiki/Creole_(markup)
    // ....

    // TBD
    private static final Set<String> sFormats;
    static {
        sFormats = new HashSet<String>();
        sFormats.add(FORMAT_MEDIAWIKI);
        sFormats.add(FORMAT_TEXTILE);
        sFormats.add(FORMAT_TWIKI);
        sFormats.add(FORMAT_TRACWIKI);
        sFormats.add(FORMAT_GOOGLECODE);
        sFormats.add(FORMAT_MARKDOWN);
        sFormats.add(FORMAT_CREOLE);
        // ...
    };
    
    // temporary
    public static String getDefaultFormat()
    {
        // ???
        //return FORMAT_TEXTILE;
        return FORMAT_TWIKI;
    }

    public static boolean isValid(String format)
    {
        if(sFormats.contains(format)) {
            return true;
        } else {
            return false;
        }
    }
    
}
