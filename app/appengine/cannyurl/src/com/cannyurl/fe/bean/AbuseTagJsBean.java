package com.cannyurl.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.cannyurl.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class AbuseTagJsBean implements Serializable, Cloneable  //, AbuseTag
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(AbuseTagJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String guid;
    private String shortLink;
    private String shortUrl;
    private String longUrl;
    private String longUrlHash;
    private String user;
    private String ipAddress;
    private Integer tag;
    private String comment;
    private String status;
    private String reviewer;
    private String action;
    private String note;
    private String pastActions;
    private Long reviewedTime;
    private Long actionTime;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public AbuseTagJsBean()
    {
        //this((String) null);
    }
    public AbuseTagJsBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public AbuseTagJsBean(String guid, String shortLink, String shortUrl, String longUrl, String longUrlHash, String user, String ipAddress, Integer tag, String comment, String status, String reviewer, String action, String note, String pastActions, Long reviewedTime, Long actionTime)
    {
        this(guid, shortLink, shortUrl, longUrl, longUrlHash, user, ipAddress, tag, comment, status, reviewer, action, note, pastActions, reviewedTime, actionTime, null, null);
    }
    public AbuseTagJsBean(String guid, String shortLink, String shortUrl, String longUrl, String longUrlHash, String user, String ipAddress, Integer tag, String comment, String status, String reviewer, String action, String note, String pastActions, Long reviewedTime, Long actionTime, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.shortLink = shortLink;
        this.shortUrl = shortUrl;
        this.longUrl = longUrl;
        this.longUrlHash = longUrlHash;
        this.user = user;
        this.ipAddress = ipAddress;
        this.tag = tag;
        this.comment = comment;
        this.status = status;
        this.reviewer = reviewer;
        this.action = action;
        this.note = note;
        this.pastActions = pastActions;
        this.reviewedTime = reviewedTime;
        this.actionTime = actionTime;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public AbuseTagJsBean(AbuseTagJsBean bean)
    {
        if(bean != null) {
            setGuid(bean.getGuid());
            setShortLink(bean.getShortLink());
            setShortUrl(bean.getShortUrl());
            setLongUrl(bean.getLongUrl());
            setLongUrlHash(bean.getLongUrlHash());
            setUser(bean.getUser());
            setIpAddress(bean.getIpAddress());
            setTag(bean.getTag());
            setComment(bean.getComment());
            setStatus(bean.getStatus());
            setReviewer(bean.getReviewer());
            setAction(bean.getAction());
            setNote(bean.getNote());
            setPastActions(bean.getPastActions());
            setReviewedTime(bean.getReviewedTime());
            setActionTime(bean.getActionTime());
            setCreatedTime(bean.getCreatedTime());
            setModifiedTime(bean.getModifiedTime());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static AbuseTagJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        AbuseTagJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(AbuseTagJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, AbuseTagJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public String getShortLink()
    {
        return this.shortLink;
    }
    public void setShortLink(String shortLink)
    {
        this.shortLink = shortLink;
    }

    public String getShortUrl()
    {
        return this.shortUrl;
    }
    public void setShortUrl(String shortUrl)
    {
        this.shortUrl = shortUrl;
    }

    public String getLongUrl()
    {
        return this.longUrl;
    }
    public void setLongUrl(String longUrl)
    {
        this.longUrl = longUrl;
    }

    public String getLongUrlHash()
    {
        return this.longUrlHash;
    }
    public void setLongUrlHash(String longUrlHash)
    {
        this.longUrlHash = longUrlHash;
    }

    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    public String getIpAddress()
    {
        return this.ipAddress;
    }
    public void setIpAddress(String ipAddress)
    {
        this.ipAddress = ipAddress;
    }

    public Integer getTag()
    {
        return this.tag;
    }
    public void setTag(Integer tag)
    {
        this.tag = tag;
    }

    public String getComment()
    {
        return this.comment;
    }
    public void setComment(String comment)
    {
        this.comment = comment;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getReviewer()
    {
        return this.reviewer;
    }
    public void setReviewer(String reviewer)
    {
        this.reviewer = reviewer;
    }

    public String getAction()
    {
        return this.action;
    }
    public void setAction(String action)
    {
        this.action = action;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    public String getPastActions()
    {
        return this.pastActions;
    }
    public void setPastActions(String pastActions)
    {
        this.pastActions = pastActions;
    }

    public Long getReviewedTime()
    {
        return this.reviewedTime;
    }
    public void setReviewedTime(Long reviewedTime)
    {
        this.reviewedTime = reviewedTime;
    }

    public Long getActionTime()
    {
        return this.actionTime;
    }
    public void setActionTime(Long actionTime)
    {
        this.actionTime = actionTime;
    }

    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:null, ");
        sb.append("shortLink:null, ");
        sb.append("shortUrl:null, ");
        sb.append("longUrl:null, ");
        sb.append("longUrlHash:null, ");
        sb.append("user:null, ");
        sb.append("ipAddress:null, ");
        sb.append("tag:0, ");
        sb.append("comment:null, ");
        sb.append("status:null, ");
        sb.append("reviewer:null, ");
        sb.append("action:null, ");
        sb.append("note:null, ");
        sb.append("pastActions:null, ");
        sb.append("reviewedTime:0, ");
        sb.append("actionTime:0, ");
        sb.append("createdTime:0, ");
        sb.append("modifiedTime:0, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:");
        if(this.getGuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGuid()).append("\", ");
        }
        sb.append("shortLink:");
        if(this.getShortLink() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getShortLink()).append("\", ");
        }
        sb.append("shortUrl:");
        if(this.getShortUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getShortUrl()).append("\", ");
        }
        sb.append("longUrl:");
        if(this.getLongUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getLongUrl()).append("\", ");
        }
        sb.append("longUrlHash:");
        if(this.getLongUrlHash() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getLongUrlHash()).append("\", ");
        }
        sb.append("user:");
        if(this.getUser() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUser()).append("\", ");
        }
        sb.append("ipAddress:");
        if(this.getIpAddress() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getIpAddress()).append("\", ");
        }
        sb.append("tag:" + this.getTag()).append(", ");
        sb.append("comment:");
        if(this.getComment() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getComment()).append("\", ");
        }
        sb.append("status:");
        if(this.getStatus() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getStatus()).append("\", ");
        }
        sb.append("reviewer:");
        if(this.getReviewer() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getReviewer()).append("\", ");
        }
        sb.append("action:");
        if(this.getAction() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getAction()).append("\", ");
        }
        sb.append("note:");
        if(this.getNote() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNote()).append("\", ");
        }
        sb.append("pastActions:");
        if(this.getPastActions() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getPastActions()).append("\", ");
        }
        sb.append("reviewedTime:" + this.getReviewedTime()).append(", ");
        sb.append("actionTime:" + this.getActionTime()).append(", ");
        sb.append("createdTime:" + this.getCreatedTime()).append(", ");
        sb.append("modifiedTime:" + this.getModifiedTime()).append(", ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getGuid() != null) {
            sb.append("\"guid\":").append("\"").append(this.getGuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"guid\":").append("null, ");
        }
        if(this.getShortLink() != null) {
            sb.append("\"shortLink\":").append("\"").append(this.getShortLink()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"shortLink\":").append("null, ");
        }
        if(this.getShortUrl() != null) {
            sb.append("\"shortUrl\":").append("\"").append(this.getShortUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"shortUrl\":").append("null, ");
        }
        if(this.getLongUrl() != null) {
            sb.append("\"longUrl\":").append("\"").append(this.getLongUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"longUrl\":").append("null, ");
        }
        if(this.getLongUrlHash() != null) {
            sb.append("\"longUrlHash\":").append("\"").append(this.getLongUrlHash()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"longUrlHash\":").append("null, ");
        }
        if(this.getUser() != null) {
            sb.append("\"user\":").append("\"").append(this.getUser()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"user\":").append("null, ");
        }
        if(this.getIpAddress() != null) {
            sb.append("\"ipAddress\":").append("\"").append(this.getIpAddress()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"ipAddress\":").append("null, ");
        }
        if(this.getTag() != null) {
            sb.append("\"tag\":").append("").append(this.getTag()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"tag\":").append("null, ");
        }
        if(this.getComment() != null) {
            sb.append("\"comment\":").append("\"").append(this.getComment()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"comment\":").append("null, ");
        }
        if(this.getStatus() != null) {
            sb.append("\"status\":").append("\"").append(this.getStatus()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"status\":").append("null, ");
        }
        if(this.getReviewer() != null) {
            sb.append("\"reviewer\":").append("\"").append(this.getReviewer()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"reviewer\":").append("null, ");
        }
        if(this.getAction() != null) {
            sb.append("\"action\":").append("\"").append(this.getAction()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"action\":").append("null, ");
        }
        if(this.getNote() != null) {
            sb.append("\"note\":").append("\"").append(this.getNote()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"note\":").append("null, ");
        }
        if(this.getPastActions() != null) {
            sb.append("\"pastActions\":").append("\"").append(this.getPastActions()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"pastActions\":").append("null, ");
        }
        if(this.getReviewedTime() != null) {
            sb.append("\"reviewedTime\":").append("").append(this.getReviewedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"reviewedTime\":").append("null, ");
        }
        if(this.getActionTime() != null) {
            sb.append("\"actionTime\":").append("").append(this.getActionTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"actionTime\":").append("null, ");
        }
        if(this.getCreatedTime() != null) {
            sb.append("\"createdTime\":").append("").append(this.getCreatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"createdTime\":").append("null, ");
        }
        if(this.getModifiedTime() != null) {
            sb.append("\"modifiedTime\":").append("").append(this.getModifiedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"modifiedTime\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("guid = " + this.guid).append(";");
        sb.append("shortLink = " + this.shortLink).append(";");
        sb.append("shortUrl = " + this.shortUrl).append(";");
        sb.append("longUrl = " + this.longUrl).append(";");
        sb.append("longUrlHash = " + this.longUrlHash).append(";");
        sb.append("user = " + this.user).append(";");
        sb.append("ipAddress = " + this.ipAddress).append(";");
        sb.append("tag = " + this.tag).append(";");
        sb.append("comment = " + this.comment).append(";");
        sb.append("status = " + this.status).append(";");
        sb.append("reviewer = " + this.reviewer).append(";");
        sb.append("action = " + this.action).append(";");
        sb.append("note = " + this.note).append(";");
        sb.append("pastActions = " + this.pastActions).append(";");
        sb.append("reviewedTime = " + this.reviewedTime).append(";");
        sb.append("actionTime = " + this.actionTime).append(";");
        sb.append("createdTime = " + this.createdTime).append(";");
        sb.append("modifiedTime = " + this.modifiedTime).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        AbuseTagJsBean cloned = new AbuseTagJsBean();
        cloned.setGuid(null);          // Do not clone the guid.
        cloned.setShortLink(this.getShortLink());   
        cloned.setShortUrl(this.getShortUrl());   
        cloned.setLongUrl(this.getLongUrl());   
        cloned.setLongUrlHash(this.getLongUrlHash());   
        cloned.setUser(this.getUser());   
        cloned.setIpAddress(this.getIpAddress());   
        cloned.setTag(this.getTag());   
        cloned.setComment(this.getComment());   
        cloned.setStatus(this.getStatus());   
        cloned.setReviewer(this.getReviewer());   
        cloned.setAction(this.getAction());   
        cloned.setNote(this.getNote());   
        cloned.setPastActions(this.getPastActions());   
        cloned.setReviewedTime(this.getReviewedTime());   
        cloned.setActionTime(this.getActionTime());   
        cloned.setCreatedTime(null);   // Reset the timestamp.
        cloned.setModifiedTime(null);   // Reset the timestamp.
        return cloned;
    }

}
