package com.cannyurl.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.cannyurl.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class AppClientJsBean implements Serializable, Cloneable  //, AppClient
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(AppClientJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String guid;
    private String owner;
    private String admin;
    private String name;
    private String clientId;
    private String clientCode;
    private String rootDomain;
    private String appDomain;
    private Boolean useAppDomain;
    private Boolean enabled;
    private String licenseMode;
    private String status;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public AppClientJsBean()
    {
        //this((String) null);
    }
    public AppClientJsBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public AppClientJsBean(String guid, String owner, String admin, String name, String clientId, String clientCode, String rootDomain, String appDomain, Boolean useAppDomain, Boolean enabled, String licenseMode, String status)
    {
        this(guid, owner, admin, name, clientId, clientCode, rootDomain, appDomain, useAppDomain, enabled, licenseMode, status, null, null);
    }
    public AppClientJsBean(String guid, String owner, String admin, String name, String clientId, String clientCode, String rootDomain, String appDomain, Boolean useAppDomain, Boolean enabled, String licenseMode, String status, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.owner = owner;
        this.admin = admin;
        this.name = name;
        this.clientId = clientId;
        this.clientCode = clientCode;
        this.rootDomain = rootDomain;
        this.appDomain = appDomain;
        this.useAppDomain = useAppDomain;
        this.enabled = enabled;
        this.licenseMode = licenseMode;
        this.status = status;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public AppClientJsBean(AppClientJsBean bean)
    {
        if(bean != null) {
            setGuid(bean.getGuid());
            setOwner(bean.getOwner());
            setAdmin(bean.getAdmin());
            setName(bean.getName());
            setClientId(bean.getClientId());
            setClientCode(bean.getClientCode());
            setRootDomain(bean.getRootDomain());
            setAppDomain(bean.getAppDomain());
            setUseAppDomain(bean.isUseAppDomain());
            setEnabled(bean.isEnabled());
            setLicenseMode(bean.getLicenseMode());
            setStatus(bean.getStatus());
            setCreatedTime(bean.getCreatedTime());
            setModifiedTime(bean.getModifiedTime());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static AppClientJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        AppClientJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(AppClientJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, AppClientJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public String getOwner()
    {
        return this.owner;
    }
    public void setOwner(String owner)
    {
        this.owner = owner;
    }

    public String getAdmin()
    {
        return this.admin;
    }
    public void setAdmin(String admin)
    {
        this.admin = admin;
    }

    public String getName()
    {
        return this.name;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getClientId()
    {
        return this.clientId;
    }
    public void setClientId(String clientId)
    {
        this.clientId = clientId;
    }

    public String getClientCode()
    {
        return this.clientCode;
    }
    public void setClientCode(String clientCode)
    {
        this.clientCode = clientCode;
    }

    public String getRootDomain()
    {
        return this.rootDomain;
    }
    public void setRootDomain(String rootDomain)
    {
        this.rootDomain = rootDomain;
    }

    public String getAppDomain()
    {
        return this.appDomain;
    }
    public void setAppDomain(String appDomain)
    {
        this.appDomain = appDomain;
    }

    public Boolean isUseAppDomain()
    {
        return this.useAppDomain;
    }
    public void setUseAppDomain(Boolean useAppDomain)
    {
        this.useAppDomain = useAppDomain;
    }

    public Boolean isEnabled()
    {
        return this.enabled;
    }
    public void setEnabled(Boolean enabled)
    {
        this.enabled = enabled;
    }

    public String getLicenseMode()
    {
        return this.licenseMode;
    }
    public void setLicenseMode(String licenseMode)
    {
        this.licenseMode = licenseMode;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:null, ");
        sb.append("owner:null, ");
        sb.append("admin:null, ");
        sb.append("name:null, ");
        sb.append("clientId:null, ");
        sb.append("clientCode:null, ");
        sb.append("rootDomain:null, ");
        sb.append("appDomain:null, ");
        sb.append("useAppDomain:false, ");
        sb.append("enabled:false, ");
        sb.append("licenseMode:null, ");
        sb.append("status:null, ");
        sb.append("createdTime:0, ");
        sb.append("modifiedTime:0, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:");
        if(this.getGuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGuid()).append("\", ");
        }
        sb.append("owner:");
        if(this.getOwner() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getOwner()).append("\", ");
        }
        sb.append("admin:");
        if(this.getAdmin() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getAdmin()).append("\", ");
        }
        sb.append("name:");
        if(this.getName() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getName()).append("\", ");
        }
        sb.append("clientId:");
        if(this.getClientId() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getClientId()).append("\", ");
        }
        sb.append("clientCode:");
        if(this.getClientCode() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getClientCode()).append("\", ");
        }
        sb.append("rootDomain:");
        if(this.getRootDomain() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getRootDomain()).append("\", ");
        }
        sb.append("appDomain:");
        if(this.getAppDomain() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getAppDomain()).append("\", ");
        }
        sb.append("useAppDomain:" + this.isUseAppDomain()).append(", ");
        sb.append("enabled:" + this.isEnabled()).append(", ");
        sb.append("licenseMode:");
        if(this.getLicenseMode() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getLicenseMode()).append("\", ");
        }
        sb.append("status:");
        if(this.getStatus() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getStatus()).append("\", ");
        }
        sb.append("createdTime:" + this.getCreatedTime()).append(", ");
        sb.append("modifiedTime:" + this.getModifiedTime()).append(", ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getGuid() != null) {
            sb.append("\"guid\":").append("\"").append(this.getGuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"guid\":").append("null, ");
        }
        if(this.getOwner() != null) {
            sb.append("\"owner\":").append("\"").append(this.getOwner()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"owner\":").append("null, ");
        }
        if(this.getAdmin() != null) {
            sb.append("\"admin\":").append("\"").append(this.getAdmin()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"admin\":").append("null, ");
        }
        if(this.getName() != null) {
            sb.append("\"name\":").append("\"").append(this.getName()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"name\":").append("null, ");
        }
        if(this.getClientId() != null) {
            sb.append("\"clientId\":").append("\"").append(this.getClientId()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"clientId\":").append("null, ");
        }
        if(this.getClientCode() != null) {
            sb.append("\"clientCode\":").append("\"").append(this.getClientCode()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"clientCode\":").append("null, ");
        }
        if(this.getRootDomain() != null) {
            sb.append("\"rootDomain\":").append("\"").append(this.getRootDomain()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"rootDomain\":").append("null, ");
        }
        if(this.getAppDomain() != null) {
            sb.append("\"appDomain\":").append("\"").append(this.getAppDomain()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"appDomain\":").append("null, ");
        }
        if(this.isUseAppDomain() != null) {
            sb.append("\"useAppDomain\":").append("").append(this.isUseAppDomain()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"useAppDomain\":").append("null, ");
        }
        if(this.isEnabled() != null) {
            sb.append("\"enabled\":").append("").append(this.isEnabled()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"enabled\":").append("null, ");
        }
        if(this.getLicenseMode() != null) {
            sb.append("\"licenseMode\":").append("\"").append(this.getLicenseMode()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"licenseMode\":").append("null, ");
        }
        if(this.getStatus() != null) {
            sb.append("\"status\":").append("\"").append(this.getStatus()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"status\":").append("null, ");
        }
        if(this.getCreatedTime() != null) {
            sb.append("\"createdTime\":").append("").append(this.getCreatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"createdTime\":").append("null, ");
        }
        if(this.getModifiedTime() != null) {
            sb.append("\"modifiedTime\":").append("").append(this.getModifiedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"modifiedTime\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("guid = " + this.guid).append(";");
        sb.append("owner = " + this.owner).append(";");
        sb.append("admin = " + this.admin).append(";");
        sb.append("name = " + this.name).append(";");
        sb.append("clientId = " + this.clientId).append(";");
        sb.append("clientCode = " + this.clientCode).append(";");
        sb.append("rootDomain = " + this.rootDomain).append(";");
        sb.append("appDomain = " + this.appDomain).append(";");
        sb.append("useAppDomain = " + this.useAppDomain).append(";");
        sb.append("enabled = " + this.enabled).append(";");
        sb.append("licenseMode = " + this.licenseMode).append(";");
        sb.append("status = " + this.status).append(";");
        sb.append("createdTime = " + this.createdTime).append(";");
        sb.append("modifiedTime = " + this.modifiedTime).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        AppClientJsBean cloned = new AppClientJsBean();
        cloned.setGuid(null);          // Do not clone the guid.
        cloned.setOwner(this.getOwner());   
        cloned.setAdmin(this.getAdmin());   
        cloned.setName(this.getName());   
        cloned.setClientId(this.getClientId());   
        cloned.setClientCode(this.getClientCode());   
        cloned.setRootDomain(this.getRootDomain());   
        cloned.setAppDomain(this.getAppDomain());   
        cloned.setUseAppDomain(this.isUseAppDomain());   
        cloned.setEnabled(this.isEnabled());   
        cloned.setLicenseMode(this.getLicenseMode());   
        cloned.setStatus(this.getStatus());   
        cloned.setCreatedTime(null);   // Reset the timestamp.
        cloned.setModifiedTime(null);   // Reset the timestamp.
        return cloned;
    }

}
