package com.cannyurl.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.cannyurl.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class BookmarkCrowdTallyJsBean extends CrowdTallyBaseJsBean implements Serializable, Cloneable  //, BookmarkCrowdTally
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(BookmarkCrowdTallyJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String bookmarkFolder;
    private String contentTag;
    private String referenceElement;
    private String elementType;
    private String keywordLink;

    // Ctors.
    public BookmarkCrowdTallyJsBean()
    {
        //this((String) null);
    }
    public BookmarkCrowdTallyJsBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BookmarkCrowdTallyJsBean(String guid, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, String tallyDate, String status, String note, Long expirationTime, String bookmarkFolder, String contentTag, String referenceElement, String elementType, String keywordLink)
    {
        this(guid, user, shortLink, domain, token, longUrl, shortUrl, tallyDate, status, note, expirationTime, bookmarkFolder, contentTag, referenceElement, elementType, keywordLink, null, null);
    }
    public BookmarkCrowdTallyJsBean(String guid, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, String tallyDate, String status, String note, Long expirationTime, String bookmarkFolder, String contentTag, String referenceElement, String elementType, String keywordLink, Long createdTime, Long modifiedTime)
    {
        super(guid, user, shortLink, domain, token, longUrl, shortUrl, tallyDate, status, note, expirationTime, createdTime, modifiedTime);

        this.bookmarkFolder = bookmarkFolder;
        this.contentTag = contentTag;
        this.referenceElement = referenceElement;
        this.elementType = elementType;
        this.keywordLink = keywordLink;
    }
    public BookmarkCrowdTallyJsBean(BookmarkCrowdTallyJsBean bean)
    {
        if(bean != null) {
            setGuid(bean.getGuid());
            setUser(bean.getUser());
            setShortLink(bean.getShortLink());
            setDomain(bean.getDomain());
            setToken(bean.getToken());
            setLongUrl(bean.getLongUrl());
            setShortUrl(bean.getShortUrl());
            setTallyDate(bean.getTallyDate());
            setStatus(bean.getStatus());
            setNote(bean.getNote());
            setExpirationTime(bean.getExpirationTime());
            setBookmarkFolder(bean.getBookmarkFolder());
            setContentTag(bean.getContentTag());
            setReferenceElement(bean.getReferenceElement());
            setElementType(bean.getElementType());
            setKeywordLink(bean.getKeywordLink());
            setCreatedTime(bean.getCreatedTime());
            setModifiedTime(bean.getModifiedTime());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static BookmarkCrowdTallyJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        BookmarkCrowdTallyJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(BookmarkCrowdTallyJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, BookmarkCrowdTallyJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    public String getUser()
    {
        return super.getUser();
    }
    public void setUser(String user)
    {
        super.setUser(user);
    }

    public String getShortLink()
    {
        return super.getShortLink();
    }
    public void setShortLink(String shortLink)
    {
        super.setShortLink(shortLink);
    }

    public String getDomain()
    {
        return super.getDomain();
    }
    public void setDomain(String domain)
    {
        super.setDomain(domain);
    }

    public String getToken()
    {
        return super.getToken();
    }
    public void setToken(String token)
    {
        super.setToken(token);
    }

    public String getLongUrl()
    {
        return super.getLongUrl();
    }
    public void setLongUrl(String longUrl)
    {
        super.setLongUrl(longUrl);
    }

    public String getShortUrl()
    {
        return super.getShortUrl();
    }
    public void setShortUrl(String shortUrl)
    {
        super.setShortUrl(shortUrl);
    }

    public String getTallyDate()
    {
        return super.getTallyDate();
    }
    public void setTallyDate(String tallyDate)
    {
        super.setTallyDate(tallyDate);
    }

    public String getStatus()
    {
        return super.getStatus();
    }
    public void setStatus(String status)
    {
        super.setStatus(status);
    }

    public String getNote()
    {
        return super.getNote();
    }
    public void setNote(String note)
    {
        super.setNote(note);
    }

    public Long getExpirationTime()
    {
        return super.getExpirationTime();
    }
    public void setExpirationTime(Long expirationTime)
    {
        super.setExpirationTime(expirationTime);
    }

    public String getBookmarkFolder()
    {
        return this.bookmarkFolder;
    }
    public void setBookmarkFolder(String bookmarkFolder)
    {
        this.bookmarkFolder = bookmarkFolder;
    }

    public String getContentTag()
    {
        return this.contentTag;
    }
    public void setContentTag(String contentTag)
    {
        this.contentTag = contentTag;
    }

    public String getReferenceElement()
    {
        return this.referenceElement;
    }
    public void setReferenceElement(String referenceElement)
    {
        this.referenceElement = referenceElement;
    }

    public String getElementType()
    {
        return this.elementType;
    }
    public void setElementType(String elementType)
    {
        this.elementType = elementType;
    }

    public String getKeywordLink()
    {
        return this.keywordLink;
    }
    public void setKeywordLink(String keywordLink)
    {
        this.keywordLink = keywordLink;
    }

    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:null, ");
        sb.append("user:null, ");
        sb.append("shortLink:null, ");
        sb.append("domain:null, ");
        sb.append("token:null, ");
        sb.append("longUrl:null, ");
        sb.append("shortUrl:null, ");
        sb.append("tallyDate:null, ");
        sb.append("status:null, ");
        sb.append("note:null, ");
        sb.append("expirationTime:0, ");
        sb.append("bookmarkFolder:null, ");
        sb.append("contentTag:null, ");
        sb.append("referenceElement:null, ");
        sb.append("elementType:null, ");
        sb.append("keywordLink:null, ");
        sb.append("createdTime:0, ");
        sb.append("modifiedTime:0, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:");
        if(this.getGuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGuid()).append("\", ");
        }
        sb.append("user:");
        if(this.getUser() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUser()).append("\", ");
        }
        sb.append("shortLink:");
        if(this.getShortLink() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getShortLink()).append("\", ");
        }
        sb.append("domain:");
        if(this.getDomain() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDomain()).append("\", ");
        }
        sb.append("token:");
        if(this.getToken() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getToken()).append("\", ");
        }
        sb.append("longUrl:");
        if(this.getLongUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getLongUrl()).append("\", ");
        }
        sb.append("shortUrl:");
        if(this.getShortUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getShortUrl()).append("\", ");
        }
        sb.append("tallyDate:");
        if(this.getTallyDate() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getTallyDate()).append("\", ");
        }
        sb.append("status:");
        if(this.getStatus() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getStatus()).append("\", ");
        }
        sb.append("note:");
        if(this.getNote() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNote()).append("\", ");
        }
        sb.append("expirationTime:" + this.getExpirationTime()).append(", ");
        sb.append("bookmarkFolder:");
        if(this.getBookmarkFolder() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getBookmarkFolder()).append("\", ");
        }
        sb.append("contentTag:");
        if(this.getContentTag() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getContentTag()).append("\", ");
        }
        sb.append("referenceElement:");
        if(this.getReferenceElement() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getReferenceElement()).append("\", ");
        }
        sb.append("elementType:");
        if(this.getElementType() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getElementType()).append("\", ");
        }
        sb.append("keywordLink:");
        if(this.getKeywordLink() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getKeywordLink()).append("\", ");
        }
        sb.append("createdTime:" + this.getCreatedTime()).append(", ");
        sb.append("modifiedTime:" + this.getModifiedTime()).append(", ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getGuid() != null) {
            sb.append("\"guid\":").append("\"").append(this.getGuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"guid\":").append("null, ");
        }
        if(this.getUser() != null) {
            sb.append("\"user\":").append("\"").append(this.getUser()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"user\":").append("null, ");
        }
        if(this.getShortLink() != null) {
            sb.append("\"shortLink\":").append("\"").append(this.getShortLink()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"shortLink\":").append("null, ");
        }
        if(this.getDomain() != null) {
            sb.append("\"domain\":").append("\"").append(this.getDomain()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"domain\":").append("null, ");
        }
        if(this.getToken() != null) {
            sb.append("\"token\":").append("\"").append(this.getToken()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"token\":").append("null, ");
        }
        if(this.getLongUrl() != null) {
            sb.append("\"longUrl\":").append("\"").append(this.getLongUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"longUrl\":").append("null, ");
        }
        if(this.getShortUrl() != null) {
            sb.append("\"shortUrl\":").append("\"").append(this.getShortUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"shortUrl\":").append("null, ");
        }
        if(this.getTallyDate() != null) {
            sb.append("\"tallyDate\":").append("\"").append(this.getTallyDate()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"tallyDate\":").append("null, ");
        }
        if(this.getStatus() != null) {
            sb.append("\"status\":").append("\"").append(this.getStatus()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"status\":").append("null, ");
        }
        if(this.getNote() != null) {
            sb.append("\"note\":").append("\"").append(this.getNote()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"note\":").append("null, ");
        }
        if(this.getExpirationTime() != null) {
            sb.append("\"expirationTime\":").append("").append(this.getExpirationTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"expirationTime\":").append("null, ");
        }
        if(this.getBookmarkFolder() != null) {
            sb.append("\"bookmarkFolder\":").append("\"").append(this.getBookmarkFolder()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"bookmarkFolder\":").append("null, ");
        }
        if(this.getContentTag() != null) {
            sb.append("\"contentTag\":").append("\"").append(this.getContentTag()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"contentTag\":").append("null, ");
        }
        if(this.getReferenceElement() != null) {
            sb.append("\"referenceElement\":").append("\"").append(this.getReferenceElement()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"referenceElement\":").append("null, ");
        }
        if(this.getElementType() != null) {
            sb.append("\"elementType\":").append("\"").append(this.getElementType()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"elementType\":").append("null, ");
        }
        if(this.getKeywordLink() != null) {
            sb.append("\"keywordLink\":").append("\"").append(this.getKeywordLink()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"keywordLink\":").append("null, ");
        }
        if(this.getCreatedTime() != null) {
            sb.append("\"createdTime\":").append("").append(this.getCreatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"createdTime\":").append("null, ");
        }
        if(this.getModifiedTime() != null) {
            sb.append("\"modifiedTime\":").append("").append(this.getModifiedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"modifiedTime\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer( super.toString() );
        sb.append("bookmarkFolder = " + this.bookmarkFolder).append(";");
        sb.append("contentTag = " + this.contentTag).append(";");
        sb.append("referenceElement = " + this.referenceElement).append(";");
        sb.append("elementType = " + this.elementType).append(";");
        sb.append("keywordLink = " + this.keywordLink).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        BookmarkCrowdTallyJsBean cloned = new BookmarkCrowdTallyJsBean();
        cloned.setGuid(null);          // Do not clone the guid.
        cloned.setUser(this.getUser());   
        cloned.setShortLink(this.getShortLink());   
        cloned.setDomain(this.getDomain());   
        cloned.setToken(this.getToken());   
        cloned.setLongUrl(this.getLongUrl());   
        cloned.setShortUrl(this.getShortUrl());   
        cloned.setTallyDate(this.getTallyDate());   
        cloned.setStatus(this.getStatus());   
        cloned.setNote(this.getNote());   
        cloned.setExpirationTime(this.getExpirationTime());   
        cloned.setBookmarkFolder(this.getBookmarkFolder());   
        cloned.setContentTag(this.getContentTag());   
        cloned.setReferenceElement(this.getReferenceElement());   
        cloned.setElementType(this.getElementType());   
        cloned.setKeywordLink(this.getKeywordLink());   
        cloned.setCreatedTime(null);   // Reset the timestamp.
        cloned.setModifiedTime(null);   // Reset the timestamp.
        return cloned;
    }

}
