package com.cannyurl.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.cannyurl.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class BookmarkFolderImportJsBean extends FolderImportBaseJsBean implements Serializable, Cloneable  //, BookmarkFolderImport
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(BookmarkFolderImportJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String bookmarkFolder;

    // Ctors.
    public BookmarkFolderImportJsBean()
    {
        //this((String) null);
    }
    public BookmarkFolderImportJsBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BookmarkFolderImportJsBean(String guid, String user, Integer precedence, String importType, String importedFolder, String importedFolderUser, String importedFolderTitle, String importedFolderPath, String status, String note, String bookmarkFolder)
    {
        this(guid, user, precedence, importType, importedFolder, importedFolderUser, importedFolderTitle, importedFolderPath, status, note, bookmarkFolder, null, null);
    }
    public BookmarkFolderImportJsBean(String guid, String user, Integer precedence, String importType, String importedFolder, String importedFolderUser, String importedFolderTitle, String importedFolderPath, String status, String note, String bookmarkFolder, Long createdTime, Long modifiedTime)
    {
        super(guid, user, precedence, importType, importedFolder, importedFolderUser, importedFolderTitle, importedFolderPath, status, note, createdTime, modifiedTime);

        this.bookmarkFolder = bookmarkFolder;
    }
    public BookmarkFolderImportJsBean(BookmarkFolderImportJsBean bean)
    {
        if(bean != null) {
            setGuid(bean.getGuid());
            setUser(bean.getUser());
            setPrecedence(bean.getPrecedence());
            setImportType(bean.getImportType());
            setImportedFolder(bean.getImportedFolder());
            setImportedFolderUser(bean.getImportedFolderUser());
            setImportedFolderTitle(bean.getImportedFolderTitle());
            setImportedFolderPath(bean.getImportedFolderPath());
            setStatus(bean.getStatus());
            setNote(bean.getNote());
            setBookmarkFolder(bean.getBookmarkFolder());
            setCreatedTime(bean.getCreatedTime());
            setModifiedTime(bean.getModifiedTime());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static BookmarkFolderImportJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        BookmarkFolderImportJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(BookmarkFolderImportJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, BookmarkFolderImportJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    public String getUser()
    {
        return super.getUser();
    }
    public void setUser(String user)
    {
        super.setUser(user);
    }

    public Integer getPrecedence()
    {
        return super.getPrecedence();
    }
    public void setPrecedence(Integer precedence)
    {
        super.setPrecedence(precedence);
    }

    public String getImportType()
    {
        return super.getImportType();
    }
    public void setImportType(String importType)
    {
        super.setImportType(importType);
    }

    public String getImportedFolder()
    {
        return super.getImportedFolder();
    }
    public void setImportedFolder(String importedFolder)
    {
        super.setImportedFolder(importedFolder);
    }

    public String getImportedFolderUser()
    {
        return super.getImportedFolderUser();
    }
    public void setImportedFolderUser(String importedFolderUser)
    {
        super.setImportedFolderUser(importedFolderUser);
    }

    public String getImportedFolderTitle()
    {
        return super.getImportedFolderTitle();
    }
    public void setImportedFolderTitle(String importedFolderTitle)
    {
        super.setImportedFolderTitle(importedFolderTitle);
    }

    public String getImportedFolderPath()
    {
        return super.getImportedFolderPath();
    }
    public void setImportedFolderPath(String importedFolderPath)
    {
        super.setImportedFolderPath(importedFolderPath);
    }

    public String getStatus()
    {
        return super.getStatus();
    }
    public void setStatus(String status)
    {
        super.setStatus(status);
    }

    public String getNote()
    {
        return super.getNote();
    }
    public void setNote(String note)
    {
        super.setNote(note);
    }

    public String getBookmarkFolder()
    {
        return this.bookmarkFolder;
    }
    public void setBookmarkFolder(String bookmarkFolder)
    {
        this.bookmarkFolder = bookmarkFolder;
    }

    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:null, ");
        sb.append("user:null, ");
        sb.append("precedence:0, ");
        sb.append("importType:null, ");
        sb.append("importedFolder:null, ");
        sb.append("importedFolderUser:null, ");
        sb.append("importedFolderTitle:null, ");
        sb.append("importedFolderPath:null, ");
        sb.append("status:null, ");
        sb.append("note:null, ");
        sb.append("bookmarkFolder:null, ");
        sb.append("createdTime:0, ");
        sb.append("modifiedTime:0, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:");
        if(this.getGuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGuid()).append("\", ");
        }
        sb.append("user:");
        if(this.getUser() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUser()).append("\", ");
        }
        sb.append("precedence:" + this.getPrecedence()).append(", ");
        sb.append("importType:");
        if(this.getImportType() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getImportType()).append("\", ");
        }
        sb.append("importedFolder:");
        if(this.getImportedFolder() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getImportedFolder()).append("\", ");
        }
        sb.append("importedFolderUser:");
        if(this.getImportedFolderUser() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getImportedFolderUser()).append("\", ");
        }
        sb.append("importedFolderTitle:");
        if(this.getImportedFolderTitle() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getImportedFolderTitle()).append("\", ");
        }
        sb.append("importedFolderPath:");
        if(this.getImportedFolderPath() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getImportedFolderPath()).append("\", ");
        }
        sb.append("status:");
        if(this.getStatus() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getStatus()).append("\", ");
        }
        sb.append("note:");
        if(this.getNote() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNote()).append("\", ");
        }
        sb.append("bookmarkFolder:");
        if(this.getBookmarkFolder() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getBookmarkFolder()).append("\", ");
        }
        sb.append("createdTime:" + this.getCreatedTime()).append(", ");
        sb.append("modifiedTime:" + this.getModifiedTime()).append(", ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getGuid() != null) {
            sb.append("\"guid\":").append("\"").append(this.getGuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"guid\":").append("null, ");
        }
        if(this.getUser() != null) {
            sb.append("\"user\":").append("\"").append(this.getUser()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"user\":").append("null, ");
        }
        if(this.getPrecedence() != null) {
            sb.append("\"precedence\":").append("").append(this.getPrecedence()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"precedence\":").append("null, ");
        }
        if(this.getImportType() != null) {
            sb.append("\"importType\":").append("\"").append(this.getImportType()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"importType\":").append("null, ");
        }
        if(this.getImportedFolder() != null) {
            sb.append("\"importedFolder\":").append("\"").append(this.getImportedFolder()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"importedFolder\":").append("null, ");
        }
        if(this.getImportedFolderUser() != null) {
            sb.append("\"importedFolderUser\":").append("\"").append(this.getImportedFolderUser()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"importedFolderUser\":").append("null, ");
        }
        if(this.getImportedFolderTitle() != null) {
            sb.append("\"importedFolderTitle\":").append("\"").append(this.getImportedFolderTitle()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"importedFolderTitle\":").append("null, ");
        }
        if(this.getImportedFolderPath() != null) {
            sb.append("\"importedFolderPath\":").append("\"").append(this.getImportedFolderPath()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"importedFolderPath\":").append("null, ");
        }
        if(this.getStatus() != null) {
            sb.append("\"status\":").append("\"").append(this.getStatus()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"status\":").append("null, ");
        }
        if(this.getNote() != null) {
            sb.append("\"note\":").append("\"").append(this.getNote()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"note\":").append("null, ");
        }
        if(this.getBookmarkFolder() != null) {
            sb.append("\"bookmarkFolder\":").append("\"").append(this.getBookmarkFolder()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"bookmarkFolder\":").append("null, ");
        }
        if(this.getCreatedTime() != null) {
            sb.append("\"createdTime\":").append("").append(this.getCreatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"createdTime\":").append("null, ");
        }
        if(this.getModifiedTime() != null) {
            sb.append("\"modifiedTime\":").append("").append(this.getModifiedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"modifiedTime\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer( super.toString() );
        sb.append("bookmarkFolder = " + this.bookmarkFolder).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        BookmarkFolderImportJsBean cloned = new BookmarkFolderImportJsBean();
        cloned.setGuid(null);          // Do not clone the guid.
        cloned.setUser(this.getUser());   
        cloned.setPrecedence(this.getPrecedence());   
        cloned.setImportType(this.getImportType());   
        cloned.setImportedFolder(this.getImportedFolder());   
        cloned.setImportedFolderUser(this.getImportedFolderUser());   
        cloned.setImportedFolderTitle(this.getImportedFolderTitle());   
        cloned.setImportedFolderPath(this.getImportedFolderPath());   
        cloned.setStatus(this.getStatus());   
        cloned.setNote(this.getNote());   
        cloned.setBookmarkFolder(this.getBookmarkFolder());   
        cloned.setCreatedTime(null);   // Reset the timestamp.
        cloned.setModifiedTime(null);   // Reset the timestamp.
        return cloned;
    }

}
