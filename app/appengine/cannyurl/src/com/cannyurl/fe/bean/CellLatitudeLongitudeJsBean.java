package com.cannyurl.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.cannyurl.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class CellLatitudeLongitudeJsBean implements Serializable, Cloneable  //, CellLatitudeLongitude
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(CellLatitudeLongitudeJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private Integer scale;
    private Integer latitude;
    private Integer longitude;

    // Ctors.
    public CellLatitudeLongitudeJsBean()
    {
        //this((String) null);
    }
    public CellLatitudeLongitudeJsBean(Integer scale, Integer latitude, Integer longitude)
    {
        this.scale = scale;
        this.latitude = latitude;
        this.longitude = longitude;
    }
    public CellLatitudeLongitudeJsBean(CellLatitudeLongitudeJsBean bean)
    {
        if(bean != null) {
            setScale(bean.getScale());
            setLatitude(bean.getLatitude());
            setLongitude(bean.getLongitude());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static CellLatitudeLongitudeJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        CellLatitudeLongitudeJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(CellLatitudeLongitudeJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, CellLatitudeLongitudeJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public Integer getScale()
    {
        return this.scale;
    }
    public void setScale(Integer scale)
    {
        this.scale = scale;
    }

    public Integer getLatitude()
    {
        return this.latitude;
    }
    public void setLatitude(Integer latitude)
    {
        this.latitude = latitude;
    }

    public Integer getLongitude()
    {
        return this.longitude;
    }
    public void setLongitude(Integer longitude)
    {
        this.longitude = longitude;
    }


    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getScale() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLatitude() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLongitude() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("scale:0, ");
        sb.append("latitude:0, ");
        sb.append("longitude:0, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("scale:" + this.getScale()).append(", ");
        sb.append("latitude:" + this.getLatitude()).append(", ");
        sb.append("longitude:" + this.getLongitude()).append(", ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getScale() != null) {
            sb.append("\"scale\":").append("").append(this.getScale()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"scale\":").append("null, ");
        }
        if(this.getLatitude() != null) {
            sb.append("\"latitude\":").append("").append(this.getLatitude()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"latitude\":").append("null, ");
        }
        if(this.getLongitude() != null) {
            sb.append("\"longitude\":").append("").append(this.getLongitude()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"longitude\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("scale = " + this.scale).append(";");
        sb.append("latitude = " + this.latitude).append(";");
        sb.append("longitude = " + this.longitude).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        CellLatitudeLongitudeJsBean cloned = new CellLatitudeLongitudeJsBean();
        cloned.setScale(this.getScale());   
        cloned.setLatitude(this.getLatitude());   
        cloned.setLongitude(this.getLongitude());   
        return cloned;
    }

}
