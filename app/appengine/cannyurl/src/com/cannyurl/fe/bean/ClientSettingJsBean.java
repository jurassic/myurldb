package com.cannyurl.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.cannyurl.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class ClientSettingJsBean extends PersonalSettingJsBean implements Serializable, Cloneable  //, ClientSetting
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ClientSettingJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String appClient;
    private String admin;
    private Boolean autoRedirectEnabled;
    private Boolean viewEnabled;
    private Boolean shareEnabled;
    private String defaultBaseDomain;
    private String defaultDomainType;
    private String defaultTokenType;
    private String defaultRedirectType;
    private Long defaultFlashDuration;
    private String defaultAccessType;
    private String defaultViewType;

    // Ctors.
    public ClientSettingJsBean()
    {
        //this((String) null);
    }
    public ClientSettingJsBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public ClientSettingJsBean(String guid, String appClient, String admin, Boolean autoRedirectEnabled, Boolean viewEnabled, Boolean shareEnabled, String defaultBaseDomain, String defaultDomainType, String defaultTokenType, String defaultRedirectType, Long defaultFlashDuration, String defaultAccessType, String defaultViewType)
    {
        this(guid, appClient, admin, autoRedirectEnabled, viewEnabled, shareEnabled, defaultBaseDomain, defaultDomainType, defaultTokenType, defaultRedirectType, defaultFlashDuration, defaultAccessType, defaultViewType, null, null);
    }
    public ClientSettingJsBean(String guid, String appClient, String admin, Boolean autoRedirectEnabled, Boolean viewEnabled, Boolean shareEnabled, String defaultBaseDomain, String defaultDomainType, String defaultTokenType, String defaultRedirectType, Long defaultFlashDuration, String defaultAccessType, String defaultViewType, Long createdTime, Long modifiedTime)
    {
        super(guid, createdTime, modifiedTime);

        this.appClient = appClient;
        this.admin = admin;
        this.autoRedirectEnabled = autoRedirectEnabled;
        this.viewEnabled = viewEnabled;
        this.shareEnabled = shareEnabled;
        this.defaultBaseDomain = defaultBaseDomain;
        this.defaultDomainType = defaultDomainType;
        this.defaultTokenType = defaultTokenType;
        this.defaultRedirectType = defaultRedirectType;
        this.defaultFlashDuration = defaultFlashDuration;
        this.defaultAccessType = defaultAccessType;
        this.defaultViewType = defaultViewType;
    }
    public ClientSettingJsBean(ClientSettingJsBean bean)
    {
        if(bean != null) {
            setGuid(bean.getGuid());
            setAppClient(bean.getAppClient());
            setAdmin(bean.getAdmin());
            setAutoRedirectEnabled(bean.isAutoRedirectEnabled());
            setViewEnabled(bean.isViewEnabled());
            setShareEnabled(bean.isShareEnabled());
            setDefaultBaseDomain(bean.getDefaultBaseDomain());
            setDefaultDomainType(bean.getDefaultDomainType());
            setDefaultTokenType(bean.getDefaultTokenType());
            setDefaultRedirectType(bean.getDefaultRedirectType());
            setDefaultFlashDuration(bean.getDefaultFlashDuration());
            setDefaultAccessType(bean.getDefaultAccessType());
            setDefaultViewType(bean.getDefaultViewType());
            setCreatedTime(bean.getCreatedTime());
            setModifiedTime(bean.getModifiedTime());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static ClientSettingJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        ClientSettingJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(ClientSettingJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, ClientSettingJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    public String getAppClient()
    {
        return this.appClient;
    }
    public void setAppClient(String appClient)
    {
        this.appClient = appClient;
    }

    public String getAdmin()
    {
        return this.admin;
    }
    public void setAdmin(String admin)
    {
        this.admin = admin;
    }

    public Boolean isAutoRedirectEnabled()
    {
        return this.autoRedirectEnabled;
    }
    public void setAutoRedirectEnabled(Boolean autoRedirectEnabled)
    {
        this.autoRedirectEnabled = autoRedirectEnabled;
    }

    public Boolean isViewEnabled()
    {
        return this.viewEnabled;
    }
    public void setViewEnabled(Boolean viewEnabled)
    {
        this.viewEnabled = viewEnabled;
    }

    public Boolean isShareEnabled()
    {
        return this.shareEnabled;
    }
    public void setShareEnabled(Boolean shareEnabled)
    {
        this.shareEnabled = shareEnabled;
    }

    public String getDefaultBaseDomain()
    {
        return this.defaultBaseDomain;
    }
    public void setDefaultBaseDomain(String defaultBaseDomain)
    {
        this.defaultBaseDomain = defaultBaseDomain;
    }

    public String getDefaultDomainType()
    {
        return this.defaultDomainType;
    }
    public void setDefaultDomainType(String defaultDomainType)
    {
        this.defaultDomainType = defaultDomainType;
    }

    public String getDefaultTokenType()
    {
        return this.defaultTokenType;
    }
    public void setDefaultTokenType(String defaultTokenType)
    {
        this.defaultTokenType = defaultTokenType;
    }

    public String getDefaultRedirectType()
    {
        return this.defaultRedirectType;
    }
    public void setDefaultRedirectType(String defaultRedirectType)
    {
        this.defaultRedirectType = defaultRedirectType;
    }

    public Long getDefaultFlashDuration()
    {
        return this.defaultFlashDuration;
    }
    public void setDefaultFlashDuration(Long defaultFlashDuration)
    {
        this.defaultFlashDuration = defaultFlashDuration;
    }

    public String getDefaultAccessType()
    {
        return this.defaultAccessType;
    }
    public void setDefaultAccessType(String defaultAccessType)
    {
        this.defaultAccessType = defaultAccessType;
    }

    public String getDefaultViewType()
    {
        return this.defaultViewType;
    }
    public void setDefaultViewType(String defaultViewType)
    {
        this.defaultViewType = defaultViewType;
    }

    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:null, ");
        sb.append("appClient:null, ");
        sb.append("admin:null, ");
        sb.append("autoRedirectEnabled:false, ");
        sb.append("viewEnabled:false, ");
        sb.append("shareEnabled:false, ");
        sb.append("defaultBaseDomain:null, ");
        sb.append("defaultDomainType:null, ");
        sb.append("defaultTokenType:null, ");
        sb.append("defaultRedirectType:null, ");
        sb.append("defaultFlashDuration:0, ");
        sb.append("defaultAccessType:null, ");
        sb.append("defaultViewType:null, ");
        sb.append("createdTime:0, ");
        sb.append("modifiedTime:0, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:");
        if(this.getGuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGuid()).append("\", ");
        }
        sb.append("appClient:");
        if(this.getAppClient() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getAppClient()).append("\", ");
        }
        sb.append("admin:");
        if(this.getAdmin() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getAdmin()).append("\", ");
        }
        sb.append("autoRedirectEnabled:" + this.isAutoRedirectEnabled()).append(", ");
        sb.append("viewEnabled:" + this.isViewEnabled()).append(", ");
        sb.append("shareEnabled:" + this.isShareEnabled()).append(", ");
        sb.append("defaultBaseDomain:");
        if(this.getDefaultBaseDomain() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDefaultBaseDomain()).append("\", ");
        }
        sb.append("defaultDomainType:");
        if(this.getDefaultDomainType() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDefaultDomainType()).append("\", ");
        }
        sb.append("defaultTokenType:");
        if(this.getDefaultTokenType() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDefaultTokenType()).append("\", ");
        }
        sb.append("defaultRedirectType:");
        if(this.getDefaultRedirectType() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDefaultRedirectType()).append("\", ");
        }
        sb.append("defaultFlashDuration:" + this.getDefaultFlashDuration()).append(", ");
        sb.append("defaultAccessType:");
        if(this.getDefaultAccessType() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDefaultAccessType()).append("\", ");
        }
        sb.append("defaultViewType:");
        if(this.getDefaultViewType() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDefaultViewType()).append("\", ");
        }
        sb.append("createdTime:" + this.getCreatedTime()).append(", ");
        sb.append("modifiedTime:" + this.getModifiedTime()).append(", ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getGuid() != null) {
            sb.append("\"guid\":").append("\"").append(this.getGuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"guid\":").append("null, ");
        }
        if(this.getAppClient() != null) {
            sb.append("\"appClient\":").append("\"").append(this.getAppClient()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"appClient\":").append("null, ");
        }
        if(this.getAdmin() != null) {
            sb.append("\"admin\":").append("\"").append(this.getAdmin()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"admin\":").append("null, ");
        }
        if(this.isAutoRedirectEnabled() != null) {
            sb.append("\"autoRedirectEnabled\":").append("").append(this.isAutoRedirectEnabled()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"autoRedirectEnabled\":").append("null, ");
        }
        if(this.isViewEnabled() != null) {
            sb.append("\"viewEnabled\":").append("").append(this.isViewEnabled()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"viewEnabled\":").append("null, ");
        }
        if(this.isShareEnabled() != null) {
            sb.append("\"shareEnabled\":").append("").append(this.isShareEnabled()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"shareEnabled\":").append("null, ");
        }
        if(this.getDefaultBaseDomain() != null) {
            sb.append("\"defaultBaseDomain\":").append("\"").append(this.getDefaultBaseDomain()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"defaultBaseDomain\":").append("null, ");
        }
        if(this.getDefaultDomainType() != null) {
            sb.append("\"defaultDomainType\":").append("\"").append(this.getDefaultDomainType()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"defaultDomainType\":").append("null, ");
        }
        if(this.getDefaultTokenType() != null) {
            sb.append("\"defaultTokenType\":").append("\"").append(this.getDefaultTokenType()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"defaultTokenType\":").append("null, ");
        }
        if(this.getDefaultRedirectType() != null) {
            sb.append("\"defaultRedirectType\":").append("\"").append(this.getDefaultRedirectType()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"defaultRedirectType\":").append("null, ");
        }
        if(this.getDefaultFlashDuration() != null) {
            sb.append("\"defaultFlashDuration\":").append("").append(this.getDefaultFlashDuration()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"defaultFlashDuration\":").append("null, ");
        }
        if(this.getDefaultAccessType() != null) {
            sb.append("\"defaultAccessType\":").append("\"").append(this.getDefaultAccessType()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"defaultAccessType\":").append("null, ");
        }
        if(this.getDefaultViewType() != null) {
            sb.append("\"defaultViewType\":").append("\"").append(this.getDefaultViewType()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"defaultViewType\":").append("null, ");
        }
        if(this.getCreatedTime() != null) {
            sb.append("\"createdTime\":").append("").append(this.getCreatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"createdTime\":").append("null, ");
        }
        if(this.getModifiedTime() != null) {
            sb.append("\"modifiedTime\":").append("").append(this.getModifiedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"modifiedTime\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer( super.toString() );
        sb.append("appClient = " + this.appClient).append(";");
        sb.append("admin = " + this.admin).append(";");
        sb.append("autoRedirectEnabled = " + this.autoRedirectEnabled).append(";");
        sb.append("viewEnabled = " + this.viewEnabled).append(";");
        sb.append("shareEnabled = " + this.shareEnabled).append(";");
        sb.append("defaultBaseDomain = " + this.defaultBaseDomain).append(";");
        sb.append("defaultDomainType = " + this.defaultDomainType).append(";");
        sb.append("defaultTokenType = " + this.defaultTokenType).append(";");
        sb.append("defaultRedirectType = " + this.defaultRedirectType).append(";");
        sb.append("defaultFlashDuration = " + this.defaultFlashDuration).append(";");
        sb.append("defaultAccessType = " + this.defaultAccessType).append(";");
        sb.append("defaultViewType = " + this.defaultViewType).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        ClientSettingJsBean cloned = new ClientSettingJsBean();
        cloned.setGuid(null);          // Do not clone the guid.
        cloned.setAppClient(this.getAppClient());   
        cloned.setAdmin(this.getAdmin());   
        cloned.setAutoRedirectEnabled(this.isAutoRedirectEnabled());   
        cloned.setViewEnabled(this.isViewEnabled());   
        cloned.setShareEnabled(this.isShareEnabled());   
        cloned.setDefaultBaseDomain(this.getDefaultBaseDomain());   
        cloned.setDefaultDomainType(this.getDefaultDomainType());   
        cloned.setDefaultTokenType(this.getDefaultTokenType());   
        cloned.setDefaultRedirectType(this.getDefaultRedirectType());   
        cloned.setDefaultFlashDuration(this.getDefaultFlashDuration());   
        cloned.setDefaultAccessType(this.getDefaultAccessType());   
        cloned.setDefaultViewType(this.getDefaultViewType());   
        cloned.setCreatedTime(null);   // Reset the timestamp.
        cloned.setModifiedTime(null);   // Reset the timestamp.
        return cloned;
    }

}
