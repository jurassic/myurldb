package com.cannyurl.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.cannyurl.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class ContactInfoStructJsBean implements Serializable, Cloneable  //, ContactInfoStruct
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ContactInfoStructJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String uuid;
    private String streetAddress;
    private String locality;
    private String region;
    private String postalCode;
    private String countryName;
    private String emailAddress;
    private String phoneNumber;
    private String faxNumber;
    private String website;
    private String note;

    // Ctors.
    public ContactInfoStructJsBean()
    {
        //this((String) null);
    }
    public ContactInfoStructJsBean(String uuid, String streetAddress, String locality, String region, String postalCode, String countryName, String emailAddress, String phoneNumber, String faxNumber, String website, String note)
    {
        this.uuid = uuid;
        this.streetAddress = streetAddress;
        this.locality = locality;
        this.region = region;
        this.postalCode = postalCode;
        this.countryName = countryName;
        this.emailAddress = emailAddress;
        this.phoneNumber = phoneNumber;
        this.faxNumber = faxNumber;
        this.website = website;
        this.note = note;
    }
    public ContactInfoStructJsBean(ContactInfoStructJsBean bean)
    {
        if(bean != null) {
            setUuid(bean.getUuid());
            setStreetAddress(bean.getStreetAddress());
            setLocality(bean.getLocality());
            setRegion(bean.getRegion());
            setPostalCode(bean.getPostalCode());
            setCountryName(bean.getCountryName());
            setEmailAddress(bean.getEmailAddress());
            setPhoneNumber(bean.getPhoneNumber());
            setFaxNumber(bean.getFaxNumber());
            setWebsite(bean.getWebsite());
            setNote(bean.getNote());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static ContactInfoStructJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        ContactInfoStructJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(ContactInfoStructJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, ContactInfoStructJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getUuid()
    {
        return this.uuid;
    }
    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }

    public String getStreetAddress()
    {
        return this.streetAddress;
    }
    public void setStreetAddress(String streetAddress)
    {
        this.streetAddress = streetAddress;
    }

    public String getLocality()
    {
        return this.locality;
    }
    public void setLocality(String locality)
    {
        this.locality = locality;
    }

    public String getRegion()
    {
        return this.region;
    }
    public void setRegion(String region)
    {
        this.region = region;
    }

    public String getPostalCode()
    {
        return this.postalCode;
    }
    public void setPostalCode(String postalCode)
    {
        this.postalCode = postalCode;
    }

    public String getCountryName()
    {
        return this.countryName;
    }
    public void setCountryName(String countryName)
    {
        this.countryName = countryName;
    }

    public String getEmailAddress()
    {
        return this.emailAddress;
    }
    public void setEmailAddress(String emailAddress)
    {
        this.emailAddress = emailAddress;
    }

    public String getPhoneNumber()
    {
        return this.phoneNumber;
    }
    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    public String getFaxNumber()
    {
        return this.faxNumber;
    }
    public void setFaxNumber(String faxNumber)
    {
        this.faxNumber = faxNumber;
    }

    public String getWebsite()
    {
        return this.website;
    }
    public void setWebsite(String website)
    {
        this.website = website;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }


    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getStreetAddress() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLocality() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getRegion() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPostalCode() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getCountryName() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getEmailAddress() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPhoneNumber() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getFaxNumber() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getWebsite() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("uuid:null, ");
        sb.append("streetAddress:null, ");
        sb.append("locality:null, ");
        sb.append("region:null, ");
        sb.append("postalCode:null, ");
        sb.append("countryName:null, ");
        sb.append("emailAddress:null, ");
        sb.append("phoneNumber:null, ");
        sb.append("faxNumber:null, ");
        sb.append("website:null, ");
        sb.append("note:null, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("uuid:");
        if(this.getUuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUuid()).append("\", ");
        }
        sb.append("streetAddress:");
        if(this.getStreetAddress() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getStreetAddress()).append("\", ");
        }
        sb.append("locality:");
        if(this.getLocality() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getLocality()).append("\", ");
        }
        sb.append("region:");
        if(this.getRegion() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getRegion()).append("\", ");
        }
        sb.append("postalCode:");
        if(this.getPostalCode() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getPostalCode()).append("\", ");
        }
        sb.append("countryName:");
        if(this.getCountryName() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getCountryName()).append("\", ");
        }
        sb.append("emailAddress:");
        if(this.getEmailAddress() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getEmailAddress()).append("\", ");
        }
        sb.append("phoneNumber:");
        if(this.getPhoneNumber() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getPhoneNumber()).append("\", ");
        }
        sb.append("faxNumber:");
        if(this.getFaxNumber() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getFaxNumber()).append("\", ");
        }
        sb.append("website:");
        if(this.getWebsite() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getWebsite()).append("\", ");
        }
        sb.append("note:");
        if(this.getNote() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNote()).append("\", ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getUuid() != null) {
            sb.append("\"uuid\":").append("\"").append(this.getUuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"uuid\":").append("null, ");
        }
        if(this.getStreetAddress() != null) {
            sb.append("\"streetAddress\":").append("\"").append(this.getStreetAddress()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"streetAddress\":").append("null, ");
        }
        if(this.getLocality() != null) {
            sb.append("\"locality\":").append("\"").append(this.getLocality()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"locality\":").append("null, ");
        }
        if(this.getRegion() != null) {
            sb.append("\"region\":").append("\"").append(this.getRegion()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"region\":").append("null, ");
        }
        if(this.getPostalCode() != null) {
            sb.append("\"postalCode\":").append("\"").append(this.getPostalCode()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"postalCode\":").append("null, ");
        }
        if(this.getCountryName() != null) {
            sb.append("\"countryName\":").append("\"").append(this.getCountryName()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"countryName\":").append("null, ");
        }
        if(this.getEmailAddress() != null) {
            sb.append("\"emailAddress\":").append("\"").append(this.getEmailAddress()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"emailAddress\":").append("null, ");
        }
        if(this.getPhoneNumber() != null) {
            sb.append("\"phoneNumber\":").append("\"").append(this.getPhoneNumber()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"phoneNumber\":").append("null, ");
        }
        if(this.getFaxNumber() != null) {
            sb.append("\"faxNumber\":").append("\"").append(this.getFaxNumber()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"faxNumber\":").append("null, ");
        }
        if(this.getWebsite() != null) {
            sb.append("\"website\":").append("\"").append(this.getWebsite()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"website\":").append("null, ");
        }
        if(this.getNote() != null) {
            sb.append("\"note\":").append("\"").append(this.getNote()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"note\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("uuid = " + this.uuid).append(";");
        sb.append("streetAddress = " + this.streetAddress).append(";");
        sb.append("locality = " + this.locality).append(";");
        sb.append("region = " + this.region).append(";");
        sb.append("postalCode = " + this.postalCode).append(";");
        sb.append("countryName = " + this.countryName).append(";");
        sb.append("emailAddress = " + this.emailAddress).append(";");
        sb.append("phoneNumber = " + this.phoneNumber).append(";");
        sb.append("faxNumber = " + this.faxNumber).append(";");
        sb.append("website = " + this.website).append(";");
        sb.append("note = " + this.note).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        ContactInfoStructJsBean cloned = new ContactInfoStructJsBean();
        cloned.setUuid(this.getUuid());   
        cloned.setStreetAddress(this.getStreetAddress());   
        cloned.setLocality(this.getLocality());   
        cloned.setRegion(this.getRegion());   
        cloned.setPostalCode(this.getPostalCode());   
        cloned.setCountryName(this.getCountryName());   
        cloned.setEmailAddress(this.getEmailAddress());   
        cloned.setPhoneNumber(this.getPhoneNumber());   
        cloned.setFaxNumber(this.getFaxNumber());   
        cloned.setWebsite(this.getWebsite());   
        cloned.setNote(this.getNote());   
        return cloned;
    }

}
