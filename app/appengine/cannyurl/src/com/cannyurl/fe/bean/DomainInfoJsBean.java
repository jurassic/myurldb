package com.cannyurl.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.cannyurl.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class DomainInfoJsBean implements Serializable, Cloneable  //, DomainInfo
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(DomainInfoJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String guid;
    private String domain;
    private Boolean banned;
    private Boolean urlShortener;
    private String category;
    private String reputation;
    private String authority;
    private String note;
    private Long verifiedTime;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public DomainInfoJsBean()
    {
        //this((String) null);
    }
    public DomainInfoJsBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null);
    }
    public DomainInfoJsBean(String guid, String domain, Boolean banned, Boolean urlShortener, String category, String reputation, String authority, String note, Long verifiedTime)
    {
        this(guid, domain, banned, urlShortener, category, reputation, authority, note, verifiedTime, null, null);
    }
    public DomainInfoJsBean(String guid, String domain, Boolean banned, Boolean urlShortener, String category, String reputation, String authority, String note, Long verifiedTime, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.domain = domain;
        this.banned = banned;
        this.urlShortener = urlShortener;
        this.category = category;
        this.reputation = reputation;
        this.authority = authority;
        this.note = note;
        this.verifiedTime = verifiedTime;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public DomainInfoJsBean(DomainInfoJsBean bean)
    {
        if(bean != null) {
            setGuid(bean.getGuid());
            setDomain(bean.getDomain());
            setBanned(bean.isBanned());
            setUrlShortener(bean.isUrlShortener());
            setCategory(bean.getCategory());
            setReputation(bean.getReputation());
            setAuthority(bean.getAuthority());
            setNote(bean.getNote());
            setVerifiedTime(bean.getVerifiedTime());
            setCreatedTime(bean.getCreatedTime());
            setModifiedTime(bean.getModifiedTime());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static DomainInfoJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        DomainInfoJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(DomainInfoJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, DomainInfoJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public String getDomain()
    {
        return this.domain;
    }
    public void setDomain(String domain)
    {
        this.domain = domain;
    }

    public Boolean isBanned()
    {
        return this.banned;
    }
    public void setBanned(Boolean banned)
    {
        this.banned = banned;
    }

    public Boolean isUrlShortener()
    {
        return this.urlShortener;
    }
    public void setUrlShortener(Boolean urlShortener)
    {
        this.urlShortener = urlShortener;
    }

    public String getCategory()
    {
        return this.category;
    }
    public void setCategory(String category)
    {
        this.category = category;
    }

    public String getReputation()
    {
        return this.reputation;
    }
    public void setReputation(String reputation)
    {
        this.reputation = reputation;
    }

    public String getAuthority()
    {
        return this.authority;
    }
    public void setAuthority(String authority)
    {
        this.authority = authority;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    public Long getVerifiedTime()
    {
        return this.verifiedTime;
    }
    public void setVerifiedTime(Long verifiedTime)
    {
        this.verifiedTime = verifiedTime;
    }

    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:null, ");
        sb.append("domain:null, ");
        sb.append("banned:false, ");
        sb.append("urlShortener:false, ");
        sb.append("category:null, ");
        sb.append("reputation:null, ");
        sb.append("authority:null, ");
        sb.append("note:null, ");
        sb.append("verifiedTime:0, ");
        sb.append("createdTime:0, ");
        sb.append("modifiedTime:0, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:");
        if(this.getGuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGuid()).append("\", ");
        }
        sb.append("domain:");
        if(this.getDomain() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDomain()).append("\", ");
        }
        sb.append("banned:" + this.isBanned()).append(", ");
        sb.append("urlShortener:" + this.isUrlShortener()).append(", ");
        sb.append("category:");
        if(this.getCategory() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getCategory()).append("\", ");
        }
        sb.append("reputation:");
        if(this.getReputation() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getReputation()).append("\", ");
        }
        sb.append("authority:");
        if(this.getAuthority() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getAuthority()).append("\", ");
        }
        sb.append("note:");
        if(this.getNote() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNote()).append("\", ");
        }
        sb.append("verifiedTime:" + this.getVerifiedTime()).append(", ");
        sb.append("createdTime:" + this.getCreatedTime()).append(", ");
        sb.append("modifiedTime:" + this.getModifiedTime()).append(", ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getGuid() != null) {
            sb.append("\"guid\":").append("\"").append(this.getGuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"guid\":").append("null, ");
        }
        if(this.getDomain() != null) {
            sb.append("\"domain\":").append("\"").append(this.getDomain()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"domain\":").append("null, ");
        }
        if(this.isBanned() != null) {
            sb.append("\"banned\":").append("").append(this.isBanned()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"banned\":").append("null, ");
        }
        if(this.isUrlShortener() != null) {
            sb.append("\"urlShortener\":").append("").append(this.isUrlShortener()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"urlShortener\":").append("null, ");
        }
        if(this.getCategory() != null) {
            sb.append("\"category\":").append("\"").append(this.getCategory()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"category\":").append("null, ");
        }
        if(this.getReputation() != null) {
            sb.append("\"reputation\":").append("\"").append(this.getReputation()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"reputation\":").append("null, ");
        }
        if(this.getAuthority() != null) {
            sb.append("\"authority\":").append("\"").append(this.getAuthority()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"authority\":").append("null, ");
        }
        if(this.getNote() != null) {
            sb.append("\"note\":").append("\"").append(this.getNote()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"note\":").append("null, ");
        }
        if(this.getVerifiedTime() != null) {
            sb.append("\"verifiedTime\":").append("").append(this.getVerifiedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"verifiedTime\":").append("null, ");
        }
        if(this.getCreatedTime() != null) {
            sb.append("\"createdTime\":").append("").append(this.getCreatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"createdTime\":").append("null, ");
        }
        if(this.getModifiedTime() != null) {
            sb.append("\"modifiedTime\":").append("").append(this.getModifiedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"modifiedTime\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("guid = " + this.guid).append(";");
        sb.append("domain = " + this.domain).append(";");
        sb.append("banned = " + this.banned).append(";");
        sb.append("urlShortener = " + this.urlShortener).append(";");
        sb.append("category = " + this.category).append(";");
        sb.append("reputation = " + this.reputation).append(";");
        sb.append("authority = " + this.authority).append(";");
        sb.append("note = " + this.note).append(";");
        sb.append("verifiedTime = " + this.verifiedTime).append(";");
        sb.append("createdTime = " + this.createdTime).append(";");
        sb.append("modifiedTime = " + this.modifiedTime).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        DomainInfoJsBean cloned = new DomainInfoJsBean();
        cloned.setGuid(null);          // Do not clone the guid.
        cloned.setDomain(this.getDomain());   
        cloned.setBanned(this.isBanned());   
        cloned.setUrlShortener(this.isUrlShortener());   
        cloned.setCategory(this.getCategory());   
        cloned.setReputation(this.getReputation());   
        cloned.setAuthority(this.getAuthority());   
        cloned.setNote(this.getNote());   
        cloned.setVerifiedTime(this.getVerifiedTime());   
        cloned.setCreatedTime(null);   // Reset the timestamp.
        cloned.setModifiedTime(null);   // Reset the timestamp.
        return cloned;
    }

}
