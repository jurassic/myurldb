package com.cannyurl.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.cannyurl.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class FolderImportBaseJsBean implements Serializable  //, FolderImportBase
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(FolderImportBaseJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String guid;
    private String user;
    private Integer precedence;
    private String importType;
    private String importedFolder;
    private String importedFolderUser;
    private String importedFolderTitle;
    private String importedFolderPath;
    private String status;
    private String note;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public FolderImportBaseJsBean()
    {
        //this((String) null);
    }
    public FolderImportBaseJsBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null);
    }
    public FolderImportBaseJsBean(String guid, String user, Integer precedence, String importType, String importedFolder, String importedFolderUser, String importedFolderTitle, String importedFolderPath, String status, String note)
    {
        this(guid, user, precedence, importType, importedFolder, importedFolderUser, importedFolderTitle, importedFolderPath, status, note, null, null);
    }
    public FolderImportBaseJsBean(String guid, String user, Integer precedence, String importType, String importedFolder, String importedFolderUser, String importedFolderTitle, String importedFolderPath, String status, String note, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.user = user;
        this.precedence = precedence;
        this.importType = importType;
        this.importedFolder = importedFolder;
        this.importedFolderUser = importedFolderUser;
        this.importedFolderTitle = importedFolderTitle;
        this.importedFolderPath = importedFolderPath;
        this.status = status;
        this.note = note;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public FolderImportBaseJsBean(FolderImportBaseJsBean bean)
    {
        if(bean != null) {
            setGuid(bean.getGuid());
            setUser(bean.getUser());
            setPrecedence(bean.getPrecedence());
            setImportType(bean.getImportType());
            setImportedFolder(bean.getImportedFolder());
            setImportedFolderUser(bean.getImportedFolderUser());
            setImportedFolderTitle(bean.getImportedFolderTitle());
            setImportedFolderPath(bean.getImportedFolderPath());
            setStatus(bean.getStatus());
            setNote(bean.getNote());
            setCreatedTime(bean.getCreatedTime());
            setModifiedTime(bean.getModifiedTime());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static FolderImportBaseJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        FolderImportBaseJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(FolderImportBaseJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, FolderImportBaseJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    public Integer getPrecedence()
    {
        return this.precedence;
    }
    public void setPrecedence(Integer precedence)
    {
        this.precedence = precedence;
    }

    public String getImportType()
    {
        return this.importType;
    }
    public void setImportType(String importType)
    {
        this.importType = importType;
    }

    public String getImportedFolder()
    {
        return this.importedFolder;
    }
    public void setImportedFolder(String importedFolder)
    {
        this.importedFolder = importedFolder;
    }

    public String getImportedFolderUser()
    {
        return this.importedFolderUser;
    }
    public void setImportedFolderUser(String importedFolderUser)
    {
        this.importedFolderUser = importedFolderUser;
    }

    public String getImportedFolderTitle()
    {
        return this.importedFolderTitle;
    }
    public void setImportedFolderTitle(String importedFolderTitle)
    {
        this.importedFolderTitle = importedFolderTitle;
    }

    public String getImportedFolderPath()
    {
        return this.importedFolderPath;
    }
    public void setImportedFolderPath(String importedFolderPath)
    {
        this.importedFolderPath = importedFolderPath;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:null, ");
        sb.append("user:null, ");
        sb.append("precedence:0, ");
        sb.append("importType:null, ");
        sb.append("importedFolder:null, ");
        sb.append("importedFolderUser:null, ");
        sb.append("importedFolderTitle:null, ");
        sb.append("importedFolderPath:null, ");
        sb.append("status:null, ");
        sb.append("note:null, ");
        sb.append("createdTime:0, ");
        sb.append("modifiedTime:0, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:");
        if(this.getGuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGuid()).append("\", ");
        }
        sb.append("user:");
        if(this.getUser() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUser()).append("\", ");
        }
        sb.append("precedence:" + this.getPrecedence()).append(", ");
        sb.append("importType:");
        if(this.getImportType() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getImportType()).append("\", ");
        }
        sb.append("importedFolder:");
        if(this.getImportedFolder() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getImportedFolder()).append("\", ");
        }
        sb.append("importedFolderUser:");
        if(this.getImportedFolderUser() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getImportedFolderUser()).append("\", ");
        }
        sb.append("importedFolderTitle:");
        if(this.getImportedFolderTitle() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getImportedFolderTitle()).append("\", ");
        }
        sb.append("importedFolderPath:");
        if(this.getImportedFolderPath() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getImportedFolderPath()).append("\", ");
        }
        sb.append("status:");
        if(this.getStatus() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getStatus()).append("\", ");
        }
        sb.append("note:");
        if(this.getNote() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNote()).append("\", ");
        }
        sb.append("createdTime:" + this.getCreatedTime()).append(", ");
        sb.append("modifiedTime:" + this.getModifiedTime()).append(", ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getGuid() != null) {
            sb.append("\"guid\":").append("\"").append(this.getGuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"guid\":").append("null, ");
        }
        if(this.getUser() != null) {
            sb.append("\"user\":").append("\"").append(this.getUser()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"user\":").append("null, ");
        }
        if(this.getPrecedence() != null) {
            sb.append("\"precedence\":").append("").append(this.getPrecedence()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"precedence\":").append("null, ");
        }
        if(this.getImportType() != null) {
            sb.append("\"importType\":").append("\"").append(this.getImportType()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"importType\":").append("null, ");
        }
        if(this.getImportedFolder() != null) {
            sb.append("\"importedFolder\":").append("\"").append(this.getImportedFolder()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"importedFolder\":").append("null, ");
        }
        if(this.getImportedFolderUser() != null) {
            sb.append("\"importedFolderUser\":").append("\"").append(this.getImportedFolderUser()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"importedFolderUser\":").append("null, ");
        }
        if(this.getImportedFolderTitle() != null) {
            sb.append("\"importedFolderTitle\":").append("\"").append(this.getImportedFolderTitle()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"importedFolderTitle\":").append("null, ");
        }
        if(this.getImportedFolderPath() != null) {
            sb.append("\"importedFolderPath\":").append("\"").append(this.getImportedFolderPath()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"importedFolderPath\":").append("null, ");
        }
        if(this.getStatus() != null) {
            sb.append("\"status\":").append("\"").append(this.getStatus()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"status\":").append("null, ");
        }
        if(this.getNote() != null) {
            sb.append("\"note\":").append("\"").append(this.getNote()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"note\":").append("null, ");
        }
        if(this.getCreatedTime() != null) {
            sb.append("\"createdTime\":").append("").append(this.getCreatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"createdTime\":").append("null, ");
        }
        if(this.getModifiedTime() != null) {
            sb.append("\"modifiedTime\":").append("").append(this.getModifiedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"modifiedTime\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("guid = " + this.guid).append(";");
        sb.append("user = " + this.user).append(";");
        sb.append("precedence = " + this.precedence).append(";");
        sb.append("importType = " + this.importType).append(";");
        sb.append("importedFolder = " + this.importedFolder).append(";");
        sb.append("importedFolderUser = " + this.importedFolderUser).append(";");
        sb.append("importedFolderTitle = " + this.importedFolderTitle).append(";");
        sb.append("importedFolderPath = " + this.importedFolderPath).append(";");
        sb.append("status = " + this.status).append(";");
        sb.append("note = " + this.note).append(";");
        sb.append("createdTime = " + this.createdTime).append(";");
        sb.append("modifiedTime = " + this.modifiedTime).append(";");
        return sb.toString();
    }


}
