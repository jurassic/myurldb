package com.cannyurl.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.cannyurl.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class FullNameStructJsBean implements Serializable, Cloneable  //, FullNameStruct
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(FullNameStructJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String uuid;
    private String displayName;
    private String lastName;
    private String firstName;
    private String middleName1;
    private String middleName2;
    private String middleInitial;
    private String salutation;
    private String suffix;
    private String note;

    // Ctors.
    public FullNameStructJsBean()
    {
        //this((String) null);
    }
    public FullNameStructJsBean(String uuid, String displayName, String lastName, String firstName, String middleName1, String middleName2, String middleInitial, String salutation, String suffix, String note)
    {
        this.uuid = uuid;
        this.displayName = displayName;
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName1 = middleName1;
        this.middleName2 = middleName2;
        this.middleInitial = middleInitial;
        this.salutation = salutation;
        this.suffix = suffix;
        this.note = note;
    }
    public FullNameStructJsBean(FullNameStructJsBean bean)
    {
        if(bean != null) {
            setUuid(bean.getUuid());
            setDisplayName(bean.getDisplayName());
            setLastName(bean.getLastName());
            setFirstName(bean.getFirstName());
            setMiddleName1(bean.getMiddleName1());
            setMiddleName2(bean.getMiddleName2());
            setMiddleInitial(bean.getMiddleInitial());
            setSalutation(bean.getSalutation());
            setSuffix(bean.getSuffix());
            setNote(bean.getNote());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static FullNameStructJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        FullNameStructJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(FullNameStructJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, FullNameStructJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getUuid()
    {
        return this.uuid;
    }
    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }

    public String getDisplayName()
    {
        return this.displayName;
    }
    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }

    public String getLastName()
    {
        return this.lastName;
    }
    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public String getFirstName()
    {
        return this.firstName;
    }
    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getMiddleName1()
    {
        return this.middleName1;
    }
    public void setMiddleName1(String middleName1)
    {
        this.middleName1 = middleName1;
    }

    public String getMiddleName2()
    {
        return this.middleName2;
    }
    public void setMiddleName2(String middleName2)
    {
        this.middleName2 = middleName2;
    }

    public String getMiddleInitial()
    {
        return this.middleInitial;
    }
    public void setMiddleInitial(String middleInitial)
    {
        this.middleInitial = middleInitial;
    }

    public String getSalutation()
    {
        return this.salutation;
    }
    public void setSalutation(String salutation)
    {
        this.salutation = salutation;
    }

    public String getSuffix()
    {
        return this.suffix;
    }
    public void setSuffix(String suffix)
    {
        this.suffix = suffix;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }


    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getDisplayName() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLastName() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getFirstName() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getMiddleName1() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getMiddleName2() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getMiddleInitial() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSalutation() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSuffix() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("uuid:null, ");
        sb.append("displayName:null, ");
        sb.append("lastName:null, ");
        sb.append("firstName:null, ");
        sb.append("middleName1:null, ");
        sb.append("middleName2:null, ");
        sb.append("middleInitial:null, ");
        sb.append("salutation:null, ");
        sb.append("suffix:null, ");
        sb.append("note:null, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("uuid:");
        if(this.getUuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUuid()).append("\", ");
        }
        sb.append("displayName:");
        if(this.getDisplayName() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDisplayName()).append("\", ");
        }
        sb.append("lastName:");
        if(this.getLastName() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getLastName()).append("\", ");
        }
        sb.append("firstName:");
        if(this.getFirstName() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getFirstName()).append("\", ");
        }
        sb.append("middleName1:");
        if(this.getMiddleName1() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getMiddleName1()).append("\", ");
        }
        sb.append("middleName2:");
        if(this.getMiddleName2() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getMiddleName2()).append("\", ");
        }
        sb.append("middleInitial:");
        if(this.getMiddleInitial() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getMiddleInitial()).append("\", ");
        }
        sb.append("salutation:");
        if(this.getSalutation() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getSalutation()).append("\", ");
        }
        sb.append("suffix:");
        if(this.getSuffix() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getSuffix()).append("\", ");
        }
        sb.append("note:");
        if(this.getNote() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNote()).append("\", ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getUuid() != null) {
            sb.append("\"uuid\":").append("\"").append(this.getUuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"uuid\":").append("null, ");
        }
        if(this.getDisplayName() != null) {
            sb.append("\"displayName\":").append("\"").append(this.getDisplayName()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"displayName\":").append("null, ");
        }
        if(this.getLastName() != null) {
            sb.append("\"lastName\":").append("\"").append(this.getLastName()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"lastName\":").append("null, ");
        }
        if(this.getFirstName() != null) {
            sb.append("\"firstName\":").append("\"").append(this.getFirstName()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"firstName\":").append("null, ");
        }
        if(this.getMiddleName1() != null) {
            sb.append("\"middleName1\":").append("\"").append(this.getMiddleName1()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"middleName1\":").append("null, ");
        }
        if(this.getMiddleName2() != null) {
            sb.append("\"middleName2\":").append("\"").append(this.getMiddleName2()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"middleName2\":").append("null, ");
        }
        if(this.getMiddleInitial() != null) {
            sb.append("\"middleInitial\":").append("\"").append(this.getMiddleInitial()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"middleInitial\":").append("null, ");
        }
        if(this.getSalutation() != null) {
            sb.append("\"salutation\":").append("\"").append(this.getSalutation()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"salutation\":").append("null, ");
        }
        if(this.getSuffix() != null) {
            sb.append("\"suffix\":").append("\"").append(this.getSuffix()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"suffix\":").append("null, ");
        }
        if(this.getNote() != null) {
            sb.append("\"note\":").append("\"").append(this.getNote()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"note\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("uuid = " + this.uuid).append(";");
        sb.append("displayName = " + this.displayName).append(";");
        sb.append("lastName = " + this.lastName).append(";");
        sb.append("firstName = " + this.firstName).append(";");
        sb.append("middleName1 = " + this.middleName1).append(";");
        sb.append("middleName2 = " + this.middleName2).append(";");
        sb.append("middleInitial = " + this.middleInitial).append(";");
        sb.append("salutation = " + this.salutation).append(";");
        sb.append("suffix = " + this.suffix).append(";");
        sb.append("note = " + this.note).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        FullNameStructJsBean cloned = new FullNameStructJsBean();
        cloned.setUuid(this.getUuid());   
        cloned.setDisplayName(this.getDisplayName());   
        cloned.setLastName(this.getLastName());   
        cloned.setFirstName(this.getFirstName());   
        cloned.setMiddleName1(this.getMiddleName1());   
        cloned.setMiddleName2(this.getMiddleName2());   
        cloned.setMiddleInitial(this.getMiddleInitial());   
        cloned.setSalutation(this.getSalutation());   
        cloned.setSuffix(this.getSuffix());   
        cloned.setNote(this.getNote());   
        return cloned;
    }

}
