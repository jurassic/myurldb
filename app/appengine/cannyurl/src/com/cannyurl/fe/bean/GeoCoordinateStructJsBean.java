package com.cannyurl.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.cannyurl.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class GeoCoordinateStructJsBean extends GeoPointStructJsBean implements Serializable, Cloneable  //, GeoCoordinateStruct
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(GeoCoordinateStructJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String accuracy;
    private String altitudeAccuracy;
    private Double heading;
    private Double speed;
    private String note;

    // Ctors.
    public GeoCoordinateStructJsBean()
    {
        //this((String) null);
    }
    public GeoCoordinateStructJsBean(String uuid, Double latitude, Double longitude, Double altitude, Boolean sensorUsed, String accuracy, String altitudeAccuracy, Double heading, Double speed, String note)
    {
        super(uuid, latitude, longitude, altitude, sensorUsed);

        this.accuracy = accuracy;
        this.altitudeAccuracy = altitudeAccuracy;
        this.heading = heading;
        this.speed = speed;
        this.note = note;
    }
    public GeoCoordinateStructJsBean(GeoCoordinateStructJsBean bean)
    {
        if(bean != null) {
            setUuid(bean.getUuid());
            setLatitude(bean.getLatitude());
            setLongitude(bean.getLongitude());
            setAltitude(bean.getAltitude());
            setSensorUsed(bean.isSensorUsed());
            setAccuracy(bean.getAccuracy());
            setAltitudeAccuracy(bean.getAltitudeAccuracy());
            setHeading(bean.getHeading());
            setSpeed(bean.getSpeed());
            setNote(bean.getNote());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static GeoCoordinateStructJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        GeoCoordinateStructJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(GeoCoordinateStructJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, GeoCoordinateStructJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getUuid()
    {
        return super.getUuid();
    }
    public void setUuid(String uuid)
    {
        super.setUuid(uuid);
    }

    public Double getLatitude()
    {
        return super.getLatitude();
    }
    public void setLatitude(Double latitude)
    {
        super.setLatitude(latitude);
    }

    public Double getLongitude()
    {
        return super.getLongitude();
    }
    public void setLongitude(Double longitude)
    {
        super.setLongitude(longitude);
    }

    public Double getAltitude()
    {
        return super.getAltitude();
    }
    public void setAltitude(Double altitude)
    {
        super.setAltitude(altitude);
    }

    public Boolean isSensorUsed()
    {
        return super.isSensorUsed();
    }
    public void setSensorUsed(Boolean sensorUsed)
    {
        super.setSensorUsed(sensorUsed);
    }

    public String getAccuracy()
    {
        return this.accuracy;
    }
    public void setAccuracy(String accuracy)
    {
        this.accuracy = accuracy;
    }

    public String getAltitudeAccuracy()
    {
        return this.altitudeAccuracy;
    }
    public void setAltitudeAccuracy(String altitudeAccuracy)
    {
        this.altitudeAccuracy = altitudeAccuracy;
    }

    public Double getHeading()
    {
        return this.heading;
    }
    public void setHeading(Double heading)
    {
        this.heading = heading;
    }

    public Double getSpeed()
    {
        return this.speed;
    }
    public void setSpeed(Double speed)
    {
        this.speed = speed;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }


    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getLatitude() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLongitude() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAltitude() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && isSensorUsed() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAccuracy() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAltitudeAccuracy() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHeading() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSpeed() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("uuid:null, ");
        sb.append("latitude:0, ");
        sb.append("longitude:0, ");
        sb.append("altitude:0, ");
        sb.append("sensorUsed:false, ");
        sb.append("accuracy:null, ");
        sb.append("altitudeAccuracy:null, ");
        sb.append("heading:0, ");
        sb.append("speed:0, ");
        sb.append("note:null, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("uuid:");
        if(this.getUuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUuid()).append("\", ");
        }
        sb.append("latitude:" + this.getLatitude()).append(", ");
        sb.append("longitude:" + this.getLongitude()).append(", ");
        sb.append("altitude:" + this.getAltitude()).append(", ");
        sb.append("sensorUsed:" + this.isSensorUsed()).append(", ");
        sb.append("accuracy:");
        if(this.getAccuracy() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getAccuracy()).append("\", ");
        }
        sb.append("altitudeAccuracy:");
        if(this.getAltitudeAccuracy() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getAltitudeAccuracy()).append("\", ");
        }
        sb.append("heading:" + this.getHeading()).append(", ");
        sb.append("speed:" + this.getSpeed()).append(", ");
        sb.append("note:");
        if(this.getNote() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNote()).append("\", ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getUuid() != null) {
            sb.append("\"uuid\":").append("\"").append(this.getUuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"uuid\":").append("null, ");
        }
        if(this.getLatitude() != null) {
            sb.append("\"latitude\":").append("").append(this.getLatitude()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"latitude\":").append("null, ");
        }
        if(this.getLongitude() != null) {
            sb.append("\"longitude\":").append("").append(this.getLongitude()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"longitude\":").append("null, ");
        }
        if(this.getAltitude() != null) {
            sb.append("\"altitude\":").append("").append(this.getAltitude()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"altitude\":").append("null, ");
        }
        if(this.isSensorUsed() != null) {
            sb.append("\"sensorUsed\":").append("").append(this.isSensorUsed()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"sensorUsed\":").append("null, ");
        }
        if(this.getAccuracy() != null) {
            sb.append("\"accuracy\":").append("\"").append(this.getAccuracy()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"accuracy\":").append("null, ");
        }
        if(this.getAltitudeAccuracy() != null) {
            sb.append("\"altitudeAccuracy\":").append("\"").append(this.getAltitudeAccuracy()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"altitudeAccuracy\":").append("null, ");
        }
        if(this.getHeading() != null) {
            sb.append("\"heading\":").append("").append(this.getHeading()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"heading\":").append("null, ");
        }
        if(this.getSpeed() != null) {
            sb.append("\"speed\":").append("").append(this.getSpeed()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"speed\":").append("null, ");
        }
        if(this.getNote() != null) {
            sb.append("\"note\":").append("\"").append(this.getNote()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"note\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer( super.toString() );
        sb.append("accuracy = " + this.accuracy).append(";");
        sb.append("altitudeAccuracy = " + this.altitudeAccuracy).append(";");
        sb.append("heading = " + this.heading).append(";");
        sb.append("speed = " + this.speed).append(";");
        sb.append("note = " + this.note).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        GeoCoordinateStructJsBean cloned = new GeoCoordinateStructJsBean();
        cloned.setUuid(this.getUuid());   
        cloned.setLatitude(this.getLatitude());   
        cloned.setLongitude(this.getLongitude());   
        cloned.setAltitude(this.getAltitude());   
        cloned.setSensorUsed(this.isSensorUsed());   
        cloned.setAccuracy(this.getAccuracy());   
        cloned.setAltitudeAccuracy(this.getAltitudeAccuracy());   
        cloned.setHeading(this.getHeading());   
        cloned.setSpeed(this.getSpeed());   
        cloned.setNote(this.getNote());   
        return cloned;
    }

}
