package com.cannyurl.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.myurldb.ws.CellLatitudeLongitude;
import com.myurldb.ws.GeoCoordinateStruct;
import com.cannyurl.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class GeoLinkJsBean implements Serializable, Cloneable  //, GeoLink
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(GeoLinkJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String guid;
    private String shortLink;
    private String shortUrl;
    private GeoCoordinateStructJsBean geoCoordinate;
    private CellLatitudeLongitudeJsBean geoCell;
    private String status;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public GeoLinkJsBean()
    {
        //this((String) null);
    }
    public GeoLinkJsBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null);
    }
    public GeoLinkJsBean(String guid, String shortLink, String shortUrl, GeoCoordinateStructJsBean geoCoordinate, CellLatitudeLongitudeJsBean geoCell, String status)
    {
        this(guid, shortLink, shortUrl, geoCoordinate, geoCell, status, null, null);
    }
    public GeoLinkJsBean(String guid, String shortLink, String shortUrl, GeoCoordinateStructJsBean geoCoordinate, CellLatitudeLongitudeJsBean geoCell, String status, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.shortLink = shortLink;
        this.shortUrl = shortUrl;
        this.geoCoordinate = geoCoordinate;
        this.geoCell = geoCell;
        this.status = status;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public GeoLinkJsBean(GeoLinkJsBean bean)
    {
        if(bean != null) {
            setGuid(bean.getGuid());
            setShortLink(bean.getShortLink());
            setShortUrl(bean.getShortUrl());
            setGeoCoordinate(bean.getGeoCoordinate());
            setGeoCell(bean.getGeoCell());
            setStatus(bean.getStatus());
            setCreatedTime(bean.getCreatedTime());
            setModifiedTime(bean.getModifiedTime());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static GeoLinkJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        GeoLinkJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(GeoLinkJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, GeoLinkJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public String getShortLink()
    {
        return this.shortLink;
    }
    public void setShortLink(String shortLink)
    {
        this.shortLink = shortLink;
    }

    public String getShortUrl()
    {
        return this.shortUrl;
    }
    public void setShortUrl(String shortUrl)
    {
        this.shortUrl = shortUrl;
    }

    public GeoCoordinateStructJsBean getGeoCoordinate()
    {  
        return this.geoCoordinate;
    }
    public void setGeoCoordinate(GeoCoordinateStructJsBean geoCoordinate)
    {
        this.geoCoordinate = geoCoordinate;
    }

    public CellLatitudeLongitudeJsBean getGeoCell()
    {  
        return this.geoCell;
    }
    public void setGeoCell(CellLatitudeLongitudeJsBean geoCell)
    {
        this.geoCell = geoCell;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:null, ");
        sb.append("shortLink:null, ");
        sb.append("shortUrl:null, ");
        sb.append("geoCoordinate:{}, ");
        sb.append("geoCell:{}, ");
        sb.append("status:null, ");
        sb.append("createdTime:0, ");
        sb.append("modifiedTime:0, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:");
        if(this.getGuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGuid()).append("\", ");
        }
        sb.append("shortLink:");
        if(this.getShortLink() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getShortLink()).append("\", ");
        }
        sb.append("shortUrl:");
        if(this.getShortUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getShortUrl()).append("\", ");
        }
        sb.append("geoCoordinate:");
        if(this.getGeoCoordinate() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGeoCoordinate()).append("\", ");
        }
        sb.append("geoCell:");
        if(this.getGeoCell() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGeoCell()).append("\", ");
        }
        sb.append("status:");
        if(this.getStatus() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getStatus()).append("\", ");
        }
        sb.append("createdTime:" + this.getCreatedTime()).append(", ");
        sb.append("modifiedTime:" + this.getModifiedTime()).append(", ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getGuid() != null) {
            sb.append("\"guid\":").append("\"").append(this.getGuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"guid\":").append("null, ");
        }
        if(this.getShortLink() != null) {
            sb.append("\"shortLink\":").append("\"").append(this.getShortLink()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"shortLink\":").append("null, ");
        }
        if(this.getShortUrl() != null) {
            sb.append("\"shortUrl\":").append("\"").append(this.getShortUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"shortUrl\":").append("null, ");
        }
        sb.append("\"geoCoordinate\":").append(this.geoCoordinate.toJsonString()).append(", ");
        sb.append("\"geoCell\":").append(this.geoCell.toJsonString()).append(", ");
        if(this.getStatus() != null) {
            sb.append("\"status\":").append("\"").append(this.getStatus()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"status\":").append("null, ");
        }
        if(this.getCreatedTime() != null) {
            sb.append("\"createdTime\":").append("").append(this.getCreatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"createdTime\":").append("null, ");
        }
        if(this.getModifiedTime() != null) {
            sb.append("\"modifiedTime\":").append("").append(this.getModifiedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"modifiedTime\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("guid = " + this.guid).append(";");
        sb.append("shortLink = " + this.shortLink).append(";");
        sb.append("shortUrl = " + this.shortUrl).append(";");
        sb.append("geoCoordinate = " + this.geoCoordinate).append(";");
        sb.append("geoCell = " + this.geoCell).append(";");
        sb.append("status = " + this.status).append(";");
        sb.append("createdTime = " + this.createdTime).append(";");
        sb.append("modifiedTime = " + this.modifiedTime).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        GeoLinkJsBean cloned = new GeoLinkJsBean();
        cloned.setGuid(null);          // Do not clone the guid.
        cloned.setShortLink(this.getShortLink());   
        cloned.setShortUrl(this.getShortUrl());   
        cloned.setGeoCoordinate( (GeoCoordinateStructJsBean) this.getGeoCoordinate().clone() );
        cloned.setGeoCell( (CellLatitudeLongitudeJsBean) this.getGeoCell().clone() );
        cloned.setStatus(this.getStatus());   
        cloned.setCreatedTime(null);   // Reset the timestamp.
        cloned.setModifiedTime(null);   // Reset the timestamp.
        return cloned;
    }

}
