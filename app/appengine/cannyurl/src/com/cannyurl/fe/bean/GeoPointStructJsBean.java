package com.cannyurl.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.cannyurl.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class GeoPointStructJsBean implements Serializable, Cloneable  //, GeoPointStruct
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(GeoPointStructJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String uuid;
    private Double latitude;
    private Double longitude;
    private Double altitude;
    private Boolean sensorUsed;

    // Ctors.
    public GeoPointStructJsBean()
    {
        //this((String) null);
    }
    public GeoPointStructJsBean(String uuid, Double latitude, Double longitude, Double altitude, Boolean sensorUsed)
    {
        this.uuid = uuid;
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
        this.sensorUsed = sensorUsed;
    }
    public GeoPointStructJsBean(GeoPointStructJsBean bean)
    {
        if(bean != null) {
            setUuid(bean.getUuid());
            setLatitude(bean.getLatitude());
            setLongitude(bean.getLongitude());
            setAltitude(bean.getAltitude());
            setSensorUsed(bean.isSensorUsed());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static GeoPointStructJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        GeoPointStructJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(GeoPointStructJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, GeoPointStructJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getUuid()
    {
        return this.uuid;
    }
    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }

    public Double getLatitude()
    {
        return this.latitude;
    }
    public void setLatitude(Double latitude)
    {
        this.latitude = latitude;
    }

    public Double getLongitude()
    {
        return this.longitude;
    }
    public void setLongitude(Double longitude)
    {
        this.longitude = longitude;
    }

    public Double getAltitude()
    {
        return this.altitude;
    }
    public void setAltitude(Double altitude)
    {
        this.altitude = altitude;
    }

    public Boolean isSensorUsed()
    {
        return this.sensorUsed;
    }
    public void setSensorUsed(Boolean sensorUsed)
    {
        this.sensorUsed = sensorUsed;
    }


    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getLatitude() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLongitude() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAltitude() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && isSensorUsed() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("uuid:null, ");
        sb.append("latitude:0, ");
        sb.append("longitude:0, ");
        sb.append("altitude:0, ");
        sb.append("sensorUsed:false, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("uuid:");
        if(this.getUuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUuid()).append("\", ");
        }
        sb.append("latitude:" + this.getLatitude()).append(", ");
        sb.append("longitude:" + this.getLongitude()).append(", ");
        sb.append("altitude:" + this.getAltitude()).append(", ");
        sb.append("sensorUsed:" + this.isSensorUsed()).append(", ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getUuid() != null) {
            sb.append("\"uuid\":").append("\"").append(this.getUuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"uuid\":").append("null, ");
        }
        if(this.getLatitude() != null) {
            sb.append("\"latitude\":").append("").append(this.getLatitude()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"latitude\":").append("null, ");
        }
        if(this.getLongitude() != null) {
            sb.append("\"longitude\":").append("").append(this.getLongitude()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"longitude\":").append("null, ");
        }
        if(this.getAltitude() != null) {
            sb.append("\"altitude\":").append("").append(this.getAltitude()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"altitude\":").append("null, ");
        }
        if(this.isSensorUsed() != null) {
            sb.append("\"sensorUsed\":").append("").append(this.isSensorUsed()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"sensorUsed\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("uuid = " + this.uuid).append(";");
        sb.append("latitude = " + this.latitude).append(";");
        sb.append("longitude = " + this.longitude).append(";");
        sb.append("altitude = " + this.altitude).append(";");
        sb.append("sensorUsed = " + this.sensorUsed).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        GeoPointStructJsBean cloned = new GeoPointStructJsBean();
        cloned.setUuid(this.getUuid());   
        cloned.setLatitude(this.getLatitude());   
        cloned.setLongitude(this.getLongitude());   
        cloned.setAltitude(this.getAltitude());   
        cloned.setSensorUsed(this.isSensorUsed());   
        return cloned;
    }

}
