package com.cannyurl.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.cannyurl.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class HashedPasswordStructJsBean implements Serializable, Cloneable  //, HashedPasswordStruct
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(HashedPasswordStructJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String uuid;
    private String plainText;
    private String hashedText;
    private String salt;
    private String algorithm;

    // Ctors.
    public HashedPasswordStructJsBean()
    {
        //this((String) null);
    }
    public HashedPasswordStructJsBean(String uuid, String plainText, String hashedText, String salt, String algorithm)
    {
        this.uuid = uuid;
        this.plainText = plainText;
        this.hashedText = hashedText;
        this.salt = salt;
        this.algorithm = algorithm;
    }
    public HashedPasswordStructJsBean(HashedPasswordStructJsBean bean)
    {
        if(bean != null) {
            setUuid(bean.getUuid());
            setPlainText(bean.getPlainText());
            setHashedText(bean.getHashedText());
            setSalt(bean.getSalt());
            setAlgorithm(bean.getAlgorithm());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static HashedPasswordStructJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        HashedPasswordStructJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(HashedPasswordStructJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, HashedPasswordStructJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getUuid()
    {
        return this.uuid;
    }
    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }

    public String getPlainText()
    {
        return this.plainText;
    }
    public void setPlainText(String plainText)
    {
        this.plainText = plainText;
    }

    public String getHashedText()
    {
        return this.hashedText;
    }
    public void setHashedText(String hashedText)
    {
        this.hashedText = hashedText;
    }

    public String getSalt()
    {
        return this.salt;
    }
    public void setSalt(String salt)
    {
        this.salt = salt;
    }

    public String getAlgorithm()
    {
        return this.algorithm;
    }
    public void setAlgorithm(String algorithm)
    {
        this.algorithm = algorithm;
    }


    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getPlainText() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHashedText() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSalt() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAlgorithm() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("uuid:null, ");
        sb.append("plainText:null, ");
        sb.append("hashedText:null, ");
        sb.append("salt:null, ");
        sb.append("algorithm:null, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("uuid:");
        if(this.getUuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUuid()).append("\", ");
        }
        sb.append("plainText:");
        if(this.getPlainText() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getPlainText()).append("\", ");
        }
        sb.append("hashedText:");
        if(this.getHashedText() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getHashedText()).append("\", ");
        }
        sb.append("salt:");
        if(this.getSalt() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getSalt()).append("\", ");
        }
        sb.append("algorithm:");
        if(this.getAlgorithm() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getAlgorithm()).append("\", ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getUuid() != null) {
            sb.append("\"uuid\":").append("\"").append(this.getUuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"uuid\":").append("null, ");
        }
        if(this.getPlainText() != null) {
            sb.append("\"plainText\":").append("\"").append(this.getPlainText()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"plainText\":").append("null, ");
        }
        if(this.getHashedText() != null) {
            sb.append("\"hashedText\":").append("\"").append(this.getHashedText()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"hashedText\":").append("null, ");
        }
        if(this.getSalt() != null) {
            sb.append("\"salt\":").append("\"").append(this.getSalt()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"salt\":").append("null, ");
        }
        if(this.getAlgorithm() != null) {
            sb.append("\"algorithm\":").append("\"").append(this.getAlgorithm()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"algorithm\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("uuid = " + this.uuid).append(";");
        sb.append("plainText = " + this.plainText).append(";");
        sb.append("hashedText = " + this.hashedText).append(";");
        sb.append("salt = " + this.salt).append(";");
        sb.append("algorithm = " + this.algorithm).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        HashedPasswordStructJsBean cloned = new HashedPasswordStructJsBean();
        cloned.setUuid(this.getUuid());   
        cloned.setPlainText(this.getPlainText());   
        cloned.setHashedText(this.getHashedText());   
        cloned.setSalt(this.getSalt());   
        cloned.setAlgorithm(this.getAlgorithm());   
        return cloned;
    }

}
