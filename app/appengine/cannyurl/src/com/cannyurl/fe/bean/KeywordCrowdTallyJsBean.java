package com.cannyurl.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.cannyurl.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class KeywordCrowdTallyJsBean extends CrowdTallyBaseJsBean implements Serializable, Cloneable  //, KeywordCrowdTally
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(KeywordCrowdTallyJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String keywordFolder;
    private String folderPath;
    private String keyword;
    private String queryKey;
    private String scope;
    private Boolean caseSensitive;

    // Ctors.
    public KeywordCrowdTallyJsBean()
    {
        //this((String) null);
    }
    public KeywordCrowdTallyJsBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public KeywordCrowdTallyJsBean(String guid, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, String tallyDate, String status, String note, Long expirationTime, String keywordFolder, String folderPath, String keyword, String queryKey, String scope, Boolean caseSensitive)
    {
        this(guid, user, shortLink, domain, token, longUrl, shortUrl, tallyDate, status, note, expirationTime, keywordFolder, folderPath, keyword, queryKey, scope, caseSensitive, null, null);
    }
    public KeywordCrowdTallyJsBean(String guid, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, String tallyDate, String status, String note, Long expirationTime, String keywordFolder, String folderPath, String keyword, String queryKey, String scope, Boolean caseSensitive, Long createdTime, Long modifiedTime)
    {
        super(guid, user, shortLink, domain, token, longUrl, shortUrl, tallyDate, status, note, expirationTime, createdTime, modifiedTime);

        this.keywordFolder = keywordFolder;
        this.folderPath = folderPath;
        this.keyword = keyword;
        this.queryKey = queryKey;
        this.scope = scope;
        this.caseSensitive = caseSensitive;
    }
    public KeywordCrowdTallyJsBean(KeywordCrowdTallyJsBean bean)
    {
        if(bean != null) {
            setGuid(bean.getGuid());
            setUser(bean.getUser());
            setShortLink(bean.getShortLink());
            setDomain(bean.getDomain());
            setToken(bean.getToken());
            setLongUrl(bean.getLongUrl());
            setShortUrl(bean.getShortUrl());
            setTallyDate(bean.getTallyDate());
            setStatus(bean.getStatus());
            setNote(bean.getNote());
            setExpirationTime(bean.getExpirationTime());
            setKeywordFolder(bean.getKeywordFolder());
            setFolderPath(bean.getFolderPath());
            setKeyword(bean.getKeyword());
            setQueryKey(bean.getQueryKey());
            setScope(bean.getScope());
            setCaseSensitive(bean.isCaseSensitive());
            setCreatedTime(bean.getCreatedTime());
            setModifiedTime(bean.getModifiedTime());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static KeywordCrowdTallyJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        KeywordCrowdTallyJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(KeywordCrowdTallyJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, KeywordCrowdTallyJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    public String getUser()
    {
        return super.getUser();
    }
    public void setUser(String user)
    {
        super.setUser(user);
    }

    public String getShortLink()
    {
        return super.getShortLink();
    }
    public void setShortLink(String shortLink)
    {
        super.setShortLink(shortLink);
    }

    public String getDomain()
    {
        return super.getDomain();
    }
    public void setDomain(String domain)
    {
        super.setDomain(domain);
    }

    public String getToken()
    {
        return super.getToken();
    }
    public void setToken(String token)
    {
        super.setToken(token);
    }

    public String getLongUrl()
    {
        return super.getLongUrl();
    }
    public void setLongUrl(String longUrl)
    {
        super.setLongUrl(longUrl);
    }

    public String getShortUrl()
    {
        return super.getShortUrl();
    }
    public void setShortUrl(String shortUrl)
    {
        super.setShortUrl(shortUrl);
    }

    public String getTallyDate()
    {
        return super.getTallyDate();
    }
    public void setTallyDate(String tallyDate)
    {
        super.setTallyDate(tallyDate);
    }

    public String getStatus()
    {
        return super.getStatus();
    }
    public void setStatus(String status)
    {
        super.setStatus(status);
    }

    public String getNote()
    {
        return super.getNote();
    }
    public void setNote(String note)
    {
        super.setNote(note);
    }

    public Long getExpirationTime()
    {
        return super.getExpirationTime();
    }
    public void setExpirationTime(Long expirationTime)
    {
        super.setExpirationTime(expirationTime);
    }

    public String getKeywordFolder()
    {
        return this.keywordFolder;
    }
    public void setKeywordFolder(String keywordFolder)
    {
        this.keywordFolder = keywordFolder;
    }

    public String getFolderPath()
    {
        return this.folderPath;
    }
    public void setFolderPath(String folderPath)
    {
        this.folderPath = folderPath;
    }

    public String getKeyword()
    {
        return this.keyword;
    }
    public void setKeyword(String keyword)
    {
        this.keyword = keyword;
    }

    public String getQueryKey()
    {
        return this.queryKey;
    }
    public void setQueryKey(String queryKey)
    {
        this.queryKey = queryKey;
    }

    public String getScope()
    {
        return this.scope;
    }
    public void setScope(String scope)
    {
        this.scope = scope;
    }

    public Boolean isCaseSensitive()
    {
        return this.caseSensitive;
    }
    public void setCaseSensitive(Boolean caseSensitive)
    {
        this.caseSensitive = caseSensitive;
    }

    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:null, ");
        sb.append("user:null, ");
        sb.append("shortLink:null, ");
        sb.append("domain:null, ");
        sb.append("token:null, ");
        sb.append("longUrl:null, ");
        sb.append("shortUrl:null, ");
        sb.append("tallyDate:null, ");
        sb.append("status:null, ");
        sb.append("note:null, ");
        sb.append("expirationTime:0, ");
        sb.append("keywordFolder:null, ");
        sb.append("folderPath:null, ");
        sb.append("keyword:null, ");
        sb.append("queryKey:null, ");
        sb.append("scope:null, ");
        sb.append("caseSensitive:false, ");
        sb.append("createdTime:0, ");
        sb.append("modifiedTime:0, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:");
        if(this.getGuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGuid()).append("\", ");
        }
        sb.append("user:");
        if(this.getUser() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUser()).append("\", ");
        }
        sb.append("shortLink:");
        if(this.getShortLink() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getShortLink()).append("\", ");
        }
        sb.append("domain:");
        if(this.getDomain() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDomain()).append("\", ");
        }
        sb.append("token:");
        if(this.getToken() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getToken()).append("\", ");
        }
        sb.append("longUrl:");
        if(this.getLongUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getLongUrl()).append("\", ");
        }
        sb.append("shortUrl:");
        if(this.getShortUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getShortUrl()).append("\", ");
        }
        sb.append("tallyDate:");
        if(this.getTallyDate() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getTallyDate()).append("\", ");
        }
        sb.append("status:");
        if(this.getStatus() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getStatus()).append("\", ");
        }
        sb.append("note:");
        if(this.getNote() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNote()).append("\", ");
        }
        sb.append("expirationTime:" + this.getExpirationTime()).append(", ");
        sb.append("keywordFolder:");
        if(this.getKeywordFolder() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getKeywordFolder()).append("\", ");
        }
        sb.append("folderPath:");
        if(this.getFolderPath() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getFolderPath()).append("\", ");
        }
        sb.append("keyword:");
        if(this.getKeyword() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getKeyword()).append("\", ");
        }
        sb.append("queryKey:");
        if(this.getQueryKey() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getQueryKey()).append("\", ");
        }
        sb.append("scope:");
        if(this.getScope() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getScope()).append("\", ");
        }
        sb.append("caseSensitive:" + this.isCaseSensitive()).append(", ");
        sb.append("createdTime:" + this.getCreatedTime()).append(", ");
        sb.append("modifiedTime:" + this.getModifiedTime()).append(", ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getGuid() != null) {
            sb.append("\"guid\":").append("\"").append(this.getGuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"guid\":").append("null, ");
        }
        if(this.getUser() != null) {
            sb.append("\"user\":").append("\"").append(this.getUser()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"user\":").append("null, ");
        }
        if(this.getShortLink() != null) {
            sb.append("\"shortLink\":").append("\"").append(this.getShortLink()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"shortLink\":").append("null, ");
        }
        if(this.getDomain() != null) {
            sb.append("\"domain\":").append("\"").append(this.getDomain()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"domain\":").append("null, ");
        }
        if(this.getToken() != null) {
            sb.append("\"token\":").append("\"").append(this.getToken()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"token\":").append("null, ");
        }
        if(this.getLongUrl() != null) {
            sb.append("\"longUrl\":").append("\"").append(this.getLongUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"longUrl\":").append("null, ");
        }
        if(this.getShortUrl() != null) {
            sb.append("\"shortUrl\":").append("\"").append(this.getShortUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"shortUrl\":").append("null, ");
        }
        if(this.getTallyDate() != null) {
            sb.append("\"tallyDate\":").append("\"").append(this.getTallyDate()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"tallyDate\":").append("null, ");
        }
        if(this.getStatus() != null) {
            sb.append("\"status\":").append("\"").append(this.getStatus()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"status\":").append("null, ");
        }
        if(this.getNote() != null) {
            sb.append("\"note\":").append("\"").append(this.getNote()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"note\":").append("null, ");
        }
        if(this.getExpirationTime() != null) {
            sb.append("\"expirationTime\":").append("").append(this.getExpirationTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"expirationTime\":").append("null, ");
        }
        if(this.getKeywordFolder() != null) {
            sb.append("\"keywordFolder\":").append("\"").append(this.getKeywordFolder()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"keywordFolder\":").append("null, ");
        }
        if(this.getFolderPath() != null) {
            sb.append("\"folderPath\":").append("\"").append(this.getFolderPath()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"folderPath\":").append("null, ");
        }
        if(this.getKeyword() != null) {
            sb.append("\"keyword\":").append("\"").append(this.getKeyword()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"keyword\":").append("null, ");
        }
        if(this.getQueryKey() != null) {
            sb.append("\"queryKey\":").append("\"").append(this.getQueryKey()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"queryKey\":").append("null, ");
        }
        if(this.getScope() != null) {
            sb.append("\"scope\":").append("\"").append(this.getScope()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"scope\":").append("null, ");
        }
        if(this.isCaseSensitive() != null) {
            sb.append("\"caseSensitive\":").append("").append(this.isCaseSensitive()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"caseSensitive\":").append("null, ");
        }
        if(this.getCreatedTime() != null) {
            sb.append("\"createdTime\":").append("").append(this.getCreatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"createdTime\":").append("null, ");
        }
        if(this.getModifiedTime() != null) {
            sb.append("\"modifiedTime\":").append("").append(this.getModifiedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"modifiedTime\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer( super.toString() );
        sb.append("keywordFolder = " + this.keywordFolder).append(";");
        sb.append("folderPath = " + this.folderPath).append(";");
        sb.append("keyword = " + this.keyword).append(";");
        sb.append("queryKey = " + this.queryKey).append(";");
        sb.append("scope = " + this.scope).append(";");
        sb.append("caseSensitive = " + this.caseSensitive).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        KeywordCrowdTallyJsBean cloned = new KeywordCrowdTallyJsBean();
        cloned.setGuid(null);          // Do not clone the guid.
        cloned.setUser(this.getUser());   
        cloned.setShortLink(this.getShortLink());   
        cloned.setDomain(this.getDomain());   
        cloned.setToken(this.getToken());   
        cloned.setLongUrl(this.getLongUrl());   
        cloned.setShortUrl(this.getShortUrl());   
        cloned.setTallyDate(this.getTallyDate());   
        cloned.setStatus(this.getStatus());   
        cloned.setNote(this.getNote());   
        cloned.setExpirationTime(this.getExpirationTime());   
        cloned.setKeywordFolder(this.getKeywordFolder());   
        cloned.setFolderPath(this.getFolderPath());   
        cloned.setKeyword(this.getKeyword());   
        cloned.setQueryKey(this.getQueryKey());   
        cloned.setScope(this.getScope());   
        cloned.setCaseSensitive(this.isCaseSensitive());   
        cloned.setCreatedTime(null);   // Reset the timestamp.
        cloned.setModifiedTime(null);   // Reset the timestamp.
        return cloned;
    }

}
