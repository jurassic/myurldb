package com.cannyurl.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.cannyurl.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class KeywordFolderJsBean extends FolderBaseJsBean implements Serializable, Cloneable  //, KeywordFolder
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(KeywordFolderJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String folderPath;

    // Ctors.
    public KeywordFolderJsBean()
    {
        //this((String) null);
    }
    public KeywordFolderJsBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public KeywordFolderJsBean(String guid, String appClient, String clientRootDomain, String user, String title, String description, String type, String category, String parent, String aggregate, String acl, Boolean exportable, String status, String note, String folderPath)
    {
        this(guid, appClient, clientRootDomain, user, title, description, type, category, parent, aggregate, acl, exportable, status, note, folderPath, null, null);
    }
    public KeywordFolderJsBean(String guid, String appClient, String clientRootDomain, String user, String title, String description, String type, String category, String parent, String aggregate, String acl, Boolean exportable, String status, String note, String folderPath, Long createdTime, Long modifiedTime)
    {
        super(guid, appClient, clientRootDomain, user, title, description, type, category, parent, aggregate, acl, exportable, status, note, createdTime, modifiedTime);

        this.folderPath = folderPath;
    }
    public KeywordFolderJsBean(KeywordFolderJsBean bean)
    {
        if(bean != null) {
            setGuid(bean.getGuid());
            setAppClient(bean.getAppClient());
            setClientRootDomain(bean.getClientRootDomain());
            setUser(bean.getUser());
            setTitle(bean.getTitle());
            setDescription(bean.getDescription());
            setType(bean.getType());
            setCategory(bean.getCategory());
            setParent(bean.getParent());
            setAggregate(bean.getAggregate());
            setAcl(bean.getAcl());
            setExportable(bean.isExportable());
            setStatus(bean.getStatus());
            setNote(bean.getNote());
            setFolderPath(bean.getFolderPath());
            setCreatedTime(bean.getCreatedTime());
            setModifiedTime(bean.getModifiedTime());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static KeywordFolderJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        KeywordFolderJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(KeywordFolderJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, KeywordFolderJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    public String getAppClient()
    {
        return super.getAppClient();
    }
    public void setAppClient(String appClient)
    {
        super.setAppClient(appClient);
    }

    public String getClientRootDomain()
    {
        return super.getClientRootDomain();
    }
    public void setClientRootDomain(String clientRootDomain)
    {
        super.setClientRootDomain(clientRootDomain);
    }

    public String getUser()
    {
        return super.getUser();
    }
    public void setUser(String user)
    {
        super.setUser(user);
    }

    public String getTitle()
    {
        return super.getTitle();
    }
    public void setTitle(String title)
    {
        super.setTitle(title);
    }

    public String getDescription()
    {
        return super.getDescription();
    }
    public void setDescription(String description)
    {
        super.setDescription(description);
    }

    public String getType()
    {
        return super.getType();
    }
    public void setType(String type)
    {
        super.setType(type);
    }

    public String getCategory()
    {
        return super.getCategory();
    }
    public void setCategory(String category)
    {
        super.setCategory(category);
    }

    public String getParent()
    {
        return super.getParent();
    }
    public void setParent(String parent)
    {
        super.setParent(parent);
    }

    public String getAggregate()
    {
        return super.getAggregate();
    }
    public void setAggregate(String aggregate)
    {
        super.setAggregate(aggregate);
    }

    public String getAcl()
    {
        return super.getAcl();
    }
    public void setAcl(String acl)
    {
        super.setAcl(acl);
    }

    public Boolean isExportable()
    {
        return super.isExportable();
    }
    public void setExportable(Boolean exportable)
    {
        super.setExportable(exportable);
    }

    public String getStatus()
    {
        return super.getStatus();
    }
    public void setStatus(String status)
    {
        super.setStatus(status);
    }

    public String getNote()
    {
        return super.getNote();
    }
    public void setNote(String note)
    {
        super.setNote(note);
    }

    public String getFolderPath()
    {
        return this.folderPath;
    }
    public void setFolderPath(String folderPath)
    {
        this.folderPath = folderPath;
    }

    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:null, ");
        sb.append("appClient:null, ");
        sb.append("clientRootDomain:null, ");
        sb.append("user:null, ");
        sb.append("title:null, ");
        sb.append("description:null, ");
        sb.append("type:null, ");
        sb.append("category:null, ");
        sb.append("parent:null, ");
        sb.append("aggregate:null, ");
        sb.append("acl:null, ");
        sb.append("exportable:false, ");
        sb.append("status:null, ");
        sb.append("note:null, ");
        sb.append("folderPath:null, ");
        sb.append("createdTime:0, ");
        sb.append("modifiedTime:0, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:");
        if(this.getGuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGuid()).append("\", ");
        }
        sb.append("appClient:");
        if(this.getAppClient() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getAppClient()).append("\", ");
        }
        sb.append("clientRootDomain:");
        if(this.getClientRootDomain() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getClientRootDomain()).append("\", ");
        }
        sb.append("user:");
        if(this.getUser() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUser()).append("\", ");
        }
        sb.append("title:");
        if(this.getTitle() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getTitle()).append("\", ");
        }
        sb.append("description:");
        if(this.getDescription() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDescription()).append("\", ");
        }
        sb.append("type:");
        if(this.getType() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getType()).append("\", ");
        }
        sb.append("category:");
        if(this.getCategory() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getCategory()).append("\", ");
        }
        sb.append("parent:");
        if(this.getParent() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getParent()).append("\", ");
        }
        sb.append("aggregate:");
        if(this.getAggregate() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getAggregate()).append("\", ");
        }
        sb.append("acl:");
        if(this.getAcl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getAcl()).append("\", ");
        }
        sb.append("exportable:" + this.isExportable()).append(", ");
        sb.append("status:");
        if(this.getStatus() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getStatus()).append("\", ");
        }
        sb.append("note:");
        if(this.getNote() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNote()).append("\", ");
        }
        sb.append("folderPath:");
        if(this.getFolderPath() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getFolderPath()).append("\", ");
        }
        sb.append("createdTime:" + this.getCreatedTime()).append(", ");
        sb.append("modifiedTime:" + this.getModifiedTime()).append(", ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getGuid() != null) {
            sb.append("\"guid\":").append("\"").append(this.getGuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"guid\":").append("null, ");
        }
        if(this.getAppClient() != null) {
            sb.append("\"appClient\":").append("\"").append(this.getAppClient()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"appClient\":").append("null, ");
        }
        if(this.getClientRootDomain() != null) {
            sb.append("\"clientRootDomain\":").append("\"").append(this.getClientRootDomain()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"clientRootDomain\":").append("null, ");
        }
        if(this.getUser() != null) {
            sb.append("\"user\":").append("\"").append(this.getUser()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"user\":").append("null, ");
        }
        if(this.getTitle() != null) {
            sb.append("\"title\":").append("\"").append(this.getTitle()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"title\":").append("null, ");
        }
        if(this.getDescription() != null) {
            sb.append("\"description\":").append("\"").append(this.getDescription()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"description\":").append("null, ");
        }
        if(this.getType() != null) {
            sb.append("\"type\":").append("\"").append(this.getType()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"type\":").append("null, ");
        }
        if(this.getCategory() != null) {
            sb.append("\"category\":").append("\"").append(this.getCategory()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"category\":").append("null, ");
        }
        if(this.getParent() != null) {
            sb.append("\"parent\":").append("\"").append(this.getParent()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"parent\":").append("null, ");
        }
        if(this.getAggregate() != null) {
            sb.append("\"aggregate\":").append("\"").append(this.getAggregate()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"aggregate\":").append("null, ");
        }
        if(this.getAcl() != null) {
            sb.append("\"acl\":").append("\"").append(this.getAcl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"acl\":").append("null, ");
        }
        if(this.isExportable() != null) {
            sb.append("\"exportable\":").append("").append(this.isExportable()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"exportable\":").append("null, ");
        }
        if(this.getStatus() != null) {
            sb.append("\"status\":").append("\"").append(this.getStatus()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"status\":").append("null, ");
        }
        if(this.getNote() != null) {
            sb.append("\"note\":").append("\"").append(this.getNote()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"note\":").append("null, ");
        }
        if(this.getFolderPath() != null) {
            sb.append("\"folderPath\":").append("\"").append(this.getFolderPath()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"folderPath\":").append("null, ");
        }
        if(this.getCreatedTime() != null) {
            sb.append("\"createdTime\":").append("").append(this.getCreatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"createdTime\":").append("null, ");
        }
        if(this.getModifiedTime() != null) {
            sb.append("\"modifiedTime\":").append("").append(this.getModifiedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"modifiedTime\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer( super.toString() );
        sb.append("folderPath = " + this.folderPath).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        KeywordFolderJsBean cloned = new KeywordFolderJsBean();
        cloned.setGuid(null);          // Do not clone the guid.
        cloned.setAppClient(this.getAppClient());   
        cloned.setClientRootDomain(this.getClientRootDomain());   
        cloned.setUser(this.getUser());   
        cloned.setTitle(this.getTitle());   
        cloned.setDescription(this.getDescription());   
        cloned.setType(this.getType());   
        cloned.setCategory(this.getCategory());   
        cloned.setParent(this.getParent());   
        cloned.setAggregate(this.getAggregate());   
        cloned.setAcl(this.getAcl());   
        cloned.setExportable(this.isExportable());   
        cloned.setStatus(this.getStatus());   
        cloned.setNote(this.getNote());   
        cloned.setFolderPath(this.getFolderPath());   
        cloned.setCreatedTime(null);   // Reset the timestamp.
        cloned.setModifiedTime(null);   // Reset the timestamp.
        return cloned;
    }

}
