package com.cannyurl.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.cannyurl.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class KeywordLinkJsBean extends NavLinkBaseJsBean implements Serializable, Cloneable  //, KeywordLink
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(KeywordLinkJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String keywordFolder;
    private String folderPath;
    private String keyword;
    private String queryKey;
    private String scope;
    private Boolean dynamic;
    private Boolean caseSensitive;

    // Ctors.
    public KeywordLinkJsBean()
    {
        //this((String) null);
    }
    public KeywordLinkJsBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public KeywordLinkJsBean(String guid, String appClient, String clientRootDomain, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, Boolean internal, Boolean caching, String memo, String status, String note, Long expirationTime, String keywordFolder, String folderPath, String keyword, String queryKey, String scope, Boolean dynamic, Boolean caseSensitive)
    {
        this(guid, appClient, clientRootDomain, user, shortLink, domain, token, longUrl, shortUrl, internal, caching, memo, status, note, expirationTime, keywordFolder, folderPath, keyword, queryKey, scope, dynamic, caseSensitive, null, null);
    }
    public KeywordLinkJsBean(String guid, String appClient, String clientRootDomain, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, Boolean internal, Boolean caching, String memo, String status, String note, Long expirationTime, String keywordFolder, String folderPath, String keyword, String queryKey, String scope, Boolean dynamic, Boolean caseSensitive, Long createdTime, Long modifiedTime)
    {
        super(guid, appClient, clientRootDomain, user, shortLink, domain, token, longUrl, shortUrl, internal, caching, memo, status, note, expirationTime, createdTime, modifiedTime);

        this.keywordFolder = keywordFolder;
        this.folderPath = folderPath;
        this.keyword = keyword;
        this.queryKey = queryKey;
        this.scope = scope;
        this.dynamic = dynamic;
        this.caseSensitive = caseSensitive;
    }
    public KeywordLinkJsBean(KeywordLinkJsBean bean)
    {
        if(bean != null) {
            setGuid(bean.getGuid());
            setAppClient(bean.getAppClient());
            setClientRootDomain(bean.getClientRootDomain());
            setUser(bean.getUser());
            setShortLink(bean.getShortLink());
            setDomain(bean.getDomain());
            setToken(bean.getToken());
            setLongUrl(bean.getLongUrl());
            setShortUrl(bean.getShortUrl());
            setInternal(bean.isInternal());
            setCaching(bean.isCaching());
            setMemo(bean.getMemo());
            setStatus(bean.getStatus());
            setNote(bean.getNote());
            setExpirationTime(bean.getExpirationTime());
            setKeywordFolder(bean.getKeywordFolder());
            setFolderPath(bean.getFolderPath());
            setKeyword(bean.getKeyword());
            setQueryKey(bean.getQueryKey());
            setScope(bean.getScope());
            setDynamic(bean.isDynamic());
            setCaseSensitive(bean.isCaseSensitive());
            setCreatedTime(bean.getCreatedTime());
            setModifiedTime(bean.getModifiedTime());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static KeywordLinkJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        KeywordLinkJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(KeywordLinkJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, KeywordLinkJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    public String getAppClient()
    {
        return super.getAppClient();
    }
    public void setAppClient(String appClient)
    {
        super.setAppClient(appClient);
    }

    public String getClientRootDomain()
    {
        return super.getClientRootDomain();
    }
    public void setClientRootDomain(String clientRootDomain)
    {
        super.setClientRootDomain(clientRootDomain);
    }

    public String getUser()
    {
        return super.getUser();
    }
    public void setUser(String user)
    {
        super.setUser(user);
    }

    public String getShortLink()
    {
        return super.getShortLink();
    }
    public void setShortLink(String shortLink)
    {
        super.setShortLink(shortLink);
    }

    public String getDomain()
    {
        return super.getDomain();
    }
    public void setDomain(String domain)
    {
        super.setDomain(domain);
    }

    public String getToken()
    {
        return super.getToken();
    }
    public void setToken(String token)
    {
        super.setToken(token);
    }

    public String getLongUrl()
    {
        return super.getLongUrl();
    }
    public void setLongUrl(String longUrl)
    {
        super.setLongUrl(longUrl);
    }

    public String getShortUrl()
    {
        return super.getShortUrl();
    }
    public void setShortUrl(String shortUrl)
    {
        super.setShortUrl(shortUrl);
    }

    public Boolean isInternal()
    {
        return super.isInternal();
    }
    public void setInternal(Boolean internal)
    {
        super.setInternal(internal);
    }

    public Boolean isCaching()
    {
        return super.isCaching();
    }
    public void setCaching(Boolean caching)
    {
        super.setCaching(caching);
    }

    public String getMemo()
    {
        return super.getMemo();
    }
    public void setMemo(String memo)
    {
        super.setMemo(memo);
    }

    public String getStatus()
    {
        return super.getStatus();
    }
    public void setStatus(String status)
    {
        super.setStatus(status);
    }

    public String getNote()
    {
        return super.getNote();
    }
    public void setNote(String note)
    {
        super.setNote(note);
    }

    public Long getExpirationTime()
    {
        return super.getExpirationTime();
    }
    public void setExpirationTime(Long expirationTime)
    {
        super.setExpirationTime(expirationTime);
    }

    public String getKeywordFolder()
    {
        return this.keywordFolder;
    }
    public void setKeywordFolder(String keywordFolder)
    {
        this.keywordFolder = keywordFolder;
    }

    public String getFolderPath()
    {
        return this.folderPath;
    }
    public void setFolderPath(String folderPath)
    {
        this.folderPath = folderPath;
    }

    public String getKeyword()
    {
        return this.keyword;
    }
    public void setKeyword(String keyword)
    {
        this.keyword = keyword;
    }

    public String getQueryKey()
    {
        return this.queryKey;
    }
    public void setQueryKey(String queryKey)
    {
        this.queryKey = queryKey;
    }

    public String getScope()
    {
        return this.scope;
    }
    public void setScope(String scope)
    {
        this.scope = scope;
    }

    public Boolean isDynamic()
    {
        return this.dynamic;
    }
    public void setDynamic(Boolean dynamic)
    {
        this.dynamic = dynamic;
    }

    public Boolean isCaseSensitive()
    {
        return this.caseSensitive;
    }
    public void setCaseSensitive(Boolean caseSensitive)
    {
        this.caseSensitive = caseSensitive;
    }

    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:null, ");
        sb.append("appClient:null, ");
        sb.append("clientRootDomain:null, ");
        sb.append("user:null, ");
        sb.append("shortLink:null, ");
        sb.append("domain:null, ");
        sb.append("token:null, ");
        sb.append("longUrl:null, ");
        sb.append("shortUrl:null, ");
        sb.append("internal:false, ");
        sb.append("caching:false, ");
        sb.append("memo:null, ");
        sb.append("status:null, ");
        sb.append("note:null, ");
        sb.append("expirationTime:0, ");
        sb.append("keywordFolder:null, ");
        sb.append("folderPath:null, ");
        sb.append("keyword:null, ");
        sb.append("queryKey:null, ");
        sb.append("scope:null, ");
        sb.append("dynamic:false, ");
        sb.append("caseSensitive:false, ");
        sb.append("createdTime:0, ");
        sb.append("modifiedTime:0, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:");
        if(this.getGuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGuid()).append("\", ");
        }
        sb.append("appClient:");
        if(this.getAppClient() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getAppClient()).append("\", ");
        }
        sb.append("clientRootDomain:");
        if(this.getClientRootDomain() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getClientRootDomain()).append("\", ");
        }
        sb.append("user:");
        if(this.getUser() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUser()).append("\", ");
        }
        sb.append("shortLink:");
        if(this.getShortLink() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getShortLink()).append("\", ");
        }
        sb.append("domain:");
        if(this.getDomain() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDomain()).append("\", ");
        }
        sb.append("token:");
        if(this.getToken() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getToken()).append("\", ");
        }
        sb.append("longUrl:");
        if(this.getLongUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getLongUrl()).append("\", ");
        }
        sb.append("shortUrl:");
        if(this.getShortUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getShortUrl()).append("\", ");
        }
        sb.append("internal:" + this.isInternal()).append(", ");
        sb.append("caching:" + this.isCaching()).append(", ");
        sb.append("memo:");
        if(this.getMemo() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getMemo()).append("\", ");
        }
        sb.append("status:");
        if(this.getStatus() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getStatus()).append("\", ");
        }
        sb.append("note:");
        if(this.getNote() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNote()).append("\", ");
        }
        sb.append("expirationTime:" + this.getExpirationTime()).append(", ");
        sb.append("keywordFolder:");
        if(this.getKeywordFolder() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getKeywordFolder()).append("\", ");
        }
        sb.append("folderPath:");
        if(this.getFolderPath() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getFolderPath()).append("\", ");
        }
        sb.append("keyword:");
        if(this.getKeyword() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getKeyword()).append("\", ");
        }
        sb.append("queryKey:");
        if(this.getQueryKey() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getQueryKey()).append("\", ");
        }
        sb.append("scope:");
        if(this.getScope() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getScope()).append("\", ");
        }
        sb.append("dynamic:" + this.isDynamic()).append(", ");
        sb.append("caseSensitive:" + this.isCaseSensitive()).append(", ");
        sb.append("createdTime:" + this.getCreatedTime()).append(", ");
        sb.append("modifiedTime:" + this.getModifiedTime()).append(", ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getGuid() != null) {
            sb.append("\"guid\":").append("\"").append(this.getGuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"guid\":").append("null, ");
        }
        if(this.getAppClient() != null) {
            sb.append("\"appClient\":").append("\"").append(this.getAppClient()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"appClient\":").append("null, ");
        }
        if(this.getClientRootDomain() != null) {
            sb.append("\"clientRootDomain\":").append("\"").append(this.getClientRootDomain()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"clientRootDomain\":").append("null, ");
        }
        if(this.getUser() != null) {
            sb.append("\"user\":").append("\"").append(this.getUser()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"user\":").append("null, ");
        }
        if(this.getShortLink() != null) {
            sb.append("\"shortLink\":").append("\"").append(this.getShortLink()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"shortLink\":").append("null, ");
        }
        if(this.getDomain() != null) {
            sb.append("\"domain\":").append("\"").append(this.getDomain()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"domain\":").append("null, ");
        }
        if(this.getToken() != null) {
            sb.append("\"token\":").append("\"").append(this.getToken()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"token\":").append("null, ");
        }
        if(this.getLongUrl() != null) {
            sb.append("\"longUrl\":").append("\"").append(this.getLongUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"longUrl\":").append("null, ");
        }
        if(this.getShortUrl() != null) {
            sb.append("\"shortUrl\":").append("\"").append(this.getShortUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"shortUrl\":").append("null, ");
        }
        if(this.isInternal() != null) {
            sb.append("\"internal\":").append("").append(this.isInternal()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"internal\":").append("null, ");
        }
        if(this.isCaching() != null) {
            sb.append("\"caching\":").append("").append(this.isCaching()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"caching\":").append("null, ");
        }
        if(this.getMemo() != null) {
            sb.append("\"memo\":").append("\"").append(this.getMemo()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"memo\":").append("null, ");
        }
        if(this.getStatus() != null) {
            sb.append("\"status\":").append("\"").append(this.getStatus()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"status\":").append("null, ");
        }
        if(this.getNote() != null) {
            sb.append("\"note\":").append("\"").append(this.getNote()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"note\":").append("null, ");
        }
        if(this.getExpirationTime() != null) {
            sb.append("\"expirationTime\":").append("").append(this.getExpirationTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"expirationTime\":").append("null, ");
        }
        if(this.getKeywordFolder() != null) {
            sb.append("\"keywordFolder\":").append("\"").append(this.getKeywordFolder()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"keywordFolder\":").append("null, ");
        }
        if(this.getFolderPath() != null) {
            sb.append("\"folderPath\":").append("\"").append(this.getFolderPath()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"folderPath\":").append("null, ");
        }
        if(this.getKeyword() != null) {
            sb.append("\"keyword\":").append("\"").append(this.getKeyword()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"keyword\":").append("null, ");
        }
        if(this.getQueryKey() != null) {
            sb.append("\"queryKey\":").append("\"").append(this.getQueryKey()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"queryKey\":").append("null, ");
        }
        if(this.getScope() != null) {
            sb.append("\"scope\":").append("\"").append(this.getScope()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"scope\":").append("null, ");
        }
        if(this.isDynamic() != null) {
            sb.append("\"dynamic\":").append("").append(this.isDynamic()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"dynamic\":").append("null, ");
        }
        if(this.isCaseSensitive() != null) {
            sb.append("\"caseSensitive\":").append("").append(this.isCaseSensitive()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"caseSensitive\":").append("null, ");
        }
        if(this.getCreatedTime() != null) {
            sb.append("\"createdTime\":").append("").append(this.getCreatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"createdTime\":").append("null, ");
        }
        if(this.getModifiedTime() != null) {
            sb.append("\"modifiedTime\":").append("").append(this.getModifiedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"modifiedTime\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer( super.toString() );
        sb.append("keywordFolder = " + this.keywordFolder).append(";");
        sb.append("folderPath = " + this.folderPath).append(";");
        sb.append("keyword = " + this.keyword).append(";");
        sb.append("queryKey = " + this.queryKey).append(";");
        sb.append("scope = " + this.scope).append(";");
        sb.append("dynamic = " + this.dynamic).append(";");
        sb.append("caseSensitive = " + this.caseSensitive).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        KeywordLinkJsBean cloned = new KeywordLinkJsBean();
        cloned.setGuid(null);          // Do not clone the guid.
        cloned.setAppClient(this.getAppClient());   
        cloned.setClientRootDomain(this.getClientRootDomain());   
        cloned.setUser(this.getUser());   
        cloned.setShortLink(this.getShortLink());   
        cloned.setDomain(this.getDomain());   
        cloned.setToken(this.getToken());   
        cloned.setLongUrl(this.getLongUrl());   
        cloned.setShortUrl(this.getShortUrl());   
        cloned.setInternal(this.isInternal());   
        cloned.setCaching(this.isCaching());   
        cloned.setMemo(this.getMemo());   
        cloned.setStatus(this.getStatus());   
        cloned.setNote(this.getNote());   
        cloned.setExpirationTime(this.getExpirationTime());   
        cloned.setKeywordFolder(this.getKeywordFolder());   
        cloned.setFolderPath(this.getFolderPath());   
        cloned.setKeyword(this.getKeyword());   
        cloned.setQueryKey(this.getQueryKey());   
        cloned.setScope(this.getScope());   
        cloned.setDynamic(this.isDynamic());   
        cloned.setCaseSensitive(this.isCaseSensitive());   
        cloned.setCreatedTime(null);   // Reset the timestamp.
        cloned.setModifiedTime(null);   // Reset the timestamp.
        return cloned;
    }

}
