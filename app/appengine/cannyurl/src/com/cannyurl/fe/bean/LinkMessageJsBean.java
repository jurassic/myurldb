package com.cannyurl.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.cannyurl.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class LinkMessageJsBean implements Serializable, Cloneable  //, LinkMessage
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(LinkMessageJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String guid;
    private String shortLink;
    private String message;
    private String password;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public LinkMessageJsBean()
    {
        //this((String) null);
    }
    public LinkMessageJsBean(String guid)
    {
        this(guid, null, null, null, null, null);
    }
    public LinkMessageJsBean(String guid, String shortLink, String message, String password)
    {
        this(guid, shortLink, message, password, null, null);
    }
    public LinkMessageJsBean(String guid, String shortLink, String message, String password, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.shortLink = shortLink;
        this.message = message;
        this.password = password;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public LinkMessageJsBean(LinkMessageJsBean bean)
    {
        if(bean != null) {
            setGuid(bean.getGuid());
            setShortLink(bean.getShortLink());
            setMessage(bean.getMessage());
            setPassword(bean.getPassword());
            setCreatedTime(bean.getCreatedTime());
            setModifiedTime(bean.getModifiedTime());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static LinkMessageJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        LinkMessageJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(LinkMessageJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, LinkMessageJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public String getShortLink()
    {
        return this.shortLink;
    }
    public void setShortLink(String shortLink)
    {
        this.shortLink = shortLink;
    }

    public String getMessage()
    {
        return this.message;
    }
    public void setMessage(String message)
    {
        this.message = message;
    }

    public String getPassword()
    {
        return this.password;
    }
    public void setPassword(String password)
    {
        this.password = password;
    }

    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:null, ");
        sb.append("shortLink:null, ");
        sb.append("message:null, ");
        sb.append("password:null, ");
        sb.append("createdTime:0, ");
        sb.append("modifiedTime:0, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:");
        if(this.getGuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGuid()).append("\", ");
        }
        sb.append("shortLink:");
        if(this.getShortLink() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getShortLink()).append("\", ");
        }
        sb.append("message:");
        if(this.getMessage() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getMessage()).append("\", ");
        }
        sb.append("password:");
        if(this.getPassword() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getPassword()).append("\", ");
        }
        sb.append("createdTime:" + this.getCreatedTime()).append(", ");
        sb.append("modifiedTime:" + this.getModifiedTime()).append(", ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getGuid() != null) {
            sb.append("\"guid\":").append("\"").append(this.getGuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"guid\":").append("null, ");
        }
        if(this.getShortLink() != null) {
            sb.append("\"shortLink\":").append("\"").append(this.getShortLink()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"shortLink\":").append("null, ");
        }
        if(this.getMessage() != null) {
            sb.append("\"message\":").append("\"").append(this.getMessage()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"message\":").append("null, ");
        }
        if(this.getPassword() != null) {
            sb.append("\"password\":").append("\"").append(this.getPassword()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"password\":").append("null, ");
        }
        if(this.getCreatedTime() != null) {
            sb.append("\"createdTime\":").append("").append(this.getCreatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"createdTime\":").append("null, ");
        }
        if(this.getModifiedTime() != null) {
            sb.append("\"modifiedTime\":").append("").append(this.getModifiedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"modifiedTime\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("guid = " + this.guid).append(";");
        sb.append("shortLink = " + this.shortLink).append(";");
        sb.append("message = " + this.message).append(";");
        sb.append("password = " + this.password).append(";");
        sb.append("createdTime = " + this.createdTime).append(";");
        sb.append("modifiedTime = " + this.modifiedTime).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        LinkMessageJsBean cloned = new LinkMessageJsBean();
        cloned.setGuid(null);          // Do not clone the guid.
        cloned.setShortLink(this.getShortLink());   
        cloned.setMessage(this.getMessage());   
        cloned.setPassword(this.getPassword());   
        cloned.setCreatedTime(null);   // Reset the timestamp.
        cloned.setModifiedTime(null);   // Reset the timestamp.
        return cloned;
    }

}
