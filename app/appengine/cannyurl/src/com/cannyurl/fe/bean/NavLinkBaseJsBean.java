package com.cannyurl.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.cannyurl.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class NavLinkBaseJsBean implements Serializable  //, NavLinkBase
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(NavLinkBaseJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String guid;
    private String appClient;
    private String clientRootDomain;
    private String user;
    private String shortLink;
    private String domain;
    private String token;
    private String longUrl;
    private String shortUrl;
    private Boolean internal;
    private Boolean caching;
    private String memo;
    private String status;
    private String note;
    private Long expirationTime;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public NavLinkBaseJsBean()
    {
        //this((String) null);
    }
    public NavLinkBaseJsBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public NavLinkBaseJsBean(String guid, String appClient, String clientRootDomain, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, Boolean internal, Boolean caching, String memo, String status, String note, Long expirationTime)
    {
        this(guid, appClient, clientRootDomain, user, shortLink, domain, token, longUrl, shortUrl, internal, caching, memo, status, note, expirationTime, null, null);
    }
    public NavLinkBaseJsBean(String guid, String appClient, String clientRootDomain, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, Boolean internal, Boolean caching, String memo, String status, String note, Long expirationTime, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.appClient = appClient;
        this.clientRootDomain = clientRootDomain;
        this.user = user;
        this.shortLink = shortLink;
        this.domain = domain;
        this.token = token;
        this.longUrl = longUrl;
        this.shortUrl = shortUrl;
        this.internal = internal;
        this.caching = caching;
        this.memo = memo;
        this.status = status;
        this.note = note;
        this.expirationTime = expirationTime;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public NavLinkBaseJsBean(NavLinkBaseJsBean bean)
    {
        if(bean != null) {
            setGuid(bean.getGuid());
            setAppClient(bean.getAppClient());
            setClientRootDomain(bean.getClientRootDomain());
            setUser(bean.getUser());
            setShortLink(bean.getShortLink());
            setDomain(bean.getDomain());
            setToken(bean.getToken());
            setLongUrl(bean.getLongUrl());
            setShortUrl(bean.getShortUrl());
            setInternal(bean.isInternal());
            setCaching(bean.isCaching());
            setMemo(bean.getMemo());
            setStatus(bean.getStatus());
            setNote(bean.getNote());
            setExpirationTime(bean.getExpirationTime());
            setCreatedTime(bean.getCreatedTime());
            setModifiedTime(bean.getModifiedTime());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static NavLinkBaseJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        NavLinkBaseJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(NavLinkBaseJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, NavLinkBaseJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public String getAppClient()
    {
        return this.appClient;
    }
    public void setAppClient(String appClient)
    {
        this.appClient = appClient;
    }

    public String getClientRootDomain()
    {
        return this.clientRootDomain;
    }
    public void setClientRootDomain(String clientRootDomain)
    {
        this.clientRootDomain = clientRootDomain;
    }

    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    public String getShortLink()
    {
        return this.shortLink;
    }
    public void setShortLink(String shortLink)
    {
        this.shortLink = shortLink;
    }

    public String getDomain()
    {
        return this.domain;
    }
    public void setDomain(String domain)
    {
        this.domain = domain;
    }

    public String getToken()
    {
        return this.token;
    }
    public void setToken(String token)
    {
        this.token = token;
    }

    public String getLongUrl()
    {
        return this.longUrl;
    }
    public void setLongUrl(String longUrl)
    {
        this.longUrl = longUrl;
    }

    public String getShortUrl()
    {
        return this.shortUrl;
    }
    public void setShortUrl(String shortUrl)
    {
        this.shortUrl = shortUrl;
    }

    public Boolean isInternal()
    {
        return this.internal;
    }
    public void setInternal(Boolean internal)
    {
        this.internal = internal;
    }

    public Boolean isCaching()
    {
        return this.caching;
    }
    public void setCaching(Boolean caching)
    {
        this.caching = caching;
    }

    public String getMemo()
    {
        return this.memo;
    }
    public void setMemo(String memo)
    {
        this.memo = memo;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    public Long getExpirationTime()
    {
        return this.expirationTime;
    }
    public void setExpirationTime(Long expirationTime)
    {
        this.expirationTime = expirationTime;
    }

    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:null, ");
        sb.append("appClient:null, ");
        sb.append("clientRootDomain:null, ");
        sb.append("user:null, ");
        sb.append("shortLink:null, ");
        sb.append("domain:null, ");
        sb.append("token:null, ");
        sb.append("longUrl:null, ");
        sb.append("shortUrl:null, ");
        sb.append("internal:false, ");
        sb.append("caching:false, ");
        sb.append("memo:null, ");
        sb.append("status:null, ");
        sb.append("note:null, ");
        sb.append("expirationTime:0, ");
        sb.append("createdTime:0, ");
        sb.append("modifiedTime:0, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:");
        if(this.getGuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGuid()).append("\", ");
        }
        sb.append("appClient:");
        if(this.getAppClient() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getAppClient()).append("\", ");
        }
        sb.append("clientRootDomain:");
        if(this.getClientRootDomain() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getClientRootDomain()).append("\", ");
        }
        sb.append("user:");
        if(this.getUser() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUser()).append("\", ");
        }
        sb.append("shortLink:");
        if(this.getShortLink() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getShortLink()).append("\", ");
        }
        sb.append("domain:");
        if(this.getDomain() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDomain()).append("\", ");
        }
        sb.append("token:");
        if(this.getToken() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getToken()).append("\", ");
        }
        sb.append("longUrl:");
        if(this.getLongUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getLongUrl()).append("\", ");
        }
        sb.append("shortUrl:");
        if(this.getShortUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getShortUrl()).append("\", ");
        }
        sb.append("internal:" + this.isInternal()).append(", ");
        sb.append("caching:" + this.isCaching()).append(", ");
        sb.append("memo:");
        if(this.getMemo() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getMemo()).append("\", ");
        }
        sb.append("status:");
        if(this.getStatus() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getStatus()).append("\", ");
        }
        sb.append("note:");
        if(this.getNote() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNote()).append("\", ");
        }
        sb.append("expirationTime:" + this.getExpirationTime()).append(", ");
        sb.append("createdTime:" + this.getCreatedTime()).append(", ");
        sb.append("modifiedTime:" + this.getModifiedTime()).append(", ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getGuid() != null) {
            sb.append("\"guid\":").append("\"").append(this.getGuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"guid\":").append("null, ");
        }
        if(this.getAppClient() != null) {
            sb.append("\"appClient\":").append("\"").append(this.getAppClient()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"appClient\":").append("null, ");
        }
        if(this.getClientRootDomain() != null) {
            sb.append("\"clientRootDomain\":").append("\"").append(this.getClientRootDomain()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"clientRootDomain\":").append("null, ");
        }
        if(this.getUser() != null) {
            sb.append("\"user\":").append("\"").append(this.getUser()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"user\":").append("null, ");
        }
        if(this.getShortLink() != null) {
            sb.append("\"shortLink\":").append("\"").append(this.getShortLink()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"shortLink\":").append("null, ");
        }
        if(this.getDomain() != null) {
            sb.append("\"domain\":").append("\"").append(this.getDomain()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"domain\":").append("null, ");
        }
        if(this.getToken() != null) {
            sb.append("\"token\":").append("\"").append(this.getToken()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"token\":").append("null, ");
        }
        if(this.getLongUrl() != null) {
            sb.append("\"longUrl\":").append("\"").append(this.getLongUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"longUrl\":").append("null, ");
        }
        if(this.getShortUrl() != null) {
            sb.append("\"shortUrl\":").append("\"").append(this.getShortUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"shortUrl\":").append("null, ");
        }
        if(this.isInternal() != null) {
            sb.append("\"internal\":").append("").append(this.isInternal()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"internal\":").append("null, ");
        }
        if(this.isCaching() != null) {
            sb.append("\"caching\":").append("").append(this.isCaching()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"caching\":").append("null, ");
        }
        if(this.getMemo() != null) {
            sb.append("\"memo\":").append("\"").append(this.getMemo()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"memo\":").append("null, ");
        }
        if(this.getStatus() != null) {
            sb.append("\"status\":").append("\"").append(this.getStatus()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"status\":").append("null, ");
        }
        if(this.getNote() != null) {
            sb.append("\"note\":").append("\"").append(this.getNote()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"note\":").append("null, ");
        }
        if(this.getExpirationTime() != null) {
            sb.append("\"expirationTime\":").append("").append(this.getExpirationTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"expirationTime\":").append("null, ");
        }
        if(this.getCreatedTime() != null) {
            sb.append("\"createdTime\":").append("").append(this.getCreatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"createdTime\":").append("null, ");
        }
        if(this.getModifiedTime() != null) {
            sb.append("\"modifiedTime\":").append("").append(this.getModifiedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"modifiedTime\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("guid = " + this.guid).append(";");
        sb.append("appClient = " + this.appClient).append(";");
        sb.append("clientRootDomain = " + this.clientRootDomain).append(";");
        sb.append("user = " + this.user).append(";");
        sb.append("shortLink = " + this.shortLink).append(";");
        sb.append("domain = " + this.domain).append(";");
        sb.append("token = " + this.token).append(";");
        sb.append("longUrl = " + this.longUrl).append(";");
        sb.append("shortUrl = " + this.shortUrl).append(";");
        sb.append("internal = " + this.internal).append(";");
        sb.append("caching = " + this.caching).append(";");
        sb.append("memo = " + this.memo).append(";");
        sb.append("status = " + this.status).append(";");
        sb.append("note = " + this.note).append(";");
        sb.append("expirationTime = " + this.expirationTime).append(";");
        sb.append("createdTime = " + this.createdTime).append(";");
        sb.append("modifiedTime = " + this.modifiedTime).append(";");
        return sb.toString();
    }


}
