package com.cannyurl.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.cannyurl.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class RolePermissionJsBean implements Serializable, Cloneable  //, RolePermission
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(RolePermissionJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String guid;
    private String role;
    private String permissionName;
    private String resource;
    private String instance;
    private String action;
    private String status;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public RolePermissionJsBean()
    {
        //this((String) null);
    }
    public RolePermissionJsBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null);
    }
    public RolePermissionJsBean(String guid, String role, String permissionName, String resource, String instance, String action, String status)
    {
        this(guid, role, permissionName, resource, instance, action, status, null, null);
    }
    public RolePermissionJsBean(String guid, String role, String permissionName, String resource, String instance, String action, String status, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.role = role;
        this.permissionName = permissionName;
        this.resource = resource;
        this.instance = instance;
        this.action = action;
        this.status = status;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public RolePermissionJsBean(RolePermissionJsBean bean)
    {
        if(bean != null) {
            setGuid(bean.getGuid());
            setRole(bean.getRole());
            setPermissionName(bean.getPermissionName());
            setResource(bean.getResource());
            setInstance(bean.getInstance());
            setAction(bean.getAction());
            setStatus(bean.getStatus());
            setCreatedTime(bean.getCreatedTime());
            setModifiedTime(bean.getModifiedTime());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static RolePermissionJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        RolePermissionJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(RolePermissionJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, RolePermissionJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public String getRole()
    {
        return this.role;
    }
    public void setRole(String role)
    {
        this.role = role;
    }

    public String getPermissionName()
    {
        return this.permissionName;
    }
    public void setPermissionName(String permissionName)
    {
        this.permissionName = permissionName;
    }

    public String getResource()
    {
        return this.resource;
    }
    public void setResource(String resource)
    {
        this.resource = resource;
    }

    public String getInstance()
    {
        return this.instance;
    }
    public void setInstance(String instance)
    {
        this.instance = instance;
    }

    public String getAction()
    {
        return this.action;
    }
    public void setAction(String action)
    {
        this.action = action;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:null, ");
        sb.append("role:null, ");
        sb.append("permissionName:null, ");
        sb.append("resource:null, ");
        sb.append("instance:null, ");
        sb.append("action:null, ");
        sb.append("status:null, ");
        sb.append("createdTime:0, ");
        sb.append("modifiedTime:0, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:");
        if(this.getGuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGuid()).append("\", ");
        }
        sb.append("role:");
        if(this.getRole() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getRole()).append("\", ");
        }
        sb.append("permissionName:");
        if(this.getPermissionName() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getPermissionName()).append("\", ");
        }
        sb.append("resource:");
        if(this.getResource() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getResource()).append("\", ");
        }
        sb.append("instance:");
        if(this.getInstance() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getInstance()).append("\", ");
        }
        sb.append("action:");
        if(this.getAction() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getAction()).append("\", ");
        }
        sb.append("status:");
        if(this.getStatus() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getStatus()).append("\", ");
        }
        sb.append("createdTime:" + this.getCreatedTime()).append(", ");
        sb.append("modifiedTime:" + this.getModifiedTime()).append(", ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getGuid() != null) {
            sb.append("\"guid\":").append("\"").append(this.getGuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"guid\":").append("null, ");
        }
        if(this.getRole() != null) {
            sb.append("\"role\":").append("\"").append(this.getRole()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"role\":").append("null, ");
        }
        if(this.getPermissionName() != null) {
            sb.append("\"permissionName\":").append("\"").append(this.getPermissionName()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"permissionName\":").append("null, ");
        }
        if(this.getResource() != null) {
            sb.append("\"resource\":").append("\"").append(this.getResource()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"resource\":").append("null, ");
        }
        if(this.getInstance() != null) {
            sb.append("\"instance\":").append("\"").append(this.getInstance()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"instance\":").append("null, ");
        }
        if(this.getAction() != null) {
            sb.append("\"action\":").append("\"").append(this.getAction()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"action\":").append("null, ");
        }
        if(this.getStatus() != null) {
            sb.append("\"status\":").append("\"").append(this.getStatus()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"status\":").append("null, ");
        }
        if(this.getCreatedTime() != null) {
            sb.append("\"createdTime\":").append("").append(this.getCreatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"createdTime\":").append("null, ");
        }
        if(this.getModifiedTime() != null) {
            sb.append("\"modifiedTime\":").append("").append(this.getModifiedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"modifiedTime\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("guid = " + this.guid).append(";");
        sb.append("role = " + this.role).append(";");
        sb.append("permissionName = " + this.permissionName).append(";");
        sb.append("resource = " + this.resource).append(";");
        sb.append("instance = " + this.instance).append(";");
        sb.append("action = " + this.action).append(";");
        sb.append("status = " + this.status).append(";");
        sb.append("createdTime = " + this.createdTime).append(";");
        sb.append("modifiedTime = " + this.modifiedTime).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        RolePermissionJsBean cloned = new RolePermissionJsBean();
        cloned.setGuid(null);          // Do not clone the guid.
        cloned.setRole(this.getRole());   
        cloned.setPermissionName(this.getPermissionName());   
        cloned.setResource(this.getResource());   
        cloned.setInstance(this.getInstance());   
        cloned.setAction(this.getAction());   
        cloned.setStatus(this.getStatus());   
        cloned.setCreatedTime(null);   // Reset the timestamp.
        cloned.setModifiedTime(null);   // Reset the timestamp.
        return cloned;
    }

}
