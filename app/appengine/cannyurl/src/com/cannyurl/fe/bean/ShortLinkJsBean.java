package com.cannyurl.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.myurldb.ws.ReferrerInfoStruct;
import com.cannyurl.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class ShortLinkJsBean implements Serializable, Cloneable  //, ShortLink
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ShortLinkJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String guid;
    private String appClient;
    private String clientRootDomain;
    private String owner;
    private String longUrlDomain;
    private String longUrl;
    private String longUrlFull;
    private String longUrlHash;
    private Boolean reuseExistingShortUrl;
    private Boolean failIfShortUrlExists;
    private String domain;
    private String domainType;
    private String usercode;
    private String username;
    private String tokenPrefix;
    private String token;
    private String tokenType;
    private String sassyTokenType;
    private String shortUrl;
    private String shortPassage;
    private String redirectType;
    private Long flashDuration;
    private String accessType;
    private String viewType;
    private String shareType;
    private Boolean readOnly;
    private String displayMessage;
    private String shortMessage;
    private Boolean keywordEnabled;
    private Boolean bookmarkEnabled;
    private ReferrerInfoStructJsBean referrerInfo;
    private String status;
    private String note;
    private Long expirationTime;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public ShortLinkJsBean()
    {
        //this((String) null);
    }
    public ShortLinkJsBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public ShortLinkJsBean(String guid, String appClient, String clientRootDomain, String owner, String longUrlDomain, String longUrl, String longUrlFull, String longUrlHash, Boolean reuseExistingShortUrl, Boolean failIfShortUrlExists, String domain, String domainType, String usercode, String username, String tokenPrefix, String token, String tokenType, String sassyTokenType, String shortUrl, String shortPassage, String redirectType, Long flashDuration, String accessType, String viewType, String shareType, Boolean readOnly, String displayMessage, String shortMessage, Boolean keywordEnabled, Boolean bookmarkEnabled, ReferrerInfoStructJsBean referrerInfo, String status, String note, Long expirationTime)
    {
        this(guid, appClient, clientRootDomain, owner, longUrlDomain, longUrl, longUrlFull, longUrlHash, reuseExistingShortUrl, failIfShortUrlExists, domain, domainType, usercode, username, tokenPrefix, token, tokenType, sassyTokenType, shortUrl, shortPassage, redirectType, flashDuration, accessType, viewType, shareType, readOnly, displayMessage, shortMessage, keywordEnabled, bookmarkEnabled, referrerInfo, status, note, expirationTime, null, null);
    }
    public ShortLinkJsBean(String guid, String appClient, String clientRootDomain, String owner, String longUrlDomain, String longUrl, String longUrlFull, String longUrlHash, Boolean reuseExistingShortUrl, Boolean failIfShortUrlExists, String domain, String domainType, String usercode, String username, String tokenPrefix, String token, String tokenType, String sassyTokenType, String shortUrl, String shortPassage, String redirectType, Long flashDuration, String accessType, String viewType, String shareType, Boolean readOnly, String displayMessage, String shortMessage, Boolean keywordEnabled, Boolean bookmarkEnabled, ReferrerInfoStructJsBean referrerInfo, String status, String note, Long expirationTime, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.appClient = appClient;
        this.clientRootDomain = clientRootDomain;
        this.owner = owner;
        this.longUrlDomain = longUrlDomain;
        this.longUrl = longUrl;
        this.longUrlFull = longUrlFull;
        this.longUrlHash = longUrlHash;
        this.reuseExistingShortUrl = reuseExistingShortUrl;
        this.failIfShortUrlExists = failIfShortUrlExists;
        this.domain = domain;
        this.domainType = domainType;
        this.usercode = usercode;
        this.username = username;
        this.tokenPrefix = tokenPrefix;
        this.token = token;
        this.tokenType = tokenType;
        this.sassyTokenType = sassyTokenType;
        this.shortUrl = shortUrl;
        this.shortPassage = shortPassage;
        this.redirectType = redirectType;
        this.flashDuration = flashDuration;
        this.accessType = accessType;
        this.viewType = viewType;
        this.shareType = shareType;
        this.readOnly = readOnly;
        this.displayMessage = displayMessage;
        this.shortMessage = shortMessage;
        this.keywordEnabled = keywordEnabled;
        this.bookmarkEnabled = bookmarkEnabled;
        this.referrerInfo = referrerInfo;
        this.status = status;
        this.note = note;
        this.expirationTime = expirationTime;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public ShortLinkJsBean(ShortLinkJsBean bean)
    {
        if(bean != null) {
            setGuid(bean.getGuid());
            setAppClient(bean.getAppClient());
            setClientRootDomain(bean.getClientRootDomain());
            setOwner(bean.getOwner());
            setLongUrlDomain(bean.getLongUrlDomain());
            setLongUrl(bean.getLongUrl());
            setLongUrlFull(bean.getLongUrlFull());
            setLongUrlHash(bean.getLongUrlHash());
            setReuseExistingShortUrl(bean.isReuseExistingShortUrl());
            setFailIfShortUrlExists(bean.isFailIfShortUrlExists());
            setDomain(bean.getDomain());
            setDomainType(bean.getDomainType());
            setUsercode(bean.getUsercode());
            setUsername(bean.getUsername());
            setTokenPrefix(bean.getTokenPrefix());
            setToken(bean.getToken());
            setTokenType(bean.getTokenType());
            setSassyTokenType(bean.getSassyTokenType());
            setShortUrl(bean.getShortUrl());
            setShortPassage(bean.getShortPassage());
            setRedirectType(bean.getRedirectType());
            setFlashDuration(bean.getFlashDuration());
            setAccessType(bean.getAccessType());
            setViewType(bean.getViewType());
            setShareType(bean.getShareType());
            setReadOnly(bean.isReadOnly());
            setDisplayMessage(bean.getDisplayMessage());
            setShortMessage(bean.getShortMessage());
            setKeywordEnabled(bean.isKeywordEnabled());
            setBookmarkEnabled(bean.isBookmarkEnabled());
            setReferrerInfo(bean.getReferrerInfo());
            setStatus(bean.getStatus());
            setNote(bean.getNote());
            setExpirationTime(bean.getExpirationTime());
            setCreatedTime(bean.getCreatedTime());
            setModifiedTime(bean.getModifiedTime());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static ShortLinkJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        ShortLinkJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(ShortLinkJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, ShortLinkJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public String getAppClient()
    {
        return this.appClient;
    }
    public void setAppClient(String appClient)
    {
        this.appClient = appClient;
    }

    public String getClientRootDomain()
    {
        return this.clientRootDomain;
    }
    public void setClientRootDomain(String clientRootDomain)
    {
        this.clientRootDomain = clientRootDomain;
    }

    public String getOwner()
    {
        return this.owner;
    }
    public void setOwner(String owner)
    {
        this.owner = owner;
    }

    public String getLongUrlDomain()
    {
        return this.longUrlDomain;
    }
    public void setLongUrlDomain(String longUrlDomain)
    {
        this.longUrlDomain = longUrlDomain;
    }

    public String getLongUrl()
    {
        return this.longUrl;
    }
    public void setLongUrl(String longUrl)
    {
        this.longUrl = longUrl;
    }

    public String getLongUrlFull()
    {
        return this.longUrlFull;
    }
    public void setLongUrlFull(String longUrlFull)
    {
        this.longUrlFull = longUrlFull;
    }

    public String getLongUrlHash()
    {
        return this.longUrlHash;
    }
    public void setLongUrlHash(String longUrlHash)
    {
        this.longUrlHash = longUrlHash;
    }

    public Boolean isReuseExistingShortUrl()
    {
        return this.reuseExistingShortUrl;
    }
    public void setReuseExistingShortUrl(Boolean reuseExistingShortUrl)
    {
        this.reuseExistingShortUrl = reuseExistingShortUrl;
    }

    public Boolean isFailIfShortUrlExists()
    {
        return this.failIfShortUrlExists;
    }
    public void setFailIfShortUrlExists(Boolean failIfShortUrlExists)
    {
        this.failIfShortUrlExists = failIfShortUrlExists;
    }

    public String getDomain()
    {
        return this.domain;
    }
    public void setDomain(String domain)
    {
        this.domain = domain;
    }

    public String getDomainType()
    {
        return this.domainType;
    }
    public void setDomainType(String domainType)
    {
        this.domainType = domainType;
    }

    public String getUsercode()
    {
        return this.usercode;
    }
    public void setUsercode(String usercode)
    {
        this.usercode = usercode;
    }

    public String getUsername()
    {
        return this.username;
    }
    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getTokenPrefix()
    {
        return this.tokenPrefix;
    }
    public void setTokenPrefix(String tokenPrefix)
    {
        this.tokenPrefix = tokenPrefix;
    }

    public String getToken()
    {
        return this.token;
    }
    public void setToken(String token)
    {
        this.token = token;
    }

    public String getTokenType()
    {
        return this.tokenType;
    }
    public void setTokenType(String tokenType)
    {
        this.tokenType = tokenType;
    }

    public String getSassyTokenType()
    {
        return this.sassyTokenType;
    }
    public void setSassyTokenType(String sassyTokenType)
    {
        this.sassyTokenType = sassyTokenType;
    }

    public String getShortUrl()
    {
        return this.shortUrl;
    }
    public void setShortUrl(String shortUrl)
    {
        this.shortUrl = shortUrl;
    }

    public String getShortPassage()
    {
        return this.shortPassage;
    }
    public void setShortPassage(String shortPassage)
    {
        this.shortPassage = shortPassage;
    }

    public String getRedirectType()
    {
        return this.redirectType;
    }
    public void setRedirectType(String redirectType)
    {
        this.redirectType = redirectType;
    }

    public Long getFlashDuration()
    {
        return this.flashDuration;
    }
    public void setFlashDuration(Long flashDuration)
    {
        this.flashDuration = flashDuration;
    }

    public String getAccessType()
    {
        return this.accessType;
    }
    public void setAccessType(String accessType)
    {
        this.accessType = accessType;
    }

    public String getViewType()
    {
        return this.viewType;
    }
    public void setViewType(String viewType)
    {
        this.viewType = viewType;
    }

    public String getShareType()
    {
        return this.shareType;
    }
    public void setShareType(String shareType)
    {
        this.shareType = shareType;
    }

    public Boolean isReadOnly()
    {
        return this.readOnly;
    }
    public void setReadOnly(Boolean readOnly)
    {
        this.readOnly = readOnly;
    }

    public String getDisplayMessage()
    {
        return this.displayMessage;
    }
    public void setDisplayMessage(String displayMessage)
    {
        this.displayMessage = displayMessage;
    }

    public String getShortMessage()
    {
        return this.shortMessage;
    }
    public void setShortMessage(String shortMessage)
    {
        this.shortMessage = shortMessage;
    }

    public Boolean isKeywordEnabled()
    {
        return this.keywordEnabled;
    }
    public void setKeywordEnabled(Boolean keywordEnabled)
    {
        this.keywordEnabled = keywordEnabled;
    }

    public Boolean isBookmarkEnabled()
    {
        return this.bookmarkEnabled;
    }
    public void setBookmarkEnabled(Boolean bookmarkEnabled)
    {
        this.bookmarkEnabled = bookmarkEnabled;
    }

    public ReferrerInfoStructJsBean getReferrerInfo()
    {  
        return this.referrerInfo;
    }
    public void setReferrerInfo(ReferrerInfoStructJsBean referrerInfo)
    {
        this.referrerInfo = referrerInfo;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    public Long getExpirationTime()
    {
        return this.expirationTime;
    }
    public void setExpirationTime(Long expirationTime)
    {
        this.expirationTime = expirationTime;
    }

    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:null, ");
        sb.append("appClient:null, ");
        sb.append("clientRootDomain:null, ");
        sb.append("owner:null, ");
        sb.append("longUrlDomain:null, ");
        sb.append("longUrl:null, ");
        sb.append("longUrlFull:null, ");
        sb.append("longUrlHash:null, ");
        sb.append("reuseExistingShortUrl:false, ");
        sb.append("failIfShortUrlExists:false, ");
        sb.append("domain:null, ");
        sb.append("domainType:null, ");
        sb.append("usercode:null, ");
        sb.append("username:null, ");
        sb.append("tokenPrefix:null, ");
        sb.append("token:null, ");
        sb.append("tokenType:null, ");
        sb.append("sassyTokenType:null, ");
        sb.append("shortUrl:null, ");
        sb.append("shortPassage:null, ");
        sb.append("redirectType:null, ");
        sb.append("flashDuration:0, ");
        sb.append("accessType:null, ");
        sb.append("viewType:null, ");
        sb.append("shareType:null, ");
        sb.append("readOnly:false, ");
        sb.append("displayMessage:null, ");
        sb.append("shortMessage:null, ");
        sb.append("keywordEnabled:false, ");
        sb.append("bookmarkEnabled:false, ");
        sb.append("referrerInfo:{}, ");
        sb.append("status:null, ");
        sb.append("note:null, ");
        sb.append("expirationTime:0, ");
        sb.append("createdTime:0, ");
        sb.append("modifiedTime:0, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:");
        if(this.getGuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGuid()).append("\", ");
        }
        sb.append("appClient:");
        if(this.getAppClient() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getAppClient()).append("\", ");
        }
        sb.append("clientRootDomain:");
        if(this.getClientRootDomain() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getClientRootDomain()).append("\", ");
        }
        sb.append("owner:");
        if(this.getOwner() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getOwner()).append("\", ");
        }
        sb.append("longUrlDomain:");
        if(this.getLongUrlDomain() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getLongUrlDomain()).append("\", ");
        }
        sb.append("longUrl:");
        if(this.getLongUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getLongUrl()).append("\", ");
        }
        sb.append("longUrlFull:");
        if(this.getLongUrlFull() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getLongUrlFull()).append("\", ");
        }
        sb.append("longUrlHash:");
        if(this.getLongUrlHash() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getLongUrlHash()).append("\", ");
        }
        sb.append("reuseExistingShortUrl:" + this.isReuseExistingShortUrl()).append(", ");
        sb.append("failIfShortUrlExists:" + this.isFailIfShortUrlExists()).append(", ");
        sb.append("domain:");
        if(this.getDomain() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDomain()).append("\", ");
        }
        sb.append("domainType:");
        if(this.getDomainType() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDomainType()).append("\", ");
        }
        sb.append("usercode:");
        if(this.getUsercode() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUsercode()).append("\", ");
        }
        sb.append("username:");
        if(this.getUsername() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUsername()).append("\", ");
        }
        sb.append("tokenPrefix:");
        if(this.getTokenPrefix() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getTokenPrefix()).append("\", ");
        }
        sb.append("token:");
        if(this.getToken() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getToken()).append("\", ");
        }
        sb.append("tokenType:");
        if(this.getTokenType() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getTokenType()).append("\", ");
        }
        sb.append("sassyTokenType:");
        if(this.getSassyTokenType() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getSassyTokenType()).append("\", ");
        }
        sb.append("shortUrl:");
        if(this.getShortUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getShortUrl()).append("\", ");
        }
        sb.append("shortPassage:");
        if(this.getShortPassage() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getShortPassage()).append("\", ");
        }
        sb.append("redirectType:");
        if(this.getRedirectType() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getRedirectType()).append("\", ");
        }
        sb.append("flashDuration:" + this.getFlashDuration()).append(", ");
        sb.append("accessType:");
        if(this.getAccessType() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getAccessType()).append("\", ");
        }
        sb.append("viewType:");
        if(this.getViewType() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getViewType()).append("\", ");
        }
        sb.append("shareType:");
        if(this.getShareType() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getShareType()).append("\", ");
        }
        sb.append("readOnly:" + this.isReadOnly()).append(", ");
        sb.append("displayMessage:");
        if(this.getDisplayMessage() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDisplayMessage()).append("\", ");
        }
        sb.append("shortMessage:");
        if(this.getShortMessage() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getShortMessage()).append("\", ");
        }
        sb.append("keywordEnabled:" + this.isKeywordEnabled()).append(", ");
        sb.append("bookmarkEnabled:" + this.isBookmarkEnabled()).append(", ");
        sb.append("referrerInfo:");
        if(this.getReferrerInfo() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getReferrerInfo()).append("\", ");
        }
        sb.append("status:");
        if(this.getStatus() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getStatus()).append("\", ");
        }
        sb.append("note:");
        if(this.getNote() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNote()).append("\", ");
        }
        sb.append("expirationTime:" + this.getExpirationTime()).append(", ");
        sb.append("createdTime:" + this.getCreatedTime()).append(", ");
        sb.append("modifiedTime:" + this.getModifiedTime()).append(", ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getGuid() != null) {
            sb.append("\"guid\":").append("\"").append(this.getGuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"guid\":").append("null, ");
        }
        if(this.getAppClient() != null) {
            sb.append("\"appClient\":").append("\"").append(this.getAppClient()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"appClient\":").append("null, ");
        }
        if(this.getClientRootDomain() != null) {
            sb.append("\"clientRootDomain\":").append("\"").append(this.getClientRootDomain()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"clientRootDomain\":").append("null, ");
        }
        if(this.getOwner() != null) {
            sb.append("\"owner\":").append("\"").append(this.getOwner()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"owner\":").append("null, ");
        }
        if(this.getLongUrlDomain() != null) {
            sb.append("\"longUrlDomain\":").append("\"").append(this.getLongUrlDomain()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"longUrlDomain\":").append("null, ");
        }
        if(this.getLongUrl() != null) {
            sb.append("\"longUrl\":").append("\"").append(this.getLongUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"longUrl\":").append("null, ");
        }
        if(this.getLongUrlFull() != null) {
            sb.append("\"longUrlFull\":").append("\"").append(this.getLongUrlFull()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"longUrlFull\":").append("null, ");
        }
        if(this.getLongUrlHash() != null) {
            sb.append("\"longUrlHash\":").append("\"").append(this.getLongUrlHash()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"longUrlHash\":").append("null, ");
        }
        if(this.isReuseExistingShortUrl() != null) {
            sb.append("\"reuseExistingShortUrl\":").append("").append(this.isReuseExistingShortUrl()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"reuseExistingShortUrl\":").append("null, ");
        }
        if(this.isFailIfShortUrlExists() != null) {
            sb.append("\"failIfShortUrlExists\":").append("").append(this.isFailIfShortUrlExists()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"failIfShortUrlExists\":").append("null, ");
        }
        if(this.getDomain() != null) {
            sb.append("\"domain\":").append("\"").append(this.getDomain()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"domain\":").append("null, ");
        }
        if(this.getDomainType() != null) {
            sb.append("\"domainType\":").append("\"").append(this.getDomainType()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"domainType\":").append("null, ");
        }
        if(this.getUsercode() != null) {
            sb.append("\"usercode\":").append("\"").append(this.getUsercode()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"usercode\":").append("null, ");
        }
        if(this.getUsername() != null) {
            sb.append("\"username\":").append("\"").append(this.getUsername()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"username\":").append("null, ");
        }
        if(this.getTokenPrefix() != null) {
            sb.append("\"tokenPrefix\":").append("\"").append(this.getTokenPrefix()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"tokenPrefix\":").append("null, ");
        }
        if(this.getToken() != null) {
            sb.append("\"token\":").append("\"").append(this.getToken()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"token\":").append("null, ");
        }
        if(this.getTokenType() != null) {
            sb.append("\"tokenType\":").append("\"").append(this.getTokenType()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"tokenType\":").append("null, ");
        }
        if(this.getSassyTokenType() != null) {
            sb.append("\"sassyTokenType\":").append("\"").append(this.getSassyTokenType()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"sassyTokenType\":").append("null, ");
        }
        if(this.getShortUrl() != null) {
            sb.append("\"shortUrl\":").append("\"").append(this.getShortUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"shortUrl\":").append("null, ");
        }
        if(this.getShortPassage() != null) {
            sb.append("\"shortPassage\":").append("\"").append(this.getShortPassage()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"shortPassage\":").append("null, ");
        }
        if(this.getRedirectType() != null) {
            sb.append("\"redirectType\":").append("\"").append(this.getRedirectType()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"redirectType\":").append("null, ");
        }
        if(this.getFlashDuration() != null) {
            sb.append("\"flashDuration\":").append("").append(this.getFlashDuration()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"flashDuration\":").append("null, ");
        }
        if(this.getAccessType() != null) {
            sb.append("\"accessType\":").append("\"").append(this.getAccessType()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"accessType\":").append("null, ");
        }
        if(this.getViewType() != null) {
            sb.append("\"viewType\":").append("\"").append(this.getViewType()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"viewType\":").append("null, ");
        }
        if(this.getShareType() != null) {
            sb.append("\"shareType\":").append("\"").append(this.getShareType()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"shareType\":").append("null, ");
        }
        if(this.isReadOnly() != null) {
            sb.append("\"readOnly\":").append("").append(this.isReadOnly()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"readOnly\":").append("null, ");
        }
        if(this.getDisplayMessage() != null) {
            sb.append("\"displayMessage\":").append("\"").append(this.getDisplayMessage()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"displayMessage\":").append("null, ");
        }
        if(this.getShortMessage() != null) {
            sb.append("\"shortMessage\":").append("\"").append(this.getShortMessage()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"shortMessage\":").append("null, ");
        }
        if(this.isKeywordEnabled() != null) {
            sb.append("\"keywordEnabled\":").append("").append(this.isKeywordEnabled()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"keywordEnabled\":").append("null, ");
        }
        if(this.isBookmarkEnabled() != null) {
            sb.append("\"bookmarkEnabled\":").append("").append(this.isBookmarkEnabled()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"bookmarkEnabled\":").append("null, ");
        }
        sb.append("\"referrerInfo\":").append(this.referrerInfo.toJsonString()).append(", ");
        if(this.getStatus() != null) {
            sb.append("\"status\":").append("\"").append(this.getStatus()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"status\":").append("null, ");
        }
        if(this.getNote() != null) {
            sb.append("\"note\":").append("\"").append(this.getNote()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"note\":").append("null, ");
        }
        if(this.getExpirationTime() != null) {
            sb.append("\"expirationTime\":").append("").append(this.getExpirationTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"expirationTime\":").append("null, ");
        }
        if(this.getCreatedTime() != null) {
            sb.append("\"createdTime\":").append("").append(this.getCreatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"createdTime\":").append("null, ");
        }
        if(this.getModifiedTime() != null) {
            sb.append("\"modifiedTime\":").append("").append(this.getModifiedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"modifiedTime\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("guid = " + this.guid).append(";");
        sb.append("appClient = " + this.appClient).append(";");
        sb.append("clientRootDomain = " + this.clientRootDomain).append(";");
        sb.append("owner = " + this.owner).append(";");
        sb.append("longUrlDomain = " + this.longUrlDomain).append(";");
        sb.append("longUrl = " + this.longUrl).append(";");
        sb.append("longUrlFull = " + this.longUrlFull).append(";");
        sb.append("longUrlHash = " + this.longUrlHash).append(";");
        sb.append("reuseExistingShortUrl = " + this.reuseExistingShortUrl).append(";");
        sb.append("failIfShortUrlExists = " + this.failIfShortUrlExists).append(";");
        sb.append("domain = " + this.domain).append(";");
        sb.append("domainType = " + this.domainType).append(";");
        sb.append("usercode = " + this.usercode).append(";");
        sb.append("username = " + this.username).append(";");
        sb.append("tokenPrefix = " + this.tokenPrefix).append(";");
        sb.append("token = " + this.token).append(";");
        sb.append("tokenType = " + this.tokenType).append(";");
        sb.append("sassyTokenType = " + this.sassyTokenType).append(";");
        sb.append("shortUrl = " + this.shortUrl).append(";");
        sb.append("shortPassage = " + this.shortPassage).append(";");
        sb.append("redirectType = " + this.redirectType).append(";");
        sb.append("flashDuration = " + this.flashDuration).append(";");
        sb.append("accessType = " + this.accessType).append(";");
        sb.append("viewType = " + this.viewType).append(";");
        sb.append("shareType = " + this.shareType).append(";");
        sb.append("readOnly = " + this.readOnly).append(";");
        sb.append("displayMessage = " + this.displayMessage).append(";");
        sb.append("shortMessage = " + this.shortMessage).append(";");
        sb.append("keywordEnabled = " + this.keywordEnabled).append(";");
        sb.append("bookmarkEnabled = " + this.bookmarkEnabled).append(";");
        sb.append("referrerInfo = " + this.referrerInfo).append(";");
        sb.append("status = " + this.status).append(";");
        sb.append("note = " + this.note).append(";");
        sb.append("expirationTime = " + this.expirationTime).append(";");
        sb.append("createdTime = " + this.createdTime).append(";");
        sb.append("modifiedTime = " + this.modifiedTime).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        ShortLinkJsBean cloned = new ShortLinkJsBean();
        cloned.setGuid(null);          // Do not clone the guid.
        cloned.setAppClient(this.getAppClient());   
        cloned.setClientRootDomain(this.getClientRootDomain());   
        cloned.setOwner(this.getOwner());   
        cloned.setLongUrlDomain(this.getLongUrlDomain());   
        cloned.setLongUrl(this.getLongUrl());   
        cloned.setLongUrlFull(this.getLongUrlFull());   
        cloned.setLongUrlHash(this.getLongUrlHash());   
        cloned.setReuseExistingShortUrl(this.isReuseExistingShortUrl());   
        cloned.setFailIfShortUrlExists(this.isFailIfShortUrlExists());   
        cloned.setDomain(this.getDomain());   
        cloned.setDomainType(this.getDomainType());   
        cloned.setUsercode(this.getUsercode());   
        cloned.setUsername(this.getUsername());   
        cloned.setTokenPrefix(this.getTokenPrefix());   
        cloned.setToken(this.getToken());   
        cloned.setTokenType(this.getTokenType());   
        cloned.setSassyTokenType(this.getSassyTokenType());   
        cloned.setShortUrl(this.getShortUrl());   
        cloned.setShortPassage(this.getShortPassage());   
        cloned.setRedirectType(this.getRedirectType());   
        cloned.setFlashDuration(this.getFlashDuration());   
        cloned.setAccessType(this.getAccessType());   
        cloned.setViewType(this.getViewType());   
        cloned.setShareType(this.getShareType());   
        cloned.setReadOnly(this.isReadOnly());   
        cloned.setDisplayMessage(this.getDisplayMessage());   
        cloned.setShortMessage(this.getShortMessage());   
        cloned.setKeywordEnabled(this.isKeywordEnabled());   
        cloned.setBookmarkEnabled(this.isBookmarkEnabled());   
        cloned.setReferrerInfo( (ReferrerInfoStructJsBean) this.getReferrerInfo().clone() );
        cloned.setStatus(this.getStatus());   
        cloned.setNote(this.getNote());   
        cloned.setExpirationTime(this.getExpirationTime());   
        cloned.setCreatedTime(null);   // Reset the timestamp.
        cloned.setModifiedTime(null);   // Reset the timestamp.
        return cloned;
    }

}
