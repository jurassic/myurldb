package com.cannyurl.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.cannyurl.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class ShortPassageAttributeJsBean implements Serializable, Cloneable  //, ShortPassageAttribute
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ShortPassageAttributeJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String domain;
    private String tokenType;
    private String displayMessage;
    private String redirectType;
    private Long flashDuration;
    private String accessType;
    private String viewType;
    private String shareType;

    // Ctors.
    public ShortPassageAttributeJsBean()
    {
        //this((String) null);
    }
    public ShortPassageAttributeJsBean(String domain, String tokenType, String displayMessage, String redirectType, Long flashDuration, String accessType, String viewType, String shareType)
    {
        this.domain = domain;
        this.tokenType = tokenType;
        this.displayMessage = displayMessage;
        this.redirectType = redirectType;
        this.flashDuration = flashDuration;
        this.accessType = accessType;
        this.viewType = viewType;
        this.shareType = shareType;
    }
    public ShortPassageAttributeJsBean(ShortPassageAttributeJsBean bean)
    {
        if(bean != null) {
            setDomain(bean.getDomain());
            setTokenType(bean.getTokenType());
            setDisplayMessage(bean.getDisplayMessage());
            setRedirectType(bean.getRedirectType());
            setFlashDuration(bean.getFlashDuration());
            setAccessType(bean.getAccessType());
            setViewType(bean.getViewType());
            setShareType(bean.getShareType());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static ShortPassageAttributeJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        ShortPassageAttributeJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(ShortPassageAttributeJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, ShortPassageAttributeJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getDomain()
    {
        return this.domain;
    }
    public void setDomain(String domain)
    {
        this.domain = domain;
    }

    public String getTokenType()
    {
        return this.tokenType;
    }
    public void setTokenType(String tokenType)
    {
        this.tokenType = tokenType;
    }

    public String getDisplayMessage()
    {
        return this.displayMessage;
    }
    public void setDisplayMessage(String displayMessage)
    {
        this.displayMessage = displayMessage;
    }

    public String getRedirectType()
    {
        return this.redirectType;
    }
    public void setRedirectType(String redirectType)
    {
        this.redirectType = redirectType;
    }

    public Long getFlashDuration()
    {
        return this.flashDuration;
    }
    public void setFlashDuration(Long flashDuration)
    {
        this.flashDuration = flashDuration;
    }

    public String getAccessType()
    {
        return this.accessType;
    }
    public void setAccessType(String accessType)
    {
        this.accessType = accessType;
    }

    public String getViewType()
    {
        return this.viewType;
    }
    public void setViewType(String viewType)
    {
        this.viewType = viewType;
    }

    public String getShareType()
    {
        return this.shareType;
    }
    public void setShareType(String shareType)
    {
        this.shareType = shareType;
    }


    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getDomain() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getTokenType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getDisplayMessage() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getRedirectType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getFlashDuration() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAccessType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getViewType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getShareType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("domain:null, ");
        sb.append("tokenType:null, ");
        sb.append("displayMessage:null, ");
        sb.append("redirectType:null, ");
        sb.append("flashDuration:0, ");
        sb.append("accessType:null, ");
        sb.append("viewType:null, ");
        sb.append("shareType:null, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("domain:");
        if(this.getDomain() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDomain()).append("\", ");
        }
        sb.append("tokenType:");
        if(this.getTokenType() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getTokenType()).append("\", ");
        }
        sb.append("displayMessage:");
        if(this.getDisplayMessage() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDisplayMessage()).append("\", ");
        }
        sb.append("redirectType:");
        if(this.getRedirectType() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getRedirectType()).append("\", ");
        }
        sb.append("flashDuration:" + this.getFlashDuration()).append(", ");
        sb.append("accessType:");
        if(this.getAccessType() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getAccessType()).append("\", ");
        }
        sb.append("viewType:");
        if(this.getViewType() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getViewType()).append("\", ");
        }
        sb.append("shareType:");
        if(this.getShareType() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getShareType()).append("\", ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getDomain() != null) {
            sb.append("\"domain\":").append("\"").append(this.getDomain()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"domain\":").append("null, ");
        }
        if(this.getTokenType() != null) {
            sb.append("\"tokenType\":").append("\"").append(this.getTokenType()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"tokenType\":").append("null, ");
        }
        if(this.getDisplayMessage() != null) {
            sb.append("\"displayMessage\":").append("\"").append(this.getDisplayMessage()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"displayMessage\":").append("null, ");
        }
        if(this.getRedirectType() != null) {
            sb.append("\"redirectType\":").append("\"").append(this.getRedirectType()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"redirectType\":").append("null, ");
        }
        if(this.getFlashDuration() != null) {
            sb.append("\"flashDuration\":").append("").append(this.getFlashDuration()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"flashDuration\":").append("null, ");
        }
        if(this.getAccessType() != null) {
            sb.append("\"accessType\":").append("\"").append(this.getAccessType()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"accessType\":").append("null, ");
        }
        if(this.getViewType() != null) {
            sb.append("\"viewType\":").append("\"").append(this.getViewType()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"viewType\":").append("null, ");
        }
        if(this.getShareType() != null) {
            sb.append("\"shareType\":").append("\"").append(this.getShareType()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"shareType\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("domain = " + this.domain).append(";");
        sb.append("tokenType = " + this.tokenType).append(";");
        sb.append("displayMessage = " + this.displayMessage).append(";");
        sb.append("redirectType = " + this.redirectType).append(";");
        sb.append("flashDuration = " + this.flashDuration).append(";");
        sb.append("accessType = " + this.accessType).append(";");
        sb.append("viewType = " + this.viewType).append(";");
        sb.append("shareType = " + this.shareType).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        ShortPassageAttributeJsBean cloned = new ShortPassageAttributeJsBean();
        cloned.setDomain(this.getDomain());   
        cloned.setTokenType(this.getTokenType());   
        cloned.setDisplayMessage(this.getDisplayMessage());   
        cloned.setRedirectType(this.getRedirectType());   
        cloned.setFlashDuration(this.getFlashDuration());   
        cloned.setAccessType(this.getAccessType());   
        cloned.setViewType(this.getViewType());   
        cloned.setShareType(this.getShareType());   
        return cloned;
    }

}
