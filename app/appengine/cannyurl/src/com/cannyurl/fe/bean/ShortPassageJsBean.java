package com.cannyurl.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.myurldb.ws.ShortPassageAttribute;
import com.cannyurl.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class ShortPassageJsBean implements Serializable, Cloneable  //, ShortPassage
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ShortPassageJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String guid;
    private String owner;
    private String longText;
    private String shortText;
    private ShortPassageAttributeJsBean attribute;
    private Boolean readOnly;
    private String status;
    private String note;
    private Long expirationTime;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public ShortPassageJsBean()
    {
        //this((String) null);
    }
    public ShortPassageJsBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null);
    }
    public ShortPassageJsBean(String guid, String owner, String longText, String shortText, ShortPassageAttributeJsBean attribute, Boolean readOnly, String status, String note, Long expirationTime)
    {
        this(guid, owner, longText, shortText, attribute, readOnly, status, note, expirationTime, null, null);
    }
    public ShortPassageJsBean(String guid, String owner, String longText, String shortText, ShortPassageAttributeJsBean attribute, Boolean readOnly, String status, String note, Long expirationTime, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.owner = owner;
        this.longText = longText;
        this.shortText = shortText;
        this.attribute = attribute;
        this.readOnly = readOnly;
        this.status = status;
        this.note = note;
        this.expirationTime = expirationTime;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public ShortPassageJsBean(ShortPassageJsBean bean)
    {
        if(bean != null) {
            setGuid(bean.getGuid());
            setOwner(bean.getOwner());
            setLongText(bean.getLongText());
            setShortText(bean.getShortText());
            setAttribute(bean.getAttribute());
            setReadOnly(bean.isReadOnly());
            setStatus(bean.getStatus());
            setNote(bean.getNote());
            setExpirationTime(bean.getExpirationTime());
            setCreatedTime(bean.getCreatedTime());
            setModifiedTime(bean.getModifiedTime());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static ShortPassageJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        ShortPassageJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(ShortPassageJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, ShortPassageJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public String getOwner()
    {
        return this.owner;
    }
    public void setOwner(String owner)
    {
        this.owner = owner;
    }

    public String getLongText()
    {
        return this.longText;
    }
    public void setLongText(String longText)
    {
        this.longText = longText;
    }

    public String getShortText()
    {
        return this.shortText;
    }
    public void setShortText(String shortText)
    {
        this.shortText = shortText;
    }

    public ShortPassageAttributeJsBean getAttribute()
    {  
        return this.attribute;
    }
    public void setAttribute(ShortPassageAttributeJsBean attribute)
    {
        this.attribute = attribute;
    }

    public Boolean isReadOnly()
    {
        return this.readOnly;
    }
    public void setReadOnly(Boolean readOnly)
    {
        this.readOnly = readOnly;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    public Long getExpirationTime()
    {
        return this.expirationTime;
    }
    public void setExpirationTime(Long expirationTime)
    {
        this.expirationTime = expirationTime;
    }

    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:null, ");
        sb.append("owner:null, ");
        sb.append("longText:null, ");
        sb.append("shortText:null, ");
        sb.append("attribute:{}, ");
        sb.append("readOnly:false, ");
        sb.append("status:null, ");
        sb.append("note:null, ");
        sb.append("expirationTime:0, ");
        sb.append("createdTime:0, ");
        sb.append("modifiedTime:0, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:");
        if(this.getGuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGuid()).append("\", ");
        }
        sb.append("owner:");
        if(this.getOwner() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getOwner()).append("\", ");
        }
        sb.append("longText:");
        if(this.getLongText() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getLongText()).append("\", ");
        }
        sb.append("shortText:");
        if(this.getShortText() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getShortText()).append("\", ");
        }
        sb.append("attribute:");
        if(this.getAttribute() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getAttribute()).append("\", ");
        }
        sb.append("readOnly:" + this.isReadOnly()).append(", ");
        sb.append("status:");
        if(this.getStatus() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getStatus()).append("\", ");
        }
        sb.append("note:");
        if(this.getNote() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNote()).append("\", ");
        }
        sb.append("expirationTime:" + this.getExpirationTime()).append(", ");
        sb.append("createdTime:" + this.getCreatedTime()).append(", ");
        sb.append("modifiedTime:" + this.getModifiedTime()).append(", ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getGuid() != null) {
            sb.append("\"guid\":").append("\"").append(this.getGuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"guid\":").append("null, ");
        }
        if(this.getOwner() != null) {
            sb.append("\"owner\":").append("\"").append(this.getOwner()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"owner\":").append("null, ");
        }
        if(this.getLongText() != null) {
            sb.append("\"longText\":").append("\"").append(this.getLongText()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"longText\":").append("null, ");
        }
        if(this.getShortText() != null) {
            sb.append("\"shortText\":").append("\"").append(this.getShortText()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"shortText\":").append("null, ");
        }
        sb.append("\"attribute\":").append(this.attribute.toJsonString()).append(", ");
        if(this.isReadOnly() != null) {
            sb.append("\"readOnly\":").append("").append(this.isReadOnly()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"readOnly\":").append("null, ");
        }
        if(this.getStatus() != null) {
            sb.append("\"status\":").append("\"").append(this.getStatus()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"status\":").append("null, ");
        }
        if(this.getNote() != null) {
            sb.append("\"note\":").append("\"").append(this.getNote()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"note\":").append("null, ");
        }
        if(this.getExpirationTime() != null) {
            sb.append("\"expirationTime\":").append("").append(this.getExpirationTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"expirationTime\":").append("null, ");
        }
        if(this.getCreatedTime() != null) {
            sb.append("\"createdTime\":").append("").append(this.getCreatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"createdTime\":").append("null, ");
        }
        if(this.getModifiedTime() != null) {
            sb.append("\"modifiedTime\":").append("").append(this.getModifiedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"modifiedTime\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("guid = " + this.guid).append(";");
        sb.append("owner = " + this.owner).append(";");
        sb.append("longText = " + this.longText).append(";");
        sb.append("shortText = " + this.shortText).append(";");
        sb.append("attribute = " + this.attribute).append(";");
        sb.append("readOnly = " + this.readOnly).append(";");
        sb.append("status = " + this.status).append(";");
        sb.append("note = " + this.note).append(";");
        sb.append("expirationTime = " + this.expirationTime).append(";");
        sb.append("createdTime = " + this.createdTime).append(";");
        sb.append("modifiedTime = " + this.modifiedTime).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        ShortPassageJsBean cloned = new ShortPassageJsBean();
        cloned.setGuid(null);          // Do not clone the guid.
        cloned.setOwner(this.getOwner());   
        cloned.setLongText(this.getLongText());   
        cloned.setShortText(this.getShortText());   
        cloned.setAttribute( (ShortPassageAttributeJsBean) this.getAttribute().clone() );
        cloned.setReadOnly(this.isReadOnly());   
        cloned.setStatus(this.getStatus());   
        cloned.setNote(this.getNote());   
        cloned.setExpirationTime(this.getExpirationTime());   
        cloned.setCreatedTime(null);   // Reset the timestamp.
        cloned.setModifiedTime(null);   // Reset the timestamp.
        return cloned;
    }

}
