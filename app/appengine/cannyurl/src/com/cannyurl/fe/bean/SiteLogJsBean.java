package com.cannyurl.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.cannyurl.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class SiteLogJsBean implements Serializable, Cloneable  //, SiteLog
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(SiteLogJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String uuid;
    private String title;
    private String pubDate;
    private String tag;
    private String content;
    private String format;
    private String note;
    private String status;

    // Ctors.
    public SiteLogJsBean()
    {
        //this((String) null);
    }
    public SiteLogJsBean(String uuid, String title, String pubDate, String tag, String content, String format, String note, String status)
    {
        this.uuid = uuid;
        this.title = title;
        this.pubDate = pubDate;
        this.tag = tag;
        this.content = content;
        this.format = format;
        this.note = note;
        this.status = status;
    }
    public SiteLogJsBean(SiteLogJsBean bean)
    {
        if(bean != null) {
            setUuid(bean.getUuid());
            setTitle(bean.getTitle());
            setPubDate(bean.getPubDate());
            setTag(bean.getTag());
            setContent(bean.getContent());
            setFormat(bean.getFormat());
            setNote(bean.getNote());
            setStatus(bean.getStatus());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static SiteLogJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        SiteLogJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(SiteLogJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, SiteLogJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getUuid()
    {
        return this.uuid;
    }
    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }

    public String getTitle()
    {
        return this.title;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getPubDate()
    {
        return this.pubDate;
    }
    public void setPubDate(String pubDate)
    {
        this.pubDate = pubDate;
    }

    public String getTag()
    {
        return this.tag;
    }
    public void setTag(String tag)
    {
        this.tag = tag;
    }

    public String getContent()
    {
        return this.content;
    }
    public void setContent(String content)
    {
        this.content = content;
    }

    public String getFormat()
    {
        return this.format;
    }
    public void setFormat(String format)
    {
        this.format = format;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }


    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getTitle() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPubDate() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getTag() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getContent() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getFormat() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getStatus() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("uuid:null, ");
        sb.append("title:null, ");
        sb.append("pubDate:null, ");
        sb.append("tag:null, ");
        sb.append("content:null, ");
        sb.append("format:null, ");
        sb.append("note:null, ");
        sb.append("status:null, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("uuid:");
        if(this.getUuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUuid()).append("\", ");
        }
        sb.append("title:");
        if(this.getTitle() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getTitle()).append("\", ");
        }
        sb.append("pubDate:");
        if(this.getPubDate() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getPubDate()).append("\", ");
        }
        sb.append("tag:");
        if(this.getTag() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getTag()).append("\", ");
        }
        sb.append("content:");
        if(this.getContent() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getContent()).append("\", ");
        }
        sb.append("format:");
        if(this.getFormat() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getFormat()).append("\", ");
        }
        sb.append("note:");
        if(this.getNote() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNote()).append("\", ");
        }
        sb.append("status:");
        if(this.getStatus() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getStatus()).append("\", ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getUuid() != null) {
            sb.append("\"uuid\":").append("\"").append(this.getUuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"uuid\":").append("null, ");
        }
        if(this.getTitle() != null) {
            sb.append("\"title\":").append("\"").append(this.getTitle()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"title\":").append("null, ");
        }
        if(this.getPubDate() != null) {
            sb.append("\"pubDate\":").append("\"").append(this.getPubDate()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"pubDate\":").append("null, ");
        }
        if(this.getTag() != null) {
            sb.append("\"tag\":").append("\"").append(this.getTag()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"tag\":").append("null, ");
        }
        if(this.getContent() != null) {
            sb.append("\"content\":").append("\"").append(this.getContent()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"content\":").append("null, ");
        }
        if(this.getFormat() != null) {
            sb.append("\"format\":").append("\"").append(this.getFormat()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"format\":").append("null, ");
        }
        if(this.getNote() != null) {
            sb.append("\"note\":").append("\"").append(this.getNote()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"note\":").append("null, ");
        }
        if(this.getStatus() != null) {
            sb.append("\"status\":").append("\"").append(this.getStatus()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"status\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("uuid = " + this.uuid).append(";");
        sb.append("title = " + this.title).append(";");
        sb.append("pubDate = " + this.pubDate).append(";");
        sb.append("tag = " + this.tag).append(";");
        sb.append("content = " + this.content).append(";");
        sb.append("format = " + this.format).append(";");
        sb.append("note = " + this.note).append(";");
        sb.append("status = " + this.status).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        SiteLogJsBean cloned = new SiteLogJsBean();
        cloned.setUuid(this.getUuid());   
        cloned.setTitle(this.getTitle());   
        cloned.setPubDate(this.getPubDate());   
        cloned.setTag(this.getTag());   
        cloned.setContent(this.getContent());   
        cloned.setFormat(this.getFormat());   
        cloned.setNote(this.getNote());   
        cloned.setStatus(this.getStatus());   
        return cloned;
    }

}
