package com.cannyurl.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.cannyurl.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class StreetAddressStructJsBean implements Serializable, Cloneable  //, StreetAddressStruct
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(StreetAddressStructJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String uuid;
    private String street1;
    private String street2;
    private String city;
    private String county;
    private String postalCode;
    private String state;
    private String province;
    private String country;
    private String countryName;
    private String note;

    // Ctors.
    public StreetAddressStructJsBean()
    {
        //this((String) null);
    }
    public StreetAddressStructJsBean(String uuid, String street1, String street2, String city, String county, String postalCode, String state, String province, String country, String countryName, String note)
    {
        this.uuid = uuid;
        this.street1 = street1;
        this.street2 = street2;
        this.city = city;
        this.county = county;
        this.postalCode = postalCode;
        this.state = state;
        this.province = province;
        this.country = country;
        this.countryName = countryName;
        this.note = note;
    }
    public StreetAddressStructJsBean(StreetAddressStructJsBean bean)
    {
        if(bean != null) {
            setUuid(bean.getUuid());
            setStreet1(bean.getStreet1());
            setStreet2(bean.getStreet2());
            setCity(bean.getCity());
            setCounty(bean.getCounty());
            setPostalCode(bean.getPostalCode());
            setState(bean.getState());
            setProvince(bean.getProvince());
            setCountry(bean.getCountry());
            setCountryName(bean.getCountryName());
            setNote(bean.getNote());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static StreetAddressStructJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        StreetAddressStructJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(StreetAddressStructJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, StreetAddressStructJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getUuid()
    {
        return this.uuid;
    }
    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }

    public String getStreet1()
    {
        return this.street1;
    }
    public void setStreet1(String street1)
    {
        this.street1 = street1;
    }

    public String getStreet2()
    {
        return this.street2;
    }
    public void setStreet2(String street2)
    {
        this.street2 = street2;
    }

    public String getCity()
    {
        return this.city;
    }
    public void setCity(String city)
    {
        this.city = city;
    }

    public String getCounty()
    {
        return this.county;
    }
    public void setCounty(String county)
    {
        this.county = county;
    }

    public String getPostalCode()
    {
        return this.postalCode;
    }
    public void setPostalCode(String postalCode)
    {
        this.postalCode = postalCode;
    }

    public String getState()
    {
        return this.state;
    }
    public void setState(String state)
    {
        this.state = state;
    }

    public String getProvince()
    {
        return this.province;
    }
    public void setProvince(String province)
    {
        this.province = province;
    }

    public String getCountry()
    {
        return this.country;
    }
    public void setCountry(String country)
    {
        this.country = country;
    }

    public String getCountryName()
    {
        return this.countryName;
    }
    public void setCountryName(String countryName)
    {
        this.countryName = countryName;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }


    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getStreet1() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getStreet2() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getCity() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getCounty() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPostalCode() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getState() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getProvince() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getCountry() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getCountryName() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("uuid:null, ");
        sb.append("street1:null, ");
        sb.append("street2:null, ");
        sb.append("city:null, ");
        sb.append("county:null, ");
        sb.append("postalCode:null, ");
        sb.append("state:null, ");
        sb.append("province:null, ");
        sb.append("country:null, ");
        sb.append("countryName:null, ");
        sb.append("note:null, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("uuid:");
        if(this.getUuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUuid()).append("\", ");
        }
        sb.append("street1:");
        if(this.getStreet1() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getStreet1()).append("\", ");
        }
        sb.append("street2:");
        if(this.getStreet2() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getStreet2()).append("\", ");
        }
        sb.append("city:");
        if(this.getCity() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getCity()).append("\", ");
        }
        sb.append("county:");
        if(this.getCounty() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getCounty()).append("\", ");
        }
        sb.append("postalCode:");
        if(this.getPostalCode() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getPostalCode()).append("\", ");
        }
        sb.append("state:");
        if(this.getState() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getState()).append("\", ");
        }
        sb.append("province:");
        if(this.getProvince() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getProvince()).append("\", ");
        }
        sb.append("country:");
        if(this.getCountry() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getCountry()).append("\", ");
        }
        sb.append("countryName:");
        if(this.getCountryName() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getCountryName()).append("\", ");
        }
        sb.append("note:");
        if(this.getNote() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNote()).append("\", ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getUuid() != null) {
            sb.append("\"uuid\":").append("\"").append(this.getUuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"uuid\":").append("null, ");
        }
        if(this.getStreet1() != null) {
            sb.append("\"street1\":").append("\"").append(this.getStreet1()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"street1\":").append("null, ");
        }
        if(this.getStreet2() != null) {
            sb.append("\"street2\":").append("\"").append(this.getStreet2()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"street2\":").append("null, ");
        }
        if(this.getCity() != null) {
            sb.append("\"city\":").append("\"").append(this.getCity()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"city\":").append("null, ");
        }
        if(this.getCounty() != null) {
            sb.append("\"county\":").append("\"").append(this.getCounty()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"county\":").append("null, ");
        }
        if(this.getPostalCode() != null) {
            sb.append("\"postalCode\":").append("\"").append(this.getPostalCode()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"postalCode\":").append("null, ");
        }
        if(this.getState() != null) {
            sb.append("\"state\":").append("\"").append(this.getState()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"state\":").append("null, ");
        }
        if(this.getProvince() != null) {
            sb.append("\"province\":").append("\"").append(this.getProvince()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"province\":").append("null, ");
        }
        if(this.getCountry() != null) {
            sb.append("\"country\":").append("\"").append(this.getCountry()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"country\":").append("null, ");
        }
        if(this.getCountryName() != null) {
            sb.append("\"countryName\":").append("\"").append(this.getCountryName()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"countryName\":").append("null, ");
        }
        if(this.getNote() != null) {
            sb.append("\"note\":").append("\"").append(this.getNote()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"note\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("uuid = " + this.uuid).append(";");
        sb.append("street1 = " + this.street1).append(";");
        sb.append("street2 = " + this.street2).append(";");
        sb.append("city = " + this.city).append(";");
        sb.append("county = " + this.county).append(";");
        sb.append("postalCode = " + this.postalCode).append(";");
        sb.append("state = " + this.state).append(";");
        sb.append("province = " + this.province).append(";");
        sb.append("country = " + this.country).append(";");
        sb.append("countryName = " + this.countryName).append(";");
        sb.append("note = " + this.note).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        StreetAddressStructJsBean cloned = new StreetAddressStructJsBean();
        cloned.setUuid(this.getUuid());   
        cloned.setStreet1(this.getStreet1());   
        cloned.setStreet2(this.getStreet2());   
        cloned.setCity(this.getCity());   
        cloned.setCounty(this.getCounty());   
        cloned.setPostalCode(this.getPostalCode());   
        cloned.setState(this.getState());   
        cloned.setProvince(this.getProvince());   
        cloned.setCountry(this.getCountry());   
        cloned.setCountryName(this.getCountryName());   
        cloned.setNote(this.getNote());   
        return cloned;
    }

}
