package com.cannyurl.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.cannyurl.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class TwitterCardBaseJsBean implements Serializable  //, TwitterCardBase
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(TwitterCardBaseJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String guid;
    private String card;
    private String url;
    private String title;
    private String description;
    private String site;
    private String siteId;
    private String creator;
    private String creatorId;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public TwitterCardBaseJsBean()
    {
        //this((String) null);
    }
    public TwitterCardBaseJsBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null);
    }
    public TwitterCardBaseJsBean(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId)
    {
        this(guid, card, url, title, description, site, siteId, creator, creatorId, null, null);
    }
    public TwitterCardBaseJsBean(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.card = card;
        this.url = url;
        this.title = title;
        this.description = description;
        this.site = site;
        this.siteId = siteId;
        this.creator = creator;
        this.creatorId = creatorId;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public TwitterCardBaseJsBean(TwitterCardBaseJsBean bean)
    {
        if(bean != null) {
            setGuid(bean.getGuid());
            setCard(bean.getCard());
            setUrl(bean.getUrl());
            setTitle(bean.getTitle());
            setDescription(bean.getDescription());
            setSite(bean.getSite());
            setSiteId(bean.getSiteId());
            setCreator(bean.getCreator());
            setCreatorId(bean.getCreatorId());
            setCreatedTime(bean.getCreatedTime());
            setModifiedTime(bean.getModifiedTime());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static TwitterCardBaseJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        TwitterCardBaseJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(TwitterCardBaseJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, TwitterCardBaseJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public String getCard()
    {
        return this.card;
    }
    public void setCard(String card)
    {
        this.card = card;
    }

    public String getUrl()
    {
        return this.url;
    }
    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getTitle()
    {
        return this.title;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getDescription()
    {
        return this.description;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getSite()
    {
        return this.site;
    }
    public void setSite(String site)
    {
        this.site = site;
    }

    public String getSiteId()
    {
        return this.siteId;
    }
    public void setSiteId(String siteId)
    {
        this.siteId = siteId;
    }

    public String getCreator()
    {
        return this.creator;
    }
    public void setCreator(String creator)
    {
        this.creator = creator;
    }

    public String getCreatorId()
    {
        return this.creatorId;
    }
    public void setCreatorId(String creatorId)
    {
        this.creatorId = creatorId;
    }

    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:null, ");
        sb.append("card:null, ");
        sb.append("url:null, ");
        sb.append("title:null, ");
        sb.append("description:null, ");
        sb.append("site:null, ");
        sb.append("siteId:null, ");
        sb.append("creator:null, ");
        sb.append("creatorId:null, ");
        sb.append("createdTime:0, ");
        sb.append("modifiedTime:0, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:");
        if(this.getGuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGuid()).append("\", ");
        }
        sb.append("card:");
        if(this.getCard() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getCard()).append("\", ");
        }
        sb.append("url:");
        if(this.getUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUrl()).append("\", ");
        }
        sb.append("title:");
        if(this.getTitle() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getTitle()).append("\", ");
        }
        sb.append("description:");
        if(this.getDescription() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDescription()).append("\", ");
        }
        sb.append("site:");
        if(this.getSite() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getSite()).append("\", ");
        }
        sb.append("siteId:");
        if(this.getSiteId() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getSiteId()).append("\", ");
        }
        sb.append("creator:");
        if(this.getCreator() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getCreator()).append("\", ");
        }
        sb.append("creatorId:");
        if(this.getCreatorId() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getCreatorId()).append("\", ");
        }
        sb.append("createdTime:" + this.getCreatedTime()).append(", ");
        sb.append("modifiedTime:" + this.getModifiedTime()).append(", ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getGuid() != null) {
            sb.append("\"guid\":").append("\"").append(this.getGuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"guid\":").append("null, ");
        }
        if(this.getCard() != null) {
            sb.append("\"card\":").append("\"").append(this.getCard()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"card\":").append("null, ");
        }
        if(this.getUrl() != null) {
            sb.append("\"url\":").append("\"").append(this.getUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"url\":").append("null, ");
        }
        if(this.getTitle() != null) {
            sb.append("\"title\":").append("\"").append(this.getTitle()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"title\":").append("null, ");
        }
        if(this.getDescription() != null) {
            sb.append("\"description\":").append("\"").append(this.getDescription()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"description\":").append("null, ");
        }
        if(this.getSite() != null) {
            sb.append("\"site\":").append("\"").append(this.getSite()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"site\":").append("null, ");
        }
        if(this.getSiteId() != null) {
            sb.append("\"siteId\":").append("\"").append(this.getSiteId()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"siteId\":").append("null, ");
        }
        if(this.getCreator() != null) {
            sb.append("\"creator\":").append("\"").append(this.getCreator()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"creator\":").append("null, ");
        }
        if(this.getCreatorId() != null) {
            sb.append("\"creatorId\":").append("\"").append(this.getCreatorId()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"creatorId\":").append("null, ");
        }
        if(this.getCreatedTime() != null) {
            sb.append("\"createdTime\":").append("").append(this.getCreatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"createdTime\":").append("null, ");
        }
        if(this.getModifiedTime() != null) {
            sb.append("\"modifiedTime\":").append("").append(this.getModifiedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"modifiedTime\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("guid = " + this.guid).append(";");
        sb.append("card = " + this.card).append(";");
        sb.append("url = " + this.url).append(";");
        sb.append("title = " + this.title).append(";");
        sb.append("description = " + this.description).append(";");
        sb.append("site = " + this.site).append(";");
        sb.append("siteId = " + this.siteId).append(";");
        sb.append("creator = " + this.creator).append(";");
        sb.append("creatorId = " + this.creatorId).append(";");
        sb.append("createdTime = " + this.createdTime).append(";");
        sb.append("modifiedTime = " + this.modifiedTime).append(";");
        return sb.toString();
    }


}
