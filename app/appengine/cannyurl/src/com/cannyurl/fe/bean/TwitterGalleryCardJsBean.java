package com.cannyurl.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.cannyurl.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class TwitterGalleryCardJsBean extends TwitterCardBaseJsBean implements Serializable, Cloneable  //, TwitterGalleryCard
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(TwitterGalleryCardJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String image0;
    private String image1;
    private String image2;
    private String image3;

    // Ctors.
    public TwitterGalleryCardJsBean()
    {
        //this((String) null);
    }
    public TwitterGalleryCardJsBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public TwitterGalleryCardJsBean(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image0, String image1, String image2, String image3)
    {
        this(guid, card, url, title, description, site, siteId, creator, creatorId, image0, image1, image2, image3, null, null);
    }
    public TwitterGalleryCardJsBean(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image0, String image1, String image2, String image3, Long createdTime, Long modifiedTime)
    {
        super(guid, card, url, title, description, site, siteId, creator, creatorId, createdTime, modifiedTime);

        this.image0 = image0;
        this.image1 = image1;
        this.image2 = image2;
        this.image3 = image3;
    }
    public TwitterGalleryCardJsBean(TwitterGalleryCardJsBean bean)
    {
        if(bean != null) {
            setGuid(bean.getGuid());
            setCard(bean.getCard());
            setUrl(bean.getUrl());
            setTitle(bean.getTitle());
            setDescription(bean.getDescription());
            setSite(bean.getSite());
            setSiteId(bean.getSiteId());
            setCreator(bean.getCreator());
            setCreatorId(bean.getCreatorId());
            setImage0(bean.getImage0());
            setImage1(bean.getImage1());
            setImage2(bean.getImage2());
            setImage3(bean.getImage3());
            setCreatedTime(bean.getCreatedTime());
            setModifiedTime(bean.getModifiedTime());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static TwitterGalleryCardJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        TwitterGalleryCardJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(TwitterGalleryCardJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, TwitterGalleryCardJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    public String getCard()
    {
        return super.getCard();
    }
    public void setCard(String card)
    {
        super.setCard(card);
    }

    public String getUrl()
    {
        return super.getUrl();
    }
    public void setUrl(String url)
    {
        super.setUrl(url);
    }

    public String getTitle()
    {
        return super.getTitle();
    }
    public void setTitle(String title)
    {
        super.setTitle(title);
    }

    public String getDescription()
    {
        return super.getDescription();
    }
    public void setDescription(String description)
    {
        super.setDescription(description);
    }

    public String getSite()
    {
        return super.getSite();
    }
    public void setSite(String site)
    {
        super.setSite(site);
    }

    public String getSiteId()
    {
        return super.getSiteId();
    }
    public void setSiteId(String siteId)
    {
        super.setSiteId(siteId);
    }

    public String getCreator()
    {
        return super.getCreator();
    }
    public void setCreator(String creator)
    {
        super.setCreator(creator);
    }

    public String getCreatorId()
    {
        return super.getCreatorId();
    }
    public void setCreatorId(String creatorId)
    {
        super.setCreatorId(creatorId);
    }

    public String getImage0()
    {
        return this.image0;
    }
    public void setImage0(String image0)
    {
        this.image0 = image0;
    }

    public String getImage1()
    {
        return this.image1;
    }
    public void setImage1(String image1)
    {
        this.image1 = image1;
    }

    public String getImage2()
    {
        return this.image2;
    }
    public void setImage2(String image2)
    {
        this.image2 = image2;
    }

    public String getImage3()
    {
        return this.image3;
    }
    public void setImage3(String image3)
    {
        this.image3 = image3;
    }

    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:null, ");
        sb.append("card:null, ");
        sb.append("url:null, ");
        sb.append("title:null, ");
        sb.append("description:null, ");
        sb.append("site:null, ");
        sb.append("siteId:null, ");
        sb.append("creator:null, ");
        sb.append("creatorId:null, ");
        sb.append("image0:null, ");
        sb.append("image1:null, ");
        sb.append("image2:null, ");
        sb.append("image3:null, ");
        sb.append("createdTime:0, ");
        sb.append("modifiedTime:0, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:");
        if(this.getGuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGuid()).append("\", ");
        }
        sb.append("card:");
        if(this.getCard() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getCard()).append("\", ");
        }
        sb.append("url:");
        if(this.getUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUrl()).append("\", ");
        }
        sb.append("title:");
        if(this.getTitle() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getTitle()).append("\", ");
        }
        sb.append("description:");
        if(this.getDescription() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDescription()).append("\", ");
        }
        sb.append("site:");
        if(this.getSite() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getSite()).append("\", ");
        }
        sb.append("siteId:");
        if(this.getSiteId() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getSiteId()).append("\", ");
        }
        sb.append("creator:");
        if(this.getCreator() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getCreator()).append("\", ");
        }
        sb.append("creatorId:");
        if(this.getCreatorId() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getCreatorId()).append("\", ");
        }
        sb.append("image0:");
        if(this.getImage0() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getImage0()).append("\", ");
        }
        sb.append("image1:");
        if(this.getImage1() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getImage1()).append("\", ");
        }
        sb.append("image2:");
        if(this.getImage2() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getImage2()).append("\", ");
        }
        sb.append("image3:");
        if(this.getImage3() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getImage3()).append("\", ");
        }
        sb.append("createdTime:" + this.getCreatedTime()).append(", ");
        sb.append("modifiedTime:" + this.getModifiedTime()).append(", ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getGuid() != null) {
            sb.append("\"guid\":").append("\"").append(this.getGuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"guid\":").append("null, ");
        }
        if(this.getCard() != null) {
            sb.append("\"card\":").append("\"").append(this.getCard()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"card\":").append("null, ");
        }
        if(this.getUrl() != null) {
            sb.append("\"url\":").append("\"").append(this.getUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"url\":").append("null, ");
        }
        if(this.getTitle() != null) {
            sb.append("\"title\":").append("\"").append(this.getTitle()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"title\":").append("null, ");
        }
        if(this.getDescription() != null) {
            sb.append("\"description\":").append("\"").append(this.getDescription()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"description\":").append("null, ");
        }
        if(this.getSite() != null) {
            sb.append("\"site\":").append("\"").append(this.getSite()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"site\":").append("null, ");
        }
        if(this.getSiteId() != null) {
            sb.append("\"siteId\":").append("\"").append(this.getSiteId()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"siteId\":").append("null, ");
        }
        if(this.getCreator() != null) {
            sb.append("\"creator\":").append("\"").append(this.getCreator()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"creator\":").append("null, ");
        }
        if(this.getCreatorId() != null) {
            sb.append("\"creatorId\":").append("\"").append(this.getCreatorId()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"creatorId\":").append("null, ");
        }
        if(this.getImage0() != null) {
            sb.append("\"image0\":").append("\"").append(this.getImage0()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"image0\":").append("null, ");
        }
        if(this.getImage1() != null) {
            sb.append("\"image1\":").append("\"").append(this.getImage1()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"image1\":").append("null, ");
        }
        if(this.getImage2() != null) {
            sb.append("\"image2\":").append("\"").append(this.getImage2()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"image2\":").append("null, ");
        }
        if(this.getImage3() != null) {
            sb.append("\"image3\":").append("\"").append(this.getImage3()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"image3\":").append("null, ");
        }
        if(this.getCreatedTime() != null) {
            sb.append("\"createdTime\":").append("").append(this.getCreatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"createdTime\":").append("null, ");
        }
        if(this.getModifiedTime() != null) {
            sb.append("\"modifiedTime\":").append("").append(this.getModifiedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"modifiedTime\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer( super.toString() );
        sb.append("image0 = " + this.image0).append(";");
        sb.append("image1 = " + this.image1).append(";");
        sb.append("image2 = " + this.image2).append(";");
        sb.append("image3 = " + this.image3).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        TwitterGalleryCardJsBean cloned = new TwitterGalleryCardJsBean();
        cloned.setGuid(null);          // Do not clone the guid.
        cloned.setCard(this.getCard());   
        cloned.setUrl(this.getUrl());   
        cloned.setTitle(this.getTitle());   
        cloned.setDescription(this.getDescription());   
        cloned.setSite(this.getSite());   
        cloned.setSiteId(this.getSiteId());   
        cloned.setCreator(this.getCreator());   
        cloned.setCreatorId(this.getCreatorId());   
        cloned.setImage0(this.getImage0());   
        cloned.setImage1(this.getImage1());   
        cloned.setImage2(this.getImage2());   
        cloned.setImage3(this.getImage3());   
        cloned.setCreatedTime(null);   // Reset the timestamp.
        cloned.setModifiedTime(null);   // Reset the timestamp.
        return cloned;
    }

}
