package com.cannyurl.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.cannyurl.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class UrlRatingJsBean implements Serializable, Cloneable  //, UrlRating
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UrlRatingJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String guid;
    private String domain;
    private String longUrl;
    private String longUrlHash;
    private String preview;
    private String flag;
    private Double rating;
    private String note;
    private Long ratedTime;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public UrlRatingJsBean()
    {
        //this((String) null);
    }
    public UrlRatingJsBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null);
    }
    public UrlRatingJsBean(String guid, String domain, String longUrl, String longUrlHash, String preview, String flag, Double rating, String note, Long ratedTime)
    {
        this(guid, domain, longUrl, longUrlHash, preview, flag, rating, note, ratedTime, null, null);
    }
    public UrlRatingJsBean(String guid, String domain, String longUrl, String longUrlHash, String preview, String flag, Double rating, String note, Long ratedTime, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.domain = domain;
        this.longUrl = longUrl;
        this.longUrlHash = longUrlHash;
        this.preview = preview;
        this.flag = flag;
        this.rating = rating;
        this.note = note;
        this.ratedTime = ratedTime;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public UrlRatingJsBean(UrlRatingJsBean bean)
    {
        if(bean != null) {
            setGuid(bean.getGuid());
            setDomain(bean.getDomain());
            setLongUrl(bean.getLongUrl());
            setLongUrlHash(bean.getLongUrlHash());
            setPreview(bean.getPreview());
            setFlag(bean.getFlag());
            setRating(bean.getRating());
            setNote(bean.getNote());
            setRatedTime(bean.getRatedTime());
            setCreatedTime(bean.getCreatedTime());
            setModifiedTime(bean.getModifiedTime());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static UrlRatingJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        UrlRatingJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(UrlRatingJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, UrlRatingJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public String getDomain()
    {
        return this.domain;
    }
    public void setDomain(String domain)
    {
        this.domain = domain;
    }

    public String getLongUrl()
    {
        return this.longUrl;
    }
    public void setLongUrl(String longUrl)
    {
        this.longUrl = longUrl;
    }

    public String getLongUrlHash()
    {
        return this.longUrlHash;
    }
    public void setLongUrlHash(String longUrlHash)
    {
        this.longUrlHash = longUrlHash;
    }

    public String getPreview()
    {
        return this.preview;
    }
    public void setPreview(String preview)
    {
        this.preview = preview;
    }

    public String getFlag()
    {
        return this.flag;
    }
    public void setFlag(String flag)
    {
        this.flag = flag;
    }

    public Double getRating()
    {
        return this.rating;
    }
    public void setRating(Double rating)
    {
        this.rating = rating;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    public Long getRatedTime()
    {
        return this.ratedTime;
    }
    public void setRatedTime(Long ratedTime)
    {
        this.ratedTime = ratedTime;
    }

    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:null, ");
        sb.append("domain:null, ");
        sb.append("longUrl:null, ");
        sb.append("longUrlHash:null, ");
        sb.append("preview:null, ");
        sb.append("flag:null, ");
        sb.append("rating:0, ");
        sb.append("note:null, ");
        sb.append("ratedTime:0, ");
        sb.append("createdTime:0, ");
        sb.append("modifiedTime:0, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:");
        if(this.getGuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGuid()).append("\", ");
        }
        sb.append("domain:");
        if(this.getDomain() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDomain()).append("\", ");
        }
        sb.append("longUrl:");
        if(this.getLongUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getLongUrl()).append("\", ");
        }
        sb.append("longUrlHash:");
        if(this.getLongUrlHash() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getLongUrlHash()).append("\", ");
        }
        sb.append("preview:");
        if(this.getPreview() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getPreview()).append("\", ");
        }
        sb.append("flag:");
        if(this.getFlag() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getFlag()).append("\", ");
        }
        sb.append("rating:" + this.getRating()).append(", ");
        sb.append("note:");
        if(this.getNote() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNote()).append("\", ");
        }
        sb.append("ratedTime:" + this.getRatedTime()).append(", ");
        sb.append("createdTime:" + this.getCreatedTime()).append(", ");
        sb.append("modifiedTime:" + this.getModifiedTime()).append(", ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getGuid() != null) {
            sb.append("\"guid\":").append("\"").append(this.getGuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"guid\":").append("null, ");
        }
        if(this.getDomain() != null) {
            sb.append("\"domain\":").append("\"").append(this.getDomain()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"domain\":").append("null, ");
        }
        if(this.getLongUrl() != null) {
            sb.append("\"longUrl\":").append("\"").append(this.getLongUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"longUrl\":").append("null, ");
        }
        if(this.getLongUrlHash() != null) {
            sb.append("\"longUrlHash\":").append("\"").append(this.getLongUrlHash()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"longUrlHash\":").append("null, ");
        }
        if(this.getPreview() != null) {
            sb.append("\"preview\":").append("\"").append(this.getPreview()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"preview\":").append("null, ");
        }
        if(this.getFlag() != null) {
            sb.append("\"flag\":").append("\"").append(this.getFlag()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"flag\":").append("null, ");
        }
        if(this.getRating() != null) {
            sb.append("\"rating\":").append("").append(this.getRating()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"rating\":").append("null, ");
        }
        if(this.getNote() != null) {
            sb.append("\"note\":").append("\"").append(this.getNote()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"note\":").append("null, ");
        }
        if(this.getRatedTime() != null) {
            sb.append("\"ratedTime\":").append("").append(this.getRatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"ratedTime\":").append("null, ");
        }
        if(this.getCreatedTime() != null) {
            sb.append("\"createdTime\":").append("").append(this.getCreatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"createdTime\":").append("null, ");
        }
        if(this.getModifiedTime() != null) {
            sb.append("\"modifiedTime\":").append("").append(this.getModifiedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"modifiedTime\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("guid = " + this.guid).append(";");
        sb.append("domain = " + this.domain).append(";");
        sb.append("longUrl = " + this.longUrl).append(";");
        sb.append("longUrlHash = " + this.longUrlHash).append(";");
        sb.append("preview = " + this.preview).append(";");
        sb.append("flag = " + this.flag).append(";");
        sb.append("rating = " + this.rating).append(";");
        sb.append("note = " + this.note).append(";");
        sb.append("ratedTime = " + this.ratedTime).append(";");
        sb.append("createdTime = " + this.createdTime).append(";");
        sb.append("modifiedTime = " + this.modifiedTime).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        UrlRatingJsBean cloned = new UrlRatingJsBean();
        cloned.setGuid(null);          // Do not clone the guid.
        cloned.setDomain(this.getDomain());   
        cloned.setLongUrl(this.getLongUrl());   
        cloned.setLongUrlHash(this.getLongUrlHash());   
        cloned.setPreview(this.getPreview());   
        cloned.setFlag(this.getFlag());   
        cloned.setRating(this.getRating());   
        cloned.setNote(this.getNote());   
        cloned.setRatedTime(this.getRatedTime());   
        cloned.setCreatedTime(null);   // Reset the timestamp.
        cloned.setModifiedTime(null);   // Reset the timestamp.
        return cloned;
    }

}
