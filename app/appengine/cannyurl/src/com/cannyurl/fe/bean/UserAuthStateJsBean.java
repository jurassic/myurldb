package com.cannyurl.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.myurldb.ws.ExternalUserIdStruct;
import com.cannyurl.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class UserAuthStateJsBean implements Serializable, Cloneable  //, UserAuthState
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UserAuthStateJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String guid;
    private String providerId;
    private String user;
    private String username;
    private String email;
    private String openId;
    private String deviceId;
    private String sessionId;
    private String authToken;
    private String authStatus;
    private String externalAuth;
    private ExternalUserIdStructJsBean externalId;
    private String status;
    private Long firstAuthTime;
    private Long lastAuthTime;
    private Long expirationTime;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public UserAuthStateJsBean()
    {
        //this((String) null);
    }
    public UserAuthStateJsBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public UserAuthStateJsBean(String guid, String providerId, String user, String username, String email, String openId, String deviceId, String sessionId, String authToken, String authStatus, String externalAuth, ExternalUserIdStructJsBean externalId, String status, Long firstAuthTime, Long lastAuthTime, Long expirationTime)
    {
        this(guid, providerId, user, username, email, openId, deviceId, sessionId, authToken, authStatus, externalAuth, externalId, status, firstAuthTime, lastAuthTime, expirationTime, null, null);
    }
    public UserAuthStateJsBean(String guid, String providerId, String user, String username, String email, String openId, String deviceId, String sessionId, String authToken, String authStatus, String externalAuth, ExternalUserIdStructJsBean externalId, String status, Long firstAuthTime, Long lastAuthTime, Long expirationTime, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.providerId = providerId;
        this.user = user;
        this.username = username;
        this.email = email;
        this.openId = openId;
        this.deviceId = deviceId;
        this.sessionId = sessionId;
        this.authToken = authToken;
        this.authStatus = authStatus;
        this.externalAuth = externalAuth;
        this.externalId = externalId;
        this.status = status;
        this.firstAuthTime = firstAuthTime;
        this.lastAuthTime = lastAuthTime;
        this.expirationTime = expirationTime;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public UserAuthStateJsBean(UserAuthStateJsBean bean)
    {
        if(bean != null) {
            setGuid(bean.getGuid());
            setProviderId(bean.getProviderId());
            setUser(bean.getUser());
            setUsername(bean.getUsername());
            setEmail(bean.getEmail());
            setOpenId(bean.getOpenId());
            setDeviceId(bean.getDeviceId());
            setSessionId(bean.getSessionId());
            setAuthToken(bean.getAuthToken());
            setAuthStatus(bean.getAuthStatus());
            setExternalAuth(bean.getExternalAuth());
            setExternalId(bean.getExternalId());
            setStatus(bean.getStatus());
            setFirstAuthTime(bean.getFirstAuthTime());
            setLastAuthTime(bean.getLastAuthTime());
            setExpirationTime(bean.getExpirationTime());
            setCreatedTime(bean.getCreatedTime());
            setModifiedTime(bean.getModifiedTime());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static UserAuthStateJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        UserAuthStateJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(UserAuthStateJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, UserAuthStateJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public String getProviderId()
    {
        return this.providerId;
    }
    public void setProviderId(String providerId)
    {
        this.providerId = providerId;
    }

    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    public String getUsername()
    {
        return this.username;
    }
    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getEmail()
    {
        return this.email;
    }
    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getOpenId()
    {
        return this.openId;
    }
    public void setOpenId(String openId)
    {
        this.openId = openId;
    }

    public String getDeviceId()
    {
        return this.deviceId;
    }
    public void setDeviceId(String deviceId)
    {
        this.deviceId = deviceId;
    }

    public String getSessionId()
    {
        return this.sessionId;
    }
    public void setSessionId(String sessionId)
    {
        this.sessionId = sessionId;
    }

    public String getAuthToken()
    {
        return this.authToken;
    }
    public void setAuthToken(String authToken)
    {
        this.authToken = authToken;
    }

    public String getAuthStatus()
    {
        return this.authStatus;
    }
    public void setAuthStatus(String authStatus)
    {
        this.authStatus = authStatus;
    }

    public String getExternalAuth()
    {
        return this.externalAuth;
    }
    public void setExternalAuth(String externalAuth)
    {
        this.externalAuth = externalAuth;
    }

    public ExternalUserIdStructJsBean getExternalId()
    {  
        return this.externalId;
    }
    public void setExternalId(ExternalUserIdStructJsBean externalId)
    {
        this.externalId = externalId;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public Long getFirstAuthTime()
    {
        return this.firstAuthTime;
    }
    public void setFirstAuthTime(Long firstAuthTime)
    {
        this.firstAuthTime = firstAuthTime;
    }

    public Long getLastAuthTime()
    {
        return this.lastAuthTime;
    }
    public void setLastAuthTime(Long lastAuthTime)
    {
        this.lastAuthTime = lastAuthTime;
    }

    public Long getExpirationTime()
    {
        return this.expirationTime;
    }
    public void setExpirationTime(Long expirationTime)
    {
        this.expirationTime = expirationTime;
    }

    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:null, ");
        sb.append("providerId:null, ");
        sb.append("user:null, ");
        sb.append("username:null, ");
        sb.append("email:null, ");
        sb.append("openId:null, ");
        sb.append("deviceId:null, ");
        sb.append("sessionId:null, ");
        sb.append("authToken:null, ");
        sb.append("authStatus:null, ");
        sb.append("externalAuth:null, ");
        sb.append("externalId:{}, ");
        sb.append("status:null, ");
        sb.append("firstAuthTime:0, ");
        sb.append("lastAuthTime:0, ");
        sb.append("expirationTime:0, ");
        sb.append("createdTime:0, ");
        sb.append("modifiedTime:0, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:");
        if(this.getGuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGuid()).append("\", ");
        }
        sb.append("providerId:");
        if(this.getProviderId() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getProviderId()).append("\", ");
        }
        sb.append("user:");
        if(this.getUser() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUser()).append("\", ");
        }
        sb.append("username:");
        if(this.getUsername() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUsername()).append("\", ");
        }
        sb.append("email:");
        if(this.getEmail() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getEmail()).append("\", ");
        }
        sb.append("openId:");
        if(this.getOpenId() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getOpenId()).append("\", ");
        }
        sb.append("deviceId:");
        if(this.getDeviceId() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDeviceId()).append("\", ");
        }
        sb.append("sessionId:");
        if(this.getSessionId() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getSessionId()).append("\", ");
        }
        sb.append("authToken:");
        if(this.getAuthToken() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getAuthToken()).append("\", ");
        }
        sb.append("authStatus:");
        if(this.getAuthStatus() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getAuthStatus()).append("\", ");
        }
        sb.append("externalAuth:");
        if(this.getExternalAuth() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getExternalAuth()).append("\", ");
        }
        sb.append("externalId:");
        if(this.getExternalId() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getExternalId()).append("\", ");
        }
        sb.append("status:");
        if(this.getStatus() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getStatus()).append("\", ");
        }
        sb.append("firstAuthTime:" + this.getFirstAuthTime()).append(", ");
        sb.append("lastAuthTime:" + this.getLastAuthTime()).append(", ");
        sb.append("expirationTime:" + this.getExpirationTime()).append(", ");
        sb.append("createdTime:" + this.getCreatedTime()).append(", ");
        sb.append("modifiedTime:" + this.getModifiedTime()).append(", ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getGuid() != null) {
            sb.append("\"guid\":").append("\"").append(this.getGuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"guid\":").append("null, ");
        }
        if(this.getProviderId() != null) {
            sb.append("\"providerId\":").append("\"").append(this.getProviderId()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"providerId\":").append("null, ");
        }
        if(this.getUser() != null) {
            sb.append("\"user\":").append("\"").append(this.getUser()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"user\":").append("null, ");
        }
        if(this.getUsername() != null) {
            sb.append("\"username\":").append("\"").append(this.getUsername()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"username\":").append("null, ");
        }
        if(this.getEmail() != null) {
            sb.append("\"email\":").append("\"").append(this.getEmail()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"email\":").append("null, ");
        }
        if(this.getOpenId() != null) {
            sb.append("\"openId\":").append("\"").append(this.getOpenId()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"openId\":").append("null, ");
        }
        if(this.getDeviceId() != null) {
            sb.append("\"deviceId\":").append("\"").append(this.getDeviceId()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"deviceId\":").append("null, ");
        }
        if(this.getSessionId() != null) {
            sb.append("\"sessionId\":").append("\"").append(this.getSessionId()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"sessionId\":").append("null, ");
        }
        if(this.getAuthToken() != null) {
            sb.append("\"authToken\":").append("\"").append(this.getAuthToken()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"authToken\":").append("null, ");
        }
        if(this.getAuthStatus() != null) {
            sb.append("\"authStatus\":").append("\"").append(this.getAuthStatus()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"authStatus\":").append("null, ");
        }
        if(this.getExternalAuth() != null) {
            sb.append("\"externalAuth\":").append("\"").append(this.getExternalAuth()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"externalAuth\":").append("null, ");
        }
        sb.append("\"externalId\":").append(this.externalId.toJsonString()).append(", ");
        if(this.getStatus() != null) {
            sb.append("\"status\":").append("\"").append(this.getStatus()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"status\":").append("null, ");
        }
        if(this.getFirstAuthTime() != null) {
            sb.append("\"firstAuthTime\":").append("").append(this.getFirstAuthTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"firstAuthTime\":").append("null, ");
        }
        if(this.getLastAuthTime() != null) {
            sb.append("\"lastAuthTime\":").append("").append(this.getLastAuthTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"lastAuthTime\":").append("null, ");
        }
        if(this.getExpirationTime() != null) {
            sb.append("\"expirationTime\":").append("").append(this.getExpirationTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"expirationTime\":").append("null, ");
        }
        if(this.getCreatedTime() != null) {
            sb.append("\"createdTime\":").append("").append(this.getCreatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"createdTime\":").append("null, ");
        }
        if(this.getModifiedTime() != null) {
            sb.append("\"modifiedTime\":").append("").append(this.getModifiedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"modifiedTime\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("guid = " + this.guid).append(";");
        sb.append("providerId = " + this.providerId).append(";");
        sb.append("user = " + this.user).append(";");
        sb.append("username = " + this.username).append(";");
        sb.append("email = " + this.email).append(";");
        sb.append("openId = " + this.openId).append(";");
        sb.append("deviceId = " + this.deviceId).append(";");
        sb.append("sessionId = " + this.sessionId).append(";");
        sb.append("authToken = " + this.authToken).append(";");
        sb.append("authStatus = " + this.authStatus).append(";");
        sb.append("externalAuth = " + this.externalAuth).append(";");
        sb.append("externalId = " + this.externalId).append(";");
        sb.append("status = " + this.status).append(";");
        sb.append("firstAuthTime = " + this.firstAuthTime).append(";");
        sb.append("lastAuthTime = " + this.lastAuthTime).append(";");
        sb.append("expirationTime = " + this.expirationTime).append(";");
        sb.append("createdTime = " + this.createdTime).append(";");
        sb.append("modifiedTime = " + this.modifiedTime).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        UserAuthStateJsBean cloned = new UserAuthStateJsBean();
        cloned.setGuid(null);          // Do not clone the guid.
        cloned.setProviderId(this.getProviderId());   
        cloned.setUser(this.getUser());   
        cloned.setUsername(this.getUsername());   
        cloned.setEmail(this.getEmail());   
        cloned.setOpenId(this.getOpenId());   
        cloned.setDeviceId(this.getDeviceId());   
        cloned.setSessionId(this.getSessionId());   
        cloned.setAuthToken(this.getAuthToken());   
        cloned.setAuthStatus(this.getAuthStatus());   
        cloned.setExternalAuth(this.getExternalAuth());   
        cloned.setExternalId( (ExternalUserIdStructJsBean) this.getExternalId().clone() );
        cloned.setStatus(this.getStatus());   
        cloned.setFirstAuthTime(this.getFirstAuthTime());   
        cloned.setLastAuthTime(this.getLastAuthTime());   
        cloned.setExpirationTime(this.getExpirationTime());   
        cloned.setCreatedTime(null);   // Reset the timestamp.
        cloned.setModifiedTime(null);   // Reset the timestamp.
        return cloned;
    }

}
