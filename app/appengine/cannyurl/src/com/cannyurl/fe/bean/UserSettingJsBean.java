package com.cannyurl.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.cannyurl.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class UserSettingJsBean extends PersonalSettingJsBean implements Serializable, Cloneable  //, UserSetting
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UserSettingJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String user;
    private String homePage;
    private String selfBio;
    private String userUrlDomain;
    private String userUrlDomainNormalized;
    private Boolean autoRedirectEnabled;
    private Boolean viewEnabled;
    private Boolean shareEnabled;
    private String defaultDomain;
    private String defaultTokenType;
    private String defaultRedirectType;
    private Long defaultFlashDuration;
    private String defaultAccessType;
    private String defaultViewType;
    private String defaultShareType;
    private String defaultPassphrase;
    private String defaultSignature;
    private Boolean useSignature;
    private String defaultEmblem;
    private Boolean useEmblem;
    private String defaultBackground;
    private Boolean useBackground;
    private String sponsorUrl;
    private String sponsorBanner;
    private String sponsorNote;
    private Boolean useSponsor;
    private String extraParams;
    private Long expirationDuration;

    // Ctors.
    public UserSettingJsBean()
    {
        //this((String) null);
    }
    public UserSettingJsBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public UserSettingJsBean(String guid, String user, String homePage, String selfBio, String userUrlDomain, String userUrlDomainNormalized, Boolean autoRedirectEnabled, Boolean viewEnabled, Boolean shareEnabled, String defaultDomain, String defaultTokenType, String defaultRedirectType, Long defaultFlashDuration, String defaultAccessType, String defaultViewType, String defaultShareType, String defaultPassphrase, String defaultSignature, Boolean useSignature, String defaultEmblem, Boolean useEmblem, String defaultBackground, Boolean useBackground, String sponsorUrl, String sponsorBanner, String sponsorNote, Boolean useSponsor, String extraParams, Long expirationDuration)
    {
        this(guid, user, homePage, selfBio, userUrlDomain, userUrlDomainNormalized, autoRedirectEnabled, viewEnabled, shareEnabled, defaultDomain, defaultTokenType, defaultRedirectType, defaultFlashDuration, defaultAccessType, defaultViewType, defaultShareType, defaultPassphrase, defaultSignature, useSignature, defaultEmblem, useEmblem, defaultBackground, useBackground, sponsorUrl, sponsorBanner, sponsorNote, useSponsor, extraParams, expirationDuration, null, null);
    }
    public UserSettingJsBean(String guid, String user, String homePage, String selfBio, String userUrlDomain, String userUrlDomainNormalized, Boolean autoRedirectEnabled, Boolean viewEnabled, Boolean shareEnabled, String defaultDomain, String defaultTokenType, String defaultRedirectType, Long defaultFlashDuration, String defaultAccessType, String defaultViewType, String defaultShareType, String defaultPassphrase, String defaultSignature, Boolean useSignature, String defaultEmblem, Boolean useEmblem, String defaultBackground, Boolean useBackground, String sponsorUrl, String sponsorBanner, String sponsorNote, Boolean useSponsor, String extraParams, Long expirationDuration, Long createdTime, Long modifiedTime)
    {
        super(guid, createdTime, modifiedTime);

        this.user = user;
        this.homePage = homePage;
        this.selfBio = selfBio;
        this.userUrlDomain = userUrlDomain;
        this.userUrlDomainNormalized = userUrlDomainNormalized;
        this.autoRedirectEnabled = autoRedirectEnabled;
        this.viewEnabled = viewEnabled;
        this.shareEnabled = shareEnabled;
        this.defaultDomain = defaultDomain;
        this.defaultTokenType = defaultTokenType;
        this.defaultRedirectType = defaultRedirectType;
        this.defaultFlashDuration = defaultFlashDuration;
        this.defaultAccessType = defaultAccessType;
        this.defaultViewType = defaultViewType;
        this.defaultShareType = defaultShareType;
        this.defaultPassphrase = defaultPassphrase;
        this.defaultSignature = defaultSignature;
        this.useSignature = useSignature;
        this.defaultEmblem = defaultEmblem;
        this.useEmblem = useEmblem;
        this.defaultBackground = defaultBackground;
        this.useBackground = useBackground;
        this.sponsorUrl = sponsorUrl;
        this.sponsorBanner = sponsorBanner;
        this.sponsorNote = sponsorNote;
        this.useSponsor = useSponsor;
        this.extraParams = extraParams;
        this.expirationDuration = expirationDuration;
    }
    public UserSettingJsBean(UserSettingJsBean bean)
    {
        if(bean != null) {
            setGuid(bean.getGuid());
            setUser(bean.getUser());
            setHomePage(bean.getHomePage());
            setSelfBio(bean.getSelfBio());
            setUserUrlDomain(bean.getUserUrlDomain());
            setUserUrlDomainNormalized(bean.getUserUrlDomainNormalized());
            setAutoRedirectEnabled(bean.isAutoRedirectEnabled());
            setViewEnabled(bean.isViewEnabled());
            setShareEnabled(bean.isShareEnabled());
            setDefaultDomain(bean.getDefaultDomain());
            setDefaultTokenType(bean.getDefaultTokenType());
            setDefaultRedirectType(bean.getDefaultRedirectType());
            setDefaultFlashDuration(bean.getDefaultFlashDuration());
            setDefaultAccessType(bean.getDefaultAccessType());
            setDefaultViewType(bean.getDefaultViewType());
            setDefaultShareType(bean.getDefaultShareType());
            setDefaultPassphrase(bean.getDefaultPassphrase());
            setDefaultSignature(bean.getDefaultSignature());
            setUseSignature(bean.isUseSignature());
            setDefaultEmblem(bean.getDefaultEmblem());
            setUseEmblem(bean.isUseEmblem());
            setDefaultBackground(bean.getDefaultBackground());
            setUseBackground(bean.isUseBackground());
            setSponsorUrl(bean.getSponsorUrl());
            setSponsorBanner(bean.getSponsorBanner());
            setSponsorNote(bean.getSponsorNote());
            setUseSponsor(bean.isUseSponsor());
            setExtraParams(bean.getExtraParams());
            setExpirationDuration(bean.getExpirationDuration());
            setCreatedTime(bean.getCreatedTime());
            setModifiedTime(bean.getModifiedTime());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static UserSettingJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        UserSettingJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(UserSettingJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, UserSettingJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    public String getHomePage()
    {
        return this.homePage;
    }
    public void setHomePage(String homePage)
    {
        this.homePage = homePage;
    }

    public String getSelfBio()
    {
        return this.selfBio;
    }
    public void setSelfBio(String selfBio)
    {
        this.selfBio = selfBio;
    }

    public String getUserUrlDomain()
    {
        return this.userUrlDomain;
    }
    public void setUserUrlDomain(String userUrlDomain)
    {
        this.userUrlDomain = userUrlDomain;
    }

    public String getUserUrlDomainNormalized()
    {
        return this.userUrlDomainNormalized;
    }
    public void setUserUrlDomainNormalized(String userUrlDomainNormalized)
    {
        this.userUrlDomainNormalized = userUrlDomainNormalized;
    }

    public Boolean isAutoRedirectEnabled()
    {
        return this.autoRedirectEnabled;
    }
    public void setAutoRedirectEnabled(Boolean autoRedirectEnabled)
    {
        this.autoRedirectEnabled = autoRedirectEnabled;
    }

    public Boolean isViewEnabled()
    {
        return this.viewEnabled;
    }
    public void setViewEnabled(Boolean viewEnabled)
    {
        this.viewEnabled = viewEnabled;
    }

    public Boolean isShareEnabled()
    {
        return this.shareEnabled;
    }
    public void setShareEnabled(Boolean shareEnabled)
    {
        this.shareEnabled = shareEnabled;
    }

    public String getDefaultDomain()
    {
        return this.defaultDomain;
    }
    public void setDefaultDomain(String defaultDomain)
    {
        this.defaultDomain = defaultDomain;
    }

    public String getDefaultTokenType()
    {
        return this.defaultTokenType;
    }
    public void setDefaultTokenType(String defaultTokenType)
    {
        this.defaultTokenType = defaultTokenType;
    }

    public String getDefaultRedirectType()
    {
        return this.defaultRedirectType;
    }
    public void setDefaultRedirectType(String defaultRedirectType)
    {
        this.defaultRedirectType = defaultRedirectType;
    }

    public Long getDefaultFlashDuration()
    {
        return this.defaultFlashDuration;
    }
    public void setDefaultFlashDuration(Long defaultFlashDuration)
    {
        this.defaultFlashDuration = defaultFlashDuration;
    }

    public String getDefaultAccessType()
    {
        return this.defaultAccessType;
    }
    public void setDefaultAccessType(String defaultAccessType)
    {
        this.defaultAccessType = defaultAccessType;
    }

    public String getDefaultViewType()
    {
        return this.defaultViewType;
    }
    public void setDefaultViewType(String defaultViewType)
    {
        this.defaultViewType = defaultViewType;
    }

    public String getDefaultShareType()
    {
        return this.defaultShareType;
    }
    public void setDefaultShareType(String defaultShareType)
    {
        this.defaultShareType = defaultShareType;
    }

    public String getDefaultPassphrase()
    {
        return this.defaultPassphrase;
    }
    public void setDefaultPassphrase(String defaultPassphrase)
    {
        this.defaultPassphrase = defaultPassphrase;
    }

    public String getDefaultSignature()
    {
        return this.defaultSignature;
    }
    public void setDefaultSignature(String defaultSignature)
    {
        this.defaultSignature = defaultSignature;
    }

    public Boolean isUseSignature()
    {
        return this.useSignature;
    }
    public void setUseSignature(Boolean useSignature)
    {
        this.useSignature = useSignature;
    }

    public String getDefaultEmblem()
    {
        return this.defaultEmblem;
    }
    public void setDefaultEmblem(String defaultEmblem)
    {
        this.defaultEmblem = defaultEmblem;
    }

    public Boolean isUseEmblem()
    {
        return this.useEmblem;
    }
    public void setUseEmblem(Boolean useEmblem)
    {
        this.useEmblem = useEmblem;
    }

    public String getDefaultBackground()
    {
        return this.defaultBackground;
    }
    public void setDefaultBackground(String defaultBackground)
    {
        this.defaultBackground = defaultBackground;
    }

    public Boolean isUseBackground()
    {
        return this.useBackground;
    }
    public void setUseBackground(Boolean useBackground)
    {
        this.useBackground = useBackground;
    }

    public String getSponsorUrl()
    {
        return this.sponsorUrl;
    }
    public void setSponsorUrl(String sponsorUrl)
    {
        this.sponsorUrl = sponsorUrl;
    }

    public String getSponsorBanner()
    {
        return this.sponsorBanner;
    }
    public void setSponsorBanner(String sponsorBanner)
    {
        this.sponsorBanner = sponsorBanner;
    }

    public String getSponsorNote()
    {
        return this.sponsorNote;
    }
    public void setSponsorNote(String sponsorNote)
    {
        this.sponsorNote = sponsorNote;
    }

    public Boolean isUseSponsor()
    {
        return this.useSponsor;
    }
    public void setUseSponsor(Boolean useSponsor)
    {
        this.useSponsor = useSponsor;
    }

    public String getExtraParams()
    {
        return this.extraParams;
    }
    public void setExtraParams(String extraParams)
    {
        this.extraParams = extraParams;
    }

    public Long getExpirationDuration()
    {
        return this.expirationDuration;
    }
    public void setExpirationDuration(Long expirationDuration)
    {
        this.expirationDuration = expirationDuration;
    }

    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:null, ");
        sb.append("user:null, ");
        sb.append("homePage:null, ");
        sb.append("selfBio:null, ");
        sb.append("userUrlDomain:null, ");
        sb.append("userUrlDomainNormalized:null, ");
        sb.append("autoRedirectEnabled:false, ");
        sb.append("viewEnabled:false, ");
        sb.append("shareEnabled:false, ");
        sb.append("defaultDomain:null, ");
        sb.append("defaultTokenType:null, ");
        sb.append("defaultRedirectType:null, ");
        sb.append("defaultFlashDuration:0, ");
        sb.append("defaultAccessType:null, ");
        sb.append("defaultViewType:null, ");
        sb.append("defaultShareType:null, ");
        sb.append("defaultPassphrase:null, ");
        sb.append("defaultSignature:null, ");
        sb.append("useSignature:false, ");
        sb.append("defaultEmblem:null, ");
        sb.append("useEmblem:false, ");
        sb.append("defaultBackground:null, ");
        sb.append("useBackground:false, ");
        sb.append("sponsorUrl:null, ");
        sb.append("sponsorBanner:null, ");
        sb.append("sponsorNote:null, ");
        sb.append("useSponsor:false, ");
        sb.append("extraParams:null, ");
        sb.append("expirationDuration:0, ");
        sb.append("createdTime:0, ");
        sb.append("modifiedTime:0, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:");
        if(this.getGuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGuid()).append("\", ");
        }
        sb.append("user:");
        if(this.getUser() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUser()).append("\", ");
        }
        sb.append("homePage:");
        if(this.getHomePage() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getHomePage()).append("\", ");
        }
        sb.append("selfBio:");
        if(this.getSelfBio() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getSelfBio()).append("\", ");
        }
        sb.append("userUrlDomain:");
        if(this.getUserUrlDomain() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUserUrlDomain()).append("\", ");
        }
        sb.append("userUrlDomainNormalized:");
        if(this.getUserUrlDomainNormalized() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUserUrlDomainNormalized()).append("\", ");
        }
        sb.append("autoRedirectEnabled:" + this.isAutoRedirectEnabled()).append(", ");
        sb.append("viewEnabled:" + this.isViewEnabled()).append(", ");
        sb.append("shareEnabled:" + this.isShareEnabled()).append(", ");
        sb.append("defaultDomain:");
        if(this.getDefaultDomain() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDefaultDomain()).append("\", ");
        }
        sb.append("defaultTokenType:");
        if(this.getDefaultTokenType() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDefaultTokenType()).append("\", ");
        }
        sb.append("defaultRedirectType:");
        if(this.getDefaultRedirectType() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDefaultRedirectType()).append("\", ");
        }
        sb.append("defaultFlashDuration:" + this.getDefaultFlashDuration()).append(", ");
        sb.append("defaultAccessType:");
        if(this.getDefaultAccessType() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDefaultAccessType()).append("\", ");
        }
        sb.append("defaultViewType:");
        if(this.getDefaultViewType() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDefaultViewType()).append("\", ");
        }
        sb.append("defaultShareType:");
        if(this.getDefaultShareType() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDefaultShareType()).append("\", ");
        }
        sb.append("defaultPassphrase:");
        if(this.getDefaultPassphrase() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDefaultPassphrase()).append("\", ");
        }
        sb.append("defaultSignature:");
        if(this.getDefaultSignature() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDefaultSignature()).append("\", ");
        }
        sb.append("useSignature:" + this.isUseSignature()).append(", ");
        sb.append("defaultEmblem:");
        if(this.getDefaultEmblem() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDefaultEmblem()).append("\", ");
        }
        sb.append("useEmblem:" + this.isUseEmblem()).append(", ");
        sb.append("defaultBackground:");
        if(this.getDefaultBackground() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDefaultBackground()).append("\", ");
        }
        sb.append("useBackground:" + this.isUseBackground()).append(", ");
        sb.append("sponsorUrl:");
        if(this.getSponsorUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getSponsorUrl()).append("\", ");
        }
        sb.append("sponsorBanner:");
        if(this.getSponsorBanner() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getSponsorBanner()).append("\", ");
        }
        sb.append("sponsorNote:");
        if(this.getSponsorNote() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getSponsorNote()).append("\", ");
        }
        sb.append("useSponsor:" + this.isUseSponsor()).append(", ");
        sb.append("extraParams:");
        if(this.getExtraParams() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getExtraParams()).append("\", ");
        }
        sb.append("expirationDuration:" + this.getExpirationDuration()).append(", ");
        sb.append("createdTime:" + this.getCreatedTime()).append(", ");
        sb.append("modifiedTime:" + this.getModifiedTime()).append(", ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getGuid() != null) {
            sb.append("\"guid\":").append("\"").append(this.getGuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"guid\":").append("null, ");
        }
        if(this.getUser() != null) {
            sb.append("\"user\":").append("\"").append(this.getUser()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"user\":").append("null, ");
        }
        if(this.getHomePage() != null) {
            sb.append("\"homePage\":").append("\"").append(this.getHomePage()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"homePage\":").append("null, ");
        }
        if(this.getSelfBio() != null) {
            sb.append("\"selfBio\":").append("\"").append(this.getSelfBio()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"selfBio\":").append("null, ");
        }
        if(this.getUserUrlDomain() != null) {
            sb.append("\"userUrlDomain\":").append("\"").append(this.getUserUrlDomain()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"userUrlDomain\":").append("null, ");
        }
        if(this.getUserUrlDomainNormalized() != null) {
            sb.append("\"userUrlDomainNormalized\":").append("\"").append(this.getUserUrlDomainNormalized()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"userUrlDomainNormalized\":").append("null, ");
        }
        if(this.isAutoRedirectEnabled() != null) {
            sb.append("\"autoRedirectEnabled\":").append("").append(this.isAutoRedirectEnabled()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"autoRedirectEnabled\":").append("null, ");
        }
        if(this.isViewEnabled() != null) {
            sb.append("\"viewEnabled\":").append("").append(this.isViewEnabled()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"viewEnabled\":").append("null, ");
        }
        if(this.isShareEnabled() != null) {
            sb.append("\"shareEnabled\":").append("").append(this.isShareEnabled()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"shareEnabled\":").append("null, ");
        }
        if(this.getDefaultDomain() != null) {
            sb.append("\"defaultDomain\":").append("\"").append(this.getDefaultDomain()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"defaultDomain\":").append("null, ");
        }
        if(this.getDefaultTokenType() != null) {
            sb.append("\"defaultTokenType\":").append("\"").append(this.getDefaultTokenType()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"defaultTokenType\":").append("null, ");
        }
        if(this.getDefaultRedirectType() != null) {
            sb.append("\"defaultRedirectType\":").append("\"").append(this.getDefaultRedirectType()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"defaultRedirectType\":").append("null, ");
        }
        if(this.getDefaultFlashDuration() != null) {
            sb.append("\"defaultFlashDuration\":").append("").append(this.getDefaultFlashDuration()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"defaultFlashDuration\":").append("null, ");
        }
        if(this.getDefaultAccessType() != null) {
            sb.append("\"defaultAccessType\":").append("\"").append(this.getDefaultAccessType()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"defaultAccessType\":").append("null, ");
        }
        if(this.getDefaultViewType() != null) {
            sb.append("\"defaultViewType\":").append("\"").append(this.getDefaultViewType()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"defaultViewType\":").append("null, ");
        }
        if(this.getDefaultShareType() != null) {
            sb.append("\"defaultShareType\":").append("\"").append(this.getDefaultShareType()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"defaultShareType\":").append("null, ");
        }
        if(this.getDefaultPassphrase() != null) {
            sb.append("\"defaultPassphrase\":").append("\"").append(this.getDefaultPassphrase()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"defaultPassphrase\":").append("null, ");
        }
        if(this.getDefaultSignature() != null) {
            sb.append("\"defaultSignature\":").append("\"").append(this.getDefaultSignature()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"defaultSignature\":").append("null, ");
        }
        if(this.isUseSignature() != null) {
            sb.append("\"useSignature\":").append("").append(this.isUseSignature()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"useSignature\":").append("null, ");
        }
        if(this.getDefaultEmblem() != null) {
            sb.append("\"defaultEmblem\":").append("\"").append(this.getDefaultEmblem()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"defaultEmblem\":").append("null, ");
        }
        if(this.isUseEmblem() != null) {
            sb.append("\"useEmblem\":").append("").append(this.isUseEmblem()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"useEmblem\":").append("null, ");
        }
        if(this.getDefaultBackground() != null) {
            sb.append("\"defaultBackground\":").append("\"").append(this.getDefaultBackground()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"defaultBackground\":").append("null, ");
        }
        if(this.isUseBackground() != null) {
            sb.append("\"useBackground\":").append("").append(this.isUseBackground()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"useBackground\":").append("null, ");
        }
        if(this.getSponsorUrl() != null) {
            sb.append("\"sponsorUrl\":").append("\"").append(this.getSponsorUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"sponsorUrl\":").append("null, ");
        }
        if(this.getSponsorBanner() != null) {
            sb.append("\"sponsorBanner\":").append("\"").append(this.getSponsorBanner()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"sponsorBanner\":").append("null, ");
        }
        if(this.getSponsorNote() != null) {
            sb.append("\"sponsorNote\":").append("\"").append(this.getSponsorNote()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"sponsorNote\":").append("null, ");
        }
        if(this.isUseSponsor() != null) {
            sb.append("\"useSponsor\":").append("").append(this.isUseSponsor()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"useSponsor\":").append("null, ");
        }
        if(this.getExtraParams() != null) {
            sb.append("\"extraParams\":").append("\"").append(this.getExtraParams()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"extraParams\":").append("null, ");
        }
        if(this.getExpirationDuration() != null) {
            sb.append("\"expirationDuration\":").append("").append(this.getExpirationDuration()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"expirationDuration\":").append("null, ");
        }
        if(this.getCreatedTime() != null) {
            sb.append("\"createdTime\":").append("").append(this.getCreatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"createdTime\":").append("null, ");
        }
        if(this.getModifiedTime() != null) {
            sb.append("\"modifiedTime\":").append("").append(this.getModifiedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"modifiedTime\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer( super.toString() );
        sb.append("user = " + this.user).append(";");
        sb.append("homePage = " + this.homePage).append(";");
        sb.append("selfBio = " + this.selfBio).append(";");
        sb.append("userUrlDomain = " + this.userUrlDomain).append(";");
        sb.append("userUrlDomainNormalized = " + this.userUrlDomainNormalized).append(";");
        sb.append("autoRedirectEnabled = " + this.autoRedirectEnabled).append(";");
        sb.append("viewEnabled = " + this.viewEnabled).append(";");
        sb.append("shareEnabled = " + this.shareEnabled).append(";");
        sb.append("defaultDomain = " + this.defaultDomain).append(";");
        sb.append("defaultTokenType = " + this.defaultTokenType).append(";");
        sb.append("defaultRedirectType = " + this.defaultRedirectType).append(";");
        sb.append("defaultFlashDuration = " + this.defaultFlashDuration).append(";");
        sb.append("defaultAccessType = " + this.defaultAccessType).append(";");
        sb.append("defaultViewType = " + this.defaultViewType).append(";");
        sb.append("defaultShareType = " + this.defaultShareType).append(";");
        sb.append("defaultPassphrase = " + this.defaultPassphrase).append(";");
        sb.append("defaultSignature = " + this.defaultSignature).append(";");
        sb.append("useSignature = " + this.useSignature).append(";");
        sb.append("defaultEmblem = " + this.defaultEmblem).append(";");
        sb.append("useEmblem = " + this.useEmblem).append(";");
        sb.append("defaultBackground = " + this.defaultBackground).append(";");
        sb.append("useBackground = " + this.useBackground).append(";");
        sb.append("sponsorUrl = " + this.sponsorUrl).append(";");
        sb.append("sponsorBanner = " + this.sponsorBanner).append(";");
        sb.append("sponsorNote = " + this.sponsorNote).append(";");
        sb.append("useSponsor = " + this.useSponsor).append(";");
        sb.append("extraParams = " + this.extraParams).append(";");
        sb.append("expirationDuration = " + this.expirationDuration).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        UserSettingJsBean cloned = new UserSettingJsBean();
        cloned.setGuid(null);          // Do not clone the guid.
        cloned.setUser(this.getUser());   
        cloned.setHomePage(this.getHomePage());   
        cloned.setSelfBio(this.getSelfBio());   
        cloned.setUserUrlDomain(this.getUserUrlDomain());   
        cloned.setUserUrlDomainNormalized(this.getUserUrlDomainNormalized());   
        cloned.setAutoRedirectEnabled(this.isAutoRedirectEnabled());   
        cloned.setViewEnabled(this.isViewEnabled());   
        cloned.setShareEnabled(this.isShareEnabled());   
        cloned.setDefaultDomain(this.getDefaultDomain());   
        cloned.setDefaultTokenType(this.getDefaultTokenType());   
        cloned.setDefaultRedirectType(this.getDefaultRedirectType());   
        cloned.setDefaultFlashDuration(this.getDefaultFlashDuration());   
        cloned.setDefaultAccessType(this.getDefaultAccessType());   
        cloned.setDefaultViewType(this.getDefaultViewType());   
        cloned.setDefaultShareType(this.getDefaultShareType());   
        cloned.setDefaultPassphrase(this.getDefaultPassphrase());   
        cloned.setDefaultSignature(this.getDefaultSignature());   
        cloned.setUseSignature(this.isUseSignature());   
        cloned.setDefaultEmblem(this.getDefaultEmblem());   
        cloned.setUseEmblem(this.isUseEmblem());   
        cloned.setDefaultBackground(this.getDefaultBackground());   
        cloned.setUseBackground(this.isUseBackground());   
        cloned.setSponsorUrl(this.getSponsorUrl());   
        cloned.setSponsorBanner(this.getSponsorBanner());   
        cloned.setSponsorNote(this.getSponsorNote());   
        cloned.setUseSponsor(this.isUseSponsor());   
        cloned.setExtraParams(this.getExtraParams());   
        cloned.setExpirationDuration(this.getExpirationDuration());   
        cloned.setCreatedTime(null);   // Reset the timestamp.
        cloned.setModifiedTime(null);   // Reset the timestamp.
        return cloned;
    }

}
