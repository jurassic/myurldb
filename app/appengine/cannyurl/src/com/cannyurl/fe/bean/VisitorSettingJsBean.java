package com.cannyurl.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.cannyurl.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class VisitorSettingJsBean extends PersonalSettingJsBean implements Serializable, Cloneable  //, VisitorSetting
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(VisitorSettingJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String user;
    private String redirectType;
    private Long flashDuration;
    private String privacyType;

    // Ctors.
    public VisitorSettingJsBean()
    {
        //this((String) null);
    }
    public VisitorSettingJsBean(String guid)
    {
        this(guid, null, null, null, null, null, null);
    }
    public VisitorSettingJsBean(String guid, String user, String redirectType, Long flashDuration, String privacyType)
    {
        this(guid, user, redirectType, flashDuration, privacyType, null, null);
    }
    public VisitorSettingJsBean(String guid, String user, String redirectType, Long flashDuration, String privacyType, Long createdTime, Long modifiedTime)
    {
        super(guid, createdTime, modifiedTime);

        this.user = user;
        this.redirectType = redirectType;
        this.flashDuration = flashDuration;
        this.privacyType = privacyType;
    }
    public VisitorSettingJsBean(VisitorSettingJsBean bean)
    {
        if(bean != null) {
            setGuid(bean.getGuid());
            setUser(bean.getUser());
            setRedirectType(bean.getRedirectType());
            setFlashDuration(bean.getFlashDuration());
            setPrivacyType(bean.getPrivacyType());
            setCreatedTime(bean.getCreatedTime());
            setModifiedTime(bean.getModifiedTime());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static VisitorSettingJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        VisitorSettingJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(VisitorSettingJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, VisitorSettingJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    public String getRedirectType()
    {
        return this.redirectType;
    }
    public void setRedirectType(String redirectType)
    {
        this.redirectType = redirectType;
    }

    public Long getFlashDuration()
    {
        return this.flashDuration;
    }
    public void setFlashDuration(Long flashDuration)
    {
        this.flashDuration = flashDuration;
    }

    public String getPrivacyType()
    {
        return this.privacyType;
    }
    public void setPrivacyType(String privacyType)
    {
        this.privacyType = privacyType;
    }

    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:null, ");
        sb.append("user:null, ");
        sb.append("redirectType:null, ");
        sb.append("flashDuration:0, ");
        sb.append("privacyType:null, ");
        sb.append("createdTime:0, ");
        sb.append("modifiedTime:0, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:");
        if(this.getGuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGuid()).append("\", ");
        }
        sb.append("user:");
        if(this.getUser() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUser()).append("\", ");
        }
        sb.append("redirectType:");
        if(this.getRedirectType() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getRedirectType()).append("\", ");
        }
        sb.append("flashDuration:" + this.getFlashDuration()).append(", ");
        sb.append("privacyType:");
        if(this.getPrivacyType() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getPrivacyType()).append("\", ");
        }
        sb.append("createdTime:" + this.getCreatedTime()).append(", ");
        sb.append("modifiedTime:" + this.getModifiedTime()).append(", ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getGuid() != null) {
            sb.append("\"guid\":").append("\"").append(this.getGuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"guid\":").append("null, ");
        }
        if(this.getUser() != null) {
            sb.append("\"user\":").append("\"").append(this.getUser()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"user\":").append("null, ");
        }
        if(this.getRedirectType() != null) {
            sb.append("\"redirectType\":").append("\"").append(this.getRedirectType()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"redirectType\":").append("null, ");
        }
        if(this.getFlashDuration() != null) {
            sb.append("\"flashDuration\":").append("").append(this.getFlashDuration()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"flashDuration\":").append("null, ");
        }
        if(this.getPrivacyType() != null) {
            sb.append("\"privacyType\":").append("\"").append(this.getPrivacyType()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"privacyType\":").append("null, ");
        }
        if(this.getCreatedTime() != null) {
            sb.append("\"createdTime\":").append("").append(this.getCreatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"createdTime\":").append("null, ");
        }
        if(this.getModifiedTime() != null) {
            sb.append("\"modifiedTime\":").append("").append(this.getModifiedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"modifiedTime\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer( super.toString() );
        sb.append("user = " + this.user).append(";");
        sb.append("redirectType = " + this.redirectType).append(";");
        sb.append("flashDuration = " + this.flashDuration).append(";");
        sb.append("privacyType = " + this.privacyType).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        VisitorSettingJsBean cloned = new VisitorSettingJsBean();
        cloned.setGuid(null);          // Do not clone the guid.
        cloned.setUser(this.getUser());   
        cloned.setRedirectType(this.getRedirectType());   
        cloned.setFlashDuration(this.getFlashDuration());   
        cloned.setPrivacyType(this.getPrivacyType());   
        cloned.setCreatedTime(null);   // Reset the timestamp.
        cloned.setModifiedTime(null);   // Reset the timestamp.
        return cloned;
    }

}
