package com.cannyurl.fe.bean.mock;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.myurldb.ws.ReferrerInfoStruct;
import com.cannyurl.fe.Validateable;
import com.cannyurl.fe.core.StringEscapeUtil;
import com.cannyurl.fe.bean.ReferrerInfoStructJsBean;
import com.cannyurl.fe.bean.ShortLinkJsBean;


// Place holder...
@JsonIgnoreProperties(ignoreUnknown = true)
public class ShortLinkMockBean extends ShortLinkJsBean implements Serializable, Cloneable, Validateable  //, ShortLink
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ShortLinkMockBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    // Error map: "field name" -> List<"error message">.
    private Map<String,List<String>> errorMap = new HashMap<String,List<String>>();

    // Ctors.
    public ShortLinkMockBean()
    {
        super();
    }
    public ShortLinkMockBean(String guid)
    {
       super(guid);
    }
    public ShortLinkMockBean(String guid, String appClient, String clientRootDomain, String owner, String longUrlDomain, String longUrl, String longUrlFull, String longUrlHash, Boolean reuseExistingShortUrl, Boolean failIfShortUrlExists, String domain, String domainType, String usercode, String username, String tokenPrefix, String token, String tokenType, String sassyTokenType, String shortUrl, String shortPassage, String redirectType, Long flashDuration, String accessType, String viewType, String shareType, Boolean readOnly, String displayMessage, String shortMessage, Boolean keywordEnabled, Boolean bookmarkEnabled, ReferrerInfoStructJsBean referrerInfo, String status, String note, Long expirationTime)
    {
        super(guid, appClient, clientRootDomain, owner, longUrlDomain, longUrl, longUrlFull, longUrlHash, reuseExistingShortUrl, failIfShortUrlExists, domain, domainType, usercode, username, tokenPrefix, token, tokenType, sassyTokenType, shortUrl, shortPassage, redirectType, flashDuration, accessType, viewType, shareType, readOnly, displayMessage, shortMessage, keywordEnabled, bookmarkEnabled, referrerInfo, status, note, expirationTime);
    }
    public ShortLinkMockBean(String guid, String appClient, String clientRootDomain, String owner, String longUrlDomain, String longUrl, String longUrlFull, String longUrlHash, Boolean reuseExistingShortUrl, Boolean failIfShortUrlExists, String domain, String domainType, String usercode, String username, String tokenPrefix, String token, String tokenType, String sassyTokenType, String shortUrl, String shortPassage, String redirectType, Long flashDuration, String accessType, String viewType, String shareType, Boolean readOnly, String displayMessage, String shortMessage, Boolean keywordEnabled, Boolean bookmarkEnabled, ReferrerInfoStructJsBean referrerInfo, String status, String note, Long expirationTime, Long createdTime, Long modifiedTime)
    {
        super(guid, appClient, clientRootDomain, owner, longUrlDomain, longUrl, longUrlFull, longUrlHash, reuseExistingShortUrl, failIfShortUrlExists, domain, domainType, usercode, username, tokenPrefix, token, tokenType, sassyTokenType, shortUrl, shortPassage, redirectType, flashDuration, accessType, viewType, shareType, readOnly, displayMessage, shortMessage, keywordEnabled, bookmarkEnabled, referrerInfo, status, note, expirationTime, createdTime, modifiedTime);
    }
    public ShortLinkMockBean(ShortLinkJsBean bean)
    {
        super(bean);
    }

    public static ShortLinkMockBean fromJsonString(String jsonStr)
    {
        ShortLinkMockBean bean = null;
        try {
            // TBD:
            bean = getObjectMapper().readValue(jsonStr, ShortLinkMockBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    
    public Map<String,List<String>> getErrorMap()
    {
        return errorMap;
    }

    public boolean hasErrors() 
    {
        // temporary. (An error without error message?)
        if(errorMap.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }
    public boolean hasErrors(String f) 
    {
        // temporary. (An error without error message?)
        List<String> errorList = errorMap.get(f);
        if(errorList == null || errorList.isEmpty()) {
            return false;
        } else {
            return true;
        }        
    }

    public String getLastError(String f) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null || errorList.isEmpty()) {
            return null;
        } else {
            return errorList.get(errorList.size() - 1);
        }
    }
    public List<String> getErrors(String f) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            return new ArrayList<String>();
        } else {
            return errorList;
        }
    }

    public List<String> addError(String f, String error) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            errorList = new ArrayList<String>();
            errorMap.put(f, errorList);
        }
        errorList.add(error);
        return errorList;
    }
    public void setError(String f, String error) 
    {
        List<String> errorList = new ArrayList<String>();
        errorList.add(error);
        errorMap.put(f, errorList);
    }
    public List<String> addErrors(String f, List<String> errors) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            errorList = new ArrayList<String>();
            errorMap.put(f, errorList);
        }
        errorList.addAll(errors);
        return errorList;
    }
    public void setErrors(String f, List<String> errors) 
    {
        errorMap.put(f, errors);
    }

    public void resetErrors()
    {
        errorMap.clear();
    }
    public void resetErrors(String f)
    {
        errorMap.remove(f);
    }


    public boolean validate()
    {
        boolean allOK = true;
       
//        // TBD
//        if(getGuid() == null) {
//            addError("guid", "guid is null");
//            allOK = false;
//        }
//        // TBD
//        if(getAppClient() == null) {
//            addError("appClient", "appClient is null");
//            allOK = false;
//        }
//        // TBD
//        if(getClientRootDomain() == null) {
//            addError("clientRootDomain", "clientRootDomain is null");
//            allOK = false;
//        }
//        // TBD
//        if(getOwner() == null) {
//            addError("owner", "owner is null");
//            allOK = false;
//        }
//        // TBD
//        if(getLongUrlDomain() == null) {
//            addError("longUrlDomain", "longUrlDomain is null");
//            allOK = false;
//        }
//        // TBD
//        if(getLongUrl() == null) {
//            addError("longUrl", "longUrl is null");
//            allOK = false;
//        }
//        // TBD
//        if(getLongUrlFull() == null) {
//            addError("longUrlFull", "longUrlFull is null");
//            allOK = false;
//        }
//        // TBD
//        if(getLongUrlHash() == null) {
//            addError("longUrlHash", "longUrlHash is null");
//            allOK = false;
//        }
//        // TBD
//        if(isReuseExistingShortUrl() == null) {
//            addError("reuseExistingShortUrl", "reuseExistingShortUrl is null");
//            allOK = false;
//        }
//        // TBD
//        if(isFailIfShortUrlExists() == null) {
//            addError("failIfShortUrlExists", "failIfShortUrlExists is null");
//            allOK = false;
//        }
//        // TBD
//        if(getDomain() == null) {
//            addError("domain", "domain is null");
//            allOK = false;
//        }
//        // TBD
//        if(getDomainType() == null) {
//            addError("domainType", "domainType is null");
//            allOK = false;
//        }
//        // TBD
//        if(getUsercode() == null) {
//            addError("usercode", "usercode is null");
//            allOK = false;
//        }
//        // TBD
//        if(getUsername() == null) {
//            addError("username", "username is null");
//            allOK = false;
//        }
//        // TBD
//        if(getTokenPrefix() == null) {
//            addError("tokenPrefix", "tokenPrefix is null");
//            allOK = false;
//        }
//        // TBD
//        if(getToken() == null) {
//            addError("token", "token is null");
//            allOK = false;
//        }
//        // TBD
//        if(getTokenType() == null) {
//            addError("tokenType", "tokenType is null");
//            allOK = false;
//        }
//        // TBD
//        if(getSassyTokenType() == null) {
//            addError("sassyTokenType", "sassyTokenType is null");
//            allOK = false;
//        }
//        // TBD
//        if(getShortUrl() == null) {
//            addError("shortUrl", "shortUrl is null");
//            allOK = false;
//        }
//        // TBD
//        if(getShortPassage() == null) {
//            addError("shortPassage", "shortPassage is null");
//            allOK = false;
//        }
//        // TBD
//        if(getRedirectType() == null) {
//            addError("redirectType", "redirectType is null");
//            allOK = false;
//        }
//        // TBD
//        if(getFlashDuration() == null) {
//            addError("flashDuration", "flashDuration is null");
//            allOK = false;
//        }
//        // TBD
//        if(getAccessType() == null) {
//            addError("accessType", "accessType is null");
//            allOK = false;
//        }
//        // TBD
//        if(getViewType() == null) {
//            addError("viewType", "viewType is null");
//            allOK = false;
//        }
//        // TBD
//        if(getShareType() == null) {
//            addError("shareType", "shareType is null");
//            allOK = false;
//        }
//        // TBD
//        if(isReadOnly() == null) {
//            addError("readOnly", "readOnly is null");
//            allOK = false;
//        }
//        // TBD
//        if(getDisplayMessage() == null) {
//            addError("displayMessage", "displayMessage is null");
//            allOK = false;
//        }
//        // TBD
//        if(getShortMessage() == null) {
//            addError("shortMessage", "shortMessage is null");
//            allOK = false;
//        }
//        // TBD
//        if(isKeywordEnabled() == null) {
//            addError("keywordEnabled", "keywordEnabled is null");
//            allOK = false;
//        }
//        // TBD
//        if(isBookmarkEnabled() == null) {
//            addError("bookmarkEnabled", "bookmarkEnabled is null");
//            allOK = false;
//        }
//        // TBD
//        if(getReferrerInfo() == null) {
//            addError("referrerInfo", "referrerInfo is null");
//            allOK = false;
//        } else {
//            ReferrerInfoStructJsBean referrerInfo = getReferrerInfo();
//            // ...
//            // allOK = false;
//        }
//        // TBD
//        if(getStatus() == null) {
//            addError("status", "status is null");
//            allOK = false;
//        }
//        // TBD
//        if(getNote() == null) {
//            addError("note", "note is null");
//            allOK = false;
//        }
//        // TBD
//        if(getExpirationTime() == null) {
//            addError("expirationTime", "expirationTime is null");
//            allOK = false;
//        }
//        // TBD
//        if(getCreatedTime() == null) {
//            addError("createdTime", "createdTime is null");
//            allOK = false;
//        }
//        // TBD
//        if(getModifiedTime() == null) {
//            addError("modifiedTime", "modifiedTime is null");
//            allOK = false;
//        }

        return allOK;
    }


    public String toJsonString()
    {
        String jsonStr = null;
        try {
            // TBD: 
            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer( super.toString() );
        sb.append("error = {");
        for(String f : errorMap.keySet()) {
            List<String> errorList = errorMap.get(f);
            if(errorList != null && !errorList.isEmpty()) {
                sb.append(f).append(": [");
                for(String e : errorList) {
                    sb.append(e).append("; ");
                }
                sb.append("];");
            }
        }
        sb.append("};");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        ShortLinkMockBean cloned = new ShortLinkMockBean((ShortLinkJsBean) super.clone());
        return cloned;
    }

}
