package com.cannyurl.fe.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.cannyurl.fe.bean.KeyValuePairStructJsBean;
import com.cannyurl.fe.bean.KeyValueRelationStructJsBean;
import com.cannyurl.fe.bean.ExternalServiceApiKeyStructJsBean;
import com.cannyurl.fe.bean.GeoPointStructJsBean;
import com.cannyurl.fe.bean.GeoCoordinateStructJsBean;
import com.cannyurl.fe.bean.CellLatitudeLongitudeJsBean;
import com.cannyurl.fe.bean.StreetAddressStructJsBean;
import com.cannyurl.fe.bean.WebProfileStructJsBean;
import com.cannyurl.fe.bean.FullNameStructJsBean;
import com.cannyurl.fe.bean.ContactInfoStructJsBean;
import com.cannyurl.fe.bean.ReferrerInfoStructJsBean;
import com.cannyurl.fe.bean.GaeAppStructJsBean;
import com.cannyurl.fe.bean.GaeUserStructJsBean;
import com.cannyurl.fe.bean.PagerStateStructJsBean;
import com.cannyurl.fe.bean.AppBrandStructJsBean;
import com.cannyurl.fe.bean.ApiConsumerJsBean;
import com.cannyurl.fe.bean.AppCustomDomainJsBean;
import com.cannyurl.fe.bean.SiteCustomDomainJsBean;
import com.cannyurl.fe.bean.UserJsBean;
import com.cannyurl.fe.bean.UserUsercodeJsBean;
import com.cannyurl.fe.bean.HashedPasswordStructJsBean;
import com.cannyurl.fe.bean.UserPasswordJsBean;
import com.cannyurl.fe.bean.ExternalUserIdStructJsBean;
import com.cannyurl.fe.bean.ExternalUserAuthJsBean;
import com.cannyurl.fe.bean.UserAuthStateJsBean;
import com.cannyurl.fe.bean.UserResourcePermissionJsBean;
import com.cannyurl.fe.bean.UserResourceProhibitionJsBean;
import com.cannyurl.fe.bean.RolePermissionJsBean;
import com.cannyurl.fe.bean.UserRoleJsBean;
import com.cannyurl.fe.bean.AppClientJsBean;
import com.cannyurl.fe.bean.ClientUserJsBean;
import com.cannyurl.fe.bean.UserCustomDomainJsBean;
import com.cannyurl.fe.bean.ClientSettingJsBean;
import com.cannyurl.fe.bean.UserSettingJsBean;
import com.cannyurl.fe.bean.VisitorSettingJsBean;
import com.cannyurl.fe.bean.TwitterCardAppInfoJsBean;
import com.cannyurl.fe.bean.TwitterCardProductDataJsBean;
import com.cannyurl.fe.bean.TwitterSummaryCardJsBean;
import com.cannyurl.fe.bean.TwitterPhotoCardJsBean;
import com.cannyurl.fe.bean.TwitterGalleryCardJsBean;
import com.cannyurl.fe.bean.TwitterAppCardJsBean;
import com.cannyurl.fe.bean.TwitterPlayerCardJsBean;
import com.cannyurl.fe.bean.TwitterProductCardJsBean;
import com.cannyurl.fe.bean.ShortPassageAttributeJsBean;
import com.cannyurl.fe.bean.ShortPassageJsBean;
import com.cannyurl.fe.bean.ShortLinkJsBean;
import com.cannyurl.fe.bean.GeoLinkJsBean;
import com.cannyurl.fe.bean.QrCodeJsBean;
import com.cannyurl.fe.bean.LinkPassphraseJsBean;
import com.cannyurl.fe.bean.LinkMessageJsBean;
import com.cannyurl.fe.bean.LinkAlbumJsBean;
import com.cannyurl.fe.bean.AlbumShortLinkJsBean;
import com.cannyurl.fe.bean.KeywordFolderJsBean;
import com.cannyurl.fe.bean.BookmarkFolderJsBean;
import com.cannyurl.fe.bean.KeywordLinkJsBean;
import com.cannyurl.fe.bean.BookmarkLinkJsBean;
import com.cannyurl.fe.bean.SpeedDialJsBean;
import com.cannyurl.fe.bean.KeywordFolderImportJsBean;
import com.cannyurl.fe.bean.BookmarkFolderImportJsBean;
import com.cannyurl.fe.bean.KeywordCrowdTallyJsBean;
import com.cannyurl.fe.bean.BookmarkCrowdTallyJsBean;
import com.cannyurl.fe.bean.DomainInfoJsBean;
import com.cannyurl.fe.bean.UrlRatingJsBean;
import com.cannyurl.fe.bean.UserRatingJsBean;
import com.cannyurl.fe.bean.AbuseTagJsBean;
import com.cannyurl.fe.bean.HelpNoticeJsBean;
import com.cannyurl.fe.bean.SiteLogJsBean;
import com.cannyurl.fe.bean.ServiceInfoJsBean;
import com.cannyurl.fe.bean.FiveTenJsBean;


// Utility functions for combining/decomposing into json string segments.
public class JsonJsBeanUtil
{
    private static final Logger log = Logger.getLogger(JsonJsBeanUtil.class.getName());

    private JsonJsBeanUtil() {}

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    // Input = jsonStr: { object: objJsonString }
    // Output = Map: object -> objJsonString
    public static Map<String, String> parseJsonObjectMapString(String jsonStr)
    {
        if(jsonStr == null) {
            log.warning("Input jsonStr is null.");
            return null;
        }

        Map<String, String> jsonObjectMap = new HashMap<String, String>();

        // ???
        try {
            JsonFactory factory = new JsonFactory();
            factory.setCodec(getObjectMapper());
            JsonParser parser = factory.createJsonParser(jsonStr);

            JsonNode topNode = parser.readValueAsTree();
            Iterator<String> fieldNames = topNode.getFieldNames();
            
            while(fieldNames.hasNext()) {
                String name = fieldNames.next();
                JsonNode leafNode = topNode.get(name);
                if(! leafNode.isNull()) {
                    String value = leafNode.toString();
                    jsonObjectMap.put(name, value);
                    if(log.isLoggable(Level.INFO)) log.info("jsonObjectMap: name = " + name + "; value = " + value);
                } else {
                    if(log.isLoggable(Level.INFO)) log.info("jsonObjectMap: Empty node skipped. name = " + name);
                }
            }
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        
        return jsonObjectMap;
    }

    // Output = jsonStr: { object: objJsonString }
    // Input = Map: object -> objJsonString
    public static String generateJsonObjectMapString(Map<String, String> jsonObjectMap)
    {
        if(jsonObjectMap == null) {
            log.warning("Input jsonObjectMap is null.");
            return null;
        }

        StringBuilder sb = new StringBuilder();

        for(String key : jsonObjectMap.keySet()) {
            String json = jsonObjectMap.get(key);
            sb.append("\"").append(key).append("\":");
            if(json == null) {
                sb.append("null");  // ????
            } else {
                sb.append(json);
            }
            sb.append(",");
        }
        String innerStr = sb.toString();
        if(innerStr.endsWith(",")) {
            innerStr = innerStr.substring(0, innerStr.length() - 1);
        }
        String jsonStr = "{" + innerStr + "}";
        if(log.isLoggable(Level.INFO)) log.info("generateJsonObjectMapString(): jsonStr = " + jsonStr);

        return jsonStr;
    }


    // Input = jsonStr: { object: objJsonString }
    // Output = Map: object -> jsBean
    public static Map<String, Object> parseJsonObjectMap(String jsonStr)
    {
        if(jsonStr == null) {
            log.warning("Input jsonStr is null.");
            return null;
        }

        Map<String, Object> jsonObjectMap = new HashMap<String, Object>();

        // ???
        try {
            JsonFactory factory = new JsonFactory();
            factory.setCodec(getObjectMapper());
            JsonParser parser = factory.createJsonParser(jsonStr);
            JsonNode topNode = parser.readValueAsTree();

/*
            // TBD: Remove the loop.
            Iterator<String> fieldNames = topNode.getFieldNames();
            while(fieldNames.hasNext()) {
                String name = fieldNames.next();
                String value = topNode.get(name).toString();
                if(log.isLoggable(Level.INFO)) log.info("jsonObjectMap: name = " + name + "; value = " + value);

                if(name.equals("keyValuePairStruct")) {
                    KeyValuePairStructJsBean bean = KeyValuePairStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("keyValueRelationStruct")) {
                    KeyValueRelationStructJsBean bean = KeyValueRelationStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("externalServiceApiKeyStruct")) {
                    ExternalServiceApiKeyStructJsBean bean = ExternalServiceApiKeyStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("geoPointStruct")) {
                    GeoPointStructJsBean bean = GeoPointStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("geoCoordinateStruct")) {
                    GeoCoordinateStructJsBean bean = GeoCoordinateStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("cellLatitudeLongitude")) {
                    CellLatitudeLongitudeJsBean bean = CellLatitudeLongitudeJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("streetAddressStruct")) {
                    StreetAddressStructJsBean bean = StreetAddressStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("webProfileStruct")) {
                    WebProfileStructJsBean bean = WebProfileStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("fullNameStruct")) {
                    FullNameStructJsBean bean = FullNameStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("contactInfoStruct")) {
                    ContactInfoStructJsBean bean = ContactInfoStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("referrerInfoStruct")) {
                    ReferrerInfoStructJsBean bean = ReferrerInfoStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("gaeAppStruct")) {
                    GaeAppStructJsBean bean = GaeAppStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("gaeUserStruct")) {
                    GaeUserStructJsBean bean = GaeUserStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("pagerStateStruct")) {
                    PagerStateStructJsBean bean = PagerStateStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("appBrandStruct")) {
                    AppBrandStructJsBean bean = AppBrandStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("apiConsumer")) {
                    ApiConsumerJsBean bean = ApiConsumerJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("appCustomDomain")) {
                    AppCustomDomainJsBean bean = AppCustomDomainJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("siteCustomDomain")) {
                    SiteCustomDomainJsBean bean = SiteCustomDomainJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("user")) {
                    UserJsBean bean = UserJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("userUsercode")) {
                    UserUsercodeJsBean bean = UserUsercodeJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("hashedPasswordStruct")) {
                    HashedPasswordStructJsBean bean = HashedPasswordStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("userPassword")) {
                    UserPasswordJsBean bean = UserPasswordJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("externalUserIdStruct")) {
                    ExternalUserIdStructJsBean bean = ExternalUserIdStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("externalUserAuth")) {
                    ExternalUserAuthJsBean bean = ExternalUserAuthJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("userAuthState")) {
                    UserAuthStateJsBean bean = UserAuthStateJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("userResourcePermission")) {
                    UserResourcePermissionJsBean bean = UserResourcePermissionJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("userResourceProhibition")) {
                    UserResourceProhibitionJsBean bean = UserResourceProhibitionJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("rolePermission")) {
                    RolePermissionJsBean bean = RolePermissionJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("userRole")) {
                    UserRoleJsBean bean = UserRoleJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("appClient")) {
                    AppClientJsBean bean = AppClientJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("clientUser")) {
                    ClientUserJsBean bean = ClientUserJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("userCustomDomain")) {
                    UserCustomDomainJsBean bean = UserCustomDomainJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("clientSetting")) {
                    ClientSettingJsBean bean = ClientSettingJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("userSetting")) {
                    UserSettingJsBean bean = UserSettingJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("visitorSetting")) {
                    VisitorSettingJsBean bean = VisitorSettingJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("twitterCardAppInfo")) {
                    TwitterCardAppInfoJsBean bean = TwitterCardAppInfoJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("twitterCardProductData")) {
                    TwitterCardProductDataJsBean bean = TwitterCardProductDataJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("twitterSummaryCard")) {
                    TwitterSummaryCardJsBean bean = TwitterSummaryCardJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("twitterPhotoCard")) {
                    TwitterPhotoCardJsBean bean = TwitterPhotoCardJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("twitterGalleryCard")) {
                    TwitterGalleryCardJsBean bean = TwitterGalleryCardJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("twitterAppCard")) {
                    TwitterAppCardJsBean bean = TwitterAppCardJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("twitterPlayerCard")) {
                    TwitterPlayerCardJsBean bean = TwitterPlayerCardJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("twitterProductCard")) {
                    TwitterProductCardJsBean bean = TwitterProductCardJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("shortPassageAttribute")) {
                    ShortPassageAttributeJsBean bean = ShortPassageAttributeJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("shortPassage")) {
                    ShortPassageJsBean bean = ShortPassageJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("shortLink")) {
                    ShortLinkJsBean bean = ShortLinkJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("geoLink")) {
                    GeoLinkJsBean bean = GeoLinkJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("qrCode")) {
                    QrCodeJsBean bean = QrCodeJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("linkPassphrase")) {
                    LinkPassphraseJsBean bean = LinkPassphraseJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("linkMessage")) {
                    LinkMessageJsBean bean = LinkMessageJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("linkAlbum")) {
                    LinkAlbumJsBean bean = LinkAlbumJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("albumShortLink")) {
                    AlbumShortLinkJsBean bean = AlbumShortLinkJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("keywordFolder")) {
                    KeywordFolderJsBean bean = KeywordFolderJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("bookmarkFolder")) {
                    BookmarkFolderJsBean bean = BookmarkFolderJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("keywordLink")) {
                    KeywordLinkJsBean bean = KeywordLinkJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("bookmarkLink")) {
                    BookmarkLinkJsBean bean = BookmarkLinkJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("speedDial")) {
                    SpeedDialJsBean bean = SpeedDialJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("keywordFolderImport")) {
                    KeywordFolderImportJsBean bean = KeywordFolderImportJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("bookmarkFolderImport")) {
                    BookmarkFolderImportJsBean bean = BookmarkFolderImportJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("keywordCrowdTally")) {
                    KeywordCrowdTallyJsBean bean = KeywordCrowdTallyJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("bookmarkCrowdTally")) {
                    BookmarkCrowdTallyJsBean bean = BookmarkCrowdTallyJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("domainInfo")) {
                    DomainInfoJsBean bean = DomainInfoJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("urlRating")) {
                    UrlRatingJsBean bean = UrlRatingJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("userRating")) {
                    UserRatingJsBean bean = UserRatingJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("abuseTag")) {
                    AbuseTagJsBean bean = AbuseTagJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("helpNotice")) {
                    HelpNoticeJsBean bean = HelpNoticeJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("siteLog")) {
                    SiteLogJsBean bean = SiteLogJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("serviceInfo")) {
                    ServiceInfoJsBean bean = ServiceInfoJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("fiveTen")) {
                    FiveTenJsBean bean = FiveTenJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
            }
*/

            JsonNode objNode = null;
            String objValueStr = null;
            objNode = topNode.findValue("keyValuePairStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                KeyValuePairStructJsBean bean = KeyValuePairStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("keyValuePairStruct", bean);               
            }
            objNode = topNode.findValue("keyValueRelationStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                KeyValueRelationStructJsBean bean = KeyValueRelationStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("keyValueRelationStruct", bean);               
            }
            objNode = topNode.findValue("externalServiceApiKeyStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                ExternalServiceApiKeyStructJsBean bean = ExternalServiceApiKeyStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("externalServiceApiKeyStruct", bean);               
            }
            objNode = topNode.findValue("geoPointStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                GeoPointStructJsBean bean = GeoPointStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("geoPointStruct", bean);               
            }
            objNode = topNode.findValue("geoCoordinateStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                GeoCoordinateStructJsBean bean = GeoCoordinateStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("geoCoordinateStruct", bean);               
            }
            objNode = topNode.findValue("cellLatitudeLongitude");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                CellLatitudeLongitudeJsBean bean = CellLatitudeLongitudeJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("cellLatitudeLongitude", bean);               
            }
            objNode = topNode.findValue("streetAddressStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                StreetAddressStructJsBean bean = StreetAddressStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("streetAddressStruct", bean);               
            }
            objNode = topNode.findValue("webProfileStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                WebProfileStructJsBean bean = WebProfileStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("webProfileStruct", bean);               
            }
            objNode = topNode.findValue("fullNameStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                FullNameStructJsBean bean = FullNameStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("fullNameStruct", bean);               
            }
            objNode = topNode.findValue("contactInfoStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                ContactInfoStructJsBean bean = ContactInfoStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("contactInfoStruct", bean);               
            }
            objNode = topNode.findValue("referrerInfoStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                ReferrerInfoStructJsBean bean = ReferrerInfoStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("referrerInfoStruct", bean);               
            }
            objNode = topNode.findValue("gaeAppStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                GaeAppStructJsBean bean = GaeAppStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("gaeAppStruct", bean);               
            }
            objNode = topNode.findValue("gaeUserStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                GaeUserStructJsBean bean = GaeUserStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("gaeUserStruct", bean);               
            }
            objNode = topNode.findValue("pagerStateStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                PagerStateStructJsBean bean = PagerStateStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("pagerStateStruct", bean);               
            }
            objNode = topNode.findValue("appBrandStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                AppBrandStructJsBean bean = AppBrandStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("appBrandStruct", bean);               
            }
            objNode = topNode.findValue("apiConsumer");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                ApiConsumerJsBean bean = ApiConsumerJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("apiConsumer", bean);               
            }
            objNode = topNode.findValue("appCustomDomain");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                AppCustomDomainJsBean bean = AppCustomDomainJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("appCustomDomain", bean);               
            }
            objNode = topNode.findValue("siteCustomDomain");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                SiteCustomDomainJsBean bean = SiteCustomDomainJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("siteCustomDomain", bean);               
            }
            objNode = topNode.findValue("user");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                UserJsBean bean = UserJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("user", bean);               
            }
            objNode = topNode.findValue("userUsercode");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                UserUsercodeJsBean bean = UserUsercodeJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("userUsercode", bean);               
            }
            objNode = topNode.findValue("hashedPasswordStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                HashedPasswordStructJsBean bean = HashedPasswordStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("hashedPasswordStruct", bean);               
            }
            objNode = topNode.findValue("userPassword");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                UserPasswordJsBean bean = UserPasswordJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("userPassword", bean);               
            }
            objNode = topNode.findValue("externalUserIdStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                ExternalUserIdStructJsBean bean = ExternalUserIdStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("externalUserIdStruct", bean);               
            }
            objNode = topNode.findValue("externalUserAuth");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                ExternalUserAuthJsBean bean = ExternalUserAuthJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("externalUserAuth", bean);               
            }
            objNode = topNode.findValue("userAuthState");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                UserAuthStateJsBean bean = UserAuthStateJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("userAuthState", bean);               
            }
            objNode = topNode.findValue("userResourcePermission");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                UserResourcePermissionJsBean bean = UserResourcePermissionJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("userResourcePermission", bean);               
            }
            objNode = topNode.findValue("userResourceProhibition");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                UserResourceProhibitionJsBean bean = UserResourceProhibitionJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("userResourceProhibition", bean);               
            }
            objNode = topNode.findValue("rolePermission");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                RolePermissionJsBean bean = RolePermissionJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("rolePermission", bean);               
            }
            objNode = topNode.findValue("userRole");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                UserRoleJsBean bean = UserRoleJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("userRole", bean);               
            }
            objNode = topNode.findValue("appClient");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                AppClientJsBean bean = AppClientJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("appClient", bean);               
            }
            objNode = topNode.findValue("clientUser");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                ClientUserJsBean bean = ClientUserJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("clientUser", bean);               
            }
            objNode = topNode.findValue("userCustomDomain");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                UserCustomDomainJsBean bean = UserCustomDomainJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("userCustomDomain", bean);               
            }
            objNode = topNode.findValue("clientSetting");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                ClientSettingJsBean bean = ClientSettingJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("clientSetting", bean);               
            }
            objNode = topNode.findValue("userSetting");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                UserSettingJsBean bean = UserSettingJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("userSetting", bean);               
            }
            objNode = topNode.findValue("visitorSetting");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                VisitorSettingJsBean bean = VisitorSettingJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("visitorSetting", bean);               
            }
            objNode = topNode.findValue("twitterCardAppInfo");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                TwitterCardAppInfoJsBean bean = TwitterCardAppInfoJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("twitterCardAppInfo", bean);               
            }
            objNode = topNode.findValue("twitterCardProductData");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                TwitterCardProductDataJsBean bean = TwitterCardProductDataJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("twitterCardProductData", bean);               
            }
            objNode = topNode.findValue("twitterSummaryCard");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                TwitterSummaryCardJsBean bean = TwitterSummaryCardJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("twitterSummaryCard", bean);               
            }
            objNode = topNode.findValue("twitterPhotoCard");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                TwitterPhotoCardJsBean bean = TwitterPhotoCardJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("twitterPhotoCard", bean);               
            }
            objNode = topNode.findValue("twitterGalleryCard");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                TwitterGalleryCardJsBean bean = TwitterGalleryCardJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("twitterGalleryCard", bean);               
            }
            objNode = topNode.findValue("twitterAppCard");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                TwitterAppCardJsBean bean = TwitterAppCardJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("twitterAppCard", bean);               
            }
            objNode = topNode.findValue("twitterPlayerCard");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                TwitterPlayerCardJsBean bean = TwitterPlayerCardJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("twitterPlayerCard", bean);               
            }
            objNode = topNode.findValue("twitterProductCard");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                TwitterProductCardJsBean bean = TwitterProductCardJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("twitterProductCard", bean);               
            }
            objNode = topNode.findValue("shortPassageAttribute");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                ShortPassageAttributeJsBean bean = ShortPassageAttributeJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("shortPassageAttribute", bean);               
            }
            objNode = topNode.findValue("shortPassage");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                ShortPassageJsBean bean = ShortPassageJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("shortPassage", bean);               
            }
            objNode = topNode.findValue("shortLink");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                ShortLinkJsBean bean = ShortLinkJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("shortLink", bean);               
            }
            objNode = topNode.findValue("geoLink");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                GeoLinkJsBean bean = GeoLinkJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("geoLink", bean);               
            }
            objNode = topNode.findValue("qrCode");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                QrCodeJsBean bean = QrCodeJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("qrCode", bean);               
            }
            objNode = topNode.findValue("linkPassphrase");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                LinkPassphraseJsBean bean = LinkPassphraseJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("linkPassphrase", bean);               
            }
            objNode = topNode.findValue("linkMessage");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                LinkMessageJsBean bean = LinkMessageJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("linkMessage", bean);               
            }
            objNode = topNode.findValue("linkAlbum");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                LinkAlbumJsBean bean = LinkAlbumJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("linkAlbum", bean);               
            }
            objNode = topNode.findValue("albumShortLink");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                AlbumShortLinkJsBean bean = AlbumShortLinkJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("albumShortLink", bean);               
            }
            objNode = topNode.findValue("keywordFolder");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                KeywordFolderJsBean bean = KeywordFolderJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("keywordFolder", bean);               
            }
            objNode = topNode.findValue("bookmarkFolder");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                BookmarkFolderJsBean bean = BookmarkFolderJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("bookmarkFolder", bean);               
            }
            objNode = topNode.findValue("keywordLink");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                KeywordLinkJsBean bean = KeywordLinkJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("keywordLink", bean);               
            }
            objNode = topNode.findValue("bookmarkLink");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                BookmarkLinkJsBean bean = BookmarkLinkJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("bookmarkLink", bean);               
            }
            objNode = topNode.findValue("speedDial");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                SpeedDialJsBean bean = SpeedDialJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("speedDial", bean);               
            }
            objNode = topNode.findValue("keywordFolderImport");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                KeywordFolderImportJsBean bean = KeywordFolderImportJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("keywordFolderImport", bean);               
            }
            objNode = topNode.findValue("bookmarkFolderImport");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                BookmarkFolderImportJsBean bean = BookmarkFolderImportJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("bookmarkFolderImport", bean);               
            }
            objNode = topNode.findValue("keywordCrowdTally");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                KeywordCrowdTallyJsBean bean = KeywordCrowdTallyJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("keywordCrowdTally", bean);               
            }
            objNode = topNode.findValue("bookmarkCrowdTally");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                BookmarkCrowdTallyJsBean bean = BookmarkCrowdTallyJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("bookmarkCrowdTally", bean);               
            }
            objNode = topNode.findValue("domainInfo");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                DomainInfoJsBean bean = DomainInfoJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("domainInfo", bean);               
            }
            objNode = topNode.findValue("urlRating");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                UrlRatingJsBean bean = UrlRatingJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("urlRating", bean);               
            }
            objNode = topNode.findValue("userRating");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                UserRatingJsBean bean = UserRatingJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("userRating", bean);               
            }
            objNode = topNode.findValue("abuseTag");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                AbuseTagJsBean bean = AbuseTagJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("abuseTag", bean);               
            }
            objNode = topNode.findValue("helpNotice");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                HelpNoticeJsBean bean = HelpNoticeJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("helpNotice", bean);               
            }
            objNode = topNode.findValue("siteLog");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                SiteLogJsBean bean = SiteLogJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("siteLog", bean);               
            }
            objNode = topNode.findValue("serviceInfo");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                ServiceInfoJsBean bean = ServiceInfoJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("serviceInfo", bean);               
            }
            objNode = topNode.findValue("fiveTen");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                FiveTenJsBean bean = FiveTenJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("fiveTen", bean);               
            }
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        
        return jsonObjectMap;
    }

    // Output = jsonStr: { object: objJsonString }
    // Input = Map: object -> jsBean
    public static String generateJsonObjectMap(Map<String, Object> jsonObjectMap)
    {
        if(jsonObjectMap == null) {
            log.warning("Input jsonObjectMap is null.");
            return null;
        }

        StringBuilder sb = new StringBuilder();

        // TBD: Remove the loop.
        for(String key : jsonObjectMap.keySet()) {
            Object jsonObj = jsonObjectMap.get(key);
            sb.append("\"").append(key).append("\":");
            if(jsonObj == null) {
                sb.append("null");  // ????
            } else {
                if(jsonObj instanceof KeyValuePairStructJsBean) {
                    sb.append(((KeyValuePairStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof KeyValueRelationStructJsBean) {
                    sb.append(((KeyValueRelationStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof ExternalServiceApiKeyStructJsBean) {
                    sb.append(((ExternalServiceApiKeyStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof GeoPointStructJsBean) {
                    sb.append(((GeoPointStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof GeoCoordinateStructJsBean) {
                    sb.append(((GeoCoordinateStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof CellLatitudeLongitudeJsBean) {
                    sb.append(((CellLatitudeLongitudeJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof StreetAddressStructJsBean) {
                    sb.append(((StreetAddressStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof WebProfileStructJsBean) {
                    sb.append(((WebProfileStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof FullNameStructJsBean) {
                    sb.append(((FullNameStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof ContactInfoStructJsBean) {
                    sb.append(((ContactInfoStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof ReferrerInfoStructJsBean) {
                    sb.append(((ReferrerInfoStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof GaeAppStructJsBean) {
                    sb.append(((GaeAppStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof GaeUserStructJsBean) {
                    sb.append(((GaeUserStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof PagerStateStructJsBean) {
                    sb.append(((PagerStateStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof AppBrandStructJsBean) {
                    sb.append(((AppBrandStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof ApiConsumerJsBean) {
                    sb.append(((ApiConsumerJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof AppCustomDomainJsBean) {
                    sb.append(((AppCustomDomainJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof SiteCustomDomainJsBean) {
                    sb.append(((SiteCustomDomainJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof UserJsBean) {
                    sb.append(((UserJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof UserUsercodeJsBean) {
                    sb.append(((UserUsercodeJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof HashedPasswordStructJsBean) {
                    sb.append(((HashedPasswordStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof UserPasswordJsBean) {
                    sb.append(((UserPasswordJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof ExternalUserIdStructJsBean) {
                    sb.append(((ExternalUserIdStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof ExternalUserAuthJsBean) {
                    sb.append(((ExternalUserAuthJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof UserAuthStateJsBean) {
                    sb.append(((UserAuthStateJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof UserResourcePermissionJsBean) {
                    sb.append(((UserResourcePermissionJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof UserResourceProhibitionJsBean) {
                    sb.append(((UserResourceProhibitionJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof RolePermissionJsBean) {
                    sb.append(((RolePermissionJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof UserRoleJsBean) {
                    sb.append(((UserRoleJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof AppClientJsBean) {
                    sb.append(((AppClientJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof ClientUserJsBean) {
                    sb.append(((ClientUserJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof UserCustomDomainJsBean) {
                    sb.append(((UserCustomDomainJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof ClientSettingJsBean) {
                    sb.append(((ClientSettingJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof UserSettingJsBean) {
                    sb.append(((UserSettingJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof VisitorSettingJsBean) {
                    sb.append(((VisitorSettingJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof TwitterCardAppInfoJsBean) {
                    sb.append(((TwitterCardAppInfoJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof TwitterCardProductDataJsBean) {
                    sb.append(((TwitterCardProductDataJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof TwitterSummaryCardJsBean) {
                    sb.append(((TwitterSummaryCardJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof TwitterPhotoCardJsBean) {
                    sb.append(((TwitterPhotoCardJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof TwitterGalleryCardJsBean) {
                    sb.append(((TwitterGalleryCardJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof TwitterAppCardJsBean) {
                    sb.append(((TwitterAppCardJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof TwitterPlayerCardJsBean) {
                    sb.append(((TwitterPlayerCardJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof TwitterProductCardJsBean) {
                    sb.append(((TwitterProductCardJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof ShortPassageAttributeJsBean) {
                    sb.append(((ShortPassageAttributeJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof ShortPassageJsBean) {
                    sb.append(((ShortPassageJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof ShortLinkJsBean) {
                    sb.append(((ShortLinkJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof GeoLinkJsBean) {
                    sb.append(((GeoLinkJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof QrCodeJsBean) {
                    sb.append(((QrCodeJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof LinkPassphraseJsBean) {
                    sb.append(((LinkPassphraseJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof LinkMessageJsBean) {
                    sb.append(((LinkMessageJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof LinkAlbumJsBean) {
                    sb.append(((LinkAlbumJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof AlbumShortLinkJsBean) {
                    sb.append(((AlbumShortLinkJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof KeywordFolderJsBean) {
                    sb.append(((KeywordFolderJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof BookmarkFolderJsBean) {
                    sb.append(((BookmarkFolderJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof KeywordLinkJsBean) {
                    sb.append(((KeywordLinkJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof BookmarkLinkJsBean) {
                    sb.append(((BookmarkLinkJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof SpeedDialJsBean) {
                    sb.append(((SpeedDialJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof KeywordFolderImportJsBean) {
                    sb.append(((KeywordFolderImportJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof BookmarkFolderImportJsBean) {
                    sb.append(((BookmarkFolderImportJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof KeywordCrowdTallyJsBean) {
                    sb.append(((KeywordCrowdTallyJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof BookmarkCrowdTallyJsBean) {
                    sb.append(((BookmarkCrowdTallyJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof DomainInfoJsBean) {
                    sb.append(((DomainInfoJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof UrlRatingJsBean) {
                    sb.append(((UrlRatingJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof UserRatingJsBean) {
                    sb.append(((UserRatingJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof AbuseTagJsBean) {
                    sb.append(((AbuseTagJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof HelpNoticeJsBean) {
                    sb.append(((HelpNoticeJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof SiteLogJsBean) {
                    sb.append(((SiteLogJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof ServiceInfoJsBean) {
                    sb.append(((ServiceInfoJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof FiveTenJsBean) {
                    sb.append(((FiveTenJsBean) jsonObj).toJsonString());
                }
            }
            sb.append(",");
        }
        String innerStr = sb.toString();
        if(innerStr.endsWith(",")) {
            innerStr = innerStr.substring(0, innerStr.length() - 1);
        }
        String jsonStr = "{" + innerStr + "}";
        if(log.isLoggable(Level.INFO)) log.info("generateJsonObjectMap(): jsonStr = " + jsonStr);

        return jsonStr;
    }
    
    
}
