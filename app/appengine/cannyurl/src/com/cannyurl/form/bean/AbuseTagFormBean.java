package com.cannyurl.form.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.cannyurl.fe.Validateable;
import com.cannyurl.fe.core.StringEscapeUtil;
import com.cannyurl.fe.bean.AbuseTagJsBean;


// Place holder...
public class AbuseTagFormBean extends AbuseTagJsBean implements Serializable, Cloneable, Validateable  //, AbuseTag
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(AbuseTagFormBean.class.getName());

    // Error map: "field name" -> List<"error message">.
    private Map<String,List<String>> errorMap = new HashMap<String,List<String>>();

    // Ctors.
    public AbuseTagFormBean()
    {
        super();
    }
    public AbuseTagFormBean(String guid)
    {
       super(guid);
    }
    public AbuseTagFormBean(String guid, String shortLink, String shortUrl, String longUrl, String longUrlHash, String user, String ipAddress, Integer tag, String comment, String status, String reviewer, String action, String note, String pastActions, Long reviewedTime, Long actionTime)
    {
        super(guid, shortLink, shortUrl, longUrl, longUrlHash, user, ipAddress, tag, comment, status, reviewer, action, note, pastActions, reviewedTime, actionTime);
    }
    public AbuseTagFormBean(String guid, String shortLink, String shortUrl, String longUrl, String longUrlHash, String user, String ipAddress, Integer tag, String comment, String status, String reviewer, String action, String note, String pastActions, Long reviewedTime, Long actionTime, Long createdTime, Long modifiedTime)
    {
        super(guid, shortLink, shortUrl, longUrl, longUrlHash, user, ipAddress, tag, comment, status, reviewer, action, note, pastActions, reviewedTime, actionTime, createdTime, modifiedTime);
    }
    public AbuseTagFormBean(AbuseTagJsBean bean)
    {
        super(bean);
    }

    public static AbuseTagFormBean fromJsonString(String jsonStr)
    {
        AbuseTagFormBean bean = null;
        try {
            // TBD:
            ObjectMapper mapper = new ObjectMapper();   // can reuse, share globally
            bean = mapper.readValue(jsonStr, AbuseTagFormBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    
    public Map<String,List<String>> getErrorMap()
    {
        return errorMap;
    }

    public boolean hasErrors() 
    {
        // temporary. (An error without error message?)
        if(errorMap.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }
    public boolean hasErrors(String f) 
    {
        // temporary. (An error without error message?)
        List<String> errorList = errorMap.get(f);
        if(errorList == null || errorList.isEmpty()) {
            return false;
        } else {
            return true;
        }        
    }

    public String getLastError(String f) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null || errorList.isEmpty()) {
            return null;
        } else {
            return errorList.get(errorList.size() - 1);
        }
    }
    public List<String> getErrors(String f) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            return new ArrayList<String>();
        } else {
            return errorList;
        }
    }

    public List<String> addError(String f, String error) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            errorList = new ArrayList<String>();
            errorMap.put(f, errorList);
        }
        errorList.add(error);
        return errorList;
    }
    public void setError(String f, String error) 
    {
        List<String> errorList = new ArrayList<String>();
        errorList.add(error);
        errorMap.put(f, errorList);
    }
    public List<String> addErrors(String f, List<String> errors) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            errorList = new ArrayList<String>();
            errorMap.put(f, errorList);
        }
        errorList.addAll(errors);
        return errorList;
    }
    public void setErrors(String f, List<String> errors) 
    {
        errorMap.put(f, errors);
    }

    public void resetErrors()
    {
        errorMap.clear();
    }
    public void resetErrors(String f)
    {
        errorMap.remove(f);
    }


    public boolean validate()
    {
        boolean allOK = true;
       
//        // TBD
//        if(getGuid() == null) {
//            addError("guid", "guid is null");
//            allOK = false;
//        }
//        // TBD
//        if(getShortLink() == null) {
//            addError("shortLink", "shortLink is null");
//            allOK = false;
//        }
//        // TBD
//        if(getShortUrl() == null) {
//            addError("shortUrl", "shortUrl is null");
//            allOK = false;
//        }
//        // TBD
//        if(getLongUrl() == null) {
//            addError("longUrl", "longUrl is null");
//            allOK = false;
//        }
//        // TBD
//        if(getLongUrlHash() == null) {
//            addError("longUrlHash", "longUrlHash is null");
//            allOK = false;
//        }
//        // TBD
//        if(getUser() == null) {
//            addError("user", "user is null");
//            allOK = false;
//        }
//        // TBD
//        if(getIpAddress() == null) {
//            addError("ipAddress", "ipAddress is null");
//            allOK = false;
//        }
//        // TBD
//        if(getTag() == null) {
//            addError("tag", "tag is null");
//            allOK = false;
//        }
//        // TBD
//        if(getComment() == null) {
//            addError("comment", "comment is null");
//            allOK = false;
//        }
//        // TBD
//        if(getStatus() == null) {
//            addError("status", "status is null");
//            allOK = false;
//        }
//        // TBD
//        if(getReviewer() == null) {
//            addError("reviewer", "reviewer is null");
//            allOK = false;
//        }
//        // TBD
//        if(getAction() == null) {
//            addError("action", "action is null");
//            allOK = false;
//        }
//        // TBD
//        if(getNote() == null) {
//            addError("note", "note is null");
//            allOK = false;
//        }
//        // TBD
//        if(getPastActions() == null) {
//            addError("pastActions", "pastActions is null");
//            allOK = false;
//        }
//        // TBD
//        if(getReviewedTime() == null) {
//            addError("reviewedTime", "reviewedTime is null");
//            allOK = false;
//        }
//        // TBD
//        if(getActionTime() == null) {
//            addError("actionTime", "actionTime is null");
//            allOK = false;
//        }
//        // TBD
//        if(getCreatedTime() == null) {
//            addError("createdTime", "createdTime is null");
//            allOK = false;
//        }
//        // TBD
//        if(getModifiedTime() == null) {
//            addError("modifiedTime", "modifiedTime is null");
//            allOK = false;
//        }

        return allOK;
    }


    public String toJsonString()
    {
        String jsonStr = null;
        try {
            // TBD: 
            ObjectMapper mapper = new ObjectMapper(); // can reuse, share globally
            StringWriter writer = new StringWriter();
            mapper.writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer( super.toString() );
        sb.append("error = {");
        for(String f : errorMap.keySet()) {
            List<String> errorList = errorMap.get(f);
            if(errorList != null && !errorList.isEmpty()) {
                sb.append(f).append(": [");
                for(String e : errorList) {
                    sb.append(e).append("; ");
                }
                sb.append("];");
            }
        }
        sb.append("};");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        AbuseTagFormBean cloned = new AbuseTagFormBean((AbuseTagJsBean) super.clone());
        return cloned;
    }

}
