package com.cannyurl.form.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.cannyurl.fe.Validateable;
import com.cannyurl.fe.core.StringEscapeUtil;
import com.cannyurl.fe.bean.BookmarkFolderImportJsBean;


// Place holder...
public class BookmarkFolderImportFormBean extends BookmarkFolderImportJsBean implements Serializable, Cloneable, Validateable  //, BookmarkFolderImport
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(BookmarkFolderImportFormBean.class.getName());

    // Error map: "field name" -> List<"error message">.
    private Map<String,List<String>> errorMap = new HashMap<String,List<String>>();

    // Ctors.
    public BookmarkFolderImportFormBean()
    {
        super();
    }
    public BookmarkFolderImportFormBean(String guid)
    {
       super(guid);
    }
    public BookmarkFolderImportFormBean(String guid, String user, Integer precedence, String importType, String status, String note, String bookmarkFolder, String importedFolder, String importedFolderUser, String importedFolderTitle, String importedFolderPath)
    {
        super(guid, user, precedence, importType, status, note, bookmarkFolder, importedFolder, importedFolderUser, importedFolderTitle, importedFolderPath);
    }
    public BookmarkFolderImportFormBean(String guid, String user, Integer precedence, String importType, String status, String note, String bookmarkFolder, String importedFolder, String importedFolderUser, String importedFolderTitle, String importedFolderPath, Long createdTime, Long modifiedTime)
    {
        super(guid, user, precedence, importType, status, note, bookmarkFolder, importedFolder, importedFolderUser, importedFolderTitle, importedFolderPath, createdTime, modifiedTime);
    }
    public BookmarkFolderImportFormBean(BookmarkFolderImportJsBean bean)
    {
        super(bean);
    }

    public static BookmarkFolderImportFormBean fromJsonString(String jsonStr)
    {
        BookmarkFolderImportFormBean bean = null;
        try {
            // TBD:
            ObjectMapper mapper = new ObjectMapper();   // can reuse, share globally
            bean = mapper.readValue(jsonStr, BookmarkFolderImportFormBean.class);
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    
    public Map<String,List<String>> getErrorMap()
    {
        return errorMap;
    }

    public boolean hasErrors() 
    {
        // temporary. (An error without error message?)
        if(errorMap.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }
    public boolean hasErrors(String f) 
    {
        // temporary. (An error without error message?)
        List<String> errorList = errorMap.get(f);
        if(errorList == null || errorList.isEmpty()) {
            return false;
        } else {
            return true;
        }        
    }

    public String getLastError(String f) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null || errorList.isEmpty()) {
            return null;
        } else {
            return errorList.get(errorList.size() - 1);
        }
    }
    public List<String> getErrors(String f) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            return new ArrayList<String>();
        } else {
            return errorList;
        }
    }

    public List<String> addError(String f, String error) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            errorList = new ArrayList<String>();
            errorMap.put(f, errorList);
        }
        errorList.add(error);
        return errorList;
    }
    public void setError(String f, String error) 
    {
        List<String> errorList = new ArrayList<String>();
        errorList.add(error);
        errorMap.put(f, errorList);
    }
    public List<String> addErrors(String f, List<String> errors) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            errorList = new ArrayList<String>();
            errorMap.put(f, errorList);
        }
        errorList.addAll(errors);
        return errorList;
    }
    public void setErrors(String f, List<String> errors) 
    {
        errorMap.put(f, errors);
    }

    public void resetErrors()
    {
        errorMap.clear();
    }
    public void resetErrors(String f)
    {
        errorMap.remove(f);
    }


    public boolean validate()
    {
        boolean allOK = true;
       
//        // TBD
//        if(getBookmarkFolder() == null) {
//            addError("bookmarkFolder", "bookmarkFolder is null");
//            allOK = false;
//        }
//        // TBD
//        if(getImportedFolder() == null) {
//            addError("importedFolder", "importedFolder is null");
//            allOK = false;
//        }
//        // TBD
//        if(getImportedFolderUser() == null) {
//            addError("importedFolderUser", "importedFolderUser is null");
//            allOK = false;
//        }
//        // TBD
//        if(getImportedFolderTitle() == null) {
//            addError("importedFolderTitle", "importedFolderTitle is null");
//            allOK = false;
//        }
//        // TBD
//        if(getImportedFolderPath() == null) {
//            addError("importedFolderPath", "importedFolderPath is null");
//            allOK = false;
//        }

        return allOK;
    }


    public String toJsonString()
    {
        String jsonStr = null;
        try {
            // TBD: 
            ObjectMapper mapper = new ObjectMapper(); // can reuse, share globally
            StringWriter writer = new StringWriter();
            mapper.writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer( super.toString() );
        sb.append("error = {");
        for(String f : errorMap.keySet()) {
            List<String> errorList = errorMap.get(f);
            if(errorList != null && !errorList.isEmpty()) {
                sb.append(f).append(": [");
                for(String e : errorList) {
                    sb.append(e).append("; ");
                }
                sb.append("];");
            }
        }
        sb.append("};");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        BookmarkFolderImportFormBean cloned = new BookmarkFolderImportFormBean((BookmarkFolderImportJsBean) super.clone());
        return cloned;
    }

}
