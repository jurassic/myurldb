package com.cannyurl.form.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.cannyurl.fe.Validateable;
import com.cannyurl.fe.core.StringEscapeUtil;
import com.cannyurl.fe.bean.KeywordCrowdTallyJsBean;


// Place holder...
public class KeywordCrowdTallyFormBean extends KeywordCrowdTallyJsBean implements Serializable, Cloneable, Validateable  //, KeywordCrowdTally
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(KeywordCrowdTallyFormBean.class.getName());

    // Error map: "field name" -> List<"error message">.
    private Map<String,List<String>> errorMap = new HashMap<String,List<String>>();

    // Ctors.
    public KeywordCrowdTallyFormBean()
    {
        super();
    }
    public KeywordCrowdTallyFormBean(String guid)
    {
       super(guid);
    }
    public KeywordCrowdTallyFormBean(String guid, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, String tallyDate, String status, String note, Long expirationTime, String keywordFolder, String folderPath, String keyword, String queryKey, String scope, Boolean caseSensitive)
    {
        super(guid, user, shortLink, domain, token, longUrl, shortUrl, tallyDate, status, note, expirationTime, keywordFolder, folderPath, keyword, queryKey, scope, caseSensitive);
    }
    public KeywordCrowdTallyFormBean(String guid, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, String tallyDate, String status, String note, Long expirationTime, String keywordFolder, String folderPath, String keyword, String queryKey, String scope, Boolean caseSensitive, Long createdTime, Long modifiedTime)
    {
        super(guid, user, shortLink, domain, token, longUrl, shortUrl, tallyDate, status, note, expirationTime, keywordFolder, folderPath, keyword, queryKey, scope, caseSensitive, createdTime, modifiedTime);
    }
    public KeywordCrowdTallyFormBean(KeywordCrowdTallyJsBean bean)
    {
        super(bean);
    }

    public static KeywordCrowdTallyFormBean fromJsonString(String jsonStr)
    {
        KeywordCrowdTallyFormBean bean = null;
        try {
            // TBD:
            ObjectMapper mapper = new ObjectMapper();   // can reuse, share globally
            bean = mapper.readValue(jsonStr, KeywordCrowdTallyFormBean.class);
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    
    public Map<String,List<String>> getErrorMap()
    {
        return errorMap;
    }

    public boolean hasErrors() 
    {
        // temporary. (An error without error message?)
        if(errorMap.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }
    public boolean hasErrors(String f) 
    {
        // temporary. (An error without error message?)
        List<String> errorList = errorMap.get(f);
        if(errorList == null || errorList.isEmpty()) {
            return false;
        } else {
            return true;
        }        
    }

    public String getLastError(String f) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null || errorList.isEmpty()) {
            return null;
        } else {
            return errorList.get(errorList.size() - 1);
        }
    }
    public List<String> getErrors(String f) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            return new ArrayList<String>();
        } else {
            return errorList;
        }
    }

    public List<String> addError(String f, String error) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            errorList = new ArrayList<String>();
            errorMap.put(f, errorList);
        }
        errorList.add(error);
        return errorList;
    }
    public void setError(String f, String error) 
    {
        List<String> errorList = new ArrayList<String>();
        errorList.add(error);
        errorMap.put(f, errorList);
    }
    public List<String> addErrors(String f, List<String> errors) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            errorList = new ArrayList<String>();
            errorMap.put(f, errorList);
        }
        errorList.addAll(errors);
        return errorList;
    }
    public void setErrors(String f, List<String> errors) 
    {
        errorMap.put(f, errors);
    }

    public void resetErrors()
    {
        errorMap.clear();
    }
    public void resetErrors(String f)
    {
        errorMap.remove(f);
    }


    public boolean validate()
    {
        boolean allOK = true;
       
//        // TBD
//        if(getKeywordFolder() == null) {
//            addError("keywordFolder", "keywordFolder is null");
//            allOK = false;
//        }
//        // TBD
//        if(getFolderPath() == null) {
//            addError("folderPath", "folderPath is null");
//            allOK = false;
//        }
//        // TBD
//        if(getKeyword() == null) {
//            addError("keyword", "keyword is null");
//            allOK = false;
//        }
//        // TBD
//        if(getQueryKey() == null) {
//            addError("queryKey", "queryKey is null");
//            allOK = false;
//        }
//        // TBD
//        if(getScope() == null) {
//            addError("scope", "scope is null");
//            allOK = false;
//        }
//        // TBD
//        if(isCaseSensitive() == null) {
//            addError("caseSensitive", "caseSensitive is null");
//            allOK = false;
//        }

        return allOK;
    }


    public String toJsonString()
    {
        String jsonStr = null;
        try {
            // TBD: 
            ObjectMapper mapper = new ObjectMapper(); // can reuse, share globally
            StringWriter writer = new StringWriter();
            mapper.writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer( super.toString() );
        sb.append("error = {");
        for(String f : errorMap.keySet()) {
            List<String> errorList = errorMap.get(f);
            if(errorList != null && !errorList.isEmpty()) {
                sb.append(f).append(": [");
                for(String e : errorList) {
                    sb.append(e).append("; ");
                }
                sb.append("];");
            }
        }
        sb.append("};");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        KeywordCrowdTallyFormBean cloned = new KeywordCrowdTallyFormBean((KeywordCrowdTallyJsBean) super.clone());
        return cloned;
    }

}
