package com.cannyurl.form.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.cannyurl.fe.Validateable;
import com.cannyurl.fe.core.StringEscapeUtil;
import com.cannyurl.fe.bean.LinkAlbumJsBean;


// Place holder...
@JsonIgnoreProperties(ignoreUnknown = true)
public class LinkAlbumFormBean extends LinkAlbumJsBean implements Serializable, Cloneable, Validateable  //, LinkAlbum
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(LinkAlbumFormBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    // Error map: "field name" -> List<"error message">.
    private Map<String,List<String>> errorMap = new HashMap<String,List<String>>();

    // Ctors.
    public LinkAlbumFormBean()
    {
        super();
    }
    public LinkAlbumFormBean(String guid)
    {
       super(guid);
    }
    public LinkAlbumFormBean(String guid, String appClient, String clientRootDomain, String owner, String name, String summary, String tokenPrefix, String permalink, String shortLink, String shortUrl, String source, String referenceUrl, Boolean autoGenerated, Integer maxLinkCount, String note, String status)
    {
        super(guid, appClient, clientRootDomain, owner, name, summary, tokenPrefix, permalink, shortLink, shortUrl, source, referenceUrl, autoGenerated, maxLinkCount, note, status);
    }
    public LinkAlbumFormBean(String guid, String appClient, String clientRootDomain, String owner, String name, String summary, String tokenPrefix, String permalink, String shortLink, String shortUrl, String source, String referenceUrl, Boolean autoGenerated, Integer maxLinkCount, String note, String status, Long createdTime, Long modifiedTime)
    {
        super(guid, appClient, clientRootDomain, owner, name, summary, tokenPrefix, permalink, shortLink, shortUrl, source, referenceUrl, autoGenerated, maxLinkCount, note, status, createdTime, modifiedTime);
    }
    public LinkAlbumFormBean(LinkAlbumJsBean bean)
    {
        super(bean);
    }

    public static LinkAlbumFormBean fromJsonString(String jsonStr)
    {
        LinkAlbumFormBean bean = null;
        try {
            // TBD:
            bean = getObjectMapper().readValue(jsonStr, LinkAlbumFormBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    
    public Map<String,List<String>> getErrorMap()
    {
        return errorMap;
    }

    public boolean hasErrors() 
    {
        // temporary. (An error without error message?)
        if(errorMap.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }
    public boolean hasErrors(String f) 
    {
        // temporary. (An error without error message?)
        List<String> errorList = errorMap.get(f);
        if(errorList == null || errorList.isEmpty()) {
            return false;
        } else {
            return true;
        }        
    }

    public String getLastError(String f) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null || errorList.isEmpty()) {
            return null;
        } else {
            return errorList.get(errorList.size() - 1);
        }
    }
    public List<String> getErrors(String f) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            return new ArrayList<String>();
        } else {
            return errorList;
        }
    }

    public List<String> addError(String f, String error) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            errorList = new ArrayList<String>();
            errorMap.put(f, errorList);
        }
        errorList.add(error);
        return errorList;
    }
    public void setError(String f, String error) 
    {
        List<String> errorList = new ArrayList<String>();
        errorList.add(error);
        errorMap.put(f, errorList);
    }
    public List<String> addErrors(String f, List<String> errors) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            errorList = new ArrayList<String>();
            errorMap.put(f, errorList);
        }
        errorList.addAll(errors);
        return errorList;
    }
    public void setErrors(String f, List<String> errors) 
    {
        errorMap.put(f, errors);
    }

    public void resetErrors()
    {
        errorMap.clear();
    }
    public void resetErrors(String f)
    {
        errorMap.remove(f);
    }


    public boolean validate()
    {
        boolean allOK = true;
       
//        // TBD
//        if(getGuid() == null) {
//            addError("guid", "guid is null");
//            allOK = false;
//        }
//        // TBD
//        if(getAppClient() == null) {
//            addError("appClient", "appClient is null");
//            allOK = false;
//        }
//        // TBD
//        if(getClientRootDomain() == null) {
//            addError("clientRootDomain", "clientRootDomain is null");
//            allOK = false;
//        }
//        // TBD
//        if(getOwner() == null) {
//            addError("owner", "owner is null");
//            allOK = false;
//        }
//        // TBD
//        if(getName() == null) {
//            addError("name", "name is null");
//            allOK = false;
//        }
//        // TBD
//        if(getSummary() == null) {
//            addError("summary", "summary is null");
//            allOK = false;
//        }
//        // TBD
//        if(getTokenPrefix() == null) {
//            addError("tokenPrefix", "tokenPrefix is null");
//            allOK = false;
//        }
//        // TBD
//        if(getPermalink() == null) {
//            addError("permalink", "permalink is null");
//            allOK = false;
//        }
//        // TBD
//        if(getShortLink() == null) {
//            addError("shortLink", "shortLink is null");
//            allOK = false;
//        }
//        // TBD
//        if(getShortUrl() == null) {
//            addError("shortUrl", "shortUrl is null");
//            allOK = false;
//        }
//        // TBD
//        if(getSource() == null) {
//            addError("source", "source is null");
//            allOK = false;
//        }
//        // TBD
//        if(getReferenceUrl() == null) {
//            addError("referenceUrl", "referenceUrl is null");
//            allOK = false;
//        }
//        // TBD
//        if(isAutoGenerated() == null) {
//            addError("autoGenerated", "autoGenerated is null");
//            allOK = false;
//        }
//        // TBD
//        if(getMaxLinkCount() == null) {
//            addError("maxLinkCount", "maxLinkCount is null");
//            allOK = false;
//        }
//        // TBD
//        if(getNote() == null) {
//            addError("note", "note is null");
//            allOK = false;
//        }
//        // TBD
//        if(getStatus() == null) {
//            addError("status", "status is null");
//            allOK = false;
//        }
//        // TBD
//        if(getCreatedTime() == null) {
//            addError("createdTime", "createdTime is null");
//            allOK = false;
//        }
//        // TBD
//        if(getModifiedTime() == null) {
//            addError("modifiedTime", "modifiedTime is null");
//            allOK = false;
//        }

        return allOK;
    }


    public String toJsonString()
    {
        String jsonStr = null;
        try {
            // TBD: 
            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer( super.toString() );
        sb.append("error = {");
        for(String f : errorMap.keySet()) {
            List<String> errorList = errorMap.get(f);
            if(errorList != null && !errorList.isEmpty()) {
                sb.append(f).append(": [");
                for(String e : errorList) {
                    sb.append(e).append("; ");
                }
                sb.append("];");
            }
        }
        sb.append("};");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        LinkAlbumFormBean cloned = new LinkAlbumFormBean((LinkAlbumJsBean) super.clone());
        return cloned;
    }

}
