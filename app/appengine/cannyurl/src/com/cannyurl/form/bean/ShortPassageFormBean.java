package com.cannyurl.form.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.myurldb.ws.ShortPassageAttribute;
import com.cannyurl.fe.Validateable;
import com.cannyurl.fe.core.StringEscapeUtil;
import com.cannyurl.fe.bean.ShortPassageAttributeJsBean;
import com.cannyurl.fe.bean.ShortPassageJsBean;


// Place holder...
public class ShortPassageFormBean extends ShortPassageJsBean implements Serializable, Cloneable, Validateable  //, ShortPassage
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ShortPassageFormBean.class.getName());

    // Error map: "field name" -> List<"error message">.
    private Map<String,List<String>> errorMap = new HashMap<String,List<String>>();

    // Ctors.
    public ShortPassageFormBean()
    {
        super();
    }
    public ShortPassageFormBean(String guid)
    {
       super(guid);
    }
    public ShortPassageFormBean(String guid, String owner, String longText, String shortText, ShortPassageAttributeJsBean attribute, Boolean readOnly, String status, String note, Long expirationTime)
    {
        super(guid, owner, longText, shortText, attribute, readOnly, status, note, expirationTime);
    }
    public ShortPassageFormBean(String guid, String owner, String longText, String shortText, ShortPassageAttributeJsBean attribute, Boolean readOnly, String status, String note, Long expirationTime, Long createdTime, Long modifiedTime)
    {
        super(guid, owner, longText, shortText, attribute, readOnly, status, note, expirationTime, createdTime, modifiedTime);
    }
    public ShortPassageFormBean(ShortPassageJsBean bean)
    {
        super(bean);
    }

    public static ShortPassageFormBean fromJsonString(String jsonStr)
    {
        ShortPassageFormBean bean = null;
        try {
            // TBD:
            ObjectMapper mapper = new ObjectMapper();   // can reuse, share globally
            bean = mapper.readValue(jsonStr, ShortPassageFormBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    
    public Map<String,List<String>> getErrorMap()
    {
        return errorMap;
    }

    public boolean hasErrors() 
    {
        // temporary. (An error without error message?)
        if(errorMap.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }
    public boolean hasErrors(String f) 
    {
        // temporary. (An error without error message?)
        List<String> errorList = errorMap.get(f);
        if(errorList == null || errorList.isEmpty()) {
            return false;
        } else {
            return true;
        }        
    }

    public String getLastError(String f) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null || errorList.isEmpty()) {
            return null;
        } else {
            return errorList.get(errorList.size() - 1);
        }
    }
    public List<String> getErrors(String f) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            return new ArrayList<String>();
        } else {
            return errorList;
        }
    }

    public List<String> addError(String f, String error) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            errorList = new ArrayList<String>();
            errorMap.put(f, errorList);
        }
        errorList.add(error);
        return errorList;
    }
    public void setError(String f, String error) 
    {
        List<String> errorList = new ArrayList<String>();
        errorList.add(error);
        errorMap.put(f, errorList);
    }
    public List<String> addErrors(String f, List<String> errors) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            errorList = new ArrayList<String>();
            errorMap.put(f, errorList);
        }
        errorList.addAll(errors);
        return errorList;
    }
    public void setErrors(String f, List<String> errors) 
    {
        errorMap.put(f, errors);
    }

    public void resetErrors()
    {
        errorMap.clear();
    }
    public void resetErrors(String f)
    {
        errorMap.remove(f);
    }


    public boolean validate()
    {
        boolean allOK = true;
       
//        // TBD
//        if(getGuid() == null) {
//            addError("guid", "guid is null");
//            allOK = false;
//        }
//        // TBD
//        if(getOwner() == null) {
//            addError("owner", "owner is null");
//            allOK = false;
//        }
//        // TBD
//        if(getLongText() == null) {
//            addError("longText", "longText is null");
//            allOK = false;
//        }
//        // TBD
//        if(getShortText() == null) {
//            addError("shortText", "shortText is null");
//            allOK = false;
//        }
//        // TBD
//        if(getAttribute() == null) {
//            addError("attribute", "attribute is null");
//            allOK = false;
//        } else {
//            ShortPassageAttributeJsBean attribute = getAttribute();
//            // ...
//            // allOK = false;
//        }
//        // TBD
//        if(isReadOnly() == null) {
//            addError("readOnly", "readOnly is null");
//            allOK = false;
//        }
//        // TBD
//        if(getStatus() == null) {
//            addError("status", "status is null");
//            allOK = false;
//        }
//        // TBD
//        if(getNote() == null) {
//            addError("note", "note is null");
//            allOK = false;
//        }
//        // TBD
//        if(getExpirationTime() == null) {
//            addError("expirationTime", "expirationTime is null");
//            allOK = false;
//        }
//        // TBD
//        if(getCreatedTime() == null) {
//            addError("createdTime", "createdTime is null");
//            allOK = false;
//        }
//        // TBD
//        if(getModifiedTime() == null) {
//            addError("modifiedTime", "modifiedTime is null");
//            allOK = false;
//        }

        return allOK;
    }


    public String toJsonString()
    {
        String jsonStr = null;
        try {
            // TBD: 
            ObjectMapper mapper = new ObjectMapper(); // can reuse, share globally
            StringWriter writer = new StringWriter();
            mapper.writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer( super.toString() );
        sb.append("error = {");
        for(String f : errorMap.keySet()) {
            List<String> errorList = errorMap.get(f);
            if(errorList != null && !errorList.isEmpty()) {
                sb.append(f).append(": [");
                for(String e : errorList) {
                    sb.append(e).append("; ");
                }
                sb.append("];");
            }
        }
        sb.append("};");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        ShortPassageFormBean cloned = new ShortPassageFormBean((ShortPassageJsBean) super.clone());
        return cloned;
    }

}
