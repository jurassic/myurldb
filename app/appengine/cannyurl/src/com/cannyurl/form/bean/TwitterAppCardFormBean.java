package com.cannyurl.form.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.cannyurl.fe.Validateable;
import com.cannyurl.fe.core.StringEscapeUtil;
import com.cannyurl.fe.bean.TwitterCardAppInfoJsBean;
import com.cannyurl.fe.bean.TwitterCardProductDataJsBean;
import com.cannyurl.fe.bean.TwitterAppCardJsBean;


// Place holder...
@JsonIgnoreProperties(ignoreUnknown = true)
public class TwitterAppCardFormBean extends TwitterAppCardJsBean implements Serializable, Cloneable, Validateable  //, TwitterAppCard
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(TwitterAppCardFormBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    // Error map: "field name" -> List<"error message">.
    private Map<String,List<String>> errorMap = new HashMap<String,List<String>>();

    // Ctors.
    public TwitterAppCardFormBean()
    {
        super();
    }
    public TwitterAppCardFormBean(String guid)
    {
       super(guid);
    }
    public TwitterAppCardFormBean(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, TwitterCardAppInfoJsBean iphoneApp, TwitterCardAppInfoJsBean ipadApp, TwitterCardAppInfoJsBean googlePlayApp)
    {
        super(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, iphoneApp, ipadApp, googlePlayApp);
    }
    public TwitterAppCardFormBean(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, TwitterCardAppInfoJsBean iphoneApp, TwitterCardAppInfoJsBean ipadApp, TwitterCardAppInfoJsBean googlePlayApp, Long createdTime, Long modifiedTime)
    {
        super(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, iphoneApp, ipadApp, googlePlayApp, createdTime, modifiedTime);
    }
    public TwitterAppCardFormBean(TwitterAppCardJsBean bean)
    {
        super(bean);
    }

    public static TwitterAppCardFormBean fromJsonString(String jsonStr)
    {
        TwitterAppCardFormBean bean = null;
        try {
            // TBD:
            bean = getObjectMapper().readValue(jsonStr, TwitterAppCardFormBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    
    public Map<String,List<String>> getErrorMap()
    {
        return errorMap;
    }

    public boolean hasErrors() 
    {
        // temporary. (An error without error message?)
        if(errorMap.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }
    public boolean hasErrors(String f) 
    {
        // temporary. (An error without error message?)
        List<String> errorList = errorMap.get(f);
        if(errorList == null || errorList.isEmpty()) {
            return false;
        } else {
            return true;
        }        
    }

    public String getLastError(String f) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null || errorList.isEmpty()) {
            return null;
        } else {
            return errorList.get(errorList.size() - 1);
        }
    }
    public List<String> getErrors(String f) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            return new ArrayList<String>();
        } else {
            return errorList;
        }
    }

    public List<String> addError(String f, String error) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            errorList = new ArrayList<String>();
            errorMap.put(f, errorList);
        }
        errorList.add(error);
        return errorList;
    }
    public void setError(String f, String error) 
    {
        List<String> errorList = new ArrayList<String>();
        errorList.add(error);
        errorMap.put(f, errorList);
    }
    public List<String> addErrors(String f, List<String> errors) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            errorList = new ArrayList<String>();
            errorMap.put(f, errorList);
        }
        errorList.addAll(errors);
        return errorList;
    }
    public void setErrors(String f, List<String> errors) 
    {
        errorMap.put(f, errors);
    }

    public void resetErrors()
    {
        errorMap.clear();
    }
    public void resetErrors(String f)
    {
        errorMap.remove(f);
    }


    public boolean validate()
    {
        boolean allOK = true;
       
//        // TBD
//        if(getImage() == null) {
//            addError("image", "image is null");
//            allOK = false;
//        }
//        // TBD
//        if(getImageWidth() == null) {
//            addError("imageWidth", "imageWidth is null");
//            allOK = false;
//        }
//        // TBD
//        if(getImageHeight() == null) {
//            addError("imageHeight", "imageHeight is null");
//            allOK = false;
//        }
//        // TBD
//        if(getIphoneApp() == null) {
//            addError("iphoneApp", "iphoneApp is null");
//            allOK = false;
//        } else {
//            TwitterCardAppInfoJsBean iphoneApp = getIphoneApp();
//            // ...
//            // allOK = false;
//        }
//        // TBD
//        if(getIpadApp() == null) {
//            addError("ipadApp", "ipadApp is null");
//            allOK = false;
//        } else {
//            TwitterCardAppInfoJsBean ipadApp = getIpadApp();
//            // ...
//            // allOK = false;
//        }
//        // TBD
//        if(getGooglePlayApp() == null) {
//            addError("googlePlayApp", "googlePlayApp is null");
//            allOK = false;
//        } else {
//            TwitterCardAppInfoJsBean googlePlayApp = getGooglePlayApp();
//            // ...
//            // allOK = false;
//        }

        return allOK;
    }


    public String toJsonString()
    {
        String jsonStr = null;
        try {
            // TBD: 
            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer( super.toString() );
        sb.append("error = {");
        for(String f : errorMap.keySet()) {
            List<String> errorList = errorMap.get(f);
            if(errorList != null && !errorList.isEmpty()) {
                sb.append(f).append(": [");
                for(String e : errorList) {
                    sb.append(e).append("; ");
                }
                sb.append("];");
            }
        }
        sb.append("};");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        TwitterAppCardFormBean cloned = new TwitterAppCardFormBean((TwitterAppCardJsBean) super.clone());
        return cloned;
    }

}
