package com.cannyurl.form.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.myurldb.ws.HashedPasswordStruct;
import com.cannyurl.fe.Validateable;
import com.cannyurl.fe.core.StringEscapeUtil;
import com.cannyurl.fe.bean.HashedPasswordStructJsBean;
import com.cannyurl.fe.bean.UserPasswordJsBean;


// Place holder...
public class UserPasswordFormBean extends UserPasswordJsBean implements Serializable, Cloneable, Validateable  //, UserPassword
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UserPasswordFormBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    // Error map: "field name" -> List<"error message">.
    private Map<String,List<String>> errorMap = new HashMap<String,List<String>>();

    // Ctors.
    public UserPasswordFormBean()
    {
        super();
    }
    public UserPasswordFormBean(String guid)
    {
       super(guid);
    }
    public UserPasswordFormBean(String guid, String admin, String user, String username, String email, String openId, HashedPasswordStructJsBean password, Boolean resetRequired, String challengeQuestion, String challengeAnswer, String status, Long lastResetTime, Long expirationTime)
    {
        super(guid, admin, user, username, email, openId, password, resetRequired, challengeQuestion, challengeAnswer, status, lastResetTime, expirationTime);
    }
    public UserPasswordFormBean(String guid, String admin, String user, String username, String email, String openId, HashedPasswordStructJsBean password, Boolean resetRequired, String challengeQuestion, String challengeAnswer, String status, Long lastResetTime, Long expirationTime, Long createdTime, Long modifiedTime)
    {
        super(guid, admin, user, username, email, openId, password, resetRequired, challengeQuestion, challengeAnswer, status, lastResetTime, expirationTime, createdTime, modifiedTime);
    }
    public UserPasswordFormBean(UserPasswordJsBean bean)
    {
        super(bean);
    }

    public static UserPasswordFormBean fromJsonString(String jsonStr)
    {
        UserPasswordFormBean bean = null;
        try {
            // TBD:
            bean = getObjectMapper().readValue(jsonStr, UserPasswordFormBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    
    public Map<String,List<String>> getErrorMap()
    {
        return errorMap;
    }

    public boolean hasErrors() 
    {
        // temporary. (An error without error message?)
        if(errorMap.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }
    public boolean hasErrors(String f) 
    {
        // temporary. (An error without error message?)
        List<String> errorList = errorMap.get(f);
        if(errorList == null || errorList.isEmpty()) {
            return false;
        } else {
            return true;
        }        
    }

    public String getLastError(String f) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null || errorList.isEmpty()) {
            return null;
        } else {
            return errorList.get(errorList.size() - 1);
        }
    }
    public List<String> getErrors(String f) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            return new ArrayList<String>();
        } else {
            return errorList;
        }
    }

    public List<String> addError(String f, String error) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            errorList = new ArrayList<String>();
            errorMap.put(f, errorList);
        }
        errorList.add(error);
        return errorList;
    }
    public void setError(String f, String error) 
    {
        List<String> errorList = new ArrayList<String>();
        errorList.add(error);
        errorMap.put(f, errorList);
    }
    public List<String> addErrors(String f, List<String> errors) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            errorList = new ArrayList<String>();
            errorMap.put(f, errorList);
        }
        errorList.addAll(errors);
        return errorList;
    }
    public void setErrors(String f, List<String> errors) 
    {
        errorMap.put(f, errors);
    }

    public void resetErrors()
    {
        errorMap.clear();
    }
    public void resetErrors(String f)
    {
        errorMap.remove(f);
    }


    public boolean validate()
    {
        boolean allOK = true;
       
//        // TBD
//        if(getGuid() == null) {
//            addError("guid", "guid is null");
//            allOK = false;
//        }
//        // TBD
//        if(getAdmin() == null) {
//            addError("admin", "admin is null");
//            allOK = false;
//        }
//        // TBD
//        if(getUser() == null) {
//            addError("user", "user is null");
//            allOK = false;
//        }
//        // TBD
//        if(getUsername() == null) {
//            addError("username", "username is null");
//            allOK = false;
//        }
//        // TBD
//        if(getEmail() == null) {
//            addError("email", "email is null");
//            allOK = false;
//        }
//        // TBD
//        if(getOpenId() == null) {
//            addError("openId", "openId is null");
//            allOK = false;
//        }
//        // TBD
//        if(getPassword() == null) {
//            addError("password", "password is null");
//            allOK = false;
//        } else {
//            HashedPasswordStructJsBean password = getPassword();
//            // ...
//            // allOK = false;
//        }
//        // TBD
//        if(isResetRequired() == null) {
//            addError("resetRequired", "resetRequired is null");
//            allOK = false;
//        }
//        // TBD
//        if(getChallengeQuestion() == null) {
//            addError("challengeQuestion", "challengeQuestion is null");
//            allOK = false;
//        }
//        // TBD
//        if(getChallengeAnswer() == null) {
//            addError("challengeAnswer", "challengeAnswer is null");
//            allOK = false;
//        }
//        // TBD
//        if(getStatus() == null) {
//            addError("status", "status is null");
//            allOK = false;
//        }
//        // TBD
//        if(getLastResetTime() == null) {
//            addError("lastResetTime", "lastResetTime is null");
//            allOK = false;
//        }
//        // TBD
//        if(getExpirationTime() == null) {
//            addError("expirationTime", "expirationTime is null");
//            allOK = false;
//        }
//        // TBD
//        if(getCreatedTime() == null) {
//            addError("createdTime", "createdTime is null");
//            allOK = false;
//        }
//        // TBD
//        if(getModifiedTime() == null) {
//            addError("modifiedTime", "modifiedTime is null");
//            allOK = false;
//        }

        return allOK;
    }


    public String toJsonString()
    {
        String jsonStr = null;
        try {
            // TBD: 
            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer( super.toString() );
        sb.append("error = {");
        for(String f : errorMap.keySet()) {
            List<String> errorList = errorMap.get(f);
            if(errorList != null && !errorList.isEmpty()) {
                sb.append(f).append(": [");
                for(String e : errorList) {
                    sb.append(e).append("; ");
                }
                sb.append("];");
            }
        }
        sb.append("};");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        UserPasswordFormBean cloned = new UserPasswordFormBean((UserPasswordJsBean) super.clone());
        return cloned;
    }

}
