package com.cannyurl.form.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.cannyurl.fe.Validateable;
import com.cannyurl.fe.core.StringEscapeUtil;
import com.cannyurl.fe.bean.UserSettingJsBean;


// Place holder...
public class UserSettingFormBean extends UserSettingJsBean implements Serializable, Cloneable, Validateable  //, UserSetting
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UserSettingFormBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    // Error map: "field name" -> List<"error message">.
    private Map<String,List<String>> errorMap = new HashMap<String,List<String>>();

    // Ctors.
    public UserSettingFormBean()
    {
        super();
    }
    public UserSettingFormBean(String guid)
    {
       super(guid);
    }
    public UserSettingFormBean(String guid, String user, String homePage, String selfBio, String userUrlDomain, String userUrlDomainNormalized, Boolean autoRedirectEnabled, Boolean viewEnabled, Boolean shareEnabled, String defaultDomain, String defaultTokenType, String defaultRedirectType, Long defaultFlashDuration, String defaultAccessType, String defaultViewType, String defaultShareType, String defaultPassphrase, String defaultSignature, Boolean useSignature, String defaultEmblem, Boolean useEmblem, String defaultBackground, Boolean useBackground, String sponsorUrl, String sponsorBanner, String sponsorNote, Boolean useSponsor, String extraParams, Long expirationDuration)
    {
        super(guid, user, homePage, selfBio, userUrlDomain, userUrlDomainNormalized, autoRedirectEnabled, viewEnabled, shareEnabled, defaultDomain, defaultTokenType, defaultRedirectType, defaultFlashDuration, defaultAccessType, defaultViewType, defaultShareType, defaultPassphrase, defaultSignature, useSignature, defaultEmblem, useEmblem, defaultBackground, useBackground, sponsorUrl, sponsorBanner, sponsorNote, useSponsor, extraParams, expirationDuration);
    }
    public UserSettingFormBean(String guid, String user, String homePage, String selfBio, String userUrlDomain, String userUrlDomainNormalized, Boolean autoRedirectEnabled, Boolean viewEnabled, Boolean shareEnabled, String defaultDomain, String defaultTokenType, String defaultRedirectType, Long defaultFlashDuration, String defaultAccessType, String defaultViewType, String defaultShareType, String defaultPassphrase, String defaultSignature, Boolean useSignature, String defaultEmblem, Boolean useEmblem, String defaultBackground, Boolean useBackground, String sponsorUrl, String sponsorBanner, String sponsorNote, Boolean useSponsor, String extraParams, Long expirationDuration, Long createdTime, Long modifiedTime)
    {
        super(guid, user, homePage, selfBio, userUrlDomain, userUrlDomainNormalized, autoRedirectEnabled, viewEnabled, shareEnabled, defaultDomain, defaultTokenType, defaultRedirectType, defaultFlashDuration, defaultAccessType, defaultViewType, defaultShareType, defaultPassphrase, defaultSignature, useSignature, defaultEmblem, useEmblem, defaultBackground, useBackground, sponsorUrl, sponsorBanner, sponsorNote, useSponsor, extraParams, expirationDuration, createdTime, modifiedTime);
    }
    public UserSettingFormBean(UserSettingJsBean bean)
    {
        super(bean);
    }

    public static UserSettingFormBean fromJsonString(String jsonStr)
    {
        UserSettingFormBean bean = null;
        try {
            // TBD:
            bean = getObjectMapper().readValue(jsonStr, UserSettingFormBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    
    public Map<String,List<String>> getErrorMap()
    {
        return errorMap;
    }

    public boolean hasErrors() 
    {
        // temporary. (An error without error message?)
        if(errorMap.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }
    public boolean hasErrors(String f) 
    {
        // temporary. (An error without error message?)
        List<String> errorList = errorMap.get(f);
        if(errorList == null || errorList.isEmpty()) {
            return false;
        } else {
            return true;
        }        
    }

    public String getLastError(String f) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null || errorList.isEmpty()) {
            return null;
        } else {
            return errorList.get(errorList.size() - 1);
        }
    }
    public List<String> getErrors(String f) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            return new ArrayList<String>();
        } else {
            return errorList;
        }
    }

    public List<String> addError(String f, String error) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            errorList = new ArrayList<String>();
            errorMap.put(f, errorList);
        }
        errorList.add(error);
        return errorList;
    }
    public void setError(String f, String error) 
    {
        List<String> errorList = new ArrayList<String>();
        errorList.add(error);
        errorMap.put(f, errorList);
    }
    public List<String> addErrors(String f, List<String> errors) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            errorList = new ArrayList<String>();
            errorMap.put(f, errorList);
        }
        errorList.addAll(errors);
        return errorList;
    }
    public void setErrors(String f, List<String> errors) 
    {
        errorMap.put(f, errors);
    }

    public void resetErrors()
    {
        errorMap.clear();
    }
    public void resetErrors(String f)
    {
        errorMap.remove(f);
    }


    public boolean validate()
    {
        boolean allOK = true;
       
//        // TBD
//        if(getHomePage() == null) {
//            addError("homePage", "homePage is null");
//            allOK = false;
//        }
//        // TBD
//        if(getSelfBio() == null) {
//            addError("selfBio", "selfBio is null");
//            allOK = false;
//        }
//        // TBD
//        if(getUserUrlDomain() == null) {
//            addError("userUrlDomain", "userUrlDomain is null");
//            allOK = false;
//        }
//        // TBD
//        if(getUserUrlDomainNormalized() == null) {
//            addError("userUrlDomainNormalized", "userUrlDomainNormalized is null");
//            allOK = false;
//        }
//        // TBD
//        if(isAutoRedirectEnabled() == null) {
//            addError("autoRedirectEnabled", "autoRedirectEnabled is null");
//            allOK = false;
//        }
//        // TBD
//        if(isViewEnabled() == null) {
//            addError("viewEnabled", "viewEnabled is null");
//            allOK = false;
//        }
//        // TBD
//        if(isShareEnabled() == null) {
//            addError("shareEnabled", "shareEnabled is null");
//            allOK = false;
//        }
//        // TBD
//        if(getDefaultDomain() == null) {
//            addError("defaultDomain", "defaultDomain is null");
//            allOK = false;
//        }
//        // TBD
//        if(getDefaultTokenType() == null) {
//            addError("defaultTokenType", "defaultTokenType is null");
//            allOK = false;
//        }
//        // TBD
//        if(getDefaultRedirectType() == null) {
//            addError("defaultRedirectType", "defaultRedirectType is null");
//            allOK = false;
//        }
//        // TBD
//        if(getDefaultFlashDuration() == null) {
//            addError("defaultFlashDuration", "defaultFlashDuration is null");
//            allOK = false;
//        }
//        // TBD
//        if(getDefaultAccessType() == null) {
//            addError("defaultAccessType", "defaultAccessType is null");
//            allOK = false;
//        }
//        // TBD
//        if(getDefaultViewType() == null) {
//            addError("defaultViewType", "defaultViewType is null");
//            allOK = false;
//        }
//        // TBD
//        if(getDefaultShareType() == null) {
//            addError("defaultShareType", "defaultShareType is null");
//            allOK = false;
//        }
//        // TBD
//        if(getDefaultPassphrase() == null) {
//            addError("defaultPassphrase", "defaultPassphrase is null");
//            allOK = false;
//        }
//        // TBD
//        if(getDefaultSignature() == null) {
//            addError("defaultSignature", "defaultSignature is null");
//            allOK = false;
//        }
//        // TBD
//        if(isUseSignature() == null) {
//            addError("useSignature", "useSignature is null");
//            allOK = false;
//        }
//        // TBD
//        if(getDefaultEmblem() == null) {
//            addError("defaultEmblem", "defaultEmblem is null");
//            allOK = false;
//        }
//        // TBD
//        if(isUseEmblem() == null) {
//            addError("useEmblem", "useEmblem is null");
//            allOK = false;
//        }
//        // TBD
//        if(getDefaultBackground() == null) {
//            addError("defaultBackground", "defaultBackground is null");
//            allOK = false;
//        }
//        // TBD
//        if(isUseBackground() == null) {
//            addError("useBackground", "useBackground is null");
//            allOK = false;
//        }
//        // TBD
//        if(getSponsorUrl() == null) {
//            addError("sponsorUrl", "sponsorUrl is null");
//            allOK = false;
//        }
//        // TBD
//        if(getSponsorBanner() == null) {
//            addError("sponsorBanner", "sponsorBanner is null");
//            allOK = false;
//        }
//        // TBD
//        if(getSponsorNote() == null) {
//            addError("sponsorNote", "sponsorNote is null");
//            allOK = false;
//        }
//        // TBD
//        if(isUseSponsor() == null) {
//            addError("useSponsor", "useSponsor is null");
//            allOK = false;
//        }
//        // TBD
//        if(getExtraParams() == null) {
//            addError("extraParams", "extraParams is null");
//            allOK = false;
//        }
//        // TBD
//        if(getExpirationDuration() == null) {
//            addError("expirationDuration", "expirationDuration is null");
//            allOK = false;
//        }

        return allOK;
    }


    public String toJsonString()
    {
        String jsonStr = null;
        try {
            // TBD: 
            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer( super.toString() );
        sb.append("error = {");
        for(String f : errorMap.keySet()) {
            List<String> errorList = errorMap.get(f);
            if(errorList != null && !errorList.isEmpty()) {
                sb.append(f).append(": [");
                for(String e : errorList) {
                    sb.append(e).append("; ");
                }
                sb.append("];");
            }
        }
        sb.append("};");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        UserSettingFormBean cloned = new UserSettingFormBean((UserSettingJsBean) super.clone());
        return cloned;
    }

}
