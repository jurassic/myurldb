package com.cannyurl.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cannyurl.app.helper.AppClientAppHelper;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.AppClientJsBean;
import com.cannyurl.wa.service.AppClientWebService;
import com.myurldb.ws.AppClient;


// temporary
public class AppClientHelper
{
    private static final Logger log = Logger.getLogger(AppClientHelper.class.getName());

    private AppClientWebService appClientWebService = null;

    private AppClientHelper() 
    {
    }

    // Initialization-on-demand holder.
    private static final class AppClientHelperHolder
    {
        private static final AppClientHelper INSTANCE = new AppClientHelper();
    }

    // Singleton method
    public static AppClientHelper getInstance()
    {
        return AppClientHelperHolder.INSTANCE;
    }

    private AppClientWebService getAppClientWebService()
    {
        if(appClientWebService == null) {
            appClientWebService = new AppClientWebService();
        }
        return appClientWebService;
    }


    public AppClientJsBean getAppClient(String guid)
    {
        AppClientJsBean bean = null;
        AppClient appClient = AppClientAppHelper.getInstance().getAppClient(guid);
        if(appClient != null) {
            bean = AppClientWebService.convertAppClientToJsBean(appClient);
        }
        return bean;
    }


    public String findAppClientKeyByClientId(String clientId)
    {
        String key = AppClientAppHelper.getInstance().findAppClientKeyByClientId(clientId);
        return key;
    }
    public AppClientJsBean findAppClientByClientId(String clientId)
    {
        AppClientJsBean bean = null;
        AppClient appClient = AppClientAppHelper.getInstance().findAppClientByClientId(clientId);
        if(appClient != null) {
            bean = AppClientWebService.convertAppClientToJsBean(appClient);
        }
        return bean;
    }

    public String findAppClientKeyByClientCode(String clientCode) throws WebException
    {
        String key = null;
        String filter = "clientCode=='" + clientCode + "'";
        // String ordering = "createdTime desc";
        String ordering = null;
        List<String> keys = getAppClientWebService().findAppClientKeys(filter, ordering, null, null, null, null, 0L, 2);
        if(keys != null && !keys.isEmpty()) {
            int sz = keys.size();
            if(sz > 1) {
                // something's wrong...
                if(log.isLoggable(Level.SEVERE)) log.severe("More than one appClient found for clientCode = " + clientCode);
                return null;   // Throw exception ???
            }
            key = keys.get(0);
        } else {
            if(log.isLoggable(Level.INFO)) log.info("Failed to get any appClient for clientCode = " + clientCode);
        }
        return key;
    }
//    public String findAppClientKeyByClientCode(String clientCode)
//    {
//        String key = AppClientAppHelper.getInstance().findAppClientKeyByClientId(clientCode);
//        return key;
//    }
    public AppClientJsBean findAppClientByClientCode(String clientCode)
    {
        AppClientJsBean bean = null;
        AppClient appClient = AppClientAppHelper.getInstance().findAppClientByClientCode(clientCode);
        if(appClient != null) {
            bean = AppClientWebService.convertAppClientToJsBean(appClient);
        }
        return bean;
    }

    public String findAppClientKeyByRootDomain(String rootDomain)
    {
        String key = AppClientAppHelper.getInstance().findAppClientKeyByClientId(rootDomain);
        return key;
    }
    public AppClientJsBean findAppClientByRootDomain(String rootDomain)
    {
        AppClientJsBean bean = null;
        AppClient appClient = AppClientAppHelper.getInstance().findAppClientByRootDomain(rootDomain);
        if(appClient != null) {
            bean = AppClientWebService.convertAppClientToJsBean(appClient);
        }
        return bean;
    }
    
    public String findAppClientKeyByAppDomain(String appDomain)
    {
        String key = AppClientAppHelper.getInstance().findAppClientKeyByClientId(appDomain);
        return key;
    }
    public AppClientJsBean findAppClientByAppDomain(String appDomain)
    {
        AppClientJsBean bean = null;
        AppClient appClient = AppClientAppHelper.getInstance().findAppClientByAppDomain(appDomain);
        if(appClient != null) {
            bean = AppClientWebService.convertAppClientToJsBean(appClient);
        }
        return bean;
    }


}
