package com.cannyurl.helper;

import java.util.logging.Logger;

import com.cannyurl.app.util.ConfigUtil;
import com.cannyurl.common.AppBrand;
import com.cannyurl.fe.bean.AppBrandStructJsBean;


// temporary
// Note: Only one "brand" is supported for each app/instance/deployment.
public class BrandingHelper
{
    private static final Logger log = Logger.getLogger(BrandingHelper.class.getName());

    private String mBrand = null;
    private BrandingHelper() 
    {
        mBrand = initBrand();
    }

    // Initialization-on-demand holder.
    private static final class BrandingHelperHolder
    {
        private static final BrandingHelper INSTANCE = new BrandingHelper();
    }

    // Singleton method
    public static BrandingHelper getInstance()
    {
        return BrandingHelperHolder.INSTANCE;
    }

    private String initBrand()
    {
        String brand = ConfigUtil.getApplicationBrand();
        if(!AppBrand.isValidBrand(brand)) {
            brand = AppBrand.getDefaultBrand();
        }
        return brand;
    }

    
    public String getAppBrand()
    { 
        return mBrand;
    }
    
    public String getBrandDisplayName()
    { 
        return AppBrand.getDisplayName(mBrand);
    }

    public String getBrandDescription()
    { 
        return AppBrand.getBrandDescription(mBrand);
    }


    // temporary
    public AppBrandStructJsBean getAppBrandStruct()
    {
        return AppBrand.getBrandStruct(mBrand);
    }

    
    public boolean isBrandCannyURL()
    {
        return AppBrand.BRAND_CANNYURL.equals(mBrand);
    }

    public boolean isBrandFlashURL()
    {
        return AppBrand.BRAND_FLASHURL.equals(mBrand);
    }

    public boolean isBrandShellURL()
    {
        return AppBrand.BRAND_SHELLURL.equals(mBrand);
    }

    public boolean isBrandFemtoURL()
    {
        return AppBrand.BRAND_FEMTOURL.equals(mBrand);
    }

    public boolean isBrandSassyURL()
    {
        return AppBrand.BRAND_SASSYURL.equals(mBrand);
    }

    public boolean isBrandBrandURL()
    {
        return AppBrand.BRAND_BRANDURL.equals(mBrand);
    }

    public boolean isBrandUserURL()
    {
        return AppBrand.BRAND_USERURL.equals(mBrand);
    }

    public boolean isBrandHostedURL()
    {
        return AppBrand.BRAND_HOSTEDURL.equals(mBrand);
    }

    public boolean isBrandQuadURL()
    {
        return AppBrand.BRAND_QUADURL.equals(mBrand);
    }


    public boolean isBrandKeywordNav()
    {
        return AppBrand.BRAND_KEYWORDNAV.equals(mBrand);
    }

    public boolean isBrandSpeedKeyword()
    {
        return AppBrand.BRAND_SPEEDKEYWORD.equals(mBrand);
    }

    public boolean isBrandQuickNavURL()
    {
        return AppBrand.BRAND_QUICKNAVURL.equals(mBrand);
    }

    public boolean isBrandQuickNavBar()
    {
        return AppBrand.BRAND_QUICKNAVBAR.equals(mBrand);
    }


    public boolean isBrandTwrim()
    {
        return AppBrand.BRAND_TWRIM.equals(mBrand);
    }

    public boolean isBrandTweetUserURL()
    {
        return AppBrand.BRAND_TWEETUSERURL.equals(mBrand);
    }

    public boolean isBrandMyPlusURL()
    {
        return AppBrand.BRAND_MYPLUSURL.equals(mBrand);
    }

    public boolean isBrandPeerURL()
    {
        return AppBrand.BRAND_PEERURL.equals(mBrand);
    }

    public boolean isBrandSignedURL()
    {
        return AppBrand.BRAND_SIGNEDURL.equals(mBrand);
    }

    public boolean isBrandTaskURL()
    {
        return AppBrand.BRAND_TASKURL.equals(mBrand);
    }

    public boolean isBrandCubbyURL()
    {
        return AppBrand.BRAND_CUBBYURL.equals(mBrand);
    }

    public boolean isBrandSmallBizURL()
    {
        return AppBrand.BRAND_SMALLBIZURL.equals(mBrand);
    }

    public boolean isBrandEnterpriseURL()
    {
        return AppBrand.BRAND_ENTERPRISEURL.equals(mBrand);
    }

}
