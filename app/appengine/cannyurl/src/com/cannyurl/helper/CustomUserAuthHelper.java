package com.cannyurl.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cannyurl.app.auth.CustomAuthUtil;
import com.cannyurl.common.HashType;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.HashedPasswordStructJsBean;
import com.cannyurl.fe.bean.UserJsBean;
import com.cannyurl.fe.bean.UserPasswordJsBean;
import com.cannyurl.wa.service.UserPasswordWebService;
import com.cannyurl.wa.service.UserWebService;
import com.myurldb.ws.core.GUID;


// Cf. app.auth.CustomAuthHelper....
public class CustomUserAuthHelper
{
    private static final Logger log = Logger.getLogger(CustomUserAuthHelper.class.getName());

    // TBD: These need to match settings in web.xml
    //private static final String AUTH_HANDLER_URLPATH = "/auth/handler";
    // ...

    // "Lazy initialization"
    //private Boolean mUseSocialAuth = null;
    // ...
    private UserWebService userWebService = null;
    private UserPasswordWebService userPasswordWebService = null;
    // ...

    private CustomUserAuthHelper() {}

    // Initialization-on-demand holder.
    private static final class CustomAuthHelperHolder
    {
        private static final CustomUserAuthHelper INSTANCE = new CustomUserAuthHelper();
    }

    // Singleton method
    public static CustomUserAuthHelper getInstance()
    {
        return CustomAuthHelperHolder.INSTANCE;
    }

    private UserWebService getUserService()
    {
        if(userWebService == null) {
            userWebService = new UserWebService();
        }
        return userWebService;
    }
    private UserPasswordWebService getUserPasswordService()
    {
        if(userPasswordWebService == null) {
            userPasswordWebService = new UserPasswordWebService();
        }
        return userPasswordWebService;
    }

    
    
    
    
    public boolean isUsernameAvailable(String username)
    {
        try {
            UserJsBean user = tryFindingUserByUsername(username);
            if(user == null) {
                return true;
            }
        } catch (WebException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Error while trying to find beans for the given username = " + username, e);
        }
        return false;
    }
    public boolean isEmailAvailable(String email)
    {
        try {
            UserJsBean user = tryFindingUserByEmail(email);
            if(user == null) {
                return true;
            }
        } catch (WebException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Error while trying to find beans for the given email = " + email, e);
        }
        return false;
    }
    public boolean isOpenIdAvailable(String openId)
    {
        try {
            UserJsBean user = tryFindingUserByOpenId(openId);
            if(user == null) {
                return true;
            }
        } catch (WebException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Error while trying to find beans for the given openId = " + openId, e);
        }
        return false;
    }
    
    
    
    
    

    public boolean saveUsername(String user, String username)
    {
        boolean suc = false;
        // ....
        return suc;        
    }
    public boolean saveUsername(String user, String username, String email, String openId)
    {
        boolean suc = false;
        // ....
        return suc;        
    }


    public boolean saveUserPasswordJsBean(String user, String passwd)
    {
        boolean suc = false;
        // ....
        return suc;        
    }
    public boolean saveUserPasswordJsBean(String user, String username, String email, String openId, String passwd)
    {
        boolean suc = false;
        // ....
        return suc;        
    }

    
    // Use any "id" - password combination...
    public boolean registerUser(String user, String username, String email, String openId, String passwd)
    {
        boolean suc = false;
        // ....
        return suc;        
    }
    
    // Use any "id" - password combination...
    public boolean authenticateUser(String user, String username, String email, String openId, String passwd)
    {
        boolean suc = false;
        // ....
        return suc;        
    }


    
    // 
    // Can be used with either JsBean or FormBean...
    // TBD:
    private UserPasswordJsBean hashAndSetPassword(UserPasswordJsBean userPassword, String passwd)
    {
        if(userPassword == null) {
            // What to do???
            log.warning("Input userPassword bean is null.");
            return null;
        }

        // temporary
        String hashMethod = null;
        String salt = null; 
        HashedPasswordStructJsBean oldPasswordStruct = userPassword.getPassword();
        // Note:
        // Because we don't do full fetch in the find() methods,
        // the existing HashedPasswordStruct will not be returned....
        // Therefore, HashedPasswordStruct will always be overwritten with new passwd/salt/hash aglo, etc...
        if(oldPasswordStruct != null) {   // This is always false, in the current design (defaultFetch==false)
            // Always try to reuse the existing value, if exits....
            hashMethod = oldPasswordStruct.getAlgorithm();
            salt = oldPasswordStruct.getSalt();
            // TBD: Does this make sense???
            // In any case, in the normal use cases, the plain text field should be null....
//            if(passwd == null) {    // Is an emtpy string a valid password????
//                passwd = oldPasswordStruct.getPlainText();  // ???
//            }
        }

        if(passwd == null) {    // Do we allow an empty password????
            // What to do???
            log.warning("Input passwd is null.");
            return null;
        }

        if(! HashType.isSupportedAlgorithm(hashMethod)) {
            // TBD: Get default hash algo from config???
            hashMethod = HashType.TYPE_SHA256;
        }
        if(salt == null || salt.isEmpty()) {
            salt = CustomAuthUtil.generateRandomSalt();
        }
        
        // New hashed password..
        String hashedPwd = CustomAuthUtil.hashPassword(passwd, salt, hashMethod);

        HashedPasswordStructJsBean passwordStruct = null;
        if(oldPasswordStruct != null) {
            // Reuse???
            passwordStruct = oldPasswordStruct;
        } else {
            passwordStruct = new HashedPasswordStructJsBean();
            passwordStruct.setUuid(GUID.generate());
        }
        // TBD: Plain password should be cleared when saved??? Or, clear it now?????
        // passwordStruct.setPlainText(passwd); 
        passwordStruct.setPlainText(null);        // ????? 
        // ....
        passwordStruct.setHashedText(hashedPwd);
        passwordStruct.setSalt(salt);
        passwordStruct.setAlgorithm(hashMethod);
        
        userPassword.setPassword(passwordStruct);

        userPassword.setLastResetTime(System.currentTimeMillis());
        return userPassword;
    }
    
    
    public UserPasswordJsBean constructUserPasswordJsBean(String user, String username, String email, String openId, String passwd)
    {
        if(passwd == null) {   // Do we allow empty passwd???
            return null;  // ???
        }
        UserPasswordJsBean userPassword = new UserPasswordJsBean();
        userPassword.setUser(user);
        if(username != null) {
            userPassword.setUsername(username);
        }
        if(email != null) {
            userPassword.setEmail(email);
        }
        if(openId != null) {
            userPassword.setOpenId(openId);
        }
        userPassword = hashAndSetPassword(userPassword, passwd);
        try {
            userPassword = (UserPasswordJsBean) getUserPasswordService().constructUserPassword(userPassword);
        } catch (WebException e) {
            log.log(Level.WARNING, "", e);
        }        
        return userPassword;
    }

    
    public UserPasswordJsBean updateUserPasswordJsBean(String user, String passwd)
    {
        UserPasswordJsBean userPassword = findUserPasswordForUser(user);
        if(userPassword == null) {
            // What to do???
            //userPassword = constructUserPasswordJsBean(user, null, null, null, passwd);
            return null;
        } else {
            userPassword = hashAndSetPassword((UserPasswordJsBean) userPassword, passwd);
            try {
                userPassword = getUserPasswordService().refreshUserPassword(userPassword);
            } catch (WebException e) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to update the passwd for user = " + user, e);
                return null;  // ???
            }
        }
        return userPassword;
    }

    public UserPasswordJsBean updateUserPasswordJsBeanByUsername(String username, String passwd)
    {
        UserPasswordJsBean userPassword = findUserPasswordByUsername(username);
        if(userPassword == null) {
            // What to do???
            //userPassword = constructUserPasswordJsBean(user, null, null, null, passwd);
            return null;
        } else {
            userPassword = hashAndSetPassword((UserPasswordJsBean) userPassword, passwd);
            try {
                userPassword = getUserPasswordService().refreshUserPassword(userPassword);
            } catch (WebException e) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to update the passwd for username = " + username, e);
                return null;  // ???
            }
        }
        return userPassword;
    }

    public UserPasswordJsBean updateUserPasswordJsBeanByEmail(String email, String passwd)
    {
        UserPasswordJsBean userPassword = findUserPasswordByEmail(email);
        if(userPassword == null) {
            // What to do???
            //userPassword = constructUserPasswordJsBean(user, null, null, null, passwd);
            return null;
        } else {
            userPassword = hashAndSetPassword((UserPasswordJsBean) userPassword, passwd);
            try {
                userPassword = getUserPasswordService().refreshUserPassword(userPassword);
            } catch (WebException e) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to update the passwd for email = " + email, e);
                return null;  // ???
            }
        }
        return userPassword;
    }

    public UserPasswordJsBean updateUserPasswordJsBeanByOpenId(String openId, String passwd)
    {
        UserPasswordJsBean userPassword = findUserPasswordByOpenId(openId);
        if(userPassword == null) {
            // What to do???
            //userPassword = constructUserPasswordJsBean(user, null, null, null, passwd);
            return null;
        } else {
            userPassword = hashAndSetPassword((UserPasswordJsBean) userPassword, passwd);
            try {
                userPassword = getUserPasswordService().refreshUserPassword(userPassword);
            } catch (WebException e) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to update the passwd for openId = " + openId, e);
                return null;  // ???
            }
        }
        return userPassword;
    }
    


    // TBD:
    // Note that the following find() methods do not do full fetch.
    // However, in order to be able to authenticate the user
    //      (e.g., by comparing the hashed value of the user-provided password and the stored hashedPsssword),
    // we will need to do full fetch....
    // TBD.....


    private UserJsBean tryFindingUserForUser(String userGuid) throws WebException
    {
        UserJsBean user = null;
        List<UserJsBean> beans = null;
        String filter = "guid=='" + userGuid + "'";   // TBD: Check status?
        String ordering = "createdTime desc";
        beans = getUserService().findUsers(filter, ordering, null, null, null, null, null, null);   
        if(beans != null && beans.size() > 0) {
            int size = beans.size();
            user = beans.get(0);                
            if(size > 1) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, size + " beans found for userGuid = " + userGuid);
            }
            // Note:
            // No full fetch...
            // This means that HashedPasswordStruct will not be returned....
            // If we update it, HashedPasswordStruct will always be overwritten with new passwd/salt/hash aglo, etc...
            // ...
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "No beans for given userGuid = " + userGuid);
        }
        return user;
    }
    public UserJsBean findUserForUser(String userGuid)
    {
        UserJsBean user = null;
        try {
            user = tryFindingUserForUser(userGuid);
        } catch (WebException e) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Failed to find beans for given userGuid = " + userGuid, e);
            return null;
        }
        return user;
    }

    private UserJsBean tryFindingUserByUsername(String username) throws WebException
    {
        UserJsBean user = null;
        List<UserJsBean> beans = null;
        String filter = "username=='" + username + "'";   // TBD: Check status?
        String ordering = "createdTime desc";
        beans = getUserService().findUsers(filter, ordering, null, null, null, null, null, null);   
        if(beans != null && beans.size() > 0) {
            int size = beans.size();
            user = beans.get(0);                
            if(size > 1) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, size + " beans found for username = " + username);
            }
            // Note:
            // No full fetch...
            // This means that HashedPasswordStruct will not be returned....
            // If we update it, HashedPasswordStruct will always be overwritten with new passwd/salt/hash aglo, etc...
            // ...
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "No beans for given username = " + username);
        }
        return user;
    }
    public UserJsBean findUserByUsername(String username)
    {
        UserJsBean user = null;
        try {
            user = tryFindingUserByUsername(username);
        } catch (WebException e) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Failed to find beans for given username = " + username, e);
        }
        return user;
    }

    private UserJsBean tryFindingUserByEmail(String email) throws WebException
    {
        UserJsBean user = null;
        List<UserJsBean> beans = null;
        String filter = "email=='" + email + "'";   // TBD: Check status?
        String ordering = "createdTime desc";
        beans = getUserService().findUsers(filter, ordering, null, null, null, null, null, null);   
        if(beans != null && beans.size() > 0) {
            int size = beans.size();
            user = beans.get(0);                
            if(size > 1) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, size + " beans found for email = " + email);
            }
            // Note:
            // No full fetch...
            // This means that HashedPasswordStruct will not be returned....
            // If we update it, HashedPasswordStruct will always be overwritten with new passwd/salt/hash aglo, etc...
            // ...
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "No beans for given email = " + email);
        }
        return user;
    }
    public UserJsBean findUserByEmail(String email)
    {
        UserJsBean user = null;
        try {
            user = tryFindingUserByEmail(email);
        } catch (WebException e) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Failed to find beans for given email = " + email, e);
        }
        return user;
    }

    private UserJsBean tryFindingUserByOpenId(String openId) throws WebException
    {
        UserJsBean user = null;
        List<UserJsBean> beans = null;
        String filter = "openId=='" + openId + "'";   // TBD: Check status?
        String ordering = "createdTime desc";
        beans = getUserService().findUsers(filter, ordering, null, null, null, null, null, null);   
        if(beans != null && beans.size() > 0) {
            int size = beans.size();
            user = beans.get(0);                
            if(size > 1) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, size + " beans found for openId = " + openId);
            }
            // Note:
            // No full fetch...
            // This means that HashedPasswordStruct will not be returned....
            // If we update it, HashedPasswordStruct will always be overwritten with new passwd/salt/hash aglo, etc...
            // ...
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "No beans for given openId = " + openId);
        }
        return user;
    }
    public UserJsBean findUserByOpenId(String openId)
    {
        UserJsBean user = null;
        try {
            user = tryFindingUserByOpenId(openId);
        } catch (WebException e) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Failed to find beans for given openId = " + openId, e);
        }
        return user;
    }


    private UserPasswordJsBean tryFindingUserPasswordForUser(String userGuid) throws WebException
    {
        UserPasswordJsBean userPassword = null;
        List<UserPasswordJsBean> beans = null;
        String filter = "user=='" + userGuid + "'";   // TBD: Check status?
        String ordering = "createdTime desc";
        beans = getUserPasswordService().findUserPasswords(filter, ordering, null, null, null, null, null, null);   
        if(beans != null && beans.size() > 0) {
            int size = beans.size();
            userPassword = beans.get(0);                
            if(size > 1) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, size + " beans found for userGuid = " + userGuid);
            }
            // Note:
            // No full fetch...
            // This means that HashedPasswordStruct will not be returned....
            // If we update it, HashedPasswordStruct will always be overwritten with new passwd/salt/hash aglo, etc...
            // ...
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "No beans for given userGuid = " + userGuid);
        }
        return userPassword;
    }
    public UserPasswordJsBean findUserPasswordForUser(String userGuid)
    {
        UserPasswordJsBean userPassword = null;
        try {
            userPassword = tryFindingUserPasswordForUser(userGuid);
        } catch (WebException e) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Failed to find beans for given userGuid = " + userGuid, e);
        }
        return userPassword;
    }

    private UserPasswordJsBean tryFindingUserPasswordByUsername(String username) throws WebException
    {
        UserPasswordJsBean userPassword = null;
        List<UserPasswordJsBean> beans = null;
        String filter = "username=='" + username + "'";   // TBD: Check status?
        String ordering = "createdTime desc";
        beans = getUserPasswordService().findUserPasswords(filter, ordering, null, null, null, null, null, null);   
        if(beans != null && beans.size() > 0) {
            int size = beans.size();
            userPassword = beans.get(0);                
            if(size > 1) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, size + " beans found for username = " + username);
            }
            // Note:
            // No full fetch...
            // This means that HashedPasswordStruct will not be returned....
            // If we update it, HashedPasswordStruct will always be overwritten with new passwd/salt/hash aglo, etc...
            // ...
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "No beans for given username = " + username);
        }
        return userPassword;
    }
    public UserPasswordJsBean findUserPasswordByUsername(String username)
    {
        UserPasswordJsBean userPassword = null;
        try {
            userPassword = tryFindingUserPasswordByUsername(username);
        } catch (WebException e) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Failed to find beans for given username = " + username, e);
        }
        return userPassword;
    }

    private UserPasswordJsBean tryFindingUserPasswordByEmail(String email) throws WebException
    {
        UserPasswordJsBean userPassword = null;
        List<UserPasswordJsBean> beans = null;
        String filter = "email=='" + email + "'";   // TBD: Check status?
        String ordering = "createdTime desc";
        beans = getUserPasswordService().findUserPasswords(filter, ordering, null, null, null, null, null, null);   
        if(beans != null && beans.size() > 0) {
            int size = beans.size();
            userPassword = beans.get(0);                
            if(size > 1) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, size + " beans found for email = " + email);
            }
            // Note:
            // No full fetch...
            // This means that HashedPasswordStruct will not be returned....
            // If we update it, HashedPasswordStruct will always be overwritten with new passwd/salt/hash aglo, etc...
            // ...
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "No beans for given email = " + email);
        }
        return userPassword;
    }
    public UserPasswordJsBean findUserPasswordByEmail(String email)
    {
        UserPasswordJsBean userPassword = null;
        try {
            userPassword = tryFindingUserPasswordByEmail(email);
        } catch (WebException e) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Failed to find beans for given email = " + email, e);
        }
        return userPassword;
    }

    private UserPasswordJsBean tryFindingUserPasswordByOpenId(String openId) throws WebException
    {
        UserPasswordJsBean userPassword = null;
        List<UserPasswordJsBean> beans = null;
        String filter = "openId=='" + openId + "'";   // TBD: Check status?
        String ordering = "createdTime desc";
        beans = getUserPasswordService().findUserPasswords(filter, ordering, null, null, null, null, null, null);   
        if(beans != null && beans.size() > 0) {
            int size = beans.size();
            userPassword = beans.get(0);                
            if(size > 1) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, size + " beans found for openId = " + openId);
            }
            // Note:
            // No full fetch...
            // This means that HashedPasswordStruct will not be returned....
            // If we update it, HashedPasswordStruct will always be overwritten with new passwd/salt/hash aglo, etc...
            // ...
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "No beans for given openId = " + openId);
        }
        return userPassword;
    }
    public UserPasswordJsBean findUserPasswordByOpenId(String openId)
    {
        UserPasswordJsBean userPassword = null;
        try {
            userPassword = tryFindingUserPasswordByOpenId(openId);
        } catch (WebException e) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Failed to find beans for given openId = " + openId, e);
        }
        return userPassword;
    }

    
}
