package com.cannyurl.helper;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.cannyurl.app.util.DomainUtil;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.UserJsBean;
import com.cannyurl.wa.service.UserWebService;


// temporary
public class DomainCheckerHelper
{
    private static final Logger log = Logger.getLogger(DomainCheckerHelper.class.getName());

    // Lazy initialization...
    private UserWebService userWebService = null;

    private DomainCheckerHelper() 
    {
    }

    // Initialization-on-demand holder.
    private static final class DomainCheckerHelperHolder
    {
        private static final DomainCheckerHelper INSTANCE = new DomainCheckerHelper();
    }

    // Singleton method
    public static DomainCheckerHelper getInstance()
    {
        return DomainCheckerHelperHolder.INSTANCE;
    }

    
    private UserWebService getUserService()
    {
        if(userWebService == null) {
            userWebService = new UserWebService();
        }
        return userWebService;
    }
    
    

    // Note that
    // in the way we implement this,
    //  it's generally better for a client (e.g., javascript) to call the user service to get username/usercode
    //  before getting getDomainForUser()...
    // Otherwise, we may end up having to call user service twice
    //  (Note: getDomainForUser() returns domain, but not username/usercode....)

    
    // TBD:
    // Note the alternating/seemingly inconsistent checkUserDB value when calling the overridden methods.
    // Based on the user-provided arguments (assuming they are not null/empty),
    //    these flag values seem most natural....
    public String getDomainForUser(String domainType)
    {
        // String domain = getDomainForUser(domainType, null);
        String domain = getDomainForUser(domainType, null, null, null, false);
        return domain;
    }
    public String getDomainForUser(String domainType, String user)
    {
        // String domain = getDomainForUser(domainType, user, null, null);
        String domain = getDomainForUser(domainType, user, null, null, true);
        return domain;
    }
    public String getDomainForUser(String domainType, String user, String username, String usercode)
    {
        // String domain = getDomainForUser(domainType, user, username, usercode, true);
        String domain = getDomainForUser(domainType, user, username, usercode, false);
        return domain;
    }
    public String getDomainForUser(String domainType, String user, String username, String usercode, boolean checkUserDB)
    {
        if(checkUserDB == true) {            
            // This should be done only if username/usercode is needed in generating domain based on domainType....
            // For now, the caller should use checkUserDB == false, if this checking is not necessary...
            if((username == null || username.isEmpty()) || (usercode == null || usercode.isEmpty()) ) {
                if(user != null && !user.isEmpty()) {
                    UserJsBean userBean = null;
                    try {
                        userBean = getUserService().getUser(user);
                    } catch (WebException e) {
                        log.log(Level.WARNING, "Failed to find a user. user.guid = " + user, e);
                    }
                    if(userBean != null) {
                        if(username == null || username.isEmpty()) {
                            String usernameFromDB = userBean.getUsername();
                            if(usernameFromDB != null && !usernameFromDB.isEmpty()) {
                                username = usernameFromDB;
                                if(log.isLoggable(Level.FINE)) log.fine("Username set to " + username + " for user = " + user);                        
                            } else {
                                // ignore
                                if(log.isLoggable(Level.INFO)) log.info("Username not found in the user object: user = " + user);                        
                            }
                        }
                        // This is really necessary only when domainType.equals(DomainType.TYPE_USERCODE)...
                        // But, now that we already have userBean, why not?
                        if(usercode == null || usercode.isEmpty()) {
                            String usercodeFromDB = userBean.getUsercode();
                            if(usercodeFromDB != null && !usercodeFromDB.isEmpty()) {
                                usercode = usercodeFromDB;
                                if(log.isLoggable(Level.FINE)) log.fine("Usercode set to " + usercode + " for user = " + user);                        
                            } else {
                                // ignore...
                                if(log.isLoggable(Level.INFO)) log.info("Usercode not found in the user object: user = " + user);                        
                            }
                        }
                    } else {
                        // Can this happen???
                        // log.warning("User not found for user = " + user);
                        if(log.isLoggable(Level.INFO)) log.info("Usercode/username cannot be retrieved because user object was not found or there was an error. user = " + user);                        
                    }
                } else {
                    // Ignore...
                    log.info("Usercode/username cannot be retrieved because user argument is not null/empty.");
                }
            } else {
                // Nothing to do...
                // We do NOT "double check" user provided input username/usercode....
            }
            
        } else {
            // Ignore
        }
        String domain = DomainUtil.getDomain(domainType, user, username, usercode);
        return domain;
    }
    // etc....
    
    
}
