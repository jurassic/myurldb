package com.cannyurl.helper;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cannyurl.fe.bean.HelpNoticeJsBean;


public class HelpNoticeHelper
{
    private static final Logger log = Logger.getLogger(HelpNoticeHelper.class.getName());
    
    // Help file location
    private String helpDir = null;
    
    private HelpNoticeHelper() 
    {
        // temporary
        helpDir = "help-notices";    // Under "/war"
    }

    // Initialization-on-demand holder.
    private static final class HelpNoticeHelperHolder
    {
        private static final HelpNoticeHelper INSTANCE = new HelpNoticeHelper();
    }

    // Singleton method
    public static HelpNoticeHelper getInstance()
    {
        return HelpNoticeHelperHolder.INSTANCE;
    }
    
    
    // TBD:
    public List<HelpNoticeJsBean> getAllHelpNotices()
    {
        return null;
    }

    
    // Cache
    // uuid -> bean...
    // TBD: Just use memcache ????
    private Map<String, HelpNoticeJsBean> helpNoticeMap = new HashMap<String, HelpNoticeJsBean>();
    // ..

    public HelpNoticeJsBean getHelpNotice(String uuid)
    {
        if(helpNoticeMap.containsKey(uuid)) {
            return helpNoticeMap.get(uuid);
        }
        
        HelpNoticeJsBean helpNotice = readHelpNotice(uuid);
        if(helpNotice != null) {
            helpNoticeMap.put(uuid, helpNotice);
        }
        
        
        // TBD
        return helpNotice;
    }

    
    // Temporary
    private static final String FIELD_UUID = "UUID";
    private static final String FIELD_TITLE = "TITLE";
    private static final String FIELD_FORMAT = "FORMAT";
    private static final String FIELD_CONTENT = "CONTENT";
    private static final String MARKER_CONTENT_END  = "::END_CONTENT::";
    // etc...

    
    private HelpNoticeJsBean readHelpNotice(String uuid)
    {
        if(log.isLoggable(Level.FINE)) log.fine("readHelpNotice() called with uuid = " + uuid);

        HelpNoticeJsBean helpNotice = null;

        // temporary
        String filename = helpDir + "/" + uuid + ".txt";   // ???
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));
            
            String title = null;
            String format = null;
            String content = null;
            
            String line = null;
            boolean inContent = false;
            StringBuilder contentBuilder = new StringBuilder();
            while((line = reader.readLine()) != null) {
                // Note that if the content field is the last field,
                // the marker is not needed...
                if((inContent == true) && line.trim().equals(MARKER_CONTENT_END)) {
                    inContent = false;
                    continue;
                }
                if(inContent == true) {
                    contentBuilder.append(line).append("\n");   // ??? new line????
                    continue;
                }
                line = line.trim();
                if(line.startsWith("#")) {   // Comment line...
                    continue;
                }
                if(line.isEmpty()) {
                    // Skip empty lines.
                    continue;
                }
                
                String[] cols = line.split("\\s*:\\s*", 2);
                if(cols == null || cols.length == 0) {
                    continue;
                } else if(cols.length == 1) {
                    if(cols[0].equals(FIELD_CONTENT)) {
                        inContent = true;
                    } else {
                        // ignore
                    }
                } else {
                    if(cols[0].equals(FIELD_UUID)) {
                        // TBD:
                        // verify cols[1] == uuid...
                    } else if(cols[0].equals(FIELD_TITLE)) {
                        title = cols[1].trim();
                    } else if(cols[0].equals(FIELD_FORMAT)) {
                        format = cols[1].trim();   // Validation ???
                    } else if(cols[0].equals(FIELD_CONTENT)) {
                        inContent = true;
                    } else {
                        // ????
                    }
                }
            }
            content = contentBuilder.toString();

            helpNotice = new HelpNoticeJsBean();
            helpNotice.setUuid(uuid);
            if(title != null) {
                helpNotice.setTitle(title);
            }
            if(format == null || format.isEmpty()) {   // TBD: isValidFormat() ???
                format = "text";   // temporary
            }
            helpNotice.setFormat(format);
            helpNotice.setContent(content);
            // ...            
            
        } catch (FileNotFoundException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "File not found error: filename = " + filename, e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Error", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Unknown error", e);
        }

        if(log.isLoggable(Level.FINE)) {
            if(helpNotice != null) {
                log.fine("helpNotice = " + helpNotice);
            }
        }
        
        return helpNotice;
    }
    
}
