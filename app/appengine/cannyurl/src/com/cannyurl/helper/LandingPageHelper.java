package com.cannyurl.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.cannyurl.af.util.PathInfoUtil;


public class LandingPageHelper
{
    private static final Logger log = Logger.getLogger(LandingPageHelper.class.getName());
    
    // This should match the URL...
    // URL pattern: .../herounit/r1c1/r1c2/r2c1/.... ?k1=v1&k2=v2...
    // (Currently, query string is being ignored...)
//    private static final String KEY_HEROUNIT = "h";   // -> HeroUnit code is a "command" in the path. Cf. parsePathInfo() implementation below.
//    private static final String KEY_Row1Col1 = "a";
//    private static final String KEY_Row1Col2 = "b";
//    private static final String KEY_Row2Col1 = "c";
//    private static final String KEY_Row2Col2 = "d";
//    private static final String KEY_Row3Col1 = "e";
//    private static final String KEY_Row3Col2 = "f";
    
    // temporary
    private static final int MAX_COUNT_PATHSEGMENTS = 10;    // Max of 10 path segments/codes...
    

    // pathInfo -> map of { pathKey -> pathVal }
    Map<String, List<String>> mPathInfoMap;
    private LandingPageHelper() 
    {
       mPathInfoMap = new HashMap<String, List<String>>();
    }

    // Initialization-on-demand holder.
    private static final class LandingPageHelperHolder
    {
        private static final LandingPageHelper INSTANCE = new LandingPageHelper();
    }

    // Singleton method
    public static LandingPageHelper getInstance()
    {
        return LandingPageHelperHolder.INSTANCE;
    }

    
    private void parsePathInfo(String pathInfo)
    {
    	if(mPathInfoMap.containsKey(pathInfo)) {
    		// nothing to do.
    	} else {
    		List<String> list = null;
    		Map<String, String> map = PathInfoUtil.parsePathInfo(pathInfo, MAX_COUNT_PATHSEGMENTS); 
    		if(map != null) {
    			list = new ArrayList<String>();
    			for(String k : map.keySet()) {   // Note that PathInfoUtil.parsePathInfo() returns the "ordered" map...
    				list.add(k);
    			}
    		}
    		mPathInfoMap.put(pathInfo, list);    		
    	}
    }

    public int getRowCount(String pathInfo)
    {
    	parsePathInfo(pathInfo);
    	List<String> list = mPathInfoMap.get(pathInfo);
    	if(list == null || list.isEmpty()) {
    		return 0;
    	} else {
    		return (int) (list.size() / 2);
    	}
    }

    public String getCodeForHeroUnit(String pathInfo)
    {
    	parsePathInfo(pathInfo);
    	List<String> list = mPathInfoMap.get(pathInfo);
    	if(list == null || list.isEmpty()) {
    		return null;
    	} else {
    		return list.get(0);
    	}
    }

    // Cell index starts from 1.
    // index==0 -> hero unit.
    public String getCodeForCell(String pathInfo, int index)
    {
    	parsePathInfo(pathInfo);
    	List<String> list = mPathInfoMap.get(pathInfo);
    	if(list == null || list.isEmpty()) {
    		return null;
    	} else {
    		int size = list.size();
    		if(size <= index) {
    			return null;
    		} else {
    			return list.get(index);
    		}
    	}
    }

}
