package com.cannyurl.helper;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;

import com.cannyurl.af.util.CacheHelper;
import com.cannyurl.fe.WebException;
import com.cannyurl.wa.service.UserWebService;
import com.pagesynopsis.web.service.PageFetchProxyWebService;
import com.pagesynopsis.web.service.PageInfoProxyWebService;


public class PageSynopsisHelper
{
    private static final Logger log = Logger.getLogger(PageSynopsisHelper.class.getName());
    
    // temporary
    private static final int MAX_MEMO_COUNT = 5000;   // Sitemap.xml max count...
    private static final int DEFAULT_MAX_MEMO_COUNT = 2500;
    private static final int DEFAULT_MAX_REDIRECTS = 5;   // ???
    // temporary

    private UserWebService userWebService = null;
    private PageInfoProxyWebService pageInfoWebService = null;
    private PageFetchProxyWebService pageFetchProxyWebService = null;
    // ...

    // Cache service
    private Cache mCache = null;
    private void initCache()
    {
        // temporary
        // mCache = CacheHelper.getDayLongInstance().getCache();
        mCache = CacheHelper.getTwoDayInstance().getCache();
    }


    private PageSynopsisHelper()
    {
        initCache();
    }

    // Initialization-on-demand holder.
    private static final class PageSynopsisHelperHolder
    {
        private static final PageSynopsisHelper INSTANCE = new PageSynopsisHelper();
    }

    // Singleton method
    public static PageSynopsisHelper getInstance()
    {
        return PageSynopsisHelperHolder.INSTANCE;
    }
    
    
    private UserWebService getUserWebService()
    {
        if(userWebService == null) {
            userWebService = new UserWebService();
        }
        return userWebService;
    }
    private PageInfoProxyWebService getPageInfoProxyWebService()
    {
        if(pageInfoWebService == null) {
            pageInfoWebService = new PageInfoProxyWebService();
        }
        return pageInfoWebService;
    }
    private PageFetchProxyWebService getPageFetchProxyWebService()
    {
        if(pageFetchProxyWebService == null) {
            pageFetchProxyWebService = new PageFetchProxyWebService();
        }
        return pageFetchProxyWebService;
    }


    // Hmmm...
    // Name conflict.... PageInfoJsBean...
    @Deprecated
    public final com.pagesynopsis.fe.bean.PageInfoJsBean findPageInfoByTargetUrl(String targetUrl) // throws WebException
    {
        return findPageInfoByTargetUrl(targetUrl, true);
    }
    @Deprecated
    public final com.pagesynopsis.fe.bean.PageInfoJsBean findPageInfoByTargetUrl(String targetUrl, Boolean fetch) // throws WebException
    {
        com.pagesynopsis.fe.bean.PageInfoJsBean bean = null;
        try {
            if(mCache != null) {
                bean = (com.pagesynopsis.fe.bean.PageInfoJsBean) mCache.get(_key_pageInfoJsBean(targetUrl));
//                if(bean != null) {
//                    destinationUrl = bean.getDestinationUrl();
//                    pageDescription = bean.getPageSummary();
//                    pageAuthor = bean.getPageAuthor();
//                }
            }
            if(bean != null) {
                log.info("Found the PageInfoJsBean in the cache for targetUrl = " + targetUrl);
            } else {
                bean = getPageInfoProxyWebService().findPageInfoByTargetUrl(targetUrl, fetch);
                if(bean != null) {
                    if(mCache != null) {
                        mCache.put(_key_pageInfoJsBean(targetUrl), bean);
                    }
                }
            }
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve com.pagesynopsis.fe.bean.PageInfoJsBean", ex);
            // ignore
            //throw new WebException(ex);
        }
        return bean;
    }

    public final com.pagesynopsis.fe.bean.PageFetchJsBean findPageFetchByTargetUrl(String targetUrl) // throws WebException
    {
        return findPageFetchByTargetUrl(targetUrl, true);
    }
    public final com.pagesynopsis.fe.bean.PageFetchJsBean findPageFetchByTargetUrl(String targetUrl, Boolean fetch) // throws WebException
    {
        return findPageFetchByTargetUrl(targetUrl, fetch, null);        
    }
    public final com.pagesynopsis.fe.bean.PageFetchJsBean findPageFetchByTargetUrl(String targetUrl, Boolean fetch, Integer maxRedirects) // throws WebException
    {
        if(maxRedirects == null || maxRedirects <= 0) {
            maxRedirects = DEFAULT_MAX_REDIRECTS;
        }
        com.pagesynopsis.fe.bean.PageFetchJsBean bean = null;
        try {
            if(mCache != null) {
                bean = (com.pagesynopsis.fe.bean.PageFetchJsBean) mCache.get(_key_pageFetchJsBean(targetUrl, maxRedirects));
            }
            if(bean != null) {
                log.info("Found the PageFetchJsBean in the cache for targetUrl = " + targetUrl);
            } else {
                bean = getPageFetchProxyWebService().findPageFetchByTargetUrl(targetUrl, fetch, maxRedirects);
                if(bean != null) {
                    if(mCache != null) {
                        mCache.put(_key_pageFetchJsBean(targetUrl, maxRedirects), bean);
                    }
                }
            }
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve com.pagesynopsis.fe.bean.PageFetchJsBean", ex);
            // ignore
            //throw new WebException(ex);
        }
        return bean;
    }


    // targetUrl cannot be null or empty...
    private static String _key_pageInfoJsBean(String targetUrl)
    {
        String prefix = "PageSynopsisHelper-PageInfoJsBean-";
        return prefix + ((targetUrl != null) ? targetUrl : "");  // ???? 
    }
    // targetUrl cannot be null or empty...
    private static String _key_pageFetchJsBean(String targetUrl, int redirects)
    {
        String prefix = "PageSynopsisHelper-PageFetchJsBean-" + redirects + "-";
        return prefix + ((targetUrl != null) ? targetUrl : "");  // ???? 
    }

}
