package com.cannyurl.helper;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.cannyurl.app.util.ConfigUtil;
import com.cannyurl.common.PagerMode;
import com.cannyurl.fe.bean.PagerStateStructJsBean;


public class PagerHelper
{
    private static final Logger log = Logger.getLogger(PagerHelper.class.getName());

    
    //
    private Integer mDefaultPageSize = null;
    private Integer mHeadlinesPageSize = null;
    private Integer mSummariesPageSize = null;
    private Integer mSummaryListPageSize = null;
    private Integer mLongUrlListPageSize = null;
    private Integer mShortUrlListPageSize = null;
    private Integer mShortUrlStreamPageSize = null;
    private Integer mFeaturedListPageSize = null;
    private Integer mFeaturedGridPageSize = null;
    private String mOrderingPrimary = null;
    private String mHeadlinesOrderingPrimary = null;
    private String mSummariesOrderingPrimary = null;
    private String mSummaryListOrderingPrimary = null;
    private String mLongUrlListOrderingPrimary = null;
    private String mShortUrlListOrderingPrimary = null;
    private String mShortUrlStreamOrderingPrimary = null;
    private String mFeaturedListOrderingPrimary = null;
    private String mFeaturedGridOrderingPrimary = null;

    
    private PagerHelper() 
    {
        init();
    }

    // Initialization-on-demand holder.
    private static final class PagerHelperHolder
    {
        private static final PagerHelper INSTANCE = new PagerHelper();
    }

    // Singleton method
    public static PagerHelper getInstance()
    {
        return PagerHelperHolder.INSTANCE;
    }

    private void init()
    {
        // TBD: Validation??? e.g., page size > 0.
        mDefaultPageSize = ConfigUtil.getPagerPageSize();
        if(mDefaultPageSize == null || mDefaultPageSize <= 0) {
            mDefaultPageSize = ConfigUtil.getDefaultPagerPageSize();  // ???
        }
        mHeadlinesPageSize = ConfigUtil.getPagerHeadlinesPageSize();
        if(mHeadlinesPageSize == null || mHeadlinesPageSize <= 0) {
            mHeadlinesPageSize = mDefaultPageSize;  // ???
        }
        mSummariesPageSize = ConfigUtil.getPagerSummariesPageSize();
        if(mSummariesPageSize == null || mSummariesPageSize <= 0) {
            mSummariesPageSize = mDefaultPageSize;  // ???
        }
        mSummaryListPageSize = ConfigUtil.getPagerSummaryListPageSize();
        if(mSummaryListPageSize == null || mSummaryListPageSize <= 0) {
            mSummaryListPageSize = mDefaultPageSize;  // ???
        }
        mLongUrlListPageSize = ConfigUtil.getPagerLongUrlListPageSize();
        if(mLongUrlListPageSize == null || mLongUrlListPageSize <= 0) {
            mLongUrlListPageSize = mDefaultPageSize;  // ???
        }
        mShortUrlListPageSize = ConfigUtil.getPagerShortUrlListPageSize();
        if(mShortUrlListPageSize == null || mShortUrlListPageSize <= 0) {
            mShortUrlListPageSize = mDefaultPageSize;  // ???
        }
        mShortUrlStreamPageSize = ConfigUtil.getPagerShortUrlStreamPageSize();
        if(mShortUrlStreamPageSize == null || mShortUrlStreamPageSize <= 0) {
            mShortUrlStreamPageSize = mDefaultPageSize;  // ???
        }
        mFeaturedListPageSize = ConfigUtil.getPagerFeaturedListPageSize();
        if(mFeaturedListPageSize == null || mFeaturedListPageSize <= 0) {
            mFeaturedListPageSize = mDefaultPageSize;  // ???
        }
        mFeaturedGridPageSize = ConfigUtil.getPagerFeaturedGridPageSize();
        if(mFeaturedGridPageSize == null || mFeaturedGridPageSize <= 0) {
            mFeaturedGridPageSize = mDefaultPageSize;  // ???
        }

        mOrderingPrimary = ConfigUtil.getPagerOrderingPrimary();
        if(mOrderingPrimary == null || mOrderingPrimary.isEmpty()) {
        	mOrderingPrimary = ConfigUtil.getDefaultPagerOrderingPrimary();  // ???
        }
        mHeadlinesOrderingPrimary = ConfigUtil.getPagerHeadlinesOrderingPrimary();
        if(mHeadlinesOrderingPrimary == null || mHeadlinesOrderingPrimary.isEmpty()) {
        	mHeadlinesOrderingPrimary = ConfigUtil.getDefaultPagerOrderingPrimary();  // ???
        }
        mSummariesOrderingPrimary = ConfigUtil.getPagerSummariesOrderingPrimary();
        if(mSummariesOrderingPrimary == null || mSummariesOrderingPrimary.isEmpty()) {
        	mSummariesOrderingPrimary = ConfigUtil.getDefaultPagerOrderingPrimary();  // ???
        }
        mSummaryListOrderingPrimary = ConfigUtil.getPagerSummaryListOrderingPrimary();
        if(mSummaryListOrderingPrimary == null || mSummaryListOrderingPrimary.isEmpty()) {
        	mSummaryListOrderingPrimary = ConfigUtil.getDefaultPagerOrderingPrimary();  // ???
        }
        mLongUrlListOrderingPrimary = ConfigUtil.getPagerLongUrlListOrderingPrimary();
        if(mLongUrlListOrderingPrimary == null || mLongUrlListOrderingPrimary.isEmpty()) {
            mLongUrlListOrderingPrimary = ConfigUtil.getDefaultPagerOrderingPrimary();  // ???
        }
        mShortUrlListOrderingPrimary = ConfigUtil.getPagerShortUrlListOrderingPrimary();
        if(mShortUrlListOrderingPrimary == null || mShortUrlListOrderingPrimary.isEmpty()) {
            mShortUrlListOrderingPrimary = ConfigUtil.getDefaultPagerOrderingPrimary();  // ???
        }
        mShortUrlStreamOrderingPrimary = ConfigUtil.getPagerShortUrlStreamOrderingPrimary();
        if(mShortUrlStreamOrderingPrimary == null || mShortUrlStreamOrderingPrimary.isEmpty()) {
            mShortUrlStreamOrderingPrimary = ConfigUtil.getDefaultPagerOrderingPrimary();  // ???
        }
        mFeaturedListOrderingPrimary = ConfigUtil.getPagerFeaturedListOrderingPrimary();
        if(mFeaturedListOrderingPrimary == null || mFeaturedListOrderingPrimary.isEmpty()) {
            mFeaturedListOrderingPrimary = ConfigUtil.getDefaultPagerOrderingPrimary();  // ???
        }
        mFeaturedGridOrderingPrimary = ConfigUtil.getPagerFeaturedGridOrderingPrimary();
        if(mFeaturedGridOrderingPrimary == null || mFeaturedGridOrderingPrimary.isEmpty()) {
            mFeaturedGridOrderingPrimary = ConfigUtil.getDefaultPagerOrderingPrimary();  // ???
        }

        // temporary
        if(log.isLoggable(Level.INFO)) {
            log.info(">>>>>>> mDefaultPageSize = " + mDefaultPageSize);
            log.info(">>>>>>> mHeadlinesPageSize = " + mHeadlinesPageSize);
            log.info(">>>>>>> mSummariesPageSize = " + mSummariesPageSize);
            log.info(">>>>>>> mSummaryListPageSize = " + mSummaryListPageSize);
            log.info(">>>>>>> mOrderingPrimary = " + mOrderingPrimary);
            log.info(">>>>>>> mArticleListPageSize = " + mLongUrlListPageSize);
            log.info(">>>>>>> mBlogPostListPageSize = " + mShortUrlListPageSize);
            log.info(">>>>>>> mShortUrlStreamPageSize = " + mShortUrlStreamPageSize);
            log.info(">>>>>>> mFeaturedListPageSize = " + mFeaturedListPageSize);
            log.info(">>>>>>> mFeaturedGridPageSize = " + mFeaturedGridPageSize);
            log.info(">>>>>>> mHeadlinesOrderingPrimary = " + mHeadlinesOrderingPrimary);
            log.info(">>>>>>> mSummariesOrderingPrimary = " + mSummariesOrderingPrimary);
            log.info(">>>>>>> mSummaryListOrderingPrimary = " + mSummaryListOrderingPrimary);
            log.info(">>>>>>> mArticleListOrderingPrimary = " + mLongUrlListOrderingPrimary);
            log.info(">>>>>>> mBlogPostListOrderingPrimary = " + mShortUrlListOrderingPrimary);
            log.info(">>>>>>> mShortUrlStreamOrderingPrimary = " + mShortUrlStreamOrderingPrimary);
            log.info(">>>>>>> mFeaturedListOrderingPrimary = " + mFeaturedListOrderingPrimary);
            log.info(">>>>>>> mFeaturedGridOrderingPrimary = " + mFeaturedGridOrderingPrimary);
        }
    }


    public int getPageSize()
    {
        return getDefaultPageSize();
    }
    public int getDefaultPageSize()
    {
        return mDefaultPageSize;
    }
    public int getHeadlinesPageSize()
    {
        return mHeadlinesPageSize;
    }
    public int getSummariesPageSize()
    {
        return mSummariesPageSize;
    }
    public int getSummaryListPageSize()
    {
        return mSummaryListPageSize;
    }
    public int getLongUrlListPageSize()
    {
        return mLongUrlListPageSize;
    }
    public int getShortUrlListPageSize()
    {
        return mShortUrlListPageSize;
    }
    public int getShortUrlStreamPageSize()
    {
        return mShortUrlStreamPageSize;
    }
    public int getFeaturedListPageSize()
    {
        return mFeaturedListPageSize;
    }
    public int getFeaturedGridPageSize()
    {
        return mFeaturedGridPageSize;
    }

    public String getOrderingPrimary()
    {
    	return mOrderingPrimary;
    }
    public String getHeadlinesOrderingPrimary()
    {
        return mHeadlinesOrderingPrimary;
    }
    public String getSummariesOrderingPrimary()
    {
        return mSummariesOrderingPrimary;
    }
    public String getSummaryListOrderingPrimary()
    {
        return mSummaryListOrderingPrimary;
    }
    public String getLongUrlListOrderingPrimary()
    {
        return mLongUrlListOrderingPrimary;
    }
    public String getShortUrlListOrderingPrimary()
    {
        return mShortUrlListOrderingPrimary;
    }
    public String getShortUrlStreamOrderingPrimary()
    {
        return mShortUrlStreamOrderingPrimary;
    }
    public String getFeaturedListOrderingPrimary()
    {
        return mFeaturedListOrderingPrimary;
    }
    public String getFeaturedGridOrderingPrimary()
    {
        return mFeaturedGridOrderingPrimary;
    }

    public void setOrderingPrimary(String orderingPrimary)
    {
        mOrderingPrimary = orderingPrimary;
        if(mOrderingPrimary == null || mOrderingPrimary.isEmpty()) {
        	mOrderingPrimary = ConfigUtil.getPagerOrderingPrimary();  // ???
        }
    }
    public void setHeadlinesOrderingPrimary(String orderingPrimary)
    {
    	mHeadlinesOrderingPrimary = orderingPrimary;
        if(mHeadlinesOrderingPrimary == null || mHeadlinesOrderingPrimary.isEmpty()) {
        	mHeadlinesOrderingPrimary = ConfigUtil.getPagerHeadlinesOrderingPrimary();  // ???
        }
    }
    public void setSummariesOrderingPrimary(String orderingPrimary)
    {
    	mSummariesOrderingPrimary = orderingPrimary;
        if(mSummariesOrderingPrimary == null || mSummariesOrderingPrimary.isEmpty()) {
        	mSummariesOrderingPrimary = ConfigUtil.getPagerSummariesOrderingPrimary();  // ???
        }
    }
    public void setSummaryListOrderingPrimary(String orderingPrimary)
    {
    	mSummaryListOrderingPrimary = orderingPrimary;
        if(mSummaryListOrderingPrimary == null || mSummaryListOrderingPrimary.isEmpty()) {
        	mSummaryListOrderingPrimary = ConfigUtil.getPagerSummaryListOrderingPrimary();  // ???
        }
    }
    public void setArticleListOrderingPrimary(String orderingPrimary)
    {
        mLongUrlListOrderingPrimary = orderingPrimary;
        if(mLongUrlListOrderingPrimary == null || mLongUrlListOrderingPrimary.isEmpty()) {
            mLongUrlListOrderingPrimary = ConfigUtil.getPagerLongUrlListOrderingPrimary();  // ???
        }
    }
    public void setBlogPostListOrderingPrimary(String orderingPrimary)
    {
        mShortUrlListOrderingPrimary = orderingPrimary;
        if(mShortUrlListOrderingPrimary == null || mShortUrlListOrderingPrimary.isEmpty()) {
            mShortUrlListOrderingPrimary = ConfigUtil.getPagerShortUrlListOrderingPrimary();  // ???
        }
    }
    public void setShortUrlStreamOrderingPrimary(String orderingPrimary)
    {
        mShortUrlStreamOrderingPrimary = orderingPrimary;
        if(mShortUrlStreamOrderingPrimary == null || mShortUrlStreamOrderingPrimary.isEmpty()) {
            mShortUrlStreamOrderingPrimary = ConfigUtil.getPagerShortUrlStreamOrderingPrimary();  // ???
        }
    }
    public void setFeaturedListOrderingPrimary(String orderingPrimary)
    {
        mFeaturedListOrderingPrimary = orderingPrimary;
        if(mFeaturedListOrderingPrimary == null || mFeaturedListOrderingPrimary.isEmpty()) {
            mFeaturedListOrderingPrimary = ConfigUtil.getPagerFeaturedListOrderingPrimary();  // ???
        }
    }
    public void setFeaturedGridOrderingPrimary(String orderingPrimary)
    {
        mFeaturedGridOrderingPrimary = orderingPrimary;
        if(mFeaturedGridOrderingPrimary == null || mFeaturedGridOrderingPrimary.isEmpty()) {
            mFeaturedGridOrderingPrimary = ConfigUtil.getPagerFeaturedGridOrderingPrimary();  // ???
        }
    }

    
    public PagerStateStructJsBean constructPagerStateFromOffset(Long offset, Integer count, Long totalCount, Long lowerBoundTotalCount)
    {
        PagerStateStructJsBean bean = new PagerStateStructJsBean();

        bean.setPagerMode(PagerMode.MODE_OFFSET);
        if(offset == null || offset < 0L) {  // ???
            offset = 0L;
        }
        bean.setCurrentOffset(offset);
        if(count == null || count <= 0) {
            count = getDefaultPageSize();
        }
        bean.setPageSize(count);
        bean.setTotalCount(totalCount);
        if(lowerBoundTotalCount == null) {
            lowerBoundTotalCount = totalCount;  // Could still be null...
        }
        bean.setLowerBoundTotalCount(lowerBoundTotalCount);
        bean.setPreviousPageOffset(getPreviousPageOffset(offset, count));
        bean.setNextPageOffset(getNextPageOffset(offset, count, totalCount, lowerBoundTotalCount));
        bean.setLastPageOffset(getLastPageOffset(offset, count, lowerBoundTotalCount));
        bean.setFirstActionEnabled(isFirstActionEnabled(offset, count));
        bean.setPreviousActionEnabled(isPreviousActionEnabled(offset, count));
        bean.setNextActionEnabled(isNextActionEnabled(offset, count, totalCount, lowerBoundTotalCount));
        bean.setLastActionEnabled(isLastActionEnabled(offset, count, totalCount));
        // etc...

        return bean;
    }

    public PagerStateStructJsBean constructPagerStateFromPage(long page, Integer pageSize, Long totalCount, Long lowerBoundTotalCount)
    {
        PagerStateStructJsBean bean = new PagerStateStructJsBean();

        bean.setPagerMode(PagerMode.MODE_PAGE);
        if(page < 0L) {  // ???
            page = 0L;
        }
        bean.setCurrentPage(page);
        if(pageSize == null || pageSize <= 0) {
            pageSize = getDefaultPageSize();
        }
        bean.setPageSize(pageSize);
        bean.setTotalCount(totalCount);
        if(lowerBoundTotalCount == null) {
            lowerBoundTotalCount = totalCount;  // Could still be null...
        }
        bean.setLowerBoundTotalCount(lowerBoundTotalCount);
        bean.setPreviousPageOffset(getPreviousPageOffset(page, pageSize));
        bean.setNextPageOffset(getNextPageOffset(page, pageSize, totalCount, lowerBoundTotalCount));
        bean.setLastPageOffset(getLastPageOffset(page, pageSize, lowerBoundTotalCount));
        bean.setFirstActionEnabled(isFirstActionEnabled(page, pageSize));
        bean.setPreviousActionEnabled(isPreviousActionEnabled(page, pageSize));
        bean.setNextActionEnabled(isNextActionEnabled(page, pageSize, totalCount, lowerBoundTotalCount));
        bean.setLastActionEnabled(isLastActionEnabled(page, pageSize, totalCount));
        // etc...

        return bean;
    }


    public long getOffsetFromPageIndex(long page)
    {
        return getOffsetFromPageIndex(page, null);
    } 

    public long getOffsetFromPageIndex(long page, Integer pageSize)
    {
        return getOffsetFromPageIndex(page, pageSize, null);
    }

    public long getOffsetFromPageIndex(long page, Integer pageSize, Long totalCount)
    {
        if(pageSize == null || pageSize <= 0) {
            pageSize = getDefaultPageSize();
        }
        long offset = 0L;
        if(pageSize != null) {
            if(page >= 0L) {
                offset = page * pageSize;
            } else {
                // ????
                if(totalCount == null || totalCount <= 0) {
                    // What to do???
                    if(log.isLoggable(Level.WARNING)) log.warning("Could not compute the offset: page = " + page + "; totalCount = " + totalCount);
                    // --> offset = 0L;
                } else {
                    long totalPages = (long) Math.ceil( ((double) totalCount) / pageSize );
                    long realPageIdx = totalPages + page;
                    if(realPageIdx < 0L) {
                        realPageIdx = 0L;
                    }
                    offset = realPageIdx * pageSize;
                }
            }
        } else {
            // ??? Can this happen????
        }
        return offset;
    }


    public Long getPreviousPageOffset(Long offset, Integer count)
    {
        if(offset == null || offset <= 0L) {
            return null;   // Null return value means there is no "previous" page....
        }
        if(count == null || count <= 0) {
            count = getDefaultPageSize();
        }
        Long prevOffset = offset - count;
        if(prevOffset < 0L) {
            prevOffset = 0L;
        }
        return prevOffset;
    }

    public Long getNextPageOffset(Long offset, Integer count, Long totalCount, Long lowerBoundTotalCount)
    {
        if(lowerBoundTotalCount == null || lowerBoundTotalCount <= 0L || (totalCount != null && lowerBoundTotalCount < totalCount) ) {
            lowerBoundTotalCount = totalCount;  // Could still be null...
        }
        if(lowerBoundTotalCount == null || lowerBoundTotalCount <= 0) {
            // What to do???
            if(log.isLoggable(Level.WARNING)) log.warning("Could not compute the next offset: offset = " + offset + "; lowerBoundTotalCount = " + lowerBoundTotalCount);
            return null;
        }
        if(offset == null) {
            offset = 0L;
        }
        if(count == null || count <= 0) {
            count = getDefaultPageSize();
        }
        Long nextOffset = offset + count;
        if(nextOffset < lowerBoundTotalCount) {
            // all is good...
        } else if((totalCount != null) && (nextOffset >= totalCount)) { 
            nextOffset = null;   // No "next" page...
        } else {
            // What to do ?????
            // ignore and go ahead.... ??? 
            if(log.isLoggable(Level.WARNING)) log.warning("Computeted nextOffset might be incorrect: nextOffset = " + nextOffset);
        }
        return nextOffset;
    }

    // Last page is computed based on the current offset, not from 0.
    public Long getLastPageOffset(Long offset, Integer count, Long totalCount)
    {
        if(totalCount == null || totalCount <= 0) {
            // What to do???
            if(log.isLoggable(Level.WARNING)) log.warning("Could not compute the last page offset: offset = " + offset + "; totalCount = " + totalCount);
            return null;
        }
        if(offset == null || offset >= totalCount) {  // always should be: offset < tatalCount...
            offset = 0L;
        }
        if(count == null || count <= 0) {
            count = getDefaultPageSize();
        }
        long remaining = totalCount - offset;
        long delta = (long) (remaining - 1) / count;
        long lastOffset = offset + delta * count;
        return lastOffset;
    }


    public Long getPreviousPageOffset(long page, Integer pageSize)
    {
        if(page <= 0L) {
            return null;
        } else if(page == 1L) {
            return 0L;
        }
        if(pageSize == null || pageSize <= 0) {
            pageSize = getDefaultPageSize();
        }
        Long prevOffset = (page - 1) * pageSize;
        if(prevOffset < 0L) {
            prevOffset = 0L;
        }
        return prevOffset;
    }

    public Long getNextPageOffset(long page, Integer pageSize, Long totalCount, Long lowerBoundTotalCount)
    {
        if(lowerBoundTotalCount == null || lowerBoundTotalCount <= 0L || (totalCount != null && lowerBoundTotalCount < totalCount) ) {
            lowerBoundTotalCount = totalCount;  // Could still be null...
        }
        if(lowerBoundTotalCount == null || lowerBoundTotalCount <= 0) {
            // What to do???
            if(log.isLoggable(Level.WARNING)) log.warning("Could not compute the next offset: page = " + page + "; lowerBoundTotalCount = " + lowerBoundTotalCount);
            return null;
        }
        if(pageSize == null || pageSize <= 0) {
            pageSize = getDefaultPageSize();
        }
        Long nextOffset = (page + 1) * pageSize;
        if(nextOffset < lowerBoundTotalCount) {
            // all is good...
        } else if((totalCount != null) && (nextOffset >= totalCount)) { 
            nextOffset = null;   // No "next" page...
        } else {
            // What to do ?????
            // ignore and go ahead.... for now...
            if(log.isLoggable(Level.WARNING)) log.warning("Computeted nextOffset might be incorrect: nextOffset = " + nextOffset);
        }
        return nextOffset;
    }

    // Last page is computed based on the current page, not from 0.
    public Long getLastPageOffset(long page, Integer pageSize, Long totalCount)
    {
        if(totalCount == null || totalCount <= 0) {
            // What to do???
            if(log.isLoggable(Level.WARNING)) log.warning("Could not compute the last page offset: page = " + page + "; totalCount = " + totalCount);
            return null;
        }
        if(pageSize == null || pageSize <= 0) {
            pageSize = getDefaultPageSize();
        }
        long offset = page * pageSize;
        if(offset >= totalCount) {  // always should be: offset < tatalCount...
            offset = 0L;
        }
        long remaining = totalCount - offset;
        long delta = (long) (remaining - 1) / pageSize;
        long lastOffset = offset + delta * pageSize;
        return lastOffset;
    }

    // Last page is computed based on the current page, not from 0.
    public long getLastPageIndex(long page, Integer pageSize, Long totalCount)
    {
        Long lastOffset = getLastPageOffset(page, pageSize, totalCount);
        if(lastOffset == null) {
            return 0L;
        }
        if(pageSize == null || pageSize <= 0) {
            pageSize = getDefaultPageSize();
        }
        long lastPageIndex = lastOffset / pageSize; 
        return lastPageIndex;
    }

    
    
    // temporary
    
    public boolean isFirstActionEnabled(Long offset, Integer count)
    {
        // temporary
        return isPreviousActionEnabled(offset, count);
    }
    public boolean isPreviousActionEnabled(Long offset, Integer count)
    {
        if(offset != null && offset > 0L) {
            return true;
        } else {
            return false;
        }
    }
    public boolean isLastActionEnabled(Long offset, Integer count, Long totalCount)
    {
        // temporary
        if(totalCount == null || totalCount <= 1L) {
            return false;
        }
        return isNextActionEnabled(offset, count, totalCount, null);
    }
    public boolean isNextActionEnabled(Long offset, Integer count, Long totalCount, Long lowerBoundTotalCount)
    {
        // temporary
        Long nextOffset = getNextPageOffset(offset, count, totalCount, lowerBoundTotalCount);
        if(nextOffset == null) {
            return false;
        } else {
            //if(nextOffset >= offset + count) {
                return true;
            //} else {
            //    return false;
            //}
        }
    }
    
    
    public boolean isFirstActionEnabled(long page, Integer pageSize)
    {
        // temporary
        return isPreviousActionEnabled(page, pageSize);
    }
    public boolean isPreviousActionEnabled(long page, Integer pageSize)
    {
        if(page == 0L) {
            return false;
        } else {
            return true;
        }
    }
    public boolean isLastActionEnabled(long page, Integer pageSize, Long totalCount)
    {
        // temporary
        if(totalCount == null || totalCount <= 1L) {
            return false;
        }
        return isNextActionEnabled(page, pageSize, totalCount, null);
    }
    public boolean isNextActionEnabled(long page, Integer pageSize, Long totalCount, Long lowerBoundTotalCount)
    {
        // temporary
        Long nextOffset = getNextPageOffset(page, pageSize, totalCount, lowerBoundTotalCount);
        if(nextOffset == null) {
            return false;
        } else {
            //if(nextOffset >= (page + 1) * pageSize) {
                return true;
            //} else {
            //    return false;
            //}
        }
    }

    
}
