package com.cannyurl.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.LinkMessageJsBean;
import com.cannyurl.fe.bean.ShortLinkJsBean;
import com.cannyurl.wa.service.LinkMessageWebService;
import com.cannyurl.wa.service.ShortLinkWebService;
import com.cannyurl.wa.service.UserWebService;


// TBD: ..
// To be used in JSP (server side)
//    to enforce passphrase requirements (projects, shortLinks, etc.)....
public class PassphraseHelper
{
    private static final Logger log = Logger.getLogger(PassphraseHelper.class.getName());

    private UserWebService userWebService = null;
    private ShortLinkWebService shortLinkWebService = null;
    private LinkMessageWebService linkMessageWebService = null;
    // TBD: client service, etc...
    // ...

    private PassphraseHelper() {}

    // Initialization-on-demand holder.
    private static final class PassphraseHelperHolder
    {
        private static final PassphraseHelper INSTANCE = new PassphraseHelper();
    }

    // Singleton method
    public static PassphraseHelper getInstance()
    {
        return PassphraseHelperHolder.INSTANCE;
    }
    
    
    private UserWebService getUserWebService()
    {
        if(userWebService == null) {
            userWebService = new UserWebService();
        }
        return userWebService;
    }
    private ShortLinkWebService getShortLinkWebService()
    {
        if(shortLinkWebService == null) {
            shortLinkWebService = new ShortLinkWebService();
        }
        return shortLinkWebService;
    }
    private LinkMessageWebService getLinkMessageWebService()
    {
        if(linkMessageWebService == null) {
            linkMessageWebService = new LinkMessageWebService();
        }
        return linkMessageWebService;
    }

    
    
    
    
}
