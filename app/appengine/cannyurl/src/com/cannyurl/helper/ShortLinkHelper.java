package com.cannyurl.helper;

import java.util.logging.Logger;

import com.cannyurl.app.helper.ShortLinkAppHelper;
import com.cannyurl.fe.WebException;
import com.myurldb.ws.BaseException;


// temporary
public class ShortLinkHelper
{
    private static final Logger log = Logger.getLogger(ShortLinkHelper.class.getName());

    private ShortLinkHelper() 
    {
    }

    // Initialization-on-demand holder.
    private static final class ShortLinkHelperHolder
    {
        private static final ShortLinkHelper INSTANCE = new ShortLinkHelper();
    }

    // Singleton method
    public static ShortLinkHelper getInstance()
    {
        return ShortLinkHelperHolder.INSTANCE;
    }

    
    public String findShortLinkKeyByToken(String token, String domain) throws WebException
    {
        String key = null;
        try {
            key = ShortLinkAppHelper.getInstance().findShortLinkKeyByToken(token, domain);
        } catch (BaseException e) {
            throw new WebException("Failed to find a shortLink key for token = " + token + "; domain = " + domain, e);
        }        
        return key;
    }

    
    // TBD:
    // ...
    
}
