package com.cannyurl.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.sf.jsr107cache.Cache;

import com.cannyurl.af.util.CacheHelper;
import com.cannyurl.app.util.ConfigUtil;
import com.cannyurl.app.util.ShortLinkUtil;
import com.cannyurl.common.RedirectType;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.ShortLinkJsBean;
import com.cannyurl.wa.service.ShortLinkWebService;
import com.cannyurl.wa.service.UserWebService;


public class ShortUrlAccessHelper
{
    private static final Logger log = Logger.getLogger(ShortUrlAccessHelper.class.getName());

    private UserWebService userWebService = null;
    private ShortLinkWebService shortLinkWebService = null;
//    private AccessRecordWebService accessRecordWebService = null;
    // ...

    private ShortUrlAccessHelper() {}

    // Initialization-on-demand holder.
    private static final class ShortUriAccessHelperHolder
    {
        private static final ShortUrlAccessHelper INSTANCE = new ShortUrlAccessHelper();
    }

    // Singleton method
    public static ShortUrlAccessHelper getInstance()
    {
        return ShortUriAccessHelperHolder.INSTANCE;
    }
    
    
    // Caching...
    private static Cache sCache = null;
    private static Cache getCache()
    {
        if(sCache == null) {
            // ????
            sCache = CacheHelper.getDayLongInstance().getCache();
        }
        return sCache;
    }
    private static String getCacheKey(String shortUrl)
    {
        return getCacheKey(shortUrl, null);
    }
    private static String getCacheKey(String shortUrl, String path)
    {
        final String PREFIX = "SUAH-ShortUrl-Parsed-";
        String key = PREFIX + shortUrl;
        if(path != null) {
            key += "-" + path + "-";
        }
        return key;
    }
    
    private static String[] getParsedUrlParts(String shortUrl)
    {
        return getParsedUrlParts(shortUrl, null);
    }
    private static String[] getParsedUrlParts(String shortUrl, String path)
    {
        String[] urlParts = null;
        if(getCache() != null) {
            urlParts = (String[]) getCache().get(getCacheKey(shortUrl, path));
        }
        if(urlParts == null) {
//            if(path == null) {
//                urlParts = ShortLinkUtil.parseFullUrl(shortUrl);
//            } else {
//                urlParts = ShortLinkUtil.parseFullUrl(shortUrl, path);                
//            }
            urlParts = ShortLinkUtil.parseFullUrl(shortUrl, path);
            if(getCache() != null) {
                if(urlParts != null) { // ???
                    getCache().put(getCacheKey(shortUrl, path), urlParts);
                }
            }
        }
        return urlParts;
    }
    
    
    private UserWebService getUserWebService()
    {
        if(userWebService == null) {
            userWebService = new UserWebService();
        }
        return userWebService;
    }
    private ShortLinkWebService getShortLinkWebService()
    {
        if(shortLinkWebService == null) {
            shortLinkWebService = new ShortLinkWebService();
        }
        return shortLinkWebService;
    }
//    private AccessRecordWebService getAccessRecordWebService()
//    {
//        if(accessRecordWebService == null) {
//            accessRecordWebService = new AccessRecordWebService();
//        }
//        return accessRecordWebService;
//    }
    
    
//    // TBD
//    public boolean createAccessRecord(AccessRecordJsBean accessRecordBean)
//    {
//        // TBD
//        try {
//            getAccessRecordWebService().createAccessRecord(accessRecordBean);
//            return true;
//        } catch (WebException e) {
//            // ignore.
//            log.log(Level.WARNING, "Failed to create AccessRecord.", e);
//        }
//
//        // TBD
//        return false;
//    }

    
    public ShortLinkJsBean getShortLink(String guid)
    {
        ShortLinkJsBean shortLinkBean = null;
        try {
            shortLinkBean = getShortLinkWebService().getShortLink(guid);
        } catch (WebException e) {
            log.log(Level.SEVERE, "Failed to find a shortLink for given guid = " + guid, e);
            return null;
        } catch (Exception e) {
            log.log(Level.SEVERE, "Failed to find a shortLink for given guid = " + guid, e);
            return null;
        }

        return shortLinkBean;
    }
    
    public String getRedirectUrl(String longUrl, String queryString)
    {
        if(longUrl == null || queryString == null || queryString.trim().isEmpty()) {
            return longUrl;
        }
        
        // TBD:
        // What happens if the longUrl contains the hashtag (#)....
        // Attach it at thee end of the reconstructed URL????
        int hashIdx = longUrl.lastIndexOf("#");
        String hashFrag = "";
        if(hashIdx > 0) {
            // (1) remove it first.
            longUrl = longUrl.substring(0, hashIdx);
            //hashFrag = longUrl.substring(hashIdx + 1);
            hashFrag = longUrl.substring(hashIdx);   // Includes "#"
        }
        
        // temporary
        String fullRedirectUrl = longUrl;
        // if the longUrl already includes "?", use "&"....
        if(longUrl.indexOf("?") > 0) {
            fullRedirectUrl = longUrl + "&" + queryString.trim();
        } else {
            fullRedirectUrl = longUrl + "?" + queryString.trim();
        }
        
        if(hashIdx > 0) {
            // (2) then put it back.
            //fullRedirectUrl += "#" + hashFrag;
            fullRedirectUrl += hashFrag;  // hashFrag includes "#"
        }

        log.log(Level.INFO, "getRedirectUrl(): fullRedirectUrl = " + fullRedirectUrl);
        return fullRedirectUrl;
    }

    // TBD: Need to be re-written???
    // Currently not being used....
    private String getRedirectType(ShortLinkJsBean linkBean, String shortUrl)
    {
        // TBD
        String redirectType = linkBean.getRedirectType();
        if(redirectType == null || redirectType.isEmpty()) {
            // TBD: Use user settings if available.
            redirectType = ConfigUtil.getSystemDefaultRedirectType();
        }
        if(shortUrl != null) {
            // String[] urlParts = ShortLinkUtil.parseFullUrl(shortUrl);
            String[] urlParts = getParsedUrlParts(shortUrl);
            if(urlParts != null) {
                String path = urlParts[1];
                if(path != null && !path.isEmpty()) {
                    // temporary
//                    if(redirectType.equals(RedirectType.TYPE_PERMANENT) || redirectType.equals(RedirectType.TYPE_TEMPORARY)) {
//                        if(path.equals("c")) {
//                            redirectType = RedirectType.TYPE_CONFIRM;
//                        } else if(path.equals("f")) {
//                            redirectType = RedirectType.TYPE_FLASH;
//                        } else if(path.equals("a")) {
//                            redirectType = RedirectType.TYPE_PERMANENT;
//                        } else {
//                            // ignore
//                        }
//                    } else if(redirectType.equals(RedirectType.TYPE_CONFIRM) || redirectType.equals(RedirectType.TYPE_FLASH)) {
//                        if(path.equals("c")) {
//                            redirectType = RedirectType.TYPE_CONFIRM;
//                        } else if(path.equals("f")) {
//                            redirectType = RedirectType.TYPE_FLASH;
//                        } else {
//                            // ignore
//                        }
//                    } else {
//                        // Do nothing.
//                    }

                    // temporary
                    // For now, a valid path always overwrites the default behavior. 
                    if(path.equals("c")) {
                        redirectType = RedirectType.TYPE_CONFIRM;
                    } else if(path.equals("f")) {
                        redirectType = RedirectType.TYPE_FLASH;
                    } else if(path.equals("a")) {
                        redirectType = RedirectType.TYPE_PERMANENT;
                    } else if(path.equals("o")) {
                        redirectType = RedirectType.TYPE_TEMPORARY;
                    } else {
                        // ignore
                    }
                    // temporary
                    
                }                
            }
        }

        log.log(Level.INFO, "getRedirectType(): redirectType = " + redirectType);
        return redirectType;
    }

    
    

    // TBD...
    public String getRequestUrlRedirectType(String requestUrl)
    {
        return null;
    }

    
    // temporary
    // Is there a more efficient way to achieve this????
    public static String getVerifyRedirectUrl(String shortUrl)
    {
        // String[] parts = ShortLinkUtil.parseFullUrl(shortUrl);
        String[] parts = getParsedUrlParts(shortUrl);
        if(parts == null || parts.length < 3) {
            // error.
            log.warning("Failed to parse the shortUrl = " + shortUrl);
            return null;  // ???
        }
        
        String url = ShortLinkUtil.buildShortUrl(parts[0], ShortLinkUtil.REDIRECT_PATH_VERIFY, parts[2]);
        log.fine("getVerifyRedirectUrl(): Returning url = " + url + " for input shortUrl = " + shortUrl);
        return url;
    }
    public static String getInfoRedirectUrl(String shortUrl)
    {
        // String[] parts = ShortLinkUtil.parseFullUrl(shortUrl);
        String[] parts = getParsedUrlParts(shortUrl);
        if(parts == null || parts.length < 3) {
            // error.
            log.warning("Failed to parse the shortUrl = " + shortUrl);
            return null;  // ???
        }
        
        String url = ShortLinkUtil.buildShortUrl(parts[0], ShortLinkUtil.REDIRECT_PATH_INFO, parts[2]);
        log.fine("getInfoRedirectUrl(): Returning url = " + url + " for input shortUrl = " + shortUrl);
        return url;
    }
    public static String getConfirmRedirectUrl(String shortUrl)
    {
        // String[] parts = ShortLinkUtil.parseFullUrl(shortUrl);
        String[] parts = getParsedUrlParts(shortUrl);
        if(parts == null || parts.length < 3) {
            // error.
            log.warning("Failed to parse the shortUrl = " + shortUrl);
            return null;  // ???
        }
        
        String url = ShortLinkUtil.buildShortUrl(parts[0], ShortLinkUtil.REDIRECT_PATH_CONFIRM, parts[2]);
        log.fine("getConfirmRedirectUrl(): Returning url = " + url + " for input shortUrl = " + shortUrl);
        return url;
    }
    public static String getFlashRedirectUrl(String shortUrl)
    {
        // String[] parts = ShortLinkUtil.parseFullUrl(shortUrl);
        String[] parts = getParsedUrlParts(shortUrl);
        if(parts == null || parts.length < 3) {
            // error.
            log.warning("Failed to parse the shortUrl = " + shortUrl);
            return null;  // ???
        }
        
        String url = ShortLinkUtil.buildShortUrl(parts[0], ShortLinkUtil.REDIRECT_PATH_FLASH, parts[2]);
        log.fine("getFlashRedirectUrl(): Returning url = " + url + " for input shortUrl = " + shortUrl);
        return url;
    }
    public static String getPermanentRedirectUrl(String shortUrl)
    {
        // String[] parts = ShortLinkUtil.parseFullUrl(shortUrl);
        String[] parts = getParsedUrlParts(shortUrl);
        if(parts == null || parts.length < 3) {
            // error.
            log.warning("Failed to parse the shortUrl = " + shortUrl);
            return null;  // ???
        }
        
        String url = ShortLinkUtil.buildShortUrl(parts[0], ShortLinkUtil.REDIRECT_PATH_PERMANENT, parts[2]);
        log.fine("getPermanentRedirectUrl(): Returning url = " + url + " for input shortUrl = " + shortUrl);
        return url;
    }
    public static String getTemporaryRedirectUrl(String shortUrl)
    {
        // String[] parts = ShortLinkUtil.parseFullUrl(shortUrl);
        String[] parts = getParsedUrlParts(shortUrl);
        if(parts == null || parts.length < 3) {
            // error.
            log.warning("Failed to parse the shortUrl = " + shortUrl);
            return null;  // ???
        }
        
        String url = ShortLinkUtil.buildShortUrl(parts[0], ShortLinkUtil.REDIRECT_PATH_TEMPORARY, parts[2]);
        log.fine("getTemporaryRedirectUrl(): Returning url = " + url + " for input shortUrl = " + shortUrl);
        return url;
    }


    // Returns the one letter "path", if found.
    // Returns null otherwise.
    public static String findPathComponent(String shortUrl)
    {
        // TBD: Is there a better way?
        // String[] parts = ShortLinkUtil.parseFullUrl(shortUrl);
        String[] parts = getParsedUrlParts(shortUrl);
        if(parts == null || parts.length < 2) {
            return null;
        } else {
            return parts[1];
        }
    }


    // TBD
    // (What happens if IP address is used ????)
    public ShortLinkJsBean findShortLink(String shortUrl, String path)
    {
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "findShortLink() called: shortUrl = " + shortUrl + "; path = " + path);

        ShortLinkJsBean shortLinkBean = null;

        // [1] Check the "default" short url first.
        if(path == null || path.isEmpty()) {   // Note that if p is set, findShortLinkByShortUrl() will always return null.
            shortLinkBean = findShortLinkByShortUrl(shortUrl);
            if(shortLinkBean != null) {
                return shortLinkBean;
            }
        } 
        
        // [2] Then check domain + token...
        // String[] urlParts = ShortLinkUtil.parseFullUrl(shortUrl, path);
        String[] urlParts = getParsedUrlParts(shortUrl, path);
        
        if(urlParts == null) {
            log.log(Level.WARNING, "Url arg is null or invalid."); 
            return null;
        }
        String domain = urlParts[0];
        String ppath = urlParts[1];  // == path.
        String token = urlParts[2];
//        // This cannot happen.
//        if(path != null && !path.isEmpty()) {
//            if(!path.equals(ppath)) {
//                // error!
//                log.log(Level.WARNING, "Input path, " + path + ", is different from the parsed path, " + ppath);
//            }
//        }

        shortLinkBean = findShortLinkByDomainAndToken(domain, token);

        // if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "findShortLink(): shortLinkBean = " + shortLinkBean);
        return shortLinkBean;
    }

    // TBD
    // (What happens if IP address is used ????)
    public ShortLinkJsBean findShortLinkByShortUrl(String shortUrl)
    {
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "findShortLinkByShortUrl() called: shortUrl = " + shortUrl);

        ShortLinkJsBean shortLinkBean = null;
        try {
            List<ShortLinkJsBean> shortLinks = null;
            String filter = "shortUrl=='" + shortUrl + "'";
            String ordering = "createdTime desc";

            shortLinks = getShortLinkWebService().findShortLinks(filter, ordering, null, null);    
            if(shortLinks != null && shortLinks.size() > 0) {
                shortLinkBean = shortLinks.get(0);
                // For debugging/diagnostic purposes.
                if(shortLinks.size() > 1) { 
                    // This should not happen.
                    log.log(Level.SEVERE, "There are more than one shortLink with the same url = " + shortUrl + ". Needs further investigation!");
                }
            } else {
                log.log(Level.WARNING, "ShortLink does not exist with given url = " + shortUrl);                
            }
        } catch (WebException e) {
            log.log(Level.SEVERE, "Failed to find a shortLink for given url = " + shortUrl, e);
            return null;
        } catch (Exception e) {
            log.log(Level.SEVERE, "Failed to find a shortLink for given url = " + shortUrl, e);
            return null;
        }

        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "findShortLinkByShortUrl(): shortLinkBean = " + shortLinkBean);
        return shortLinkBean;
    }

    public ShortLinkJsBean findShortLinkByDomainAndToken(String domain, String token)
    {
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "findShortLinkByDomainAndToken() called: domain = " + domain + "; token = " + token);

        ShortLinkJsBean shortLinkBean = null;
        try {
            List<ShortLinkJsBean> shortLinks = null;
            String filter = "domain=='" + domain + "' && token=='" + token + "'";
            // TBD: Path should not be used in finding shortLink...
            //      Path can be used to modify access/view type in certain cases....
            //if(path != null && path.length() > 0) {
            //    filter += " && path=='" + path + "'";
            //}
            String ordering = "createdTime desc";

            shortLinks = getShortLinkWebService().findShortLinks(filter, ordering, null, null);    
            if(shortLinks != null && shortLinks.size() > 0) {
                shortLinkBean = shortLinks.get(0);
                // For debugging/diagnostic purposes.
                if(shortLinks.size() > 1) { 
                    // This should not happen.
                    log.log(Level.SEVERE, "There are more than one shortLink with the same url. Needs further investigation! domain = " + domain + "; token = " + token);
                }
            } else {
                log.log(Level.WARNING, "ShortLink does not exist with given url. domain = " + domain + "; token = " + token);                
            }
        } catch (WebException e) {
            log.log(Level.SEVERE, "Failed to find a shortLink for given url. domain = " + domain + "; token = " + token, e);
            return null;
        } catch (Exception e) {
            log.log(Level.SEVERE, "Failed to find a shortLink for given url. domain = " + domain + "; token = " + token, e);
            return null;
        }

        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "findShortLinkByDomainAndToken(): shortLinkBean = " + shortLinkBean);
        return shortLinkBean;
    }

    
    
}
