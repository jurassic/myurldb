package com.cannyurl.helper;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.cannyurl.app.util.ConfigUtil;
import com.cannyurl.app.util.ShortLinkUtil;
import com.cannyurl.app.util.TokenGeneratorUtil;
import com.cannyurl.common.DomainType;
import com.cannyurl.common.TokenGenerationMethod;
import com.cannyurl.common.TokenType;
import com.cannyurl.fe.WebException;
import com.myurldb.ws.BaseException;


// temporary
public class ShortUrlGeneratorHelper
{
    private static final Logger log = Logger.getLogger(ShortUrlGeneratorHelper.class.getName());

    private ShortUrlGeneratorHelper() 
    {
    }

    // Initialization-on-demand holder.
    private static final class ShortUrlGeneratorHelperHolder
    {
        private static final ShortUrlGeneratorHelper INSTANCE = new ShortUrlGeneratorHelper();
    }

    // Singleton method
    public static ShortUrlGeneratorHelper getInstance()
    {
        return ShortUrlGeneratorHelperHolder.INSTANCE;
    }

    
    // TBD:
    // The logic here might be slightly different from that implemented in ShortLinkAppService.validateShortLink().
    // Clearly, we need to refactor and use the same logic in both places...
    // ....

    
    public String generateShortUrl(String tokenType, String sassyTokenType)
    {
        return generateShortUrl(tokenType, sassyTokenType, null);
    }
    public String generateShortUrl(String tokenType, String sassyTokenType, String domain)
    {
        return generateShortUrl(tokenType, sassyTokenType, domain, null);
    }
    public String generateShortUrl(String tokenType, String sassyTokenType, String domain, String domainType)
    {
        return generateShortUrl(tokenType, sassyTokenType, domain, domainType, null);
    }
    public String generateShortUrl(String tokenType, String sassyTokenType, String domain, String domainType, String user)
    {
        return generateShortUrl(tokenType, sassyTokenType, domain, domainType, user, null, null);
    }
    public String generateShortUrl(String tokenType, String sassyTokenType, String domain, String domainType, String user, String username, String usercode)
    {
        return generateShortUrl(tokenType, sassyTokenType, domain, domainType, user, username, usercode, false);
    }

    // Note the "precedence" the method arguments.
    //   (e.g., if domain is specified, domainType, etc. are ignored, etc.)
    public String generateShortUrl(String tokenType, String sassyTokenType, String domain, String domainType, String user, String username, String usercode, boolean checkUserDB)
    {
        if(domain == null || domain.isEmpty()) {
            if(!DomainType.isValidType(domainType)) {
                if(log.isLoggable(Level.INFO)) log.info("Input domainType = " + domainType + " will be ignored.");
                // ????
                // domainType = DomainType.TYPE_CUSTOM;
                domainType = ConfigUtil.getSystemDefaultDomainType();
                if(log.isLoggable(Level.INFO)) log.info("Using the domainType = " + domainType);
            }
            domain = DomainCheckerHelper.getInstance().getDomainForUser(domainType, user, username, usercode, checkUserDB);
        }
        if(log.isLoggable(Level.FINE)) log.fine("domain = " + domain);
        
        String token = TokenGeneratorHelper.getInstance().generateToken(domainType, tokenType, sassyTokenType);
        if(log.isLoggable(Level.FINE)) log.fine("token = " + token);

        String shortUrl = ShortLinkUtil.buildShortUrl(domain, null, token);
        if(log.isLoggable(Level.INFO)) log.info("shortUrl generated: " + shortUrl);
        
        return shortUrl;
    }   
    
}
