package com.cannyurl.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.ShortLinkJsBean;
import com.cannyurl.wa.service.ShortLinkWebService;
import com.cannyurl.wa.service.UserWebService;


public class ShortUrlListHelper
{
    private static final Logger log = Logger.getLogger(ShortUrlListHelper.class.getName());

    private UserWebService userWebService = null;
    private ShortLinkWebService shortLinkWebService = null;
    // ...

    private ShortUrlListHelper() {}

    // Initialization-on-demand holder.
    private static final class ShortUrlListHelperHolder
    {
        private static final ShortUrlListHelper INSTANCE = new ShortUrlListHelper();
    }

    // Singleton method
    public static ShortUrlListHelper getInstance()
    {
        return ShortUrlListHelperHolder.INSTANCE;
    }
    
    
    private UserWebService getUserWebService()
    {
        if(userWebService == null) {
            userWebService = new UserWebService();
        }
        return userWebService;
    }
    private ShortLinkWebService getShortLinkWebService()
    {
        if(shortLinkWebService == null) {
            shortLinkWebService = new ShortLinkWebService();
        }
        return shortLinkWebService;
    }


    // temporary. Is there a better way?????
    public int getTotalCountForAll()
    {
        return getTotalCountForAll(null, null);
    }
    public int getTotalCountForAll(Long offset, Integer maxCount)
    {
        int totalCount = 0;
        try {
            String filter = null;   // Status?
            String ordering = "createdTime desc";
            List<String> keys = getShortLinkWebService().findShortLinkKeys(filter, ordering, null, null, null, null, offset, maxCount);
            if(keys != null) {
                totalCount = keys.size();
            }
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to find beans.", e);
        }
        return totalCount;
    }

    public List<ShortLinkJsBean> findShortLinksForAll(Long offset, Integer count)
    {
        List<ShortLinkJsBean> shortLinks = null;
        try {
            String filter = null;
            String ordering = "createdTime desc";
            shortLinks = getShortLinkWebService().findShortLinks(filter, ordering, null, null, null, null, offset, count);   
            if(shortLinks != null) {
                int size = shortLinks.size(); 
                // For debugging/diagnostic purposes.
                if(size > 0) { 
                    if(log.isLoggable(Level.INFO)) log.log(Level.INFO, size + " shortLinks found for offset = " + offset + "; count = " + count);
                }
            } else {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "No shortLinks for given offset = " + offset + "; count = " + count);
            }
        } catch (WebException e) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Failed to find shortLinks for given offset = " + offset + "; count = " + count, e);
            return null;
        }

        return shortLinks;
    }
    
    
    // temporary. Is there a better way?????
    public int getTotalCountForUser(String userGuid)
    {
        return getTotalCountForUser(userGuid, null, null);
    }
    public int getTotalCountForUser(String userGuid, Long offset, Integer maxCount)
    {
        int totalCount = 0;
        try {
            String filter = "owner=='" + userGuid + "'";   // Status?
            String ordering = "createdTime desc";
            List<String> keys = getShortLinkWebService().findShortLinkKeys(filter, ordering, null, null, null, null, offset, maxCount);
            if(keys != null) {
                totalCount = keys.size();
            }
        } catch (WebException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to find beans for given user = " + userGuid, e);
        }
        return totalCount;
    }

    public List<ShortLinkJsBean> findShortLinksForUser(String userGuid, Long offset, Integer count)
    {
        List<ShortLinkJsBean> shortLinks = null;
        try {
            // TBD:
            // Return only the shortLinks which were created while the user was logged on ????
            // ... (e.g., use ShortLink.loggedOn field???)
            String filter = "owner=='" + userGuid + "'";
            String ordering = "createdTime desc";
            shortLinks = getShortLinkWebService().findShortLinks(filter, ordering, null, null, null, null, offset, count);   
            if(shortLinks != null) {
                int size = shortLinks.size(); 
                // For debugging/diagnostic purposes.
                if(size > 0) { 
                    log.log(Level.INFO, size + " shortLinks found for userGuid = " + userGuid + "; offset = " + offset + "; count = " + count);
                }
            } else {
                log.log(Level.WARNING, "No shortLinks for given userGuid = " + userGuid + "; offset = " + offset + "; count = " + count);
            }
        } catch (WebException e) {
            log.log(Level.SEVERE, "Failed to find shortLinks for given userGuid = " + userGuid + "; offset = " + offset + "; count = " + count, e);
            return null;
        }

        return shortLinks;
    }

    
    // temporary. Is there a better way?????
    public int getTotalCountForAppClient(String appClient)
    {
        return getTotalCountForAppClient(appClient, null, null);
    }
    public int getTotalCountForAppClient(String appClient, Long offset, Integer maxCount)
    {
        int totalCount = 0;
        try {
            String filter = "appClient=='" + appClient + "'";   // Status?
            String ordering = "createdTime desc";
            List<String> keys = getShortLinkWebService().findShortLinkKeys(filter, ordering, null, null, null, null, offset, maxCount);
            if(keys != null) {
                totalCount = keys.size();
            }
        } catch (WebException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to find beans for given user = " + appClient, e);
        }
        return totalCount;
    }

    public List<ShortLinkJsBean> findShortLinksForAppClient(String appClient, Long offset, Integer count)
    {
        List<ShortLinkJsBean> shortLinks = null;
        try {
            // TBD:
            // Return only the shortLinks which were created while the user was logged on ????
            // ... (e.g., use ShortLink.loggedOn field???)
            String filter = "appClient=='" + appClient + "'";
            String ordering = "createdTime desc";
            shortLinks = getShortLinkWebService().findShortLinks(filter, ordering, null, null, null, null, offset, count);   
            if(shortLinks != null) {
                int size = shortLinks.size(); 
                // For debugging/diagnostic purposes.
                if(size > 0) { 
                    log.log(Level.INFO, size + " shortLinks found for appClient = " + appClient + "; offset = " + offset + "; count = " + count);
                }
            } else {
                log.log(Level.WARNING, "No shortLinks for given appClient = " + appClient + "; offset = " + offset + "; count = " + count);
            }
        } catch (WebException e) {
            log.log(Level.SEVERE, "Failed to find shortLinks for given appClient = " + appClient + "; offset = " + offset + "; count = " + count, e);
            return null;
        }

        return shortLinks;
    }

    
    // temporary. Is there a better way?????
    public int getTotalCountForClientRootDomain(String clientRootDomain)
    {
        return getTotalCountForClientRootDomain(clientRootDomain, null, null);
    }
    public int getTotalCountForClientRootDomain(String clientRootDomain, Long offset, Integer maxCount)
    {
        int totalCount = 0;
        try {
            String filter = "clientRootDomain=='" + clientRootDomain + "'";   // Status?
            String ordering = "createdTime desc";
            List<String> keys = getShortLinkWebService().findShortLinkKeys(filter, ordering, null, null, null, null, offset, maxCount);
            if(keys != null) {
                totalCount = keys.size();
            }
        } catch (WebException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to find beans for given user = " + clientRootDomain, e);
        }
        return totalCount;
    }

    public List<ShortLinkJsBean> findShortLinksForClientRootDomain(String clientRootDomain, Long offset, Integer count)
    {
        List<ShortLinkJsBean> shortLinks = null;
        try {
            // TBD:
            // Return only the shortLinks which were created while the user was logged on ????
            // ... (e.g., use ShortLink.loggedOn field???)
            String filter = "clientRootDomain=='" + clientRootDomain + "'";
            String ordering = "createdTime desc";
            shortLinks = getShortLinkWebService().findShortLinks(filter, ordering, null, null, null, null, offset, count);   
            if(shortLinks != null) {
                int size = shortLinks.size(); 
                // For debugging/diagnostic purposes.
                if(size > 0) { 
                    log.log(Level.INFO, size + " shortLinks found for clientRootDomain = " + clientRootDomain + "; offset = " + offset + "; count = " + count);
                }
            } else {
                log.log(Level.WARNING, "No shortLinks for given clientRootDomain = " + clientRootDomain + "; offset = " + offset + "; count = " + count);
            }
        } catch (WebException e) {
            log.log(Level.SEVERE, "Failed to find shortLinks for given clientRootDomain = " + clientRootDomain + "; offset = " + offset + "; count = " + count, e);
            return null;
        }

        return shortLinks;
    }


}
