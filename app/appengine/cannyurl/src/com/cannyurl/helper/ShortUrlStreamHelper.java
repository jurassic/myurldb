package com.cannyurl.helper;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.cannyurl.app.stream.helper.ShortUrlStreamServiceHelper;
import com.cannyurl.fe.bean.CellLatitudeLongitudeJsBean;
import com.cannyurl.fe.bean.ShortLinkJsBean;
import com.cannyurl.wa.service.ShortLinkWebService;
import com.cannyurl.wa.util.CellLatitudeLongitudeWebUtil;
import com.myurldb.ws.CellLatitudeLongitude;
import com.myurldb.ws.ShortLink;


// Note:
// Notice that we use JsBeans here not AfBeans...
// ....
public class ShortUrlStreamHelper
{
    private static final Logger log = Logger.getLogger(ShortUrlStreamHelper.class.getName());


    private ShortUrlStreamHelper() {}

    // Initialization-on-demand holder.
    private static final class ShortUrlStreamHelperHolder
    {
        private static final ShortUrlStreamHelper INSTANCE = new ShortUrlStreamHelper();
    }

    // Singleton method
    public static ShortUrlStreamHelper getInstance()
    {
        return ShortUrlStreamHelperHolder.INSTANCE;
    }
    
    
    // TBD:
    public static ShortLinkJsBean convertShortLinkToJsBean(ShortLink shortlink)
    {
        if(shortlink == null) {
            return null;
        }
        ShortLinkJsBean jsBean = null;
        if(shortlink instanceof ShortLink) {
            jsBean = ShortLinkWebService.convertShortLinkToJsBean((ShortLink) shortlink);
        } else {
            // ????
        }        
        return jsBean;
    }
    


    // TBD: Check all ShortLink subclasses ????
//    public ShortLinkJsBean getShortLink(String guid)
//    {
//        // ???
//    }
    
    public ShortLinkJsBean getShortLink(String guid)
    {
        ShortLink shortlink = ShortUrlStreamServiceHelper.getInstance().getShortLink(guid);
        ShortLinkJsBean shortLinkBean = null;
        if(shortlink != null) {
            shortLinkBean = ShortLinkWebService.convertShortLinkToJsBean((ShortLink) shortlink);
        } else {
        	// ???
        }
        return shortLinkBean;
    }


    
    public int getShortLinkCount(CellLatitudeLongitudeJsBean geoCell, String termType, String dayHour)
    {
        CellLatitudeLongitude geoCellBean = null;
        if(geoCell != null) {
            geoCellBean = CellLatitudeLongitudeWebUtil.convertCellLatitudeLongitudeJsBeanToBean(geoCell);
        }
        return ShortUrlStreamServiceHelper.getInstance().getShortLinkCount(geoCellBean, termType, dayHour);
    }


    // TBD: Allow multiple geoCell input....
    public List<String> fetchShortLinkKeys(CellLatitudeLongitudeJsBean geoCell, String termType, String dayHour)
    {
        CellLatitudeLongitude geoCellBean = null;
        if(geoCell != null) {
            geoCellBean = CellLatitudeLongitudeWebUtil.convertCellLatitudeLongitudeJsBeanToBean(geoCell);
        }
        return ShortUrlStreamServiceHelper.getInstance().fetchShortLinkKeys(geoCellBean, termType, dayHour);
    }
    public List<ShortLinkJsBean> fetchShortLinks(CellLatitudeLongitudeJsBean geoCell, String termType, String dayHour)
    {
        return fetchShortLinks(geoCell, termType, dayHour, false);
    }
    public List<ShortLinkJsBean> fetchShortLinks(CellLatitudeLongitudeJsBean geoCell, String termType, String dayHour, boolean fullFetch)
    {
        List<ShortLinkJsBean> shortLinkBeans = new ArrayList<ShortLinkJsBean>();
        CellLatitudeLongitude geoCellBean = null;
        if(geoCell != null) {
            geoCellBean = CellLatitudeLongitudeWebUtil.convertCellLatitudeLongitudeJsBeanToBean(geoCell);
        }
        List<ShortLink> shortLinkList = ShortUrlStreamServiceHelper.getInstance().fetchShortLinks(geoCellBean, termType, dayHour, fullFetch);
        if(shortLinkList != null && !shortLinkList.isEmpty()) {
            for(ShortLink a : shortLinkList) {
                ShortLinkJsBean bean = convertShortLinkToJsBean(a);
                shortLinkBeans.add(bean);
            }
        }
        return shortLinkBeans;
    }
    
    
    public int getShortLinkCount(CellLatitudeLongitudeJsBean geoCell, Long startTime)
    {
        return getShortLinkCount(geoCell, startTime, null);  // null == no limit
    }
    public int getShortLinkCount(CellLatitudeLongitudeJsBean geoCell, Long startTime, Integer maxCount)
    {
        CellLatitudeLongitude geoCellBean = null;
        if(geoCell != null) {
            geoCellBean = CellLatitudeLongitudeWebUtil.convertCellLatitudeLongitudeJsBeanToBean(geoCell);
        }
        return ShortUrlStreamServiceHelper.getInstance().getShortLinkCount(geoCellBean, startTime, maxCount);
    }


    
    // TBD: Allow multiple geoCell input....
    public List<String> fetchShortLinkKeys(CellLatitudeLongitudeJsBean geoCell, Long startTime)
    {
        return fetchShortLinkKeys(geoCell, startTime, (Integer) null);  // null == no limit
    }
    public List<String> fetchShortLinkKeys(CellLatitudeLongitudeJsBean geoCell, Long startTime, Integer maxCount)
    {
        CellLatitudeLongitude geoCellBean = null;
        if(geoCell != null) {
            geoCellBean = CellLatitudeLongitudeWebUtil.convertCellLatitudeLongitudeJsBeanToBean(geoCell);
        }
        return ShortUrlStreamServiceHelper.getInstance().fetchShortLinkKeys(geoCellBean, startTime, maxCount);
    }

    public List<String> fetchShortLinkKeys(CellLatitudeLongitudeJsBean geoCell, Long startTime, Long endTime)
    {
        CellLatitudeLongitude geoCellBean = null;
        if(geoCell != null) {
            geoCellBean = CellLatitudeLongitudeWebUtil.convertCellLatitudeLongitudeJsBeanToBean(geoCell);
        }
        return ShortUrlStreamServiceHelper.getInstance().fetchShortLinkKeys(geoCellBean, startTime, endTime);
    }


    
    
    // TBD:
    // ....

    
    // TBD: Allow multiple geoCell input....
    public List<ShortLinkJsBean> fetchShortLinks(CellLatitudeLongitudeJsBean geoCell, Long startTime)
    {
        return fetchShortLinks(geoCell, startTime, (Integer) null);  // null == no limit
    }
    public List<ShortLinkJsBean> fetchShortLinks(CellLatitudeLongitudeJsBean geoCell, Long startTime, Integer maxCount)
    {
        return fetchShortLinks(geoCell, startTime, maxCount, false);
    }
    public List<ShortLinkJsBean> fetchShortLinks(CellLatitudeLongitudeJsBean geoCell, Long startTime, Integer maxCount, boolean fullFetch)
    {
        List<ShortLinkJsBean> shortLinkBeans = new ArrayList<ShortLinkJsBean>();
        CellLatitudeLongitude geoCellBean = null;
        if(geoCell != null) {
            geoCellBean = CellLatitudeLongitudeWebUtil.convertCellLatitudeLongitudeJsBeanToBean(geoCell);
        }
        List<ShortLink> shortLinkList = ShortUrlStreamServiceHelper.getInstance().fetchShortLinks(geoCellBean, startTime, maxCount, fullFetch);
        if(shortLinkList != null && !shortLinkList.isEmpty()) {
            for(ShortLink a : shortLinkList) {
                ShortLinkJsBean bean = convertShortLinkToJsBean(a);
                shortLinkBeans.add(bean);
            }
        }
        return shortLinkBeans;
    }

    public List<ShortLinkJsBean> fetchShortLinks(CellLatitudeLongitudeJsBean geoCell, Long startTime, Long endTime)
    {
        return fetchShortLinks(geoCell, startTime, endTime, false);
    }
    public List<ShortLinkJsBean> fetchShortLinks(CellLatitudeLongitudeJsBean geoCell, Long startTime, Long endTime, boolean fullFetch)
    {
        List<ShortLinkJsBean> shortLinkBeans = new ArrayList<ShortLinkJsBean>();
        CellLatitudeLongitude geoCellBean = null;
        if(geoCell != null) {
            geoCellBean = CellLatitudeLongitudeWebUtil.convertCellLatitudeLongitudeJsBeanToBean(geoCell);
        }
        List<ShortLink> shortLinkList = ShortUrlStreamServiceHelper.getInstance().fetchShortLinks(geoCellBean, startTime, endTime, fullFetch);
        if(shortLinkList != null && !shortLinkList.isEmpty()) {
            for(ShortLink a : shortLinkList) {
                ShortLinkJsBean bean = convertShortLinkToJsBean(a);
                shortLinkBeans.add(bean);
            }
        }
        return shortLinkBeans;
    }


}
