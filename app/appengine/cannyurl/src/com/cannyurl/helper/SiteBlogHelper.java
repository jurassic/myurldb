package com.cannyurl.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.cannyurl.af.util.PathInfoUtil;


public class SiteBlogHelper
{
    private static final Logger log = Logger.getLogger(SiteBlogHelper.class.getName());
    
    // This should match the URL...
    // URL pattern: .../abc-xyz
    // (Currently, everything after the first path segment, e.g., "/abc-xyz", is ignored including query string...)
    // "Code" is "abc-xyz"... etc...
    

    // pathInfo -> "code"
    Map<String, String> mPathInfoMap;
    private SiteBlogHelper() 
    {
       mPathInfoMap = new HashMap<String, String>();
    }

    // Initialization-on-demand holder.
    private static final class SiteBlogHelperHolder
    {
        private static final SiteBlogHelper INSTANCE = new SiteBlogHelper();
    }

    // Singleton method
    public static SiteBlogHelper getInstance()
    {
        return SiteBlogHelperHolder.INSTANCE;
    }

    
    private void parsePathInfo(String pathInfo)
    {
    	if(mPathInfoMap.containsKey(pathInfo)) {
    		// nothing to do.
    	} else {
    		String code = null;
    		Map<String, String> map = PathInfoUtil.parsePathInfo(pathInfo, 1); 
    		if(map != null && !map.isEmpty()) {
    		    code = map.keySet().iterator().next();  // ???
    		}
    		mPathInfoMap.put(pathInfo, code);    		
    	}
    }

    public String getCodeForSiteBlog(String pathInfo)
    {
    	parsePathInfo(pathInfo);
    	return mPathInfoMap.get(pathInfo);
    }

}
