package com.cannyurl.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cannyurl.af.util.PathInfoUtil;


public class SiteHelpHelper
{
    private static final Logger log = Logger.getLogger(SiteHelpHelper.class.getName());
    
    // This should match the URL...
    // URL pattern: .../abc-xyz
    // (Currently, everything after the first path segment, e.g., "/abc-xyz", is ignored including query string...)
    // "Code" is "abc-xyz"... etc...
    

    // pathInfo -> "code"
    Map<String, String> mPathInfoMap;
    private SiteHelpHelper() 
    {
       mPathInfoMap = new HashMap<String, String>();
    }

    // Initialization-on-demand holder.
    private static final class SiteHelpHelperHolder
    {
        private static final SiteHelpHelper INSTANCE = new SiteHelpHelper();
    }

    // Singleton method
    public static SiteHelpHelper getInstance()
    {
        return SiteHelpHelperHolder.INSTANCE;
    }

    
    private void parsePathInfo(String pathInfo)
    {
    	if(mPathInfoMap.containsKey(pathInfo)) {
    		// nothing to do.
    	} else {
    		String code = null;
    		Map<String, String> map = PathInfoUtil.parsePathInfo(pathInfo, 1); 
    		if(map != null && !map.isEmpty()) {
    		    code = map.keySet().iterator().next();  // ???
    		    if(log.isLoggable(Level.INFO)) log.info("pathInfo = " + pathInfo + "; code = " + code);
    		}
    		mPathInfoMap.put(pathInfo, code);
    	}
    }

    public String getCodeForSiteHelp(String pathInfo)
    {
    	parsePathInfo(pathInfo);
    	return mPathInfoMap.get(pathInfo);
    }

}
