package com.cannyurl.helper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cannyurl.af.util.PathInfoUtil;
import com.cannyurl.fe.bean.SiteLogJsBean;


// Note:
// Due to the difficulty in implementing this for general use cases...
// We make a few assumptions:
// (1) All site log files should be in the form of yyyy-mm-dd-<appBrand> or yyyy-mm-dd-all
// (2) We can only support log files that are included in ALL app brands or in a single appBrand.
// ....
public class SiteLogHelper
{
    private static final Logger log = Logger.getLogger(SiteLogHelper.class.getName());
    
    public static final String SITE_LOG_DIR = "site-logs";    // Under "/war"
    
    // This should match the URL...
    // URL pattern: .../abc-xyz-<brand>
    // (Currently, everything after the first path segment, e.g., "/abc-xyz-<brand>", is ignored including query string...)
    // (Note that this makes it very convenient to add additional text after the code-brand, e.g., title, etc....)
    // "Code" is "abc-xyz" before the appBrand/all... etc...

    // pathInfo -> "code"
    Map<String, String> mPathInfoMap;

    // Help file location
    private String siteLogDir = null;

    
    private SiteLogHelper() 
    {
        // temporary
        siteLogDir = SITE_LOG_DIR; 
        mPathInfoMap = new HashMap<String, String>();
    }
    

    // Initialization-on-demand holder.
    private static final class SiteLogHelperHolder
    {
        private static final SiteLogHelper INSTANCE = new SiteLogHelper();
    }

    // Singleton method
    public static SiteLogHelper getInstance()
    {
        return SiteLogHelperHolder.INSTANCE;
    }


    public int getSiteLogCountForAppBrand(final String appBrand)
    {
        int count = 0;
        File dir = new File(siteLogDir);   // ???
        if(dir.exists() && dir.isDirectory()) {
            String[] files = dir.list(new FilenameFilter() {
                @Override
                public boolean accept(File directory, String filename)
                {
                    if(appBrand == null || filename.endsWith("-" + appBrand + ".txt") || filename.endsWith("-all.txt")) {   // ????
                        return true;
                    } else {
                        return false;
                    }
                }
            });
            if(files != null) {
                count = files.length;
            } else {
                // ???
            }
        } else {
            // ???
            if(log.isLoggable(Level.WARNING)) log.warning("Invalid directory path: " + siteLogDir);
        }
        return count;
    }
    
    // TBD:
    public List<String> getSiteLogCodesForAppBrand(final String appBrand)
    {
        List<String> siteCodeLogs = null;

        File dir = new File(siteLogDir);   // ???
        if(dir.exists() && dir.isDirectory()) {
            File[] files = dir.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File directory, String filename)
                {
                    if(appBrand == null || filename.endsWith("-" + appBrand + ".txt") || filename.endsWith("-all.txt")) {   // ????
                        return true;
                    } else {
                        return false;
                    }
                }
            });
            if(files != null) {
                int count = files.length;

                List<String> list = new ArrayList<String>();
                for(File f : files) {
                    String name = f.getName();
                    String code = null;
                    String brand = null;
                    String[] parsed = getCodeAndAppBrandFromFileName(name);
                    if(parsed != null && parsed.length > 0) {
                        code = parsed[0];
                        if(parsed.length > 1) {
                            brand = parsed[1];
                        }
                    }
                    if((code != null && !code.isEmpty()) && (brand == null || brand.isEmpty() || brand.equals(appBrand) || brand.equals("all"))) {
                        list.add(code);
                    } else {
                        // ????
                        // Can this happen ?????
                    }
                }
                
                Collections.sort(list, new Comparator<String>() {
                    @Override
                    public int compare(String o1, String o2)
                    {
                        // Reverse chronological ordering....
                        if(o1 == null && o2 == null) {
                            return 0;
                        }
                        if(o1 == null) {
                            return 1;
                        }
                        return -1 * o1.compareTo(o2);
                    }
                });
                
                siteCodeLogs = list;
            } else {
                // ???
            }
        } else {
            // ??? What to do??
            if(log.isLoggable(Level.WARNING)) log.warning("Invalid directory path: " + siteLogDir);
        }

        return siteCodeLogs;
    }

    // TBD:
    public List<SiteLogJsBean> getSiteLogsForAppBrand(final String appBrand)
    {
        List<SiteLogJsBean> siteLogList = null;
        siteLogList = new ArrayList<SiteLogJsBean>();
    
        List<String> codes = getSiteLogCodesForAppBrand(appBrand);
        if(codes != null && !codes.isEmpty()) {
            for(String c : codes) {
                SiteLogJsBean bean = getSiteLog(c, appBrand);
                if(bean != null) {
                    siteLogList.add(bean);
                } else {
                    // ???
                }
            }
        }

        return siteLogList;
    }

    
    
    // Despite the comment in the top of this class,
    // We assume this more general file name pattern:
    // <code>-<appBrand>.txt, where <appBrand> can be "all".
    private String[] getCodeAndAppBrandFromFileName(String name)
    {
        if(name == null || name.isEmpty()) {
            return null;
        }
        String code = null; 
        String appBrand = null;
        int idx1 = name.lastIndexOf(".txt");
        String root = null;
        if(idx1 > 0) {
            root = name.substring(0, idx1);
        } else {
            root = name;
        }
        int idx2 = root.lastIndexOf("-");
        if(idx2 > 0) {
            code = root.substring(0, idx2);
            appBrand = root.substring(idx2+1);
        } else {
            code = root;
            appBrand = null;
        }
        return new String[]{code, appBrand};
    }

    private void parsePathInfo(String pathInfo)
    {
    	if(mPathInfoMap.containsKey(pathInfo)) {
    		// nothing to do.
    	} else {
    		String code = null;
    		Map<String, String> map = PathInfoUtil.parsePathInfo(pathInfo, 1); 
    		if(map != null) {
    		    code = map.keySet().iterator().next();  // ???
    		}
    		mPathInfoMap.put(pathInfo, code);    		
    	}
    }

    public String getCodeForSiteLog(String pathInfo)
    {
    	parsePathInfo(pathInfo);
    	return mPathInfoMap.get(pathInfo);
    }


    // Cache
    // code -> bean...
    // TBD: Just use memcache ????
    // private Map<String, SiteLogJsBean> siteLogMap = new HashMap<String, SiteLogJsBean>();
    // ..

    // TBD: Just use MemCache ???
    public SiteLogJsBean getSiteLog(String code, String appBrand)
    {
        //if(siteLogMap.containsKey(code)) {
        //    return siteLogMap.get(code);
        //}
        
        SiteLogJsBean siteLog = readSiteLog(code, appBrand);
        //if(siteLog != null) {
        //    siteLogMap.put(code, siteLog);
        //}
        
        // TBD
        return siteLog;
    }

    
    // Temporary
    private static final String FIELD_UUID = "UUID";
    private static final String FIELD_PUBDATE = "PUBDATE";
    private static final String FIELD_TITLE = "TITLE";
    private static final String FIELD_FORMAT = "FORMAT";
    private static final String FIELD_CONTENT = "CONTENT";
    private static final String MARKER_CONTENT_END  = "::END_CONTENT::";
    // etc...

    
    // TBD:
    // code vs. uuid ???
    // Note: uuid is not really needed....
    private SiteLogJsBean readSiteLog(String code, String appBrand)
    {
        if(log.isLoggable(Level.FINE)) log.fine("readSiteLog() called with code = " + code + "; appBrand = " + appBrand);

        SiteLogJsBean siteLog = null;

        // temporary
        // TBD: URL unescape the code????
        if(appBrand == null) {
            appBrand = "all";    // ???
        }
        // ....
        String filename = null;
        BufferedReader reader = null;
        try {
            InputStream is = null;
            try {
                filename = siteLogDir + "/" + code + "-" + appBrand + ".txt";   // ???
                is = new FileInputStream(filename);
            } catch (FileNotFoundException e) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "File not found error: filename = " + filename, e);
                // Try one more time....
                filename = siteLogDir + "/" + code + "-all.txt";   // ???
                is = new FileInputStream(filename);
                // Note that no catch here....
            }

            reader = new BufferedReader(new InputStreamReader(is));
            
            String uuid = null;
            String pubDate = null;
            String title = null;
            String format = null;
            String content = null;
            
            String line = null;
            boolean inContent = false;
            StringBuilder contentBuilder = new StringBuilder();
            while((line = reader.readLine()) != null) {
                // Note that if the content field is the last field,
                // the marker is not needed...
                if((inContent == true) && line.trim().equals(MARKER_CONTENT_END)) {
                    inContent = false;
                    continue;
                }
                if(inContent == true) {
                    contentBuilder.append(line).append("\n");   // ??? new line????
                    continue;
                }
                line = line.trim();
                if(line.startsWith("#")) {   // Comment line...
                    continue;
                }
                if(line.isEmpty()) {
                    // Skip empty lines.
                    continue;
                }
                
                String[] cols = line.split("\\s*:\\s*", 2);
                if(cols == null || cols.length == 0) {
                    continue;
                } else if(cols.length == 1) {
                    if(cols[0].equals(FIELD_CONTENT)) {
                        inContent = true;
                    } else {
                        // ignore
                    }
                } else {
                    if(cols[0].equals(FIELD_UUID)) {
                        uuid = cols[1].trim();
                    } else if(cols[0].equals(FIELD_PUBDATE)) {
                        pubDate = cols[1].trim();
                    } else if(cols[0].equals(FIELD_TITLE)) {
                        title = cols[1].trim();
                    } else if(cols[0].equals(FIELD_FORMAT)) {
                        format = cols[1].trim();   // Validation ???
                    } else if(cols[0].equals(FIELD_CONTENT)) {
                        inContent = true;
                    } else {
                        // ????
                    }
                }
            }
            content = contentBuilder.toString();

            siteLog = new SiteLogJsBean();
            if(uuid != null) {
                siteLog.setUuid(uuid);
            }
            if(pubDate != null) {
                siteLog.setPubDate(pubDate);
            }
            if(title != null) {
                siteLog.setTitle(title);
            }
            if(format == null || format.isEmpty()) {   // TBD: isValidFormat() ???
                format = "text";   // temporary
            }
            siteLog.setFormat(format);
            siteLog.setContent(content);
            // ...            
        } catch (FileNotFoundException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "File not found error: filename = " + filename, e);            
        } catch (IOException e) {
            log.log(Level.WARNING, "Error: filename = " + filename, e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Unknown error: filename = " + filename, e);
        } finally {
            if(reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    log.log(Level.WARNING, "Error while closing the reader", e);
                }
            }
        }

        if(log.isLoggable(Level.FINE)) {
            if(siteLog != null) {
                log.fine("siteLog = " + siteLog);
            }
        }
        
        return siteLog;
    }
    
}
