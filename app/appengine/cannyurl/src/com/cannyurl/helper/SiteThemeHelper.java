package com.cannyurl.helper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.sf.jsr107cache.Cache;

import com.cannyurl.af.config.Config;
import com.cannyurl.af.util.CacheHelper;


// temporary
public class SiteThemeHelper
{
    private static final Logger log = Logger.getLogger(SiteThemeHelper.class.getName());
    private static Random sRandom = new Random(System.currentTimeMillis());

    private static final String KEY_THEME_BOOTSTRAP = "cannyurlapp.ui.themelist.bootstrap";
    private static final String KEY_THEME_JQUERYUI = "cannyurlapp.ui.themelist.jqueryui";

    private List<String> bootstrapThemeList = new ArrayList<String>();
    private List<String> jqueryUiThemeList = new ArrayList<String>();

    private SiteThemeHelper() 
    {
        initThemes();
    }

    // Initialization-on-demand holder.
    private static final class ThemeHelperHolder
    {
        private static final SiteThemeHelper INSTANCE = new SiteThemeHelper();
    }

    // Singleton method
    public static SiteThemeHelper getInstance()
    {
        return ThemeHelperHolder.INSTANCE;
    }

    // temporary
    private Cache getCache()
    {
        // return CacheHelper.getLongInstance().getCache();
        // return CacheHelper.getHourLongInstance().getCache();
        // return CacheHelper.getDayLongInstance().getCache();
        return CacheHelper.getWeekLongInstance().getCache();
    }
    private static String getBootstrapThemeCacheKey()
    {
        return "BootstrapTheme";
    }
    private static String getJqueryUiThemeCacheKey()
    {
        return "JqueryUiTheme";
    }


    private void initThemes()
    {
        String bootstrapThemes = Config.getInstance().getString(KEY_THEME_BOOTSTRAP, "");
        String[] bootstrapArr = bootstrapThemes.split(",\\s*");
        bootstrapThemeList.addAll(Arrays.asList(bootstrapArr));            
        if(log.isLoggable(Level.INFO)) {
            log.log(Level.INFO, "Bootstrap theme list read from the config: ");
            for(String d : bootstrapThemeList) {
                log.log(Level.INFO, "-- bootstrap theme: " + d);
            }
        }
        String jqueryUiThemes = Config.getInstance().getString(KEY_THEME_JQUERYUI, "");
        String[] jqueryUiArr = jqueryUiThemes.split(",\\s*");
        jqueryUiThemeList.addAll(Arrays.asList(jqueryUiArr));            
        if(log.isLoggable(Level.INFO)) {
            log.log(Level.INFO, "Jquery UI theme list read from the config: ");
            for(String d : jqueryUiThemeList) {
                log.log(Level.INFO, "-- jqueryUi theme: " + d);
            }
        }
    }

    // TBD
    public final String getRandomBootstrapTheme()
    {
        int idx = sRandom.nextInt(bootstrapThemeList.size());
        return bootstrapThemeList.get(idx);
    }
    public final String getRandomJqueryUiTheme()
    {
        int idx = sRandom.nextInt(jqueryUiThemeList.size());
        return jqueryUiThemeList.get(idx);
    }

    public String getBootstrapTheme()
    {
        String theme = null;
        if(getCache() != null) {
            theme = (String) getCache().get(getBootstrapThemeCacheKey());
        }
        if(theme == null) {
            theme = getRandomBootstrapTheme();
            if(theme != null) {
                if(getCache() != null) {
                    getCache().put(getBootstrapThemeCacheKey(), theme);
                }
            }
        }
//      if(theme == null) {
//      // Use default theme???
//  }
        return theme;
    }
    public String getJqueryUiTheme()
    {
        String theme = null;
        if(getCache() != null) {
            theme = (String) getCache().get(getJqueryUiThemeCacheKey());
        }
        if(theme == null) {
            theme = getRandomJqueryUiTheme();
            if(theme != null) {
                if(getCache() != null) {
                    getCache().put(getJqueryUiThemeCacheKey(), theme);
                }
            }
        }
//      if(theme == null) {
//      // Use default theme???
//  }
        return theme;
    }

}
