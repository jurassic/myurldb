package com.cannyurl.helper;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.ShortLinkJsBean;
import com.cannyurl.wa.service.ShortLinkWebService;
import com.cannyurl.wa.service.UserWebService;


public class SitemapHelper
{
    private static final Logger log = Logger.getLogger(SitemapHelper.class.getName());
    
    // temporary
    private static final int MAX_MEMO_COUNT = 5000;   // Sitemap.xml max count...
    private static final int DEFAULT_MAX_MEMO_COUNT = 250;
    // temporary

    private UserWebService userWebService = null;
    private ShortLinkWebService shortLinkWebService = null;
    // ...

    private SitemapHelper() {}

    // Initialization-on-demand holder.
    private static final class SitemapHelperHolder
    {
        private static final SitemapHelper INSTANCE = new SitemapHelper();
    }

    // Singleton method
    public static SitemapHelper getInstance()
    {
        return SitemapHelperHolder.INSTANCE;
    }
    
    
    private UserWebService getUserWebService()
    {
        if(userWebService == null) {
            userWebService = new UserWebService();
        }
        return userWebService;
    }
    private ShortLinkWebService getShortLinkWebService()
    {
        if(shortLinkWebService == null) {
            shortLinkWebService = new ShortLinkWebService();
        }
        return shortLinkWebService;
    }

    
    // TBD:
    // Take appBrand as an argument ????
    // (or, topLevelUrl, etc....)
    // ....
    
    public List<ShortLinkJsBean> findRecentShortLinks()
    {
    	return findRecentShortLinks(null);
    }
    public List<ShortLinkJsBean> findRecentShortLinks(Integer maxCount)
    {
    	return findRecentShortLinks(null, maxCount);
    }
    public List<ShortLinkJsBean> findRecentShortLinks(Long listOffset, Integer maxCount)
    {
        List<ShortLinkJsBean> shortLinks = null;
        try {
            String filter = null;
            String ordering = "createdTime desc";
            Long offset = 0L;
            if(listOffset != null) {
            	offset = listOffset;
            }
            Integer count = DEFAULT_MAX_MEMO_COUNT;
            if(maxCount != null) {
            	count = maxCount;   // TBD: Validation??? ( maxCount < MAX_MEMO_COUNT ???? )
            }
            shortLinks = getShortLinkWebService().findShortLinks(filter, ordering, null, null, null, null, offset, count);    
        } catch (WebException e) {
            log.log(Level.SEVERE, "Failed to find shortLinks.", e);
            return null;
        }

        return shortLinks;
    }

    
    // Format the timestamp to W3C date format: "yyyy-mm-dd".
    public static String formatDate(Long time)
    {
        if(time == null) {
            return null;  // ???
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String date = dateFormat.format(new Date(time));
        return date;
    }
    
}
