package com.cannyurl.helper;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.cannyurl.common.TableSortedStateBean;


// Utility methods for managing 
//   both "session" (browser session) and "persistent session" (sessionCookie).
public class TableSortOrderHelper
{
    private static final Logger log = Logger.getLogger(TableSortOrderHelper.class.getName());
    
    public static final String SESSION_ATTR_TABLESORTORDER_BEAN = "com.cannyurl.tablesortorder";

    private TableSortOrderHelper()
    {
        // TBD.
    }

    // Initialization-on-demand holder.
    private static final class TableSortOrderHelperHolder
    {
        private static final TableSortOrderHelper INSTANCE = new TableSortOrderHelper();
    }

    // Singleton method
    public static TableSortOrderHelper getInstance()
    {
        return TableSortOrderHelperHolder.INSTANCE;
    }

    
    // paramOrdering == "field" | "field asc" | "field desc"
    // Returns zero or one element map: field -> int, where int == +1 if asc, -1 if desc.
    public static Map<String, Integer> parseParamOrdering(String paramOrdering)
    {
    	Map<String, Integer> map = new HashMap<String, Integer>();
    	if(paramOrdering != null) {
    		paramOrdering = paramOrdering.trim();
        	if(!paramOrdering.isEmpty()) {
        		String[] parts = paramOrdering.split("\\s+", 2);
        		Integer colOrder = null;
        		if(parts.length == 1) {
        			colOrder = 1;   // Asc, by default.
        		} else if(parts.length == 2) {
        			if(parts[1].equalsIgnoreCase("desc")) {
        				colOrder = -1;
        			} else {   // Everything else... including "asc"...
        				colOrder = 1;
        			}
        		}
    			map.put(parts[0], colOrder);
        	}
    	}
    	return map;
    }

    
    public TableSortedStateBean getTableSortedState(HttpServletRequest request)
    {
        HttpSession session = request.getSession(true);
        if(session == null) {
            log.warning("Session arg is null. Cannot set TableSortedStateBean.");
            return null;
        }
        TableSortedStateBean sBean = setTableSortedStateBean(request);
        return sBean;
    }

    public TableSortedStateBean setTableSortedStateBean(HttpServletRequest request)
    {
        HttpSession session = request.getSession(true);
        if(session == null) {
            log.warning("Session arg is null. Cannot set TableSortedStateBean.");
            return null;   // TBD: Throw exception?
        }

        TableSortedStateBean sBean = null;
        Object obj = session.getAttribute( SESSION_ATTR_TABLESORTORDER_BEAN );
        if(obj != null) {
            sBean = (TableSortedStateBean) obj;
            if(log.isLoggable(Level.FINE)) log.fine("tableSortedStateBean found in the current session: " + sBean);
        } else {
            // Ignore
            log.info("tableSortedStateBean object not found in the current session.");
        }

        if(sBean == null) {
            // Create a new tableSortedStateBean.
            sBean = new TableSortedStateBean();
            session.setAttribute( SESSION_ATTR_TABLESORTORDER_BEAN, sBean );
            if(log.isLoggable(Level.INFO)) log.info("Setting session.tableSortedStateBean to " + sBean);
        }

        return sBean;
    }

}
