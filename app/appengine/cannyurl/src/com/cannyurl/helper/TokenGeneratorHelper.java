package com.cannyurl.helper;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.cannyurl.app.util.ConfigUtil;
import com.cannyurl.app.util.TokenGeneratorUtil;
import com.cannyurl.app.util.TokenLockHelper;
import com.cannyurl.common.TokenGenerationMethod;
import com.cannyurl.fe.WebException;
import com.myurldb.ws.BaseException;


// temporary
public class TokenGeneratorHelper
{
    private static final Logger log = Logger.getLogger(TokenGeneratorHelper.class.getName());

    private TokenGeneratorHelper() 
    {
    }

    // Initialization-on-demand holder.
    private static final class TokenGeneratorHelperHolder
    {
        private static final TokenGeneratorHelper INSTANCE = new TokenGeneratorHelper();
    }

    // Singleton method
    public static TokenGeneratorHelper getInstance()
    {
        return TokenGeneratorHelperHolder.INSTANCE;
    }


    public String generateRandomToken(String tokenType, String sassyTokenType)
    {
        return TokenGeneratorUtil.generateRandomToken(tokenType, sassyTokenType);
    }
    
    
    public String generateNextSequentialToken(String domain, String tokenType) // throws WebException
    {
        String token = null;
        try {
            token = TokenGeneratorUtil.generateNextSequentialToken(domain, tokenType);
            int cnt = 0;
            final int max_tries = 10;   // temporary
            // what happens if token == null????
            while(token != null && TokenLockHelper.getInstance().isShortUrlTokenLocked(domain, token) == true && cnt++ < max_tries) {
                token = TokenGeneratorUtil.generateNextSequentialToken(domain, tokenType, token);
                if(log.isLoggable(Level.FINE)) log.fine("New token generated because the previous token was locked: token = " + token);
            }
            if(token != null) {
                // Lock it for 30 seconds...
                // Note that we do not unlock it.
                TokenLockHelper.getInstance().lockShortUrlToken(domain, token);
                if(log.isLoggable(Level.INFO)) log.info("Token has been locked: token = " + token);
            } else {
                log.warning("Failed to generate a sequential token.");
            }
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to generate a sequential token.", e);
            // throw new WebException(e);
        }
        return token;
    }
    
    // TBD:
    // How to return tokenType = TokenType.TYPE_SEQUENTIAL... ????
    public String generateToken(String domain, String tokenType, String sassyTokenType) // throws WebException
    {
        // TBD:
        String tokenGenerationMethod = ConfigUtil.getTokenGenerationMethod();
        boolean isUniqueToken = ConfigUtil.isTokenGenerationUniqueToken();
        // ....
        
        // TBD:
        String token = null;
        if(isUniqueToken == true) {
            final int max_queries = 5;   // temporary
            boolean isUnique = false;
            int query_count = 0;
            while(isUnique == false && ++query_count <= max_queries) {
                if(TokenGenerationMethod.METHOD_SEQUENTIAL.equals(tokenGenerationMethod)) {
                    token = generateNextSequentialToken(domain, tokenType);
                    // tokenType = TokenType.TYPE_SEQUENTIAL;
                } else {  // if(TokenGenerationMethod.METHOD_RANDOM.equals(tokenGenerationMethod)) {
                    token = generateRandomToken(tokenType, sassyTokenType);   
                }
                try {
                    String existingShortUrlKey = ShortLinkHelper.getInstance().findShortLinkKeyByToken(token, domain);
                    if(existingShortUrlKey != null) {
                        if(log.isLoggable(Level.FINE)) log.fine("No existing shortUrl found for token = " + token + "; domain = " + domain);
                        isUnique = true;
                    } else {
                        if(log.isLoggable(Level.FINE)) log.fine("shortUrl already exists for token = " + token + "; domain = " + domain + ". Trying a new token... query_count = " + query_count);
                    }
                } catch(WebException ex) {
                    // Error occurred while querying...
                    // In such a case, existingShortUrlKey == null does not mean the shortUrl with given token/domain does not exist.
                    // What to do???
                    log.log(Level.WARNING, "Short URL query failed for token = " + token + "; domain = " + domain + "; query_count = " + query_count);
                }
            }
            if(isUnique == false) {
                // Couldn't find the unique token ....
                log.warning("Failed to find a unique token for tokenType = " + tokenType + "; domain = " + domain);
                // throw new WebException("Failed to find a unique token for tokenType = " + tokenType + " and domain = " + domain);
            } else {
                if(log.isLoggable(Level.INFO)) log.info("Unique token found: token = " + token + "; domain = " + domain);
            }
        } else {
            if(TokenGenerationMethod.METHOD_SEQUENTIAL.equals(tokenGenerationMethod)) {
                token = generateNextSequentialToken(domain, tokenType);
                // tokenType = TokenType.TYPE_SEQUENTIAL;
                if(log.isLoggable(Level.INFO)) log.info("Next sequential token generated: token = " + token + "; domain = " + domain);
            } else {  // if(TokenGenerationMethod.METHOD_RANDOM.equals(tokenGenerationMethod)) {
                token = generateRandomToken(tokenType, sassyTokenType);
                if(log.isLoggable(Level.INFO)) log.info("Random token generated: token = " + token + "; domain = " + domain);
            }
        }
 
        return token;
    }
    
    
    
}
