package com.cannyurl.helper;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;

import com.cannyurl.af.util.URLUtil;


// Note: This URLHelper is different from those of other projects....
// Note: http://docs.oracle.com/javase/tutorial/networking/urls/urlInfo.html
public class UrlHelper
{
    private static final Logger log = Logger.getLogger(UrlHelper.class.getName());

    private UrlHelper() {}

    // Initialization-on-demand holder.
    private static final class URLHelperHolder
    {
        private static final UrlHelper INSTANCE = new UrlHelper();
    }

    // Singleton method
    public static UrlHelper getInstance()
    {
        return URLHelperHolder.INSTANCE;
    }

    
    public String getGuidFromPathInfo(String pathInfo)
    {
        String guid = null;
        if(pathInfo != null && !pathInfo.isEmpty()) {
            if(pathInfo.startsWith("/")) {
                guid = pathInfo.substring(1);
            } else {
                guid = pathInfo;
            }
            // TBD:
            // Check if guid is a valid GUID ????
            // The "guid" may include "/" ???
            // ....
        }
        return guid;
    }

    
    // temporary implementation.
    public String getHostname(String requestURL)
    {
        URL url = null;
        try {
            url = new URL(requestURL);
        } catch (MalformedURLException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Invalid url: " + requestURL, e);
            return null;  // ???
        }
        String hostname = url.getHost();
        return hostname;
    }

    // temporary implementation.
    public String getTopLevelURL(String requestURL)
    {
        return getTopLevelURL(requestURL, false);
    }
    public String getTopLevelURL(String requestURL, boolean includePath)
    {
        return getTopLevelURL(requestURL, includePath, false);
    }
    public String getTopLevelURL(String requestURL, boolean includePath, boolean removeFilename)
    {
        URL url = null;
        try {
            url = new URL(requestURL);
        } catch (MalformedURLException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Invalid url: " + requestURL, e);
            return null;  // ???
        }
        
        String topLevelURL = null;

        //String protocol = request.getProtocol();
        String scheme = url.getProtocol();
        String hostname = url.getHost();
        String authority = url.getAuthority();
        int port = url.getPort();
        StringBuilder uriSb = new StringBuilder();
        uriSb.append(scheme);
        uriSb.append("://");

        if(authority != null && !authority.isEmpty()) {
            uriSb.append(authority);
        } else {    // Can this happen???
            uriSb.append(hostname);
            if(port > 0 && (("http".equals(scheme) && port != 80) || ("https".equals(scheme) && port != 443))) {
                uriSb.append(":");
                uriSb.append(port);
            }
        }

        if(includePath) {
            String path = url.getPath();
            if(path != null && !path.isEmpty()) {
                if(removeFilename) {
                    int idx = path.lastIndexOf("/");
                    if(idx == -1) {
                        uriSb.append("/");                // Just include the trailing slash...                        
                    } else {
                        path = path.substring(0, idx+1);  // Include the trailing slash...
                        uriSb.append(path);
                    }
                } else {
                    uriSb.append(path);                   // NOTE: path may not end with "/"....
                }
            } else {
                uriSb.append("/");                        // Include the trailing slash...
            }
        } else {
            uriSb.append("/");                            // Include the trailing slash...            
        }

        topLevelURL = uriSb.toString();
        if(topLevelURL.endsWith("//")) {  // Can this happen??? But, just in case....
            if(log.isLoggable(Level.WARNING)) log.warning("topLevelURL ends with multiple slashes. Need to debug this." + topLevelURL);
            topLevelURL = topLevelURL.substring(0, topLevelURL.length() - 1);
        }
        if(log.isLoggable(Level.INFO)) log.info("getTopLevelURL(): topLevelURL = " + topLevelURL);

        return topLevelURL;
    }

    // temporary implementation.
    public String getTopLevelURLFromRequestURL(String requestURL)
    {
        if(requestURL == null || requestURL.length() < 10) {
            log.warning("Invalid requestURL.");
            return "/";  // ???
        }
        
        String topLevelURL = null;
        int idx = requestURL.indexOf("/", 9);
        if(idx < 0) {
            if(log.isLoggable(Level.WARNING)) log.warning("Invalid requestURL = " + requestURL);
            topLevelURL = requestURL;  // ???          
        } else {
            topLevelURL = requestURL.substring(0, idx + 1);  // Include the trailing "/".
        }
        if(log.isLoggable(Level.INFO)) log.info("getTopLevelURLFromRequestURL(): topLevelURL = " + topLevelURL);

        return topLevelURL;
    }

    // temporary implementation.
    public String getTopLevelURLFromRequest(HttpServletRequest request)
    {
        if(request == null) {
            log.warning("Invalid request.");
            return "/";  // ???
        }
        
        String topLevelURL = null;

        //String protocol = request.getProtocol();
        String scheme = request.getScheme();
        String hostname = request.getServerName();
        int port = request.getServerPort();
        StringBuffer uriSb = new StringBuffer();
        uriSb.append(scheme);
        uriSb.append("://");
        uriSb.append(hostname);
        if(("http".equals(scheme) && port != 80) || ("https".equals(scheme) && port != 443)) {
            uriSb.append(":");
            uriSb.append(port);
        }
        uriSb.append("/");
        topLevelURL = uriSb.toString();
        if(log.isLoggable(Level.INFO)) log.info("getTopLevelURLFromRequest(): topLevelURL = " + topLevelURL);

        return topLevelURL;
    }

}
