package com.cannyurl.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cannyurl.app.helper.UserAppHelper;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.UserJsBean;
import com.cannyurl.wa.service.UserWebService;
import com.myurldb.ws.User;


// temporary
public class UserHelper
{
    private static final Logger log = Logger.getLogger(UserHelper.class.getName());

    private UserWebService userWebService = null;

    private UserHelper() 
    {
    }

    // Initialization-on-demand holder.
    private static final class UserHelperHolder
    {
        private static final UserHelper INSTANCE = new UserHelper();
    }

    // Singleton method
    public static UserHelper getInstance()
    {
        return UserHelperHolder.INSTANCE;
    }

    private UserWebService getUserWebService()
    {
        if(userWebService == null) {
            userWebService = new UserWebService();
        }
        return userWebService;
    }

    
    public UserJsBean getUser(String guid)
    {
        UserJsBean bean = null;
        User user = UserAppHelper.getInstance().getUser(guid);
        if(user != null) {
            bean = UserWebService.convertUserToJsBean(user);
        }
        return bean;
    }
    
        
    public UserJsBean findUserByUsername(String username)  // throws WebException
    {
        UserJsBean user = null;
        try {
            String filter = "username=='" + username + "'";
            // String ordering = "createdTime desc";
            String ordering = null;
//            List<UserJsBean> beans = getUserWebService().findUsers(filter, ordering, null, null, null, null, 0L, 2);
//            if(beans != null && !beans.isEmpty()) {
//                int sz = beans.size();
//                if(sz > 1) {
//                    // something's wrong...
//                }
//                user = beans.get(0);
//                // full fetch.
//                user = getUser(user.getGuid());
//            } else {
//                // ???
//            }
            List<String> keys = getUserWebService().findUserKeys(filter, ordering, null, null, null, null, 0L, 2);
            if(keys != null && !keys.isEmpty()) {
                int sz = keys.size();
                if(sz > 1) {
                    // something's wrong...
                    if(log.isLoggable(Level.SEVERE)) log.severe("More than one userUsercode found for username = " + username);
                    return null;   // Throw exception ???
                }
                String key = keys.get(0);
                // full fetch.
                user = getUser(key);
            } else {
                // Can this happen????
                if(log.isLoggable(Level.WARNING)) log.warning("Failed to get any user for username = " + username);
            }
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to get user for username = " + username, e);
        }        
        return user;
    }

    public UserJsBean findUserByUsercode(String usercode)  // throws WebException
    {
        UserJsBean user = null;
        try {
            String filter = "usercode=='" + usercode + "'";
            // String ordering = "createdTime desc";
            String ordering = null;
//            List<UserJsBean> beans = getUserWebService().findUsers(filter, ordering, null, null, null, null, 0L, 2);
//            if(beans != null && !beans.isEmpty()) {
//                int sz = beans.size();
//                if(sz > 1) {
//                    // something's wrong...
//                }
//                user = beans.get(0);
//                // full fetch.
//                user = getUser(user.getGuid());
//            } else {
//                // ???
//            }
            List<String> keys = getUserWebService().findUserKeys(filter, ordering, null, null, null, null, 0L, 2);
            if(keys != null && !keys.isEmpty()) {
                int sz = keys.size();
                if(sz > 1) {
                    // something's wrong...
                    if(log.isLoggable(Level.SEVERE)) log.severe("More than one userUsercode found for usercode = " + usercode);
                    return null;   // Throw exception ???
                }
                String key = keys.get(0);
                // full fetch.
                user = getUser(key);
            } else {
                // Can this happen????
                if(log.isLoggable(Level.WARNING)) log.warning("Failed to get any user for usercode = " + usercode);
            }
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to get user for usercode = " + usercode, e);
        }        
        return user;
    }

    
    // The problem with the above two methods is that
    //    the caller often does not know if they have username or usercode
    //    or sometimes they need to call twice to check both....

    public UserJsBean findUserByUsernameOrUsercode(String userNameOrCode)  // throws WebException
    {
        UserJsBean user = null;
        try {
            String filter = "username=='" + userNameOrCode + "' || usercode=='" + userNameOrCode + "'";
            // String ordering = "createdTime desc";
            String ordering = null;
            List<String> keys = getUserWebService().findUserKeys(filter, ordering, null, null, null, null, 0L, 2);
            if(keys != null && !keys.isEmpty()) {
                int sz = keys.size();
                if(sz > 1) {
                    // something's wrong...
                    if(log.isLoggable(Level.SEVERE)) log.severe("More than one user found for userNameOrCode = " + userNameOrCode);
                    return null;   // Throw exception ???
                }
                String key = keys.get(0);
                // full fetch.
                user = getUser(key);
            } else {
                // Can this happen????
                if(log.isLoggable(Level.WARNING)) log.warning("Failed to get any user for userNameOrCode = " + userNameOrCode);
            }
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to get user for userNameOrCode = " + userNameOrCode, e);
        }        
        return user;
    }

}
