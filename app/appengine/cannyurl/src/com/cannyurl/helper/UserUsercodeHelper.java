package com.cannyurl.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cannyurl.app.helper.UserUsercodeAppHelper;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.UserUsercodeJsBean;
import com.cannyurl.wa.service.UserUsercodeWebService;
import com.myurldb.ws.UserUsercode;



// TBD: Usercode validation???
// length, etc...
// Also, usercode cannot contain ".", etc...
// ....


// temporary
public class UserUsercodeHelper
{
    private static final Logger log = Logger.getLogger(UserUsercodeHelper.class.getName());

    private UserUsercodeWebService userUsercodeWebService = null;

    private UserUsercodeHelper() 
    {
    }

    // Initialization-on-demand holder.
    private static final class UserUsercodeHelperHolder
    {
        private static final UserUsercodeHelper INSTANCE = new UserUsercodeHelper();
    }

    // Singleton method
    public static UserUsercodeHelper getInstance()
    {
        return UserUsercodeHelperHolder.INSTANCE;
    }

    private UserUsercodeWebService getUserUsercodeWebService()
    {
        if(userUsercodeWebService == null) {
            userUsercodeWebService = new UserUsercodeWebService();
        }
        return userUsercodeWebService;
    }

    
    public UserUsercodeJsBean getUserUsercode(String guid)
    {
        UserUsercodeJsBean bean = null;
        UserUsercode userUsercode = UserUsercodeAppHelper.getInstance().getUserUsercode(guid);
        if(userUsercode != null) {
            bean = UserUsercodeWebService.convertUserUsercodeToJsBean(userUsercode);
        }
        return bean;
    }
    

    // user == User.guid.
    public String findUserUsercodeKeyByUser(String user) throws WebException
    {
        String key = null;
        String filter = "user=='" + user + "'";
        // String ordering = "createdTime desc";
        String ordering = null;
        List<String> keys = getUserUsercodeWebService().findUserUsercodeKeys(filter, ordering, null, null, null, null, 0L, 2);
        if(keys != null && !keys.isEmpty()) {
            int sz = keys.size();
            if(sz > 1) {
                // something's wrong...
                if(log.isLoggable(Level.SEVERE)) log.severe("More than one userUsercode found for user = " + user);
                return null;   // Throw exception ???
            }
            key = keys.get(0);
        } else {
            if(log.isLoggable(Level.INFO)) log.info("Failed to get any userUsercode for user = " + user);
        }
        return key;
    }
    public UserUsercodeJsBean findUserUsercodeByUser(String user)  // throws WebException
    {
        UserUsercodeJsBean userUsercode = null;
        try {
            String key = findUserUsercodeKeyByUser(user);
            if(key != null) {
                // full fetch.
                userUsercode = getUserUsercode(key);
            } else {
                // Can this happen????
                if(log.isLoggable(Level.WARNING)) log.warning("Failed to get any userUsercode for user = " + user);
            }
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to get userUsercode for user = " + user, e);
        }        
        return userUsercode;
    }


    public String findUserUsercodeKeyByUsername(String username) throws WebException
    {
        String key = null;
        String filter = "username=='" + username + "'";
        // String ordering = "createdTime desc";
        String ordering = null;
        List<String> keys = getUserUsercodeWebService().findUserUsercodeKeys(filter, ordering, null, null, null, null, 0L, 2);
        if(keys != null && !keys.isEmpty()) {
            int sz = keys.size();
            if(sz > 1) {
                // something's wrong...
                if(log.isLoggable(Level.SEVERE)) log.severe("More than one userUsercode found for username = " + username);
                return null;   // Throw exception ???
            }
            key = keys.get(0);
        } else {
            if(log.isLoggable(Level.INFO)) log.info("Failed to get any userUsercode for username = " + username);
        }
        return key;
    }
    public UserUsercodeJsBean findUserUsercodeByUsername(String username)  // throws WebException
    {
        UserUsercodeJsBean userUsercode = null;
        try {
            String key = findUserUsercodeKeyByUsername(username);
            if(key != null) {
                // full fetch.
                userUsercode = getUserUsercode(key);
            } else {
                // Can this happen????
                if(log.isLoggable(Level.WARNING)) log.warning("Failed to get any userUsercode for username = " + username);
            }
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to get userUsercode for username = " + username, e);
        }        
        return userUsercode;
    }


    public String findUserUsercodeKeyByUsercode(String usercode) throws WebException
    {
        String key = null;
        String filter = "usercode=='" + usercode + "'";
        // String ordering = "createdTime desc";
        String ordering = null;
        List<String> keys = getUserUsercodeWebService().findUserUsercodeKeys(filter, ordering, null, null, null, null, 0L, 2);
        if(keys != null && !keys.isEmpty()) {
            int sz = keys.size();
            if(sz > 1) {
                // something's wrong...
                if(log.isLoggable(Level.SEVERE)) log.severe("More than one userUsercode found for usercode = " + usercode);
                return null;   // Throw exception ???
            }
            key = keys.get(0);
        } else {
            if(log.isLoggable(Level.INFO)) log.info("Failed to get any userUsercode for usercode = " + usercode);
        }
        return key;
    }
    public UserUsercodeJsBean findUserUsercodeByUsercode(String usercode)  // throws WebException
    {
        UserUsercodeJsBean userUsercode = null;
        try {
            String key = findUserUsercodeKeyByUsercode(usercode);
            if(key != null) {
                // full fetch.
                userUsercode = getUserUsercode(key);
            } else {
                // Can this happen????
                if(log.isLoggable(Level.WARNING)) log.warning("Failed to get any userUsercode for usercode = " + usercode);
            }
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to get userUsercode for usercode = " + usercode, e);
        }        
        return userUsercode;
    }

}
