package com.cannyurl.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cannyurl.af.auth.common.CommonAuthUtil;
import com.cannyurl.app.auth.RequestAuthStateBean;
import com.cannyurl.app.auth.filter.AuthFilterUtil;
import com.myurldb.ws.core.StatusCode;


// Mainly, for ajax calls....
public class AuthStateServlet extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(AuthStateServlet.class.getName());

    // Query params...
    // format == xml, json, jsonp, scrpt, text, html, ....  Currently, only "json" is allowed.
    // Note that the default format is either text of json depending on the method....
    // format=json overwrites the output format of the method which would have otherwise produced text content.  
    private static final String QUERY_PARAM_FORMAT = "format";
    private static final String QUERY_PARAM_JSONP_CALLBACK1 = "jsonp";
    private static final String QUERY_PARAM_JSONP_CALLBACK2 = "callback";
    // ...
    private static final String PATH_COMMAND_AUTHSTRUCT = "authstruct";
    private static final String PATH_COMMAND_AUTHENTICATED = "authenticated";
    private static final String PATH_COMMAND_LOGINURL = "loginurl";
    private static final String PATH_COMMAND_LOGOUTURL = "logouturl";
    // ...

    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        log.info("doGet() called.");

        // TBD:
        // Based on pathInfo + query params
        // Process different requests...  ???
        // ...
        String requestUrl = req.getRequestURL().toString();
        String contextPath = req.getContextPath();
        String servletPath = req.getServletPath();
        String pathInfo = req.getPathInfo();
        String queryString = req.getQueryString();
        if(log.isLoggable(Level.FINE)) log.fine("requestUrl = " + requestUrl);
        if(log.isLoggable(Level.FINE)) log.fine("contextPath = " + contextPath);
        if(log.isLoggable(Level.FINE)) log.fine("servletPath = " + servletPath);
        if(log.isLoggable(Level.FINE)) log.fine("pathInfo = " + pathInfo);
        if(log.isLoggable(Level.FINE)) log.fine("queryString = " + queryString);

        
        // TBD:
        
        // URL Patterns
        // servlet path: /ajax/authstate
        // /ajax/authstate/authstruct
        // /ajax/authstate/authenticated
        // /ajax/authstate/loginurl
        // /ajax/authstate/logouturl
        // ....
        
        
        String command = null;
        if(pathInfo != null && !pathInfo.isEmpty() && !pathInfo.equals("/")) {
            if(pathInfo.startsWith("/")) {
                pathInfo = pathInfo.substring(1);
            }
            command = pathInfo;
        } else {
            // ????
            command = PATH_COMMAND_AUTHSTRUCT;
        }
        if(log.isLoggable(Level.FINE)) log.fine("command = " + command);

        // JSONP support
        String jsonpCallback = null;
        String[] callbacks1 = req.getParameterValues(QUERY_PARAM_JSONP_CALLBACK1);
        if(callbacks1 != null && callbacks1.length > 0) {
            jsonpCallback = callbacks1[0];  // ???
        }
        if(jsonpCallback == null || jsonpCallback.isEmpty()) {
            String[] callbacks2 = req.getParameterValues(QUERY_PARAM_JSONP_CALLBACK2);
            if(callbacks2 != null && callbacks2.length > 0) {
                jsonpCallback = callbacks2[0];  // ???
            }
        }        

        boolean jsonOrJsonpRequested = false;
        if((req.getParameter(QUERY_PARAM_FORMAT) != null && req.getParameter(QUERY_PARAM_FORMAT).equalsIgnoreCase("json")) || (jsonpCallback != null && !jsonpCallback.isEmpty())) {
            jsonOrJsonpRequested = true;
        }

        
//      // 
//      if(comebackUrl != null && !comebackUrl.isEmpty()) {
//          req.setParameter(CommonAuthUtil.PARAM_COMEBACKURL, comebackUrl);
//      }
//      // 
//      if(fallbackUrl != null && !fallbackUrl.isEmpty()) {
//          req.setParameter(CommonAuthUtil.PARAM_FALLBACKURL, fallbackUrl);
//      }
        
        RequestAuthStateBean authStateBean = AuthFilterUtil.createAuthStateBean(req, resp);
        if(log.isLoggable(Level.FINE)) log.fine("Return authStateBean = " + authStateBean);

        if(authStateBean != null) {
            // Call this before getting writer....
            String contentType = "text/plain;charset=UTF-8";
            if(command.equals(PATH_COMMAND_AUTHSTRUCT) || jsonOrJsonpRequested) {
                contentType = "application/json;charset=UTF-8";
                if(jsonpCallback != null && !jsonpCallback.isEmpty()) {
                    contentType = "application/javascript;charset=UTF-8";
                }
            }
            resp.setContentType(contentType);            

            PrintWriter writer = resp.getWriter();
            if(command.equals(PATH_COMMAND_AUTHSTRUCT)) {
                //String jsonStr = "{\"authStateBean\":\"" + authStateBean.toJsonString() + "\"}";
                String jsonStr = authStateBean.toJsonString();
                if(jsonpCallback != null && !jsonpCallback.isEmpty()) {
                    jsonStr = jsonpCallback + "(" + jsonStr + ")";
                }
                writer.print(jsonStr);
            } else {
                Object value = null;  // ???
                if(command.equals(PATH_COMMAND_AUTHENTICATED)) {
                    Boolean isAuthenticated = authStateBean.isAuthenticated();
                    if(isAuthenticated != null) {
                        value = isAuthenticated;
                    } else {
                        value = Boolean.FALSE;
                    }
                } else if(command.equals(PATH_COMMAND_LOGINURL)) {
                    value = authStateBean.getLoginUrl();
                } else if(command.equals(PATH_COMMAND_LOGOUTURL)) {
                    value = authStateBean.getLogoutUrl();
                } else {
                    // ????
                }
                if(jsonOrJsonpRequested) {
                    String jsonStr = null;
                    if(command.equals(PATH_COMMAND_AUTHENTICATED)) {   // if(value instanceof Boolean) {
                        jsonStr = "{\"" + command + "\":" + value + "}";
                    } else {
                        jsonStr = "{\"" + command + "\":\"" + value + "\"}";
                    }
                    if(jsonpCallback != null && !jsonpCallback.isEmpty()) {
                        jsonStr = jsonpCallback + "(" + jsonStr + ")";
                    }
                    writer.print(jsonStr);
                } else {
                    writer.print(value);
                }
            }
            resp.setStatus(StatusCode.OK);  
        } else {
            resp.setStatus(StatusCode.NOT_FOUND);  // ???
        }
        // ...
        
        log.info("doGet(): BOTTOM");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }

}
