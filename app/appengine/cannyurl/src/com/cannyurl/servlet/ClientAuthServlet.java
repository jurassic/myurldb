package com.cannyurl.servlet;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.myurldb.ws.core.StatusCode;


// Not implemented...
// Mainly, for ajax calls....
public class ClientAuthServlet extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ClientAuthServlet.class.getName());

    // Query params...
    private static final String QUERY_PARAM_JSON = "json";
    private static final String QUERY_PARAM_JSONP_CALLBACK1 = "jsonp";
    private static final String QUERY_PARAM_JSONP_CALLBACK2 = "callback";

    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        log.info("doGet() called.");

        // TBD:
        // Based on pathInfo + query params
        // Process different requests...  ???
        // ...
        String requestUrl = req.getRequestURL().toString();
        String contextPath = req.getContextPath();
        String servletPath = req.getServletPath();
        String pathInfo = req.getPathInfo();
        String queryString = req.getQueryString();
        if(log.isLoggable(Level.FINE)) log.fine("requestUrl = " + requestUrl);
        if(log.isLoggable(Level.FINE)) log.fine("contextPath = " + contextPath);
        if(log.isLoggable(Level.FINE)) log.fine("servletPath = " + servletPath);
        if(log.isLoggable(Level.FINE)) log.fine("pathInfo = " + pathInfo);
        if(log.isLoggable(Level.FINE)) log.fine("queryString = " + queryString);


        // TBD:
        
        // URL Patterns
        // servlet path: /ajax/clientauth
        // /ajax/clientauth/loggedon
        // /ajax/clientauth/authstate
        // ....
        

        
        // JSONP support
        String jsonpCallback = null;
        String[] callbacks1 = req.getParameterValues(QUERY_PARAM_JSONP_CALLBACK1);
        if(callbacks1 != null && callbacks1.length > 0) {
            jsonpCallback = callbacks1[0];  // ???
        }
        if(jsonpCallback == null || jsonpCallback.isEmpty()) {
            String[] callbacks2 = req.getParameterValues(QUERY_PARAM_JSONP_CALLBACK2);
            if(callbacks2 != null && callbacks2.length > 0) {
                jsonpCallback = callbacks2[0];  // ???
            }
        }        

//        // Fetch the help shortLink
//        HelpNoticeJsBean helpNotice = HelpNoticeHelper.getInstance().getHelpNotice(uuid);
//        // ...
//        
//        if(helpNotice != null) {
//            //String jsonStr = "{\"helpNotice\":\"" + helpNotice.toJsonString() + "\"}";
//            String jsonStr = helpNotice.toJsonString();
//            if(jsonpCallback != null && !jsonpCallback.isEmpty()) {
//                jsonStr = jsonpCallback + "(" + jsonStr + ")";
//                //resp.setContentType("application/javascript");  // ???
//                // Call this before getting writer....
//                resp.setContentType("application/javascript;charset=UTF-8");
//            } else {
//                //resp.setContentType("application/json");  // ????
//                // Call this before getting writer....
//                resp.setContentType("application/json;charset=UTF-8");  // ???                
//            }
//            PrintWriter writer = resp.getWriter();
//            writer.print(jsonStr);
//            resp.setStatus(StatusCode.OK);  
//        } else {
//            resp.setStatus(StatusCode.NOT_FOUND);  // ???
//        }
//        // ...

        
        resp.setStatus(StatusCode.NOT_FOUND); 
        log.info("doGet(): BOTTOM");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }

}
