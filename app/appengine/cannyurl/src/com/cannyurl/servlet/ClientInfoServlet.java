package com.cannyurl.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.cannyurl.af.config.Config;
import com.myurldb.ws.core.StatusCode;


// Mainly, for ajax calls....
public class ClientInfoServlet extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ClientInfoServlet.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    // Query params...
    // format == xml, json, jsonp, scrpt, text, html, ....  Currently, only "json" is allowed.
    // Note that the default format is either text of json depending on the method....
    // format=json overwrites the output format of the method which would have otherwise produced text content.  
    private static final String QUERY_PARAM_FORMAT = "format";
    private static final String QUERY_PARAM_JSONP_CALLBACK1 = "jsonp";
    private static final String QUERY_PARAM_JSONP_CALLBACK2 = "callback";
    // ...
    private static final String OOMMAND_IPADDRESS = "ipaddress";
    private static final String OOMMAND_HOSTNAME = "hostname";
    // ...
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        log.info("doGet() called.");

        // TBD:
        // Based on pathInfo + query params
        // Process different requests...  ???
        // ...
        String requestUrl = req.getRequestURL().toString();
        String contextPath = req.getContextPath();
        String servletPath = req.getServletPath();
        String pathInfo = req.getPathInfo();
        String queryString = req.getQueryString();
        if(log.isLoggable(Level.FINE)) log.fine("requestUrl = " + requestUrl);
        if(log.isLoggable(Level.FINE)) log.fine("contextPath = " + contextPath);
        if(log.isLoggable(Level.FINE)) log.fine("servletPath = " + servletPath);
        if(log.isLoggable(Level.FINE)) log.fine("pathInfo = " + pathInfo);
        if(log.isLoggable(Level.FINE)) log.fine("queryString = " + queryString);

        
        // TBD
        
        // URL Patterns
        // servlet path: /ajax/clientinfo
        // /ajax/clientinfo/ipaddress
        // /ajax/clientinfo/hostname
        // ....
        

        String command = "";   // ??
        if(pathInfo != null && !pathInfo.isEmpty() && !pathInfo.equals("/")) {
            if(pathInfo.startsWith("/")) {
                pathInfo = pathInfo.substring(1);
            }
            String[] parts = pathInfo.split("/");
            if(parts != null && parts.length > 0) {
                command = parts[0];
                if(parts.length > 1) {
                    // ...
                }
            }
        }
        if(log.isLoggable(Level.FINE)) log.fine("command = " + command);

        // JSONP support
        String jsonpCallback = null;
        String[] callbacks1 = req.getParameterValues(QUERY_PARAM_JSONP_CALLBACK1);
        if(callbacks1 != null && callbacks1.length > 0) {
            jsonpCallback = callbacks1[0];  // ???
        }
        if(jsonpCallback == null || jsonpCallback.isEmpty()) {
            String[] callbacks2 = req.getParameterValues(QUERY_PARAM_JSONP_CALLBACK2);
            if(callbacks2 != null && callbacks2.length > 0) {
                jsonpCallback = callbacks2[0];  // ???
            }
        }        

        boolean jsonOrJsonpRequested = false;
        if((req.getParameter(QUERY_PARAM_FORMAT) != null && req.getParameter(QUERY_PARAM_FORMAT).equalsIgnoreCase("json")) || (jsonpCallback != null && !jsonpCallback.isEmpty())) {
            jsonOrJsonpRequested = true;
        }

        String value = null;
        if(command.equals(OOMMAND_IPADDRESS)) {
            value = req.getRemoteAddr();
        } else if(command.equals(OOMMAND_HOSTNAME)) {
            value = req.getRemoteHost();
        } else {
            // ????
            if(log.isLoggable(Level.WARNING)) log.warning("Unrecognized command = " + command);
        }
        if(log.isLoggable(Level.FINE)) log.fine("Return value = " + value);

        if(value != null) {
            // Call this before getting writer....
            String contentType = "text/plain;charset=UTF-8";
            if(jsonOrJsonpRequested) {
                contentType = "application/json;charset=UTF-8";
                if(jsonpCallback != null && !jsonpCallback.isEmpty()) {
                    contentType = "application/javascript;charset=UTF-8";
                }
            }
            resp.setContentType(contentType);            

            PrintWriter writer = resp.getWriter();
            if(jsonOrJsonpRequested) {
                String jsonStr = "";
                String jsonObj = toJsonString(value);
                jsonStr =  "{\"" + command + "\":" + jsonObj + "}";
                if(jsonpCallback != null && !jsonpCallback.isEmpty()) {
                    jsonStr = jsonpCallback + "(" + jsonStr + ")";
                }
                writer.print(jsonStr);
            } else {
                writer.print(value);
            }
            resp.setStatus(StatusCode.OK);  
        } else {
            resp.setStatus(StatusCode.NOT_FOUND);  // ???
        }
        // ...
        
        log.info("doGet(): BOTTOM");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }


    private static String toJsonString(Object obj)
    {
        String jsonStr = null;
        try {
            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, obj);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;
    }
    
}
