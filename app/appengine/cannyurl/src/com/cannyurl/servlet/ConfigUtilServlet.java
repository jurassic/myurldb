package com.cannyurl.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cannyurl.af.config.Config;
import com.myurldb.ws.core.StatusCode;


// Mainly, for ajax calls....
public class ConfigUtilServlet extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ConfigUtilServlet.class.getName());

    // Query params...
    // format == xml, json, jsonp, scrpt, text, html, ....  Currently, only "json" is allowed.
    // Note that the default format is either text of json depending on the method....
    // format=json overwrites the output format of the method which would have otherwise produced text content.  
    private static final String QUERY_PARAM_FORMAT = "format";
    private static final String QUERY_PARAM_JSONP_CALLBACK1 = "jsonp";
    private static final String QUERY_PARAM_JSONP_CALLBACK2 = "callback";
    // ...
    private static final String PATH_DATATYPE_BOOLEAN = "boolean";
    private static final String PATH_DATATYPE_SHORT = "short";
    private static final String PATH_DATATYPE_INTEGER = "integer";
    private static final String PATH_DATATYPE_LONG = "long";
    private static final String PATH_DATATYPE_FLOAT = "float";
    private static final String PATH_DATATYPE_DOUBLE = "double";
    private static final String PATH_DATATYPE_STRING = "string";
    // ...

    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        log.info("doGet() called.");

        // TBD:
        // Based on pathInfo + query params
        // Process different requests...  ???
        // ...
        String requestUrl = req.getRequestURL().toString();
        String contextPath = req.getContextPath();
        String servletPath = req.getServletPath();
        String pathInfo = req.getPathInfo();
        String queryString = req.getQueryString();
        if(log.isLoggable(Level.FINE)) log.fine("requestUrl = " + requestUrl);
        if(log.isLoggable(Level.FINE)) log.fine("contextPath = " + contextPath);
        if(log.isLoggable(Level.FINE)) log.fine("servletPath = " + servletPath);
        if(log.isLoggable(Level.FINE)) log.fine("pathInfo = " + pathInfo);
        if(log.isLoggable(Level.FINE)) log.fine("queryString = " + queryString);
         
        
        // TBD
        
        // URL Patterns
        // servlet path: /ajax/configutil
        // /ajax/configutil/<var>
        // /ajax/configutil/<var>/<var type>
        // /ajax/configutil/<var>/<var type>/<default value>
        // ....
        

        String varName = "";   // ??
        String varType = null;
        String defaultStrVal = null;
        if(pathInfo != null && !pathInfo.isEmpty() && !pathInfo.equals("/")) {
            if(pathInfo.startsWith("/")) {
                pathInfo = pathInfo.substring(1);
            }
            String[] parts = pathInfo.split("/", 3);
            if(parts != null && parts.length > 0) {
                varName = parts[0];
                if(parts.length > 1) {
                    varType = parts[1];
                    if(parts.length > 2) {
                        defaultStrVal = parts[2];
                    }
                }
            }
        }
        if(log.isLoggable(Level.FINE)) log.fine("varName = " + varName + "; varType = " + varType + "; defaultStrVal = " + defaultStrVal);

        // JSONP support
        String jsonpCallback = null;
        String[] callbacks1 = req.getParameterValues(QUERY_PARAM_JSONP_CALLBACK1);
        if(callbacks1 != null && callbacks1.length > 0) {
            jsonpCallback = callbacks1[0];  // ???
        }
        if(jsonpCallback == null || jsonpCallback.isEmpty()) {
            String[] callbacks2 = req.getParameterValues(QUERY_PARAM_JSONP_CALLBACK2);
            if(callbacks2 != null && callbacks2.length > 0) {
                jsonpCallback = callbacks2[0];  // ???
            }
        }        

        boolean jsonOrJsonpRequested = false;
        if((req.getParameter(QUERY_PARAM_FORMAT) != null && req.getParameter(QUERY_PARAM_FORMAT).equalsIgnoreCase("json")) || (jsonpCallback != null && !jsonpCallback.isEmpty())) {
            jsonOrJsonpRequested = true;
        }

        Object value = null;
        if(varType != null) {
            if(varType.equals(PATH_DATATYPE_BOOLEAN)) {
                Boolean defaultVal = null;
                if(defaultStrVal != null && !defaultStrVal.isEmpty()) {
                    try {
                        defaultVal = Boolean.valueOf(defaultStrVal);
                    } catch(Exception e) {
                        // ignore
                    }
                }
                if(defaultVal == null) {
                    value = Config.getInstance().getBoolean(varName);
                } else {
                    value = Config.getInstance().getBoolean(varName, defaultVal);                    
                }
            } else if(varType.equals(PATH_DATATYPE_SHORT)) {
                Short defaultVal = null;
                if(defaultStrVal != null && !defaultStrVal.isEmpty()) {
                    try {
                        defaultVal = Short.valueOf(defaultStrVal);
                    } catch(Exception e) {
                        // ignore
                    }
                }
                if(defaultVal == null) {
                    value = Config.getInstance().getShort(varName);
                } else {
                    value = Config.getInstance().getShort(varName, defaultVal);                    
                }
            } else if(varType.equals(PATH_DATATYPE_INTEGER)) {
                Integer defaultVal = null;
                if(defaultStrVal != null && !defaultStrVal.isEmpty()) {
                    try {
                        defaultVal = Integer.valueOf(defaultStrVal);
                    } catch(Exception e) {
                        // ignore
                    }
                }
                if(defaultVal == null) {
                    value = Config.getInstance().getInteger(varName);
                } else {
                    value = Config.getInstance().getInteger(varName, defaultVal);                    
                }
            } else if(varType.equals(PATH_DATATYPE_LONG)) {
                Long defaultVal = null;
                if(defaultStrVal != null && !defaultStrVal.isEmpty()) {
                    try {
                        defaultVal = Long.valueOf(defaultStrVal);
                    } catch(Exception e) {
                        // ignore
                    }
                }
                if(defaultVal == null) {
                    value = Config.getInstance().getLong(varName);
                } else {
                    value = Config.getInstance().getLong(varName, defaultVal);                    
                }
            } else if(varType.equals(PATH_DATATYPE_FLOAT)) {
                Float defaultVal = null;
                if(defaultStrVal != null && !defaultStrVal.isEmpty()) {
                    try {
                        defaultVal = Float.valueOf(defaultStrVal);
                    } catch(Exception e) {
                        // ignore
                    }
                }
                if(defaultVal == null) {
                    value = Config.getInstance().getFloat(varName);
                } else {
                    value = Config.getInstance().getFloat(varName, defaultVal);                    
                }
            } else if(varType.equals(PATH_DATATYPE_DOUBLE)) {
                Double defaultVal = null;
                if(defaultStrVal != null && !defaultStrVal.isEmpty()) {
                    try {
                        defaultVal = Double.valueOf(defaultStrVal);
                    } catch(Exception e) {
                        // ignore
                    }
                }
                if(defaultVal == null) {
                    value = Config.getInstance().getDouble(varName);
                } else {
                    value = Config.getInstance().getDouble(varName, defaultVal);                    
                }
            } else if(varType.equals(PATH_DATATYPE_STRING)) {
                if(defaultStrVal == null) {
                    value = Config.getInstance().getString(varName);
                } else {
                    value = Config.getInstance().getString(varName, defaultStrVal);                    
                }
            } else {
                // ?????
            }
        } else {
            // ???
            varType = PATH_DATATYPE_STRING;
            value = Config.getInstance().getProperty(varName);
            if(value != null && ! (value instanceof String)) {
                try {
                    String strVal = value.toString();   // ?????
                    value = strVal;
                } catch(Exception e) {
                    // ????
                }
            }
        }
        if(log.isLoggable(Level.FINE)) log.fine("Return value = " + value);

        if(value != null) {
            // Call this before getting writer....
            String contentType = "text/plain;charset=UTF-8";
            if(jsonOrJsonpRequested) {
                contentType = "application/json;charset=UTF-8";
                if(jsonpCallback != null && !jsonpCallback.isEmpty()) {
                    contentType = "application/javascript;charset=UTF-8";
                }
            }
            resp.setContentType(contentType);            

            PrintWriter writer = resp.getWriter();
            if(jsonOrJsonpRequested) {
                String jsonStr = "";
                if(varType.equals(PATH_DATATYPE_STRING)) {
                    jsonStr =  "{\"" + varName + "\":\"" + value + "\"}";    
                } else {
                    // int, bool, etc...
                    jsonStr =  "{\"" + varName + "\":" + value + "}";
                }
                if(jsonpCallback != null && !jsonpCallback.isEmpty()) {
                    jsonStr = jsonpCallback + "(" + jsonStr + ")";
                }
                writer.print(jsonStr);
            } else {
                writer.print(value);
            }
            resp.setStatus(StatusCode.OK);  
        } else {
            resp.setStatus(StatusCode.NOT_FOUND);  // ???
        }
        // ...
        
        log.info("doGet(): BOTTOM");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }

}
