package com.cannyurl.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cannyurl.af.core.JerseyClient;
import com.cannyurl.af.util.PathInfoUtil;
import com.cannyurl.helper.CustomUserAuthHelper;
import com.cannyurl.util.JsonBeanUtil;
import com.cannyurl.wa.service.UserWebService;
import com.myurldb.ws.core.StatusCode;


// Mainly, for ajax calls....
public class CustomAuthServlet extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(CustomAuthServlet.class.getName());

    // temporary
    private static final int PATH_COMMAND_COUNT = 1;   // Same for GET/POST/PUT....
    // ...
    public static final String COMMAND_ISAVAILABLE = "isavailable";
    // ...
    public static final String PATHPARAM_USERNAME = "username";
    public static final String PATHPARAM_EMAIL = "email";
    public static final String PATHPARAM_OPENID = "openid";
    // ...
    
    
    // TBD: Is this safe for concurrent calls???
    private UserWebService userWebService = null;
//    private UserPasswordWebService userPasswordWebService = null;
    // etc...

    private UserWebService getUserService()
    {
        if(userWebService == null) {
            userWebService = new UserWebService();
        }
        return userWebService;
    }
//    private UserPasswordWebService getUserPasswordService()
//    {
//        if(userPasswordWebService == null) {
//            userPasswordWebService = new UserPasswordWebService();
//        }
//        return userPasswordWebService;
//    }
    // etc. ...


    @Override
    public void init() throws ServletException
    {
        super.init();
        
        // Hack: "Preload" the Jersey client.... to reduce the initial loading time... 
        JerseyClient.getInstance().initClient();  // The call does not do anything other than initializing the JerseyClient singletong instance...
        // ...
    }

    
    // URL: ".../isavailable/username/<username>", ".../isavailable/email/<email>", ... 
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        log.info("doGet() called.");

        String pathInfo = req.getPathInfo();
        Map<String,String> pathMap = parsePathInfo(pathInfo);
        String command = getCommand(pathMap);
        
        // Call this before getting writer....
        resp.setContentType("application/json;charset=UTF-8");
        Map<String, String> jsonMap = new HashMap<String, String>();

        if(COMMAND_ISAVAILABLE.equals(command)) {
            String username = getPathParam(pathMap, PATHPARAM_USERNAME);
            String email = getPathParam(pathMap, PATHPARAM_EMAIL);
            String openId = getPathParam(pathMap, PATHPARAM_OPENID);
            
            Boolean isAvailable = null;
            if(username != null) {
                isAvailable = CustomUserAuthHelper.getInstance().isUsernameAvailable(username);
            } else if(email != null) {
                isAvailable = CustomUserAuthHelper.getInstance().isEmailAvailable(email);
            } else if(openId != null) {
                isAvailable = CustomUserAuthHelper.getInstance().isOpenIdAvailable(openId);
            } else {
                // Error..
            }
            
            if(isAvailable == null) {
                resp.setStatus(StatusCode.BAD_REQUEST);
                jsonMap.put("error", "Bad URL: Required parameter is missing.");
            } else {
                resp.setStatus(StatusCode.OK);
                jsonMap.put("isAvailable", Boolean.toString(isAvailable));
            }
        } else {
            // error...
            resp.setStatus(StatusCode.BAD_REQUEST);
            jsonMap.put("error", "Bad URL");
        }
        String output = JsonBeanUtil.generateJsonObjectString(jsonMap);
        PrintWriter out = resp.getWriter();
        out.write(output);
    }

    
    // TBD:
    // URL: ".../register", ...
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        log.info("doPost() called.");

        
        
        
    }

    
    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }
    
    

    private static Map<String,String> parsePathInfo(String pathInfo)
    {
        Map<String,String> map = PathInfoUtil.parsePathInfo(pathInfo, PATH_COMMAND_COUNT);
        return map;
    }


    // pathInfo -> "command"
    private static String getCommand(Map<String,String> pathMap)
    {
        String command = null;
        if(pathMap != null) {
            Map.Entry<String, String> entry = pathMap.entrySet().iterator().next();
            if(entry != null) {
                command = entry.getKey();
            }
        }
        return command;    
    }
    private static String getCommand(String pathInfo)
    {
        Map<String,String> map = PathInfoUtil.parsePathInfo(pathInfo, PATH_COMMAND_COUNT);
        return getCommand(map);
    }
    
    private static String getPathParam(Map<String,String> pathMap, String key)
    {
        String value = null;
        if(pathMap != null) {
            value = pathMap.get(key);
        }
        return value;
    }
    private static String getPathParam(String pathInfo, String key)
    {
        Map<String,String> map = PathInfoUtil.parsePathInfo(pathInfo, PATH_COMMAND_COUNT);
        return getPathParam(map, key);
    }

    
    
    // ????
    private static String getUsername(String pathInfo)
    {
        String username = getPathParam(pathInfo, PATHPARAM_USERNAME);
        return username;
    }
    
    
    
}
