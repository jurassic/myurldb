package com.cannyurl.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cannyurl.af.auth.SessionBean;
import com.cannyurl.af.auth.UserSessionManager;
import com.cannyurl.app.util.ConfigUtil;
import com.cannyurl.common.TokenType;
import com.cannyurl.helper.DomainCheckerHelper;
import com.myurldb.ws.core.StatusCode;


// Get domain for a given domainType/user...
public class DomainCheckerServlet extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(DomainCheckerServlet.class.getName());

    // Query params...
    // private static final String QUERY_PARAM_USER = "user";   // user.guid???
    private static final String QUERY_PARAM_JSONP_CALLBACK1 = "jsonp";
    private static final String QUERY_PARAM_JSONP_CALLBACK2 = "callback";

    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        log.info("doGet() called.");

        // TBD:
        // Based on pathInfo + query params
        // Process different requests...  ???
        // ...
        String requestUrl = req.getRequestURL().toString();
        String contextPath = req.getContextPath();
        String servletPath = req.getServletPath();
        String pathInfo = req.getPathInfo();
        String queryString = req.getQueryString();
        if(log.isLoggable(Level.FINE)) log.fine("requestUrl = " + requestUrl);
        if(log.isLoggable(Level.FINE)) log.fine("contextPath = " + contextPath);
        if(log.isLoggable(Level.FINE)) log.fine("servletPath = " + servletPath);
        if(log.isLoggable(Level.FINE)) log.fine("pathInfo = " + pathInfo);
        if(log.isLoggable(Level.FINE)) log.fine("queryString = " + queryString);

        
        
        
        // /ajax/domaingen/<domaintype>
        //    user should be obtained from user/auth service ....
        // ....
        // /domaingen/ ?  or /domaincheck/ ?????
        // ....
        

        
        if(pathInfo == null || pathInfo.isEmpty() || pathInfo.equals("/")) {
            if(log.isLoggable(Level.WARNING)) log.warning("Invalid pathInfo.");
            resp.setStatus(StatusCode.BAD_REQUEST);
            return;
        }
        
        int pathInfoLen = pathInfo.length();
        String domainType = pathInfo.substring(1);   // Exclude the leading "/"...
        if(! TokenType.isValidType(domainType)) {
            domainType = ConfigUtil.getSystemDefaultTokenType();
        }
        if(log.isLoggable(Level.FINE)) log.fine("domainType = " + domainType);


        // JSONP support
        String jsonpCallback = null;
        String[] callbacks1 = req.getParameterValues(QUERY_PARAM_JSONP_CALLBACK1);
        if(callbacks1 != null && callbacks1.length > 0) {
            jsonpCallback = callbacks1[0];  // ???
        }
        if(jsonpCallback == null || jsonpCallback.isEmpty()) {
            String[] callbacks2 = req.getParameterValues(QUERY_PARAM_JSONP_CALLBACK2);
            if(callbacks2 != null && callbacks2.length > 0) {
                jsonpCallback = callbacks2[0];  // ???
            }
        }
        
        
        // TBD:
        // Get User
        String user = null;   // User guid.
//        HttpSession session = req.getSession();
//        if(session != null) { 
//        }
        SessionBean sessionBean = UserSessionManager.getInstance().setSessionBean(req, resp);
        if(sessionBean != null) {
            user = sessionBean.getUserId();
            // username, usercode???   
        }
        
        // Note: username/usercode will be filled-in in getDomainForUser()....
        String domain = DomainCheckerHelper.getInstance().getDomainForUser(domainType, user);
        
        if(domain != null) {
            // TBD: Is it necessary to url-encode domain??  ... Probably not....
            String jsonStr = "{\"domain\":\"" + domain + "\"}";
            if(jsonpCallback != null && !jsonpCallback.isEmpty()) {
                jsonStr = jsonpCallback + "(" + jsonStr + ")";
                //resp.setContentType("application/javascript");  // ???
                resp.setContentType("application/javascript;charset=UTF-8");
            } else {
                //resp.setContentType("application/json");  // ????
                resp.setContentType("application/json;charset=UTF-8");  // ???                
            }
            PrintWriter writer = resp.getWriter();
            writer.print(jsonStr);
            resp.setStatus(StatusCode.OK);  
        } else {
            resp.setStatus(StatusCode.NOT_FOUND);  // ???
        }
        // ...
        
        log.info("doGet(): BOTTOM");
    }

}
