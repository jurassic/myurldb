package com.cannyurl.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cannyurl.af.core.JerseyClient;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.KeywordLinkJsBean;
import com.cannyurl.fe.bean.SpeedDialJsBean;
import com.cannyurl.helper.UrlHelper;
import com.cannyurl.util.JsonBeanUtil;
import com.cannyurl.wa.service.KeywordLinkWebService;
import com.cannyurl.wa.service.SpeedDialWebService;
import com.cannyurl.wa.service.UserWebService;
import com.myurldb.ws.core.StatusCode;


// Mainly, for ajax calls....
// Payload: { "keywordLink": keywordLinkBean, "speedDial": speedDialBean }
public class KeywordLinkServlet extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(KeywordLinkServlet.class.getName());
    
    // Arbitrary cutoff. Note: 500 is the max length for String type...
    private static final int SUMMARY_LENGTH_CUTOFF = 450;

    // TBD: Is this safe for concurrent calls???
    private UserWebService userWebService = null;
    private KeywordLinkWebService keywordLinkWebService = null;
    private SpeedDialWebService speedDialWebService = null;
    // etc...

    private UserWebService getUserService()
    {
        if(userWebService == null) {
            userWebService = new UserWebService();
        }
        return userWebService;
    }
    private KeywordLinkWebService getKeywordLinkService()
    {
        if(keywordLinkWebService == null) {
            keywordLinkWebService = new KeywordLinkWebService();
        }
        return keywordLinkWebService;
    }
    private SpeedDialWebService getSpeedDialService()
    {
        if(speedDialWebService == null) {
            speedDialWebService = new SpeedDialWebService();
        }
        return speedDialWebService;
    }
    // etc. ...

    

    @Override
    public void init() throws ServletException
    {
        super.init();
        
        // Hack: "Preload" the Jersey client.... to reduce the initial loading time... 
        JerseyClient.getInstance().initClient();  // The call does not do anything other than initializing the JerseyClient singletong instance...
        // ...
    }

    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }

    
    // TBD:
    // depending on keywordLink.type...
    // ...
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        log.info("doPost(): TOP");

        // TBD: Check Accept header. Etc...
        // For now, both input and output are application/json.
        KeywordLinkJsBean outKeywordLinkBean = null;
        SpeedDialJsBean outSpeedDialBean = null;
        try {
            BufferedReader reader = req.getReader();
            String jsonStr = reader.readLine();

            KeywordLinkJsBean inKeywordLinkBean = null;
            SpeedDialJsBean inSpeedDialBean = null;
            Map<String,String> jsonObjectMap = JsonBeanUtil.parseJsonObjectString(jsonStr);
            for(String key : jsonObjectMap.keySet()) {
                String json = jsonObjectMap.get(key);
                log.info("key = " + key + "; json = " + json);
                
                if(key.equals("keywordLink")) {
                    inKeywordLinkBean = KeywordLinkJsBean.fromJsonString(json);
                } else if(key.equals("speedDial")) {
                    inSpeedDialBean = SpeedDialJsBean.fromJsonString(json);
                } else {
                    // TBD:
                    // This should not happen, for now.
                    log.warning("Unregconized key in the json input string. key = " + key);
                }
            }

            // TBD: Make sure neither is null???
            // Or, just process what is inputted....
            
            // TBD: Verify keywordLink.type is compatible with speedDialBean object type!!!
            if(inKeywordLinkBean != null) {
                // TBD: Validate and populate some missing fields, etc...
                // etc. ...
                // ...

                outKeywordLinkBean = getKeywordLinkService().constructKeywordLink(inKeywordLinkBean);
            }
            if(inSpeedDialBean != null) {
                // TBD: Validate and populate some missing fields, etc...
                // TBD: Use keywordLink.type instead of speedDialBean object type???
                if(outKeywordLinkBean != null) {
                    inSpeedDialBean.setKeywordLink(outKeywordLinkBean.getGuid());
                }
                outSpeedDialBean = getSpeedDialService().constructSpeedDial((SpeedDialJsBean) inSpeedDialBean);
            }
            
            // ...

        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to save keywordLink and speedDial.", e);
        }
        
        if(outKeywordLinkBean != null) {
            resp.setStatus(StatusCode.OK);   // StatusCode.ACCEPTED ?
            resp.setContentType("application/json;charset=UTF-8");
            //resp.setHeader("Cache-Control", "no-cache");    // ?????

            // Location header???
            //String guid = outKeywordLinkBean.getGuid();
            
            // Construct output
            Map<String, String> jsonMap = new HashMap<String, String>();
            jsonMap.put("keywordLink", outKeywordLinkBean.toJsonString());
            
            if(outSpeedDialBean != null) {
                jsonMap.put("speedDial", outSpeedDialBean.toJsonString());
            }
            String output = JsonBeanUtil.generateJsonObjectString(jsonMap);
            //String output = "{\"keywordLink\":" + outKeywordLinkBean.toJsonString() + ",\"speedDial\":" + outSpeedDialBean.toJsonString() + "}";

            PrintWriter out = resp.getWriter();
            out.write(output);
        } else {
            resp.setStatus(StatusCode.INTERNAL_SERVER_ERROR);  // ????
        }

        log.info("doPost(): BOTTOM");
    }

    // TBD: Need to use refreshXXX() rather than updateXXX()
    //      (Some UI fields need to be updated based on the server data....)
    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        log.info("doPut(): TOP");

        // TBD: Check Accept header. Etc...

        String pathInfo = req.getPathInfo();
        String guid = UrlHelper.getInstance().getGuidFromPathInfo(pathInfo);        

        // guid should not be null!!!
        if(guid != null && !guid.isEmpty()) {
            Boolean suc = null;
            try {
                BufferedReader reader = req.getReader();
                String jsonStr = reader.readLine();
                
                KeywordLinkJsBean inKeywordLinkBean = null;
                SpeedDialJsBean inSpeedDialBean = null;
                Map<String,String> jsonObjectMap = JsonBeanUtil.parseJsonObjectString(jsonStr);
                for(String key : jsonObjectMap.keySet()) {
                    String json = jsonObjectMap.get(key);
                    log.info("key = " + key + "; json = " + json);
                    
                    if(key.equals("keywordLink")) {
                        inKeywordLinkBean = KeywordLinkJsBean.fromJsonString(json);
                    } else if(key.equals("speedDial")) {
                        inSpeedDialBean = SpeedDialJsBean.fromJsonString(json);
                    } else {
                        // TBD:
                        // This should not happen, for now.
                        log.warning("Unregconized key in the json input string. key = " + key);
                    }
                }

                // TBD: Make sure neither is null???
                // Or, just process what is inputted....

                Boolean sucKeywordLink = null;
                if(inKeywordLinkBean != null) {
                    String beanGuid = inKeywordLinkBean.getGuid();
                    if(guid.equals(beanGuid)) {
                        inKeywordLinkBean.setModifiedTime(System.currentTimeMillis());
                        sucKeywordLink = getKeywordLinkService().updateKeywordLink(inKeywordLinkBean);
                        log.finer("sucKeywordLink = " + sucKeywordLink);
                    } else {
                        log.log(Level.WARNING, "Inconsistent input. pathGuid = " + guid + "; beanGuid = " + beanGuid);
                        // ???
                    }
                }

                Boolean sucSpeedDial = null;
                if(inSpeedDialBean != null) {
                    // TBD: Validate and populate some missing fields, etc...
                    inSpeedDialBean.setModifiedTime(System.currentTimeMillis());

                    // TBD: Use keywordLink.type isntead of speedDialBean object type???
                    sucSpeedDial = getSpeedDialService().updateSpeedDial((SpeedDialJsBean) inSpeedDialBean);
                }
                
                // TBD: ...
                if(Boolean.TRUE.equals(sucKeywordLink)) {    //  && Boolean.TRUE.equals(sucSpeedDial)) {
                    suc = Boolean.TRUE;
                }
                // ...

            } catch (WebException e) {
                log.log(Level.WARNING, "Failed.", e);
            }
            if(Boolean.TRUE.equals(suc)) {
                resp.setStatus(StatusCode.OK);   // StatusCode.ACCEPTED ?
            } else {
                resp.setStatus(StatusCode.INTERNAL_SERVER_ERROR);  // ????
            }
        } else {
            // ???
            resp.setStatus(StatusCode.NOT_FOUND);  // ???
        }

        log.info("doPut(): BOTTOM");
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }
    
    
    
    // TBD:
    // validators???
    // e.g., max length of title, etc.
    // ...
    
    
    
}
