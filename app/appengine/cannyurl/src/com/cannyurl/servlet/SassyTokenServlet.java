package com.cannyurl.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cannyurl.app.sassy.SassyUrlUtil;
import com.cannyurl.app.util.ConfigUtil;
import com.cannyurl.common.SassyTokenType;
import com.cannyurl.fe.bean.FiveTenJsBean;
import com.cannyurl.wa.service.FiveTenWebService;
import com.myurldb.ws.core.StatusCode;


// Deprecated
// Use TokenGeneratorServlet instead....
public class SassyTokenServlet extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(SassyTokenServlet.class.getName());

    // Query params...
    private static final String QUERY_PARAM_LENGTH = "length";
    private static final String QUERY_PARAM_JSONP_CALLBACK1 = "jsonp";
    private static final String QUERY_PARAM_JSONP_CALLBACK2 = "callback";

    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        log.info("doGet() called.");

        // TBD:
        // Based on pathInfo + query params
        // Process different requests...  ???
        // ...
        String requestUrl = req.getRequestURL().toString();
        String contextPath = req.getContextPath();
        String servletPath = req.getServletPath();
        String pathInfo = req.getPathInfo();
        String queryString = req.getQueryString();
        log.fine("requestUrl = " + requestUrl);
        log.fine("contextPath = " + contextPath);
        log.fine("servletPath = " + servletPath);
        log.fine("pathInfo = " + pathInfo);
        log.fine("queryString = " + queryString);

        // Default value
        String sassyTokenType = ConfigUtil.getSystemDefaultSassyTokenType();
        if(pathInfo != null && !pathInfo.isEmpty()) {
            if(pathInfo.startsWith("/")) {
                pathInfo = pathInfo.substring(1);
            }
            if(SassyTokenType.isValidType(pathInfo)) {
                sassyTokenType = pathInfo;
            } else {
                // ignore. just use the default value.
                log.warning("Invalid sassyTokenType = " + sassyTokenType);
            }
        }
        log.fine("sassyTokenType = " + sassyTokenType);

        // Default value
        int length = SassyUrlUtil.getDefaultSassyTokenLength(sassyTokenType);
        
        String paramLength = req.getParameter(QUERY_PARAM_LENGTH);
        if(paramLength != null && !paramLength.isEmpty()) {
            try {
                length = Integer.parseInt(paramLength);
            } catch(NumberFormatException e) {
                // ignore
                log.warning("Invalid length param = " + paramLength);
            }
        }
        log.fine("length = " + length);
        
        // JSONP support
        String jsonpCallback = null;
        String[] callbacks1 = req.getParameterValues(QUERY_PARAM_JSONP_CALLBACK1);
        if(callbacks1 != null && callbacks1.length > 0) {
            jsonpCallback = callbacks1[0];  // ???
        }
        if(jsonpCallback == null || jsonpCallback.isEmpty()) {
            String[] callbacks2 = req.getParameterValues(QUERY_PARAM_JSONP_CALLBACK2);
            if(callbacks2 != null && callbacks2.length > 0) {
                jsonpCallback = callbacks2[0];  // ???
            }
        }        

        // Generate token
        String token = SassyUrlUtil.generateSassyUrlToken(sassyTokenType, length);
        
        if(token != null) {
            String jsonStr = "{\"sassyToken\":\"" + token + "\"}";
            if(jsonpCallback != null && !jsonpCallback.isEmpty()) {
                jsonStr = jsonpCallback + "(" + jsonStr + ")";
                //resp.setContentType("application/javascript");  // ???
                resp.setContentType("application/javascript;charset=UTF-8");
            } else {
                //resp.setContentType("application/json");  // ????
                resp.setContentType("application/json;charset=UTF-8");  // ???                
            }
            PrintWriter writer = resp.getWriter();
            writer.print(jsonStr);
            resp.setStatus(StatusCode.OK);  
        } else {
            resp.setStatus(StatusCode.NOT_FOUND);  // ???
        }
        // ...
        
        log.info("doGet(): BOTTOM");
    }

}
