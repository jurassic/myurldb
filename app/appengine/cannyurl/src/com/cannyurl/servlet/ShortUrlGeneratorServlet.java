package com.cannyurl.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cannyurl.af.auth.SessionBean;
import com.cannyurl.af.auth.UserSessionManager;
import com.cannyurl.app.sassy.SassyUrlUtil;
import com.cannyurl.app.util.ConfigUtil;
import com.cannyurl.app.util.TokenUtil;
import com.cannyurl.common.SassyTokenType;
import com.cannyurl.common.TokenType;
import com.cannyurl.helper.ShortUrlGeneratorHelper;
import com.myurldb.ws.core.StatusCode;


// Mainly, for ajax calls....
public class ShortUrlGeneratorServlet extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ShortUrlGeneratorServlet.class.getName());

    // Query params...
    // private static final String QUERY_PARAM_LENGTH = "length";
    private static final String QUERY_PARAM_DOMAIN = "domain";
    private static final String QUERY_PARAM_DOMAINTYPE = "domaintype";
    private static final String QUERY_PARAM_JSONP_CALLBACK1 = "jsonp";
    private static final String QUERY_PARAM_JSONP_CALLBACK2 = "callback";

    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        log.info("doGet() called.");

        // TBD:
        // Based on pathInfo + query params
        // Process different requests...  ???
        // ...
        String requestUrl = req.getRequestURL().toString();
        String contextPath = req.getContextPath();
        String servletPath = req.getServletPath();
        String pathInfo = req.getPathInfo();
        String queryString = req.getQueryString();
        if(log.isLoggable(Level.FINE)) log.fine("requestUrl = " + requestUrl);
        if(log.isLoggable(Level.FINE)) log.fine("contextPath = " + contextPath);
        if(log.isLoggable(Level.FINE)) log.fine("servletPath = " + servletPath);
        if(log.isLoggable(Level.FINE)) log.fine("pathInfo = " + pathInfo);
        if(log.isLoggable(Level.FINE)) log.fine("queryString = " + queryString);

        
        
        
        // /ajax/shorturl/<tokentype>
        // /ajax/shorturl/sassy/<sassytokentype>
        // ....
        
        
        if(pathInfo == null || pathInfo.isEmpty() || pathInfo.equals("/")) {
            if(log.isLoggable(Level.WARNING)) log.warning("Invalid pathInfo.");
            resp.setStatus(StatusCode.BAD_REQUEST);
            return;
        }
        
        int pathInfoLen = pathInfo.length();
        String tokenType = null;
        String sassyTokenType = null;
        int tokenLength = -1;
        // boolean isSassyTokenRequested = false;
        if(pathInfo.startsWith("/sassy/")) {
            // isSassyTokenRequested = true;
            if(pathInfoLen > 7) {
                String theRest = pathInfo.substring(7);
                int x = theRest.indexOf("/");
                if(x > 0) {
                    sassyTokenType = theRest.substring(0, x);
                    if(x < theRest.length() - 1) {
                        String lengthParam = theRest.substring(x + 1);
                        try {
                            tokenLength = Integer.parseInt(lengthParam);
                        } catch(Exception e) {
                            // ignore...
                            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "lengthParam = " + lengthParam, e);
                        }
                    }
                } else {
                    sassyTokenType = theRest;
                }
                sassyTokenType = pathInfo.substring(7);
            }
            if(! SassyTokenType.isValidType(sassyTokenType)) {
                sassyTokenType = ConfigUtil.getSystemDefaultSassyTokenType();
            }
            if(tokenLength <= 0) {
                tokenLength = SassyUrlUtil.getDefaultSassyTokenLength(sassyTokenType);
            }
        } else {
            String theRest = pathInfo.substring(1);   // Exclude the leading "/"...
            int x = theRest.indexOf("/");
            if(x > 0) {
                tokenType = theRest.substring(0, x);
                if(x < theRest.length() - 1) {
                    String lengthParam = theRest.substring(x + 1);
                    try {
                        tokenLength = Integer.parseInt(lengthParam);
                    } catch(Exception e) {
                        // ignore...
                        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "lengthParam = " + lengthParam, e);
                    }
                }
            } else {
                tokenType = theRest;
            }
            if(! TokenType.isValidType(tokenType)) {
                tokenType = ConfigUtil.getSystemDefaultTokenType();
            }
            if(tokenLength <= 0) {
                tokenLength = TokenUtil.getDefaultTokenLength(tokenType);
            }
        }
        if(log.isLoggable(Level.FINE)) log.fine("tokenType = " + tokenType);
        if(log.isLoggable(Level.FINE)) log.fine("sassyTokenType = " + sassyTokenType);
        if(log.isLoggable(Level.FINE)) log.fine("tokenLength = " + tokenLength);
        // if(log.isLoggable(Level.FINE)) log.fine("isSassyTokenRequested = " + isSassyTokenRequested);
        

        String domain = req.getParameter(QUERY_PARAM_DOMAIN);
        if(log.isLoggable(Level.FINE)) log.fine("domain = " + domain);

        String domainType = req.getParameter(QUERY_PARAM_DOMAINTYPE);
        if(log.isLoggable(Level.FINE)) log.fine("domainType = " + domainType);


        // JSONP support
        String jsonpCallback = null;
        String[] callbacks1 = req.getParameterValues(QUERY_PARAM_JSONP_CALLBACK1);
        if(callbacks1 != null && callbacks1.length > 0) {
            jsonpCallback = callbacks1[0];  // ???
        }
        if(jsonpCallback == null || jsonpCallback.isEmpty()) {
            String[] callbacks2 = req.getParameterValues(QUERY_PARAM_JSONP_CALLBACK2);
            if(callbacks2 != null && callbacks2.length > 0) {
                jsonpCallback = callbacks2[0];  // ???
            }
        }        

        
        // TBD:
        // Get User
        String user = null;   // User guid.
//        HttpSession session = req.getSession();
//        if(session != null) { 
//        }
        SessionBean sessionBean = UserSessionManager.getInstance().setSessionBean(req, resp);
        if(sessionBean != null) {
            user = sessionBean.getUserId();
            // username, usercode???   
        }
        

        // TBD:
        String shortUrl = ShortUrlGeneratorHelper.getInstance().generateShortUrl(tokenType, sassyTokenType, domain, domainType, user);
        
        // TBD:
        if(shortUrl != null) {
            // TBD: Do we need to URL-encode shortUrl ?????
            // String jsonStr = "{\"shortUrl\":\"" + shortUrl + "\", \"token\":\"" + token + "\", \"domain\":\"" + domain + "\"}";
            String jsonStr = "{\"shortUrl\":\"" + shortUrl + "\"}";
            if(jsonpCallback != null && !jsonpCallback.isEmpty()) {
                jsonStr = jsonpCallback + "(" + jsonStr + ")";
                //resp.setContentType("application/javascript");  // ???
                resp.setContentType("application/javascript;charset=UTF-8");
            } else {
                //resp.setContentType("application/json");  // ????
                resp.setContentType("application/json;charset=UTF-8");  // ???                
            }
            PrintWriter writer = resp.getWriter();
            writer.print(jsonStr);
            resp.setStatus(StatusCode.OK);  
        } else {
            resp.setStatus(StatusCode.NOT_FOUND);  // ???
        }
        // ...
        
        log.info("doGet(): BOTTOM");
    }

}
