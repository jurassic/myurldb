package com.cannyurl.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cannyurl.af.core.JerseyClient;
import com.cannyurl.af.util.PathInfoUtil;
import com.cannyurl.app.util.TimeRangeUtil;
import com.cannyurl.common.TermType;
import com.cannyurl.fe.bean.CellLatitudeLongitudeJsBean;
import com.cannyurl.fe.bean.ShortLinkJsBean;
import com.cannyurl.helper.ShortUrlStreamHelper;
import com.cannyurl.util.ShortUrlStreamUtil;
import com.cannyurl.util.QueryParamUtil;
import com.cannyurl.wa.service.ShortLinkWebService;
import com.cannyurl.wa.service.UserWebService;
import com.myurldb.ws.core.StatusCode;


// Mainly, for ajax calls....
public class ShortUrlStreamServlet extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ShortUrlStreamServlet.class.getName());
    
    // Arbitrary cutoff. Note: 500 is the max length for String type...
    private static final int SUMMARY_LENGTH_CUTOFF = 450;

    // TBD: Is this safe for concurrent calls???
    private UserWebService userWebService = null;
    private ShortLinkWebService shortLinkWebService = null;
    // etc...

    private UserWebService getUserService()
    {
        if(userWebService == null) {
            userWebService = new UserWebService();
        }
        return userWebService;
    }
    private ShortLinkWebService getShortLinkService()
    {
        if(shortLinkWebService == null) {
            shortLinkWebService = new ShortLinkWebService();
        }
        return shortLinkWebService;
    }
    // etc. ...

    

    @Override
    public void init() throws ServletException
    {
        super.init();
        
        // Hack: "Preload" the Jersey client.... to reduce the initial loading time... 
        JerseyClient.getInstance().initClient();  // The call does not do anything other than initializing the JerseyClient singletong instance...
        // ...
    }

    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        log.info("doGet(): TOP");

        
        // Three different modes
        // (1)  Get ShortLink for a given key/guid
        // (2)  Fetch key list - From startTime till now
        // (3)  Fetch key list - Hour / TenMinute / Minute
        // (4??)   Get X number of most recent keys in the past ????
        // ... No full list fetch, for now ...
        // ...
        
        // Path
        // (1)  .../key/<guid>
        // (2)  ...[/geocell/<geocell>]/since/<startTime>
        //      ...[/geocell/<geocell>]/since/<startTime>/max/<count>
        // (3)  ...[/geocell/<geocell>]/tenmins/<dayhour>
        //      ...[/geocell/<geocell>]/minute/<dayhour>
        //      ...[/geocell/<geocell>]/hour/<dayhour>
        //      (if millisec is given, it is converted to a dayhour string...)
        // (4???)  ...[/geocell/<geocell>]/count/<count> ?????
        
        
        String pathInfo = req.getPathInfo();
        if(log.isLoggable(Level.FINE)) log.fine("pathInfo = " + pathInfo);
        Map<String,String> pathMap = PathInfoUtil.parsePathInfo(pathInfo);
        
        String shortLinkKey = null;
        CellLatitudeLongitudeJsBean targetGeoCell = null;
        Long startTime = null;
        Integer maxCount = null;
        String termType = null;
        String dayHour = null;
        
        if(pathMap != null) {
            if(pathMap.containsKey("key")) {
                shortLinkKey = pathMap.get("key");
            } else {
                if(pathMap.containsKey("geocell")) {
                    String geoCellStr = pathMap.get("geocell");
                    if(geoCellStr != null && !geoCellStr.isEmpty()) {   // TBD: validation...
                        // Does this work???
                        if(log.isLoggable(java.util.logging.Level.WARNING)) log.warning("Param geocell found: geoCellStr = " + geoCellStr);
                        targetGeoCell = QueryParamUtil.parseGeoCellParam(geoCellStr);
                        // TBD:
                        //....
                    }
                    if(targetGeoCell == null) {
                        if(log.isLoggable(Level.WARNING)) log.warning("targetGeoCell is null when the path include geocell.");
                        resp.setStatus(StatusCode.BAD_REQUEST);  // ????
                    }
                }
                if(pathMap.containsKey("since")) {
                    String startTimeStr = pathMap.get("since");
                    try {
                        startTime = Long.parseLong(startTimeStr); 
                    } catch(Exception e) {
                        // Bail out???
                        if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse startTime param as a long integer: " + startTimeStr, e);
                        resp.setStatus(StatusCode.BAD_REQUEST);  // ????
                    }
                    if(pathMap.containsKey("max")) {
                        String maxCountStr = pathMap.get("max");
                        try {
                            maxCount = Integer.parseInt(maxCountStr);
                        } catch(Exception e) {
                            // Ignore...
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to parse max param as an integer: " + maxCountStr);
                        }
                    }
                } else {
                    String dateTimePathSegment = null;
                    if(pathMap.containsKey("minute")) {
                        termType = TermType.TERM_MINUTE;
                        String termArg = pathMap.get("minute");  // Validation ??
                        try {
                            long timeMillis = Long.parseLong(termArg);
                            dateTimePathSegment = TimeRangeUtil.getDayHourMinute(timeMillis);
                        } catch(Exception e) {
                            // ignore...
                            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Param is not a unix epoch milliseconds: " + termArg, e);
                        }
                        if(dateTimePathSegment == null) {
                            dateTimePathSegment = termArg;  // Validation ??
                        }
                    } else if(pathMap.containsKey("tenmins")) {
                        termType = TermType.TERM_TENMINS;
                        String termArg = pathMap.get("tenmins"); 
                        try {
                            long timeMillis = Long.parseLong(termArg);
                            dateTimePathSegment = TimeRangeUtil.getDayHourTenMins(timeMillis);
                        } catch(Exception e) {
                            // ignore...
                            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Param is not a unix epoch milliseconds: " + termArg, e);
                        }
                        if(dateTimePathSegment == null) {
                            dateTimePathSegment = termArg;  // Validation ??
                        }
                    } else if(pathMap.containsKey("hourly")) {
                        termType = TermType.TERM_HOURLY;
                        String termArg = pathMap.get("hourly"); 
                        try {
                            long timeMillis = Long.parseLong(termArg);
                            dateTimePathSegment = TimeRangeUtil.getDayHour(timeMillis);
                        } catch(Exception e) {
                            // ignore...
                            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Param is not a unix epoch milliseconds: " + termArg, e);
                        }
                        if(dateTimePathSegment == null) {
                            dateTimePathSegment = termArg;  // Validation ??
                        }
                    } else if(pathMap.containsKey("daily")) {
                        termType = TermType.TERM_DAILY;
                        String termArg = pathMap.get("daily"); 
                        try {
                            long timeMillis = Long.parseLong(termArg);
                            dateTimePathSegment = TimeRangeUtil.getDay(timeMillis);
                        } catch(Exception e) {
                            // ignore...
                            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Param is not a unix epoch milliseconds: " + termArg, e);
                        }
                        if(dateTimePathSegment == null) {
                            dateTimePathSegment = termArg;  // Validation ??
                        }
                    } else {
                        // Error
                        // ...
                    }
                    if(dateTimePathSegment != null && !dateTimePathSegment.isEmpty()) {
                        dayHour = ShortUrlStreamUtil.getDateTimeStringFromUrlPath(dateTimePathSegment);
                        // if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Input dayHour = " + dayHour);
                    } else {
                        // ???
                        log.warning("Term type is missing or unrecognized.");
                        resp.setStatus(StatusCode.BAD_REQUEST);  // ????
                    }
                }
            }
        }        
        if(log.isLoggable(Level.FINE)) {
            log.fine("Param shortLinkKey = " + shortLinkKey);
            log.fine("Param targetGeoCell = " + targetGeoCell);
            log.fine("Param startTime = " + startTime);
            log.fine("Param maxCount = " + maxCount);
            log.fine("Param termType = " + termType);
            log.fine("Param dayHour = " + dayHour);
        }
 
//        // TBD:
//        String servletPath = req.getServletPath();
//        String geoCellStr = getGeoCellParamFromServletPath(servletPath);
//        // ...
        
        // (1)
        if(shortLinkKey != null) {
            ShortLinkJsBean shortlink = ShortUrlStreamHelper.getInstance().getShortLink(shortLinkKey);
            if(log.isLoggable(Level.FINE)) log.fine("shortlink = " + shortlink);
            
            if(shortlink != null) {                
                // TBD: Need to check....
                String jsonStr = shortlink.toJsonString();  // Polymorphic ????
                if(log.isLoggable(Level.INFO)) log.info("jsonStr = " + jsonStr);

                // Call this before getting writer....
                resp.setContentType("application/json;charset=UTF-8");
                PrintWriter out = resp.getWriter();
                out.write(jsonStr);
                resp.setStatus(StatusCode.OK);
            } else {
                if(log.isLoggable(Level.WARNING)) log.warning("ShortLink not found for shortLinkKey = " + shortLinkKey);
                resp.setStatus(StatusCode.NOT_FOUND);  // ???
            }
        } else {
            boolean invalidRequest = false;
            List<String> shortLinkKeys = null;
            
            // (2)
            if(startTime != null && startTime > 0L) {
                shortLinkKeys = ShortUrlStreamHelper.getInstance().fetchShortLinkKeys(targetGeoCell, startTime, maxCount);
                if(log.isLoggable(Level.INFO)) {
                    if(shortLinkKeys != null) {
                        if(log.isLoggable(Level.FINE)) {
                            int s = shortLinkKeys.size();
                            log.fine("shortLinkKeys found: size = " + s + ", for targetGeoCell " + targetGeoCell + "; startTime = " + startTime + "; maxCount = " + maxCount);
                            if(log.isLoggable(Level.FINER)) {
                                for(int j=0;j<s; j++) {
                                    log.finer("j = " + j + "; struct = " + shortLinkKeys.get(j));                            
                                }
                            }
                        }
                    } else {
                        log.info("No shortLinkKeys found for targetGeoCell " + targetGeoCell + "; startTime = " + startTime + "; maxCount = " + maxCount);                        
                    }
                }
            } else {
                // (3)
                if(termType != null) {                
                    shortLinkKeys = ShortUrlStreamHelper.getInstance().fetchShortLinkKeys(targetGeoCell, termType, dayHour);
                    if(log.isLoggable(Level.INFO)) {
                        if(shortLinkKeys != null) {
                            if(log.isLoggable(Level.FINE)) {
                                int s = shortLinkKeys.size();
                                log.fine("shortLinkKeys found: size = " + s + ", for targetGeoCell " + targetGeoCell + "; termType = " + termType + "; dayHour = " + dayHour);
                                if(log.isLoggable(Level.FINER)) {
                                    for(int j=0;j<s; j++) {
                                        log.finer("j = " + j + "; struct = " + shortLinkKeys.get(j));                            
                                    }
                                }
                            }
                        } else {
                            log.info("No shortLinkKeys found for targetGeoCell " + targetGeoCell + "; termType = " + termType + "; dayHour = " + dayHour);                        
                        }
                    }
                } else {
                    // ????
                    invalidRequest = true;
                }                
            }
            
            
            // TBD
            // Set header "Access-Control-Allow-Origin", "*"  ???
            // ...


            if(shortLinkKeys != null) {
                
                // If shortLinkKeys.isEmpty(), return 404 ???
                // resp.setStatus(StatusCode.NOT_FOUND);  // ???
                // Probably not. Empty list is a valid response... (404 means the url/page does not exist...)

                String jsonStr = "[";
                int size = shortLinkKeys.size();
                for(int i=0; i< size; i++) {
                    jsonStr += "\"" + shortLinkKeys.get(i) + "\"";
                    if(i < size-1) {
                        jsonStr += ",";
                    }
                }
                jsonStr += "]";
                if(log.isLoggable(Level.INFO)) log.info("jsonStr = " + jsonStr);

                // Call this before getting writer....
                resp.setContentType("application/json;charset=UTF-8");
                PrintWriter out = resp.getWriter();
                out.write(jsonStr);
                resp.setStatus(StatusCode.OK);
            } else {
                if(invalidRequest) {
                    if(log.isLoggable(Level.WARNING)) log.warning("Invalid request: Required params are missing.");
                    resp.setStatus(StatusCode.BAD_REQUEST);  // ????
                } else {
                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to retrieve shortLink key list...");
                    resp.setStatus(StatusCode.INTERNAL_SERVER_ERROR);  // ????
                }
            }
        }
        
        log.info("doGet(): BOTTOM");
    }

    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }
    
    
    
    // TBD:
    // validators???
    // e.g., max length of title, etc.
    // ...
    
    
    
//    // ex: /ajax/stream/tech, etc.
//    private static String getGeoCellParamFromServletPath(String servletPath)
//    {
//        if(servletPath == null || servletPath.isEmpty()) {
//            return null;
//        }
//
//        String path = servletPath;
//        if(servletPath.startsWith("/")) {
//            path = servletPath.substring(1);
//        }
//        String[] parts = path.split("/", 3);
//        if(parts == null || parts.length < 3) {
//            return null;
//        }
//
//        String geoCellStr = parts[2];
//        if(log.isLoggable(Level.FINE)) log.fine("geoCellStr = " + geoCellStr);
//        
//        // TBD:
//        if(geoCellStr != null && geoCellStr.equals("global")) {
//            // ???
//            geoCellStr = null;
//        }
//
//        return geoCellStr;
//    }
//
   
    
    
}
