package com.cannyurl.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cannyurl.app.sassy.SassyUrlUtil;
import com.cannyurl.app.util.ConfigUtil;
import com.cannyurl.app.util.TokenUtil;
import com.cannyurl.common.SassyTokenType;
import com.cannyurl.common.TokenType;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.FiveTenJsBean;
import com.cannyurl.helper.ShortLinkHelper;
import com.cannyurl.wa.service.FiveTenWebService;
import com.myurldb.ws.core.StatusCode;


// Mainly, for ajax calls....
public class TokenCheckServlet extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(TokenCheckServlet.class.getName());

    // Query params...
    // private static final String QUERY_PARAM_LENGTH = "length";
    private static final String QUERY_PARAM_DOMAIN = "domain";
    private static final String QUERY_PARAM_JSONP_CALLBACK1 = "jsonp";
    private static final String QUERY_PARAM_JSONP_CALLBACK2 = "callback";

    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        log.info("doGet() called.");

        // TBD:
        // Based on pathInfo + query params
        // Process different requests...  ???
        // ...
        String requestUrl = req.getRequestURL().toString();
        String contextPath = req.getContextPath();
        String servletPath = req.getServletPath();
        String pathInfo = req.getPathInfo();
        String queryString = req.getQueryString();
        log.fine("requestUrl = " + requestUrl);
        log.fine("contextPath = " + contextPath);
        log.fine("servletPath = " + servletPath);
        log.fine("pathInfo = " + pathInfo);
        log.fine("queryString = " + queryString);

        
        
        
        // /ajax/tokencheck/<token>
        // ....
        
        if(pathInfo == null || pathInfo.isEmpty() || pathInfo.equals("/")) {
            if(log.isLoggable(Level.WARNING)) log.warning("Invalid pathInfo.");
            resp.setStatus(StatusCode.BAD_REQUEST);
            return;
        }

        
        String token = pathInfo.substring(1);   // Exclude the leading "/"...
        if(log.isLoggable(Level.FINE)) log.fine("token = " + token);



        String domain = req.getParameter(QUERY_PARAM_DOMAIN);
        if(domain == null || domain.isEmpty()) {
            // TBD: Create a default domain???
            // String domainType = ConfigUtil.getSystemDefaultDomainType();
            // domain = ...
            // ....
            
            // For now, just bail out...
            if(log.isLoggable(Level.WARNING)) log.warning("Required param, domain, is missing.");
            resp.setStatus(StatusCode.BAD_REQUEST);
            return;
        }
        if(log.isLoggable(Level.FINE)) log.fine("domain = " + domain);

        
        
        // JSONP support
        String jsonpCallback = null;
        String[] callbacks1 = req.getParameterValues(QUERY_PARAM_JSONP_CALLBACK1);
        if(callbacks1 != null && callbacks1.length > 0) {
            jsonpCallback = callbacks1[0];  // ???
        }
        if(jsonpCallback == null || jsonpCallback.isEmpty()) {
            String[] callbacks2 = req.getParameterValues(QUERY_PARAM_JSONP_CALLBACK2);
            if(callbacks2 != null && callbacks2.length > 0) {
                jsonpCallback = callbacks2[0];  // ???
            }
        }        

        
        boolean isExisting = false;
        try {
            String existingShortUrlKey = ShortLinkHelper.getInstance().findShortLinkKeyByToken(token, domain);
            if(existingShortUrlKey == null) {
                if(log.isLoggable(Level.FINE)) log.fine("No existing shortUrl found for token = " + token + "; domain = " + domain);
            } else {
                isExisting = true;
                if(log.isLoggable(Level.FINE)) log.fine("shortUlr already exists for token = " + token + "; domain = " + domain);
            }
        } catch(WebException ex) {
            // Error occurred while querying...
            log.log(Level.WARNING, "Short URL query failed for token = " + token + "; domain = " + domain);
            resp.setStatus(StatusCode.INTERNAL_SERVER_ERROR);
            return ;
        }
       
 
        String jsonStr = "{\"existing\":" + isExisting + "}";
        if(jsonpCallback != null && !jsonpCallback.isEmpty()) {
            jsonStr = jsonpCallback + "(" + jsonStr + ")";
            //resp.setContentType("application/javascript");  // ???
            resp.setContentType("application/javascript;charset=UTF-8");
        } else {
            //resp.setContentType("application/json");  // ????
            resp.setContentType("application/json;charset=UTF-8");  // ???                
        }
        PrintWriter writer = resp.getWriter();
        writer.print(jsonStr);
        resp.setStatus(StatusCode.OK);  
        // ...
        

        log.info("doGet(): BOTTOM");
    }

}
