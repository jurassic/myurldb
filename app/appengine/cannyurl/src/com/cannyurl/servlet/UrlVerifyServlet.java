package com.cannyurl.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cannyurl.app.sassy.SassyUrlUtil;
import com.cannyurl.helper.PageSynopsisHelper;
import com.cannyurl.util.JsonBeanUtil;
import com.cannyurl.util.QueryParamUtil;
import com.myurldb.ws.core.StatusCode;


// TBD:

// Mainly, for ajax calls....
public class UrlVerifyServlet extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UrlVerifyServlet.class.getName());

    // Query params...
    private static final String QUERY_PARAM_PAGEURL = QueryParamUtil.QUERY_PARAM_PAGEURL;
    private static final String QUERY_PARAM_JSONP_CALLBACK1 = "jsonp";
    private static final String QUERY_PARAM_JSONP_CALLBACK2 = "callback";

    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        log.info("doGet() called.");

        // TBD:
        // Based on pathInfo + query params
        // Process different requests...  ???
        // ...
        String requestUrl = req.getRequestURL().toString();
        String contextPath = req.getContextPath();
        String servletPath = req.getServletPath();
        String pathInfo = req.getPathInfo();
        String queryString = req.getQueryString();
        log.fine("requestUrl = " + requestUrl);
        log.fine("contextPath = " + contextPath);
        log.fine("servletPath = " + servletPath);
        log.fine("pathInfo = " + pathInfo);
        log.fine("queryString = " + queryString);

        // Required input param...
        String pageUrl = req.getParameter(QUERY_PARAM_PAGEURL);
        log.fine("pageUrl = " + pageUrl);
        
        // JSONP support
        String jsonpCallback = null;
        String[] callbacks1 = req.getParameterValues(QUERY_PARAM_JSONP_CALLBACK1);
        if(callbacks1 != null && callbacks1.length > 0) {
            jsonpCallback = callbacks1[0];  // ???
        }
        if(jsonpCallback == null || jsonpCallback.isEmpty()) {
            String[] callbacks2 = req.getParameterValues(QUERY_PARAM_JSONP_CALLBACK2);
            if(callbacks2 != null && callbacks2.length > 0) {
                jsonpCallback = callbacks2[0];  // ???
            }
        }

        if(pageUrl != null && !pageUrl.isEmpty()) {
            // Check the page...
            com.pagesynopsis.fe.bean.PageFetchJsBean webPageBean = PageSynopsisHelper.getInstance().findPageFetchByTargetUrl(pageUrl);
            if(webPageBean != null) {
                resp.setStatus(StatusCode.OK);  

                // Construct output
                Map<String, String> jsonMap = new HashMap<String, String>();
                jsonMap.put("pageFetch", webPageBean.toJsonString());
                String jsonStr = JsonBeanUtil.generateJsonObjectString(jsonMap);
                if(jsonpCallback != null && !jsonpCallback.isEmpty()) {
                    jsonStr = jsonpCallback + "(" + jsonStr + ")";
                    //resp.setContentType("application/javascript");  // ???
                    resp.setContentType("application/javascript;charset=UTF-8");
                } else {
                    //resp.setContentType("application/json");  // ????
                    resp.setContentType("application/json;charset=UTF-8");  // ???                
                }
                PrintWriter writer = resp.getWriter();
                writer.print(jsonStr);
            } else {
                resp.setStatus(StatusCode.NOT_FOUND);  // ???
            }
        } else {
            resp.setStatus(StatusCode.BAD_REQUEST);   // ???
        }
        // ...
        
        log.info("doGet(): BOTTOM");
    }

}
