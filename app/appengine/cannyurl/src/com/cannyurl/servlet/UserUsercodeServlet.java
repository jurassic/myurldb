package com.cannyurl.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cannyurl.af.core.JerseyClient;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.UserUsercodeJsBean;
import com.cannyurl.fe.bean.LinkMessageJsBean;
import com.cannyurl.helper.UrlHelper;
import com.cannyurl.util.JsonBeanUtil;
import com.cannyurl.wa.service.UserUsercodeWebService;
import com.cannyurl.wa.service.UserWebService;
import com.myurldb.ws.core.StatusCode;


// For AJAX calls... saving/updating userUsercodeBean....
// Payload: { "userUsercode": userUsercodeBean }
// Cf. UsercodeServlet.
public class UserUsercodeServlet extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UserUsercodeServlet.class.getName());
    
    // Arbitrary cutoff. Note: 500 is the max length for String type...
    private static final int SUMMARY_LENGTH_CUTOFF = 450;

    // TBD: Is this safe for concurrent calls???
    private UserWebService userWebService = null;
    private UserUsercodeWebService userUsercodeWebService = null;
    // etc...

    private UserWebService getUserService()
    {
        if(userWebService == null) {
            userWebService = new UserWebService();
        }
        return userWebService;
    }
    private UserUsercodeWebService getUserUsercodeService()
    {
        if(userUsercodeWebService == null) {
            userUsercodeWebService = new UserUsercodeWebService();
        }
        return userUsercodeWebService;
    }
    // etc. ...

    

    @Override
    public void init() throws ServletException
    {
        super.init();
        
        // Hack: "Preload" the Jersey client.... to reduce the initial loading time... 
        JerseyClient.getInstance().initClient();  // The call does not do anything other than initializing the JerseyClient singletong instance...
        // ...
    }

    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }

    
    // TBD:
    // depending on userUsercode.type...
    // ...
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        log.info("doPost(): TOP");

        // TBD: Check Accept header. Etc...
        // For now, both input and output are application/json.
        UserUsercodeJsBean outUserUsercodeBean = null;
        try {
            BufferedReader reader = req.getReader();
            String jsonStr = reader.readLine();

            UserUsercodeJsBean inUserUsercodeBean = null;
            Map<String,String> jsonObjectMap = JsonBeanUtil.parseJsonObjectString(jsonStr);
            for(String key : jsonObjectMap.keySet()) {
                String json = jsonObjectMap.get(key);
                log.info("key = " + key + "; json = " + json);
                
                if(key.equals("userUsercode")) {
                    inUserUsercodeBean = UserUsercodeJsBean.fromJsonString(json);
                } else {
                    // TBD:
                    // This should not happen, for now.
                    log.warning("Unregconized key in the json input string. key = " + key);
                }
            }

            // TBD: Make sure neither is null???
            // Or, just process what is inputted....
            
            // TBD: Verify userUsercode.type is compatible with linkMessageBean object type!!!
            if(inUserUsercodeBean != null) {
                // TBD: Validate and populate some missing fields, etc...
                // etc. ...
                // ...
                
                
                // TBD:
                // Validate username, if set, based on user auth status
                // ...
                
                
                // etc..
                // ...
                
                outUserUsercodeBean = getUserUsercodeService().constructUserUsercode(inUserUsercodeBean);
            }
            
            // ...

        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to save userUsercode.", e);
        }
        
        if(outUserUsercodeBean != null) {
            resp.setStatus(StatusCode.OK);   // StatusCode.ACCEPTED ?
            resp.setContentType("application/json;charset=UTF-8");
            //resp.setHeader("Cache-Control", "no-cache");    // ?????

            // Location header???
            //String guid = outUserUsercodeBean.getGuid();
            
            // Construct output
            Map<String, String> jsonMap = new HashMap<String, String>();
            jsonMap.put("userUsercode", outUserUsercodeBean.toJsonString());
            
            String output = JsonBeanUtil.generateJsonObjectString(jsonMap);
            //String output = "{\"userUsercode\":" + outUserUsercodeBean.toJsonString()  + "}";

            PrintWriter out = resp.getWriter();
            out.write(output);
        } else {
            resp.setStatus(StatusCode.INTERNAL_SERVER_ERROR);  // ????
        }

        log.info("doPost(): BOTTOM");
    }

    // TBD: Need to use refreshXXX() rather than updateXXX()
    //      (Some UI fields need to be updated based on the server data....)
    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        log.info("doPut(): TOP");

        // TBD: Check Accept header. Etc...

        String pathInfo = req.getPathInfo();
        String guid = UrlHelper.getInstance().getGuidFromPathInfo(pathInfo);        

        // guid should not be null!!!
        if(guid != null && !guid.isEmpty()) {
            Boolean suc = null;
            try {
                BufferedReader reader = req.getReader();
                String jsonStr = reader.readLine();
                
                UserUsercodeJsBean inUserUsercodeBean = null;
                Map<String,String> jsonObjectMap = JsonBeanUtil.parseJsonObjectString(jsonStr);
                for(String key : jsonObjectMap.keySet()) {
                    String json = jsonObjectMap.get(key);
                    log.info("key = " + key + "; json = " + json);
                    
                    if(key.equals("userUsercode")) {
                        inUserUsercodeBean = UserUsercodeJsBean.fromJsonString(json);
                    } else {
                        // TBD:
                        // This should not happen, for now.
                        log.warning("Unregconized key in the json input string. key = " + key);
                    }
                }

                // TBD: Make sure neither is null???
                // Or, just process what is inputted....

                Boolean sucUserUsercode = null;
                if(inUserUsercodeBean != null) {
                    String beanGuid = inUserUsercodeBean.getGuid();
                    if(guid.equals(beanGuid)) {
                        inUserUsercodeBean.setModifiedTime(System.currentTimeMillis());
                        // TBD:
                        // Validate/update other fields?
                        // ...
                        sucUserUsercode = getUserUsercodeService().updateUserUsercode(inUserUsercodeBean);
                        log.finer("sucUserUsercode = " + sucUserUsercode);
                    } else {
                        log.log(Level.WARNING, "Inconsistent input. pathGuid = " + guid + "; beanGuid = " + beanGuid);
                        // ???
                    }
                }
                
                // TBD: ...
                if(Boolean.TRUE.equals(sucUserUsercode)) {    //  && Boolean.TRUE.equals(sucLinkMessage)) {
                    suc = Boolean.TRUE;
                }
                // ...

            } catch (WebException e) {
                log.log(Level.WARNING, "Failed.", e);
            }
            if(Boolean.TRUE.equals(suc)) {
                resp.setStatus(StatusCode.OK);   // StatusCode.ACCEPTED ?
            } else {
                resp.setStatus(StatusCode.INTERNAL_SERVER_ERROR);  // ????
            }
        } else {
            // ???
            resp.setStatus(StatusCode.NOT_FOUND);  // ???
        }

        log.info("doPut(): BOTTOM");
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }
    
    
    
    // TBD:
    // validators???
    // e.g., max length of title, etc.
    // ...
    
    
    
}
