package com.cannyurl.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cannyurl.fe.WebException;
import com.cannyurl.helper.UserUsercodeHelper;
import com.myurldb.ws.core.StatusCode;


// For checking usercode.
// Cf. UserUsercodeServlet.
public class UsercodeServlet extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UsercodeServlet.class.getName());

    // Query params...
    // private static final String QUERY_PARAM_LENGTH = "length";
    private static final String QUERY_PARAM_USER = "user";
    private static final String QUERY_PARAM_JSONP_CALLBACK1 = "jsonp";
    private static final String QUERY_PARAM_JSONP_CALLBACK2 = "callback";

    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        log.info("doGet() called.");

        // TBD:
        // Based on pathInfo + query params
        // Process different requests...  ???
        // ...
        String requestUrl = req.getRequestURL().toString();
        String contextPath = req.getContextPath();
        String servletPath = req.getServletPath();
        String pathInfo = req.getPathInfo();
        String queryString = req.getQueryString();
        if(log.isLoggable(Level.FINE)) log.fine("requestUrl = " + requestUrl);
        if(log.isLoggable(Level.FINE)) log.fine("contextPath = " + contextPath);
        if(log.isLoggable(Level.FINE)) log.fine("servletPath = " + servletPath);
        if(log.isLoggable(Level.FINE)) log.fine("pathInfo = " + pathInfo);
        if(log.isLoggable(Level.FINE)) log.fine("queryString = " + queryString);

        
        
        
        // /ajax/usercode/new                   --> Not implemented...
        // /ajax/usercode/create/<usercode>     --> Not implemented... Just use wa servlet....
        // /ajax/usercode/check/<usercode>
        // ....
        
        
        if(pathInfo == null || pathInfo.isEmpty() || pathInfo.equals("/")) {
            if(log.isLoggable(Level.WARNING)) log.warning("Invalid pathInfo.");
            resp.setStatus(StatusCode.BAD_REQUEST);
            return;
        }
        
        int pathInfoLen = pathInfo.length();
        boolean isChecking = false;
        String inputUsercode = null;
        if(pathInfo.startsWith("/check/")) {
            if(pathInfoLen > 7) {
                isChecking = true;
                inputUsercode = pathInfo.substring(7);
            } else {
                // ????
                if(log.isLoggable(Level.WARNING)) log.warning("Required param, usercode, is missing.");
                resp.setStatus(StatusCode.BAD_REQUEST);
                return;
            }
        } else {
            // new..
            // create....
        }
        if(log.isLoggable(Level.FINE)) log.fine("isChecking = " + isChecking);
        if(log.isLoggable(Level.FINE)) log.fine("inputUsercode = " + inputUsercode);
        

        String user = req.getParameter(QUERY_PARAM_USER);
        if(log.isLoggable(Level.FINE)) log.fine("user = " + user);
//        if(isChecking == false) {
//            if(user == null || user.isEmpty()) {
//                // ????
//                if(log.isLoggable(Level.WARNING)) log.warning("Required param, user, is missing.");
//                resp.setStatus(StatusCode.BAD_REQUEST);
//                return;
//            }
//        }


        // JSONP support
        String jsonpCallback = null;
        String[] callbacks1 = req.getParameterValues(QUERY_PARAM_JSONP_CALLBACK1);
        if(callbacks1 != null && callbacks1.length > 0) {
            jsonpCallback = callbacks1[0];  // ???
        }
        if(jsonpCallback == null || jsonpCallback.isEmpty()) {
            String[] callbacks2 = req.getParameterValues(QUERY_PARAM_JSONP_CALLBACK2);
            if(callbacks2 != null && callbacks2.length > 0) {
                jsonpCallback = callbacks2[0];  // ???
            }
        }        

        
        // TBD:
        String outputUsercode = null;
        boolean isExisting = false;
        if(isChecking == false) {   // new/create
            // outputUsercode = ... ;   // ???
        } else {
            
            // Note:
            // We do not check the min/max lengths, etc...
            // only whether the input usercode is currently used or not....
            // ....
            
            try {
                String existingUsercodeKey = UserUsercodeHelper.getInstance().findUserUsercodeKeyByUsercode(inputUsercode);
                if(existingUsercodeKey == null) {
                    if(log.isLoggable(Level.FINE)) log.fine("No existing userUsercode found for inputUsercode = " + inputUsercode);
                } else {
                    isExisting = true;
                    if(log.isLoggable(Level.FINE)) log.fine("userUsercode already exists for inputUsercode = " + inputUsercode);
                }
            } catch(WebException ex) {
                // Error occurred while querying...
                log.log(Level.WARNING, "Short URL query failed for inputUsercode = " + inputUsercode);
                resp.setStatus(StatusCode.INTERNAL_SERVER_ERROR);
                return;
            }

        }
        
        // TBD:
        String jsonStr = null;
        if(isChecking == false) {   // new
            // Not implemented...
            jsonStr = "{\"usercode\":\"\"}";
            // ...
        } else {
            jsonStr = "{\"existing\":" + isExisting + "}";
        }
        if(jsonpCallback != null && !jsonpCallback.isEmpty()) {
            jsonStr = jsonpCallback + "(" + jsonStr + ")";
            //resp.setContentType("application/javascript");  // ???
            resp.setContentType("application/javascript;charset=UTF-8");
        } else {
            //resp.setContentType("application/json");  // ????
            resp.setContentType("application/json;charset=UTF-8");  // ???                
        }
        PrintWriter writer = resp.getWriter();
        writer.print(jsonStr);
        resp.setStatus(StatusCode.OK);  
        // ...
        
        log.info("doGet(): BOTTOM");
    }

}
