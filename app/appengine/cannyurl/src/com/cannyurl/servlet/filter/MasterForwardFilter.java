package com.cannyurl.servlet.filter;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import com.cannyurl.helper.UrlHelper;


// We are having problems with "/*" mapping for ShortLinkView.jsp
// We use this filter to map "/*" to appropriate pages.
// Unfortunately, this does not seem to work. Redirect loop error.
public class MasterForwardFilter implements Filter
{
    private static final Logger log = Logger.getLogger(MasterForwardFilter.class.getName());

    // TBD
    private FilterConfig config = null;

    public MasterForwardFilter()
    {
    }

    @Override
    public void destroy()
    {
        this.config = null;
    }

    @Override
    public void init(FilterConfig config) throws ServletException
    {
        this.config = config;
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res,
            FilterChain chain) throws IOException, ServletException
    {
        // [1] Parse url.
        String contextPath = ((HttpServletRequest) req).getContextPath();
        String servletPath = ((HttpServletRequest) req).getServletPath();
        String requestHost = ((HttpServletRequest) req).getServerName();
        String requestURI = ((HttpServletRequest) req).getRequestURI();
        String requestUrl = ((HttpServletRequest) req).getRequestURL().toString();
        String topLevelUrl = UrlHelper.getInstance().getTopLevelURLFromRequestURL(requestUrl);
        String pathInfo = ((HttpServletRequest) req).getPathInfo();
        String queryString = ((HttpServletRequest) req).getQueryString();
        
        // [2] 
        if(pathInfo == null || pathInfo.isEmpty() || pathInfo.equals("/") || pathInfo.equals("/home")) {
            config.getServletContext().getRequestDispatcher("/home").forward(req, res);
        } else if(pathInfo.equals("/shorten") || pathInfo.startsWith("/shorten/")) {
            // config.getServletContext().getRequestDispatcher("/shorten").forward(req, res);
            config.getServletContext().getRequestDispatcher(requestURI).forward(req, res);
        }

        
        // [3] Continue through filter chain.
        chain.doFilter(req, res);        	
    }
    
}
