package com.cannyurl.shared;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.cannyurl.helper.ShortUrlAccessHelper;

public class LongUrlVerifier
{
    private static final Logger log = Logger.getLogger(LongUrlVerifier.class.getName());

    private LongUrlVerifier() {}

    // TBD:
    // This regex pattern is incomplete.
    // It returns true for patterns such as "http://xxx", which is very likely incorrect...
	public static native boolean isValidUrl(String url) /*-{
		var pattern = /(http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
		return pattern.test(url);
	}-*/;

}
