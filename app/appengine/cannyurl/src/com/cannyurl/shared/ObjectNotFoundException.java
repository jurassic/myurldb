package com.cannyurl.shared;

import com.cannyurl.fe.WebException;

public class ObjectNotFoundException extends WebException
{
    private static final long serialVersionUID = 1L;

    public ObjectNotFoundException()
    {
        // TODO Auto-generated constructor stub
    }

    public ObjectNotFoundException(String message)
    {
        super(message);
        // TODO Auto-generated constructor stub
    }

    public ObjectNotFoundException(String message, Throwable cause)
    {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

    public ObjectNotFoundException(Throwable cause)
    {
        super(cause);
        // TODO Auto-generated constructor stub
    }

}
