package com.cannyurl.shared;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.cannyurl.helper.ShortUrlAccessHelper;

public class ShortUrlVerifier
{
    private static final Logger log = Logger.getLogger(ShortUrlVerifier.class.getName());

    private ShortUrlVerifier() {}

    
    // Cf. ShortLinkUtil.buildShortUrl() ???
    public static String getFullUrl(String domain, String path, String token)
    {
        if(domain == null || token == null) {
            log.log(Level.INFO, "Input args are invalid.");
            return null;
        }
        StringBuffer sb = new StringBuffer();
        sb.append(domain.trim());
        if(!domain.trim().endsWith("/")) {
            sb.append("/");
        }
        if(path != null && path.trim().length() > 0) {
            sb.append(path.trim());
            if(!path.trim().endsWith("/")) {
                sb.append("/");
            }
        }
        sb.append(token.trim());
        return sb.toString();
    }

}
