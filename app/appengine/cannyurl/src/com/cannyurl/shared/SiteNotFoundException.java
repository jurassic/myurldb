package com.cannyurl.shared;

import com.cannyurl.fe.WebException;

public class SiteNotFoundException extends WebException
{
    private static final long serialVersionUID = 1L;

    public SiteNotFoundException()
    {
        // TODO Auto-generated constructor stub
    }

    public SiteNotFoundException(String message)
    {
        super(message);
        // TODO Auto-generated constructor stub
    }

    public SiteNotFoundException(String message, Throwable cause)
    {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

    public SiteNotFoundException(Throwable cause)
    {
        super(cause);
        // TODO Auto-generated constructor stub
    }

}
