package com.cannyurl.util;

import java.util.logging.Logger;


// Place holder for now...

public class CommonValidater
{
    private static final Logger log = Logger.getLogger(CommonValidater.class.getName());

    private CommonValidater() {}

    // ???
    //  private static final String EMAIL_PATTERN = 
    //  "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    //private static final String TWITTER_PATTERN = 
    //  "^@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

  
    // temporary
    public static boolean isValidEmailAddress(String email)
    {
        if(email == null) {
            return false;
        }
        email = email.trim();
        int idxAt = email.indexOf("@");
        if(idxAt < 1 || idxAt > email.length() - 5) {   // "a@b.co"
            return false;
        }
        if(email.indexOf(".") < idxAt) {
            return false;
        }
        // etc.....

        return true;
    }

    // temporary
    public static boolean isValidTwitterId(String id)
    {
        if(id == null) {
            return false;
        } 
        id = id.trim();
        if(id.indexOf("@") != 0) {
            return false;
        }
        // etc.
        
        return true;
    }
   

}
