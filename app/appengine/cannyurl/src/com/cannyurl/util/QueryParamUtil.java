package com.cannyurl.util;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import com.cannyurl.fe.bean.CellLatitudeLongitudeJsBean;

public class QueryParamUtil
{
    private static final Logger log = Logger.getLogger(QueryParamUtil.class.getName());

    private QueryParamUtil() {}

    // TBD: This is really a path param, not a query param...
    public static final String PARAM_GEOCELL = "geocell";
    // etc....

    public static final String QUERY_PARAM_PAGEURL = "pageurl";
    public static final String PARAM_OFFSET = "offset";
    public static final String PARAM_COUNT = "count";
    public static final String PARAM_PAGESIZE = "pagesize";
    public static final String PARAM_ORDERING = "ordering";    // "field [asc/desc]"

    
    
    // Format: "scale, latitude, longitude"
    public static CellLatitudeLongitudeJsBean parseGeoCellParam(String geoCellStr)
    {
        CellLatitudeLongitudeJsBean targetGeoCell = null;
        if(geoCellStr != null && !geoCellStr.isEmpty()) {   // TBD: validation...
            // Does this work???
            if(log.isLoggable(Level.WARNING)) log.warning("Input param geocell: geoCellStr = " + geoCellStr);
            String[] geoCellParts = geoCellStr.split(",", 3);    // ????
            // ....
            targetGeoCell = new CellLatitudeLongitudeJsBean();
            if(geoCellParts != null) {
                int len = geoCellParts.length;
                if(len > 0) {
                    try {
                        Integer scale = Integer.parseInt(geoCellParts[0]);
                        if(scale != null) {
                            targetGeoCell.setScale(scale);
                        }
                    } catch(Exception e) {
                        // ignore ???
                        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Failed to parse scale.", e);
                    }
                    if(len > 1) {
                        try {
                            Integer latitude = Integer.parseInt(geoCellParts[1]);
                            if(latitude != null) {
                                targetGeoCell.setLatitude(latitude);
                            }
                        } catch(Exception e) {
                            // ignore ???
                            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Failed to parse latitude.", e);
                        }
                        if(len > 2) {
                            try {
                                Integer longitude = Integer.parseInt(geoCellParts[2]);
                                if(longitude != null) {
                                    targetGeoCell.setLongitude(longitude);
                                }
                            } catch(Exception e) {
                                // ignore ???
                                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Failed to parse longitude.", e);
                            }
                        }
                    }
                }
            } else {
                // ???
            }
            // TBD:
            //....
        }
        return targetGeoCell;
    }
    
    public static CellLatitudeLongitudeJsBean getParamGeoCell(HttpServletRequest request)
    {
        CellLatitudeLongitudeJsBean targetGeoCell = null;
        if(request != null) {
            String strGeoCell = request.getParameter(PARAM_GEOCELL);
            if(strGeoCell != null) {
                try {
                    targetGeoCell = parseGeoCellParam(strGeoCell);
                } catch(NumberFormatException e) {
                    // ignore
                    if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Invalid param: geoCell = " + strGeoCell, e);
                }
            }
        }
        return targetGeoCell;
    }

    

    public static String getParamPageUrl(HttpServletRequest request)
    {
        String pageUrl = request.getParameter(QUERY_PARAM_PAGEURL);
        return pageUrl;
    }

    public static Long getParamOffset(HttpServletRequest request)
    {
        Long paramOffset = null;
        if(request != null) {
            String strOffset = request.getParameter(PARAM_OFFSET);
            if(strOffset != null) {
                try {
                    paramOffset = Long.parseLong(strOffset);
                } catch(NumberFormatException e) {
                    // ignore
                    log.log(Level.INFO, "Invalid param: offset = " + strOffset, e);
                }
            }
        }
        return paramOffset;
    }

    public static Integer getParamCount(HttpServletRequest request)
    {
        Integer paramCount = null;
        if(request != null) {
            String strCount = request.getParameter(PARAM_COUNT);
            if(strCount != null) {
                try {
                    paramCount = Integer.parseInt(strCount);
                } catch(NumberFormatException e) {
                    // ignore
                    log.log(Level.INFO, "Invalid param: count = " + strCount, e);
                }
            }
        }
        return paramCount;
    }

    public static Integer getParamPageSize(HttpServletRequest request)
    {
        Integer paramPageSize = null;
        if(request != null) {
            String strPageSize = request.getParameter(PARAM_PAGESIZE);
            if(strPageSize != null) {
                try {
                    paramPageSize = Integer.parseInt(strPageSize);
                } catch(NumberFormatException e) {
                    // ignore
                    if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Invalid param: count = " + strPageSize, e);
                }
            }
        }
        return paramPageSize;
    }

    public static String getParamOrdering(HttpServletRequest request)
    {
        String paramOrdering = null;
        if(request != null) {
            paramOrdering = request.getParameter(PARAM_ORDERING);
        }
        return paramOrdering;
    }

    
    // TBD:
    
//    public static Map<String, String> parseQueryString(HttpServletRequest request)
//    {
//        Map<String, String> paramMap = new HashMap<String, String>();
//        if(request != null) {
//           // TBD ...            
//        }
//        return paramMap;
//    }

}
