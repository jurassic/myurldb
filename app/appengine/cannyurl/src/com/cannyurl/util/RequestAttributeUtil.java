package com.cannyurl.util;

import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;


public class RequestAttributeUtil
{
    private static final Logger log = Logger.getLogger(RequestAttributeUtil.class.getName());

    private RequestAttributeUtil() {}

    // ServletPath + pathInfo - (optional) guid.
    public static final String ATTR_ORIGINAL_REQUESTPATH = "com.cannyurl.original.requestpath";
    public static final String ATTR_WRITER_USERNAME = "com.cannyurl.writer.username";
    public static final String ATTR_USER_USERCODE = "com.cannyurl.user.usercode";


    public static String getAttrOriginalRequestPath(HttpServletRequest request)
    {
        String attrOriginalServletPath = null;
        if(request != null) {
            attrOriginalServletPath = (String) request.getAttribute(ATTR_ORIGINAL_REQUESTPATH);
        }
        return attrOriginalServletPath;
    }

    public static String getAttrWriterUsername(HttpServletRequest request)
    {
        String attrWriterUsername = null;
        if(request != null) {
            attrWriterUsername = (String) request.getAttribute(ATTR_WRITER_USERNAME);
        }
        return attrWriterUsername;
    }

    public static String getAttrUserUsercode(HttpServletRequest request)
    {
        String attrWriterUsercode = null;
        if(request != null) {
            attrWriterUsercode = (String) request.getAttribute(ATTR_USER_USERCODE);
        }
        return attrWriterUsercode;
    }

}
