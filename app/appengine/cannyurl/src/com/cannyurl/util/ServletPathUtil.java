package com.cannyurl.util;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServletPathUtil
{
    private static final Logger log = Logger.getLogger(ServletPathUtil.class.getName());

	private ServletPathUtil() {}

	
	
	
	
	
	// ????
	// Copied from BlogStoaApp.....
	// ????
	
	// Cache
	private static final Map<String, String[]> sMapForBlogPost;
	static {
		sMapForBlogPost = new HashMap<String, String[]>();

		sMapForBlogPost.put("/post/edit", new String[]{null, null});    // ????
//		sMapForBlogPost.put("/post/text", new String[]{InputContentType.TYPE_TEXT, null});
//		sMapForBlogPost.put("/post/html", new String[]{InputContentType.TYPE_HTML, null});
//		sMapForBlogPost.put("/post/wikitext", new String[]{InputContentType.TYPE_WIKITEXT, null});
//		sMapForBlogPost.put("/post/wikitext/twiki", new String[]{InputContentType.TYPE_WIKITEXT, WikiTextFormat.FORMAT_TWIKI});
//		sMapForBlogPost.put("/post/wikitext/textile", new String[]{InputContentType.TYPE_WIKITEXT, WikiTextFormat.FORMAT_TEXTILE});
//		sMapForBlogPost.put("/post/wikitext/mediawiki", new String[]{InputContentType.TYPE_WIKITEXT, WikiTextFormat.FORMAT_MEDIAWIKI});
		// ...
		
	}

	public static String getInputTypeForBlogPost(String servletPath)
	{
		String inputType = null;
		String[] typeAndFormat = getInputTypeAndFormatForBlogPost(servletPath);
		if(typeAndFormat != null && typeAndFormat.length > 0) {
			inputType = typeAndFormat[0];
		}
		return inputType;
	}

	public static String getInputFormatForBlogPost(String servletPath)
	{
		String inputFormat = null;
		String[] typeAndFormat = getInputTypeAndFormatForBlogPost(servletPath);
		if(typeAndFormat != null && typeAndFormat.length > 1) {
			inputFormat = typeAndFormat[2];
		}
		return inputFormat;
	}

	// Returns an array of [ type, format ]...
	public static String[] getInputTypeAndFormatForBlogPost(String servletPath)
	{
		if(servletPath == null || servletPath.isEmpty()) {
			return null;
		}
		
		if(sMapForBlogPost.containsKey(servletPath)) {
			if(log.isLoggable(Level.FINER)) log.finer("Predefined servletPath = " + servletPath);
			return sMapForBlogPost.get(servletPath);
		}
		if(log.isLoggable(Level.INFO)) log.info("servletPath not recoginzed: " + servletPath);
		
		String[] typeAndFormat = new String[2];
		typeAndFormat[0] = null;
		typeAndFormat[1] = null;

		if(servletPath.startsWith("/")) {
			servletPath = servletPath.substring(1);
		}

		String[] parts = servletPath.split("/", 3);
		int len = parts.length;
		if(len > 1) {
			typeAndFormat[0] = parts[1];
			if(len > 2) {
				typeAndFormat[1] = parts[2];
			}
		}
		
		return typeAndFormat;
	}
	

}
