package com.cannyurl.util;

import java.util.logging.Logger;


// Temporary
public final class ShortUrlStreamUtil
{
    private static final Logger log = Logger.getLogger(ShortUrlStreamUtil.class.getName());

    private ShortUrlStreamUtil() {}
    
    
    // This is used to make the URL path "pretty".
    // Space in path is url-encoded to "%20", which is kind of ugly for date time string *yyyy-mm-dd HH:mm"
    // Instead, we simply use "+" in place of space " ".
    public static String getUrlPathForDateTimeString(String dataTime)
    {
        if(dataTime == null) {
            return null;  // ????
        }
        //String sanitized = dataTime.replaceAll("\\s+", "\\+");   // You cannot uniquely recover the original string if we use this transformation....
        String sanitized = dataTime.replaceAll(" ", "\\+");
        return sanitized;
    }
    public static String getDateTimeStringFromUrlPath(String sanitized)
    {
        if(sanitized == null) {
            return null;  // ????
        }
        String dateTime = sanitized.replaceAll("\\+", " ");
        return dateTime;
    }
    
    
    
}
