package com.cannyurl.util;

import java.util.logging.Logger;

import com.cannyurl.app.util.ConfigUtil;
import com.cannyurl.app.util.TextUtil;
import com.cannyurl.fe.bean.BookmarkLinkJsBean;
import com.cannyurl.fe.bean.NavLinkBaseJsBean;
import com.myurldb.ws.core.GUID;


// temporary
public class TwitterCardUtil
{
    private static final Logger log = Logger.getLogger(TwitterCardUtil.class.getName());

    private TwitterCardUtil() 
    {
        //
    }

    
    public static TwitterSummaryCardExBean generateTwitterSummaryCard(NavLinkBaseJsBean navLink)
    { 
        if(navLink instanceof BookmarkLinkJsBean) {
            return generateTwitterSummaryCardForBookmarkLink((BookmarkLinkJsBean) navLink);
        } else {
            // TBD:
            // ...
        }
        return null;
    }
    
    
    private static TwitterSummaryCardExBean generateTwitterSummaryCardForBookmarkLink(BookmarkLinkJsBean bookmarkLink)
    { 
        if(bookmarkLink == null) {
            return null;
        }
        
        TwitterSummaryCardExBean summaryCard = new TwitterSummaryCardExBean();
        
        String uuid = GUID.generate();
        summaryCard.setGuid(uuid);
        
        String site = ConfigUtil.getTwitterCardDefaultSite();
        if(site != null && !site.isEmpty()) {
            summaryCard.setSite(site);
        }
        // TBD: Does this make sense? BookmarkLinks are copies of other original content....
//        String creator = ConfigUtil.getTwitterCardDefaultCreator();
//        if(creator != null && !creator.isEmpty()) {
//            summaryCard.setCreator(creator);
//        }
        
//        String title = bookmarkLink.getSanitizedMessage();
//        if(title != null && !title.isEmpty()) {
//            if(title.length() > 200) {
//                title = TextUtil.truncateAtWordBoundary(title, 200, 100, false);
//            }
//            summaryCard.setTitle(title);
//        }
        
//        String description = bookmarkLink.getTextContent();   // ????
//        if(description != null && !description.isEmpty()) {
//            if(description.length() > 200) {
//                description = TextUtil.truncateAtWordBoundary(description, 200, 160, true);
//            }
//            summaryCard.setDescription(description);
//        }
        
//        String url = bookmarkLink.getPermalink();
//        if(url != null && !url.isEmpty()) {     // Length check???
//            summaryCard.setUrl(url);
//        }
        
//        String image = bookmarkLink.getImageUrl();
//        if(image != null && !image.isEmpty()) {     // Length check???
//            summaryCard.setImage(image);
//        }
        

        
        // Etc..
        // ...
        
        return summaryCard;
    }
    

    
    // TBD:
    // Use velocity template ???
    // Reference doc: https://dev.twitter.com/docs/cards
    private static String convertToHtml(TwitterSummaryCardExBean summaryCard)
    {
        if(summaryCard == null) {
            return null;
        }

        StringBuilder sb = new StringBuilder();
        // ...        
        
        return sb.toString();
    }

}
