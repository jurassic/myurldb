package com.cannyurl.util;

import com.cannyurl.af.util.HtmlTextUtil;
import com.cannyurl.fe.bean.TwitterSummaryCardJsBean;


public class TwitterSummaryCardExBean extends TwitterSummaryCardJsBean
{
    private static final long serialVersionUID = 1L;

    public TwitterSummaryCardExBean()
    {
        super();
    }

    public TwitterSummaryCardExBean(String guid, String card, String url,
            String title, String description, String site, String siteId,
            String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, Long createdTime,
            Long modifiedTime)
    {
        super(guid, card, url, title, description, site, siteId, creator, creatorId,
                image, imageWidth, imageHeight, createdTime, modifiedTime);
    }

    public TwitterSummaryCardExBean(String guid, String card, String url,
            String title, String description, String site, String siteId,
            String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight)
    {
        super(guid, card, url, title, description, site, siteId, creator, creatorId,
                image, imageWidth, imageHeight);
    }

    public TwitterSummaryCardExBean(String guid)
    {
        super(guid);
    }

    public TwitterSummaryCardExBean(TwitterSummaryCardJsBean bean)
    {
        super(bean);
    }


    public String getEscapedTitle()
    {
        String title = super.getTitle();
        // ???
        String escTitle = HtmlTextUtil.escapeForHtml(title);
        return escTitle;
    }

    public String getEscapedDescription()
    {
        String description = super.getDescription();
        // ???
        String escDescription = HtmlTextUtil.escapeForHtml(description);
        return escDescription;
    }


}
