package com.cannyurl.util;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

public class TwitterUtil
{
    private static final Logger log = Logger.getLogger(TwitterUtil.class.getName());

    private TwitterUtil() {}

    // temporary implementation
    private static final String INVALID_KEY = "comsumer_key_required";
    private static final Map<String, String> sKeyMap = new HashMap<String, String>();
    static {
        sKeyMap.put("http://www.su2.us/", "BlcfGSJG7esPoQMbaSzjEg");
        // sKeyMap.put("http://a.su2.us/", "RyTB1DR4sDHO9q4VMePfUQ");
        // sKeyMap.put("http://b.su2.us/", "RLkUy8BtPAUe6pHcN0ItgA");
        // sKeyMap.put("http://c.su2.us/", "R2rkKUljtRJYUKcowekB7w");
        // sKeyMap.put("http://d.su2.us/", "EZgbKpPgAU1i52fFAWLA");
        // sKeyMap.put("http://e.su2.us/", "4ysEdklsuSDmGJZiJyyg");
        // sKeyMap.put("http://f.su2.us/", "BbkkNSdC7nf0AF1eINFxIw");
        // sKeyMap.put("http://g.su2.us/", "rYOF4ZmG6KGqW9h8zRFQSQ");
        // sKeyMap.put("http://h.su2.us/", "PJy6OsPQLwQ3QSfVKZ5Kg");
        // sKeyMap.put("http://i.su2.us/", "19d4sUFkyjg8CnfqnkW6g");
        // sKeyMap.put("http://j.su2.us/", "jtX84UjsHhMtI25uDDWOEA");

        sKeyMap.put("http://k.su2.us/", "BlcfGSJG7esPoQMbaSzjEg");
        sKeyMap.put("http://l.su2.us/", "BlcfGSJG7esPoQMbaSzjEg");
        sKeyMap.put("http://m.su2.us/", "BlcfGSJG7esPoQMbaSzjEg");
        sKeyMap.put("http://n.su2.us/", "BlcfGSJG7esPoQMbaSzjEg");
        sKeyMap.put("http://o.su2.us/", "BlcfGSJG7esPoQMbaSzjEg");

        // sKeyMap.put("http://p.su2.us/", "RyTB1DR4sDHO9q4VMePfUQ");
        // sKeyMap.put("http://q.su2.us/", "RyTB1DR4sDHO9q4VMePfUQ");
        // sKeyMap.put("http://r.su2.us/", "RyTB1DR4sDHO9q4VMePfUQ");
        // sKeyMap.put("http://s.su2.us/", "RyTB1DR4sDHO9q4VMePfUQ");

        // sKeyMap.put("http://t.su2.us/", "RLkUy8BtPAUe6pHcN0ItgA");
        // sKeyMap.put("http://u.su2.us/", "RLkUy8BtPAUe6pHcN0ItgA");
        // sKeyMap.put("http://v.su2.us/", "RLkUy8BtPAUe6pHcN0ItgA");
        // sKeyMap.put("http://w.su2.us/", "RLkUy8BtPAUe6pHcN0ItgA");

        // sKeyMap.put("http://x.su2.us/", "R2rkKUljtRJYUKcowekB7w");
        // sKeyMap.put("http://y.su2.us/", "R2rkKUljtRJYUKcowekB7w");
        // sKeyMap.put("http://z.su2.us/", "R2rkKUljtRJYUKcowekB7w");

        // ...
        sKeyMap.put("http://www.mq.ms/", "Qls1dFrveR3Vq3mWjKLw");
        sKeyMap.put("http://a.mq.ms/", "UWVv8NSGWB39Fvxvj2BBw");
        sKeyMap.put("http://b.mq.ms/", "09klKHMG5imEYtH2Th9A");
        sKeyMap.put("http://c.mq.ms/", "mmk2tGqv1lZFVLqdw9EoQ");
        sKeyMap.put("http://d.mq.ms/", "hui59tUJoW4V8HhcT5ojw");
        sKeyMap.put("http://e.mq.ms/", "qtufvDJynDb787sgWPUoHw");
        sKeyMap.put("http://f.mq.ms/", "Dyxj9MBQthA8keiuRWlBQ");
        sKeyMap.put("http://g.mq.ms/", "GDNhEpOuhlUI3jmkkSSg");
        sKeyMap.put("http://h.mq.ms/", "MSeorCq5EOjWNMoHWprbA");
        sKeyMap.put("http://i.mq.ms/", "AquykZEh55mnRUSyjWk7Ig");
        sKeyMap.put("http://j.mq.ms/", "cDoawg8ewOcOOzlaLSnzQ");

        sKeyMap.put("http://k.mq.ms/", "Qls1dFrveR3Vq3mWjKLw");
        sKeyMap.put("http://l.mq.ms/", "Qls1dFrveR3Vq3mWjKLw");
        sKeyMap.put("http://m.mq.ms/", "Qls1dFrveR3Vq3mWjKLw");
        sKeyMap.put("http://n.mq.ms/", "Qls1dFrveR3Vq3mWjKLw");
        sKeyMap.put("http://o.mq.ms/", "Qls1dFrveR3Vq3mWjKLw");

        sKeyMap.put("http://p.mq.ms/", "UWVv8NSGWB39Fvxvj2BBw");
        sKeyMap.put("http://q.mq.ms/", "UWVv8NSGWB39Fvxvj2BBw");
        sKeyMap.put("http://r.mq.ms/", "UWVv8NSGWB39Fvxvj2BBw");
        sKeyMap.put("http://s.mq.ms/", "UWVv8NSGWB39Fvxvj2BBw");

        sKeyMap.put("http://t.mq.ms/", "09klKHMG5imEYtH2Th9A");
        sKeyMap.put("http://u.mq.ms/", "09klKHMG5imEYtH2Th9A");
        sKeyMap.put("http://v.mq.ms/", "09klKHMG5imEYtH2Th9A");
        sKeyMap.put("http://w.mq.ms/", "09klKHMG5imEYtH2Th9A");

        sKeyMap.put("http://x.mq.ms/", "mmk2tGqv1lZFVLqdw9EoQ");
        sKeyMap.put("http://y.mq.ms/", "mmk2tGqv1lZFVLqdw9EoQ");
        sKeyMap.put("http://z.mq.ms/", "mmk2tGqv1lZFVLqdw9EoQ");

//        // ...
//        sKeyMap.put("http://www.fa0.us/", "3HP1NDlSEIuft9dXKVDPQ");
//        sKeyMap.put("http://a.fa0.us/", "3HP1NDlSEIuft9dXKVDPQ");
//        sKeyMap.put("http://b.fa0.us/", "3HP1NDlSEIuft9dXKVDPQ");
//        sKeyMap.put("http://c.fa0.us/", "3HP1NDlSEIuft9dXKVDPQ");
//        sKeyMap.put("http://d.fa0.us/", "3HP1NDlSEIuft9dXKVDPQ");
//
//        sKeyMap.put("http://e.fa0.us/", "tRDmjV5tcBXLzOZd3Ujg");
//        sKeyMap.put("http://f.fa0.us/", "tRDmjV5tcBXLzOZd3Ujg");
//        sKeyMap.put("http://g.fa0.us/", "tRDmjV5tcBXLzOZd3Ujg");
//        sKeyMap.put("http://h.fa0.us/", "tRDmjV5tcBXLzOZd3Ujg");
//        sKeyMap.put("http://i.fa0.us/", "tRDmjV5tcBXLzOZd3Ujg");
//
//        sKeyMap.put("http://j.fa0.us/", "UppERcvWhoQhfVAOh8Og");
//        sKeyMap.put("http://k.fa0.us/", "UppERcvWhoQhfVAOh8Og");
//        sKeyMap.put("http://l.fa0.us/", "UppERcvWhoQhfVAOh8Og");
//        sKeyMap.put("http://m.fa0.us/", "UppERcvWhoQhfVAOh8Og");
//
//        sKeyMap.put("http://n.fa0.us/", "gia83Az2cwWEiYXfiHEQ");
//        sKeyMap.put("http://o.fa0.us/", "gia83Az2cwWEiYXfiHEQ");
//        sKeyMap.put("http://p.fa0.us/", "gia83Az2cwWEiYXfiHEQ");
//        sKeyMap.put("http://q.fa0.us/", "gia83Az2cwWEiYXfiHEQ");
//
//        sKeyMap.put("http://r.fa0.us/", "wF6ohwoSrJUoo2pMlfNbig");
//        sKeyMap.put("http://s.fa0.us/", "wF6ohwoSrJUoo2pMlfNbig");
//        sKeyMap.put("http://t.fa0.us/", "wF6ohwoSrJUoo2pMlfNbig");
//        sKeyMap.put("http://u.fa0.us/", "wF6ohwoSrJUoo2pMlfNbig");
//
//        sKeyMap.put("http://v.fa0.us/", "j3hQ7hK4TC0GPp5a1AdIw");
//        sKeyMap.put("http://w.fa0.us/", "j3hQ7hK4TC0GPp5a1AdIw");
//        sKeyMap.put("http://x.fa0.us/", "j3hQ7hK4TC0GPp5a1AdIw");
//        sKeyMap.put("http://y.fa0.us/", "j3hQ7hK4TC0GPp5a1AdIw");
//        sKeyMap.put("http://z.fa0.us/", "j3hQ7hK4TC0GPp5a1AdIw");

        // ...
        sKeyMap.put("http://www.fm.gs/", "hN2QgrQCZPx01DGMZ7EnA");
        sKeyMap.put("http://a.fm.gs/", "hN2QgrQCZPx01DGMZ7EnA");
        sKeyMap.put("http://b.fm.gs/", "hN2QgrQCZPx01DGMZ7EnA");
        sKeyMap.put("http://c.fm.gs/", "hN2QgrQCZPx01DGMZ7EnA");
        sKeyMap.put("http://d.fm.gs/", "hN2QgrQCZPx01DGMZ7EnA");

        sKeyMap.put("http://e.fm.gs/", "WVKsn90dRZOix8MOKw9VHw");
        sKeyMap.put("http://f.fm.gs/", "WVKsn90dRZOix8MOKw9VHw");
        sKeyMap.put("http://g.fm.gs/", "WVKsn90dRZOix8MOKw9VHw");
        sKeyMap.put("http://h.fm.gs/", "WVKsn90dRZOix8MOKw9VHw");
        sKeyMap.put("http://i.fm.gs/", "WVKsn90dRZOix8MOKw9VHw");

        sKeyMap.put("http://j.fm.gs/", "n7UAXv4eyqwbq5jEi4oBig");
        sKeyMap.put("http://k.fm.gs/", "n7UAXv4eyqwbq5jEi4oBig");
        sKeyMap.put("http://l.fm.gs/", "n7UAXv4eyqwbq5jEi4oBig");
        sKeyMap.put("http://m.fm.gs/", "n7UAXv4eyqwbq5jEi4oBig");

        sKeyMap.put("http://n.fm.gs/", "DlXfz4Zse08rfrqUKAnYg");
        sKeyMap.put("http://o.fm.gs/", "DlXfz4Zse08rfrqUKAnYg");
        sKeyMap.put("http://p.fm.gs/", "DlXfz4Zse08rfrqUKAnYg");
        sKeyMap.put("http://q.fm.gs/", "DlXfz4Zse08rfrqUKAnYg");

        sKeyMap.put("http://r.fm.gs/", "HqfBYCEnfIykK3rMLVKA");
        sKeyMap.put("http://s.fm.gs/", "HqfBYCEnfIykK3rMLVKA");
        sKeyMap.put("http://t.fm.gs/", "HqfBYCEnfIykK3rMLVKA");
        sKeyMap.put("http://u.fm.gs/", "HqfBYCEnfIykK3rMLVKA");

        sKeyMap.put("http://v.fm.gs/", "YgM8Zqs056vuMyuiOlhA");
        sKeyMap.put("http://w.fm.gs/", "YgM8Zqs056vuMyuiOlhA");
        sKeyMap.put("http://x.fm.gs/", "YgM8Zqs056vuMyuiOlhA");
        sKeyMap.put("http://y.fm.gs/", "YgM8Zqs056vuMyuiOlhA");
        sKeyMap.put("http://z.fm.gs/", "YgM8Zqs056vuMyuiOlhA");

        // ...
        sKeyMap.put("http://www.oc.gs/", "IKm0CtHwoxyIlFWYvvJDyQ");
        sKeyMap.put("http://a.oc.gs/", "IKm0CtHwoxyIlFWYvvJDyQ");
        sKeyMap.put("http://b.oc.gs/", "IKm0CtHwoxyIlFWYvvJDyQ");
        sKeyMap.put("http://c.oc.gs/", "IKm0CtHwoxyIlFWYvvJDyQ");
        sKeyMap.put("http://d.oc.gs/", "IKm0CtHwoxyIlFWYvvJDyQ");

        sKeyMap.put("http://e.oc.gs/", "XxBIm4OKmAgnmJZTBoUwA");
        sKeyMap.put("http://f.oc.gs/", "XxBIm4OKmAgnmJZTBoUwA");
        sKeyMap.put("http://g.oc.gs/", "XxBIm4OKmAgnmJZTBoUwA");
        sKeyMap.put("http://h.oc.gs/", "XxBIm4OKmAgnmJZTBoUwA");
        sKeyMap.put("http://i.oc.gs/", "XxBIm4OKmAgnmJZTBoUwA");

        sKeyMap.put("http://j.oc.gs/", "717o9W8q2rkttT3RYpQ5Q");
        sKeyMap.put("http://k.oc.gs/", "717o9W8q2rkttT3RYpQ5Q");
        sKeyMap.put("http://l.oc.gs/", "717o9W8q2rkttT3RYpQ5Q");
        sKeyMap.put("http://m.oc.gs/", "717o9W8q2rkttT3RYpQ5Q");

        sKeyMap.put("http://n.oc.gs/", "xRQPiQ5VplNzxwnZiq5ALA");
        sKeyMap.put("http://o.oc.gs/", "xRQPiQ5VplNzxwnZiq5ALA");
        sKeyMap.put("http://p.oc.gs/", "xRQPiQ5VplNzxwnZiq5ALA");
        sKeyMap.put("http://q.oc.gs/", "xRQPiQ5VplNzxwnZiq5ALA");

        sKeyMap.put("http://r.oc.gs/", "TIAaPAegVJox7rD1usttog");
        sKeyMap.put("http://s.oc.gs/", "TIAaPAegVJox7rD1usttog");
        sKeyMap.put("http://t.oc.gs/", "TIAaPAegVJox7rD1usttog");
        sKeyMap.put("http://u.oc.gs/", "TIAaPAegVJox7rD1usttog");

        sKeyMap.put("http://v.oc.gs/", "pEqFJDZGz7E7jVeoLdG44A");
        sKeyMap.put("http://w.oc.gs/", "pEqFJDZGz7E7jVeoLdG44A");
        sKeyMap.put("http://x.oc.gs/", "pEqFJDZGz7E7jVeoLdG44A");
        sKeyMap.put("http://y.oc.gs/", "pEqFJDZGz7E7jVeoLdG44A");
        sKeyMap.put("http://z.oc.gs/", "pEqFJDZGz7E7jVeoLdG44A");

        // ...
        sKeyMap.put("http://www.twr.im/", "muOvD7mApUZ1pfi9MjMg");
        sKeyMap.put("http://a.twr.im/", "PixtpYRRJEoQd9lOWxO39w");
        sKeyMap.put("http://b.twr.im/", "JG1XVn1t5Q6raOfzNbL0Q");
        
        
        
        // ...
        //sKeyMap.put("http://www.iu.am/", "");
        //sKeyMap.put("http://i.iu.am/", "");
        //sKeyMap.put("http://u.iu.am/", "");
        // ...
        // etc.
        // ...

        
        // For devel testing...
        sKeyMap.put("http://localhost:8899/", "abcdef");
        // For devel testing...

    }

    public static String getConsumerKey(String topLevelUrl)
    {
        if(sKeyMap.containsKey(topLevelUrl)) {
            return sKeyMap.get(topLevelUrl);
        } else {
            log.warning("Invalid key = " + topLevelUrl);
            return INVALID_KEY;
        }
    }

    public static boolean isTwitterSetUp(String topLevelUrl)
    {
        return sKeyMap.containsKey(topLevelUrl);
    }

}
