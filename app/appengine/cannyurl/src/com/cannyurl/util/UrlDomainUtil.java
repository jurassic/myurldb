package com.cannyurl.util;

import java.util.logging.Logger;


// Copy of com.urlmover.util.DomainUtil...
public final class UrlDomainUtil
{
    private static final Logger log = Logger.getLogger(UrlDomainUtil.class.getName());

    private UrlDomainUtil() {}
    

    // TBD:
    // Returns the baseUrl, etc., "http://abc.com", ... (Note: No trailing slash.)
    public static String getDomain(String url)
    {
        return getBaseUrl(url);
    }
    public static String getBaseUrl(String url)
    {
        String[] parts = parseUrl(url);
        if(parts != null && parts.length > 0) {
            return parts[0];
        } else {
            return null;
        }
    }
    // Returns [baseUrl, path]
    // Note the the path starts with "/" and baseUrl does not include the last "/".
    public static String[] parseUrl(String url)
    {
        if(url == null) {
            return null;
        }
        String[] parts = new String[2];
        int idx1 = url.indexOf("//");
        if(idx1 > 0) {
            int idx2 = url.indexOf('/', idx1+2);
            if(idx2 > 0) {
                parts[0] = url.substring(0, idx2);
                parts[1] = url.substring(idx2);
            } else {
                parts[0] = url;
                parts[1] = "";   // ????
            }
        } else {
            // ???
            parts[0] = url;    // ????
            parts[1] = null;   // ????
        }
        return parts;
    }
    
    public static String buildUrl(String baseUrl, String path) 
    {
        if(baseUrl == null || baseUrl.isEmpty()) {  // ???
            return null;   // ???
        }
        if(baseUrl.endsWith("/")) {
            baseUrl = baseUrl.substring(0, baseUrl.length() - 1);
        }
        if(path == null) {
            path = "";   // ???
        }
        if(path.length() > 0 && !path.startsWith("/")) {
            path = "/" + path;   // Note: only exception: "" -> "".
        }
        String fullUrl = baseUrl + path;
        return fullUrl;
    }
    
}
