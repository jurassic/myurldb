package com.cannyurl.validate;

import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import com.cannyurl.app.util.ConfigUtil;
import com.cannyurl.app.util.ReservedWordRegistry;
import com.cannyurl.fe.WebException;
import com.cannyurl.helper.AppClientHelper;


// TBD:
// Where does the validate package go????
// util ... -> app.util ... -> app.helper ...  helper ...  -> validate ... -> servlet ?????
// ....
// Put validate before app.X ????
// ...

public class ClientCodeValidater
{
    private static final Logger log = Logger.getLogger(ClientCodeValidater.class.getName());

    private ClientCodeValidater() {}
    
    private static Pattern sPattern = null;
    static {
        // two or more chars starting with alphabets...
        // Case insensitive??? Make it lower cases only????
        String regex = "^[a-zA-Z][a-zA-Z0-9_]+$"; 
        // String regex = "^[a-z][a-z0-9_]+$"; 
        sPattern = Pattern.compile(regex);
    }


    // temporary
    // TBD: We should "fold the case"... that is, all clientCode should be lower cases only...
    public static boolean isValidClientCode(String clientCode)
    {
        if(clientCode == null) {
            return false;
        }
        // clientCode = clientCode.trim();   // Regex takes care of this...
        // trim() should not really be called.
        // clientCode including space is not valid.

        // Length constraints.
        int clientCodeLength = clientCode.length();
        int minLength = ConfigUtil.getClientCodeMinLength();
        int maxLength = ConfigUtil.getClientCodeMaxLength();
        if(clientCodeLength < minLength) {
            return false;
        }
        if(clientCodeLength > maxLength) {
            return false;
        }
        
        // "Reserved word" check...
        Set<String> reservedWords = ReservedWordRegistry.getReservedWordSet(clientCodeLength);
        if(reservedWords != null && !reservedWords.isEmpty()) {
            if(reservedWords.contains(clientCode.toLowerCase())) {
                if(log.isLoggable(Level.INFO)) log.info("clientCode is invalid because it is a reserved word: " + clientCode);
                return false;
            }
        }

        // Use regex.
        if(! sPattern.matcher(clientCode).matches()) {
            return false;
        }
        // ....

        // Check the DB..
        // Note that we can only check the lower cases only (We'll need an additional field in the table to support upper/lower case alphabets) 
        String key = null;
        try {
            String lowercase = clientCode.toLowerCase();
            key = AppClientHelper.getInstance().findAppClientKeyByClientCode(lowercase);
        } catch(WebException e) {
            log.log(Level.WARNING, "Error while querying AppClient: clientCode = " + clientCode, e);
            return false;   // ????
        }
    
        if(key == null) {
            return true;
        } else {
            if(log.isLoggable(Level.INFO)) log.info("AppClient already exists for clientCode = " + clientCode);
            return false;
        }
    }


}
