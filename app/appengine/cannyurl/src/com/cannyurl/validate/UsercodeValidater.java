package com.cannyurl.validate;

import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import com.cannyurl.app.util.ConfigUtil;
import com.cannyurl.app.util.ReservedWordRegistry;
import com.cannyurl.fe.WebException;
import com.cannyurl.helper.UserUsercodeHelper;


// TBD:
// Where does the validate package go????
// util ... -> app.util ... -> app.helper ...  helper ...  -> validate ... -> servlet ?????
// ....
// Put validate before app.X ????
// ...

public class UsercodeValidater
{
    private static final Logger log = Logger.getLogger(UsercodeValidater.class.getName());

    private UsercodeValidater() {}
    
    private static Pattern sPattern = null;
    static {
        // two or more chars starting with alphabets...
        // Case insensitive??? Make it lower cases only????
        String regex = "^[a-zA-Z][a-zA-Z0-9_]+$"; 
        // String regex = "^[a-z][a-z0-9_]+$"; 
        sPattern = Pattern.compile(regex);
    }


    // temporary
    // TBD: We should "fold the case"... that is, all usercode should be lower cases only...
    public static boolean isValidUsercode(String usercode)
    {
        if(usercode == null) {
            return false;
        }
        // usercode = usercode.trim();   // Regex takes care of this...
        // trim() should not really be called.
        // usercode including space is not valid.

        // Length constraints.
        int usercodeLength = usercode.length();
        int minLength = ConfigUtil.getUsercodeMinLength();
        int maxLength = ConfigUtil.getUsercodeMaxLength();
        if(usercodeLength < minLength) {
            return false;
        }
        if(usercodeLength > maxLength) {
            return false;
        }
        
        // "Reserved word" check...
        Set<String> reservedWords = ReservedWordRegistry.getReservedWordSet(usercodeLength);
        if(reservedWords != null && !reservedWords.isEmpty()) {
            if(reservedWords.contains(usercode.toLowerCase())) {
                if(log.isLoggable(Level.INFO)) log.info("usercode is invalid because it is a reserved word: " + usercode);
                return false;
            }
        }

        // Use regex.
        if(! sPattern.matcher(usercode).matches()) {
            return false;
        }
        // ....

        // Check the DB..
        // TBD: Query User or UserUsercode???
        // Note that we can only check the lower cases only (We'll need an additional field in the table to support upper/lower case alphabets) 
        String key = null;
        try {
            String lowercase = usercode.toLowerCase();
            key = UserUsercodeHelper.getInstance().findUserUsercodeKeyByUsercode(lowercase);
        } catch(WebException e) {
            log.log(Level.WARNING, "Error while querying UserUsercode: usercode = " + usercode, e);
            return false;   // ????
        }
    
        if(key == null) {
            return true;
        } else {
            if(log.isLoggable(Level.INFO)) log.info("UserUsercode already exists for usercode = " + usercode);
            return false;
        }
    }


}
