package com.cannyurl.validate.ajax;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cannyurl.fe.WebException;
import com.cannyurl.helper.ShortLinkHelper;
import com.cannyurl.validate.ClientCodeValidater;
import com.myurldb.ws.core.StatusCode;


// Mainly, for ajax calls....
public class ClientCodeValidateServlet extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ClientCodeValidateServlet.class.getName());

    // Query params...
    // private static final String QUERY_PARAM_LENGTH = "length";
    private static final String QUERY_PARAM_JSONP_CALLBACK1 = "jsonp";
    private static final String QUERY_PARAM_JSONP_CALLBACK2 = "callback";

    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        log.info("doGet() called.");

        // TBD:
        // Based on pathInfo + query params
        // Process different requests...  ???
        // ...
        String requestUrl = req.getRequestURL().toString();
        String contextPath = req.getContextPath();
        String servletPath = req.getServletPath();
        String pathInfo = req.getPathInfo();
        String queryString = req.getQueryString();
        log.fine("requestUrl = " + requestUrl);
        log.fine("contextPath = " + contextPath);
        log.fine("servletPath = " + servletPath);
        log.fine("pathInfo = " + pathInfo);
        log.fine("queryString = " + queryString);

        
        
        // URL pattern....
        // /ajax/validate/clientCode/<clientCode>
        // ....

        if(pathInfo == null || pathInfo.isEmpty() || pathInfo.equals("/")) {
            if(log.isLoggable(Level.WARNING)) log.warning("Invalid pathInfo.");
            resp.setStatus(StatusCode.BAD_REQUEST);
            return;
        }

        
        String clientCode = pathInfo.substring(1);   // Exclude the leading "/"...
        if(log.isLoggable(Level.FINE)) log.fine("clientCode = " + clientCode);
        
        
        // JSONP support
        String jsonpCallback = null;
        String[] callbacks1 = req.getParameterValues(QUERY_PARAM_JSONP_CALLBACK1);
        if(callbacks1 != null && callbacks1.length > 0) {
            jsonpCallback = callbacks1[0];  // ???
        }
        if(jsonpCallback == null || jsonpCallback.isEmpty()) {
            String[] callbacks2 = req.getParameterValues(QUERY_PARAM_JSONP_CALLBACK2);
            if(callbacks2 != null && callbacks2.length > 0) {
                jsonpCallback = callbacks2[0];  // ???
            }
        }
        
        boolean isValid = ClientCodeValidater.isValidClientCode(clientCode);

        String jsonStr = "{\"valid\":" + isValid + "}";
        if(jsonpCallback != null && !jsonpCallback.isEmpty()) {
            jsonStr = jsonpCallback + "(" + jsonStr + ")";
            //resp.setContentType("application/javascript");  // ???
            resp.setContentType("application/javascript;charset=UTF-8");
        } else {
            //resp.setContentType("application/json");  // ????
            resp.setContentType("application/json;charset=UTF-8");  // ???                
        }
        PrintWriter writer = resp.getWriter();
        writer.print(jsonStr);
        resp.setStatus(StatusCode.OK);  
        // ...
        

        log.info("doGet(): BOTTOM");
    }

}
