package com.cannyurl.wa.altservlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//import com.myurldb.ws.GeoLink;
//import com.myurldb.ws.CellLatitudeLongitude;
//import com.myurldb.ws.GeoCoordinateStruct;
import com.myurldb.ws.core.StatusCode;
import com.cannyurl.fe.Validateable;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.GeoLinkJsBean;
import com.cannyurl.fe.bean.CellLatitudeLongitudeJsBean;
import com.cannyurl.fe.bean.GeoCoordinateStructJsBean;
import com.cannyurl.wa.service.GeoLinkWebService;


// Provides JSONP support
public class GeoLinkAltServlet extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(GeoLinkAltServlet.class.getName());
    private static final String FORMBEAN_CLASS_NAME = "com.myurldb.form.bean.GeoLinkFormBean";

    private static final String QUERY_PARAM_JSONP_PAYLOAD = "payload";
    private static final String QUERY_PARAM_JSONP_CALLBACK1 = "jsonp";
    private static final String QUERY_PARAM_JSONP_CALLBACK2 = "callback";


    // TBD: Is this safe for concurrent calls???
    private GeoLinkWebService mService = null;
    private GeoLinkWebService getService()
    {
        if(mService == null) {
            mService = new GeoLinkWebService();
        }
        return mService;
    }

    private Class<?> formBeanClass = null;
    private void loadFormBeanClass()
    {
        try {
            formBeanClass = Class.forName(FORMBEAN_CLASS_NAME);
        } catch(Exception e) {
            // ignore
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, FORMBEAN_CLASS_NAME + " does not exist.", e);
        }
    }


    @Override
    public void init() throws ServletException
    {
        super.init();
        loadFormBeanClass();
    }

    @Override
    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        loadFormBeanClass();
    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        log.finer("doGet() called.");

        // TBD: Check Accept header. Etc...

        String requestUrl = req.getRequestURL().toString();
        String contextPath = req.getContextPath();
        String servletPath = req.getServletPath();
        String pathInfo = req.getPathInfo();
        String queryString = req.getQueryString();
        if(log.isLoggable(Level.FINE)) {
            log.fine("requestUrl = " + requestUrl);
            log.fine("contextPath = " + contextPath);
            log.fine("servletPath = " + servletPath);
            log.fine("pathInfo = " + pathInfo);
            log.fine("queryString = " + queryString);
        }

        // JSONP support
        String jsonpPayload = null;
        String[] payloads1 = req.getParameterValues(QUERY_PARAM_JSONP_PAYLOAD);
        if(payloads1 != null && payloads1.length > 0) {
            jsonpPayload = payloads1[0];  // ???
        }
        String jsonpCallback = null;
        String[] callbacks1 = req.getParameterValues(QUERY_PARAM_JSONP_CALLBACK1);
        if(callbacks1 != null && callbacks1.length > 0) {
            jsonpCallback = callbacks1[0];  // ???
        }
        if(jsonpCallback == null || jsonpCallback.isEmpty()) {
            String[] callbacks2 = req.getParameterValues(QUERY_PARAM_JSONP_CALLBACK2);
            if(callbacks2 != null && callbacks2.length > 0) {
                jsonpCallback = callbacks2[0];  // ???
            }
        }        

        String guid = null;
        if(pathInfo == null || pathInfo.isEmpty()) {
            // Get list???
            // For now, use the query param. ???
            guid = req.getParameter("guid");
        } else {
            if(pathInfo.startsWith("/")) {
                guid = pathInfo.substring(1);
            } else {
                guid = pathInfo;
            }
        }

        if(jsonpPayload == null) {
            if(guid != null && !guid.isEmpty()) {
                try {
                    // [1] GET
                    GeoLinkJsBean bean = getService().getGeoLink(guid);
                    String jsonStr = bean.toJsonString();
                    if(jsonpCallback != null && !jsonpCallback.isEmpty()) {
                        jsonStr = jsonpCallback + "(" + jsonStr + ")";
                        //resp.setContentType("application/javascript");  // ???
                        resp.setContentType("application/javascript;charset=UTF-8");
                    } else {
                        //resp.setContentType("application/json");  // ????
                        resp.setContentType("application/json;charset=UTF-8");  // ???                
                    }
                    // resp.setHeader("Cache-Control", "no-cache");     // ?????
                    resp.setStatus(StatusCode.OK);   // StatusCode.ACCEPTED ?
                    PrintWriter out = resp.getWriter();
                    out.write(jsonStr);
                } catch (WebException e) {
                    log.log(Level.WARNING, "Failed.", e);
                    resp.setStatus(StatusCode.NOT_FOUND);  // ???
                }
            } else {
                try {
                    // [2] List
                    String filter = req.getParameter("filter");
                    String ordering = req.getParameter("ordering");
                    Long offset = null;
                    String strOffset = req.getParameter("offset");
                    if(strOffset != null) {
                        try {
                            offset = Long.parseLong(strOffset);
                        } catch(Exception e) {
                            // ignore
                            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Invalid param: offset = " + strOffset, e);
                        }
                    }
                    Integer count = null;
                    String strCount = req.getParameter("count");
                    if(strCount != null) {
                        try {
                            count = Integer.parseInt(strCount);
                        } catch(Exception e) {
                            // ignore
                            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Invalid param: count = " + strCount, e);
                        }
                    }
                    if(count == null) {
                        // To prevent accidental fetching of a large number of records...
                        count = 10;  // temporary
                        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Param count is missing or invalid. Using count = " + count);
                    }

                    List<GeoLinkJsBean> beans = getService().findGeoLinks(filter, ordering, null, null, null, null, offset, count);
                    if(beans != null) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("[");
                        for(int i=0; i<beans.size(); i++) {
                            GeoLinkJsBean b = beans.get(i);
                            sb.append(b.toJsonString());
                            if(i < beans.size()-1) {
                                sb.append(",");
                            }
                        }
                        sb.append("]");
                        String jsonStr = sb.toString(); 

                        if(jsonpCallback != null && !jsonpCallback.isEmpty()) {
                            jsonStr = jsonpCallback + "(" + jsonStr + ")";
                            //resp.setContentType("application/javascript");  // ???
                            resp.setContentType("application/javascript;charset=UTF-8");
                        } else {
                            //resp.setContentType("application/json");  // ????
                            resp.setContentType("application/json;charset=UTF-8");  // ???                
                        }
                        // resp.setHeader("Cache-Control", "no-cache");     // ?????
                        resp.setStatus(StatusCode.OK);
                        PrintWriter out = resp.getWriter();
                        out.write(jsonStr);                
                    } else {
                        resp.setStatus(StatusCode.NOT_FOUND);  // ???
                    }
                } catch (WebException e) {
                    log.log(Level.WARNING, "Failed.", e);
                    resp.setStatus(StatusCode.INTERNAL_SERVER_ERROR);
                }
            }
        } else {
            // JSONP ajax form submit (through GET)
            
            boolean isPost = true;
            if(guid != null && !guid.isEmpty()) {
                isPost = false; 
            }

            boolean validated = true;
            boolean succeeded = false;
            GeoLinkJsBean outBean = null;
            try {
                String jsonStr = "";
                if(jsonpPayload != null) {
                    jsonStr = jsonpPayload;
                }
                GeoLinkJsBean inBean = null;
                if(formBeanClass != null) {
                    try {
                        if(jsonStr != null && !jsonStr.isEmpty()) {
                            inBean = (GeoLinkJsBean) formBeanClass.getMethod("fromJsonString", String.class).invoke(null, jsonStr);
                        } else {
                            // This should not normally happen...
                            inBean = (GeoLinkJsBean) formBeanClass.getConstructor().newInstance();
                            //inBean = (GeoLinkJsBean) formBeanClass.getConstructor(String.class).newInstance(guid);
                        }
                    } catch(Exception e1) {
                        if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, FORMBEAN_CLASS_NAME + ".fromJsonString() failed.", e1);
                    }
                }
                if(inBean == null) {
                    if(jsonStr != null && !jsonStr.isEmpty()) {
                        inBean = GeoLinkJsBean.fromJsonString(jsonStr);
                    } else {
                        // This should not normally happen...
                        inBean = new GeoLinkJsBean();
                        //inBean = new GeoLinkJsBean(guid);
                    }
                }
                if(inBean instanceof Validateable) {
                    validated = ((Validateable) inBean).validate();
                }
                if(validated == true) {
                    try {
                        if(isPost == true) {
                            // [3] POST
                            outBean = getService().constructGeoLink(inBean);
                            // String guid = getService().createGeoLink(inBean);
                        } else {
                            // [4] PUT
                            outBean = getService().refreshGeoLink(inBean);
                            // Boolean suc = getService().updateGeoLink(inBean);
                        }
                        succeeded = true;
                    } catch (WebException e1) {
                        log.log(Level.WARNING, "Server error.", e1);
                    }
                }
                if(outBean == null) {
                    // ???
                    outBean = inBean;
                }
            } catch (Exception e) {
                log.log(Level.WARNING, "Unknown error.", e);
            }

            
            if(succeeded == true) {
                // Location header???
                // guid = outBean.getGuid();
                String jsonStr = outBean.toJsonString();
                if(jsonpCallback != null && !jsonpCallback.isEmpty()) {
                    jsonStr = jsonpCallback + "(" + jsonStr + ")";
                    //resp.setContentType("application/javascript");  // ???
                    resp.setContentType("application/javascript;charset=UTF-8");
                } else {
                    //resp.setContentType("application/json");  // ????
                    resp.setContentType("application/json;charset=UTF-8");  // ???                
                }
                // resp.setHeader("Cache-Control", "no-cache");     // ?????
                resp.setStatus(StatusCode.OK);   // StatusCode.ACCEPTED ?
                PrintWriter out = resp.getWriter();
                out.write(jsonStr);
            } else {
                if(outBean == null) {
                    resp.setStatus(StatusCode.INTERNAL_SERVER_ERROR);  // ????
                } else {
                    if(validated == false) {
                        resp.setStatus(StatusCode.BAD_REQUEST);
                    } else {
                        resp.setStatus(StatusCode.INTERNAL_SERVER_ERROR);  // ???
                        if(outBean instanceof Validateable) {
                            ((Validateable) outBean).addError(Validateable.FIELD_BEANWIDE, "Server error.");
                        }
                        PrintWriter out = resp.getWriter();
                        out.write(outBean.toJsonString());
                    }
                }
            }
        }
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        log.finer("doPost() called.");
        throw new ServletException("Not supported.");
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        log.finer("doPut() called.");
        throw new ServletException("Not supported.");
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        log.finer("doDelete() called.");
        throw new ServletException("Not supported.");
    }
    
    
}
