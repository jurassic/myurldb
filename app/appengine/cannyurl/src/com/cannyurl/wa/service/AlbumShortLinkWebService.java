package com.cannyurl.wa.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.AlbumShortLink;
import com.cannyurl.af.bean.AlbumShortLinkBean;
import com.cannyurl.af.service.AlbumShortLinkService;
import com.cannyurl.af.service.manager.ServiceManager;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.AlbumShortLinkJsBean;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class AlbumShortLinkWebService // implements AlbumShortLinkService
{
    private static final Logger log = Logger.getLogger(AlbumShortLinkWebService.class.getName());
     
    // Af service interface.
    private AlbumShortLinkService mService = null;

    public AlbumShortLinkWebService()
    {
        this(ServiceManager.getAlbumShortLinkService());
    }
    public AlbumShortLinkWebService(AlbumShortLinkService service)
    {
        mService = service;
    }
    
    private AlbumShortLinkService getService()
    {
        if(mService == null) {
            mService = ServiceManager.getAlbumShortLinkService();
        }
        return mService;
    }
    
    
    public AlbumShortLinkJsBean getAlbumShortLink(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            AlbumShortLink albumShortLink = getService().getAlbumShortLink(guid);
            AlbumShortLinkJsBean bean = convertAlbumShortLinkToJsBean(albumShortLink);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getAlbumShortLink(String guid, String field) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().getAlbumShortLink(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<AlbumShortLinkJsBean> getAlbumShortLinks(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<AlbumShortLinkJsBean> jsBeans = new ArrayList<AlbumShortLinkJsBean>();
            List<AlbumShortLink> albumShortLinks = getService().getAlbumShortLinks(guids);
            if(albumShortLinks != null) {
                for(AlbumShortLink albumShortLink : albumShortLinks) {
                    jsBeans.add(convertAlbumShortLinkToJsBean(albumShortLink));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<AlbumShortLinkJsBean> getAllAlbumShortLinks() throws WebException
    {
        return getAllAlbumShortLinks(null, null, null);
    }

    public List<AlbumShortLinkJsBean> getAllAlbumShortLinks(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<AlbumShortLinkJsBean> jsBeans = new ArrayList<AlbumShortLinkJsBean>();
            List<AlbumShortLink> albumShortLinks = getService().getAllAlbumShortLinks(ordering, offset, count);
            if(albumShortLinks != null) {
                for(AlbumShortLink albumShortLink : albumShortLinks) {
                    jsBeans.add(convertAlbumShortLinkToJsBean(albumShortLink));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> getAllAlbumShortLinkKeys(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllAlbumShortLinkKeys(ordering, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<AlbumShortLinkJsBean> findAlbumShortLinks(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findAlbumShortLinks(filter, ordering, params, values, null, null, null, null);
    }

    public List<AlbumShortLinkJsBean> findAlbumShortLinks(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<AlbumShortLinkJsBean> jsBeans = new ArrayList<AlbumShortLinkJsBean>();
            List<AlbumShortLink> albumShortLinks = getService().findAlbumShortLinks(filter, ordering, params, values, grouping, unique, offset, count);
            if(albumShortLinks != null) {
                for(AlbumShortLink albumShortLink : albumShortLinks) {
                    jsBeans.add(convertAlbumShortLinkToJsBean(albumShortLink));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> findAlbumShortLinkKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findAlbumShortLinkKeys(filter, ordering, params, values, grouping, unique, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        log.finer("BEGIN");

        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createAlbumShortLink(String user, String linkAlbum, String shortLink, String shortUrl, String longUrl, String note, String status) throws WebException
    {
        try {
            return getService().createAlbumShortLink(user, linkAlbum, shortLink, shortUrl, longUrl, note, status);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createAlbumShortLink(String jsonStr) throws WebException
    {
        return createAlbumShortLink(AlbumShortLinkJsBean.fromJsonString(jsonStr));
    }

    public String createAlbumShortLink(AlbumShortLinkJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            AlbumShortLink albumShortLink = convertAlbumShortLinkJsBeanToBean(jsBean);
            return getService().createAlbumShortLink(albumShortLink);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public AlbumShortLinkJsBean constructAlbumShortLink(String jsonStr) throws WebException
    {
        return constructAlbumShortLink(AlbumShortLinkJsBean.fromJsonString(jsonStr));
    }

    public AlbumShortLinkJsBean constructAlbumShortLink(AlbumShortLinkJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            AlbumShortLink albumShortLink = convertAlbumShortLinkJsBeanToBean(jsBean);
            albumShortLink = getService().constructAlbumShortLink(albumShortLink);
            jsBean = convertAlbumShortLinkToJsBean(albumShortLink);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateAlbumShortLink(String guid, String user, String linkAlbum, String shortLink, String shortUrl, String longUrl, String note, String status) throws WebException
    {
        try {
            return getService().updateAlbumShortLink(guid, user, linkAlbum, shortLink, shortUrl, longUrl, note, status);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateAlbumShortLink(String jsonStr) throws WebException
    {
        return updateAlbumShortLink(AlbumShortLinkJsBean.fromJsonString(jsonStr));
    }

    public Boolean updateAlbumShortLink(AlbumShortLinkJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            AlbumShortLink albumShortLink = convertAlbumShortLinkJsBeanToBean(jsBean);
            return getService().updateAlbumShortLink(albumShortLink);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public AlbumShortLinkJsBean refreshAlbumShortLink(String jsonStr) throws WebException
    {
        return refreshAlbumShortLink(AlbumShortLinkJsBean.fromJsonString(jsonStr));
    }

    public AlbumShortLinkJsBean refreshAlbumShortLink(AlbumShortLinkJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            AlbumShortLink albumShortLink = convertAlbumShortLinkJsBeanToBean(jsBean);
            albumShortLink = getService().refreshAlbumShortLink(albumShortLink);
            jsBean = convertAlbumShortLinkToJsBean(albumShortLink);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteAlbumShortLink(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteAlbumShortLink(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteAlbumShortLink(AlbumShortLinkJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            AlbumShortLink albumShortLink = convertAlbumShortLinkJsBeanToBean(jsBean);
            return getService().deleteAlbumShortLink(albumShortLink);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteAlbumShortLinks(String filter, String params, List<String> values) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteAlbumShortLinks(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }


    public static AlbumShortLinkJsBean convertAlbumShortLinkToJsBean(AlbumShortLink albumShortLink)
    {
        AlbumShortLinkJsBean jsBean = null;
        if(albumShortLink != null) {
            jsBean = new AlbumShortLinkJsBean();
            jsBean.setGuid(albumShortLink.getGuid());
            jsBean.setUser(albumShortLink.getUser());
            jsBean.setLinkAlbum(albumShortLink.getLinkAlbum());
            jsBean.setShortLink(albumShortLink.getShortLink());
            jsBean.setShortUrl(albumShortLink.getShortUrl());
            jsBean.setLongUrl(albumShortLink.getLongUrl());
            jsBean.setNote(albumShortLink.getNote());
            jsBean.setStatus(albumShortLink.getStatus());
            jsBean.setCreatedTime(albumShortLink.getCreatedTime());
            jsBean.setModifiedTime(albumShortLink.getModifiedTime());
        }
        return jsBean;
    }

    public static AlbumShortLink convertAlbumShortLinkJsBeanToBean(AlbumShortLinkJsBean jsBean)
    {
        AlbumShortLinkBean albumShortLink = null;
        if(jsBean != null) {
            albumShortLink = new AlbumShortLinkBean();
            albumShortLink.setGuid(jsBean.getGuid());
            albumShortLink.setUser(jsBean.getUser());
            albumShortLink.setLinkAlbum(jsBean.getLinkAlbum());
            albumShortLink.setShortLink(jsBean.getShortLink());
            albumShortLink.setShortUrl(jsBean.getShortUrl());
            albumShortLink.setLongUrl(jsBean.getLongUrl());
            albumShortLink.setNote(jsBean.getNote());
            albumShortLink.setStatus(jsBean.getStatus());
            albumShortLink.setCreatedTime(jsBean.getCreatedTime());
            albumShortLink.setModifiedTime(jsBean.getModifiedTime());
        }
        return albumShortLink;
    }

}
