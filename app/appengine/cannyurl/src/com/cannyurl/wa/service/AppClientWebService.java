package com.cannyurl.wa.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.AppClient;
import com.cannyurl.af.bean.AppClientBean;
import com.cannyurl.af.service.AppClientService;
import com.cannyurl.af.service.manager.ServiceManager;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.AppClientJsBean;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class AppClientWebService // implements AppClientService
{
    private static final Logger log = Logger.getLogger(AppClientWebService.class.getName());
     
    // Af service interface.
    private AppClientService mService = null;

    public AppClientWebService()
    {
        this(ServiceManager.getAppClientService());
    }
    public AppClientWebService(AppClientService service)
    {
        mService = service;
    }
    
    private AppClientService getService()
    {
        if(mService == null) {
            mService = ServiceManager.getAppClientService();
        }
        return mService;
    }
    
    
    public AppClientJsBean getAppClient(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            AppClient appClient = getService().getAppClient(guid);
            AppClientJsBean bean = convertAppClientToJsBean(appClient);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getAppClient(String guid, String field) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().getAppClient(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<AppClientJsBean> getAppClients(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<AppClientJsBean> jsBeans = new ArrayList<AppClientJsBean>();
            List<AppClient> appClients = getService().getAppClients(guids);
            if(appClients != null) {
                for(AppClient appClient : appClients) {
                    jsBeans.add(convertAppClientToJsBean(appClient));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<AppClientJsBean> getAllAppClients() throws WebException
    {
        return getAllAppClients(null, null, null);
    }

    public List<AppClientJsBean> getAllAppClients(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<AppClientJsBean> jsBeans = new ArrayList<AppClientJsBean>();
            List<AppClient> appClients = getService().getAllAppClients(ordering, offset, count);
            if(appClients != null) {
                for(AppClient appClient : appClients) {
                    jsBeans.add(convertAppClientToJsBean(appClient));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> getAllAppClientKeys(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllAppClientKeys(ordering, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<AppClientJsBean> findAppClients(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findAppClients(filter, ordering, params, values, null, null, null, null);
    }

    public List<AppClientJsBean> findAppClients(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<AppClientJsBean> jsBeans = new ArrayList<AppClientJsBean>();
            List<AppClient> appClients = getService().findAppClients(filter, ordering, params, values, grouping, unique, offset, count);
            if(appClients != null) {
                for(AppClient appClient : appClients) {
                    jsBeans.add(convertAppClientToJsBean(appClient));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> findAppClientKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findAppClientKeys(filter, ordering, params, values, grouping, unique, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        log.finer("BEGIN");

        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createAppClient(String owner, String admin, String name, String clientId, String clientCode, String rootDomain, String appDomain, Boolean useAppDomain, Boolean enabled, String licenseMode, String status) throws WebException
    {
        try {
            return getService().createAppClient(owner, admin, name, clientId, clientCode, rootDomain, appDomain, useAppDomain, enabled, licenseMode, status);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createAppClient(String jsonStr) throws WebException
    {
        return createAppClient(AppClientJsBean.fromJsonString(jsonStr));
    }

    public String createAppClient(AppClientJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            AppClient appClient = convertAppClientJsBeanToBean(jsBean);
            return getService().createAppClient(appClient);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public AppClientJsBean constructAppClient(String jsonStr) throws WebException
    {
        return constructAppClient(AppClientJsBean.fromJsonString(jsonStr));
    }

    public AppClientJsBean constructAppClient(AppClientJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            AppClient appClient = convertAppClientJsBeanToBean(jsBean);
            appClient = getService().constructAppClient(appClient);
            jsBean = convertAppClientToJsBean(appClient);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateAppClient(String guid, String owner, String admin, String name, String clientId, String clientCode, String rootDomain, String appDomain, Boolean useAppDomain, Boolean enabled, String licenseMode, String status) throws WebException
    {
        try {
            return getService().updateAppClient(guid, owner, admin, name, clientId, clientCode, rootDomain, appDomain, useAppDomain, enabled, licenseMode, status);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateAppClient(String jsonStr) throws WebException
    {
        return updateAppClient(AppClientJsBean.fromJsonString(jsonStr));
    }

    public Boolean updateAppClient(AppClientJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            AppClient appClient = convertAppClientJsBeanToBean(jsBean);
            return getService().updateAppClient(appClient);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public AppClientJsBean refreshAppClient(String jsonStr) throws WebException
    {
        return refreshAppClient(AppClientJsBean.fromJsonString(jsonStr));
    }

    public AppClientJsBean refreshAppClient(AppClientJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            AppClient appClient = convertAppClientJsBeanToBean(jsBean);
            appClient = getService().refreshAppClient(appClient);
            jsBean = convertAppClientToJsBean(appClient);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteAppClient(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteAppClient(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteAppClient(AppClientJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            AppClient appClient = convertAppClientJsBeanToBean(jsBean);
            return getService().deleteAppClient(appClient);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteAppClients(String filter, String params, List<String> values) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteAppClients(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }


    public static AppClientJsBean convertAppClientToJsBean(AppClient appClient)
    {
        AppClientJsBean jsBean = null;
        if(appClient != null) {
            jsBean = new AppClientJsBean();
            jsBean.setGuid(appClient.getGuid());
            jsBean.setOwner(appClient.getOwner());
            jsBean.setAdmin(appClient.getAdmin());
            jsBean.setName(appClient.getName());
            jsBean.setClientId(appClient.getClientId());
            jsBean.setClientCode(appClient.getClientCode());
            jsBean.setRootDomain(appClient.getRootDomain());
            jsBean.setAppDomain(appClient.getAppDomain());
            jsBean.setUseAppDomain(appClient.isUseAppDomain());
            jsBean.setEnabled(appClient.isEnabled());
            jsBean.setLicenseMode(appClient.getLicenseMode());
            jsBean.setStatus(appClient.getStatus());
            jsBean.setCreatedTime(appClient.getCreatedTime());
            jsBean.setModifiedTime(appClient.getModifiedTime());
        }
        return jsBean;
    }

    public static AppClient convertAppClientJsBeanToBean(AppClientJsBean jsBean)
    {
        AppClientBean appClient = null;
        if(jsBean != null) {
            appClient = new AppClientBean();
            appClient.setGuid(jsBean.getGuid());
            appClient.setOwner(jsBean.getOwner());
            appClient.setAdmin(jsBean.getAdmin());
            appClient.setName(jsBean.getName());
            appClient.setClientId(jsBean.getClientId());
            appClient.setClientCode(jsBean.getClientCode());
            appClient.setRootDomain(jsBean.getRootDomain());
            appClient.setAppDomain(jsBean.getAppDomain());
            appClient.setUseAppDomain(jsBean.isUseAppDomain());
            appClient.setEnabled(jsBean.isEnabled());
            appClient.setLicenseMode(jsBean.getLicenseMode());
            appClient.setStatus(jsBean.getStatus());
            appClient.setCreatedTime(jsBean.getCreatedTime());
            appClient.setModifiedTime(jsBean.getModifiedTime());
        }
        return appClient;
    }

}
