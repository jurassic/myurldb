package com.cannyurl.wa.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.BookmarkFolder;
import com.cannyurl.af.bean.BookmarkFolderBean;
import com.cannyurl.af.service.BookmarkFolderService;
import com.cannyurl.af.service.manager.ServiceManager;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.BookmarkFolderJsBean;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class BookmarkFolderWebService // implements BookmarkFolderService
{
    private static final Logger log = Logger.getLogger(BookmarkFolderWebService.class.getName());
     
    // Af service interface.
    private BookmarkFolderService mService = null;

    public BookmarkFolderWebService()
    {
        this(ServiceManager.getBookmarkFolderService());
    }
    public BookmarkFolderWebService(BookmarkFolderService service)
    {
        mService = service;
    }
    
    private BookmarkFolderService getService()
    {
        if(mService == null) {
            mService = ServiceManager.getBookmarkFolderService();
        }
        return mService;
    }
    
    
    public BookmarkFolderJsBean getBookmarkFolder(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            BookmarkFolder bookmarkFolder = getService().getBookmarkFolder(guid);
            BookmarkFolderJsBean bean = convertBookmarkFolderToJsBean(bookmarkFolder);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getBookmarkFolder(String guid, String field) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().getBookmarkFolder(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<BookmarkFolderJsBean> getBookmarkFolders(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<BookmarkFolderJsBean> jsBeans = new ArrayList<BookmarkFolderJsBean>();
            List<BookmarkFolder> bookmarkFolders = getService().getBookmarkFolders(guids);
            if(bookmarkFolders != null) {
                for(BookmarkFolder bookmarkFolder : bookmarkFolders) {
                    jsBeans.add(convertBookmarkFolderToJsBean(bookmarkFolder));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<BookmarkFolderJsBean> getAllBookmarkFolders() throws WebException
    {
        return getAllBookmarkFolders(null, null, null);
    }

    public List<BookmarkFolderJsBean> getAllBookmarkFolders(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<BookmarkFolderJsBean> jsBeans = new ArrayList<BookmarkFolderJsBean>();
            List<BookmarkFolder> bookmarkFolders = getService().getAllBookmarkFolders(ordering, offset, count);
            if(bookmarkFolders != null) {
                for(BookmarkFolder bookmarkFolder : bookmarkFolders) {
                    jsBeans.add(convertBookmarkFolderToJsBean(bookmarkFolder));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> getAllBookmarkFolderKeys(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllBookmarkFolderKeys(ordering, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<BookmarkFolderJsBean> findBookmarkFolders(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findBookmarkFolders(filter, ordering, params, values, null, null, null, null);
    }

    public List<BookmarkFolderJsBean> findBookmarkFolders(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<BookmarkFolderJsBean> jsBeans = new ArrayList<BookmarkFolderJsBean>();
            List<BookmarkFolder> bookmarkFolders = getService().findBookmarkFolders(filter, ordering, params, values, grouping, unique, offset, count);
            if(bookmarkFolders != null) {
                for(BookmarkFolder bookmarkFolder : bookmarkFolders) {
                    jsBeans.add(convertBookmarkFolderToJsBean(bookmarkFolder));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> findBookmarkFolderKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findBookmarkFolderKeys(filter, ordering, params, values, grouping, unique, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        log.finer("BEGIN");

        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createBookmarkFolder(String appClient, String clientRootDomain, String user, String title, String description, String type, String category, String parent, String aggregate, String acl, Boolean exportable, String status, String note, String keywordFolder) throws WebException
    {
        try {
            return getService().createBookmarkFolder(appClient, clientRootDomain, user, title, description, type, category, parent, aggregate, acl, exportable, status, note, keywordFolder);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createBookmarkFolder(String jsonStr) throws WebException
    {
        return createBookmarkFolder(BookmarkFolderJsBean.fromJsonString(jsonStr));
    }

    public String createBookmarkFolder(BookmarkFolderJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            BookmarkFolder bookmarkFolder = convertBookmarkFolderJsBeanToBean(jsBean);
            return getService().createBookmarkFolder(bookmarkFolder);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public BookmarkFolderJsBean constructBookmarkFolder(String jsonStr) throws WebException
    {
        return constructBookmarkFolder(BookmarkFolderJsBean.fromJsonString(jsonStr));
    }

    public BookmarkFolderJsBean constructBookmarkFolder(BookmarkFolderJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            BookmarkFolder bookmarkFolder = convertBookmarkFolderJsBeanToBean(jsBean);
            bookmarkFolder = getService().constructBookmarkFolder(bookmarkFolder);
            jsBean = convertBookmarkFolderToJsBean(bookmarkFolder);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateBookmarkFolder(String guid, String appClient, String clientRootDomain, String user, String title, String description, String type, String category, String parent, String aggregate, String acl, Boolean exportable, String status, String note, String keywordFolder) throws WebException
    {
        try {
            return getService().updateBookmarkFolder(guid, appClient, clientRootDomain, user, title, description, type, category, parent, aggregate, acl, exportable, status, note, keywordFolder);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateBookmarkFolder(String jsonStr) throws WebException
    {
        return updateBookmarkFolder(BookmarkFolderJsBean.fromJsonString(jsonStr));
    }

    public Boolean updateBookmarkFolder(BookmarkFolderJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            BookmarkFolder bookmarkFolder = convertBookmarkFolderJsBeanToBean(jsBean);
            return getService().updateBookmarkFolder(bookmarkFolder);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public BookmarkFolderJsBean refreshBookmarkFolder(String jsonStr) throws WebException
    {
        return refreshBookmarkFolder(BookmarkFolderJsBean.fromJsonString(jsonStr));
    }

    public BookmarkFolderJsBean refreshBookmarkFolder(BookmarkFolderJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            BookmarkFolder bookmarkFolder = convertBookmarkFolderJsBeanToBean(jsBean);
            bookmarkFolder = getService().refreshBookmarkFolder(bookmarkFolder);
            jsBean = convertBookmarkFolderToJsBean(bookmarkFolder);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteBookmarkFolder(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteBookmarkFolder(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteBookmarkFolder(BookmarkFolderJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            BookmarkFolder bookmarkFolder = convertBookmarkFolderJsBeanToBean(jsBean);
            return getService().deleteBookmarkFolder(bookmarkFolder);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteBookmarkFolders(String filter, String params, List<String> values) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteBookmarkFolders(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }


    public static BookmarkFolderJsBean convertBookmarkFolderToJsBean(BookmarkFolder bookmarkFolder)
    {
        BookmarkFolderJsBean jsBean = null;
        if(bookmarkFolder != null) {
            jsBean = new BookmarkFolderJsBean();
            jsBean.setGuid(bookmarkFolder.getGuid());
            jsBean.setAppClient(bookmarkFolder.getAppClient());
            jsBean.setClientRootDomain(bookmarkFolder.getClientRootDomain());
            jsBean.setUser(bookmarkFolder.getUser());
            jsBean.setTitle(bookmarkFolder.getTitle());
            jsBean.setDescription(bookmarkFolder.getDescription());
            jsBean.setType(bookmarkFolder.getType());
            jsBean.setCategory(bookmarkFolder.getCategory());
            jsBean.setParent(bookmarkFolder.getParent());
            jsBean.setAggregate(bookmarkFolder.getAggregate());
            jsBean.setAcl(bookmarkFolder.getAcl());
            jsBean.setExportable(bookmarkFolder.isExportable());
            jsBean.setStatus(bookmarkFolder.getStatus());
            jsBean.setNote(bookmarkFolder.getNote());
            jsBean.setKeywordFolder(bookmarkFolder.getKeywordFolder());
            jsBean.setCreatedTime(bookmarkFolder.getCreatedTime());
            jsBean.setModifiedTime(bookmarkFolder.getModifiedTime());
        }
        return jsBean;
    }

    public static BookmarkFolder convertBookmarkFolderJsBeanToBean(BookmarkFolderJsBean jsBean)
    {
        BookmarkFolderBean bookmarkFolder = null;
        if(jsBean != null) {
            bookmarkFolder = new BookmarkFolderBean();
            bookmarkFolder.setGuid(jsBean.getGuid());
            bookmarkFolder.setAppClient(jsBean.getAppClient());
            bookmarkFolder.setClientRootDomain(jsBean.getClientRootDomain());
            bookmarkFolder.setUser(jsBean.getUser());
            bookmarkFolder.setTitle(jsBean.getTitle());
            bookmarkFolder.setDescription(jsBean.getDescription());
            bookmarkFolder.setType(jsBean.getType());
            bookmarkFolder.setCategory(jsBean.getCategory());
            bookmarkFolder.setParent(jsBean.getParent());
            bookmarkFolder.setAggregate(jsBean.getAggregate());
            bookmarkFolder.setAcl(jsBean.getAcl());
            bookmarkFolder.setExportable(jsBean.isExportable());
            bookmarkFolder.setStatus(jsBean.getStatus());
            bookmarkFolder.setNote(jsBean.getNote());
            bookmarkFolder.setKeywordFolder(jsBean.getKeywordFolder());
            bookmarkFolder.setCreatedTime(jsBean.getCreatedTime());
            bookmarkFolder.setModifiedTime(jsBean.getModifiedTime());
        }
        return bookmarkFolder;
    }

}
