package com.cannyurl.wa.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.BookmarkLink;
import com.cannyurl.af.bean.BookmarkLinkBean;
import com.cannyurl.af.service.BookmarkLinkService;
import com.cannyurl.af.service.manager.ServiceManager;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.BookmarkLinkJsBean;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class BookmarkLinkWebService // implements BookmarkLinkService
{
    private static final Logger log = Logger.getLogger(BookmarkLinkWebService.class.getName());
     
    // Af service interface.
    private BookmarkLinkService mService = null;

    public BookmarkLinkWebService()
    {
        this(ServiceManager.getBookmarkLinkService());
    }
    public BookmarkLinkWebService(BookmarkLinkService service)
    {
        mService = service;
    }
    
    private BookmarkLinkService getService()
    {
        if(mService == null) {
            mService = ServiceManager.getBookmarkLinkService();
        }
        return mService;
    }
    
    
    public BookmarkLinkJsBean getBookmarkLink(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            BookmarkLink bookmarkLink = getService().getBookmarkLink(guid);
            BookmarkLinkJsBean bean = convertBookmarkLinkToJsBean(bookmarkLink);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getBookmarkLink(String guid, String field) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().getBookmarkLink(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<BookmarkLinkJsBean> getBookmarkLinks(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<BookmarkLinkJsBean> jsBeans = new ArrayList<BookmarkLinkJsBean>();
            List<BookmarkLink> bookmarkLinks = getService().getBookmarkLinks(guids);
            if(bookmarkLinks != null) {
                for(BookmarkLink bookmarkLink : bookmarkLinks) {
                    jsBeans.add(convertBookmarkLinkToJsBean(bookmarkLink));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<BookmarkLinkJsBean> getAllBookmarkLinks() throws WebException
    {
        return getAllBookmarkLinks(null, null, null);
    }

    public List<BookmarkLinkJsBean> getAllBookmarkLinks(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<BookmarkLinkJsBean> jsBeans = new ArrayList<BookmarkLinkJsBean>();
            List<BookmarkLink> bookmarkLinks = getService().getAllBookmarkLinks(ordering, offset, count);
            if(bookmarkLinks != null) {
                for(BookmarkLink bookmarkLink : bookmarkLinks) {
                    jsBeans.add(convertBookmarkLinkToJsBean(bookmarkLink));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> getAllBookmarkLinkKeys(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllBookmarkLinkKeys(ordering, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<BookmarkLinkJsBean> findBookmarkLinks(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findBookmarkLinks(filter, ordering, params, values, null, null, null, null);
    }

    public List<BookmarkLinkJsBean> findBookmarkLinks(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<BookmarkLinkJsBean> jsBeans = new ArrayList<BookmarkLinkJsBean>();
            List<BookmarkLink> bookmarkLinks = getService().findBookmarkLinks(filter, ordering, params, values, grouping, unique, offset, count);
            if(bookmarkLinks != null) {
                for(BookmarkLink bookmarkLink : bookmarkLinks) {
                    jsBeans.add(convertBookmarkLinkToJsBean(bookmarkLink));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> findBookmarkLinkKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findBookmarkLinkKeys(filter, ordering, params, values, grouping, unique, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        log.finer("BEGIN");

        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createBookmarkLink(String appClient, String clientRootDomain, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, Boolean internal, Boolean caching, String memo, String status, String note, Long expirationTime, String bookmarkFolder, String contentTag, String referenceElement, String elementType, String keywordLink) throws WebException
    {
        try {
            return getService().createBookmarkLink(appClient, clientRootDomain, user, shortLink, domain, token, longUrl, shortUrl, internal, caching, memo, status, note, expirationTime, bookmarkFolder, contentTag, referenceElement, elementType, keywordLink);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createBookmarkLink(String jsonStr) throws WebException
    {
        return createBookmarkLink(BookmarkLinkJsBean.fromJsonString(jsonStr));
    }

    public String createBookmarkLink(BookmarkLinkJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            BookmarkLink bookmarkLink = convertBookmarkLinkJsBeanToBean(jsBean);
            return getService().createBookmarkLink(bookmarkLink);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public BookmarkLinkJsBean constructBookmarkLink(String jsonStr) throws WebException
    {
        return constructBookmarkLink(BookmarkLinkJsBean.fromJsonString(jsonStr));
    }

    public BookmarkLinkJsBean constructBookmarkLink(BookmarkLinkJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            BookmarkLink bookmarkLink = convertBookmarkLinkJsBeanToBean(jsBean);
            bookmarkLink = getService().constructBookmarkLink(bookmarkLink);
            jsBean = convertBookmarkLinkToJsBean(bookmarkLink);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateBookmarkLink(String guid, String appClient, String clientRootDomain, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, Boolean internal, Boolean caching, String memo, String status, String note, Long expirationTime, String bookmarkFolder, String contentTag, String referenceElement, String elementType, String keywordLink) throws WebException
    {
        try {
            return getService().updateBookmarkLink(guid, appClient, clientRootDomain, user, shortLink, domain, token, longUrl, shortUrl, internal, caching, memo, status, note, expirationTime, bookmarkFolder, contentTag, referenceElement, elementType, keywordLink);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateBookmarkLink(String jsonStr) throws WebException
    {
        return updateBookmarkLink(BookmarkLinkJsBean.fromJsonString(jsonStr));
    }

    public Boolean updateBookmarkLink(BookmarkLinkJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            BookmarkLink bookmarkLink = convertBookmarkLinkJsBeanToBean(jsBean);
            return getService().updateBookmarkLink(bookmarkLink);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public BookmarkLinkJsBean refreshBookmarkLink(String jsonStr) throws WebException
    {
        return refreshBookmarkLink(BookmarkLinkJsBean.fromJsonString(jsonStr));
    }

    public BookmarkLinkJsBean refreshBookmarkLink(BookmarkLinkJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            BookmarkLink bookmarkLink = convertBookmarkLinkJsBeanToBean(jsBean);
            bookmarkLink = getService().refreshBookmarkLink(bookmarkLink);
            jsBean = convertBookmarkLinkToJsBean(bookmarkLink);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteBookmarkLink(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteBookmarkLink(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteBookmarkLink(BookmarkLinkJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            BookmarkLink bookmarkLink = convertBookmarkLinkJsBeanToBean(jsBean);
            return getService().deleteBookmarkLink(bookmarkLink);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteBookmarkLinks(String filter, String params, List<String> values) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteBookmarkLinks(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }


    public static BookmarkLinkJsBean convertBookmarkLinkToJsBean(BookmarkLink bookmarkLink)
    {
        BookmarkLinkJsBean jsBean = null;
        if(bookmarkLink != null) {
            jsBean = new BookmarkLinkJsBean();
            jsBean.setGuid(bookmarkLink.getGuid());
            jsBean.setAppClient(bookmarkLink.getAppClient());
            jsBean.setClientRootDomain(bookmarkLink.getClientRootDomain());
            jsBean.setUser(bookmarkLink.getUser());
            jsBean.setShortLink(bookmarkLink.getShortLink());
            jsBean.setDomain(bookmarkLink.getDomain());
            jsBean.setToken(bookmarkLink.getToken());
            jsBean.setLongUrl(bookmarkLink.getLongUrl());
            jsBean.setShortUrl(bookmarkLink.getShortUrl());
            jsBean.setInternal(bookmarkLink.isInternal());
            jsBean.setCaching(bookmarkLink.isCaching());
            jsBean.setMemo(bookmarkLink.getMemo());
            jsBean.setStatus(bookmarkLink.getStatus());
            jsBean.setNote(bookmarkLink.getNote());
            jsBean.setExpirationTime(bookmarkLink.getExpirationTime());
            jsBean.setBookmarkFolder(bookmarkLink.getBookmarkFolder());
            jsBean.setContentTag(bookmarkLink.getContentTag());
            jsBean.setReferenceElement(bookmarkLink.getReferenceElement());
            jsBean.setElementType(bookmarkLink.getElementType());
            jsBean.setKeywordLink(bookmarkLink.getKeywordLink());
            jsBean.setCreatedTime(bookmarkLink.getCreatedTime());
            jsBean.setModifiedTime(bookmarkLink.getModifiedTime());
        }
        return jsBean;
    }

    public static BookmarkLink convertBookmarkLinkJsBeanToBean(BookmarkLinkJsBean jsBean)
    {
        BookmarkLinkBean bookmarkLink = null;
        if(jsBean != null) {
            bookmarkLink = new BookmarkLinkBean();
            bookmarkLink.setGuid(jsBean.getGuid());
            bookmarkLink.setAppClient(jsBean.getAppClient());
            bookmarkLink.setClientRootDomain(jsBean.getClientRootDomain());
            bookmarkLink.setUser(jsBean.getUser());
            bookmarkLink.setShortLink(jsBean.getShortLink());
            bookmarkLink.setDomain(jsBean.getDomain());
            bookmarkLink.setToken(jsBean.getToken());
            bookmarkLink.setLongUrl(jsBean.getLongUrl());
            bookmarkLink.setShortUrl(jsBean.getShortUrl());
            bookmarkLink.setInternal(jsBean.isInternal());
            bookmarkLink.setCaching(jsBean.isCaching());
            bookmarkLink.setMemo(jsBean.getMemo());
            bookmarkLink.setStatus(jsBean.getStatus());
            bookmarkLink.setNote(jsBean.getNote());
            bookmarkLink.setExpirationTime(jsBean.getExpirationTime());
            bookmarkLink.setBookmarkFolder(jsBean.getBookmarkFolder());
            bookmarkLink.setContentTag(jsBean.getContentTag());
            bookmarkLink.setReferenceElement(jsBean.getReferenceElement());
            bookmarkLink.setElementType(jsBean.getElementType());
            bookmarkLink.setKeywordLink(jsBean.getKeywordLink());
            bookmarkLink.setCreatedTime(jsBean.getCreatedTime());
            bookmarkLink.setModifiedTime(jsBean.getModifiedTime());
        }
        return bookmarkLink;
    }

}
