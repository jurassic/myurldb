package com.cannyurl.wa.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.ClientUser;
import com.cannyurl.af.bean.ClientUserBean;
import com.cannyurl.af.service.ClientUserService;
import com.cannyurl.af.service.manager.ServiceManager;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.ClientUserJsBean;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class ClientUserWebService // implements ClientUserService
{
    private static final Logger log = Logger.getLogger(ClientUserWebService.class.getName());
     
    // Af service interface.
    private ClientUserService mService = null;

    public ClientUserWebService()
    {
        this(ServiceManager.getClientUserService());
    }
    public ClientUserWebService(ClientUserService service)
    {
        mService = service;
    }
    
    private ClientUserService getService()
    {
        if(mService == null) {
            mService = ServiceManager.getClientUserService();
        }
        return mService;
    }
    
    
    public ClientUserJsBean getClientUser(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            ClientUser clientUser = getService().getClientUser(guid);
            ClientUserJsBean bean = convertClientUserToJsBean(clientUser);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getClientUser(String guid, String field) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().getClientUser(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<ClientUserJsBean> getClientUsers(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<ClientUserJsBean> jsBeans = new ArrayList<ClientUserJsBean>();
            List<ClientUser> clientUsers = getService().getClientUsers(guids);
            if(clientUsers != null) {
                for(ClientUser clientUser : clientUsers) {
                    jsBeans.add(convertClientUserToJsBean(clientUser));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<ClientUserJsBean> getAllClientUsers() throws WebException
    {
        return getAllClientUsers(null, null, null);
    }

    public List<ClientUserJsBean> getAllClientUsers(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<ClientUserJsBean> jsBeans = new ArrayList<ClientUserJsBean>();
            List<ClientUser> clientUsers = getService().getAllClientUsers(ordering, offset, count);
            if(clientUsers != null) {
                for(ClientUser clientUser : clientUsers) {
                    jsBeans.add(convertClientUserToJsBean(clientUser));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> getAllClientUserKeys(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllClientUserKeys(ordering, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<ClientUserJsBean> findClientUsers(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findClientUsers(filter, ordering, params, values, null, null, null, null);
    }

    public List<ClientUserJsBean> findClientUsers(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<ClientUserJsBean> jsBeans = new ArrayList<ClientUserJsBean>();
            List<ClientUser> clientUsers = getService().findClientUsers(filter, ordering, params, values, grouping, unique, offset, count);
            if(clientUsers != null) {
                for(ClientUser clientUser : clientUsers) {
                    jsBeans.add(convertClientUserToJsBean(clientUser));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> findClientUserKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findClientUserKeys(filter, ordering, params, values, grouping, unique, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        log.finer("BEGIN");

        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createClientUser(String appClient, String user, String role, Boolean provisioned, Boolean licensed, String status) throws WebException
    {
        try {
            return getService().createClientUser(appClient, user, role, provisioned, licensed, status);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createClientUser(String jsonStr) throws WebException
    {
        return createClientUser(ClientUserJsBean.fromJsonString(jsonStr));
    }

    public String createClientUser(ClientUserJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            ClientUser clientUser = convertClientUserJsBeanToBean(jsBean);
            return getService().createClientUser(clientUser);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public ClientUserJsBean constructClientUser(String jsonStr) throws WebException
    {
        return constructClientUser(ClientUserJsBean.fromJsonString(jsonStr));
    }

    public ClientUserJsBean constructClientUser(ClientUserJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            ClientUser clientUser = convertClientUserJsBeanToBean(jsBean);
            clientUser = getService().constructClientUser(clientUser);
            jsBean = convertClientUserToJsBean(clientUser);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateClientUser(String guid, String appClient, String user, String role, Boolean provisioned, Boolean licensed, String status) throws WebException
    {
        try {
            return getService().updateClientUser(guid, appClient, user, role, provisioned, licensed, status);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateClientUser(String jsonStr) throws WebException
    {
        return updateClientUser(ClientUserJsBean.fromJsonString(jsonStr));
    }

    public Boolean updateClientUser(ClientUserJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            ClientUser clientUser = convertClientUserJsBeanToBean(jsBean);
            return getService().updateClientUser(clientUser);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public ClientUserJsBean refreshClientUser(String jsonStr) throws WebException
    {
        return refreshClientUser(ClientUserJsBean.fromJsonString(jsonStr));
    }

    public ClientUserJsBean refreshClientUser(ClientUserJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            ClientUser clientUser = convertClientUserJsBeanToBean(jsBean);
            clientUser = getService().refreshClientUser(clientUser);
            jsBean = convertClientUserToJsBean(clientUser);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteClientUser(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteClientUser(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteClientUser(ClientUserJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            ClientUser clientUser = convertClientUserJsBeanToBean(jsBean);
            return getService().deleteClientUser(clientUser);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteClientUsers(String filter, String params, List<String> values) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteClientUsers(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }


    public static ClientUserJsBean convertClientUserToJsBean(ClientUser clientUser)
    {
        ClientUserJsBean jsBean = null;
        if(clientUser != null) {
            jsBean = new ClientUserJsBean();
            jsBean.setGuid(clientUser.getGuid());
            jsBean.setAppClient(clientUser.getAppClient());
            jsBean.setUser(clientUser.getUser());
            jsBean.setRole(clientUser.getRole());
            jsBean.setProvisioned(clientUser.isProvisioned());
            jsBean.setLicensed(clientUser.isLicensed());
            jsBean.setStatus(clientUser.getStatus());
            jsBean.setCreatedTime(clientUser.getCreatedTime());
            jsBean.setModifiedTime(clientUser.getModifiedTime());
        }
        return jsBean;
    }

    public static ClientUser convertClientUserJsBeanToBean(ClientUserJsBean jsBean)
    {
        ClientUserBean clientUser = null;
        if(jsBean != null) {
            clientUser = new ClientUserBean();
            clientUser.setGuid(jsBean.getGuid());
            clientUser.setAppClient(jsBean.getAppClient());
            clientUser.setUser(jsBean.getUser());
            clientUser.setRole(jsBean.getRole());
            clientUser.setProvisioned(jsBean.isProvisioned());
            clientUser.setLicensed(jsBean.isLicensed());
            clientUser.setStatus(jsBean.getStatus());
            clientUser.setCreatedTime(jsBean.getCreatedTime());
            clientUser.setModifiedTime(jsBean.getModifiedTime());
        }
        return clientUser;
    }

}
