package com.cannyurl.wa.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.ExternalUserIdStruct;
import com.myurldb.ws.ExternalUserAuth;
import com.cannyurl.af.bean.ExternalUserAuthBean;
import com.cannyurl.af.service.ExternalUserAuthService;
import com.cannyurl.af.service.manager.ServiceManager;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.ExternalUserIdStructJsBean;
import com.cannyurl.fe.bean.ExternalUserAuthJsBean;
import com.cannyurl.wa.util.ExternalUserIdStructWebUtil;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class ExternalUserAuthWebService // implements ExternalUserAuthService
{
    private static final Logger log = Logger.getLogger(ExternalUserAuthWebService.class.getName());
     
    // Af service interface.
    private ExternalUserAuthService mService = null;

    public ExternalUserAuthWebService()
    {
        this(ServiceManager.getExternalUserAuthService());
    }
    public ExternalUserAuthWebService(ExternalUserAuthService service)
    {
        mService = service;
    }
    
    private ExternalUserAuthService getService()
    {
        if(mService == null) {
            mService = ServiceManager.getExternalUserAuthService();
        }
        return mService;
    }
    
    
    public ExternalUserAuthJsBean getExternalUserAuth(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            ExternalUserAuth externalUserAuth = getService().getExternalUserAuth(guid);
            ExternalUserAuthJsBean bean = convertExternalUserAuthToJsBean(externalUserAuth);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getExternalUserAuth(String guid, String field) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().getExternalUserAuth(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<ExternalUserAuthJsBean> getExternalUserAuths(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<ExternalUserAuthJsBean> jsBeans = new ArrayList<ExternalUserAuthJsBean>();
            List<ExternalUserAuth> externalUserAuths = getService().getExternalUserAuths(guids);
            if(externalUserAuths != null) {
                for(ExternalUserAuth externalUserAuth : externalUserAuths) {
                    jsBeans.add(convertExternalUserAuthToJsBean(externalUserAuth));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<ExternalUserAuthJsBean> getAllExternalUserAuths() throws WebException
    {
        return getAllExternalUserAuths(null, null, null);
    }

    public List<ExternalUserAuthJsBean> getAllExternalUserAuths(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<ExternalUserAuthJsBean> jsBeans = new ArrayList<ExternalUserAuthJsBean>();
            List<ExternalUserAuth> externalUserAuths = getService().getAllExternalUserAuths(ordering, offset, count);
            if(externalUserAuths != null) {
                for(ExternalUserAuth externalUserAuth : externalUserAuths) {
                    jsBeans.add(convertExternalUserAuthToJsBean(externalUserAuth));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> getAllExternalUserAuthKeys(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllExternalUserAuthKeys(ordering, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<ExternalUserAuthJsBean> findExternalUserAuths(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findExternalUserAuths(filter, ordering, params, values, null, null, null, null);
    }

    public List<ExternalUserAuthJsBean> findExternalUserAuths(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<ExternalUserAuthJsBean> jsBeans = new ArrayList<ExternalUserAuthJsBean>();
            List<ExternalUserAuth> externalUserAuths = getService().findExternalUserAuths(filter, ordering, params, values, grouping, unique, offset, count);
            if(externalUserAuths != null) {
                for(ExternalUserAuth externalUserAuth : externalUserAuths) {
                    jsBeans.add(convertExternalUserAuthToJsBean(externalUserAuth));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> findExternalUserAuthKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findExternalUserAuthKeys(filter, ordering, params, values, grouping, unique, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        log.finer("BEGIN");

        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createExternalUserAuth(String user, String authType, String providerId, String providerDomain, ExternalUserIdStructJsBean externalUserId, String requestToken, String accessToken, String accessTokenSecret, String email, String firstName, String lastName, String fullName, String displayName, String description, String gender, String dateOfBirth, String profileImageUrl, String timeZone, String postalCode, String location, String country, String language, String status, Long authTime, Long expirationTime) throws WebException
    {
        try {
            return getService().createExternalUserAuth(user, authType, providerId, providerDomain, ExternalUserIdStructWebUtil.convertExternalUserIdStructJsBeanToBean(externalUserId), requestToken, accessToken, accessTokenSecret, email, firstName, lastName, fullName, displayName, description, gender, dateOfBirth, profileImageUrl, timeZone, postalCode, location, country, language, status, authTime, expirationTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createExternalUserAuth(String jsonStr) throws WebException
    {
        return createExternalUserAuth(ExternalUserAuthJsBean.fromJsonString(jsonStr));
    }

    public String createExternalUserAuth(ExternalUserAuthJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            ExternalUserAuth externalUserAuth = convertExternalUserAuthJsBeanToBean(jsBean);
            return getService().createExternalUserAuth(externalUserAuth);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public ExternalUserAuthJsBean constructExternalUserAuth(String jsonStr) throws WebException
    {
        return constructExternalUserAuth(ExternalUserAuthJsBean.fromJsonString(jsonStr));
    }

    public ExternalUserAuthJsBean constructExternalUserAuth(ExternalUserAuthJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            ExternalUserAuth externalUserAuth = convertExternalUserAuthJsBeanToBean(jsBean);
            externalUserAuth = getService().constructExternalUserAuth(externalUserAuth);
            jsBean = convertExternalUserAuthToJsBean(externalUserAuth);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateExternalUserAuth(String guid, String user, String authType, String providerId, String providerDomain, ExternalUserIdStructJsBean externalUserId, String requestToken, String accessToken, String accessTokenSecret, String email, String firstName, String lastName, String fullName, String displayName, String description, String gender, String dateOfBirth, String profileImageUrl, String timeZone, String postalCode, String location, String country, String language, String status, Long authTime, Long expirationTime) throws WebException
    {
        try {
            return getService().updateExternalUserAuth(guid, user, authType, providerId, providerDomain, ExternalUserIdStructWebUtil.convertExternalUserIdStructJsBeanToBean(externalUserId), requestToken, accessToken, accessTokenSecret, email, firstName, lastName, fullName, displayName, description, gender, dateOfBirth, profileImageUrl, timeZone, postalCode, location, country, language, status, authTime, expirationTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateExternalUserAuth(String jsonStr) throws WebException
    {
        return updateExternalUserAuth(ExternalUserAuthJsBean.fromJsonString(jsonStr));
    }

    public Boolean updateExternalUserAuth(ExternalUserAuthJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            ExternalUserAuth externalUserAuth = convertExternalUserAuthJsBeanToBean(jsBean);
            return getService().updateExternalUserAuth(externalUserAuth);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public ExternalUserAuthJsBean refreshExternalUserAuth(String jsonStr) throws WebException
    {
        return refreshExternalUserAuth(ExternalUserAuthJsBean.fromJsonString(jsonStr));
    }

    public ExternalUserAuthJsBean refreshExternalUserAuth(ExternalUserAuthJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            ExternalUserAuth externalUserAuth = convertExternalUserAuthJsBeanToBean(jsBean);
            externalUserAuth = getService().refreshExternalUserAuth(externalUserAuth);
            jsBean = convertExternalUserAuthToJsBean(externalUserAuth);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteExternalUserAuth(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteExternalUserAuth(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteExternalUserAuth(ExternalUserAuthJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            ExternalUserAuth externalUserAuth = convertExternalUserAuthJsBeanToBean(jsBean);
            return getService().deleteExternalUserAuth(externalUserAuth);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteExternalUserAuths(String filter, String params, List<String> values) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteExternalUserAuths(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }


    public static ExternalUserAuthJsBean convertExternalUserAuthToJsBean(ExternalUserAuth externalUserAuth)
    {
        ExternalUserAuthJsBean jsBean = null;
        if(externalUserAuth != null) {
            jsBean = new ExternalUserAuthJsBean();
            jsBean.setGuid(externalUserAuth.getGuid());
            jsBean.setUser(externalUserAuth.getUser());
            jsBean.setAuthType(externalUserAuth.getAuthType());
            jsBean.setProviderId(externalUserAuth.getProviderId());
            jsBean.setProviderDomain(externalUserAuth.getProviderDomain());
            jsBean.setExternalUserId(ExternalUserIdStructWebUtil.convertExternalUserIdStructToJsBean(externalUserAuth.getExternalUserId()));
            jsBean.setRequestToken(externalUserAuth.getRequestToken());
            jsBean.setAccessToken(externalUserAuth.getAccessToken());
            jsBean.setAccessTokenSecret(externalUserAuth.getAccessTokenSecret());
            jsBean.setEmail(externalUserAuth.getEmail());
            jsBean.setFirstName(externalUserAuth.getFirstName());
            jsBean.setLastName(externalUserAuth.getLastName());
            jsBean.setFullName(externalUserAuth.getFullName());
            jsBean.setDisplayName(externalUserAuth.getDisplayName());
            jsBean.setDescription(externalUserAuth.getDescription());
            jsBean.setGender(externalUserAuth.getGender());
            jsBean.setDateOfBirth(externalUserAuth.getDateOfBirth());
            jsBean.setProfileImageUrl(externalUserAuth.getProfileImageUrl());
            jsBean.setTimeZone(externalUserAuth.getTimeZone());
            jsBean.setPostalCode(externalUserAuth.getPostalCode());
            jsBean.setLocation(externalUserAuth.getLocation());
            jsBean.setCountry(externalUserAuth.getCountry());
            jsBean.setLanguage(externalUserAuth.getLanguage());
            jsBean.setStatus(externalUserAuth.getStatus());
            jsBean.setAuthTime(externalUserAuth.getAuthTime());
            jsBean.setExpirationTime(externalUserAuth.getExpirationTime());
            jsBean.setCreatedTime(externalUserAuth.getCreatedTime());
            jsBean.setModifiedTime(externalUserAuth.getModifiedTime());
        }
        return jsBean;
    }

    public static ExternalUserAuth convertExternalUserAuthJsBeanToBean(ExternalUserAuthJsBean jsBean)
    {
        ExternalUserAuthBean externalUserAuth = null;
        if(jsBean != null) {
            externalUserAuth = new ExternalUserAuthBean();
            externalUserAuth.setGuid(jsBean.getGuid());
            externalUserAuth.setUser(jsBean.getUser());
            externalUserAuth.setAuthType(jsBean.getAuthType());
            externalUserAuth.setProviderId(jsBean.getProviderId());
            externalUserAuth.setProviderDomain(jsBean.getProviderDomain());
            externalUserAuth.setExternalUserId(ExternalUserIdStructWebUtil.convertExternalUserIdStructJsBeanToBean(jsBean.getExternalUserId()));
            externalUserAuth.setRequestToken(jsBean.getRequestToken());
            externalUserAuth.setAccessToken(jsBean.getAccessToken());
            externalUserAuth.setAccessTokenSecret(jsBean.getAccessTokenSecret());
            externalUserAuth.setEmail(jsBean.getEmail());
            externalUserAuth.setFirstName(jsBean.getFirstName());
            externalUserAuth.setLastName(jsBean.getLastName());
            externalUserAuth.setFullName(jsBean.getFullName());
            externalUserAuth.setDisplayName(jsBean.getDisplayName());
            externalUserAuth.setDescription(jsBean.getDescription());
            externalUserAuth.setGender(jsBean.getGender());
            externalUserAuth.setDateOfBirth(jsBean.getDateOfBirth());
            externalUserAuth.setProfileImageUrl(jsBean.getProfileImageUrl());
            externalUserAuth.setTimeZone(jsBean.getTimeZone());
            externalUserAuth.setPostalCode(jsBean.getPostalCode());
            externalUserAuth.setLocation(jsBean.getLocation());
            externalUserAuth.setCountry(jsBean.getCountry());
            externalUserAuth.setLanguage(jsBean.getLanguage());
            externalUserAuth.setStatus(jsBean.getStatus());
            externalUserAuth.setAuthTime(jsBean.getAuthTime());
            externalUserAuth.setExpirationTime(jsBean.getExpirationTime());
            externalUserAuth.setCreatedTime(jsBean.getCreatedTime());
            externalUserAuth.setModifiedTime(jsBean.getModifiedTime());
        }
        return externalUserAuth;
    }

}
