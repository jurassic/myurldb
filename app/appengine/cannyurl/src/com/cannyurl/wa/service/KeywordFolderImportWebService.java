package com.cannyurl.wa.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.KeywordFolderImport;
import com.cannyurl.af.bean.KeywordFolderImportBean;
import com.cannyurl.af.service.KeywordFolderImportService;
import com.cannyurl.af.service.manager.ServiceManager;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.KeywordFolderImportJsBean;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class KeywordFolderImportWebService // implements KeywordFolderImportService
{
    private static final Logger log = Logger.getLogger(KeywordFolderImportWebService.class.getName());
     
    // Af service interface.
    private KeywordFolderImportService mService = null;

    public KeywordFolderImportWebService()
    {
        this(ServiceManager.getKeywordFolderImportService());
    }
    public KeywordFolderImportWebService(KeywordFolderImportService service)
    {
        mService = service;
    }
    
    private KeywordFolderImportService getService()
    {
        if(mService == null) {
            mService = ServiceManager.getKeywordFolderImportService();
        }
        return mService;
    }
    
    
    public KeywordFolderImportJsBean getKeywordFolderImport(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            KeywordFolderImport keywordFolderImport = getService().getKeywordFolderImport(guid);
            KeywordFolderImportJsBean bean = convertKeywordFolderImportToJsBean(keywordFolderImport);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getKeywordFolderImport(String guid, String field) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().getKeywordFolderImport(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<KeywordFolderImportJsBean> getKeywordFolderImports(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<KeywordFolderImportJsBean> jsBeans = new ArrayList<KeywordFolderImportJsBean>();
            List<KeywordFolderImport> keywordFolderImports = getService().getKeywordFolderImports(guids);
            if(keywordFolderImports != null) {
                for(KeywordFolderImport keywordFolderImport : keywordFolderImports) {
                    jsBeans.add(convertKeywordFolderImportToJsBean(keywordFolderImport));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<KeywordFolderImportJsBean> getAllKeywordFolderImports() throws WebException
    {
        return getAllKeywordFolderImports(null, null, null);
    }

    public List<KeywordFolderImportJsBean> getAllKeywordFolderImports(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<KeywordFolderImportJsBean> jsBeans = new ArrayList<KeywordFolderImportJsBean>();
            List<KeywordFolderImport> keywordFolderImports = getService().getAllKeywordFolderImports(ordering, offset, count);
            if(keywordFolderImports != null) {
                for(KeywordFolderImport keywordFolderImport : keywordFolderImports) {
                    jsBeans.add(convertKeywordFolderImportToJsBean(keywordFolderImport));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> getAllKeywordFolderImportKeys(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllKeywordFolderImportKeys(ordering, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<KeywordFolderImportJsBean> findKeywordFolderImports(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findKeywordFolderImports(filter, ordering, params, values, null, null, null, null);
    }

    public List<KeywordFolderImportJsBean> findKeywordFolderImports(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<KeywordFolderImportJsBean> jsBeans = new ArrayList<KeywordFolderImportJsBean>();
            List<KeywordFolderImport> keywordFolderImports = getService().findKeywordFolderImports(filter, ordering, params, values, grouping, unique, offset, count);
            if(keywordFolderImports != null) {
                for(KeywordFolderImport keywordFolderImport : keywordFolderImports) {
                    jsBeans.add(convertKeywordFolderImportToJsBean(keywordFolderImport));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> findKeywordFolderImportKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findKeywordFolderImportKeys(filter, ordering, params, values, grouping, unique, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        log.finer("BEGIN");

        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createKeywordFolderImport(String user, Integer precedence, String importType, String importedFolder, String importedFolderUser, String importedFolderTitle, String importedFolderPath, String status, String note, String keywordFolder) throws WebException
    {
        try {
            return getService().createKeywordFolderImport(user, precedence, importType, importedFolder, importedFolderUser, importedFolderTitle, importedFolderPath, status, note, keywordFolder);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createKeywordFolderImport(String jsonStr) throws WebException
    {
        return createKeywordFolderImport(KeywordFolderImportJsBean.fromJsonString(jsonStr));
    }

    public String createKeywordFolderImport(KeywordFolderImportJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            KeywordFolderImport keywordFolderImport = convertKeywordFolderImportJsBeanToBean(jsBean);
            return getService().createKeywordFolderImport(keywordFolderImport);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public KeywordFolderImportJsBean constructKeywordFolderImport(String jsonStr) throws WebException
    {
        return constructKeywordFolderImport(KeywordFolderImportJsBean.fromJsonString(jsonStr));
    }

    public KeywordFolderImportJsBean constructKeywordFolderImport(KeywordFolderImportJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            KeywordFolderImport keywordFolderImport = convertKeywordFolderImportJsBeanToBean(jsBean);
            keywordFolderImport = getService().constructKeywordFolderImport(keywordFolderImport);
            jsBean = convertKeywordFolderImportToJsBean(keywordFolderImport);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateKeywordFolderImport(String guid, String user, Integer precedence, String importType, String importedFolder, String importedFolderUser, String importedFolderTitle, String importedFolderPath, String status, String note, String keywordFolder) throws WebException
    {
        try {
            return getService().updateKeywordFolderImport(guid, user, precedence, importType, importedFolder, importedFolderUser, importedFolderTitle, importedFolderPath, status, note, keywordFolder);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateKeywordFolderImport(String jsonStr) throws WebException
    {
        return updateKeywordFolderImport(KeywordFolderImportJsBean.fromJsonString(jsonStr));
    }

    public Boolean updateKeywordFolderImport(KeywordFolderImportJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            KeywordFolderImport keywordFolderImport = convertKeywordFolderImportJsBeanToBean(jsBean);
            return getService().updateKeywordFolderImport(keywordFolderImport);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public KeywordFolderImportJsBean refreshKeywordFolderImport(String jsonStr) throws WebException
    {
        return refreshKeywordFolderImport(KeywordFolderImportJsBean.fromJsonString(jsonStr));
    }

    public KeywordFolderImportJsBean refreshKeywordFolderImport(KeywordFolderImportJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            KeywordFolderImport keywordFolderImport = convertKeywordFolderImportJsBeanToBean(jsBean);
            keywordFolderImport = getService().refreshKeywordFolderImport(keywordFolderImport);
            jsBean = convertKeywordFolderImportToJsBean(keywordFolderImport);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteKeywordFolderImport(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteKeywordFolderImport(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteKeywordFolderImport(KeywordFolderImportJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            KeywordFolderImport keywordFolderImport = convertKeywordFolderImportJsBeanToBean(jsBean);
            return getService().deleteKeywordFolderImport(keywordFolderImport);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteKeywordFolderImports(String filter, String params, List<String> values) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteKeywordFolderImports(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }


    public static KeywordFolderImportJsBean convertKeywordFolderImportToJsBean(KeywordFolderImport keywordFolderImport)
    {
        KeywordFolderImportJsBean jsBean = null;
        if(keywordFolderImport != null) {
            jsBean = new KeywordFolderImportJsBean();
            jsBean.setGuid(keywordFolderImport.getGuid());
            jsBean.setUser(keywordFolderImport.getUser());
            jsBean.setPrecedence(keywordFolderImport.getPrecedence());
            jsBean.setImportType(keywordFolderImport.getImportType());
            jsBean.setImportedFolder(keywordFolderImport.getImportedFolder());
            jsBean.setImportedFolderUser(keywordFolderImport.getImportedFolderUser());
            jsBean.setImportedFolderTitle(keywordFolderImport.getImportedFolderTitle());
            jsBean.setImportedFolderPath(keywordFolderImport.getImportedFolderPath());
            jsBean.setStatus(keywordFolderImport.getStatus());
            jsBean.setNote(keywordFolderImport.getNote());
            jsBean.setKeywordFolder(keywordFolderImport.getKeywordFolder());
            jsBean.setCreatedTime(keywordFolderImport.getCreatedTime());
            jsBean.setModifiedTime(keywordFolderImport.getModifiedTime());
        }
        return jsBean;
    }

    public static KeywordFolderImport convertKeywordFolderImportJsBeanToBean(KeywordFolderImportJsBean jsBean)
    {
        KeywordFolderImportBean keywordFolderImport = null;
        if(jsBean != null) {
            keywordFolderImport = new KeywordFolderImportBean();
            keywordFolderImport.setGuid(jsBean.getGuid());
            keywordFolderImport.setUser(jsBean.getUser());
            keywordFolderImport.setPrecedence(jsBean.getPrecedence());
            keywordFolderImport.setImportType(jsBean.getImportType());
            keywordFolderImport.setImportedFolder(jsBean.getImportedFolder());
            keywordFolderImport.setImportedFolderUser(jsBean.getImportedFolderUser());
            keywordFolderImport.setImportedFolderTitle(jsBean.getImportedFolderTitle());
            keywordFolderImport.setImportedFolderPath(jsBean.getImportedFolderPath());
            keywordFolderImport.setStatus(jsBean.getStatus());
            keywordFolderImport.setNote(jsBean.getNote());
            keywordFolderImport.setKeywordFolder(jsBean.getKeywordFolder());
            keywordFolderImport.setCreatedTime(jsBean.getCreatedTime());
            keywordFolderImport.setModifiedTime(jsBean.getModifiedTime());
        }
        return keywordFolderImport;
    }

}
