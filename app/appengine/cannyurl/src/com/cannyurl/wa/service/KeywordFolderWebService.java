package com.cannyurl.wa.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.KeywordFolder;
import com.cannyurl.af.bean.KeywordFolderBean;
import com.cannyurl.af.service.KeywordFolderService;
import com.cannyurl.af.service.manager.ServiceManager;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.KeywordFolderJsBean;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class KeywordFolderWebService // implements KeywordFolderService
{
    private static final Logger log = Logger.getLogger(KeywordFolderWebService.class.getName());
     
    // Af service interface.
    private KeywordFolderService mService = null;

    public KeywordFolderWebService()
    {
        this(ServiceManager.getKeywordFolderService());
    }
    public KeywordFolderWebService(KeywordFolderService service)
    {
        mService = service;
    }
    
    private KeywordFolderService getService()
    {
        if(mService == null) {
            mService = ServiceManager.getKeywordFolderService();
        }
        return mService;
    }
    
    
    public KeywordFolderJsBean getKeywordFolder(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            KeywordFolder keywordFolder = getService().getKeywordFolder(guid);
            KeywordFolderJsBean bean = convertKeywordFolderToJsBean(keywordFolder);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getKeywordFolder(String guid, String field) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().getKeywordFolder(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<KeywordFolderJsBean> getKeywordFolders(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<KeywordFolderJsBean> jsBeans = new ArrayList<KeywordFolderJsBean>();
            List<KeywordFolder> keywordFolders = getService().getKeywordFolders(guids);
            if(keywordFolders != null) {
                for(KeywordFolder keywordFolder : keywordFolders) {
                    jsBeans.add(convertKeywordFolderToJsBean(keywordFolder));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<KeywordFolderJsBean> getAllKeywordFolders() throws WebException
    {
        return getAllKeywordFolders(null, null, null);
    }

    public List<KeywordFolderJsBean> getAllKeywordFolders(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<KeywordFolderJsBean> jsBeans = new ArrayList<KeywordFolderJsBean>();
            List<KeywordFolder> keywordFolders = getService().getAllKeywordFolders(ordering, offset, count);
            if(keywordFolders != null) {
                for(KeywordFolder keywordFolder : keywordFolders) {
                    jsBeans.add(convertKeywordFolderToJsBean(keywordFolder));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> getAllKeywordFolderKeys(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllKeywordFolderKeys(ordering, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<KeywordFolderJsBean> findKeywordFolders(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findKeywordFolders(filter, ordering, params, values, null, null, null, null);
    }

    public List<KeywordFolderJsBean> findKeywordFolders(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<KeywordFolderJsBean> jsBeans = new ArrayList<KeywordFolderJsBean>();
            List<KeywordFolder> keywordFolders = getService().findKeywordFolders(filter, ordering, params, values, grouping, unique, offset, count);
            if(keywordFolders != null) {
                for(KeywordFolder keywordFolder : keywordFolders) {
                    jsBeans.add(convertKeywordFolderToJsBean(keywordFolder));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> findKeywordFolderKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findKeywordFolderKeys(filter, ordering, params, values, grouping, unique, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        log.finer("BEGIN");

        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createKeywordFolder(String appClient, String clientRootDomain, String user, String title, String description, String type, String category, String parent, String aggregate, String acl, Boolean exportable, String status, String note, String folderPath) throws WebException
    {
        try {
            return getService().createKeywordFolder(appClient, clientRootDomain, user, title, description, type, category, parent, aggregate, acl, exportable, status, note, folderPath);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createKeywordFolder(String jsonStr) throws WebException
    {
        return createKeywordFolder(KeywordFolderJsBean.fromJsonString(jsonStr));
    }

    public String createKeywordFolder(KeywordFolderJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            KeywordFolder keywordFolder = convertKeywordFolderJsBeanToBean(jsBean);
            return getService().createKeywordFolder(keywordFolder);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public KeywordFolderJsBean constructKeywordFolder(String jsonStr) throws WebException
    {
        return constructKeywordFolder(KeywordFolderJsBean.fromJsonString(jsonStr));
    }

    public KeywordFolderJsBean constructKeywordFolder(KeywordFolderJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            KeywordFolder keywordFolder = convertKeywordFolderJsBeanToBean(jsBean);
            keywordFolder = getService().constructKeywordFolder(keywordFolder);
            jsBean = convertKeywordFolderToJsBean(keywordFolder);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateKeywordFolder(String guid, String appClient, String clientRootDomain, String user, String title, String description, String type, String category, String parent, String aggregate, String acl, Boolean exportable, String status, String note, String folderPath) throws WebException
    {
        try {
            return getService().updateKeywordFolder(guid, appClient, clientRootDomain, user, title, description, type, category, parent, aggregate, acl, exportable, status, note, folderPath);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateKeywordFolder(String jsonStr) throws WebException
    {
        return updateKeywordFolder(KeywordFolderJsBean.fromJsonString(jsonStr));
    }

    public Boolean updateKeywordFolder(KeywordFolderJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            KeywordFolder keywordFolder = convertKeywordFolderJsBeanToBean(jsBean);
            return getService().updateKeywordFolder(keywordFolder);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public KeywordFolderJsBean refreshKeywordFolder(String jsonStr) throws WebException
    {
        return refreshKeywordFolder(KeywordFolderJsBean.fromJsonString(jsonStr));
    }

    public KeywordFolderJsBean refreshKeywordFolder(KeywordFolderJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            KeywordFolder keywordFolder = convertKeywordFolderJsBeanToBean(jsBean);
            keywordFolder = getService().refreshKeywordFolder(keywordFolder);
            jsBean = convertKeywordFolderToJsBean(keywordFolder);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteKeywordFolder(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteKeywordFolder(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteKeywordFolder(KeywordFolderJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            KeywordFolder keywordFolder = convertKeywordFolderJsBeanToBean(jsBean);
            return getService().deleteKeywordFolder(keywordFolder);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteKeywordFolders(String filter, String params, List<String> values) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteKeywordFolders(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }


    public static KeywordFolderJsBean convertKeywordFolderToJsBean(KeywordFolder keywordFolder)
    {
        KeywordFolderJsBean jsBean = null;
        if(keywordFolder != null) {
            jsBean = new KeywordFolderJsBean();
            jsBean.setGuid(keywordFolder.getGuid());
            jsBean.setAppClient(keywordFolder.getAppClient());
            jsBean.setClientRootDomain(keywordFolder.getClientRootDomain());
            jsBean.setUser(keywordFolder.getUser());
            jsBean.setTitle(keywordFolder.getTitle());
            jsBean.setDescription(keywordFolder.getDescription());
            jsBean.setType(keywordFolder.getType());
            jsBean.setCategory(keywordFolder.getCategory());
            jsBean.setParent(keywordFolder.getParent());
            jsBean.setAggregate(keywordFolder.getAggregate());
            jsBean.setAcl(keywordFolder.getAcl());
            jsBean.setExportable(keywordFolder.isExportable());
            jsBean.setStatus(keywordFolder.getStatus());
            jsBean.setNote(keywordFolder.getNote());
            jsBean.setFolderPath(keywordFolder.getFolderPath());
            jsBean.setCreatedTime(keywordFolder.getCreatedTime());
            jsBean.setModifiedTime(keywordFolder.getModifiedTime());
        }
        return jsBean;
    }

    public static KeywordFolder convertKeywordFolderJsBeanToBean(KeywordFolderJsBean jsBean)
    {
        KeywordFolderBean keywordFolder = null;
        if(jsBean != null) {
            keywordFolder = new KeywordFolderBean();
            keywordFolder.setGuid(jsBean.getGuid());
            keywordFolder.setAppClient(jsBean.getAppClient());
            keywordFolder.setClientRootDomain(jsBean.getClientRootDomain());
            keywordFolder.setUser(jsBean.getUser());
            keywordFolder.setTitle(jsBean.getTitle());
            keywordFolder.setDescription(jsBean.getDescription());
            keywordFolder.setType(jsBean.getType());
            keywordFolder.setCategory(jsBean.getCategory());
            keywordFolder.setParent(jsBean.getParent());
            keywordFolder.setAggregate(jsBean.getAggregate());
            keywordFolder.setAcl(jsBean.getAcl());
            keywordFolder.setExportable(jsBean.isExportable());
            keywordFolder.setStatus(jsBean.getStatus());
            keywordFolder.setNote(jsBean.getNote());
            keywordFolder.setFolderPath(jsBean.getFolderPath());
            keywordFolder.setCreatedTime(jsBean.getCreatedTime());
            keywordFolder.setModifiedTime(jsBean.getModifiedTime());
        }
        return keywordFolder;
    }

}
