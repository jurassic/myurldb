package com.cannyurl.wa.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.LinkAlbum;
import com.cannyurl.af.bean.LinkAlbumBean;
import com.cannyurl.af.service.LinkAlbumService;
import com.cannyurl.af.service.manager.ServiceManager;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.LinkAlbumJsBean;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class LinkAlbumWebService // implements LinkAlbumService
{
    private static final Logger log = Logger.getLogger(LinkAlbumWebService.class.getName());
     
    // Af service interface.
    private LinkAlbumService mService = null;

    public LinkAlbumWebService()
    {
        this(ServiceManager.getLinkAlbumService());
    }
    public LinkAlbumWebService(LinkAlbumService service)
    {
        mService = service;
    }
    
    private LinkAlbumService getService()
    {
        if(mService == null) {
            mService = ServiceManager.getLinkAlbumService();
        }
        return mService;
    }
    
    
    public LinkAlbumJsBean getLinkAlbum(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            LinkAlbum linkAlbum = getService().getLinkAlbum(guid);
            LinkAlbumJsBean bean = convertLinkAlbumToJsBean(linkAlbum);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getLinkAlbum(String guid, String field) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().getLinkAlbum(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<LinkAlbumJsBean> getLinkAlbums(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<LinkAlbumJsBean> jsBeans = new ArrayList<LinkAlbumJsBean>();
            List<LinkAlbum> linkAlbums = getService().getLinkAlbums(guids);
            if(linkAlbums != null) {
                for(LinkAlbum linkAlbum : linkAlbums) {
                    jsBeans.add(convertLinkAlbumToJsBean(linkAlbum));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<LinkAlbumJsBean> getAllLinkAlbums() throws WebException
    {
        return getAllLinkAlbums(null, null, null);
    }

    public List<LinkAlbumJsBean> getAllLinkAlbums(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<LinkAlbumJsBean> jsBeans = new ArrayList<LinkAlbumJsBean>();
            List<LinkAlbum> linkAlbums = getService().getAllLinkAlbums(ordering, offset, count);
            if(linkAlbums != null) {
                for(LinkAlbum linkAlbum : linkAlbums) {
                    jsBeans.add(convertLinkAlbumToJsBean(linkAlbum));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> getAllLinkAlbumKeys(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllLinkAlbumKeys(ordering, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<LinkAlbumJsBean> findLinkAlbums(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findLinkAlbums(filter, ordering, params, values, null, null, null, null);
    }

    public List<LinkAlbumJsBean> findLinkAlbums(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<LinkAlbumJsBean> jsBeans = new ArrayList<LinkAlbumJsBean>();
            List<LinkAlbum> linkAlbums = getService().findLinkAlbums(filter, ordering, params, values, grouping, unique, offset, count);
            if(linkAlbums != null) {
                for(LinkAlbum linkAlbum : linkAlbums) {
                    jsBeans.add(convertLinkAlbumToJsBean(linkAlbum));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> findLinkAlbumKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findLinkAlbumKeys(filter, ordering, params, values, grouping, unique, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        log.finer("BEGIN");

        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createLinkAlbum(String appClient, String clientRootDomain, String owner, String name, String summary, String tokenPrefix, String permalink, String shortLink, String shortUrl, String source, String referenceUrl, Boolean autoGenerated, Integer maxLinkCount, String note, String status) throws WebException
    {
        try {
            return getService().createLinkAlbum(appClient, clientRootDomain, owner, name, summary, tokenPrefix, permalink, shortLink, shortUrl, source, referenceUrl, autoGenerated, maxLinkCount, note, status);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createLinkAlbum(String jsonStr) throws WebException
    {
        return createLinkAlbum(LinkAlbumJsBean.fromJsonString(jsonStr));
    }

    public String createLinkAlbum(LinkAlbumJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            LinkAlbum linkAlbum = convertLinkAlbumJsBeanToBean(jsBean);
            return getService().createLinkAlbum(linkAlbum);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public LinkAlbumJsBean constructLinkAlbum(String jsonStr) throws WebException
    {
        return constructLinkAlbum(LinkAlbumJsBean.fromJsonString(jsonStr));
    }

    public LinkAlbumJsBean constructLinkAlbum(LinkAlbumJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            LinkAlbum linkAlbum = convertLinkAlbumJsBeanToBean(jsBean);
            linkAlbum = getService().constructLinkAlbum(linkAlbum);
            jsBean = convertLinkAlbumToJsBean(linkAlbum);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateLinkAlbum(String guid, String appClient, String clientRootDomain, String owner, String name, String summary, String tokenPrefix, String permalink, String shortLink, String shortUrl, String source, String referenceUrl, Boolean autoGenerated, Integer maxLinkCount, String note, String status) throws WebException
    {
        try {
            return getService().updateLinkAlbum(guid, appClient, clientRootDomain, owner, name, summary, tokenPrefix, permalink, shortLink, shortUrl, source, referenceUrl, autoGenerated, maxLinkCount, note, status);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateLinkAlbum(String jsonStr) throws WebException
    {
        return updateLinkAlbum(LinkAlbumJsBean.fromJsonString(jsonStr));
    }

    public Boolean updateLinkAlbum(LinkAlbumJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            LinkAlbum linkAlbum = convertLinkAlbumJsBeanToBean(jsBean);
            return getService().updateLinkAlbum(linkAlbum);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public LinkAlbumJsBean refreshLinkAlbum(String jsonStr) throws WebException
    {
        return refreshLinkAlbum(LinkAlbumJsBean.fromJsonString(jsonStr));
    }

    public LinkAlbumJsBean refreshLinkAlbum(LinkAlbumJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            LinkAlbum linkAlbum = convertLinkAlbumJsBeanToBean(jsBean);
            linkAlbum = getService().refreshLinkAlbum(linkAlbum);
            jsBean = convertLinkAlbumToJsBean(linkAlbum);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteLinkAlbum(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteLinkAlbum(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteLinkAlbum(LinkAlbumJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            LinkAlbum linkAlbum = convertLinkAlbumJsBeanToBean(jsBean);
            return getService().deleteLinkAlbum(linkAlbum);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteLinkAlbums(String filter, String params, List<String> values) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteLinkAlbums(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }


    public static LinkAlbumJsBean convertLinkAlbumToJsBean(LinkAlbum linkAlbum)
    {
        LinkAlbumJsBean jsBean = null;
        if(linkAlbum != null) {
            jsBean = new LinkAlbumJsBean();
            jsBean.setGuid(linkAlbum.getGuid());
            jsBean.setAppClient(linkAlbum.getAppClient());
            jsBean.setClientRootDomain(linkAlbum.getClientRootDomain());
            jsBean.setOwner(linkAlbum.getOwner());
            jsBean.setName(linkAlbum.getName());
            jsBean.setSummary(linkAlbum.getSummary());
            jsBean.setTokenPrefix(linkAlbum.getTokenPrefix());
            jsBean.setPermalink(linkAlbum.getPermalink());
            jsBean.setShortLink(linkAlbum.getShortLink());
            jsBean.setShortUrl(linkAlbum.getShortUrl());
            jsBean.setSource(linkAlbum.getSource());
            jsBean.setReferenceUrl(linkAlbum.getReferenceUrl());
            jsBean.setAutoGenerated(linkAlbum.isAutoGenerated());
            jsBean.setMaxLinkCount(linkAlbum.getMaxLinkCount());
            jsBean.setNote(linkAlbum.getNote());
            jsBean.setStatus(linkAlbum.getStatus());
            jsBean.setCreatedTime(linkAlbum.getCreatedTime());
            jsBean.setModifiedTime(linkAlbum.getModifiedTime());
        }
        return jsBean;
    }

    public static LinkAlbum convertLinkAlbumJsBeanToBean(LinkAlbumJsBean jsBean)
    {
        LinkAlbumBean linkAlbum = null;
        if(jsBean != null) {
            linkAlbum = new LinkAlbumBean();
            linkAlbum.setGuid(jsBean.getGuid());
            linkAlbum.setAppClient(jsBean.getAppClient());
            linkAlbum.setClientRootDomain(jsBean.getClientRootDomain());
            linkAlbum.setOwner(jsBean.getOwner());
            linkAlbum.setName(jsBean.getName());
            linkAlbum.setSummary(jsBean.getSummary());
            linkAlbum.setTokenPrefix(jsBean.getTokenPrefix());
            linkAlbum.setPermalink(jsBean.getPermalink());
            linkAlbum.setShortLink(jsBean.getShortLink());
            linkAlbum.setShortUrl(jsBean.getShortUrl());
            linkAlbum.setSource(jsBean.getSource());
            linkAlbum.setReferenceUrl(jsBean.getReferenceUrl());
            linkAlbum.setAutoGenerated(jsBean.isAutoGenerated());
            linkAlbum.setMaxLinkCount(jsBean.getMaxLinkCount());
            linkAlbum.setNote(jsBean.getNote());
            linkAlbum.setStatus(jsBean.getStatus());
            linkAlbum.setCreatedTime(jsBean.getCreatedTime());
            linkAlbum.setModifiedTime(jsBean.getModifiedTime());
        }
        return linkAlbum;
    }

}
