package com.cannyurl.wa.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.LinkMessage;
import com.cannyurl.af.bean.LinkMessageBean;
import com.cannyurl.af.service.LinkMessageService;
import com.cannyurl.af.service.manager.ServiceManager;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.LinkMessageJsBean;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class LinkMessageWebService // implements LinkMessageService
{
    private static final Logger log = Logger.getLogger(LinkMessageWebService.class.getName());
     
    // Af service interface.
    private LinkMessageService mService = null;

    public LinkMessageWebService()
    {
        this(ServiceManager.getLinkMessageService());
    }
    public LinkMessageWebService(LinkMessageService service)
    {
        mService = service;
    }
    
    private LinkMessageService getService()
    {
        if(mService == null) {
            mService = ServiceManager.getLinkMessageService();
        }
        return mService;
    }
    
    
    public LinkMessageJsBean getLinkMessage(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            LinkMessage linkMessage = getService().getLinkMessage(guid);
            LinkMessageJsBean bean = convertLinkMessageToJsBean(linkMessage);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getLinkMessage(String guid, String field) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().getLinkMessage(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<LinkMessageJsBean> getLinkMessages(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<LinkMessageJsBean> jsBeans = new ArrayList<LinkMessageJsBean>();
            List<LinkMessage> linkMessages = getService().getLinkMessages(guids);
            if(linkMessages != null) {
                for(LinkMessage linkMessage : linkMessages) {
                    jsBeans.add(convertLinkMessageToJsBean(linkMessage));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<LinkMessageJsBean> getAllLinkMessages() throws WebException
    {
        return getAllLinkMessages(null, null, null);
    }

    public List<LinkMessageJsBean> getAllLinkMessages(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<LinkMessageJsBean> jsBeans = new ArrayList<LinkMessageJsBean>();
            List<LinkMessage> linkMessages = getService().getAllLinkMessages(ordering, offset, count);
            if(linkMessages != null) {
                for(LinkMessage linkMessage : linkMessages) {
                    jsBeans.add(convertLinkMessageToJsBean(linkMessage));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> getAllLinkMessageKeys(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllLinkMessageKeys(ordering, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<LinkMessageJsBean> findLinkMessages(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findLinkMessages(filter, ordering, params, values, null, null, null, null);
    }

    public List<LinkMessageJsBean> findLinkMessages(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<LinkMessageJsBean> jsBeans = new ArrayList<LinkMessageJsBean>();
            List<LinkMessage> linkMessages = getService().findLinkMessages(filter, ordering, params, values, grouping, unique, offset, count);
            if(linkMessages != null) {
                for(LinkMessage linkMessage : linkMessages) {
                    jsBeans.add(convertLinkMessageToJsBean(linkMessage));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> findLinkMessageKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findLinkMessageKeys(filter, ordering, params, values, grouping, unique, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        log.finer("BEGIN");

        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createLinkMessage(String shortLink, String message, String password) throws WebException
    {
        try {
            return getService().createLinkMessage(shortLink, message, password);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createLinkMessage(String jsonStr) throws WebException
    {
        return createLinkMessage(LinkMessageJsBean.fromJsonString(jsonStr));
    }

    public String createLinkMessage(LinkMessageJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            LinkMessage linkMessage = convertLinkMessageJsBeanToBean(jsBean);
            return getService().createLinkMessage(linkMessage);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public LinkMessageJsBean constructLinkMessage(String jsonStr) throws WebException
    {
        return constructLinkMessage(LinkMessageJsBean.fromJsonString(jsonStr));
    }

    public LinkMessageJsBean constructLinkMessage(LinkMessageJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            LinkMessage linkMessage = convertLinkMessageJsBeanToBean(jsBean);
            linkMessage = getService().constructLinkMessage(linkMessage);
            jsBean = convertLinkMessageToJsBean(linkMessage);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateLinkMessage(String guid, String shortLink, String message, String password) throws WebException
    {
        try {
            return getService().updateLinkMessage(guid, shortLink, message, password);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateLinkMessage(String jsonStr) throws WebException
    {
        return updateLinkMessage(LinkMessageJsBean.fromJsonString(jsonStr));
    }

    public Boolean updateLinkMessage(LinkMessageJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            LinkMessage linkMessage = convertLinkMessageJsBeanToBean(jsBean);
            return getService().updateLinkMessage(linkMessage);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public LinkMessageJsBean refreshLinkMessage(String jsonStr) throws WebException
    {
        return refreshLinkMessage(LinkMessageJsBean.fromJsonString(jsonStr));
    }

    public LinkMessageJsBean refreshLinkMessage(LinkMessageJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            LinkMessage linkMessage = convertLinkMessageJsBeanToBean(jsBean);
            linkMessage = getService().refreshLinkMessage(linkMessage);
            jsBean = convertLinkMessageToJsBean(linkMessage);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteLinkMessage(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteLinkMessage(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteLinkMessage(LinkMessageJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            LinkMessage linkMessage = convertLinkMessageJsBeanToBean(jsBean);
            return getService().deleteLinkMessage(linkMessage);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteLinkMessages(String filter, String params, List<String> values) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteLinkMessages(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }


    public static LinkMessageJsBean convertLinkMessageToJsBean(LinkMessage linkMessage)
    {
        LinkMessageJsBean jsBean = null;
        if(linkMessage != null) {
            jsBean = new LinkMessageJsBean();
            jsBean.setGuid(linkMessage.getGuid());
            jsBean.setShortLink(linkMessage.getShortLink());
            jsBean.setMessage(linkMessage.getMessage());
            jsBean.setPassword(linkMessage.getPassword());
            jsBean.setCreatedTime(linkMessage.getCreatedTime());
            jsBean.setModifiedTime(linkMessage.getModifiedTime());
        }
        return jsBean;
    }

    public static LinkMessage convertLinkMessageJsBeanToBean(LinkMessageJsBean jsBean)
    {
        LinkMessageBean linkMessage = null;
        if(jsBean != null) {
            linkMessage = new LinkMessageBean();
            linkMessage.setGuid(jsBean.getGuid());
            linkMessage.setShortLink(jsBean.getShortLink());
            linkMessage.setMessage(jsBean.getMessage());
            linkMessage.setPassword(jsBean.getPassword());
            linkMessage.setCreatedTime(jsBean.getCreatedTime());
            linkMessage.setModifiedTime(jsBean.getModifiedTime());
        }
        return linkMessage;
    }

}
