package com.cannyurl.wa.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.QrCode;
import com.cannyurl.af.bean.QrCodeBean;
import com.cannyurl.af.service.QrCodeService;
import com.cannyurl.af.service.manager.ServiceManager;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.QrCodeJsBean;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class QrCodeWebService // implements QrCodeService
{
    private static final Logger log = Logger.getLogger(QrCodeWebService.class.getName());
     
    // Af service interface.
    private QrCodeService mService = null;

    public QrCodeWebService()
    {
        this(ServiceManager.getQrCodeService());
    }
    public QrCodeWebService(QrCodeService service)
    {
        mService = service;
    }
    
    private QrCodeService getService()
    {
        if(mService == null) {
            mService = ServiceManager.getQrCodeService();
        }
        return mService;
    }
    
    
    public QrCodeJsBean getQrCode(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            QrCode qrCode = getService().getQrCode(guid);
            QrCodeJsBean bean = convertQrCodeToJsBean(qrCode);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getQrCode(String guid, String field) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().getQrCode(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<QrCodeJsBean> getQrCodes(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<QrCodeJsBean> jsBeans = new ArrayList<QrCodeJsBean>();
            List<QrCode> qrCodes = getService().getQrCodes(guids);
            if(qrCodes != null) {
                for(QrCode qrCode : qrCodes) {
                    jsBeans.add(convertQrCodeToJsBean(qrCode));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<QrCodeJsBean> getAllQrCodes() throws WebException
    {
        return getAllQrCodes(null, null, null);
    }

    public List<QrCodeJsBean> getAllQrCodes(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<QrCodeJsBean> jsBeans = new ArrayList<QrCodeJsBean>();
            List<QrCode> qrCodes = getService().getAllQrCodes(ordering, offset, count);
            if(qrCodes != null) {
                for(QrCode qrCode : qrCodes) {
                    jsBeans.add(convertQrCodeToJsBean(qrCode));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> getAllQrCodeKeys(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllQrCodeKeys(ordering, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<QrCodeJsBean> findQrCodes(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findQrCodes(filter, ordering, params, values, null, null, null, null);
    }

    public List<QrCodeJsBean> findQrCodes(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<QrCodeJsBean> jsBeans = new ArrayList<QrCodeJsBean>();
            List<QrCode> qrCodes = getService().findQrCodes(filter, ordering, params, values, grouping, unique, offset, count);
            if(qrCodes != null) {
                for(QrCode qrCode : qrCodes) {
                    jsBeans.add(convertQrCodeToJsBean(qrCode));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> findQrCodeKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findQrCodeKeys(filter, ordering, params, values, grouping, unique, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        log.finer("BEGIN");

        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createQrCode(String shortLink, String imageLink, String imageUrl, String type, String status) throws WebException
    {
        try {
            return getService().createQrCode(shortLink, imageLink, imageUrl, type, status);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createQrCode(String jsonStr) throws WebException
    {
        return createQrCode(QrCodeJsBean.fromJsonString(jsonStr));
    }

    public String createQrCode(QrCodeJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            QrCode qrCode = convertQrCodeJsBeanToBean(jsBean);
            return getService().createQrCode(qrCode);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public QrCodeJsBean constructQrCode(String jsonStr) throws WebException
    {
        return constructQrCode(QrCodeJsBean.fromJsonString(jsonStr));
    }

    public QrCodeJsBean constructQrCode(QrCodeJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            QrCode qrCode = convertQrCodeJsBeanToBean(jsBean);
            qrCode = getService().constructQrCode(qrCode);
            jsBean = convertQrCodeToJsBean(qrCode);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateQrCode(String guid, String shortLink, String imageLink, String imageUrl, String type, String status) throws WebException
    {
        try {
            return getService().updateQrCode(guid, shortLink, imageLink, imageUrl, type, status);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateQrCode(String jsonStr) throws WebException
    {
        return updateQrCode(QrCodeJsBean.fromJsonString(jsonStr));
    }

    public Boolean updateQrCode(QrCodeJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            QrCode qrCode = convertQrCodeJsBeanToBean(jsBean);
            return getService().updateQrCode(qrCode);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public QrCodeJsBean refreshQrCode(String jsonStr) throws WebException
    {
        return refreshQrCode(QrCodeJsBean.fromJsonString(jsonStr));
    }

    public QrCodeJsBean refreshQrCode(QrCodeJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            QrCode qrCode = convertQrCodeJsBeanToBean(jsBean);
            qrCode = getService().refreshQrCode(qrCode);
            jsBean = convertQrCodeToJsBean(qrCode);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteQrCode(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteQrCode(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteQrCode(QrCodeJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            QrCode qrCode = convertQrCodeJsBeanToBean(jsBean);
            return getService().deleteQrCode(qrCode);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteQrCodes(String filter, String params, List<String> values) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteQrCodes(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }


    public static QrCodeJsBean convertQrCodeToJsBean(QrCode qrCode)
    {
        QrCodeJsBean jsBean = null;
        if(qrCode != null) {
            jsBean = new QrCodeJsBean();
            jsBean.setGuid(qrCode.getGuid());
            jsBean.setShortLink(qrCode.getShortLink());
            jsBean.setImageLink(qrCode.getImageLink());
            jsBean.setImageUrl(qrCode.getImageUrl());
            jsBean.setType(qrCode.getType());
            jsBean.setStatus(qrCode.getStatus());
            jsBean.setCreatedTime(qrCode.getCreatedTime());
            jsBean.setModifiedTime(qrCode.getModifiedTime());
        }
        return jsBean;
    }

    public static QrCode convertQrCodeJsBeanToBean(QrCodeJsBean jsBean)
    {
        QrCodeBean qrCode = null;
        if(jsBean != null) {
            qrCode = new QrCodeBean();
            qrCode.setGuid(jsBean.getGuid());
            qrCode.setShortLink(jsBean.getShortLink());
            qrCode.setImageLink(jsBean.getImageLink());
            qrCode.setImageUrl(jsBean.getImageUrl());
            qrCode.setType(jsBean.getType());
            qrCode.setStatus(jsBean.getStatus());
            qrCode.setCreatedTime(jsBean.getCreatedTime());
            qrCode.setModifiedTime(jsBean.getModifiedTime());
        }
        return qrCode;
    }

}
