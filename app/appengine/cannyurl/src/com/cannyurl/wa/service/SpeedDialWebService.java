package com.cannyurl.wa.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.SpeedDial;
import com.cannyurl.af.bean.SpeedDialBean;
import com.cannyurl.af.service.SpeedDialService;
import com.cannyurl.af.service.manager.ServiceManager;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.SpeedDialJsBean;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class SpeedDialWebService // implements SpeedDialService
{
    private static final Logger log = Logger.getLogger(SpeedDialWebService.class.getName());
     
    // Af service interface.
    private SpeedDialService mService = null;

    public SpeedDialWebService()
    {
        this(ServiceManager.getSpeedDialService());
    }
    public SpeedDialWebService(SpeedDialService service)
    {
        mService = service;
    }
    
    private SpeedDialService getService()
    {
        if(mService == null) {
            mService = ServiceManager.getSpeedDialService();
        }
        return mService;
    }
    
    
    public SpeedDialJsBean getSpeedDial(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            SpeedDial speedDial = getService().getSpeedDial(guid);
            SpeedDialJsBean bean = convertSpeedDialToJsBean(speedDial);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getSpeedDial(String guid, String field) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().getSpeedDial(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<SpeedDialJsBean> getSpeedDials(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<SpeedDialJsBean> jsBeans = new ArrayList<SpeedDialJsBean>();
            List<SpeedDial> speedDials = getService().getSpeedDials(guids);
            if(speedDials != null) {
                for(SpeedDial speedDial : speedDials) {
                    jsBeans.add(convertSpeedDialToJsBean(speedDial));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<SpeedDialJsBean> getAllSpeedDials() throws WebException
    {
        return getAllSpeedDials(null, null, null);
    }

    public List<SpeedDialJsBean> getAllSpeedDials(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<SpeedDialJsBean> jsBeans = new ArrayList<SpeedDialJsBean>();
            List<SpeedDial> speedDials = getService().getAllSpeedDials(ordering, offset, count);
            if(speedDials != null) {
                for(SpeedDial speedDial : speedDials) {
                    jsBeans.add(convertSpeedDialToJsBean(speedDial));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> getAllSpeedDialKeys(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllSpeedDialKeys(ordering, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<SpeedDialJsBean> findSpeedDials(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findSpeedDials(filter, ordering, params, values, null, null, null, null);
    }

    public List<SpeedDialJsBean> findSpeedDials(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<SpeedDialJsBean> jsBeans = new ArrayList<SpeedDialJsBean>();
            List<SpeedDial> speedDials = getService().findSpeedDials(filter, ordering, params, values, grouping, unique, offset, count);
            if(speedDials != null) {
                for(SpeedDial speedDial : speedDials) {
                    jsBeans.add(convertSpeedDialToJsBean(speedDial));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> findSpeedDialKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findSpeedDialKeys(filter, ordering, params, values, grouping, unique, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        log.finer("BEGIN");

        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createSpeedDial(String user, String code, String shortcut, String shortLink, String keywordLink, String bookmarkLink, String longUrl, String shortUrl, Boolean caseSensitive, String status, String note) throws WebException
    {
        try {
            return getService().createSpeedDial(user, code, shortcut, shortLink, keywordLink, bookmarkLink, longUrl, shortUrl, caseSensitive, status, note);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createSpeedDial(String jsonStr) throws WebException
    {
        return createSpeedDial(SpeedDialJsBean.fromJsonString(jsonStr));
    }

    public String createSpeedDial(SpeedDialJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            SpeedDial speedDial = convertSpeedDialJsBeanToBean(jsBean);
            return getService().createSpeedDial(speedDial);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public SpeedDialJsBean constructSpeedDial(String jsonStr) throws WebException
    {
        return constructSpeedDial(SpeedDialJsBean.fromJsonString(jsonStr));
    }

    public SpeedDialJsBean constructSpeedDial(SpeedDialJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            SpeedDial speedDial = convertSpeedDialJsBeanToBean(jsBean);
            speedDial = getService().constructSpeedDial(speedDial);
            jsBean = convertSpeedDialToJsBean(speedDial);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateSpeedDial(String guid, String user, String code, String shortcut, String shortLink, String keywordLink, String bookmarkLink, String longUrl, String shortUrl, Boolean caseSensitive, String status, String note) throws WebException
    {
        try {
            return getService().updateSpeedDial(guid, user, code, shortcut, shortLink, keywordLink, bookmarkLink, longUrl, shortUrl, caseSensitive, status, note);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateSpeedDial(String jsonStr) throws WebException
    {
        return updateSpeedDial(SpeedDialJsBean.fromJsonString(jsonStr));
    }

    public Boolean updateSpeedDial(SpeedDialJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            SpeedDial speedDial = convertSpeedDialJsBeanToBean(jsBean);
            return getService().updateSpeedDial(speedDial);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public SpeedDialJsBean refreshSpeedDial(String jsonStr) throws WebException
    {
        return refreshSpeedDial(SpeedDialJsBean.fromJsonString(jsonStr));
    }

    public SpeedDialJsBean refreshSpeedDial(SpeedDialJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            SpeedDial speedDial = convertSpeedDialJsBeanToBean(jsBean);
            speedDial = getService().refreshSpeedDial(speedDial);
            jsBean = convertSpeedDialToJsBean(speedDial);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteSpeedDial(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteSpeedDial(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteSpeedDial(SpeedDialJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            SpeedDial speedDial = convertSpeedDialJsBeanToBean(jsBean);
            return getService().deleteSpeedDial(speedDial);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteSpeedDials(String filter, String params, List<String> values) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteSpeedDials(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }


    public static SpeedDialJsBean convertSpeedDialToJsBean(SpeedDial speedDial)
    {
        SpeedDialJsBean jsBean = null;
        if(speedDial != null) {
            jsBean = new SpeedDialJsBean();
            jsBean.setGuid(speedDial.getGuid());
            jsBean.setUser(speedDial.getUser());
            jsBean.setCode(speedDial.getCode());
            jsBean.setShortcut(speedDial.getShortcut());
            jsBean.setShortLink(speedDial.getShortLink());
            jsBean.setKeywordLink(speedDial.getKeywordLink());
            jsBean.setBookmarkLink(speedDial.getBookmarkLink());
            jsBean.setLongUrl(speedDial.getLongUrl());
            jsBean.setShortUrl(speedDial.getShortUrl());
            jsBean.setCaseSensitive(speedDial.isCaseSensitive());
            jsBean.setStatus(speedDial.getStatus());
            jsBean.setNote(speedDial.getNote());
            jsBean.setCreatedTime(speedDial.getCreatedTime());
            jsBean.setModifiedTime(speedDial.getModifiedTime());
        }
        return jsBean;
    }

    public static SpeedDial convertSpeedDialJsBeanToBean(SpeedDialJsBean jsBean)
    {
        SpeedDialBean speedDial = null;
        if(jsBean != null) {
            speedDial = new SpeedDialBean();
            speedDial.setGuid(jsBean.getGuid());
            speedDial.setUser(jsBean.getUser());
            speedDial.setCode(jsBean.getCode());
            speedDial.setShortcut(jsBean.getShortcut());
            speedDial.setShortLink(jsBean.getShortLink());
            speedDial.setKeywordLink(jsBean.getKeywordLink());
            speedDial.setBookmarkLink(jsBean.getBookmarkLink());
            speedDial.setLongUrl(jsBean.getLongUrl());
            speedDial.setShortUrl(jsBean.getShortUrl());
            speedDial.setCaseSensitive(jsBean.isCaseSensitive());
            speedDial.setStatus(jsBean.getStatus());
            speedDial.setNote(jsBean.getNote());
            speedDial.setCreatedTime(jsBean.getCreatedTime());
            speedDial.setModifiedTime(jsBean.getModifiedTime());
        }
        return speedDial;
    }

}
