package com.cannyurl.wa.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.TwitterAppCard;
import com.cannyurl.af.bean.TwitterAppCardBean;
import com.cannyurl.af.service.TwitterAppCardService;
import com.cannyurl.af.service.manager.ServiceManager;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.TwitterCardAppInfoJsBean;
import com.cannyurl.fe.bean.TwitterCardProductDataJsBean;
import com.cannyurl.fe.bean.TwitterAppCardJsBean;
import com.cannyurl.wa.util.TwitterCardAppInfoWebUtil;
import com.cannyurl.wa.util.TwitterCardProductDataWebUtil;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class TwitterAppCardWebService // implements TwitterAppCardService
{
    private static final Logger log = Logger.getLogger(TwitterAppCardWebService.class.getName());
     
    // Af service interface.
    private TwitterAppCardService mService = null;

    public TwitterAppCardWebService()
    {
        this(ServiceManager.getTwitterAppCardService());
    }
    public TwitterAppCardWebService(TwitterAppCardService service)
    {
        mService = service;
    }
    
    private TwitterAppCardService getService()
    {
        if(mService == null) {
            mService = ServiceManager.getTwitterAppCardService();
        }
        return mService;
    }
    
    
    public TwitterAppCardJsBean getTwitterAppCard(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            TwitterAppCard twitterAppCard = getService().getTwitterAppCard(guid);
            TwitterAppCardJsBean bean = convertTwitterAppCardToJsBean(twitterAppCard);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getTwitterAppCard(String guid, String field) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().getTwitterAppCard(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<TwitterAppCardJsBean> getTwitterAppCards(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<TwitterAppCardJsBean> jsBeans = new ArrayList<TwitterAppCardJsBean>();
            List<TwitterAppCard> twitterAppCards = getService().getTwitterAppCards(guids);
            if(twitterAppCards != null) {
                for(TwitterAppCard twitterAppCard : twitterAppCards) {
                    jsBeans.add(convertTwitterAppCardToJsBean(twitterAppCard));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<TwitterAppCardJsBean> getAllTwitterAppCards() throws WebException
    {
        return getAllTwitterAppCards(null, null, null);
    }

    public List<TwitterAppCardJsBean> getAllTwitterAppCards(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<TwitterAppCardJsBean> jsBeans = new ArrayList<TwitterAppCardJsBean>();
            List<TwitterAppCard> twitterAppCards = getService().getAllTwitterAppCards(ordering, offset, count);
            if(twitterAppCards != null) {
                for(TwitterAppCard twitterAppCard : twitterAppCards) {
                    jsBeans.add(convertTwitterAppCardToJsBean(twitterAppCard));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> getAllTwitterAppCardKeys(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllTwitterAppCardKeys(ordering, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<TwitterAppCardJsBean> findTwitterAppCards(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findTwitterAppCards(filter, ordering, params, values, null, null, null, null);
    }

    public List<TwitterAppCardJsBean> findTwitterAppCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<TwitterAppCardJsBean> jsBeans = new ArrayList<TwitterAppCardJsBean>();
            List<TwitterAppCard> twitterAppCards = getService().findTwitterAppCards(filter, ordering, params, values, grouping, unique, offset, count);
            if(twitterAppCards != null) {
                for(TwitterAppCard twitterAppCard : twitterAppCards) {
                    jsBeans.add(convertTwitterAppCardToJsBean(twitterAppCard));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> findTwitterAppCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findTwitterAppCardKeys(filter, ordering, params, values, grouping, unique, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        log.finer("BEGIN");

        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createTwitterAppCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, TwitterCardAppInfoJsBean iphoneApp, TwitterCardAppInfoJsBean ipadApp, TwitterCardAppInfoJsBean googlePlayApp) throws WebException
    {
        try {
            return getService().createTwitterAppCard(card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, TwitterCardAppInfoWebUtil.convertTwitterCardAppInfoJsBeanToBean(iphoneApp), TwitterCardAppInfoWebUtil.convertTwitterCardAppInfoJsBeanToBean(ipadApp), TwitterCardAppInfoWebUtil.convertTwitterCardAppInfoJsBeanToBean(googlePlayApp));
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createTwitterAppCard(String jsonStr) throws WebException
    {
        return createTwitterAppCard(TwitterAppCardJsBean.fromJsonString(jsonStr));
    }

    public String createTwitterAppCard(TwitterAppCardJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            TwitterAppCard twitterAppCard = convertTwitterAppCardJsBeanToBean(jsBean);
            return getService().createTwitterAppCard(twitterAppCard);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public TwitterAppCardJsBean constructTwitterAppCard(String jsonStr) throws WebException
    {
        return constructTwitterAppCard(TwitterAppCardJsBean.fromJsonString(jsonStr));
    }

    public TwitterAppCardJsBean constructTwitterAppCard(TwitterAppCardJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            TwitterAppCard twitterAppCard = convertTwitterAppCardJsBeanToBean(jsBean);
            twitterAppCard = getService().constructTwitterAppCard(twitterAppCard);
            jsBean = convertTwitterAppCardToJsBean(twitterAppCard);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateTwitterAppCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, TwitterCardAppInfoJsBean iphoneApp, TwitterCardAppInfoJsBean ipadApp, TwitterCardAppInfoJsBean googlePlayApp) throws WebException
    {
        try {
            return getService().updateTwitterAppCard(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, TwitterCardAppInfoWebUtil.convertTwitterCardAppInfoJsBeanToBean(iphoneApp), TwitterCardAppInfoWebUtil.convertTwitterCardAppInfoJsBeanToBean(ipadApp), TwitterCardAppInfoWebUtil.convertTwitterCardAppInfoJsBeanToBean(googlePlayApp));
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateTwitterAppCard(String jsonStr) throws WebException
    {
        return updateTwitterAppCard(TwitterAppCardJsBean.fromJsonString(jsonStr));
    }

    public Boolean updateTwitterAppCard(TwitterAppCardJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            TwitterAppCard twitterAppCard = convertTwitterAppCardJsBeanToBean(jsBean);
            return getService().updateTwitterAppCard(twitterAppCard);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public TwitterAppCardJsBean refreshTwitterAppCard(String jsonStr) throws WebException
    {
        return refreshTwitterAppCard(TwitterAppCardJsBean.fromJsonString(jsonStr));
    }

    public TwitterAppCardJsBean refreshTwitterAppCard(TwitterAppCardJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            TwitterAppCard twitterAppCard = convertTwitterAppCardJsBeanToBean(jsBean);
            twitterAppCard = getService().refreshTwitterAppCard(twitterAppCard);
            jsBean = convertTwitterAppCardToJsBean(twitterAppCard);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteTwitterAppCard(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteTwitterAppCard(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteTwitterAppCard(TwitterAppCardJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            TwitterAppCard twitterAppCard = convertTwitterAppCardJsBeanToBean(jsBean);
            return getService().deleteTwitterAppCard(twitterAppCard);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteTwitterAppCards(String filter, String params, List<String> values) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteTwitterAppCards(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }


    public static TwitterAppCardJsBean convertTwitterAppCardToJsBean(TwitterAppCard twitterAppCard)
    {
        TwitterAppCardJsBean jsBean = null;
        if(twitterAppCard != null) {
            jsBean = new TwitterAppCardJsBean();
            jsBean.setGuid(twitterAppCard.getGuid());
            jsBean.setCard(twitterAppCard.getCard());
            jsBean.setUrl(twitterAppCard.getUrl());
            jsBean.setTitle(twitterAppCard.getTitle());
            jsBean.setDescription(twitterAppCard.getDescription());
            jsBean.setSite(twitterAppCard.getSite());
            jsBean.setSiteId(twitterAppCard.getSiteId());
            jsBean.setCreator(twitterAppCard.getCreator());
            jsBean.setCreatorId(twitterAppCard.getCreatorId());
            jsBean.setImage(twitterAppCard.getImage());
            jsBean.setImageWidth(twitterAppCard.getImageWidth());
            jsBean.setImageHeight(twitterAppCard.getImageHeight());
            jsBean.setIphoneApp(TwitterCardAppInfoWebUtil.convertTwitterCardAppInfoToJsBean(twitterAppCard.getIphoneApp()));
            jsBean.setIpadApp(TwitterCardAppInfoWebUtil.convertTwitterCardAppInfoToJsBean(twitterAppCard.getIpadApp()));
            jsBean.setGooglePlayApp(TwitterCardAppInfoWebUtil.convertTwitterCardAppInfoToJsBean(twitterAppCard.getGooglePlayApp()));
            jsBean.setCreatedTime(twitterAppCard.getCreatedTime());
            jsBean.setModifiedTime(twitterAppCard.getModifiedTime());
        }
        return jsBean;
    }

    public static TwitterAppCard convertTwitterAppCardJsBeanToBean(TwitterAppCardJsBean jsBean)
    {
        TwitterAppCardBean twitterAppCard = null;
        if(jsBean != null) {
            twitterAppCard = new TwitterAppCardBean();
            twitterAppCard.setGuid(jsBean.getGuid());
            twitterAppCard.setCard(jsBean.getCard());
            twitterAppCard.setUrl(jsBean.getUrl());
            twitterAppCard.setTitle(jsBean.getTitle());
            twitterAppCard.setDescription(jsBean.getDescription());
            twitterAppCard.setSite(jsBean.getSite());
            twitterAppCard.setSiteId(jsBean.getSiteId());
            twitterAppCard.setCreator(jsBean.getCreator());
            twitterAppCard.setCreatorId(jsBean.getCreatorId());
            twitterAppCard.setImage(jsBean.getImage());
            twitterAppCard.setImageWidth(jsBean.getImageWidth());
            twitterAppCard.setImageHeight(jsBean.getImageHeight());
            twitterAppCard.setIphoneApp(TwitterCardAppInfoWebUtil.convertTwitterCardAppInfoJsBeanToBean(jsBean.getIphoneApp()));
            twitterAppCard.setIpadApp(TwitterCardAppInfoWebUtil.convertTwitterCardAppInfoJsBeanToBean(jsBean.getIpadApp()));
            twitterAppCard.setGooglePlayApp(TwitterCardAppInfoWebUtil.convertTwitterCardAppInfoJsBeanToBean(jsBean.getGooglePlayApp()));
            twitterAppCard.setCreatedTime(jsBean.getCreatedTime());
            twitterAppCard.setModifiedTime(jsBean.getModifiedTime());
        }
        return twitterAppCard;
    }

}
