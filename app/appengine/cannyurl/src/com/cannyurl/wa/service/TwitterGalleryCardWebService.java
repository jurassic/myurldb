package com.cannyurl.wa.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.TwitterGalleryCard;
import com.cannyurl.af.bean.TwitterGalleryCardBean;
import com.cannyurl.af.service.TwitterGalleryCardService;
import com.cannyurl.af.service.manager.ServiceManager;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.TwitterCardAppInfoJsBean;
import com.cannyurl.fe.bean.TwitterCardProductDataJsBean;
import com.cannyurl.fe.bean.TwitterGalleryCardJsBean;
import com.cannyurl.wa.util.TwitterCardAppInfoWebUtil;
import com.cannyurl.wa.util.TwitterCardProductDataWebUtil;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class TwitterGalleryCardWebService // implements TwitterGalleryCardService
{
    private static final Logger log = Logger.getLogger(TwitterGalleryCardWebService.class.getName());
     
    // Af service interface.
    private TwitterGalleryCardService mService = null;

    public TwitterGalleryCardWebService()
    {
        this(ServiceManager.getTwitterGalleryCardService());
    }
    public TwitterGalleryCardWebService(TwitterGalleryCardService service)
    {
        mService = service;
    }
    
    private TwitterGalleryCardService getService()
    {
        if(mService == null) {
            mService = ServiceManager.getTwitterGalleryCardService();
        }
        return mService;
    }
    
    
    public TwitterGalleryCardJsBean getTwitterGalleryCard(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            TwitterGalleryCard twitterGalleryCard = getService().getTwitterGalleryCard(guid);
            TwitterGalleryCardJsBean bean = convertTwitterGalleryCardToJsBean(twitterGalleryCard);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getTwitterGalleryCard(String guid, String field) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().getTwitterGalleryCard(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<TwitterGalleryCardJsBean> getTwitterGalleryCards(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<TwitterGalleryCardJsBean> jsBeans = new ArrayList<TwitterGalleryCardJsBean>();
            List<TwitterGalleryCard> twitterGalleryCards = getService().getTwitterGalleryCards(guids);
            if(twitterGalleryCards != null) {
                for(TwitterGalleryCard twitterGalleryCard : twitterGalleryCards) {
                    jsBeans.add(convertTwitterGalleryCardToJsBean(twitterGalleryCard));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<TwitterGalleryCardJsBean> getAllTwitterGalleryCards() throws WebException
    {
        return getAllTwitterGalleryCards(null, null, null);
    }

    public List<TwitterGalleryCardJsBean> getAllTwitterGalleryCards(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<TwitterGalleryCardJsBean> jsBeans = new ArrayList<TwitterGalleryCardJsBean>();
            List<TwitterGalleryCard> twitterGalleryCards = getService().getAllTwitterGalleryCards(ordering, offset, count);
            if(twitterGalleryCards != null) {
                for(TwitterGalleryCard twitterGalleryCard : twitterGalleryCards) {
                    jsBeans.add(convertTwitterGalleryCardToJsBean(twitterGalleryCard));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> getAllTwitterGalleryCardKeys(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllTwitterGalleryCardKeys(ordering, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<TwitterGalleryCardJsBean> findTwitterGalleryCards(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findTwitterGalleryCards(filter, ordering, params, values, null, null, null, null);
    }

    public List<TwitterGalleryCardJsBean> findTwitterGalleryCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<TwitterGalleryCardJsBean> jsBeans = new ArrayList<TwitterGalleryCardJsBean>();
            List<TwitterGalleryCard> twitterGalleryCards = getService().findTwitterGalleryCards(filter, ordering, params, values, grouping, unique, offset, count);
            if(twitterGalleryCards != null) {
                for(TwitterGalleryCard twitterGalleryCard : twitterGalleryCards) {
                    jsBeans.add(convertTwitterGalleryCardToJsBean(twitterGalleryCard));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> findTwitterGalleryCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findTwitterGalleryCardKeys(filter, ordering, params, values, grouping, unique, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        log.finer("BEGIN");

        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createTwitterGalleryCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image0, String image1, String image2, String image3) throws WebException
    {
        try {
            return getService().createTwitterGalleryCard(card, url, title, description, site, siteId, creator, creatorId, image0, image1, image2, image3);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createTwitterGalleryCard(String jsonStr) throws WebException
    {
        return createTwitterGalleryCard(TwitterGalleryCardJsBean.fromJsonString(jsonStr));
    }

    public String createTwitterGalleryCard(TwitterGalleryCardJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            TwitterGalleryCard twitterGalleryCard = convertTwitterGalleryCardJsBeanToBean(jsBean);
            return getService().createTwitterGalleryCard(twitterGalleryCard);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public TwitterGalleryCardJsBean constructTwitterGalleryCard(String jsonStr) throws WebException
    {
        return constructTwitterGalleryCard(TwitterGalleryCardJsBean.fromJsonString(jsonStr));
    }

    public TwitterGalleryCardJsBean constructTwitterGalleryCard(TwitterGalleryCardJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            TwitterGalleryCard twitterGalleryCard = convertTwitterGalleryCardJsBeanToBean(jsBean);
            twitterGalleryCard = getService().constructTwitterGalleryCard(twitterGalleryCard);
            jsBean = convertTwitterGalleryCardToJsBean(twitterGalleryCard);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateTwitterGalleryCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image0, String image1, String image2, String image3) throws WebException
    {
        try {
            return getService().updateTwitterGalleryCard(guid, card, url, title, description, site, siteId, creator, creatorId, image0, image1, image2, image3);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateTwitterGalleryCard(String jsonStr) throws WebException
    {
        return updateTwitterGalleryCard(TwitterGalleryCardJsBean.fromJsonString(jsonStr));
    }

    public Boolean updateTwitterGalleryCard(TwitterGalleryCardJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            TwitterGalleryCard twitterGalleryCard = convertTwitterGalleryCardJsBeanToBean(jsBean);
            return getService().updateTwitterGalleryCard(twitterGalleryCard);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public TwitterGalleryCardJsBean refreshTwitterGalleryCard(String jsonStr) throws WebException
    {
        return refreshTwitterGalleryCard(TwitterGalleryCardJsBean.fromJsonString(jsonStr));
    }

    public TwitterGalleryCardJsBean refreshTwitterGalleryCard(TwitterGalleryCardJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            TwitterGalleryCard twitterGalleryCard = convertTwitterGalleryCardJsBeanToBean(jsBean);
            twitterGalleryCard = getService().refreshTwitterGalleryCard(twitterGalleryCard);
            jsBean = convertTwitterGalleryCardToJsBean(twitterGalleryCard);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteTwitterGalleryCard(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteTwitterGalleryCard(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteTwitterGalleryCard(TwitterGalleryCardJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            TwitterGalleryCard twitterGalleryCard = convertTwitterGalleryCardJsBeanToBean(jsBean);
            return getService().deleteTwitterGalleryCard(twitterGalleryCard);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteTwitterGalleryCards(String filter, String params, List<String> values) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteTwitterGalleryCards(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }


    public static TwitterGalleryCardJsBean convertTwitterGalleryCardToJsBean(TwitterGalleryCard twitterGalleryCard)
    {
        TwitterGalleryCardJsBean jsBean = null;
        if(twitterGalleryCard != null) {
            jsBean = new TwitterGalleryCardJsBean();
            jsBean.setGuid(twitterGalleryCard.getGuid());
            jsBean.setCard(twitterGalleryCard.getCard());
            jsBean.setUrl(twitterGalleryCard.getUrl());
            jsBean.setTitle(twitterGalleryCard.getTitle());
            jsBean.setDescription(twitterGalleryCard.getDescription());
            jsBean.setSite(twitterGalleryCard.getSite());
            jsBean.setSiteId(twitterGalleryCard.getSiteId());
            jsBean.setCreator(twitterGalleryCard.getCreator());
            jsBean.setCreatorId(twitterGalleryCard.getCreatorId());
            jsBean.setImage0(twitterGalleryCard.getImage0());
            jsBean.setImage1(twitterGalleryCard.getImage1());
            jsBean.setImage2(twitterGalleryCard.getImage2());
            jsBean.setImage3(twitterGalleryCard.getImage3());
            jsBean.setCreatedTime(twitterGalleryCard.getCreatedTime());
            jsBean.setModifiedTime(twitterGalleryCard.getModifiedTime());
        }
        return jsBean;
    }

    public static TwitterGalleryCard convertTwitterGalleryCardJsBeanToBean(TwitterGalleryCardJsBean jsBean)
    {
        TwitterGalleryCardBean twitterGalleryCard = null;
        if(jsBean != null) {
            twitterGalleryCard = new TwitterGalleryCardBean();
            twitterGalleryCard.setGuid(jsBean.getGuid());
            twitterGalleryCard.setCard(jsBean.getCard());
            twitterGalleryCard.setUrl(jsBean.getUrl());
            twitterGalleryCard.setTitle(jsBean.getTitle());
            twitterGalleryCard.setDescription(jsBean.getDescription());
            twitterGalleryCard.setSite(jsBean.getSite());
            twitterGalleryCard.setSiteId(jsBean.getSiteId());
            twitterGalleryCard.setCreator(jsBean.getCreator());
            twitterGalleryCard.setCreatorId(jsBean.getCreatorId());
            twitterGalleryCard.setImage0(jsBean.getImage0());
            twitterGalleryCard.setImage1(jsBean.getImage1());
            twitterGalleryCard.setImage2(jsBean.getImage2());
            twitterGalleryCard.setImage3(jsBean.getImage3());
            twitterGalleryCard.setCreatedTime(jsBean.getCreatedTime());
            twitterGalleryCard.setModifiedTime(jsBean.getModifiedTime());
        }
        return twitterGalleryCard;
    }

}
