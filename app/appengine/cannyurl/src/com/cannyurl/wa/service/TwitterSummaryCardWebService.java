package com.cannyurl.wa.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.TwitterSummaryCard;
import com.cannyurl.af.bean.TwitterSummaryCardBean;
import com.cannyurl.af.service.TwitterSummaryCardService;
import com.cannyurl.af.service.manager.ServiceManager;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.TwitterCardAppInfoJsBean;
import com.cannyurl.fe.bean.TwitterCardProductDataJsBean;
import com.cannyurl.fe.bean.TwitterSummaryCardJsBean;
import com.cannyurl.wa.util.TwitterCardAppInfoWebUtil;
import com.cannyurl.wa.util.TwitterCardProductDataWebUtil;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class TwitterSummaryCardWebService // implements TwitterSummaryCardService
{
    private static final Logger log = Logger.getLogger(TwitterSummaryCardWebService.class.getName());
     
    // Af service interface.
    private TwitterSummaryCardService mService = null;

    public TwitterSummaryCardWebService()
    {
        this(ServiceManager.getTwitterSummaryCardService());
    }
    public TwitterSummaryCardWebService(TwitterSummaryCardService service)
    {
        mService = service;
    }
    
    private TwitterSummaryCardService getService()
    {
        if(mService == null) {
            mService = ServiceManager.getTwitterSummaryCardService();
        }
        return mService;
    }
    
    
    public TwitterSummaryCardJsBean getTwitterSummaryCard(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            TwitterSummaryCard twitterSummaryCard = getService().getTwitterSummaryCard(guid);
            TwitterSummaryCardJsBean bean = convertTwitterSummaryCardToJsBean(twitterSummaryCard);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getTwitterSummaryCard(String guid, String field) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().getTwitterSummaryCard(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<TwitterSummaryCardJsBean> getTwitterSummaryCards(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<TwitterSummaryCardJsBean> jsBeans = new ArrayList<TwitterSummaryCardJsBean>();
            List<TwitterSummaryCard> twitterSummaryCards = getService().getTwitterSummaryCards(guids);
            if(twitterSummaryCards != null) {
                for(TwitterSummaryCard twitterSummaryCard : twitterSummaryCards) {
                    jsBeans.add(convertTwitterSummaryCardToJsBean(twitterSummaryCard));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<TwitterSummaryCardJsBean> getAllTwitterSummaryCards() throws WebException
    {
        return getAllTwitterSummaryCards(null, null, null);
    }

    public List<TwitterSummaryCardJsBean> getAllTwitterSummaryCards(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<TwitterSummaryCardJsBean> jsBeans = new ArrayList<TwitterSummaryCardJsBean>();
            List<TwitterSummaryCard> twitterSummaryCards = getService().getAllTwitterSummaryCards(ordering, offset, count);
            if(twitterSummaryCards != null) {
                for(TwitterSummaryCard twitterSummaryCard : twitterSummaryCards) {
                    jsBeans.add(convertTwitterSummaryCardToJsBean(twitterSummaryCard));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> getAllTwitterSummaryCardKeys(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllTwitterSummaryCardKeys(ordering, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<TwitterSummaryCardJsBean> findTwitterSummaryCards(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findTwitterSummaryCards(filter, ordering, params, values, null, null, null, null);
    }

    public List<TwitterSummaryCardJsBean> findTwitterSummaryCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<TwitterSummaryCardJsBean> jsBeans = new ArrayList<TwitterSummaryCardJsBean>();
            List<TwitterSummaryCard> twitterSummaryCards = getService().findTwitterSummaryCards(filter, ordering, params, values, grouping, unique, offset, count);
            if(twitterSummaryCards != null) {
                for(TwitterSummaryCard twitterSummaryCard : twitterSummaryCards) {
                    jsBeans.add(convertTwitterSummaryCardToJsBean(twitterSummaryCard));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> findTwitterSummaryCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findTwitterSummaryCardKeys(filter, ordering, params, values, grouping, unique, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        log.finer("BEGIN");

        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createTwitterSummaryCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight) throws WebException
    {
        try {
            return getService().createTwitterSummaryCard(card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createTwitterSummaryCard(String jsonStr) throws WebException
    {
        return createTwitterSummaryCard(TwitterSummaryCardJsBean.fromJsonString(jsonStr));
    }

    public String createTwitterSummaryCard(TwitterSummaryCardJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            TwitterSummaryCard twitterSummaryCard = convertTwitterSummaryCardJsBeanToBean(jsBean);
            return getService().createTwitterSummaryCard(twitterSummaryCard);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public TwitterSummaryCardJsBean constructTwitterSummaryCard(String jsonStr) throws WebException
    {
        return constructTwitterSummaryCard(TwitterSummaryCardJsBean.fromJsonString(jsonStr));
    }

    public TwitterSummaryCardJsBean constructTwitterSummaryCard(TwitterSummaryCardJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            TwitterSummaryCard twitterSummaryCard = convertTwitterSummaryCardJsBeanToBean(jsBean);
            twitterSummaryCard = getService().constructTwitterSummaryCard(twitterSummaryCard);
            jsBean = convertTwitterSummaryCardToJsBean(twitterSummaryCard);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateTwitterSummaryCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight) throws WebException
    {
        try {
            return getService().updateTwitterSummaryCard(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateTwitterSummaryCard(String jsonStr) throws WebException
    {
        return updateTwitterSummaryCard(TwitterSummaryCardJsBean.fromJsonString(jsonStr));
    }

    public Boolean updateTwitterSummaryCard(TwitterSummaryCardJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            TwitterSummaryCard twitterSummaryCard = convertTwitterSummaryCardJsBeanToBean(jsBean);
            return getService().updateTwitterSummaryCard(twitterSummaryCard);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public TwitterSummaryCardJsBean refreshTwitterSummaryCard(String jsonStr) throws WebException
    {
        return refreshTwitterSummaryCard(TwitterSummaryCardJsBean.fromJsonString(jsonStr));
    }

    public TwitterSummaryCardJsBean refreshTwitterSummaryCard(TwitterSummaryCardJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            TwitterSummaryCard twitterSummaryCard = convertTwitterSummaryCardJsBeanToBean(jsBean);
            twitterSummaryCard = getService().refreshTwitterSummaryCard(twitterSummaryCard);
            jsBean = convertTwitterSummaryCardToJsBean(twitterSummaryCard);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteTwitterSummaryCard(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteTwitterSummaryCard(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteTwitterSummaryCard(TwitterSummaryCardJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            TwitterSummaryCard twitterSummaryCard = convertTwitterSummaryCardJsBeanToBean(jsBean);
            return getService().deleteTwitterSummaryCard(twitterSummaryCard);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteTwitterSummaryCards(String filter, String params, List<String> values) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteTwitterSummaryCards(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }


    public static TwitterSummaryCardJsBean convertTwitterSummaryCardToJsBean(TwitterSummaryCard twitterSummaryCard)
    {
        TwitterSummaryCardJsBean jsBean = null;
        if(twitterSummaryCard != null) {
            jsBean = new TwitterSummaryCardJsBean();
            jsBean.setGuid(twitterSummaryCard.getGuid());
            jsBean.setCard(twitterSummaryCard.getCard());
            jsBean.setUrl(twitterSummaryCard.getUrl());
            jsBean.setTitle(twitterSummaryCard.getTitle());
            jsBean.setDescription(twitterSummaryCard.getDescription());
            jsBean.setSite(twitterSummaryCard.getSite());
            jsBean.setSiteId(twitterSummaryCard.getSiteId());
            jsBean.setCreator(twitterSummaryCard.getCreator());
            jsBean.setCreatorId(twitterSummaryCard.getCreatorId());
            jsBean.setImage(twitterSummaryCard.getImage());
            jsBean.setImageWidth(twitterSummaryCard.getImageWidth());
            jsBean.setImageHeight(twitterSummaryCard.getImageHeight());
            jsBean.setCreatedTime(twitterSummaryCard.getCreatedTime());
            jsBean.setModifiedTime(twitterSummaryCard.getModifiedTime());
        }
        return jsBean;
    }

    public static TwitterSummaryCard convertTwitterSummaryCardJsBeanToBean(TwitterSummaryCardJsBean jsBean)
    {
        TwitterSummaryCardBean twitterSummaryCard = null;
        if(jsBean != null) {
            twitterSummaryCard = new TwitterSummaryCardBean();
            twitterSummaryCard.setGuid(jsBean.getGuid());
            twitterSummaryCard.setCard(jsBean.getCard());
            twitterSummaryCard.setUrl(jsBean.getUrl());
            twitterSummaryCard.setTitle(jsBean.getTitle());
            twitterSummaryCard.setDescription(jsBean.getDescription());
            twitterSummaryCard.setSite(jsBean.getSite());
            twitterSummaryCard.setSiteId(jsBean.getSiteId());
            twitterSummaryCard.setCreator(jsBean.getCreator());
            twitterSummaryCard.setCreatorId(jsBean.getCreatorId());
            twitterSummaryCard.setImage(jsBean.getImage());
            twitterSummaryCard.setImageWidth(jsBean.getImageWidth());
            twitterSummaryCard.setImageHeight(jsBean.getImageHeight());
            twitterSummaryCard.setCreatedTime(jsBean.getCreatedTime());
            twitterSummaryCard.setModifiedTime(jsBean.getModifiedTime());
        }
        return twitterSummaryCard;
    }

}
