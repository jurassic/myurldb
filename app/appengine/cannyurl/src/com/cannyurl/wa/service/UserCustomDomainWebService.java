package com.cannyurl.wa.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.UserCustomDomain;
import com.cannyurl.af.bean.UserCustomDomainBean;
import com.cannyurl.af.service.UserCustomDomainService;
import com.cannyurl.af.service.manager.ServiceManager;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.UserCustomDomainJsBean;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class UserCustomDomainWebService // implements UserCustomDomainService
{
    private static final Logger log = Logger.getLogger(UserCustomDomainWebService.class.getName());
     
    // Af service interface.
    private UserCustomDomainService mService = null;

    public UserCustomDomainWebService()
    {
        this(ServiceManager.getUserCustomDomainService());
    }
    public UserCustomDomainWebService(UserCustomDomainService service)
    {
        mService = service;
    }
    
    private UserCustomDomainService getService()
    {
        if(mService == null) {
            mService = ServiceManager.getUserCustomDomainService();
        }
        return mService;
    }
    
    
    public UserCustomDomainJsBean getUserCustomDomain(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            UserCustomDomain userCustomDomain = getService().getUserCustomDomain(guid);
            UserCustomDomainJsBean bean = convertUserCustomDomainToJsBean(userCustomDomain);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getUserCustomDomain(String guid, String field) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().getUserCustomDomain(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<UserCustomDomainJsBean> getUserCustomDomains(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<UserCustomDomainJsBean> jsBeans = new ArrayList<UserCustomDomainJsBean>();
            List<UserCustomDomain> userCustomDomains = getService().getUserCustomDomains(guids);
            if(userCustomDomains != null) {
                for(UserCustomDomain userCustomDomain : userCustomDomains) {
                    jsBeans.add(convertUserCustomDomainToJsBean(userCustomDomain));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<UserCustomDomainJsBean> getAllUserCustomDomains() throws WebException
    {
        return getAllUserCustomDomains(null, null, null);
    }

    public List<UserCustomDomainJsBean> getAllUserCustomDomains(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<UserCustomDomainJsBean> jsBeans = new ArrayList<UserCustomDomainJsBean>();
            List<UserCustomDomain> userCustomDomains = getService().getAllUserCustomDomains(ordering, offset, count);
            if(userCustomDomains != null) {
                for(UserCustomDomain userCustomDomain : userCustomDomains) {
                    jsBeans.add(convertUserCustomDomainToJsBean(userCustomDomain));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> getAllUserCustomDomainKeys(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllUserCustomDomainKeys(ordering, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<UserCustomDomainJsBean> findUserCustomDomains(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findUserCustomDomains(filter, ordering, params, values, null, null, null, null);
    }

    public List<UserCustomDomainJsBean> findUserCustomDomains(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<UserCustomDomainJsBean> jsBeans = new ArrayList<UserCustomDomainJsBean>();
            List<UserCustomDomain> userCustomDomains = getService().findUserCustomDomains(filter, ordering, params, values, grouping, unique, offset, count);
            if(userCustomDomains != null) {
                for(UserCustomDomain userCustomDomain : userCustomDomains) {
                    jsBeans.add(convertUserCustomDomainToJsBean(userCustomDomain));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> findUserCustomDomainKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findUserCustomDomainKeys(filter, ordering, params, values, grouping, unique, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        log.finer("BEGIN");

        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createUserCustomDomain(String owner, String domain, Boolean verified, String status, Long verifiedTime) throws WebException
    {
        try {
            return getService().createUserCustomDomain(owner, domain, verified, status, verifiedTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createUserCustomDomain(String jsonStr) throws WebException
    {
        return createUserCustomDomain(UserCustomDomainJsBean.fromJsonString(jsonStr));
    }

    public String createUserCustomDomain(UserCustomDomainJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            UserCustomDomain userCustomDomain = convertUserCustomDomainJsBeanToBean(jsBean);
            return getService().createUserCustomDomain(userCustomDomain);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public UserCustomDomainJsBean constructUserCustomDomain(String jsonStr) throws WebException
    {
        return constructUserCustomDomain(UserCustomDomainJsBean.fromJsonString(jsonStr));
    }

    public UserCustomDomainJsBean constructUserCustomDomain(UserCustomDomainJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            UserCustomDomain userCustomDomain = convertUserCustomDomainJsBeanToBean(jsBean);
            userCustomDomain = getService().constructUserCustomDomain(userCustomDomain);
            jsBean = convertUserCustomDomainToJsBean(userCustomDomain);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateUserCustomDomain(String guid, String owner, String domain, Boolean verified, String status, Long verifiedTime) throws WebException
    {
        try {
            return getService().updateUserCustomDomain(guid, owner, domain, verified, status, verifiedTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateUserCustomDomain(String jsonStr) throws WebException
    {
        return updateUserCustomDomain(UserCustomDomainJsBean.fromJsonString(jsonStr));
    }

    public Boolean updateUserCustomDomain(UserCustomDomainJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            UserCustomDomain userCustomDomain = convertUserCustomDomainJsBeanToBean(jsBean);
            return getService().updateUserCustomDomain(userCustomDomain);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public UserCustomDomainJsBean refreshUserCustomDomain(String jsonStr) throws WebException
    {
        return refreshUserCustomDomain(UserCustomDomainJsBean.fromJsonString(jsonStr));
    }

    public UserCustomDomainJsBean refreshUserCustomDomain(UserCustomDomainJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            UserCustomDomain userCustomDomain = convertUserCustomDomainJsBeanToBean(jsBean);
            userCustomDomain = getService().refreshUserCustomDomain(userCustomDomain);
            jsBean = convertUserCustomDomainToJsBean(userCustomDomain);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteUserCustomDomain(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteUserCustomDomain(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteUserCustomDomain(UserCustomDomainJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            UserCustomDomain userCustomDomain = convertUserCustomDomainJsBeanToBean(jsBean);
            return getService().deleteUserCustomDomain(userCustomDomain);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteUserCustomDomains(String filter, String params, List<String> values) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteUserCustomDomains(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }


    public static UserCustomDomainJsBean convertUserCustomDomainToJsBean(UserCustomDomain userCustomDomain)
    {
        UserCustomDomainJsBean jsBean = null;
        if(userCustomDomain != null) {
            jsBean = new UserCustomDomainJsBean();
            jsBean.setGuid(userCustomDomain.getGuid());
            jsBean.setOwner(userCustomDomain.getOwner());
            jsBean.setDomain(userCustomDomain.getDomain());
            jsBean.setVerified(userCustomDomain.isVerified());
            jsBean.setStatus(userCustomDomain.getStatus());
            jsBean.setVerifiedTime(userCustomDomain.getVerifiedTime());
            jsBean.setCreatedTime(userCustomDomain.getCreatedTime());
            jsBean.setModifiedTime(userCustomDomain.getModifiedTime());
        }
        return jsBean;
    }

    public static UserCustomDomain convertUserCustomDomainJsBeanToBean(UserCustomDomainJsBean jsBean)
    {
        UserCustomDomainBean userCustomDomain = null;
        if(jsBean != null) {
            userCustomDomain = new UserCustomDomainBean();
            userCustomDomain.setGuid(jsBean.getGuid());
            userCustomDomain.setOwner(jsBean.getOwner());
            userCustomDomain.setDomain(jsBean.getDomain());
            userCustomDomain.setVerified(jsBean.isVerified());
            userCustomDomain.setStatus(jsBean.getStatus());
            userCustomDomain.setVerifiedTime(jsBean.getVerifiedTime());
            userCustomDomain.setCreatedTime(jsBean.getCreatedTime());
            userCustomDomain.setModifiedTime(jsBean.getModifiedTime());
        }
        return userCustomDomain;
    }

}
