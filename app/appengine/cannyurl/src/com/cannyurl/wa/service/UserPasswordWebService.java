package com.cannyurl.wa.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.HashedPasswordStruct;
import com.myurldb.ws.UserPassword;
import com.cannyurl.af.bean.UserPasswordBean;
import com.cannyurl.af.service.UserPasswordService;
import com.cannyurl.af.service.manager.ServiceManager;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.HashedPasswordStructJsBean;
import com.cannyurl.fe.bean.UserPasswordJsBean;
import com.cannyurl.wa.util.HashedPasswordStructWebUtil;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class UserPasswordWebService // implements UserPasswordService
{
    private static final Logger log = Logger.getLogger(UserPasswordWebService.class.getName());
     
    // Af service interface.
    private UserPasswordService mService = null;

    public UserPasswordWebService()
    {
        this(ServiceManager.getUserPasswordService());
    }
    public UserPasswordWebService(UserPasswordService service)
    {
        mService = service;
    }
    
    private UserPasswordService getService()
    {
        if(mService == null) {
            mService = ServiceManager.getUserPasswordService();
        }
        return mService;
    }
    
    
    public UserPasswordJsBean getUserPassword(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            UserPassword userPassword = getService().getUserPassword(guid);
            UserPasswordJsBean bean = convertUserPasswordToJsBean(userPassword);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getUserPassword(String guid, String field) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().getUserPassword(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<UserPasswordJsBean> getUserPasswords(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<UserPasswordJsBean> jsBeans = new ArrayList<UserPasswordJsBean>();
            List<UserPassword> userPasswords = getService().getUserPasswords(guids);
            if(userPasswords != null) {
                for(UserPassword userPassword : userPasswords) {
                    jsBeans.add(convertUserPasswordToJsBean(userPassword));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<UserPasswordJsBean> getAllUserPasswords() throws WebException
    {
        return getAllUserPasswords(null, null, null);
    }

    public List<UserPasswordJsBean> getAllUserPasswords(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<UserPasswordJsBean> jsBeans = new ArrayList<UserPasswordJsBean>();
            List<UserPassword> userPasswords = getService().getAllUserPasswords(ordering, offset, count);
            if(userPasswords != null) {
                for(UserPassword userPassword : userPasswords) {
                    jsBeans.add(convertUserPasswordToJsBean(userPassword));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> getAllUserPasswordKeys(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllUserPasswordKeys(ordering, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<UserPasswordJsBean> findUserPasswords(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findUserPasswords(filter, ordering, params, values, null, null, null, null);
    }

    public List<UserPasswordJsBean> findUserPasswords(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<UserPasswordJsBean> jsBeans = new ArrayList<UserPasswordJsBean>();
            List<UserPassword> userPasswords = getService().findUserPasswords(filter, ordering, params, values, grouping, unique, offset, count);
            if(userPasswords != null) {
                for(UserPassword userPassword : userPasswords) {
                    jsBeans.add(convertUserPasswordToJsBean(userPassword));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> findUserPasswordKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findUserPasswordKeys(filter, ordering, params, values, grouping, unique, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        log.finer("BEGIN");

        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createUserPassword(String admin, String user, String username, String email, String openId, HashedPasswordStructJsBean password, Boolean resetRequired, String challengeQuestion, String challengeAnswer, String status, Long lastResetTime, Long expirationTime) throws WebException
    {
        try {
            return getService().createUserPassword(admin, user, username, email, openId, HashedPasswordStructWebUtil.convertHashedPasswordStructJsBeanToBean(password), resetRequired, challengeQuestion, challengeAnswer, status, lastResetTime, expirationTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createUserPassword(String jsonStr) throws WebException
    {
        return createUserPassword(UserPasswordJsBean.fromJsonString(jsonStr));
    }

    public String createUserPassword(UserPasswordJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            UserPassword userPassword = convertUserPasswordJsBeanToBean(jsBean);
            return getService().createUserPassword(userPassword);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public UserPasswordJsBean constructUserPassword(String jsonStr) throws WebException
    {
        return constructUserPassword(UserPasswordJsBean.fromJsonString(jsonStr));
    }

    public UserPasswordJsBean constructUserPassword(UserPasswordJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            UserPassword userPassword = convertUserPasswordJsBeanToBean(jsBean);
            userPassword = getService().constructUserPassword(userPassword);
            jsBean = convertUserPasswordToJsBean(userPassword);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateUserPassword(String guid, String admin, String user, String username, String email, String openId, HashedPasswordStructJsBean password, Boolean resetRequired, String challengeQuestion, String challengeAnswer, String status, Long lastResetTime, Long expirationTime) throws WebException
    {
        try {
            return getService().updateUserPassword(guid, admin, user, username, email, openId, HashedPasswordStructWebUtil.convertHashedPasswordStructJsBeanToBean(password), resetRequired, challengeQuestion, challengeAnswer, status, lastResetTime, expirationTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateUserPassword(String jsonStr) throws WebException
    {
        return updateUserPassword(UserPasswordJsBean.fromJsonString(jsonStr));
    }

    public Boolean updateUserPassword(UserPasswordJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            UserPassword userPassword = convertUserPasswordJsBeanToBean(jsBean);
            return getService().updateUserPassword(userPassword);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public UserPasswordJsBean refreshUserPassword(String jsonStr) throws WebException
    {
        return refreshUserPassword(UserPasswordJsBean.fromJsonString(jsonStr));
    }

    public UserPasswordJsBean refreshUserPassword(UserPasswordJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            UserPassword userPassword = convertUserPasswordJsBeanToBean(jsBean);
            userPassword = getService().refreshUserPassword(userPassword);
            jsBean = convertUserPasswordToJsBean(userPassword);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteUserPassword(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteUserPassword(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteUserPassword(UserPasswordJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            UserPassword userPassword = convertUserPasswordJsBeanToBean(jsBean);
            return getService().deleteUserPassword(userPassword);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteUserPasswords(String filter, String params, List<String> values) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteUserPasswords(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }


    public static UserPasswordJsBean convertUserPasswordToJsBean(UserPassword userPassword)
    {
        UserPasswordJsBean jsBean = null;
        if(userPassword != null) {
            jsBean = new UserPasswordJsBean();
            jsBean.setGuid(userPassword.getGuid());
            jsBean.setAdmin(userPassword.getAdmin());
            jsBean.setUser(userPassword.getUser());
            jsBean.setUsername(userPassword.getUsername());
            jsBean.setEmail(userPassword.getEmail());
            jsBean.setOpenId(userPassword.getOpenId());
            jsBean.setPassword(HashedPasswordStructWebUtil.convertHashedPasswordStructToJsBean(userPassword.getPassword()));
            jsBean.setResetRequired(userPassword.isResetRequired());
            jsBean.setChallengeQuestion(userPassword.getChallengeQuestion());
            jsBean.setChallengeAnswer(userPassword.getChallengeAnswer());
            jsBean.setStatus(userPassword.getStatus());
            jsBean.setLastResetTime(userPassword.getLastResetTime());
            jsBean.setExpirationTime(userPassword.getExpirationTime());
            jsBean.setCreatedTime(userPassword.getCreatedTime());
            jsBean.setModifiedTime(userPassword.getModifiedTime());
        }
        return jsBean;
    }

    public static UserPassword convertUserPasswordJsBeanToBean(UserPasswordJsBean jsBean)
    {
        UserPasswordBean userPassword = null;
        if(jsBean != null) {
            userPassword = new UserPasswordBean();
            userPassword.setGuid(jsBean.getGuid());
            userPassword.setAdmin(jsBean.getAdmin());
            userPassword.setUser(jsBean.getUser());
            userPassword.setUsername(jsBean.getUsername());
            userPassword.setEmail(jsBean.getEmail());
            userPassword.setOpenId(jsBean.getOpenId());
            userPassword.setPassword(HashedPasswordStructWebUtil.convertHashedPasswordStructJsBeanToBean(jsBean.getPassword()));
            userPassword.setResetRequired(jsBean.isResetRequired());
            userPassword.setChallengeQuestion(jsBean.getChallengeQuestion());
            userPassword.setChallengeAnswer(jsBean.getChallengeAnswer());
            userPassword.setStatus(jsBean.getStatus());
            userPassword.setLastResetTime(jsBean.getLastResetTime());
            userPassword.setExpirationTime(jsBean.getExpirationTime());
            userPassword.setCreatedTime(jsBean.getCreatedTime());
            userPassword.setModifiedTime(jsBean.getModifiedTime());
        }
        return userPassword;
    }

}
