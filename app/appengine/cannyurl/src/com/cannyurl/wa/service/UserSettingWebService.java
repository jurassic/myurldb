package com.cannyurl.wa.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.UserSetting;
import com.cannyurl.af.bean.UserSettingBean;
import com.cannyurl.af.service.UserSettingService;
import com.cannyurl.af.service.manager.ServiceManager;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.UserSettingJsBean;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class UserSettingWebService // implements UserSettingService
{
    private static final Logger log = Logger.getLogger(UserSettingWebService.class.getName());
     
    // Af service interface.
    private UserSettingService mService = null;

    public UserSettingWebService()
    {
        this(ServiceManager.getUserSettingService());
    }
    public UserSettingWebService(UserSettingService service)
    {
        mService = service;
    }
    
    private UserSettingService getService()
    {
        if(mService == null) {
            mService = ServiceManager.getUserSettingService();
        }
        return mService;
    }
    
    
    public UserSettingJsBean getUserSetting(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            UserSetting userSetting = getService().getUserSetting(guid);
            UserSettingJsBean bean = convertUserSettingToJsBean(userSetting);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getUserSetting(String guid, String field) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().getUserSetting(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<UserSettingJsBean> getUserSettings(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<UserSettingJsBean> jsBeans = new ArrayList<UserSettingJsBean>();
            List<UserSetting> userSettings = getService().getUserSettings(guids);
            if(userSettings != null) {
                for(UserSetting userSetting : userSettings) {
                    jsBeans.add(convertUserSettingToJsBean(userSetting));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<UserSettingJsBean> getAllUserSettings() throws WebException
    {
        return getAllUserSettings(null, null, null);
    }

    public List<UserSettingJsBean> getAllUserSettings(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<UserSettingJsBean> jsBeans = new ArrayList<UserSettingJsBean>();
            List<UserSetting> userSettings = getService().getAllUserSettings(ordering, offset, count);
            if(userSettings != null) {
                for(UserSetting userSetting : userSettings) {
                    jsBeans.add(convertUserSettingToJsBean(userSetting));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> getAllUserSettingKeys(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllUserSettingKeys(ordering, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<UserSettingJsBean> findUserSettings(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findUserSettings(filter, ordering, params, values, null, null, null, null);
    }

    public List<UserSettingJsBean> findUserSettings(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<UserSettingJsBean> jsBeans = new ArrayList<UserSettingJsBean>();
            List<UserSetting> userSettings = getService().findUserSettings(filter, ordering, params, values, grouping, unique, offset, count);
            if(userSettings != null) {
                for(UserSetting userSetting : userSettings) {
                    jsBeans.add(convertUserSettingToJsBean(userSetting));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> findUserSettingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findUserSettingKeys(filter, ordering, params, values, grouping, unique, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        log.finer("BEGIN");

        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createUserSetting(String user, String homePage, String selfBio, String userUrlDomain, String userUrlDomainNormalized, Boolean autoRedirectEnabled, Boolean viewEnabled, Boolean shareEnabled, String defaultDomain, String defaultTokenType, String defaultRedirectType, Long defaultFlashDuration, String defaultAccessType, String defaultViewType, String defaultShareType, String defaultPassphrase, String defaultSignature, Boolean useSignature, String defaultEmblem, Boolean useEmblem, String defaultBackground, Boolean useBackground, String sponsorUrl, String sponsorBanner, String sponsorNote, Boolean useSponsor, String extraParams, Long expirationDuration) throws WebException
    {
        try {
            return getService().createUserSetting(user, homePage, selfBio, userUrlDomain, userUrlDomainNormalized, autoRedirectEnabled, viewEnabled, shareEnabled, defaultDomain, defaultTokenType, defaultRedirectType, defaultFlashDuration, defaultAccessType, defaultViewType, defaultShareType, defaultPassphrase, defaultSignature, useSignature, defaultEmblem, useEmblem, defaultBackground, useBackground, sponsorUrl, sponsorBanner, sponsorNote, useSponsor, extraParams, expirationDuration);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createUserSetting(String jsonStr) throws WebException
    {
        return createUserSetting(UserSettingJsBean.fromJsonString(jsonStr));
    }

    public String createUserSetting(UserSettingJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            UserSetting userSetting = convertUserSettingJsBeanToBean(jsBean);
            return getService().createUserSetting(userSetting);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public UserSettingJsBean constructUserSetting(String jsonStr) throws WebException
    {
        return constructUserSetting(UserSettingJsBean.fromJsonString(jsonStr));
    }

    public UserSettingJsBean constructUserSetting(UserSettingJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            UserSetting userSetting = convertUserSettingJsBeanToBean(jsBean);
            userSetting = getService().constructUserSetting(userSetting);
            jsBean = convertUserSettingToJsBean(userSetting);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateUserSetting(String guid, String user, String homePage, String selfBio, String userUrlDomain, String userUrlDomainNormalized, Boolean autoRedirectEnabled, Boolean viewEnabled, Boolean shareEnabled, String defaultDomain, String defaultTokenType, String defaultRedirectType, Long defaultFlashDuration, String defaultAccessType, String defaultViewType, String defaultShareType, String defaultPassphrase, String defaultSignature, Boolean useSignature, String defaultEmblem, Boolean useEmblem, String defaultBackground, Boolean useBackground, String sponsorUrl, String sponsorBanner, String sponsorNote, Boolean useSponsor, String extraParams, Long expirationDuration) throws WebException
    {
        try {
            return getService().updateUserSetting(guid, user, homePage, selfBio, userUrlDomain, userUrlDomainNormalized, autoRedirectEnabled, viewEnabled, shareEnabled, defaultDomain, defaultTokenType, defaultRedirectType, defaultFlashDuration, defaultAccessType, defaultViewType, defaultShareType, defaultPassphrase, defaultSignature, useSignature, defaultEmblem, useEmblem, defaultBackground, useBackground, sponsorUrl, sponsorBanner, sponsorNote, useSponsor, extraParams, expirationDuration);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateUserSetting(String jsonStr) throws WebException
    {
        return updateUserSetting(UserSettingJsBean.fromJsonString(jsonStr));
    }

    public Boolean updateUserSetting(UserSettingJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            UserSetting userSetting = convertUserSettingJsBeanToBean(jsBean);
            return getService().updateUserSetting(userSetting);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public UserSettingJsBean refreshUserSetting(String jsonStr) throws WebException
    {
        return refreshUserSetting(UserSettingJsBean.fromJsonString(jsonStr));
    }

    public UserSettingJsBean refreshUserSetting(UserSettingJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            UserSetting userSetting = convertUserSettingJsBeanToBean(jsBean);
            userSetting = getService().refreshUserSetting(userSetting);
            jsBean = convertUserSettingToJsBean(userSetting);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteUserSetting(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteUserSetting(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteUserSetting(UserSettingJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            UserSetting userSetting = convertUserSettingJsBeanToBean(jsBean);
            return getService().deleteUserSetting(userSetting);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteUserSettings(String filter, String params, List<String> values) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteUserSettings(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }


    public static UserSettingJsBean convertUserSettingToJsBean(UserSetting userSetting)
    {
        UserSettingJsBean jsBean = null;
        if(userSetting != null) {
            jsBean = new UserSettingJsBean();
            jsBean.setGuid(userSetting.getGuid());
            jsBean.setUser(userSetting.getUser());
            jsBean.setHomePage(userSetting.getHomePage());
            jsBean.setSelfBio(userSetting.getSelfBio());
            jsBean.setUserUrlDomain(userSetting.getUserUrlDomain());
            jsBean.setUserUrlDomainNormalized(userSetting.getUserUrlDomainNormalized());
            jsBean.setAutoRedirectEnabled(userSetting.isAutoRedirectEnabled());
            jsBean.setViewEnabled(userSetting.isViewEnabled());
            jsBean.setShareEnabled(userSetting.isShareEnabled());
            jsBean.setDefaultDomain(userSetting.getDefaultDomain());
            jsBean.setDefaultTokenType(userSetting.getDefaultTokenType());
            jsBean.setDefaultRedirectType(userSetting.getDefaultRedirectType());
            jsBean.setDefaultFlashDuration(userSetting.getDefaultFlashDuration());
            jsBean.setDefaultAccessType(userSetting.getDefaultAccessType());
            jsBean.setDefaultViewType(userSetting.getDefaultViewType());
            jsBean.setDefaultShareType(userSetting.getDefaultShareType());
            jsBean.setDefaultPassphrase(userSetting.getDefaultPassphrase());
            jsBean.setDefaultSignature(userSetting.getDefaultSignature());
            jsBean.setUseSignature(userSetting.isUseSignature());
            jsBean.setDefaultEmblem(userSetting.getDefaultEmblem());
            jsBean.setUseEmblem(userSetting.isUseEmblem());
            jsBean.setDefaultBackground(userSetting.getDefaultBackground());
            jsBean.setUseBackground(userSetting.isUseBackground());
            jsBean.setSponsorUrl(userSetting.getSponsorUrl());
            jsBean.setSponsorBanner(userSetting.getSponsorBanner());
            jsBean.setSponsorNote(userSetting.getSponsorNote());
            jsBean.setUseSponsor(userSetting.isUseSponsor());
            jsBean.setExtraParams(userSetting.getExtraParams());
            jsBean.setExpirationDuration(userSetting.getExpirationDuration());
            jsBean.setCreatedTime(userSetting.getCreatedTime());
            jsBean.setModifiedTime(userSetting.getModifiedTime());
        }
        return jsBean;
    }

    public static UserSetting convertUserSettingJsBeanToBean(UserSettingJsBean jsBean)
    {
        UserSettingBean userSetting = null;
        if(jsBean != null) {
            userSetting = new UserSettingBean();
            userSetting.setGuid(jsBean.getGuid());
            userSetting.setUser(jsBean.getUser());
            userSetting.setHomePage(jsBean.getHomePage());
            userSetting.setSelfBio(jsBean.getSelfBio());
            userSetting.setUserUrlDomain(jsBean.getUserUrlDomain());
            userSetting.setUserUrlDomainNormalized(jsBean.getUserUrlDomainNormalized());
            userSetting.setAutoRedirectEnabled(jsBean.isAutoRedirectEnabled());
            userSetting.setViewEnabled(jsBean.isViewEnabled());
            userSetting.setShareEnabled(jsBean.isShareEnabled());
            userSetting.setDefaultDomain(jsBean.getDefaultDomain());
            userSetting.setDefaultTokenType(jsBean.getDefaultTokenType());
            userSetting.setDefaultRedirectType(jsBean.getDefaultRedirectType());
            userSetting.setDefaultFlashDuration(jsBean.getDefaultFlashDuration());
            userSetting.setDefaultAccessType(jsBean.getDefaultAccessType());
            userSetting.setDefaultViewType(jsBean.getDefaultViewType());
            userSetting.setDefaultShareType(jsBean.getDefaultShareType());
            userSetting.setDefaultPassphrase(jsBean.getDefaultPassphrase());
            userSetting.setDefaultSignature(jsBean.getDefaultSignature());
            userSetting.setUseSignature(jsBean.isUseSignature());
            userSetting.setDefaultEmblem(jsBean.getDefaultEmblem());
            userSetting.setUseEmblem(jsBean.isUseEmblem());
            userSetting.setDefaultBackground(jsBean.getDefaultBackground());
            userSetting.setUseBackground(jsBean.isUseBackground());
            userSetting.setSponsorUrl(jsBean.getSponsorUrl());
            userSetting.setSponsorBanner(jsBean.getSponsorBanner());
            userSetting.setSponsorNote(jsBean.getSponsorNote());
            userSetting.setUseSponsor(jsBean.isUseSponsor());
            userSetting.setExtraParams(jsBean.getExtraParams());
            userSetting.setExpirationDuration(jsBean.getExpirationDuration());
            userSetting.setCreatedTime(jsBean.getCreatedTime());
            userSetting.setModifiedTime(jsBean.getModifiedTime());
        }
        return userSetting;
    }

}
