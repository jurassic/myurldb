package com.cannyurl.wa.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.GeoPointStruct;
import com.myurldb.ws.StreetAddressStruct;
import com.myurldb.ws.GaeAppStruct;
import com.myurldb.ws.FullNameStruct;
import com.myurldb.ws.GaeUserStruct;
import com.myurldb.ws.User;
import com.cannyurl.af.bean.UserBean;
import com.cannyurl.af.service.UserService;
import com.cannyurl.af.service.manager.ServiceManager;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.GeoPointStructJsBean;
import com.cannyurl.fe.bean.StreetAddressStructJsBean;
import com.cannyurl.fe.bean.GaeAppStructJsBean;
import com.cannyurl.fe.bean.FullNameStructJsBean;
import com.cannyurl.fe.bean.GaeUserStructJsBean;
import com.cannyurl.fe.bean.UserJsBean;
import com.cannyurl.wa.util.GeoPointStructWebUtil;
import com.cannyurl.wa.util.StreetAddressStructWebUtil;
import com.cannyurl.wa.util.GaeAppStructWebUtil;
import com.cannyurl.wa.util.FullNameStructWebUtil;
import com.cannyurl.wa.util.GaeUserStructWebUtil;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class UserWebService // implements UserService
{
    private static final Logger log = Logger.getLogger(UserWebService.class.getName());
     
    // Af service interface.
    private UserService mService = null;

    public UserWebService()
    {
        this(ServiceManager.getUserService());
    }
    public UserWebService(UserService service)
    {
        mService = service;
    }
    
    private UserService getService()
    {
        if(mService == null) {
            mService = ServiceManager.getUserService();
        }
        return mService;
    }
    
    
    public UserJsBean getUser(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            User user = getService().getUser(guid);
            UserJsBean bean = convertUserToJsBean(user);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getUser(String guid, String field) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().getUser(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<UserJsBean> getUsers(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<UserJsBean> jsBeans = new ArrayList<UserJsBean>();
            List<User> users = getService().getUsers(guids);
            if(users != null) {
                for(User user : users) {
                    jsBeans.add(convertUserToJsBean(user));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<UserJsBean> getAllUsers() throws WebException
    {
        return getAllUsers(null, null, null);
    }

    public List<UserJsBean> getAllUsers(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<UserJsBean> jsBeans = new ArrayList<UserJsBean>();
            List<User> users = getService().getAllUsers(ordering, offset, count);
            if(users != null) {
                for(User user : users) {
                    jsBeans.add(convertUserToJsBean(user));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> getAllUserKeys(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllUserKeys(ordering, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<UserJsBean> findUsers(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findUsers(filter, ordering, params, values, null, null, null, null);
    }

    public List<UserJsBean> findUsers(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<UserJsBean> jsBeans = new ArrayList<UserJsBean>();
            List<User> users = getService().findUsers(filter, ordering, params, values, grouping, unique, offset, count);
            if(users != null) {
                for(User user : users) {
                    jsBeans.add(convertUserToJsBean(user));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> findUserKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findUserKeys(filter, ordering, params, values, grouping, unique, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        log.finer("BEGIN");

        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createUser(String managerApp, Long appAcl, GaeAppStructJsBean gaeApp, String aeryId, String sessionId, String ancestorGuid, FullNameStructJsBean name, String usercode, String username, String nickname, String avatar, String email, String openId, GaeUserStructJsBean gaeUser, String entityType, Boolean surrogate, Boolean obsolete, String timeZone, String location, StreetAddressStructJsBean streetAddress, GeoPointStructJsBean geoPoint, String ipAddress, String referer, String status, Long emailVerifiedTime, Long openIdVerifiedTime, Long authenticatedTime) throws WebException
    {
        try {
            return getService().createUser(managerApp, appAcl, GaeAppStructWebUtil.convertGaeAppStructJsBeanToBean(gaeApp), aeryId, sessionId, ancestorGuid, FullNameStructWebUtil.convertFullNameStructJsBeanToBean(name), usercode, username, nickname, avatar, email, openId, GaeUserStructWebUtil.convertGaeUserStructJsBeanToBean(gaeUser), entityType, surrogate, obsolete, timeZone, location, StreetAddressStructWebUtil.convertStreetAddressStructJsBeanToBean(streetAddress), GeoPointStructWebUtil.convertGeoPointStructJsBeanToBean(geoPoint), ipAddress, referer, status, emailVerifiedTime, openIdVerifiedTime, authenticatedTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createUser(String jsonStr) throws WebException
    {
        return createUser(UserJsBean.fromJsonString(jsonStr));
    }

    public String createUser(UserJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            User user = convertUserJsBeanToBean(jsBean);
            return getService().createUser(user);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public UserJsBean constructUser(String jsonStr) throws WebException
    {
        return constructUser(UserJsBean.fromJsonString(jsonStr));
    }

    public UserJsBean constructUser(UserJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            User user = convertUserJsBeanToBean(jsBean);
            user = getService().constructUser(user);
            jsBean = convertUserToJsBean(user);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateUser(String guid, String managerApp, Long appAcl, GaeAppStructJsBean gaeApp, String aeryId, String sessionId, String ancestorGuid, FullNameStructJsBean name, String usercode, String username, String nickname, String avatar, String email, String openId, GaeUserStructJsBean gaeUser, String entityType, Boolean surrogate, Boolean obsolete, String timeZone, String location, StreetAddressStructJsBean streetAddress, GeoPointStructJsBean geoPoint, String ipAddress, String referer, String status, Long emailVerifiedTime, Long openIdVerifiedTime, Long authenticatedTime) throws WebException
    {
        try {
            return getService().updateUser(guid, managerApp, appAcl, GaeAppStructWebUtil.convertGaeAppStructJsBeanToBean(gaeApp), aeryId, sessionId, ancestorGuid, FullNameStructWebUtil.convertFullNameStructJsBeanToBean(name), usercode, username, nickname, avatar, email, openId, GaeUserStructWebUtil.convertGaeUserStructJsBeanToBean(gaeUser), entityType, surrogate, obsolete, timeZone, location, StreetAddressStructWebUtil.convertStreetAddressStructJsBeanToBean(streetAddress), GeoPointStructWebUtil.convertGeoPointStructJsBeanToBean(geoPoint), ipAddress, referer, status, emailVerifiedTime, openIdVerifiedTime, authenticatedTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateUser(String jsonStr) throws WebException
    {
        return updateUser(UserJsBean.fromJsonString(jsonStr));
    }

    public Boolean updateUser(UserJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            User user = convertUserJsBeanToBean(jsBean);
            return getService().updateUser(user);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public UserJsBean refreshUser(String jsonStr) throws WebException
    {
        return refreshUser(UserJsBean.fromJsonString(jsonStr));
    }

    public UserJsBean refreshUser(UserJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            User user = convertUserJsBeanToBean(jsBean);
            user = getService().refreshUser(user);
            jsBean = convertUserToJsBean(user);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteUser(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteUser(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteUser(UserJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            User user = convertUserJsBeanToBean(jsBean);
            return getService().deleteUser(user);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteUsers(String filter, String params, List<String> values) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteUsers(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }


    public static UserJsBean convertUserToJsBean(User user)
    {
        UserJsBean jsBean = null;
        if(user != null) {
            jsBean = new UserJsBean();
            jsBean.setGuid(user.getGuid());
            jsBean.setManagerApp(user.getManagerApp());
            jsBean.setAppAcl(user.getAppAcl());
            jsBean.setGaeApp(GaeAppStructWebUtil.convertGaeAppStructToJsBean(user.getGaeApp()));
            jsBean.setAeryId(user.getAeryId());
            jsBean.setSessionId(user.getSessionId());
            jsBean.setAncestorGuid(user.getAncestorGuid());
            jsBean.setName(FullNameStructWebUtil.convertFullNameStructToJsBean(user.getName()));
            jsBean.setUsercode(user.getUsercode());
            jsBean.setUsername(user.getUsername());
            jsBean.setNickname(user.getNickname());
            jsBean.setAvatar(user.getAvatar());
            jsBean.setEmail(user.getEmail());
            jsBean.setOpenId(user.getOpenId());
            jsBean.setGaeUser(GaeUserStructWebUtil.convertGaeUserStructToJsBean(user.getGaeUser()));
            jsBean.setEntityType(user.getEntityType());
            jsBean.setSurrogate(user.isSurrogate());
            jsBean.setObsolete(user.isObsolete());
            jsBean.setTimeZone(user.getTimeZone());
            jsBean.setLocation(user.getLocation());
            jsBean.setStreetAddress(StreetAddressStructWebUtil.convertStreetAddressStructToJsBean(user.getStreetAddress()));
            jsBean.setGeoPoint(GeoPointStructWebUtil.convertGeoPointStructToJsBean(user.getGeoPoint()));
            jsBean.setIpAddress(user.getIpAddress());
            jsBean.setReferer(user.getReferer());
            jsBean.setStatus(user.getStatus());
            jsBean.setEmailVerifiedTime(user.getEmailVerifiedTime());
            jsBean.setOpenIdVerifiedTime(user.getOpenIdVerifiedTime());
            jsBean.setAuthenticatedTime(user.getAuthenticatedTime());
            jsBean.setCreatedTime(user.getCreatedTime());
            jsBean.setModifiedTime(user.getModifiedTime());
        }
        return jsBean;
    }

    public static User convertUserJsBeanToBean(UserJsBean jsBean)
    {
        UserBean user = null;
        if(jsBean != null) {
            user = new UserBean();
            user.setGuid(jsBean.getGuid());
            user.setManagerApp(jsBean.getManagerApp());
            user.setAppAcl(jsBean.getAppAcl());
            user.setGaeApp(GaeAppStructWebUtil.convertGaeAppStructJsBeanToBean(jsBean.getGaeApp()));
            user.setAeryId(jsBean.getAeryId());
            user.setSessionId(jsBean.getSessionId());
            user.setAncestorGuid(jsBean.getAncestorGuid());
            user.setName(FullNameStructWebUtil.convertFullNameStructJsBeanToBean(jsBean.getName()));
            user.setUsercode(jsBean.getUsercode());
            user.setUsername(jsBean.getUsername());
            user.setNickname(jsBean.getNickname());
            user.setAvatar(jsBean.getAvatar());
            user.setEmail(jsBean.getEmail());
            user.setOpenId(jsBean.getOpenId());
            user.setGaeUser(GaeUserStructWebUtil.convertGaeUserStructJsBeanToBean(jsBean.getGaeUser()));
            user.setEntityType(jsBean.getEntityType());
            user.setSurrogate(jsBean.isSurrogate());
            user.setObsolete(jsBean.isObsolete());
            user.setTimeZone(jsBean.getTimeZone());
            user.setLocation(jsBean.getLocation());
            user.setStreetAddress(StreetAddressStructWebUtil.convertStreetAddressStructJsBeanToBean(jsBean.getStreetAddress()));
            user.setGeoPoint(GeoPointStructWebUtil.convertGeoPointStructJsBeanToBean(jsBean.getGeoPoint()));
            user.setIpAddress(jsBean.getIpAddress());
            user.setReferer(jsBean.getReferer());
            user.setStatus(jsBean.getStatus());
            user.setEmailVerifiedTime(jsBean.getEmailVerifiedTime());
            user.setOpenIdVerifiedTime(jsBean.getOpenIdVerifiedTime());
            user.setAuthenticatedTime(jsBean.getAuthenticatedTime());
            user.setCreatedTime(jsBean.getCreatedTime());
            user.setModifiedTime(jsBean.getModifiedTime());
        }
        return user;
    }

}
