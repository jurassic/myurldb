package com.cannyurl.wa.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.VisitorSetting;
import com.cannyurl.af.bean.VisitorSettingBean;
import com.cannyurl.af.service.VisitorSettingService;
import com.cannyurl.af.service.manager.ServiceManager;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.VisitorSettingJsBean;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class VisitorSettingWebService // implements VisitorSettingService
{
    private static final Logger log = Logger.getLogger(VisitorSettingWebService.class.getName());
     
    // Af service interface.
    private VisitorSettingService mService = null;

    public VisitorSettingWebService()
    {
        this(ServiceManager.getVisitorSettingService());
    }
    public VisitorSettingWebService(VisitorSettingService service)
    {
        mService = service;
    }
    
    private VisitorSettingService getService()
    {
        if(mService == null) {
            mService = ServiceManager.getVisitorSettingService();
        }
        return mService;
    }
    
    
    public VisitorSettingJsBean getVisitorSetting(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            VisitorSetting visitorSetting = getService().getVisitorSetting(guid);
            VisitorSettingJsBean bean = convertVisitorSettingToJsBean(visitorSetting);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getVisitorSetting(String guid, String field) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().getVisitorSetting(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<VisitorSettingJsBean> getVisitorSettings(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<VisitorSettingJsBean> jsBeans = new ArrayList<VisitorSettingJsBean>();
            List<VisitorSetting> visitorSettings = getService().getVisitorSettings(guids);
            if(visitorSettings != null) {
                for(VisitorSetting visitorSetting : visitorSettings) {
                    jsBeans.add(convertVisitorSettingToJsBean(visitorSetting));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<VisitorSettingJsBean> getAllVisitorSettings() throws WebException
    {
        return getAllVisitorSettings(null, null, null);
    }

    public List<VisitorSettingJsBean> getAllVisitorSettings(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<VisitorSettingJsBean> jsBeans = new ArrayList<VisitorSettingJsBean>();
            List<VisitorSetting> visitorSettings = getService().getAllVisitorSettings(ordering, offset, count);
            if(visitorSettings != null) {
                for(VisitorSetting visitorSetting : visitorSettings) {
                    jsBeans.add(convertVisitorSettingToJsBean(visitorSetting));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> getAllVisitorSettingKeys(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllVisitorSettingKeys(ordering, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<VisitorSettingJsBean> findVisitorSettings(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findVisitorSettings(filter, ordering, params, values, null, null, null, null);
    }

    public List<VisitorSettingJsBean> findVisitorSettings(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<VisitorSettingJsBean> jsBeans = new ArrayList<VisitorSettingJsBean>();
            List<VisitorSetting> visitorSettings = getService().findVisitorSettings(filter, ordering, params, values, grouping, unique, offset, count);
            if(visitorSettings != null) {
                for(VisitorSetting visitorSetting : visitorSettings) {
                    jsBeans.add(convertVisitorSettingToJsBean(visitorSetting));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> findVisitorSettingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findVisitorSettingKeys(filter, ordering, params, values, grouping, unique, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        log.finer("BEGIN");

        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createVisitorSetting(String user, String redirectType, Long flashDuration, String privacyType) throws WebException
    {
        try {
            return getService().createVisitorSetting(user, redirectType, flashDuration, privacyType);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createVisitorSetting(String jsonStr) throws WebException
    {
        return createVisitorSetting(VisitorSettingJsBean.fromJsonString(jsonStr));
    }

    public String createVisitorSetting(VisitorSettingJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            VisitorSetting visitorSetting = convertVisitorSettingJsBeanToBean(jsBean);
            return getService().createVisitorSetting(visitorSetting);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public VisitorSettingJsBean constructVisitorSetting(String jsonStr) throws WebException
    {
        return constructVisitorSetting(VisitorSettingJsBean.fromJsonString(jsonStr));
    }

    public VisitorSettingJsBean constructVisitorSetting(VisitorSettingJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            VisitorSetting visitorSetting = convertVisitorSettingJsBeanToBean(jsBean);
            visitorSetting = getService().constructVisitorSetting(visitorSetting);
            jsBean = convertVisitorSettingToJsBean(visitorSetting);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateVisitorSetting(String guid, String user, String redirectType, Long flashDuration, String privacyType) throws WebException
    {
        try {
            return getService().updateVisitorSetting(guid, user, redirectType, flashDuration, privacyType);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateVisitorSetting(String jsonStr) throws WebException
    {
        return updateVisitorSetting(VisitorSettingJsBean.fromJsonString(jsonStr));
    }

    public Boolean updateVisitorSetting(VisitorSettingJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            VisitorSetting visitorSetting = convertVisitorSettingJsBeanToBean(jsBean);
            return getService().updateVisitorSetting(visitorSetting);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public VisitorSettingJsBean refreshVisitorSetting(String jsonStr) throws WebException
    {
        return refreshVisitorSetting(VisitorSettingJsBean.fromJsonString(jsonStr));
    }

    public VisitorSettingJsBean refreshVisitorSetting(VisitorSettingJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            VisitorSetting visitorSetting = convertVisitorSettingJsBeanToBean(jsBean);
            visitorSetting = getService().refreshVisitorSetting(visitorSetting);
            jsBean = convertVisitorSettingToJsBean(visitorSetting);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteVisitorSetting(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteVisitorSetting(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteVisitorSetting(VisitorSettingJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            VisitorSetting visitorSetting = convertVisitorSettingJsBeanToBean(jsBean);
            return getService().deleteVisitorSetting(visitorSetting);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteVisitorSettings(String filter, String params, List<String> values) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteVisitorSettings(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }


    public static VisitorSettingJsBean convertVisitorSettingToJsBean(VisitorSetting visitorSetting)
    {
        VisitorSettingJsBean jsBean = null;
        if(visitorSetting != null) {
            jsBean = new VisitorSettingJsBean();
            jsBean.setGuid(visitorSetting.getGuid());
            jsBean.setUser(visitorSetting.getUser());
            jsBean.setRedirectType(visitorSetting.getRedirectType());
            jsBean.setFlashDuration(visitorSetting.getFlashDuration());
            jsBean.setPrivacyType(visitorSetting.getPrivacyType());
            jsBean.setCreatedTime(visitorSetting.getCreatedTime());
            jsBean.setModifiedTime(visitorSetting.getModifiedTime());
        }
        return jsBean;
    }

    public static VisitorSetting convertVisitorSettingJsBeanToBean(VisitorSettingJsBean jsBean)
    {
        VisitorSettingBean visitorSetting = null;
        if(jsBean != null) {
            visitorSetting = new VisitorSettingBean();
            visitorSetting.setGuid(jsBean.getGuid());
            visitorSetting.setUser(jsBean.getUser());
            visitorSetting.setRedirectType(jsBean.getRedirectType());
            visitorSetting.setFlashDuration(jsBean.getFlashDuration());
            visitorSetting.setPrivacyType(jsBean.getPrivacyType());
            visitorSetting.setCreatedTime(jsBean.getCreatedTime());
            visitorSetting.setModifiedTime(jsBean.getModifiedTime());
        }
        return visitorSetting;
    }

}
