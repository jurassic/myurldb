package com.cannyurl.wa.util;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.AppBrandStruct;
import com.cannyurl.af.bean.AppBrandStructBean;
import com.cannyurl.fe.bean.AppBrandStructJsBean;


public class AppBrandStructWebUtil
{
    private static final Logger log = Logger.getLogger(AppBrandStructWebUtil.class.getName());

    // Static methods only.
    private AppBrandStructWebUtil() {}
    

    public static AppBrandStructJsBean convertAppBrandStructToJsBean(AppBrandStruct appBrandStruct)
    {
        AppBrandStructJsBean jsBean = null;
        if(appBrandStruct != null) {
            jsBean = new AppBrandStructJsBean();
            jsBean.setBrand(appBrandStruct.getBrand());
            jsBean.setName(appBrandStruct.getName());
            jsBean.setDescription(appBrandStruct.getDescription());
        }
        return jsBean;
    }

    public static AppBrandStruct convertAppBrandStructJsBeanToBean(AppBrandStructJsBean jsBean)
    {
        AppBrandStructBean appBrandStruct = null;
        if(jsBean != null) {
            appBrandStruct = new AppBrandStructBean();
            appBrandStruct.setBrand(jsBean.getBrand());
            appBrandStruct.setName(jsBean.getName());
            appBrandStruct.setDescription(jsBean.getDescription());
        }
        return appBrandStruct;
    }

}
