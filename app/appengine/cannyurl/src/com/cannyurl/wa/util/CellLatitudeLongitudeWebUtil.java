package com.cannyurl.wa.util;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.CellLatitudeLongitude;
import com.cannyurl.af.bean.CellLatitudeLongitudeBean;
import com.cannyurl.fe.bean.CellLatitudeLongitudeJsBean;


public class CellLatitudeLongitudeWebUtil
{
    private static final Logger log = Logger.getLogger(CellLatitudeLongitudeWebUtil.class.getName());

    // Static methods only.
    private CellLatitudeLongitudeWebUtil() {}
    

    public static CellLatitudeLongitudeJsBean convertCellLatitudeLongitudeToJsBean(CellLatitudeLongitude cellLatitudeLongitude)
    {
        CellLatitudeLongitudeJsBean jsBean = null;
        if(cellLatitudeLongitude != null) {
            jsBean = new CellLatitudeLongitudeJsBean();
            jsBean.setScale(cellLatitudeLongitude.getScale());
            jsBean.setLatitude(cellLatitudeLongitude.getLatitude());
            jsBean.setLongitude(cellLatitudeLongitude.getLongitude());
        }
        return jsBean;
    }

    public static CellLatitudeLongitude convertCellLatitudeLongitudeJsBeanToBean(CellLatitudeLongitudeJsBean jsBean)
    {
        CellLatitudeLongitudeBean cellLatitudeLongitude = null;
        if(jsBean != null) {
            cellLatitudeLongitude = new CellLatitudeLongitudeBean();
            cellLatitudeLongitude.setScale(jsBean.getScale());
            cellLatitudeLongitude.setLatitude(jsBean.getLatitude());
            cellLatitudeLongitude.setLongitude(jsBean.getLongitude());
        }
        return cellLatitudeLongitude;
    }

}
