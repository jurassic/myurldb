package com.cannyurl.wa.util;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.ContactInfoStruct;
import com.cannyurl.af.bean.ContactInfoStructBean;
import com.cannyurl.fe.bean.ContactInfoStructJsBean;


public class ContactInfoStructWebUtil
{
    private static final Logger log = Logger.getLogger(ContactInfoStructWebUtil.class.getName());

    // Static methods only.
    private ContactInfoStructWebUtil() {}
    

    public static ContactInfoStructJsBean convertContactInfoStructToJsBean(ContactInfoStruct contactInfoStruct)
    {
        ContactInfoStructJsBean jsBean = null;
        if(contactInfoStruct != null) {
            jsBean = new ContactInfoStructJsBean();
            jsBean.setUuid(contactInfoStruct.getUuid());
            jsBean.setStreetAddress(contactInfoStruct.getStreetAddress());
            jsBean.setLocality(contactInfoStruct.getLocality());
            jsBean.setRegion(contactInfoStruct.getRegion());
            jsBean.setPostalCode(contactInfoStruct.getPostalCode());
            jsBean.setCountryName(contactInfoStruct.getCountryName());
            jsBean.setEmailAddress(contactInfoStruct.getEmailAddress());
            jsBean.setPhoneNumber(contactInfoStruct.getPhoneNumber());
            jsBean.setFaxNumber(contactInfoStruct.getFaxNumber());
            jsBean.setWebsite(contactInfoStruct.getWebsite());
            jsBean.setNote(contactInfoStruct.getNote());
        }
        return jsBean;
    }

    public static ContactInfoStruct convertContactInfoStructJsBeanToBean(ContactInfoStructJsBean jsBean)
    {
        ContactInfoStructBean contactInfoStruct = null;
        if(jsBean != null) {
            contactInfoStruct = new ContactInfoStructBean();
            contactInfoStruct.setUuid(jsBean.getUuid());
            contactInfoStruct.setStreetAddress(jsBean.getStreetAddress());
            contactInfoStruct.setLocality(jsBean.getLocality());
            contactInfoStruct.setRegion(jsBean.getRegion());
            contactInfoStruct.setPostalCode(jsBean.getPostalCode());
            contactInfoStruct.setCountryName(jsBean.getCountryName());
            contactInfoStruct.setEmailAddress(jsBean.getEmailAddress());
            contactInfoStruct.setPhoneNumber(jsBean.getPhoneNumber());
            contactInfoStruct.setFaxNumber(jsBean.getFaxNumber());
            contactInfoStruct.setWebsite(jsBean.getWebsite());
            contactInfoStruct.setNote(jsBean.getNote());
        }
        return contactInfoStruct;
    }

}
