package com.cannyurl.wa.util;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.ExternalUserIdStruct;
import com.cannyurl.af.bean.ExternalUserIdStructBean;
import com.cannyurl.fe.bean.ExternalUserIdStructJsBean;


public class ExternalUserIdStructWebUtil
{
    private static final Logger log = Logger.getLogger(ExternalUserIdStructWebUtil.class.getName());

    // Static methods only.
    private ExternalUserIdStructWebUtil() {}
    

    public static ExternalUserIdStructJsBean convertExternalUserIdStructToJsBean(ExternalUserIdStruct externalUserIdStruct)
    {
        ExternalUserIdStructJsBean jsBean = null;
        if(externalUserIdStruct != null) {
            jsBean = new ExternalUserIdStructJsBean();
            jsBean.setUuid(externalUserIdStruct.getUuid());
            jsBean.setId(externalUserIdStruct.getId());
            jsBean.setName(externalUserIdStruct.getName());
            jsBean.setEmail(externalUserIdStruct.getEmail());
            jsBean.setUsername(externalUserIdStruct.getUsername());
            jsBean.setOpenId(externalUserIdStruct.getOpenId());
            jsBean.setNote(externalUserIdStruct.getNote());
        }
        return jsBean;
    }

    public static ExternalUserIdStruct convertExternalUserIdStructJsBeanToBean(ExternalUserIdStructJsBean jsBean)
    {
        ExternalUserIdStructBean externalUserIdStruct = null;
        if(jsBean != null) {
            externalUserIdStruct = new ExternalUserIdStructBean();
            externalUserIdStruct.setUuid(jsBean.getUuid());
            externalUserIdStruct.setId(jsBean.getId());
            externalUserIdStruct.setName(jsBean.getName());
            externalUserIdStruct.setEmail(jsBean.getEmail());
            externalUserIdStruct.setUsername(jsBean.getUsername());
            externalUserIdStruct.setOpenId(jsBean.getOpenId());
            externalUserIdStruct.setNote(jsBean.getNote());
        }
        return externalUserIdStruct;
    }

}
