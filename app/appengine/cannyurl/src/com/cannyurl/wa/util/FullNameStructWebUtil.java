package com.cannyurl.wa.util;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.FullNameStruct;
import com.cannyurl.af.bean.FullNameStructBean;
import com.cannyurl.fe.bean.FullNameStructJsBean;


public class FullNameStructWebUtil
{
    private static final Logger log = Logger.getLogger(FullNameStructWebUtil.class.getName());

    // Static methods only.
    private FullNameStructWebUtil() {}
    

    public static FullNameStructJsBean convertFullNameStructToJsBean(FullNameStruct fullNameStruct)
    {
        FullNameStructJsBean jsBean = null;
        if(fullNameStruct != null) {
            jsBean = new FullNameStructJsBean();
            jsBean.setUuid(fullNameStruct.getUuid());
            jsBean.setDisplayName(fullNameStruct.getDisplayName());
            jsBean.setLastName(fullNameStruct.getLastName());
            jsBean.setFirstName(fullNameStruct.getFirstName());
            jsBean.setMiddleName1(fullNameStruct.getMiddleName1());
            jsBean.setMiddleName2(fullNameStruct.getMiddleName2());
            jsBean.setMiddleInitial(fullNameStruct.getMiddleInitial());
            jsBean.setSalutation(fullNameStruct.getSalutation());
            jsBean.setSuffix(fullNameStruct.getSuffix());
            jsBean.setNote(fullNameStruct.getNote());
        }
        return jsBean;
    }

    public static FullNameStruct convertFullNameStructJsBeanToBean(FullNameStructJsBean jsBean)
    {
        FullNameStructBean fullNameStruct = null;
        if(jsBean != null) {
            fullNameStruct = new FullNameStructBean();
            fullNameStruct.setUuid(jsBean.getUuid());
            fullNameStruct.setDisplayName(jsBean.getDisplayName());
            fullNameStruct.setLastName(jsBean.getLastName());
            fullNameStruct.setFirstName(jsBean.getFirstName());
            fullNameStruct.setMiddleName1(jsBean.getMiddleName1());
            fullNameStruct.setMiddleName2(jsBean.getMiddleName2());
            fullNameStruct.setMiddleInitial(jsBean.getMiddleInitial());
            fullNameStruct.setSalutation(jsBean.getSalutation());
            fullNameStruct.setSuffix(jsBean.getSuffix());
            fullNameStruct.setNote(jsBean.getNote());
        }
        return fullNameStruct;
    }

}
