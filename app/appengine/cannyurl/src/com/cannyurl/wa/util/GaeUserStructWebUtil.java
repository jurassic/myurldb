package com.cannyurl.wa.util;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.GaeUserStruct;
import com.cannyurl.af.bean.GaeUserStructBean;
import com.cannyurl.fe.bean.GaeUserStructJsBean;


public class GaeUserStructWebUtil
{
    private static final Logger log = Logger.getLogger(GaeUserStructWebUtil.class.getName());

    // Static methods only.
    private GaeUserStructWebUtil() {}
    

    public static GaeUserStructJsBean convertGaeUserStructToJsBean(GaeUserStruct gaeUserStruct)
    {
        GaeUserStructJsBean jsBean = null;
        if(gaeUserStruct != null) {
            jsBean = new GaeUserStructJsBean();
            jsBean.setAuthDomain(gaeUserStruct.getAuthDomain());
            jsBean.setFederatedIdentity(gaeUserStruct.getFederatedIdentity());
            jsBean.setNickname(gaeUserStruct.getNickname());
            jsBean.setUserId(gaeUserStruct.getUserId());
            jsBean.setEmail(gaeUserStruct.getEmail());
            jsBean.setNote(gaeUserStruct.getNote());
        }
        return jsBean;
    }

    public static GaeUserStruct convertGaeUserStructJsBeanToBean(GaeUserStructJsBean jsBean)
    {
        GaeUserStructBean gaeUserStruct = null;
        if(jsBean != null) {
            gaeUserStruct = new GaeUserStructBean();
            gaeUserStruct.setAuthDomain(jsBean.getAuthDomain());
            gaeUserStruct.setFederatedIdentity(jsBean.getFederatedIdentity());
            gaeUserStruct.setNickname(jsBean.getNickname());
            gaeUserStruct.setUserId(jsBean.getUserId());
            gaeUserStruct.setEmail(jsBean.getEmail());
            gaeUserStruct.setNote(jsBean.getNote());
        }
        return gaeUserStruct;
    }

}
