package com.cannyurl.wa.util;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.GeoPointStruct;
import com.cannyurl.af.bean.GeoPointStructBean;
import com.cannyurl.fe.bean.GeoPointStructJsBean;


public class GeoPointStructWebUtil
{
    private static final Logger log = Logger.getLogger(GeoPointStructWebUtil.class.getName());

    // Static methods only.
    private GeoPointStructWebUtil() {}
    

    public static GeoPointStructJsBean convertGeoPointStructToJsBean(GeoPointStruct geoPointStruct)
    {
        GeoPointStructJsBean jsBean = null;
        if(geoPointStruct != null) {
            jsBean = new GeoPointStructJsBean();
            jsBean.setUuid(geoPointStruct.getUuid());
            jsBean.setLatitude(geoPointStruct.getLatitude());
            jsBean.setLongitude(geoPointStruct.getLongitude());
            jsBean.setAltitude(geoPointStruct.getAltitude());
            jsBean.setSensorUsed(geoPointStruct.isSensorUsed());
        }
        return jsBean;
    }

    public static GeoPointStruct convertGeoPointStructJsBeanToBean(GeoPointStructJsBean jsBean)
    {
        GeoPointStructBean geoPointStruct = null;
        if(jsBean != null) {
            geoPointStruct = new GeoPointStructBean();
            geoPointStruct.setUuid(jsBean.getUuid());
            geoPointStruct.setLatitude(jsBean.getLatitude());
            geoPointStruct.setLongitude(jsBean.getLongitude());
            geoPointStruct.setAltitude(jsBean.getAltitude());
            geoPointStruct.setSensorUsed(jsBean.isSensorUsed());
        }
        return geoPointStruct;
    }

}
