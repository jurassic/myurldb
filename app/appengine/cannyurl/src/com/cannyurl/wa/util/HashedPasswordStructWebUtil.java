package com.cannyurl.wa.util;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.HashedPasswordStruct;
import com.cannyurl.af.bean.HashedPasswordStructBean;
import com.cannyurl.fe.bean.HashedPasswordStructJsBean;


public class HashedPasswordStructWebUtil
{
    private static final Logger log = Logger.getLogger(HashedPasswordStructWebUtil.class.getName());

    // Static methods only.
    private HashedPasswordStructWebUtil() {}
    

    public static HashedPasswordStructJsBean convertHashedPasswordStructToJsBean(HashedPasswordStruct hashedPasswordStruct)
    {
        HashedPasswordStructJsBean jsBean = null;
        if(hashedPasswordStruct != null) {
            jsBean = new HashedPasswordStructJsBean();
            jsBean.setUuid(hashedPasswordStruct.getUuid());
            jsBean.setPlainText(hashedPasswordStruct.getPlainText());
            jsBean.setHashedText(hashedPasswordStruct.getHashedText());
            jsBean.setSalt(hashedPasswordStruct.getSalt());
            jsBean.setAlgorithm(hashedPasswordStruct.getAlgorithm());
        }
        return jsBean;
    }

    public static HashedPasswordStruct convertHashedPasswordStructJsBeanToBean(HashedPasswordStructJsBean jsBean)
    {
        HashedPasswordStructBean hashedPasswordStruct = null;
        if(jsBean != null) {
            hashedPasswordStruct = new HashedPasswordStructBean();
            hashedPasswordStruct.setUuid(jsBean.getUuid());
            hashedPasswordStruct.setPlainText(jsBean.getPlainText());
            hashedPasswordStruct.setHashedText(jsBean.getHashedText());
            hashedPasswordStruct.setSalt(jsBean.getSalt());
            hashedPasswordStruct.setAlgorithm(jsBean.getAlgorithm());
        }
        return hashedPasswordStruct;
    }

}
