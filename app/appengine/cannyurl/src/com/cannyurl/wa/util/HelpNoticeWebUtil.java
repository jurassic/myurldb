package com.cannyurl.wa.util;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.HelpNotice;
import com.cannyurl.af.bean.HelpNoticeBean;
import com.cannyurl.fe.bean.HelpNoticeJsBean;


public class HelpNoticeWebUtil
{
    private static final Logger log = Logger.getLogger(HelpNoticeWebUtil.class.getName());

    // Static methods only.
    private HelpNoticeWebUtil() {}
    

    public static HelpNoticeJsBean convertHelpNoticeToJsBean(HelpNotice helpNotice)
    {
        HelpNoticeJsBean jsBean = null;
        if(helpNotice != null) {
            jsBean = new HelpNoticeJsBean();
            jsBean.setUuid(helpNotice.getUuid());
            jsBean.setTitle(helpNotice.getTitle());
            jsBean.setContent(helpNotice.getContent());
            jsBean.setFormat(helpNotice.getFormat());
            jsBean.setNote(helpNotice.getNote());
            jsBean.setStatus(helpNotice.getStatus());
        }
        return jsBean;
    }

    public static HelpNotice convertHelpNoticeJsBeanToBean(HelpNoticeJsBean jsBean)
    {
        HelpNoticeBean helpNotice = null;
        if(jsBean != null) {
            helpNotice = new HelpNoticeBean();
            helpNotice.setUuid(jsBean.getUuid());
            helpNotice.setTitle(jsBean.getTitle());
            helpNotice.setContent(jsBean.getContent());
            helpNotice.setFormat(jsBean.getFormat());
            helpNotice.setNote(jsBean.getNote());
            helpNotice.setStatus(jsBean.getStatus());
        }
        return helpNotice;
    }

}
