package com.cannyurl.app.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;


public class ShortLinkUtilTest
{
    private final LocalServiceTestHelper helper =
        new LocalServiceTestHelper(new LocalDatastoreServiceTestConfig());

    @Before
    public void setUp() throws Exception {
        helper.setUp();
    }

    @After
    public void tearDown() throws Exception {
        helper.tearDown();
    }

    
    @Test
    public void testParseFullUrl()
    {
        System.out.println(">>>>>>>>>> Begin: testParseFullUrl()");
        String shortUrl = null;
        String[] parts = null;
        
        shortUrl = "http://su2.us";
        parts = ShortLinkUtil.parseFullUrl(shortUrl);
        assertNull(parts);

        shortUrl = "http://su2.us/";
        parts = ShortLinkUtil.parseFullUrl(shortUrl);
        assertNull(parts);

        shortUrl = "http://su2.us/x";
        parts = ShortLinkUtil.parseFullUrl(shortUrl);
        assertNull(parts);

        shortUrl = "http://su2.us/xy/";
        parts = ShortLinkUtil.parseFullUrl(shortUrl);
        assertNull(parts);

        shortUrl = "http://su2.us/a/x";
        parts = ShortLinkUtil.parseFullUrl(shortUrl);
        assertNull(parts);

        shortUrl = "http://su2.us/a/xy/";
        parts = ShortLinkUtil.parseFullUrl(shortUrl);
        assertNull(parts);

        shortUrl = "http://su2.us/@uv";
        parts = ShortLinkUtil.parseFullUrl(shortUrl);
        assertNull(parts);

        shortUrl = "http://su2.us/@uv/";
        parts = ShortLinkUtil.parseFullUrl(shortUrl);
        assertNull(parts);

        shortUrl = "http://su2.us/@uv/x";
        parts = ShortLinkUtil.parseFullUrl(shortUrl);
        assertNull(parts);

        shortUrl = "http://su2.us/@uv/xy/";
        parts = ShortLinkUtil.parseFullUrl(shortUrl);
        assertNull(parts);

        shortUrl = "http://su2.us/@uv/a/x";
        parts = ShortLinkUtil.parseFullUrl(shortUrl);
        assertNull(parts);

        shortUrl = "http://su2.us/@uv/a/xy/";
        parts = ShortLinkUtil.parseFullUrl(shortUrl);
        assertNull(parts);

        shortUrl = "http://su2.us/xy";
        parts = ShortLinkUtil.parseFullUrl(shortUrl);
        assertNotNull(parts);
        assertEquals("http://su2.us/", parts[0]);
        assertNull(parts[1]);
        assertEquals("xy", parts[2]);

        shortUrl = "http://su2.us/xy/z";
        parts = ShortLinkUtil.parseFullUrl(shortUrl);
        assertNotNull(parts);
        assertEquals("http://su2.us/", parts[0]);
        assertNull(parts[1]);
        assertEquals("xy/z", parts[2]);

        shortUrl = "http://su2.us/a/xy";
        parts = ShortLinkUtil.parseFullUrl(shortUrl);
        assertNotNull(parts);
        assertEquals("http://su2.us/", parts[0]);
        assertEquals("a", parts[1]);
        assertEquals("xy", parts[2]);

        shortUrl = "http://su2.us/@uv/xy";
        parts = ShortLinkUtil.parseFullUrl(shortUrl);
        assertNotNull(parts);
        assertEquals("http://su2.us/@uv/", parts[0]);
        assertNull(parts[1]);
        assertEquals("xy", parts[2]);

        shortUrl = "http://su2.us/@uv/xy/z";
        parts = ShortLinkUtil.parseFullUrl(shortUrl);
        assertNotNull(parts);
        assertEquals("http://su2.us/@uv/", parts[0]);
        assertNull(parts[1]);
        assertEquals("xy/z", parts[2]);

        shortUrl = "http://su2.us/@uv/a/xy";
        parts = ShortLinkUtil.parseFullUrl(shortUrl);
        assertNotNull(parts);
        assertEquals("http://su2.us/@uv/", parts[0]);
        assertEquals("a", parts[1]);
        assertEquals("xy", parts[2]);

        shortUrl = "http://su2.us/@uv/a/xy/zw/pq";
        parts = ShortLinkUtil.parseFullUrl(shortUrl);
        assertNotNull(parts);
        assertEquals("http://su2.us/@uv/", parts[0]);
        assertEquals("a", parts[1]);
        assertEquals("xy/zw/pq", parts[2]);

    }

}
