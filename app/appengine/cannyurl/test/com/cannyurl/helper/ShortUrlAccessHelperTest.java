package com.cannyurl.helper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;


public class ShortUrlAccessHelperTest
{
    private final LocalServiceTestHelper helper =
        new LocalServiceTestHelper(new LocalDatastoreServiceTestConfig());

    @Before
    public void setUp() throws Exception {
        helper.setUp();
    }

    @After
    public void tearDown() throws Exception {
        helper.tearDown();
    }

    
    @Test
    public void testParseFullUrl()
    {
        System.out.println(">>>>>>>>>> Begin: testParseFullUrl()");
        String shortUrl = null;
        String[] parts = null;
     
        
        // These methods have been moved to app.util.ShortLinkUtil...
        
/*     
        shortUrl = "http://su2.us";
        parts = ShortUrlAccessHelper.parseFullUrl(shortUrl);
        assertNull(parts);

        shortUrl = "http://su2.us/";
        parts = ShortUrlAccessHelper.parseFullUrl(shortUrl);
        assertNull(parts);

        shortUrl = "http://su2.us/x";
        parts = ShortUrlAccessHelper.parseFullUrl(shortUrl);
        assertNull(parts);

        shortUrl = "http://su2.us/xy/";
        parts = ShortUrlAccessHelper.parseFullUrl(shortUrl);
        assertNull(parts);

        shortUrl = "http://su2.us/a/x";
        parts = ShortUrlAccessHelper.parseFullUrl(shortUrl);
        assertNull(parts);

        shortUrl = "http://su2.us/a/xy/";
        parts = ShortUrlAccessHelper.parseFullUrl(shortUrl);
        assertNull(parts);

        shortUrl = "http://su2.us/@uv";
        parts = ShortUrlAccessHelper.parseFullUrl(shortUrl);
        assertNull(parts);

        shortUrl = "http://su2.us/@uv/";
        parts = ShortUrlAccessHelper.parseFullUrl(shortUrl);
        assertNull(parts);

        shortUrl = "http://su2.us/@uv/x";
        parts = ShortUrlAccessHelper.parseFullUrl(shortUrl);
        assertNull(parts);

        shortUrl = "http://su2.us/@uv/xy/";
        parts = ShortUrlAccessHelper.parseFullUrl(shortUrl);
        assertNull(parts);

        shortUrl = "http://su2.us/@uv/a/x";
        parts = ShortUrlAccessHelper.parseFullUrl(shortUrl);
        assertNull(parts);

        shortUrl = "http://su2.us/@uv/a/xy/";
        parts = ShortUrlAccessHelper.parseFullUrl(shortUrl);
        assertNull(parts);

        shortUrl = "http://su2.us/xy";
        parts = ShortUrlAccessHelper.parseFullUrl(shortUrl);
        assertNotNull(parts);
        assertEquals("http://su2.us/", parts[0]);
        assertNull(parts[1]);
        assertEquals("xy", parts[2]);

        shortUrl = "http://su2.us/xy/z";
        parts = ShortUrlAccessHelper.parseFullUrl(shortUrl);
        assertNotNull(parts);
        assertEquals("http://su2.us/", parts[0]);
        assertNull(parts[1]);
        assertEquals("xy/z", parts[2]);

        shortUrl = "http://su2.us/a/xy";
        parts = ShortUrlAccessHelper.parseFullUrl(shortUrl);
        assertNotNull(parts);
        assertEquals("http://su2.us/", parts[0]);
        assertEquals("a", parts[1]);
        assertEquals("xy", parts[2]);

        shortUrl = "http://su2.us/@uv/xy";
        parts = ShortUrlAccessHelper.parseFullUrl(shortUrl);
        assertNotNull(parts);
        assertEquals("http://su2.us/@uv/", parts[0]);
        assertNull(parts[1]);
        assertEquals("xy", parts[2]);

        shortUrl = "http://su2.us/@uv/xy/z";
        parts = ShortUrlAccessHelper.parseFullUrl(shortUrl);
        assertNotNull(parts);
        assertEquals("http://su2.us/@uv/", parts[0]);
        assertNull(parts[1]);
        assertEquals("xy/z", parts[2]);

        shortUrl = "http://su2.us/@uv/a/xy";
        parts = ShortUrlAccessHelper.parseFullUrl(shortUrl);
        assertNotNull(parts);
        assertEquals("http://su2.us/@uv/", parts[0]);
        assertEquals("a", parts[1]);
        assertEquals("xy", parts[2]);

        shortUrl = "http://su2.us/@uv/a/xy/zw/pq";
        parts = ShortUrlAccessHelper.parseFullUrl(shortUrl);
        assertNotNull(parts);
        assertEquals("http://su2.us/@uv/", parts[0]);
        assertEquals("a", parts[1]);
        assertEquals("xy/zw/pq", parts[2]);
*/
    }

}
