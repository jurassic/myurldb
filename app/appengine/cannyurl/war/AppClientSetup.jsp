<%@ page import="com.myurldb.ws.core.*, com.myurldb.ws.*, com.cannyurl.af.util.*, com.cannyurl.af.auth.common.*, com.cannyurl.af.auth.*, com.cannyurl.af.auth.googleapps.*, com.cannyurl.af.auth.user.*, com.cannyurl.fe.*, com.cannyurl.fe.core.*, com.cannyurl.fe.bean.*, com.cannyurl.fe.util.*, com.cannyurl.wa.service.*, com.cannyurl.common.*, com.cannyurl.util.*, com.cannyurl.app.auth.*, com.cannyurl.app.auth.filter.*, com.cannyurl.app.util.*, com.cannyurl.helper.*" 
%><%@ page contentType="text/html; charset=UTF-8" 
%><%@ page errorPage="/error/UnknownError" 
%><%!
private static final java.util.logging.Logger logger = java.util.logging.Logger.getLogger("AppClientSetup");
%><%
boolean isAppAuthDisabled = ConfigUtil.isApplicationAuthDisabled();
boolean isAppAuthOptional = ConfigUtil.isApplicationAuthOptional();
String appAuthMode = ConfigUtil.getApplicationAuthMode();
%><%
// {1] Session
SessionBean sessionBean = UserSessionManager.getInstance().setSessionBean(request, response);
String sessionToken = sessionBean.getToken();
String userId = sessionBean.getUserId();

// [2] Parse url.
String contextPath = request.getContextPath();
String servletPath = request.getServletPath();
String requestHost = request.getServerName();
//String requestURI = request.getRequestURI();
String requestUrl = request.getRequestURL().toString();
String topLevelUrl = UrlHelper.getInstance().getTopLevelURLFromRequestURL(requestUrl);
String pathInfo = request.getPathInfo();
String queryString = request.getQueryString();
%><%

// temporary
String paramAppsDomain = request.getParameter(GoogleAppsAuthUtil.PARAM_APPSDOMAIN);
String paramCallback = null;
if(paramAppsDomain != null && !paramAppsDomain.isEmpty()) {
    // Note that callback param is relevant only if appsDomain is set....
    paramCallback = request.getParameter(GoogleAppsAuthUtil.PARAM_GOOGLEAPPS_CALLBACK);
}
// ....

%><%
// "Branding"
String appBrand = BrandingHelper.getInstance().getAppBrand();
String brandDisplayName = BrandingHelper.getInstance().getBrandDisplayName();
String brandDescription = BrandingHelper.getInstance().getBrandDescription();
%><%
String themeBootstrap = SiteThemeHelper.getInstance().getBootstrapTheme();
String themeJqueryUi = SiteThemeHelper.getInstance().getJqueryUiTheme();
%><%
boolean isUserAuthenticated = false;
String userUsername = null;
String loginUrl = null;
String logoutUrl = null;
RequestAuthStateBean authStateBean = (RequestAuthStateBean) request.getAttribute(AuthUtil.REQUEST_ATTR_AUTHSTATEBEAN);
if(authStateBean == null) {
    // Filter might not have applied (because of the /* mapping, etc...) 
    // Try one more time ???
    authStateBean = AuthFilterUtil.createAuthStateBean(request, response);
    if(authStateBean != null) {
        // Add it to the current request object....
        request.setAttribute(AuthUtil.REQUEST_ATTR_AUTHSTATEBEAN, authStateBean);
    }
}
// TBD: 
if(authStateBean != null) {
    isUserAuthenticated = authStateBean.isAuthenticated();
    if(isUserAuthenticated) {
        logoutUrl = authStateBean.getLogoutUrl();
        userUsername = authStateBean.getUsername();
    } else {
        // TBD:
        // paramCallback ????
        // ....
        loginUrl = authStateBean.getLoginUrl();
    }
}
%><%
if(isUserAuthenticated == false) {
    // ????
    // response.sendRedirect(loginUrl);
}
%><%
// [3] Local "global" variables.
AppClientJsBean appClientBean = null;
String appClientId = null;  // != requestUrl, in general. (requestUrl may include the "path" compoennt)...
String appClientCode = null;
//String requestUrlRedirectType = null;


// [4] Parse the reuqest URL.
// Find appClientBean and requestUrlRedirectType, if any.
boolean requiresRefresh = false;   // Redirect to the "canonical" url, if necessary...
if(servletPath != null && servletPath.length() > 2) {   // ???

    String guid = UrlHelper.getInstance().getGuidFromPathInfo(pathInfo);
	if(guid != null && !guid.isEmpty()) {  // TBD: validation??
        try {
	        appClientBean = new AppClientWebService().getAppClient(guid);
        } catch (Exception e) {
            // Ignore...
        }
        // Heck!!! In case of delay in saving a newly crated bean, try one more time ????
        if(appClientBean == null) {
             try {
    	        appClientBean = new AppClientWebService().getAppClient(guid);
		    } catch (Exception e) {
		        // Ignore...
		    }
        }

	} else {
	    // ????
	    // ....
	}

} else {

    // TBD:
    // Get clientId and/or clientCode from pathInfo/query param, and then
    // get appClient using clientId, clientCode, etc. ????
    // e.g., appClientBean = AppClientHelper.getInstance().findAppClient(clientId);
    // ...

}

%><%

if(appClientBean == null) {
    // This is primarily used during Google Apps initial installation...
    if(paramAppsDomain != null && !paramAppsDomain.isEmpty()) {
        appClientBean = AppClientHelper.getInstance().findAppClientByRootDomain(paramAppsDomain);
    }
}

%><%

// ???
if(appClientId == null || appClientId.isEmpty()) {
	if(appClientBean != null) {
	    appClientId = appClientBean.getClientId();   
	}
}
if(appClientCode == null || appClientCode.isEmpty()) {
	if(appClientBean != null) {
	    appClientCode = appClientBean.getClientCode();
	}
}
%><%

String appClientGuid = null;
String appClientName = "";
String appClientRootDomain = "";
String appClientAppDomain = "";
Boolean appClientUseAppDomain = true;  // ???
if(appClientBean != null) {
    appClientGuid = appClientBean.getGuid();
	appClientName = (appClientBean.getName() != null) ? appClientBean.getName() : appClientName;
	appClientRootDomain = (appClientBean.getRootDomain() != null) ? appClientBean.getRootDomain() : appClientRootDomain;
	appClientAppDomain = (appClientBean.getAppDomain() != null) ? appClientBean.getAppDomain() : appClientAppDomain;
	appClientUseAppDomain = (appClientBean.isUseAppDomain() != null) ? appClientBean.isUseAppDomain() : appClientUseAppDomain;
}
String appClientHostedDomain = "";
if(appClientCode != null) {
    // TBD:
    appClientHostedDomain = DomainUtil.buildAppClientHostedDomain(appClientCode);
    // ...
}
// ...
%><%
// temporary
if(appClientAppDomain == null || appClientAppDomain.isEmpty()) {
    if(appClientRootDomain == null || appClientRootDomain.isEmpty()) {
        if(paramAppsDomain != null) {
            appClientRootDomain = paramAppsDomain;
        }
    }
    if(appClientRootDomain != null && !appClientRootDomain.isEmpty()) {
        // TBD:
        // How to get Google Apps subdomain????
        // temporary
        appClientAppDomain = appClientRootDomain;
        // ....
    }
}
// ...
%><%

String appClientOwner = null;
String appClientAdmin = null;
if(appClientBean != null) {
    appClientOwner = appClientBean.getOwner();
    appClientAdmin = appClientBean.getAdmin();
}

%><%

String appClientPageTitle = brandDisplayName + " | Client Setup";
String appClientDescription = AppBrand.getBrandDescription(appBrand);

%><%
// ....
%><%
// TBD: Update the AccessRecord...
// ??? Should we do it a bit earlier (e.g., before error redictring, or even before permalink redirect ????)
// ???
// Note: we can use a separate accessRecord service...
// ...
%><!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!-- Consider adding an manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<html>
  <head>
    <meta charset="utf-8">
    <title><%=appClientPageTitle%></title>
    <meta name="description" content="<%=appClientDescription%>">

    <!-- Mobile viewport optimized: j.mp/bplateviewport -->
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <link rel="shortcut icon" href="/img/favicon-<%=appBrand%>.ico" />
    <link rel="icon" href="/img/favicon-<%=appBrand%>.ico" type="image/x-icon" />

    <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- CSS: implied media=all -->
    <%@ include file="/fragment/style.jspf" %>
    <!-- end CSS-->

    <!-- All JavaScript at the bottom, except for Modernizr / Respond.
       Modernizr enables HTML5 elements & feature detects; Respond is a polyfill for min/max-width CSS3 Media Queries
       For optimal performance, use a custom Modernizr build: www.modernizr.com/download/ -->
    <script src="http://ww2.filestoa.com/js/modernizr/modernizr-2.0.6.min.js"></script>

  </head>

  <body>

<jsp:include page="/fragment/TopNavbar">
<jsp:param name="pageCode" value="appclient/setup"/>
<jsp:param name="brandDisplayName" value="<%=brandDisplayName%>"/>
</jsp:include>


    <div class="container-fluid">

      <!-- Main hero unit for a primary marketing message or call to action -->
      <div id="main" class="hero-unit">
      <form class="form-horizontal" id="form_appclient_view" method="POST" action="" accept-charset="UTF-8">

      <div id="appclient_preface">
      <fieldset name="fieldset_appclient_preface">
        <table style="background-color: transparent;">
        <tr>
        <td style="vertical-align:bottom;">
        <div id="cannyurl_logo">
	 	    <h1><%=brandDisplayName%><sup>&beta;</sup></h1>
        </div>
        </td>
        </tr>
        </table>
      </fieldset>
      </div>


      <div id="appclient_form_body">
      <fieldset name="fieldset_appclient_body">

		  <div class="control-group">
            <label class="control-label" for="input_appclient_name" id="input_appclient_name_label" title="Input company name">Name:</label>
		    <div class="controls">
               <input class="input-large" type="text" name="input_appclient_name" id="input_appclient_name" size="35" placeholder="e.g., My Company Name" value="<%=appClientName%>"></input>
               <button class="btn" type="button" name="button_appclient_setup" id="button_appclient_setup" title="Set up a new client and save" style="width:70px">Setup</button>  

<% if(paramCallback != null && !paramCallback.isEmpty()) { %>
               <button class="btn" type="button" name="button_appclient_cancel" id="button_appclient_cancel" title="Cancel and return without completing the setup" style="width:70px">Cancel</button>  
<% } %>

		    </div>
		  </div>
		  <div class="control-group">
            <label class="control-label" for="input_appclient_clientcode" id="input_appclient_clientcode_label" title="Input client Code">Client Code:</label>
		    <div class="controls">
               <input class="input-large" type="text" name="input_appclient_clientcode" id="input_appclient_clientcode" size="35" placeholder="e.g., my_company_name" value="<%=(appClientCode == null) ? "" : appClientCode%>"></input>        
               <button class="btn" type="button" name="button_appclient_clientcode_check" id="button_appclient_clientcode_check" title="Check the availability of this client Code" style="width:70px">Setup</button>  
		    </div>
		  </div>
		  <div class="control-group">
		    <div class="controls">
		      <label class="checkbox">
                <input class="input-large" type="checkbox" id="checkbox_appclient_useappdomain" name="checkbox_appclient_useappdomain" value="useappdomain"
                <%if(appClientUseAppDomain == true) { %>checked<% } else { %> <% } %>
                > Use your custom app domain for short URLs
		      </label>
		    </div>
		  </div>
		  <div id="div_control_group_appclient_appdomain" class="control-group">
            <label class="control-label" for="input_appclient_appdomain" id="input_appclient_appdomain_label" title="App Domain">App Domain:</label>
		    <div class="controls">
                <input class="input-large uneditable-input" type="text" name="input_appclient_appdomain" id="input_appclient_appdomain" size="35" readonly value="<%=appClientAppDomain%>"></input>        
		    </div>
		  </div>
		  <div id="div_control_group_appclient_hosteddomain" class="control-group">
            <label class="control-label" for="input_appclient_hosteddomain" id="input_appclient_hosteddomain_label" title="Hosted Domain">Hosted Domain:</label>
		    <div class="controls">
                <input class="input-large uneditable-input" type="text" name="input_appclient_hosteddomain" id="input_appclient_hosteddomain" size="35" readonly value="<%=appClientHostedDomain%>"></input>        
		    </div>
		  </div>

      </fieldset>
      </div>


      </form>
      </div>  <!--   Hero unit  -->




<!--   BEGIN: Content   -->



<% if(BrandingHelper.getInstance().isBrandCannyURL()) { %>
<% } else if(BrandingHelper.getInstance().isBrandFlashURL()) { %>
<% } else if(BrandingHelper.getInstance().isBrandFemtoURL()) { %>
<% } else if(BrandingHelper.getInstance().isBrandSassyURL()) { %>
<% } else if(BrandingHelper.getInstance().isBrandUserURL()) { %>
<% } else if(BrandingHelper.getInstance().isBrandQuadURL()) { %>
<% } else if(BrandingHelper.getInstance().isBrandSpeedKeyword()) { %>
<% } else if(BrandingHelper.getInstance().isBrandTweetUserURL()) { %>
<% } else if(BrandingHelper.getInstance().isBrandSmallBizURL()) { %>
<% } else { %>
<% } %>



<!--   END: Content   -->





<!-- Page Footer -->    
<%@ include file="/fragment/Footer.jspf" %>
<!-- Page Footer -->    

    </div> <!--! end of #container -->



    <div id="div_status_floater" style="display: none;" class="alert">
    </div>
 

    <!-- JavaScript at the bottom for fast page loading -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="http://ww2.filestoa.com/js/jquery/jquery-1.8.3.min.js"><\/script>')</script>
	<script src="http://ww2.filestoa.com/js/jquery/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="http://ww2.filestoa.com/js/jquery/plugin/jquery.floatobject-1.4.js"></script>
    <script src="http://ww2.filestoa.com/js/jquery/plugin/jquery-ui-timepicker-addon.js"></script>
    <script src="http://ww2.filestoa.com/js/jquery/plugin/history.js/uncompressed/history.adapter.jquery.js"></script>
    <script src="http://ww2.filestoa.com/js/jquery/plugin/history.js/uncompressed/history.js"></script>
    <script src="http://ww2.filestoa.com/js/jquery/plugin/history.js/uncompressed/history.html4.js"></script>
    <script src="http://ww2.filestoa.com/js/bootstrap/bootstrap-2.0.js"></script>
    <script type="text/javascript" src="/js/core/debugutil.js"></script>
    <script type="text/javascript" src="/js/core/urlutil-1.0.js"></script>
    <script type="text/javascript" src="http://ww2.filestoa.com/js/core/datetimeutil-1.0.js"></script>
    <script type="text/javascript" src="http://ww2.filestoa.com/js/core/emailutil-1.0.js"></script>
    <script type="text/javascript" src="/js/bean/appclientjsbean-1.0.js"></script>
    <script type="text/javascript" src="/js/mylibs/mytimer.js"></script>
    <script defer type="text/javascript" src="/js/mylibs/redirecturl-1.0.js"></script>
	<script defer type="text/javascript" src="/js/plugins.js"></script>
    <script defer type="text/javascript" src="/js/script.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/fiveten-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/statushelper-1.1.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/tweethelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/signuphelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/feedbackhelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/emailhelper-1.0.js"></script>
    <!-- end scripts-->


    <script>
    // Global vars
    var feedbackHelper;
    $(function() {
    	var targetService = "<%=brandDisplayName%>";
    	var targetPage = "setup";
    	feedbackHelper = new webstoa.FeedbackHelper(targetService, targetPage);
    });
    </script>

    <script>
    // Email address "decoding"...
    $(function() {
        $("a").each(function() {
        	var anchor = $(this);
        	webstoa.EmailUtil.decodeMailtoAnchor(anchor);
        });
    });
    </script>

    <script>
    $(function() {
        $("#anchor_feedback_footer").click(function() {
            if(DEBUG_ENABLED) console.log("Feedback button clicked."); 
            var targetItem = 'footer';
            feedbackHelper.feedback(targetItem);
		    return false;
        });
    });
    </script>


    <script>
    // Function definitions.
    function updateStatus(msg, type, time) {
    	if(!time) {
    		time = 5000;   // milliseconds.
    	}
    	if(!type) {
    		// type = "info";  // warning, error, etc....
    	}
    	//$("#div_status_floater").text(msg);
    	//$("#div_status_floater").fadeIn().delay(time).fadeOut();
		$("#div_status_floater").textInOut(msg, type, time);
    }    
    </script>

    <script>
    // temporary
    function replaceURLWithHTMLLinks(text) {
        if(text) {
            var exp = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
            //var exp = /\(?\bhttp:\/\/[-A-Za-z0-9+&@#\/%?=~_()|!:,.;]*[-A-Za-z0-9+&@#/%=~_()|]/ig;
            return text.replace(exp,"<a rel='nofollow' href='$1'>$1</a>"); 
        }
        return '';    // ???
    }
    </script>



    <script>
    // "Init"
    $(function() {
        // select() ????
        var currentVal = $("#input_appclient_clientcode").val();
        if(currentVal) {
	        $("#input_appclient_clientcode").focus().val('').val(currentVal);
        } else {
	        $("#input_appclient_clientcode").focus();
        }

    	// TBD: initially hide $("#div_status_floater") ...
    	$("#div_status_floater").text("").hide();
    
        // Background image test.
        //$("#container").css("background-image", 'url(/img/bg/oceansunset.jpg)');
        //$(document.body).css("background", "silver");
    	//$("#container").css("background", "ivory");

        // TBD: Location...
    	var params = {x: 585, y: 70, speed: 'normal' };
    	$("#div_status_floater").makeFloat(params);
    	
    	// temporary
    	// linkify the content
    	var contentOriginal = $("#appclient_view_body_main_display_message").html();
    	var contentLinkified = replaceURLWithHTMLLinks(contentOriginal);
    	$("#appclient_view_body_main_display_message").html(contentLinkified);
    	// temporary
    	
        // $("#input_appclient_clientcode").removeAttr('readonly');

    	// TBD:
    	toggleDomainInputFields();
        refrehAppClientHostedDomain();
        // ...
    	
    	// Peeng?
    	//var fiveTenTimer = new fiveten.FiveTenTimer(12972, 2);  // ???
    	//fiveTenTimer.start();
    });
    </script>


    <script>
    function disableSaveButtons(delay)
    {
    	$("#button_appclient_setup").attr('disabled','disabled');
    	$("#button_appclient_clientcode_check").attr('disabled','disabled');
    	var milli;
    	if(delay) {
    		milli = delay;
    	} else {
    		milli = 7500;  // ???
    	}
    	setTimeout(resetAndEnableSaveButtons, milli);
    }
    function enableSaveButtons()
    {
    	setTimeout(resetAndEnableSaveButtons, 1250);  // ???
    }
    function resetAndEnableSaveButtons()
    {
    	$("#button_appclient_setup").removeAttr('disabled');
    	$("#button_appclient_clientcode_check").removeAttr('disabled');
    }
    </script>

    <script>
    // App-related vars.
    var userGuid;
    var appClientJsBean;
    // var isClientCodeValidated = false;
    var isClientCodeChecked = false;
    var isClientCodeUsable = false;    // true means - validated/checked and it can be used by this client...
    var editState = 'state_new';
<%
if(userId != null) {
%>
    userGuid = "<%=userId%>";
<%
}
%>
<% if(appClientBean == null) { %>
    appClientJsBean = new cannyurl.wa.bean.AppClientJsBean();
    if(userGuid) {
    	appClientJsBean.setOwner(userGuid);
    }
<% } else { %>
    editState = 'state_edit';
	var appClientJsObjectStr = '<%=appClientBean.toEscapedJsonStringForJavascript()%>';
	if(DEBUG_ENABLED) console.log("appClientJsObjectStr = " + appClientJsObjectStr);
	appClientJsBean = cannyurl.wa.bean.AppClientJsBean.fromJSON(appClientJsObjectStr);
    if(DEBUG_ENABLED) console.log("appClientJsBean = " + appClientJsBean.toString());
<% } %>
    if(DEBUG_ENABLED) console.log("editState = " + editState);
    if(DEBUG_ENABLED) console.log("appClientJsBean = " + appClientJsBean.toString());
    </script>

    <script>
    // Additional Init based on editState.
    $(function() {
      if(editState == 'state_new') {
          $("#button_appclient_setup").text("Set up");
          $("#button_appclient_clientcode_check").text("Check");
      
      
      } else {
          $("#button_appclient_setup").text("Update");
          $("#button_appclient_clientcode_check").text("Check");

    	  
      }
    });
    </script>

    <script>
    // Ajax form handlers.
  $(function() {
	  // ...
  });
    </script>

  <script>
  function toggleDomainInputFields()
  {
	    if ($("#checkbox_appclient_useappdomain").is(':checked')) {
	        // the checkbox was checked 
    	    $("#div_control_group_appclient_appdomain").show();
    	    $("#div_control_group_appclient_hosteddomain").hide();
	    } else {
	        // the checkbox was unchecked
    	    $("#div_control_group_appclient_appdomain").hide();
    	    $("#div_control_group_appclient_hosteddomain").show();
	    }
  }
  </script>
 
  <script>
  function validateClientCode(clientCode)
  {
	  // isClientCodeValidated = true;

	    if(! clientCode) {
	    	return false;
	    }

	    var clientCodeLength = clientCode.length;
        var minLength = <%=ConfigUtil.getClientCodeMinLength()%>;
        var maxLength = <%=ConfigUtil.getClientCodeMaxLength()%>;
        if(clientCodeLength < minLength) {
            return false;
        }
        if(clientCodeLength > maxLength) {
            return false;
        }

        return true;
  }
  </script>
 
  <script>
  // TBD:
  function checkClientCode(clientCode, proceedToSave)
  {
	  var validated = validateClientCode(clientCode);
	  if(! validated) {
		  isClientCodeChecked = false;
    	  isClientCodeUsable = false;   	 
	      updateStatus('Client Code, "' + clientCode + '", is invalid. Please try a different code.', 'error', 7550);    		  
		  return;
	  }
      // TBD:
      clientCode = clientCode.toLowerCase();
      // ....

	  // Reset these flags before calling ajax.... ?????
	  isClientCodeChecked = false;
	  isClientCodeUsable = false;
	  // ...
	  
      var clientCodeCheckUrl = "<%=topLevelUrl%>" + "ajax/validate/clientcode/" + clientCode;
      $.getJSON(clientCodeCheckUrl, function(data) {
		  isClientCodeChecked = true;
    	  if(data) {
             var isValid = data['valid'];
             if(isValid) {
             	  isClientCodeChecked = true;
            	  isClientCodeUsable = true;
             } else {
            	  isClientCodeChecked = true;
            	  isClientCodeUsable = false;            	 
             }
    	  } else {
    		  // ????
        	  isClientCodeChecked = true;
        	  isClientCodeUsable = false;            	 
    	  }
    	  if(isClientCodeUsable) {
    	      updateStatus('Client Code, "' + clientCode + '", is available.', 'info', 7550);
    	      if(proceedToSave) {
    	    	  // TBD:
    	    	  saveAppClientJsBean();
    	          // ...
    	      }
    	  } else {
    	      updateStatus('Your specified client code may not be available. Please try again.', 'error', 5550);    		  
    	  }
      })
      .fail(function() { 
		  isClientCodeChecked = true;
    	  isClientCodeUsable = false;            	 
	      updateStatus('Client Code checking failed', 'error', 5550);    		  
      });
  }
  </script>

  <script>
  function buildAppClientHostedDomain(clientCode)
  {
      // temporary
      // Copy of DomainUtil.buildAppClientHostedDomain()
      var baseDomain = "<%=DomainUtil.getDefaultBaseDomain()%>";
      var slashslash = baseDomain.indexOf("://") + 3;
      var prefix = baseDomain.substring(0, slashslash);
      var theRest = baseDomain.substring(slashslash);
      var domain = prefix + clientCode + "." + theRest;
      return domain;
  }
  </script>

  <script>
  function refrehAppClientHostedDomain()
  {
      // temporary
		var clientCode = $("#input_appclient_clientcode").val();
		var hostedDomain = "";
		if(clientCode) {
			hostedDomain = buildAppClientHostedDomain(clientCode);
		}
		$("#input_appclient_hosteddomain").val(hostedDomain);
		// ....
  }
  </script>

  <script>
  $(function() {
	  // ...
      $("#input_appclient_clientcode").keyup(function(event) {
    	  refrehAppClientHostedDomain();
    	  // isClientCodeValidated = false;
    	  isClientCodeChecked = false;
    	  isClientCodeUsable = false;
      });
  });
  </script>

  <script>
  $(function() {
	  // ...
	  $("#checkbox_appclient_useappdomain").click(function() {
		    var $this = $(this);
		    // $this will contain a reference to the checkbox   
            toggleDomainInputFields();
		});
	  });
   </script>


    <script>
    $(function() {

    	$("#button_appclient_clientcode_check").click(function() {
    	    if(DEBUG_ENABLED) console.log("ClientCode check button clicked.");

            var clientCode = $("#input_appclient_clientcode").val();
            if(clientCode) {
        	    checkClientCode(clientCode);
            } else {
            	// ????
            }
            
            return false;
    	});

    });
    </script>

<% if(paramCallback != null && !paramCallback.isEmpty()) { %>
    <script>
    $(function() {
    	$("#button_appclient_cancel").click(function() {
    	    if(DEBUG_ENABLED) console.log("Cancel button clicked.");
            window.location = "<%=paramCallback%>";
    	});
    });
    </script>
<% } %>

    <script>
    $(function() {

    	$("#button_appclient_setup").click(function() {
    	    if(DEBUG_ENABLED) console.log("Setup button clicked.");

    	    if(! isClientCodeChecked) {
      	        // disableSaveButtons();    // ????    	    	
    	        var clientCode = $("#input_appclient_clientcode").val().trim();
    	    	checkClientCode(clientCode, true);
    	    	return false;
    	    }
    	    
    	    if(isClientCodeUsable) {
    	      updateStatus('Saving the client setup...', 'info', 5550);
    	      disableSaveButtons();    	    	
    	      saveAppClientJsBean();
    	    } else {
    	    	// ???
        	    updateStatus('The client code may not be available. Please try again.', 'error', 5550);    		  
    	    }

    	    // TBD: ...
    	    // window.location = setupPageUrl;

            return false;
    	
    	});

    });
    </script>


    <script>
    var saveAppClientJsBean = function() {
    	
        // validate and process form here      

        var name = $("#input_appclient_name").val();
        if(DEBUG_ENABLED) console.log("name = " + name);
        appClientJsBean.setName(name);

        var clientCode = $("#input_appclient_clientcode").val().trim();
        if(DEBUG_ENABLED) console.log("clientCode = " + clientCode);
        // if(clientCode == '') {
        //     if(DEBUG_ENABLED) console.log("Client code is not set.");
        //     updateStatus('Client code is not set.', 'error', 5550);
        //     enableSaveButtons();
      	//     return false;
        // }
        // TBD:
        clientCode = clientCode.toLowerCase();
        // ....
        appClientJsBean.setClientCode(clientCode);

        var appDomain = $("#input_appclient_appdomain").val().trim();
        if(DEBUG_ENABLED) console.log("appDomain = " + appDomain);
        appClientJsBean.setAppDomain(appDomain);   // ???

        var useAppDomainChecked = false;
        if($("#checkbox_applcient_useappdomain").is(':checked')) {
      	    useAppDomainChecked = true;
        }
        appClientJsBean.setUseAppDomain(useAppDomainChecked);
        
        if(userGuid) {   
        	appClientJsBean.setOwner(userGuid);  // This should not be necessary..
        }
        
        // Update the modifiedTime field ....
     	if(editState != 'state_new') {
     	    var modifiedTime = (new Date()).getTime();
            appClientJsBean.setModifiedTime(modifiedTime);
     	}

        // Payload...
        var appClientJsonStr = JSON.stringify(appClientJsBean);
        if(DEBUG_ENABLED) console.log("appClientJsonStr = " + appClientJsonStr);
        var payload = "{\"appClient\":" + appClientJsonStr;
        // TBD...
        payload += "}";
        if(DEBUG_ENABLED) console.log("payload = " + payload);
        
        
        // Web service settings.
        var appClientPostEndPoint = "<%=topLevelUrl%>" + "ajax/appclient";
        if(DEBUG_ENABLED) console.log("appClientPostEndPoint =" + appClientPostEndPoint);
        var httpMethod;
        var webServiceUrl;
        if(editState == 'state_new') {
      	  httpMethod = "POST";
      	  webServiceUrl = appClientPostEndPoint;
  	  } else if(editState == 'state_edit') {
      	  httpMethod = "PUT";
      	  webServiceUrl = appClientPostEndPoint + "/" + appClientJsBean.getGuid();
        } else {
      	  // ???
     		  if(DEBUG_ENABLED) console.log("Error. Invalid editState = " + editState);
            updateStatus('Error occurred.', 'error', 5550);
            enableSaveButtons();
      	  return false;
        }

        if(httpMethod) {
          $.ajax({
      	    type: httpMethod,
      	    url: webServiceUrl,
      	    data: payload,
      	    dataType: "json",
      	    contentType: "application/json; charset=UTF-8",
      	    success: function(data, textStatus) {
      	      // TBD
      	      if(DEBUG_ENABLED) console.log("appClientJsBean successfully saved. textStatus = " + textStatus);
      	      if(DEBUG_ENABLED) console.log("data = " + data);
      	      if(DEBUG_ENABLED) console.log(data);

    	      // History
    	      // Do this only if the current editState == 'state_new'
    	      if(editState == 'state_new') {
	    	      var History = window.History;
	    	      if(History) {
	   	    	    	// TBD: Need to html escape????
	    				var htmlPageTitle = '<%=brandDisplayName%> | Client Setup ';
	    	            // TBD: This does not work on HTML4 browsers (e.g., IE)
				        History.pushState({state:1}, htmlPageTitle, "/appclient/setup/" + appClientJsBean.getGuid());
	    	      }
    	      }
    	      
    	      // If it is currently editState == 'state_new', change it to 'state_edit'.
    	      // If it is currently editState == 'state_edit', that's ok too.
    	      editState = 'state_edit';

      	      // Parse data...
      	      if(data) {   // POST only..
  	    	      // Replace/Update the JsBeans ...

  	    	      var appClientObj = data["appClient"];
  	    	      if(DEBUG_ENABLED) console.log("appClientObj = " + appClientObj);
  	    	      if(DEBUG_ENABLED) console.log(appClientObj);
  	    	      appClientJsBean = cannyurl.wa.bean.AppClientJsBean.create(appClientObj);
  	    	      if(DEBUG_ENABLED) console.log("New appClientJsBean = " + appClientJsBean);
  	    	      if(DEBUG_ENABLED) console.log(appClientJsBean);
  	    	      
  	    	      
  	    	      // Delay this just a bit so that the server has time to actually save it (Note: we are using async call for saving).
  	    	      // This helps when a new appClient is saved. 
  	    	      // TBD: In both save/update, various buttons should be disabled before the ajax call
  	    	      //   and they should be re-enabled after the call successuflly returns or after certain preset time.
  	    	      window.setTimeout(refreshFormFields, 1250);
  	              updateStatus('Client setup successfully saved.', 'info', 5550);
      	      } else {
      	    	  // ignore
      	    	  // PUT currently returns "OK" only.
      	    	  // TBD: Change the ajax server side code so that it returns the updated data.
      	    	  //      Based on the returned/updated data, some fields (e.g., appClient) may need to be updated....
  	              updateStatus('Client setup successfully updated.', 'info', 5550);
      	      } 
      	      enableSaveButtons();
      	    
<% if(paramCallback != null && !paramCallback.isEmpty()) { %>
              window.location = "<%=paramCallback%>";
<% } %>
 
      	    },
      	    error: function(req, textStatus) {
      	    	var errorMsg = 'Failed to set up client. status = ' + textStatus;
            	if(DEBUG_ENABLED) console.log(errorMsg);
      	    	updateStatus(errorMsg, 'error', 5550);
      	    	enableSaveButtons();
      	    }
  	   	  });
        } else {
      	  // ???
      	  enableSaveButtons();
        }
        
    };
   </script>

  <script>
  // Refresh input fields...
  var refreshFormFields = function() {
	if(DEBUG_ENABLED) console.log("refreshFormFields() called......");
	if(DEBUG_ENABLED) console.log("editState = " + editState);
	
	if(editState == 'state_new') {
        $("#button_appclient_setup").text("Set up");
        $("#button_appclient_clientcode_check").text("Check");
		$("#input_appclient_name").val('');
		$("#input_appclient_clientcode").val('');
		$("#input_appclient_appdomain").val('');
		$("#checkbox_appclient_useappdomain").attr('checked', true);  // ???

	} else {
        $("#button_appclient_setup").text("Update");
        $("#button_appclient_clientcode_check").text("Check");

        if(appClientJsBean) {
			var name = appClientJsBean.getName();
			$("#input_appclient_name").val(name);
			var clientCode = appClientJsBean.getClientCode();
			$("#input_appclient_clientcode").val(clientCode);
			var appDomain = appClientJsBean.getAppDomain();
			$("#input_appclient_appdomain").val(appDomain);
			// var useAppDomain = appClientJsBean.isUseAppDomain();
			var useAppDomain = appClientJsBean.getUseAppDomain();     // ???
			$("#checkbox_appclient_useappdomain").attr('checked', useAppDomain);
			// ...

		} else {
			// ????
		}
	}
	
	// ...
	toggleDomainInputFields();
    refrehAppClientHostedDomain();
    // ...
    
  };
 </script>




<script>
$(function() {
	// Top menu handlers
    $("#topmenu_nav_stream").click(function() {
        if(DEBUG_ENABLED) console.log("topmenu_nav_stream anchor clicked."); 
      	window.location = "/stream";
        return false;
    });
    $("#topmenu_nav_shorten").click(function() {
        if(DEBUG_ENABLED) console.log("topmenu_nav_shorten anchor clicked."); 
      	window.location = "/shorten";
        return false;
    });
    $("#topmenu_nav_verify").click(function() {
        if(DEBUG_ENABLED) console.log("topmenu_nav_verify anchor clicked."); 
      	window.location = "/verify";
        return false;
    });
    $("#topmenu_nav_view").click(function() {
        if(DEBUG_ENABLED) console.log("topmenu_nav_view anchor clicked."); 
        $("#topmenu_nav_view").fadeOut(450).fadeIn(950);
        return false;
    });
    $("#topmenu_nav_info").click(function() {
        if(DEBUG_ENABLED) console.log("topmenu_nav_info anchor clicked."); 
        $("#topmenu_nav_info").fadeOut(450).fadeIn(950);
        return false;
    });
    $("#topmenu_nav_list").click(function() {
        if(DEBUG_ENABLED) console.log("topmenu_nav_list anchor clicked."); 
        $("#topmenu_nav_list").fadeOut(450).fadeIn(950);
        return false;
    });
<%
if(isAppAuthDisabled == true || isAppAuthOptional == true || isUserAuthenticated == true) {
%>
    $("#topmenu_nav_info").click(function() {
        if(DEBUG_ENABLED) console.log("topmenu_nav_info anchor clicked."); 
        if(appClientJsBean) {
            //var redirectUrlHelper = new cannyurl.redirecturl.RedirectUrlHelper(appClientJsBean);
            //if(redirectUrlHelper.isReady()) {
            //    var infoUrl = redirectUrlHelper.infoUrl();
            //    window.location = infoUrl;
            //} else {
                var guid = appClientJsBean.getGuid();
                window.location = "/info/" + guid;	
            //}
        } else {
        	window.location = "/info";
        }
        return false;
    });
    //$("#topmenu_nav_list").click(function() {
    //    if(DEBUG_ENABLED) console.log("topmenu_nav_list anchor clicked."); 
    //    window.location = "/list";
    //    return false;
    //});
<%
}
%>
});
</script>


<%
String googleAnalyticsCode = AnalyticsHelper.getInstance().getGoogleAnalyticsCode();
if(googleAnalyticsCode != null && !googleAnalyticsCode.isEmpty()) {    // And, if it is enabled (e.g. through config). TBD...
%>
    <!-- Change UA-XXXXX-X to be your site's ID -->
    <script>
      window._gaq = [['_setAccount','<%=googleAnalyticsCode%>'],['_trackPageview'],['_trackPageLoadTime']];
      Modernizr.load({
        load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
      });
    </script>
<%
}
%>

    <!-- Prompt IE 6 users to install Chrome Frame. Remove this if you want to support IE 6.
       chromium.org/developers/how-tos/chrome-frame-getting-started -->
    <!--[if lt IE 7 ]>
      <script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
      <script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
    <![endif]-->

    </body>
</html>