<%@ page import="com.myurldb.ws.core.*, com.cannyurl.af.util.*, com.cannyurl.af.auth.common.*, com.cannyurl.af.auth.*, com.cannyurl.af.auth.googleplus.*, com.cannyurl.af.auth.user.*, com.cannyurl.af.search.*, com.cannyurl.fe.*, com.cannyurl.fe.core.*, com.cannyurl.fe.bean.*, com.cannyurl.fe.util.*, com.cannyurl.wa.service.*, com.cannyurl.common.*, com.cannyurl.util.*, com.cannyurl.app.auth.*, com.cannyurl.app.util.*, com.cannyurl.helper.*" 
%><%@ page contentType="text/html; charset=UTF-8" 
%><%@ page errorPage="/error/UnknownError" 
%><%!
private static final java.util.logging.Logger logger = java.util.logging.Logger.getLogger("GooglePlusLogin");
%><%
// {1] Session
SessionBean sessionBean = UserSessionManager.getInstance().setSessionBean(request, response);
String sessionToken = sessionBean.getToken();
String userId = sessionBean.getUserId();

// [2] Parse url.
String requestURI = request.getRequestURI();
String requestUrl = request.getRequestURL().toString();
String topLevelUrl = UrlHelper.getInstance().getTopLevelURLFromRequestURL(requestUrl);
String contextPath = request.getContextPath();
String servletPath = request.getServletPath();
String pathInfo = request.getPathInfo();
String queryString = request.getQueryString();
String referer = request.getHeader("referer");
//...
%><%
// For CSRF prevention...

// String formId1 = AuthUtil.getDefaultTopNavBarAuthFormId();
String formId1 = GooglePlusAuthHelper.getInstance().getDefaultSignInFormId();   // ???
String csrfState1 = CsrfHelper.getInstance().setCsrfStateSessionAttribute(session, null, formId1);   // ????
// String csrfState1 = CsrfHelper.getInstance().setCsrfStateSessionAttribute(session);
// ...
%><%
// Google+ signin

GooglePlusSignInStruct signInStruct = GooglePlusSignInHelper.getInstance().buildGooglePlusSignInStruct();
// signInStruct.setDataCallback("googlePlusSignInCallback");
String signInButtonHtml = signInStruct.buildSignInButtonHtml();

String tokenHandlerUrl = GooglePlusAuthHelper.getInstance().getCallbackTokenHandlerUri(topLevelUrl);
String callbackAuthAjaxUrl = GooglePlusAuthHelper.getInstance().getCallbackAuthAjaxUri(topLevelUrl);
%><%
// "Branding"
String appBrand = BrandingHelper.getInstance().getAppBrand();
String brandDisplayName = BrandingHelper.getInstance().getBrandDisplayName();
String brandDescription = BrandingHelper.getInstance().getBrandDescription();
%><%String themeBootstrap = SiteThemeHelper.getInstance().getBootstrapTheme();
String themeJqueryUi = SiteThemeHelper.getInstance().getJqueryUiTheme();%><%
AuthUserService authUserService = AuthUserServiceFactory.getAuthUserService();
AuthUser authUser = authUserService.getCurrentUser(); // or req.getUserPrincipal()
//...
%><%
// 
String comebackUrl = request.getParameter(CommonAuthUtil.PARAM_COMEBACKURL);
if(comebackUrl == null || comebackUrl.isEmpty()) {
/* */
    if(referer != null && !referer.isEmpty()) {
        comebackUrl = referer;  // ???
    } else {
        // ???
        String thispageUrl = request.getRequestURI();
        if(queryString != null && !queryString.isEmpty()) {
            thispageUrl += "?" + queryString;
        }
        comebackUrl = thispageUrl;  // ????
    }
/* */
}
String encodedComebackUrl = null;
if(comebackUrl != null && !comebackUrl.isEmpty()) {
	try {
	    encodedComebackUrl = java.net.URLEncoder.encode(comebackUrl, "UTF-8");
	} catch(Exception e) {
	    // ignore
	}
}
//...
%><%
// 
String fallbackUrl = request.getParameter(CommonAuthUtil.PARAM_FALLBACKURL);
if(fallbackUrl == null || fallbackUrl.isEmpty()) {
    // if(referer != null && !referer.isEmpty()) {
    //     fallbackUrl = referer;  // ???
    // } else {
        // String thispageUrl = request.getRequestURI();
        // if(queryString != null && !queryString.isEmpty()) {
        //     thispageUrl += "?" + queryString;
        // }
        // fallbackUrl = thispageUrl;  // ????
        // Note: Using the current page (/login) can potentially cause recursive call loop...
        //       It's probably safe to use something else, preferably a page that does not require auth...
        // fallbackUrl = "/home";
        fallbackUrl = "/error/AuthFailure";
    // }
}
String encodedFallbackUrl = null;
try {
    encodedFallbackUrl = java.net.URLEncoder.encode(fallbackUrl, "UTF-8");
} catch(Exception e) {
    // ignore
}
//...

%><%
//...
String loginAuthUrl = "/auth/manager?openid_identifier=googleplus";
if(encodedComebackUrl != null) {
	loginAuthUrl += "&comebackUrl=" + encodedComebackUrl;
}
if(encodedFallbackUrl != null) {
	loginAuthUrl += "&fallbackUrl=" + encodedFallbackUrl;
}
// String logoutAuthUrl = "";
// ....

%><%
boolean isUserAuthenticated = false;
// isUserAuthenticated = AuthManager.getInstance().isUserLoggedIn(session, CommonAuthUtil.AUTH_SERVICE_PROVIDERID_GOOGLEPLUS);
// ...
/*
if(isUserAuthenticated) {
    // TBD: 
    //     redirect to camebackurl...
    response.sendRedirect(comebackUrl);
    // ....
} else {
    // TBD:
    //     redirect to google login page...
    //     redirect or forward ????
    response.sendRedirect(loginAuthUrl);
    // ....
}
*/
// ...
%><!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Sign In with Google</title> 

    <!-- Le styles -->
    <%@ include file="/fragment/style.jspf" %>
    <link rel="stylesheet" type="text/css" href="http://ww4.filestoa.com/css/openid/openid.css" />

    <!-- Le fav and touch icons -->
	<link rel="shortcut icon" href="/img/favicon-<%=appBrand%>.ico" />
	<link rel="icon" href="/img/favicon-<%=appBrand%>.ico" type="image/x-icon" />


  <!-- BEGIN Google+ SignIn -->
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js">
  </script>
  <script type="text/javascript">
  // jQuery.noConflict();
  </script>
  <script type="text/javascript">
  (function () {
      var po = document.createElement('script');
      po.type = 'text/javascript';
      po.async = true;
      po.src = 'https://plus.google.com/js/client:plusone.js?onload=start';
      var s = document.getElementsByTagName('script')[0];
      s.parentNode.insertBefore(po, s);
    })();
  </script>
  <!-- END Google+ SignIn -->

    </head>

  <body>
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
           <a class="brand" href="/home"><%=brandDisplayName%> (beta)</a>
          <div class="nav-collapse">
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container-fluid">

      <!-- Main hero unit for a primary marketing message or call to action -->
      <div id="main" class="hero-unit">
        <h2>Sign In with Google+</h2>

<div>
<% if(authUser == null) { %>
authUser is null.
<% } else { %>
authUser is not null...
<% } %>
</div>

<div>
<% if(isUserAuthenticated) { %>
User is authenticated.
<% } else { %>
User is not authenticated.
<% } %>
</div>


<div>



<form class="form-inline" id="<%=formId1%>" method="POST" action="" accept-charset="UTF-8">
<!-- 
<input type="hidden" name="<%=CsrfHelper.REQUEST_PARAM_CSRF_STATE%>" value="<%=csrfState1%>"></input>
 -->


<%=signInButtonHtml%>
<div id="results"></div>



</form>



</div>

      </div>

      <hr>

      <footer>
        <p>&copy; <%=brandDisplayName%> 2013</p>
      </footer>
    </div> <!-- /container -->


<!-- Google+ SignIn callback function -->
<script type="text/javascript">
function googlePlusSignInCallback(authResult) 
{
  if (authResult['code']) {

    // Hide the sign-in button now that the user is authorized, for example:
    $('#signinButton').attr('style', 'display: none');

    // Send the code to the server
    // var webservicePostUrl = '<%=tokenHandlerUrl%>';
    var webservicePostUrl = '<%=callbackAuthAjaxUrl%>/connect';

    // TBD
    var csrfState = "<%=csrfState1%>";
    var encodedCsrfState = encodeURIComponent(csrfState);   // ???
    webservicePostUrl += "?" + "<%=CsrfHelper.REQUEST_PARAM_CSRF_STATE%>=" + encodedCsrfState;
    
    var payload = '{"code":"' + authResult['code'] + '"}';
    $.ajax({
      type: 'POST',
      url: webservicePostUrl,
      processData: false,
      data: payload,
      contentType: 'application/json;charset=UTF-8',
      success: function(result) {
        // Handle or verify the server response if necessary.

        // Prints the list of people that the user has allowed the app to know
        // to the console.
        console.log(result);
        if (result['profile'] && result['people']){
          $('#results').html('Hello ' + result['profile']['displayName'] + '. You successfully made a server side call to people.get and people.list');
        } else {
          $('#results').html('Failed to make a server-side call. Check your configuration and console.');
        }
      }
    });
  } else if (authResult['error']) {
    // There was an error.
    // Possible error codes:
    //   "access_denied" - User denied access to your app
    //   "immediate_failed" - Could not automatially log in the user
    console.log('There was an error: ' + authResult['error']);
  }
}
</script>


  </body>
</html>

