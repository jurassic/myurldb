<%@ page import="com.myurldb.ws.core.*, com.myurldb.ws.*, com.cannyurl.af.util.*, com.cannyurl.af.auth.common.*, com.cannyurl.af.auth.*, com.cannyurl.af.auth.user.*, com.cannyurl.fe.*, com.cannyurl.fe.core.*, com.cannyurl.fe.bean.*, com.cannyurl.fe.util.*, com.cannyurl.wa.service.*, com.cannyurl.common.*, com.cannyurl.util.*, com.cannyurl.app.auth.*, com.cannyurl.app.util.*, com.cannyurl.helper.*" 
%><%@ page contentType="text/html; charset=UTF-8" 
%><%@ page errorPage="/error/UnknownError" 
%><%// {1] Session
SessionBean sessionBean = UserSessionManager.getInstance().setSessionBean(request, response);
String sessionToken = sessionBean.getToken();
String userId = sessionBean.getUserId();

// [2] Parse url.
String requestUrl = request.getRequestURL().toString();
String topLevelUrl = UrlHelper.getInstance().getTopLevelURLFromRequestURL(requestUrl);
String pathInfo = request.getPathInfo();
String queryString = request.getQueryString();
%><%
// "Branding"
String appBrand = BrandingHelper.getInstance().getAppBrand();
String brandDisplayName = BrandingHelper.getInstance().getBrandDisplayName();
String brandDescription = BrandingHelper.getInstance().getBrandDescription();
%><%String themeBootstrap = SiteThemeHelper.getInstance().getBootstrapTheme();
String themeJqueryUi = SiteThemeHelper.getInstance().getJqueryUiTheme();%><%
AuthUserService authUserService = AuthUserServiceFactory.getAuthUserService();
AuthUser authUser = authUserService.getCurrentUser(); // or req.getUserPrincipal()
String nickName = "";
String loginUrl = null;
String logoutUrl = null;
String comebackUrl = null;
String encodedComebackUrl = null;
if(authUser != null) {
    nickName = authUser.getNickname(); 
    logoutUrl = authUserService.createLogoutURL(request.getRequestURI());
} else {
    comebackUrl = request.getRequestURI();
    encodedComebackUrl = java.net.URLEncoder.encode(comebackUrl, "UTF-8");
    loginUrl = "/auth?comebackUrl=" + encodedComebackUrl;
}
%><%
String twitterComebackUrl = null;
String encodedTwitterComebackUrl = null;
twitterComebackUrl = request.getRequestURI();
try {
    encodedTwitterComebackUrl = java.net.URLEncoder.encode(twitterComebackUrl, "UTF-8");
} catch(Exception e) {
    // ignore
}
//if(encodedTwitterComebackUrl == null) {
//    encodedTwitterComebackUrl = "";  // ???
//}%><%String twitterFallbackUrl = "/error/Unauthorized";
String encodedTwitterFallbackUrl = null;
try {
    encodedTwitterFallbackUrl = java.net.URLEncoder.encode(twitterFallbackUrl, "UTF-8");
} catch(Exception e) {
    // ignore
}
//if(encodedTwitterFallbackUrl == null) {
//    encodedTwitterFallbackUrl = "";   // ????
//}%><%String twitterLoginPageUrl = "/twitter/login";
if(encodedTwitterComebackUrl != null) {
    twitterLoginPageUrl += "?comebackUrl=" + encodedTwitterComebackUrl;
	if(encodedTwitterFallbackUrl != null) {
	    twitterLoginPageUrl += "&fallbackUrl=" + encodedTwitterFallbackUrl;
	}
} else {
	if(encodedTwitterFallbackUrl != null) {
	    twitterLoginPageUrl += "?fallbackUrl=" + encodedTwitterFallbackUrl;
	}
}%><%// For Twitter twitterLogin....
// TBD: This should be done using a filter...
String providerId = CommonAuthUtil.AUTH_SERVICE_PROVIDERID_TWITTER;
// boolean isUserAuthenticated = AuthManager.getInstance().isUserLoggedIn(session, providerId);
boolean isUserAuthenticated = false;
String twitterUserUsername = null;
ExternalUserIdStruct externalUserId = AuthManager.getInstance().getExternalUserIdStruct(session, providerId);
if(externalUserId != null) {
    isUserAuthenticated = true;
    twitterUserUsername = externalUserId.getUsername();
} else {
    // externalUserId == null does not mean the user is not authenticated....
    isUserAuthenticated = AuthManager.getInstance().isUserLoggedIn(session, providerId);
}%><%if( BrandingHelper.getInstance().isBrandUserURL() 
    || BrandingHelper.getInstance().isBrandTwrim() ) {
    if(isUserAuthenticated == false) {
        // ????
        // response.sendRedirect(twitterLoginPageUrl);
    }
} else {
    if(authUser == null) {
        // ????
        // response.sendRedirect(loginUrl);
    }
}%><%// if(twitterUserUsername != null) {
//     twitterUserUsername = "";      // ???
// }
String twitterHandle = "";
String twitterUserProfileUrl = "";
if(twitterUserUsername != null && !twitterUserUsername.isEmpty()) {
    twitterHandle = "@" + twitterUserUsername;      // ???
    twitterUserProfileUrl = "http://twitter.com/" + twitterUserUsername;
}%><%// [3] Local "global" variables.
//String editState = null;     // "state_new", "state_edit", "state_read" (?), "state_unknown" == null.
KeywordLinkJsBean keywordLinkBean = null;

// [4] Initial validation
// URL "/edit/<guid>" ???
String guid = UrlHelper.getInstance().getGuidFromPathInfo(pathInfo);
// check first if guid is a valid GUID....
if(guid != null && !guid.isEmpty()) {   // TBD: validation...
    try {
        keywordLinkBean = new KeywordLinkWebService().getKeywordLink(guid);
    } catch(Exception e) {
        // ignore...
    }
	if(keywordLinkBean != null) {
	    //editState = "state_edit";
	    String shortUrlUser = keywordLinkBean.getUser();            // This cannot be null
	    Long createdTime = keywordLinkBean.getCreatedTime();    // This cannot be null
	    long cutoffTime = new java.util.Date().getTime() - 12 * 3600 * 1000L;   // Arbitrary. 12 hours ago.
		if((shortUrlUser != null && !shortUrlUser.equals(userId)) || (createdTime != null && createdTime < cutoffTime)) {
		    // If the user is not the author,
		    // or, the shortUrl is "too old", 
	    	// then make this shortUrl non-editable!!!
	    	// Or, just forward it to "/edit" ???
	    	// temporary
	    	response.sendRedirect("/edit");
		}
	} else {
	    // guid is not null, but keywordLinkBean is not found...
	    // TBD: Show error message? Or, just redirect to the shortUrl create page???
	    // temporary
    	response.sendRedirect("/edit");
	}
} else {
    //editState = "state_new";

}

// [5] JSP/Javascript variables..
String shortUrlLongUrl = "";
//String shortUrlLongUrl = KeywordLinkUtil.createDefaultKeywordLinkTitle();
// etc....
String shortUrlKeywordMemo = "";
// ....
String shortUrlToken = "";
String shortUrlShortUrl = "";
long shortUrlCreatedTime = 0L;
long shortUrlModifiedTime = 0L;
// ...
String shortUrlRedirectType = "";
String shortUrlDomain = "";
String shortUrlNote = "";
long expirationTime = 0L;
if(keywordLinkBean != null) {
    shortUrlLongUrl = (keywordLinkBean.getLongUrl() != null) ? keywordLinkBean.getLongUrl() : shortUrlLongUrl;
    
    shortUrlDomain = (keywordLinkBean.getDomain() != null) ? keywordLinkBean.getDomain() : shortUrlDomain;
    shortUrlToken = (keywordLinkBean.getToken() != null) ? keywordLinkBean.getToken() : shortUrlToken;
    shortUrlKeywordMemo = (keywordLinkBean.getMemo() != null) ? keywordLinkBean.getMemo() : shortUrlKeywordMemo;
 
    String shortUrl = keywordLinkBean.getShortUrl();
    if(URLUtil.isValidUrl(shortUrl)) {
        shortUrlShortUrl = shortUrl;
    } else {
        // ???
        //shortUrlShortUrl = "";  // Build it from domain and token???? 
    }

    shortUrlCreatedTime = (keywordLinkBean.getCreatedTime() != null) ? keywordLinkBean.getCreatedTime() : shortUrlCreatedTime;
    shortUrlModifiedTime = (keywordLinkBean.getModifiedTime() != null) ? keywordLinkBean.getModifiedTime() : shortUrlModifiedTime;
    expirationTime = (keywordLinkBean.getExpirationTime() != null) ? keywordLinkBean.getExpirationTime() : expirationTime;

    //shortUrlRedirectType = (keywordLinkBean.getRedirectType() != null) ? keywordLinkBean.getRedirectType() : shortUrlRedirectType;
    shortUrlNote = (keywordLinkBean.getNote() != null) ? keywordLinkBean.getNote() : shortUrlNote;
}%><%

if(shortUrlRedirectType == null || shortUrlRedirectType.isEmpty()) {
    shortUrlRedirectType = ConfigUtil.getSystemDefaultRedirectType();
    if(keywordLinkBean != null) {
        //keywordLinkBean.setRedirectType(shortUrlRedirectType);
    }
}
%><!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!-- Consider adding an manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<html>
  <head>
    <meta charset="utf-8">
    <title><%=brandDisplayName%> | <%=brandDescription%></title>
    <meta name="author" content="Aery Software">
    <meta name="description" content="<%=AppBrand.getBrandDescription(appBrand)%>. It also provides URL redirect / URL Shortener Web Services API. But, <%=brandDisplayName%> is so much more than a URL shortener or URL redirection application." />


    <!-- Mobile viewport optimized: j.mp/bplateviewport -->
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <link rel="shortcut icon" href="/img/favicon-<%=appBrand%>.ico" />
    <link rel="icon" href="/img/favicon-<%=appBrand%>.ico" type="image/x-icon" />

    <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- CSS: implied media=all -->
    <%@ include file="/fragment/style.jspf" %>
    <!-- end CSS-->

    <!-- All JavaScript at the bottom, except for Modernizr / Respond.
       Modernizr enables HTML5 elements & feature detects; Respond is a polyfill for min/max-width CSS3 Media Queries
       For optimal performance, use a custom Modernizr build: www.modernizr.com/download/ -->
    <script src="http://ww2.filestoa.com/js/modernizr/modernizr-2.0.6.min.js"></script>

<%
	if(TwitterUtil.isTwitterSetUp(topLevelUrl)) {
%>
    <!--  Twitter @anywhere API  -->
    <script src="http://platform.twitter.com/anywhere.js?id=<%=TwitterUtil.getConsumerKey(topLevelUrl)%>&v=1" type="text/javascript"></script>
<%
	}
%>
  </head>

  <body>

<jsp:include page="/fragment/TopNavbar">
<jsp:param name="pageCode" value="keyword/edit"/>
<jsp:param name="brandDisplayName" value="<%=brandDisplayName%>"/>
</jsp:include>


<!--  
    <div id="page_header" class="page_header_top">
    &nbsp;
    </div>
-->

    <div id="container">
      <div id="main" role="main">
      <form id="form_shorturl_edit" method="POST" action="">

      <div id="shorturl_header">
      <fieldset name="fieldset_shorturl_header">
        <table>
        <tr>
        <td style="vertical-align:bottom;">
        <div id="cannyurl_logo">
	 	    <h1><%=brandDisplayName%><sup>&beta;</sup></h1>
        </div>
        </td>
        <td style="vertical-align:top;">
            <div id="cannyurl_questionmark" style="margin:-5px -35px 0px -5px;">
                <a href="#" id="cannyurl_questionmark_link"><img src="/img/question-50.png" alt="<%=brandDisplayName%> Help" title="<%=brandDisplayName%> Help"></img></a>
	 	    </div>
        </td>
        </tr>
        </table>
      </fieldset>
      </div>

      <div id="shorturl_body">
      <fieldset name="fieldset_shorturl_body">
        <div id="shorturl_body_top">
        <table>
          <tr>
          <td>
            <label class="input_label_text" for="input_shorturl_longurl" id="input_shorturl_longurl_label" title="Input URL. Copy and paste the target Web site URL here.">Long&nbsp;URL:</label>
            <br/>
            <span id="title_char_counter">400</span>
          </td>
          <td colspan="4">
            <div id="shorturl_longurl">
              <textarea name="input_shorturl_longurl" id="input_shorturl_longurl" cols="70" rows="2"><%=shortUrlLongUrl%></textarea>
            </div>
          </td>
          </tr>

          <tr>
          <td>
            <label class="input_label_text" for="input_shorturl_keyword" id="input_shorturl_keyword_label" title="">Keyword:</label>
          </td>
          <td>
            <div id="shorturl_keyword">
              <input type="text" name="input_shorturl_keyword" id="input_shorturl_keyword" size="20"/>
            </div>
          </td>
          <td style="vertical-align:bottom;">
            <button type="button" class="button_shorturl_primary_target" name="button_shorturl_save" id="button_shorturl_save" title="Create a keyword URL" style="width:90px">Create</button> 
          </td>
          <td>
            <button type="button" class="button_shorturl_primary_target" name="button_shorturl_shorten" id="button_shorturl_shorten" title="Just shorten the long URL" style="width:90px">Shorten</button>           
          </td>
          <td>
              <select id="input_shorturl_options_token_type" title="Select the token type. A token is a random string at the end of a short URL.">  
<%
  	String tokenTypeDefaultValue = ConfigUtil.getSystemDefaultTokenType();
    for(String[] tt : TokenType.TYPES) {
        String val = tt[0];   
        String label = tt[1];
        // Exlcude "custom" for now.... Exclude "sassy" as well... etc...
        if(!val.equals(TokenType.TYPE_CUSTOM) && !val.equals(TokenType.TYPE_SASSY) && !val.equals(TokenType.TYPE_KEYWORD)) {
            if(val.equals(tokenTypeDefaultValue)) {
%>
		          <option value="<%=val%>" selected><%=label%></option>
<%
            } else {
%>
		          <option value="<%=val%>"><%=label%></option>
<%
	        }
        }
    }
%>
              </select>
          </td>
          </tr>
        
        
          <tr>
          <td>
            <label class="input_label_text" for="input_shorturl_shorturl" id="input_shorturl_shorturl_label" title="Shortened URL">Short&nbsp;URL:</label>
          </td>
          <td colspan="3">
            <div id="shorturl_shorturl">
              <input type="text" name="input_shorturl_shorturl" id="input_shorturl_shorturl" readonly size="30" value="<%=shortUrlShortUrl%>"></input>

              <button type="button" name="button_shorturl_verify_shorturl" id="button_shorturl_verify_shorturl" title="Verify the Short URL" style="width:70px">Verify</button>
              <button type="button" name="button_shorturl_view_shorturl" id="button_shorturl_view_shorturl" title="View the target page" style="width:70px">View</button>
<%
	if(TwitterUtil.isTwitterSetUp(topLevelUrl)) {
%>
              <button type="button" name="button_shorturl_tweet" id="button_shorturl_tweet" title="Tweet this short URL" style="width:70px">Tweet</button>  
<%
    }
%>
              <button type="button" name="button_shorturl_new" id="button_shorturl_new" title="Create a new short URL" style="width:70px">New</button>
            </div>
          </td>
          <td>&nbsp;
          </td>
          </tr>
          
          
          <tr>
          <td>&nbsp;
          </td>
          <td colspan="1">
			<table>
			
<%
    String defaultScope = KeywordScope.SCOPE_USER;  // temporary
    for( String[] scope : KeywordScope.SCOPES ) {
        String val = scope[0];
        String label = scope[1];
        if(!val.equals(KeywordScope.SCOPE_CROWD) && !val.equals(KeywordScope.SCOPE_SYSTEM)) {
%>
			<tr>
				<td>
<%
            if(val.equals(defaultScope)) {
%>
	            <input type="radio" name="navscrope" value="<%=val%>" checked="checked" /> <%=label %>
<%
            } else {
%>
	            <input type="radio" name="navscrope" value="<%=val%>"/> <%=label %>
<%
            }
%>
				</td>
				<td>&nbsp;
				</td>
			</tr>
<%
        }
    }
%>			
			</table>
          </td>
          <td colspan="2" style="vertical-align:bottom; width:200px;">
            <button type="button" name="button_navkeyword_scope_default" id="button_navkeyword_scope_default" title="Set this as my default" style="width:120px">Set As Default</button> 
          </td>
          <td>&nbsp;
          </td>
          </tr>
          
        </table>
        </div>

        <div id="shorturl_body_middle">
          <div id="shorturl_body_main_keyword_memo_label">
            <label class="input_label_text" for="input_shorturl_keyword_memo" id="input_shorturl_keyword_memo_label" title='Include any optional memo.'>Memo (optional):</label>
            &nbsp;
            <span id="keyword_memo_char_counter">500</span> chars remaining
          </div>
          <div id="shorturl_body_main" class="shorturl_body_main_keyword_memo">
              <div id="shorturl_keyword_memo_textarea">
                  <textarea name="input_shorturl_keyword_memo" id="input_shorturl_keyword_memo" cols="100" rows="4"><%=shortUrlKeywordMemo%></textarea>
              </div>
          </div>

        </div>
      </fieldset>
      </div>

      </form>
      </div>
      
<!-- Page Footer -->    
<%@ include file="/fragment/Footer.jspf" %>
<!-- Page Footer -->    

    </div> <!--! end of #container -->



    <div id="div_questionmark_dialog" title="<%=brandDisplayName%> Help" style="display: none;">
        <div id="questionmark_help_preface">
        If you are new to <%=brandDisplayName%>,<br/>
        Here's quick instruction:
        </div>
        <div id="questionmark_help_main">
<%
if( BrandingHelper.getInstance().isBrandCannyURL() || BrandingHelper.getInstance().isBrandQuickNavURL() ) {
%>
        <p>
        <%=brandDisplayName%> (tm) is a safe URL shortener service.
        You can create a short URL by specifying a target (long) URL.
        You can optionally include a "message" for the viewers (e.g., users who click on the short URL link).
        The short URLs are "safe" in that the short URL is not automatically redirected to the target Web page.
        Viewers can view which Web site the short URL is redirecting to
        and decide whether to proceed or not.
        </p>
<%
	if(BrandingHelper.getInstance().isBrandCannyURL()) {
%>

        <p>
        When the short URL is followed, 
        some basic information regarding the target Web site such as its page title is displayed
        as well as the short URL creator's message, if any.
        The user can then proceed to the Web page by pressing the "Proceed" button.
        </p>
<%
	} else if(BrandingHelper.getInstance().isBrandQuickNavURL()) {
%>
        <p>
        When the short URL is followed, 
        some basic information regarding the target Web site such as its page title is displayed
        as well as the short URL creator's message, if any.
        This short URL view page is "flashed" for a preset amount of time, e.g., 10 seconds by default.
        The creator/user can specify different values while creating the short URL. 
        When the time is up, the viewer/user will be automatically redirected to the target Web page.
        The user can opt out of the redirection by pressing the "Stop" button.
        </p>
<%
	}
}
%>

<%
	if(TwitterUtil.isTwitterSetUp(topLevelUrl)) {
%>
        <p>
        You can also tweet the short URL using the "Tweet Short URL" button directly from the <%=brandDisplayName%> web pages.
        Note that you'll need your own Twitter account in order to be able to use this feature.
        </p>
<%
	}
%>
        </div>
    </div>


<%
	if(TwitterUtil.isTwitterSetUp(topLevelUrl)) {
%>
    <div id="div_twitter_tweet_dialog" title="<%=brandDisplayName%> - Tweet" style="display: none;">
        <div id="twitter_tweet_body">
          <div id="twitter_tweet_body_tweetbox"></div>
        </div>
    </div>
<%
	}
%>


    <div id="div_status_floater" style="display: none;" class="alert">
    </div>
 

    <!-- JavaScript at the bottom for fast page loading -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="http://ww2.filestoa.com/js/jquery/jquery-1.8.3.min.js"><\/script>')</script>
	<script src="http://ww2.filestoa.com/js/jquery/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="http://ww2.filestoa.com/js/jquery/plugin/jquery.floatobject-1.4.js"></script>
    <script src="http://ww2.filestoa.com/js/jquery/plugin/jquery-ui-timepicker-addon.js"></script>
    <script src="http://ww2.filestoa.com/js/jquery/plugin/history.js/uncompressed/history.adapter.jquery.js"></script>
    <script src="http://ww2.filestoa.com/js/jquery/plugin/history.js/uncompressed/history.js"></script>
    <script src="http://ww2.filestoa.com/js/jquery/plugin/history.js/uncompressed/history.html4.js"></script>
    <script src="http://ww2.filestoa.com/js/bootstrap/bootstrap-2.0.js"></script>
    <script type="text/javascript" src="/js/core/debugutil.js"></script>
    <script type="text/javascript" src="http://ww2.filestoa.com/js/core/datetimeutil-1.0.js"></script>
    <script type="text/javascript" src="http://ww2.filestoa.com/js/core/emailutil-1.0.js"></script>
    <script type="text/javascript" src="/js/bean/keywordlinkjsbean.js"></script>
	<script defer type="text/javascript" src="/js/plugins.js"></script>
    <script defer type="text/javascript" src="/js/script.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/fiveten-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/statushelper-1.1.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/tweethelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/signuphelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/feedbackhelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/emailhelper-1.0.js"></script>
    <!-- end scripts-->


    <script>
    // Global vars
    var feedbackHelper;
    $(function() {
    	var targetService = "<%=brandDisplayName%>";
    	var targetPage = "shorten";
    	feedbackHelper = new webstoa.FeedbackHelper(targetService, targetPage);
    });
    </script>

    <script>
    // Email address "decoding"...
    $(function() {
        $("a").each(function() {
        	var anchor = $(this);
        	webstoa.EmailUtil.decodeMailtoAnchor(anchor);
        });
    });
    </script>

    <script>
    $(function() {
        $("#anchor_feedback_footer").click(function() {
            if(DEBUG_ENABLED) console.log("Feedback button clicked."); 
            var targetItem = 'footer';
            feedbackHelper.feedback(targetItem);
		    return false;
        });
    });
    </script>


    
    <script>
    // Function definitions.
    function updateStatus(msg, type, time) {
    	if(!time) {
    		time = 5000;   // milliseconds.
    	}
    	if(!type) {
    		// type = "info";  // warning, error, etc....
    	}
    	//$("#div_status_floater").text(msg);
    	//$("#div_status_floater").fadeIn().delay(time).fadeOut();
		$("#div_status_floater").textInOut(msg, type, time);
    }    
    function countKeywordMemoChar() {
    	var max = 500;   // TBD: Depending on the contentType...
    	var value = $('#input_shorturl_keyword_memo').val();
        var len = value.length;
    	var remaining = (max - len) >= 0 ? (max - len) : 0; 
        if (len > max) {
         	$('#input_shorturl_keyword_memo').val(value.substring(0, max));
        } else {
         	//$('#input_shorturl_keyword_memo').val(value);
        }
        $('#keyword_memo_char_counter').text(remaining);
    }
    function countTitleChar() {
    	var max = 400;    // TBD
    	var value = $('#input_shorturl_longurl').val();
        var len = value.length;
    	var remaining = (max - len) >= 0 ? (max - len) : 0; 
        if (len > max) {
         	$('#input_shorturl_longurl').val(value.substring(0, max));
        } else {
         	//$('#input_shorturl_longurl').val(value);
        }
        $('#title_char_counter').text(remaining);
    }
    </script>

    <script>
    function disableSaveButton(delay)
    {
    	$("#button_shorturl_save").attr('disabled','disabled');
    	var milli;
    	if(delay) {
    		milli = delay;
    	} else {
    		milli = 7500;  // ???
    	}
    	setTimeout(resetAndEnableSaveButton, milli);
    }
    function enableSaveButton()
    {
    	setTimeout(resetAndEnableSaveButton, 1250);  // ???
    }
    function resetAndEnableSaveButton()
    {
    	$("#button_shorturl_save").removeAttr('disabled');
    }
    </script>

    <script>
    // "Init"
    $(function() {
<%if(keywordLinkBean == null) {%>
        $("#input_shorturl_longurl").focus().select();
<%} else {%>
        $("#input_shorturl_longurl").focus();
<%}%>
        // TBD: initially hide $("#div_status_floater") ...
    	$("#div_status_floater").text("").hide();
    
        // Background image test.
        //$("#container").css("background-image", 'url(/img/bg/snowycabin.jpg)');
        //$(document.body).css("background", "grey");

        // TBD: Location...
    	var params = {x: 510, y: 55, speed: 'normal' };
    	$("#div_status_floater").makeFloat(params);
    	
    	// Peeng?
    	//var fiveTenTimer = new fiveten.FiveTenTimer(19771, 5);   // ???
    	//fiveTenTimer.start();
    });
    $(function() {
    	// TBD: Activate this only if shortUrl.type == shortTextContent ???
    	//countKeywordLinkChar();
        //$("#input_shorturl_keyword_memo").keyup(function(event) {
		//	countKeywordLinkChar();
        //});
    });
    $(function() {
    	countTitleChar();
        $("#input_shorturl_longurl").keyup(function(event) {
			countTitleChar();
        });
        countKeywordMemoChar();
        $("#input_shorturl_keyword_memo").keyup(function(event) {
        	countKeywordMemoChar();
        });
    });
    </script>

    <script>
    // Global constants.
    var customTokenMinLength = <%=ConfigUtil.getCustomTokenMinLength()%>;
    var customTokenMaxLength = <%=ConfigUtil.getCustomTokenMaxLength()%>;
    
    // App-related vars.
    var userGuid;
    var keywordLinkJsBean;
    var targetPageTitle = '';
    var targetPageDescription = '';
    var editState = 'state_new';
<%if(userId != null) {%>
    userGuid = "<%=userId%>";
<%}%>
<%if(keywordLinkBean == null) {%>
    keywordLinkJsBean = new cannyurl.wa.bean.KeywordLinkJsBean();
    if(userGuid) {
    	keywordLinkJsBean.setUser(userGuid);
    }
    // ....
<%} else {%>
    editState = 'state_edit';
    var shortUrlJsObjectStr = '<%=keywordLinkBean.toEscapedJsonStringForJavascript()%>';
    if(DEBUG_ENABLED) console.log("shortUrlJsObjectStr = " + shortUrlJsObjectStr);
    keywordLinkJsBean = cannyurl.wa.bean.KeywordLinkJsBean.fromJSON(shortUrlJsObjectStr);
<%}%>

    if(DEBUG_ENABLED) console.log("editState = " + editState);
    if(DEBUG_ENABLED) console.log("keywordLinkJsBean = " + keywordLinkJsBean.toString());
    </script>

    <script>
    // Additional Init based on editState.
    $(function() {
      if(editState == 'state_new') {
        $("#button_shorturl_save").text("Create");
        $("#input_shorturl_options_token_type").show();
        $("#button_shorturl_verify_shorturl").hide();
    	$("#button_shorturl_view_shorturl").hide();
    	$("#button_shorturl_tweet").hide();
        $("#button_shorturl_new").hide();
        $("#input_shorturl_longurl").removeAttr('readonly');
    	$(document.body).css("background", "silver");
    	$("#container").css("background", "white");
    	targetPageTitle = '';
    	targetPageDescription = '';
      } else if(editState == 'state_edit') {
        $("#button_shorturl_save").text("Update");
        $("#input_shorturl_options_token_type").hide();
    	$("#button_shorturl_verify_shorturl").show();
    	$("#button_shorturl_view_shorturl").show();
    	$("#button_shorturl_tweet").show();
    	$("#button_shorturl_options").show();
        $("#button_shorturl_new").show();
        $("#input_shorturl_longurl").attr('readonly','readonly');
    	$(document.body).css("background", "silver");
    	$("#container").css("background", "ivory");
    	setTargetPageTitleAndDescription();
      } else {
    	// 'state_read'  // TBD...
        $("#button_shorturl_save").hide();
        $("#input_shorturl_options_token_type").show();
    	$("#button_shorturl_verify_shorturl").hide();
    	$("#button_shorturl_view_shorturl").hide();
    	$("#button_shorturl_tweet").hide();
    	$("#button_shorturl_options").show();
        $("#button_shorturl_new").hide();
        $("#input_shorturl_longurl").removeAttr('readonly');
    	$(document.body).css("background", "silver");
    	$("#container").css("background", "gray");
    	targetPageTitle = '';
    	targetPageDescription = '';
    	// TBD: make title/content readonly...
      }
    });
    </script>

    <script>
    // Event handlers
    $(function() {
    	
    	$("#cannyurl_questionmark_link").click(function() {
    	    if(DEBUG_ENABLED) console.log("Question Mark link clicked.");

    	    $( "#div_questionmark_dialog" ).dialog({
                height: 360,
                width: 560,
                modal: true,
                buttons: {
                     OK: function() {
                         $(this).dialog('close'); 
                     }
                }
            });

    		return false;
    	});
    	
    	$("#button_shorturl_verify_shorturl").click(function() {
    	    if(DEBUG_ENABLED) console.log("ShortUrl verify button clicked.");
    		if(keywordLinkJsBean) {
   		        updateStatus('Opening the verify page ...', 'info', 5550);
                // TBD....
                var redirectUrlHelper = new cannyurl.redirecturl.RedirectUrlHelper(keywordLinkJsBean);
                if(redirectUrlHelper.isReady()) {
                    var verifyRedirectUrl = redirectUrlHelper.verifyUrl();
                    window.location = verifyRedirectUrl;
                } else {
            	    if(DEBUG_ENABLED) console.log("ShortUrl verify failed.");
       		        updateStatus('Failed to open the verify page. ...', 'error', 5550);
                }
    		} else {
        	    if(DEBUG_ENABLED) console.log("ShortUrl verify cannot be performed.");
   		        updateStatus('Verify page cannot be opened. ...', 'error', 5550);
    		}
    		return false;
    	});
        
    	$("#button_shorturl_view_shorturl").click(function() {
    	    if(DEBUG_ENABLED) console.log("ShortUrl view button clicked.");
    		var shortUrl = $("#input_shorturl_shorturl").val();
    		if(shortUrl != '') {
   		        updateStatus('Redirecting ...', 'info', 5550);
	    		window.location = shortUrl;
    		}
    		return false;
    	});

	    
	    $("#button_shorturl_new").click(function() {
	      if(DEBUG_ENABLED) console.log("New button clicked.");
	      var shortUrlNewPage = "<%=topLevelUrl%>" + "edit/";
	      var ans = window.confirm("Create a new short URL?");
	      if(ans) {
		      updateStatus('Creating a new short URL...', 'info', 5550);
	 	      window.location = shortUrlNewPage;
		    }
	    });

    });
    </script>

    <script>
    // Ajax form handlers.
  $(function() {
    $("#button_shorturl_save").click(function() {
      // TBD:
      // Disable the button here
      // and enable again (some delay) after ajax call returns
      //     or it times out (because the call has failed, etc.).
      
    
      //if(DEBUG_ENABLED) console.log("Save button clicked.");
      updateStatus('Saving the short URL...', 'info', 5550);
      disableSaveButton();
    	
      saveKeywordLinkJsBean();

    });
  });
  </script>

  <script>
  var saveKeywordLinkJsBean = function() {

      // validate and process form here      
      var longUrl = $("#input_shorturl_longurl").val().trim();
      if(DEBUG_ENABLED) console.log("longUrl = " + longUrl);
      if(longUrl == "") {    // TBD: Check if longUrl is a valid URL...  (Note: Missing http:// is appended on the server side...)
          if(DEBUG_ENABLED) console.log("longUrl is empty");
          updateStatus('Long URL cannot be empty.', 'error', 5550);
          enableSaveButton();
    	  return false;
      }
      keywordLinkJsBean.setLongUrl(longUrl);
      if(userGuid) {   
         keywordLinkJsBean.setUser(userGuid);  // This should not be necessary..
      }

      // Update the beans with input values
      
      var keywordMemo = $("#input_shorturl_keyword_memo").val().trim();
      if(DEBUG_ENABLED) console.log("keywordMemo = " + keywordMemo);
      keywordLinkJsBean.setMemo(keywordMemo);
      
   	  // Update the modifiedTime field ....
   	  if(editState != 'state_new') {
   		  var modifiedTime = (new Date()).getTime();
          keywordLinkJsBean.setModifiedTime(modifiedTime);
   	  }

      // Payload...
      var shortUrlJsonStr = JSON.stringify(keywordLinkJsBean);
      if(DEBUG_ENABLED) console.log("shortUrlJsonStr = " + shortUrlJsonStr);
      var payload = "{\"keywordLink\":" + shortUrlJsonStr;
      // TBD...
      payload += "}";
      if(DEBUG_ENABLED) console.log("payload = " + payload);
      
      
      // Web service settings.
      var shortUrlPostEndPoint = "<%=topLevelUrl%>" + "ajax/keywordlink";
      if(DEBUG_ENABLED) console.log("shortUrlPostEndPoint =" + shortUrlPostEndPoint);
      var httpMethod;
      var webServiceUrl;
      if(editState == 'state_new') {
    	  httpMethod = "POST";
    	  webServiceUrl = shortUrlPostEndPoint;
	  } else if(editState == 'state_edit') {
    	  httpMethod = "PUT";
    	  webServiceUrl = shortUrlPostEndPoint + "/" + keywordLinkJsBean.getGuid();
      } else {
    	  // ???
   		  if(DEBUG_ENABLED) console.log("Error. Invalid editState = " + editState);
          updateStatus('Error occurred.', 'error', 5550);
          enableSaveButton();
    	  return false;
      }

      if(httpMethod) {
        $.ajax({
    	    type: httpMethod,
    	    url: webServiceUrl,
    	    data: payload,
    	    dataType: "json",
    	    contentType: "application/json; charset=UTF-8",
    	    success: function(data, textStatus) {
    	      // TBD
    	      if(DEBUG_ENABLED) console.log("keywordLinkJsBean successfully saved. textStatus = " + textStatus);
    	      if(DEBUG_ENABLED) console.log("data = " + data);
    	      if(DEBUG_ENABLED) console.log(data);

    	      // History
    	      // Do this only if the current editState == 'state_new'
    	      if(editState == 'state_new') {
	    	      var History = window.History;
	    	      if(History) {
	   	    	    	// TBD: Need to html escape????
	    				var htmlPageTitle = '<%=brandDisplayName%> - ' + keywordLinkJsBean.getLongUrl();
	    	            // TBD: This does not work on HTML4 browsers (e.g., IE)
				        History.pushState({state:1}, htmlPageTitle, "/edit/" + keywordLinkJsBean.getGuid());
	    	      }
    	      }
    	      
    	      // If it is currently editState == 'state_new', change it to 'state_edit'.
    	      // If it is currently editState == 'state_edit', that's ok too.
    	      editState = 'state_edit';

    	      // Parse data...
    	      if(data) {   // POST only..
	    	      // Replace/Update the JsBeans ...

	    	      var shortUrlObj = data["keywordLink"];
	    	      if(DEBUG_ENABLED) console.log("shortUrlObj = " + shortUrlObj);
	    	      if(DEBUG_ENABLED) console.log(shortUrlObj);
	    	      keywordLinkJsBean = cannyurl.wa.bean.KeywordLinkJsBean.create(shortUrlObj);
	    	      if(DEBUG_ENABLED) console.log("New keywordLinkJsBean = " + keywordLinkJsBean);
	    	      if(DEBUG_ENABLED) console.log(keywordLinkJsBean);
	    
	    	      // Delay this just a bit so that the server has time to actually save it (Note: we are using async call for saving).
	    	      // This helps when a new shortUrl is saved. 
	    	      // TBD: In both save/update, various buttons should be disabled before the ajax call
	    	      //   and they should be re-enabled after the call successuflly returns or after certain preset time.
	    	      window.setTimeout(refreshFormFields, 1250);
	              updateStatus('Short URL successfully saved.', 'info', 5550);
    	      } else {
    	    	  // ignore
    	    	  // PUT currently returns "OK" only.
    	    	  // TBD: Change the ajax server side code so that it returns the updated data.
    	    	  //      Based on the returned/updated data, some fields (e.g., shortUrl) may need to be updated....
	              updateStatus('Short URL successfully updated.', 'info', 5550);
    	      } 
    	      enableSaveButton();
    	      
    	      // For performance heck!!!!
    	      // "Finger" PageSynopsis page here ...
    	      // Note: This also updates targetPageTitle/Description global variables.....
    	      fetchPageInfo(longUrl);
    	      // Heck!!!
    	    },
    	    error: function(req, textStatus) {
    	    	var errorMsg = 'Failed to save the short URL. status = ' + textStatus;
          	    if(DEBUG_ENABLED) console.log(errorMsg);

          	    if(keywordLinkJsBean && keywordLinkJsBean.getTokenType() == 'custom') {
    	    		errorMsg += ". Try a differnt custom token.";
    	    	}
    	    	updateStatus(errorMsg, 'error', 5550);
    	    	enableSaveButton();
    	    }
	   	  });
      } else {
    	  // ???
    	  enableSaveButton();
      }

  };
  </script>

  <script>
  // Refresh input fields...
  var refreshFormFields = function() {
	if(DEBUG_ENABLED) console.log("refreshFormFields() called......");

	if(editState == 'state_new') {
		if(DEBUG_ENABLED) console.log("editState = " + editState);

        $("#button_shorturl_save").text("Create");
        $("#input_shorturl_options_token_type").show();
    	$("#button_shorturl_verify_shorturl").hide();
    	$("#button_shorturl_view_shorturl").hide();
    	$("#button_shorturl_tweet").hide();
    	$("#button_shorturl_options").show();
        $("#button_shorturl_new").hide();
        $("#input_shorturl_longurl").removeAttr('readonly');
        $("#input_shorturl_options_custom_token").removeAttr('readonly');
    	$(document.body).css("background", "silver");
    	$("#container").css("background", "white");
        targetPageTitle = '';
        targetPageDescription = '';

        // Refresh the title...
		var htmlPageTitle = '<%=brandDisplayName%>';
        $('title').text(htmlPageTitle);
	} else if(editState == 'state_edit') {
		if(DEBUG_ENABLED) console.log("editState = " + editState);

		var htmlPageTitle = '<%=brandDisplayName%>';
		if(keywordLinkJsBean) {
			var title = keywordLinkJsBean.getLongUrl();
			htmlPageTitle += ' - ' + title;    // TBD: Need to html escape????

		    // TBD:
			var shortUrl = keywordLinkJsBean.getShortUrl();
			$("#input_shorturl_shorturl").val(shortUrl);
			var longUrl = keywordLinkJsBean.getLongUrl();    // LongURL might have been modified on the server side (e.g., http;// added, etc.)
			$("#input_shorturl_longurl").val(longUrl);
			// Refresh other fields????
			
			// TBD: Update the shortUrl title???
		    // Is that necessary?
			
	        $("#button_shorturl_save").text("Update");
	        $("#input_shorturl_options_token_type").hide();
	    	$("#button_shorturl_verify_shorturl").show();
	    	$("#button_shorturl_view_shorturl").show();
	    	$("#button_shorturl_tweet").show();
	    	$("#button_shorturl_options").show();
	        $("#button_shorturl_new").show();
	        $("#input_shorturl_longurl").attr('readonly','readonly');;
	        $("#input_shorturl_options_custom_token").attr('readonly','readonly');
	    	$(document.body).css("background", "silver");
	    	$("#container").css("background", "ivory");
            setTargetPageTitleAndDescription();
		} else {
			// ???
		}	

		// Refresh the title...
        $('title').text(htmlPageTitle);
	} else {
		if(DEBUG_ENABLED) console.log("invalid state: editState = " + editState);

		// ????
		// Refresh the title...
		var htmlPageTitle = '<%=brandDisplayName%>';
        $('title').text(htmlPageTitle);
	}
  };
  </script>

  <script>
  // Primarily for "fingering" PageSynopsis site.
  var fetchPageInfo = function(longUrl) {
    if(DEBUG_ENABLED) console.log("fetchPageInfo() called......");
    
    // TBD:
    // Check first if longUrl is a valid URL...
    // temporary
    var urlregex = new RegExp("^http");
    if(!urlregex.test(longUrl)) {
    	if(DEBUG_ENABLED) console.log("longUrl invalid. Skipping fetchPagInfo(): longUrl = " + longUrl);
    	return;
    }
    // ....

    var encodedLongUrl = encodeURIComponent(longUrl);
    $.ajax({
	    type: 'GET',
	    url: 'http://www.pagesynopsis.com/pageinfo?targetUrl=' +  encodedLongUrl,
	    dataType: "jsonp",
	    timeout: 20000,
	    jsonp: 'callback',
	    success: function(data, textStatus) {
	        if(DEBUG_ENABLED) console.log("PageInfo successfully fetched. textStatus = " + textStatus );
		    //if(DEBUG_ENABLED) console.log("data = " + data);
		    //if(DEBUG_ENABLED) console.log(data);

		    // Parse data...
		    if(data) {
		        // TBD: We can optionally display Page info on the Edit page as well
		        //      e.g., as a popup, or bubble help, etc...
		        // ....

		        var pageTitle = data.pageTitle;
		        var pageAuthor = data.pageAuthor;
		        var pageDescription = data.pageDescription;
			    if(DEBUG_ENABLED) console.log('pageTitle = ' + pageTitle);
			    if(DEBUG_ENABLED) console.log('pageAuthor = ' + pageAuthor);
			    if(DEBUG_ENABLED) console.log('pageDescription = ' + pageDescription);
			    // ...
		        
			    // Save it to the global var....
			    if(pageTitle) {
			    	targetPageTitle = pageTitle;
			    } else {
				    //targetPageTitle = '';   // ????
			    }
			    if(pageDescription) {
			    	targetPageDescription = pageDescription;
			    } else {
				    //targetPageDescription = '';   // ????
			    }
		    } 
	    },
	    error: function(xhr, textStatus) {
	        if(DEBUG_ENABLED) console.log("PageInfo fetch failed. textStatus = " + textStatus );
	    }
    });
  };
  
  $(function() {
	  // ...
  });
  </script>

  
 
<%
if(TwitterUtil.isTwitterSetUp(topLevelUrl)) {
%>
    <script>
    $(function() {

      $("#button_shorturl_tweet").click(function() {
        if(DEBUG_ENABLED) console.log("Tweet button clicked."); 
        
		//var shortUrl = keywordLinkJsBean.getShortUrl();
		var shortUrl = $("#input_shorturl_shorturl").val();
        //var keywordMemo = keywordLinkJsBean.getMemo();
		var keywordMemo = $("#input_shorturl_keyword_memo").val();
		tweetShortUrlAndMessage(shortUrl, keywordMemo);
		return false;
      });
    
    });
  </script>
  
  <script>
    var tweetShortUrlAndMessage = function(shortUrl, keywordMemo) {
        var defaultTweetMessage = shortUrl + ' - ';
        var trailingMessage = keywordMemo;
        if(!trailingMessage) {
        	trailingMessage = targetPageDescription;
        }
        if(!trailingMessage) {
        	trailingMessage = targetPageTitle;
        }
        
        if(trailingMessage) {
            var rem = 140 - defaultTweetMessage.length - 5;  // 2=5-3: buffer...
            var len = trailingMessage.length;
            //var pad = Math.min(rem, len);
            if(len <= rem) {
                defaultTweetMessage += trailingMessage;            	
            } else {
                defaultTweetMessage += trailingMessage.substring(0, rem) + '...';
            }
		}

		// Hack!!!!
        $("#twitter_tweet_body").append('<div id="twitter_tweet_body_tweetbox_dynamic"></div>');
        twttr.anywhere(function (T) {
            T("#twitter_tweet_body_tweetbox_dynamic").tweetBox({
              height: 80,
              width: 400,
              label: "Tweet this short URL with message",
              defaultContent: defaultTweetMessage,
              onTweet: onTweetSuccess
            });
          });

	    $( "#div_twitter_tweet_dialog" ).dialog({
            height: 270,
            width: 450,
            modal: true,
            buttons: {
                Close: function() {
                    $(this).dialog('close'); 
                }
            }
        });

		return false;
      };

    $(function() {
      // Hack!!!!
      $("#div_twitter_tweet_dialog").bind('dialogclose', function(event) { 
          if($("#twitter_tweet_body_tweetbox_dynamic")) {
       	    $("#twitter_tweet_body_tweetbox_dynamic").remove();
          }
      }); 
       
    });

    var onTweetSuccess = function(plainTwt, htmlTwt) {
    	if(DEBUG_ENABLED) console.log("onTweetSuccess(): plainTwt = " + plainTwt);
        updateStatus('Your message has been successfully tweeted', 'info', 5550);
    	$( "#div_twitter_tweet_dialog" ).dialog('close');
    };
  </script>
<%
}
%>



<script>
  var setTargetPageTitleAndDescription = function() {
	  // For now, it does notthing...
	  // fetchPageInfo() is called when the short link is saved the first time...

	  // This may still need to be called for the cases when /edit/<guid> is directly loaded...  Ignore for now....
	  //fetchPageInfo();
  };
</script>


<%
String googleAnalyticsCode = AnalyticsHelper.getInstance().getGoogleAnalyticsCode();
if(googleAnalyticsCode != null && !googleAnalyticsCode.isEmpty()) {    // And, if it is enabled (e.g. through config). TBD...
%>
    <!-- Change UA-XXXXX-X to be your site's ID -->
    <script>
      window._gaq = [['_setAccount','<%=googleAnalyticsCode%>'],['_trackPageview'],['_trackPageLoadTime']];
      Modernizr.load({
        load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
      });
    </script>
<%
}
%>

    <!-- Prompt IE 6 users to install Chrome Frame. Remove this if you want to support IE 6.
       chromium.org/developers/how-tos/chrome-frame-getting-started -->
    <!--[if lt IE 7 ]>
      <script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
      <script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
    <![endif]-->

  </body>
</html>