<%@ page import="com.myurldb.ws.core.*, com.myurldb.ws.*, com.cannyurl.af.util.*, com.cannyurl.af.auth.common.*, com.cannyurl.af.auth.*, com.cannyurl.af.auth.user.*, com.cannyurl.fe.*, com.cannyurl.fe.core.*, com.cannyurl.fe.bean.*, com.cannyurl.fe.util.*, com.cannyurl.wa.service.*, com.cannyurl.common.*, com.cannyurl.util.*, com.cannyurl.app.auth.*, com.cannyurl.app.util.*, com.cannyurl.helper.*" 
%><%@ page contentType="text/html; charset=UTF-8" 
%><%@ page errorPage="/error/UnknownError" 
%><%// {1] Session
SessionBean sessionBean = UserSessionManager.getInstance().setSessionBean(request, response);
String sessionToken = sessionBean.getToken();
String userId = sessionBean.getUserId();

// [2] Parse url.
String contextPath = request.getContextPath();
String servletPath = request.getServletPath();
String requestHost = request.getServerName();
//String requestURI = request.getRequestURI();
String requestUrl = request.getRequestURL().toString();
String topLevelUrl = UrlHelper.getInstance().getTopLevelURLFromRequestURL(requestUrl);
String pathInfo = request.getPathInfo();
String queryString = request.getQueryString();

%><%
// "Branding"
String appBrand = BrandingHelper.getInstance().getAppBrand();
String brandDisplayName = BrandingHelper.getInstance().getBrandDisplayName();
String brandDescription = BrandingHelper.getInstance().getBrandDescription();
%><%String themeBootstrap = SiteThemeHelper.getInstance().getBootstrapTheme();
String themeJqueryUi = SiteThemeHelper.getInstance().getJqueryUiTheme();%><%
AuthUserService authUserService = AuthUserServiceFactory.getAuthUserService();
AuthUser authUser = authUserService.getCurrentUser(); // or req.getUserPrincipal()
String nickName = "";
String loginUrl = null;
String logoutUrl = null;
String comebackUrl = null;
String encodedComebackUrl = null;
if(authUser != null) {
    nickName = authUser.getNickname(); 
    logoutUrl = authUserService.createLogoutURL(request.getRequestURI());
} else {
    comebackUrl = request.getRequestURI();
    encodedComebackUrl = java.net.URLEncoder.encode(comebackUrl, "UTF-8");
    loginUrl = "/auth?comebackUrl=" + encodedComebackUrl;
}
%><%
String twitterComebackUrl = null;
String encodedTwitterComebackUrl = null;
twitterComebackUrl = request.getRequestURI();
try {
    encodedTwitterComebackUrl = java.net.URLEncoder.encode(twitterComebackUrl, "UTF-8");
} catch(Exception e) {
    // ignore
}
//if(encodedTwitterComebackUrl == null) {
//    encodedTwitterComebackUrl = "";  // ???
//}
%><%
String twitterFallbackUrl = "/error/Unauthorized";
String encodedTwitterFallbackUrl = null;
try {
    encodedTwitterFallbackUrl = java.net.URLEncoder.encode(twitterFallbackUrl, "UTF-8");
} catch(Exception e) {
    // ignore
}
//if(encodedTwitterFallbackUrl == null) {
//    encodedTwitterFallbackUrl = "";   // ????
//}
%><%
String twitterLoginPageUrl = "/twitter/login";
if(encodedTwitterComebackUrl != null) {
    twitterLoginPageUrl += "?comebackUrl=" + encodedTwitterComebackUrl;
	if(encodedTwitterFallbackUrl != null) {
	    twitterLoginPageUrl += "&fallbackUrl=" + encodedTwitterFallbackUrl;
	}
} else {
	if(encodedTwitterFallbackUrl != null) {
	    twitterLoginPageUrl += "?fallbackUrl=" + encodedTwitterFallbackUrl;
	}
}
%><%
// For Twitter twitterLogin....
// TBD: This should be done using a filter...
String providerId = CommonAuthUtil.AUTH_SERVICE_PROVIDERID_TWITTER;
// boolean isUserAuthenticated = AuthManager.getInstance().isUserLoggedIn(session, providerId);
boolean isUserAuthenticated = false;
String twitterUserUsername = null;
ExternalUserIdStruct externalUserId = AuthManager.getInstance().getExternalUserIdStruct(session, providerId);
if(externalUserId != null) {
    isUserAuthenticated = true;
    twitterUserUsername = externalUserId.getUsername();
} else {
    // externalUserId == null does not mean the user is not authenticated....
    isUserAuthenticated = AuthManager.getInstance().isUserLoggedIn(session, providerId);
}

%><%
// if( BrandingHelper.getInstance().isBrandUserURL() 
//     || BrandingHelper.getInstance().isBrandTwrim() ) {
//     if(isUserAuthenticated == false) {
//         // ????
//         response.sendRedirect(twitterLoginPageUrl);
//     }
// } else {
//     if(authUser == null) {
//         // ????
//         response.sendRedirect(loginUrl);
//     }
// }
%><%

// if(twitterUserUsername != null) {
//     twitterUserUsername = "";      // ???
// }
String twitterHandle = "";
String twitterUserProfileUrl = "";
if(twitterUserUsername != null && !twitterUserUsername.isEmpty()) {
    twitterHandle = "@" + twitterUserUsername;      // ???
    twitterUserProfileUrl = "http://twitter.com/" + twitterUserUsername;
}

%><%
// ...
%><!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!-- Consider adding an manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<html>
  <head>
    <meta charset="utf-8">
    <title><%=brandDisplayName%> | Nav Dashboard</title>
    <meta name="author" content="Aery Software">
    <meta name="description" content="<%=AppBrand.getBrandDescription(appBrand)%>. It also provides URL redirect / URL Shortener Web Services API. But, <%=brandDisplayName%> is so much more than a URL shortener or URL redirection application." />

    <!-- Mobile viewport optimized: j.mp/bplateviewport -->
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <link rel="shortcut icon" href="/img/favicon-<%=appBrand%>.ico" />
    <link rel="icon" href="/img/favicon-<%=appBrand%>.ico" type="image/x-icon" />

    <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- CSS: implied media=all -->
    <%@ include file="/fragment/style.jspf" %>
    <!-- end CSS-->

    <!-- All JavaScript at the bottom, except for Modernizr / Respond.
       Modernizr enables HTML5 elements & feature detects; Respond is a polyfill for min/max-width CSS3 Media Queries
       For optimal performance, use a custom Modernizr build: www.modernizr.com/download/ -->
    <script src="http://ww2.filestoa.com/js/modernizr/modernizr-2.0.6.min.js"></script>

  </head>

  <body>

<jsp:include page="/fragment/TopNavbar">
<jsp:param name="pageCode" value="navlink/dashboard"/>
<jsp:param name="brandDisplayName" value="<%=brandDisplayName%>"/>
</jsp:include>


<!--  
    <div id="page_header" class="page_header_top">
    &nbsp;
	</div>
-->

    <div id="container">
      <div id="main" role="main">
      <form id="form_navkeyword_view" method="POST" action="">

        <table>
        <tr>
        <td>
        <div id="cannyurl_logo">
	 	    <h1><%=brandDisplayName%><sup>&beta;</sup></h1>
        </div>
        </td>
        <td style="vertical-align:top;">
            <div id="cannyurl_questionmark" style="margin:-5px -35px 0px -5px;">
                <a href="#" id="cannyurl_questionmark_link"><img src="/img/question-50.png" alt="<%=brandDisplayName%> Help" title="<%=brandDisplayName%> Help"></img></a>
	 	    </div>
        </td>
        </tr>
        </table>

      <div id="cannyurl_view_header">
      <fieldset name="fieldset_navkeyword_header">
      <table>
      <tr>
      <td>
          <label class="input_label_text" for="input_navkeyword_shorturl" id="input_navkeyword_shorturl_label" title="Input short URL">Keyword:</label>
      </td>
      <td>
        <div id="cannyurl_view_shorturl">
          <input type="text" name="input_navkeyword_shorturl" id="input_navkeyword_shorturl" size="35"></input>        
          <input type="hidden" name="hidden_navkeyword_shorturl" id="hidden_navkeyword_shorturl"></input>
          <button type="button" name="button_navkeyword_nav" id="button_navkeyword_nav" title="Got to this keyword" style="width:70px">Go</button>  
          <button type="button" name="button_navkeyword_check" id="button_navkeyword_check" title="Verify this keyword" style="width:70px">Check</button>  
          <button type="button" name="button_navkeyword_browse" id="button_navkeyword_browse" title="Browse this keyword" style="width:70px">Browse</button>  
        </div>
      </td>
      </tr>
      </table>
      </fieldset>
      </div>     
      

      <div id="cannyurl_body">
      <fieldset name="fieldset_navkeyword_body">

        <div id="cannyurl_navigation_scope">
        <table>
        <tr>
        <td style="width:220px;">
			<table>

<%
    java.util.List<String> defaultScopes = new java.util.ArrayList<String>();  // temporary
    defaultScopes.add(KeywordScope.SCOPE_USER);
    //defaultScopes.add(KeywordScope.SCOPE_GROUP);
    for( String[] scope : KeywordScope.TYPES ) {
        String val = scope[0];
        String name = scope[1];
        if(!val.equals(KeywordScope.SCOPE_CROWD) && !val.equals(KeywordScope.SCOPE_SYSTEM)) {
%>
			<tr>
				<td>
<%
            if(defaultScopes.contains(val)) {
%>
	            <input type="checkbox" name="navscope" value="<%=val%>" checked="checked" /> <%=name %>
<%
            } else {
%>
	            <input type="checkbox" name="navscope" value="<%=val%>"/> <%=name %>
<%
            }
%>
				</td>
				<td>&nbsp;
				</td>
			</tr>
<%
        }
    }
%>			

			</table>
        </td>
        <td style="vertical-align:bottom; width:200px;">

          <button type="button" name="button_navkeyword_scope_default" id="button_navkeyword_scope_default" title="Set this as my default" style="width:120px">Set As Default</button> 

        </td>
        </tr>
        </table>
        </div>

      </fieldset>
      </div>


      <div id="cannyurl_footer">
      <fieldset name="fieldset_navkeyword_footer">

        <div id="cannyurl_extra_menu">
          <button type="button" name="button_navkeyword_list_mine" id="button_navkeyword_list_mine" title="List my keywords" style="width:100px">Show Mine</button> 
          <button type="button" name="button_navkeyword_list_glabal" id="button_navkeyword_list_gloabl" title="List global keywords" style="width:100px">Show Global</button> 
          <button type="button" name="button_navkeyword_list_all" id="button_navkeyword_list_all" title="List all users' keywords" style="width:100px">Show All</button> 
          <button type="button" name="button_navkeyword_new" id="button_navkeyword_new" title="Create a brand new keyword" style="width:100px">New</button> 
        </div>

      </fieldset>
      </div>

      </form>
      </div>
      
<!-- Page Footer -->    
<%@ include file="/fragment/Footer.jspf" %>
<!-- Page Footer -->    

    </div> <!--! end of #container -->


    

    <div id="div_questionmark_dialog" title="<%=brandDisplayName%> Help" style="display: none;">
        <div id="questionmark_help_preface">
        If you are new to <%=brandDisplayName%>,<br/>
        Here's quick instruction:
        </div>
        <div id="questionmark_help_main">
        TBD
        </div>
    </div>



    <div id="div_status_floater" style="display: none;" class="alert">
    </div>
 

    <!-- JavaScript at the bottom for fast page loading -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="http://ww2.filestoa.com/js/jquery/jquery-1.8.3.min.js"><\/script>')</script>
	<script src="http://ww2.filestoa.com/js/jquery/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="http://ww2.filestoa.com/js/jquery/plugin/jquery.floatobject-1.4.js"></script>
    <script src="http://ww2.filestoa.com/js/jquery/plugin/jquery-ui-timepicker-addon.js"></script>
    <script src="http://ww2.filestoa.com/js/jquery/plugin/history.js/uncompressed/history.adapter.jquery.js"></script>
    <script src="http://ww2.filestoa.com/js/jquery/plugin/history.js/uncompressed/history.js"></script>
    <script src="http://ww2.filestoa.com/js/jquery/plugin/history.js/uncompressed/history.html4.js"></script>
    <script src="http://ww2.filestoa.com/js/bootstrap/bootstrap-2.0.js"></script>
    <script type="text/javascript" src="/js/core/debugutil.js"></script>
    <script type="text/javascript" src="http://ww2.filestoa.com/js/core/datetimeutil-1.0.js"></script>
    <script type="text/javascript" src="http://ww2.filestoa.com/js/core/emailutil-1.0.js"></script>
    <script type="text/javascript" src="/js/bean/keywordlinkjsbean.js"></script>
	<script defer type="text/javascript" src="/js/plugins.js"></script>
    <script defer type="text/javascript" src="/js/script.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/fiveten-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/statushelper-1.1.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/tweethelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/signuphelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/feedbackhelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/emailhelper-1.0.js"></script>
    <!-- end scripts-->


    <script>
    // Global vars
    var feedbackHelper;
    $(function() {
    	var targetService = "<%=brandDisplayName%>";
    	var targetPage = "shorten";
    	feedbackHelper = new webstoa.FeedbackHelper(targetService, targetPage);
    });
    </script>

    <script>
    // Email address "decoding"...
    $(function() {
        $("a").each(function() {
        	var anchor = $(this);
        	webstoa.EmailUtil.decodeMailtoAnchor(anchor);
        });
    });
    </script>

    <script>
    $(function() {
        $("#anchor_feedback_footer").click(function() {
            if(DEBUG_ENABLED) console.log("Feedback button clicked."); 
            var targetItem = 'footer';
            feedbackHelper.feedback(targetItem);
		    return false;
        });
    });
    </script>

 

    <script>
    // Function definitions.
    function updateStatus(msg, type, time) {
    	if(!time) {
    		time = 5000;   // milliseconds.
    	}
    	if(!type) {
    		// type = "info";  // warning, error, etc....
    	}
    	//$("#div_status_floater").text(msg);
    	//$("#div_status_floater").fadeIn().delay(time).fadeOut();
		$("#div_status_floater").textInOut(msg, type, time);
    }    
    </script>

    <script>
    // temporary
    function replaceURLWithHTMLLinks(text) {
        if(text) {
            var exp = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
            //var exp = /\(?\bhttp:\/\/[-A-Za-z0-9+&@#\/%?=~_()|!:,.;]*[-A-Za-z0-9+&@#/%=~_()|]/ig;
            return text.replace(exp,"<a href='$1'>$1</a>");
        }
        return '';    // ???
    }
    </script>


    <script>
    // "Init"
    $(function() {
    	// TBD: initially hide $("#div_status_floater") ...
    	$("#div_status_floater").text("").hide();
    
        // Background image test.
        //$("#container").css("background-image", 'url(/img/bg/oceansunset.jpg)');
        $(document.body).css("background", "silver");
    	$("#container").css("background", "ivory");

        // TBD: Location...
    	var params = {x: 530, y: 15, speed: 'normal' };
    	$("#div_status_floater").makeFloat(params);

    	// Peeng?
    	//var fiveTenTimer = new fiveten.FiveTenTimer(14839, 4);  // ???
    	//fiveTenTimer.start();
    });
    </script>

    <script>
    // App-related vars.
    var userGuid;
    //var accessRecordJsBean;
<%
if(userId != null) {
%>
    userGuid = "<%=userId%>";
<%
}
%>
    </script>

    <script>
    // Event handlers
    $(function() {
    	
    	$("#cannyurl_questionmark_link").click(function() {
    	    if(DEBUG_ENABLED) console.log("Question Mark link clicked.");

    	    $( "#div_questionmark_dialog" ).dialog({
                height: 360,
                width: 560,
                modal: true,
                buttons: {
                     OK: function() {
                         $(this).dialog('close'); 
                     }
                }
            });

    		return false;
    	});
    });
    </script>

    <script>
    $(function() {
        $("#button_navkeyword_nav").click(function() {
            if(DEBUG_ENABLED) console.log("Nav button clicked.");  
            
          });

        $("#button_navkeyword_browse").click(function() {
            if(DEBUG_ENABLED) console.log("Browse button clicked.");  
            
          });

        $("#button_navkeyword_list_mine").click(function() {
            if(DEBUG_ENABLED) console.log("List Mine button clicked.");  
            
          });

        $("#button_navkeyword_list_global").click(function() {
            if(DEBUG_ENABLED) console.log("List Globall button clicked.");  
            
          });

        $("#button_navkeyword_list_all").click(function() {
            if(DEBUG_ENABLED) console.log("List All button clicked.");  
            
          });

        $("#button_navkeyword_new").click(function() {
            if(DEBUG_ENABLED) console.log("New button clicked.");  
            
            var navKeywordNewPage = "<%=topLevelUrl%>" + "edit/";
            if(DEBUG_ENABLED) console.log("navKeywordNewPage = " + navKeywordNewPage);
            var ans = window.confirm("Create a new short URL?");
            if(ans) {
    	           updateStatus('Creating a new short URL...', 'info', 5550);
       	       window.location = navKeywordNewPage;
      	    }
          });

    	$("#button_navkeyword_check").click(function() {
    	    if(DEBUG_ENABLED) console.log("NavKeyword check button clicked.");

            var fullNavKeyword = $("#input_navkeyword_shorturl").val();   // This can potentially include an additional query string
    		// TBD: use "hidden_navkeyword_shorturl" (which excludes query string)  ???
    		if(fullNavKeyword != '') {
    			// TBD...

    		} else {
        	    if(DEBUG_ENABLED) console.log("NavKeyword check cannot be performed.");
   		        updateStatus('Verify page cannot be opened. ...', 'error', 5550);
    		}
    		return false;
    	});

    });
    </script>

    <script>
    // Ajax form handlers.
  $(function() {
	  // ...
  });
    </script>


<%
String googleAnalyticsCode = AnalyticsHelper.getInstance().getGoogleAnalyticsCode();
if(googleAnalyticsCode != null && !googleAnalyticsCode.isEmpty()) {    // And, if it is enabled (e.g. through config). TBD...
%>
    <!-- Change UA-XXXXX-X to be your site's ID -->
    <script>
      window._gaq = [['_setAccount','<%=googleAnalyticsCode%>'],['_trackPageview'],['_trackPageLoadTime']];
      Modernizr.load({
        load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
      });
    </script>
<%
}
%>

    <!-- Prompt IE 6 users to install Chrome Frame. Remove this if you want to support IE 6.
       chromium.org/developers/how-tos/chrome-frame-getting-started -->
    <!--[if lt IE 7 ]>
      <script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
      <script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
    <![endif]-->

    </body>
</html>