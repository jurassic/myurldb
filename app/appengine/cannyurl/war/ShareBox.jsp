<%@ page import="com.myurldb.ws.core.*, com.cannyurl.af.util.*, com.cannyurl.af.auth.*, com.cannyurl.wa.service.*, com.cannyurl.common.*, com.cannyurl.util.*, com.cannyurl.helper.*" 
%><%@ page contentType="text/html; charset=UTF-8" 
%><%@ page errorPage="/error/UnknownError" 
%><%
// {1] Session
SessionBean sessionBean = UserSessionManager.getInstance().setSessionBean(request, response);
String sessionToken = sessionBean.getToken();
String userId = sessionBean.getUserId();

// [2] Parse url.
String contextPath = request.getContextPath();
String servletPath = request.getServletPath();
String requestUrl = request.getRequestURL().toString();
String topLevelUrl = UrlHelper.getInstance().getTopLevelURLFromRequestURL(requestUrl);
String pathInfo = request.getPathInfo();
String queryString = request.getQueryString();
String editPageUrl = topLevelUrl + "shorten";
%><%
// "Branding"
String appBrand = BrandingHelper.getInstance().getAppBrand();
String brandDisplayName = BrandingHelper.getInstance().getBrandDisplayName();
String brandDescription = BrandingHelper.getInstance().getBrandDescription();
%><%String themeBootstrap = SiteThemeHelper.getInstance().getBootstrapTheme();
String themeJqueryUi = SiteThemeHelper.getInstance().getJqueryUiTheme();%><%
// TBD
String shortUrlShortUrl = "";
// ...
%><%
// temporary
String PARAM_TWEET_MESSAGE = "message";
// temporary
String defaultTweetMessage = "";
if(TwitterUtil.isTwitterSetUp(topLevelUrl)) {
    String paramTweetMessage = request.getParameter(PARAM_TWEET_MESSAGE);
    if(paramTweetMessage != null && !paramTweetMessage.isEmpty()) {

        // Aha.... paramTweetMessage has already been decoded.... ????
//        String decodedTweetMessage = null;
//        try {
//	        decodedTweetMessage = java.net.URLDecoder.decode(paramTweetMessage, "UTF-8");
//        } catch(Exception ex) {
//            // ignore
//            java.util.logging.Logger.getLogger("TweetBox").log(java.util.logging.Level.SEVERE, "Why does this fail???", ex);
//        }

        String decodedTweetMessage = paramTweetMessage;
        if(decodedTweetMessage != null) {
            int len = decodedTweetMessage.length();
            if(len <= 140) {
            	defaultTweetMessage = decodedTweetMessage;
            } else {
                defaultTweetMessage = decodedTweetMessage.substring(0, 140);
            }
        }
        // TBD: "%" symbol fails for some reason......
        defaultTweetMessage = HtmlTextUtil.escapeForJavascript(defaultTweetMessage);
    }
}

%><!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!-- Consider adding an manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<html>
  <head>
    <meta charset="utf-8">
    <title><%=brandDisplayName%> | Tweet</title>
    <meta name="author" content="Aery Software">
    <meta name="description" content="<%=AppBrand.getBrandDescription(appBrand)%>. It also provides free URL Shortener Web Services API. You can use <%=brandDisplayName%> shortener service to create a short URL, a safe URL, a canny URL, and a flash URL. But, <%=brandDisplayName%> is so much more than a URL shortener." />


    <!-- Mobile viewport optimized: j.mp/bplateviewport -->
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <link rel="shortcut icon" href="/img/favicon-<%=appBrand%>.ico" />
    <link rel="icon" href="/img/favicon-<%=appBrand%>.ico" type="image/x-icon" />

    <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- CSS: implied media=all -->
    <%@ include file="/fragment/style.jspf" %>
    <!-- end CSS-->

    <!-- All JavaScript at the bottom, except for Modernizr / Respond.
       Modernizr enables HTML5 elements & feature detects; Respond is a polyfill for min/max-width CSS3 Media Queries
       For optimal performance, use a custom Modernizr build: www.modernizr.com/download/ -->
    <script src="http://ww2.filestoa.com/js/modernizr/modernizr-2.0.6.min.js"></script>

<%
if(TwitterUtil.isTwitterSetUp(topLevelUrl)) {
%>
    <!--  Twitter @anywhere API  -->
    <script src="http://platform.twitter.com/anywhere.js?id=<%=TwitterUtil.getConsumerKey(topLevelUrl)%>&v=1" type="text/javascript"></script>
<%
}
%>
  </head>

  <body>


<jsp:include page="/fragment/TopNavbar">
<jsp:param name="pageCode" value="tweetbox"/>
<jsp:param name="brandDisplayName" value="<%=brandDisplayName%>"/>
</jsp:include>


  
  
   <div class="container-fluid">
     <div id="main" class="hero-unit">


<!-- 
<div id="div_share_buttons" style="margin-top:-20px;">

<span class="fb-like pull-right" data-send="false" data-layout="button_count" data-width="450" data-show-faces="false" data-font="arial"></span>

<span class="pull-right">
<span class="g-plusone" data-size="medium" data-annotation="none"></span>
</span>

<span class="pull-right blogarticle_view_information_shortlink">
<a href="https://twitter.com/share" class="twitter-share-button" accesskey="t" 
    data-url="<%=shortUrlShortUrl%>" 
    data-counturl="<%=shortUrlShortUrl%>" 
    data-text="<%=defaultTweetMessage%>"
    data-count="none"
    data-lang="en">Tweet</a>
</span>

<span class="pull-right">
<script type="IN/Share"></script>
</span>

</div>
-->



     <form id="form_shorturl_view" method="POST" action="">

        <div id="cannyurl_logo">
	 	    <h1><%=brandDisplayName%><sup>&beta;</sup></h1>
        </div>


<%
if(TwitterUtil.isTwitterSetUp(topLevelUrl)) {
%>
    <div id="twitter_tweet_body_container">
        <div id="twitter_tweet_body">
          <div id="twitter_tweet_body_tweetbox"></div>
        </div>
    </div>
<%
}
%>

      </form>
      </div>


<!-- Page Footer -->    
<%@ include file="/fragment/Footer.jspf" %>
<!-- Page Footer -->    

    </div> <!--! end of #container -->



    <div id="div_status_floater" style="display: none;" class="alert">
    </div>
 
   
    <!--  Tweet Button  -->
    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

	<!--  Facebook Like  -->
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
	
	<!--  LinkedIn Share  -->
	<script src="//platform.linkedin.com/in.js" type="text/javascript"></script>
	
	<!-- Google+ - Place this tag after the last +1 button tag. ???  -->
	<script type="text/javascript">
	  (function() {
	    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
	    po.src = 'https://apis.google.com/js/plusone.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
	  })();
	</script>
    
    

    <!-- JavaScript at the bottom for fast page loading -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="http://ww2.filestoa.com/js/jquery/jquery-1.8.3.min.js"><\/script>')</script>
	<script src="http://ww2.filestoa.com/js/jquery/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="http://ww2.filestoa.com/js/jquery/plugin/jquery.floatobject-1.4.js"></script>
    <script src="http://ww2.filestoa.com/js/bootstrap/bootstrap-2.0.js"></script>
    <script type="text/javascript" src="/js/core/debugutil.js"></script>
    <script type="text/javascript" src="http://ww2.filestoa.com/js/core/datetimeutil-1.0.js"></script>
    <script type="text/javascript" src="http://ww2.filestoa.com/js/core/emailutil-1.0.js"></script>
	<script defer type="text/javascript" src="/js/plugins.js"></script>
    <script defer type="text/javascript" src="/js/script.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/fiveten-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/statushelper-1.1.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/tweethelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/signuphelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/feedbackhelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/emailhelper-1.0.js"></script>
    <!-- end scripts-->


    <script>
    // Global vars
    var feedbackHelper;
    $(function() {
    	var targetService = "<%=brandDisplayName%>";
    	var targetPage = "shorten";
    	feedbackHelper = new webstoa.FeedbackHelper(targetService, targetPage);
    });
    </script>

    <script>
    // Email address "decoding"...
    $(function() {
        $("a").each(function() {
        	var anchor = $(this);
        	webstoa.EmailUtil.decodeMailtoAnchor(anchor);
        });
    });
    </script>

    <script>
    $(function() {
        $("#anchor_feedback_footer").click(function() {
            if(DEBUG_ENABLED) console.log("Feedback button clicked."); 
            var targetItem = 'footer';
            feedbackHelper.feedback(targetItem);
		    return false;
        });
    });
    </script>


    
    <script>
    // Function definitions.
    function updateStatus(msg, type, time) {
    	if(!time) {
    		time = 5000;   // milliseconds.
    	}
    	if(!type) {
    		// type = "info";  // warning, error, etc....
    	}
    	//$("#div_status_floater").text(msg);
    	//$("#div_status_floater").fadeIn().delay(time).fadeOut();
		$("#div_status_floater").textInOut(msg, type, time);
    }    
    </script>

    <script>
    // "Init"
    $(function() {
    	// TBD: initially hide $("#div_status_floater") ...
    	$("#div_status_floater").text("").hide();
    
        // Background image test.
        //$("#container").css("background-image", 'url(/img/bg/oceansunset.jpg)');
        $(document.body).css("background", "silver");
    	$("#container").css("background", "ivory");

        // TBD: Location...
    	var params = {x: 530, y: 15, speed: 'normal' };
    	$("#div_status_floater").makeFloat(params);
    	
    	// Peeng?
    	//var fiveTenTimer = new fiveten.FiveTenTimer(11392, 3);  // ???
    	//fiveTenTimer.start();
    });
    </script>

    <script>
    function delayedRedirect() {
    	var milli = 3000;
       	setTimeout(redirectToShortUrlEditPage, milli);
    }
    function redirectToShortUrlEditPage() {
    	window.location = '<%=editPageUrl%>';
    }
    </script>

    <script>
    // App-related vars.
    var userGuid;
<%
if(userId != null) {
%>
    userGuid = "<%=userId%>";
<%
}
%>
    </script>

<%
if(TwitterUtil.isTwitterSetUp(topLevelUrl)) {
%>
    <script>
    $(function() {

        twttr.anywhere(function (T) {
              T("#twitter_tweet_body").tweetBox({
              height: 80,
              width: 500,
              label: "Tweet this short URL and message",
              defaultContent: "<%=defaultTweetMessage%>",
              onTweet: onTweetSuccess
            });
          });
       
    });
    
    var onTweetSuccess = function(plainTwt, htmlTwt) {
    	if(DEBUG_ENABLED) console.log("onTweetSuccess(): plainTwt = " + plainTwt);
        updateStatus('Successfully tweeted the message. Now exiting...', 'info', 5550);
        delayedRedirect();
    };
    </script>
<%
}
%>


<%
String googleAnalyticsCode = AnalyticsHelper.getInstance().getGoogleAnalyticsCode();
if(googleAnalyticsCode != null && !googleAnalyticsCode.isEmpty()) {    // And, if it is enabled (e.g. through config). TBD...
%>
    <!-- Change UA-XXXXX-X to be your site's ID -->
    <script>
      window._gaq = [['_setAccount','<%=googleAnalyticsCode%>'],['_trackPageview'],['_trackPageLoadTime']];
      Modernizr.load({
        load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
      });
    </script>
<%
}
%>

    <!-- Prompt IE 6 users to install Chrome Frame. Remove this if you want to support IE 6.
       chromium.org/developers/how-tos/chrome-frame-getting-started -->
    <!--[if lt IE 7 ]>
      <script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
      <script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
    <![endif]-->

    </body>
</html>