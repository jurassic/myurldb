<%@ page import="com.myurldb.ws.core.*, com.myurldb.ws.*, com.cannyurl.af.util.*, com.cannyurl.af.auth.common.*, com.cannyurl.af.auth.*, com.cannyurl.af.auth.user.*, com.cannyurl.fe.*, com.cannyurl.fe.core.*, com.cannyurl.fe.bean.*, com.cannyurl.fe.util.*, com.cannyurl.wa.service.*, com.cannyurl.common.*, com.cannyurl.util.*, com.cannyurl.app.auth.*, com.cannyurl.app.util.*, com.cannyurl.helper.*" 
%><%@ page contentType="text/html; charset=UTF-8" 
%><%@ page errorPage="/error/UnknownError" 
%><%!
private static final java.util.logging.Logger logger = java.util.logging.Logger.getLogger("ShortLinkEdit");
%><%
boolean isAppAuthDisabled = ConfigUtil.isApplicationAuthDisabled();
boolean isAppAuthOptional = ConfigUtil.isApplicationAuthOptional();
String appAuthMode = ConfigUtil.getApplicationAuthMode();
boolean isUsingTwitterAuth = false;
if(AppAuthMode.MODE_TWITTER.equals(appAuthMode)) {
    isUsingTwitterAuth = true;
}
%><%
// {1] Session
SessionBean sessionBean = UserSessionManager.getInstance().setSessionBean(request, response);
String sessionToken = sessionBean.getToken();
String userId = sessionBean.getUserId();

// [2] Parse url.
String requestURI = request.getRequestURI();
String requestUrl = request.getRequestURL().toString();
String topLevelUrl = UrlHelper.getInstance().getTopLevelURLFromRequestURL(requestUrl);
String contextPath = request.getContextPath();
String servletPath = request.getServletPath();
String pathInfo = request.getPathInfo();
String queryString = request.getQueryString();
//...
%><%
// "Branding"
String appBrand = BrandingHelper.getInstance().getAppBrand();
String brandDisplayName = BrandingHelper.getInstance().getBrandDisplayName();
String brandDescription = BrandingHelper.getInstance().getBrandDescription();
%><%
String themeBootstrap = SiteThemeHelper.getInstance().getBootstrapTheme();
String themeJqueryUi = SiteThemeHelper.getInstance().getJqueryUiTheme();
%><%
boolean isUserAuthenticated = false;
String userUsername = null;
String loginUrl = null;
String logoutUrl = null;
RequestAuthStateBean authStateBean = (RequestAuthStateBean) request.getAttribute(AuthUtil.REQUEST_ATTR_AUTHSTATEBEAN);
if(authStateBean != null) {
    isUserAuthenticated = authStateBean.isAuthenticated();
    if(isUserAuthenticated) {
        logoutUrl = authStateBean.getLogoutUrl();
        userUsername = authStateBean.getUsername();
    } else {
        loginUrl = authStateBean.getLoginUrl();
    }   
}
%><%
String twitterHandle = "";
String twitterUserProfileUrl = "";
if(isUsingTwitterAuth) {
	if(userUsername != null && !userUsername.isEmpty()) {
	    twitterHandle = "@" + userUsername;      // ???
	    twitterUserProfileUrl = "http://twitter.com/" + userUsername;
	}
}
%><%
if(isUserAuthenticated == false) {
    // ????
    // response.sendRedirect(loginUrl);
}
%><%
// [3] Local "global" variables.
//String editState = null;     // "state_new", "state_edit", "state_read" (?), "state_unknown" == null.
ShortLinkJsBean shortLinkBean = null;

// [4] Initial validation
// URL "/shorten/<guid>" ???
String guid = UrlHelper.getInstance().getGuidFromPathInfo(pathInfo);
// check first if guid is a valid GUID....
if(guid != null && !guid.isEmpty()) {   // TBD: validation...
    try {
        shortLinkBean = new ShortLinkWebService().getShortLink(guid);
    } catch(Exception e) {
        // ignore...
    }
	if(shortLinkBean != null) {
	    //editState = "state_edit";
	    String shortUrlUser = shortLinkBean.getOwner();            // This cannot be null
	    Long createdTime = shortLinkBean.getCreatedTime();    // This cannot be null
	    long cutoffTime = new java.util.Date().getTime() - 12 * 3600 * 1000L;   // Arbitrary. 12 hours ago.
		if((shortUrlUser != null && !shortUrlUser.equals(userId)) || (createdTime != null && createdTime < cutoffTime)) {
		    // If the user is not the author,
		    // or, the shortUrl is "too old", 
	    	// then make this shortUrl non-editable!!!
	    	// Or, just forward it to "/shorten" ???
	    	// temporary
	    	response.sendRedirect("/shorten");
		}
	} else {
	    // guid is not null, but shortLinkBean is not found...
	    // TBD: Show error message? Or, just redirect to the shortUrl create page???
	    // temporary
    	response.sendRedirect("/shorten");
	}
} else {
    //editState = "state_new";
    
    // TBD:
    // Parse the query string, and if longUrl is present,
    // create a ShortLink ....  ??????
    // --> This does not work, because we are using GET and GET should be idempotent....
    // ....

/*
    // temporary
    String PARAM_LONGURL = "longUrl";
    String paramLongUrl = request.getParameter(PARAM_LONGURL);
    if(paramLongUrl != null && !paramLongUrl.isEmpty()) {
        ShortLinkJsBean inputBean = new ShortLinkJsBean();
        //guid = GUID.generate();
        //inputBean.setGuid(guid);
        if(userId != null) {
            inputBean.setOwner(userId);
        }
        inputBean.setLongUrl(paramLongUrl);
        // etc...
        try {
            shortLinkBean = new ShortLinkWebService().constructShortLink(inputBean);
        } catch(Exception e) {
            // ignore...
        }
        if(shortLinkBean != null) {
            guid = shortLinkBean.getGuid();
        }
    }
*/

}

// [5] JSP/Javascript variables..
String shortUrlLongUrl = "";
//String shortUrlLongUrl = ShortLinkUtil.createDefaultShortLinkTitle();
// etc....
String shortUrlShortMessage = "";
// ....
String shortUrlDomain = "";
String shortUrlToken = "";
String shortUrlShortUrl = "";
Long shortUrlFlashDuration = 0L;
long shortUrlCreatedTime = 0L;
long shortUrlModifiedTime = 0L;
// ...
String shortUrlRedirectType = "";
String shortUrlNote = "";
long expirationTime = 0L;
if(shortLinkBean != null) {
    shortUrlLongUrl = (shortLinkBean.getLongUrl() != null) ? shortLinkBean.getLongUrl() : shortUrlLongUrl;
    
    shortUrlDomain = (shortLinkBean.getDomain() != null) ? shortLinkBean.getDomain() : shortUrlDomain;
    shortUrlToken = (shortLinkBean.getToken() != null) ? shortLinkBean.getToken() : shortUrlToken;
    shortUrlShortMessage = (shortLinkBean.getShortMessage() != null) ? shortLinkBean.getShortMessage() : shortUrlShortMessage;
    shortUrlFlashDuration = (shortLinkBean.getFlashDuration() != null) ? shortLinkBean.getFlashDuration() : shortUrlFlashDuration;
 
    String shortUrl = shortLinkBean.getShortUrl();
    if(URLUtil.isValidUrl(shortUrl)) {
        shortUrlShortUrl = shortUrl;
    } else {
        // ???
        //shortUrlShortUrl = "";  // Build it from domain and token???? 
    }

    shortUrlCreatedTime = (shortLinkBean.getCreatedTime() != null) ? shortLinkBean.getCreatedTime() : shortUrlCreatedTime;
    shortUrlModifiedTime = (shortLinkBean.getModifiedTime() != null) ? shortLinkBean.getModifiedTime() : shortUrlModifiedTime;
    expirationTime = (shortLinkBean.getExpirationTime() != null) ? shortLinkBean.getExpirationTime() : expirationTime;

    shortUrlRedirectType = (shortLinkBean.getRedirectType() != null) ? shortLinkBean.getRedirectType() : shortUrlRedirectType;
    shortUrlNote = (shortLinkBean.getNote() != null) ? shortLinkBean.getNote() : shortUrlNote;
}
%><%
if(shortUrlRedirectType == null || shortUrlRedirectType.isEmpty()) {
    shortUrlRedirectType = ConfigUtil.getSystemDefaultRedirectType();
    if(shortLinkBean != null) {
        shortLinkBean.setRedirectType(shortUrlRedirectType);
    }
}
%><%
if(shortUrlDomain == null || shortUrlDomain.isEmpty()) {
    if(ConfigUtil.isUiShortLinkEditDomainPrefilled()) {
        String domainType = null;
        if(shortLinkBean != null) {
            domainType = shortLinkBean.getDomainType();  // Does this make sense when shortUrlDomain==null/"" ????
        }
        domainType = DomainUtil.validateInitialDomainType(domainType);
        shortUrlDomain = DomainCheckerHelper.getInstance().getDomainForUser(domainType, userId);
    }
}
%><%
// Global config flags.
boolean showingMessageBox = false;
if((ConfigUtil.isGeneralIncludeMessageBox() || RedirectType.canIncludeMessage(shortUrlRedirectType)) && ConfigUtil.isGeneralExcludeMessageBox() == false) {
    showingMessageBox = true;
}
%><%
// Tweet button
// String tweetVia = "@" + appBrand;
String defaultTweetMessage = " - " + shortUrlShortUrl;
// ...
%><%
// ...
%><!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!-- Consider adding an manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<html>
  <head>
    <meta charset="utf-8">
    <title><%=brandDisplayName%> | <%=brandDescription%></title>
    <meta name="author" content="URL Foundry">
    <meta name="description" content="<%=AppBrand.getBrandDescription(appBrand)%>. It also provides free URL Shortener Web Services API. You can use <%=brandDisplayName%> shortener service to create a short URL, a safe URL, a canny URL, and a flash URL. But, <%=brandDisplayName%> is so much more than a URL shortener." />

    <!-- Mobile viewport optimized: j.mp/bplateviewport -->
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <link rel="shortcut icon" href="/img/favicon-<%=appBrand%>.ico" />
    <link rel="icon" href="/img/favicon-<%=appBrand%>.ico" type="image/x-icon" />

    <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- CSS: implied media=all -->
    <%@ include file="/fragment/style.jspf" %>
    <!-- end CSS-->

    <!-- All JavaScript at the bottom, except for Modernizr / Respond.
       Modernizr enables HTML5 elements & feature detects; Respond is a polyfill for min/max-width CSS3 Media Queries
       For optimal performance, use a custom Modernizr build: www.modernizr.com/download/ -->
    <script src="http://ww2.filestoa.com/js/modernizr/modernizr-2.0.6.min.js"></script>

  </head>

  <body>

<jsp:include page="/fragment/TopNavbar">
<jsp:param name="pageCode" value="shortlink/edit"/>
<jsp:param name="brandDisplayName" value="<%=brandDisplayName%>"/>
</jsp:include>



    <div class="container-fluid">


<%
if(isAppAuthDisabled == true || isAppAuthOptional == true || isUserAuthenticated == true) {
%>

      <!-- Main hero unit for when isUserAuthenticated == true  -->
      <div id="main" class="hero-unit">

<!-- 
<div id="div_share_buttons" style="margin-top:-20px;">
</div>
-->


      <form class="form-inline" id="form_shorturl_edit" method="POST" action="" accept-charset="UTF-8">

      <div id="shorturl_header">
      <fieldset name="fieldset_shorturl_header">
        <table style="background-color: transparent;">
        <tr>
        <td style="vertical-align:bottom;">
        <div id="cannyurl_logo">
<!-- 
	 	    <a href="/"><img id="cannyurl_editpage_logo" src="/img/<%=appBrand%>-beta.png" alt="<%=brandDisplayName%> (beta)" title="Go to <%=brandDisplayName%> home"></img></a>
 -->
	 	    <h1><%=brandDisplayName%><sup>&beta;</sup></h1>
        </div>
        </td>
        <td style="vertical-align:top;">
            <div id="cannyurl_questionmark" style="margin:-5px -25px 0px 5px;">
                <a href="#" id="cannyurl_questionmark_link"><img src="/img/question-50.png" alt="<%=brandDisplayName%> Help" title="<%=brandDisplayName%> Help"></img></a>
	 	    </div>
        </td>
        <td style="vertical-align:bottom; padding: 0px 0px 8px 40px">
        <label class="input_label_text" for="input_shorturl_options_flash_duration" id="input_shorturl_options_flash_duration_label" title="Select the flash duration">Flash Duration:</label>
        <br>
        <select class="input-large" id="input_shorturl_options_flash_duration" title="Select the flash duration">
<%
int flashSeconds = 10;    // Default value..
if(shortUrlFlashDuration != null && shortUrlFlashDuration > 0L) {
    flashSeconds = (int) (shortUrlFlashDuration / 1000L);      // Math.floor?
    if( flashSeconds > 0 && ! (flashSeconds==3 || flashSeconds==5 || flashSeconds==10 || flashSeconds==15 || flashSeconds==20 || flashSeconds==30)) {    
%>
	      <option value="<%=flashSeconds%>" selected><%=flashSeconds%> Second Flash</option>
<%
    }
}
%>
	      <option value="3" <%if(flashSeconds == 3){%>selected<%}%>>3 Second Flash</option>
	      <option value="5" <%if(flashSeconds == 5){%>selected<%}%>>5 Second Flash</option>
	      <option value="10" <%if(flashSeconds == 10){%>selected<%}%>>10 Second Flash</option>
	      <option value="15" <%if(flashSeconds == 15){%>selected<%}%>>15 Second Flash</option>
	      <option value="20" <%if(flashSeconds == 20){%>selected<%}%>>20 Second Flash</option>
	      <option value="30" <%if(flashSeconds == 30){%>selected<%}%>>30 Second Flash</option>
        </select>
        </td>
        </tr>
        </table>
      </fieldset>
      </div>

      <div id="shorturl_body">
      <fieldset name="fieldset_shorturl_body">
        <div id="shorturl_body_top">
        <table style="background-color: transparent;">
          <tr>
          <td>
            <label class="input_label_text" for="input_shorturl_shorturl" id="input_shorturl_shorturl_label" title="Shortened URL">Short&nbsp;URL:</label>
          </td>
          <td colspan="2">
            <div id="shorturl_shorturl">


<% if(ConfigUtil.isUiShortLinkEditDomainPrefilled()) { %>
              <input class="input-large uneditable-input" type="text" name="input_shorturl_shorturldomain" id="input_shorturl_shorturldomain" readonly value="<%=shortUrlDomain%>"></input>
              <input class="input-small" type="text" name="input_shorturl_shorturltoken" id="input_shorturl_shorturltoken" title="Type in a custom token or leave it blank" value="<%=shortUrlToken%>"></input>
              <input class="input-xlarge uneditable-input" type="text" name="input_shorturl_shorturl" id="input_shorturl_shorturl" readonly value="<%=shortUrlShortUrl%>"></input>
<% } else { %>
              <input class="input-xlarge uneditable-input" type="text" name="input_shorturl_shorturl" id="input_shorturl_shorturl" readonly value="<%=shortUrlShortUrl%>"></input>
<% } %>


<%
// if(BrandingHelper.getInstance().isBrandSassyURL()) {
if(ConfigUtil.isGeneralUseSassyToken()) {
%>
              <select class="input-large" id="input_shorturl_options_sassytoken_type" title="Select the token type. A token is a random string at the end of a short URL.">  
<%
String sassyTokenTypeDefaultValue = ConfigUtil.getSystemDefaultSassyTokenType();
for(String[] tt : SassyTokenType.TYPES) {
    String val = tt[0];   
    String label = tt[1];
    if(val.equals(sassyTokenTypeDefaultValue)) {
%>
                  <option value="<%=val%>" selected><%=label%></option>
<%
    } else {
%>
		          <option value="<%=val%>"><%=label%></option>
<%      
    }
}
%>
              </select>
<%
} else {
%>
              <select class="input-large" id="input_shorturl_options_token_type" title="Select the token type. A token is a random string at the end of a short URL.">  
<%
// TBD:
// For keyword apps, remove both token and sassy token options???
// ....
String tokenTypeDefaultValue = ConfigUtil.getSystemDefaultTokenType();
for(String[] tt : TokenType.TYPES) {
    String val = tt[0];   
    String label = tt[1];
    if(!val.equals(TokenType.TYPE_CUSTOM) 
            && !val.equals(TokenType.TYPE_TINY)       // Exlcude "tiny" for now.... 
            && !val.equals(TokenType.TYPE_SASSY)      // Exclude "sassy" as well...
            && !val.equals(TokenType.TYPE_SEQUENTIAL) // "seqeuntinal" is really a system setting...
            && !val.equals(TokenType.TYPE_KEYWORD)    // Keyword does not belong here...
            ) {
        if(val.equals(tokenTypeDefaultValue)) {
%>
		          <option value="<%=val%>" selected><%=label%></option>
<%
        } else {
%>
		          <option value="<%=val%>"><%=label%></option>
<%      
        }
    }
}
%>
              </select>
<%
}
%>


              <button class="btn" type="button" name="button_shorturl_view_shorturl" id="button_shorturl_view_shorturl" title="View the target page" style="width:70px">View</button>
              <button class="btn" type="button" name="button_shorturl_verify_shorturl" id="button_shorturl_verify_shorturl" title="Verify the Short URL" style="width:70px">Verify</button>
<!-- -->
              <button class="btn" type="button" name="button_shorturl_new" id="button_shorturl_new" title="Create a new short URL" style="width:70px">New</button>
<!-- -->


            </div>
          </td>
          </tr>
          <tr>
          <td>
            <label class="input_label_text" for="input_shorturl_longurl" id="input_shorturl_longurl_label" title="Input URL. Copy and paste the target Web site URL here.">Long&nbsp;URL:</label>
            <br>
            <span id="longurl_char_counter">500</span>
          </td>
          <td>
            <div id="shorturl_longurl">
              <textarea name="input_shorturl_longurl" id="input_shorturl_longurl" placeholder="Type in, or copy and paste, the target URL here." cols="70" rows="2"><%=shortUrlLongUrl%></textarea>
            </div>
          </td>
          <td style="vertical-align:middle;">
            <button class="btn" type="button" name="button_shorturl_options" id="button_shorturl_options" title="Update short URL options" style="width:90px">Options</button><br/>
            <button class="btn button_shorturl_primary_target" type="button" name="button_shorturl_save" id="button_shorturl_save" title="Create/Update short URL" style="width:90px">Create</button> 
          </td>
          </tr>
        </table>
        </div>
        <div id="shorturl_body_middle">
<%
if(showingMessageBox) {
%>
          <div id="shorturl_body_main_short_message_label">
            <label class="input_label_text" for="input_shorturl_short_message" id="input_shorturl_short_message_label" title='This message will be displayed when the short URL is opened. Please add any description to help the user in deciding whether to visit the Web site or not.'>Message to the viewer (optional):</label>
            &nbsp;
            <span id="short_message_char_counter">500</span> chars remaining
          </div>
          <div id="shorturl_body_main" class="shorturl_body_main_short_message">
              <div id="shorturl_short_message_textarea">
                  <textarea name="input_shorturl_short_message" id="input_shorturl_short_message" cols="100" rows="4"><%=shortUrlShortMessage%></textarea>
              </div>
          </div>
<%
}
%>

          <div id="div_femtourl_variations_container" style="display: none;">
<%
// if(BrandingHelper.getInstance().isBrandFemtoURL()) { 
if(ConfigUtil.isGeneralShowUrlVariations()) {
%>
            <div id="div_femtourl_variations_title">
              <span id="span_femtourl_variations_title">Femto URL Variations</span>
              <a href="#" id="femtourl_variations_questionmark_link"><img src="/img/question-25.png" alt="Femto URL Variations Help" title="What are these" style="margin:-8px 0px 5px -5px;"></img></a>
            </div>
            <div id="div_femtourl_variations_sisterurls">
            <table style="background-color: transparent;">
              <tr>
                <td>
                  <label class="input_label_text" id="input_femtourl_variations_femtourl_label" for="input_femtourl_variations_femtourl" title="Permanent redirect (301)">Femto URL (Original):</label>
                </td>
                <td>
                  <input type="text" class="input_femtourl_variations" name="input_femtourl_variations_femtourl" id="input_femtourl_variations_femtourl" readonly size="25"></input>
                </td>
                <td>
                  <button class="btn" type="button" name="button_femtourl_variations_femtourl_view" id="button_femtourl_variations_femtourl_view" title="View the target page" style="width:70px">View</button>
                </td>
              </tr>
              <tr>
                <td>
                  <label class="input_label_text" id="input_femtourl_variations_cannyurl_label" for="input_femtourl_variations_cannyurl" title="User confirmation required before redirect">Canny URL (Preview):</label>
                </td>
                <td>
                  <input type="text" class="input_femtourl_variations" name="input_femtourl_variations_cannyurl" id="input_femtourl_variations_cannyurl" readonly size="25"></input>
                </td>
                <td>
                  <button class="btn" type="button" name="button_femtourl_variations_cannyurl_view" id="button_femtourl_variations_cannyurl_view" title="View the target page" style="width:70px">View</button>
                </td>
              </tr>
              <tr>
                <td>
                  <label class="input_label_text" id="input_femtourl_variations_flashurl_label" for="input_femtourl_variations_flashurl" title='Preview through "flash" page'>Flash URL (10 seconds):&nbsp;</label>
                </td>
                <td>
                  <input type="text" class="input_femtourl_variations" name="input_femtourl_variations_flashurl" id="input_femtourl_variations_flashurl" readonly size="25"></input>
                </td>
                <td>
                  <button class="btn" type="button" name="button_femtourl_variations_flashurl_view" id="button_femtourl_variations_flashurl_view" title="View the target page" style="width:70px">View</button>
                </td>
              </tr>
              <tr>
                <td>
                  <label class="input_label_text" id="input_femtourl_variations_quickurl_label" for="input_femtourl_variations_quickurl" title='Preview through "flash" page'>Flash URL (5 seconds):</label>
                </td>
                <td>
                  <input type="text" class="input_femtourl_variations" name="input_femtourl_variations_quickurl" id="input_femtourl_variations_quickurl" readonly size="25"></input>
                </td>
                <td>
                  <button class="btn" type="button" name="button_femtourl_variations_quickurl_view" id="button_femtourl_variations_quickurl_view" title="View the target page" style="width:70px">View</button>
                </td>
              </tr>
            </table>
            </div>
<%
}
%>
          </div>

        </div>
      </fieldset>
      </div>

      </form>
      </div>  <!--   Hero unit  -->
<%
} else {     // isUserAuthenticated == false
%>
      <!-- Main hero unit for when isUserAuthenticated == false  -->
      <div id="main" class="hero-unit">

        <div id="app_info_logo">
	 	    <h1><%=brandDisplayName%><sup>&beta;</sup></h1>
        </div>

<div id="app_info_main">

<%
if( BrandingHelper.getInstance().isBrandCannyURL() 
        || BrandingHelper.getInstance().isBrandFlashURL() 
        || BrandingHelper.getInstance().isBrandSassyURL() 
        || BrandingHelper.getInstance().isBrandFemtoURL() ) {
%>
        <p>
        <%=brandDisplayName%> (tm) is a safe URL shortener application.
        You can create a short URL by specifying a target (long) URL, which is currently limited to the maximum of 400 characters.
        You can optionally include a "message" for the viewers (e.g., the users who click on the short URL link).
        The message should be limited to 500 characters (as of this writing).
        The short URLs are "safe" in that the short URL is not automatically redirected to the target Web page.
        Viewers can view which Web site the short URL is redirecting to
        and decide whether to proceed or not.
        The short URL message will be displayed when the user/viewer sees the short URL information.
        When the user "confirms" the redirect, the target Web page will be displayed.
        </p>
<%
} else if( BrandingHelper.getInstance().isBrandTweetUserURL() ) {
%>
        <p>
        <%=brandDisplayName%> (tm) is a new kind of URL shortener service.
        You can create a short URL with your Twitter handle embedded as your "signature".
        This will give other users assurance as to where the short URL has originated,
        which will likely increase the click rate.
        <%=brandDisplayName%> can help you disseminate your message to wider audience.
        </p>
<%
} else {
%>
        <p>
        <%=brandDisplayName%> (tm) is a new kind of URL shortener service.
        You can create a short URL by specifying a target (long) URL,
        which points to a destination Web site.
        (Some of the unique features of <%=brandDisplayName%> will be described in the Help page.)
        </p>
<%
}
%>


</div>

<%if(isAppAuthDisabled == false) {%>
<div>

<%if(isUsingTwitterAuth) {%>
Please sign in to create your personal Twitter URLs:
<br>
<a href="<%=loginUrl%>"><img src="/img/twitter/sign-in-with-twitter-link.png"></img></a>
<%} else {%>
Please sign in to create your personal URLs:
<br>
<a href="<%=loginUrl%>">Login</a>
<%}%>

</div>
<%}%>

      </div>  <!--   Hero unit  -->
<%
}            // isUserAuthenticated
%>




<!--   BEGIN: Content   -->


<% if(BrandingHelper.getInstance().isBrandCannyURL()) { %>
<% } else if(BrandingHelper.getInstance().isBrandFlashURL()) { %>
<% } else if(BrandingHelper.getInstance().isBrandFemtoURL()) { %>
<% } else if(BrandingHelper.getInstance().isBrandSassyURL()) { %>
<% } else if(BrandingHelper.getInstance().isBrandUserURL()) { %>
<% } else if(BrandingHelper.getInstance().isBrandQuadURL()) { %>
<% } else if(BrandingHelper.getInstance().isBrandSpeedKeyword()) { %>
<% } else if(BrandingHelper.getInstance().isBrandTweetUserURL()) { %>

<div class="row-fluid">
  <div class="span6">
    <h2>URL Shortening</h2>
    <p>
    URL shorteners are commonly used in social media or in other applications
    to make Web page URLs shorter or more memorable.
    While URL shortening has many legitimate uses, unfortunately, 
    it is also widely used, and exploited, by hackers and scammers,
    for instance, for phishing and other illegal purposes.
    An innocent and trusting user who clicks shortened, and often cryptic, URLs
    may be infected by malwares or may otherwise be exposed to potential harms.
    </p>
  </div>
  <div class="span6">
    <h2>Why <%=brandDisplayName%>?</h2>
    <p>
    <%=brandDisplayName%> is a new kind of URL shortener,
    which reduces or eliminates many of the potential pitfalls of the existing URL shorteners.
    First and foremost, it uses a user's Twitter handle as a "signature" of a short URL,
    thereby providing the transparency and accountability to URL shortening.
    </p>
  </div>
</div>
<div class="row-fluid">
  <div class="span6">
    <h2></h2>
    <p></p>
  </div>
  <div class="span6">
    <h2></h2>
    <p></p>
  </div>
</div>

<% } else if(BrandingHelper.getInstance().isBrandSmallBizURL()) { %>

<div class="row-fluid">
  <div class="span6">
    <h2>Small Business URL Shortener</h2>
    <p>
    abc
    </p>
  </div>
  <div class="span6">
    <h2>Why use <%=brandDisplayName%>?</h2>
    <p>
    xyz
    </p>
  </div>
</div>
<div class="row-fluid">
  <div class="span6">
    <h2></h2>
    <p></p>
  </div>
  <div class="span6">
    <h2></h2>
    <p></p>
  </div>
</div>

<% } else { %>
<% } %>




<% if(ConfigUtil.isUiShortLinkEditDomainPrefilled()) { %>
<% } else { %>
<% } %>

<% if(ConfigUtil.isGeneralUseSassyToken()) { %>
<% } else { %>
<% } %>

<% if(ConfigUtil.isGeneralIncludeFlashDuration()) { %>
<% } else { %>
<% } %>

<% if(showingMessageBox) { %>
<% } %>

<% if(ConfigUtil.isGeneralShowUrlVariations()) { %>
<% } %>



<!--   END: Content   -->




<!-- Page Footer -->    
<%@ include file="/fragment/Footer.jspf" %>
<!-- Page Footer -->    

    </div> <!--! end of #container -->



    <div id="div_questionmark_dialog" title="<%=brandDisplayName%> Help" style="display: none;">
        <div id="questionmark_help_preface">
        If you are new to <%=brandDisplayName%>,<br/>
        here's quick instruction:
        </div>
        <div id="questionmark_help_main">
<%
if( BrandingHelper.getInstance().isBrandCannyURL() || BrandingHelper.getInstance().isBrandFlashURL() ) {
%>
        <p>
        <%=brandDisplayName%> (tm) is a safe URL shortener service.
        You can create a short URL by specifying a target (long) URL.
        You can optionally include a "message" for the viewers (e.g., users who click on the short URL link).
        The short URLs are "safe" in that the short URL is not automatically redirected to the target Web page.
        Viewers can view which Web site the short URL is redirecting to
        and decide whether to proceed or not.
        </p>
<%
    if(BrandingHelper.getInstance().isBrandCannyURL()) {
%>

        <p>
        When the short URL is followed, 
        some basic information regarding the target Web site such as its page title is displayed
        as well as the short URL creator's message, if any.
        The user can then proceed to the Web page by pressing the "Proceed" button.
        </p>
<%
    } else if(BrandingHelper.getInstance().isBrandFlashURL()) {
%>
        <p>
        When the short URL is followed, 
        some basic information regarding the target Web site such as its page title is displayed
        as well as the short URL creator's message, if any.
        This short URL view page is "flashed" for a preset amount of time, e.g., 10 seconds by default.
        The creator/user can specify different values while creating the short URL. 
        When the time is up, the viewer/user will be automatically redirected to the target Web page.
        The user can opt out of the redirection by pressing the "Stop" button.
        </p>
<%
    }
} else if(BrandingHelper.getInstance().isBrandFemtoURL()) {
%>
        <p>
        <%=brandDisplayName%> (tm) is a new kind of URL shortener service.
        You can create a short URL, or "femto URL", by specifying a target (long) URL,
        which points to a destination Web site.
        A femto URL is a very short URL which is intended for quick use.
        It also provides various different ways of accessing the target Web page.
        Either the creator or the viewer/user of a femto URL can decide 
        how to redirect to the target Web page using the short URL.
        For example, it can be automatically redirected, or it can require the viewer's confirmation, etc.
        </p>
<%
} else if(BrandingHelper.getInstance().isBrandSassyURL()) {
%>
        <p>
        <%=brandDisplayName%> (tm) is a new kind of URL shortener service.
        You can create a short "sassy URL" by specifying a target (long) URL.
        A sassy URL comprises only valid English words, and hence more "readable" than random strings,
        which most URL shortening services automatically generate.
        (Note: We use two letter, three letter, four letter English words (or, acronyms),
        some of which may be rarely used.)
        </p>
<%
} else if(BrandingHelper.getInstance().isBrandQuadURL()) {
%>
        <p>
        <%=brandDisplayName%> (tm) is a URL shortener service which shortens multiple URLs at the same time.
        You can create a "quad URL" by inputting either multple URLs or a passage that contains multiple URLs.
        Depending on the user options, a set of short URLs, each of which coressponding to a long URL in the set/passage,
        may also be created.
        A quad URL points to a Web page that contains the set of (original) long URLs. 
        </p>
<%
} else if(BrandingHelper.getInstance().isBrandUserURL()) {
%>
        <p>
        <%=brandDisplayName%> (tm) is a personalized vanity URL shortener service.
        You can create a short "user URL" by specifying a target (long) URL.
        Each user on the system is given a unique "URL prefix" (e.g., domain and path) e.g., based on the username,
        and every short URL created by a given user includes such URL prefix.
        User URLs are personalized and branded. 
        </p>
<%
} else if( BrandingHelper.getInstance().isBrandTweetUserURL() ) {
%>
        <p>
        <%=brandDisplayName%> (tm) is a personalized URL shortener service.
        You can create a short URL with your Twitter handle as your branding or signature.
        Some of the unique features of <%=brandDisplayName%> will be described in the Help page.
        </p>
<%
} else {
%>
        <p>
        <%=brandDisplayName%> (tm) is a new kind of URL shortener service.
        Some of the unique features of <%=brandDisplayName%> will be described in the Help page.
        </p>
<%
}
%>
        <p>
        Once a short URL is created, you can "verify" the settings.
        The verify page can be used for short URLs created by other services as well.
        Certain short URL information, such as the (optional) message, can be edited within a short time window (e.g., a few hours)
        from the same browser window.
        </p>

      </div>
    </div>


<%
// if(BrandingHelper.getInstance().isBrandFemtoURL()) { 
if(ConfigUtil.isGeneralShowUrlVariations()) {
%>
    <div id="femtourl_variations_help_dialog" title="Femto URL Variations - Help" style="display: none;">
        <div id="femtourl_variations_help_preface">
        What are these "Femto URL variations"?
        </div>
        <div id="femtourl_variations_help_main">
        <p>
        A femto URL is a set of URLs, each of which has a differnt "redirection" characteristic.
        These different URLs are associated with different "path indicia" to let the users/viewers know which category the given short URL belongs to.
        Femto URL Shortener service creates a short URL which automatically redirects to the target Web site by default.
        This can be modified by adding the following indicia.
        </p> 
        <table id="femtourl_variations_help_table" style="background-color: transparent;">
        <thead>
        <tr>
        <th>Path</th><th width="100px;">Name</th><th>Description</th>
        </tr>
        </thead>
        <tbody>
        <tr>
        <td class="femtourl_variations_td_path">/p</td><td class="femtourl_variations_td_name">Femto URL&trade;</td>
        <td>Automatic redirection using HTTP response code 301 (aka "Permanent redirect").
         This is the default (e.g., without a path indicium) on Femto URL service.</td>
        </tr>
        <tr>
        <td class="femtourl_variations_td_path">/c</td><td class="femtourl_variations_td_name">Canny URL&trade;</td>
        <td>User will be directed to a "preview" page where (some basic) target Web site information is displayed.
         User's explicit confirmation is required before redirection. The "safest" option for the users/viewers.</td>
        </tr>
        <tr>
        <td class="femtourl_variations_td_path">/f</td><td class="femtourl_variations_td_name">Flash URL&trade;</td>
        <td>User will be directed to a preview page first.
        But after a certain preset time (10 second "flash" by default), the user is then automatically redirected to the target Web site.
        The user has the option not to visit the site during that time period.
        The path indicium can be a number between 1 and 9, which sets the flash duration.</td>
        </tr>
        </tbody>
        </table>
        </div>
    </div>
<%
}
%>

    <div id="div_shorturl_options_dialog" title="Short URL Options" style="display: none;">
        <div id="shorturl_options_preface">
        Additional <%=brandDisplayName%> Options
        </div>
        <form id="form_shorturl_options_dialog">
        <fieldset name="fieldset_shorturl_options_dialog">
        <div id="shorturl_options_main">

          <div id="shorturl_options_shorturl_toprow">
            <table style="background-color: transparent;">
            <tr>
            <td>
              <div id="shorturl_options_custom_token">
                <label class="input_label_text" for="input_shorturl_options_custom_token" id="input_shorturl_options_custom_token_label" title='Specify custom token'>Custom Token:</label>
                <br/>
                <input type="text" name="input_shorturl_options_custom_token" id="input_shorturl_options_custom_token" size="40"></input>
              </div>
            </td>
            </tr>
            </table>
          </div>

<!-- 
          <div id="shorturl_options_startdate_expirationtime">
            <table>
            <tr>
            <td>
              <div id="shorturl_options_expirationtime">
              <div id="shorturl_options_expirationtime_label">
              <label class="input_label_text" for="input_shorturl_options_expirationtime" id="input_shorturl_options_expirationtime_label">Expiration Time:</label>
              </div>
              <input type="text" name="input_shorturl_options_expirationtime" id="input_shorturl_options_expirationtime" size="25" value=""></input>
              <input type="hidden" name="input_shorturl_options_expirationtime_millisecs" id="input_shorturl_options_expirationtime_millisecs" value="<%=expirationTime%>"></input>
              </div>
            </td>
            </tr>
            </table>
          </div>
 -->

<%
if(showingMessageBox == false) {
    // TBD: ...
    // Include Optional message textarea here???
    // URLs can be modified (through "path"), and hence message can be viewable even when automatically redirected by default...
    // ....
%>

<%
}
%>

        </div>
        </fieldset>
        </form>
    </div>

    
    <div id="div_status_floater" style="display: none;" class="alert">
    </div>
 

    <!-- JavaScript at the bottom for fast page loading -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="http://ww2.filestoa.com/js/jquery/jquery-1.8.3.min.js"><\/script>')</script>
	<script src="http://ww2.filestoa.com/js/jquery/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="http://ww2.filestoa.com/js/jquery/plugin/jquery.floatobject-1.4.js"></script>
    <script src="http://ww2.filestoa.com/js/jquery/plugin/jquery-ui-timepicker-addon.js"></script>
    <script src="http://ww2.filestoa.com/js/jquery/plugin/history.js/uncompressed/history.adapter.jquery.js"></script>
    <script src="http://ww2.filestoa.com/js/jquery/plugin/history.js/uncompressed/history.js"></script>
    <script src="http://ww2.filestoa.com/js/jquery/plugin/history.js/uncompressed/history.html4.js"></script>
    <script src="http://ww2.filestoa.com/js/bootstrap/bootstrap-2.3.1.js"></script>
    <script type="text/javascript" src="/js/core/debugutil.js"></script>
    <script type="text/javascript" src="/js/core/urlutil-1.0.js"></script>
    <script type="text/javascript" src="http://ww2.filestoa.com/js/core/datetimeutil-1.0.js"></script>
    <script type="text/javascript" src="http://ww2.filestoa.com/js/core/emailutil-1.0.js"></script>
    <script type="text/javascript" src="/js/bean/shortlinkjsbean-1.0.js"></script>
    <script type="text/javascript" src="/js/mylibs/mytimer.js"></script>
    <script defer type="text/javascript" src="/js/mylibs/redirecturl-1.0.js"></script>
	<script defer type="text/javascript" src="/js/plugins.js"></script>
    <script defer type="text/javascript" src="/js/script.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/fiveten-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/statushelper-1.1.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/tweethelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/signuphelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/feedbackhelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/emailhelper-1.0.js"></script>
    <!-- end scripts-->


    <script>
    // Global vars
    var feedbackHelper;
    $(function() {
    	var targetService = "<%=brandDisplayName%>";
    	var targetPage = "shorten";
    	feedbackHelper = new webstoa.FeedbackHelper(targetService, targetPage);
    });
    </script>

    <script>
    // Email address "decoding"...
    $(function() {
        $("a").each(function() {
        	var anchor = $(this);
        	webstoa.EmailUtil.decodeMailtoAnchor(anchor);
        });
    });
    </script>

    <script>
    $(function() {
        $("#anchor_feedback_footer").click(function() {
            if(DEBUG_ENABLED) console.log("Feedback button clicked."); 
            var targetItem = 'footer';
            feedbackHelper.feedback(targetItem);
		    return false;
        });
    });
    </script>


    <script>
    // Function definitions.
    function updateStatus(msg, type, time) {
    	if(!time) {
    		time = 5000;   // milliseconds.
    	}
    	if(!type) {
    		// type = "info";  // warning, error, etc....
    	}
    	//$("#div_status_floater").text(msg);
    	//$("#div_status_floater").fadeIn().delay(time).fadeOut();
		$("#div_status_floater").textInOut(msg, type, time);
    }
    // TBD:
  	// long url should not really be truncated!!! 
    function countLongUrlChar() {
    	// var max = 400;    // TBD
    	var max = 500;    // TBD
    	if($('#input_shorturl_longurl').length > 0) {
	    	var value = $('#input_shorturl_longurl').val();
	        var len = value.length;
	    	// var remaining = (max - len) >= 0 ? (max - len) : 0; 
	    	var remaining = max - len; 
	        if (len > max) {
	         	// $('#input_shorturl_longurl').val(value.substring(0, max));
	        } else {
	         	// //$('#input_shorturl_longurl').val(value);
	        }
	        $('#longurl_char_counter').text(remaining);
	        if(remaining > 0) {
	        	$('#longurl_char_counter').css('color','black');
	        } else {
	        	$('#longurl_char_counter').css('color','red');	        	
	        }
    	}
    }
<%
if(showingMessageBox) {
%>
    function countShortMessageChar() {
    	var max = 500;   // TBD: Depending on the contentType...
    	if($('#input_shorturl_short_message').length > 0) {
	    	var value = $('#input_shorturl_short_message').val();
	        var len = value.length;
	    	var remaining = (max - len) >= 0 ? (max - len) : 0; 
	        if (len > max) {
	         	$('#input_shorturl_short_message').val(value.substring(0, max));
	        } else {
	         	//$('#input_shorturl_short_message').val(value);
	        }
	        $('#short_message_char_counter').text(remaining);
    	}
    }
<%
}
%>
    </script>

    <script>
    // Global constants.
    var customTokenMinLength = <%=ConfigUtil.getCustomTokenMinLength()%>;
    var customTokenMaxLength = <%=ConfigUtil.getCustomTokenMaxLength()%>;
    function isCustomTokenValid(token) {
        if(! token) {
        	return false;
        }
        var len = token.length;
        if(len < customTokenMinLength || len > customTokenMaxLength) {
        	return false;
        }
    	var regex;
<% if(ConfigUtil.isTokenFormatAllowSpace()) { %>
    <% if(ConfigUtil.isTokenFormatAllowSlash()) { %>
        regex = new RegExp("^[a-zA-Z0-9/\s]+$");
    <% } else { %>
        regex = new RegExp("^[a-zA-Z0-9\s]+$");
    <% } %>
<% } else { %>
    <% if(ConfigUtil.isTokenFormatAllowSlash()) { %>
        regex = new RegExp("^[a-zA-Z0-9/]+$");
    <% } else { %>
        regex = new RegExp("^[a-zA-Z0-9]+$");
    <% } %>
<% } %>
    	return regex.test(token);
    }
    </script>

    <script>
    function disableSaveButton(delay)
    {
    	$("#button_shorturl_save").attr('disabled','disabled');
    	var milli;
    	if(delay) {
    		milli = delay;
    	} else {
    		milli = 7500;  // ???
    	}
    	setTimeout(resetAndEnableSaveButton, milli);
    }
    function enableSaveButton()
    {
    	setTimeout(resetAndEnableSaveButton, 1250);  // ???
    }
    function resetAndEnableSaveButton()
    {
    	$("#button_shorturl_save").removeAttr('disabled');
    }
    </script>

    <script>
    // "Init"
    $(function() {
<%
if(shortLinkBean == null) {
%>
        $("#input_shorturl_longurl").focus().select();
<%
} else {
%>
        // $("#input_shorturl_longurl").focus();
        $("#input_shorturl_shorturl").focus().select();
<%
}
%>
        // TBD: initially hide $("#div_status_floater") ...
    	$("#div_status_floater").text("").hide();
    
        // Background image test.
        //$("#container").css("background-image", 'url(/img/bg/snowycabin.jpg)');
        //$(document.body).css("background", "grey");

        // TBD: Location...
    	var params = {x: 585, y: 70, speed: 'normal' };
    	$("#div_status_floater").makeFloat(params);
    	
    	// Peeng?
    	//var fiveTenTimer = new fiveten.FiveTenTimer(19771, 3);   // ???
    	//fiveTenTimer.start();
    });
    $(function() {
    	// TBD: PopOver init...
    	var popoverOptions = {
    	    placement: 'bottom',
    		trigger: 'manual',
    		title: 'Copy the Short URL',
    		content: 'Use Ctrl+C or Command+C to copy the shortened URL.'
    	};
        $("#input_shorturl_shorturl").popover(popoverOptions);
        // ...
    });
    $(function() {
    	countLongUrlChar();
        $("#input_shorturl_longurl").keyup(function(event) {
			countLongUrlChar();
        });
<%
if(showingMessageBox) {
%>
        countShortMessageChar();
        $("#input_shorturl_short_message").keyup(function(event) {
        	countShortMessageChar();
        });
<%
}
%>

    });
    </script>

    <script>
    // Temporary
    // To be used to "sync" the shortUrl token field and the info dialog customToken field.
    function copyAndSyncCustomToken() {
        if( editState == 'state_new' ) {
            var inputToken = $("#input_shorturl_shorturltoken").val();
            $("#input_shorturl_options_custom_token").val(inputToken);
        }
    }
    $(function() {
<% if(ConfigUtil.isUiShortLinkEditDomainPrefilled()) { %>
$("#input_shorturl_shorturltoken").keyup(function(event) {
	copyAndSyncCustomToken();
});
<% } %>

    });
    </script>

    <script>
    $(function() {
    	//$("#input_shorturl_options_expirationtime").datepicker($.datepicker.regional['en']);
    	//$.datepicker.setDefaults($.datepicker.regional['']);

        $("#input_shorturl_options_expirationtime").datetimepicker({
    	    dateFormat : 'yy-mm-dd',
            timeFormat: 'hh:mm',
    		changeMonth: true,
			numberOfMonths: 1,
			onSelect : function(dateText, inst) {
				var endT = $(this).datetimepicker('getDate');
			    var milli = endT.getTime();
		        $("#input_shorturl_options_expirationtime_millisecs").val(milli);
			    // TBD:
		        //var milli = $.datepicker.formatDate('@', $(this).datetimepicker('getDate'));
		        //$("#input_shorturl_options_expirationtime_millisecs").val(milli);
		    }
        });

    });    
    </script>

    <script>
    // App-related vars.
    var userGuid;
    var shortLinkJsBean;
    var targetPageTitle = '';
    var targetPageDescription = '';
    var twitterUsername;
<%if(isUsingTwitterAuth) {%>
<%if(userUsername != null && !userUsername.isEmpty()) {%>
    twitterUsername = "<%=userUsername%>";
<%}%>
<%}%>
    var editState = 'state_new';
<%
if(userId != null) {
%>
    userGuid = "<%=userId%>";
<%
}
%>
<%
if(shortLinkBean == null) {
%>
    shortLinkJsBean = new cannyurl.wa.bean.ShortLinkJsBean();
    if(userGuid) {
    	shortLinkJsBean.setOwner(userGuid);
    }
    // ....
<%
} else {
%>
    editState = 'state_edit';
    var shortUrlJsObjectStr = '<%=shortLinkBean.toEscapedJsonStringForJavascript()%>';
    if(DEBUG_ENABLED) console.log("shortUrlJsObjectStr = " + shortUrlJsObjectStr);
    shortLinkJsBean = cannyurl.wa.bean.ShortLinkJsBean.fromJSON(shortUrlJsObjectStr);
<%
}
%>

    if(DEBUG_ENABLED) console.log("editState = " + editState);
    if(DEBUG_ENABLED) console.log("shortLinkJsBean = " + shortLinkJsBean.toString());
    </script>

    <script>
    // Additional Init based on editState.
    $(function() {
      if(editState == 'state_new') {
        $("#button_shorturl_save").text("Create");

<% if(ConfigUtil.isUiShortLinkEditTokenTypeSelectionDisplayed() && ! TokenGenerationMethod.METHOD_SEQUENTIAL.equals(ConfigUtil.getTokenGenerationMethod())) { %>
<%
//if(BrandingHelper.getInstance().isBrandSassyURL()) {
if(ConfigUtil.isGeneralUseSassyToken()) {
%>
     $("#input_shorturl_options_sassytoken_type").show();
     // $("#input_shorturl_options_token_type").hide();
<%
} else {
%>
     // $("#input_shorturl_options_sassytoken_type").hide();
     $("#input_shorturl_options_token_type").show();
<%
}
%>
<% } else { %>
<%
// if(BrandingHelper.getInstance().isBrandSassyURL()) {
if(ConfigUtil.isGeneralUseSassyToken()) {
%>
        $("#input_shorturl_options_sassytoken_type").hide();
        // $("#input_shorturl_options_token_type").hide();
<%
} else {
%>
        // $("#input_shorturl_options_sassytoken_type").hide();
        $("#input_shorturl_options_token_type").hide();
<%
}
%>
<% } %>

<%
// if(BrandingHelper.getInstance().isBrandFlashURL()) {
if(ConfigUtil.isGeneralIncludeFlashDuration()) {
%>
        $("#input_shorturl_options_flash_duration_label").show();
        $("#input_shorturl_options_flash_duration").show();
<%
} else {
%>
        $("#input_shorturl_options_flash_duration_label").hide();
        $("#input_shorturl_options_flash_duration").hide();
<%
}
%>
        $("#button_shorturl_verify_shorturl").hide();
    	$("#button_shorturl_view_shorturl").hide();
        // $("#div_share_buttons").hide();
    	$("#button_shorturl_options").show();
        $("#button_shorturl_new").hide();
<% if(ConfigUtil.isUiShortLinkEditDomainPrefilled()) { %>
        $("#input_shorturl_shorturldomain").show();
        $("#input_shorturl_shorturltoken").show();
        $("#input_shorturl_shorturl").hide();
<% } %>        
        $("#input_shorturl_shorturltoken").removeAttr('readonly');
        $("#input_shorturl_longurl").removeAttr('readonly');
        $("#input_shorturl_options_custom_token").removeAttr('readonly');
    	//$(document.body).css("background", "silver");
    	//$("#container").css("background", "white");
    	hideFemtoUrlVariations();
    	targetPageTitle = '';
    	targetPageDescription = '';
      } else if(editState == 'state_edit') {
        $("#button_shorturl_save").text("Update");

<% if(ConfigUtil.isUiShortLinkEditTokenTypeSelectionDisplayed() && ! TokenGenerationMethod.METHOD_SEQUENTIAL.equals(ConfigUtil.getTokenGenerationMethod())) { %>
<%
//if(BrandingHelper.getInstance().isBrandSassyURL()) {
if(ConfigUtil.isGeneralUseSassyToken()) {
%>
     $("#input_shorturl_options_sassytoken_type").hide();
     // $("#input_shorturl_options_token_type").hide();
<%
} else {
%>
     // $("#input_shorturl_options_sassytoken_type").hide();
     $("#input_shorturl_options_token_type").hide();
<%
}
%>
<% } else { %>
<%
// if(BrandingHelper.getInstance().isBrandSassyURL()) {
if(ConfigUtil.isGeneralUseSassyToken()) {
%>
        $("#input_shorturl_options_sassytoken_type").hide();
        // $("#input_shorturl_options_token_type").hide();
<%
} else {
%>
        // $("#input_shorturl_options_sassytoken_type").hide();
        $("#input_shorturl_options_token_type").hide();
<%
}
%>
<% } %>

        $("#input_shorturl_options_flash_duration_label").hide();
        $("#input_shorturl_options_flash_duration").hide();
    	$("#button_shorturl_verify_shorturl").show();
    	$("#button_shorturl_view_shorturl").show();
        // // $("#div_share_buttons").show();
        // $("#div_share_buttons").hide();
    	$("#button_shorturl_options").show();
        $("#button_shorturl_new").show();
<% if(ConfigUtil.isUiShortLinkEditDomainPrefilled()) { %>
    	$("#input_shorturl_shorturldomain").hide();
    	$("#input_shorturl_shorturltoken").hide();
        $("#input_shorturl_shorturl").show();
<% } %>
        $("#input_shorturl_shorturltoken").attr('readonly','readonly');
        $("#input_shorturl_longurl").attr('readonly','readonly');
        $("#input_shorturl_options_custom_token").attr('readonly','readonly');
    	//$(document.body).css("background", "silver");
    	//$("#container").css("background", "ivory");
    	showFemtoUrlVariations();
    	setTargetPageTitleAndDescription();
      } else {
    	// 'state_read'  // TBD...
        $("#button_shorturl_save").hide();

<% if(ConfigUtil.isUiShortLinkEditTokenTypeSelectionDisplayed() && ! TokenGenerationMethod.METHOD_SEQUENTIAL.equals(ConfigUtil.getTokenGenerationMethod())) { %>
<%
// if(BrandingHelper.getInstance().isBrandSassyURL()) {
if(ConfigUtil.isGeneralUseSassyToken()) {
%>
     $("#input_shorturl_options_sassytoken_type").show();
     // $("#input_shorturl_options_token_type").hide();
<%
} else {
%>
     // $("#input_shorturl_options_sassytoken_type").hide();
     $("#input_shorturl_options_token_type").show();
<%
}
%>
<% } else { %>
<%
// if(BrandingHelper.getInstance().isBrandSassyURL()) {
if(ConfigUtil.isGeneralUseSassyToken()) {
%>
     $("#input_shorturl_options_sassytoken_type").hide();
     // $("#input_shorturl_options_token_type").hide();
<%
} else {
%>
     // $("#input_shorturl_options_sassytoken_type").hide();
     $("#input_shorturl_options_token_type").hide();
<%
}
%>
<% } %>

<%
// if(BrandingHelper.getInstance().isBrandFlashURL()) {
if(ConfigUtil.isGeneralIncludeFlashDuration()) {
%>
        $("#input_shorturl_options_flash_duration_label").show();
        $("#input_shorturl_options_flash_duration").show();
<%
} else {
%>
        $("#input_shorturl_options_flash_duration_label").hide();
        $("#input_shorturl_options_flash_duration").hide();
<%
}
%>
    	$("#button_shorturl_verify_shorturl").hide();
    	$("#button_shorturl_view_shorturl").hide();
        // $("#div_share_buttons").hide();
    	$("#button_shorturl_options").show();
        $("#button_shorturl_new").hide();
<% if(ConfigUtil.isUiShortLinkEditDomainPrefilled()) { %>
    	$("#input_shorturl_shorturldomain").show();
    	$("#input_shorturl_shorturltoken").show();
        $("#input_shorturl_shorturl").hide();
<% } %>
        $("#input_shorturl_shorturltoken").removeAttr('readonly');
        $("#input_shorturl_longurl").removeAttr('readonly');
        $("#input_shorturl_options_custom_token").removeAttr('readonly');
    	//$(document.body).css("background", "silver");
    	//$("#container").css("background", "gray");
    	hideFemtoUrlVariations();
    	targetPageTitle = '';
    	targetPageDescription = '';
    	// TBD: make title/content readonly...
      }
    });
    </script>

    <script>
    // Event handlers
    $(function() {
    	
    	$("#cannyurl_questionmark_link").click(function() {
    	    if(DEBUG_ENABLED) console.log("Question Mark link clicked.");

    	    $( "#div_questionmark_dialog" ).dialog({
                height: 360,
                width: 560,
                modal: true,
                buttons: {
                     OK: function() {
                         $(this).dialog('close'); 
                     }
                }
            });

    		return false;
    	});
        
<%
// if(BrandingHelper.getInstance().isBrandFemtoURL()) { 
if(ConfigUtil.isGeneralShowUrlVariations()) {
%>    	
    	$("#femtourl_variations_questionmark_link").click(function() {
    	    if(DEBUG_ENABLED) console.log("FemtoURL help link clicked.");
    	    $( "#femtourl_variations_help_dialog" ).dialog({
                height: 360,
                width: 500,
                modal: true,
                buttons: {
                     OK: function() {
                         $(this).dialog('close'); 
                     }
                }
            });
    		return false;
    	});
<%
}
%>
    	
    	$("#button_shorturl_verify_shorturl").click(function() {
    	    if(DEBUG_ENABLED) console.log("ShortUrl verify button clicked.");
    		if(shortLinkJsBean) {
   		        updateStatus('Opening the verify page ...', 'info', 5550);
                // TBD....
                var redirectUrlHelper = new cannyurl.redirecturl.RedirectUrlHelper(shortLinkJsBean);
                if(redirectUrlHelper.isReady()) {
                    var verifyRedirectUrl = redirectUrlHelper.verifyUrl();
                    window.location = verifyRedirectUrl;
                } else {
            	    if(DEBUG_ENABLED) console.log("ShortUrl verify failed.");
       		        updateStatus('Failed to open the verify page. ...', 'error', 5550);
                }
    		} else {
        	    if(DEBUG_ENABLED) console.log("ShortUrl verify cannot be performed.");
   		        updateStatus('Verify page cannot be opened. ...', 'error', 5550);
    		}
    		return false;
    	});
        
    	$("#button_shorturl_view_shorturl").click(function() {
    	    if(DEBUG_ENABLED) console.log("ShortUrl view button clicked.");
    		var shortUrl = $("#input_shorturl_shorturl").val();
    		if(shortUrl != '') {
   		        updateStatus('Redirecting ...', 'info', 5550);
	    		window.location = shortUrl;
    		} else {
                // $("#button_shorturl_view_shorturl").fadeOut(450).fadeIn(950);
    		}
    		return false;
    	});

<%
// if(BrandingHelper.getInstance().isBrandFemtoURL()) { 
if(ConfigUtil.isGeneralShowUrlVariations()) {
%>    	
    	$("#button_femtourl_variations_femtourl_view").click(function() {
    	    if(DEBUG_ENABLED) console.log("ShortUrl, femtourl, view button clicked.");
    		var shortUrl = $("#input_femtourl_variations_femtourl").val();
    		if(shortUrl != '') {
   		        updateStatus('Redirecting ...', 'info', 5550);
	    		window.location = shortUrl;
    		}
    		return false;
    	});
    	$("#button_femtourl_variations_cannyurl_view").click(function() {
    	    if(DEBUG_ENABLED) console.log("ShortUrl, cannyurl, view button clicked.");
    		var shortUrl = $("#input_femtourl_variations_cannyurl").val();
    		if(shortUrl != '') {
   		        updateStatus('Redirecting ...', 'info', 5550);
	    		window.location = shortUrl;
    		}
    		return false;
    	});
    	$("#button_femtourl_variations_flashurl_view").click(function() {
    	    if(DEBUG_ENABLED) console.log("ShortUrl, flashurl, view button clicked.");
    		var shortUrl = $("#input_femtourl_variations_flashurl").val();
    		if(shortUrl != '') {
   		        updateStatus('Redirecting ...', 'info', 5550);
	    		window.location = shortUrl;
    		}
    		return false;
    	});
    	$("#button_femtourl_variations_quickurl_view").click(function() {
    	    if(DEBUG_ENABLED) console.log("ShortUrl, quickurl, view button clicked.");
    		var shortUrl = $("#input_femtourl_variations_quickurl").val();
    		if(shortUrl != '') {
   		        updateStatus('Redirecting ...', 'info', 5550);
	    		window.location = shortUrl;
    		}
    		return false;
    	});    	
<%
}
%>
  	
	    $("#button_shorturl_options").click(function() {
		    if(DEBUG_ENABLED) console.log("ShortLink Options button clicked.");

/*
		    // TBD: TokenType has been moved out of options dialog box....
		    var tokenType = '';
		    if(shortLinkJsBean) {
		    	tokenType = shortLinkJsBean.getTokenType();
		    }
		    if(tokenType) {
		    	$("#input_shorturl_options_token_type").val(tokenType);
		    } else {
		    	//$("#input_shorturl_options_token_type").val('');  // ???
		    }
*/

		    var customToken = '';
            var tokenType = '';
		    if(shortLinkJsBean) {
		    	customToken = shortLinkJsBean.getToken();   // ???
		    	tokenType = shortLinkJsBean.getTokenType();
		    }
<% if(ConfigUtil.isUiShortLinkEditDomainPrefilled()) { %>
            if( editState == 'state_new' ) {
                var inputToken = $("#input_shorturl_shorturltoken").val();    // ?????
            	customToken = inputToken;   // ????
                if(inputToken != '') {       // ????
                	tokenType = 'custom';
                } else {
                	// What to do?????
                	tokenType = '';   // ????
                }
            }
<% } %>
<% if(ConfigUtil.isUiShortLinkEditDomainPrefilled()) { %>
            if(tokenType == 'custom') {
                $("#input_shorturl_options_custom_token").val(customToken);    // ?????
            }
<% } else { %>
		    if( customToken != '' && (editState == 'state_new' || tokenType == 'custom') ) {
		    	$("#input_shorturl_options_custom_token").val(customToken);
		    } else {
		    	$("#input_shorturl_options_custom_token").val('');  // ???
		    }
<% } %>

		    var expirationDate = '';
		    var expirationTime = 0;
		    if(shortLinkJsBean) {
		    	expirationTime = shortLinkJsBean.getExpirationTime();
		    	if(expirationTime && expirationTime > 0) {
		    		expirationDate = getStringFromTime(expirationTime);
		    	}
		    }
		    if(expirationTime) {
		    	$("#input_shorturl_options_expirationtime_millisecs").val(expirationTime);
		    } else {
		    	$("#input_shorturl_options_expirationtime_millisecs").val('0');  // ???
		    }
		    if(expirationDate) {
		    	$("#input_shorturl_options_expirationtime").val(expirationDate);
		    } else {
		    	$("#input_shorturl_options_expirationtime").val('');  // ???
		    }

		    
   	      if(editState == 'state_new') {
            $( "#div_shorturl_options_dialog" ).dialog({
                height: 250,
                width: 400,
                modal: true,
                buttons: {
                     Update: function() {
    
                    	// TBD
                        //updateStatus('Saving the short URL...', 'info', 5550);
                        //disableSaveButton();
                        //saveShortLinkJsBean();
						// ...

						var tok = $("#input_shorturl_options_custom_token").val().trim();
						
						// TBD:
					    // Validate tok ????
					    // ...

					    if(shortLinkJsBean) {
					        shortLinkJsBean.setToken(tok);
						}
						
						if(tok != '') {
							// temporary
					        $("#input_shorturl_shorturl").val('(' + tok + ')');
<% if(ConfigUtil.isUiShortLinkEditDomainPrefilled()) { %>
					        $("#input_shorturl_shorturltoken").val(tok);
<% } %>
						} else {
					        $("#input_shorturl_shorturl").val('');
<% if(ConfigUtil.isUiShortLinkEditDomainPrefilled()) { %>
     				        $("#input_shorturl_shorturltoken").val('');
<% } %>
						}

<% if(ConfigUtil.isUiShortLinkEditTokenTypeSelectionDisplayed() && ! TokenGenerationMethod.METHOD_SEQUENTIAL.equals(ConfigUtil.getTokenGenerationMethod())) { %>
<%
// if(BrandingHelper.getInstance().isBrandSassyURL()) {
if(ConfigUtil.isGeneralUseSassyToken()) {
%>
						if(tok != '') {
						    $("#input_shorturl_options_sassytoken_type").attr('disabled','disabled');;
						} else {
						    $("#input_shorturl_options_sassytoken_type").removeAttr('disabled');
						}
<%
} else {
%>
						if(tok != '') {
						    $("#input_shorturl_options_token_type").attr('disabled','disabled');;
						} else {
						    $("#input_shorturl_options_token_type").removeAttr('disabled');
						}
<%
}
%>
<% } else { %>
<% } %>

                        // Close the dialog....
                        $(this).dialog('close'); 
                     },
                     Reset: function() {
	                   	$("#input_shorturl_options_custom_token").val('');
<% if(ConfigUtil.isUiShortLinkEditDomainPrefilled()) { %>
                        //  $("#input_shorturl_shorturltoken") ?????
<% } %>

					    // temporary
				        $("#input_shorturl_shorturl").val('');
<% if(ConfigUtil.isUiShortLinkEditDomainPrefilled()) { %>
  				        $("#input_shorturl_shorturltoken").val('');
<% } %>

<% if(ConfigUtil.isUiShortLinkEditTokenTypeSelectionDisplayed() && ! TokenGenerationMethod.METHOD_SEQUENTIAL.equals(ConfigUtil.getTokenGenerationMethod())) { %>
<%
// if(BrandingHelper.getInstance().isBrandSassyURL()) {
if(ConfigUtil.isGeneralUseSassyToken()) {
%>
                        $("#input_shorturl_options_sassytoken_type").removeAttr('disabled');
                        // $("#input_shorturl_options_token_type").removeAttr('disabled');
<%
} else {
%>
                        // $("#input_shorturl_options_sassytoken_type").removeAttr('disabled');
                        $("#input_shorturl_options_token_type").removeAttr('disabled');
<%
}
%>
<% } else { %>
<% } %>

                        if(shortLinkJsBean) {
					        shortLinkJsBean.setToken('');
						}
                        $(this).dialog('close');
                     },
                     Cancel: function() {
                         $(this).dialog('close'); 
                         //aform.resetForm();
                     }
                }
            });
   	      } else {
              $( "#div_shorturl_options_dialog" ).dialog({
                  height: 250,
                  width: 400,
                  modal: true,
                  buttons: {
                       Cancel: function() {
                           $(this).dialog('close'); 
                       }
                  }
              });
   	      }
            
		});
    	
/* */
	    $("#button_shorturl_new").click(function() {
	      if(DEBUG_ENABLED) console.log("New button clicked.");
	      var shortUrlNewPage = "<%=topLevelUrl%>" + "shorten/";
	      var ans = window.confirm("Create a new short URL?");
	      if(ans) {
		      updateStatus('Creating a new short URL...', 'info', 5550);
	 	      window.location = shortUrlNewPage;
		  }
	    });
/* */

    });
    </script>

    <script>
    // Ajax form handlers.
  $(function() {
    $("#button_shorturl_save").click(function() {
      // TBD:
      // Disable the button here
      // and enable again (some delay) after ajax call returns
      //     or it times out (because the call has failed, etc.).
      
    
      //if(DEBUG_ENABLED) console.log("Save button clicked.");
      updateStatus('Saving the short URL...', 'info', 5550);
      disableSaveButton();
    	
      saveShortLinkJsBean();

    });
  });
  </script>

  <script>
  var saveShortLinkJsBean = function() {

      // validate and process form here    

      var longUrl = $("#input_shorturl_longurl").val().trim();
      if(DEBUG_ENABLED) console.log("longUrl = " + longUrl);
      var longUrlInvalid = false;
      if(! longUrl) {
          if(DEBUG_ENABLED) console.log("longUrl is empty");
          longUrlInvalid = true;
      } else {
          // URLs without leading "http://" or "https://"
          // (Note: Missing http:// is appended on the server side...)
          // But isUrlInvalid() will return true, if we don't do this here.
          if(longUrl.indexOf("http://") !== 0 && longUrl.indexOf("https://") !== 0) {
    	      longUrl = "http://" + longUrl;
    	      $("#input_shorturl_longurl").val(longUrl);   // ????
          }
          if(isUrlInvalid(longUrl)) {  
              longUrlInvalid = true;
          }
      }
      if(longUrlInvalid) {
          if(DEBUG_ENABLED) console.log("Target URL is invalid");
          updateStatus('Target URL is invalid.', 'error', 5550);
          enableSaveButton();
    	  return false;
      }

      // temporary
      var longUrlLength = longUrl.length;
      if(longUrlLength > 500) {
          if(DEBUG_ENABLED) console.log("Target URL is too long");
          updateStatus('Target URL is too long. Currently, the URL should be no longer than 500 characters.', 'error', 5550);
          enableSaveButton();
    	  return false;    	  
      }


      shortLinkJsBean.setLongUrl(longUrl);
      if(userGuid) {   
         shortLinkJsBean.setOwner(userGuid);  // This should not be necessary..
      }
      // TBD: Depending on authMode/domaintype....
      // for now, just use the twitter username, if set...
      if(twitterUsername) {
    	  shortLinkJsBean.setUsername(twitterUsername);
      }
      if(DEBUG_ENABLED) console.log("twitterUsername = " + twitterUsername);
      // ...

      
      // Update the beans with input values

<% if(ConfigUtil.isUiShortLinkEditTokenTypeSelectionDisplayed() && ! TokenGenerationMethod.METHOD_SEQUENTIAL.equals(ConfigUtil.getTokenGenerationMethod())) { %>
<%
// if(BrandingHelper.getInstance().isBrandSassyURL()) {
if(ConfigUtil.isGeneralUseSassyToken()) {
%>
	var sassyTokenType = $("#input_shorturl_options_sassytoken_type").val().trim();
	if(DEBUG_ENABLED) console.log("sassyTokenType = " + sassyTokenType);
	if(sassyTokenType) {
	    shortLinkJsBean.setTokenType('<%=TokenType.TYPE_SASSY%>');  // ???
	    // ????
	    shortLinkJsBean.setSassyTokenType(sassyTokenType);
	    // ...
	}
<%
} else {
%>
    // Note: this may be overwritten below if customToken is set....
	var tokenType = $("#input_shorturl_options_token_type").val().trim();
	if(DEBUG_ENABLED) console.log("tokenType = " + tokenType);
	if(tokenType) {
	    shortLinkJsBean.setTokenType(tokenType);
	}
<%
}
%>
<% } else { %>

<% if(TokenGenerationMethod.METHOD_SEQUENTIAL.equals(ConfigUtil.getTokenGenerationMethod())) { %>
    // ???
	var tokenType = 'sequential';	
	if(DEBUG_ENABLED) console.log("tokenType = " + tokenType);
	if(tokenType) {
	    shortLinkJsBean.setTokenType(tokenType);
	}
<% } %>

<% } %>

      var flashDurationSecs = $("#input_shorturl_options_flash_duration").val().trim();
      if(DEBUG_ENABLED) console.log("flashDurationSecs = " + flashDurationSecs);
      var expirationTime = $("#input_shorturl_options_expirationtime_millisecs").val();
      if(DEBUG_ENABLED) console.log("expirationTime = " + expirationTime);
      if(flashDurationSecs) {
          shortLinkJsBean.setFlashDuration(parseInt(flashDurationSecs) * 1000);  // millisecs
      }
      if(expirationTime) {
          shortLinkJsBean.setExpirationTime(expirationTime);
      }
      // ...
      
      // TBD:
      // set tokenType to custom if customToken != ''....
      var customToken = $("#input_shorturl_options_custom_token").val().trim();
      if(DEBUG_ENABLED) console.log("customToken = " + customToken);
      if(customToken != '') {
    	  // var tokenLen = customToken.length;
    	  // if(tokenLen < customTokenMinLength || tokenLen > customTokenMaxLength) {
	      //     updateStatus('Invalid custom token. Should be longer than ' + customTokenMinLength + ' and shorter than ' + customTokenMaxLength, 'error', 5550);
	      //     enableSaveButton();
	      //  return false;
    	  // }
    	  if(! isCustomTokenValid(customToken)) {
	          updateStatus('Invalid custom token: ' + customToken + '. Custom token should be alphanumeric, and it should be longer than ' + customTokenMinLength + ' and shorter than ' + customTokenMaxLength + ' characters', 'error', 5550);
	          enableSaveButton();
	    	  return false;    		  
    	  }

    	  shortLinkJsBean.setToken(customToken);
    	  tokenType = 'custom';
          shortLinkJsBean.setTokenType(tokenType);
      }
      // ...
      

<%
if(showingMessageBox) {
%>
      var shortMessage = $("#input_shorturl_short_message").val().trim();
      if(DEBUG_ENABLED) console.log("shortMessage = " + shortMessage);

      var shortMessageLength = shortMessage.length;
      if(longUrlLength > 500) {
          if(DEBUG_ENABLED) console.log("shortMessage is too long");
          shortMessage = shortMessage.substring(0, 497) + "...";     // ????
      }
      shortLinkJsBean.setShortMessage(shortMessage);
<%
}
%>      
      
   	  // Update the modifiedTime field ....
   	  if(editState != 'state_new') {
   		  var modifiedTime = (new Date()).getTime();
          shortLinkJsBean.setModifiedTime(modifiedTime);
   	  }

      // Payload...
      var shortUrlJsonStr = JSON.stringify(shortLinkJsBean);
      if(DEBUG_ENABLED) console.log("shortUrlJsonStr = " + shortUrlJsonStr);
      var payload = "{\"shortLink\":" + shortUrlJsonStr;
      // TBD...
      payload += "}";
      if(DEBUG_ENABLED) console.log("payload = " + payload);
      
      
      // Web service settings.
      var shortUrlPostEndPoint = "<%=topLevelUrl%>" + "ajax/shortlink";
      if(DEBUG_ENABLED) console.log("shortUrlPostEndPoint =" + shortUrlPostEndPoint);
      var httpMethod;
      var webServiceUrl;
      if(editState == 'state_new') {
    	  httpMethod = "POST";
    	  webServiceUrl = shortUrlPostEndPoint;
	  } else if(editState == 'state_edit') {
    	  httpMethod = "PUT";
    	  webServiceUrl = shortUrlPostEndPoint + "/" + shortLinkJsBean.getGuid();
      } else {
    	  // ???
   		  if(DEBUG_ENABLED) console.log("Error. Invalid editState = " + editState);
          updateStatus('Error occurred.', 'error', 5550);
          enableSaveButton();
    	  return false;
      }

      if(httpMethod) {
        $.ajax({
    	    type: httpMethod,
    	    url: webServiceUrl,
    	    data: payload,
    	    dataType: "json",
    	    contentType: "application/json; charset=UTF-8",
    	    success: function(data, textStatus) {
    	      // TBD
    	      if(DEBUG_ENABLED) console.log("shortLinkJsBean successfully saved. textStatus = " + textStatus);
    	      if(DEBUG_ENABLED) console.log("data = " + data);
    	      if(DEBUG_ENABLED) console.log(data);

    	      // History
    	      // Do this only if the current editState == 'state_new'
    	      if(editState == 'state_new') {
	    	      var History = window.History;
	    	      if(History) {
	   	    	    	// TBD: Need to html escape????
	    				var htmlPageTitle = '<%=brandDisplayName%> - ' + shortLinkJsBean.getLongUrl();
	    	            // TBD: This does not work on HTML4 browsers (e.g., IE)
				        History.pushState({state:1}, htmlPageTitle, "/shorten/" + shortLinkJsBean.getGuid());
	    	      }
    	      }
    	      
    	      // If it is currently editState == 'state_new', change it to 'state_edit'.
    	      // If it is currently editState == 'state_edit', that's ok too.
    	      editState = 'state_edit';

    	      // Parse data...
    	      if(data) {   // POST only..
	    	      // Replace/Update the JsBeans ...

	    	      var shortUrlObj = data["shortLink"];
	    	      if(DEBUG_ENABLED) console.log("shortUrlObj = " + shortUrlObj);
	    	      if(DEBUG_ENABLED) console.log(shortUrlObj);
	    	      shortLinkJsBean = cannyurl.wa.bean.ShortLinkJsBean.create(shortUrlObj);
	    	      if(DEBUG_ENABLED) console.log("New shortLinkJsBean = " + shortLinkJsBean);
	    	      if(DEBUG_ENABLED) console.log(shortLinkJsBean);
	    
	    	      // Delay this just a bit so that the server has time to actually save it (Note: we are using async call for saving).
	    	      // This helps when a new shortUrl is saved. 
	    	      // TBD: In both save/update, various buttons should be disabled before the ajax call
	    	      //   and they should be re-enabled after the call successuflly returns or after certain preset time.
	    	      window.setTimeout(refreshFormFields, 1250);
	              updateStatus('Short URL successfully saved.', 'info', 5550);
    	      } else {
    	    	  // ignore
    	    	  // PUT currently returns "OK" only.
    	    	  // TBD: Change the ajax server side code so that it returns the updated data.
    	    	  //      Based on the returned/updated data, some fields (e.g., shortUrl) may need to be updated....
	              updateStatus('Short URL successfully updated.', 'info', 5550);
    	      } 
    	      enableSaveButton();
    	      
    	      // For performance heck!!!!
    	      // "Finger" PageSynopsis page here ...
    	      // Note: This also updates targetPageTitle/Description global variables.....
    	      fetchPageInfo(longUrl);
    	      // Heck!!!
    	    },
    	    error: function(req, textStatus) {
    	    	var errorMsg = 'Failed to save the short URL. status = ' + textStatus;
          	    if(DEBUG_ENABLED) console.log(errorMsg);

          	    if(shortLinkJsBean && shortLinkJsBean.getTokenType() == 'custom') {
    	    		errorMsg += ". Try a differnt custom token.";
    	    	}
    	    	updateStatus(errorMsg, 'error', 5550);
    	    	enableSaveButton();
    	    }
	   	  });
      } else {
    	  // ???
    	  enableSaveButton();
      }

  };
  </script>

  <script>
  // Refresh input fields...
  var refreshFormFields = function() {
	if(DEBUG_ENABLED) console.log("refreshFormFields() called......");

	if(editState == 'state_new') {
		if(DEBUG_ENABLED) console.log("editState = " + editState);

        $("#button_shorturl_save").text("Create");

<% if(ConfigUtil.isUiShortLinkEditTokenTypeSelectionDisplayed() && ! TokenGenerationMethod.METHOD_SEQUENTIAL.equals(ConfigUtil.getTokenGenerationMethod())) { %>
<%
// if(BrandingHelper.getInstance().isBrandSassyURL()) {
if(ConfigUtil.isGeneralUseSassyToken()) {
%>
        $("#input_shorturl_options_sassytoken_type").show();
        // $("#input_shorturl_options_token_type").hide();
<%
} else {
%>
        // $("#input_shorturl_options_sassytoken_type").hide();
        $("#input_shorturl_options_token_type").show();
<%
}
%>
<% } else { %>
<%
// if(BrandingHelper.getInstance().isBrandSassyURL()) {
if(ConfigUtil.isGeneralUseSassyToken()) {
%>
        $("#input_shorturl_options_sassytoken_type").hide();
        // $("#input_shorturl_options_token_type").hide();
<%
} else {
%>
        // $("#input_shorturl_options_sassytoken_type").hide();
        $("#input_shorturl_options_token_type").hide();
<%
}
%>
<% } %>

<%
// if(BrandingHelper.getInstance().isBrandFlashURL()) {
if(ConfigUtil.isGeneralIncludeFlashDuration()) {
%>
        $("#input_shorturl_options_flash_duration_label").show();
        $("#input_shorturl_options_flash_duration").show();
<%
} else {
%>
        $("#input_shorturl_options_flash_duration_label").hide();
        $("#input_shorturl_options_flash_duration").hide();
<%
}
%>
    	$("#button_shorturl_verify_shorturl").hide();
    	$("#button_shorturl_view_shorturl").hide();
        // $("#div_share_buttons").hide();
    	$("#button_shorturl_options").show();
        $("#button_shorturl_new").hide();
<% if(ConfigUtil.isUiShortLinkEditDomainPrefilled()) { %>
    	$("#input_shorturl_shorturldomain").show();
    	$("#input_shorturl_shorturltoken").show();
        $("#input_shorturl_shorturl").hide();
<% } %>
        $("#input_shorturl_shorturltoken").removeAttr('readonly');
        $("#input_shorturl_longurl").removeAttr('readonly');
        $("#input_shorturl_options_custom_token").removeAttr('readonly');
    	//$(document.body).css("background", "silver");
    	//$("#container").css("background", "white");
        hideFemtoUrlVariations();
        targetPageTitle = '';
        targetPageDescription = '';

        // Refresh the title...
		var htmlPageTitle = '<%=brandDisplayName%>';
        $('title').text(htmlPageTitle);
	} else if(editState == 'state_edit') {
		if(DEBUG_ENABLED) console.log("editState = " + editState);

		var htmlPageTitle = '<%=brandDisplayName%>';
		if(shortLinkJsBean) {
			var title = shortLinkJsBean.getLongUrl();
			htmlPageTitle += ' - ' + title;    // TBD: Need to html escape????

		    // TBD:
			var shortUrl = shortLinkJsBean.getShortUrl();
			$("#input_shorturl_shorturl").val(shortUrl);
<% if(ConfigUtil.isUiShortLinkEditDomainPrefilled()) { %>
            var token = shortLinkJsBean.getToken();
	        $("#input_shorturl_shorturltoken").val(token);
<% } %>
			var longUrl = shortLinkJsBean.getLongUrl();    // LongURL might have been modified on the server side (e.g., http;// added, etc.)
			$("#input_shorturl_longurl").val(longUrl);
			
			// Refresh other fields????
			
			// TBD: Update the shortUrl title???
		    // Is that necessary?
			
	        $("#button_shorturl_save").text("Update");

<% if(ConfigUtil.isUiShortLinkEditTokenTypeSelectionDisplayed() && ! TokenGenerationMethod.METHOD_SEQUENTIAL.equals(ConfigUtil.getTokenGenerationMethod())) { %>
<%
// if(BrandingHelper.getInstance().isBrandSassyURL()) {
if(ConfigUtil.isGeneralUseSassyToken()) {
%>
           $("#input_shorturl_options_sassytoken_type").hide();
           // $("#input_shorturl_options_token_type").hide();
<%
} else {
%>
           // $("#input_shorturl_options_sassytoken_type").hide();
           $("#input_shorturl_options_token_type").hide();
<%
}
%>
<% } else { %>
<%
// if(BrandingHelper.getInstance().isBrandSassyURL()) {
if(ConfigUtil.isGeneralUseSassyToken()) {
%>
           $("#input_shorturl_options_sassytoken_type").hide();
           // $("#input_shorturl_options_token_type").hide();
<%
} else {
%>
           // $("#input_shorturl_options_sassytoken_type").hide();
           $("#input_shorturl_options_token_type").hide();
<%
}
%>
<% } %>

	        $("#input_shorturl_options_flash_duration_label").hide();
	        $("#input_shorturl_options_flash_duration").hide();
	    	$("#button_shorturl_verify_shorturl").show();
	    	$("#button_shorturl_view_shorturl").show();
	        // // $("#div_share_buttons").show();
	        // $("#div_share_buttons").hide();
	    	$("#button_shorturl_options").show();
	        $("#button_shorturl_new").show();
<% if(ConfigUtil.isUiShortLinkEditDomainPrefilled()) { %>
    	$("#input_shorturl_shorturldomain").hide();
    	$("#input_shorturl_shorturltoken").hide();
        $("#input_shorturl_shorturl").show();
<% } %>
	        $("#input_shorturl_shorturltoken").attr('readonly','readonly');
	        $("#input_shorturl_longurl").attr('readonly','readonly');;
	        $("#input_shorturl_options_custom_token").attr('readonly','readonly');
	    	//$(document.body).css("background", "silver");
	    	//$("#container").css("background", "ivory");
            showFemtoUrlVariations();
            setTargetPageTitleAndDescription();
		} else {
			// ???
		}	

		// Refresh the title...
        $('title').text(htmlPageTitle);



		// Focus...
        $("#input_shorturl_shorturl").focus().select();
        $("#input_shorturl_shorturl").popover('show');
        setTimeout(function() {$("#input_shorturl_shorturl").popover('hide');}, 5750);
        // ....
		
		
		// ....
		

     } else {
		if(DEBUG_ENABLED) console.log("invalid state: editState = " + editState);

		// ????
		// Refresh the title...
		var htmlPageTitle = '<%=brandDisplayName%>';
        $('title').text(htmlPageTitle);
	}
  };
  </script>

  <script>
  var showFemtoUrlVariations = function() {
	if(DEBUG_ENABLED) console.log("showFemtoUrlVariations() called......");

    var showVariations = false;
<%
// if(BrandingHelper.getInstance().isBrandFemtoURL()) { 
if(ConfigUtil.isGeneralShowUrlVariations()) {
%>
if(shortLinkJsBean) {
  // TBD....
  var redirectUrlHelper = new cannyurl.redirecturl.RedirectUrlHelper(shortLinkJsBean);
  if(redirectUrlHelper.isReady()) {
    var femtoUrl = redirectUrlHelper.permanentUrl();
    $("#input_femtourl_variations_femtourl").val(femtoUrl);
    var cannyUrl = redirectUrlHelper.confirmUrl();
    $("#input_femtourl_variations_cannyurl").val(cannyUrl);
    var flashUrl = redirectUrlHelper.flashUrl();
    $("#input_femtourl_variations_flashurl").val(flashUrl);
    var quickUrl = redirectUrlHelper.flashUrl(5);
    $("#input_femtourl_variations_quickurl").val(quickUrl);
    showVariations = true;
  } else {
    if(DEBUG_ENABLED) console.log("Failed to initialize cannyurl.redirecturl.RedirectUrlHelper.");
  }
}
<%
}
%>

    if(showVariations == true) {
      $("#div_femtourl_variations_container").show();
    }
  };
  var hideFemtoUrlVariations = function() {
	if(DEBUG_ENABLED) console.log("hideFemtoUrlVariations() called......");

<%
// if(BrandingHelper.getInstance().isBrandFemtoURL()) { 
if(ConfigUtil.isGeneralShowUrlVariations()) {
%>
    $("#input_femtourl_variations_femtourl").val('');
    $("#input_femtourl_variations_cannyurl").val('');
    $("#input_femtourl_variations_flashurl").val('');
    $("#input_femtourl_variations_quickurl").val('');
<%
}
%>

	$("#div_femtourl_variations_container").hide();
  };
  </script>

  <script>
  // Primarily for "fingering" PageSynopsis site.
  var fetchPageInfo = function(longUrl) {
    if(DEBUG_ENABLED) console.log("fetchPageInfo() called......");
    
    // TBD:
    // Check first if longUrl is a valid URL...
    // temporary
    // TBD: Use isUrlInvalid() ?????
    var urlregex = new RegExp("^http");
    if(!urlregex.test(longUrl)) {
    	if(DEBUG_ENABLED) console.log("longUrl invalid. Skipping fetchPagInfo(): longUrl = " + longUrl);
    	return;
    }
    // ....

    var encodedLongUrl = encodeURIComponent(longUrl);
    $.ajax({
	    type: 'GET',
	    // url: 'http://www.pagesynopsis.com/pageinfo?targetUrl=' +  encodedLongUrl,
	    url: 'http://www.pagesynopsis.com/pagefetch?targetUrl=' +  encodedLongUrl,
	    dataType: "jsonp",
	    timeout: 20000,
	    jsonp: 'callback',
	    success: function(data, textStatus) {
	        if(DEBUG_ENABLED) console.log("PageInfo successfully fetched. textStatus = " + textStatus );
		    //if(DEBUG_ENABLED) console.log("data = " + data);
		    //if(DEBUG_ENABLED) console.log(data);

		    // Parse data...
		    if(data) {
		        // TBD: We can optionally display Page info on the Edit page as well
		        //      e.g., as a popup, or bubble help, etc...
		        // ....

		        var pageTitle = data.pageTitle;
		        var pageAuthor = data.pageAuthor;
		        // var pageDescription = data.pageDescription;
		        var pageDescription = data.pageSummary;
			    if(DEBUG_ENABLED) console.log('pageTitle = ' + pageTitle);
			    if(DEBUG_ENABLED) console.log('pageAuthor = ' + pageAuthor);
			    if(DEBUG_ENABLED) console.log('pageDescription = ' + pageDescription);
			    // ...
		        
			    // Save it to the global var....
			    if(pageTitle) {
			    	targetPageTitle = pageTitle;
			    } else {
				    //targetPageTitle = '';   // ????
			    }
			    if(pageDescription) {
			    	targetPageDescription = pageDescription;
			    } else {
				    //targetPageDescription = '';   // ????
			    }
		    } 
	    },
	    error: function(xhr, textStatus) {
	        if(DEBUG_ENABLED) console.log("PageInfo fetch failed. textStatus = " + textStatus );
	    }
    });
  };
  
  $(function() {
	  // ...
  });
  </script>


<script>
  var setTargetPageTitleAndDescription = function() {
	  // For now, it does notthing...
	  // fetchPageInfo() is called when the short link is saved the first time...

	  // This may still need to be called for the cases when /shorten/<guid> is directly loaded...  Ignore for now....
	  //fetchPageInfo();
  };
</script>



<script>
$(function() {
	// Top menu handlers
	// Note: /view requires guid....
	// TBD: Due to the way we use subdomains...
	//      Every time we change url (subdomain), the login state changes...
	//      We use 27 subdomains, therfore this is not not very usable...
	// For now, all urls should point to relative URLs "/view", "/verify", etc...
	//      rather than path-based URLs, e.g., a.fm.gs/i/abcd, ....
    $("#topmenu_nav_shorten").click(function() {
        if(DEBUG_ENABLED) console.log("topmenu_nav_shorten anchor clicked."); 
        window.location = "/shorten";
        //if(editState == 'state_new') {
        //    window.location = "/shorten";
        //} else {
        //    if(shortLinkJsBean) {
        //        var guid = shortLinkJsBean.getGuid();
        //        window.location = "/shorten/" + guid;            		
        //    } else {
        //        // ??? error ???
        //    }
        //}
        return false;
    });
    $("#topmenu_nav_view").click(function() {
        if(DEBUG_ENABLED) console.log("topmenu_nav_view anchor clicked."); 
        if(editState == 'state_new') {
            ////window.location = "/view";
            // ???
            $("#topmenu_nav_view").fadeOut(450).fadeIn(950);
        } else {
            if(shortLinkJsBean) {
            	//var shortUrl = shortLinkJsBean.getShortUrl();
                //if(! shortUrl) {
                //	var domain = shortLinkJsBean.getDomain();
                //   	var token = shortLinkJsBean.getToken();
                //   	if(domain && token) {
                //      	shortUrl = domain + token;
                //   	} else {
                        var guid = shortLinkJsBean.getGuid();
                        shortUrl = "/view/" + guid;  // temporary 
                //   	}
                //}
                window.location = shortUrl; 
            } else {
                // ??? error ???
            }
        }
        return false;
    });
    $("#topmenu_nav_verify").click(function() {
        if(DEBUG_ENABLED) console.log("topmenu_nav_verify anchor clicked."); 
        if(editState == 'state_new') {
            window.location = "/verify";
        } else {
            if(shortLinkJsBean) {
                //var redirectUrlHelper = new cannyurl.redirecturl.RedirectUrlHelper(shortLinkJsBean);
                //if(redirectUrlHelper.isReady()) {
                //    var verifyUrl = redirectUrlHelper.verifyUrl();
                //    window.location = verifyUrl;
                //} else {
                    var guid = shortLinkJsBean.getGuid();
                    window.location = "/verify/" + guid;	
                //}
            } else {
                // ??? error ???
            }
        }
        return false;
    });
<%
if(isAppAuthDisabled == true || isAppAuthOptional == true || isUserAuthenticated == true) {
%>
    $("#topmenu_nav_info").click(function() {
        if(DEBUG_ENABLED) console.log("topmenu_nav_info anchor clicked."); 
        if(editState == 'state_new') {
            window.location = "/info";
            // ???
        } else {
            if(shortLinkJsBean) {
                //var redirectUrlHelper = new cannyurl.redirecturl.RedirectUrlHelper(shortLinkJsBean);
                //if(redirectUrlHelper.isReady()) {
                //    var infoUrl = redirectUrlHelper.infoUrl();
                //    window.location = infoUrl;
                //} else {
                    var guid = shortLinkJsBean.getGuid();
                    window.location = "/info/" + guid;	
                //}
            } else {
                // ??? error ???
            }
        }
        return false;
    });
    //$("#topmenu_nav_list").click(function() {
    //    if(DEBUG_ENABLED) console.log("topmenu_nav_list anchor clicked."); 
    //    window.location = "/list";
    //    return false;
    //});
<%
}
%>
});
</script>



<%
String googleAnalyticsCode = AnalyticsHelper.getInstance().getGoogleAnalyticsCode();
if(googleAnalyticsCode != null && !googleAnalyticsCode.isEmpty()) {    // And, if it is enabled (e.g. through config). TBD...
%>
    <!-- Change UA-XXXXX-X to be your site's ID -->
    <script>
      window._gaq = [['_setAccount','<%=googleAnalyticsCode%>'],['_trackPageview'],['_trackPageLoadTime']];
      Modernizr.load({
        load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
      });
    </script>
<%
}
%>

    <!-- Prompt IE 6 users to install Chrome Frame. Remove this if you want to support IE 6.
       chromium.org/developers/how-tos/chrome-frame-getting-started -->
    <!--[if lt IE 7 ]>
      <script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
      <script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
    <![endif]-->

  </body>
</html>