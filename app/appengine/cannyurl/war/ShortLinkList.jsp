<%@ page import="com.myurldb.ws.core.*, com.myurldb.ws.*, com.cannyurl.af.util.*, com.cannyurl.af.auth.common.*, com.cannyurl.af.auth.*, com.cannyurl.af.auth.user.*, com.cannyurl.fe.*, com.cannyurl.fe.core.*, com.cannyurl.fe.bean.*, com.cannyurl.fe.util.*, com.cannyurl.wa.service.*, com.cannyurl.common.*, com.cannyurl.util.*, com.cannyurl.app.auth.*, com.cannyurl.app.util.*, com.cannyurl.helper.*" 
%><%@ page contentType="text/html; charset=UTF-8" 
%><%@ page errorPage="/error/UnknownError" 
%><%!
private static final java.util.logging.Logger logger = java.util.logging.Logger.getLogger("ShortLinkList");
%><%
boolean isAppAuthDisabled = ConfigUtil.isApplicationAuthDisabled();
// boolean isAppAuthOptional = ConfigUtil.isApplicationAuthOptional();
String appAuthMode = ConfigUtil.getApplicationAuthMode();
boolean isUsingTwitterAuth = false;
if(AppAuthMode.MODE_TWITTER.equals(appAuthMode)) {
    isUsingTwitterAuth = true;
}
%><%
// {1] Session
SessionBean sessionBean = UserSessionManager.getInstance().setSessionBean(request, response);
String sessionToken = sessionBean.getToken();
String userId = sessionBean.getUserId();

// [2] Parse url.
String requestURI = request.getRequestURI();
String requestUrl = request.getRequestURL().toString();
String topLevelUrl = UrlHelper.getInstance().getTopLevelURLFromRequestURL(requestUrl);
String contextPath = request.getContextPath();
String servletPath = request.getServletPath();
String pathInfo = request.getPathInfo();
String queryString = request.getQueryString();
java.util.Map<String,Object> queryParams = URLUtil.parseParamMap(request.getParameterMap());
//...
Long paramOffset = QueryParamUtil.getParamOffset(request);
if(paramOffset == null) {
    paramOffset = 0L;
}
Integer paramCount = QueryParamUtil.getParamCount(request);
if(paramCount == null || paramCount <= 0) {
    paramCount = PagerHelper.getInstance().getShortUrlListPageSize();
}
//Integer paramPageSise = QueryParamUtil.getParamPageSize(request); // vs. paramCount???
String paramOrdering = QueryParamUtil.getParamOrdering(request);
if(paramOrdering == null || paramOrdering.isEmpty()) {
	paramOrdering = PagerHelper.getInstance().getShortUrlListOrderingPrimary();
}
// ...
%><%
// Sort order
java.util.Map<String, Integer> columnOrderMap = null;
TableSortedStateBean tableSortedStateBean = TableSortOrderHelper.getInstance().getTableSortedState(request);
if(tableSortedStateBean != null) {
    columnOrderMap = tableSortedStateBean.getColumnOrderMap(requestUrl);
    // columnOrderMap cannot be null...
}
java.util.Map<String, Integer> paramOrderPair = TableSortOrderHelper.parseParamOrdering(paramOrdering);
if(paramOrderPair != null && !paramOrderPair.isEmpty()) {
	// paramOrderPair can have no more than one element, in the current implementation.
    for(String c : paramOrderPair.keySet()) {
    	Integer o = paramOrderPair.get(c);
        columnOrderMap.put(c, o);
    }
}
// ...
%><%
// "Branding"
String appBrand = BrandingHelper.getInstance().getAppBrand();
String brandDisplayName = BrandingHelper.getInstance().getBrandDisplayName();
String brandDescription = BrandingHelper.getInstance().getBrandDescription();
%><%
String themeBootstrap = SiteThemeHelper.getInstance().getBootstrapTheme();
String themeJqueryUi = SiteThemeHelper.getInstance().getJqueryUiTheme();
%><%
// Using AuthLoginFilter... User is already logged on if this page is displayed....
boolean isUserAuthenticated = true;
// boolean isUserAuthenticated = false;
// RequestAuthStateBean authStateBean = (RequestAuthStateBean) request.getAttribute(AuthUtil.REQUEST_ATTR_AUTHSTATEBEAN);
// if(authStateBean != null) {
//     isUserAuthenticated = authStateBean.isAuthenticated();
// }
%><%
// [4] Initial validation
// URL patterns:
// "/list"
// "/list/<user-guid>"
// "/list/@<user-twitter-handle>"
// "/list/all"
// "/list/client/<app_client>"
// "/list/appsdomain/<domain>"
// ....
// TBD: 
// What about "http://username.smb.gs/list" ?????
// ....
String targetUser = null;
// String targetAll = null;
String targetClient = null;
String targetAppsDomain = null;
String targetScope = UrlHelper.getInstance().getGuidFromPathInfo(pathInfo);
// TBD: (1) check first if guid is a valid GUID.... ???
// ????
        

// TBD:
// (2) AppClient/clientRootDomain ???
if(targetScope != null) {
    if(targetScope.equals("all")) {
        // TBD:
        // List for all
    } else {
        int idxX = targetScope.indexOf("/");
        if(idxX > 0) {
            String prfx = targetScope.substring(0, idxX);
            targetScope = targetScope.substring(idxX + 1);
            if(prfx.equals("client")) {
                targetClient = targetScope; 
            } else if(prfx.equals("appsdomain")) {
                targetAppsDomain = targetScope;
            } else {
                // ????
            }
        } else {
            targetUser = targetScope;
        }
    }
}
// ....


// (3)
if(targetUser != null && !targetUser.isEmpty()) {   // TBD: validation...
    // TBD: Check if guid == userId ????
    //....
    if(targetUser.startsWith("@")
    		|| targetUser.startsWith("~")
    		|| targetUser.startsWith("+")
    		|| targetUser.startsWith("-")
    		|| targetUser.startsWith("!")
            ) {
        // Twitter handle or other username/usercode...
        // TBD: (1) How do you know if it's username or usercode???
        //      (2) What about the case ConfigUtil.isUseDomainUserURL() == true ????
        String username = targetUser.substring(1);
        UserJsBean userBean = UserHelper.getInstance().findUserByUsernameOrUsercode(username);
        if(userBean != null) {
            targetUser = userBean.getGuid();
        } else {
            // ????
            // What to do????
            // targetUser = null; // If we do this, we'll end up doing targetUser <- userId, which may not be the one the user wanted...
            // ....
        }
    } else {
        // Note the order of checking each option...
        // [1] Is this a guid?
        // this is true if the guid/target is in the standard format. but, we do allow guid in an arbitrary format....
        // Most cases, this should be true though...
		if(GUID.isValid(targetUser)) { 
		    // Just use the targetUser param...
		} else {
		    // [2] Is this a username/usercode ???
		    boolean usernameFound = false;
	        if(ConfigUtil.isUseDomainUserURL() == true) {
	            UserJsBean userBean = UserHelper.getInstance().findUserByUsernameOrUsercode(targetUser);
	            if(userBean != null) {
	                targetUser = userBean.getGuid();
	                usernameFound = true;
	            } else {
	                // ????
	                // Is targetUser User.guid???
	                // or, has error happened???
	                // What to do????
	                // See below the actual implementation
	                // we'll just end up getting an empty list (since targetUser is invalid)
	                // ....
	            }
	        }
	        // [3] ????
	        if(usernameFound == false) {
			    // Just use the targetUser param...	            
	        }        
		}        
    }    
} else {

	// TBD:
	// targetClient
	// targetAppsDomain
    // ????
    if(targetClient != null && !targetClient.isEmpty()) {
        
    } else if(targetAppsDomain != null && !targetAppsDomain.isEmpty()) {
        
    }

}

if( (targetUser == null || targetUser.isEmpty())
        && (targetClient == null || targetClient.isEmpty())   
        && (targetAppsDomain == null || targetAppsDomain.isEmpty())   
        ) {
    // error??
    // bail out... ???
    // Or, just use the current userId ???
    targetUser = userId;  // ???
    //...
}

%><%
// ?????
boolean isShowingInfoLink = ConfigUtil.isApplicationIngressDBEnabled() && ConfigUtil.isApplicationUrlTallyEnabled();
// .....
%><%
//...
%><!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!-- Consider adding an manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<html>
  <head>
    <meta charset="utf-8">
    <title><%=brandDisplayName%> | Shortlink List</title>
    <meta name="author" content="URL Foundry">
    <meta name="description" content="<%=AppBrand.getBrandDescription(appBrand)%>. It also provides free URL Shortener Web Services API. You can use <%=brandDisplayName%> shortener service to create a short URL, a safe URL, a canny URL, and a flash URL. But, <%=brandDisplayName%> is so much more than a URL shortener." />

    <!-- Mobile viewport optimized: j.mp/bplateviewport -->
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <link rel="shortcut icon" href="/img/favicon-<%=appBrand%>.ico" />
    <link rel="icon" href="/img/favicon-<%=appBrand%>.ico" type="image/x-icon" />

    <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- CSS: implied media=all -->
    <%@ include file="/fragment/style.jspf" %>
    <!-- end CSS-->

    <!-- All JavaScript at the bottom, except for Modernizr / Respond.
       Modernizr enables HTML5 elements & feature detects; Respond is a polyfill for min/max-width CSS3 Media Queries
       For optimal performance, use a custom Modernizr build: www.modernizr.com/download/ -->
    <script src="http://ww2.filestoa.com/js/modernizr/modernizr-2.0.6.min.js"></script>

  </head>

  <body>

<jsp:include page="/fragment/TopNavbar">
<jsp:param name="pageCode" value="shortlink/list"/>
<jsp:param name="brandDisplayName" value="<%=brandDisplayName%>"/>
</jsp:include>


    <div class="container-fluid">

      <!-- Main hero unit for a primary marketing message or call to action -->
      <div id="main" class="hero-unit">
      <form id="form_shorturl_view" method="POST" action="">

        <div id="shorturl_header">
        <fieldset name="fieldset_shorturl_header">
        <table style="background-color: transparent;">
        <tr>
        <td style="vertical-align:bottom;">

        <div id="cannyurl_logo">
	 	    <h1><%=brandDisplayName%><sup>&beta;</sup></h1>
        </div>

        </td>
        </tr>
        </table>
      </fieldset>
      </div>
      

      <div id="shorturl_body">

      <div id="shortlink_list_main" class="shortlink_list_main">
      <fieldset name="fieldset_shortlink_list_main">



<%
PagerStateStructJsBean pagerState = null;
java.util.List<ShortLinkJsBean> shortLinks = null;
if(isAppAuthDisabled == true || isUserAuthenticated == true) {    // TBD: Remove this???

if(targetUser != null) {
    shortLinks = ShortUrlListHelper.getInstance().findShortLinksForUser(targetUser, paramOffset, paramCount);
} else {
    if(targetClient != null) {
        ShortUrlListHelper.getInstance().findShortLinksForAppClient(targetClient, paramOffset, paramCount);
    } else {
        if(targetAppsDomain != null) {
            ShortUrlListHelper.getInstance().findShortLinksForClientRootDomain(targetAppsDomain, paramOffset, paramCount);
        } else {
		    shortLinks = ShortUrlListHelper.getInstance().findShortLinksForAll(paramOffset, paramCount);
        }
    }
}
if(shortLinks != null && !shortLinks.isEmpty()) {
    int aSize = 0;
    if(targetUser != null) {
        aSize = ShortUrlListHelper.getInstance().getTotalCountForUser(targetUser, paramOffset, paramCount + paramCount*4 + 1);  // Look ahead 5 pages
    } else {
        if(targetClient != null) {
            aSize = ShortUrlListHelper.getInstance().getTotalCountForAppClient(targetClient, paramOffset, paramCount + paramCount*4 + 1);  // Look ahead 5 pages
        } else {
            if(targetAppsDomain != null) {
                aSize = ShortUrlListHelper.getInstance().getTotalCountForClientRootDomain(targetAppsDomain, paramOffset, paramCount + paramCount*4 + 1);  // Look ahead 5 pages
            } else {
	            aSize = ShortUrlListHelper.getInstance().getTotalCountForAll(paramOffset, paramCount + paramCount*4 + 1);  // Look ahead 5 pages
            }
        }
    }
    Long totalSize = null;
    if(aSize < paramCount*4 + 1) {
        totalSize = paramOffset + aSize;
    }
    pagerState = PagerHelper.getInstance().constructPagerStateFromOffset(paramOffset, paramCount, totalSize, paramOffset + aSize);
%>
<table id="table_shortlink_list_records" class="table table-striped" style="background-color: transparent;">
<thead>
<tr><th>Short URL</th><th>Long URL</th><th>Created Time</th><th>&nbsp;</th></tr>
</thead>
<tbody>
<%	
    for(ShortLinkJsBean m : shortLinks) {
        String jGuid = m.getGuid();
        String jUser = m.getOwner();
        String jToken = m.getToken();
        String jDomain = m.getDomain();
        String jShortUrl = m.getShortUrl();
        String jEscapedShortUrl = "";
        if(jShortUrl == null) {
            jShortUrl = ""; // ????
        } else {
            jEscapedShortUrl = HtmlTextUtil.escapeForHtml(jShortUrl); 
        }
        String jLongUrl = m.getLongUrl();
        String jEscapedLongUrl = "";
        if(jLongUrl != null) {
            jEscapedLongUrl = HtmlTextUtil.escapeForHtml(jLongUrl); 
            // Truncate, if too long
            int lulen = jEscapedLongUrl.length();
            if(lulen > 80) {  // Arbitrary.... TBD: Use a class constant from a util class, etc...
            	jEscapedLongUrl = jEscapedLongUrl.substring(0, 80 - 3) + "...";
            }
        }
        String jStatus = m.getStatus();
        if(jStatus == null) {
        	jStatus = "";  // ???
        }
        Long jCreatedT = m.getCreatedTime();
        Long jModifiedT = m.getModifiedTime();
        //String jNote = j.getNote();
        //String jEscapedNote = HtmlTextUtil.escapeForHtml(jNote);  // ????
        String jCreatedD = "";
        if(jCreatedT != null && jCreatedT > 0L) {
            jCreatedD = DateUtil.formatDateNoSeconds(jCreatedT);  // UTC. Will be overwritten through Javascript code below...
        }
        String jModifiedD = "";
        if(jModifiedT != null && jModifiedT > 0L) {
            jModifiedD = DateUtil.formatDateNoSeconds(jModifiedT);   // UTC. Will be overwritten through Javascript code below...
        }
%>
<tr id="row_<%=jGuid%>">
<td class="td_shortlink_list_shorturl">
<%
boolean isValidShortUrl = URLUtil.isValidUrl(jShortUrl);  
if(isValidShortUrl) {
%>
<span id="shorturl_<%=jGuid%>"><a href="<%=jShortUrl%>"><%=jEscapedShortUrl%></a></span>
<%
} else {
%>
<span id="shorturl_<%=jGuid%>"><%=jEscapedShortUrl%></span>    
<%
}
%>
</td>
<td class="td_shortlink_list_longurl">
<%
boolean isValidLongUrl = URLUtil.isValidUrl(jLongUrl);  
if(isValidLongUrl) {
%>
<span id="longurl_<%=jGuid%>"><a href="<%=jLongUrl%>"><%=jEscapedLongUrl%></a></span>
<%
} else {
%>
<span id="longurl_<%=jGuid%>"><%=jEscapedLongUrl%></span>    
<%
}
%>
</td>
<td class="td_shortlink_list_createdtime"><span id="createdtime_<%=jGuid%>"><%=jCreatedD%></span></td>
<td class="td_shortlink_list_button">
<% if(isShowingInfoLink == true) { %>
<button type="button" class="btn button_shortlink_list_info" id="button_info_<%=jGuid%>" title="View the short URL information" style="width:100px">Info</button>
<% } else { %>
&nbsp;
<% } %>
</td>
</tr>
<%
    }
} else {
%>
<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
<%
}
%>
</tbody>
</table>
<%
} else {
%>
<p>
<span class="label label-warning">No Access</span> &nbsp; 
You need to log on to see your short URL list.
</p>
<%
}
%>



      </fieldset>
      </div>

      </div>


<%
if(pagerState != null) {
%>
<div id="shorturl_list_pager">
<ul class="pager">
  <li id="shorturl_listpager_first" class="disabled">
    <a id="shorturl_listpager_anchor_first" title="Go to the first page" href="#">First</a>
  </li>
  <li id="shorturl_listpager_previous" class="active">
    <a id="shorturl_listpager_anchor_previous" title="Go to the previous page" href="#">&larr; Previous</a>
  </li>
  <li id="shorturl_listpager_next">
    <a id="shorturl_listpager_anchor_next" title="Go to the next page" href="#">Next &rarr;</a>
  </li>
  <li id="shorturl_listpager_last">
    <a id="shorturl_listpager_anchor_last" title="Go to the last page" href="#">Last</a>
  </li>
</ul>
</div>
<%
}
%>

      </form>
      </div>  <!--   Hero unit  -->


<!-- Page Footer -->    
<%@ include file="/fragment/Footer.jspf" %>
<!-- Page Footer -->    

    </div> <!--! end of #container -->



    <div id="div_status_floater" style="display: none;" class="alert">
    </div>
 

    <!-- JavaScript at the bottom for fast page loading -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="http://ww2.filestoa.com/js/jquery/jquery-1.8.3.min.js"><\/script>')</script>
	<script src="http://ww2.filestoa.com/js/jquery/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="http://ww2.filestoa.com/js/jquery/plugin/jquery.floatobject-1.4.js"></script>
    <script src="http://ww2.filestoa.com/js/jquery/plugin/jquery-ui-timepicker-addon.js"></script>
    <script src="http://ww2.filestoa.com/js/jquery/plugin/history.js/uncompressed/history.adapter.jquery.js"></script>
    <script src="http://ww2.filestoa.com/js/jquery/plugin/history.js/uncompressed/history.js"></script>
    <script src="http://ww2.filestoa.com/js/jquery/plugin/history.js/uncompressed/history.html4.js"></script>
    <script src="http://ww2.filestoa.com/js/bootstrap/bootstrap-2.0.js"></script>
    <script type="text/javascript" src="/js/core/debugutil.js"></script>
    <script type="text/javascript" src="http://ww2.filestoa.com/js/core/datetimeutil-1.0.js"></script>
    <script type="text/javascript" src="http://ww2.filestoa.com/js/core/emailutil-1.0.js"></script>
    <script type="text/javascript" src="/js/bean/shortlinkjsbean-1.0.js"></script>
    <script type="text/javascript" src="/js/bean/pagerstatestructjsbean-1.0.js"></script>
    <script type="text/javascript" src="/js/mylibs/mytimer.js"></script>
    <script defer type="text/javascript" src="/js/mylibs/redirecturl-1.0.js"></script>
	<script defer type="text/javascript" src="/js/plugins.js"></script>
    <script defer type="text/javascript" src="/js/script.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/fiveten-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/statushelper-1.1.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/tweethelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/signuphelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/feedbackhelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/emailhelper-1.0.js"></script>
    <!-- end scripts-->


    <script>
    // Global vars
    var feedbackHelper;
    $(function() {
    	var targetService = "<%=brandDisplayName%>";
    	var targetPage = "list";
    	feedbackHelper = new webstoa.FeedbackHelper(targetService, targetPage);
    });
    </script>

    <script>
    // Email address "decoding"...
    $(function() {
        $("a").each(function() {
        	var anchor = $(this);
        	webstoa.EmailUtil.decodeMailtoAnchor(anchor);
        });
    });
    </script>

    <script>
    $(function() {
        $("#anchor_feedback_footer").click(function() {
            if(DEBUG_ENABLED) console.log("Feedback button clicked."); 
            var targetItem = 'footer';
            feedbackHelper.feedback(targetItem);
		    return false;
        });
    });
    </script>


    <script>
    // Function definitions.
    function updateStatus(msg, type, time) {
    	if(!time) {
    		time = 5000;   // milliseconds.
    	}
    	if(!type) {
    		// type = "info";  // warning, error, etc....
    	}
    	//$("#div_status_floater").text(msg);
    	//$("#div_status_floater").fadeIn().delay(time).fadeOut();
		$("#div_status_floater").textInOut(msg, type, time);
    }    
    </script>

    <script>
    // temporary
    function replaceURLWithHTMLLinks(text) {
        if(text) {
            var exp = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
            //var exp = /\(?\bhttp:\/\/[-A-Za-z0-9+&@#\/%?=~_()|!:,.;]*[-A-Za-z0-9+&@#/%=~_()|]/ig;
            return text.replace(exp,"<a rel='nofollow' href='$1'>$1</a>"); 
        }
        return '';    // ???
    }
    </script>


    <script>
    // "Init"
    $(function() {
    	// TBD: initially hide $("#div_status_floater") ...
    	$("#div_status_floater").text("").hide();
    
        // Background image test.
        //$("#container").css("background-image", 'url(/img/bg/oceansunset.jpg)');
        //$(document.body).css("background", "silver");
    	//$("#container").css("background", "ivory");

        // TBD: Location...
    	var params = {x: 585, y: 70, speed: 'normal' };
    	$("#div_status_floater").makeFloat(params);
    	
    	// temporary
    	// linkify the content
    	var contentOriginal = $("#shorturl_view_body_main_short_message").html();
    	var contentLinkified = replaceURLWithHTMLLinks(contentOriginal);
    	$("#shorturl_view_body_main_short_message").html(contentLinkified);
    	// temporary

    	// Peeng?
    	//var fiveTenTimer = new fiveten.FiveTenTimer(14839, 2);  // ???
    	//fiveTenTimer.start();
    });
    </script>


    <script>
$(function() {
    // Initialize all datetime strings.
var createdTimeId;
var createdTimeStr;  

<%
//java.util.List<ShortLinkJsBean> shortLinks = ShortUrlListHelper.getInstance().findShortLinksForUser(targetUser, paramOffset, paramCount);
if(shortLinks != null && shortLinks.size() > 0) {
    for(ShortLinkJsBean m : shortLinks) {
        String jGuid = m.getGuid();
        Long jCreatedT = m.getCreatedTime();
%>
createdTimeId = "createdtime_<%=jGuid%>";
createdTimeStr = getStringFromTime(<%=jCreatedT%>);  
$("#" + createdTimeId).text(createdTimeStr);
<%
    }
}
%>
});    
    </script>


    <script>
    // App-related vars.
    var userGuid;
    var shortLinkJsBean;
    //var accessRecordJsBean;
<%
if(userId != null) {
%>
    userGuid = "<%=userId%>";
<%
}
%>
    </script>


    <script>
    // Event handlers for the list...
    $(function() {
<% if(isShowingInfoLink == true) { %>
        $("button.button_shortlink_list_info").live('click', function() {
            if(DEBUG_ENABLED) console.log("Shortlink info button clicked.");
            //window.alert('Not implemented yet.');
            //window.alert('target: ' + event.target.id);

            //var guid =  event.target.id.substring(12);  // Note: this does not work on IE....
            var guid =  $(this).attr('id').substring(12);  // prefix: "button_info_"
            //window.alert('guid: ' + guid);
            var infoPageUrl = "/info/" + guid;

            // TBD:
            // Use domain + "i" + token ????

            window.location = infoPageUrl; 
        });
<% } %>
    });
    </script>


    <script>
    $(function() {

    });
    </script>



    <script>
    // Ajax form handlers.
  $(function() {
	  // ...
  });
    </script>


<script>
$(function() {
	// Top menu handlers
	// Note: /view and /info reuire guid....
	// TBD: Due to the way we use subdomains...
	//      Every time we change url (subdomain), the login state changes...
	//      We use 27 subdomains, therfore this is not not very usable...
	// For now, all urls should point to relative URLs "/view", "/verify", etc...
	//      rather than path-based URLs, e.g., a.fm.gs/i/abcd, ....
//    $("#topmenu_nav_shorten").click(function() {
//        if(DEBUG_ENABLED) console.log("topmenu_nav_shorten anchor clicked."); 
//        window.location = "/shorten";
//        return false;
//    });
    $("#topmenu_nav_view").click(function() {
        if(DEBUG_ENABLED) console.log("topmenu_nav_view anchor clicked."); 
        $("#topmenu_nav_view").fadeOut(450).fadeIn(950);
        return false;
    });
//    $("#topmenu_nav_verify").click(function() {
//        if(DEBUG_ENABLED) console.log("topmenu_nav_verify anchor clicked."); 
//        window.location = "/verify";
//        return false;
//    });
<%
if(isAppAuthDisabled == true || isUserAuthenticated == true) {
%>
//    $("#topmenu_nav_info").click(function() {
//        if(DEBUG_ENABLED) console.log("topmenu_nav_info anchor clicked."); 
//        window.location = "/info";
//        return false;
//    });
//    $("#topmenu_nav_list").click(function() {
//        if(DEBUG_ENABLED) console.log("topmenu_nav_list anchor clicked."); 
//        //window.location = "/list";
//        return false;
//    });
<%
}
%>
});
</script>





<%
if(pagerState != null) {
%>
    <script>
    // Pager utilitiees...
    $(function() {
    	// Sorting support....

<%
if(paramOrderPair != null && !paramOrderPair.isEmpty()) {
	// paramOrderPair can have no more than one element, in the current implementation.
    for(String c : paramOrderPair.keySet()) {
    	Integer o = paramOrderPair.get(c);
    	String markerHtml = null;
    	if(o == null || o == 1) {
    		markerHtml = "<i class=\"icon-chevron-down\"></i>";
    	} else {
    		markerHtml = "<i class=\"icon-chevron-up\"></i>";    		
    	}
%>
$("#table_summary_list_marker_" + "<%=c%>").html('<%=markerHtml%>');
<%
    }
}
%>




        
    });
    </script>
<%
}
%>



<%
if(pagerState != null) {
%>
    <script>
    // Pager utiliitiees...
    $(function() {
            // Button visual state...
<%
    if(pagerState.isFirstActionEnabled()) {
%>
    		$("#shorturl_listpager_first").addClass("active");
    		$("#shorturl_listpager_first").removeClass("disabled");
    		$("#shorturl_listpager_anchor_first").attr("title","Go to the first page");
<%
    } else {
%>
    		$("#shorturl_listpager_first").addClass("disabled");
    		$("#shorturl_listpager_first").removeClass("active");
    		$("#shorturl_listpager_anchor_first").attr("title","");
    		$("#shorturl_listpager_anchor_first").css("color","gray");
<%
    }
%>
<%
    if(pagerState.isPreviousActionEnabled()) {
%>
            $("#shorturl_listpager_previous").addClass("active");
            $("#shorturl_listpager_previous").removeClass("disabled");
    		$("#shorturl_listpager_anchor_previous").attr("title","Go to the previous page");
<%
    } else {
%>
            $("#shorturl_listpager_previous").addClass("disabled");
            $("#shorturl_listpager_previous").removeClass("active");
    		$("#shorturl_listpager_anchor_previous").attr("title","");
    		$("#shorturl_listpager_anchor_previous").css("color","gray");
<%
    }
%>
<%
    if(pagerState.isNextActionEnabled()) {
%>
    		$("#shorturl_listpager_next").addClass("active");
    		$("#shorturl_listpager_next").removeClass("disabled");
    		$("#shorturl_listpager_anchor_next").attr("title","Go to the next page");
<%
    } else {
%>
    		$("#shorturl_listpager_next").addClass("disabled");
    		$("#shorturl_listpager_next").removeClass("active");
    		$("#shorturl_listpager_anchor_next").attr("title","");
    		$("#shorturl_listpager_anchor_next").css("color","gray");
<%
    }
%>
<%
    if(pagerState.isLastActionEnabled()) {
%>
            $("#shorturl_listpager_last").addClass("active");
            $("#shorturl_listpager_last").removeClass("disabled");
    		$("#shorturl_listpager_anchor_last").attr("title","Go to the last page");
<%
    } else {
%>
            $("#shorturl_listpager_last").addClass("disabled");
            $("#shorturl_listpager_last").removeClass("active");
    		$("#shorturl_listpager_anchor_last").attr("title","");
    		$("#shorturl_listpager_anchor_last").css("color","gray");
<%
    }
%>

        // Event handlers..
        // var pageUrl = "<%=requestURI%>";
        $("#shorturl_listpager_anchor_first").click(function(e) {
            if(DEBUG_ENABLED) console.log("First anchor clicked."); 
<%
    if(pagerState.isFirstActionEnabled()) {
       String firstPageUrl = URLUtil.buildUrl(requestURI, URLUtil.replaceQueryParam(queryParams, QueryParamUtil.PARAM_OFFSET, 0L));
%>
                var href = "<%=firstPageUrl%>";
                window.location = href;
<%
    } else {
%>
        		e.preventDefault();
                $("#shorturl_listpager_first").fadeOut(250).fadeIn(750);
<%
    }
%>
		    return false;
        });
        $("#shorturl_listpager_anchor_previous").click(function(e) {
            if(DEBUG_ENABLED) console.log("Previous anchor clicked."); 
<%
    if(pagerState.isPreviousActionEnabled()) {
       String previousPageUrl = URLUtil.buildUrl(requestURI, URLUtil.replaceQueryParam(queryParams, QueryParamUtil.PARAM_OFFSET, pagerState.getPreviousPageOffset()));
%>
                var href = "<%=previousPageUrl%>";
                window.location = href;
<%
    } else {
%>
        		e.preventDefault();
                $("#shorturl_listpager_previous").fadeOut(250).fadeIn(750);
<%
    }
%>
		    return false;
        });
        $("#shorturl_listpager_anchor_next").click(function(e) {
            if(DEBUG_ENABLED) console.log("Next anchor clicked."); 
<%
    if(pagerState.isNextActionEnabled()) {
       String nextPageUrl = URLUtil.buildUrl(requestURI, URLUtil.replaceQueryParam(queryParams, QueryParamUtil.PARAM_OFFSET, pagerState.getNextPageOffset()));
%>
                var href = "<%=nextPageUrl%>";
                window.location = href;
<%
    } else {
%>
        		e.preventDefault();
                $("#shorturl_listpager_next").fadeOut(250).fadeIn(750);
<%
    }
%>
		    return false;
        });
        $("#shorturl_listpager_anchor_last").click(function(e) {
            if(DEBUG_ENABLED) console.log("Last anchor clicked."); 
<%
    if(pagerState.isLastActionEnabled()) {
       String lastPageUrl = URLUtil.buildUrl(requestURI, URLUtil.replaceQueryParam(queryParams, QueryParamUtil.PARAM_OFFSET, pagerState.getLastPageOffset()));
%>
                var href = "<%=lastPageUrl%>";
                window.location = href;
<%
    } else {
%>
        		e.preventDefault();
                $("#shorturl_listpager_last").fadeOut(250).fadeIn(750);
<%
    }
%>
		    return false;
        });        
    });
    </script>
<%
}
%>  



<%
String googleAnalyticsCode = AnalyticsHelper.getInstance().getGoogleAnalyticsCode();
if(googleAnalyticsCode != null && !googleAnalyticsCode.isEmpty()) {    // And, if it is enabled (e.g. through config). TBD...
%>
    <!-- Change UA-XXXXX-X to be your site's ID -->
    <script>
      window._gaq = [['_setAccount','<%=googleAnalyticsCode%>'],['_trackPageview'],['_trackPageLoadTime']];
      Modernizr.load({
        load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
      });
    </script>
<%
}
%>

    <!-- Prompt IE 6 users to install Chrome Frame. Remove this if you want to support IE 6.
       chromium.org/developers/how-tos/chrome-frame-getting-started -->
    <!--[if lt IE 7 ]>
      <script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
      <script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
    <![endif]-->

    </body>
</html>