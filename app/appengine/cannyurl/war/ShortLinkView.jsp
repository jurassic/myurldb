<%@ page import="com.myurldb.ws.core.*, com.myurldb.ws.*, com.cannyurl.af.util.*, com.cannyurl.af.auth.common.*, com.cannyurl.af.auth.*, com.cannyurl.af.auth.user.*, com.cannyurl.fe.*, com.cannyurl.fe.core.*, com.cannyurl.fe.bean.*, com.cannyurl.fe.util.*, com.cannyurl.wa.service.*, com.cannyurl.common.*, com.cannyurl.util.*, com.cannyurl.app.auth.*, com.cannyurl.app.auth.filter.*, com.cannyurl.app.util.*, com.cannyurl.helper.*" 
%><%@ page contentType="text/html; charset=UTF-8" 
%><%@ page errorPage="/error/UnknownError" 
%><%!
private static final java.util.logging.Logger logger = java.util.logging.Logger.getLogger("ShortLinkView");
%><%
boolean isAppAuthDisabled = ConfigUtil.isApplicationAuthDisabled();
boolean isAppAuthOptional = ConfigUtil.isApplicationAuthOptional();
String appAuthMode = ConfigUtil.getApplicationAuthMode();
boolean isUsingTwitterAuth = false;
if(AppAuthMode.MODE_TWITTER.equals(appAuthMode)) {
    isUsingTwitterAuth = true;
}
%><%// {1] Session
SessionBean sessionBean = UserSessionManager.getInstance().setSessionBean(request, response);
String sessionToken = sessionBean.getToken();
String userId = sessionBean.getUserId();

// [2] Parse url.
String contextPath = request.getContextPath();
String servletPath = request.getServletPath();
String requestHost = request.getServerName();
//String requestURI = request.getRequestURI();
String requestUrl = request.getRequestURL().toString();
String topLevelUrl = UrlHelper.getInstance().getTopLevelURLFromRequestURL(requestUrl);
String pathInfo = request.getPathInfo();
String queryString = request.getQueryString();%>

<%
    // TBD: Need to map "/" to the home page. but, access page currently mapped to "/*".
//  Use static welcome-page instead as a home page maybe?
if(servletPath == null || servletPath.isEmpty() || servletPath.equals("/")) {
    if(pathInfo == null || pathInfo.isEmpty() || pathInfo.equals("/")) {
%>
<jsp:forward page="/home" />
<%
    }
}
%><%
// "Branding"
String appBrand = BrandingHelper.getInstance().getAppBrand();
String brandDisplayName = BrandingHelper.getInstance().getBrandDisplayName();
String brandDescription = BrandingHelper.getInstance().getBrandDescription();
%><%
    String themeBootstrap = SiteThemeHelper.getInstance().getBootstrapTheme();
String themeJqueryUi = SiteThemeHelper.getInstance().getJqueryUiTheme();
%><%
boolean isUserAuthenticated = false;
String userUsername = null;
String loginUrl = null;
String logoutUrl = null;
RequestAuthStateBean authStateBean = (RequestAuthStateBean) request.getAttribute(AuthUtil.REQUEST_ATTR_AUTHSTATEBEAN);
if(authStateBean == null) {
    // Filter might not have applied (because of the /* mapping, etc...) 
    // Try one more time ???
    authStateBean = AuthFilterUtil.createAuthStateBean(request, response);
    if(authStateBean != null) {
        // Add it to the current request object....
        request.setAttribute(AuthUtil.REQUEST_ATTR_AUTHSTATEBEAN, authStateBean);
    }
}
if(authStateBean != null) {
    isUserAuthenticated = authStateBean.isAuthenticated();
    if(isUserAuthenticated) {
        logoutUrl = authStateBean.getLogoutUrl();
        userUsername = authStateBean.getUsername();
    } else {
        loginUrl = authStateBean.getLoginUrl();
    }   
}
%><%
String twitterHandle = "";
String twitterUserProfileUrl = "";
if(isUsingTwitterAuth) {
	if(userUsername != null && !userUsername.isEmpty()) {
	    twitterHandle = "@" + userUsername;      // ???
	    twitterUserProfileUrl = "http://twitter.com/" + userUsername;
	}
}
%><%
if(isUserAuthenticated == false) {
    // ????
    // response.sendRedirect(loginUrl);
}
%><%
// [3] Local "global" variables.
ShortLinkJsBean shortLinkBean = null;
String shortUrlShortUrl = null;  // != requestUrl, in general. (requestUrl may include the "path" compoennt)...
String shortUrlRedirectType = null;
String requestUrlRedirectType = null;
Long requestUrlFlashDuration = null;


// [4] Parse the reuqest URL.
// Find shortLinkBean and requestUrlRedirectType, if any.
boolean requiresRefresh = false;   // Redirect to the "canonical" url, if necessary...
if(servletPath != null && servletPath.length() > 2) {   // ???

    // parse the URL sevletpath.
    if(servletPath.equals("/confirm")) {
        requestUrlRedirectType = RedirectType.TYPE_CONFIRM;
    } else if(servletPath.equals("/flash")) {
        requestUrlRedirectType = RedirectType.TYPE_FLASH;
    } else if(servletPath.equals("/permanent")) {
        requestUrlRedirectType = RedirectType.TYPE_PERMANENT;
    } else if(servletPath.equals("/temporary")) {
        requestUrlRedirectType = RedirectType.TYPE_TEMPORARY;
    } else {
        // ???	 
        // error. bail out. ???
        // ...
    }

    String guid = UrlHelper.getInstance().getGuidFromPathInfo(pathInfo);
	if(guid != null && !guid.isEmpty()) {  // TBD: validation??
        try {
	        shortLinkBean = new ShortLinkWebService().getShortLink(guid);
        } catch (Exception e) {
    // Ignore...
        }
        // Heck!!! In case of delay in saving a newly crated bean, try one more time ????
        if(shortLinkBean == null) {
    try {
    	        shortLinkBean = new ShortLinkWebService().getShortLink(guid);
    } catch (Exception e) {
        // Ignore...
    }
        }
	} else {
	    // ????
        if(! RedirectType.TYPE_VERIFY.equals(requestUrlRedirectType)) {
    // error.
    // guid is required...
    // ...
        }
	}

//    if(servletPath.equals("/confirm") || servletPath.equals("/flash")) {
//        if(shortLinkBean != null) {
//            requiresRefresh = true;
//        }
//    }

// ????
//    if(servletPath.equals("/verify")) {
//        requiresRefresh = true;
//    }

} else {   // servletPath.length == 1 or 2.
    String path = null;
    if(servletPath != null && servletPath.length() == 2) {   // ???
        path = servletPath.substring(1);    
    } else {    
        // ???
        // TBD: Problem parsing url patterns like http://abc.com/x/pqr, where "x" is a single char that is not a "servletPath".
        // .... This is really invalid. But, in the current implementation, 
        //      "x" is parsed as "path" component.
        // ....
        // Note: note the pattern, http://abc.com/@twt/c/pqr,
        //      where the servletPath == null, but "path" is "c"
        // .....
        
        path = ShortUrlAccessHelper.findPathComponent(requestUrl);
    }
    
    if(path != null) {
	    if(path.equals("v")) {
	        requestUrlRedirectType = RedirectType.TYPE_VERIFY;
	    } else if(path.equals("c")) {
	        requestUrlRedirectType = RedirectType.TYPE_CONFIRM;
	    } else if(path.equals("f")) {
	        requestUrlRedirectType = RedirectType.TYPE_FLASH;
	    } else if(path.equals("p")) {
	        requestUrlRedirectType = RedirectType.TYPE_PERMANENT;
	    } else if(path.equals("t")) {
	        requestUrlRedirectType = RedirectType.TYPE_TEMPORARY;
	    } else {
	    
	        // temporary
	        int duration = -1;
	        try {
		        duration = Integer.parseInt(path);
	        } catch(NumberFormatException nfex) {
	            // ignore
	        }
	        if(duration >= 0) {
		        requestUrlRedirectType = RedirectType.TYPE_FLASH;
		        if(duration == 0) {
		            requestUrlFlashDuration = 10000L;   // 10 seconds.
		        } else {
		            requestUrlFlashDuration = duration * 1000L;
		        } 
	        }
	        // else ???
	                
	        // ???	                
	        // error. bail out. ???
	        // ignore ????
	        // ...
	    }        
    }

    // We have two choices regarding the "invalid path" such as "x".
    // (1) Path should be either null or should be valid otherwise.
    //     We'll throw error when encounber an invalid path. ==> With the if() below
    // (2) The url with an Invalid path is processed and is treated as the same url with no path.
    //     That is, http://abc.com/x/pqr is treated as http://abc.com/pqr  (with its redirectType attr, etc.) ==> Without if() below.
    if(path == null || (RedirectType.TYPE_VERIFY.equals(requestUrlRedirectType) || RedirectType.isValidType(requestUrlRedirectType))) {
        // Note that shortUrlShortUrl != requestUrl, in general. (e.g., path, etc.)
        shortLinkBean = ShortUrlAccessHelper.getInstance().findShortLink(requestUrl, path);
        // Heck!!! In case of delay in saving a newly crated bean, try one more time ????
        // If it's really request error (404), it's probably ok to show the error page slowly.
        // But, this retry can potentially reduce some of the false errors (due to slow GAE)...
        if(shortLinkBean == null) {
    shortLinkBean = ShortUrlAccessHelper.getInstance().findShortLink(requestUrl, path);
    // Heck!!! One more time!!!
    if(shortLinkBean == null) {
        shortLinkBean = ShortUrlAccessHelper.getInstance().findShortLink(requestUrl, path);
    }
        }
    } else {
        // Error
        // ...
    }
}


if(shortLinkBean != null) {
    shortUrlShortUrl = shortLinkBean.getShortUrl();   // Default short url...	    
    shortUrlRedirectType = shortLinkBean.getRedirectType();
}
if(requestUrlRedirectType == null) {
    requestUrlRedirectType = shortUrlRedirectType;  // ???
}
// Note: At this point, requestUrlRedirectType cannot be null, probably. (???)

String fullShortUrl = "";    // shortUrl with query param...
if(shortUrlShortUrl != null && !shortUrlShortUrl.isEmpty()) {
    fullShortUrl = shortUrlShortUrl;
    if(queryString != null && !queryString.isEmpty()) {
        fullShortUrl += "?" + queryString;
    }
}


//[6] Error. Bail out.
if(!RedirectType.TYPE_VERIFY.equals(requestUrlRedirectType)) {
    if(shortLinkBean == null) {
        // error...
        // Forward it to the error page...
        // Because of the "/*", we have to display the "home" page in place of 404 ???
        // /error/Default404 vs. /home ????
        // ......
%>
<jsp:forward page="/error/Default404" >
<jsp:param name="RequestURL" value="<%=requestUrl%>" />
</jsp:forward>
<%    
    }
}
%><%


////////////////////////////////////
// shortLinkBean can be null, at this point, only for redirect==verify with no input ShortLink...
//////////////////////////////////////


%><%

// TBD:
// This whole routing should be moved to a Java class/method
// ....


String shortUrlLongUrl = "";
String fullRedirectUrl = "";
if(shortLinkBean != null) {
	shortUrlLongUrl = shortLinkBean.getLongUrl();
}
if(shortUrlLongUrl != null && !shortUrlLongUrl.isEmpty() && URLUtil.isValidUrl(shortUrlLongUrl)) {   
    fullRedirectUrl = ShortUrlAccessHelper.getInstance().getRedirectUrl(shortUrlLongUrl, queryString);
}

com.ingressdb.fe.bean.AccessRecordJsBean accessRecordBean = null;
boolean isIngressDBEnabled = ConfigUtil.isApplicationIngressDBEnabled();
if(isIngressDBEnabled == true) {
if(shortUrlLongUrl != null && !shortUrlLongUrl.isEmpty() && URLUtil.isValidUrl(shortUrlLongUrl)) {

    // If it fails for any reason, just ignore and move on...
    try {
	    // TBD:
	    // Record it to the IngressDB.AccessRecord table....
	    accessRecordBean = new com.ingressdb.fe.bean.AccessRecordJsBean();
	    accessRecordBean.setGuid(GUID.generate());
	    accessRecordBean.setAppId(appBrand);            // temporary
	    accessRecordBean.setTargetType("ShortLink");    // temporary
	    accessRecordBean.setTargetAction("view");       // temporary
	    accessRecordBean.setTargetGuid(shortLinkBean.getGuid());
	    accessRecordBean.setHostname(requestHost);
	    accessRecordBean.setRequestUrl(requestUrl);
	    if(queryString != null) {
	        accessRecordBean.setQueryString(queryString);
	    }
		
  	    if(shortUrlShortUrl != null) {   // Can this be null???
  	        // Use customField 1 as the "primary key" == shortUrl
  	        accessRecordBean.setCustomField1(shortUrlShortUrl);
	    }

	    // TBD:
	    accessRecordBean.setFirstAccessTime(System.currentTimeMillis());
	    // ...
	    
	    // TBD:
	    // Go through all headers? ...    request.getHeaderNames();
	    // ...

	    // TBD:
	    String referer = request.getHeader("referer");
	    if(referer != null && !referer.isEmpty()) {
	        accessRecordBean.setReferer(referer);
	    }
	    String userAgent = request.getHeader("user-agent");
	    if(userAgent != null && !userAgent.isEmpty()) {
	        accessRecordBean.setUserAgent(userAgent);
	    }
	    String remoteAddr = request.getRemoteAddr();
	    if(remoteAddr != null && !remoteAddr.isEmpty()) {
	        accessRecordBean.setIpAddress(remoteAddr);
	    }
	    java.util.List<String> extras = new java.util.ArrayList<String>();
	    String remoteHost = request.getRemoteHost();
	    if(remoteHost != null && !remoteHost.isEmpty()) {
	        //accessRecordBean.setRemoteHost(remoteHost);
	        extras.add("remoteHost:" + remoteHost);
	    }
	    String acceptLanguage = request.getHeader("accept-language");
	    if(acceptLanguage != null && !acceptLanguage.isEmpty()) {
	        //accessRecordBean.setAcceptLanguage(acceptLanguage);
	        extras.add("acceptLanguage:" + acceptLanguage);
	    }
	    accessRecordBean.setExtras(extras);
	    // TBD: Country, etc.
	    //  How to get country???? From IP addr using geo database????
	    // etc.
	    // ....

	    java.util.List<String> attributes = new java.util.ArrayList<String>();
	    if(shortUrlShortUrl != null) {   // Can this be null???
	        attributes.add("shortUrl:" + shortUrlShortUrl);
	    }
	    attributes.add("longUrl:" + shortUrlLongUrl);   // Or, fullRedirectUrl ???
	    if(requestUrlRedirectType != null) {
	        attributes.add("redirectType:" + requestUrlRedirectType);
	    }
	    // etc.
	    accessRecordBean.setAttributes(attributes);
	    // ...
	
	    // TBD: Maybe, exclude "verify" access ???
	    String accessRecordGuid = new com.ingressdb.rw.service.AccessRecordWebService().createAccessRecord(accessRecordBean);
	    //ShortUrlAccessHelper.getInstance().createAccessRecord(accessRecordBean);   // This method deleted.......
	    // ...
    } catch(Exception ex) {
        // ignore
    }
} else {
    // ???
}
}  // isIngressDBEnabled == true
%><%

// Perform "redirect" here...
if(fullRedirectUrl != null && !fullRedirectUrl.isEmpty() && URLUtil.isValidUrl(fullRedirectUrl)) {
    // Handle 301 and 302 only.
    if(RedirectType.TYPE_VERIFY.equals(requestUrlRedirectType)) {
        // ...
    } else if(RedirectType.TYPE_CONFIRM.equals(requestUrlRedirectType)) {
        // ...
    } else if(RedirectType.TYPE_FLASH.equals(requestUrlRedirectType)) {
        // ...
    } else if(RedirectType.TYPE_PERMANENT.equals(requestUrlRedirectType)) {
        //response.setStatus(HttpServletResponse.SC_MOVED_PERMANENTLY);
        //response.setHeader("Location",fullRedirectUrl);
        response.sendRedirect(fullRedirectUrl);    
        return;  // ???
    } else if(RedirectType.TYPE_TEMPORARY.equals(requestUrlRedirectType)) {
        // TBD
        response.setStatus(302);
        response.setHeader("Location",fullRedirectUrl);
        return;  // ???
		// ???
    } else {
        // ???
    }
}

%><%


String shortUrlDomain = "";
String shortUrlToken = "";
if(shortLinkBean != null) {
	shortUrlDomain = (shortLinkBean.getDomain() != null) ? shortLinkBean.getDomain() : shortUrlDomain;
	shortUrlToken = (shortLinkBean.getToken() != null) ? shortLinkBean.getToken() : shortUrlToken;
}


//Redirect to the canonical url???

// Let's skip refresh....
//if(requiresRefresh) {
//    // TBD
//    String canonicalUrl = "";  // Build it from topLevelUrl, requestUrlRedirectType, shortUrlDomain, shortUrlToken
//    response.sendRedirect(canonicalUrl);
//}


%><%

String shortUrlCreatedDateTime = "";
String shortUrlModifiedDateTime = "";

// [5] ....
Long createdTime = null;
Long modifiedTime = null;
if(shortLinkBean != null) {
    createdTime = shortLinkBean.getCreatedTime();
    modifiedTime = shortLinkBean.getModifiedTime();
    shortUrlShortUrl = shortLinkBean.getShortUrl();
    shortUrlCreatedDateTime = DateUtil.formatDate(createdTime);
}
if(shortUrlShortUrl == null) {
    shortUrlShortUrl = "";
}
if(createdTime != null && createdTime != 0L) {
    shortUrlCreatedDateTime = DateUtil.formatDate(createdTime);
}
if(modifiedTime != null && modifiedTime != 0L) {
    shortUrlModifiedDateTime = DateUtil.formatDate(modifiedTime);
}

%><%

String shortLinkOwner = null;
if(shortLinkBean != null) {
    shortLinkOwner = shortLinkBean.getOwner();
}

boolean hasEditPermission = false;
long cutoffTime = new java.util.Date().getTime() - 12 * 3600 * 1000L;   // Arbitrary. 12 hours ago.
if(shortLinkOwner != null && userId != null) {
    if(shortLinkOwner.equals(userId) && (createdTime != null && createdTime >= cutoffTime )) {
        hasEditPermission = true;
    } else {
        hasEditPermission = false;
    }
}


%><%

String shortUrlShortMessage = "";
//...
String tokenType = null;
Long flashDuration = null;
Long expirationTime = null;
//....
String shortUrlNote = null;
//....

if(shortLinkBean != null) {
    shortUrlShortMessage = shortLinkBean.getShortMessage();
    // ...
    tokenType = shortLinkBean.getTokenType();
    flashDuration = shortLinkBean.getFlashDuration();
    expirationTime = shortLinkBean.getExpirationTime();
    // ....
    shortUrlNote = (shortLinkBean.getNote() != null) ? shortLinkBean.getNote() : shortUrlNote;
}

// temporary
if(flashDuration == null || flashDuration <= 0L) {
    flashDuration = 10000L;
}
if(requestUrlFlashDuration == null) {
    requestUrlFlashDuration = flashDuration;
}

int flashDurationSeconds = (int) (flashDuration.longValue() / 1000);
int requestUrlFlashDurationSeconds = (int) (requestUrlFlashDuration.longValue() / 1000);
// ...

%><%

// TBD: Check expirationTime, and
//      redirect to the "expired page" if expirationTime is set and earlier than now
//      .....

%><%

String shortUrlGuid = "";
String shortUrlEscapedLongUrl = "";
String shortUrlPageTitle = brandDisplayName;
String shortUrlAuthor = "";
String shortUrlDescription = AppBrand.getBrandDescription(appBrand);

if(shortLinkBean != null) {
    shortUrlGuid = shortLinkBean.getGuid();
    shortUrlEscapedLongUrl = HtmlTextUtil.escapeForHtml(shortLinkBean.getLongUrl());
    shortUrlPageTitle = HtmlTextUtil.escapeForHtml(brandDisplayName + " - " + HtmlPageUtil.createHtmlPageTitle(shortLinkBean.getShortUrl()));  // TBD
    shortUrlAuthor = "Anonymous User (" + shortLinkBean.getOwner() + ")";   // TBD
    shortUrlDescription = brandDisplayName + " - " + HtmlTextUtil.escapeForHtml(shortUrlShortMessage);  // ???
}


%><%

//Target web site.
String webPageTitle = "";
String webPageAuthor = "";
String webPageDescription = "";
Integer webPageRedirectCount = null;
com.pagesynopsis.fe.bean.PageFetchJsBean webPageBean = null;
//if(shortLinkBean != null) {
if(fullRedirectUrl != null && !fullRedirectUrl.isEmpty()) {
	webPageBean = PageSynopsisHelper.getInstance().findPageFetchByTargetUrl(fullRedirectUrl);
	if(webPageBean != null) {
	    webPageTitle = webPageBean.getPageTitle();
	    webPageAuthor = webPageBean.getPageAuthor();
	    webPageDescription = webPageBean.getPageSummary();  // ??
	    webPageRedirectCount = webPageBean.getResultRedirectCount();
	    // ????
	    String destinationurl = webPageBean.getDestinationUrl();
	    if(destinationurl != null && !destinationurl.isEmpty()) {
		    fullRedirectUrl = destinationurl;
	    }
	    // ????
	}
}
%><%
//...

// String tweetVia = "@" + appBrand;
String defaultTweetMessage = "";
if(shortLinkBean != null) {
    // ...
    String susurl = shortUrlShortUrl;
    if(fullShortUrl != null && !fullShortUrl.isEmpty()) {
        susurl = fullShortUrl;    // ????
    }
    String escapedMessage = "";
    if(shortUrlShortMessage != null && !shortUrlShortMessage.isEmpty()) {
	    escapedMessage = HtmlTextUtil.escapeForJavascript(shortUrlShortMessage);  // ??? escape for HTML ????
    } else if(webPageTitle != null && !webPageTitle.isEmpty()) {
	    escapedMessage = HtmlTextUtil.escapeForJavascript(webPageTitle);
    } else if(webPageDescription != null && !webPageDescription.isEmpty()) {
	    escapedMessage = HtmlTextUtil.escapeForJavascript(webPageDescription);
    }
    int maxTweetTitleLength = 140 - 20 - susurl.length();   // 20: buffer...
    String truncatedMessage = TextUtil.truncateAtWordBoundary(escapedMessage, maxTweetTitleLength, maxTweetTitleLength - 20, true);
    defaultTweetMessage = truncatedMessage + " " + susurl;
    /// ...
}
%><%
// ....
%><%
// TBD: Update the AccessRecord...
// ??? Should we do it a bit earlier (e.g., before error redictring, or even before permalink redirect ????)
// ???
// Note: we can use a separate accessRecord service...
// ...
%><!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!-- Consider adding an manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<html>
  <head>
    <meta charset="utf-8">
    <title><%=shortUrlPageTitle%></title>
    <meta name="author" content="<%=shortUrlAuthor%>">
    <meta name="description" content="<%=shortUrlDescription%>">

    <!-- Mobile viewport optimized: j.mp/bplateviewport -->
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <link rel="shortcut icon" href="/img/favicon-<%=appBrand%>.ico" />
    <link rel="icon" href="/img/favicon-<%=appBrand%>.ico" type="image/x-icon" />

    <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- CSS: implied media=all -->
    <%@ include file="/fragment/style.jspf" %>
    <!-- end CSS-->

    <!-- All JavaScript at the bottom, except for Modernizr / Respond.
       Modernizr enables HTML5 elements & feature detects; Respond is a polyfill for min/max-width CSS3 Media Queries
       For optimal performance, use a custom Modernizr build: www.modernizr.com/download/ -->
    <script src="http://ww2.filestoa.com/js/modernizr/modernizr-2.0.6.min.js"></script>

  </head>

  <body>

<jsp:include page="/fragment/TopNavbar">
<jsp:param name="pageCode" value="shortlink/view"/>
<jsp:param name="brandDisplayName" value="<%=brandDisplayName%>"/>
</jsp:include>



    <div class="container-fluid">

      <!-- Main hero unit for a primary marketing message or call to action -->
      <div id="main" class="hero-unit">


      <form class="form-inline" id="form_shorturl_view" method="POST" action="" accept-charset="UTF-8">

        <div id="cannyurl_logo">
	 	    <h1><%=brandDisplayName%><sup>&beta;</sup></h1>
        </div>

      <div id="shorturl_view_header">
      <fieldset name="fieldset_shorturl_header">
      <table style="background-color: transparent;">
      <tr>
      <td>
          <label class="input_label_text" for="input_shorturl_shorturl" id="input_shorturl_shorturl_label" title="Input short URL">Short URL:</label>
      </td>
      <td>
        <div id="shorturl_view_shorturl">
          <input class="input-xlarge uneditable-input" type="text" name="input_shorturl_shorturl" id="input_shorturl_shorturl" size="35" value="<%=fullShortUrl%>"></input>        
          <input type="hidden" name="hidden_shorturl_shorturl" id="hidden_shorturl_shorturl" value="<%=shortUrlShortUrl%>"></input>
<%
if(RedirectType.TYPE_VERIFY.equals(requestUrlRedirectType)) {
%>
          <button class="btn" type="button" name="button_shorturl_verify" id="button_shorturl_verify" title="Verify this short URL" style="width:70px">Verify</button>  
<%
}
%>
<%
if(hasEditPermission == true) {
%>
          <button class="btn" type="button" name="button_shorturl_edit" id="button_shorturl_edit" title="Edit the short URL info" style="width:70px">Edit</button> 
<%
}
%>      
          <button class="btn" type="button" name="button_shorturl_new" id="button_shorturl_new" title="Create a brand new short URL" style="width:70px">New</button> 
        </div>
      </td>
      </tr>
      <tr>
      <td>
          <label class="input_label_text" for="input_shorturl_redirecturl" id="input_shorturl_redirecturl_label" title="Long URL of the target Web page">Target URL:</label>
      </td>
      <td>
        <div id="shorturl_view_redirecturl">
          <textarea name="input_shorturl_redirecturl" id="input_shorturl_redirecturl" cols="70" rows="2" readonly><%=fullRedirectUrl%></textarea>
        </div>
      </td>
	  </tr>
      </table>
      </fieldset>
      </div>
      
      
<%
if(RedirectType.TYPE_CONFIRM.equals(requestUrlRedirectType) || RedirectType.TYPE_FLASH.equals(requestUrlRedirectType)) {
%>
      <div id="shorturl_view_prompt">
      <fieldset name="fieldset_shorturl_prompt">

<%
if(RedirectType.TYPE_FLASH.equals(requestUrlRedirectType)) {
%>
<div style="width:95%;">
<div class="progress progress-info">
  <div class="bar" style="width: 100%;" id="flashurl_view_progressbar"></div>
</div>
</div>
<%
}
%>      

        <div id="shorturl_view_prompt_message">
<%
if(RedirectType.TYPE_CONFIRM.equals(requestUrlRedirectType)) {
%>
        Would you like to proceed to this website?
<%
} else {
%>
        You will be redirected to the destination website in <span><%=requestUrlFlashDurationSeconds%></span> seconds. 
<%
}
%>      

        </div>
        <div id="shorturl_view_prompt_buttons">
          <button class="btn-success shorturl_prompt_button" type="button" name="button_shorturl_prompt_proceed" id="button_shorturl_prompt_proceed" title="Procced to this page">Proceed</button>  
          
<%
if(RedirectType.TYPE_FLASH.equals(requestUrlRedirectType)) {
%>
          &nbsp;
          <button class="btn-warning shorturl_prompt_button" type="button" name="button_shorturl_prompt_stop" id="button_shorturl_prompt_stop" title="Stop redirection">Stop</button>  
<!-- 
          &nbsp;
          <span id="shorturl_view_prompt_counter"><span><%=requestUrlFlashDurationSeconds%></span> seconds</span>
-->
<%
}
%>      
          
        </div>
      </fieldset>
      </div>
<%
}
%>      
      
      
      
      
<%
if(RedirectType.TYPE_VERIFY.equals(requestUrlRedirectType)) {
%>
      <div id="shorturl_view_detail">
<%
/*
    if(requestUrlRedirectType != null && !requestUrlRedirectType.isEmpty()) {
%>
        <div id="shorturl_view_detail_redirect_type">
<span class="shorturl_view_detail_request_redirect_type_label">
Current Redirect Type:
</span><span class="shorturl_view_detail_request_redirect_type_text">
<%=requestUrlRedirectType%>
</span>
        </div>
<%
    }
*/
%>
<%
    if(shortUrlRedirectType != null && !shortUrlRedirectType.isEmpty()) {
%>
        <div id="shorturl_view_detail_shortlink_redirect_type">
<span class="shorturl_view_detail_shortlink_redirect_type_label">
Redirect Type:
</span><span class="shorturl_view_detail_shortlink_redirect_type_text">
"<%=shortUrlRedirectType%>" - <%=RedirectType.getLabel(shortUrlRedirectType)%>
</span>
        </div>
<%
    }
%>

<%
    if(RedirectType.TYPE_FLASH.equals(shortUrlRedirectType)) {
        if(flashDuration != null) {
%>


        <div id="shorturl_view_detail_shortlink_flash_duration">
<span class="shorturl_view_detail_shortlink_flash_duration_label">
Flash Duration:
</span><span class="shorturl_view_detail_shortlink_flash_duration_text">
<%=flashDurationSeconds%> seconds
</span>
        </div>
<%
        }
    }
%>


<%
    if(expirationTime != null && expirationTime > 0L) {
%>
        <div id="shorturl_view_detail_expiration_time">
<span class="shorturl_view_detail_expiration_time_label">
Short URL Expiration Time:
</span><span class="shorturl_view_detail_expiration_time_text">
<%=DateUtil.formatDateNoSeconds(expirationTime.longValue())%>
</span>
        </div>
<%
    }
%>
      </div>
<%
}
%>      


      <div id="shorturl_body">
      <fieldset name="fieldset_shorturl_body">

<%
if(shortUrlShortMessage != null && !shortUrlShortMessage.isEmpty()) {   // TBD: Validation of shortUrlShortMessage ????
    String escapedShortMessage = HtmlTextUtil.escapeForHtml(shortUrlShortMessage, true); 
%>
        <div id="shorturl_body_main" class="shorturl_view_body">
          <h3 id="shorturl_body_main_short_message_text">Message from the Creator of this Short URL</h3>
          <div id="shorturl_view_body_main_short_message">
<%=escapedShortMessage%>
          </div>
        </div>
<%
}
%>


<%
if(RedirectType.TYPE_VERIFY.equals(requestUrlRedirectType) || RedirectType.TYPE_CONFIRM.equals(requestUrlRedirectType) || RedirectType.TYPE_FLASH.equals(requestUrlRedirectType)) {
%>
      <div id="shorturl_body_main_pageinfo" class="shorturl_view_body">
          <h3 id="shorturl_body_main_pageinfo_text">Target Website Information</h3>
          <div id="shorturl_view_body_main_pageinfo">

            <table class="table" style="background-color: transparent;">
<%
if(webPageBean != null) {
boolean isAtLeastOneFieldFound = false;
%>
<%
if(webPageTitle != null && !webPageTitle.isEmpty()) {
    isAtLeastOneFieldFound = true;
%>
            <tr>
            <td class="pageinfo_titlecolumn_text">Page&nbsp;Title:</td>
            <td><%=webPageTitle%></td>
            </tr>
<%
}
%>            
<%
if(webPageAuthor != null && !webPageAuthor.isEmpty()) {
    isAtLeastOneFieldFound = true;
%>
            <tr>
            <td class="pageinfo_titlecolumn_text">Page&nbsp;Author:</td>
            <td><%=webPageAuthor%></td>
            </tr>
<%
}
%>
<%
if(webPageDescription != null && !webPageDescription.isEmpty()) {
    isAtLeastOneFieldFound = true;
%>
            <tr>
            <td class="pageinfo_titlecolumn_text">Page&nbsp;Description:</td>
            <td><%=webPageDescription%></td>
            </tr>
<%
}
%>

<%
if(webPageRedirectCount != null && webPageRedirectCount > 1) {
    isAtLeastOneFieldFound = true;
%>
            <tr class="alert alert-error">
            <td class="pageinfo_titlecolumn_text">Warning:</td>
            <td>
            The short URL has multiple back-to-back redirections (redirect count: <%=webPageRedirectCount%>).
            Generally, this should be considred as a red flag.
            </tr>
<%
}
%>
<%
if(isAtLeastOneFieldFound == false) {
%>
            <tr class="alert">
            <td class="pageinfo_titlecolumn_text">Warning:</td>
            <td>
            No page information is found for the short URL, <i><%=fullShortUrl%></i>.
            </tr>
<%
}
%>
<%
} else {   // webPageBean == null
%>
            <tr class="alert">
            <td class="pageinfo_titlecolumn_text">Error:</td>
            <td>
            Failed to retrieve the page info for the short URL. <i><%=fullShortUrl%></i>.
            The failure might have been due to the <%=brandDisplayName%> server error.
            Or because the short URL was invalid
            or it had too many redirections (the URL redirects to another URL, which in turn redirects to another URL, etc.).
            </tr>
<%    
}
%>
            </table>

          </div>
      </div>
<%
}
%>


      </fieldset>
      </div>

      <div id="shorturl_footer">
      <fieldset name="fieldset_shorturl_footer">
        <div id="shorturl_created_time">
          <span id="shorturl_created_time_label">Created:&nbsp;</span>                    
          <span id="shorturl_created_time_text"><%=shortUrlCreatedDateTime%></span>
<% if(shortUrlModifiedDateTime != null && !shortUrlModifiedDateTime.isEmpty()) { %>
          &nbsp;&nbsp;
          <span id="shorturl_modified_time_label">Modified:&nbsp;</span>                    
          <span id="shorturl_modified_time_text"><%=shortUrlModifiedDateTime%></span>
<% } %>
        </div>
      </fieldset>
      </div>

      </form>
      </div>  <!--   Hero unit  -->


<!-- Page Footer -->    
<%@ include file="/fragment/Footer.jspf" %>
<!-- Page Footer -->    

    </div> <!--! end of #container -->



    <div id="div_status_floater" style="display: none;" class="alert">
    </div>
 

    <!-- JavaScript at the bottom for fast page loading -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="http://ww2.filestoa.com/js/jquery/jquery-1.8.3.min.js"><\/script>')</script>
	<script src="http://ww2.filestoa.com/js/jquery/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="http://ww2.filestoa.com/js/jquery/plugin/jquery.floatobject-1.4.js"></script>
    <script src="http://ww2.filestoa.com/js/jquery/plugin/jquery-ui-timepicker-addon.js"></script>
    <script src="http://ww2.filestoa.com/js/jquery/plugin/history.js/uncompressed/history.adapter.jquery.js"></script>
    <script src="http://ww2.filestoa.com/js/jquery/plugin/history.js/uncompressed/history.js"></script>
    <script src="http://ww2.filestoa.com/js/jquery/plugin/history.js/uncompressed/history.html4.js"></script>
    <script src="http://ww2.filestoa.com/js/bootstrap/bootstrap-2.0.js"></script>
    <script type="text/javascript" src="/js/core/debugutil.js"></script>
    <script type="text/javascript" src="/js/core/progressbar-1.0.js"></script>
    <script type="text/javascript" src="http://ww2.filestoa.com/js/core/datetimeutil-1.0.js"></script>
    <script type="text/javascript" src="http://ww2.filestoa.com/js/core/emailutil-1.0.js"></script>
    <script type="text/javascript" src="/js/bean/shortlinkjsbean-1.0.js"></script>
    <script type="text/javascript" src="/js/mylibs/mytimer-1.1.js"></script>
    <script type="text/javascript" src="/js/mylibs/redirecturl-1.0.js"></script>
	<script defer type="text/javascript" src="/js/plugins.js"></script>
    <script defer type="text/javascript" src="/js/script.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/fiveten-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/statushelper-1.1.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/tweethelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/signuphelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/feedbackhelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/emailhelper-1.0.js"></script>
    <!-- end scripts-->



    <script>
    // Global vars
    var feedbackHelper;
    $(function() {
    	var targetService = "<%=brandDisplayName%>";
    	var targetPage = "view";
    	feedbackHelper = new webstoa.FeedbackHelper(targetService, targetPage);
    });
    </script>

    <script>
    // Email address "decoding"...
    $(function() {
        $("a").each(function() {
        	var anchor = $(this);
        	webstoa.EmailUtil.decodeMailtoAnchor(anchor);
        });
    });
    </script>

    <script>
    $(function() {
        $("#anchor_feedback_footer").click(function() {
            if(DEBUG_ENABLED) console.log("Feedback button clicked."); 
            var targetItem = 'footer';
            feedbackHelper.feedback(targetItem);
		    return false;
        });
    });
    </script>


    <script>
    // Function definitions.
    function updateStatus(msg, type, time) {
    	if(!time) {
    		time = 5000;   // milliseconds.
    	}
    	if(!type) {
    		// type = "info";  // warning, error, etc....
    	}
    	//$("#div_status_floater").text(msg);
    	//$("#div_status_floater").fadeIn().delay(time).fadeOut();
		$("#div_status_floater").textInOut(msg, type, time);
    }    
    </script>

    <script>
    // temporary
    function replaceURLWithHTMLLinks(text) {
        if(text) {
            var exp = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
            //var exp = /\(?\bhttp:\/\/[-A-Za-z0-9+&@#\/%?=~_()|!:,.;]*[-A-Za-z0-9+&@#/%=~_()|]/ig;
            return text.replace(exp,"<a rel='nofollow' href='$1'>$1</a>"); 
        }
        return '';    // ???
    }
    </script>


    <script>
$(function() {
// Initialize all datetime strings.
<% if(shortUrlCreatedDateTime != null && !shortUrlCreatedDateTime.isEmpty()) { %>
var createdTimeStr = getStringFromTime(<%=createdTime%>);  
$("#shorturl_created_time_text").text(createdTimeStr);
<% } %>
<% if(shortUrlModifiedDateTime != null && !shortUrlModifiedDateTime.isEmpty()) { %>
var modifiedTimeStr = getStringFromTime(<%=modifiedTime%>);  
$("#shorturl_modified_time_text").text(modifiedTimeStr);
<% } %>
});    
    </script>


    <script>
  $(function() {
	  $("#flashurl_view_progressbar").css("width", "100%");
  });
  </script>
    <script>
    var refreshProgressBar = function(currentValue) {
        // if(DEBUG_ENABLED) console.log("refreshProgressBar() called...... currentValue = " + currentValue);
        // alert("currentValue = " + currentValue);
        
        // temporary
        var max = 100.0;
        var min = 0.0;
        if(currentValue != null) {
            // if(currentValue < min + 0.5) {
            // 	currentValue = min;
            // }
            // var percent = Math.floor(currentValue) + "%";
        	var ratio = (currentValue * 100.0 / (max - min));
            if(ratio < min + 0.5) {
            	ratio = min;
            }
            var percent = Math.floor(ratio) + "%";
	        $("#flashurl_view_progressbar").css("width", percent);

        }        
    }
    </script>
    <script>
    // Global vars
    var progressBar;
    var flashTimer = new mytimer.Timer(<%=requestUrlFlashDuration%>);
  $(function() {
	  // ...
      var flashMillis = <%=requestUrlFlashDuration%>;   // flashDuration...
	  progressBar = new cannyurl.ProgressBar(flashMillis, 150, 100.0, 0.0);
	  progressBar.setDisplayCallback(refreshProgressBar);
      flashTimer.setProgressBar(progressBar);
	  // ...
  });
    var flashRedirectEnabled = true;
    function delayedRedirect() {
    	var milli = <%=requestUrlFlashDuration%>;   // flashDuration...
       	setTimeout(redirectToTargetURL, milli);
    	
    	// ????
       	// if(progressBar) {
       	// 	rogressBar.start(true); 
       	// }
    }
    function redirectToTargetURL() {
    	if(flashRedirectEnabled == true) {
            window.location = "<%=fullRedirectUrl%>"
    	}
    }
    var stopFlashTimer = function() {
        flashRedirectEnabled = false;
        $("#button_shorturl_prompt_stop").attr('disabled','disabled');
        if(flashTimer) {
        	flashTimer.stop();
        }
        // ????
        // if(progressBar) {
        // 	progressBar.reset();
        // }        // ....    	
    };
    </script>


    <script>
    // "Init"
    $(function() {
    	// TBD: initially hide $("#div_status_floater") ...
    	$("#div_status_floater").text("").hide();
    
        // Background image test.
        //$("#container").css("background-image", 'url(/img/bg/oceansunset.jpg)');
        //$(document.body).css("background", "silver");
    	//$("#container").css("background", "ivory");

        // TBD: Location...
    	var params = {x: 585, y: 70, speed: 'normal' };
    	$("#div_status_floater").makeFloat(params);
    	
    	// temporary
    	// linkify the content
    	var contentOriginal = $("#shorturl_view_body_main_short_message").html();
    	var contentLinkified = replaceURLWithHTMLLinks(contentOriginal);
    	$("#shorturl_view_body_main_short_message").html(contentLinkified);
    	// temporary
    	
<%
if(RedirectType.TYPE_VERIFY.equals(requestUrlRedirectType)) {
%>
        $("#input_shorturl_shorturl").removeAttr('readonly');
<%
} else {
%>
        $("#input_shorturl_shorturl").attr('readonly','readonly');
<%
}
%>
<%
if(RedirectType.TYPE_FLASH.equals(requestUrlRedirectType)) {
%>
        // Start the flash countdown...
        //delayedRedirect();
        //var counterText = $("#shorturl_view_prompt_counter span");
        var counterText = $("#shorturl_view_prompt_message span");
        flashTimer.setCounterTextElement(counterText);
        flashTimer.setCountdownAction(redirectToTargetURL);
        flashTimer.start();
<%
}
%>

    	// Peeng?
    	//var fiveTenTimer = new fiveten.FiveTenTimer(14839, 2);  // ???
    	//fiveTenTimer.start();
    });
    </script>

    <script>
    // App-related vars.
    var userGuid;
    var shortLinkJsBean;
    //var accessRecordJsBean;
<%
if(userId != null) {
%>
    userGuid = "<%=userId%>";
<%
}
%>
<%
if(shortLinkBean != null) {
%>
	var shortUrlJsObjectStr = '<%=shortLinkBean.toEscapedJsonStringForJavascript()%>';
	if(DEBUG_ENABLED) console.log("shortUrlJsObjectStr = " + shortUrlJsObjectStr);
	shortLinkJsBean = cannyurl.wa.bean.ShortLinkJsBean.fromJSON(shortUrlJsObjectStr);
    if(DEBUG_ENABLED) console.log("shortLinkJsBean = " + shortLinkJsBean.toString());
<%
}
%>
<%
if(isIngressDBEnabled == true) {
    if(accessRecordBean != null) {
/*
%>
	//var accessRecordJsObjectStr = '<%=accessRecordBean.toEscapedJsonStringForJavascript()%>';
	//if(DEBUG_ENABLED) console.log("accessRecordJsObjectStr = " + accessRecordJsObjectStr);
	//accessRecordJsBean = cannyurl.wa.bean.AccessRecordJsBean.fromJSON(accessRecordJsObjectStr);
    //if(DEBUG_ENABLED) console.log("accessRecordJsBean = " + accessRecordJsBean.toString());
<%
*/
    }
}
%>
    </script>

    <script>
    $(function() {
        $("#button_shorturl_new").click(function() {
          if(DEBUG_ENABLED) console.log("New button clicked.");  
          stopFlashTimer();   // Any button click stops the flash timer...
          
          var shortUrlNewPage = "<%=topLevelUrl%>" + "shorten/";
          if(DEBUG_ENABLED) console.log("shortUrlNewPage = " + shortUrlNewPage);
          var ans = window.confirm("Create a new short URL?");
          if(ans) {
  	           updateStatus('Creating a new short URL...', 'info', 5550);
     	       window.location = shortUrlNewPage;
    	    }
        });

        $("#button_shorturl_edit").click(function() {
            if(DEBUG_ENABLED) console.log("Edit button clicked.");
            stopFlashTimer();   // Any button click stops the flash timer...

            var guid = '';
            if(shortLinkJsBean) {
                guid = shortLinkJsBean.getGuid();
            }
            var shortUrlEditPage = "<%=topLevelUrl%>" + "shorten/" + guid;
            if(DEBUG_ENABLED) console.log("shortUrlEditPage = " + shortUrlEditPage);
            var ans = window.confirm("Edit the short URL?");
            if(ans) {
    	      updateStatus('Opening the shortUrl edit page...', 'info', 5550);
       	      window.location = shortUrlEditPage;
      	    }
          });

    	$("#button_shorturl_verify").click(function() {
    	    if(DEBUG_ENABLED) console.log("ShortUrl verify button clicked.");
            stopFlashTimer();   // Any button click stops the flash timer...

            var fullShortUrl = $("#input_shorturl_shorturl").val();   // This can potentially include an additional query string
    		// TBD: use "hidden_shorturl_shorturl" (which excludes query string)  ???
    		if(fullShortUrl != '') {
                var redirectUrlHelper = new cannyurl.redirecturl.RedirectUrlHelper();
                redirectUrlHelper.parseShortUrl(fullShortUrl);  // Can this handle query string????
                if(redirectUrlHelper.isReady()) {
      		        updateStatus('Opening the verify page ...', 'info', 5550);
                    var verifyRedirectUrl = redirectUrlHelper.verifyUrl();
                    window.location = verifyRedirectUrl;
                } else {
            	    if(DEBUG_ENABLED) console.log("ShortUrl verify failed.");
       		        updateStatus('Failed to open the verify page. ...', 'error', 5550);
                }
    		} else {
        	    if(DEBUG_ENABLED) console.log("ShortUrl verify cannot be performed.");
   		        updateStatus('Verify page cannot be opened. ...', 'error', 5550);
    		}
    		return false;
    	});

    });
    </script>

    <script>
    $(function() {
        $("#button_shorturl_prompt_proceed").click(function() {
            if(DEBUG_ENABLED) console.log("Proceed button clicked.");
            var redirectUrl = "<%=fullRedirectUrl%>";
            if(DEBUG_ENABLED) console.log("redirectUrl = " + redirectUrl);
    	      updateStatus('Redirecting...', 'info', 5550);
     	      window.location = redirectUrl;
          });

        $("#button_shorturl_prompt_stop").click(function() {
            if(DEBUG_ENABLED) console.log("Stop button clicked.");
            // TBD..
            stopFlashTimer();
            // ....
          });

    });
    </script>



    <script>
    // Ajax form handlers.
  $(function() {
	  // ...
  });
    </script>


<script>
$(function() {
	// Top menu handlers
	// Note: /view and /info reuire guid....
	// TBD: Due to the way we use subdomains...
	//      Every time we change url (subdomain), the login state changes...
	//      We use 27 subdomains, therfore this is not not very usable...
	// For now, all urls should point to relative URLs "/view", "/verify", etc...
	//      rather than path-based URLs, e.g., a.fm.gs/i/abcd, ....
    $("#topmenu_nav_shorten").click(function() {
        if(DEBUG_ENABLED) console.log("topmenu_nav_shorten anchor clicked."); 
        if(shortLinkJsBean) {
            var guid = shortLinkJsBean.getGuid();
            window.location = "/shorten/" + guid;            		
        } else {
            // ??? error ???
        }
        return false;
    });
    $("#topmenu_nav_view").click(function() {
        if(DEBUG_ENABLED) console.log("topmenu_nav_view anchor clicked."); 
        //    if(shortLinkJsBean) {
        //        var shortUrl = shortLinkJsBean.getShortUrl();
        //        if(! shortUrl) {
        //        	var domain = shortLinkJsBean.getDomain();
        //        	var token = shortLinkJsBean.getToken();
        //        	shortUrl = domain + token;
        //        }
        //        window.location = shortUrl;            		
        //    } else {
        //        // ??? error ???
        //    }
        return false;
    });
    $("#topmenu_nav_verify").click(function() {
        if(DEBUG_ENABLED) console.log("topmenu_nav_verify anchor clicked."); 
        if(shortLinkJsBean) {
            //var redirectUrlHelper = new cannyurl.redirecturl.RedirectUrlHelper(shortLinkJsBean);
            //if(redirectUrlHelper.isReady()) {
            //    var verifyUrl = redirectUrlHelper.verifyUrl();
            //    window.location = verifyUrl;
            //} else {
                var guid = shortLinkJsBean.getGuid();
                window.location = "/verify/" + guid;	
            //}
        } else {
            // ??? error ???
        }
        return false;
    });
<%
// ????
if(isAppAuthDisabled == true || isAppAuthOptional == true || isUserAuthenticated == true) {
%>
    $("#topmenu_nav_info").click(function() {
        if(DEBUG_ENABLED) console.log("topmenu_nav_info anchor clicked."); 
        if(shortLinkJsBean) {
            //var redirectUrlHelper = new cannyurl.redirecturl.RedirectUrlHelper(shortLinkJsBean);
            //if(redirectUrlHelper.isReady()) {
            //    var infoUrl = redirectUrlHelper.infoUrl();
            //    window.location = infoUrl;
            //} else {
                var guid = shortLinkJsBean.getGuid();
                window.location = "/info/" + guid;	
            //}
        } else {
            // ??? error ???
        }
        return false;
    });
    //$("#topmenu_nav_list").click(function() {
    //    if(DEBUG_ENABLED) console.log("topmenu_nav_list anchor clicked."); 
    //    window.location = "/list";
    //    return false;
    //});
<%
}
%>
});
</script>


<%
String googleAnalyticsCode = AnalyticsHelper.getInstance().getGoogleAnalyticsCode();
if(googleAnalyticsCode != null && !googleAnalyticsCode.isEmpty()) {    // And, if it is enabled (e.g. through config). TBD...
%>
    <!-- Change UA-XXXXX-X to be your site's ID -->
    <script>
      window._gaq = [['_setAccount','<%=googleAnalyticsCode%>'],['_trackPageview'],['_trackPageLoadTime']];
      Modernizr.load({
        load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
      });
    </script>
<%
}
%>

    <!-- Prompt IE 6 users to install Chrome Frame. Remove this if you want to support IE 6.
       chromium.org/developers/how-tos/chrome-frame-getting-started -->
    <!--[if lt IE 7 ]>
      <script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
      <script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
    <![endif]-->

    </body>
</html>