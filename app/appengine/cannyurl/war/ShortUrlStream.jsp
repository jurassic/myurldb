<%@ page import="com.myurldb.ws.core.*, com.myurldb.ws.*, com.cannyurl.af.util.*, com.cannyurl.af.auth.common.*, com.cannyurl.af.auth.*, com.cannyurl.af.auth.user.*, com.cannyurl.fe.*, com.cannyurl.fe.core.*, com.cannyurl.fe.bean.*, com.cannyurl.fe.util.*, com.cannyurl.wa.service.*, com.cannyurl.common.*, com.cannyurl.util.*, com.cannyurl.app.auth.*, com.cannyurl.app.auth.filter.*, com.cannyurl.app.util.*, com.cannyurl.helper.*" 
%><%@ page contentType="text/html; charset=UTF-8" 
%><%@ page errorPage="/error/UnknownError" 
%><%!
private static final java.util.logging.Logger logger = java.util.logging.Logger.getLogger("ShortUrlStream");
%><%
boolean isAppAuthDisabled = ConfigUtil.isApplicationAuthDisabled();
boolean isAppAuthOptional = ConfigUtil.isApplicationAuthOptional();
String appAuthMode = ConfigUtil.getApplicationAuthMode();
boolean isUsingTwitterAuth = false;
if(AppAuthMode.MODE_TWITTER.equals(appAuthMode)) {
    isUsingTwitterAuth = true;
}
%><%
// {1] Session
SessionBean sessionBean = UserSessionManager.getInstance().setSessionBean(request, response);
String sessionToken = sessionBean.getToken();
String userId = sessionBean.getUserId();

// [2] Parse url.
String requestURI = request.getRequestURI();
String requestUrl = request.getRequestURL().toString();
String topLevelUrl = UrlHelper.getInstance().getTopLevelURLFromRequestURL(requestUrl);
String contextPath = request.getContextPath();
String servletPath = request.getServletPath();
String pathInfo = request.getPathInfo();
String queryString = request.getQueryString();
java.util.Map<String,Object> queryParams = URLUtil.parseParamMap(request.getParameterMap());
java.util.Map<String,String> pathParamMap = PathInfoUtil.parsePathInfo(pathInfo);

//...
%><%
// "Branding"
String appBrand = BrandingHelper.getInstance().getAppBrand();
String brandDisplayName = BrandingHelper.getInstance().getBrandDisplayName();
String brandDescription = BrandingHelper.getInstance().getBrandDescription();
%><%String themeBootstrap = SiteThemeHelper.getInstance().getBootstrapTheme();
String themeJqueryUi = SiteThemeHelper.getInstance().getJqueryUiTheme();%><%
boolean isUserAuthenticated = false;
String userUsername = null;
String loginUrl = null;
String logoutUrl = null;
RequestAuthStateBean authStateBean = (RequestAuthStateBean) request.getAttribute(AuthUtil.REQUEST_ATTR_AUTHSTATEBEAN);
if(authStateBean == null) {
    // Filter might not have applied (because of the /* mapping, etc...) 
    // Try one more time ???
    authStateBean = AuthFilterUtil.createAuthStateBean(request, response);
    if(authStateBean != null) {
        // Add it to the current request object....
        request.setAttribute(AuthUtil.REQUEST_ATTR_AUTHSTATEBEAN, authStateBean);
    }
}
if(authStateBean != null) {
    isUserAuthenticated = authStateBean.isAuthenticated();
    if(isUserAuthenticated) {
        logoutUrl = authStateBean.getLogoutUrl();
        userUsername = authStateBean.getUsername();
    } else {
        loginUrl = authStateBean.getLoginUrl();
    }   
}
%><%
String twitterHandle = "";
String twitterUserProfileUrl = "";
if(isUsingTwitterAuth) {
	if(userUsername != null && !userUsername.isEmpty()) {
	    twitterHandle = "@" + userUsername;      // ???
	    twitterUserProfileUrl = "http://twitter.com/" + userUsername;
	}
}
%><%
if(isUserAuthenticated == false) {
    // ????
    // response.sendRedirect(loginUrl);
}
%><%
// String getCellStr = ServletPathUtil.getGeoCellString(servletPath);
// CellLatitudeLongitudeJsBean geoCellStruct = ServletPathUtil.getGeoCellStruct(servletPath);
// ???
        
String geoCellStr = null;
CellLatitudeLongitudeJsBean geoCellStruct = null;
if(pathParamMap != null) {
    geoCellStr = pathParamMap.get("geocell");
}
if(geoCellStr != null && !geoCellStr.isEmpty()) {   // TBD: validation...
    // Does this work???
    if(logger.isLoggable(java.util.logging.Level.WARNING)) logger.warning("Param geocell found: geoCellStr = " + geoCellStr);
    geoCellStruct = QueryParamUtil.parseGeoCellParam(geoCellStr);
    // TBD:
    //....
}        


%><%
// ???
String pageCode = servletPath;
if(pageCode != null && pageCode.startsWith("/")) {
    pageCode = servletPath.substring(1);
}
%><%
int pageSize = ConfigUtil.getPagerShortUrlStreamPageSize();
%><%

// [4] Initial validation
// URL "/stream/tech" ???
// ????
// ....
%><%
// ...
%><!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!-- Consider adding an manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<html>
  <head>
    <meta charset="utf-8">
    <title>Short URL Stream | <%=brandDisplayName%></title>
    <meta name="author" content="Aery Software" />
    <meta name="description" content="<%=AppBrand.getBrandDescription(appBrand)%>. It also provides free URL Shortener Web Services API." />

    <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Mobile viewport optimized: j.mp/bplateviewport -->
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <!-- CSS: implied media=all -->
    <%@ include file="/fragment/style.jspf" %>
    <!-- end CSS-->

    <!-- Le fav and touch icons -->
	<link rel="shortcut icon" href="/img/favicon-<%=appBrand%>.ico" />
	<link rel="icon" href="/img/favicon-<%=appBrand%>.ico" type="image/x-icon" />

    <!-- All JavaScript at the bottom, except for Modernizr / Respond.
       Modernizr enables HTML5 elements & feature detects; Respond is a polyfill for min/max-width CSS3 Media Queries
       For optimal performance, use a custom Modernizr build: www.modernizr.com/download/ -->
    <script src="http://ww2.filestoa.com/js/modernizr/modernizr-2.0.6.min.js"></script>
 
    <!--  Tweet Button  -->
    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

  </head>

  <body>

<jsp:include page="/fragment/TopNavbar">
<jsp:param name="pageCode" value="shortlink/stream"/>
<jsp:param name="brandDisplayName" value="<%=brandDisplayName%>"/>
</jsp:include>

    <div class="container-fluid">

      <!-- Main hero unit for a primary marketing message or call to action -->
      <div class="hero-unit">




      <form class="form-inline" id="form_message_list" method="POST" action="" accept-charset="UTF-8">


      <div id="message_list_header">

      <h2>Short URL Stream
      </h2>
      </div>

<div class="progress progress-info">
  <div class="bar" style="width: 100%;" id="table_message_list_progressbar"></div>
</div>

      <div id="message_body" class="message_list_body">

      <div id="message_list_main" class="message_list_main">
      <fieldset name="fieldset_message_list_main">



<table id="table_message_list_records" class="table table-striped" style="background-color: transparent;">
<thead>
<tr id="table_header_message_list">
<th><a id="table_message_list_header_redirectType" class="table_column_title" href="#"><span>Type</span><span id="table_message_list_marker_redirectType"></span></a></th>
<th><a id="table_message_list_header_shortUrl" class="table_column_title" href="#"><span>Short&nbsp;URL</span><span id="table_message_list_marker_shortUrl"></span></a></th>
<th><a id="table_message_list_header_longUrl" class="table_column_title" href="#"><span>Long&nbsp;URL</span><span id="table_message_list_marker_longUrl"></span></a></th>
<!-- 
<th style="width:100px;"><a id="table_message_list_header_message" class="table_column_title" href="#"><span>Message</span><span id="table_message_list_marker_message"></span></a></th>
 -->
<th>
  <span class="pull-right">
      <input type="text" class="pull-right" id="table_message_list_progress_counter" title="Current message count in the queue" size="3" style="text-align:right; width:30px;" disabled value="#">
  </span>
  <!-- 
  <span id="table_message_list_progress_indicia" class="pull-right">
     <span id="table_message_list_progress_counter" title="Current message count in the queue" style="display:inline-block; width: 30px; text-align: right; background-color: powderblue">#</span>
  </span>
   -->
</th>
<th>
  <span id="table_message_list_progress_buttons" class="pull-right">
     <a id="table_message_list_progress_nextbutton" href="#" title="Display next message" class="btn btn-mini"><i class="icon-step-forward"></i></a>
     <a id="table_message_list_progress_pausebutton" href="#" title="Pause/restart auto refreshing" class="btn btn-mini"><i class="icon-pause"></i></a>
  </span>
</th>
<!-- 
<th><a id="table_message_list_header_message" class="table_column_title" href="#"><span>Message</span><span id="table_message_list_marker_message"></span></a></th>
<th><a id="table_message_list_header_createdTime" class="table_column_title" href="#"><span id="table_message_list_marker_createdTime" class="pull-right"></span><span class="pull-right">Created Time</span></a></th>
 -->
</tr>
</thead>
<tbody>

<%
final long NOW = System.currentTimeMillis();
// final long timespanMillis = 2 * 3600 * 1000L;
final long timespanMillis = 6 * 3600 * 1000L;       // For weekends/nights... we may need to go back a little bit... (Note maxBatchCount below) 
final String termType = TermType.TERM_TENMINS;
String currentTenMins = TimeRangeUtil.getDayHourTenMins(NOW);
String previousTenMins = TimeRangeUtil.getPreviousTenMins(currentTenMins);


//String streamerStartingTenMins = TimeRangeUtil.getNextTenMins(previousTenMins);
String streamerStartingTenMins = previousTenMins;   // *** Note: we show some "actions" when the page loads... 
//// String previousPreviousTenMins = TimeRangeUtil.getPreviousTenMins(previousTenMins);
//// String streamerStartingTenMins = previousPreviousTenMins;   // temporary
//
long streamerStartTime = TimeRangeUtil.getMilli(streamerStartingTenMins);


//Get the key list.
//TBD: Get the full list??? Probably not... (Note: even if we have "full list", each shortlink may not be a "full fetch"....)
java.util.List<String> messageBatch = null;



// long[] tt = TimeRangeUtil.getMilliRange(termType, previousTenMins);
// long endingTime = NOW - TermType.getTimeSpanMillis(termType);   // To avoid complications at the boundary... TBD: Need to check getPreviousTermList() impplementation...
long endingTime = NOW - 2 * TermType.getTimeSpanMillis(termType);  // *** See note above...
//// long endingTime = NOW - 3 * TermType.getTimeSpanMillis(termType);  // temporary
long beginningTime = endingTime - timespanMillis;
// java.util.List<TermStruct> previousTermList = TimeRangeUtil.getPreviousTermList(beginningTime, endingTime, termType);
// java.util.List<TermStruct> previousTermList = TimeRangeUtil.getPreviousTermList(beginningTime, endingTime, termType, TermType.TERM_SIXHOURS);
java.util.List<TermStruct> previousTermList = TimeRangeUtil.getPreviousTermList(beginningTime, endingTime, termType, TermType.TERM_HOURLY);


// TBD:
// Filter by originApp ????
// .....

// temporary... To avoid fetching too many records...
//int maxBatchCount = 20;    // This number does not really need to be big...
int maxBatchCount = pageSize;
int cumulativeCount = 0;
if(previousTermList != null && !previousTermList.isEmpty()) {
    messageBatch = new java.util.ArrayList<String>();  // ???
            
    // [new]
    int termListSize = previousTermList.size();
    for(int s=termListSize-1; s>=0; s--) {
        TermStruct struct = previousTermList.get(s);
        String tt = struct.getTermType();
        String dh = struct.getDayHour();
        if(logger.isLoggable(java.util.logging.Level.INFO)) logger.info("tt = " + tt + "; dh = " + dh);
        // Note: each portion list is chronologically sorted ("createdTime asc")....
        java.util.List<String> portion = ShortUrlStreamHelper.getInstance().fetchShortLinkKeys(geoCellStruct, tt, dh);
        if(portion != null && !portion.isEmpty()) {
            // --> Therefore, we need to reverse the order....
            // TBD: Would it be more efficient to add them through a loop???? (instead of reverse() and then allAll()) ????
            java.util.Collections.reverse(portion);
            messageBatch.addAll(portion);
        }
        if(messageBatch.size() >= maxBatchCount) {
            if(logger.isLoggable(java.util.logging.Level.INFO)) logger.info("Fetched too many messages. Size = " + messageBatch.size());
            break;
        }
    }

    // [old]
/*            
    for(TermStruct struct : previousTermList) {
        String tt = struct.getTermType();
        String dh = struct.getDayHour();
        if(logger.isLoggable(java.util.logging.Level.INFO)) logger.info("tt = " + tt + "; dh = " + dh);
        java.util.List<String> portion = ShortUrlStreamHelper.getInstance().fetchMessageKeys(geoCellStruct, tt, dh);
        if(portion != null && !portion.isEmpty()) {
            messageBatch.addAll(portion);
        }
        if(messageBatch.size() >= maxBatchCount) {
            if(logger.isLoggable(java.util.logging.Level.INFO)) logger.info("Fetched too many messages. Size = " + messageBatch.size());
            break;
        }
    }
*/
}



if(messageBatch == null) {
    // probably an error.
    // bail out... ????
    messageBatch = new java.util.ArrayList<String>();  // ???
}

int batchSize = messageBatch.size();
if(logger.isLoggable(java.util.logging.Level.INFO)) logger.info("batchSize = " + batchSize);


// Main loop...
if(batchSize > 0) {

// Top row <--> Most recent message..., etc...
// [old]
// for(int i=batchSize-1; i>=0; i--) {
// [new] 
for(int i=0; i<batchSize; i++) {
    String messageKey = messageBatch.get(i);
    ShortLinkJsBean shortlink = ShortUrlStreamHelper.getInstance().getShortLink(messageKey);
    if(shortlink == null) {
        // Can this happen ????
        // what to do???
        continue;
    }
    String jGuid = shortlink.getGuid();     // Could be different from messageKey...
    String jUser = "";
    String jLongUrl = "";
    String jShortUrl = "";
    String redirectType = RedirectType.getDefaultType();   // ???
    String redirectTypeToken = RedirectType.getToken(redirectType);
    String redirectTypeLabel = RedirectType.getLabel(redirectType);
    String messageShortMessage = "";
    String messageTitle = "";      // ???
    String messageViewLink = "";
    String messageVerivyLink = "";
    String messageInfoLink = "";

    ShortLinkJsBean bean = (ShortLinkJsBean) shortlink;
    jUser = bean.getOwner();
    jShortUrl = bean.getShortUrl();
    jLongUrl = bean.getLongUrl();
    redirectType = bean.getRedirectType();
    redirectTypeToken = RedirectType.getToken(redirectType);
    redirectTypeLabel = RedirectType.getLabel(redirectType);
    messageShortMessage = bean.getShortMessage();
    messageTitle = TextUtil.truncateAtWordBoundary(messageShortMessage, 30, 20, true);
    messageViewLink = "/view/" + jGuid;
    messageVerivyLink = "/verify/" + jGuid;
    messageInfoLink = "/info/" + jGuid;       // Only for their own URLs created by the authenticated users.. ????

	if(jShortUrl == null) {
	    jShortUrl = "";  // temporary
	} else {
	    // TBD: Convert yyyy-mm-dd to mm/dd ????
	    String[] ymd = jShortUrl.split("-", 3);
	    if(ymd != null && ymd.length == 3) {
	        jShortUrl = ymd[1] + "/" + ymd[2];
	    } else {
	        // ?????
	        if(jShortUrl.length() > 5 && jShortUrl.startsWith("201")) {   // 2012 or 2013, etc...
		        jShortUrl = jShortUrl.substring(5, jShortUrl.length());  // ???
	        } else {
	            // ????
	            // jShortUrl = "";
	        }
	    }
	}

	if(jUser == null) {
	    jUser = "";  // ????   Use the current sesion user, userId, ????
	}
	if(jLongUrl == null) {
	    jLongUrl = "";  // temporary
	}
    String jTruncatedLongUrl = TextUtil.truncateAtWordBoundary(jLongUrl, 80, 80, true);
    // ...

    // Escape or not escape ?????
	// String jEscapedTitle = HtmlTextUtil.escapeForHtml(messageTitle, true);
	String jEscapedTitle = HtmlTextUtil.escapeForHtml(messageTitle, false);
	//String jEscapedTitle = messageTitle;
	// TBD: truncate 
	// String jEscapedSummary = HtmlTextUtil.escapeForHtml(messageShortMessage, true);
	String jEscapedSummary = HtmlTextUtil.escapeForHtml(messageShortMessage, false);
	//String jEscapedSummary = messageShortMessage;
	// ....
    Long jCreatedT = shortlink.getCreatedTime();
    Long jModifiedT = shortlink.getModifiedTime();
    String jCreatedD = "";
    if(jCreatedT != null && jCreatedT > 0L) {
        jCreatedD = DateUtil.formatDateNoSeconds(jCreatedT);     // UTC. Will be overwritten through Javascript code below...
    }
    String jModifiedD = "";
    if(jModifiedT != null && jModifiedT > 0L) {
        jModifiedD = DateUtil.formatDateNoSeconds(jModifiedT);   // UTC. Will be overwritten through Javascript code below...
    }
%>
<tr id="row_<%=jGuid%>">
<td class="td_message_list_redirecttype" style="width:30px;">
<span title="<%=redirectTypeLabel%>"><b><%=redirectTypeToken%></b></span>
</td>
<td class="td_message_list_shorturl" style="width:40px;"><a href="<%=jShortUrl%>"><span id="shorturl_<%=jGuid%>"><%=jShortUrl%></span></a></td>
<td class="td_message_list_longurl" style="width:105px;" colspan="2"><span id="longurl_<%=jGuid%>" title="<%=jLongUrl%>"><%=jTruncatedLongUrl%></span></td>
<!-- 
<td class="td_message_list_message" colspan="2"><span id="message_<%=jGuid%>">
<%=jEscapedTitle%>
</span></td>
 -->
<!-- 
<td class="td_message_list_createdtime"><span id="createdtime_<%=jGuid%>" class="pull-right"><%=jCreatedD%></span></td>
-->

<td class="td_message_list_button">
<span class="pull-right">
<!-- 
<a class="btn btn-mini" style="width: 55px;" id="button_read_<%=jGuid%>" href="/view/<%=jGuid%>" title="Visit the Web page">View &raquo;</a>
-->
<a class="btn btn-mini" style="width: 55px;" id="button_verify_<%=jGuid%>" href="/verify/<%=jGuid%>" title="Verify the short URL">Verify &raquo;</a>
</span>
</td>
</tr>

<% 
if(jEscapedSummary != null && !jEscapedSummary.isEmpty()) {
%>
<tr id="row_message_<%=jGuid%>">
<td>&nbsp;</td>
<td class="td_message_list_message" colspan="4">
<span id="message_<%=jGuid%>"><%=jEscapedSummary%></span>    
</td>
<td>&nbsp;</td>
</tr>
<%
}
%>

<%
}
} else {
%>
<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
<%
}
%>


</tbody>
</table>

<!-- 
TBD:
"More" button???
e.g., for previous days, etc......
 -->

      </fieldset>
      </div>

      </div>

      </form>



      </div>  <!--   Hero unit  -->

      <hr>

<%@ include file="/fragment/Footer.jspf" %>

    </div> <!--! end of #container -->


    <div id="div_status_floater" style="display: none;" class="alert">
    </div>
    
    
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="http://ww2.filestoa.com/js/jquery/jquery-1.7.2.min.js"><\/script>')</script>
	<script src="http://ww2.filestoa.com/js/jquery/jquery-ui-1.8.20.custom.min.js"></script>
    <script src="http://ww2.filestoa.com/js/jquery/plugin/jquery.floatobject-1.4.js"></script>
    <script src="http://ww2.filestoa.com/js/jquery/plugin/jquery-ui-timepicker-addon.js"></script>
    <script src="http://ww2.filestoa.com/js/jquery/plugin/history.js/uncompressed/history.adapter.jquery.js"></script>
    <script src="http://ww2.filestoa.com/js/jquery/plugin/history.js/uncompressed/history.js"></script>
    <script src="http://ww2.filestoa.com/js/jquery/plugin/history.js/uncompressed/history.html4.js"></script>
    <script src="http://ww2.filestoa.com/js/bootstrap/bootstrap-2.0.js"></script>
    <script type="text/javascript" src="/js/core/debugutil.js"></script>
    <script type="text/javascript" src="http://ww2.filestoa.com/js/core/datetimeutil-1.0.js"></script>
    <script type="text/javascript" src="http://ww2.filestoa.com/js/core/emailutil-1.0.js"></script>
    <script type="text/javascript" src="/js/bean/shortlinkjsbean-1.0.js"></script>
    <script type="text/javascript" src="/js/bean/pagerstatestructjsbean-1.0.js"></script>

    <script type="text/javascript" src="/js/stream/progressbar-1.1.js"></script>
    <script type="text/javascript" src="/js/stream/basicqueue-1.1.js"></script>
    <script type="text/javascript" src="/js/stream/shorturlstreamer-1.1.js"></script>

	<script defer type="text/javascript" src="/js/plugins.js"></script>
    <script defer type="text/javascript" src="/js/script.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/fiveten-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/statushelper-1.1.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/tweethelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/signuphelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/feedbackhelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/emailhelper-1.0.js"></script>
    <!-- end scripts-->


    <script>
        // Global vars
    	var statusHelper;
    	var tweetHelper;
    	var signupHelper;
    	var feedbackHelper;
    	var emailHelper;
    </script>
    <script>
    $(function() {
    	// Init...
    	statusHelper = new webstoa.StatusHelper(400, 65);
    	// test.
    	//statusHelper.update('test status message');

    	var servicename = "<%=brandDisplayName%>";
    	var messagename = "Initial pre-launch trial";
    	var pageid = "contact";
    	signupHelper = new webstoa.SignupHelper(servicename, messagename, pageid);

    	var targetApp = "<%=brandDisplayName%>";
    	var targetPage = "messagelist";
    	feedbackHelper = new webstoa.FeedbackHelper(targetApp, targetPage);

    	var senderEmail = webstoa.EmailUtil.decodeEmailAddress('gaeemail+gmail' + '+com');
    	var defaultSenderName = "<%=brandDisplayName%>";
    	var subject = "Interesting site: <%=brandDisplayName%>";
    	var defaultMessage = "Hi,\n\n<%=brandDisplayName%>, <%=topLevelUrl%>, is a blogging helper service. Please check it out.\n\n-me\n";
    	emailHelper = new webstoa.EmailHelper(senderEmail, subject, defaultMessage, defaultSenderName);
    });
    </script>

    <script>
    // Email address "decoding"...
    $(function() {
        $("a").each(function() {
        	var anchor = $(this);
        	webstoa.EmailUtil.decodeMailtoAnchor(anchor);
        });
    });
    </script>

    <script>
    $(function() {
        $("#anchor_mainmenu_signup").click(function() {
            if(DEBUG_ENABLED) console.log("Signup button clicked."); 

       	    signupHelper.signup();
       	    //statusHelper.update('Thanks for signing up.');
		    return false;
        });
    });
    </script>

    <script>
    $(function() {
        $("#anchor_topmenu_share").click(function() {
            if(DEBUG_ENABLED) console.log("Share button clicked."); 
       	    emailHelper.email();
		    return false;
        });
    });
    </script>

    <script>
    $(function() {
        $("#anchor_feedback_footer").click(function() {
            if(DEBUG_ENABLED) console.log("Feedback button clicked."); 

            var targetItem = 'footer';
            feedbackHelper.feedback(targetItem);
		    return false;
        });
    });
    </script>


    <script>
    $("#topmenu_nav_view").click(function(e) {
        if(DEBUG_ENABLED) console.log("topmenu_nav_view anchor clicked."); 
   		e.preventDefault();
        $("#topmenu_nav_view").fadeOut(450).fadeIn(950);
        return false;
    });
    </script>


    <script>
    // Function definitions.
    function updateStatus(msg, type, time) {
    	if(!time) {
    		time = 5000;   // milliseconds.
    	}
    	if(!type) {
    		// type = "info";  // warning, error, etc....
    	}
    	//$("#div_status_floater").text(msg);
    	//$("#div_status_floater").fadeIn().delay(time).fadeOut();
		$("#div_status_floater").textInOut(msg, type, time);
    }    
    </script>

    <script>
    // temporary
    function stripHTMLTags(html) {
        if(html) {
           // return html.replace(/<(?:.|\n)*?>/gm, '');
           var tmp = document.createElement("DIV");
           tmp.innerHTML = html;
           return tmp.textContent||tmp.innerText;
        }
        return '';    // ???
    }
    </script>

    <script>
    // temporary
    function escapeForHtml(str) {
        if(str) {
        	  //if (jQuery !== undefined) {
        		    return jQuery('<div/>').text(str).html();
        		    // return jQuery('<div/>').html(str).text();  // "Unescape" ???
       		  //}
        	  //return str.replace(/&/g, '&amp;').replace(/>/g, '&gt;').replace(/</g, '&lt;').replace(/"/g, '&quot;');
        }
        return '';    // ???
    }
    </script>

    <script>
    // temporary
    function replaceURLWithHTMLLinks(text) {
        if(text) {
            var exp = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
            //var exp = /\(?\bhttp:\/\/[-A-Za-z0-9+&@#\/%?=~_()|!:,.;]*[-A-Za-z0-9+&@#/%=~_()|]/ig;
            return text.replace(exp,"<a href='$1'>$1</a>");
        }
        return '';    // ???
    }
    </script>

    <script>
    // "Init"
    $(function() {
    	// TBD: initially hide $("#div_status_floater") ...
    	$("#div_status_floater").text("").hide();
    
        // Background image test.
        //$("#container").css("background-image", 'url(/img/bg/oceansunset.jpg)');
        //$(document.body).css("background", "silver");
    	//$("#container").css("background", "ivory");

        // TBD: Location...
    	var params = {x: 530, y: 15, speed: 'normal' };
    	$("#div_status_floater").makeFloat(params);
    	
    	// temporary
    	// linkify the content
    	//var contentOriginal = $("#message_content").html();
    	//var contentLinkified = replaceURLWithHTMLLinks(contentOriginal);
    	//$("#message_content").html(contentLinkified);
    	// temporary
    	
    	// Peeng?
    	//var fiveTenTimer = new fiveten.FiveTenTimer(21889, 12);  // ???
    	//fiveTenTimer.start();
    });
    </script>

    <script>
$(function() {
    // Initialize all datetime strings.
var createdTimeId;
var createdTimeStr;  

<%
// //java.util.List<String> messages = ShortLinkListHelper.getInstance().findShortLinksForUser(targetUser, paramOffset, paramCount);
// if(messageBatch != null && messageBatch.size() > 0) {
//     for(String m : messageBatch) {
//         String jGuid = m.getGuid();   // ????
//         Long jCreatedT = m.getCreatedTime();
%>
// Temporarily commented out...
// createdTimeId = "createdtime_ -- jGuid -- ";
// createdTimeStr = getStringFromTime( -- jCreatedT -- );  
// Temporarily commented out...
// $("#" + createdTimeId).text(createdTimeStr);
// ...
<%
//     }
// }
%>
});    
    </script>


    <script>
    // App-related vars.
    var userGuid;
<%
if(userId != null) {
%>
    userGuid = "<%=userId%>";
<%
}
%>
    </script>


    <script>
    // Event handlers for the list...
    $(function() {

        $("button.button_message_list_view").live('click', function() {
            if(DEBUG_ENABLED) console.log("View message button clicked.");
            //window.alert('Not implemented yet.');
            //window.alert('target: ' + event.target.id);

            //var guid =  event.target.id.substring(12);  // Note: this does not work on IE....
            var guid =  $(this).attr('id').substring(12);  // prefix: "button_view_"
            //window.alert('guid: ' + guid);

            // TBD...
            var viewPageUrl = "/view/" + guid;
            window.location = viewPageUrl; 
        });
    	
    });
    </script>



    <script>
    // Global var
    var messageStreamer = null;
    </script>


    <script>
  $(function() {
	  $("#table_message_list_progressbar").css("width", "0%");
	  // $("#table_message_list_progress_counter").text("0");
	  $("#table_message_list_progress_counter").val("0");
      $("#table_message_list_progress_nextbutton").attr('disabled','disabled');
  });
  </script>
    <script>
    var refreshProgressBar = function(currentValue) {
        // if(DEBUG_ENABLED) console.log("refreshProgressBar() called...... currentValue = " + currentValue);
        // alert("currentValue = " + currentValue);
        
        // temporary
        var max = 100.0;
        var min = 0.0;
        if(currentValue != null) {
            // if(currentValue < min + 0.5) {
            // 	currentValue = min;
            // }
            // var percent = Math.floor(currentValue) + "%";
        	var ratio = (currentValue * 100.0 / (max - min));
            if(ratio < min + 0.5) {
            	ratio = min;
            }
            var percent = Math.floor(ratio) + "%";
	        $("#table_message_list_progressbar").css("width", percent);
	        
	        // TBD: Do this only intermittently????
	        if(messageStreamer) {
    	        var messageCount = messageStreamer.getMessageCount();
	            if(messageCount != null && messageCount >= 0) {
                    // $("#table_message_list_progress_counter").text(messageCount);
                    $("#table_message_list_progress_counter").val(messageCount);
                    if(messageCount == 0) {
	                    $("#table_message_list_progress_nextbutton").attr('disabled','disabled');
                    } else {
	                    $("#table_message_list_progress_nextbutton").removeAttr('disabled');                    	
                    }
	            }
	        }
        }        
    }
    </script>


    <script>
    var prependShortLinkRow = function(shortlinkJsBean) {
        if(DEBUG_ENABLED) console.log("prependShortLinkRow() called......");
        
        if(shortlinkJsBean) {
        	// if(DEBUG_ENABLED) console.log("shortlinkJsBean = " + shortlinkJsBean);

        	var guid = shortlinkJsBean.getGuid();
          	 // alert("TBD: Display() guid = " + guid);
            
            var redirectType = "";
            var redirectTypeToken = "";
            var redirectTypeLabel = "";
            var shortUrl = "";
            var longUrl = "";
            var truncatedLongUrl = "";
            var messageShortMessage = "";
            var messageTitle = "";
            var messageViewLink = "";
            var messageInfoLink = "";

            redirectType = shortlinkJsBean.getRedirectType();
            // temporary
            if(redirectType == 'permanent') {
            	redirectTypeToken = 'P';
            	redirectTypeLabel = 'Redirect automatically (301)';
            } else if(redirectType == 'temporary') {
            	redirectTypeToken = 'T';
            	redirectTypeLabel = 'Redirect automatically (302)';
            } else if(redirectType == 'flash') {
            	redirectTypeToken = 'F';
            	redirectTypeLabel = 'Delayed redirect (Flash)';
            } else if(redirectType == 'confirm') {
            	redirectTypeToken = 'C';
            	redirectTypeLabel = 'User confirmation required';
            } else {
            	// ????
            }
            // temporary

            shortUrl = shortlinkJsBean.getShortUrl();
            longUrl = shortlinkJsBean.getLongUrl();
            var longUrlLen = longUrl.length;
            if(longUrlLen > 80) {
            	truncatedLongUrl = longUrl.substring(0, 80 - 3) + '...';
            } else {
            	truncatedLongUrl = longUrl;
            }
            messageShortMessage = shortlinkJsBean.getShortMessage();
            messageTitle = messageShortMessage;  // ???
            messageViewLink = "/view/" + guid;
            messageVerifyLink = "/verify/" + guid;
            messageInfoLink = "/info/" + guid;

        	var escapedTitle = "";
        	if(messageTitle) {
        		// TBD: ...
        		// escapedTitle = escapeForHtml(messageTitle);  // ????
        		escapedTitle = messageTitle;
        	}
        	var escapedSummary = "";
        	if(messageShortMessage) {
        		// TBD: ...
        		// escapedSummary = escapeForHtml(messageShortMessage);  // ????
        		escapedSummary = messageShortMessage;
        	}
        	
        	if(! shortUrl) {    // if(shortUrl == null || shortUrl == undefined) ???
        		shortUrl = "";  // ???
        	} else {
        	    // TBD: Convert yyyy-mm-dd to mm/dd ????
        	    var ymd = shortUrl.split("-", 3);
        	    if(ymd != null && ymd.length == 3) {
        	    	shortUrl = ymd[1] + "/" + ymd[2];
        	    } else {
        	        // ?????
        	        if(shortUrl.length > 5 && shortUrl.substring(0,3) == '201') {
	        	        shortUrl = shortUrl.substring(5, shortUrl.length);
        	        } else {
        	        	// ???
        	        }
        	    }
        	}
        	if(! longUrl) {
        		longUrl = "";  // ???
        	}
          	 
          	 var newMessageMetaRow = '<tr id="row_' + guid + '">'
	             + '<td class="td_message_list_redirecttype" style="width:32px;">'
	             + '<span id="redirecttype_' + guid + '" title="' + redirectTypeLabel + '"><b>' + redirectTypeToken;
	         newMessageMetaRow += '</b></span></td>'
        	     + '<td class="td_message_list_shorturl" style="width:70px;"><a href="' + shortUrl + ' "><span id="shorturl_' + guid + '">' + shortUrl + '</span></a></td>'
            	 + '<td class="td_message_list_longurl" style="width:105px;" colspan="2"><span id="longurl_' + guid + '" title="' + longUrl + '">' + truncatedLongUrl + '</span></td>';
    	     // newMessageMetaRow += '<td class="td_message_list_message" colspan="2"><span id="message_' + guid + '">';
    	     // newMessageMetaRow += escapedTitle;
    	     // newMessageMetaRow += '</span></td>';
            	 // + '<td class="td_message_list_createdtime"><span id="createdtime_' + guid + '" class="pull-right">&nbsp;</span></td>'
     	     newMessageMetaRow += '<td class="td_message_list_button">';
     	     newMessageMetaRow += '<span class="pull-right">'
       	     // newMessageMetaRow += '<a class="btn btn-mini" style="width: 55px;" id="button_read_' + guid + '" href="' + messageViewLink + '" title="Visit the web page">View &raquo;</a>';
       	     newMessageMetaRow += '<a class="btn btn-mini" style="width: 55px;" id="button_read_' + guid + '" href="' + messageVerifyLink + '" title="Verify the short URL">Verify &raquo;</a>';
     	     newMessageMetaRow += '</span>';
     	     newMessageMetaRow +=  '</td></tr>';
          	 
     	     var newMessageContentRow;
     	     if(escapedSummary) {
	             newMessageContentRow = '<tr id="row_message_' + guid + '">'
	                 + '<td>&nbsp;</td>'
	                 + '<td class="td_message_list_message" colspan="4">'
	                 + '<span id="message_' + guid + '">' + escapedSummary + '</span>'
	                 + '</td><td>&nbsp;</td></tr>';
     	     }          	 
             
     	     if(newMessageContentRow) {
              	 //$('#table_message_list_records tbody tr:first').before(newMessageContentRow);
                 $('#table_message_list_records > tbody').prepend(newMessageContentRow);
     	     }
          	 //$('#table_message_list_records tbody tr:first').before(newMessageMetaRow);
             $('#table_message_list_records > tbody').prepend(newMessageMetaRow);

        } else {
        	// Ignore...
            if(DEBUG_ENABLED) console.log("shortlinkJsBean is null.");    	 
        }

    }
    </script>


    <script>
  $(function() {
	  // ...
	  // Note that the progressbar.duration should be == ShortUrlStreamer.displayMillis.
	  var progressBar = new webstoa.stream.ProgressBar(7500, 150, 100.0, 0.0);
	  progressBar.setDisplayCallback(refreshProgressBar);
	  // ...
	  var streamerStartTime = <%=streamerStartTime%>;
	  var geoCellStruct;
<%
if(geoCellStruct != null) {
    Integer gcScale = geoCellStruct.getScale();
    Integer gcLatitude = geoCellStruct.getLatitude();
    Integer gcLongitude = geoCellStruct.getLongitude();
    if(gcScale != null && gcLatitude != null && gcLongitude != null) {   // ????
%>
      geoCellStruct = {};
      geoCellStruct.scale = <%=gcScale%>;
      geoCellStruct.latitude = <%=gcLatitude%>;
      geoCellStruct.longitude = <%=gcLongitude%>;
<%
    }
}
%>
      // Note that the progressbar.duration should be == ShortUrlStreamer.displayMillis.
	  messageStreamer = new cannyurl.stream.ShortUrlStreamer(geoCellStruct, "tenmins", streamerStartTime, 60000, 7500);
	  messageStreamer.setDisplayCallback(prependShortLinkRow);
	  messageStreamer.setProgressBar(progressBar);
	  messageStreamer.start();
  });
    </script>

  <script>
  $("#table_message_list_progress_nextbutton").click(function(e) {
      if(DEBUG_ENABLED) console.log("table_message_list_progress_nextbutton anchor clicked."); 
      if(messageStreamer && messageStreamer.getMessageCount() > 0) {
          $("#table_message_list_progress_nextbutton").fadeOut(450).fadeIn(950);
          messageStreamer.displayNow();
      } else {
          if(DEBUG_ENABLED) console.log("No messages in the queue......");
     	  e.preventDefault();    	  
          $("#table_message_list_progress_nextbutton").fadeOut(450).fadeIn(950);
      }      
      return false;
  });
  </script>

  <script>
  // TBD:
  // Global state var
  var autoProgressing = true;
  var setPauseButtonLabel = function(isAutoProgressing) {
	  if(isAutoProgressing) {
		  // $("#table_message_list_progress_pausebutton").text("Pause");
		  $("#table_message_list_progress_pausebutton").html('<i class="icon-pause"></i>');
	  } else {
		  // $("#table_message_list_progress_pausebutton").text("Start");		  
		  $("#table_message_list_progress_pausebutton").html('<i class="icon-play"></i>');		  
	  }
  }
  setPauseButtonLabel(autoProgressing);
  
  $("#table_message_list_progress_pausebutton").click(function(e) {
      if(DEBUG_ENABLED) console.log("table_message_list_progress_pausebutton anchor clicked."); 
      if(messageStreamer) {
	      if(autoProgressing) {
	          $("#table_message_list_progress_pausebutton").fadeOut(450).fadeIn(950);
	          messageStreamer.pause();
	          autoProgressing = false;
	      } else {
	          $("#table_message_list_progress_pausebutton").fadeOut(450).fadeIn(950);
	          messageStreamer.restart();
	          autoProgressing = true;
	      }
	      setPauseButtonLabel(autoProgressing);
      } else {
    	  // ???
      }
      return false;
  });
    </script>

    <script>
  $(function() {
	  // ...
  });
    </script>



<%
String googleAnalyticsCode = AnalyticsHelper.getInstance().getGoogleAnalyticsCode();
if(googleAnalyticsCode != null && !googleAnalyticsCode.isEmpty()) {    // And, if it is enabled (e.g. through config). TBD...
%>
    <!-- Change UA-XXXXX-X to be your site's ID -->
    <script>
      window._gaq = [['_setAccount','<%=googleAnalyticsCode%>'],['_trackPageview'],['_trackPageLoadTime']];
      Modernizr.load({
        load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
      });
    </script>
<%
}
%>

    <!-- Prompt IE 6 users to install Chrome Frame. Remove this if you want to support IE 6.
       chromium.org/developers/how-tos/chrome-frame-getting-started -->
    <!--[if lt IE 7 ]>
      <script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
      <script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
    <![endif]-->

    </body>
</html>