<%@ page import="com.myurldb.ws.core.*, com.myurldb.ws.*, com.cannyurl.af.util.*, com.cannyurl.af.auth.common.*, com.cannyurl.af.auth.*, com.cannyurl.af.auth.user.*, com.cannyurl.fe.*, com.cannyurl.fe.core.*, com.cannyurl.fe.bean.*, com.cannyurl.fe.util.*, com.cannyurl.wa.service.*, com.cannyurl.common.*, com.cannyurl.util.*, com.cannyurl.app.auth.*, com.cannyurl.app.auth.filter.*, com.cannyurl.app.util.*, com.cannyurl.helper.*" 
%><%@ page contentType="text/html; charset=UTF-8" 
%><%@ page errorPage="/error/UnknownError" 
%><%!
private static final java.util.logging.Logger logger = java.util.logging.Logger.getLogger("SiteLogView");
%><%
boolean isAppAuthDisabled = ConfigUtil.isApplicationAuthDisabled();
// boolean isAppAuthOptional = ConfigUtil.isApplicationAuthOptional();
String appAuthMode = ConfigUtil.getApplicationAuthMode();
boolean isUsingTwitterAuth = false;
if(AppAuthMode.MODE_TWITTER.equals(appAuthMode)) {
    isUsingTwitterAuth = true;
}
%><%
// [1] Session
SessionBean sessionBean = UserSessionManager.getInstance().setSessionBean(request, response);
String sessionToken = sessionBean.getToken();
String userId = sessionBean.getUserId();

// [2] Parse url.
String requestURI = request.getRequestURI();
String requestUrl = request.getRequestURL().toString();
String topLevelUrl = UrlHelper.getInstance().getTopLevelURLFromRequestURL(requestUrl);
String contextPath = request.getContextPath();
String servletPath = request.getServletPath();
String pathInfo = request.getPathInfo();
String queryString = request.getQueryString();
//...
%><%
// "Branding"
String appBrand = BrandingHelper.getInstance().getAppBrand();
String brandDisplayName = BrandingHelper.getInstance().getBrandDisplayName();
String brandDescription = BrandingHelper.getInstance().getBrandDescription();
%><%String themeBootstrap = SiteThemeHelper.getInstance().getBootstrapTheme();
String themeJqueryUi = SiteThemeHelper.getInstance().getJqueryUiTheme();%><%
boolean isUserAuthenticated = false;
String userUsername = null;
String loginUrl = null;
String logoutUrl = null;
RequestAuthStateBean authStateBean = (RequestAuthStateBean) request.getAttribute(AuthUtil.REQUEST_ATTR_AUTHSTATEBEAN);
if(authStateBean == null) {
    // Filter might not have applied (because of the /* mapping, etc...) 
    // Try one more time ???
    authStateBean = AuthFilterUtil.createAuthStateBean(request, response);
    if(authStateBean != null) {
        // Add it to the current request object....
        request.setAttribute(AuthUtil.REQUEST_ATTR_AUTHSTATEBEAN, authStateBean);
    }
}
if(authStateBean != null) {
    isUserAuthenticated = authStateBean.isAuthenticated();
    if(isUserAuthenticated) {
        logoutUrl = authStateBean.getLogoutUrl();
        userUsername = authStateBean.getUsername();
    } else {
        loginUrl = authStateBean.getLoginUrl();
    }   
}
%><%
String twitterHandle = "";
String twitterUserProfileUrl = "";
if(isUsingTwitterAuth) {
	if(userUsername != null && !userUsername.isEmpty()) {
	    twitterHandle = "@" + userUsername;      // ???
	    twitterUserProfileUrl = "http://twitter.com/" + userUsername;
	}
}
%><%
if(isUserAuthenticated == false) {
    // ????
    // response.sendRedirect(loginUrl);
}
%><%
// ...
String codeForSiteLog = SiteLogHelper.getInstance().getCodeForSiteLog(pathInfo);
// ...
String pageTitle = "Site Blog";
if(codeForSiteLog != null) {
    pageTitle += " - " + codeForSiteLog;
}
%><!DOCTYPE html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!-- Consider adding an manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><%=brandDisplayName%> | <%=pageTitle%></title>
    <meta name="description" content="<%=AppBrand.getBrandDescription(appBrand)%>. It also provides free URL Shortener Web Services API." />

    <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Mobile viewport optimized: j.mp/bplateviewport -->
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <!-- CSS: implied media=all -->
    <%@ include file="/fragment/style.jspf" %>
    <!-- end CSS-->

    <!-- Le fav and touch icons -->
	<link rel="shortcut icon" href="/img/favicon-<%=appBrand%>.ico" />
	<link rel="icon" href="/img/favicon-<%=appBrand%>.ico" type="image/x-icon" />

    <!-- All JavaScript at the bottom, except for Modernizr / Respond.
       Modernizr enables HTML5 elements & feature detects; Respond is a polyfill for min/max-width CSS3 Media Queries
       For optimal performance, use a custom Modernizr build: www.modernizr.com/download/ -->
    <script src="http://ww2.filestoa.com/js/modernizr/modernizr-2.0.6.min.js"></script>

   </head>

  <body>

<jsp:include page="/fragment/TopNavbar">
<jsp:param name="pageCode" value="sitelog"/>
<jsp:param name="brandDisplayName" value="<%=brandDisplayName%>"/>
</jsp:include>

    <div class="container-fluid">

       <div class="hero-unit">


<%
if(codeForSiteLog != null) {
    SiteLogJsBean slBean = SiteLogHelper.getInstance().getSiteLog(codeForSiteLog, appBrand);
	if(slBean != null) {
        String jTitle = slBean.getTitle();
        if(jTitle == null) {
            jTitle = "";   // ???
        }
        String jPubDate = slBean.getPubDate();
        String jFormat = slBean.getFormat();   // Ignored, for now...
        String jContent = slBean.getContent();
        if(jContent == null) {
            jContent = "";   // ???
        }
        
        // temporary
        String defaultTweetMessage = TextUtil.truncateAtWordBoundary(jTitle, 80, 60, true);
        // ...
%>


<div id="div_share_buttons" style="margin-top:-20px;">

<span class="fb-like pull-right" data-send="false" data-layout="button_count" data-width="450" data-show-faces="false" data-font="arial"></span>

<span class="pull-right">
<span class="g-plusone" data-size="medium" data-annotation="none"></span>
</span>

<span class="pull-right blogarticle_view_information_shortlink">
<a href="https://twitter.com/share" class="twitter-share-button" accesskey="t" 
    data-url="<%=requestUrl%>" 
    data-counturl="<%=requestUrl%>" 
    data-text="<%=defaultTweetMessage%>"
    data-count="none"
    data-lang="en">Tweet</a>
</span>

<span class="pull-right">
<script type="IN/Share"></script>
</span>

</div>



         <h1><%=jTitle%>
<% if(jPubDate != null && !jPubDate.isEmpty()) { %>
          <small> - <%=jPubDate%></small>
<% } %>
        </h1>
        <p>
        <%=jContent%>
        </p>

        <p><a class="btn btn-tiny" href="/blog" title="Back to the list">Blog &raquo;</a></p>

<%
	} else {
		// 404 page
%>
<jsp:forward page="/error/Default404" >
  <jsp:param name="RequestURL" value="<%=requestUrl%>" />
</jsp:forward>
<%
	}
%>
       </div>   <!--   Hero unit -->
<%
}
%>



      <hr>

<%@ include file="/fragment/Footer.jspf" %>

    </div> <!-- /container -->


    <!--  Tweet Button  -->
    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

	<!--  Facebook Like  -->
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
	
	<!--  LinkedIn Share  -->
	<script src="//platform.linkedin.com/in.js" type="text/javascript"></script>
	
	<!-- Google+ - Place this tag after the last +1 button tag. ???  -->
	<script type="text/javascript">
	  (function() {
	    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
	    po.src = 'https://apis.google.com/js/plusone.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
	  })();
	</script>


    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="http://ww2.filestoa.com/js/jquery/jquery-1.8.3.min.js"><\/script>')</script>
	<script src="http://ww2.filestoa.com/js/jquery/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="http://ww2.filestoa.com/js/jquery/plugin/jquery.floatobject-1.4.js"></script>
    <script src="http://ww2.filestoa.com/js/bootstrap/bootstrap-2.0.js"></script>
    <script type="text/javascript" src="/js/core/debugutil.js"></script>
	<script src="/js/plugins.js"></script>
    <script src="/js/script.js"></script>
    <script type="text/javascript" src="http://ww2.filestoa.com/js/core/datetimeutil-1.0.js"></script>
    <script type="text/javascript" src="http://ww2.filestoa.com/js/core/emailutil-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/fiveten-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/statushelper-1.1.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/tweethelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/signuphelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/feedbackhelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/emailhelper-1.0.js"></script>


    <script>
        // Global vars
    	var statusHelper;
    	var tweetHelper;
    	var signupHelper;
    	var feedbackHelper;
    	var emailHelper;
    </script>
    <script>
    $(function() {
    	// Init...
    	statusHelper = new webstoa.StatusHelper(400, 65);
    	// test.
    	//statusHelper.update('test status message');

    	var servicename = "<%=brandDisplayName%>";
    	var launchname = "Initial pre-launch trial";
    	var pageid = "sitelog";
    	signupHelper = new webstoa.SignupHelper(servicename, launchname, pageid);

    	var targetService = "<%=brandDisplayName%>";
    	var targetPage = "sitelog";
    	feedbackHelper = new webstoa.FeedbackHelper(targetService, targetPage);

    	var senderEmail = webstoa.EmailUtil.decodeEmailAddress('gaeemail+gmail' + '+com');
    	var defaultSenderName = "<%=brandDisplayName%>";
    	var subject = "Interesting site: <%=brandDisplayName%>";
    	var defaultMessage = "Hi,\n\n<%=brandDisplayName%>, <%=topLevelUrl%>, is a new kind of tech news digest service. Please check it out.\n\n-me\n";
    	emailHelper = new webstoa.EmailHelper(senderEmail, subject, defaultMessage, defaultSenderName);
    });
    </script>

    <script>
    // Email address "decoding"...
    $(function() {
        $("a").each(function() {
        	var anchor = $(this);
        	webstoa.EmailUtil.decodeMailtoAnchor(anchor);
        });
    });
    </script>

    <script>
    $(function() {
        $("#anchor_topmenu_share").click(function() {
            if(DEBUG_ENABLED) console.log("Share button clicked."); 
       	    emailHelper.email();
		    return false;
        });
    });
    </script>

    <script>
    $(function() {
        $("#anchor_feedback_footer").click(function() {
            if(DEBUG_ENABLED) console.log("Feedback button clicked."); 

            var targetItem = 'footer';
            feedbackHelper.feedback(targetItem);
		    return false;
        });
    });
    </script>



    <script>
    $("#topmenu_nav_view").click(function(e) {
        if(DEBUG_ENABLED) console.log("topmenu_nav_view anchor clicked."); 
   		e.preventDefault();
        $("#topmenu_nav_view").fadeOut(450).fadeIn(950);
        return false;
    });
    </script>



    <!--  Google Analytics  -->
<%
String googleAnalyticsCode = AnalyticsHelper.getInstance().getGoogleAnalyticsCode();
if(googleAnalyticsCode != null && !googleAnalyticsCode.isEmpty()) {    // And, if it is enabled (e.g. through config). TBD...
%>
    <!-- Change UA-XXXXX-X to be your site's ID -->
    <script>
      window._gaq = [['_setAccount','<%=googleAnalyticsCode%>'],['_trackPageview'],['_trackPageLoadTime']];
      Modernizr.load({
        load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
      });
    </script>
<%
}
%>

  </body>
</html>
