<%@ page import="com.myurldb.ws.core.*, com.cannyurl.af.util.*, com.cannyurl.af.auth.*, com.cannyurl.af.auth.user.*, com.cannyurl.af.search.*, com.cannyurl.fe.*, com.cannyurl.fe.core.*, com.cannyurl.fe.bean.*, com.cannyurl.fe.util.*, com.cannyurl.wa.service.*, com.cannyurl.common.*, com.cannyurl.util.*,  com.cannyurl.app.util.*, com.cannyurl.helper.*" 
%><%@ page contentType="text/html; charset=UTF-8" 
%><%@ page errorPage="/error/UnknownError" 
%><%!
private static final java.util.logging.Logger logger = java.util.logging.Logger.getLogger("TestConsole");
%><%
// {1] Session
SessionBean sessionBean = UserSessionManager.getInstance().setSessionBean(request, response);
String sessionToken = sessionBean.getToken();
String userId = sessionBean.getUserId();

// [2] Parse url.
String requestURI = request.getRequestURI();
String requestUrl = request.getRequestURL().toString();
String topLevelUrl = UrlHelper.getInstance().getTopLevelURLFromRequestURL(requestUrl);
String contextPath = request.getContextPath();
String servletPath = request.getServletPath();
String pathInfo = request.getPathInfo();
String queryString = request.getQueryString();
//...
%><%
// "Branding"
String appBrand = BrandingHelper.getInstance().getAppBrand();
String brandDisplayName = BrandingHelper.getInstance().getBrandDisplayName();
String brandDescription = BrandingHelper.getInstance().getBrandDescription();
%><%
String themeBootstrap = SiteThemeHelper.getInstance().getBootstrapTheme();
String themeJqueryUi = SiteThemeHelper.getInstance().getJqueryUiTheme();
%><%
// ...
%><!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title><%=brandDisplayName%> | Test Console</title> 

    <!-- CSS: implied media=all -->
    <%@ include file="/fragment/style.jspf" %>
    <link rel="stylesheet" type="text/css" href="http://ww2.filestoa.com/css/openid/openid.css" />
    <!-- end CSS-->

    <!-- Le fav and touch icons -->
	<link rel="shortcut icon" href="/img/favicon-<%=appBrand%>.ico" />
	<link rel="icon" href="/img/favicon-<%=appBrand%>.ico" type="image/x-icon" />

  </head>

  <body>
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
           <a class="brand" href="/home"><%=brandDisplayName%> (beta)</a>
          <div class="nav-collapse">
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container-fluid">

      <!-- Main hero unit for a primary marketing message or call to action -->
      <div id="main" class="hero-unit">
        <h2>Test Page</h2>

<div>
<form class="form-horizontal" method="post" action="" accept-charset="UTF-8">

  <div class="control-group">
    <div class="controls">
      <button type="submit" class="btn" id="submit_status_message">Send</button>
    </div>
  </div>

<div class="control-group">
<label for="status_message" class="control-label">Status Message</label>
<div class="controls">
<textarea id="status_message" name="status_message" style="width:400px;height:80px;">
</textarea>
</div>
</div>


<div class="control-group">
<label for="status_message_result" class="control-label">Result</label>
<div class="controls">
<textarea id="status_message_result" name="status_message_result" style="width:400px;height:80px;">
</textarea>
</div>
</div>


</form>


</div>

      </div>

      <hr>

      <footer>
        <p>&copy; <%=brandDisplayName%> 2012</p>
      </footer>
    </div> <!-- /container -->


    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="http://ww4.filestoa.com/js/jquery/jquery-1.8.3.min.js"><\/script>')</script>
	<script src="http://ww4.filestoa.com/js/jquery/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="/js/core/debugutil.js"></script>
    <script type="text/javascript" src="/js/bean/statusmessagejsbean-1.0.js"></script>

    <script>
    // Ajax form handlers.
  $(function() {
    $("#submit_status_message").click(function() {

        // Payload...
        var statusMessageJsBean = new cannyurl.wa.bean.StatusMessageJsBean();
        statusMessageJsBean.setPostingType("appPosting");

        // testing
        // statusMessageJsBean.setDeferred(false);
        statusMessageJsBean.setDeferred(true);
        
        var message = $("#status_message").val();
        statusMessageJsBean.setOriginalMessage(message);
        var statusMessageJsonStr = JSON.stringify(statusMessageJsBean);
        if(DEBUG_ENABLED) console.log("statusMessageJsonStr = " + statusMessageJsonStr);
        var payload = "{\"statusMessage\":" + statusMessageJsonStr;
        // TBD...
        payload += "}";
        if(DEBUG_ENABLED) console.log("payload = " + payload);

        $.ajax({
    	    type: "POST",
    	    url: "/ajax/statusmessage",
    	    data: payload,
    	    dataType: "json",
    	    contentType: "application/json; charset=UTF-8",
    	    success: function(data, textStatus) {
    	      // TBD
    	      if(DEBUG_ENABLED) console.log("statusMessageJsBean successfully saved. textStatus = " + textStatus);
    	      if(DEBUG_ENABLED) console.log("data = " + data);
    	      if(DEBUG_ENABLED) console.log(data);

    	      // Parse data...
    	      if(data) {   // POST only..
	    	      // Replace/Update the JsBeans ...

	    	      var statusMessageObj = data["statusMessage"];
	    	      if(DEBUG_ENABLED) console.log("statusMessageObj = " + statusMessageObj);
	    	      if(DEBUG_ENABLED) console.log(statusMessageObj);
	    	      
	    	      $("#status_message_result").html(statusMessageObj);

    	      } else {
    	    	  // ignore
    	      } 
    	    },
    	    error: function(req, textStatus) {
          	    if(DEBUG_ENABLED) console.log('Failed to save the statusMessage. status = ' + textStatus);
    	    }
	   	  });
    	
        return false;
    });
  });
    </script>


  </body>
</html>

