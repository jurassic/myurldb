<%@ page import="com.myurldb.ws.core.*, com.cannyurl.af.util.*, com.cannyurl.af.auth.common.*, com.cannyurl.af.auth.*, com.cannyurl.af.auth.user.*, com.cannyurl.af.search.*, com.cannyurl.fe.*, com.cannyurl.fe.core.*, com.cannyurl.fe.bean.*, com.cannyurl.fe.util.*, com.cannyurl.wa.service.*, com.cannyurl.common.*, com.cannyurl.util.*, com.cannyurl.app.auth.*, com.cannyurl.app.util.*, com.cannyurl.helper.*" 
%><%@ page contentType="text/html; charset=UTF-8" 
%><%@ page errorPage="/error/UnknownError" 
%><%!
private static final java.util.logging.Logger logger = java.util.logging.Logger.getLogger("TwitterLogout");
%><%
// {1] Session
SessionBean sessionBean = UserSessionManager.getInstance().setSessionBean(request, response);
String sessionToken = sessionBean.getToken();
String userId = sessionBean.getUserId();

// [2] Parse url.
String requestURI = request.getRequestURI();
String requestUrl = request.getRequestURL().toString();
String topLevelUrl = UrlHelper.getInstance().getTopLevelURLFromRequestURL(requestUrl);
String contextPath = request.getContextPath();
String servletPath = request.getServletPath();
String pathInfo = request.getPathInfo();
String queryString = request.getQueryString();
String referer = request.getHeader("referer");
//...
%><%
// "Branding"
String appBrand = BrandingHelper.getInstance().getAppBrand();
String brandDisplayName = BrandingHelper.getInstance().getBrandDisplayName();
String brandDescription = BrandingHelper.getInstance().getBrandDescription();
%><%String themeBootstrap = SiteThemeHelper.getInstance().getBootstrapTheme();
String themeJqueryUi = SiteThemeHelper.getInstance().getJqueryUiTheme();%><%
AuthUserService authUserService = AuthUserServiceFactory.getAuthUserService();
AuthUser authUser = authUserService.getCurrentUser(); // or req.getUserPrincipal()
// ...
%><%
// 
String comebackUrl = request.getParameter(CommonAuthUtil.PARAM_COMEBACKURL);
if(comebackUrl == null || comebackUrl.isEmpty()) {
    if(referer != null && !referer.isEmpty()) {
        comebackUrl = referer;  // ???
    } else {
        // ???
        comebackUrl = "/home";
    }
}
// String encodedComebackUrl = java.net.URLEncoder.encode(comebackUrl, "UTF-8");
//...
%><%
// 
String fallbackUrl = request.getParameter(CommonAuthUtil.PARAM_FALLBACKURL);
if(fallbackUrl == null || fallbackUrl.isEmpty()) {
    if(referer != null && !referer.isEmpty()) {
        fallbackUrl = referer;  // ???
    } else {
        // ???
        fallbackUrl = "/home";
    }
}
// String encodedFallbackUrl = java.net.URLEncoder.encode(fallbackUrl, "UTF-8");
//...

%><%
String providerId = CommonAuthUtil.AUTH_SERVICE_PROVIDERID_TWITTER;
boolean isUserAuthenticated = AuthManager.getInstance().isUserLoggedIn(session, providerId);
if(isUserAuthenticated) {
    // TBD: 
    //     logout
    boolean successfullyProcessed = AuthManager.getInstance().processUserLogOut(session, providerId);
    if(logger.isLoggable(java.util.logging.Level.FINE)) logger.fine("successfullyProcessed = " + successfullyProcessed);

    if(successfullyProcessed) {

        // Log out from Twitter ???
        // Is that necessary ???
        // Maybe, just clear the Twitter cookie ???
        // ????
                
        // What about the main app cookie (associated with user.guid) ???
        // Should we clear it ????
        // ....
        
        // Redirect to camebackurl...
        response.sendRedirect(comebackUrl);
        // ...
    } else {
        // Redirect to fallbackUrl... ???
        response.sendRedirect(fallbackUrl);
        // ...
    }
} else {
    // TBD:
    // Redirect to fallbackUrl... ???
    response.sendRedirect(fallbackUrl);
    // ...
}
// ...
%><!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Sign out</title> 

    <!-- Le styles -->
    <%@ include file="/fragment/style.jspf" %>
    <link rel="stylesheet" type="text/css" href="http://ww4.filestoa.com/css/openid/openid.css" />

    <!-- Le fav and touch icons -->
	<link rel="shortcut icon" href="/img/favicon-<%=appBrand%>.ico" />
	<link rel="icon" href="/img/favicon-<%=appBrand%>.ico" type="image/x-icon" />

    </head>

  <body>
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
           <a class="brand" href="/home"><%=brandDisplayName%> (beta)</a>
          <div class="nav-collapse">
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container-fluid">

      <!-- Main hero unit for a primary marketing message or call to action -->
      <div id="main" class="hero-unit">
        <h2>Sign In with Twitter</h2>

<div>
<% if(authUser == null) { %>
authUser is null.
<% } else { %>
authUser is not null...
<% } %>
</div>

<div>
<% if(isUserAuthenticated) { %>
User is authenticated.
<% } else { %>
User is not authenticated.
<% } %>
</div>


<div>

<a href="/auth/manager?openid_identifier=twitter&comebackUrl=%2Flogin&fallbackUrl=%2Flogin"><img src="/img/twitter/sign-in-with-twitter-link.png"></img></a>

</div>

      </div>

      <hr>

      <footer>
        <p>&copy; <%=brandDisplayName%> 2013</p>
      </footer>
    </div> <!-- /container -->

  </body>
</html>

