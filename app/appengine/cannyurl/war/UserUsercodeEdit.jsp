<%@ page import="com.myurldb.ws.core.*, com.myurldb.ws.*, com.cannyurl.af.util.*, com.cannyurl.af.auth.common.*, com.cannyurl.af.auth.*, com.cannyurl.af.auth.googleapps.*, com.cannyurl.af.auth.user.*, com.cannyurl.fe.*, com.cannyurl.fe.core.*, com.cannyurl.fe.bean.*, com.cannyurl.fe.util.*, com.cannyurl.wa.service.*, com.cannyurl.common.*, com.cannyurl.util.*, com.cannyurl.app.auth.*, com.cannyurl.app.util.*, com.cannyurl.helper.*" 
%><%@ page contentType="text/html; charset=UTF-8" 
%><%@ page errorPage="/error/UnknownError" 
%><%!
private static final java.util.logging.Logger logger = java.util.logging.Logger.getLogger("UserUsercodeSetup");
%><%
boolean isAppAuthDisabled = ConfigUtil.isApplicationAuthDisabled();
boolean isAppAuthOptional = ConfigUtil.isApplicationAuthOptional();
String appAuthMode = ConfigUtil.getApplicationAuthMode();
boolean isUsingTwitterAuth = false;
if(AppAuthMode.MODE_TWITTER.equals(appAuthMode)) {
    isUsingTwitterAuth = true;
}
%><%
// {1] Session
SessionBean sessionBean = UserSessionManager.getInstance().setSessionBean(request, response);
String sessionToken = sessionBean.getToken();
String userId = sessionBean.getUserId();

// [2] Parse url.
String contextPath = request.getContextPath();
String servletPath = request.getServletPath();
String requestHost = request.getServerName();
//String requestURI = request.getRequestURI();
String requestUrl = request.getRequestURL().toString();
String topLevelUrl = UrlHelper.getInstance().getTopLevelURLFromRequestURL(requestUrl);
String pathInfo = request.getPathInfo();
String queryString = request.getQueryString();
%><%

// temporary
String PARAM_APPSDOMAIN = GoogleAppsAuthUtil.PARAM_APPSDOMAIN;
String paramAppsDomain = request.getParameter(PARAM_APPSDOMAIN);

String requestAppsDomain = null;
if(paramAppsDomain != null && !paramAppsDomain.isEmpty()) {
    requestAppsDomain = paramAppsDomain;
}
// TBD...

%><%
// "Branding"
String appBrand = BrandingHelper.getInstance().getAppBrand();
String brandDisplayName = BrandingHelper.getInstance().getBrandDisplayName();
String brandDescription = BrandingHelper.getInstance().getBrandDescription();
%><%
String themeBootstrap = SiteThemeHelper.getInstance().getBootstrapTheme();
String themeJqueryUi = SiteThemeHelper.getInstance().getJqueryUiTheme();
%><%
boolean isUserAuthenticated = false;
String userUsername = null;
String loginUrl = null;
String logoutUrl = null;
RequestAuthStateBean authStateBean = (RequestAuthStateBean) request.getAttribute(AuthUtil.REQUEST_ATTR_AUTHSTATEBEAN);
if(authStateBean != null) {
    isUserAuthenticated = authStateBean.isAuthenticated();
    if(isUserAuthenticated) {
        logoutUrl = authStateBean.getLogoutUrl();
        userUsername = authStateBean.getUsername();
    } else {
        loginUrl = authStateBean.getLoginUrl();
    }   
}
%><%
String twitterHandle = "";
String twitterUserProfileUrl = "";
if(isUsingTwitterAuth) {
	if(userUsername != null && !userUsername.isEmpty()) {
	    twitterHandle = "@" + userUsername;      // ???
	    twitterUserProfileUrl = "http://twitter.com/" + userUsername;
	}
}
%><%
if(isUserAuthenticated == false) {
    // ????
    // response.sendRedirect(loginUrl);
}
%><%
// [3] Local "global" variables.
UserUsercodeJsBean userUsercodeBean = null;
String userUsercodeUsername = null;  // != requestUrl, in general. (requestUrl may include the "path" compoennt)...
String userUsercode = null;
//String requestUrlRedirectType = null;


// [4] Parse the reuqest URL.
// Find userUsercodeBean and requestUrlRedirectType, if any.
boolean requiresRefresh = false;   // Redirect to the "canonical" url, if necessary...
if(servletPath != null && servletPath.length() > 2) {   // ???

    String guid = UrlHelper.getInstance().getGuidFromPathInfo(pathInfo);
	if(guid != null && !guid.isEmpty()) {  // TBD: validation??
        try {
	        userUsercodeBean = new UserUsercodeWebService().getUserUsercode(guid);
        } catch (Exception e) {
            // Ignore...
        }
        // Heck!!! In case of delay in saving a newly crated bean, try one more time ????
        if(userUsercodeBean == null) {
             try {
    	        userUsercodeBean = new UserUsercodeWebService().getUserUsercode(guid);
		    } catch (Exception e) {
		        // Ignore...
		    }
        }

	} else {
	    // ????
	    // ....
	}

} else {

    // TBD:
    // Get clientId and/or usercode from pathInfo/query param, and then
    // get userUsercode using clientId, usercode, etc. ????
    // e.g., userUsercodeBean = UserUsercodeHelper.getInstance().findUserUsercode(clientId);
    // ...

}

// ???
if(userUsercodeUsername == null || userUsercodeUsername.isEmpty()) {
	if(userUsercodeBean != null) {
	    userUsercodeUsername = userUsercodeBean.getUsername();  
	}
}
if(userUsercode == null || userUsercode.isEmpty()) {
	if(userUsercodeBean != null) {
	    userUsercode = userUsercodeBean.getUsercode();
	}
}
%><%

String userUsercodeGuid = null;
if(userUsercodeBean != null) {
    userUsercodeGuid = userUsercodeBean.getGuid();
}
String userUsercodeDomain = "";
if(userUsercode != null) {
    // TBD:
    userUsercodeDomain = DomainUtil.buildDefaultUsercodeBasedDomain(userUsercode);
    // ...
}
// ...
%><%

String userUsercodeUser = null;
String userUsercodeAdmin = null;
if(userUsercodeBean != null) {
    userUsercodeUser = userUsercodeBean.getUser();
    userUsercodeAdmin = userUsercodeBean.getAdmin();
}

%><%

String userUsercodePageTitle = brandDisplayName + " | Client Setup";
String userUsercodeDescription = AppBrand.getBrandDescription(appBrand);

%><%
// ....
%><%
// TBD: Update the AccessRecord...
// ??? Should we do it a bit earlier (e.g., before error redictring, or even before permalink redirect ????)
// ???
// Note: we can use a separate accessRecord service...
// ...
%><!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!-- Consider adding an manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<html>
  <head>
    <meta charset="utf-8">
    <title><%=userUsercodePageTitle%></title>
    <meta name="description" content="<%=userUsercodeDescription%>">

    <!-- Mobile viewport optimized: j.mp/bplateviewport -->
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <link rel="shortcut icon" href="/img/favicon-<%=appBrand%>.ico" />
    <link rel="icon" href="/img/favicon-<%=appBrand%>.ico" type="image/x-icon" />

    <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- CSS: implied media=all -->
    <%@ include file="/fragment/style.jspf" %>
    <!-- end CSS-->

    <!-- All JavaScript at the bottom, except for Modernizr / Respond.
       Modernizr enables HTML5 elements & feature detects; Respond is a polyfill for min/max-width CSS3 Media Queries
       For optimal performance, use a custom Modernizr build: www.modernizr.com/download/ -->
    <script src="http://ww2.filestoa.com/js/modernizr/modernizr-2.0.6.min.js"></script>

  </head>

  <body>

<jsp:include page="/fragment/TopNavbar">
<jsp:param name="pageCode" value="usercode/edit"/>
<jsp:param name="brandDisplayName" value="<%=brandDisplayName%>"/>
</jsp:include>


    <div class="container-fluid">

      <!-- Main hero unit for a primary marketing message or call to action -->
      <div id="main" class="hero-unit">
      <form class="form-horizontal" id="form_userusercode_view" method="POST" action="" accept-charset="UTF-8">

      <div id="userusercode_preface">
      <fieldset name="fieldset_userusercode_preface">
        <table style="background-color: transparent;">
        <tr>
        <td style="vertical-align:bottom;">
        <div id="cannyurl_logo">
	 	    <h1><%=brandDisplayName%><sup>&beta;</sup></h1>
        </div>
        </td>
        </tr>
        </table>
      </fieldset>
      </div>


      <div id="userusercode_form_body">
      <fieldset name="fieldset_userusercode_body">

		  <div class="control-group">
            <label class="control-label" for="input_userusercode_usercode" id="input_userusercode_usercode_label" title="Input client Code">Usercode:</label>
		    <div class="controls">
               <input class="input-large" type="text" name="input_userusercode_usercode" id="input_userusercode_usercode" size="35" placeholder="e.g., my_name" value="<%=(userUsercode == null) ? "" : userUsercode%>"></input>        
               <button class="btn" type="button" name="button_userusercode_usercode_check" id="button_userusercode_usercode_check" title="Check the availability of this client Code" style="width:70px">Setup</button>  
               <button class="btn" type="button" name="button_userusercode_setup" id="button_userusercode_setup" title="Create or update usercode" style="width:70px">Save</button>  
		    </div>
		  </div>
		  <div id="div_control_group_userusercode_customdomain" class="control-group">
            <label class="control-label" for="input_userusercode_customdomain" id="input_userusercode_customdomain_label" title="Vanity Domain">Vanity Domain:</label>
		    <div class="controls">
                <input class="input-large uneditable-input" type="text" name="input_userusercode_customdomain" id="input_userusercode_customdomain" size="35" readonly value="<%=userUsercodeDomain%>"></input>        
		    </div>
		  </div>

      </fieldset>
      </div>


      </form>
      </div>  <!--   Hero unit  -->




<!--   BEGIN: Content   -->



<% if(BrandingHelper.getInstance().isBrandCannyURL()) { %>
<% } else if(BrandingHelper.getInstance().isBrandFlashURL()) { %>
<% } else if(BrandingHelper.getInstance().isBrandFemtoURL()) { %>
<% } else if(BrandingHelper.getInstance().isBrandSassyURL()) { %>
<% } else if(BrandingHelper.getInstance().isBrandUserURL()) { %>
<% } else if(BrandingHelper.getInstance().isBrandQuadURL()) { %>
<% } else if(BrandingHelper.getInstance().isBrandSpeedKeyword()) { %>
<% } else if(BrandingHelper.getInstance().isBrandTweetUserURL()) { %>
<% } else if(BrandingHelper.getInstance().isBrandSmallBizURL()) { %>
<% } else { %>
<% } %>



<!--   END: Content   -->





<!-- Page Footer -->    
<%@ include file="/fragment/Footer.jspf" %>
<!-- Page Footer -->    

    </div> <!--! end of #container -->



    <div id="div_status_floater" style="display: none;" class="alert">
    </div>
 

    <!-- JavaScript at the bottom for fast page loading -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="http://ww2.filestoa.com/js/jquery/jquery-1.8.3.min.js"><\/script>')</script>
	<script src="http://ww2.filestoa.com/js/jquery/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="http://ww2.filestoa.com/js/jquery/plugin/jquery.floatobject-1.4.js"></script>
    <script src="http://ww2.filestoa.com/js/jquery/plugin/jquery-ui-timepicker-addon.js"></script>
    <script src="http://ww2.filestoa.com/js/jquery/plugin/history.js/uncompressed/history.adapter.jquery.js"></script>
    <script src="http://ww2.filestoa.com/js/jquery/plugin/history.js/uncompressed/history.js"></script>
    <script src="http://ww2.filestoa.com/js/jquery/plugin/history.js/uncompressed/history.html4.js"></script>
    <script src="http://ww2.filestoa.com/js/bootstrap/bootstrap-2.0.js"></script>
    <script type="text/javascript" src="/js/core/debugutil.js"></script>
    <script type="text/javascript" src="/js/core/urlutil-1.0.js"></script>
    <script type="text/javascript" src="http://ww2.filestoa.com/js/core/datetimeutil-1.0.js"></script>
    <script type="text/javascript" src="http://ww2.filestoa.com/js/core/emailutil-1.0.js"></script>
    <script type="text/javascript" src="/js/bean/userusercodejsbean-1.0.js"></script>
    <script type="text/javascript" src="/js/mylibs/mytimer.js"></script>
    <script defer type="text/javascript" src="/js/mylibs/redirecturl-1.0.js"></script>
	<script defer type="text/javascript" src="/js/plugins.js"></script>
    <script defer type="text/javascript" src="/js/script.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/fiveten-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/statushelper-1.1.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/tweethelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/signuphelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/feedbackhelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/emailhelper-1.0.js"></script>
    <!-- end scripts-->


    <script>
    // Global vars
    var feedbackHelper;
    $(function() {
    	var targetService = "<%=brandDisplayName%>";
    	var targetPage = "usercode";
    	feedbackHelper = new webstoa.FeedbackHelper(targetService, targetPage);
    });
    </script>

    <script>
    // Email address "decoding"...
    $(function() {
        $("a").each(function() {
        	var anchor = $(this);
        	webstoa.EmailUtil.decodeMailtoAnchor(anchor);
        });
    });
    </script>

    <script>
    $(function() {
        $("#anchor_feedback_footer").click(function() {
            if(DEBUG_ENABLED) console.log("Feedback button clicked."); 
            var targetItem = 'footer';
            feedbackHelper.feedback(targetItem);
		    return false;
        });
    });
    </script>


    <script>
    // Function definitions.
    function updateStatus(msg, type, time) {
    	if(!time) {
    		time = 5000;   // milliseconds.
    	}
    	if(!type) {
    		// type = "info";  // warning, error, etc....
    	}
    	//$("#div_status_floater").text(msg);
    	//$("#div_status_floater").fadeIn().delay(time).fadeOut();
		$("#div_status_floater").textInOut(msg, type, time);
    }    
    </script>

    <script>
    // temporary
    function replaceURLWithHTMLLinks(text) {
        if(text) {
            var exp = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
            //var exp = /\(?\bhttp:\/\/[-A-Za-z0-9+&@#\/%?=~_()|!:,.;]*[-A-Za-z0-9+&@#/%=~_()|]/ig;
            return text.replace(exp,"<a rel='nofollow' href='$1'>$1</a>"); 
        }
        return '';    // ???
    }
    </script>



    <script>
    // "Init"
    $(function() {
        // select() ????
        var currentVal = $("#input_userusercode_usercode").val();
        if(currentVal) {
	        $("#input_userusercode_usercode").focus().val('').val(currentVal);
        } else {
	        $("#input_userusercode_usercode").focus();
        }

    	// TBD: initially hide $("#div_status_floater") ...
    	$("#div_status_floater").text("").hide();
    
        // Background image test.
        //$("#container").css("background-image", 'url(/img/bg/oceansunset.jpg)');
        //$(document.body).css("background", "silver");
    	//$("#container").css("background", "ivory");

        // TBD: Location...
    	var params = {x: 585, y: 70, speed: 'normal' };
    	$("#div_status_floater").makeFloat(params);
    	    	
        // $("#input_userusercode_usercode").removeAttr('readonly');

    	// TBD:
        refrehUserUsercodeVanityDomain();
        // ...
    	
    	// Peeng?
    	//var fiveTenTimer = new fiveten.FiveTenTimer(12972, 2);  // ???
    	//fiveTenTimer.start();
    });
    </script>


    <script>
    function disableSaveButtons(delay)
    {
    	$("#button_userusercode_setup").attr('disabled','disabled');
    	$("#button_userusercode_usercode_check").attr('disabled','disabled');
    	var milli;
    	if(delay) {
    		milli = delay;
    	} else {
    		milli = 7500;  // ???
    	}
    	setTimeout(resetAndEnableSaveButtons, milli);
    }
    function enableSaveButtons()
    {
    	setTimeout(resetAndEnableSaveButtons, 1250);  // ???
    }
    function resetAndEnableSaveButtons()
    {
    	$("#button_userusercode_setup").removeAttr('disabled');
    	$("#button_userusercode_usercode_check").removeAttr('disabled');
    }
    </script>

    <script>
    // App-related vars.
    var userGuid;
    var userUsercodeJsBean;
    // var isUsercodeValidated = false;
    var isUsercodeChecked = false;
    var isUsercodeUsable = false;    // true means - validated/checked and it can be used by this client...
    var editState = 'state_new';
<%
if(userId != null) {
%>
    userGuid = "<%=userId%>";
<%
}
%>
<% if(userUsercodeBean == null) { %>
    userUsercodeJsBean = new cannyurl.wa.bean.UserUsercodeJsBean();
    if(userGuid) {
    	userUsercodeJsBean.setUser(userGuid);
    }
<% } else { %>
    editState = 'state_edit';
	var userUsercodeJsObjectStr = '<%=userUsercodeBean.toEscapedJsonStringForJavascript()%>';
	if(DEBUG_ENABLED) console.log("userUsercodeJsObjectStr = " + userUsercodeJsObjectStr);
	userUsercodeJsBean = cannyurl.wa.bean.UserUsercodeJsBean.fromJSON(userUsercodeJsObjectStr);
    if(DEBUG_ENABLED) console.log("userUsercodeJsBean = " + userUsercodeJsBean.toString());
<% } %>
    if(DEBUG_ENABLED) console.log("editState = " + editState);
    if(DEBUG_ENABLED) console.log("userUsercodeJsBean = " + userUsercodeJsBean.toString());
    </script>

    <script>
    // Additional Init based on editState.
    $(function() {
      if(editState == 'state_new') {
          $("#button_userusercode_setup").text("Save");
          $("#button_userusercode_usercode_check").text("Check");
      
      
      } else {
          $("#button_userusercode_setup").text("Update");
          $("#button_userusercode_usercode_check").text("Check");

    	  
      }
    });
    </script>

    <script>
    // Ajax form handlers.
  $(function() {
	  // ...
  });
    </script>

 
  <script>
  function validateUsercode(usercode)
  {
	  // isUsercodeValidated = true;

	    if(! usercode) {
	    	return false;
	    }

	    var usercodeLength = usercode.length;
        var minLength = <%=ConfigUtil.getUsercodeMinLength()%>;
        var maxLength = <%=ConfigUtil.getUsercodeMaxLength()%>;
        if(usercodeLength < minLength) {
            return false;
        }
        if(usercodeLength > maxLength) {
            return false;
        }

        return true;
  }
  </script>
 
  <script>
  // TBD:
  function checkUsercode(usercode, proceedToSave)
  {
	  var validated = validateUsercode(usercode);
	  if(! validated) {
		  isUsercodeChecked = false;
    	  isUsercodeUsable = false;   	 
	      updateStatus('Usercode, "' + usercode + '", is invalid. Please try a different code.', 'error', 7550);    		  
		  return;
	  }
      // TBD:
      usercode = usercode.toLowerCase();
      // ....

	  // Reset these flags before calling ajax.... ?????
	  isUsercodeChecked = false;
	  isUsercodeUsable = false;
	  // ...
	  
      var usercodeCheckUrl = "<%=topLevelUrl%>" + "ajax/validate/usercode/" + usercode;
      $.getJSON(usercodeCheckUrl, function(data) {
		  isUsercodeChecked = true;
    	  if(data) {
             var isValid = data['valid'];
             if(isValid) {
             	  isUsercodeChecked = true;
            	  isUsercodeUsable = true;
             } else {
            	  isUsercodeChecked = true;
            	  isUsercodeUsable = false;            	 
             }
    	  } else {
    		  // ????
        	  isUsercodeChecked = true;
        	  isUsercodeUsable = false;            	 
    	  }
    	  if(isUsercodeUsable) {
    	      updateStatus('Usercode, "' + usercode + '", is available.', 'info', 7550);
    	      if(proceedToSave) {
    	    	  // TBD:
    	    	  saveUserUsercodeJsBean();
    	          // ...
    	      }
    	  } else {
    	      updateStatus('Your specified usercode may not be available. Please try again.', 'error', 5550);    		  
    	  }
      })
      .fail(function() { 
		  isUsercodeChecked = true;
    	  isUsercodeUsable = false;            	 
	      updateStatus('Usercode checking failed', 'error', 5550);    		  
      });
  }
  </script>

  <script>
  function buildUserUsercodeVanityDomain(usercode)
  {
      // temporary
      // Copy of DomainUtil.buildUserUsercodeVanityDomain()
      var baseDomain = "<%=DomainUtil.getDefaultBaseDomain()%>";
      var slashslash = baseDomain.indexOf("://") + 3;
      var prefix = baseDomain.substring(0, slashslash);
      var theRest = baseDomain.substring(slashslash);
      var domain = prefix + usercode + "." + theRest;
      return domain;
  }
  </script>

  <script>
  function refrehUserUsercodeVanityDomain()
  {
      // temporary
		var usercode = $("#input_userusercode_usercode").val();
		var vanityDomain = "";
		if(usercode) {
			vanityDomain = buildUserUsercodeVanityDomain(usercode);
		}
		$("#input_userusercode_customdomain").val(vanityDomain);
		// ....
  }
  </script>

  <script>
  $(function() {
	  // ...
      $("#input_userusercode_usercode").keyup(function(event) {
    	  refrehUserUsercodeVanityDomain();
    	  // isUsercodeValidated = false;
    	  isUsercodeChecked = false;
    	  isUsercodeUsable = false;
      });
  });
  </script>


    <script>
    $(function() {

    	$("#button_userusercode_usercode_check").click(function() {
    	    if(DEBUG_ENABLED) console.log("Usercode check button clicked.");

            var usercode = $("#input_userusercode_usercode").val();
            if(usercode) {
        	    checkUsercode(usercode);
            } else {
            	// ????
            }
            
            return false;
    	});

    });
    </script>

    <script>
    $(function() {

    	$("#button_userusercode_setup").click(function() {
    	    if(DEBUG_ENABLED) console.log("Setup button clicked.");

    	    if(! isUsercodeChecked) {
      	        // disableSaveButtons();    // ????    	    	
    	        var usercode = $("#input_userusercode_usercode").val().trim();
    	    	checkUsercode(usercode, true);
    	    	return false;
    	    }
    	    
    	    if(isUsercodeUsable) {
    	      updateStatus('Saving the usercode...', 'info', 5550);
    	      disableSaveButtons();    	    	
    	      saveUserUsercodeJsBean();
    	    } else {
    	    	// ???
        	    updateStatus('The usercode may not be available. Please try again.', 'error', 5550);    		  
    	    }

    	    // TBD: ...
    	    // window.location = setupPageUrl;

            return false;
    	
    	});

    });
    </script>


    <script>
    var saveUserUsercodeJsBean = function() {
    	
        // validate and process form here      

        var usercode = $("#input_userusercode_usercode").val().trim();
        if(DEBUG_ENABLED) console.log("usercode = " + usercode);
        // if(usercode == '') {
        //     if(DEBUG_ENABLED) console.log("Client code is not set.");
        //     updateStatus('Client code is not set.', 'error', 5550);
        //     enableSaveButtons();
      	//     return false;
        // }
        // TBD:
        usercode = usercode.toLowerCase();
        // ....
        userUsercodeJsBean.setUsercode(usercode);
        
        if(userGuid) {   
        	userUsercodeJsBean.setUser(userGuid);  // This should not be necessary..
        }
        
        // Update the modifiedTime field ....
     	if(editState != 'state_new') {
     	    var modifiedTime = (new Date()).getTime();
            userUsercodeJsBean.setModifiedTime(modifiedTime);
     	}

        // Payload...
        var userUsercodeJsonStr = JSON.stringify(userUsercodeJsBean);
        if(DEBUG_ENABLED) console.log("userUsercodeJsonStr = " + userUsercodeJsonStr);
        var payload = "{\"userUsercode\":" + userUsercodeJsonStr;
        // TBD...
        payload += "}";
        if(DEBUG_ENABLED) console.log("payload = " + payload);
        
        
        // Web service settings.
        var userUsercodePostEndPoint = "<%=topLevelUrl%>" + "ajax/userusercode";
        if(DEBUG_ENABLED) console.log("userUsercodePostEndPoint =" + userUsercodePostEndPoint);
        var httpMethod;
        var webServiceUrl;
        if(editState == 'state_new') {
      	  httpMethod = "POST";
      	  webServiceUrl = userUsercodePostEndPoint;
  	  } else if(editState == 'state_edit') {
      	  httpMethod = "PUT";
      	  webServiceUrl = userUsercodePostEndPoint + "/" + userUsercodeJsBean.getGuid();
        } else {
      	  // ???
     		  if(DEBUG_ENABLED) console.log("Error. Invalid editState = " + editState);
            updateStatus('Error occurred.', 'error', 5550);
            enableSaveButtons();
      	  return false;
        }

        if(httpMethod) {
          $.ajax({
      	    type: httpMethod,
      	    url: webServiceUrl,
      	    data: payload,
      	    dataType: "json",
      	    contentType: "application/json; charset=UTF-8",
      	    success: function(data, textStatus) {
      	      // TBD
      	      if(DEBUG_ENABLED) console.log("userUsercodeJsBean successfully saved. textStatus = " + textStatus);
      	      if(DEBUG_ENABLED) console.log("data = " + data);
      	      if(DEBUG_ENABLED) console.log(data);

    	      // History
    	      // Do this only if the current editState == 'state_new'
    	      if(editState == 'state_new') {
	    	      var History = window.History;
	    	      if(History) {
	   	    	    	// TBD: Need to html escape????
	    				var htmlPageTitle = '<%=brandDisplayName%> | Client Setup ';
	    	            // TBD: This does not work on HTML4 browsers (e.g., IE)
				        History.pushState({state:1}, htmlPageTitle, "/usercode/edit/" + userUsercodeJsBean.getGuid());
	    	      }
    	      }
    	      
    	      // If it is currently editState == 'state_new', change it to 'state_edit'.
    	      // If it is currently editState == 'state_edit', that's ok too.
    	      editState = 'state_edit';

      	      // Parse data...
      	      if(data) {   // POST only..
  	    	      // Replace/Update the JsBeans ...

  	    	      var userUsercodeObj = data["userUsercode"];
  	    	      if(DEBUG_ENABLED) console.log("userUsercodeObj = " + userUsercodeObj);
  	    	      if(DEBUG_ENABLED) console.log(userUsercodeObj);
  	    	      userUsercodeJsBean = cannyurl.wa.bean.UserUsercodeJsBean.create(userUsercodeObj);
  	    	      if(DEBUG_ENABLED) console.log("New userUsercodeJsBean = " + userUsercodeJsBean);
  	    	      if(DEBUG_ENABLED) console.log(userUsercodeJsBean);
  	    	      
  	    	      
  	    	      // Delay this just a bit so that the server has time to actually save it (Note: we are using async call for saving).
  	    	      // This helps when a new userUsercode is saved. 
  	    	      // TBD: In both save/update, various buttons should be disabled before the ajax call
  	    	      //   and they should be re-enabled after the call successuflly returns or after certain preset time.
  	    	      window.setTimeout(refreshFormFields, 1250);
  	              updateStatus('Usercode successfully saved.', 'info', 5550);
      	      } else {
      	    	  // ignore
      	    	  // PUT currently returns "OK" only.
      	    	  // TBD: Change the ajax server side code so that it returns the updated data.
      	    	  //      Based on the returned/updated data, some fields (e.g., userUsercode) may need to be updated....
  	              updateStatus('Usercode successfully updated.', 'info', 5550);
      	      } 
      	      enableSaveButtons();
      	    },
      	    error: function(req, textStatus) {
      	    	var errorMsg = 'Failed to set up client. status = ' + textStatus;
            	if(DEBUG_ENABLED) console.log(errorMsg);
      	    	updateStatus(errorMsg, 'error', 5550);
      	    	enableSaveButtons();
      	    }
  	   	  });
        } else {
      	  // ???
      	  enableSaveButtons();
        }
        
    };
   </script>

  <script>
  // Refresh input fields...
  var refreshFormFields = function() {
	if(DEBUG_ENABLED) console.log("refreshFormFields() called......");
	if(DEBUG_ENABLED) console.log("editState = " + editState);
	
	if(editState == 'state_new') {
        $("#button_userusercode_setup").text("Save");
        $("#button_userusercode_usercode_check").text("Check");
		$("#input_userusercode_usercode").val('');
		$("#input_userusercode_customdomain").val('');

	} else {
        $("#button_userusercode_setup").text("Update");
        $("#button_userusercode_usercode_check").text("Check");

        if(userUsercodeJsBean) {
			var usercode = userUsercodeJsBean.getUsercode();
			$("#input_userusercode_usercode").val(usercode);
			// $("#input_userusercode_customdomain").val('');    // ???
			// ...

		} else {
			// ????
		}
	}
	
	// ...
    refrehUserUsercodeVanityDomain();
    // ...
    
  };
 </script>




<script>
$(function() {
	// Top menu handlers
	// Note: /view and /info reuire guid....
	// TBD: Due to the way we use subdomains...
	//      Every time we change url (subdomain), the login state changes...
	//      We use 27 subdomains, therfore this is not not very usable...
	// For now, all urls should point to relative URLs "/view", "/setup", etc...
	//      rather than path-based URLs, e.g., a.fm.gs/i/abcd, ....
    $("#topmenu_nav_shorten").click(function() {
        if(DEBUG_ENABLED) console.log("topmenu_nav_shorten anchor clicked."); 
        if(userUsercodeJsBean) {
            var guid = userUsercodeJsBean.getGuid();
            window.location = "/shorten/" + guid;            		
        } else {
        	window.location = "/shorten";
        }
        return false;
    });
    $("#topmenu_nav_view").click(function() {
        if(DEBUG_ENABLED) console.log("topmenu_nav_view anchor clicked."); 
        if(userUsercodeJsBean) {
        	//var userUsercode = userUsercodeJsBean.getShortUrl();
            //if(! userUsercode) {
            //	var domain = userUsercodeJsBean.getDomain();
            //   	var token = userUsercodeJsBean.getToken();
            //   	if(domain && token) {
            //      	userUsercode = domain + token;
            //   	} else {
                    var guid = userUsercodeJsBean.getGuid();
                    userUsercode = "/view/" + guid;  // temporary               		
            //   	}
            //}
            window.location = userUsercode;            		
        } else {
            // ??? error ???
            $("#topmenu_nav_view").fadeOut(450).fadeIn(950);
        }
        return false;
    });
    $("#topmenu_nav_setup").click(function() {
        if(DEBUG_ENABLED) console.log("topmenu_nav_setup anchor clicked."); 
        //    if(userUsercodeJsBean) {
        //        var guid = userUsercodeJsBean.getGuid();
        //        window.location = "/setup/" + guid;            		
        //    } else {
        //        // ??? error ???
        //    }
        return false;
    });
<%
if(isAppAuthDisabled == true || isAppAuthOptional == true || isUserAuthenticated == true) {
%>
    $("#topmenu_nav_info").click(function() {
        if(DEBUG_ENABLED) console.log("topmenu_nav_info anchor clicked."); 
        if(userUsercodeJsBean) {
            //var redirectUrlHelper = new cannyurl.redirecturl.RedirectUrlHelper(userUsercodeJsBean);
            //if(redirectUrlHelper.isReady()) {
            //    var infoUrl = redirectUrlHelper.infoUrl();
            //    window.location = infoUrl;
            //} else {
                var guid = userUsercodeJsBean.getGuid();
                window.location = "/info/" + guid;	
            //}
        } else {
        	window.location = "/info";
        }
        return false;
    });
    //$("#topmenu_nav_list").click(function() {
    //    if(DEBUG_ENABLED) console.log("topmenu_nav_list anchor clicked."); 
    //    window.location = "/list";
    //    return false;
    //});
<%
}
%>
});
</script>


<%
String googleAnalyticsCode = AnalyticsHelper.getInstance().getGoogleAnalyticsCode();
if(googleAnalyticsCode != null && !googleAnalyticsCode.isEmpty()) {    // And, if it is enabled (e.g. through config). TBD...
%>
    <!-- Change UA-XXXXX-X to be your site's ID -->
    <script>
      window._gaq = [['_setAccount','<%=googleAnalyticsCode%>'],['_trackPageview'],['_trackPageLoadTime']];
      Modernizr.load({
        load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
      });
    </script>
<%
}
%>

    <!-- Prompt IE 6 users to install Chrome Frame. Remove this if you want to support IE 6.
       chromium.org/developers/how-tos/chrome-frame-getting-started -->
    <!--[if lt IE 7 ]>
      <script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
      <script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
    <![endif]-->

    </body>
</html>