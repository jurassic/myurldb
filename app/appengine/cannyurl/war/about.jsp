<%@ page import="com.myurldb.ws.core.*, com.myurldb.ws.*, com.cannyurl.af.util.*, com.cannyurl.af.auth.common.*, com.cannyurl.af.auth.*, com.cannyurl.af.auth.user.*, com.cannyurl.fe.*, com.cannyurl.fe.core.*, com.cannyurl.fe.bean.*, com.cannyurl.fe.util.*, com.cannyurl.wa.service.*, com.cannyurl.common.*, com.cannyurl.util.*, com.cannyurl.app.auth.*, com.cannyurl.app.auth.filter.*, com.cannyurl.app.util.*, com.cannyurl.helper.*" 
%><%@ page contentType="text/html; charset=UTF-8" 
%><%@ page errorPage="/error/UnknownError" 
%><%!
private static final java.util.logging.Logger logger = java.util.logging.Logger.getLogger("About");
%><%
boolean isAppAuthDisabled = ConfigUtil.isApplicationAuthDisabled();
// boolean isAppAuthOptional = ConfigUtil.isApplicationAuthOptional();
String appAuthMode = ConfigUtil.getApplicationAuthMode();
boolean isUsingTwitterAuth = false;
if(AppAuthMode.MODE_TWITTER.equals(appAuthMode)) {
    isUsingTwitterAuth = true;
}
%><%// {1] Session
SessionBean sessionBean = UserSessionManager.getInstance().setSessionBean(request, response);
String sessionToken = sessionBean.getToken();
String userId = sessionBean.getUserId();

// [2] Parse url.
String contextPath = request.getContextPath();
String servletPath = request.getServletPath();
String requestHost = request.getServerName();
//String requestURI = request.getRequestURI();
String requestUrl = request.getRequestURL().toString();
String topLevelUrl = UrlHelper.getInstance().getTopLevelURLFromRequestURL(requestUrl);
String pathInfo = request.getPathInfo();
String queryString = request.getQueryString();
%><%
// "Branding"
String appBrand = BrandingHelper.getInstance().getAppBrand();
String brandDisplayName = BrandingHelper.getInstance().getBrandDisplayName();
String brandDescription = BrandingHelper.getInstance().getBrandDescription();
%><%String themeBootstrap = SiteThemeHelper.getInstance().getBootstrapTheme();
String themeJqueryUi = SiteThemeHelper.getInstance().getJqueryUiTheme();%><%
boolean isUserAuthenticated = false;
String userUsername = null;
String loginUrl = null;
String logoutUrl = null;
RequestAuthStateBean authStateBean = (RequestAuthStateBean) request.getAttribute(AuthUtil.REQUEST_ATTR_AUTHSTATEBEAN);
if(authStateBean == null) {
    // Filter might not have applied (because of the /* mapping, etc...) 
    // Try one more time ???
    authStateBean = AuthFilterUtil.createAuthStateBean(request, response);
    if(authStateBean != null) {
        // Add it to the current request object....
        request.setAttribute(AuthUtil.REQUEST_ATTR_AUTHSTATEBEAN, authStateBean);
    }
}
if(authStateBean != null) {
    isUserAuthenticated = authStateBean.isAuthenticated();
    if(isUserAuthenticated) {
        logoutUrl = authStateBean.getLogoutUrl();
        userUsername = authStateBean.getUsername();
    } else {
        loginUrl = authStateBean.getLoginUrl();
    }   
}
%><%
String twitterHandle = "";
String twitterUserProfileUrl = "";
if(isUsingTwitterAuth) {
	if(userUsername != null && !userUsername.isEmpty()) {
	    twitterHandle = "@" + userUsername;      // ???
	    twitterUserProfileUrl = "http://twitter.com/" + userUsername;
	}
}
%><%
if(isUserAuthenticated == false) {
    // ????
    // response.sendRedirect(loginUrl);
}
%><%
// ...
%><!DOCTYPE html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!-- Consider adding an manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><%=brandDisplayName%> | About</title>
    <meta name="description" content="<%=brandDisplayName%> service.">

    <!-- Mobile viewport optimized: j.mp/bplateviewport -->
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <link rel="shortcut icon" href="/img/favicon-<%=appBrand%>.ico" />
    <link rel="icon" href="/img/favicon-<%=appBrand%>.ico" type="image/x-icon" />

    <!-- CSS: implied media=all -->
    <%@ include file="/fragment/style.jspf" %>
    <link rel="stylesheet" type="text/css" href="/css/cannyurl-<%=appBrand%>.css"/>
    <!-- end CSS-->

    <!-- All JavaScript at the bottom, except for Modernizr / Respond.
       Modernizr enables HTML5 elements & feature detects; Respond is a polyfill for min/max-width CSS3 Media Queries
       For optimal performance, use a custom Modernizr build: www.modernizr.com/download/ -->
    <script src="http://ww2.filestoa.com/js/modernizr/modernizr-2.0.6.min.js"></script>

    </head>

  <body>

<jsp:include page="/fragment/TopNavbar">
<jsp:param name="pageCode" value="about"/>
<jsp:param name="brandDisplayName" value="<%=brandDisplayName%>"/>
</jsp:include>

    <div class="container-fluid">

      <!-- Main hero unit for a primary marketing message or call to action -->
      <div class="hero-unit">
        <h1>Making the Web Safe</h1>
        <p> 
        We provide "URL redirection" services (including URL shortenting).
        Our mission is to make the Web safer, one URL at a time.
        Please note that <%=brandDisplayName%> is currently in a "beta" release.
        </p>
        <p><a id="anchor_mainmenu_signup" class="btn btn-primary btn-large" title="Please let us know your email address, and we will keep you posted on our progress at <%=brandDisplayName%>.">Sign up &raquo;</a></p>
      </div>  <!--   Hero unit  -->


      <!-- Example row of columns -->
      <div class="row-fluid">
        <div class="span12">
          <h2>Contact Information</h2>
<%if(BrandingHelper.getInstance().isBrandUserURL()) {%>
		<p>
		Please contact us at <a href="contact:support+<%=appBrand%>+com">support at <%=appBrand%></a>
		if you have any questions or suggestions.
		You can also find us on Twitter.
		Our handle is <a href="http://twitter.com/userurls">@userurls</a>.
		</p>
<%} else if(BrandingHelper.getInstance().isBrandTwrim()) {%>
		<p>
		Please contact us at <a href="contact:support+twr+im">support at <%=appBrand%></a>
		if you have any questions or suggestions.
		You can also find us on Twitter.
		Our handle is <a href="http://twitter.com/<%=appBrand%>">@<%=appBrand%></a>.
		</p>
<%} else {%>
		<p>
		Please contact us at <a href="contact:support+<%=appBrand%>+com">support at <%=appBrand%></a>
		if you have any questions or suggestions.
		You can also find us on Twitter.
		Our handle is <a href="http://twitter.com/<%=appBrand%>">@<%=appBrand%></a>.
		</p>
<%} %>
        </div>
      </div>

      <div class="row-fluid">
        <div class="span12">
          <h2>Terms of Service</h2>
 		Although <%=brandDisplayName%> is an open (and free) service,
        we do not condone or permit abuse (or any illegal or immoral use) of <%=brandDisplayName%>.
		Please notify us of any suspicious activities such as spamming or phishing,
		or otherwise questionable activities such as posting generally offensive or hateful content, etc. 
		</p>
        </div>
      </div>

      <div class="row-fluid">
        <div class="span12">
          <h2>Google App Engine</h2>
		<p>
		<%=brandDisplayName%> is built and being run on <a href="http://code.google.com/appengine">Google App Engine.</a>
		The fact that <%=brandDisplayName%> is powered by Google App Engine means that you, as a user of the service,
		can be assured of the reliability of the service.
		Furthermore, your data will be safe since it is stored on Google's data storage.
		<%=brandDisplayName%> is hosted on Google's infrastructure,
		and it will be available as long as Google is in service.  
		</p>
		<p>
		<img src="http://code.google.com/appengine/images/appengine-noborder-120x30.gif" 
		alt="Powered by Google App Engine" />
		</p>
        </div>
      </div>

      <div class="row-fluid">
        <div class="span12">
          <h2>Some Common Questions</h2>
		<h3>The application feels somewhat slow at times?</h3>
		<p>
		<%=brandDisplayName%> is hosted on Google's infrastructure.
		When a user makes a request (e.g., to create or view a short URL, etc.), 
		necessary resource is dynamically allocated, which takes certain amount of time.
		Currently, <%=brandDisplayName%> has relatviely small traffic (we are in a beta phase),
		and a request may incur a certain "initialization" overhead.
		Eventually, this will improve as more people start using <%=brandDisplayName%>.
		</p>
        </div>
      </div>

      <div class="row-fluid">
        <div class="span12">
		<h3>I sometimes see 404 (Page Not Found) error?</h3>
		<p>
		This is related to the earlier question.
		If you try to view a short URL immediately after "saving" it, you may sometimes see this error.
		Again, this is due to the fact that it takes a bit of time for the application to "warm up".
		<%=brandDisplayName%> uses "asynchronous mode" when saving/updating data.
		While it should normally take only a fraction of a second, it can take a few seconds or longer
		to persist the changes if the application has been idle for a while. 
		However, you can be assured that, even when you encounter this error, your data is safe. 
		Your new or updated short URL data will be saved, eventually (e.g., within 30 seconds at max, even in the worst case).
		Just remember your URL and check back later if you run into this error.
		</p>
        </div>
      </div>

      <div class="row-fluid">
        <div class="span12">
		<h3>When will <%=brandDisplayName%> be fully released?</h3>
		<p>
		<%=brandDisplayName%> is a free service. We will likely keep this as "beta" for a while.
		New features will be gradually added over time.
		Please let us know if you have any particular features you'd like to see on <%=brandDisplayName%>.
		We will likely prioritize our development based on users' demands/feedback. 
		Also, we'd greatly appreciate any bug report (other than, maybe, the performance issues). 
		</p>
        </div>
      </div>

<hr>

<!-- Page Footer -->    
<%@ include file="/fragment/Footer.jspf" %>
<!-- Page Footer -->    

    </div> <!--! end of #container -->

    <div id="div_status_floater" style="display: none;" class="alert">
    </div>

    
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="http://ww2.filestoa.com/js/jquery/jquery-1.8.3.min.js"><\/script>')</script>
	<script src="http://ww2.filestoa.com/js/jquery/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="http://ww2.filestoa.com/js/jquery/plugin/jquery.floatobject-1.4.js"></script>
    <script src="http://ww2.filestoa.com/js/bootstrap/bootstrap-2.0.js"></script>
    <script type="text/javascript" src="/js/core/debugutil.js"></script>
	<script src="/js/plugins.js"></script>
    <script src="/js/script.js"></script>
    <script type="text/javascript" src="http://ww2.filestoa.com/js/core/datetimeutil-1.0.js"></script>
    <script type="text/javascript" src="http://ww2.filestoa.com/js/core/emailutil-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/fiveten-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/statushelper-1.1.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/tweethelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/signuphelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/feedbackhelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/emailhelper-1.0.js"></script>


    <script>
    // Global vars
    var feedbackHelper;
    $(function() {
    	var targetService = "<%=brandDisplayName%>";
    	var targetPage = "about";
    	feedbackHelper = new webstoa.FeedbackHelper(targetService, targetPage);
    });
    </script>

    <script>
    // Email address "decoding"...
    $(function() {
        $("a").each(function() {
        	var anchor = $(this);
        	webstoa.EmailUtil.decodeMailtoAnchor(anchor);
        });
    });
    </script>

    <script>
    $(function() {
        $("#anchor_feedback_footer").click(function() {
            if(DEBUG_ENABLED) console.log("Feedback button clicked."); 
            var targetItem = 'footer';
            feedbackHelper.feedback(targetItem);
		    return false;
        });
    });
    </script>

    <script>
    var signupHelper;
    $(function() {
    	// Init...
    	var servicename = "<%=brandDisplayName%>";
    	var launchname = "Initial pre-launch trial";
    	var pageid = "about";
    	signupHelper = new webstoa.SignupHelper(servicename, launchname, pageid);
    });
    </script>

    <script>
    $(function() {
        $("#anchor_mainmenu_signup").click(function() {
            if(DEBUG_ENABLED) console.log("Signup button clicked."); 

       	    signupHelper.signup();
       	    //statusHelper.update('Thanks for signing up.');
		    return false;
        });
    });
    </script>


<script>
$(function() {
	// Top menu handlers
    $("#topmenu_nav_view").click(function() {
        if(DEBUG_ENABLED) console.log("topmenu_nav_view anchor clicked."); 
        ////window.location = "/view";
        // ???
        $("#topmenu_nav_view").fadeOut(450).fadeIn(950);
        return false;
    });
});
</script>


    
    <!--  Google Analytics  -->
<%
String googleAnalyticsCode = AnalyticsHelper.getInstance().getGoogleAnalyticsCode();
if(googleAnalyticsCode != null && !googleAnalyticsCode.isEmpty()) {    // And, if it is enabled (e.g. through config). TBD...
%>
    <!-- Change UA-XXXXX-X to be your site's ID -->
    <script>
      window._gaq = [['_setAccount','<%=googleAnalyticsCode%>'],['_trackPageview'],['_trackPageLoadTime']];
      Modernizr.load({
        load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
      });
    </script>
<%
}
%>

  </body>
</html>
