<%@ page import="com.myurldb.ws.core.*, com.cannyurl.af.util.*, com.cannyurl.af.auth.common.*, com.cannyurl.af.auth.*, com.cannyurl.af.auth.facebook.*, com.cannyurl.af.auth.twitter.*, com.cannyurl.af.auth.googleapps.*, com.cannyurl.af.auth.user.*, com.cannyurl.fe.*, com.cannyurl.fe.core.*, com.cannyurl.fe.bean.*, com.cannyurl.fe.util.*, com.cannyurl.wa.service.*, com.cannyurl.common.*, com.cannyurl.util.*, com.cannyurl.app.auth.*, com.cannyurl.helper.*" 
%><%@ page contentType="text/html; charset=UTF-8" 
%><%@ page errorPage="/error/UnknownError" 
%><%
// Last update
// 3/03/13:
%><%!
private static final java.util.logging.Logger logger = java.util.logging.Logger.getLogger("Auth.Selector");
%><%
// {1] Session
SessionBean sessionBean = UserSessionManager.getInstance().setSessionBean(request, response);
String sessionToken = sessionBean.getToken();
String userId = sessionBean.getUserId();

//[2] Parse url.
String requestUrl = request.getRequestURL().toString();
String topLevelUrl = UrlHelper.getInstance().getTopLevelURLFromRequestURL(requestUrl);
String contextPath = request.getContextPath();
String servletPath = request.getServletPath();
String pathInfo = request.getPathInfo();
String queryString = request.getQueryString();
String referer = request.getHeader("referer");
//...
%><%
// "Branding"
String appBrand = BrandingHelper.getInstance().getAppBrand();
String brandDisplayName = BrandingHelper.getInstance().getBrandDisplayName();
String brandDescription = BrandingHelper.getInstance().getBrandDescription();
%><%String themeBootstrap = SiteThemeHelper.getInstance().getBootstrapTheme();
String themeJqueryUi = SiteThemeHelper.getInstance().getJqueryUiTheme();%><%
// 
String comebackUrl = request.getParameter(CommonAuthUtil.PARAM_COMEBACKURL);
if(comebackUrl == null || comebackUrl.isEmpty()) {
    if(referer != null && !referer.isEmpty()) {
        comebackUrl = referer;  // ???
    } else {
        String thispageUrl = request.getRequestURI();
        if(queryString != null && !queryString.isEmpty()) {
            thispageUrl += "?" + queryString;
        }
        comebackUrl = thispageUrl;  // ????
    }
}
//...
%><%
// 
String fallbackUrl = request.getParameter(CommonAuthUtil.PARAM_FALLBACKURL);
if(fallbackUrl == null || fallbackUrl.isEmpty()) {
    String thispageUrl = request.getRequestURI();
    if(queryString != null && !queryString.isEmpty()) {
        thispageUrl += "?" + queryString;
    }
    fallbackUrl = thispageUrl;  // ????
}
//...

%><%
//java.security.Principal principle = request.getUserPrincipal();
AuthUserService authUserService = AuthUserServiceFactory.getAuthUserService();
AuthUser authUser = authUserService.getCurrentUser(); // or req.getUserPrincipal()

%><%
// Forward (not redirect) to manager...
// Note: For forward, (relative) url path is used, not an abstract url.
//       Parameters can be passed through jsp:param, and hence there is no need to mupdate the url path....
// String authManagerUrl = AuthHelper.getInstance().getAuthManagerUrl(topLevelUrl, comebackUrl, fallbackUrl);
String authManagerPage = AuthHelper.getInstance().getAuthManagerUrlPath();
// logger.fine("authManagerPage = " + authManagerPage);
// ...

%><%

//.... Process requests here that do not require user input...
//   and redirect/forward....
if(AuthConfigManager.getInstance().ssoGoogleApps()) {
    // Note: Normally, in case of google apps sso, this page should not have reached....
    String gaAppsDomain = AuthUtil.getGoogleAppsSsoAppsDomain(request);
    if(gaAppsDomain == null || gaAppsDomain.isEmpty()) {
        // ???
        gaAppsDomain = GoogleAppsAuthHelper.getInstance().getDeaultAppsDomain();
    }
    if(gaAppsDomain == null || gaAppsDomain.isEmpty()) {
        // Error...
        // What to do???
        // bail out? stay on this page?
    } else {
        request.setAttribute(GoogleAppsAuthUtil.REQUEST_ATTR_APPSDOMAIN, gaAppsDomain);
        // request.setAttribute(AuthUtil.REQUEST_ATTR_CALLBACKPAGE, CommonAuthUtil.AUTH_SERVICE_PROVIDERID_GOOGLEAPPS);  // ??
%>
<jsp:forward page="<%=authManagerPage%>" >
  <jsp:param name="comebackUrl" value="<%=comebackUrl%>" />
  <jsp:param name="fallbackUrl" value="<%=fallbackUrl%>" />
</jsp:forward>
<%
    }
    // ...
}
// etc...
//....
//%><%

%><!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!-- Consider adding an manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<html>
  <head>
    <meta charset="utf-8">
    <title><%=brandDisplayName%> | Auth</title>
    <meta name="author" content="Aery Software">
    <meta name="description" content="<%=AppBrand.getBrandDescription(appBrand)%>.">
    <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!-- Mobile viewport optimized: j.mp/bplateviewport -->
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <!-- Le styles -->
    <%@ include file="/fragment/style.jspf" %>
    <link rel="stylesheet" type="text/css" href="http://ww2.filestoa.com/css/openid/openid.css" />

    <!-- Le fav and touch icons -->
	<link rel="shortcut icon" href="/img/favicon-<%=appBrand%>.ico" />
	<link rel="icon" href="/img/favicon-<%=appBrand%>.ico" type="image/x-icon" />

    <!-- All JavaScript at the bottom, except for Modernizr / Respond.
       Modernizr enables HTML5 elements & feature detects; Respond is a polyfill for min/max-width CSS3 Media Queries
       For optimal performance, use a custom Modernizr build: www.modernizr.com/download/ -->
    <script src="http://ww2.filestoa.com/js/modernizr/modernizr-2.0.6.min.js"></script>

  </head>

  <body>


    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="i-bar"></span>
            <span class="i-bar"></span>
            <span class="i-bar"></span>
          </a>
          <a class="brand" href="/home"><%=brandDisplayName%> <sup>&beta;</sup></a>
          <div class="nav-collapse">
            <ul class="nav">
<!-- 
              <li><a id="topmenu_nav_home" href="/home">Home</a></li>
-->
              <li><a id="topmenu_nav_about" href="/about" title="About <%=brandDisplayName%>">About</a></li>
<!--
              <li><a id="topmenu_nav_contact" href="/contact" title="Contact information">Contact</a></li>
              <li><a id="topmenu_nav_blog" href="/blog" title="<%=brandDisplayName%> blog">Blog</a></li>
-->
            </ul>
            <p class="navbar-text pull-right">
            </p>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container-fluid">



<%
if(authUser != null) {
    String nickName = authUser.getNickname(); 
    String logoutUrl = authUserService.createLogoutURL(request.getRequestURI());
%>
      <div id="main" class="hero-unit">
My name = <%=nickName%>
<br/>
<a class="btn" href="<%=logoutUrl%>">Logout</a>
      </div>
<%
} else {
    String authUrl = request.getRequestURI();
    if(queryString != null && !queryString.isEmpty()) {
        authUrl += "?" + queryString;
    }
%>

      <!-- Main hero unit for a primary marketing message or call to action -->
      <div id="main" class="hero-unit">

      <div id="memo_auth_body" class="memo_auth_view_body">

        <div id="memo_auth_body_main" class="memo_auth_body_main_content">
          <div id="memo_auth_content">

	<h2>Login through Your Favorite Service</h2>
	<p>Please select a service and specify your username, if necessary.
	Note that <%=brandDisplayName%> delegates authentication to these services (often known as OpenID or OAuth providers),
	and it does not store your user credentials or any otherwise sensitive information.
	(Note: You can also create a <%=brandDisplayName%> account and use it for logging in,
	if you do not wish to use one of these popular authentication services.)
	</p>

<%
%>

	<!-- Simple OpenID Selector -->
	<form action="<%=authManagerPage%>" method="get" id="openid_form">
<%
if(comebackUrl != null && !comebackUrl.isEmpty()) {
%>
		<input type="hidden" name="<%=CommonAuthUtil.PARAM_COMEBACKURL%>" value="<%=comebackUrl%>" />
<%
}
%>
<%
if(fallbackUrl != null && !fallbackUrl.isEmpty()) {
%>
		<input type="hidden" name="<%=CommonAuthUtil.PARAM_FALLBACKURL%>" value="<%=fallbackUrl%>" />
<%
}
%>
		<fieldset>
			<legend>Sign-in or Create New Account</legend>
			<div id="openid_choice">
				<p>Please click your account provider:</p>
				<div id="openid_btns"></div>
			</div>
			<div id="openid_input_area">
				<input id="openid_identifier" name="openid_identifier" type="text" value="http://" />
				<input id="openid_submit" type="submit" value="Sign-In"/>
			</div>
			<noscript>
				<p>OpenID is service that allows you to log-on to many different websites using a single indentity.
				Find out <a href="http://openid.net/what/">more about OpenID</a> and <a href="http://openid.net/get/">how to get an OpenID enabled account</a>.</p>
			</noscript>
		</fieldset>
	</form>
	<!-- /Simple OpenID Selector -->

          </div>
        </div>
      </div>
      </div>



      <!-- Example row of columns -->
      <div class="row-fluid">
        <div class="span6">
          <h2>Log on through <%=brandDisplayName%></h2>
          <p>
          You can also log on using local username/password without relying on delegated authentication.
          </p>
          
          <form id="form_authenticate" action="/auth/authenticate">
          </form>

       </div>
        <div class="span6">
          <h2>Register on <%=brandDisplayName%></h2>
          <p>
          If you are new, you can register.
          </p>
          
          <form id="form_register" action="/auth/register">
          </form>

       </div>
      </div>


<%
}
%>


      <hr>

      <footer>
        <p>&copy; <%=brandDisplayName%> 2012&nbsp; <a id="anchor_feedback_footer" href="#" title="Feedback"><i class="icon-comment"></i></a></p>
      </footer>
    </div> <!--! end of #container -->


    <div id="div_status_floater" style="display: none;">
      <div id="status_message">
        <span id="span_status_message">(Status)</span>
      </div>
    </div>
    
    
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="http://ww2.filestoa.com/js/jquery/jquery-1.8.3.min.js"><\/script>')</script>
	<script src="http://ww2.filestoa.com/js/jquery/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="http://ww2.filestoa.com/js/jquery/plugin/jquery.floatobject-1.4.js"></script>
	<script src="/js/openid/openid-jquery.js"></script>
	<script src="/js/openid/openid-en.js"></script>
    <script src="http://ww2.filestoa.com/js/bootstrap/bootstrap-2.0.js"></script>
    <script type="text/javascript" src="/js/core/debugutil.js"></script>
    <script type="text/javascript" src="http://ww2.filestoa.com/js/core/datetimeutil-1.0.js"></script>
    <script type="text/javascript" src="http://ww2.filestoa.com/js/core/emailutil-1.0.js"></script>
	<script defer type="text/javascript" src="/js/plugins.js"></script>
    <script defer type="text/javascript" src="/js/script.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/fiveten-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/statushelper-1.1.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/tweethelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/signuphelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/feedbackhelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/emailhelper-1.0.js"></script>
    <!-- end scripts-->



    <script>
        // Global vars
    	var statusHelper;
    	var tweetHelper;
    	var signupHelper;
    	var feedbackHelper;
    	var emailHelper;
    </script>
    <script>
    $(function() {
    	// Init...
    	statusHelper = new webstoa.StatusHelper(400, 65);
    	// test.
    	//statusHelper.update('test status message');

    	var servicename = "<%=brandDisplayName%>";
    	var launchname = "Initial pre-launch trial";
    	var pageid = "home";
    	signupHelper = new webstoa.SignupHelper(servicename, launchname, pageid);

    	var targetService = "<%=brandDisplayName%>";
    	var targetPage = "home";
    	feedbackHelper = new webstoa.FeedbackHelper(targetService, targetPage);

    	var senderEmail = webstoa.EmailUtil.decodeEmailAddress('gaeemail+gmail' + '+com');
    	var defaultSenderName = "<%=brandDisplayName%>";
    	var subject = "Interesting site: <%=brandDisplayName%>";
    	var defaultMessage = "Hi,\n\n<%=brandDisplayName%>, http://www.cannyurl.com/, is a scratch paper for Web surfers. Please check it out.\n\n-me\n";
    	emailHelper = new webstoa.EmailHelper(senderEmail, subject, defaultMessage, defaultSenderName);
    });
    </script>

    <script>
    // Email address "decoding"...
    $(function() {
        $("a").each(function() {
        	var anchor = $(this);
        	webstoa.EmailUtil.decodeMailtoAnchor(anchor);
        });
    });
    </script>

    <script>
    $(function() {
        $("#anchor_feedback_footer").click(function() {
            if(DEBUG_ENABLED) console.log("Feedback button clicked."); 

            var targetItem = 'footer';
            feedbackHelper.feedback(targetItem);
		    return false;
        });
    });
    </script>



    <script>
    // Function definitions.
    function updateStatus(msg, type, time) {
    	if(!time) {
    		time = 5000;   // milliseconds.
    	}
    	if(!type) {
    		// type = "info";  // warning, error, etc....
    	}
    	//$("#div_status_floater").text(msg);
    	//$("#div_status_floater").fadeIn().delay(time).fadeOut();
		$("#div_status_floater").textInOut(msg, type, time);
    }    
    </script>

    <script>
    // temporary
    //function replaceURLWithHTMLLinks(text) {
    //    var exp = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
    //    //var exp = /\(?\bhttp:\/\/[-A-Za-z0-9+&@#\/%?=~_()|!:,.;]*[-A-Za-z0-9+&@#/%=~_()|]/ig;
    //    return text.replace(exp,"<a href='$1'>$1</a>"); 
    //}
    </script>

    <script>
    // "Init"
    $(function() {
    	// TBD: initially hide $("#div_status_floater") ...
    	$("#div_status_floater").text("").hide();
    
        // Background image test.
        //$("#container").css("background-image", 'url(/img/bg/oceansunset.jpg)');
        //$(document.body).css("background", "silver");
    	//$("#container").css("background", "ivory");

        // TBD: Location...
    	var params = {x: 530, y: 65, speed: 'normal' };
    	$("#div_status_floater").makeFloat(params);
    	
    	// temporary
    	// linkify the content
    	//var contentOriginal = $("#memo_auth_content").html();
    	//var contentLinkified = replaceURLWithHTMLLinks(contentOriginal);
    	//$("#memo_auth_content").html(contentLinkified);
    	// temporary
    	
    	// Peeng?
    	//var fiveTenTimer = new fiveten.FiveTenTimer(21889, 12);  // ???
    	//fiveTenTimer.start();
    });
    </script>


    <script>
    // App-related vars.
    var userGuid;
<%
if(userId != null) {
%>
    userGuid = "<%=userId%>";
<%
}
%>
    if(DEBUG_ENABLED) console.log("userGuid = " + userGuid);
    </script>


	<script type="text/javascript">
		$(document).ready(function() {
			openid.init('openid_identifier');
			//openid.setDemoMode(true); //Stops form submission for client javascript-only test purposes
		});
	</script>


<%
String googleAnalyticsCode = AnalyticsHelper.getInstance().getGoogleAnalyticsCode();
if(googleAnalyticsCode != null && !googleAnalyticsCode.isEmpty()) {    // And, if it is enabled (e.g. through config). TBD...
%>
    <!-- Change UA-XXXXX-X to be your site's ID -->
    <script>
      window._gaq = [['_setAccount','<%=googleAnalyticsCode%>'],['_trackPageview'],['_trackPageLoadTime']];
      Modernizr.load({
        load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
      });
    </script>
<%
}
%>

    <!-- Prompt IE 6 users to install Chrome Frame. Remove this if you want to support IE 6.
       chromium.org/developers/how-tos/chrome-frame-getting-started -->
    <!--[if lt IE 7 ]>
      <script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
      <script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
    <![endif]-->

    </body>
</html>