<%@ page import="com.myurldb.ws.core.*, com.cannyurl.af.util.*, com.cannyurl.af.auth.common.*, com.cannyurl.af.auth.*, com.cannyurl.af.auth.facebook.*, com.cannyurl.af.auth.twitter.*, com.cannyurl.af.auth.googleapps.*, com.cannyurl.af.auth.user.*, com.cannyurl.fe.*, com.cannyurl.fe.core.*, com.cannyurl.fe.bean.*, com.cannyurl.fe.util.*, com.cannyurl.wa.service.*, com.cannyurl.common.*, com.cannyurl.util.*, com.cannyurl.app.auth.*, com.cannyurl.helper.*" 
%><%@ page contentType="text/html; charset=UTF-8" 
%><%@ page errorPage="/error/UnknownError" 
%><%
// Last update
// 3/03/13: Google+ SignIn (Work in progress)
// 1/14/13: Now trying to retrieve OAuth request token from the session, if the request to Twitter fails...
%><%!
private static final java.util.logging.Logger logger = java.util.logging.Logger.getLogger("Auth.Manager");
%><%
// {1] Session
SessionBean sessionBean = UserSessionManager.getInstance().setSessionBean(request, response);
String sessionToken = sessionBean.getToken();
String userId = sessionBean.getUserId();

//[2] Parse url.
String requestUrl = request.getRequestURL().toString();
String topLevelUrl = UrlHelper.getInstance().getTopLevelURLFromRequestURL(requestUrl);
String contextPath = request.getContextPath();
String servletPath = request.getServletPath();
String pathInfo = request.getPathInfo();
String queryString = request.getQueryString();
String referer = request.getHeader("referer");
//...
%><%
// "Branding"
String appBrand = BrandingHelper.getInstance().getAppBrand();
String brandDisplayName = BrandingHelper.getInstance().getBrandDisplayName();
String brandDescription = BrandingHelper.getInstance().getBrandDescription();
%><%String themeBootstrap = SiteThemeHelper.getInstance().getBootstrapTheme();
String themeJqueryUi = SiteThemeHelper.getInstance().getJqueryUiTheme();%><%
// 
String comebackUrl = request.getParameter(CommonAuthUtil.PARAM_COMEBACKURL);
if(comebackUrl == null || comebackUrl.isEmpty()) {
    if(referer != null && !referer.isEmpty()) {
        comebackUrl = referer;  // ???
    } else {
        String thispageUrl = request.getRequestURI();
        if(queryString != null && !queryString.isEmpty()) {
            thispageUrl += "?" + queryString;
        }
        comebackUrl = thispageUrl;  // ????
    }
}
//...
%><%
//
String fallbackUrl = request.getParameter(CommonAuthUtil.PARAM_FALLBACKURL);
if(fallbackUrl == null || fallbackUrl.isEmpty()) {
    String thispageUrl = request.getRequestURI();
    if(queryString != null && !queryString.isEmpty()) {
        thispageUrl += "?" + queryString;
    }
    fallbackUrl = thispageUrl;  // ????
}
//...

%><%
//java.security.Principal principle = request.getUserPrincipal();
AuthUserService authUserService = AuthUserServiceFactory.getAuthUserService();
AuthUser authUser = authUserService.getCurrentUser(); // or req.getUserPrincipal()

%><%
// etc...
//....
//%><%
// 
%><!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!-- Consider adding an manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<html>
  <head>
    <meta charset="utf-8">
    <title><%=brandDisplayName%> | Auth</title>
    <meta name="author" content="Aery Software">
    <meta name="description" content="<%=AppBrand.getBrandDescription(appBrand)%>.">
    <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!-- Mobile viewport optimized: j.mp/bplateviewport -->
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <!-- Le styles -->
    <%@ include file="/fragment/style.jspf" %>
    <link rel="stylesheet" type="text/css" href="http://ww2.filestoa.com/css/openid/openid.css" />

    <!-- Le fav and touch icons -->
	<link rel="shortcut icon" href="/img/favicon-<%=appBrand%>.ico" />
	<link rel="icon" href="/img/favicon-<%=appBrand%>.ico" type="image/x-icon" />

    <!-- All JavaScript at the bottom, except for Modernizr / Respond.
       Modernizr enables HTML5 elements & feature detects; Respond is a polyfill for min/max-width CSS3 Media Queries
       For optimal performance, use a custom Modernizr build: www.modernizr.com/download/ -->
    <script src="http://ww2.filestoa.com/js/modernizr/modernizr-2.0.6.min.js"></script>

  </head>

  <body>


<%
if(authUser != null) {
    String nickName = authUser.getNickname(); 
    String logoutUrl = authUserService.createLogoutURL(request.getRequestURI());
%>
      <div id="main" class="hero-unit">
My name = <%=nickName%>
<br/>
<a class="btn" href="<%=logoutUrl%>">Logout</a>
      </div>
<%
} else {
	String gaAppsDomain = null;
	String openIdVerifier = null;
	String openIdUsername = null;
	String openIdProviderId = null;
    // Google Apps SSO???
	if(AuthConfigManager.getInstance().ssoGoogleApps()) {
        // [1a] Google Apps SSO...
	    gaAppsDomain = AuthUtil.getGoogleAppsSsoAppsDomain(request);
	    if(gaAppsDomain == null || gaAppsDomain.isEmpty()) {
	        // ???
			gaAppsDomain = (String) request.getAttribute(GoogleAppsAuthUtil.REQUEST_ATTR_APPSDOMAIN);
	    }
	    if(gaAppsDomain == null || gaAppsDomain.isEmpty()) {
	        // ???
			gaAppsDomain = (String) request.getAttribute(GoogleAppsAuthUtil.REQUEST_ATTR_APPSDOMAIN);
	    }
	    if(gaAppsDomain == null || gaAppsDomain.isEmpty()) {
	        // ???
		    gaAppsDomain = GoogleAppsAuthHelper.getInstance().getDeaultAppsDomain();
	    }
	    request.removeAttribute(GoogleAppsAuthUtil.REQUEST_ATTR_APPSDOMAIN);  // ???
	    if(gaAppsDomain == null || gaAppsDomain.isEmpty()) {
	        // Error...
	        // What to do???
	        // bail out? stay on this page?
	    } else {
		    // ???
            openIdProviderId =  CommonAuthUtil.AUTH_SERVICE_PROVIDERID_GOOGLEAPPS;
	        session.setAttribute(AuthUtil.SESSION_ATTR_AUTH_PROVIDERID,openIdProviderId);
	        session.setAttribute(AuthUtil.SESSION_ATTR_AUTH_FEDERATEDIDENTITY, gaAppsDomain);
	    }
	} else { 
	    // Handle the open ID selector form...
		openIdVerifier = request.getParameter("openid_identifier");   // This matches the input id in Auth1Selector.jsp....
		// openIdUsername = request.getParameter("openid_username");     // ????	
		
	    if(openIdVerifier != null) {
		    // [1b] Store the "current" provider id and open Id url in the session...
		    if(CommonAuthUtil.AUTH_SERVICE_PROVIDERID_TWITTER.equals(openIdVerifier)) {
		        openIdProviderId = CommonAuthUtil.AUTH_SERVICE_PROVIDERID_TWITTER;
		    } else if(CommonAuthUtil.AUTH_SERVICE_PROVIDERID_FACEBOOK.equals(openIdVerifier)) {
		        openIdProviderId = CommonAuthUtil.AUTH_SERVICE_PROVIDERID_FACEBOOK;
		    } else if(CommonAuthUtil.AUTH_SERVICE_PROVIDERID_YAHOO.equals(openIdVerifier) || openIdVerifier.contains("yahoo.com")) {           // ???
		        openIdProviderId = CommonAuthUtil.AUTH_SERVICE_PROVIDERID_YAHOO;
		    } else if(CommonAuthUtil.AUTH_SERVICE_PROVIDERID_GOOGLE.equals(openIdVerifier) || openIdVerifier.contains("google.com")) {         // ???
		        openIdProviderId = CommonAuthUtil.AUTH_SERVICE_PROVIDERID_GOOGLE;
		    } else if(CommonAuthUtil.AUTH_SERVICE_PROVIDERID_AOL.equals(openIdVerifier) || openIdVerifier.contains("aol.com")) {               // ???
		        openIdProviderId = CommonAuthUtil.AUTH_SERVICE_PROVIDERID_AOL;
		    } else if(CommonAuthUtil.AUTH_SERVICE_PROVIDERID_MYOPENID.equals(openIdVerifier) || openIdVerifier.contains("myopenid.com")) {     // ???
		        openIdProviderId = CommonAuthUtil.AUTH_SERVICE_PROVIDERID_MYOPENID;
		    } else if(CommonAuthUtil.AUTH_SERVICE_PROVIDERID_LIVEJOURNAL.equals(openIdVerifier) || openIdVerifier.contains("livejournal.com")) {   // ???
		        openIdProviderId = CommonAuthUtil.AUTH_SERVICE_PROVIDERID_LIVEJOURNAL;
		    } else if(CommonAuthUtil.AUTH_SERVICE_PROVIDERID_FLICKR.equals(openIdVerifier) || openIdVerifier.contains("flickr.com")) {         // ???
		        openIdProviderId = CommonAuthUtil.AUTH_SERVICE_PROVIDERID_FLICKR;
		    } else if(CommonAuthUtil.AUTH_SERVICE_PROVIDERID_WORDPRESS.equals(openIdVerifier) || openIdVerifier.contains("wordpress.com")) {   // ???
		        openIdProviderId = CommonAuthUtil.AUTH_SERVICE_PROVIDERID_WORDPRESS;
		    } else if(CommonAuthUtil.AUTH_SERVICE_PROVIDERID_BLOGGER.equals(openIdVerifier) || openIdVerifier.contains("blogger.com")) {       // ???
		        openIdProviderId = CommonAuthUtil.AUTH_SERVICE_PROVIDERID_BLOGGER;
		    } else if(CommonAuthUtil.AUTH_SERVICE_PROVIDERID_VERISIGN.equals(openIdVerifier) || openIdVerifier.contains("verisignlabs.com")) { // ???
		        openIdProviderId = CommonAuthUtil.AUTH_SERVICE_PROVIDERID_VERISIGN;
		    } else if(CommonAuthUtil.AUTH_SERVICE_PROVIDERID_CLAIMID.equals(openIdVerifier) || openIdVerifier.contains("claimid.com")) {       // ???
		        openIdProviderId = CommonAuthUtil.AUTH_SERVICE_PROVIDERID_CLAIMID;
		    } else if(CommonAuthUtil.AUTH_SERVICE_PROVIDERID_CLICKPASS.equals(openIdVerifier) || openIdVerifier.contains("clickpass.com")) {   // ???
		        openIdProviderId = CommonAuthUtil.AUTH_SERVICE_PROVIDERID_CLICKPASS;
		    } else if(CommonAuthUtil.AUTH_SERVICE_PROVIDERID_YAMMER.equals(openIdVerifier)) {
		        // ???
		        openIdProviderId = CommonAuthUtil.AUTH_SERVICE_PROVIDERID_YAMMER;
		    } else if(CommonAuthUtil.AUTH_SERVICE_PROVIDERID_LINKEDIN.equals(openIdVerifier)) {
		        // ???
		        openIdProviderId = CommonAuthUtil.AUTH_SERVICE_PROVIDERID_LINKEDIN;
		    } else if(CommonAuthUtil.AUTH_SERVICE_PROVIDERID_MYSPACE.equals(openIdVerifier)) {
		        // ???
		        openIdProviderId = CommonAuthUtil.AUTH_SERVICE_PROVIDERID_MYSPACE;
		    } else if(CommonAuthUtil.AUTH_SERVICE_PROVIDERID_FOURSQUARE.equals(openIdVerifier)) {
		        // ???
		        openIdProviderId = CommonAuthUtil.AUTH_SERVICE_PROVIDERID_FOURSQUARE;
		    } else {
		        // ????
		        openIdProviderId = openIdVerifier;
		    }

	        session.setAttribute(AuthUtil.SESSION_ATTR_AUTH_PROVIDERID, openIdProviderId);
		    session.setAttribute(AuthUtil.SESSION_ATTR_AUTH_FEDERATEDIDENTITY, openIdVerifier);
		} else {
		    // ????
		}
	}	

    // [2] Find the correct login url for the given openId and/or providerId...
    String openIdLoginUrl = null;
    if(AuthConfigManager.getInstance().ssoGoogleApps()) {   // TBD: Check gaAppsDomain == null ???
        session.setAttribute(AuthUtil.SESSION_ATTR_AUTH_MODE, "googleapps");

        String destinationUrl = AuthHelper.getInstance().getAuthHandlerUrl(topLevelUrl, comebackUrl, fallbackUrl);
        openIdLoginUrl = authUserService.createLoginURL(destinationUrl, null, gaAppsDomain, new java.util.HashSet<String>());
    } else {
	    if(AuthHelper.getInstance().useTwitter4j(openIdProviderId)) {
	        session.setAttribute(AuthUtil.SESSION_ATTR_AUTH_MODE, "twitter4j");
	        
	        // TBD: Check the session first...
	        // boolean isAuthenticated = false;
	        // AuthToken authToken = (AuthToken) session.getAttribute(CommonAuthUtil.SESSION_ATTR_AUTHTOKEN);
	        // if(authToken != null) {
	        //     isAuthenticated = authToken.isAuthenticated(CommonAuthUtil.AUTH_SERVICE_PROVIDERID_TWITTER);
	        //}
	        // // twitter4j.auth.AccessToken accessToken = Twitter4jHelper.getInstance().getOAuthAccessToken(userId);
	        // // if(accessToken != null) {
	        // //     // Skip...
	        // // }
	        
	        boolean isAuthenticated = AuthManager.getInstance().isUserLoggedIn(session, CommonAuthUtil.AUTH_SERVICE_PROVIDERID_TWITTER);
	        
	        if(isAuthenticated) {
	            // Skip
	    	    response.setStatus(302);   // temporary redirect
	    	    response.setHeader( "Location", comebackUrl );
	    	    response.setHeader( "Connection", "close" );
	        }
	        
	        String callbackUrl = Twitter4jHelper.getInstance().getCallbackUri(topLevelUrl, comebackUrl, fallbackUrl);
	        twitter4j.auth.RequestToken requestToken = null;
	        // try {
	            // ????
	            // requestToken = Twitter4jHelper.getInstance().getRequestToken(session, callbackUrl);   // This makes hard to get a new/fresh request token....
	            requestToken = Twitter4jHelper.getInstance().getRequestToken(callbackUrl);               // Does this make sense????
	            // ????
	        // } catch(Exception e) {
	        //     // Ignore ???
	        //     logger.log(java.util.logging.Level.INFO, "Failed to get request token.", e);
	        // }
	        if(requestToken != null) {
		        session.setAttribute(Twitter4jHelper.SESSION_ATTR_TWITTER4J_REQUESTTOKEN, requestToken);
	        } else {
	            // ????
                // If the request token exists in the session, try using it ????
                // How to check if the request token is "fresh" enough ?????
                // requestToken = (twitter4j.auth.RequestToken) session.getAttribute(Twitter4jHelper.SESSION_ATTR_TWITTER4J_REQUESTTOKEN);
                // if(requestToken != null) {
                //     logger.info("requestToken obtained from the session. requestToken = " + requestToken);
                // }
                // .... 
            }
	        if(requestToken != null) {  // TBD: Validation??? (especially, the token was from the session)
		        // ???
		        openIdLoginUrl = requestToken.getAuthenticationURL();
		        // openIdLoginUrl = requestToken.getAuthorizationURL();
		        // ...
	        }

	    } else if(AuthHelper.getInstance().useSocialAuth(openIdProviderId)) {
	        session.setAttribute(AuthUtil.SESSION_ATTR_AUTH_MODE, "socialauth");
	
	        // //String successUrl = SocialAuthHelper.getInstance().getSuccessRedirectUri(topLevelUrl, comebackUrl, fallbackUrl);
	        // openIdLoginUrl = SocialAuthHelper.getInstance().getAuthenticationUrl(session, openIdVerifier, topLevelUrl, comebackUrl, fallbackUrl);
	        openIdLoginUrl = SocialAuthHelper.getInstance().getAuthenticationUrl(session, openIdProviderId, topLevelUrl, comebackUrl, fallbackUrl);
	        // ....
	    } else {
	        session.setAttribute(AuthUtil.SESSION_ATTR_AUTH_MODE, "gae-openid");
	
	        // TBD: In the case of Twitter (OAuth 1.0)
	        // Need to get request token...
	        if(CommonAuthUtil.AUTH_SERVICE_PROVIDERID_TWITTER.equals(openIdProviderId)) {
	            // TBD...
	        }
	        // ...
	
	        // GAE OpenID ...
	        String destinationUrl = null;
	        String randomNonce = CommonAuthUtil.generateRandomNonce();
	        if(CommonAuthUtil.AUTH_SERVICE_PROVIDERID_TWITTER.equals(openIdProviderId)) {
	            destinationUrl = AuthHelper.getInstance().getTwitterCallbackUrl(topLevelUrl, comebackUrl, fallbackUrl);
	        } else if(CommonAuthUtil.AUTH_SERVICE_PROVIDERID_FACEBOOK.equals(openIdProviderId)) {
	            session.setAttribute(FacebookAuthUtil.SESSION_ATTR_STATE, randomNonce);
	            destinationUrl = AuthHelper.getInstance().getFacebookCallbackUrl(topLevelUrl, comebackUrl, fallbackUrl);
	        } else {
	            if(CommonAuthUtil.AUTH_SERVICE_PROVIDERID_GOOGLE.equals(openIdProviderId)) {
		         
	                // ???
	                // GAE seems to have a bug...
	                // We can only include one param, comebackUrl OR fallbackUrl....
	                // If we include both, the one added first seems to be removed....
	                // So, as a workaround, we only add one param, comebackUrl....
	                // destinationUrl = AuthHelper.getInstance().getAuthHandlerUrl(topLevelUrl, comebackUrl, fallbackUrl);
	                destinationUrl = AuthHelper.getInstance().getAuthHandlerUrl(topLevelUrl, comebackUrl, null);	                
	                // temporary		            
	                
	            } else {
                    // TBD:
		            destinationUrl = AuthHelper.getInstance().getAuthHandlerUrl(topLevelUrl, comebackUrl, fallbackUrl);
	            }
	        }

	        // temporary
	        // logger.warning(">>>>>>>>>>>>>>>>>> destinationUrl = " + destinationUrl);
	        // temporary
	    
	        openIdLoginUrl = authUserService.createLoginURL(destinationUrl, null, openIdVerifier, new java.util.HashSet<String>(), randomNonce);

	        // temporary
	        // logger.warning(">>>>>>>>>>>>>>>>>> openIdLoginUrl = " + openIdLoginUrl);
	        // temporary

	    }
    }

    // [3] Redirect
    if(openIdLoginUrl != null) {
	    response.sendRedirect(openIdLoginUrl);
	    // response.setStatus(302);   // temporary redirect
	    // response.setHeader( "Location", openIdLoginUrl );
	    // response.setHeader( "Connection", "close" );
    } else {
        // ?????
        // Forward to auth_failed page ???
        if(fallbackUrl != null) {
            response.sendRedirect(fallbackUrl);
        } else {
            response.sendRedirect("/error/AuthFailure");  // Or, "/error/Unauthorized" ????
        }
        // response.setStatus(302);   // temporary redirect
	    // response.setHeader( "Location", fallbackUrl );
	    // // response.setHeader( "Location", "/error/Unauthorized" );
	    // response.setHeader( "Connection", "close" );
        // ....
    }
%>
<%
}
%>

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="http://ww2.filestoa.com/js/jquery/jquery-1.8.3.min.js"><\/script>')</script>
	<script src="http://ww2.filestoa.com/js/jquery/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="http://ww2.filestoa.com/js/jquery/plugin/jquery.floatobject-1.4.js"></script>
	<script src="/js/openid/openid-jquery.js"></script>
	<script src="/js/openid/openid-en.js"></script>
    <script src="http://ww2.filestoa.com/js/bootstrap/bootstrap-2.0.js"></script>
    <script type="text/javascript" src="/js/core/debugutil.js"></script>
    <script type="text/javascript" src="http://ww2.filestoa.com/js/core/datetimeutil-1.0.js"></script>
    <script type="text/javascript" src="http://ww2.filestoa.com/js/core/emailutil-1.0.js"></script>
	<script defer type="text/javascript" src="/js/plugins.js"></script>
    <script defer type="text/javascript" src="/js/script.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/fiveten-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/statushelper-1.1.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/tweethelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/signuphelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/feedbackhelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/emailhelper-1.0.js"></script>
    <!-- end scripts-->


    <script>
        // Global vars
    	var statusHelper;
    	var tweetHelper;
    	var signupHelper;
    	var feedbackHelper;
    	var emailHelper;
    </script>
    <script>
    $(function() {
    	// Init...
    	statusHelper = new webstoa.StatusHelper(400, 65);
    	// test.
    	//statusHelper.update('test status message');

    	var servicename = "<%=brandDisplayName%>";
    	var launchname = "Initial pre-launch trial";
    	var pageid = "home";
    	signupHelper = new webstoa.SignupHelper(servicename, launchname, pageid);

    	var targetService = "<%=brandDisplayName%>";
    	var targetPage = "home";
    	feedbackHelper = new webstoa.FeedbackHelper(targetService, targetPage);

    	var senderEmail = webstoa.EmailUtil.decodeEmailAddress('gaeemail+gmail' + '+com');
    	var defaultSenderName = "<%=brandDisplayName%>";
    	var subject = "Interesting site: <%=brandDisplayName%>";
    	var defaultMessage = "Hi,\n\n<%=brandDisplayName%>, http://www.cannyurl.com/, is a scratch paper for Web surfers. Please check it out.\n\n-me\n";
    	emailHelper = new webstoa.EmailHelper(senderEmail, subject, defaultMessage, defaultSenderName);
    });
    </script>

    <script>
    // Email address "decoding"...
    $(function() {
        $("a").each(function() {
        	var anchor = $(this);
        	webstoa.EmailUtil.decodeMailtoAnchor(anchor);
        });
    });
    </script>

    <script>
    $(function() {
        $("#anchor_feedback_footer").click(function() {
            if(DEBUG_ENABLED) console.log("Feedback button clicked."); 

            var targetItem = 'footer';
            feedbackHelper.feedback(targetItem);
		    return false;
        });
    });
    </script>



    <script>
    // Function definitions.
    function updateStatus(msg, type, time) {
    	if(!time) {
    		time = 5000;   // milliseconds.
    	}
    	if(!type) {
    		// type = "info";  // warning, error, etc....
    	}
    	//$("#div_status_floater").text(msg);
    	//$("#div_status_floater").fadeIn().delay(time).fadeOut();
		$("#div_status_floater").textInOut(msg, type, time);
    }    
    </script>

    <script>
    // temporary
    //function replaceURLWithHTMLLinks(text) {
    //    var exp = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
    //    //var exp = /\(?\bhttp:\/\/[-A-Za-z0-9+&@#\/%?=~_()|!:,.;]*[-A-Za-z0-9+&@#/%=~_()|]/ig;
    //    return text.replace(exp,"<a href='$1'>$1</a>"); 
    //}
    </script>

    <script>
    // "Init"
    $(function() {
    	// TBD: initially hide $("#div_status_floater") ...
    	//$("#div_status_floater").text("").hide();
    
        // Background image test.
        //$("#container").css("background-image", 'url(/img/bg/oceansunset.jpg)');
        //$(document.body).css("background", "silver");
    	//$("#container").css("background", "ivory");

        // TBD: Location...
    	//var params = {x: 530, y: 65, speed: 'normal' };
    	//$("#div_status_floater").makeFloat(params);
    	
    	// temporary
    	// linkify the content
    	//var contentOriginal = $("#memo_auth_content").html();
    	//var contentLinkified = replaceURLWithHTMLLinks(contentOriginal);
    	//$("#memo_auth_content").html(contentLinkified);
    	// temporary
    	
    	// Peeng?
    	//var fiveTenTimer = new fiveten.FiveTenTimer(21889, 12);  // ???
    	//fiveTenTimer.start();
    });
    </script>


    <script>
    // App-related vars.
    var userGuid;
<%
if(userId != null) {
%>
    userGuid = "<%=userId%>";
<%
}
%>
    if(DEBUG_ENABLED) console.log("userGuid = " + userGuid);
    </script>


	<script type="text/javascript">
		$(document).ready(function() {
			openid.init('openid_identifier');
			//openid.setDemoMode(true); //Stops form submission for client javascript-only test purposes
		});
	</script>


<%
String googleAnalyticsCode = AnalyticsHelper.getInstance().getGoogleAnalyticsCode();
if(googleAnalyticsCode != null && !googleAnalyticsCode.isEmpty()) {    // And, if it is enabled (e.g. through config). TBD...
%>
    <!-- Change UA-XXXXX-X to be your site's ID -->
    <script>
      window._gaq = [['_setAccount','<%=googleAnalyticsCode%>'],['_trackPageview'],['_trackPageLoadTime']];
      Modernizr.load({
        load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
      });
    </script>
<%
}
%>

    <!-- Prompt IE 6 users to install Chrome Frame. Remove this if you want to support IE 6.
       chromium.org/developers/how-tos/chrome-frame-getting-started -->
    <!--[if lt IE 7 ]>
      <script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
      <script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
    <![endif]-->

    </body>
</html>