<%@ page import="com.myurldb.ws.core.*, com.cannyurl.af.util.*, com.cannyurl.af.auth.common.*, com.cannyurl.af.auth.*, com.cannyurl.af.auth.facebook.*, com.cannyurl.af.auth.user.*, com.cannyurl.fe.*, com.cannyurl.fe.core.*, com.cannyurl.fe.bean.*, com.cannyurl.fe.util.*, com.cannyurl.wa.service.*, com.cannyurl.common.*, com.cannyurl.util.*, com.cannyurl.app.auth.*, com.cannyurl.helper.*" 
%><%@ page contentType="text/html; charset=UTF-8" 
%><%@ page errorPage="/error/UnknownError" 
%><%
// Last update
// 3/03/13
%><%!
private static final java.util.logging.Logger logger = java.util.logging.Logger.getLogger("Auth.FacebookCallback");
%><%
// {1] Session
SessionBean sessionBean = UserSessionManager.getInstance().setSessionBean(request, response);
String sessionToken = sessionBean.getToken();
String userId = sessionBean.getUserId();

//[2] Parse url.
String requestUrl = request.getRequestURL().toString();
String topLevelUrl = UrlHelper.getInstance().getTopLevelURLFromRequestURL(requestUrl);
String contextPath = request.getContextPath();
String servletPath = request.getServletPath();
String pathInfo = request.getPathInfo();
String queryString = request.getQueryString();
//...
%><%
//
String comebackUrl = request.getParameter(CommonAuthUtil.PARAM_COMEBACKURL);
String fallbackUrl = request.getParameter(CommonAuthUtil.PARAM_FALLBACKURL);
//...
%><%


//TBD:
//Exchange request token with access token
//and store accessToken in DB
//....

String requestVarState = (String) request.getParameter(FacebookAuthUtil.PARAM_STATE);
String sessionVarState = (String) session.getAttribute(FacebookAuthUtil.SESSION_ATTR_STATE);
if(requestVarState == null || !requestVarState.equals(sessionVarState)) {
    // error
    // bail out...
    // TBD...
}


String error = (String) request.getParameter(FacebookAuthUtil.PARAM_ERROR);
if(error != null || error.isEmpty()) {
    String error_reason = (String) request.getParameter(FacebookAuthUtil.PARAM_ERROR_REASON);
    String error_description = (String) request.getParameter(FacebookAuthUtil.PARAM_ERROR_DESCRIPTION);
    // error
    // display error page
    // bail out...
    // TBD....
}


String code = (String) request.getParameter(FacebookAuthUtil.PARAM_CODE);
if(code == null || code.isEmpty()) {
    // Can this happen??
    // error
    // bail out...
}

String callbackUrl = (String) request.getParameter(FacebookAuthUtil.PARAM_REDIRECT_URI);  // ????
String accessTokenUrl = FacebookAuthHelper.getInstance().getAcessTokenExchangeUrl(callbackUrl, code);
String content = HttpContentUtil.getStringContent(accessTokenUrl);
if(content != null) {
    java.util.Map<String,Object> resultMap = FacebookAuthHelper.getInstance().parseAccessTokenContent(content);
    String accessToken = (String) resultMap.get(FacebookAuthUtil.RESPONSE_VAR_ACCESS_TOKEN);
    Integer expires = (Integer) resultMap.get(FacebookAuthUtil.RESPONSE_VAR_EXPIRES);
    
    request.setAttribute(FacebookAuthUtil.REQUEST_ATTR_ACCESS_TOKEN, accessToken);
    request.setAttribute(FacebookAuthUtil.REQUEST_ATTR_EXPIRES, expires);
    
}



%><%

// Forward (not redirect) to handler...
// Note: For forward, (relative) url path is used, not an abstract url.
//       Parameters can be passed through jsp:param, and hence there is no need to mupdate the url path....
// String authHandlerUrl = AuthHelper.getInstance().getAuthHandlerUrl(topLevelUrl, comebackUrl, fallbackUrl);
String authHandlerPage = AuthHelper.getInstance().getAuthHandlerUrlPath();
// logger.fine("authHandlerPage = " + authHandlerPage);
// ...

session.removeAttribute(FacebookAuthUtil.SESSION_ATTR_STATE);
request.setAttribute(AuthUtil.REQUEST_ATTR_CALLBACKPAGE, CommonAuthUtil.AUTH_SERVICE_PROVIDERID_FACEBOOK);
%>
<jsp:forward page="<%=authHandlerPage%>" >
  <jsp:param name="comebackUrl" value="<%=comebackUrl%>" />
  <jsp:param name="fallbackUrl" value="<%=fallbackUrl%>" />
</jsp:forward>
