<%@ page import="com.myurldb.ws.core.*, com.cannyurl.af.util.*, com.cannyurl.af.auth.common.*, com.cannyurl.af.auth.*, com.cannyurl.af.auth.googleplus.*, com.cannyurl.af.auth.user.*, com.cannyurl.fe.*, com.cannyurl.fe.core.*, com.cannyurl.fe.bean.*, com.cannyurl.fe.util.*, com.cannyurl.wa.service.*, com.cannyurl.common.*, com.cannyurl.util.*, com.cannyurl.app.auth.*, com.cannyurl.helper.*" 
%><%@ page contentType="text/html; charset=UTF-8" 
%><%@ page errorPage="/error/UnknownError" 
%><%
// Last update
// 3/03/13: Google+ SignIn (Work in progress)
%><%!
private static final java.util.logging.Logger logger = java.util.logging.Logger.getLogger("Auth.GooglePlusCallback");
%><%
// {1] Session
SessionBean sessionBean = UserSessionManager.getInstance().setSessionBean(request, response);
String sessionToken = sessionBean.getToken();
String userId = sessionBean.getUserId();

//[2] Parse url.
String requestUrl = request.getRequestURL().toString();
String topLevelUrl = UrlHelper.getInstance().getTopLevelURLFromRequestURL(requestUrl);
String contextPath = request.getContextPath();
String servletPath = request.getServletPath();
String pathInfo = request.getPathInfo();
String queryString = request.getQueryString();
//...
%><%
//
String comebackUrl = request.getParameter(CommonAuthUtil.PARAM_COMEBACKURL);
String fallbackUrl = request.getParameter(CommonAuthUtil.PARAM_FALLBACKURL);
//...
%><%
// Verify csrf_state param.....
String formId = GooglePlusAuthHelper.getInstance().getDefaultSignInFormId();   // ???
boolean isVerified = CsrfHelper.getInstance().verifyCsrfState(request, formId);
if(isVerified == false) {
    // TBD:
    // Bail out....
    
}

%><%


String gPlusId = request.getParameter("gplus_id");
if(logger.isLoggable(java.util.logging.Level.INFO)) logger.info("gPlusId = " + gPlusId);

java.io.BufferedReader reader = request.getReader();
String code = reader.readLine();
if(logger.isLoggable(java.util.logging.Level.INFO)) logger.info("code = " + code);

%><%

com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse tokenResponse = GooglePlusSignInHelper.getInstance().convertToGooglePlusTokenResponse(code);
if(tokenResponse != null) {

    session.setAttribute(GooglePlusAuthUtil.SESSION_ATTR_TOKENRESPONSE, tokenResponse);

}


%><%

// Forward (not redirect) to handler...
// Note: For forward, (relative) url path is used, not an abstract url.
//       Parameters can be passed through jsp:param, and hence there is no need to mupdate the url path....
// String authHandlerUrl = AuthHelper.getInstance().getAuthHandlerUrl(topLevelUrl, comebackUrl, fallbackUrl);
String authHandlerPage = AuthHelper.getInstance().getAuthHandlerUrlPath();
// logger.fine("authHandlerPage = " + authHandlerPage);
// ...

request.setAttribute(AuthUtil.REQUEST_ATTR_CALLBACKPAGE, CommonAuthUtil.AUTH_SERVICE_PROVIDERID_GOOGLEPLUS);
%>
<jsp:forward page="<%=authHandlerPage%>" >
  <jsp:param name="comebackUrl" value="<%=comebackUrl%>" />
  <jsp:param name="fallbackUrl" value="<%=fallbackUrl%>" />
</jsp:forward>
