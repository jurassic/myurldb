<%@ page import="com.myurldb.ws.core.*, com.cannyurl.af.util.*, com.cannyurl.af.auth.common.*, com.cannyurl.af.auth.*, com.cannyurl.af.auth.facebook.*, com.cannyurl.af.auth.twitter.*, com.cannyurl.af.auth.user.*, com.cannyurl.fe.*, com.cannyurl.fe.core.*, com.cannyurl.fe.bean.*, com.cannyurl.fe.util.*, com.cannyurl.wa.service.*, com.cannyurl.common.*, com.cannyurl.util.*, com.cannyurl.app.auth.*, com.cannyurl.helper.*" 
%><%@ page contentType="text/html; charset=UTF-8" 
%><%@ page errorPage="/error/UnknownError" 
%><%
// Last update
// 3/03/13:
// 2/13/13: Facebook login support
%><%!
private static final java.util.logging.Logger logger = java.util.logging.Logger.getLogger("Auth.SocialAuthCallback");
%><%
// {1] Session
SessionBean sessionBean = UserSessionManager.getInstance().setSessionBean(request, response);
String sessionToken = sessionBean.getToken();
String userId = sessionBean.getUserId();

//[2] Parse url.
String requestUrl = request.getRequestURL().toString();
String topLevelUrl = UrlHelper.getInstance().getTopLevelURLFromRequestURL(requestUrl);
String contextPath = request.getContextPath();
String servletPath = request.getServletPath();
String pathInfo = request.getPathInfo();
String queryString = request.getQueryString();
//...
%><%
//
String comebackUrl = request.getParameter(CommonAuthUtil.PARAM_COMEBACKURL);
String fallbackUrl = request.getParameter(CommonAuthUtil.PARAM_FALLBACKURL);
//...
%><%


// ...
// org.brickred.socialauth.AuthProvider socialAuthProvider = SocialAuthHelper.getInstance().getAuthProvider(request);
// //Profile userProfile = socialAuthProvider.getUserProfile();

org.brickred.socialauth.SocialAuthManager socialAuthMaanger = SocialAuthHelper.getInstance().getSocialAuthManager(session, false);
if(socialAuthMaanger != null) {
    
    // ????
    // // java.util.Map<String, String> paramsMap = org.brickred.socialauth.util.SocialAuthUtil.getRequestParametersMap(request); 
    // // org.brickred.socialauth.AuthProvider socialAuthProvider = socialAuthMaanger.connect(paramsMap);
    org.brickred.socialauth.AuthProvider socialAuthProvider = SocialAuthHelper.getInstance().getAuthProvider(request);
    // org.brickred.socialauth.AuthProvider socialAuthProvider = socialAuthMaanger.getCurrentAuthProvider();
    // ?????
    if(socialAuthProvider != null) {
        
        org.brickred.socialauth.util.AccessGrant accessGrant = socialAuthProvider.getAccessGrant();
        if(accessGrant != null) {
            // String key = accessGrant.getKey();
            // String secret = accessGrant.getSecret();
            request.setAttribute(SocialAuthHelper.REQUEST_ATTR_SOCIALAUTH_ACCESSGRANT, accessGrant);

            // String providerId = socialAuthProvider.getProviderId();

            org.brickred.socialauth.Profile userProfile = null;
            try {
	            userProfile = socialAuthProvider.getUserProfile();
            } catch(Exception e) {
                // Ignore ???
                logger.log(java.util.logging.Level.WARNING, "Failed to get userProfile.", e);
            }
            if(userProfile != null) {
                request.setAttribute(SocialAuthHelper.REQUEST_ATTR_SOCIALAUTH_USERPROFILE, userProfile);
            }
            
    	} else {
    	    // ????
    	    logger.warning("Failed to get accessGrant.");
        }
    } else {
        // ???
        logger.warning("Failed to get socialAuthProvider.");
    }

    // ????
    session.removeAttribute(SocialAuthHelper.SESSION_ATTR_SOCIALAUTH_AUTHMANAGER);
    // ...
    
    
} else {
    // ????
    logger.warning("socialAuthMaanger not found in the session.");
}



%><%

// Forward (not redirect) to handler...
// Note: For forward, (relative) url path is used, not an abstract url.
//       Parameters can be passed through jsp:param, and hence there is no need to mupdate the url path....
// String authHandlerUrl = AuthHelper.getInstance().getAuthHandlerUrl(topLevelUrl, comebackUrl, fallbackUrl);
String authHandlerPage = AuthHelper.getInstance().getAuthHandlerUrlPath();
// logger.fine("authHandlerPage = " + authHandlerPage);
// ...

request.setAttribute(AuthUtil.REQUEST_ATTR_CALLBACKPAGE, "socialauth");
%>
<jsp:forward page="<%=authHandlerPage%>" >
  <jsp:param name="comebackUrl" value="<%=comebackUrl%>" />
  <jsp:param name="fallbackUrl" value="<%=fallbackUrl%>" />
</jsp:forward>
