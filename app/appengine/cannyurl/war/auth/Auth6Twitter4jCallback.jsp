<%@ page import="com.myurldb.ws.core.*, com.cannyurl.af.util.*, com.cannyurl.af.auth.common.*, com.cannyurl.af.auth.*, com.cannyurl.af.auth.facebook.*, com.cannyurl.af.auth.twitter.*, com.cannyurl.af.auth.user.*, com.cannyurl.fe.*, com.cannyurl.fe.core.*, com.cannyurl.fe.bean.*, com.cannyurl.fe.util.*, com.cannyurl.wa.service.*, com.cannyurl.common.*, com.cannyurl.util.*, com.cannyurl.app.auth.*, com.cannyurl.helper.*" 
%><%@ page contentType="text/html; charset=UTF-8" 
%><%@ page errorPage="/error/UnknownError" 
%><%
// Last update
// 3/03/13:
%><%!
private static final java.util.logging.Logger logger = java.util.logging.Logger.getLogger("auth.Twitter4jCallback");
%><%
// {1] Session
SessionBean sessionBean = UserSessionManager.getInstance().setSessionBean(request, response);
String sessionToken = sessionBean.getToken();
String userId = sessionBean.getUserId();

//[2] Parse url.
String requestUrl = request.getRequestURL().toString();
String topLevelUrl = UrlHelper.getInstance().getTopLevelURLFromRequestURL(requestUrl);
String contextPath = request.getContextPath();
String servletPath = request.getServletPath();
String pathInfo = request.getPathInfo();
String queryString = request.getQueryString();
//...
%><%
//
String comebackUrl = request.getParameter(CommonAuthUtil.PARAM_COMEBACKURL);
String fallbackUrl = request.getParameter(CommonAuthUtil.PARAM_FALLBACKURL);
//...
%><%
// logger.fine("Twitter4jCallback called.");

// TBD:
// ...

twitter4j.auth.RequestToken requestToken = (twitter4j.auth.RequestToken) session.getAttribute(Twitter4jHelper.SESSION_ATTR_TWITTER4J_REQUESTTOKEN);
if(requestToken != null) {
	String token = request.getParameter(TwitterAuthUtil.PARAM_REQUEST_TOKEN);
	String verifier = request.getParameter(TwitterAuthUtil.PARAM_TOKEN_VERIFIER);
	twitter4j.auth.AccessToken accessToken = Twitter4jHelper.getInstance().getOAuthAccessToken(session, requestToken, verifier);
	if(accessToken != null) {
		session.removeAttribute(Twitter4jHelper.SESSION_ATTR_TWITTER4J_REQUESTTOKEN);
		request.setAttribute(Twitter4jHelper.REQUEST_ATTR_TWITTER4J_ACCESSTOKEN, accessToken);
	} else {
	    // ????
	    logger.warning("Failed to get accessToken for token = " + token + "; verifier = " + verifier);
	}
} else {
    // ????
    logger.warning("Failed to get requestToken from session.");
}


%><%

// Forward (not redirect) to handler...
// Note: For forward, (relative) url path is used, not an abstract url.
//       Parameters can be passed through jsp:param, and hence there is no need to mupdate the url path....
// String authHandlerUrl = AuthHelper.getInstance().getAuthHandlerUrl(topLevelUrl, comebackUrl, fallbackUrl);
String authHandlerPage = AuthHelper.getInstance().getAuthHandlerUrlPath();
// logger.fine("authHandlerPage = " + authHandlerPage);
// ...

request.setAttribute(AuthUtil.REQUEST_ATTR_CALLBACKPAGE, "twitter4j");
%>
<jsp:forward page="<%=authHandlerPage%>" >
  <jsp:param name="comebackUrl" value="<%=comebackUrl%>" />
  <jsp:param name="fallbackUrl" value="<%=fallbackUrl%>" />
</jsp:forward>

