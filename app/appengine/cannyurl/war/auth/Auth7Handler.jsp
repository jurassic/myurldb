<%@ page import="com.myurldb.ws.core.*, com.myurldb.ws.*, com.cannyurl.af.util.*, com.cannyurl.af.auth.common.*, com.cannyurl.af.auth.*, com.cannyurl.af.auth.facebook.*, com.cannyurl.af.auth.twitter.*, com.cannyurl.af.auth.user.*, com.cannyurl.af.bean.*, com.cannyurl.fe.*, com.cannyurl.fe.core.*, com.cannyurl.fe.bean.*, com.cannyurl.fe.util.*, com.cannyurl.wa.service.*, com.cannyurl.common.*, com.cannyurl.util.*, com.cannyurl.app.auth.*, com.cannyurl.helper.*" 
%><%@ page contentType="text/html; charset=UTF-8" 
%><%@ page errorPage="/error/UnknownError" 
%><%
// Last update
// 3/03/13: Google+ SignIn (Work in progress)
// 2/13/13: Added Facebook support.
// 2/09/13: Google OpenID support.
// 1/14/13: Twitter4j-based Implementation of Twitter login.
%><%!
private static final java.util.logging.Logger logger = java.util.logging.Logger.getLogger("Auth.Handler");
%><%
// {1] Session
SessionBean sessionBean = UserSessionManager.getInstance().setSessionBean(request, response);
String sessionGuid = sessionBean.getGuid();
String sessionToken = sessionBean.getToken();
String userId = sessionBean.getUserId();

// [2] Parse url.
String requestUrl = request.getRequestURL().toString();
String topLevelUrl = UrlHelper.getInstance().getTopLevelURLFromRequestURL(requestUrl);
String contextPath = request.getContextPath();
String servletPath = request.getServletPath();
String pathInfo = request.getPathInfo();
String queryString = request.getQueryString();
// ...
%><%
// "Branding"
String appBrand = BrandingHelper.getInstance().getAppBrand();
String brandDisplayName = BrandingHelper.getInstance().getBrandDisplayName();
String brandDescription = BrandingHelper.getInstance().getBrandDescription();
%><%String themeBootstrap = SiteThemeHelper.getInstance().getBootstrapTheme();
String themeJqueryUi = SiteThemeHelper.getInstance().getJqueryUiTheme();%><%
// 
String comebackUrl = request.getParameter(CommonAuthUtil.PARAM_COMEBACKURL);
String fallbackUrl = request.getParameter(CommonAuthUtil.PARAM_FALLBACKURL);
// ...

%><%

String providerId = (String) session.getAttribute(AuthUtil.SESSION_ATTR_AUTH_PROVIDERID); 
// ...

%><%
// 
long now = System.currentTimeMillis();
boolean createNew = false;
UserAuthStateBean userAuthState = (UserAuthStateBean) session.getAttribute(AuthUtil.getSessionAttrKeyUserAuthState(providerId));
if(userAuthState == null) {
    // Reuse an existing UserAuthStateBean from DB, if any... ???
    if(userId != null) {
    	userAuthState = (UserAuthStateBean) UserAuthStateHelper.getInstance().findUserAuthStateForUser(userId, providerId);
    }
    if(userAuthState == null) {
        createNew = true;
	    userAuthState = new UserAuthStateBean();
	    userAuthState.setGuid(GUID.generate());
	    userAuthState.setProviderId(providerId);
	    // userAuthState.setFirstAuthTime(now);   // ????
	    // etc...
	    ExternalUserIdStructBean externalUserIdStruct = new ExternalUserIdStructBean();
	    externalUserIdStruct.setUuid(GUID.generate());
	    userAuthState.setExternalId(externalUserIdStruct);
	    // ...
    }
}
//userAuthState.setLastAuthTime(now);
// ....
if(userId != null) {
    userAuthState.setUser(userId);
}
// ...

// (createNew == false) only ?????
// If userAuthState is not found in the DB, why check if externalUserAuth exists in DB ???
// TBD: Need to check this ....
ExternalUserAuthBean externalUserAuth = null;
if(createNew == false && userAuthState != null) {    
    String extUserAuthGuid = userAuthState.getExternalAuth();
    externalUserAuth = (ExternalUserAuthBean) ExternalUserAuthHelper.getInstance().findExternalUserAuthByUser(userId, providerId, extUserAuthGuid);
}
// ...

%><%
// 
// logger.fine("Handler page called.");

String callbackPage = (String) request.getAttribute(AuthUtil.REQUEST_ATTR_CALLBACKPAGE);
if("twitter4j".equals(callbackPage)) {
    twitter4j.auth.AccessToken accessToken = (twitter4j.auth.AccessToken) request.getAttribute(Twitter4jHelper.REQUEST_ATTR_TWITTER4J_ACCESSTOKEN);
    if(accessToken != null) {
        logger.info("Found Twitter accessToken.");

        String authUserDisplayName = null;   // ????
        String authUserFullName = null;      // ????
        String authUserEmail = null;         // ????
        String authUserUsername = accessToken.getScreenName();
        long authUserUserId = accessToken.getUserId();

        // ??? If we have found a valid userAuthState for user, just use it... ?????
        // if(createNew == true) {
            // Try looking up the userAuthobject one more time, using username/userId ?????
            UserAuthState uas = null;
            if(authUserUserId > 0L) {
                uas = UserAuthStateHelper.getInstance().findUserAuthStateByUserId(String.valueOf(authUserUserId), providerId);
            } else if(authUserUsername != null && !authUserUsername.isEmpty()) {
	            uas = UserAuthStateHelper.getInstance().findUserAuthStateByUsername(authUserUsername, providerId);                
            }
            if(uas != null) {
                if(logger.isLoggable(java.util.logging.Level.INFO)) logger.info("Existing UserAuthState found for authUserUserId = " + authUserUserId + "; authUserUsername = " + authUserUsername);
                //
                // userId = userAuthState.getUser();
                String altUserId = uas.getUser();
                // Now, altUserId can be (will likely be) different from userId
                // What to do ?????
                if(altUserId != null && !altUserId.equals(userId)) {
	                String originalUserId = userId;
	                userId = altUserId;
	                sessionBean.setUserId(userId);
	                sessionBean = UserSessionManager.getInstance().refreshSessionBean(request, response, sessionBean);
	                if(logger.isLoggable(java.util.logging.Level.WARNING)) logger.warning("Existing UserAuthState found: userid reset to userId = " + userId + "; originalUserId = " + originalUserId);
	                // ....
                }
                userAuthState = (UserAuthStateBean) uas;
                String extAuthGuid = userAuthState.getExternalAuth();
                ExternalUserAuthBean extUserAuth = (ExternalUserAuthBean) ExternalUserAuthHelper.getInstance().findExternalUserAuthByUser(userId, providerId, extAuthGuid);
                // Since we replaced userAuthState, we need to replace externalUserAuth as well, even if it is null.... ?????
                // if(extUserAuth != null) {
                    externalUserAuth = extUserAuth;
                // }
                // ...
                // userAuthState.setExternalAuth(externalUserAuth.getGuid());
            } else {   // if(createNew == true) ????
                // User may have logged out
                // In such a case, no valid UserAuthState (e.g., with status=authenticated) will be found in the DB (if we correctly implemented the logic)
                // But, the user may still have logged on before....
                // The best thing to do is check ExternalUserAuth table....
                // ?????
                ExternalUserAuthBean extUserAuth = null;
                if(authUserUserId > 0L) {
                    extUserAuth = (ExternalUserAuthBean) ExternalUserAuthHelper.getInstance().findExternalUserAuthByUserId(authUserUserId, providerId);
                } else if(authUserUsername != null && !authUserUsername.isEmpty()) {
                    extUserAuth = (ExternalUserAuthBean) ExternalUserAuthHelper.getInstance().findExternalUserAuthByUsername(authUserUsername, providerId);
                }
                if(extUserAuth != null) {
                    if(logger.isLoggable(java.util.logging.Level.INFO)) logger.info("Existing ExternalUserAuth found for authUserUserId = " + authUserUserId + "; authUserUsername = " + authUserUsername);
                    externalUserAuth = extUserAuth;

                    String altUserId = externalUserAuth.getUser();
                    // Now, altUserId can be (will likely be) different from userId
                    // What to do ?????
                    if(altUserId != null && !altUserId.equals(userId)) {
	                    String originalUserId = userId;
	                    userId = altUserId;
	                    sessionBean.setUserId(userId);
	                    sessionBean = UserSessionManager.getInstance().refreshSessionBean(request, response, sessionBean);
	                    if(logger.isLoggable(java.util.logging.Level.WARNING)) logger.warning("Existing ExternalUserAuth found: userid reset to userId = " + userId + "; originalUserId = " + originalUserId);
	                    // ....
                    }
                    
                    String extAuthGuid = externalUserAuth.getGuid();
                    userAuthState.setExternalAuth(extAuthGuid);

                    // Do we need to do this here???
                    // ExternalUserIdStruct extUserStruct = externalUserAuth.getExternalUserId();
                    // String extAuthUserUsername = extUserStruct.getUsername();
                    // Long extAuthUserUserId = extUserStruct.getUserId();
                    // // assert extAuthUserUsername == authUserUsername  ???
                    // // assert extAuthUserUserId == authUserUserId ????
                    // userAuthState.setExternalId(extUserStruct);   // Clone ExternalUserIdStruct ???
                    // ...
                    
                }
                    
            }
        // }

        // Create/update externalUserAuth....
        externalUserAuth = Twitter4jHelper.getInstance().saveTwitterAccessToken(session, userId, accessToken, externalUserAuth);
        if(externalUserAuth != null) {
            String extAuthGuid = externalUserAuth.getGuid();
        	if(extAuthGuid != null) {
            	userAuthState.setExternalAuth(extAuthGuid);
        	} else {
            	// ???
            	userAuthState.setExternalAuth(null);  // ???
        	}
        } else {
            // ???
        	userAuthState.setExternalAuth(null);  // ???
        }
        userAuthState.setAuthToken(accessToken.getToken());
        userAuthState.setAuthStatus(AuthStatus.STATUS_AUTHENTICATED);
        // ...

        // TBD: use externalUserAuth (refreshed from DB after saving) for twitter credentials ????
        if(externalUserAuth != null) {
            authUserFullName = externalUserAuth.getFullName();
            authUserDisplayName = externalUserAuth.getDisplayName();
            authUserEmail = externalUserAuth.getEmail();  // ???
            ExternalUserIdStructBean extuserIdStructBean = (ExternalUserIdStructBean) externalUserAuth.getExternalUserId();
            if(extuserIdStructBean != null) {
                // String extAuthUserEmail = extuserIdStructBean.getEmail();
                // if(extAuthUserEmail != null && !extAuthUserEmail.isEmpty()) {
	            //     authUserEmail = extAuthUserEmail;
                // }
                authUserUsername = extuserIdStructBean.getUsername();
                // TBD:
                // authUserUserId = extuserIdStructBean.getUserId();
                String authUserUserIdStr = extuserIdStructBean.getId();
                if(authUserUserIdStr != null) {
                    try {
                        authUserUserId = Long.parseLong(authUserUserIdStr);
                    } catch(Exception e) {
                        // ignore
                    }
                }
                // ....
                // ?????
                if(authUserFullName == null || authUserFullName.isEmpty()) {
                    authUserFullName = extuserIdStructBean.getName(); 
                }
                if(authUserEmail == null || authUserEmail.isEmpty()) {
                    authUserEmail = extuserIdStructBean.getEmail();
                }
                // ?????
            }
        }          
        
  	    ExternalUserIdStructBean extUserIdStruct = (ExternalUserIdStructBean) userAuthState.getExternalId();
        if(extUserIdStruct == null) {
	  	    extUserIdStruct = new ExternalUserIdStructBean();
	        extUserIdStruct.setUuid(GUID.generate());
	        userAuthState.setExternalId(extUserIdStruct);
	    }
        // ????
        if(authUserFullName != null && !authUserFullName.isEmpty()) {
            extUserIdStruct.setName(authUserFullName);
        } else if(authUserDisplayName != null && !authUserDisplayName.isEmpty()) {
            // ????
            // userAuthState.setDisplayName(authUserDisplayName);
            extUserIdStruct.setName(authUserDisplayName);
        }
        if(authUserEmail != null && !authUserEmail.isEmpty()) {
            userAuthState.setEmail(authUserEmail);
            extUserIdStruct.setEmail(authUserEmail);
        }
        if(authUserUsername != null && !authUserUsername.isEmpty()) {
            userAuthState.setUsername(authUserUsername);
            extUserIdStruct.setUsername(authUserUsername);
        }
        if(authUserUserId > 0) {
            extUserIdStruct.setId(String.valueOf(authUserUserId));
        }
        userAuthState.setExternalId(extUserIdStruct);   // This should not be necessary...  ????
        // ...
        
    } else {
        logger.info("Failed to find accessToken.");
        userAuthState.setAuthStatus(AuthStatus.STATUS_AUTH_FAILED);  // ???        
    }
    // ...
    // TBD: Update authUser...
    // ....
    
} else if("socialauth".equals(callbackPage)) {
    // TBD:

    org.brickred.socialauth.util.AccessGrant accessGrant = (org.brickred.socialauth.util.AccessGrant) request.getAttribute(SocialAuthHelper.REQUEST_ATTR_SOCIALAUTH_ACCESSGRANT);
    if(accessGrant != null) {
        logger.info("Found SocialAuth accessGrant.");
        
        // TBD:
        // Note that the providerId used in SocialAuth library may not coincide the providerId string we use...
        // We need a conversion table.....
        String socialAuthProviderId = accessGrant.getProviderId();
        if(providerId == null) {
            providerId = socialAuthProviderId;  // ???
        } else {
            if(socialAuthProviderId != null) {
	            if(! providerId.equals(socialAuthProviderId)) {
	                // Can this happen ?????
	                logger.warning("Session providerId is different from SocialAuth providerId: providerId= " + providerId + "; socialAuthProviderId = " + socialAuthProviderId);
	                providerId = socialAuthProviderId;  // ???                
	            }
            } else {
                // ?????
                // Can this happen????
            }
        }

        String accessGrantKey = accessGrant.getKey();
        String accessGrantSecret = accessGrant.getSecret();

        String authUserFullName = null;      // ????
        String authUserDisplayName = null;   // ????
        String authUserEmail = null;         // ????
        String authUserUsername = null;      // ????
        Long authUserUserId = null;          // ????
        
        // ????
        org.brickred.socialauth.Profile userProfile = (org.brickred.socialauth.Profile) request.getAttribute(SocialAuthHelper.REQUEST_ATTR_SOCIALAUTH_USERPROFILE);
        if(userProfile != null) {
            logger.info("Found SocialAuth userProfile.");

            authUserFullName = userProfile.getFullName();
            authUserDisplayName = userProfile.getDisplayName();
            authUserEmail = userProfile.getEmail();
            String authUserId = userProfile.getValidatedId();   // ????
            if(authUserId != null && !authUserId.isEmpty()) {
                // ????
                Long longId = null;
                try {
                    longId = Long.valueOf(authUserId);
                } catch(Exception e) {
                    // ignore...
                }
                if(longId != null) {
                    authUserUserId = longId; 
                } else {
                    authUserUsername = authUserId;
                }
            }
             if(logger.isLoggable(java.util.logging.Level.FINE)) {
                 logger.fine("authUserFullName = " + authUserFullName);
                 logger.fine("authUserDisplayName = " + authUserDisplayName);
                 logger.fine("authUserEmail = " + authUserEmail);
                 logger.fine("authUserUsername = " + authUserUsername);
                 logger.fine("authUserUserId = " + authUserUserId);
             }
                    
        } else {
            // ???
            logger.warning("SocialAuth userProfile not found in the request.");
        }

        if(providerId != null) {
            if(providerId.equals(CommonAuthUtil.AUTH_SERVICE_PROVIDERID_TWITTER)) {
                // ....
            } else if(providerId.equals(CommonAuthUtil.AUTH_SERVICE_PROVIDERID_FACEBOOK)) {
                // ....            
            } else {
                // etc...
                // ...
            }
            
        } else {
            // ???
        }

        
        // ??? If we have found a valid userAuthState for user, just use it... ?????
        // if(createNew == true) {
            // Try looking up the userAuthobject one more time, using username/userId ?????
            UserAuthState uas = null;
            if(authUserUserId > 0L) {
	            // uas = UserAuthStateHelper.getInstance().findUserAuthStateByUserId(authUserUserId, providerId);
	            uas = UserAuthStateHelper.getInstance().findUserAuthStateByUserId(String.valueOf(authUserUserId), providerId);
            } else if(authUserUsername != null && !authUserUsername.isEmpty()) {
	            uas = UserAuthStateHelper.getInstance().findUserAuthStateByUsername(authUserUsername, providerId);                
            }
            if(uas != null) {
                if(logger.isLoggable(java.util.logging.Level.INFO)) logger.info("Existing UserAuthState found for authUserUserId = " + authUserUserId + "; authUserUsername = " + authUserUsername);
                //
                // userId = userAuthState.getUser();
                String altUserId = uas.getUser();
                // Now, altUserId can be (will likely be) different from userId
                // What to do ?????
                if(altUserId != null && !altUserId.equals(userId)) {
	                String originalUserId = userId;
	                userId = altUserId;
	                sessionBean.setUserId(userId);
	                sessionBean = UserSessionManager.getInstance().refreshSessionBean(request, response, sessionBean);
	                if(logger.isLoggable(java.util.logging.Level.WARNING)) logger.warning("Existing UserAuthState found: userid reset to userId = " + userId + "; originalUserId = " + originalUserId);
	                // ....
                }
                userAuthState = (UserAuthStateBean) uas;
                String extAuthGuid = userAuthState.getExternalAuth();
                ExternalUserAuthBean extUserAuth = (ExternalUserAuthBean) ExternalUserAuthHelper.getInstance().findExternalUserAuthByUser(userId, providerId, extAuthGuid);
                // Since we replaced userAuthState, we need to replace externalUserAuth as well, even if it is null.... ?????
                // if(extUserAuth != null) {
                    externalUserAuth = extUserAuth;
                // }
                // ...
                // userAuthState.setExternalAuth(externalUserAuth.getGuid());
            } else {   // if(createNew == true) ????
                // User may have logged out
                // In such a case, no valid UserAuthState (e.g., with status=authenticated) will be found in the DB (if we correctly implemented the logic)
                // But, the user may still have logged on before....
                // The best thing to do is check ExternalUserAuth table....
                // ?????
                ExternalUserAuthBean extUserAuth = null;
                if(authUserUserId > 0L) {
                    extUserAuth = (ExternalUserAuthBean) ExternalUserAuthHelper.getInstance().findExternalUserAuthByUserId(authUserUserId, providerId);
                } else if(authUserUsername != null && !authUserUsername.isEmpty()) {
                    extUserAuth = (ExternalUserAuthBean) ExternalUserAuthHelper.getInstance().findExternalUserAuthByUsername(authUserUsername, providerId);
                }
                if(extUserAuth != null) {
                    if(logger.isLoggable(java.util.logging.Level.INFO)) logger.info("Existing ExternalUserAuth found for authUserUserId = " + authUserUserId + "; authUserUsername = " + authUserUsername);
                    externalUserAuth = extUserAuth;

                    String altUserId = externalUserAuth.getUser();
                    // Now, altUserId can be (will likely be) different from userId
                    // What to do ?????
                    if(altUserId != null && !altUserId.equals(userId)) {
	                    String originalUserId = userId;
	                    userId = altUserId;
	                    sessionBean.setUserId(userId);
	                    sessionBean = UserSessionManager.getInstance().refreshSessionBean(request, response, sessionBean);
	                    if(logger.isLoggable(java.util.logging.Level.WARNING)) logger.warning("Existing ExternalUserAuth found: userid reset to userId = " + userId + "; originalUserId = " + originalUserId);
	                    // ....
                    }
                    
                    String extAuthGuid = externalUserAuth.getGuid();
                    userAuthState.setExternalAuth(extAuthGuid);

                    // Do we need to do this here???
                    // ExternalUserIdStruct extUserStruct = externalUserAuth.getExternalUserId();
                    // String extAuthUserUsername = extUserStruct.getUsername();
                    // Long extAuthUserUserId = extUserStruct.getUserId();
                    // // assert extAuthUserUsername == authUserUsername  ???
                    // // assert extAuthUserUserId == authUserUserId ????
                    // userAuthState.setExternalId(extUserStruct);   // Clone ExternalUserIdStruct ???
                    // ...
                    
                }
                    
            }
        // }

        // Create/update externalUserAuth....
        externalUserAuth = SocialAuthHelper.getInstance().saveSocialAuthAccessGrant(session, userId, providerId, accessGrant, userProfile, externalUserAuth);
        if(externalUserAuth != null) {
            String extAuthGuid = externalUserAuth.getGuid();
        	if(extAuthGuid != null) {
            	userAuthState.setExternalAuth(extAuthGuid);
        	} else {
            	// ???
            	userAuthState.setExternalAuth(null);  // ???
        	}
        } else {
            // ???
        	userAuthState.setExternalAuth(null);  // ???
        }
        userAuthState.setAuthToken(accessGrantKey);
        userAuthState.setAuthStatus(AuthStatus.STATUS_AUTHENTICATED);
        // ...

        // TBD: use externalUserAuth (refreshed from DB after saving) for auth credentials ????
        if(externalUserAuth != null) {
            authUserFullName = externalUserAuth.getFullName();
            authUserDisplayName = externalUserAuth.getDisplayName();
            authUserEmail = externalUserAuth.getEmail();  // ???
            ExternalUserIdStructBean extuserIdStructBean = (ExternalUserIdStructBean) externalUserAuth.getExternalUserId();
            if(extuserIdStructBean != null) {
                // String extAuthUserEmail = extuserIdStructBean.getEmail();
                // if(extAuthUserEmail != null && !extAuthUserEmail.isEmpty()) {
	            //     authUserEmail = extAuthUserEmail;
                // }
                authUserUsername = extuserIdStructBean.getUsername();
                // TBD:
                // authUserUserId = extuserIdStructBean.getUserId();
                String authUserUserIdStr = extuserIdStructBean.getId();
                if(authUserUserIdStr != null) {
                    try {
                        authUserUserId = Long.parseLong(authUserUserIdStr);
                    } catch(Exception e) {
                        // ignore
                    }
                }
                // ....
                // ?????
                if(authUserFullName == null || authUserFullName.isEmpty()) {
                    authUserFullName = extuserIdStructBean.getName(); 
                }
                if(authUserEmail == null || authUserEmail.isEmpty()) {
                    authUserEmail = extuserIdStructBean.getEmail();
                }
                // ?????
            }
        }

        ExternalUserIdStructBean extUserIdStruct = (ExternalUserIdStructBean) userAuthState.getExternalId();
        if(extUserIdStruct == null) {
	  	    extUserIdStruct = new ExternalUserIdStructBean();
	        extUserIdStruct.setUuid(GUID.generate());
	        userAuthState.setExternalId(extUserIdStruct);
	    }
        // ????
        if(authUserFullName != null && !authUserFullName.isEmpty()) {
            extUserIdStruct.setName(authUserFullName);
        } else if(authUserDisplayName != null && !authUserDisplayName.isEmpty()) {
            // ????
            // // userAuthState.setDisplayName(authUserDisplayName);
            // extUserIdStruct.setName(authUserDisplayName);
        }
        if(authUserEmail != null && !authUserEmail.isEmpty()) {
            userAuthState.setEmail(authUserEmail);
            extUserIdStruct.setEmail(authUserEmail);
        }
        if(authUserUserId != null && authUserUserId > 0L) {
            extUserIdStruct.setId(String.valueOf(authUserUserId));
        }
        // ????
        if(authUserUsername != null && !authUserUsername.isEmpty()) {
            userAuthState.setUsername(authUserUsername);
            extUserIdStruct.setUsername(authUserUsername);
        } else if(authUserUserId != null && authUserUserId > 0L) {
            // ???
            // userAuthState.setUsername(authUserUserId.toString());
            // extUserIdStruct.setUsername(authUserUserId.toString());            
        }
        userAuthState.setExternalId(extUserIdStruct);   // This should not be necessary...  ????
        // ...

        // etc....
        
    } else {
        logger.warning("Failed to find request accessGrant.");
        userAuthState.setAuthStatus(AuthStatus.STATUS_AUTH_FAILED);  // ???        
    }

    request.removeAttribute(SocialAuthHelper.REQUEST_ATTR_SOCIALAUTH_ACCESSGRANT);
    request.removeAttribute(SocialAuthHelper.REQUEST_ATTR_SOCIALAUTH_USERPROFILE);

} else if(CommonAuthUtil.AUTH_SERVICE_PROVIDERID_TWITTER.equals(callbackPage)) {
    // TBD:
    
} else if(CommonAuthUtil.AUTH_SERVICE_PROVIDERID_FACEBOOK.equals(callbackPage)) {
    // TBD:

        String authToken = (String) request.getAttribute(FacebookAuthUtil.REQUEST_ATTR_ACCESS_TOKEN);
    Integer expires = (Integer) request.getAttribute(FacebookAuthUtil.REQUEST_ATTR_EXPIRES);

    if(authToken != null) {
        // ???
        String extAuthGuid = AuthHelper.getInstance().saveAcessToken(CommonAuthUtil.AUTH_SERVICE_PROVIDERID_FACEBOOK, userId, authToken, expires);
        if(extAuthGuid != null) {
            userAuthState.setExternalAuth(extAuthGuid);
 	        userAuthState.setAuthToken(authToken);
 	        userAuthState.setAuthStatus(AuthStatus.STATUS_AUTHENTICATED);
 	        // ...
        } else {
            // ???
        }
    } else {
        userAuthState.setAuthStatus(AuthStatus.STATUS_AUTH_FAILED);  // ???        
    }

    request.removeAttribute(FacebookAuthUtil.REQUEST_ATTR_ACCESS_TOKEN);
    request.removeAttribute(FacebookAuthUtil.REQUEST_ATTR_EXPIRES);
    
    // TBD:

} else {
    // gae-openid or googleapps...
    
    // TBD: Has GAE updated the user data at this point????
    AuthUserService authUserService = AuthUserServiceFactory.getAuthUserService();
    AuthUser authUser = authUserService.getCurrentUser(); // or req.getUserPrincipal()
    if(authUser != null) {
        
        String gaeOpenId = authUser.getNickname();      // ????
        // String authDomain = authUser.getAuthDomain();
        // String authUserGuid = AuthConfigManager.getInstance().findUserGuidByGaeNicknameAndAuthDomain(gaeOpenId, authDomain);
        String federatedIdentity = authUser.getFederatedIdentity();   // TBD: Can this be null/empty ????
        String authUserGuid = AuthConfigManager.getInstance().findUserGuidByGaeNicknameAndFederatedIdentity(gaeOpenId, federatedIdentity);
        if(authUserGuid == null) {
            authUser.setUserGuid(userId);
        } else {
            if(!authUserGuid.equals(userId)) {
                // ???
                // What to do????
                //authUser.setUserGuid(userId);
                // ???
            }
        }
        
        // ...
        String authUserName = authUser.getNickname();
        String authUserEmail = authUser.getEmail();
        String authUserUsername = null;
        // Long authUserUserId = null;
        // String authUserIdentifier = authUser.getUserId();    // ???
        // if(authUserIdentifier != null && !authUserIdentifier.isEmpty()) {
        //     // ????
        //     Long longId = null;
        //     try {
        //         longId = Long.valueOf(authUserIdentifier);
        //     } catch(Exception e) {
        //         // ignore...
        //     }
        //     if(longId != null) {
        //         authUserUserId = longId; 
        //     } else {
        //         authUserUsername = authUserIdentifier;
        //     }
        // }
        String authUserUserId = authUser.getUserId();    // ???

        ExternalUserIdStructBean extUserIdStruct = (ExternalUserIdStructBean) userAuthState.getExternalId();
        if(extUserIdStruct == null) {
	  	    extUserIdStruct = new ExternalUserIdStructBean();
	        extUserIdStruct.setUuid(GUID.generate());
	        userAuthState.setExternalId(extUserIdStruct);
	    }
        if(authUserName != null && !authUserName.isEmpty()) {
            // userAuthState.setDisplayName(authUserName);   // ???
            extUserIdStruct.setName(authUserName);
        }
        if(authUserEmail != null && !authUserEmail.isEmpty()) {
            userAuthState.setEmail(authUserEmail);
            extUserIdStruct.setEmail(authUserEmail);
        }
//        if(authUserUsername != null && !authUserUsername.isEmpty()) {
//            userAuthState.setUsername(authUserUsername);
//            extUserIdStruct.setUsername(authUserUsername);
//        }
        if(authUserUserId != null && ! authUserUserId.isEmpty()) {
             extUserIdStruct.setId(authUserUserId);
        }
        if(federatedIdentity != null && !federatedIdentity.isEmpty()) {
            extUserIdStruct.setOpenId(federatedIdentity);
        }
        userAuthState.setExternalId(extUserIdStruct);   // This should not be necessary... ????
        // ....
        
        
        // TBD: 
        if(AuthUtil.SESSION_ATTR_AUTH_MODE.equals("googleapps")) {
            // googleapps
            
        } else {
            // gae-openid
            
        }
        
        // TBD: how to set federatedIdentity for authUser ????
//    String newFederatedIdentity = (String) session.getAttribute(AuthUtil.SESSION_ATTR_AUTH_FEDERATEDIDENTITY);
//    if(newFederatedIdentity != null && !newFederatedIdentity.isEmpty()) {
//        if(federatedIdentity != null && !federatedIdentity.isEmpty()) {
//            if(! federatedIdentity.equals(newFederatedIdentity)) {
//                // Something's wrong...
//                // What to do ???
//            }
//        }
//        // authUser.setFederatedIdentity(newFederatedIdentity);   // This does not work...
//    } else {
//        // ?????
//    }


        // Create or Update the user...
        Boolean created = AuthConfigManager.getInstance().createOrUpdateUser(userId, sessionGuid, authUser);

        userAuthState.setAuthStatus(AuthStatus.STATUS_AUTHENTICATED);
        // ....

    } else {
        // ????
        userAuthState.setAuthStatus(AuthStatus.STATUS_AUTH_FAILED);  // ???  
    }
    
}


%><%

// TBD...
// Create UserAuthState
// Store it in DB
// and, store it in the Session...
// ...

boolean successfullyLoggedIn = false;

boolean serviceAuthencticated = false;
if(userAuthState != null) {
	if(AuthStatus.STATUS_AUTHENTICATED.equals(userAuthState.getAuthStatus())) {
	    serviceAuthencticated = true;
	} else {
	    serviceAuthencticated = false;
	}
}

// ???
// if(serviceAuthencticated == true) {
    successfullyLoggedIn = AuthManager.getInstance().processUserLogIn(session, providerId, userAuthState, createNew);
// }
if(logger.isLoggable(java.util.logging.Level.FINE)) logger.fine("successfullyLoggedIn = " + successfullyLoggedIn);

/*
userAuthState = (UserAuthStateBean) UserAuthStateHelper.getInstance().saveUserAuthState(userAuthState, createNew);
if(userAuthState != null) {
	session.setAttribute(AuthUtil.getSessionAttrKeyUserAuthState(providerId), userAuthState);
	// ...
	AuthToken authToken = (AuthToken) session.getAttribute(CommonAuthUtil.SESSION_ATTR_AUTHTOKEN);
	// ...
	String authStatus = userAuthState.getAuthStatus();
	if(AuthStatus.STATUS_AUTHENTICATED.equals(authStatus)) {
		String username = null;
		ExternalUserIdStruct extUserId = userAuthState.getExternalId();
		if(extUserId != null) {
		    // TBD: use openId, etc. ????
		    username = extUserId.getUsername();
		}
		if(authToken == null) {
		    authToken = new AuthToken();
		}
		authToken.addAuthState(providerId, username);
		session.setAttribute(CommonAuthUtil.SESSION_ATTR_AUTHTOKEN, authToken);
	} else {
		if(authToken != null) {
		    authToken.removeAuthState(providerId);
			session.setAttribute(CommonAuthUtil.SESSION_ATTR_AUTHTOKEN, authToken);
		}
	}
} else {
    // ???
}
*/
// ...

// TBD:
// All UserAuthStates should be removed from the session
// when the user logs out...
// ...


%><%
// ???
request.removeAttribute(AuthUtil.REQUEST_ATTR_CALLBACKPAGE);
session.removeAttribute(AuthUtil.SESSION_ATTR_AUTH_MODE);
session.removeAttribute(AuthUtil.SESSION_ATTR_AUTH_PROVIDERID); 
session.removeAttribute(AuthUtil.SESSION_ATTR_AUTH_FEDERATEDIDENTITY);
// ...
%><%
// ...

// Redirect
// ????
if(successfullyLoggedIn == true) {
	if(comebackUrl != null) {
	    response.sendRedirect(comebackUrl);
	    // response.setStatus(302);   // temporary redirect
	    // response.setHeader( "Location", comebackUrl );
	    // response.setHeader( "Connection", "close" );
	} else {
	    // ????? 
	    // Bug: We somehow lose comebackUrl param after authentication (e.g., usign gmail)
	    // Why ???
	    // What to do ???
	    // Just redirect to "/home", for now.   ????
	    response.sendRedirect("/home");
	    // ????
	}
} else {
	if(fallbackUrl != null) {
	    response.sendRedirect(fallbackUrl);
	    // response.setStatus(302);   // temporary redirect
	    // response.setHeader( "Location", fallbackUrl );
	    // response.setHeader( "Connection", "close" );
	} else {
	    // ????? 
	    response.sendRedirect("/error/AuthFailure");
	    // ????
	}
}

%><!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!-- Consider adding an manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<html>
  <head>
    <meta charset="utf-8">
    <title><%=brandDisplayName%> | Auth</title>
    <meta name="author" content="Aery Software">
    <meta name="description" content="<%=AppBrand.getBrandDescription(appBrand)%>.">

    <link rel="shortcut icon" href="/img/favicon-<%=appBrand%>.ico" />
    <link rel="icon" href="/img/favicon-<%=appBrand%>.ico" type="image/x-icon" />

    <!-- Mobile viewport optimized: j.mp/bplateviewport -->
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <!-- CSS: implied media=all -->
    <%@ include file="/fragment/style.jspf" %>
    <link rel="stylesheet" type="text/css" href="/css/cannyurl-<%=appBrand%>.css"/>
    <!-- end CSS-->
    
    <!-- All JavaScript at the bottom, except for Modernizr / Respond.
       Modernizr enables HTML5 elements & feature detects; Respond is a polyfill for min/max-width CSS3 Media Queries
       For optimal performance, use a custom Modernizr build: www.modernizr.com/download/ -->
    <script src="http://ww2.filestoa.com/js/modernizr/modernizr-2.0.6.min.js"></script>

  </head>

  <body>

    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="i-bar"></span>
            <span class="i-bar"></span>
            <span class="i-bar"></span>
          </a>
          <a class="brand" href="/home"><%=brandDisplayName%> <sup>&beta;</sup></a>
          <div class="nav-collapse">
            <ul class="nav">
<!-- 
              <li><a id="topmenu_nav_home" href="/home">Home</a></li>
-->
              <li><a id="topmenu_nav_about" href="/about" title="About <%=brandDisplayName%>">About</a></li>
<!--
              <li><a id="topmenu_nav_contact" href="/contact" title="Contact information">Contact</a></li>
              <li><a id="topmenu_nav_blog" href="/blog" title="<%=brandDisplayName%> blog">Blog</a></li>
-->
            </ul>
            <p class="navbar-text pull-right">
            </p>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container-fluid">



<%
//java.security.Principal principle = request.getUserPrincipal();
AuthUserService authUserService = AuthUserServiceFactory.getAuthUserService();
AuthUser authUser = authUserService.getCurrentUser(); // or req.getUserPrincipal()
%>

<%
if(authUser != null) {
    String nickName = authUser.getNickname(); 
    String logoutUrl = authUserService.createLogoutURL(request.getRequestURI());
%>
      <div id="main" class="hero-unit">
Successfully logged on: <%=nickName%>
<br/>
Go to <a href="/home">Home</a>.
<!--
<br/>
<a href="<%=logoutUrl%>">Logout</a>
-->
      </div>
<%
%>
<%
} else {
    // Note:
    // When using GAE OpenID framework,

    
    
    if(comebackUrl != null && !comebackUrl.isEmpty()) {
        // ???
        response.sendRedirect(comebackUrl); 
    }
    
    
    
    String authUrl = request.getRequestURI();
    if(queryString != null && !queryString.isEmpty()) {
        authUrl += "?" + queryString;
    }
%>



<%
}
%>


      <hr>

      <footer>
        <p>&copy; <%=brandDisplayName%> 2012&nbsp; <a id="anchor_feedback_footer" href="#" title="Feedback"><i class="icon-comment"></i></a></p>
      </footer>
    </div> <!--! end of #container -->


    <div id="div_status_floater" style="display: none;">
      <div id="status_message">
        <span id="span_status_message">(Status)</span>
      </div>
    </div>
    
    
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="http://ww2.filestoa.com/js/jquery/jquery-1.8.3.min.js"><\/script>')</script>
	<script src="http://ww2.filestoa.com/js/jquery/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="http://ww2.filestoa.com/js/jquery/plugin/jquery.floatobject-1.4.js"></script>
	<script src="/js/openid/openid-jquery.js"></script>
	<script src="/js/openid/openid-en.js"></script>
    <script src="http://ww2.filestoa.com/js/bootstrap/bootstrap-2.0.js"></script>
    <script type="text/javascript" src="/js/core/debugutil.js"></script>
    <script type="text/javascript" src="http://ww2.filestoa.com/js/core/datetimeutil-1.0.js"></script>
    <script type="text/javascript" src="http://ww2.filestoa.com/js/core/emailutil-1.0.js"></script>
	<script defer type="text/javascript" src="/js/plugins.js"></script>
    <script defer type="text/javascript" src="/js/script.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/fiveten-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/statushelper-1.1.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/tweethelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/signuphelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/feedbackhelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/emailhelper-1.0.js"></script>
    <!-- end scripts-->



    <script>
        // Global vars
    	var statusHelper;
    	var tweetHelper;
    	var signupHelper;
    	var feedbackHelper;
    	var emailHelper;
    </script>
    <script>
    $(function() {
    	// Init...
    	statusHelper = new webstoa.StatusHelper(400, 65);
    	// test.
    	//statusHelper.update('test status message');

    	var servicename = "<%=brandDisplayName%>";
    	var launchname = "Initial pre-launch trial";
    	var pageid = "home";
    	signupHelper = new webstoa.SignupHelper(servicename, launchname, pageid);

    	var targetService = "<%=brandDisplayName%>";
    	var targetPage = "home";
    	feedbackHelper = new webstoa.FeedbackHelper(targetService, targetPage);

    	var senderEmail = webstoa.EmailUtil.decodeEmailAddress('gaeemail+gmail' + '+com');
    	var defaultSenderName = "<%=brandDisplayName%>";
    	var subject = "Interesting site: <%=brandDisplayName%>";
    	var defaultMessage = "Hi,\n\n<%=brandDisplayName%>, http://www.cannyurl.com/, is a scratch paper for Web surfers. Please check it out.\n\n-me\n";
    	emailHelper = new webstoa.EmailHelper(senderEmail, subject, defaultMessage, defaultSenderName);
    });
    </script>

    <script>
    // Email address "decoding"...
    $(function() {
        $("a").each(function() {
        	var anchor = $(this);
        	webstoa.EmailUtil.decodeMailtoAnchor(anchor);
        });
    });
    </script>

    <script>
    $(function() {
        $("#anchor_feedback_footer").click(function() {
            if(DEBUG_ENABLED) console.log("Feedback button clicked."); 

            var targetItem = 'footer';
            feedbackHelper.feedback(targetItem);
		    return false;
        });
    });
    </script>



    <script>
    // Function definitions.
    function updateStatus(msg, type, time) {
    	if(!time) {
    		time = 5000;   // milliseconds.
    	}
    	if(!type) {
    		// type = "info";  // warning, error, etc....
    	}
    	//$("#div_status_floater").text(msg);
    	//$("#div_status_floater").fadeIn().delay(time).fadeOut();
		$("#div_status_floater").textInOut(msg, type, time);
    }    
    </script>

    <script>
    // temporary
    //function replaceURLWithHTMLLinks(text) {
    //    var exp = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
    //    //var exp = /\(?\bhttp:\/\/[-A-Za-z0-9+&@#\/%?=~_()|!:,.;]*[-A-Za-z0-9+&@#/%=~_()|]/ig;
    //    return text.replace(exp,"<a href='$1'>$1</a>"); 
    //}
    </script>

    <script>
    // "Init"
    $(function() {
    	// TBD: initially hide $("#div_status_floater") ...
    	$("#div_status_floater").text("").hide();
    
        // Background image test.
        //$("#container").css("background-image", 'url(/img/bg/oceansunset.jpg)');
        //$(document.body).css("background", "silver");
    	//$("#container").css("background", "ivory");

        // TBD: Location...
    	var params = {x: 530, y: 65, speed: 'normal' };
    	$("#div_status_floater").makeFloat(params);
    	
    	// temporary
    	// linkify the content
    	//var contentOriginal = $("#memo_auth_content").html();
    	//var contentLinkified = replaceURLWithHTMLLinks(contentOriginal);
    	//$("#memo_auth_content").html(contentLinkified);
    	// temporary
    	
    	// Peeng?
    	//var fiveTenTimer = new fiveten.FiveTenTimer(21889, 12);  // ???
    	//fiveTenTimer.start();
    });
    </script>


    <script>
    // App-related vars.
    var userGuid;
<%
if(userId != null) {
%>
    userGuid = "<%=userId%>";
<%
}
%>
    if(DEBUG_ENABLED) console.log("userGuid = " + userGuid);
    </script>


	<script type="text/javascript">
		$(document).ready(function() {
			openid.init('openid_identifier');
			//openid.setDemoMode(true); //Stops form submission for client javascript-only test purposes
		});
	</script>


<%
String googleAnalyticsCode = AnalyticsHelper.getInstance().getGoogleAnalyticsCode();
if(googleAnalyticsCode != null && !googleAnalyticsCode.isEmpty()) {    // And, if it is enabled (e.g. through config). TBD...
%>
    <!-- Change UA-XXXXX-X to be your site's ID -->
    <script>
      window._gaq = [['_setAccount','<%=googleAnalyticsCode%>'],['_trackPageview'],['_trackPageLoadTime']];
      Modernizr.load({
        load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
      });
    </script>
<%
}
%>

    <!-- Prompt IE 6 users to install Chrome Frame. Remove this if you want to support IE 6.
       chromium.org/developers/how-tos/chrome-frame-getting-started -->
    <!--[if lt IE 7 ]>
      <script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
      <script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
    <![endif]-->

    </body>
</html>