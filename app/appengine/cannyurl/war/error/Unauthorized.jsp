<%@ page import="com.cannyurl.fe.*, com.cannyurl.fe.bean.*, com.cannyurl.util.*, com.cannyurl.helper.*" %>
<%@ page isErrorPage="true" 
%><%
// "Branding"
String appBrand = BrandingHelper.getInstance().getAppBrand();
String brandDisplayName = BrandingHelper.getInstance().getBrandDisplayName();
String brandDescription = BrandingHelper.getInstance().getBrandDescription();
%><%
    String themeBootstrap = SiteThemeHelper.getInstance().getBootstrapTheme();
String themeJqueryUi = SiteThemeHelper.getInstance().getJqueryUiTheme();
%><%
%><!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Unauthorized | <%=brandDisplayName%></title> 

    <link rel="shortcut icon" href="/img/favicon-<%=appBrand%>.ico" />
    <link rel="icon" href="/img/favicon-<%=appBrand%>.ico" type="image/x-icon" />

    <!-- Le styles -->
    <%@ include file="/fragment/style.jspf" %>
    <link rel="stylesheet" type="text/css" href="/css/cannyurl-<%=appBrand%>.css"/>

  </head>

  <body>
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="brand" href="/home"><%=brandDisplayName%> <sup>&beta;</sup></a>
          <div class="nav-collapse">
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container-fluid">

      <!-- Main hero unit for a primary marketing message or call to action -->
      <div id="main" class="hero-unit">
        <h1>Unauthorized</h1>
		<p>
You are not authorized to access this page.
<br/>
-- Back to <a href="/home">Home</a>.
		</p>
      </div>

      <hr>

      <footer>
        <p>&copy; <%=brandDisplayName%> 2013</p>
      </footer>
    </div> <!-- /container -->

  </body>
</html>
