<%@ page import="com.myurldb.ws.*, com.cannyurl.af.util.*, com.cannyurl.af.auth.common.*, com.cannyurl.af.auth.*, com.cannyurl.af.auth.user.*, com.cannyurl.common.*, com.cannyurl.util.*, com.cannyurl.app.auth.*, com.cannyurl.app.util.*, com.cannyurl.helper.*"
%><%
boolean isAppAuthDisabled = ConfigUtil.isApplicationAuthDisabled();
//boolean isAppAuthOptional = ConfigUtil.isApplicationAuthOptional();
String appAuthMode = ConfigUtil.getApplicationAuthMode();
boolean isUsingGoogleAuth = false;
boolean isUsingTwitterAuth = false;
boolean isUsingFacebookAuth = false;
if(AppAuthMode.MODE_GOOGLE.equals(appAuthMode)) {
 isUsingGoogleAuth = true;
} else if(AppAuthMode.MODE_TWITTER.equals(appAuthMode)) {
 isUsingTwitterAuth = true;
} else if(AppAuthMode.MODE_FACEBOOK.equals(appAuthMode)) {
 isUsingFacebookAuth = true;
}
%><%
String providerId = null;
boolean isUserAuthenticated = false;
// TBD:
// Long userUserId = null;
String userUserId = null;
// ...
String userUsername = null;
String userEmail = null;
String userName = null;
String userOpenId = null;
String loginUrl = null;
String logoutUrl = null;
RequestAuthStateBean authStateBean = (RequestAuthStateBean) request.getAttribute(AuthUtil.REQUEST_ATTR_AUTHSTATEBEAN);
if(authStateBean != null) {
    isUserAuthenticated = authStateBean.isAuthenticated();
    if(isUserAuthenticated) {
        logoutUrl = authStateBean.getLogoutUrl();
        userUserId = authStateBean.getUserId();
        userUsername = authStateBean.getUsername();
        userEmail = authStateBean.getEmail();
        userName = authStateBean.getName();
        userOpenId = authStateBean.getOpenId();
    } else {
        loginUrl = authStateBean.getLoginUrl();
    }
}
%><%
String twitterHandle = "";
String twitterUserProfileUrl = "";
if(isUsingTwitterAuth) {
	if(userUsername != null && !userUsername.isEmpty()) {
	    twitterHandle = "@" + userUsername;      // ???
	    twitterUserProfileUrl = "http://twitter.com/" + userUsername;
	}
}
%><%
// ?????
boolean isShowingInfoLink = ConfigUtil.isApplicationIngressDBEnabled() && ConfigUtil.isApplicationUrlTallyEnabled();
// .....
%><%
String pageCode = request.getParameter("pageCode");
String brandDisplayName = request.getParameter("brandDisplayName");
// ...
%>

    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="i-bar"></span>
            <span class="i-bar"></span>
            <span class="i-bar"></span>
          </a>
          <a class="brand" href="/home"><%=brandDisplayName%> <sup>&beta;</sup></a>
          <div class="nav-collapse">
            <ul class="nav">
<%
if(DebugUtil.isBetaFeatureEnabled()) {
%>
<%
if(DebugUtil.isDevelFeatureEnabled()) {
%>
              <li <%if(pageCode.equals("shortlink/stream")){%>class="active"<%}%>><a id="topmenu_nav_stream" href="/stream" title="Short URL stream">Stream</a></li>
<%
}  // Devel
%>
<%
}  // Beta
%>
              <li <%if(pageCode.equals("shortlink/edit")){%>class="active"<%}%>><a id="topmenu_nav_shorten" href="/shorten" title="Shorten URL or update short URL">Shorten</a></li>
              <li <%if(pageCode.equals("shortlink/view")){%>class="active"<%}%>><a id="topmenu_nav_view" href="/view" title="Follow short URL">View</a></li>
              <li <%if(pageCode.equals("shortlink/verify")){%>class="active"<%}%>><a id="topmenu_nav_verify" href="/verify" title="Verify a short URL">Verify</a></li>
<%
if(isAppAuthDisabled == true || isUserAuthenticated == true) {  
%>
<% if(isShowingInfoLink == true) { %>
              <li <%if(pageCode.equals("shortlink/info")){%>class="active"<%}%>><a id="topmenu_nav_info" href="/info" title="View Short URL information">Info</a></li>
<% } %>
              <li <%if(pageCode.equals("shortlink/list")){%>class="active"<%}%>><a id="topmenu_nav_list" href="/list" title="View my short URL list">List</a></li>
<%
}
%>

<%
if(DebugUtil.isBetaFeatureEnabled()) {
%>
<%if(BrandingHelper.getInstance().isBrandTweetUserURL()) {%>
<%
if(DebugUtil.isDevelFeatureEnabled()) {
%>
              <li <%if(pageCode.equals("tweet/edit")){%>class="active"<%}%>><a id="topmenu_nav_tweet_edit" href="/tweet/edit" title="Tweet a new message">New Tweet</a></li>
              <li <%if(pageCode.equals("tweet/list")){%>class="active"<%}%>><a id="topmenu_nav_tweet_list" href="/tweet/list" title="My tweet list">Tweet List</a></li>
<%
}  // Devel
%>
<%}%>
<%
}  // Beta
%>
            </ul>
            <p class="navbar-text pull-right">
<!-- 
              <a id="anchor_topmenu_tweet" href="#" title="Share it on Twitter"><i class="icon-retweet icon-white"></i></a>&nbsp;
              <a id="anchor_topmenu_share" href="#" title="Email it to your friend"><i class="icon-share icon-white"></i></a>&nbsp;
              <a id="anchor_topmenu_email" href="contact:info+cannyurl+com?subject=Re: <%=brandDisplayName%>" title="Email us"><i class="icon-envelope icon-white"></i></a>
              &nbsp;
-->

<%
if(isAppAuthDisabled == false) {  
%>
<%
if(DebugUtil.isBetaFeatureEnabled()) {
%>
<%
if(isAppAuthDisabled == true || isUserAuthenticated == true) {  
%>
<%if(isUsingGoogleAuth) {%>
<%if(userName != null && !userName.isEmpty()) {%>
              <span><%=userName%></span>&nbsp;
<%} else if (userEmail != null && !userEmail.isEmpty()) {%>
              <span><%=userEmail%></span>&nbsp;
<%}%>
              <a id="cannyurl_logout_anchor" href="<%=logoutUrl%>" title="Logout from <%=brandDisplayName%>"><button class="btn-info btn-mini" id="cannyurl_logout_button"><i class="icon-off icon-white"></i> Logout</button></a>
<%} else if(isUsingTwitterAuth) {%>
<%if(userUsername != null && !userUsername.isEmpty()) {%>
<!-- 
              <a href="<%=twitterUserProfileUrl%>"><span><%=twitterHandle%></span></a>&nbsp;
 -->
              <span><%=twitterHandle%></span>&nbsp;
<%}%>
              <a id="cannyurl_logout_anchor" href="<%=logoutUrl%>" title="Logout from <%=brandDisplayName%>"><button class="btn-info btn-mini" id="cannyurl_logout_button"><i class="icon-off icon-white"></i> Logout</button></a>
<%} else if(isUsingFacebookAuth) {%>
<%if(userName != null && !userName.isEmpty()) {%>
              <span><%=userName%></span>&nbsp;
<%} else if (userEmail != null && !userEmail.isEmpty()) {%>
              <span><%=userEmail%></span>&nbsp;
<%}%>
              <a id="cannyurl_logout_anchor" href="<%=logoutUrl%>" title="Logout from <%=brandDisplayName%>"><button class="btn-info btn-mini" id="cannyurl_logout_button"><i class="icon-off icon-white"></i> Logout</button></a>
<%} else {%>
              <a id="cannyurl_logout_anchor" href="<%=logoutUrl%>" title="Logout from <%=brandDisplayName%>"><button class="btn-info btn-mini" id="cannyurl_logout_button"><i class="icon-off icon-white"></i> Logout</button></a>
<%}%>
<%
if(DebugUtil.isDevelFeatureEnabled()) {
%>
<%
}  // Devel
%>
<%
} else {
%>
<%if(isUsingGoogleAuth) {%>
              <a id="cannyurl_login_anchor" href="<%=loginUrl%>" title="Login to <%=brandDisplayName%> using Google"><img src="/img/google/signin-with-google.png" alt="Login to <%=brandDisplayName%> with Google account"></img></a>
<%} else if(isUsingTwitterAuth) {%>
              <a id="cannyurl_login_anchor" href="<%=loginUrl%>" title="Login to <%=brandDisplayName%> using Twitter"><img src="/img/twitter/sign-in-with-twitter-gray.png" alt="Login to <%=brandDisplayName%> with Twitter account"></img></a>
<%} else if(isUsingFacebookAuth) {%>
              <a id="cannyurl_login_anchor" href="<%=loginUrl%>" title="Login to <%=brandDisplayName%> using Facebook"><img src="/img/facebook/login-with-facebook.png" alt="Login to <%=brandDisplayName%> with Facebook account"></img></a>
<%} else {%>
              <a id="cannyurl_login_anchor" href="<%=loginUrl%>" title="Login to <%=brandDisplayName%>"><button class="btn-info btn-mini" id="cannyurl_login_button"><i class="icon-user icon-white"></i> Login</button></a>
<%}%>
<%
}
%>
<%
if(DebugUtil.isDevelFeatureEnabled()) {
%>
<%
}  // Devel
%>
<%
}  // Beta
%>
<%
}  // isAppAuthDisabled == false
%>
            </p>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>
