<%@ page import="com.myurldb.ws.core.*, com.myurldb.ws.*, com.cannyurl.af.util.*, com.cannyurl.af.auth.common.*, com.cannyurl.af.auth.*, com.cannyurl.af.auth.user.*, com.cannyurl.fe.*, com.cannyurl.fe.core.*, com.cannyurl.fe.bean.*, com.cannyurl.fe.util.*, com.cannyurl.wa.service.*, com.cannyurl.common.*, com.cannyurl.util.*, com.cannyurl.app.auth.*, com.cannyurl.app.auth.filter.*, com.cannyurl.app.util.*, com.cannyurl.helper.*" 
%><%@ page contentType="text/html; charset=UTF-8" 
%><%@ page errorPage="/error/UnknownError" 
%><%!
private static final java.util.logging.Logger logger = java.util.logging.Logger.getLogger("Help");
%><%
boolean isAppAuthDisabled = ConfigUtil.isApplicationAuthDisabled();
// boolean isAppAuthOptional = ConfigUtil.isApplicationAuthOptional();
String appAuthMode = ConfigUtil.getApplicationAuthMode();
boolean isUsingTwitterAuth = false;
if(AppAuthMode.MODE_TWITTER.equals(appAuthMode)) {
    isUsingTwitterAuth = true;
}
%><%// {1] Session
SessionBean sessionBean = UserSessionManager.getInstance().setSessionBean(request, response);
String sessionToken = sessionBean.getToken();
String userId = sessionBean.getUserId();

// [2] Parse url.
String contextPath = request.getContextPath();
String servletPath = request.getServletPath();
String requestHost = request.getServerName();
//String requestURI = request.getRequestURI();
String requestUrl = request.getRequestURL().toString();
String topLevelUrl = UrlHelper.getInstance().getTopLevelURLFromRequestURL(requestUrl);
String pathInfo = request.getPathInfo();
String queryString = request.getQueryString();
%><%
// "Branding"
String appBrand = BrandingHelper.getInstance().getAppBrand();
String brandDisplayName = BrandingHelper.getInstance().getBrandDisplayName();
String brandDescription = BrandingHelper.getInstance().getBrandDescription();
%><%String themeBootstrap = SiteThemeHelper.getInstance().getBootstrapTheme();
String themeJqueryUi = SiteThemeHelper.getInstance().getJqueryUiTheme();%><%
boolean isUserAuthenticated = false;
String userUsername = null;
String loginUrl = null;
String logoutUrl = null;
RequestAuthStateBean authStateBean = (RequestAuthStateBean) request.getAttribute(AuthUtil.REQUEST_ATTR_AUTHSTATEBEAN);
if(authStateBean == null) {
    // Filter might not have applied (because of the /* mapping, etc...) 
    // Try one more time ???
    authStateBean = AuthFilterUtil.createAuthStateBean(request, response);
    if(authStateBean != null) {
        // Add it to the current request object....
        request.setAttribute(AuthUtil.REQUEST_ATTR_AUTHSTATEBEAN, authStateBean);
    }
}
if(authStateBean != null) {
    isUserAuthenticated = authStateBean.isAuthenticated();
    if(isUserAuthenticated) {
        logoutUrl = authStateBean.getLogoutUrl();
        userUsername = authStateBean.getUsername();
    } else {
        loginUrl = authStateBean.getLoginUrl();
    }   
}
%><%
String twitterHandle = "";
String twitterUserProfileUrl = "";
if(isUsingTwitterAuth) {
	if(userUsername != null && !userUsername.isEmpty()) {
	    twitterHandle = "@" + userUsername;      // ???
	    twitterUserProfileUrl = "http://twitter.com/" + userUsername;
	}
}
%><%
if(isUserAuthenticated == false) {
    // ????
    // response.sendRedirect(loginUrl);
}
%><%
// Global config flags.
boolean showingTweetButton = false;
if((ConfigUtil.isGeneralIncludeTweetButton() || TwitterUtil.isTwitterSetUp(topLevelUrl)) && ConfigUtil.isGeneralExcludeTweetButton() == false) {
    showingTweetButton = true;
}
%><%
// ...
%><!DOCTYPE html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!-- Consider adding an manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><%=brandDisplayName%> | Help</title>
    <meta name="description" content="<%=brandDisplayName%> service.">

    <!-- Mobile viewport optimized: j.mp/bplateviewport -->
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <link rel="shortcut icon" href="/img/favicon-<%=appBrand%>.ico" />
    <link rel="icon" href="/img/favicon-<%=appBrand%>.ico" type="image/x-icon" />

    <!-- CSS: implied media=all -->
    <%@ include file="/fragment/style.jspf" %>
    <link rel="stylesheet" type="text/css" href="/css/cannyurl-<%=appBrand%>.css"/>
    <!-- end CSS-->

    <!-- All JavaScript at the bottom, except for Modernizr / Respond.
       Modernizr enables HTML5 elements & feature detects; Respond is a polyfill for min/max-width CSS3 Media Queries
       For optimal performance, use a custom Modernizr build: www.modernizr.com/download/ -->
    <script src="http://ww2.filestoa.com/js/modernizr/modernizr-2.0.6.min.js"></script>

    </head>

  <body>

<jsp:include page="/fragment/TopNavbar">
<jsp:param name="pageCode" value="help"/>
<jsp:param name="brandDisplayName" value="<%=brandDisplayName%>"/>
</jsp:include>

    <div class="container-fluid">

      <!-- Main hero unit for a primary marketing message or call to action -->
      <div class="hero-unit">
        <h1>Getting Started</h1>
<%
if( BrandingHelper.getInstance().isBrandTwrim() ) {
%>
        <p> 
        Short URLs on <%=brandDisplayName%> are based on users' Twitter handles.
        Therefore, you need to log on to <%=brandDisplayName%> via Twitter service (known as OAuth)
        before you can create short URLs.
		This is only used to validate your Twitter handle,
		which is "embedded" in all short URLs you create.
        </p>
<%
} else {
%>
        <p> 
		<%=brandDisplayName%> does not require you to log into the service.
		You don't need any special account or permission to create and/or view the short URLs.
		You simply visit the site and start creating/verifying short URLs.
		All short URLs are public by default.
        </p>
<%
}
%>
        <p><a id="anchor_mainmenu_signup" class="btn btn-primary btn-large" title="Please provide your email address, and we will keep you posted on our progress at <%=brandDisplayName%>.">Sign up &raquo;</a></p>
      </div>  <!--   Hero unit  -->

      <!-- Example row of columns -->
      <div class="row-fluid">
        <div class="span12">
	    <h2>All Short URL Information is Public</h2>
		<p>
		It should be noted that all your short URL information on <%=brandDisplayName%> is publicly accessible.
		As long as you know the short URL, you can access the information associated with the URL, including the URL message, regardless of who originally created it.
		While it is rather unlikely that anybody who does not know your short URL will find and read your short URL info/message,
		it is advised not to include any proprietary or otherwise sensitive information on short URL.
		</p>
        </div>
      </div>

      <div class="row-fluid">
        <div class="span12">
	    <h2>Short URLs are "Read-Only"</h2>
		<p>
		A short URL, or any URL in fact, is generally considered "permanent".
		This is true with short URLs created on <%=brandDisplayName%> service. 
		Once you save a short URL, there'll be a short time window in which you can 
		edit or modify certain (limited) short URL content (e.g., from the same browser window)
		such as the short URL viewer message.
		After the time window, the short URL becomes "read-only", that is, 
		the short URL information cannot be modified or deleted, even by the author.
		</p>
        </div>
      </div>

      <div class="row-fluid">
        <div class="span12">
<%
if(! BrandingHelper.getInstance().isBrandSassyURL()) {
%>
	    <h2>Short URL Token Types</h2>
		<p>
		A "token" in the context of <%=brandDisplayName%> application is a trailing part of a generated short URL (e.g., after the last forward slash "/"),
		which is typically a random string.
		<%=brandDisplayName%> supports five different types of tokens.
		A token type determines what kind of letters are to be used in a token.
		The recommended value is "As Short As Possible", and the generated token comprises upper/lower case alphabets and numbers.
		The token length is, currently, 4, but it'll likely increase over time.
		You can also try the "Make It Even Shorter" option, whose length is currently set to 3.
		The generated token may include special characters such as "$" and ",", etc. besides the alphanumeric characters.
		The "Alphabets and Numbers" type uses lower case alphabets and numbers, and the current token length is 5.
		The "Alphabets only" type uses lower case alphabets only and the current token length is 6.
		The current token length for the "Numbers only" type is 7 and it only uses 10 digits, 0 through 9.
		</p>
		<p>
		<%=brandDisplayName%> service also supports user-supplied "custom tokens". 
		The length of a custom token should be equal to or longer than <%=ConfigUtil.getCustomTokenMinLength()%>.
		</p>
<%
}
%>
<%
if(BrandingHelper.getInstance().isBrandSassyURL()) {
    int mixedcaseLength = ConfigUtil.getSassyTokenMixedcaseLength();
    int lowercapsLength = ConfigUtil.getSassyTokenLowercapsLength();
    int lowercaseLength = ConfigUtil.getSassyTokenLowercaseLength();
%>
	    <h2>Sassy URL Token Types</h2>
		<p>
		A "token" in the context of <%=brandDisplayName%> application is a trailing part of a generated short URL (e.g., after the last forward slash "/""),
		which is typically a random string.
        A sassy URL comprises only valid English words, and hence more "readable" than random strings.
        We use two letter, three letter, four letter English words (or, acronyms).
		<%=brandDisplayName%> supports three different types of sassy tokens.
		A token type determines what kind of letters are to be used in a token.
		The recommended value is "Allow Caps", and the generated token comprises lower case alphabets and
		the "words" in a token may be individually capitalized.
		The token length is, currently, <%=lowercapsLength%>.
		You can also try the "Mixed Case" option, whose length is currently set to <%=mixedcaseLength%>.
		The generated token may include both lower and upper case alphabets.
		The "Lowercases Only" type uses only lower case alphabets only and the current token length is <%=lowercaseLength%>.
		</p>
		<p>
		<%=brandDisplayName%> service also supports user-supplied "custom tokens". 
		The length of a custom token should be equal to or longer than <%=ConfigUtil.getCustomTokenMinLength()%>.
		</p>
<%
}
%>
        </div>
      </div>

<%
if(showingTweetButton) {
%>
      <div class="row-fluid">
        <div class="span12">
	    <h2>Twitter Integration</h2>
		<p>
		Now you can tweet your short URL information via Twitter @anywhere API.
        Just press the "Tweet" button to open the Tweet Box. 
        It has a default tweet message filled in based on the short URL and the (optional) message. 
        You can edit the message before posting the tweet. 
        Note that Twitter requires that you grant access to the app (i.e., <%=brandDisplayName%>)
        and you'll have to log in to Twitter (with your own Twitter account).
        This authentication step is performed by Twitter (through a mechanism called OAuth).
        <%=brandDisplayName%> is not involved in any way in this step, and it does NOT read or store your Twitter credentials. 
        </p>		
        </div>
      </div>
<%
}
%>

<hr>

<!-- Page Footer -->    
<%@ include file="/fragment/Footer.jspf" %>
<!-- Page Footer -->    

    </div> <!--! end of #container -->

    <div id="div_status_floater" style="display: none;" class="alert">
    </div>

    
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="http://ww2.filestoa.com/js/jquery/jquery-1.8.3.min.js"><\/script>')</script>
	<script src="http://ww2.filestoa.com/js/jquery/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="http://ww2.filestoa.com/js/jquery/plugin/jquery.floatobject-1.4.js"></script>
    <script src="http://ww2.filestoa.com/js/bootstrap/bootstrap-2.0.js"></script>
    <script type="text/javascript" src="/js/core/debugutil.js"></script>
	<script src="/js/plugins.js"></script>
    <script src="/js/script.js"></script>
    <script type="text/javascript" src="http://ww2.filestoa.com/js/core/datetimeutil-1.0.js"></script>
    <script type="text/javascript" src="http://ww2.filestoa.com/js/core/emailutil-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/fiveten-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/statushelper-1.1.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/tweethelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/signuphelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/feedbackhelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/emailhelper-1.0.js"></script>


    <script>
    // Global vars
    var feedbackHelper;
    $(function() {
    	var targetService = "<%=brandDisplayName%>";
    	var targetPage = "view";
    	feedbackHelper = new webstoa.FeedbackHelper(targetService, targetPage);
    });
    </script>

    <script>
    // Email address "decoding"...
    $(function() {
        $("a").each(function() {
        	var anchor = $(this);
        	webstoa.EmailUtil.decodeMailtoAnchor(anchor);
        });
    });
    </script>

    <script>
    $(function() {
        $("#anchor_feedback_footer").click(function() {
            if(DEBUG_ENABLED) console.log("Feedback button clicked."); 
            var targetItem = 'footer';
            feedbackHelper.feedback(targetItem);
		    return false;
        });
    });
    </script>

    <script>
    var signupHelper;
    $(function() {
    	// Init...
    	var servicename = "<%=brandDisplayName%>";
    	var launchname = "Initial pre-launch trial";
    	var pageid = "help";
    	signupHelper = new webstoa.SignupHelper(servicename, launchname, pageid);
    });
    </script>

    <script>
    $(function() {
        $("#anchor_mainmenu_signup").click(function() {
            if(DEBUG_ENABLED) console.log("Signup button clicked."); 

       	    signupHelper.signup();
       	    //statusHelper.update('Thanks for signing up.');
		    return false;
        });
    });
    </script>


<script>
$(function() {
	// Top menu handlers
    $("#topmenu_nav_view").click(function() {
        if(DEBUG_ENABLED) console.log("topmenu_nav_view anchor clicked."); 
        ////window.location = "/view";
        // ???
        $("#topmenu_nav_view").fadeOut(450).fadeIn(950);
        return false;
    });
});
</script>


    
    <!--  Google Analytics  -->
<%
String googleAnalyticsCode = AnalyticsHelper.getInstance().getGoogleAnalyticsCode();
if(googleAnalyticsCode != null && !googleAnalyticsCode.isEmpty()) {    // And, if it is enabled (e.g. through config). TBD...
%>
    <!-- Change UA-XXXXX-X to be your site's ID -->
    <script>
      window._gaq = [['_setAccount','<%=googleAnalyticsCode%>'],['_trackPageview'],['_trackPageLoadTime']];
      Modernizr.load({
        load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
      });
    </script>
<%
}
%>

  </body>
</html>
