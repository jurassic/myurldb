<%@ page import="com.cannyurl.common.*,com.cannyurl.app.util.*,com.cannyurl.helper.*" 
%><%@ page contentType="text/html; charset=UTF-8" 
%><%@ page errorPage="/error/UnknownError" 
%><%!
private static final java.util.logging.Logger logger = java.util.logging.Logger.getLogger("Home");
%><%
String requestUrl = request.getRequestURL().toString();
String topLevelUrl = UrlHelper.getInstance().getTopLevelURLFromRequestURL(requestUrl);
%><%
// "Branding"
String appBrand = BrandingHelper.getInstance().getAppBrand();
String brandDisplayName = BrandingHelper.getInstance().getBrandDisplayName();
String brandDescription = BrandingHelper.getInstance().getBrandDescription();
%><%
String themeBootstrap = SiteThemeHelper.getInstance().getBootstrapTheme();
String themeJqueryUi = SiteThemeHelper.getInstance().getJqueryUiTheme();
%><%
// temporary
String frontPageUrl = ConfigUtil.getUiWebAppFrontPage();
if(frontPageUrl != null && !frontPageUrl.isEmpty() && !frontPageUrl.equals("/home")) {
    response.sendRedirect(frontPageUrl);   // 302
    // response.setStatus(301);
    // response.setHeader( "Location", frontPageUrl );
    // response.setHeader( "Connection", "close" );
} else {
%><!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title><%=brandDisplayName%> | <%=brandDescription%></title>
    <meta name="author" content="Aery Software">
    <meta name="description" content="<%=AppBrand.getBrandDescription(appBrand)%>. It also provides free URL Shortener Web Services API." />

    <!-- Mobile viewport optimized: j.mp/bplateviewport -->
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <link rel="shortcut icon" href="/img/favicon-<%=appBrand%>.ico" />
    <link rel="icon" href="/img/favicon-<%=appBrand%>.ico" type="image/x-icon" />

    <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- CSS: implied media=all -->
    <%@ include file="/fragment/style.jspf" %>
    <!-- end CSS-->

    <!-- All JavaScript at the bottom, except for Modernizr / Respond.
       Modernizr enables HTML5 elements & feature detects; Respond is a polyfill for min/max-width CSS3 Media Queries
       For optimal performance, use a custom Modernizr build: www.modernizr.com/download/ -->
    <script src="http://ww2.filestoa.com/js/modernizr/modernizr-2.0.6.min.js"></script>
    
</head>
<body>


<jsp:include page="/fragment/TopNavbar">
<jsp:param name="pageCode" value="home"/>
<jsp:param name="brandDisplayName" value="<%=brandDisplayName%>"/>
</jsp:include>


    <div class="container-fluid">

      <!-- Main hero unit for a primary marketing message or call to action -->
      <div id="main" class="hero-unit">

<div id="cannyurl_logo">
	 	    <h1><%=brandDisplayName%><sup>&beta;</sup></h1>
</div>


<div>
        <p>
        <%=brandDisplayName%> (tm) is a safe URL shortener service, among other things.
        You can create a "safe", and optionally "branded", short URL directed to any target (long URL) Web page.
        </p>
<%
if(BrandingHelper.getInstance().isBrandCannyURL()) {
%>
        <p>
        The short URLs are "safe" in that each short URL can be configured not to be automatically redirected to the target Web page.
        Viewers can view which Web site the short URL is actually redirecting to
        and decide whether to proceed or not.
        You can optionally include a "message" for the viewers (e.g., users who click on the short URL link).
        </p>
<%
} else if(BrandingHelper.getInstance().isBrandFlashURL()) {
%>
        <p>
        The short URLs are "safe" in that each short URL can be configured not to be automatically redirected to the target Web page.
        Viewers can view which Web site the short URL is actually redirecting to
        and decide whether to proceed or not.
        You can optionally include a "message" for the viewers (e.g., users who click on the short URL link).
        </p>
<%
} else if(BrandingHelper.getInstance().isBrandFemtoURL()) {
%>
        <p>
        We will be online soon. Please check back later.
        </p>
<%
} else if(BrandingHelper.getInstance().isBrandSassyURL()) {
%>
        <p>
        We will be online soon. Please check back later.
        </p>
<%
} else if(BrandingHelper.getInstance().isBrandUserURL()) {
%>
        <p>
        We will be online soon. Please check back later.
        </p>
<%
} else if(BrandingHelper.getInstance().isBrandQuadURL()) {
%>
        <p>
        We will be online soon. Please check back later.
        </p>
<%
} else if(BrandingHelper.getInstance().isBrandSpeedKeyword()) {
%>
        <p>
        We will be online soon. Please check back later.
        </p>
<%
} else if(BrandingHelper.getInstance().isBrandTweetUserURL()) {
%>
        <p>
        We are re-inventing URL shortening.
        <%=brandDisplayName%> is a new kind of "personally branded" short URL service.
        We provide various features to users who value personal branding.
        Personally branded URLs will be more likely to be clicked
        because users will tend to trust branded URLs more than anonymous/cryptic-looking short URLs. 
        </p>
<%
} else if(BrandingHelper.getInstance().isBrandSmallBizURL()) {
%>
        <p>
        <%=brandDisplayName%> is also, more generally, a new kind of URL redirection service.
        We provide various URL redirection services to small business.
        </p>
<%
}
%>
        <p>
        We can be reached through email at <a href="contact:info+<%=appBrand%>+com">info at <%=appBrand%></a>
        or on Twitter at <a href="http://twitter.com/<%=appBrand%>">@<%=appBrand%></a>.
        We are also building our home page at <a href="http://www.<%=appBrand%>.com/">www.<%=appBrand%>.com</a> (under construction).
        </p>

        <p><a id="anchor_mainmenu_signup" class="btn btn-primary btn-large" title="Please let us know your email address, and we will keep you posted on our progress at <%=brandDisplayName%>.">Sign up &raquo;</a></p>
</div>
</div>

<!-- Page Footer -->    
<%@ include file="/fragment/Footer.jspf" %>
<!-- Page Footer -->    

    </div> <!--! end of #container -->


    
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="http://ww2.filestoa.com/js/jquery/jquery-1.8.3.min.js"><\/script>')</script>
	<script src="http://ww2.filestoa.com/js/jquery/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="http://ww2.filestoa.com/js/jquery/plugin/jquery.floatobject-1.4.js"></script>
    <script src="http://ww2.filestoa.com/js/bootstrap/bootstrap-2.0.js"></script>
    <script type="text/javascript" src="/js/core/debugutil.js"></script>
	<script src="/js/plugins.js"></script>
    <script src="/js/script.js"></script>
    <script type="text/javascript" src="http://ww2.filestoa.com/js/core/datetimeutil-1.0.js"></script>
    <script type="text/javascript" src="http://ww2.filestoa.com/js/core/emailutil-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/signuphelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/feedbackhelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://ww2.filestoa.com/js/util/emailhelper-1.0.js"></script>


    <script>
    // Global vars
    var feedbackHelper;
    $(function() {
    	var targetService = "<%=brandDisplayName%>";
    	var targetPage = "home";
    	feedbackHelper = new webstoa.FeedbackHelper(targetService, targetPage);
    });
    </script>

    <script>
    // Email address "decoding"...
    $(function() {
        $("a").each(function() {
        	var anchor = $(this);
        	webstoa.EmailUtil.decodeMailtoAnchor(anchor);
        });
    });
    </script>

    <script>
    $(function() {
        $("#anchor_feedback_footer").click(function() {
            if(DEBUG_ENABLED) console.log("Feedback button clicked."); 
            var targetItem = 'footer';
            feedbackHelper.feedback(targetItem);
		    return false;
        });
    });
    </script>

    <script>
    var signupHelper;
    $(function() {
    	// Init...
    	var servicename = "<%=brandDisplayName%>";
    	var launchname = "Special Customer Trial";
    	var pageid = "home";
    	signupHelper = new webstoa.SignupHelper(servicename, launchname, pageid);
    });
    </script>

    <script>
    $(function() {
        $("#anchor_mainmenu_signup").click(function() {
            if(DEBUG_ENABLED) console.log("Signup button clicked."); 

       	    signupHelper.signup();
       	    //statusHelper.update('Thanks for signing up.');
		    return false;
        });
    });
    </script>

<script>
$(function() {
	// Top menu handlers
    $("#topmenu_nav_view").click(function() {
        if(DEBUG_ENABLED) console.log("topmenu_nav_view anchor clicked."); 
        ////window.location = "/view";
        // ???
        $("#topmenu_nav_view").fadeOut(450).fadeIn(950);
        return false;
    });
});
</script>

</body>
</html>
<%
}
%>

