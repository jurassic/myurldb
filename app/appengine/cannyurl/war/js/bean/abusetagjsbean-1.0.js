//////////////////////////////////////////////////////////
// <script src="/js/bean/abusetagjsbean-1.0.js"></script>
// Last modified time: 1365474465677.
//////////////////////////////////////////////////////////

var cannyurl = cannyurl || {};
cannyurl.wa = cannyurl.wa || {};
cannyurl.wa.bean = cannyurl.wa.bean || {};
cannyurl.wa.bean.AbuseTagJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var guid = generateUuid();
    var shortLink;
    var shortUrl;
    var longUrl;
    var longUrlHash;
    var user;
    var ipAddress;
    var tag;
    var comment;
    var status;
    var reviewer;
    var action;
    var note;
    var pastActions;
    var reviewedTime;
    var actionTime;
    var createdTime = getCurrentTime();
    var modifiedTime;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getGuid = function() { return guid; };
    this.setGuid = function(value) { guid = value; };
    this.getShortLink = function() { return shortLink; };
    this.setShortLink = function(value) { shortLink = value; };
    this.getShortUrl = function() { return shortUrl; };
    this.setShortUrl = function(value) { shortUrl = value; };
    this.getLongUrl = function() { return longUrl; };
    this.setLongUrl = function(value) { longUrl = value; };
    this.getLongUrlHash = function() { return longUrlHash; };
    this.setLongUrlHash = function(value) { longUrlHash = value; };
    this.getUser = function() { return user; };
    this.setUser = function(value) { user = value; };
    this.getIpAddress = function() { return ipAddress; };
    this.setIpAddress = function(value) { ipAddress = value; };
    this.getTag = function() { return tag; };
    this.setTag = function(value) { tag = value; };
    this.getComment = function() { return comment; };
    this.setComment = function(value) { comment = value; };
    this.getStatus = function() { return status; };
    this.setStatus = function(value) { status = value; };
    this.getReviewer = function() { return reviewer; };
    this.setReviewer = function(value) { reviewer = value; };
    this.getAction = function() { return action; };
    this.setAction = function(value) { action = value; };
    this.getNote = function() { return note; };
    this.setNote = function(value) { note = value; };
    this.getPastActions = function() { return pastActions; };
    this.setPastActions = function(value) { pastActions = value; };
    this.getReviewedTime = function() { return reviewedTime; };
    this.setReviewedTime = function(value) { reviewedTime = value; };
    this.getActionTime = function() { return actionTime; };
    this.setActionTime = function(value) { actionTime = value; };
    this.getCreatedTime = function() { return createdTime; };
    this.setCreatedTime = function(value) { createdTime = value; };
    this.getModifiedTime = function() { return modifiedTime; };
    this.setModifiedTime = function(value) { modifiedTime = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new cannyurl.wa.bean.AbuseTagJsBean();

      o.setGuid(generateUuid());
      if(shortLink !== undefined && shortLink != null) {
        o.setShortLink(shortLink);
      }
      if(shortUrl !== undefined && shortUrl != null) {
        o.setShortUrl(shortUrl);
      }
      if(longUrl !== undefined && longUrl != null) {
        o.setLongUrl(longUrl);
      }
      if(longUrlHash !== undefined && longUrlHash != null) {
        o.setLongUrlHash(longUrlHash);
      }
      if(user !== undefined && user != null) {
        o.setUser(user);
      }
      if(ipAddress !== undefined && ipAddress != null) {
        o.setIpAddress(ipAddress);
      }
      if(tag !== undefined && tag != null) {
        o.setTag(tag);
      }
      if(comment !== undefined && comment != null) {
        o.setComment(comment);
      }
      if(status !== undefined && status != null) {
        o.setStatus(status);
      }
      if(reviewer !== undefined && reviewer != null) {
        o.setReviewer(reviewer);
      }
      if(action !== undefined && action != null) {
        o.setAction(action);
      }
      if(note !== undefined && note != null) {
        o.setNote(note);
      }
      if(pastActions !== undefined && pastActions != null) {
        o.setPastActions(pastActions);
      }
      if(reviewedTime !== undefined && reviewedTime != null) {
        o.setReviewedTime(reviewedTime);
      }
      if(actionTime !== undefined && actionTime != null) {
        o.setActionTime(actionTime);
      }
      o.setCreatedTime(getCurrentTime());
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(guid !== undefined && guid != null) {
        jsonObj.guid = guid;
      } // Otherwise ignore...
      if(shortLink !== undefined && shortLink != null) {
        jsonObj.shortLink = shortLink;
      } // Otherwise ignore...
      if(shortUrl !== undefined && shortUrl != null) {
        jsonObj.shortUrl = shortUrl;
      } // Otherwise ignore...
      if(longUrl !== undefined && longUrl != null) {
        jsonObj.longUrl = longUrl;
      } // Otherwise ignore...
      if(longUrlHash !== undefined && longUrlHash != null) {
        jsonObj.longUrlHash = longUrlHash;
      } // Otherwise ignore...
      if(user !== undefined && user != null) {
        jsonObj.user = user;
      } // Otherwise ignore...
      if(ipAddress !== undefined && ipAddress != null) {
        jsonObj.ipAddress = ipAddress;
      } // Otherwise ignore...
      if(tag !== undefined && tag != null) {
        jsonObj.tag = tag;
      } // Otherwise ignore...
      if(comment !== undefined && comment != null) {
        jsonObj.comment = comment;
      } // Otherwise ignore...
      if(status !== undefined && status != null) {
        jsonObj.status = status;
      } // Otherwise ignore...
      if(reviewer !== undefined && reviewer != null) {
        jsonObj.reviewer = reviewer;
      } // Otherwise ignore...
      if(action !== undefined && action != null) {
        jsonObj.action = action;
      } // Otherwise ignore...
      if(note !== undefined && note != null) {
        jsonObj.note = note;
      } // Otherwise ignore...
      if(pastActions !== undefined && pastActions != null) {
        jsonObj.pastActions = pastActions;
      } // Otherwise ignore...
      if(reviewedTime !== undefined && reviewedTime != null) {
        jsonObj.reviewedTime = reviewedTime;
      } // Otherwise ignore...
      if(actionTime !== undefined && actionTime != null) {
        jsonObj.actionTime = actionTime;
      } // Otherwise ignore...
      if(createdTime !== undefined && createdTime != null) {
        jsonObj.createdTime = createdTime;
      } // Otherwise ignore...
      if(modifiedTime !== undefined && modifiedTime != null) {
        jsonObj.modifiedTime = modifiedTime;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(guid) {
        str += "\"guid\":\"" + guid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"guid\":null, ";
      }
      if(shortLink) {
        str += "\"shortLink\":\"" + shortLink + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"shortLink\":null, ";
      }
      if(shortUrl) {
        str += "\"shortUrl\":\"" + shortUrl + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"shortUrl\":null, ";
      }
      if(longUrl) {
        str += "\"longUrl\":\"" + longUrl + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"longUrl\":null, ";
      }
      if(longUrlHash) {
        str += "\"longUrlHash\":\"" + longUrlHash + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"longUrlHash\":null, ";
      }
      if(user) {
        str += "\"user\":\"" + user + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"user\":null, ";
      }
      if(ipAddress) {
        str += "\"ipAddress\":\"" + ipAddress + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"ipAddress\":null, ";
      }
      if(tag) {
        str += "\"tag\":" + tag + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"tag\":null, ";
      }
      if(comment) {
        str += "\"comment\":\"" + comment + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"comment\":null, ";
      }
      if(status) {
        str += "\"status\":\"" + status + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"status\":null, ";
      }
      if(reviewer) {
        str += "\"reviewer\":\"" + reviewer + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"reviewer\":null, ";
      }
      if(action) {
        str += "\"action\":\"" + action + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"action\":null, ";
      }
      if(note) {
        str += "\"note\":\"" + note + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"note\":null, ";
      }
      if(pastActions) {
        str += "\"pastActions\":\"" + pastActions + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"pastActions\":null, ";
      }
      if(reviewedTime) {
        str += "\"reviewedTime\":" + reviewedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"reviewedTime\":null, ";
      }
      if(actionTime) {
        str += "\"actionTime\":" + actionTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"actionTime\":null, ";
      }
      if(createdTime) {
        str += "\"createdTime\":" + createdTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"createdTime\":null, ";
      }
      if(modifiedTime) {
        str += "\"modifiedTime\":" + modifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"modifiedTime\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "guid:" + guid + ", ";
      str += "shortLink:" + shortLink + ", ";
      str += "shortUrl:" + shortUrl + ", ";
      str += "longUrl:" + longUrl + ", ";
      str += "longUrlHash:" + longUrlHash + ", ";
      str += "user:" + user + ", ";
      str += "ipAddress:" + ipAddress + ", ";
      str += "tag:" + tag + ", ";
      str += "comment:" + comment + ", ";
      str += "status:" + status + ", ";
      str += "reviewer:" + reviewer + ", ";
      str += "action:" + action + ", ";
      str += "note:" + note + ", ";
      str += "pastActions:" + pastActions + ", ";
      str += "reviewedTime:" + reviewedTime + ", ";
      str += "actionTime:" + actionTime + ", ";
      str += "createdTime:" + createdTime + ", ";
      str += "modifiedTime:" + modifiedTime + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

cannyurl.wa.bean.AbuseTagJsBean.create = function(obj) {
  var o = new cannyurl.wa.bean.AbuseTagJsBean();

  if(obj.guid !== undefined && obj.guid != null) {
    o.setGuid(obj.guid);
  }
  if(obj.shortLink !== undefined && obj.shortLink != null) {
    o.setShortLink(obj.shortLink);
  }
  if(obj.shortUrl !== undefined && obj.shortUrl != null) {
    o.setShortUrl(obj.shortUrl);
  }
  if(obj.longUrl !== undefined && obj.longUrl != null) {
    o.setLongUrl(obj.longUrl);
  }
  if(obj.longUrlHash !== undefined && obj.longUrlHash != null) {
    o.setLongUrlHash(obj.longUrlHash);
  }
  if(obj.user !== undefined && obj.user != null) {
    o.setUser(obj.user);
  }
  if(obj.ipAddress !== undefined && obj.ipAddress != null) {
    o.setIpAddress(obj.ipAddress);
  }
  if(obj.tag !== undefined && obj.tag != null) {
    o.setTag(obj.tag);
  }
  if(obj.comment !== undefined && obj.comment != null) {
    o.setComment(obj.comment);
  }
  if(obj.status !== undefined && obj.status != null) {
    o.setStatus(obj.status);
  }
  if(obj.reviewer !== undefined && obj.reviewer != null) {
    o.setReviewer(obj.reviewer);
  }
  if(obj.action !== undefined && obj.action != null) {
    o.setAction(obj.action);
  }
  if(obj.note !== undefined && obj.note != null) {
    o.setNote(obj.note);
  }
  if(obj.pastActions !== undefined && obj.pastActions != null) {
    o.setPastActions(obj.pastActions);
  }
  if(obj.reviewedTime !== undefined && obj.reviewedTime != null) {
    o.setReviewedTime(obj.reviewedTime);
  }
  if(obj.actionTime !== undefined && obj.actionTime != null) {
    o.setActionTime(obj.actionTime);
  }
  if(obj.createdTime !== undefined && obj.createdTime != null) {
    o.setCreatedTime(obj.createdTime);
  }
  if(obj.modifiedTime !== undefined && obj.modifiedTime != null) {
    o.setModifiedTime(obj.modifiedTime);
  }
    
  return o;
};

cannyurl.wa.bean.AbuseTagJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = cannyurl.wa.bean.AbuseTagJsBean.create(jsonObj);
  return obj;
};
