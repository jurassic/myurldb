//////////////////////////////////////////////////////////
// <script src="/js/bean/appbrandstructjsbean-1.0.js"></script>
// Last modified time: 1365474464929.
//////////////////////////////////////////////////////////

var cannyurl = cannyurl || {};
cannyurl.wa = cannyurl.wa || {};
cannyurl.wa.bean = cannyurl.wa.bean || {};
cannyurl.wa.bean.AppBrandStructJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var brand;
    var name;
    var description;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getBrand = function() { return brand; };
    this.setBrand = function(value) { brand = value; };
    this.getName = function() { return name; };
    this.setName = function(value) { name = value; };
    this.getDescription = function() { return description; };
    this.setDescription = function(value) { description = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new cannyurl.wa.bean.AppBrandStructJsBean();

      if(brand !== undefined && brand != null) {
        o.setBrand(brand);
      }
      if(name !== undefined && name != null) {
        o.setName(name);
      }
      if(description !== undefined && description != null) {
        o.setDescription(description);
      }
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(brand !== undefined && brand != null) {
        jsonObj.brand = brand;
      } // Otherwise ignore...
      if(name !== undefined && name != null) {
        jsonObj.name = name;
      } // Otherwise ignore...
      if(description !== undefined && description != null) {
        jsonObj.description = description;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(brand) {
        str += "\"brand\":\"" + brand + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"brand\":null, ";
      }
      if(name) {
        str += "\"name\":\"" + name + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"name\":null, ";
      }
      if(description) {
        str += "\"description\":\"" + description + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"description\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "brand:" + brand + ", ";
      str += "name:" + name + ", ";
      str += "description:" + description + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

cannyurl.wa.bean.AppBrandStructJsBean.create = function(obj) {
  var o = new cannyurl.wa.bean.AppBrandStructJsBean();

  if(obj.brand !== undefined && obj.brand != null) {
    o.setBrand(obj.brand);
  }
  if(obj.name !== undefined && obj.name != null) {
    o.setName(obj.name);
  }
  if(obj.description !== undefined && obj.description != null) {
    o.setDescription(obj.description);
  }
    
  return o;
};

cannyurl.wa.bean.AppBrandStructJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = cannyurl.wa.bean.AppBrandStructJsBean.create(jsonObj);
  return obj;
};
