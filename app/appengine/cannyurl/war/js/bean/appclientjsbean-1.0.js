//////////////////////////////////////////////////////////
// <script src="/js/bean/appclientjsbean-1.0.js"></script>
// Last modified time: 1365474465092.
//////////////////////////////////////////////////////////

var cannyurl = cannyurl || {};
cannyurl.wa = cannyurl.wa || {};
cannyurl.wa.bean = cannyurl.wa.bean || {};
cannyurl.wa.bean.AppClientJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var guid = generateUuid();
    var owner;
    var admin;
    var name;
    var clientId;
    var clientCode;
    var rootDomain;
    var appDomain;
    var useAppDomain;
    var enabled;
    var licenseMode;
    var status;
    var createdTime = getCurrentTime();
    var modifiedTime;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getGuid = function() { return guid; };
    this.setGuid = function(value) { guid = value; };
    this.getOwner = function() { return owner; };
    this.setOwner = function(value) { owner = value; };
    this.getAdmin = function() { return admin; };
    this.setAdmin = function(value) { admin = value; };
    this.getName = function() { return name; };
    this.setName = function(value) { name = value; };
    this.getClientId = function() { return clientId; };
    this.setClientId = function(value) { clientId = value; };
    this.getClientCode = function() { return clientCode; };
    this.setClientCode = function(value) { clientCode = value; };
    this.getRootDomain = function() { return rootDomain; };
    this.setRootDomain = function(value) { rootDomain = value; };
    this.getAppDomain = function() { return appDomain; };
    this.setAppDomain = function(value) { appDomain = value; };
    this.getUseAppDomain = function() { return useAppDomain; };
    this.setUseAppDomain = function(value) { useAppDomain = value; };
    this.getEnabled = function() { return enabled; };
    this.setEnabled = function(value) { enabled = value; };
    this.getLicenseMode = function() { return licenseMode; };
    this.setLicenseMode = function(value) { licenseMode = value; };
    this.getStatus = function() { return status; };
    this.setStatus = function(value) { status = value; };
    this.getCreatedTime = function() { return createdTime; };
    this.setCreatedTime = function(value) { createdTime = value; };
    this.getModifiedTime = function() { return modifiedTime; };
    this.setModifiedTime = function(value) { modifiedTime = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new cannyurl.wa.bean.AppClientJsBean();

      o.setGuid(generateUuid());
      if(owner !== undefined && owner != null) {
        o.setOwner(owner);
      }
      if(admin !== undefined && admin != null) {
        o.setAdmin(admin);
      }
      if(name !== undefined && name != null) {
        o.setName(name);
      }
      if(clientId !== undefined && clientId != null) {
        o.setClientId(clientId);
      }
      if(clientCode !== undefined && clientCode != null) {
        o.setClientCode(clientCode);
      }
      if(rootDomain !== undefined && rootDomain != null) {
        o.setRootDomain(rootDomain);
      }
      if(appDomain !== undefined && appDomain != null) {
        o.setAppDomain(appDomain);
      }
      if(useAppDomain !== undefined && useAppDomain != null) {
        o.setUseAppDomain(useAppDomain);
      }
      if(enabled !== undefined && enabled != null) {
        o.setEnabled(enabled);
      }
      if(licenseMode !== undefined && licenseMode != null) {
        o.setLicenseMode(licenseMode);
      }
      if(status !== undefined && status != null) {
        o.setStatus(status);
      }
      o.setCreatedTime(getCurrentTime());
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(guid !== undefined && guid != null) {
        jsonObj.guid = guid;
      } // Otherwise ignore...
      if(owner !== undefined && owner != null) {
        jsonObj.owner = owner;
      } // Otherwise ignore...
      if(admin !== undefined && admin != null) {
        jsonObj.admin = admin;
      } // Otherwise ignore...
      if(name !== undefined && name != null) {
        jsonObj.name = name;
      } // Otherwise ignore...
      if(clientId !== undefined && clientId != null) {
        jsonObj.clientId = clientId;
      } // Otherwise ignore...
      if(clientCode !== undefined && clientCode != null) {
        jsonObj.clientCode = clientCode;
      } // Otherwise ignore...
      if(rootDomain !== undefined && rootDomain != null) {
        jsonObj.rootDomain = rootDomain;
      } // Otherwise ignore...
      if(appDomain !== undefined && appDomain != null) {
        jsonObj.appDomain = appDomain;
      } // Otherwise ignore...
      if(useAppDomain !== undefined && useAppDomain != null) {
        jsonObj.useAppDomain = useAppDomain;
      } // Otherwise ignore...
      if(enabled !== undefined && enabled != null) {
        jsonObj.enabled = enabled;
      } // Otherwise ignore...
      if(licenseMode !== undefined && licenseMode != null) {
        jsonObj.licenseMode = licenseMode;
      } // Otherwise ignore...
      if(status !== undefined && status != null) {
        jsonObj.status = status;
      } // Otherwise ignore...
      if(createdTime !== undefined && createdTime != null) {
        jsonObj.createdTime = createdTime;
      } // Otherwise ignore...
      if(modifiedTime !== undefined && modifiedTime != null) {
        jsonObj.modifiedTime = modifiedTime;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(guid) {
        str += "\"guid\":\"" + guid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"guid\":null, ";
      }
      if(owner) {
        str += "\"owner\":\"" + owner + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"owner\":null, ";
      }
      if(admin) {
        str += "\"admin\":\"" + admin + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"admin\":null, ";
      }
      if(name) {
        str += "\"name\":\"" + name + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"name\":null, ";
      }
      if(clientId) {
        str += "\"clientId\":\"" + clientId + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"clientId\":null, ";
      }
      if(clientCode) {
        str += "\"clientCode\":\"" + clientCode + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"clientCode\":null, ";
      }
      if(rootDomain) {
        str += "\"rootDomain\":\"" + rootDomain + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"rootDomain\":null, ";
      }
      if(appDomain) {
        str += "\"appDomain\":\"" + appDomain + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"appDomain\":null, ";
      }
      if(useAppDomain) {
        str += "\"useAppDomain\":" + useAppDomain + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"useAppDomain\":null, ";
      }
      if(enabled) {
        str += "\"enabled\":" + enabled + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"enabled\":null, ";
      }
      if(licenseMode) {
        str += "\"licenseMode\":\"" + licenseMode + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"licenseMode\":null, ";
      }
      if(status) {
        str += "\"status\":\"" + status + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"status\":null, ";
      }
      if(createdTime) {
        str += "\"createdTime\":" + createdTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"createdTime\":null, ";
      }
      if(modifiedTime) {
        str += "\"modifiedTime\":" + modifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"modifiedTime\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "guid:" + guid + ", ";
      str += "owner:" + owner + ", ";
      str += "admin:" + admin + ", ";
      str += "name:" + name + ", ";
      str += "clientId:" + clientId + ", ";
      str += "clientCode:" + clientCode + ", ";
      str += "rootDomain:" + rootDomain + ", ";
      str += "appDomain:" + appDomain + ", ";
      str += "useAppDomain:" + useAppDomain + ", ";
      str += "enabled:" + enabled + ", ";
      str += "licenseMode:" + licenseMode + ", ";
      str += "status:" + status + ", ";
      str += "createdTime:" + createdTime + ", ";
      str += "modifiedTime:" + modifiedTime + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

cannyurl.wa.bean.AppClientJsBean.create = function(obj) {
  var o = new cannyurl.wa.bean.AppClientJsBean();

  if(obj.guid !== undefined && obj.guid != null) {
    o.setGuid(obj.guid);
  }
  if(obj.owner !== undefined && obj.owner != null) {
    o.setOwner(obj.owner);
  }
  if(obj.admin !== undefined && obj.admin != null) {
    o.setAdmin(obj.admin);
  }
  if(obj.name !== undefined && obj.name != null) {
    o.setName(obj.name);
  }
  if(obj.clientId !== undefined && obj.clientId != null) {
    o.setClientId(obj.clientId);
  }
  if(obj.clientCode !== undefined && obj.clientCode != null) {
    o.setClientCode(obj.clientCode);
  }
  if(obj.rootDomain !== undefined && obj.rootDomain != null) {
    o.setRootDomain(obj.rootDomain);
  }
  if(obj.appDomain !== undefined && obj.appDomain != null) {
    o.setAppDomain(obj.appDomain);
  }
  if(obj.useAppDomain !== undefined && obj.useAppDomain != null) {
    o.setUseAppDomain(obj.useAppDomain);
  }
  if(obj.enabled !== undefined && obj.enabled != null) {
    o.setEnabled(obj.enabled);
  }
  if(obj.licenseMode !== undefined && obj.licenseMode != null) {
    o.setLicenseMode(obj.licenseMode);
  }
  if(obj.status !== undefined && obj.status != null) {
    o.setStatus(obj.status);
  }
  if(obj.createdTime !== undefined && obj.createdTime != null) {
    o.setCreatedTime(obj.createdTime);
  }
  if(obj.modifiedTime !== undefined && obj.modifiedTime != null) {
    o.setModifiedTime(obj.modifiedTime);
  }
    
  return o;
};

cannyurl.wa.bean.AppClientJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = cannyurl.wa.bean.AppClientJsBean.create(jsonObj);
  return obj;
};
