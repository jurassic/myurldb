//////////////////////////////////////////////////////////
// <script src="/js/bean/appcustomdomainjsbean-1.0.js"></script>
// Last modified time: 1365474464958.
//////////////////////////////////////////////////////////

var cannyurl = cannyurl || {};
cannyurl.wa = cannyurl.wa || {};
cannyurl.wa.bean = cannyurl.wa.bean || {};
cannyurl.wa.bean.AppCustomDomainJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var guid = generateUuid();
    var appId;
    var siteDomain;
    var domain;
    var verified;
    var status;
    var verifiedTime;
    var createdTime = getCurrentTime();
    var modifiedTime;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getGuid = function() { return guid; };
    this.setGuid = function(value) { guid = value; };
    this.getAppId = function() { return appId; };
    this.setAppId = function(value) { appId = value; };
    this.getSiteDomain = function() { return siteDomain; };
    this.setSiteDomain = function(value) { siteDomain = value; };
    this.getDomain = function() { return domain; };
    this.setDomain = function(value) { domain = value; };
    this.getVerified = function() { return verified; };
    this.setVerified = function(value) { verified = value; };
    this.getStatus = function() { return status; };
    this.setStatus = function(value) { status = value; };
    this.getVerifiedTime = function() { return verifiedTime; };
    this.setVerifiedTime = function(value) { verifiedTime = value; };
    this.getCreatedTime = function() { return createdTime; };
    this.setCreatedTime = function(value) { createdTime = value; };
    this.getModifiedTime = function() { return modifiedTime; };
    this.setModifiedTime = function(value) { modifiedTime = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new cannyurl.wa.bean.AppCustomDomainJsBean();

      o.setGuid(generateUuid());
      if(appId !== undefined && appId != null) {
        o.setAppId(appId);
      }
      if(siteDomain !== undefined && siteDomain != null) {
        o.setSiteDomain(siteDomain);
      }
      if(domain !== undefined && domain != null) {
        o.setDomain(domain);
      }
      if(verified !== undefined && verified != null) {
        o.setVerified(verified);
      }
      if(status !== undefined && status != null) {
        o.setStatus(status);
      }
      if(verifiedTime !== undefined && verifiedTime != null) {
        o.setVerifiedTime(verifiedTime);
      }
      o.setCreatedTime(getCurrentTime());
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(guid !== undefined && guid != null) {
        jsonObj.guid = guid;
      } // Otherwise ignore...
      if(appId !== undefined && appId != null) {
        jsonObj.appId = appId;
      } // Otherwise ignore...
      if(siteDomain !== undefined && siteDomain != null) {
        jsonObj.siteDomain = siteDomain;
      } // Otherwise ignore...
      if(domain !== undefined && domain != null) {
        jsonObj.domain = domain;
      } // Otherwise ignore...
      if(verified !== undefined && verified != null) {
        jsonObj.verified = verified;
      } // Otherwise ignore...
      if(status !== undefined && status != null) {
        jsonObj.status = status;
      } // Otherwise ignore...
      if(verifiedTime !== undefined && verifiedTime != null) {
        jsonObj.verifiedTime = verifiedTime;
      } // Otherwise ignore...
      if(createdTime !== undefined && createdTime != null) {
        jsonObj.createdTime = createdTime;
      } // Otherwise ignore...
      if(modifiedTime !== undefined && modifiedTime != null) {
        jsonObj.modifiedTime = modifiedTime;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(guid) {
        str += "\"guid\":\"" + guid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"guid\":null, ";
      }
      if(appId) {
        str += "\"appId\":\"" + appId + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"appId\":null, ";
      }
      if(siteDomain) {
        str += "\"siteDomain\":\"" + siteDomain + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"siteDomain\":null, ";
      }
      if(domain) {
        str += "\"domain\":\"" + domain + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"domain\":null, ";
      }
      if(verified) {
        str += "\"verified\":" + verified + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"verified\":null, ";
      }
      if(status) {
        str += "\"status\":\"" + status + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"status\":null, ";
      }
      if(verifiedTime) {
        str += "\"verifiedTime\":" + verifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"verifiedTime\":null, ";
      }
      if(createdTime) {
        str += "\"createdTime\":" + createdTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"createdTime\":null, ";
      }
      if(modifiedTime) {
        str += "\"modifiedTime\":" + modifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"modifiedTime\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "guid:" + guid + ", ";
      str += "appId:" + appId + ", ";
      str += "siteDomain:" + siteDomain + ", ";
      str += "domain:" + domain + ", ";
      str += "verified:" + verified + ", ";
      str += "status:" + status + ", ";
      str += "verifiedTime:" + verifiedTime + ", ";
      str += "createdTime:" + createdTime + ", ";
      str += "modifiedTime:" + modifiedTime + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

cannyurl.wa.bean.AppCustomDomainJsBean.create = function(obj) {
  var o = new cannyurl.wa.bean.AppCustomDomainJsBean();

  if(obj.guid !== undefined && obj.guid != null) {
    o.setGuid(obj.guid);
  }
  if(obj.appId !== undefined && obj.appId != null) {
    o.setAppId(obj.appId);
  }
  if(obj.siteDomain !== undefined && obj.siteDomain != null) {
    o.setSiteDomain(obj.siteDomain);
  }
  if(obj.domain !== undefined && obj.domain != null) {
    o.setDomain(obj.domain);
  }
  if(obj.verified !== undefined && obj.verified != null) {
    o.setVerified(obj.verified);
  }
  if(obj.status !== undefined && obj.status != null) {
    o.setStatus(obj.status);
  }
  if(obj.verifiedTime !== undefined && obj.verifiedTime != null) {
    o.setVerifiedTime(obj.verifiedTime);
  }
  if(obj.createdTime !== undefined && obj.createdTime != null) {
    o.setCreatedTime(obj.createdTime);
  }
  if(obj.modifiedTime !== undefined && obj.modifiedTime != null) {
    o.setModifiedTime(obj.modifiedTime);
  }
    
  return o;
};

cannyurl.wa.bean.AppCustomDomainJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = cannyurl.wa.bean.AppCustomDomainJsBean.create(jsonObj);
  return obj;
};
