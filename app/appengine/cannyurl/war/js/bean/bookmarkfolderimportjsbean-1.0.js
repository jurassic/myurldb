//////////////////////////////////////////////////////////
// <script src="/js/bean/bookmarkfolderimportjsbean-1.0.js"></script>
// Last modified time: 1365474465603.
//////////////////////////////////////////////////////////

var cannyurl = cannyurl || {};
cannyurl.wa = cannyurl.wa || {};
cannyurl.wa.bean = cannyurl.wa.bean || {};
cannyurl.wa.bean.BookmarkFolderImportJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var guid = generateUuid();
    var user;
    var precedence;
    var importType;
    var importedFolder;
    var importedFolderUser;
    var importedFolderTitle;
    var importedFolderPath;
    var status;
    var note;
    var bookmarkFolder;
    var createdTime = getCurrentTime();
    var modifiedTime;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getGuid = function() { return guid; };
    this.setGuid = function(value) { guid = value; };
    this.getUser = function() { return user; };
    this.setUser = function(value) { user = value; };
    this.getPrecedence = function() { return precedence; };
    this.setPrecedence = function(value) { precedence = value; };
    this.getImportType = function() { return importType; };
    this.setImportType = function(value) { importType = value; };
    this.getImportedFolder = function() { return importedFolder; };
    this.setImportedFolder = function(value) { importedFolder = value; };
    this.getImportedFolderUser = function() { return importedFolderUser; };
    this.setImportedFolderUser = function(value) { importedFolderUser = value; };
    this.getImportedFolderTitle = function() { return importedFolderTitle; };
    this.setImportedFolderTitle = function(value) { importedFolderTitle = value; };
    this.getImportedFolderPath = function() { return importedFolderPath; };
    this.setImportedFolderPath = function(value) { importedFolderPath = value; };
    this.getStatus = function() { return status; };
    this.setStatus = function(value) { status = value; };
    this.getNote = function() { return note; };
    this.setNote = function(value) { note = value; };
    this.getBookmarkFolder = function() { return bookmarkFolder; };
    this.setBookmarkFolder = function(value) { bookmarkFolder = value; };
    this.getCreatedTime = function() { return createdTime; };
    this.setCreatedTime = function(value) { createdTime = value; };
    this.getModifiedTime = function() { return modifiedTime; };
    this.setModifiedTime = function(value) { modifiedTime = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new cannyurl.wa.bean.BookmarkFolderImportJsBean();

      o.setGuid(generateUuid());
      if(user !== undefined && user != null) {
        o.setUser(user);
      }
      if(precedence !== undefined && precedence != null) {
        o.setPrecedence(precedence);
      }
      if(importType !== undefined && importType != null) {
        o.setImportType(importType);
      }
      if(importedFolder !== undefined && importedFolder != null) {
        o.setImportedFolder(importedFolder);
      }
      if(importedFolderUser !== undefined && importedFolderUser != null) {
        o.setImportedFolderUser(importedFolderUser);
      }
      if(importedFolderTitle !== undefined && importedFolderTitle != null) {
        o.setImportedFolderTitle(importedFolderTitle);
      }
      if(importedFolderPath !== undefined && importedFolderPath != null) {
        o.setImportedFolderPath(importedFolderPath);
      }
      if(status !== undefined && status != null) {
        o.setStatus(status);
      }
      if(note !== undefined && note != null) {
        o.setNote(note);
      }
      if(bookmarkFolder !== undefined && bookmarkFolder != null) {
        o.setBookmarkFolder(bookmarkFolder);
      }
      o.setCreatedTime(getCurrentTime());
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(guid !== undefined && guid != null) {
        jsonObj.guid = guid;
      } // Otherwise ignore...
      if(user !== undefined && user != null) {
        jsonObj.user = user;
      } // Otherwise ignore...
      if(precedence !== undefined && precedence != null) {
        jsonObj.precedence = precedence;
      } // Otherwise ignore...
      if(importType !== undefined && importType != null) {
        jsonObj.importType = importType;
      } // Otherwise ignore...
      if(importedFolder !== undefined && importedFolder != null) {
        jsonObj.importedFolder = importedFolder;
      } // Otherwise ignore...
      if(importedFolderUser !== undefined && importedFolderUser != null) {
        jsonObj.importedFolderUser = importedFolderUser;
      } // Otherwise ignore...
      if(importedFolderTitle !== undefined && importedFolderTitle != null) {
        jsonObj.importedFolderTitle = importedFolderTitle;
      } // Otherwise ignore...
      if(importedFolderPath !== undefined && importedFolderPath != null) {
        jsonObj.importedFolderPath = importedFolderPath;
      } // Otherwise ignore...
      if(status !== undefined && status != null) {
        jsonObj.status = status;
      } // Otherwise ignore...
      if(note !== undefined && note != null) {
        jsonObj.note = note;
      } // Otherwise ignore...
      if(bookmarkFolder !== undefined && bookmarkFolder != null) {
        jsonObj.bookmarkFolder = bookmarkFolder;
      } // Otherwise ignore...
      if(createdTime !== undefined && createdTime != null) {
        jsonObj.createdTime = createdTime;
      } // Otherwise ignore...
      if(modifiedTime !== undefined && modifiedTime != null) {
        jsonObj.modifiedTime = modifiedTime;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(guid) {
        str += "\"guid\":\"" + guid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"guid\":null, ";
      }
      if(user) {
        str += "\"user\":\"" + user + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"user\":null, ";
      }
      if(precedence) {
        str += "\"precedence\":" + precedence + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"precedence\":null, ";
      }
      if(importType) {
        str += "\"importType\":\"" + importType + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"importType\":null, ";
      }
      if(importedFolder) {
        str += "\"importedFolder\":\"" + importedFolder + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"importedFolder\":null, ";
      }
      if(importedFolderUser) {
        str += "\"importedFolderUser\":\"" + importedFolderUser + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"importedFolderUser\":null, ";
      }
      if(importedFolderTitle) {
        str += "\"importedFolderTitle\":\"" + importedFolderTitle + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"importedFolderTitle\":null, ";
      }
      if(importedFolderPath) {
        str += "\"importedFolderPath\":\"" + importedFolderPath + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"importedFolderPath\":null, ";
      }
      if(status) {
        str += "\"status\":\"" + status + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"status\":null, ";
      }
      if(note) {
        str += "\"note\":\"" + note + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"note\":null, ";
      }
      if(bookmarkFolder) {
        str += "\"bookmarkFolder\":\"" + bookmarkFolder + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"bookmarkFolder\":null, ";
      }
      if(createdTime) {
        str += "\"createdTime\":" + createdTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"createdTime\":null, ";
      }
      if(modifiedTime) {
        str += "\"modifiedTime\":" + modifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"modifiedTime\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "guid:" + guid + ", ";
      str += "user:" + user + ", ";
      str += "precedence:" + precedence + ", ";
      str += "importType:" + importType + ", ";
      str += "importedFolder:" + importedFolder + ", ";
      str += "importedFolderUser:" + importedFolderUser + ", ";
      str += "importedFolderTitle:" + importedFolderTitle + ", ";
      str += "importedFolderPath:" + importedFolderPath + ", ";
      str += "status:" + status + ", ";
      str += "note:" + note + ", ";
      str += "bookmarkFolder:" + bookmarkFolder + ", ";
      str += "createdTime:" + createdTime + ", ";
      str += "modifiedTime:" + modifiedTime + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

cannyurl.wa.bean.BookmarkFolderImportJsBean.create = function(obj) {
  var o = new cannyurl.wa.bean.BookmarkFolderImportJsBean();

  if(obj.guid !== undefined && obj.guid != null) {
    o.setGuid(obj.guid);
  }
  if(obj.user !== undefined && obj.user != null) {
    o.setUser(obj.user);
  }
  if(obj.precedence !== undefined && obj.precedence != null) {
    o.setPrecedence(obj.precedence);
  }
  if(obj.importType !== undefined && obj.importType != null) {
    o.setImportType(obj.importType);
  }
  if(obj.importedFolder !== undefined && obj.importedFolder != null) {
    o.setImportedFolder(obj.importedFolder);
  }
  if(obj.importedFolderUser !== undefined && obj.importedFolderUser != null) {
    o.setImportedFolderUser(obj.importedFolderUser);
  }
  if(obj.importedFolderTitle !== undefined && obj.importedFolderTitle != null) {
    o.setImportedFolderTitle(obj.importedFolderTitle);
  }
  if(obj.importedFolderPath !== undefined && obj.importedFolderPath != null) {
    o.setImportedFolderPath(obj.importedFolderPath);
  }
  if(obj.status !== undefined && obj.status != null) {
    o.setStatus(obj.status);
  }
  if(obj.note !== undefined && obj.note != null) {
    o.setNote(obj.note);
  }
  if(obj.bookmarkFolder !== undefined && obj.bookmarkFolder != null) {
    o.setBookmarkFolder(obj.bookmarkFolder);
  }
  if(obj.createdTime !== undefined && obj.createdTime != null) {
    o.setCreatedTime(obj.createdTime);
  }
  if(obj.modifiedTime !== undefined && obj.modifiedTime != null) {
    o.setModifiedTime(obj.modifiedTime);
  }
    
  return o;
};

cannyurl.wa.bean.BookmarkFolderImportJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = cannyurl.wa.bean.BookmarkFolderImportJsBean.create(jsonObj);
  return obj;
};
