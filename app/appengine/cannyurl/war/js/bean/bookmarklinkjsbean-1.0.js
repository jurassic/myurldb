//////////////////////////////////////////////////////////
// <script src="/js/bean/bookmarklinkjsbean-1.0.js"></script>
// Last modified time: 1365474465572.
//////////////////////////////////////////////////////////

var cannyurl = cannyurl || {};
cannyurl.wa = cannyurl.wa || {};
cannyurl.wa.bean = cannyurl.wa.bean || {};
cannyurl.wa.bean.BookmarkLinkJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var guid = generateUuid();
    var appClient;
    var clientRootDomain;
    var user;
    var shortLink;
    var domain;
    var token;
    var longUrl;
    var shortUrl;
    var internal;
    var caching;
    var memo;
    var status;
    var note;
    var expirationTime;
    var bookmarkFolder;
    var contentTag;
    var referenceElement;
    var elementType;
    var keywordLink;
    var createdTime = getCurrentTime();
    var modifiedTime;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getGuid = function() { return guid; };
    this.setGuid = function(value) { guid = value; };
    this.getAppClient = function() { return appClient; };
    this.setAppClient = function(value) { appClient = value; };
    this.getClientRootDomain = function() { return clientRootDomain; };
    this.setClientRootDomain = function(value) { clientRootDomain = value; };
    this.getUser = function() { return user; };
    this.setUser = function(value) { user = value; };
    this.getShortLink = function() { return shortLink; };
    this.setShortLink = function(value) { shortLink = value; };
    this.getDomain = function() { return domain; };
    this.setDomain = function(value) { domain = value; };
    this.getToken = function() { return token; };
    this.setToken = function(value) { token = value; };
    this.getLongUrl = function() { return longUrl; };
    this.setLongUrl = function(value) { longUrl = value; };
    this.getShortUrl = function() { return shortUrl; };
    this.setShortUrl = function(value) { shortUrl = value; };
    this.getInternal = function() { return internal; };
    this.setInternal = function(value) { internal = value; };
    this.getCaching = function() { return caching; };
    this.setCaching = function(value) { caching = value; };
    this.getMemo = function() { return memo; };
    this.setMemo = function(value) { memo = value; };
    this.getStatus = function() { return status; };
    this.setStatus = function(value) { status = value; };
    this.getNote = function() { return note; };
    this.setNote = function(value) { note = value; };
    this.getExpirationTime = function() { return expirationTime; };
    this.setExpirationTime = function(value) { expirationTime = value; };
    this.getBookmarkFolder = function() { return bookmarkFolder; };
    this.setBookmarkFolder = function(value) { bookmarkFolder = value; };
    this.getContentTag = function() { return contentTag; };
    this.setContentTag = function(value) { contentTag = value; };
    this.getReferenceElement = function() { return referenceElement; };
    this.setReferenceElement = function(value) { referenceElement = value; };
    this.getElementType = function() { return elementType; };
    this.setElementType = function(value) { elementType = value; };
    this.getKeywordLink = function() { return keywordLink; };
    this.setKeywordLink = function(value) { keywordLink = value; };
    this.getCreatedTime = function() { return createdTime; };
    this.setCreatedTime = function(value) { createdTime = value; };
    this.getModifiedTime = function() { return modifiedTime; };
    this.setModifiedTime = function(value) { modifiedTime = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new cannyurl.wa.bean.BookmarkLinkJsBean();

      o.setGuid(generateUuid());
      if(appClient !== undefined && appClient != null) {
        o.setAppClient(appClient);
      }
      if(clientRootDomain !== undefined && clientRootDomain != null) {
        o.setClientRootDomain(clientRootDomain);
      }
      if(user !== undefined && user != null) {
        o.setUser(user);
      }
      if(shortLink !== undefined && shortLink != null) {
        o.setShortLink(shortLink);
      }
      if(domain !== undefined && domain != null) {
        o.setDomain(domain);
      }
      if(token !== undefined && token != null) {
        o.setToken(token);
      }
      if(longUrl !== undefined && longUrl != null) {
        o.setLongUrl(longUrl);
      }
      if(shortUrl !== undefined && shortUrl != null) {
        o.setShortUrl(shortUrl);
      }
      if(internal !== undefined && internal != null) {
        o.setInternal(internal);
      }
      if(caching !== undefined && caching != null) {
        o.setCaching(caching);
      }
      if(memo !== undefined && memo != null) {
        o.setMemo(memo);
      }
      if(status !== undefined && status != null) {
        o.setStatus(status);
      }
      if(note !== undefined && note != null) {
        o.setNote(note);
      }
      if(expirationTime !== undefined && expirationTime != null) {
        o.setExpirationTime(expirationTime);
      }
      if(bookmarkFolder !== undefined && bookmarkFolder != null) {
        o.setBookmarkFolder(bookmarkFolder);
      }
      if(contentTag !== undefined && contentTag != null) {
        o.setContentTag(contentTag);
      }
      if(referenceElement !== undefined && referenceElement != null) {
        o.setReferenceElement(referenceElement);
      }
      if(elementType !== undefined && elementType != null) {
        o.setElementType(elementType);
      }
      if(keywordLink !== undefined && keywordLink != null) {
        o.setKeywordLink(keywordLink);
      }
      o.setCreatedTime(getCurrentTime());
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(guid !== undefined && guid != null) {
        jsonObj.guid = guid;
      } // Otherwise ignore...
      if(appClient !== undefined && appClient != null) {
        jsonObj.appClient = appClient;
      } // Otherwise ignore...
      if(clientRootDomain !== undefined && clientRootDomain != null) {
        jsonObj.clientRootDomain = clientRootDomain;
      } // Otherwise ignore...
      if(user !== undefined && user != null) {
        jsonObj.user = user;
      } // Otherwise ignore...
      if(shortLink !== undefined && shortLink != null) {
        jsonObj.shortLink = shortLink;
      } // Otherwise ignore...
      if(domain !== undefined && domain != null) {
        jsonObj.domain = domain;
      } // Otherwise ignore...
      if(token !== undefined && token != null) {
        jsonObj.token = token;
      } // Otherwise ignore...
      if(longUrl !== undefined && longUrl != null) {
        jsonObj.longUrl = longUrl;
      } // Otherwise ignore...
      if(shortUrl !== undefined && shortUrl != null) {
        jsonObj.shortUrl = shortUrl;
      } // Otherwise ignore...
      if(internal !== undefined && internal != null) {
        jsonObj.internal = internal;
      } // Otherwise ignore...
      if(caching !== undefined && caching != null) {
        jsonObj.caching = caching;
      } // Otherwise ignore...
      if(memo !== undefined && memo != null) {
        jsonObj.memo = memo;
      } // Otherwise ignore...
      if(status !== undefined && status != null) {
        jsonObj.status = status;
      } // Otherwise ignore...
      if(note !== undefined && note != null) {
        jsonObj.note = note;
      } // Otherwise ignore...
      if(expirationTime !== undefined && expirationTime != null) {
        jsonObj.expirationTime = expirationTime;
      } // Otherwise ignore...
      if(bookmarkFolder !== undefined && bookmarkFolder != null) {
        jsonObj.bookmarkFolder = bookmarkFolder;
      } // Otherwise ignore...
      if(contentTag !== undefined && contentTag != null) {
        jsonObj.contentTag = contentTag;
      } // Otherwise ignore...
      if(referenceElement !== undefined && referenceElement != null) {
        jsonObj.referenceElement = referenceElement;
      } // Otherwise ignore...
      if(elementType !== undefined && elementType != null) {
        jsonObj.elementType = elementType;
      } // Otherwise ignore...
      if(keywordLink !== undefined && keywordLink != null) {
        jsonObj.keywordLink = keywordLink;
      } // Otherwise ignore...
      if(createdTime !== undefined && createdTime != null) {
        jsonObj.createdTime = createdTime;
      } // Otherwise ignore...
      if(modifiedTime !== undefined && modifiedTime != null) {
        jsonObj.modifiedTime = modifiedTime;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(guid) {
        str += "\"guid\":\"" + guid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"guid\":null, ";
      }
      if(appClient) {
        str += "\"appClient\":\"" + appClient + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"appClient\":null, ";
      }
      if(clientRootDomain) {
        str += "\"clientRootDomain\":\"" + clientRootDomain + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"clientRootDomain\":null, ";
      }
      if(user) {
        str += "\"user\":\"" + user + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"user\":null, ";
      }
      if(shortLink) {
        str += "\"shortLink\":\"" + shortLink + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"shortLink\":null, ";
      }
      if(domain) {
        str += "\"domain\":\"" + domain + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"domain\":null, ";
      }
      if(token) {
        str += "\"token\":\"" + token + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"token\":null, ";
      }
      if(longUrl) {
        str += "\"longUrl\":\"" + longUrl + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"longUrl\":null, ";
      }
      if(shortUrl) {
        str += "\"shortUrl\":\"" + shortUrl + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"shortUrl\":null, ";
      }
      if(internal) {
        str += "\"internal\":" + internal + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"internal\":null, ";
      }
      if(caching) {
        str += "\"caching\":" + caching + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"caching\":null, ";
      }
      if(memo) {
        str += "\"memo\":\"" + memo + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"memo\":null, ";
      }
      if(status) {
        str += "\"status\":\"" + status + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"status\":null, ";
      }
      if(note) {
        str += "\"note\":\"" + note + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"note\":null, ";
      }
      if(expirationTime) {
        str += "\"expirationTime\":" + expirationTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"expirationTime\":null, ";
      }
      if(bookmarkFolder) {
        str += "\"bookmarkFolder\":\"" + bookmarkFolder + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"bookmarkFolder\":null, ";
      }
      if(contentTag) {
        str += "\"contentTag\":\"" + contentTag + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"contentTag\":null, ";
      }
      if(referenceElement) {
        str += "\"referenceElement\":\"" + referenceElement + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"referenceElement\":null, ";
      }
      if(elementType) {
        str += "\"elementType\":\"" + elementType + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"elementType\":null, ";
      }
      if(keywordLink) {
        str += "\"keywordLink\":\"" + keywordLink + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"keywordLink\":null, ";
      }
      if(createdTime) {
        str += "\"createdTime\":" + createdTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"createdTime\":null, ";
      }
      if(modifiedTime) {
        str += "\"modifiedTime\":" + modifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"modifiedTime\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "guid:" + guid + ", ";
      str += "appClient:" + appClient + ", ";
      str += "clientRootDomain:" + clientRootDomain + ", ";
      str += "user:" + user + ", ";
      str += "shortLink:" + shortLink + ", ";
      str += "domain:" + domain + ", ";
      str += "token:" + token + ", ";
      str += "longUrl:" + longUrl + ", ";
      str += "shortUrl:" + shortUrl + ", ";
      str += "internal:" + internal + ", ";
      str += "caching:" + caching + ", ";
      str += "memo:" + memo + ", ";
      str += "status:" + status + ", ";
      str += "note:" + note + ", ";
      str += "expirationTime:" + expirationTime + ", ";
      str += "bookmarkFolder:" + bookmarkFolder + ", ";
      str += "contentTag:" + contentTag + ", ";
      str += "referenceElement:" + referenceElement + ", ";
      str += "elementType:" + elementType + ", ";
      str += "keywordLink:" + keywordLink + ", ";
      str += "createdTime:" + createdTime + ", ";
      str += "modifiedTime:" + modifiedTime + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

cannyurl.wa.bean.BookmarkLinkJsBean.create = function(obj) {
  var o = new cannyurl.wa.bean.BookmarkLinkJsBean();

  if(obj.guid !== undefined && obj.guid != null) {
    o.setGuid(obj.guid);
  }
  if(obj.appClient !== undefined && obj.appClient != null) {
    o.setAppClient(obj.appClient);
  }
  if(obj.clientRootDomain !== undefined && obj.clientRootDomain != null) {
    o.setClientRootDomain(obj.clientRootDomain);
  }
  if(obj.user !== undefined && obj.user != null) {
    o.setUser(obj.user);
  }
  if(obj.shortLink !== undefined && obj.shortLink != null) {
    o.setShortLink(obj.shortLink);
  }
  if(obj.domain !== undefined && obj.domain != null) {
    o.setDomain(obj.domain);
  }
  if(obj.token !== undefined && obj.token != null) {
    o.setToken(obj.token);
  }
  if(obj.longUrl !== undefined && obj.longUrl != null) {
    o.setLongUrl(obj.longUrl);
  }
  if(obj.shortUrl !== undefined && obj.shortUrl != null) {
    o.setShortUrl(obj.shortUrl);
  }
  if(obj.internal !== undefined && obj.internal != null) {
    o.setInternal(obj.internal);
  }
  if(obj.caching !== undefined && obj.caching != null) {
    o.setCaching(obj.caching);
  }
  if(obj.memo !== undefined && obj.memo != null) {
    o.setMemo(obj.memo);
  }
  if(obj.status !== undefined && obj.status != null) {
    o.setStatus(obj.status);
  }
  if(obj.note !== undefined && obj.note != null) {
    o.setNote(obj.note);
  }
  if(obj.expirationTime !== undefined && obj.expirationTime != null) {
    o.setExpirationTime(obj.expirationTime);
  }
  if(obj.bookmarkFolder !== undefined && obj.bookmarkFolder != null) {
    o.setBookmarkFolder(obj.bookmarkFolder);
  }
  if(obj.contentTag !== undefined && obj.contentTag != null) {
    o.setContentTag(obj.contentTag);
  }
  if(obj.referenceElement !== undefined && obj.referenceElement != null) {
    o.setReferenceElement(obj.referenceElement);
  }
  if(obj.elementType !== undefined && obj.elementType != null) {
    o.setElementType(obj.elementType);
  }
  if(obj.keywordLink !== undefined && obj.keywordLink != null) {
    o.setKeywordLink(obj.keywordLink);
  }
  if(obj.createdTime !== undefined && obj.createdTime != null) {
    o.setCreatedTime(obj.createdTime);
  }
  if(obj.modifiedTime !== undefined && obj.modifiedTime != null) {
    o.setModifiedTime(obj.modifiedTime);
  }
    
  return o;
};

cannyurl.wa.bean.BookmarkLinkJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = cannyurl.wa.bean.BookmarkLinkJsBean.create(jsonObj);
  return obj;
};
