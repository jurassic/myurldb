//////////////////////////////////////////////////////////
// <script src="/js/bean/celllatitudelongitudejsbean-1.0.js"></script>
// Last modified time: 1365474464831.
//////////////////////////////////////////////////////////

var cannyurl = cannyurl || {};
cannyurl.wa = cannyurl.wa || {};
cannyurl.wa.bean = cannyurl.wa.bean || {};
cannyurl.wa.bean.CellLatitudeLongitudeJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var scale;
    var latitude;
    var longitude;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getScale = function() { return scale; };
    this.setScale = function(value) { scale = value; };
    this.getLatitude = function() { return latitude; };
    this.setLatitude = function(value) { latitude = value; };
    this.getLongitude = function() { return longitude; };
    this.setLongitude = function(value) { longitude = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new cannyurl.wa.bean.CellLatitudeLongitudeJsBean();

      if(scale !== undefined && scale != null) {
        o.setScale(scale);
      }
      if(latitude !== undefined && latitude != null) {
        o.setLatitude(latitude);
      }
      if(longitude !== undefined && longitude != null) {
        o.setLongitude(longitude);
      }
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(scale !== undefined && scale != null) {
        jsonObj.scale = scale;
      } // Otherwise ignore...
      if(latitude !== undefined && latitude != null) {
        jsonObj.latitude = latitude;
      } // Otherwise ignore...
      if(longitude !== undefined && longitude != null) {
        jsonObj.longitude = longitude;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(scale) {
        str += "\"scale\":" + scale + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"scale\":null, ";
      }
      if(latitude) {
        str += "\"latitude\":" + latitude + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"latitude\":null, ";
      }
      if(longitude) {
        str += "\"longitude\":" + longitude + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"longitude\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "scale:" + scale + ", ";
      str += "latitude:" + latitude + ", ";
      str += "longitude:" + longitude + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

cannyurl.wa.bean.CellLatitudeLongitudeJsBean.create = function(obj) {
  var o = new cannyurl.wa.bean.CellLatitudeLongitudeJsBean();

  if(obj.scale !== undefined && obj.scale != null) {
    o.setScale(obj.scale);
  }
  if(obj.latitude !== undefined && obj.latitude != null) {
    o.setLatitude(obj.latitude);
  }
  if(obj.longitude !== undefined && obj.longitude != null) {
    o.setLongitude(obj.longitude);
  }
    
  return o;
};

cannyurl.wa.bean.CellLatitudeLongitudeJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = cannyurl.wa.bean.CellLatitudeLongitudeJsBean.create(jsonObj);
  return obj;
};
