//////////////////////////////////////////////////////////
// <script src="/js/bean/clientsettingjsbean-1.0.js"></script>
// Last modified time: 1365474465115.
//////////////////////////////////////////////////////////

var cannyurl = cannyurl || {};
cannyurl.wa = cannyurl.wa || {};
cannyurl.wa.bean = cannyurl.wa.bean || {};
cannyurl.wa.bean.ClientSettingJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var guid = generateUuid();
    var appClient;
    var admin;
    var autoRedirectEnabled;
    var viewEnabled;
    var shareEnabled;
    var defaultBaseDomain;
    var defaultDomainType;
    var defaultTokenType;
    var defaultRedirectType;
    var defaultFlashDuration;
    var defaultAccessType;
    var defaultViewType;
    var createdTime = getCurrentTime();
    var modifiedTime;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getGuid = function() { return guid; };
    this.setGuid = function(value) { guid = value; };
    this.getAppClient = function() { return appClient; };
    this.setAppClient = function(value) { appClient = value; };
    this.getAdmin = function() { return admin; };
    this.setAdmin = function(value) { admin = value; };
    this.getAutoRedirectEnabled = function() { return autoRedirectEnabled; };
    this.setAutoRedirectEnabled = function(value) { autoRedirectEnabled = value; };
    this.getViewEnabled = function() { return viewEnabled; };
    this.setViewEnabled = function(value) { viewEnabled = value; };
    this.getShareEnabled = function() { return shareEnabled; };
    this.setShareEnabled = function(value) { shareEnabled = value; };
    this.getDefaultBaseDomain = function() { return defaultBaseDomain; };
    this.setDefaultBaseDomain = function(value) { defaultBaseDomain = value; };
    this.getDefaultDomainType = function() { return defaultDomainType; };
    this.setDefaultDomainType = function(value) { defaultDomainType = value; };
    this.getDefaultTokenType = function() { return defaultTokenType; };
    this.setDefaultTokenType = function(value) { defaultTokenType = value; };
    this.getDefaultRedirectType = function() { return defaultRedirectType; };
    this.setDefaultRedirectType = function(value) { defaultRedirectType = value; };
    this.getDefaultFlashDuration = function() { return defaultFlashDuration; };
    this.setDefaultFlashDuration = function(value) { defaultFlashDuration = value; };
    this.getDefaultAccessType = function() { return defaultAccessType; };
    this.setDefaultAccessType = function(value) { defaultAccessType = value; };
    this.getDefaultViewType = function() { return defaultViewType; };
    this.setDefaultViewType = function(value) { defaultViewType = value; };
    this.getCreatedTime = function() { return createdTime; };
    this.setCreatedTime = function(value) { createdTime = value; };
    this.getModifiedTime = function() { return modifiedTime; };
    this.setModifiedTime = function(value) { modifiedTime = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new cannyurl.wa.bean.ClientSettingJsBean();

      o.setGuid(generateUuid());
      if(appClient !== undefined && appClient != null) {
        o.setAppClient(appClient);
      }
      if(admin !== undefined && admin != null) {
        o.setAdmin(admin);
      }
      if(autoRedirectEnabled !== undefined && autoRedirectEnabled != null) {
        o.setAutoRedirectEnabled(autoRedirectEnabled);
      }
      if(viewEnabled !== undefined && viewEnabled != null) {
        o.setViewEnabled(viewEnabled);
      }
      if(shareEnabled !== undefined && shareEnabled != null) {
        o.setShareEnabled(shareEnabled);
      }
      if(defaultBaseDomain !== undefined && defaultBaseDomain != null) {
        o.setDefaultBaseDomain(defaultBaseDomain);
      }
      if(defaultDomainType !== undefined && defaultDomainType != null) {
        o.setDefaultDomainType(defaultDomainType);
      }
      if(defaultTokenType !== undefined && defaultTokenType != null) {
        o.setDefaultTokenType(defaultTokenType);
      }
      if(defaultRedirectType !== undefined && defaultRedirectType != null) {
        o.setDefaultRedirectType(defaultRedirectType);
      }
      if(defaultFlashDuration !== undefined && defaultFlashDuration != null) {
        o.setDefaultFlashDuration(defaultFlashDuration);
      }
      if(defaultAccessType !== undefined && defaultAccessType != null) {
        o.setDefaultAccessType(defaultAccessType);
      }
      if(defaultViewType !== undefined && defaultViewType != null) {
        o.setDefaultViewType(defaultViewType);
      }
      o.setCreatedTime(getCurrentTime());
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(guid !== undefined && guid != null) {
        jsonObj.guid = guid;
      } // Otherwise ignore...
      if(appClient !== undefined && appClient != null) {
        jsonObj.appClient = appClient;
      } // Otherwise ignore...
      if(admin !== undefined && admin != null) {
        jsonObj.admin = admin;
      } // Otherwise ignore...
      if(autoRedirectEnabled !== undefined && autoRedirectEnabled != null) {
        jsonObj.autoRedirectEnabled = autoRedirectEnabled;
      } // Otherwise ignore...
      if(viewEnabled !== undefined && viewEnabled != null) {
        jsonObj.viewEnabled = viewEnabled;
      } // Otherwise ignore...
      if(shareEnabled !== undefined && shareEnabled != null) {
        jsonObj.shareEnabled = shareEnabled;
      } // Otherwise ignore...
      if(defaultBaseDomain !== undefined && defaultBaseDomain != null) {
        jsonObj.defaultBaseDomain = defaultBaseDomain;
      } // Otherwise ignore...
      if(defaultDomainType !== undefined && defaultDomainType != null) {
        jsonObj.defaultDomainType = defaultDomainType;
      } // Otherwise ignore...
      if(defaultTokenType !== undefined && defaultTokenType != null) {
        jsonObj.defaultTokenType = defaultTokenType;
      } // Otherwise ignore...
      if(defaultRedirectType !== undefined && defaultRedirectType != null) {
        jsonObj.defaultRedirectType = defaultRedirectType;
      } // Otherwise ignore...
      if(defaultFlashDuration !== undefined && defaultFlashDuration != null) {
        jsonObj.defaultFlashDuration = defaultFlashDuration;
      } // Otherwise ignore...
      if(defaultAccessType !== undefined && defaultAccessType != null) {
        jsonObj.defaultAccessType = defaultAccessType;
      } // Otherwise ignore...
      if(defaultViewType !== undefined && defaultViewType != null) {
        jsonObj.defaultViewType = defaultViewType;
      } // Otherwise ignore...
      if(createdTime !== undefined && createdTime != null) {
        jsonObj.createdTime = createdTime;
      } // Otherwise ignore...
      if(modifiedTime !== undefined && modifiedTime != null) {
        jsonObj.modifiedTime = modifiedTime;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(guid) {
        str += "\"guid\":\"" + guid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"guid\":null, ";
      }
      if(appClient) {
        str += "\"appClient\":\"" + appClient + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"appClient\":null, ";
      }
      if(admin) {
        str += "\"admin\":\"" + admin + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"admin\":null, ";
      }
      if(autoRedirectEnabled) {
        str += "\"autoRedirectEnabled\":" + autoRedirectEnabled + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"autoRedirectEnabled\":null, ";
      }
      if(viewEnabled) {
        str += "\"viewEnabled\":" + viewEnabled + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"viewEnabled\":null, ";
      }
      if(shareEnabled) {
        str += "\"shareEnabled\":" + shareEnabled + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"shareEnabled\":null, ";
      }
      if(defaultBaseDomain) {
        str += "\"defaultBaseDomain\":\"" + defaultBaseDomain + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"defaultBaseDomain\":null, ";
      }
      if(defaultDomainType) {
        str += "\"defaultDomainType\":\"" + defaultDomainType + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"defaultDomainType\":null, ";
      }
      if(defaultTokenType) {
        str += "\"defaultTokenType\":\"" + defaultTokenType + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"defaultTokenType\":null, ";
      }
      if(defaultRedirectType) {
        str += "\"defaultRedirectType\":\"" + defaultRedirectType + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"defaultRedirectType\":null, ";
      }
      if(defaultFlashDuration) {
        str += "\"defaultFlashDuration\":" + defaultFlashDuration + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"defaultFlashDuration\":null, ";
      }
      if(defaultAccessType) {
        str += "\"defaultAccessType\":\"" + defaultAccessType + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"defaultAccessType\":null, ";
      }
      if(defaultViewType) {
        str += "\"defaultViewType\":\"" + defaultViewType + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"defaultViewType\":null, ";
      }
      if(createdTime) {
        str += "\"createdTime\":" + createdTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"createdTime\":null, ";
      }
      if(modifiedTime) {
        str += "\"modifiedTime\":" + modifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"modifiedTime\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "guid:" + guid + ", ";
      str += "appClient:" + appClient + ", ";
      str += "admin:" + admin + ", ";
      str += "autoRedirectEnabled:" + autoRedirectEnabled + ", ";
      str += "viewEnabled:" + viewEnabled + ", ";
      str += "shareEnabled:" + shareEnabled + ", ";
      str += "defaultBaseDomain:" + defaultBaseDomain + ", ";
      str += "defaultDomainType:" + defaultDomainType + ", ";
      str += "defaultTokenType:" + defaultTokenType + ", ";
      str += "defaultRedirectType:" + defaultRedirectType + ", ";
      str += "defaultFlashDuration:" + defaultFlashDuration + ", ";
      str += "defaultAccessType:" + defaultAccessType + ", ";
      str += "defaultViewType:" + defaultViewType + ", ";
      str += "createdTime:" + createdTime + ", ";
      str += "modifiedTime:" + modifiedTime + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

cannyurl.wa.bean.ClientSettingJsBean.create = function(obj) {
  var o = new cannyurl.wa.bean.ClientSettingJsBean();

  if(obj.guid !== undefined && obj.guid != null) {
    o.setGuid(obj.guid);
  }
  if(obj.appClient !== undefined && obj.appClient != null) {
    o.setAppClient(obj.appClient);
  }
  if(obj.admin !== undefined && obj.admin != null) {
    o.setAdmin(obj.admin);
  }
  if(obj.autoRedirectEnabled !== undefined && obj.autoRedirectEnabled != null) {
    o.setAutoRedirectEnabled(obj.autoRedirectEnabled);
  }
  if(obj.viewEnabled !== undefined && obj.viewEnabled != null) {
    o.setViewEnabled(obj.viewEnabled);
  }
  if(obj.shareEnabled !== undefined && obj.shareEnabled != null) {
    o.setShareEnabled(obj.shareEnabled);
  }
  if(obj.defaultBaseDomain !== undefined && obj.defaultBaseDomain != null) {
    o.setDefaultBaseDomain(obj.defaultBaseDomain);
  }
  if(obj.defaultDomainType !== undefined && obj.defaultDomainType != null) {
    o.setDefaultDomainType(obj.defaultDomainType);
  }
  if(obj.defaultTokenType !== undefined && obj.defaultTokenType != null) {
    o.setDefaultTokenType(obj.defaultTokenType);
  }
  if(obj.defaultRedirectType !== undefined && obj.defaultRedirectType != null) {
    o.setDefaultRedirectType(obj.defaultRedirectType);
  }
  if(obj.defaultFlashDuration !== undefined && obj.defaultFlashDuration != null) {
    o.setDefaultFlashDuration(obj.defaultFlashDuration);
  }
  if(obj.defaultAccessType !== undefined && obj.defaultAccessType != null) {
    o.setDefaultAccessType(obj.defaultAccessType);
  }
  if(obj.defaultViewType !== undefined && obj.defaultViewType != null) {
    o.setDefaultViewType(obj.defaultViewType);
  }
  if(obj.createdTime !== undefined && obj.createdTime != null) {
    o.setCreatedTime(obj.createdTime);
  }
  if(obj.modifiedTime !== undefined && obj.modifiedTime != null) {
    o.setModifiedTime(obj.modifiedTime);
  }
    
  return o;
};

cannyurl.wa.bean.ClientSettingJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = cannyurl.wa.bean.ClientSettingJsBean.create(jsonObj);
  return obj;
};
