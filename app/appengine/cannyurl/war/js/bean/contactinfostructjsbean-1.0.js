//////////////////////////////////////////////////////////
// <script src="/js/bean/contactinfostructjsbean-1.0.js"></script>
// Last modified time: 1365474464860.
//////////////////////////////////////////////////////////

var cannyurl = cannyurl || {};
cannyurl.wa = cannyurl.wa || {};
cannyurl.wa.bean = cannyurl.wa.bean || {};
cannyurl.wa.bean.ContactInfoStructJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var uuid;
    var streetAddress;
    var locality;
    var region;
    var postalCode;
    var countryName;
    var emailAddress;
    var phoneNumber;
    var faxNumber;
    var website;
    var note;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getUuid = function() { return uuid; };
    this.setUuid = function(value) { uuid = value; };
    this.getStreetAddress = function() { return streetAddress; };
    this.setStreetAddress = function(value) { streetAddress = value; };
    this.getLocality = function() { return locality; };
    this.setLocality = function(value) { locality = value; };
    this.getRegion = function() { return region; };
    this.setRegion = function(value) { region = value; };
    this.getPostalCode = function() { return postalCode; };
    this.setPostalCode = function(value) { postalCode = value; };
    this.getCountryName = function() { return countryName; };
    this.setCountryName = function(value) { countryName = value; };
    this.getEmailAddress = function() { return emailAddress; };
    this.setEmailAddress = function(value) { emailAddress = value; };
    this.getPhoneNumber = function() { return phoneNumber; };
    this.setPhoneNumber = function(value) { phoneNumber = value; };
    this.getFaxNumber = function() { return faxNumber; };
    this.setFaxNumber = function(value) { faxNumber = value; };
    this.getWebsite = function() { return website; };
    this.setWebsite = function(value) { website = value; };
    this.getNote = function() { return note; };
    this.setNote = function(value) { note = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new cannyurl.wa.bean.ContactInfoStructJsBean();

      if(uuid !== undefined && uuid != null) {
        o.setUuid(uuid);
      }
      if(streetAddress !== undefined && streetAddress != null) {
        o.setStreetAddress(streetAddress);
      }
      if(locality !== undefined && locality != null) {
        o.setLocality(locality);
      }
      if(region !== undefined && region != null) {
        o.setRegion(region);
      }
      if(postalCode !== undefined && postalCode != null) {
        o.setPostalCode(postalCode);
      }
      if(countryName !== undefined && countryName != null) {
        o.setCountryName(countryName);
      }
      if(emailAddress !== undefined && emailAddress != null) {
        o.setEmailAddress(emailAddress);
      }
      if(phoneNumber !== undefined && phoneNumber != null) {
        o.setPhoneNumber(phoneNumber);
      }
      if(faxNumber !== undefined && faxNumber != null) {
        o.setFaxNumber(faxNumber);
      }
      if(website !== undefined && website != null) {
        o.setWebsite(website);
      }
      if(note !== undefined && note != null) {
        o.setNote(note);
      }
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(uuid !== undefined && uuid != null) {
        jsonObj.uuid = uuid;
      } // Otherwise ignore...
      if(streetAddress !== undefined && streetAddress != null) {
        jsonObj.streetAddress = streetAddress;
      } // Otherwise ignore...
      if(locality !== undefined && locality != null) {
        jsonObj.locality = locality;
      } // Otherwise ignore...
      if(region !== undefined && region != null) {
        jsonObj.region = region;
      } // Otherwise ignore...
      if(postalCode !== undefined && postalCode != null) {
        jsonObj.postalCode = postalCode;
      } // Otherwise ignore...
      if(countryName !== undefined && countryName != null) {
        jsonObj.countryName = countryName;
      } // Otherwise ignore...
      if(emailAddress !== undefined && emailAddress != null) {
        jsonObj.emailAddress = emailAddress;
      } // Otherwise ignore...
      if(phoneNumber !== undefined && phoneNumber != null) {
        jsonObj.phoneNumber = phoneNumber;
      } // Otherwise ignore...
      if(faxNumber !== undefined && faxNumber != null) {
        jsonObj.faxNumber = faxNumber;
      } // Otherwise ignore...
      if(website !== undefined && website != null) {
        jsonObj.website = website;
      } // Otherwise ignore...
      if(note !== undefined && note != null) {
        jsonObj.note = note;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(uuid) {
        str += "\"uuid\":\"" + uuid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"uuid\":null, ";
      }
      if(streetAddress) {
        str += "\"streetAddress\":\"" + streetAddress + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"streetAddress\":null, ";
      }
      if(locality) {
        str += "\"locality\":\"" + locality + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"locality\":null, ";
      }
      if(region) {
        str += "\"region\":\"" + region + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"region\":null, ";
      }
      if(postalCode) {
        str += "\"postalCode\":\"" + postalCode + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"postalCode\":null, ";
      }
      if(countryName) {
        str += "\"countryName\":\"" + countryName + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"countryName\":null, ";
      }
      if(emailAddress) {
        str += "\"emailAddress\":\"" + emailAddress + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"emailAddress\":null, ";
      }
      if(phoneNumber) {
        str += "\"phoneNumber\":\"" + phoneNumber + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"phoneNumber\":null, ";
      }
      if(faxNumber) {
        str += "\"faxNumber\":\"" + faxNumber + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"faxNumber\":null, ";
      }
      if(website) {
        str += "\"website\":\"" + website + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"website\":null, ";
      }
      if(note) {
        str += "\"note\":\"" + note + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"note\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "uuid:" + uuid + ", ";
      str += "streetAddress:" + streetAddress + ", ";
      str += "locality:" + locality + ", ";
      str += "region:" + region + ", ";
      str += "postalCode:" + postalCode + ", ";
      str += "countryName:" + countryName + ", ";
      str += "emailAddress:" + emailAddress + ", ";
      str += "phoneNumber:" + phoneNumber + ", ";
      str += "faxNumber:" + faxNumber + ", ";
      str += "website:" + website + ", ";
      str += "note:" + note + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

cannyurl.wa.bean.ContactInfoStructJsBean.create = function(obj) {
  var o = new cannyurl.wa.bean.ContactInfoStructJsBean();

  if(obj.uuid !== undefined && obj.uuid != null) {
    o.setUuid(obj.uuid);
  }
  if(obj.streetAddress !== undefined && obj.streetAddress != null) {
    o.setStreetAddress(obj.streetAddress);
  }
  if(obj.locality !== undefined && obj.locality != null) {
    o.setLocality(obj.locality);
  }
  if(obj.region !== undefined && obj.region != null) {
    o.setRegion(obj.region);
  }
  if(obj.postalCode !== undefined && obj.postalCode != null) {
    o.setPostalCode(obj.postalCode);
  }
  if(obj.countryName !== undefined && obj.countryName != null) {
    o.setCountryName(obj.countryName);
  }
  if(obj.emailAddress !== undefined && obj.emailAddress != null) {
    o.setEmailAddress(obj.emailAddress);
  }
  if(obj.phoneNumber !== undefined && obj.phoneNumber != null) {
    o.setPhoneNumber(obj.phoneNumber);
  }
  if(obj.faxNumber !== undefined && obj.faxNumber != null) {
    o.setFaxNumber(obj.faxNumber);
  }
  if(obj.website !== undefined && obj.website != null) {
    o.setWebsite(obj.website);
  }
  if(obj.note !== undefined && obj.note != null) {
    o.setNote(obj.note);
  }
    
  return o;
};

cannyurl.wa.bean.ContactInfoStructJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = cannyurl.wa.bean.ContactInfoStructJsBean.create(jsonObj);
  return obj;
};
