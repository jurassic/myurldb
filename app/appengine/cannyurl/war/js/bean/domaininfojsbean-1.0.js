//////////////////////////////////////////////////////////
// <script src="/js/bean/domaininfojsbean-1.0.js"></script>
// Last modified time: 1365474465654.
//////////////////////////////////////////////////////////

var cannyurl = cannyurl || {};
cannyurl.wa = cannyurl.wa || {};
cannyurl.wa.bean = cannyurl.wa.bean || {};
cannyurl.wa.bean.DomainInfoJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var guid = generateUuid();
    var domain;
    var banned;
    var urlShortener;
    var category;
    var reputation;
    var authority;
    var note;
    var verifiedTime;
    var createdTime = getCurrentTime();
    var modifiedTime;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getGuid = function() { return guid; };
    this.setGuid = function(value) { guid = value; };
    this.getDomain = function() { return domain; };
    this.setDomain = function(value) { domain = value; };
    this.getBanned = function() { return banned; };
    this.setBanned = function(value) { banned = value; };
    this.getUrlShortener = function() { return urlShortener; };
    this.setUrlShortener = function(value) { urlShortener = value; };
    this.getCategory = function() { return category; };
    this.setCategory = function(value) { category = value; };
    this.getReputation = function() { return reputation; };
    this.setReputation = function(value) { reputation = value; };
    this.getAuthority = function() { return authority; };
    this.setAuthority = function(value) { authority = value; };
    this.getNote = function() { return note; };
    this.setNote = function(value) { note = value; };
    this.getVerifiedTime = function() { return verifiedTime; };
    this.setVerifiedTime = function(value) { verifiedTime = value; };
    this.getCreatedTime = function() { return createdTime; };
    this.setCreatedTime = function(value) { createdTime = value; };
    this.getModifiedTime = function() { return modifiedTime; };
    this.setModifiedTime = function(value) { modifiedTime = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new cannyurl.wa.bean.DomainInfoJsBean();

      o.setGuid(generateUuid());
      if(domain !== undefined && domain != null) {
        o.setDomain(domain);
      }
      if(banned !== undefined && banned != null) {
        o.setBanned(banned);
      }
      if(urlShortener !== undefined && urlShortener != null) {
        o.setUrlShortener(urlShortener);
      }
      if(category !== undefined && category != null) {
        o.setCategory(category);
      }
      if(reputation !== undefined && reputation != null) {
        o.setReputation(reputation);
      }
      if(authority !== undefined && authority != null) {
        o.setAuthority(authority);
      }
      if(note !== undefined && note != null) {
        o.setNote(note);
      }
      if(verifiedTime !== undefined && verifiedTime != null) {
        o.setVerifiedTime(verifiedTime);
      }
      o.setCreatedTime(getCurrentTime());
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(guid !== undefined && guid != null) {
        jsonObj.guid = guid;
      } // Otherwise ignore...
      if(domain !== undefined && domain != null) {
        jsonObj.domain = domain;
      } // Otherwise ignore...
      if(banned !== undefined && banned != null) {
        jsonObj.banned = banned;
      } // Otherwise ignore...
      if(urlShortener !== undefined && urlShortener != null) {
        jsonObj.urlShortener = urlShortener;
      } // Otherwise ignore...
      if(category !== undefined && category != null) {
        jsonObj.category = category;
      } // Otherwise ignore...
      if(reputation !== undefined && reputation != null) {
        jsonObj.reputation = reputation;
      } // Otherwise ignore...
      if(authority !== undefined && authority != null) {
        jsonObj.authority = authority;
      } // Otherwise ignore...
      if(note !== undefined && note != null) {
        jsonObj.note = note;
      } // Otherwise ignore...
      if(verifiedTime !== undefined && verifiedTime != null) {
        jsonObj.verifiedTime = verifiedTime;
      } // Otherwise ignore...
      if(createdTime !== undefined && createdTime != null) {
        jsonObj.createdTime = createdTime;
      } // Otherwise ignore...
      if(modifiedTime !== undefined && modifiedTime != null) {
        jsonObj.modifiedTime = modifiedTime;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(guid) {
        str += "\"guid\":\"" + guid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"guid\":null, ";
      }
      if(domain) {
        str += "\"domain\":\"" + domain + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"domain\":null, ";
      }
      if(banned) {
        str += "\"banned\":" + banned + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"banned\":null, ";
      }
      if(urlShortener) {
        str += "\"urlShortener\":" + urlShortener + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"urlShortener\":null, ";
      }
      if(category) {
        str += "\"category\":\"" + category + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"category\":null, ";
      }
      if(reputation) {
        str += "\"reputation\":\"" + reputation + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"reputation\":null, ";
      }
      if(authority) {
        str += "\"authority\":\"" + authority + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"authority\":null, ";
      }
      if(note) {
        str += "\"note\":\"" + note + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"note\":null, ";
      }
      if(verifiedTime) {
        str += "\"verifiedTime\":" + verifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"verifiedTime\":null, ";
      }
      if(createdTime) {
        str += "\"createdTime\":" + createdTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"createdTime\":null, ";
      }
      if(modifiedTime) {
        str += "\"modifiedTime\":" + modifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"modifiedTime\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "guid:" + guid + ", ";
      str += "domain:" + domain + ", ";
      str += "banned:" + banned + ", ";
      str += "urlShortener:" + urlShortener + ", ";
      str += "category:" + category + ", ";
      str += "reputation:" + reputation + ", ";
      str += "authority:" + authority + ", ";
      str += "note:" + note + ", ";
      str += "verifiedTime:" + verifiedTime + ", ";
      str += "createdTime:" + createdTime + ", ";
      str += "modifiedTime:" + modifiedTime + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

cannyurl.wa.bean.DomainInfoJsBean.create = function(obj) {
  var o = new cannyurl.wa.bean.DomainInfoJsBean();

  if(obj.guid !== undefined && obj.guid != null) {
    o.setGuid(obj.guid);
  }
  if(obj.domain !== undefined && obj.domain != null) {
    o.setDomain(obj.domain);
  }
  if(obj.banned !== undefined && obj.banned != null) {
    o.setBanned(obj.banned);
  }
  if(obj.urlShortener !== undefined && obj.urlShortener != null) {
    o.setUrlShortener(obj.urlShortener);
  }
  if(obj.category !== undefined && obj.category != null) {
    o.setCategory(obj.category);
  }
  if(obj.reputation !== undefined && obj.reputation != null) {
    o.setReputation(obj.reputation);
  }
  if(obj.authority !== undefined && obj.authority != null) {
    o.setAuthority(obj.authority);
  }
  if(obj.note !== undefined && obj.note != null) {
    o.setNote(obj.note);
  }
  if(obj.verifiedTime !== undefined && obj.verifiedTime != null) {
    o.setVerifiedTime(obj.verifiedTime);
  }
  if(obj.createdTime !== undefined && obj.createdTime != null) {
    o.setCreatedTime(obj.createdTime);
  }
  if(obj.modifiedTime !== undefined && obj.modifiedTime != null) {
    o.setModifiedTime(obj.modifiedTime);
  }
    
  return o;
};

cannyurl.wa.bean.DomainInfoJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = cannyurl.wa.bean.DomainInfoJsBean.create(jsonObj);
  return obj;
};
