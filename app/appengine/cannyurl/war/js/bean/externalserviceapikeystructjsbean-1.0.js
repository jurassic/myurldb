//////////////////////////////////////////////////////////
// <script src="/js/bean/externalserviceapikeystructjsbean-1.0.js"></script>
// Last modified time: 1365474464810.
//////////////////////////////////////////////////////////

var cannyurl = cannyurl || {};
cannyurl.wa = cannyurl.wa || {};
cannyurl.wa.bean = cannyurl.wa.bean || {};
cannyurl.wa.bean.ExternalServiceApiKeyStructJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var uuid;
    var service;
    var key;
    var secret;
    var note;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getUuid = function() { return uuid; };
    this.setUuid = function(value) { uuid = value; };
    this.getService = function() { return service; };
    this.setService = function(value) { service = value; };
    this.getKey = function() { return key; };
    this.setKey = function(value) { key = value; };
    this.getSecret = function() { return secret; };
    this.setSecret = function(value) { secret = value; };
    this.getNote = function() { return note; };
    this.setNote = function(value) { note = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new cannyurl.wa.bean.ExternalServiceApiKeyStructJsBean();

      if(uuid !== undefined && uuid != null) {
        o.setUuid(uuid);
      }
      if(service !== undefined && service != null) {
        o.setService(service);
      }
      if(key !== undefined && key != null) {
        o.setKey(key);
      }
      if(secret !== undefined && secret != null) {
        o.setSecret(secret);
      }
      if(note !== undefined && note != null) {
        o.setNote(note);
      }
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(uuid !== undefined && uuid != null) {
        jsonObj.uuid = uuid;
      } // Otherwise ignore...
      if(service !== undefined && service != null) {
        jsonObj.service = service;
      } // Otherwise ignore...
      if(key !== undefined && key != null) {
        jsonObj.key = key;
      } // Otherwise ignore...
      if(secret !== undefined && secret != null) {
        jsonObj.secret = secret;
      } // Otherwise ignore...
      if(note !== undefined && note != null) {
        jsonObj.note = note;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(uuid) {
        str += "\"uuid\":\"" + uuid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"uuid\":null, ";
      }
      if(service) {
        str += "\"service\":\"" + service + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"service\":null, ";
      }
      if(key) {
        str += "\"key\":\"" + key + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"key\":null, ";
      }
      if(secret) {
        str += "\"secret\":\"" + secret + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"secret\":null, ";
      }
      if(note) {
        str += "\"note\":\"" + note + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"note\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "uuid:" + uuid + ", ";
      str += "service:" + service + ", ";
      str += "key:" + key + ", ";
      str += "secret:" + secret + ", ";
      str += "note:" + note + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

cannyurl.wa.bean.ExternalServiceApiKeyStructJsBean.create = function(obj) {
  var o = new cannyurl.wa.bean.ExternalServiceApiKeyStructJsBean();

  if(obj.uuid !== undefined && obj.uuid != null) {
    o.setUuid(obj.uuid);
  }
  if(obj.service !== undefined && obj.service != null) {
    o.setService(obj.service);
  }
  if(obj.key !== undefined && obj.key != null) {
    o.setKey(obj.key);
  }
  if(obj.secret !== undefined && obj.secret != null) {
    o.setSecret(obj.secret);
  }
  if(obj.note !== undefined && obj.note != null) {
    o.setNote(obj.note);
  }
    
  return o;
};

cannyurl.wa.bean.ExternalServiceApiKeyStructJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = cannyurl.wa.bean.ExternalServiceApiKeyStructJsBean.create(jsonObj);
  return obj;
};
