//////////////////////////////////////////////////////////
// <script src="/js/bean/fullnamestructjsbean-1.0.js"></script>
// Last modified time: 1365474464853.
//////////////////////////////////////////////////////////

var cannyurl = cannyurl || {};
cannyurl.wa = cannyurl.wa || {};
cannyurl.wa.bean = cannyurl.wa.bean || {};
cannyurl.wa.bean.FullNameStructJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var uuid;
    var displayName;
    var lastName;
    var firstName;
    var middleName1;
    var middleName2;
    var middleInitial;
    var salutation;
    var suffix;
    var note;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getUuid = function() { return uuid; };
    this.setUuid = function(value) { uuid = value; };
    this.getDisplayName = function() { return displayName; };
    this.setDisplayName = function(value) { displayName = value; };
    this.getLastName = function() { return lastName; };
    this.setLastName = function(value) { lastName = value; };
    this.getFirstName = function() { return firstName; };
    this.setFirstName = function(value) { firstName = value; };
    this.getMiddleName1 = function() { return middleName1; };
    this.setMiddleName1 = function(value) { middleName1 = value; };
    this.getMiddleName2 = function() { return middleName2; };
    this.setMiddleName2 = function(value) { middleName2 = value; };
    this.getMiddleInitial = function() { return middleInitial; };
    this.setMiddleInitial = function(value) { middleInitial = value; };
    this.getSalutation = function() { return salutation; };
    this.setSalutation = function(value) { salutation = value; };
    this.getSuffix = function() { return suffix; };
    this.setSuffix = function(value) { suffix = value; };
    this.getNote = function() { return note; };
    this.setNote = function(value) { note = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new cannyurl.wa.bean.FullNameStructJsBean();

      if(uuid !== undefined && uuid != null) {
        o.setUuid(uuid);
      }
      if(displayName !== undefined && displayName != null) {
        o.setDisplayName(displayName);
      }
      if(lastName !== undefined && lastName != null) {
        o.setLastName(lastName);
      }
      if(firstName !== undefined && firstName != null) {
        o.setFirstName(firstName);
      }
      if(middleName1 !== undefined && middleName1 != null) {
        o.setMiddleName1(middleName1);
      }
      if(middleName2 !== undefined && middleName2 != null) {
        o.setMiddleName2(middleName2);
      }
      if(middleInitial !== undefined && middleInitial != null) {
        o.setMiddleInitial(middleInitial);
      }
      if(salutation !== undefined && salutation != null) {
        o.setSalutation(salutation);
      }
      if(suffix !== undefined && suffix != null) {
        o.setSuffix(suffix);
      }
      if(note !== undefined && note != null) {
        o.setNote(note);
      }
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(uuid !== undefined && uuid != null) {
        jsonObj.uuid = uuid;
      } // Otherwise ignore...
      if(displayName !== undefined && displayName != null) {
        jsonObj.displayName = displayName;
      } // Otherwise ignore...
      if(lastName !== undefined && lastName != null) {
        jsonObj.lastName = lastName;
      } // Otherwise ignore...
      if(firstName !== undefined && firstName != null) {
        jsonObj.firstName = firstName;
      } // Otherwise ignore...
      if(middleName1 !== undefined && middleName1 != null) {
        jsonObj.middleName1 = middleName1;
      } // Otherwise ignore...
      if(middleName2 !== undefined && middleName2 != null) {
        jsonObj.middleName2 = middleName2;
      } // Otherwise ignore...
      if(middleInitial !== undefined && middleInitial != null) {
        jsonObj.middleInitial = middleInitial;
      } // Otherwise ignore...
      if(salutation !== undefined && salutation != null) {
        jsonObj.salutation = salutation;
      } // Otherwise ignore...
      if(suffix !== undefined && suffix != null) {
        jsonObj.suffix = suffix;
      } // Otherwise ignore...
      if(note !== undefined && note != null) {
        jsonObj.note = note;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(uuid) {
        str += "\"uuid\":\"" + uuid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"uuid\":null, ";
      }
      if(displayName) {
        str += "\"displayName\":\"" + displayName + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"displayName\":null, ";
      }
      if(lastName) {
        str += "\"lastName\":\"" + lastName + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"lastName\":null, ";
      }
      if(firstName) {
        str += "\"firstName\":\"" + firstName + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"firstName\":null, ";
      }
      if(middleName1) {
        str += "\"middleName1\":\"" + middleName1 + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"middleName1\":null, ";
      }
      if(middleName2) {
        str += "\"middleName2\":\"" + middleName2 + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"middleName2\":null, ";
      }
      if(middleInitial) {
        str += "\"middleInitial\":\"" + middleInitial + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"middleInitial\":null, ";
      }
      if(salutation) {
        str += "\"salutation\":\"" + salutation + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"salutation\":null, ";
      }
      if(suffix) {
        str += "\"suffix\":\"" + suffix + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"suffix\":null, ";
      }
      if(note) {
        str += "\"note\":\"" + note + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"note\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "uuid:" + uuid + ", ";
      str += "displayName:" + displayName + ", ";
      str += "lastName:" + lastName + ", ";
      str += "firstName:" + firstName + ", ";
      str += "middleName1:" + middleName1 + ", ";
      str += "middleName2:" + middleName2 + ", ";
      str += "middleInitial:" + middleInitial + ", ";
      str += "salutation:" + salutation + ", ";
      str += "suffix:" + suffix + ", ";
      str += "note:" + note + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

cannyurl.wa.bean.FullNameStructJsBean.create = function(obj) {
  var o = new cannyurl.wa.bean.FullNameStructJsBean();

  if(obj.uuid !== undefined && obj.uuid != null) {
    o.setUuid(obj.uuid);
  }
  if(obj.displayName !== undefined && obj.displayName != null) {
    o.setDisplayName(obj.displayName);
  }
  if(obj.lastName !== undefined && obj.lastName != null) {
    o.setLastName(obj.lastName);
  }
  if(obj.firstName !== undefined && obj.firstName != null) {
    o.setFirstName(obj.firstName);
  }
  if(obj.middleName1 !== undefined && obj.middleName1 != null) {
    o.setMiddleName1(obj.middleName1);
  }
  if(obj.middleName2 !== undefined && obj.middleName2 != null) {
    o.setMiddleName2(obj.middleName2);
  }
  if(obj.middleInitial !== undefined && obj.middleInitial != null) {
    o.setMiddleInitial(obj.middleInitial);
  }
  if(obj.salutation !== undefined && obj.salutation != null) {
    o.setSalutation(obj.salutation);
  }
  if(obj.suffix !== undefined && obj.suffix != null) {
    o.setSuffix(obj.suffix);
  }
  if(obj.note !== undefined && obj.note != null) {
    o.setNote(obj.note);
  }
    
  return o;
};

cannyurl.wa.bean.FullNameStructJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = cannyurl.wa.bean.FullNameStructJsBean.create(jsonObj);
  return obj;
};
