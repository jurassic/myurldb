//////////////////////////////////////////////////////////
// <script src="/js/bean/geocoordinatestructjsbean-1.0.js"></script>
// Last modified time: 1365474464824.
//////////////////////////////////////////////////////////

var cannyurl = cannyurl || {};
cannyurl.wa = cannyurl.wa || {};
cannyurl.wa.bean = cannyurl.wa.bean || {};
cannyurl.wa.bean.GeoCoordinateStructJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var uuid;
    var latitude;
    var longitude;
    var altitude;
    var sensorUsed;
    var accuracy;
    var altitudeAccuracy;
    var heading;
    var speed;
    var note;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getUuid = function() { return uuid; };
    this.setUuid = function(value) { uuid = value; };
    this.getLatitude = function() { return latitude; };
    this.setLatitude = function(value) { latitude = value; };
    this.getLongitude = function() { return longitude; };
    this.setLongitude = function(value) { longitude = value; };
    this.getAltitude = function() { return altitude; };
    this.setAltitude = function(value) { altitude = value; };
    this.getSensorUsed = function() { return sensorUsed; };
    this.setSensorUsed = function(value) { sensorUsed = value; };
    this.getAccuracy = function() { return accuracy; };
    this.setAccuracy = function(value) { accuracy = value; };
    this.getAltitudeAccuracy = function() { return altitudeAccuracy; };
    this.setAltitudeAccuracy = function(value) { altitudeAccuracy = value; };
    this.getHeading = function() { return heading; };
    this.setHeading = function(value) { heading = value; };
    this.getSpeed = function() { return speed; };
    this.setSpeed = function(value) { speed = value; };
    this.getNote = function() { return note; };
    this.setNote = function(value) { note = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new cannyurl.wa.bean.GeoCoordinateStructJsBean();

      if(uuid !== undefined && uuid != null) {
        o.setUuid(uuid);
      }
      if(latitude !== undefined && latitude != null) {
        o.setLatitude(latitude);
      }
      if(longitude !== undefined && longitude != null) {
        o.setLongitude(longitude);
      }
      if(altitude !== undefined && altitude != null) {
        o.setAltitude(altitude);
      }
      if(sensorUsed !== undefined && sensorUsed != null) {
        o.setSensorUsed(sensorUsed);
      }
      if(accuracy !== undefined && accuracy != null) {
        o.setAccuracy(accuracy);
      }
      if(altitudeAccuracy !== undefined && altitudeAccuracy != null) {
        o.setAltitudeAccuracy(altitudeAccuracy);
      }
      if(heading !== undefined && heading != null) {
        o.setHeading(heading);
      }
      if(speed !== undefined && speed != null) {
        o.setSpeed(speed);
      }
      if(note !== undefined && note != null) {
        o.setNote(note);
      }
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(uuid !== undefined && uuid != null) {
        jsonObj.uuid = uuid;
      } // Otherwise ignore...
      if(latitude !== undefined && latitude != null) {
        jsonObj.latitude = latitude;
      } // Otherwise ignore...
      if(longitude !== undefined && longitude != null) {
        jsonObj.longitude = longitude;
      } // Otherwise ignore...
      if(altitude !== undefined && altitude != null) {
        jsonObj.altitude = altitude;
      } // Otherwise ignore...
      if(sensorUsed !== undefined && sensorUsed != null) {
        jsonObj.sensorUsed = sensorUsed;
      } // Otherwise ignore...
      if(accuracy !== undefined && accuracy != null) {
        jsonObj.accuracy = accuracy;
      } // Otherwise ignore...
      if(altitudeAccuracy !== undefined && altitudeAccuracy != null) {
        jsonObj.altitudeAccuracy = altitudeAccuracy;
      } // Otherwise ignore...
      if(heading !== undefined && heading != null) {
        jsonObj.heading = heading;
      } // Otherwise ignore...
      if(speed !== undefined && speed != null) {
        jsonObj.speed = speed;
      } // Otherwise ignore...
      if(note !== undefined && note != null) {
        jsonObj.note = note;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(uuid) {
        str += "\"uuid\":\"" + uuid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"uuid\":null, ";
      }
      if(latitude) {
        str += "\"latitude\":" + latitude + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"latitude\":null, ";
      }
      if(longitude) {
        str += "\"longitude\":" + longitude + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"longitude\":null, ";
      }
      if(altitude) {
        str += "\"altitude\":" + altitude + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"altitude\":null, ";
      }
      if(sensorUsed) {
        str += "\"sensorUsed\":" + sensorUsed + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"sensorUsed\":null, ";
      }
      if(accuracy) {
        str += "\"accuracy\":\"" + accuracy + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"accuracy\":null, ";
      }
      if(altitudeAccuracy) {
        str += "\"altitudeAccuracy\":\"" + altitudeAccuracy + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"altitudeAccuracy\":null, ";
      }
      if(heading) {
        str += "\"heading\":" + heading + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"heading\":null, ";
      }
      if(speed) {
        str += "\"speed\":" + speed + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"speed\":null, ";
      }
      if(note) {
        str += "\"note\":\"" + note + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"note\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "uuid:" + uuid + ", ";
      str += "latitude:" + latitude + ", ";
      str += "longitude:" + longitude + ", ";
      str += "altitude:" + altitude + ", ";
      str += "sensorUsed:" + sensorUsed + ", ";
      str += "accuracy:" + accuracy + ", ";
      str += "altitudeAccuracy:" + altitudeAccuracy + ", ";
      str += "heading:" + heading + ", ";
      str += "speed:" + speed + ", ";
      str += "note:" + note + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

cannyurl.wa.bean.GeoCoordinateStructJsBean.create = function(obj) {
  var o = new cannyurl.wa.bean.GeoCoordinateStructJsBean();

  if(obj.uuid !== undefined && obj.uuid != null) {
    o.setUuid(obj.uuid);
  }
  if(obj.latitude !== undefined && obj.latitude != null) {
    o.setLatitude(obj.latitude);
  }
  if(obj.longitude !== undefined && obj.longitude != null) {
    o.setLongitude(obj.longitude);
  }
  if(obj.altitude !== undefined && obj.altitude != null) {
    o.setAltitude(obj.altitude);
  }
  if(obj.sensorUsed !== undefined && obj.sensorUsed != null) {
    o.setSensorUsed(obj.sensorUsed);
  }
  if(obj.accuracy !== undefined && obj.accuracy != null) {
    o.setAccuracy(obj.accuracy);
  }
  if(obj.altitudeAccuracy !== undefined && obj.altitudeAccuracy != null) {
    o.setAltitudeAccuracy(obj.altitudeAccuracy);
  }
  if(obj.heading !== undefined && obj.heading != null) {
    o.setHeading(obj.heading);
  }
  if(obj.speed !== undefined && obj.speed != null) {
    o.setSpeed(obj.speed);
  }
  if(obj.note !== undefined && obj.note != null) {
    o.setNote(obj.note);
  }
    
  return o;
};

cannyurl.wa.bean.GeoCoordinateStructJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = cannyurl.wa.bean.GeoCoordinateStructJsBean.create(jsonObj);
  return obj;
};
