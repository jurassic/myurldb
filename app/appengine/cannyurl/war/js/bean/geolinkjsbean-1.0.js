//////////////////////////////////////////////////////////
// <script src="/js/bean/geolinkjsbean-1.0.js"></script>
// Last modified time: 1365474465379.
//////////////////////////////////////////////////////////

var cannyurl = cannyurl || {};
cannyurl.wa = cannyurl.wa || {};
cannyurl.wa.bean = cannyurl.wa.bean || {};
cannyurl.wa.bean.GeoLinkJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var guid = generateUuid();
    var shortLink;
    var shortUrl;
    var geoCoordinate;
    var geoCell;
    var status;
    var createdTime = getCurrentTime();
    var modifiedTime;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getGuid = function() { return guid; };
    this.setGuid = function(value) { guid = value; };
    this.getShortLink = function() { return shortLink; };
    this.setShortLink = function(value) { shortLink = value; };
    this.getShortUrl = function() { return shortUrl; };
    this.setShortUrl = function(value) { shortUrl = value; };
    this.getGeoCoordinate = function() { return geoCoordinate; };
    this.setGeoCoordinate = function(value) { geoCoordinate = value; };
    this.getGeoCell = function() { return geoCell; };
    this.setGeoCell = function(value) { geoCell = value; };
    this.getStatus = function() { return status; };
    this.setStatus = function(value) { status = value; };
    this.getCreatedTime = function() { return createdTime; };
    this.setCreatedTime = function(value) { createdTime = value; };
    this.getModifiedTime = function() { return modifiedTime; };
    this.setModifiedTime = function(value) { modifiedTime = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new cannyurl.wa.bean.GeoLinkJsBean();

      o.setGuid(generateUuid());
      if(shortLink !== undefined && shortLink != null) {
        o.setShortLink(shortLink);
      }
      if(shortUrl !== undefined && shortUrl != null) {
        o.setShortUrl(shortUrl);
      }
      //o.setGeoCoordinate(geoCoordinate.clone());
      if(geoCoordinate !== undefined && geoCoordinate != null) {
        o.setGeoCoordinate(geoCoordinate);
      }
      //o.setGeoCell(geoCell.clone());
      if(geoCell !== undefined && geoCell != null) {
        o.setGeoCell(geoCell);
      }
      if(status !== undefined && status != null) {
        o.setStatus(status);
      }
      o.setCreatedTime(getCurrentTime());
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(guid !== undefined && guid != null) {
        jsonObj.guid = guid;
      } // Otherwise ignore...
      if(shortLink !== undefined && shortLink != null) {
        jsonObj.shortLink = shortLink;
      } // Otherwise ignore...
      if(shortUrl !== undefined && shortUrl != null) {
        jsonObj.shortUrl = shortUrl;
      } // Otherwise ignore...
      if(geoCoordinate !== undefined && geoCoordinate != null) {
        jsonObj.geoCoordinate = geoCoordinate;
      } // Otherwise ignore...
      if(geoCell !== undefined && geoCell != null) {
        jsonObj.geoCell = geoCell;
      } // Otherwise ignore...
      if(status !== undefined && status != null) {
        jsonObj.status = status;
      } // Otherwise ignore...
      if(createdTime !== undefined && createdTime != null) {
        jsonObj.createdTime = createdTime;
      } // Otherwise ignore...
      if(modifiedTime !== undefined && modifiedTime != null) {
        jsonObj.modifiedTime = modifiedTime;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(guid) {
        str += "\"guid\":\"" + guid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"guid\":null, ";
      }
      if(shortLink) {
        str += "\"shortLink\":\"" + shortLink + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"shortLink\":null, ";
      }
      if(shortUrl) {
        str += "\"shortUrl\":\"" + shortUrl + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"shortUrl\":null, ";
      }
      str += "\"geoCoordinate\":" + geoCoordinate.toJsonString() + ", ";
      str += "\"geoCell\":" + geoCell.toJsonString() + ", ";
      if(status) {
        str += "\"status\":\"" + status + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"status\":null, ";
      }
      if(createdTime) {
        str += "\"createdTime\":" + createdTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"createdTime\":null, ";
      }
      if(modifiedTime) {
        str += "\"modifiedTime\":" + modifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"modifiedTime\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "guid:" + guid + ", ";
      str += "shortLink:" + shortLink + ", ";
      str += "shortUrl:" + shortUrl + ", ";
      str += "geoCoordinate:" + geoCoordinate + ", ";
      str += "geoCell:" + geoCell + ", ";
      str += "status:" + status + ", ";
      str += "createdTime:" + createdTime + ", ";
      str += "modifiedTime:" + modifiedTime + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

cannyurl.wa.bean.GeoLinkJsBean.create = function(obj) {
  var o = new cannyurl.wa.bean.GeoLinkJsBean();

  if(obj.guid !== undefined && obj.guid != null) {
    o.setGuid(obj.guid);
  }
  if(obj.shortLink !== undefined && obj.shortLink != null) {
    o.setShortLink(obj.shortLink);
  }
  if(obj.shortUrl !== undefined && obj.shortUrl != null) {
    o.setShortUrl(obj.shortUrl);
  }
  if(obj.geoCoordinate !== undefined && obj.geoCoordinate != null) {
    o.setGeoCoordinate(obj.geoCoordinate);
  }
  if(obj.geoCell !== undefined && obj.geoCell != null) {
    o.setGeoCell(obj.geoCell);
  }
  if(obj.status !== undefined && obj.status != null) {
    o.setStatus(obj.status);
  }
  if(obj.createdTime !== undefined && obj.createdTime != null) {
    o.setCreatedTime(obj.createdTime);
  }
  if(obj.modifiedTime !== undefined && obj.modifiedTime != null) {
    o.setModifiedTime(obj.modifiedTime);
  }
    
  return o;
};

cannyurl.wa.bean.GeoLinkJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = cannyurl.wa.bean.GeoLinkJsBean.create(jsonObj);
  return obj;
};
