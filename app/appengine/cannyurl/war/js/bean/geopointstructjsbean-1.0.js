//////////////////////////////////////////////////////////
// <script src="/js/bean/geopointstructjsbean-1.0.js"></script>
// Last modified time: 1365474464817.
//////////////////////////////////////////////////////////

var cannyurl = cannyurl || {};
cannyurl.wa = cannyurl.wa || {};
cannyurl.wa.bean = cannyurl.wa.bean || {};
cannyurl.wa.bean.GeoPointStructJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var uuid;
    var latitude;
    var longitude;
    var altitude;
    var sensorUsed;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getUuid = function() { return uuid; };
    this.setUuid = function(value) { uuid = value; };
    this.getLatitude = function() { return latitude; };
    this.setLatitude = function(value) { latitude = value; };
    this.getLongitude = function() { return longitude; };
    this.setLongitude = function(value) { longitude = value; };
    this.getAltitude = function() { return altitude; };
    this.setAltitude = function(value) { altitude = value; };
    this.getSensorUsed = function() { return sensorUsed; };
    this.setSensorUsed = function(value) { sensorUsed = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new cannyurl.wa.bean.GeoPointStructJsBean();

      if(uuid !== undefined && uuid != null) {
        o.setUuid(uuid);
      }
      if(latitude !== undefined && latitude != null) {
        o.setLatitude(latitude);
      }
      if(longitude !== undefined && longitude != null) {
        o.setLongitude(longitude);
      }
      if(altitude !== undefined && altitude != null) {
        o.setAltitude(altitude);
      }
      if(sensorUsed !== undefined && sensorUsed != null) {
        o.setSensorUsed(sensorUsed);
      }
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(uuid !== undefined && uuid != null) {
        jsonObj.uuid = uuid;
      } // Otherwise ignore...
      if(latitude !== undefined && latitude != null) {
        jsonObj.latitude = latitude;
      } // Otherwise ignore...
      if(longitude !== undefined && longitude != null) {
        jsonObj.longitude = longitude;
      } // Otherwise ignore...
      if(altitude !== undefined && altitude != null) {
        jsonObj.altitude = altitude;
      } // Otherwise ignore...
      if(sensorUsed !== undefined && sensorUsed != null) {
        jsonObj.sensorUsed = sensorUsed;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(uuid) {
        str += "\"uuid\":\"" + uuid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"uuid\":null, ";
      }
      if(latitude) {
        str += "\"latitude\":" + latitude + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"latitude\":null, ";
      }
      if(longitude) {
        str += "\"longitude\":" + longitude + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"longitude\":null, ";
      }
      if(altitude) {
        str += "\"altitude\":" + altitude + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"altitude\":null, ";
      }
      if(sensorUsed) {
        str += "\"sensorUsed\":" + sensorUsed + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"sensorUsed\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "uuid:" + uuid + ", ";
      str += "latitude:" + latitude + ", ";
      str += "longitude:" + longitude + ", ";
      str += "altitude:" + altitude + ", ";
      str += "sensorUsed:" + sensorUsed + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

cannyurl.wa.bean.GeoPointStructJsBean.create = function(obj) {
  var o = new cannyurl.wa.bean.GeoPointStructJsBean();

  if(obj.uuid !== undefined && obj.uuid != null) {
    o.setUuid(obj.uuid);
  }
  if(obj.latitude !== undefined && obj.latitude != null) {
    o.setLatitude(obj.latitude);
  }
  if(obj.longitude !== undefined && obj.longitude != null) {
    o.setLongitude(obj.longitude);
  }
  if(obj.altitude !== undefined && obj.altitude != null) {
    o.setAltitude(obj.altitude);
  }
  if(obj.sensorUsed !== undefined && obj.sensorUsed != null) {
    o.setSensorUsed(obj.sensorUsed);
  }
    
  return o;
};

cannyurl.wa.bean.GeoPointStructJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = cannyurl.wa.bean.GeoPointStructJsBean.create(jsonObj);
  return obj;
};
