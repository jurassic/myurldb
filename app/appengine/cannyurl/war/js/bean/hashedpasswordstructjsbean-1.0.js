//////////////////////////////////////////////////////////
// <script src="/js/bean/hashedpasswordstructjsbean-1.0.js"></script>
// Last modified time: 1365474464993.
//////////////////////////////////////////////////////////

var cannyurl = cannyurl || {};
cannyurl.wa = cannyurl.wa || {};
cannyurl.wa.bean = cannyurl.wa.bean || {};
cannyurl.wa.bean.HashedPasswordStructJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var uuid;
    var plainText;
    var hashedText;
    var salt;
    var algorithm;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getUuid = function() { return uuid; };
    this.setUuid = function(value) { uuid = value; };
    this.getPlainText = function() { return plainText; };
    this.setPlainText = function(value) { plainText = value; };
    this.getHashedText = function() { return hashedText; };
    this.setHashedText = function(value) { hashedText = value; };
    this.getSalt = function() { return salt; };
    this.setSalt = function(value) { salt = value; };
    this.getAlgorithm = function() { return algorithm; };
    this.setAlgorithm = function(value) { algorithm = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new cannyurl.wa.bean.HashedPasswordStructJsBean();

      if(uuid !== undefined && uuid != null) {
        o.setUuid(uuid);
      }
      if(plainText !== undefined && plainText != null) {
        o.setPlainText(plainText);
      }
      if(hashedText !== undefined && hashedText != null) {
        o.setHashedText(hashedText);
      }
      if(salt !== undefined && salt != null) {
        o.setSalt(salt);
      }
      if(algorithm !== undefined && algorithm != null) {
        o.setAlgorithm(algorithm);
      }
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(uuid !== undefined && uuid != null) {
        jsonObj.uuid = uuid;
      } // Otherwise ignore...
      if(plainText !== undefined && plainText != null) {
        jsonObj.plainText = plainText;
      } // Otherwise ignore...
      if(hashedText !== undefined && hashedText != null) {
        jsonObj.hashedText = hashedText;
      } // Otherwise ignore...
      if(salt !== undefined && salt != null) {
        jsonObj.salt = salt;
      } // Otherwise ignore...
      if(algorithm !== undefined && algorithm != null) {
        jsonObj.algorithm = algorithm;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(uuid) {
        str += "\"uuid\":\"" + uuid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"uuid\":null, ";
      }
      if(plainText) {
        str += "\"plainText\":\"" + plainText + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"plainText\":null, ";
      }
      if(hashedText) {
        str += "\"hashedText\":\"" + hashedText + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"hashedText\":null, ";
      }
      if(salt) {
        str += "\"salt\":\"" + salt + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"salt\":null, ";
      }
      if(algorithm) {
        str += "\"algorithm\":\"" + algorithm + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"algorithm\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "uuid:" + uuid + ", ";
      str += "plainText:" + plainText + ", ";
      str += "hashedText:" + hashedText + ", ";
      str += "salt:" + salt + ", ";
      str += "algorithm:" + algorithm + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

cannyurl.wa.bean.HashedPasswordStructJsBean.create = function(obj) {
  var o = new cannyurl.wa.bean.HashedPasswordStructJsBean();

  if(obj.uuid !== undefined && obj.uuid != null) {
    o.setUuid(obj.uuid);
  }
  if(obj.plainText !== undefined && obj.plainText != null) {
    o.setPlainText(obj.plainText);
  }
  if(obj.hashedText !== undefined && obj.hashedText != null) {
    o.setHashedText(obj.hashedText);
  }
  if(obj.salt !== undefined && obj.salt != null) {
    o.setSalt(obj.salt);
  }
  if(obj.algorithm !== undefined && obj.algorithm != null) {
    o.setAlgorithm(obj.algorithm);
  }
    
  return o;
};

cannyurl.wa.bean.HashedPasswordStructJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = cannyurl.wa.bean.HashedPasswordStructJsBean.create(jsonObj);
  return obj;
};
