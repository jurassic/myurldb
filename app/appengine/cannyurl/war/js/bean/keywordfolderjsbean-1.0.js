//////////////////////////////////////////////////////////
// <script src="/js/bean/keywordfolderjsbean-1.0.js"></script>
// Last modified time: 1365474465439.
//////////////////////////////////////////////////////////

var cannyurl = cannyurl || {};
cannyurl.wa = cannyurl.wa || {};
cannyurl.wa.bean = cannyurl.wa.bean || {};
cannyurl.wa.bean.KeywordFolderJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var guid = generateUuid();
    var appClient;
    var clientRootDomain;
    var user;
    var title;
    var description;
    var type;
    var category;
    var parent;
    var aggregate;
    var acl;
    var exportable;
    var status;
    var note;
    var folderPath;
    var createdTime = getCurrentTime();
    var modifiedTime;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getGuid = function() { return guid; };
    this.setGuid = function(value) { guid = value; };
    this.getAppClient = function() { return appClient; };
    this.setAppClient = function(value) { appClient = value; };
    this.getClientRootDomain = function() { return clientRootDomain; };
    this.setClientRootDomain = function(value) { clientRootDomain = value; };
    this.getUser = function() { return user; };
    this.setUser = function(value) { user = value; };
    this.getTitle = function() { return title; };
    this.setTitle = function(value) { title = value; };
    this.getDescription = function() { return description; };
    this.setDescription = function(value) { description = value; };
    this.getType = function() { return type; };
    this.setType = function(value) { type = value; };
    this.getCategory = function() { return category; };
    this.setCategory = function(value) { category = value; };
    this.getParent = function() { return parent; };
    this.setParent = function(value) { parent = value; };
    this.getAggregate = function() { return aggregate; };
    this.setAggregate = function(value) { aggregate = value; };
    this.getAcl = function() { return acl; };
    this.setAcl = function(value) { acl = value; };
    this.getExportable = function() { return exportable; };
    this.setExportable = function(value) { exportable = value; };
    this.getStatus = function() { return status; };
    this.setStatus = function(value) { status = value; };
    this.getNote = function() { return note; };
    this.setNote = function(value) { note = value; };
    this.getFolderPath = function() { return folderPath; };
    this.setFolderPath = function(value) { folderPath = value; };
    this.getCreatedTime = function() { return createdTime; };
    this.setCreatedTime = function(value) { createdTime = value; };
    this.getModifiedTime = function() { return modifiedTime; };
    this.setModifiedTime = function(value) { modifiedTime = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new cannyurl.wa.bean.KeywordFolderJsBean();

      o.setGuid(generateUuid());
      if(appClient !== undefined && appClient != null) {
        o.setAppClient(appClient);
      }
      if(clientRootDomain !== undefined && clientRootDomain != null) {
        o.setClientRootDomain(clientRootDomain);
      }
      if(user !== undefined && user != null) {
        o.setUser(user);
      }
      if(title !== undefined && title != null) {
        o.setTitle(title);
      }
      if(description !== undefined && description != null) {
        o.setDescription(description);
      }
      if(type !== undefined && type != null) {
        o.setType(type);
      }
      if(category !== undefined && category != null) {
        o.setCategory(category);
      }
      if(parent !== undefined && parent != null) {
        o.setParent(parent);
      }
      if(aggregate !== undefined && aggregate != null) {
        o.setAggregate(aggregate);
      }
      if(acl !== undefined && acl != null) {
        o.setAcl(acl);
      }
      if(exportable !== undefined && exportable != null) {
        o.setExportable(exportable);
      }
      if(status !== undefined && status != null) {
        o.setStatus(status);
      }
      if(note !== undefined && note != null) {
        o.setNote(note);
      }
      if(folderPath !== undefined && folderPath != null) {
        o.setFolderPath(folderPath);
      }
      o.setCreatedTime(getCurrentTime());
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(guid !== undefined && guid != null) {
        jsonObj.guid = guid;
      } // Otherwise ignore...
      if(appClient !== undefined && appClient != null) {
        jsonObj.appClient = appClient;
      } // Otherwise ignore...
      if(clientRootDomain !== undefined && clientRootDomain != null) {
        jsonObj.clientRootDomain = clientRootDomain;
      } // Otherwise ignore...
      if(user !== undefined && user != null) {
        jsonObj.user = user;
      } // Otherwise ignore...
      if(title !== undefined && title != null) {
        jsonObj.title = title;
      } // Otherwise ignore...
      if(description !== undefined && description != null) {
        jsonObj.description = description;
      } // Otherwise ignore...
      if(type !== undefined && type != null) {
        jsonObj.type = type;
      } // Otherwise ignore...
      if(category !== undefined && category != null) {
        jsonObj.category = category;
      } // Otherwise ignore...
      if(parent !== undefined && parent != null) {
        jsonObj.parent = parent;
      } // Otherwise ignore...
      if(aggregate !== undefined && aggregate != null) {
        jsonObj.aggregate = aggregate;
      } // Otherwise ignore...
      if(acl !== undefined && acl != null) {
        jsonObj.acl = acl;
      } // Otherwise ignore...
      if(exportable !== undefined && exportable != null) {
        jsonObj.exportable = exportable;
      } // Otherwise ignore...
      if(status !== undefined && status != null) {
        jsonObj.status = status;
      } // Otherwise ignore...
      if(note !== undefined && note != null) {
        jsonObj.note = note;
      } // Otherwise ignore...
      if(folderPath !== undefined && folderPath != null) {
        jsonObj.folderPath = folderPath;
      } // Otherwise ignore...
      if(createdTime !== undefined && createdTime != null) {
        jsonObj.createdTime = createdTime;
      } // Otherwise ignore...
      if(modifiedTime !== undefined && modifiedTime != null) {
        jsonObj.modifiedTime = modifiedTime;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(guid) {
        str += "\"guid\":\"" + guid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"guid\":null, ";
      }
      if(appClient) {
        str += "\"appClient\":\"" + appClient + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"appClient\":null, ";
      }
      if(clientRootDomain) {
        str += "\"clientRootDomain\":\"" + clientRootDomain + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"clientRootDomain\":null, ";
      }
      if(user) {
        str += "\"user\":\"" + user + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"user\":null, ";
      }
      if(title) {
        str += "\"title\":\"" + title + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"title\":null, ";
      }
      if(description) {
        str += "\"description\":\"" + description + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"description\":null, ";
      }
      if(type) {
        str += "\"type\":\"" + type + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"type\":null, ";
      }
      if(category) {
        str += "\"category\":\"" + category + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"category\":null, ";
      }
      if(parent) {
        str += "\"parent\":\"" + parent + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"parent\":null, ";
      }
      if(aggregate) {
        str += "\"aggregate\":\"" + aggregate + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"aggregate\":null, ";
      }
      if(acl) {
        str += "\"acl\":\"" + acl + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"acl\":null, ";
      }
      if(exportable) {
        str += "\"exportable\":" + exportable + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"exportable\":null, ";
      }
      if(status) {
        str += "\"status\":\"" + status + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"status\":null, ";
      }
      if(note) {
        str += "\"note\":\"" + note + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"note\":null, ";
      }
      if(folderPath) {
        str += "\"folderPath\":\"" + folderPath + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"folderPath\":null, ";
      }
      if(createdTime) {
        str += "\"createdTime\":" + createdTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"createdTime\":null, ";
      }
      if(modifiedTime) {
        str += "\"modifiedTime\":" + modifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"modifiedTime\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "guid:" + guid + ", ";
      str += "appClient:" + appClient + ", ";
      str += "clientRootDomain:" + clientRootDomain + ", ";
      str += "user:" + user + ", ";
      str += "title:" + title + ", ";
      str += "description:" + description + ", ";
      str += "type:" + type + ", ";
      str += "category:" + category + ", ";
      str += "parent:" + parent + ", ";
      str += "aggregate:" + aggregate + ", ";
      str += "acl:" + acl + ", ";
      str += "exportable:" + exportable + ", ";
      str += "status:" + status + ", ";
      str += "note:" + note + ", ";
      str += "folderPath:" + folderPath + ", ";
      str += "createdTime:" + createdTime + ", ";
      str += "modifiedTime:" + modifiedTime + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

cannyurl.wa.bean.KeywordFolderJsBean.create = function(obj) {
  var o = new cannyurl.wa.bean.KeywordFolderJsBean();

  if(obj.guid !== undefined && obj.guid != null) {
    o.setGuid(obj.guid);
  }
  if(obj.appClient !== undefined && obj.appClient != null) {
    o.setAppClient(obj.appClient);
  }
  if(obj.clientRootDomain !== undefined && obj.clientRootDomain != null) {
    o.setClientRootDomain(obj.clientRootDomain);
  }
  if(obj.user !== undefined && obj.user != null) {
    o.setUser(obj.user);
  }
  if(obj.title !== undefined && obj.title != null) {
    o.setTitle(obj.title);
  }
  if(obj.description !== undefined && obj.description != null) {
    o.setDescription(obj.description);
  }
  if(obj.type !== undefined && obj.type != null) {
    o.setType(obj.type);
  }
  if(obj.category !== undefined && obj.category != null) {
    o.setCategory(obj.category);
  }
  if(obj.parent !== undefined && obj.parent != null) {
    o.setParent(obj.parent);
  }
  if(obj.aggregate !== undefined && obj.aggregate != null) {
    o.setAggregate(obj.aggregate);
  }
  if(obj.acl !== undefined && obj.acl != null) {
    o.setAcl(obj.acl);
  }
  if(obj.exportable !== undefined && obj.exportable != null) {
    o.setExportable(obj.exportable);
  }
  if(obj.status !== undefined && obj.status != null) {
    o.setStatus(obj.status);
  }
  if(obj.note !== undefined && obj.note != null) {
    o.setNote(obj.note);
  }
  if(obj.folderPath !== undefined && obj.folderPath != null) {
    o.setFolderPath(obj.folderPath);
  }
  if(obj.createdTime !== undefined && obj.createdTime != null) {
    o.setCreatedTime(obj.createdTime);
  }
  if(obj.modifiedTime !== undefined && obj.modifiedTime != null) {
    o.setModifiedTime(obj.modifiedTime);
  }
    
  return o;
};

cannyurl.wa.bean.KeywordFolderJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = cannyurl.wa.bean.KeywordFolderJsBean.create(jsonObj);
  return obj;
};
