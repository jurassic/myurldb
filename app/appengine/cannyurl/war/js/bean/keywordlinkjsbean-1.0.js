//////////////////////////////////////////////////////////
// <script src="/js/bean/keywordlinkjsbean-1.0.js"></script>
// Last modified time: 1365474465560.
//////////////////////////////////////////////////////////

var cannyurl = cannyurl || {};
cannyurl.wa = cannyurl.wa || {};
cannyurl.wa.bean = cannyurl.wa.bean || {};
cannyurl.wa.bean.KeywordLinkJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var guid = generateUuid();
    var appClient;
    var clientRootDomain;
    var user;
    var shortLink;
    var domain;
    var token;
    var longUrl;
    var shortUrl;
    var internal;
    var caching;
    var memo;
    var status;
    var note;
    var expirationTime;
    var keywordFolder;
    var folderPath;
    var keyword;
    var queryKey;
    var scope;
    var dynamic;
    var caseSensitive;
    var createdTime = getCurrentTime();
    var modifiedTime;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getGuid = function() { return guid; };
    this.setGuid = function(value) { guid = value; };
    this.getAppClient = function() { return appClient; };
    this.setAppClient = function(value) { appClient = value; };
    this.getClientRootDomain = function() { return clientRootDomain; };
    this.setClientRootDomain = function(value) { clientRootDomain = value; };
    this.getUser = function() { return user; };
    this.setUser = function(value) { user = value; };
    this.getShortLink = function() { return shortLink; };
    this.setShortLink = function(value) { shortLink = value; };
    this.getDomain = function() { return domain; };
    this.setDomain = function(value) { domain = value; };
    this.getToken = function() { return token; };
    this.setToken = function(value) { token = value; };
    this.getLongUrl = function() { return longUrl; };
    this.setLongUrl = function(value) { longUrl = value; };
    this.getShortUrl = function() { return shortUrl; };
    this.setShortUrl = function(value) { shortUrl = value; };
    this.getInternal = function() { return internal; };
    this.setInternal = function(value) { internal = value; };
    this.getCaching = function() { return caching; };
    this.setCaching = function(value) { caching = value; };
    this.getMemo = function() { return memo; };
    this.setMemo = function(value) { memo = value; };
    this.getStatus = function() { return status; };
    this.setStatus = function(value) { status = value; };
    this.getNote = function() { return note; };
    this.setNote = function(value) { note = value; };
    this.getExpirationTime = function() { return expirationTime; };
    this.setExpirationTime = function(value) { expirationTime = value; };
    this.getKeywordFolder = function() { return keywordFolder; };
    this.setKeywordFolder = function(value) { keywordFolder = value; };
    this.getFolderPath = function() { return folderPath; };
    this.setFolderPath = function(value) { folderPath = value; };
    this.getKeyword = function() { return keyword; };
    this.setKeyword = function(value) { keyword = value; };
    this.getQueryKey = function() { return queryKey; };
    this.setQueryKey = function(value) { queryKey = value; };
    this.getScope = function() { return scope; };
    this.setScope = function(value) { scope = value; };
    this.getDynamic = function() { return dynamic; };
    this.setDynamic = function(value) { dynamic = value; };
    this.getCaseSensitive = function() { return caseSensitive; };
    this.setCaseSensitive = function(value) { caseSensitive = value; };
    this.getCreatedTime = function() { return createdTime; };
    this.setCreatedTime = function(value) { createdTime = value; };
    this.getModifiedTime = function() { return modifiedTime; };
    this.setModifiedTime = function(value) { modifiedTime = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new cannyurl.wa.bean.KeywordLinkJsBean();

      o.setGuid(generateUuid());
      if(appClient !== undefined && appClient != null) {
        o.setAppClient(appClient);
      }
      if(clientRootDomain !== undefined && clientRootDomain != null) {
        o.setClientRootDomain(clientRootDomain);
      }
      if(user !== undefined && user != null) {
        o.setUser(user);
      }
      if(shortLink !== undefined && shortLink != null) {
        o.setShortLink(shortLink);
      }
      if(domain !== undefined && domain != null) {
        o.setDomain(domain);
      }
      if(token !== undefined && token != null) {
        o.setToken(token);
      }
      if(longUrl !== undefined && longUrl != null) {
        o.setLongUrl(longUrl);
      }
      if(shortUrl !== undefined && shortUrl != null) {
        o.setShortUrl(shortUrl);
      }
      if(internal !== undefined && internal != null) {
        o.setInternal(internal);
      }
      if(caching !== undefined && caching != null) {
        o.setCaching(caching);
      }
      if(memo !== undefined && memo != null) {
        o.setMemo(memo);
      }
      if(status !== undefined && status != null) {
        o.setStatus(status);
      }
      if(note !== undefined && note != null) {
        o.setNote(note);
      }
      if(expirationTime !== undefined && expirationTime != null) {
        o.setExpirationTime(expirationTime);
      }
      if(keywordFolder !== undefined && keywordFolder != null) {
        o.setKeywordFolder(keywordFolder);
      }
      if(folderPath !== undefined && folderPath != null) {
        o.setFolderPath(folderPath);
      }
      if(keyword !== undefined && keyword != null) {
        o.setKeyword(keyword);
      }
      if(queryKey !== undefined && queryKey != null) {
        o.setQueryKey(queryKey);
      }
      if(scope !== undefined && scope != null) {
        o.setScope(scope);
      }
      if(dynamic !== undefined && dynamic != null) {
        o.setDynamic(dynamic);
      }
      if(caseSensitive !== undefined && caseSensitive != null) {
        o.setCaseSensitive(caseSensitive);
      }
      o.setCreatedTime(getCurrentTime());
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(guid !== undefined && guid != null) {
        jsonObj.guid = guid;
      } // Otherwise ignore...
      if(appClient !== undefined && appClient != null) {
        jsonObj.appClient = appClient;
      } // Otherwise ignore...
      if(clientRootDomain !== undefined && clientRootDomain != null) {
        jsonObj.clientRootDomain = clientRootDomain;
      } // Otherwise ignore...
      if(user !== undefined && user != null) {
        jsonObj.user = user;
      } // Otherwise ignore...
      if(shortLink !== undefined && shortLink != null) {
        jsonObj.shortLink = shortLink;
      } // Otherwise ignore...
      if(domain !== undefined && domain != null) {
        jsonObj.domain = domain;
      } // Otherwise ignore...
      if(token !== undefined && token != null) {
        jsonObj.token = token;
      } // Otherwise ignore...
      if(longUrl !== undefined && longUrl != null) {
        jsonObj.longUrl = longUrl;
      } // Otherwise ignore...
      if(shortUrl !== undefined && shortUrl != null) {
        jsonObj.shortUrl = shortUrl;
      } // Otherwise ignore...
      if(internal !== undefined && internal != null) {
        jsonObj.internal = internal;
      } // Otherwise ignore...
      if(caching !== undefined && caching != null) {
        jsonObj.caching = caching;
      } // Otherwise ignore...
      if(memo !== undefined && memo != null) {
        jsonObj.memo = memo;
      } // Otherwise ignore...
      if(status !== undefined && status != null) {
        jsonObj.status = status;
      } // Otherwise ignore...
      if(note !== undefined && note != null) {
        jsonObj.note = note;
      } // Otherwise ignore...
      if(expirationTime !== undefined && expirationTime != null) {
        jsonObj.expirationTime = expirationTime;
      } // Otherwise ignore...
      if(keywordFolder !== undefined && keywordFolder != null) {
        jsonObj.keywordFolder = keywordFolder;
      } // Otherwise ignore...
      if(folderPath !== undefined && folderPath != null) {
        jsonObj.folderPath = folderPath;
      } // Otherwise ignore...
      if(keyword !== undefined && keyword != null) {
        jsonObj.keyword = keyword;
      } // Otherwise ignore...
      if(queryKey !== undefined && queryKey != null) {
        jsonObj.queryKey = queryKey;
      } // Otherwise ignore...
      if(scope !== undefined && scope != null) {
        jsonObj.scope = scope;
      } // Otherwise ignore...
      if(dynamic !== undefined && dynamic != null) {
        jsonObj.dynamic = dynamic;
      } // Otherwise ignore...
      if(caseSensitive !== undefined && caseSensitive != null) {
        jsonObj.caseSensitive = caseSensitive;
      } // Otherwise ignore...
      if(createdTime !== undefined && createdTime != null) {
        jsonObj.createdTime = createdTime;
      } // Otherwise ignore...
      if(modifiedTime !== undefined && modifiedTime != null) {
        jsonObj.modifiedTime = modifiedTime;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(guid) {
        str += "\"guid\":\"" + guid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"guid\":null, ";
      }
      if(appClient) {
        str += "\"appClient\":\"" + appClient + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"appClient\":null, ";
      }
      if(clientRootDomain) {
        str += "\"clientRootDomain\":\"" + clientRootDomain + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"clientRootDomain\":null, ";
      }
      if(user) {
        str += "\"user\":\"" + user + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"user\":null, ";
      }
      if(shortLink) {
        str += "\"shortLink\":\"" + shortLink + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"shortLink\":null, ";
      }
      if(domain) {
        str += "\"domain\":\"" + domain + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"domain\":null, ";
      }
      if(token) {
        str += "\"token\":\"" + token + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"token\":null, ";
      }
      if(longUrl) {
        str += "\"longUrl\":\"" + longUrl + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"longUrl\":null, ";
      }
      if(shortUrl) {
        str += "\"shortUrl\":\"" + shortUrl + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"shortUrl\":null, ";
      }
      if(internal) {
        str += "\"internal\":" + internal + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"internal\":null, ";
      }
      if(caching) {
        str += "\"caching\":" + caching + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"caching\":null, ";
      }
      if(memo) {
        str += "\"memo\":\"" + memo + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"memo\":null, ";
      }
      if(status) {
        str += "\"status\":\"" + status + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"status\":null, ";
      }
      if(note) {
        str += "\"note\":\"" + note + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"note\":null, ";
      }
      if(expirationTime) {
        str += "\"expirationTime\":" + expirationTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"expirationTime\":null, ";
      }
      if(keywordFolder) {
        str += "\"keywordFolder\":\"" + keywordFolder + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"keywordFolder\":null, ";
      }
      if(folderPath) {
        str += "\"folderPath\":\"" + folderPath + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"folderPath\":null, ";
      }
      if(keyword) {
        str += "\"keyword\":\"" + keyword + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"keyword\":null, ";
      }
      if(queryKey) {
        str += "\"queryKey\":\"" + queryKey + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"queryKey\":null, ";
      }
      if(scope) {
        str += "\"scope\":\"" + scope + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"scope\":null, ";
      }
      if(dynamic) {
        str += "\"dynamic\":" + dynamic + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"dynamic\":null, ";
      }
      if(caseSensitive) {
        str += "\"caseSensitive\":" + caseSensitive + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"caseSensitive\":null, ";
      }
      if(createdTime) {
        str += "\"createdTime\":" + createdTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"createdTime\":null, ";
      }
      if(modifiedTime) {
        str += "\"modifiedTime\":" + modifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"modifiedTime\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "guid:" + guid + ", ";
      str += "appClient:" + appClient + ", ";
      str += "clientRootDomain:" + clientRootDomain + ", ";
      str += "user:" + user + ", ";
      str += "shortLink:" + shortLink + ", ";
      str += "domain:" + domain + ", ";
      str += "token:" + token + ", ";
      str += "longUrl:" + longUrl + ", ";
      str += "shortUrl:" + shortUrl + ", ";
      str += "internal:" + internal + ", ";
      str += "caching:" + caching + ", ";
      str += "memo:" + memo + ", ";
      str += "status:" + status + ", ";
      str += "note:" + note + ", ";
      str += "expirationTime:" + expirationTime + ", ";
      str += "keywordFolder:" + keywordFolder + ", ";
      str += "folderPath:" + folderPath + ", ";
      str += "keyword:" + keyword + ", ";
      str += "queryKey:" + queryKey + ", ";
      str += "scope:" + scope + ", ";
      str += "dynamic:" + dynamic + ", ";
      str += "caseSensitive:" + caseSensitive + ", ";
      str += "createdTime:" + createdTime + ", ";
      str += "modifiedTime:" + modifiedTime + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

cannyurl.wa.bean.KeywordLinkJsBean.create = function(obj) {
  var o = new cannyurl.wa.bean.KeywordLinkJsBean();

  if(obj.guid !== undefined && obj.guid != null) {
    o.setGuid(obj.guid);
  }
  if(obj.appClient !== undefined && obj.appClient != null) {
    o.setAppClient(obj.appClient);
  }
  if(obj.clientRootDomain !== undefined && obj.clientRootDomain != null) {
    o.setClientRootDomain(obj.clientRootDomain);
  }
  if(obj.user !== undefined && obj.user != null) {
    o.setUser(obj.user);
  }
  if(obj.shortLink !== undefined && obj.shortLink != null) {
    o.setShortLink(obj.shortLink);
  }
  if(obj.domain !== undefined && obj.domain != null) {
    o.setDomain(obj.domain);
  }
  if(obj.token !== undefined && obj.token != null) {
    o.setToken(obj.token);
  }
  if(obj.longUrl !== undefined && obj.longUrl != null) {
    o.setLongUrl(obj.longUrl);
  }
  if(obj.shortUrl !== undefined && obj.shortUrl != null) {
    o.setShortUrl(obj.shortUrl);
  }
  if(obj.internal !== undefined && obj.internal != null) {
    o.setInternal(obj.internal);
  }
  if(obj.caching !== undefined && obj.caching != null) {
    o.setCaching(obj.caching);
  }
  if(obj.memo !== undefined && obj.memo != null) {
    o.setMemo(obj.memo);
  }
  if(obj.status !== undefined && obj.status != null) {
    o.setStatus(obj.status);
  }
  if(obj.note !== undefined && obj.note != null) {
    o.setNote(obj.note);
  }
  if(obj.expirationTime !== undefined && obj.expirationTime != null) {
    o.setExpirationTime(obj.expirationTime);
  }
  if(obj.keywordFolder !== undefined && obj.keywordFolder != null) {
    o.setKeywordFolder(obj.keywordFolder);
  }
  if(obj.folderPath !== undefined && obj.folderPath != null) {
    o.setFolderPath(obj.folderPath);
  }
  if(obj.keyword !== undefined && obj.keyword != null) {
    o.setKeyword(obj.keyword);
  }
  if(obj.queryKey !== undefined && obj.queryKey != null) {
    o.setQueryKey(obj.queryKey);
  }
  if(obj.scope !== undefined && obj.scope != null) {
    o.setScope(obj.scope);
  }
  if(obj.dynamic !== undefined && obj.dynamic != null) {
    o.setDynamic(obj.dynamic);
  }
  if(obj.caseSensitive !== undefined && obj.caseSensitive != null) {
    o.setCaseSensitive(obj.caseSensitive);
  }
  if(obj.createdTime !== undefined && obj.createdTime != null) {
    o.setCreatedTime(obj.createdTime);
  }
  if(obj.modifiedTime !== undefined && obj.modifiedTime != null) {
    o.setModifiedTime(obj.modifiedTime);
  }
    
  return o;
};

cannyurl.wa.bean.KeywordLinkJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = cannyurl.wa.bean.KeywordLinkJsBean.create(jsonObj);
  return obj;
};
