//////////////////////////////////////////////////////////
// <script src="/js/bean/linkmessagejsbean-1.0.js"></script>
// Last modified time: 1365474465410.
//////////////////////////////////////////////////////////

var cannyurl = cannyurl || {};
cannyurl.wa = cannyurl.wa || {};
cannyurl.wa.bean = cannyurl.wa.bean || {};
cannyurl.wa.bean.LinkMessageJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var guid = generateUuid();
    var shortLink;
    var message;
    var password;
    var createdTime = getCurrentTime();
    var modifiedTime;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getGuid = function() { return guid; };
    this.setGuid = function(value) { guid = value; };
    this.getShortLink = function() { return shortLink; };
    this.setShortLink = function(value) { shortLink = value; };
    this.getMessage = function() { return message; };
    this.setMessage = function(value) { message = value; };
    this.getPassword = function() { return password; };
    this.setPassword = function(value) { password = value; };
    this.getCreatedTime = function() { return createdTime; };
    this.setCreatedTime = function(value) { createdTime = value; };
    this.getModifiedTime = function() { return modifiedTime; };
    this.setModifiedTime = function(value) { modifiedTime = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new cannyurl.wa.bean.LinkMessageJsBean();

      o.setGuid(generateUuid());
      if(shortLink !== undefined && shortLink != null) {
        o.setShortLink(shortLink);
      }
      if(message !== undefined && message != null) {
        o.setMessage(message);
      }
      if(password !== undefined && password != null) {
        o.setPassword(password);
      }
      o.setCreatedTime(getCurrentTime());
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(guid !== undefined && guid != null) {
        jsonObj.guid = guid;
      } // Otherwise ignore...
      if(shortLink !== undefined && shortLink != null) {
        jsonObj.shortLink = shortLink;
      } // Otherwise ignore...
      if(message !== undefined && message != null) {
        jsonObj.message = message;
      } // Otherwise ignore...
      if(password !== undefined && password != null) {
        jsonObj.password = password;
      } // Otherwise ignore...
      if(createdTime !== undefined && createdTime != null) {
        jsonObj.createdTime = createdTime;
      } // Otherwise ignore...
      if(modifiedTime !== undefined && modifiedTime != null) {
        jsonObj.modifiedTime = modifiedTime;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(guid) {
        str += "\"guid\":\"" + guid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"guid\":null, ";
      }
      if(shortLink) {
        str += "\"shortLink\":\"" + shortLink + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"shortLink\":null, ";
      }
      if(message) {
        str += "\"message\":\"" + message + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"message\":null, ";
      }
      if(password) {
        str += "\"password\":\"" + password + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"password\":null, ";
      }
      if(createdTime) {
        str += "\"createdTime\":" + createdTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"createdTime\":null, ";
      }
      if(modifiedTime) {
        str += "\"modifiedTime\":" + modifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"modifiedTime\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "guid:" + guid + ", ";
      str += "shortLink:" + shortLink + ", ";
      str += "message:" + message + ", ";
      str += "password:" + password + ", ";
      str += "createdTime:" + createdTime + ", ";
      str += "modifiedTime:" + modifiedTime + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

cannyurl.wa.bean.LinkMessageJsBean.create = function(obj) {
  var o = new cannyurl.wa.bean.LinkMessageJsBean();

  if(obj.guid !== undefined && obj.guid != null) {
    o.setGuid(obj.guid);
  }
  if(obj.shortLink !== undefined && obj.shortLink != null) {
    o.setShortLink(obj.shortLink);
  }
  if(obj.message !== undefined && obj.message != null) {
    o.setMessage(obj.message);
  }
  if(obj.password !== undefined && obj.password != null) {
    o.setPassword(obj.password);
  }
  if(obj.createdTime !== undefined && obj.createdTime != null) {
    o.setCreatedTime(obj.createdTime);
  }
  if(obj.modifiedTime !== undefined && obj.modifiedTime != null) {
    o.setModifiedTime(obj.modifiedTime);
  }
    
  return o;
};

cannyurl.wa.bean.LinkMessageJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = cannyurl.wa.bean.LinkMessageJsBean.create(jsonObj);
  return obj;
};
