//////////////////////////////////////////////////////////
// <script src="/js/bean/qrcodejsbean-1.0.js"></script>
// Last modified time: 1365474465391.
//////////////////////////////////////////////////////////

var cannyurl = cannyurl || {};
cannyurl.wa = cannyurl.wa || {};
cannyurl.wa.bean = cannyurl.wa.bean || {};
cannyurl.wa.bean.QrCodeJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var guid = generateUuid();
    var shortLink;
    var imageLink;
    var imageUrl;
    var type;
    var status;
    var createdTime = getCurrentTime();
    var modifiedTime;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getGuid = function() { return guid; };
    this.setGuid = function(value) { guid = value; };
    this.getShortLink = function() { return shortLink; };
    this.setShortLink = function(value) { shortLink = value; };
    this.getImageLink = function() { return imageLink; };
    this.setImageLink = function(value) { imageLink = value; };
    this.getImageUrl = function() { return imageUrl; };
    this.setImageUrl = function(value) { imageUrl = value; };
    this.getType = function() { return type; };
    this.setType = function(value) { type = value; };
    this.getStatus = function() { return status; };
    this.setStatus = function(value) { status = value; };
    this.getCreatedTime = function() { return createdTime; };
    this.setCreatedTime = function(value) { createdTime = value; };
    this.getModifiedTime = function() { return modifiedTime; };
    this.setModifiedTime = function(value) { modifiedTime = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new cannyurl.wa.bean.QrCodeJsBean();

      o.setGuid(generateUuid());
      if(shortLink !== undefined && shortLink != null) {
        o.setShortLink(shortLink);
      }
      if(imageLink !== undefined && imageLink != null) {
        o.setImageLink(imageLink);
      }
      if(imageUrl !== undefined && imageUrl != null) {
        o.setImageUrl(imageUrl);
      }
      if(type !== undefined && type != null) {
        o.setType(type);
      }
      if(status !== undefined && status != null) {
        o.setStatus(status);
      }
      o.setCreatedTime(getCurrentTime());
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(guid !== undefined && guid != null) {
        jsonObj.guid = guid;
      } // Otherwise ignore...
      if(shortLink !== undefined && shortLink != null) {
        jsonObj.shortLink = shortLink;
      } // Otherwise ignore...
      if(imageLink !== undefined && imageLink != null) {
        jsonObj.imageLink = imageLink;
      } // Otherwise ignore...
      if(imageUrl !== undefined && imageUrl != null) {
        jsonObj.imageUrl = imageUrl;
      } // Otherwise ignore...
      if(type !== undefined && type != null) {
        jsonObj.type = type;
      } // Otherwise ignore...
      if(status !== undefined && status != null) {
        jsonObj.status = status;
      } // Otherwise ignore...
      if(createdTime !== undefined && createdTime != null) {
        jsonObj.createdTime = createdTime;
      } // Otherwise ignore...
      if(modifiedTime !== undefined && modifiedTime != null) {
        jsonObj.modifiedTime = modifiedTime;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(guid) {
        str += "\"guid\":\"" + guid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"guid\":null, ";
      }
      if(shortLink) {
        str += "\"shortLink\":\"" + shortLink + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"shortLink\":null, ";
      }
      if(imageLink) {
        str += "\"imageLink\":\"" + imageLink + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"imageLink\":null, ";
      }
      if(imageUrl) {
        str += "\"imageUrl\":\"" + imageUrl + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"imageUrl\":null, ";
      }
      if(type) {
        str += "\"type\":\"" + type + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"type\":null, ";
      }
      if(status) {
        str += "\"status\":\"" + status + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"status\":null, ";
      }
      if(createdTime) {
        str += "\"createdTime\":" + createdTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"createdTime\":null, ";
      }
      if(modifiedTime) {
        str += "\"modifiedTime\":" + modifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"modifiedTime\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "guid:" + guid + ", ";
      str += "shortLink:" + shortLink + ", ";
      str += "imageLink:" + imageLink + ", ";
      str += "imageUrl:" + imageUrl + ", ";
      str += "type:" + type + ", ";
      str += "status:" + status + ", ";
      str += "createdTime:" + createdTime + ", ";
      str += "modifiedTime:" + modifiedTime + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

cannyurl.wa.bean.QrCodeJsBean.create = function(obj) {
  var o = new cannyurl.wa.bean.QrCodeJsBean();

  if(obj.guid !== undefined && obj.guid != null) {
    o.setGuid(obj.guid);
  }
  if(obj.shortLink !== undefined && obj.shortLink != null) {
    o.setShortLink(obj.shortLink);
  }
  if(obj.imageLink !== undefined && obj.imageLink != null) {
    o.setImageLink(obj.imageLink);
  }
  if(obj.imageUrl !== undefined && obj.imageUrl != null) {
    o.setImageUrl(obj.imageUrl);
  }
  if(obj.type !== undefined && obj.type != null) {
    o.setType(obj.type);
  }
  if(obj.status !== undefined && obj.status != null) {
    o.setStatus(obj.status);
  }
  if(obj.createdTime !== undefined && obj.createdTime != null) {
    o.setCreatedTime(obj.createdTime);
  }
  if(obj.modifiedTime !== undefined && obj.modifiedTime != null) {
    o.setModifiedTime(obj.modifiedTime);
  }
    
  return o;
};

cannyurl.wa.bean.QrCodeJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = cannyurl.wa.bean.QrCodeJsBean.create(jsonObj);
  return obj;
};
