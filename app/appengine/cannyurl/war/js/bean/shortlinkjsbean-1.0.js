//////////////////////////////////////////////////////////
// <script src="/js/bean/shortlinkjsbean-1.0.js"></script>
// Last modified time: 1365474465303.
//////////////////////////////////////////////////////////

var cannyurl = cannyurl || {};
cannyurl.wa = cannyurl.wa || {};
cannyurl.wa.bean = cannyurl.wa.bean || {};
cannyurl.wa.bean.ShortLinkJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var guid = generateUuid();
    var appClient;
    var clientRootDomain;
    var owner;
    var longUrlDomain;
    var longUrl;
    var longUrlFull;
    var longUrlHash;
    var reuseExistingShortUrl;
    var failIfShortUrlExists;
    var domain;
    var domainType;
    var usercode;
    var username;
    var tokenPrefix;
    var token;
    var tokenType;
    var sassyTokenType;
    var shortUrl;
    var shortPassage;
    var redirectType;
    var flashDuration;
    var accessType;
    var viewType;
    var shareType;
    var readOnly;
    var displayMessage;
    var shortMessage;
    var keywordEnabled;
    var bookmarkEnabled;
    var referrerInfo;
    var status;
    var note;
    var expirationTime;
    var createdTime = getCurrentTime();
    var modifiedTime;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getGuid = function() { return guid; };
    this.setGuid = function(value) { guid = value; };
    this.getAppClient = function() { return appClient; };
    this.setAppClient = function(value) { appClient = value; };
    this.getClientRootDomain = function() { return clientRootDomain; };
    this.setClientRootDomain = function(value) { clientRootDomain = value; };
    this.getOwner = function() { return owner; };
    this.setOwner = function(value) { owner = value; };
    this.getLongUrlDomain = function() { return longUrlDomain; };
    this.setLongUrlDomain = function(value) { longUrlDomain = value; };
    this.getLongUrl = function() { return longUrl; };
    this.setLongUrl = function(value) { longUrl = value; };
    this.getLongUrlFull = function() { return longUrlFull; };
    this.setLongUrlFull = function(value) { longUrlFull = value; };
    this.getLongUrlHash = function() { return longUrlHash; };
    this.setLongUrlHash = function(value) { longUrlHash = value; };
    this.getReuseExistingShortUrl = function() { return reuseExistingShortUrl; };
    this.setReuseExistingShortUrl = function(value) { reuseExistingShortUrl = value; };
    this.getFailIfShortUrlExists = function() { return failIfShortUrlExists; };
    this.setFailIfShortUrlExists = function(value) { failIfShortUrlExists = value; };
    this.getDomain = function() { return domain; };
    this.setDomain = function(value) { domain = value; };
    this.getDomainType = function() { return domainType; };
    this.setDomainType = function(value) { domainType = value; };
    this.getUsercode = function() { return usercode; };
    this.setUsercode = function(value) { usercode = value; };
    this.getUsername = function() { return username; };
    this.setUsername = function(value) { username = value; };
    this.getTokenPrefix = function() { return tokenPrefix; };
    this.setTokenPrefix = function(value) { tokenPrefix = value; };
    this.getToken = function() { return token; };
    this.setToken = function(value) { token = value; };
    this.getTokenType = function() { return tokenType; };
    this.setTokenType = function(value) { tokenType = value; };
    this.getSassyTokenType = function() { return sassyTokenType; };
    this.setSassyTokenType = function(value) { sassyTokenType = value; };
    this.getShortUrl = function() { return shortUrl; };
    this.setShortUrl = function(value) { shortUrl = value; };
    this.getShortPassage = function() { return shortPassage; };
    this.setShortPassage = function(value) { shortPassage = value; };
    this.getRedirectType = function() { return redirectType; };
    this.setRedirectType = function(value) { redirectType = value; };
    this.getFlashDuration = function() { return flashDuration; };
    this.setFlashDuration = function(value) { flashDuration = value; };
    this.getAccessType = function() { return accessType; };
    this.setAccessType = function(value) { accessType = value; };
    this.getViewType = function() { return viewType; };
    this.setViewType = function(value) { viewType = value; };
    this.getShareType = function() { return shareType; };
    this.setShareType = function(value) { shareType = value; };
    this.getReadOnly = function() { return readOnly; };
    this.setReadOnly = function(value) { readOnly = value; };
    this.getDisplayMessage = function() { return displayMessage; };
    this.setDisplayMessage = function(value) { displayMessage = value; };
    this.getShortMessage = function() { return shortMessage; };
    this.setShortMessage = function(value) { shortMessage = value; };
    this.getKeywordEnabled = function() { return keywordEnabled; };
    this.setKeywordEnabled = function(value) { keywordEnabled = value; };
    this.getBookmarkEnabled = function() { return bookmarkEnabled; };
    this.setBookmarkEnabled = function(value) { bookmarkEnabled = value; };
    this.getReferrerInfo = function() { return referrerInfo; };
    this.setReferrerInfo = function(value) { referrerInfo = value; };
    this.getStatus = function() { return status; };
    this.setStatus = function(value) { status = value; };
    this.getNote = function() { return note; };
    this.setNote = function(value) { note = value; };
    this.getExpirationTime = function() { return expirationTime; };
    this.setExpirationTime = function(value) { expirationTime = value; };
    this.getCreatedTime = function() { return createdTime; };
    this.setCreatedTime = function(value) { createdTime = value; };
    this.getModifiedTime = function() { return modifiedTime; };
    this.setModifiedTime = function(value) { modifiedTime = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new cannyurl.wa.bean.ShortLinkJsBean();

      o.setGuid(generateUuid());
      if(appClient !== undefined && appClient != null) {
        o.setAppClient(appClient);
      }
      if(clientRootDomain !== undefined && clientRootDomain != null) {
        o.setClientRootDomain(clientRootDomain);
      }
      if(owner !== undefined && owner != null) {
        o.setOwner(owner);
      }
      if(longUrlDomain !== undefined && longUrlDomain != null) {
        o.setLongUrlDomain(longUrlDomain);
      }
      if(longUrl !== undefined && longUrl != null) {
        o.setLongUrl(longUrl);
      }
      if(longUrlFull !== undefined && longUrlFull != null) {
        o.setLongUrlFull(longUrlFull);
      }
      if(longUrlHash !== undefined && longUrlHash != null) {
        o.setLongUrlHash(longUrlHash);
      }
      if(reuseExistingShortUrl !== undefined && reuseExistingShortUrl != null) {
        o.setReuseExistingShortUrl(reuseExistingShortUrl);
      }
      if(failIfShortUrlExists !== undefined && failIfShortUrlExists != null) {
        o.setFailIfShortUrlExists(failIfShortUrlExists);
      }
      if(domain !== undefined && domain != null) {
        o.setDomain(domain);
      }
      if(domainType !== undefined && domainType != null) {
        o.setDomainType(domainType);
      }
      if(usercode !== undefined && usercode != null) {
        o.setUsercode(usercode);
      }
      if(username !== undefined && username != null) {
        o.setUsername(username);
      }
      if(tokenPrefix !== undefined && tokenPrefix != null) {
        o.setTokenPrefix(tokenPrefix);
      }
      if(token !== undefined && token != null) {
        o.setToken(token);
      }
      if(tokenType !== undefined && tokenType != null) {
        o.setTokenType(tokenType);
      }
      if(sassyTokenType !== undefined && sassyTokenType != null) {
        o.setSassyTokenType(sassyTokenType);
      }
      if(shortUrl !== undefined && shortUrl != null) {
        o.setShortUrl(shortUrl);
      }
      if(shortPassage !== undefined && shortPassage != null) {
        o.setShortPassage(shortPassage);
      }
      if(redirectType !== undefined && redirectType != null) {
        o.setRedirectType(redirectType);
      }
      if(flashDuration !== undefined && flashDuration != null) {
        o.setFlashDuration(flashDuration);
      }
      if(accessType !== undefined && accessType != null) {
        o.setAccessType(accessType);
      }
      if(viewType !== undefined && viewType != null) {
        o.setViewType(viewType);
      }
      if(shareType !== undefined && shareType != null) {
        o.setShareType(shareType);
      }
      if(readOnly !== undefined && readOnly != null) {
        o.setReadOnly(readOnly);
      }
      if(displayMessage !== undefined && displayMessage != null) {
        o.setDisplayMessage(displayMessage);
      }
      if(shortMessage !== undefined && shortMessage != null) {
        o.setShortMessage(shortMessage);
      }
      if(keywordEnabled !== undefined && keywordEnabled != null) {
        o.setKeywordEnabled(keywordEnabled);
      }
      if(bookmarkEnabled !== undefined && bookmarkEnabled != null) {
        o.setBookmarkEnabled(bookmarkEnabled);
      }
      //o.setReferrerInfo(referrerInfo.clone());
      if(referrerInfo !== undefined && referrerInfo != null) {
        o.setReferrerInfo(referrerInfo);
      }
      if(status !== undefined && status != null) {
        o.setStatus(status);
      }
      if(note !== undefined && note != null) {
        o.setNote(note);
      }
      if(expirationTime !== undefined && expirationTime != null) {
        o.setExpirationTime(expirationTime);
      }
      o.setCreatedTime(getCurrentTime());
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(guid !== undefined && guid != null) {
        jsonObj.guid = guid;
      } // Otherwise ignore...
      if(appClient !== undefined && appClient != null) {
        jsonObj.appClient = appClient;
      } // Otherwise ignore...
      if(clientRootDomain !== undefined && clientRootDomain != null) {
        jsonObj.clientRootDomain = clientRootDomain;
      } // Otherwise ignore...
      if(owner !== undefined && owner != null) {
        jsonObj.owner = owner;
      } // Otherwise ignore...
      if(longUrlDomain !== undefined && longUrlDomain != null) {
        jsonObj.longUrlDomain = longUrlDomain;
      } // Otherwise ignore...
      if(longUrl !== undefined && longUrl != null) {
        jsonObj.longUrl = longUrl;
      } // Otherwise ignore...
      if(longUrlFull !== undefined && longUrlFull != null) {
        jsonObj.longUrlFull = longUrlFull;
      } // Otherwise ignore...
      if(longUrlHash !== undefined && longUrlHash != null) {
        jsonObj.longUrlHash = longUrlHash;
      } // Otherwise ignore...
      if(reuseExistingShortUrl !== undefined && reuseExistingShortUrl != null) {
        jsonObj.reuseExistingShortUrl = reuseExistingShortUrl;
      } // Otherwise ignore...
      if(failIfShortUrlExists !== undefined && failIfShortUrlExists != null) {
        jsonObj.failIfShortUrlExists = failIfShortUrlExists;
      } // Otherwise ignore...
      if(domain !== undefined && domain != null) {
        jsonObj.domain = domain;
      } // Otherwise ignore...
      if(domainType !== undefined && domainType != null) {
        jsonObj.domainType = domainType;
      } // Otherwise ignore...
      if(usercode !== undefined && usercode != null) {
        jsonObj.usercode = usercode;
      } // Otherwise ignore...
      if(username !== undefined && username != null) {
        jsonObj.username = username;
      } // Otherwise ignore...
      if(tokenPrefix !== undefined && tokenPrefix != null) {
        jsonObj.tokenPrefix = tokenPrefix;
      } // Otherwise ignore...
      if(token !== undefined && token != null) {
        jsonObj.token = token;
      } // Otherwise ignore...
      if(tokenType !== undefined && tokenType != null) {
        jsonObj.tokenType = tokenType;
      } // Otherwise ignore...
      if(sassyTokenType !== undefined && sassyTokenType != null) {
        jsonObj.sassyTokenType = sassyTokenType;
      } // Otherwise ignore...
      if(shortUrl !== undefined && shortUrl != null) {
        jsonObj.shortUrl = shortUrl;
      } // Otherwise ignore...
      if(shortPassage !== undefined && shortPassage != null) {
        jsonObj.shortPassage = shortPassage;
      } // Otherwise ignore...
      if(redirectType !== undefined && redirectType != null) {
        jsonObj.redirectType = redirectType;
      } // Otherwise ignore...
      if(flashDuration !== undefined && flashDuration != null) {
        jsonObj.flashDuration = flashDuration;
      } // Otherwise ignore...
      if(accessType !== undefined && accessType != null) {
        jsonObj.accessType = accessType;
      } // Otherwise ignore...
      if(viewType !== undefined && viewType != null) {
        jsonObj.viewType = viewType;
      } // Otherwise ignore...
      if(shareType !== undefined && shareType != null) {
        jsonObj.shareType = shareType;
      } // Otherwise ignore...
      if(readOnly !== undefined && readOnly != null) {
        jsonObj.readOnly = readOnly;
      } // Otherwise ignore...
      if(displayMessage !== undefined && displayMessage != null) {
        jsonObj.displayMessage = displayMessage;
      } // Otherwise ignore...
      if(shortMessage !== undefined && shortMessage != null) {
        jsonObj.shortMessage = shortMessage;
      } // Otherwise ignore...
      if(keywordEnabled !== undefined && keywordEnabled != null) {
        jsonObj.keywordEnabled = keywordEnabled;
      } // Otherwise ignore...
      if(bookmarkEnabled !== undefined && bookmarkEnabled != null) {
        jsonObj.bookmarkEnabled = bookmarkEnabled;
      } // Otherwise ignore...
      if(referrerInfo !== undefined && referrerInfo != null) {
        jsonObj.referrerInfo = referrerInfo;
      } // Otherwise ignore...
      if(status !== undefined && status != null) {
        jsonObj.status = status;
      } // Otherwise ignore...
      if(note !== undefined && note != null) {
        jsonObj.note = note;
      } // Otherwise ignore...
      if(expirationTime !== undefined && expirationTime != null) {
        jsonObj.expirationTime = expirationTime;
      } // Otherwise ignore...
      if(createdTime !== undefined && createdTime != null) {
        jsonObj.createdTime = createdTime;
      } // Otherwise ignore...
      if(modifiedTime !== undefined && modifiedTime != null) {
        jsonObj.modifiedTime = modifiedTime;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(guid) {
        str += "\"guid\":\"" + guid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"guid\":null, ";
      }
      if(appClient) {
        str += "\"appClient\":\"" + appClient + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"appClient\":null, ";
      }
      if(clientRootDomain) {
        str += "\"clientRootDomain\":\"" + clientRootDomain + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"clientRootDomain\":null, ";
      }
      if(owner) {
        str += "\"owner\":\"" + owner + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"owner\":null, ";
      }
      if(longUrlDomain) {
        str += "\"longUrlDomain\":\"" + longUrlDomain + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"longUrlDomain\":null, ";
      }
      if(longUrl) {
        str += "\"longUrl\":\"" + longUrl + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"longUrl\":null, ";
      }
      if(longUrlFull) {
        str += "\"longUrlFull\":\"" + longUrlFull + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"longUrlFull\":null, ";
      }
      if(longUrlHash) {
        str += "\"longUrlHash\":\"" + longUrlHash + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"longUrlHash\":null, ";
      }
      if(reuseExistingShortUrl) {
        str += "\"reuseExistingShortUrl\":" + reuseExistingShortUrl + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"reuseExistingShortUrl\":null, ";
      }
      if(failIfShortUrlExists) {
        str += "\"failIfShortUrlExists\":" + failIfShortUrlExists + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"failIfShortUrlExists\":null, ";
      }
      if(domain) {
        str += "\"domain\":\"" + domain + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"domain\":null, ";
      }
      if(domainType) {
        str += "\"domainType\":\"" + domainType + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"domainType\":null, ";
      }
      if(usercode) {
        str += "\"usercode\":\"" + usercode + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"usercode\":null, ";
      }
      if(username) {
        str += "\"username\":\"" + username + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"username\":null, ";
      }
      if(tokenPrefix) {
        str += "\"tokenPrefix\":\"" + tokenPrefix + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"tokenPrefix\":null, ";
      }
      if(token) {
        str += "\"token\":\"" + token + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"token\":null, ";
      }
      if(tokenType) {
        str += "\"tokenType\":\"" + tokenType + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"tokenType\":null, ";
      }
      if(sassyTokenType) {
        str += "\"sassyTokenType\":\"" + sassyTokenType + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"sassyTokenType\":null, ";
      }
      if(shortUrl) {
        str += "\"shortUrl\":\"" + shortUrl + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"shortUrl\":null, ";
      }
      if(shortPassage) {
        str += "\"shortPassage\":\"" + shortPassage + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"shortPassage\":null, ";
      }
      if(redirectType) {
        str += "\"redirectType\":\"" + redirectType + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"redirectType\":null, ";
      }
      if(flashDuration) {
        str += "\"flashDuration\":" + flashDuration + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"flashDuration\":null, ";
      }
      if(accessType) {
        str += "\"accessType\":\"" + accessType + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"accessType\":null, ";
      }
      if(viewType) {
        str += "\"viewType\":\"" + viewType + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"viewType\":null, ";
      }
      if(shareType) {
        str += "\"shareType\":\"" + shareType + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"shareType\":null, ";
      }
      if(readOnly) {
        str += "\"readOnly\":" + readOnly + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"readOnly\":null, ";
      }
      if(displayMessage) {
        str += "\"displayMessage\":\"" + displayMessage + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"displayMessage\":null, ";
      }
      if(shortMessage) {
        str += "\"shortMessage\":\"" + shortMessage + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"shortMessage\":null, ";
      }
      if(keywordEnabled) {
        str += "\"keywordEnabled\":" + keywordEnabled + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"keywordEnabled\":null, ";
      }
      if(bookmarkEnabled) {
        str += "\"bookmarkEnabled\":" + bookmarkEnabled + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"bookmarkEnabled\":null, ";
      }
      str += "\"referrerInfo\":" + referrerInfo.toJsonString() + ", ";
      if(status) {
        str += "\"status\":\"" + status + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"status\":null, ";
      }
      if(note) {
        str += "\"note\":\"" + note + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"note\":null, ";
      }
      if(expirationTime) {
        str += "\"expirationTime\":" + expirationTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"expirationTime\":null, ";
      }
      if(createdTime) {
        str += "\"createdTime\":" + createdTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"createdTime\":null, ";
      }
      if(modifiedTime) {
        str += "\"modifiedTime\":" + modifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"modifiedTime\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "guid:" + guid + ", ";
      str += "appClient:" + appClient + ", ";
      str += "clientRootDomain:" + clientRootDomain + ", ";
      str += "owner:" + owner + ", ";
      str += "longUrlDomain:" + longUrlDomain + ", ";
      str += "longUrl:" + longUrl + ", ";
      str += "longUrlFull:" + longUrlFull + ", ";
      str += "longUrlHash:" + longUrlHash + ", ";
      str += "reuseExistingShortUrl:" + reuseExistingShortUrl + ", ";
      str += "failIfShortUrlExists:" + failIfShortUrlExists + ", ";
      str += "domain:" + domain + ", ";
      str += "domainType:" + domainType + ", ";
      str += "usercode:" + usercode + ", ";
      str += "username:" + username + ", ";
      str += "tokenPrefix:" + tokenPrefix + ", ";
      str += "token:" + token + ", ";
      str += "tokenType:" + tokenType + ", ";
      str += "sassyTokenType:" + sassyTokenType + ", ";
      str += "shortUrl:" + shortUrl + ", ";
      str += "shortPassage:" + shortPassage + ", ";
      str += "redirectType:" + redirectType + ", ";
      str += "flashDuration:" + flashDuration + ", ";
      str += "accessType:" + accessType + ", ";
      str += "viewType:" + viewType + ", ";
      str += "shareType:" + shareType + ", ";
      str += "readOnly:" + readOnly + ", ";
      str += "displayMessage:" + displayMessage + ", ";
      str += "shortMessage:" + shortMessage + ", ";
      str += "keywordEnabled:" + keywordEnabled + ", ";
      str += "bookmarkEnabled:" + bookmarkEnabled + ", ";
      str += "referrerInfo:" + referrerInfo + ", ";
      str += "status:" + status + ", ";
      str += "note:" + note + ", ";
      str += "expirationTime:" + expirationTime + ", ";
      str += "createdTime:" + createdTime + ", ";
      str += "modifiedTime:" + modifiedTime + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

cannyurl.wa.bean.ShortLinkJsBean.create = function(obj) {
  var o = new cannyurl.wa.bean.ShortLinkJsBean();

  if(obj.guid !== undefined && obj.guid != null) {
    o.setGuid(obj.guid);
  }
  if(obj.appClient !== undefined && obj.appClient != null) {
    o.setAppClient(obj.appClient);
  }
  if(obj.clientRootDomain !== undefined && obj.clientRootDomain != null) {
    o.setClientRootDomain(obj.clientRootDomain);
  }
  if(obj.owner !== undefined && obj.owner != null) {
    o.setOwner(obj.owner);
  }
  if(obj.longUrlDomain !== undefined && obj.longUrlDomain != null) {
    o.setLongUrlDomain(obj.longUrlDomain);
  }
  if(obj.longUrl !== undefined && obj.longUrl != null) {
    o.setLongUrl(obj.longUrl);
  }
  if(obj.longUrlFull !== undefined && obj.longUrlFull != null) {
    o.setLongUrlFull(obj.longUrlFull);
  }
  if(obj.longUrlHash !== undefined && obj.longUrlHash != null) {
    o.setLongUrlHash(obj.longUrlHash);
  }
  if(obj.reuseExistingShortUrl !== undefined && obj.reuseExistingShortUrl != null) {
    o.setReuseExistingShortUrl(obj.reuseExistingShortUrl);
  }
  if(obj.failIfShortUrlExists !== undefined && obj.failIfShortUrlExists != null) {
    o.setFailIfShortUrlExists(obj.failIfShortUrlExists);
  }
  if(obj.domain !== undefined && obj.domain != null) {
    o.setDomain(obj.domain);
  }
  if(obj.domainType !== undefined && obj.domainType != null) {
    o.setDomainType(obj.domainType);
  }
  if(obj.usercode !== undefined && obj.usercode != null) {
    o.setUsercode(obj.usercode);
  }
  if(obj.username !== undefined && obj.username != null) {
    o.setUsername(obj.username);
  }
  if(obj.tokenPrefix !== undefined && obj.tokenPrefix != null) {
    o.setTokenPrefix(obj.tokenPrefix);
  }
  if(obj.token !== undefined && obj.token != null) {
    o.setToken(obj.token);
  }
  if(obj.tokenType !== undefined && obj.tokenType != null) {
    o.setTokenType(obj.tokenType);
  }
  if(obj.sassyTokenType !== undefined && obj.sassyTokenType != null) {
    o.setSassyTokenType(obj.sassyTokenType);
  }
  if(obj.shortUrl !== undefined && obj.shortUrl != null) {
    o.setShortUrl(obj.shortUrl);
  }
  if(obj.shortPassage !== undefined && obj.shortPassage != null) {
    o.setShortPassage(obj.shortPassage);
  }
  if(obj.redirectType !== undefined && obj.redirectType != null) {
    o.setRedirectType(obj.redirectType);
  }
  if(obj.flashDuration !== undefined && obj.flashDuration != null) {
    o.setFlashDuration(obj.flashDuration);
  }
  if(obj.accessType !== undefined && obj.accessType != null) {
    o.setAccessType(obj.accessType);
  }
  if(obj.viewType !== undefined && obj.viewType != null) {
    o.setViewType(obj.viewType);
  }
  if(obj.shareType !== undefined && obj.shareType != null) {
    o.setShareType(obj.shareType);
  }
  if(obj.readOnly !== undefined && obj.readOnly != null) {
    o.setReadOnly(obj.readOnly);
  }
  if(obj.displayMessage !== undefined && obj.displayMessage != null) {
    o.setDisplayMessage(obj.displayMessage);
  }
  if(obj.shortMessage !== undefined && obj.shortMessage != null) {
    o.setShortMessage(obj.shortMessage);
  }
  if(obj.keywordEnabled !== undefined && obj.keywordEnabled != null) {
    o.setKeywordEnabled(obj.keywordEnabled);
  }
  if(obj.bookmarkEnabled !== undefined && obj.bookmarkEnabled != null) {
    o.setBookmarkEnabled(obj.bookmarkEnabled);
  }
  if(obj.referrerInfo !== undefined && obj.referrerInfo != null) {
    o.setReferrerInfo(obj.referrerInfo);
  }
  if(obj.status !== undefined && obj.status != null) {
    o.setStatus(obj.status);
  }
  if(obj.note !== undefined && obj.note != null) {
    o.setNote(obj.note);
  }
  if(obj.expirationTime !== undefined && obj.expirationTime != null) {
    o.setExpirationTime(obj.expirationTime);
  }
  if(obj.createdTime !== undefined && obj.createdTime != null) {
    o.setCreatedTime(obj.createdTime);
  }
  if(obj.modifiedTime !== undefined && obj.modifiedTime != null) {
    o.setModifiedTime(obj.modifiedTime);
  }
    
  return o;
};

cannyurl.wa.bean.ShortLinkJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = cannyurl.wa.bean.ShortLinkJsBean.create(jsonObj);
  return obj;
};
