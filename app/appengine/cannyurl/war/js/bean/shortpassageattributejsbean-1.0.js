//////////////////////////////////////////////////////////
// <script src="/js/bean/shortpassageattributejsbean-1.0.js"></script>
// Last modified time: 1365474465286.
//////////////////////////////////////////////////////////

var cannyurl = cannyurl || {};
cannyurl.wa = cannyurl.wa || {};
cannyurl.wa.bean = cannyurl.wa.bean || {};
cannyurl.wa.bean.ShortPassageAttributeJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var domain;
    var tokenType;
    var displayMessage;
    var redirectType;
    var flashDuration;
    var accessType;
    var viewType;
    var shareType;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getDomain = function() { return domain; };
    this.setDomain = function(value) { domain = value; };
    this.getTokenType = function() { return tokenType; };
    this.setTokenType = function(value) { tokenType = value; };
    this.getDisplayMessage = function() { return displayMessage; };
    this.setDisplayMessage = function(value) { displayMessage = value; };
    this.getRedirectType = function() { return redirectType; };
    this.setRedirectType = function(value) { redirectType = value; };
    this.getFlashDuration = function() { return flashDuration; };
    this.setFlashDuration = function(value) { flashDuration = value; };
    this.getAccessType = function() { return accessType; };
    this.setAccessType = function(value) { accessType = value; };
    this.getViewType = function() { return viewType; };
    this.setViewType = function(value) { viewType = value; };
    this.getShareType = function() { return shareType; };
    this.setShareType = function(value) { shareType = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new cannyurl.wa.bean.ShortPassageAttributeJsBean();

      if(domain !== undefined && domain != null) {
        o.setDomain(domain);
      }
      if(tokenType !== undefined && tokenType != null) {
        o.setTokenType(tokenType);
      }
      if(displayMessage !== undefined && displayMessage != null) {
        o.setDisplayMessage(displayMessage);
      }
      if(redirectType !== undefined && redirectType != null) {
        o.setRedirectType(redirectType);
      }
      if(flashDuration !== undefined && flashDuration != null) {
        o.setFlashDuration(flashDuration);
      }
      if(accessType !== undefined && accessType != null) {
        o.setAccessType(accessType);
      }
      if(viewType !== undefined && viewType != null) {
        o.setViewType(viewType);
      }
      if(shareType !== undefined && shareType != null) {
        o.setShareType(shareType);
      }
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(domain !== undefined && domain != null) {
        jsonObj.domain = domain;
      } // Otherwise ignore...
      if(tokenType !== undefined && tokenType != null) {
        jsonObj.tokenType = tokenType;
      } // Otherwise ignore...
      if(displayMessage !== undefined && displayMessage != null) {
        jsonObj.displayMessage = displayMessage;
      } // Otherwise ignore...
      if(redirectType !== undefined && redirectType != null) {
        jsonObj.redirectType = redirectType;
      } // Otherwise ignore...
      if(flashDuration !== undefined && flashDuration != null) {
        jsonObj.flashDuration = flashDuration;
      } // Otherwise ignore...
      if(accessType !== undefined && accessType != null) {
        jsonObj.accessType = accessType;
      } // Otherwise ignore...
      if(viewType !== undefined && viewType != null) {
        jsonObj.viewType = viewType;
      } // Otherwise ignore...
      if(shareType !== undefined && shareType != null) {
        jsonObj.shareType = shareType;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(domain) {
        str += "\"domain\":\"" + domain + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"domain\":null, ";
      }
      if(tokenType) {
        str += "\"tokenType\":\"" + tokenType + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"tokenType\":null, ";
      }
      if(displayMessage) {
        str += "\"displayMessage\":\"" + displayMessage + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"displayMessage\":null, ";
      }
      if(redirectType) {
        str += "\"redirectType\":\"" + redirectType + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"redirectType\":null, ";
      }
      if(flashDuration) {
        str += "\"flashDuration\":" + flashDuration + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"flashDuration\":null, ";
      }
      if(accessType) {
        str += "\"accessType\":\"" + accessType + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"accessType\":null, ";
      }
      if(viewType) {
        str += "\"viewType\":\"" + viewType + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"viewType\":null, ";
      }
      if(shareType) {
        str += "\"shareType\":\"" + shareType + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"shareType\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "domain:" + domain + ", ";
      str += "tokenType:" + tokenType + ", ";
      str += "displayMessage:" + displayMessage + ", ";
      str += "redirectType:" + redirectType + ", ";
      str += "flashDuration:" + flashDuration + ", ";
      str += "accessType:" + accessType + ", ";
      str += "viewType:" + viewType + ", ";
      str += "shareType:" + shareType + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

cannyurl.wa.bean.ShortPassageAttributeJsBean.create = function(obj) {
  var o = new cannyurl.wa.bean.ShortPassageAttributeJsBean();

  if(obj.domain !== undefined && obj.domain != null) {
    o.setDomain(obj.domain);
  }
  if(obj.tokenType !== undefined && obj.tokenType != null) {
    o.setTokenType(obj.tokenType);
  }
  if(obj.displayMessage !== undefined && obj.displayMessage != null) {
    o.setDisplayMessage(obj.displayMessage);
  }
  if(obj.redirectType !== undefined && obj.redirectType != null) {
    o.setRedirectType(obj.redirectType);
  }
  if(obj.flashDuration !== undefined && obj.flashDuration != null) {
    o.setFlashDuration(obj.flashDuration);
  }
  if(obj.accessType !== undefined && obj.accessType != null) {
    o.setAccessType(obj.accessType);
  }
  if(obj.viewType !== undefined && obj.viewType != null) {
    o.setViewType(obj.viewType);
  }
  if(obj.shareType !== undefined && obj.shareType != null) {
    o.setShareType(obj.shareType);
  }
    
  return o;
};

cannyurl.wa.bean.ShortPassageAttributeJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = cannyurl.wa.bean.ShortPassageAttributeJsBean.create(jsonObj);
  return obj;
};
