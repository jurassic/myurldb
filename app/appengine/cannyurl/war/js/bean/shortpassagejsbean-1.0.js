//////////////////////////////////////////////////////////
// <script src="/js/bean/shortpassagejsbean-1.0.js"></script>
// Last modified time: 1365474465294.
//////////////////////////////////////////////////////////

var cannyurl = cannyurl || {};
cannyurl.wa = cannyurl.wa || {};
cannyurl.wa.bean = cannyurl.wa.bean || {};
cannyurl.wa.bean.ShortPassageJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var guid = generateUuid();
    var owner;
    var longText;
    var shortText;
    var attribute;
    var readOnly;
    var status;
    var note;
    var expirationTime;
    var createdTime = getCurrentTime();
    var modifiedTime;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getGuid = function() { return guid; };
    this.setGuid = function(value) { guid = value; };
    this.getOwner = function() { return owner; };
    this.setOwner = function(value) { owner = value; };
    this.getLongText = function() { return longText; };
    this.setLongText = function(value) { longText = value; };
    this.getShortText = function() { return shortText; };
    this.setShortText = function(value) { shortText = value; };
    this.getAttribute = function() { return attribute; };
    this.setAttribute = function(value) { attribute = value; };
    this.getReadOnly = function() { return readOnly; };
    this.setReadOnly = function(value) { readOnly = value; };
    this.getStatus = function() { return status; };
    this.setStatus = function(value) { status = value; };
    this.getNote = function() { return note; };
    this.setNote = function(value) { note = value; };
    this.getExpirationTime = function() { return expirationTime; };
    this.setExpirationTime = function(value) { expirationTime = value; };
    this.getCreatedTime = function() { return createdTime; };
    this.setCreatedTime = function(value) { createdTime = value; };
    this.getModifiedTime = function() { return modifiedTime; };
    this.setModifiedTime = function(value) { modifiedTime = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new cannyurl.wa.bean.ShortPassageJsBean();

      o.setGuid(generateUuid());
      if(owner !== undefined && owner != null) {
        o.setOwner(owner);
      }
      if(longText !== undefined && longText != null) {
        o.setLongText(longText);
      }
      if(shortText !== undefined && shortText != null) {
        o.setShortText(shortText);
      }
      //o.setAttribute(attribute.clone());
      if(attribute !== undefined && attribute != null) {
        o.setAttribute(attribute);
      }
      if(readOnly !== undefined && readOnly != null) {
        o.setReadOnly(readOnly);
      }
      if(status !== undefined && status != null) {
        o.setStatus(status);
      }
      if(note !== undefined && note != null) {
        o.setNote(note);
      }
      if(expirationTime !== undefined && expirationTime != null) {
        o.setExpirationTime(expirationTime);
      }
      o.setCreatedTime(getCurrentTime());
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(guid !== undefined && guid != null) {
        jsonObj.guid = guid;
      } // Otherwise ignore...
      if(owner !== undefined && owner != null) {
        jsonObj.owner = owner;
      } // Otherwise ignore...
      if(longText !== undefined && longText != null) {
        jsonObj.longText = longText;
      } // Otherwise ignore...
      if(shortText !== undefined && shortText != null) {
        jsonObj.shortText = shortText;
      } // Otherwise ignore...
      if(attribute !== undefined && attribute != null) {
        jsonObj.attribute = attribute;
      } // Otherwise ignore...
      if(readOnly !== undefined && readOnly != null) {
        jsonObj.readOnly = readOnly;
      } // Otherwise ignore...
      if(status !== undefined && status != null) {
        jsonObj.status = status;
      } // Otherwise ignore...
      if(note !== undefined && note != null) {
        jsonObj.note = note;
      } // Otherwise ignore...
      if(expirationTime !== undefined && expirationTime != null) {
        jsonObj.expirationTime = expirationTime;
      } // Otherwise ignore...
      if(createdTime !== undefined && createdTime != null) {
        jsonObj.createdTime = createdTime;
      } // Otherwise ignore...
      if(modifiedTime !== undefined && modifiedTime != null) {
        jsonObj.modifiedTime = modifiedTime;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(guid) {
        str += "\"guid\":\"" + guid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"guid\":null, ";
      }
      if(owner) {
        str += "\"owner\":\"" + owner + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"owner\":null, ";
      }
      if(longText) {
        str += "\"longText\":\"" + longText + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"longText\":null, ";
      }
      if(shortText) {
        str += "\"shortText\":\"" + shortText + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"shortText\":null, ";
      }
      str += "\"attribute\":" + attribute.toJsonString() + ", ";
      if(readOnly) {
        str += "\"readOnly\":" + readOnly + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"readOnly\":null, ";
      }
      if(status) {
        str += "\"status\":\"" + status + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"status\":null, ";
      }
      if(note) {
        str += "\"note\":\"" + note + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"note\":null, ";
      }
      if(expirationTime) {
        str += "\"expirationTime\":" + expirationTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"expirationTime\":null, ";
      }
      if(createdTime) {
        str += "\"createdTime\":" + createdTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"createdTime\":null, ";
      }
      if(modifiedTime) {
        str += "\"modifiedTime\":" + modifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"modifiedTime\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "guid:" + guid + ", ";
      str += "owner:" + owner + ", ";
      str += "longText:" + longText + ", ";
      str += "shortText:" + shortText + ", ";
      str += "attribute:" + attribute + ", ";
      str += "readOnly:" + readOnly + ", ";
      str += "status:" + status + ", ";
      str += "note:" + note + ", ";
      str += "expirationTime:" + expirationTime + ", ";
      str += "createdTime:" + createdTime + ", ";
      str += "modifiedTime:" + modifiedTime + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

cannyurl.wa.bean.ShortPassageJsBean.create = function(obj) {
  var o = new cannyurl.wa.bean.ShortPassageJsBean();

  if(obj.guid !== undefined && obj.guid != null) {
    o.setGuid(obj.guid);
  }
  if(obj.owner !== undefined && obj.owner != null) {
    o.setOwner(obj.owner);
  }
  if(obj.longText !== undefined && obj.longText != null) {
    o.setLongText(obj.longText);
  }
  if(obj.shortText !== undefined && obj.shortText != null) {
    o.setShortText(obj.shortText);
  }
  if(obj.attribute !== undefined && obj.attribute != null) {
    o.setAttribute(obj.attribute);
  }
  if(obj.readOnly !== undefined && obj.readOnly != null) {
    o.setReadOnly(obj.readOnly);
  }
  if(obj.status !== undefined && obj.status != null) {
    o.setStatus(obj.status);
  }
  if(obj.note !== undefined && obj.note != null) {
    o.setNote(obj.note);
  }
  if(obj.expirationTime !== undefined && obj.expirationTime != null) {
    o.setExpirationTime(obj.expirationTime);
  }
  if(obj.createdTime !== undefined && obj.createdTime != null) {
    o.setCreatedTime(obj.createdTime);
  }
  if(obj.modifiedTime !== undefined && obj.modifiedTime != null) {
    o.setModifiedTime(obj.modifiedTime);
  }
    
  return o;
};

cannyurl.wa.bean.ShortPassageJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = cannyurl.wa.bean.ShortPassageJsBean.create(jsonObj);
  return obj;
};
