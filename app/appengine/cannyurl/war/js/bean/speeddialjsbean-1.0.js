//////////////////////////////////////////////////////////
// <script src="/js/bean/speeddialjsbean-1.0.js"></script>
// Last modified time: 1365474465585.
//////////////////////////////////////////////////////////

var cannyurl = cannyurl || {};
cannyurl.wa = cannyurl.wa || {};
cannyurl.wa.bean = cannyurl.wa.bean || {};
cannyurl.wa.bean.SpeedDialJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var guid = generateUuid();
    var user;
    var code;
    var shortcut;
    var shortLink;
    var keywordLink;
    var bookmarkLink;
    var longUrl;
    var shortUrl;
    var caseSensitive;
    var status;
    var note;
    var createdTime = getCurrentTime();
    var modifiedTime;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getGuid = function() { return guid; };
    this.setGuid = function(value) { guid = value; };
    this.getUser = function() { return user; };
    this.setUser = function(value) { user = value; };
    this.getCode = function() { return code; };
    this.setCode = function(value) { code = value; };
    this.getShortcut = function() { return shortcut; };
    this.setShortcut = function(value) { shortcut = value; };
    this.getShortLink = function() { return shortLink; };
    this.setShortLink = function(value) { shortLink = value; };
    this.getKeywordLink = function() { return keywordLink; };
    this.setKeywordLink = function(value) { keywordLink = value; };
    this.getBookmarkLink = function() { return bookmarkLink; };
    this.setBookmarkLink = function(value) { bookmarkLink = value; };
    this.getLongUrl = function() { return longUrl; };
    this.setLongUrl = function(value) { longUrl = value; };
    this.getShortUrl = function() { return shortUrl; };
    this.setShortUrl = function(value) { shortUrl = value; };
    this.getCaseSensitive = function() { return caseSensitive; };
    this.setCaseSensitive = function(value) { caseSensitive = value; };
    this.getStatus = function() { return status; };
    this.setStatus = function(value) { status = value; };
    this.getNote = function() { return note; };
    this.setNote = function(value) { note = value; };
    this.getCreatedTime = function() { return createdTime; };
    this.setCreatedTime = function(value) { createdTime = value; };
    this.getModifiedTime = function() { return modifiedTime; };
    this.setModifiedTime = function(value) { modifiedTime = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new cannyurl.wa.bean.SpeedDialJsBean();

      o.setGuid(generateUuid());
      if(user !== undefined && user != null) {
        o.setUser(user);
      }
      if(code !== undefined && code != null) {
        o.setCode(code);
      }
      if(shortcut !== undefined && shortcut != null) {
        o.setShortcut(shortcut);
      }
      if(shortLink !== undefined && shortLink != null) {
        o.setShortLink(shortLink);
      }
      if(keywordLink !== undefined && keywordLink != null) {
        o.setKeywordLink(keywordLink);
      }
      if(bookmarkLink !== undefined && bookmarkLink != null) {
        o.setBookmarkLink(bookmarkLink);
      }
      if(longUrl !== undefined && longUrl != null) {
        o.setLongUrl(longUrl);
      }
      if(shortUrl !== undefined && shortUrl != null) {
        o.setShortUrl(shortUrl);
      }
      if(caseSensitive !== undefined && caseSensitive != null) {
        o.setCaseSensitive(caseSensitive);
      }
      if(status !== undefined && status != null) {
        o.setStatus(status);
      }
      if(note !== undefined && note != null) {
        o.setNote(note);
      }
      o.setCreatedTime(getCurrentTime());
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(guid !== undefined && guid != null) {
        jsonObj.guid = guid;
      } // Otherwise ignore...
      if(user !== undefined && user != null) {
        jsonObj.user = user;
      } // Otherwise ignore...
      if(code !== undefined && code != null) {
        jsonObj.code = code;
      } // Otherwise ignore...
      if(shortcut !== undefined && shortcut != null) {
        jsonObj.shortcut = shortcut;
      } // Otherwise ignore...
      if(shortLink !== undefined && shortLink != null) {
        jsonObj.shortLink = shortLink;
      } // Otherwise ignore...
      if(keywordLink !== undefined && keywordLink != null) {
        jsonObj.keywordLink = keywordLink;
      } // Otherwise ignore...
      if(bookmarkLink !== undefined && bookmarkLink != null) {
        jsonObj.bookmarkLink = bookmarkLink;
      } // Otherwise ignore...
      if(longUrl !== undefined && longUrl != null) {
        jsonObj.longUrl = longUrl;
      } // Otherwise ignore...
      if(shortUrl !== undefined && shortUrl != null) {
        jsonObj.shortUrl = shortUrl;
      } // Otherwise ignore...
      if(caseSensitive !== undefined && caseSensitive != null) {
        jsonObj.caseSensitive = caseSensitive;
      } // Otherwise ignore...
      if(status !== undefined && status != null) {
        jsonObj.status = status;
      } // Otherwise ignore...
      if(note !== undefined && note != null) {
        jsonObj.note = note;
      } // Otherwise ignore...
      if(createdTime !== undefined && createdTime != null) {
        jsonObj.createdTime = createdTime;
      } // Otherwise ignore...
      if(modifiedTime !== undefined && modifiedTime != null) {
        jsonObj.modifiedTime = modifiedTime;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(guid) {
        str += "\"guid\":\"" + guid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"guid\":null, ";
      }
      if(user) {
        str += "\"user\":\"" + user + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"user\":null, ";
      }
      if(code) {
        str += "\"code\":\"" + code + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"code\":null, ";
      }
      if(shortcut) {
        str += "\"shortcut\":\"" + shortcut + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"shortcut\":null, ";
      }
      if(shortLink) {
        str += "\"shortLink\":\"" + shortLink + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"shortLink\":null, ";
      }
      if(keywordLink) {
        str += "\"keywordLink\":\"" + keywordLink + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"keywordLink\":null, ";
      }
      if(bookmarkLink) {
        str += "\"bookmarkLink\":\"" + bookmarkLink + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"bookmarkLink\":null, ";
      }
      if(longUrl) {
        str += "\"longUrl\":\"" + longUrl + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"longUrl\":null, ";
      }
      if(shortUrl) {
        str += "\"shortUrl\":\"" + shortUrl + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"shortUrl\":null, ";
      }
      if(caseSensitive) {
        str += "\"caseSensitive\":" + caseSensitive + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"caseSensitive\":null, ";
      }
      if(status) {
        str += "\"status\":\"" + status + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"status\":null, ";
      }
      if(note) {
        str += "\"note\":\"" + note + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"note\":null, ";
      }
      if(createdTime) {
        str += "\"createdTime\":" + createdTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"createdTime\":null, ";
      }
      if(modifiedTime) {
        str += "\"modifiedTime\":" + modifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"modifiedTime\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "guid:" + guid + ", ";
      str += "user:" + user + ", ";
      str += "code:" + code + ", ";
      str += "shortcut:" + shortcut + ", ";
      str += "shortLink:" + shortLink + ", ";
      str += "keywordLink:" + keywordLink + ", ";
      str += "bookmarkLink:" + bookmarkLink + ", ";
      str += "longUrl:" + longUrl + ", ";
      str += "shortUrl:" + shortUrl + ", ";
      str += "caseSensitive:" + caseSensitive + ", ";
      str += "status:" + status + ", ";
      str += "note:" + note + ", ";
      str += "createdTime:" + createdTime + ", ";
      str += "modifiedTime:" + modifiedTime + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

cannyurl.wa.bean.SpeedDialJsBean.create = function(obj) {
  var o = new cannyurl.wa.bean.SpeedDialJsBean();

  if(obj.guid !== undefined && obj.guid != null) {
    o.setGuid(obj.guid);
  }
  if(obj.user !== undefined && obj.user != null) {
    o.setUser(obj.user);
  }
  if(obj.code !== undefined && obj.code != null) {
    o.setCode(obj.code);
  }
  if(obj.shortcut !== undefined && obj.shortcut != null) {
    o.setShortcut(obj.shortcut);
  }
  if(obj.shortLink !== undefined && obj.shortLink != null) {
    o.setShortLink(obj.shortLink);
  }
  if(obj.keywordLink !== undefined && obj.keywordLink != null) {
    o.setKeywordLink(obj.keywordLink);
  }
  if(obj.bookmarkLink !== undefined && obj.bookmarkLink != null) {
    o.setBookmarkLink(obj.bookmarkLink);
  }
  if(obj.longUrl !== undefined && obj.longUrl != null) {
    o.setLongUrl(obj.longUrl);
  }
  if(obj.shortUrl !== undefined && obj.shortUrl != null) {
    o.setShortUrl(obj.shortUrl);
  }
  if(obj.caseSensitive !== undefined && obj.caseSensitive != null) {
    o.setCaseSensitive(obj.caseSensitive);
  }
  if(obj.status !== undefined && obj.status != null) {
    o.setStatus(obj.status);
  }
  if(obj.note !== undefined && obj.note != null) {
    o.setNote(obj.note);
  }
  if(obj.createdTime !== undefined && obj.createdTime != null) {
    o.setCreatedTime(obj.createdTime);
  }
  if(obj.modifiedTime !== undefined && obj.modifiedTime != null) {
    o.setModifiedTime(obj.modifiedTime);
  }
    
  return o;
};

cannyurl.wa.bean.SpeedDialJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = cannyurl.wa.bean.SpeedDialJsBean.create(jsonObj);
  return obj;
};
