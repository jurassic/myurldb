//////////////////////////////////////////////////////////
// <script src="/js/bean/streetaddressstructjsbean-1.0.js"></script>
// Last modified time: 1365474464838.
//////////////////////////////////////////////////////////

var cannyurl = cannyurl || {};
cannyurl.wa = cannyurl.wa || {};
cannyurl.wa.bean = cannyurl.wa.bean || {};
cannyurl.wa.bean.StreetAddressStructJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var uuid;
    var street1;
    var street2;
    var city;
    var county;
    var postalCode;
    var state;
    var province;
    var country;
    var countryName;
    var note;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getUuid = function() { return uuid; };
    this.setUuid = function(value) { uuid = value; };
    this.getStreet1 = function() { return street1; };
    this.setStreet1 = function(value) { street1 = value; };
    this.getStreet2 = function() { return street2; };
    this.setStreet2 = function(value) { street2 = value; };
    this.getCity = function() { return city; };
    this.setCity = function(value) { city = value; };
    this.getCounty = function() { return county; };
    this.setCounty = function(value) { county = value; };
    this.getPostalCode = function() { return postalCode; };
    this.setPostalCode = function(value) { postalCode = value; };
    this.getState = function() { return state; };
    this.setState = function(value) { state = value; };
    this.getProvince = function() { return province; };
    this.setProvince = function(value) { province = value; };
    this.getCountry = function() { return country; };
    this.setCountry = function(value) { country = value; };
    this.getCountryName = function() { return countryName; };
    this.setCountryName = function(value) { countryName = value; };
    this.getNote = function() { return note; };
    this.setNote = function(value) { note = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new cannyurl.wa.bean.StreetAddressStructJsBean();

      if(uuid !== undefined && uuid != null) {
        o.setUuid(uuid);
      }
      if(street1 !== undefined && street1 != null) {
        o.setStreet1(street1);
      }
      if(street2 !== undefined && street2 != null) {
        o.setStreet2(street2);
      }
      if(city !== undefined && city != null) {
        o.setCity(city);
      }
      if(county !== undefined && county != null) {
        o.setCounty(county);
      }
      if(postalCode !== undefined && postalCode != null) {
        o.setPostalCode(postalCode);
      }
      if(state !== undefined && state != null) {
        o.setState(state);
      }
      if(province !== undefined && province != null) {
        o.setProvince(province);
      }
      if(country !== undefined && country != null) {
        o.setCountry(country);
      }
      if(countryName !== undefined && countryName != null) {
        o.setCountryName(countryName);
      }
      if(note !== undefined && note != null) {
        o.setNote(note);
      }
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(uuid !== undefined && uuid != null) {
        jsonObj.uuid = uuid;
      } // Otherwise ignore...
      if(street1 !== undefined && street1 != null) {
        jsonObj.street1 = street1;
      } // Otherwise ignore...
      if(street2 !== undefined && street2 != null) {
        jsonObj.street2 = street2;
      } // Otherwise ignore...
      if(city !== undefined && city != null) {
        jsonObj.city = city;
      } // Otherwise ignore...
      if(county !== undefined && county != null) {
        jsonObj.county = county;
      } // Otherwise ignore...
      if(postalCode !== undefined && postalCode != null) {
        jsonObj.postalCode = postalCode;
      } // Otherwise ignore...
      if(state !== undefined && state != null) {
        jsonObj.state = state;
      } // Otherwise ignore...
      if(province !== undefined && province != null) {
        jsonObj.province = province;
      } // Otherwise ignore...
      if(country !== undefined && country != null) {
        jsonObj.country = country;
      } // Otherwise ignore...
      if(countryName !== undefined && countryName != null) {
        jsonObj.countryName = countryName;
      } // Otherwise ignore...
      if(note !== undefined && note != null) {
        jsonObj.note = note;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(uuid) {
        str += "\"uuid\":\"" + uuid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"uuid\":null, ";
      }
      if(street1) {
        str += "\"street1\":\"" + street1 + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"street1\":null, ";
      }
      if(street2) {
        str += "\"street2\":\"" + street2 + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"street2\":null, ";
      }
      if(city) {
        str += "\"city\":\"" + city + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"city\":null, ";
      }
      if(county) {
        str += "\"county\":\"" + county + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"county\":null, ";
      }
      if(postalCode) {
        str += "\"postalCode\":\"" + postalCode + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"postalCode\":null, ";
      }
      if(state) {
        str += "\"state\":\"" + state + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"state\":null, ";
      }
      if(province) {
        str += "\"province\":\"" + province + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"province\":null, ";
      }
      if(country) {
        str += "\"country\":\"" + country + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"country\":null, ";
      }
      if(countryName) {
        str += "\"countryName\":\"" + countryName + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"countryName\":null, ";
      }
      if(note) {
        str += "\"note\":\"" + note + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"note\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "uuid:" + uuid + ", ";
      str += "street1:" + street1 + ", ";
      str += "street2:" + street2 + ", ";
      str += "city:" + city + ", ";
      str += "county:" + county + ", ";
      str += "postalCode:" + postalCode + ", ";
      str += "state:" + state + ", ";
      str += "province:" + province + ", ";
      str += "country:" + country + ", ";
      str += "countryName:" + countryName + ", ";
      str += "note:" + note + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

cannyurl.wa.bean.StreetAddressStructJsBean.create = function(obj) {
  var o = new cannyurl.wa.bean.StreetAddressStructJsBean();

  if(obj.uuid !== undefined && obj.uuid != null) {
    o.setUuid(obj.uuid);
  }
  if(obj.street1 !== undefined && obj.street1 != null) {
    o.setStreet1(obj.street1);
  }
  if(obj.street2 !== undefined && obj.street2 != null) {
    o.setStreet2(obj.street2);
  }
  if(obj.city !== undefined && obj.city != null) {
    o.setCity(obj.city);
  }
  if(obj.county !== undefined && obj.county != null) {
    o.setCounty(obj.county);
  }
  if(obj.postalCode !== undefined && obj.postalCode != null) {
    o.setPostalCode(obj.postalCode);
  }
  if(obj.state !== undefined && obj.state != null) {
    o.setState(obj.state);
  }
  if(obj.province !== undefined && obj.province != null) {
    o.setProvince(obj.province);
  }
  if(obj.country !== undefined && obj.country != null) {
    o.setCountry(obj.country);
  }
  if(obj.countryName !== undefined && obj.countryName != null) {
    o.setCountryName(obj.countryName);
  }
  if(obj.note !== undefined && obj.note != null) {
    o.setNote(obj.note);
  }
    
  return o;
};

cannyurl.wa.bean.StreetAddressStructJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = cannyurl.wa.bean.StreetAddressStructJsBean.create(jsonObj);
  return obj;
};
