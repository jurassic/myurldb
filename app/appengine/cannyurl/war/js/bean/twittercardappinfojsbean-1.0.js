//////////////////////////////////////////////////////////
// <script src="/js/bean/twittercardappinfojsbean-1.0.js"></script>
// Last modified time: 1365474465143.
//////////////////////////////////////////////////////////

var cannyurl = cannyurl || {};
cannyurl.wa = cannyurl.wa || {};
cannyurl.wa.bean = cannyurl.wa.bean || {};
cannyurl.wa.bean.TwitterCardAppInfoJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var name;
    var id;
    var url;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getName = function() { return name; };
    this.setName = function(value) { name = value; };
    this.getId = function() { return id; };
    this.setId = function(value) { id = value; };
    this.getUrl = function() { return url; };
    this.setUrl = function(value) { url = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new cannyurl.wa.bean.TwitterCardAppInfoJsBean();

      if(name !== undefined && name != null) {
        o.setName(name);
      }
      if(id !== undefined && id != null) {
        o.setId(id);
      }
      if(url !== undefined && url != null) {
        o.setUrl(url);
      }
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(name !== undefined && name != null) {
        jsonObj.name = name;
      } // Otherwise ignore...
      if(id !== undefined && id != null) {
        jsonObj.id = id;
      } // Otherwise ignore...
      if(url !== undefined && url != null) {
        jsonObj.url = url;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(name) {
        str += "\"name\":\"" + name + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"name\":null, ";
      }
      if(id) {
        str += "\"id\":\"" + id + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"id\":null, ";
      }
      if(url) {
        str += "\"url\":\"" + url + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"url\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "name:" + name + ", ";
      str += "id:" + id + ", ";
      str += "url:" + url + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

cannyurl.wa.bean.TwitterCardAppInfoJsBean.create = function(obj) {
  var o = new cannyurl.wa.bean.TwitterCardAppInfoJsBean();

  if(obj.name !== undefined && obj.name != null) {
    o.setName(obj.name);
  }
  if(obj.id !== undefined && obj.id != null) {
    o.setId(obj.id);
  }
  if(obj.url !== undefined && obj.url != null) {
    o.setUrl(obj.url);
  }
    
  return o;
};

cannyurl.wa.bean.TwitterCardAppInfoJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = cannyurl.wa.bean.TwitterCardAppInfoJsBean.create(jsonObj);
  return obj;
};
