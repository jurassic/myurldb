//////////////////////////////////////////////////////////
// <script src="/js/bean/twittercardproductdatajsbean-1.0.js"></script>
// Last modified time: 1365474465152.
//////////////////////////////////////////////////////////

var cannyurl = cannyurl || {};
cannyurl.wa = cannyurl.wa || {};
cannyurl.wa.bean = cannyurl.wa.bean || {};
cannyurl.wa.bean.TwitterCardProductDataJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var data;
    var label;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getData = function() { return data; };
    this.setData = function(value) { data = value; };
    this.getLabel = function() { return label; };
    this.setLabel = function(value) { label = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new cannyurl.wa.bean.TwitterCardProductDataJsBean();

      if(data !== undefined && data != null) {
        o.setData(data);
      }
      if(label !== undefined && label != null) {
        o.setLabel(label);
      }
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(data !== undefined && data != null) {
        jsonObj.data = data;
      } // Otherwise ignore...
      if(label !== undefined && label != null) {
        jsonObj.label = label;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(data) {
        str += "\"data\":\"" + data + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"data\":null, ";
      }
      if(label) {
        str += "\"label\":\"" + label + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"label\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "data:" + data + ", ";
      str += "label:" + label + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

cannyurl.wa.bean.TwitterCardProductDataJsBean.create = function(obj) {
  var o = new cannyurl.wa.bean.TwitterCardProductDataJsBean();

  if(obj.data !== undefined && obj.data != null) {
    o.setData(obj.data);
  }
  if(obj.label !== undefined && obj.label != null) {
    o.setLabel(obj.label);
  }
    
  return o;
};

cannyurl.wa.bean.TwitterCardProductDataJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = cannyurl.wa.bean.TwitterCardProductDataJsBean.create(jsonObj);
  return obj;
};
