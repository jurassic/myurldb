//////////////////////////////////////////////////////////
// <script src="/js/bean/twittergallerycardjsbean-1.0.js"></script>
// Last modified time: 1365474465176.
//////////////////////////////////////////////////////////

var cannyurl = cannyurl || {};
cannyurl.wa = cannyurl.wa || {};
cannyurl.wa.bean = cannyurl.wa.bean || {};
cannyurl.wa.bean.TwitterGalleryCardJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var guid = generateUuid();
    var card;
    var url;
    var title;
    var description;
    var site;
    var siteId;
    var creator;
    var creatorId;
    var image0;
    var image1;
    var image2;
    var image3;
    var createdTime = getCurrentTime();
    var modifiedTime;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getGuid = function() { return guid; };
    this.setGuid = function(value) { guid = value; };
    this.getCard = function() { return card; };
    this.setCard = function(value) { card = value; };
    this.getUrl = function() { return url; };
    this.setUrl = function(value) { url = value; };
    this.getTitle = function() { return title; };
    this.setTitle = function(value) { title = value; };
    this.getDescription = function() { return description; };
    this.setDescription = function(value) { description = value; };
    this.getSite = function() { return site; };
    this.setSite = function(value) { site = value; };
    this.getSiteId = function() { return siteId; };
    this.setSiteId = function(value) { siteId = value; };
    this.getCreator = function() { return creator; };
    this.setCreator = function(value) { creator = value; };
    this.getCreatorId = function() { return creatorId; };
    this.setCreatorId = function(value) { creatorId = value; };
    this.getImage0 = function() { return image0; };
    this.setImage0 = function(value) { image0 = value; };
    this.getImage1 = function() { return image1; };
    this.setImage1 = function(value) { image1 = value; };
    this.getImage2 = function() { return image2; };
    this.setImage2 = function(value) { image2 = value; };
    this.getImage3 = function() { return image3; };
    this.setImage3 = function(value) { image3 = value; };
    this.getCreatedTime = function() { return createdTime; };
    this.setCreatedTime = function(value) { createdTime = value; };
    this.getModifiedTime = function() { return modifiedTime; };
    this.setModifiedTime = function(value) { modifiedTime = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new cannyurl.wa.bean.TwitterGalleryCardJsBean();

      o.setGuid(generateUuid());
      if(card !== undefined && card != null) {
        o.setCard(card);
      }
      if(url !== undefined && url != null) {
        o.setUrl(url);
      }
      if(title !== undefined && title != null) {
        o.setTitle(title);
      }
      if(description !== undefined && description != null) {
        o.setDescription(description);
      }
      if(site !== undefined && site != null) {
        o.setSite(site);
      }
      if(siteId !== undefined && siteId != null) {
        o.setSiteId(siteId);
      }
      if(creator !== undefined && creator != null) {
        o.setCreator(creator);
      }
      if(creatorId !== undefined && creatorId != null) {
        o.setCreatorId(creatorId);
      }
      if(image0 !== undefined && image0 != null) {
        o.setImage0(image0);
      }
      if(image1 !== undefined && image1 != null) {
        o.setImage1(image1);
      }
      if(image2 !== undefined && image2 != null) {
        o.setImage2(image2);
      }
      if(image3 !== undefined && image3 != null) {
        o.setImage3(image3);
      }
      o.setCreatedTime(getCurrentTime());
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(guid !== undefined && guid != null) {
        jsonObj.guid = guid;
      } // Otherwise ignore...
      if(card !== undefined && card != null) {
        jsonObj.card = card;
      } // Otherwise ignore...
      if(url !== undefined && url != null) {
        jsonObj.url = url;
      } // Otherwise ignore...
      if(title !== undefined && title != null) {
        jsonObj.title = title;
      } // Otherwise ignore...
      if(description !== undefined && description != null) {
        jsonObj.description = description;
      } // Otherwise ignore...
      if(site !== undefined && site != null) {
        jsonObj.site = site;
      } // Otherwise ignore...
      if(siteId !== undefined && siteId != null) {
        jsonObj.siteId = siteId;
      } // Otherwise ignore...
      if(creator !== undefined && creator != null) {
        jsonObj.creator = creator;
      } // Otherwise ignore...
      if(creatorId !== undefined && creatorId != null) {
        jsonObj.creatorId = creatorId;
      } // Otherwise ignore...
      if(image0 !== undefined && image0 != null) {
        jsonObj.image0 = image0;
      } // Otherwise ignore...
      if(image1 !== undefined && image1 != null) {
        jsonObj.image1 = image1;
      } // Otherwise ignore...
      if(image2 !== undefined && image2 != null) {
        jsonObj.image2 = image2;
      } // Otherwise ignore...
      if(image3 !== undefined && image3 != null) {
        jsonObj.image3 = image3;
      } // Otherwise ignore...
      if(createdTime !== undefined && createdTime != null) {
        jsonObj.createdTime = createdTime;
      } // Otherwise ignore...
      if(modifiedTime !== undefined && modifiedTime != null) {
        jsonObj.modifiedTime = modifiedTime;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(guid) {
        str += "\"guid\":\"" + guid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"guid\":null, ";
      }
      if(card) {
        str += "\"card\":\"" + card + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"card\":null, ";
      }
      if(url) {
        str += "\"url\":\"" + url + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"url\":null, ";
      }
      if(title) {
        str += "\"title\":\"" + title + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"title\":null, ";
      }
      if(description) {
        str += "\"description\":\"" + description + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"description\":null, ";
      }
      if(site) {
        str += "\"site\":\"" + site + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"site\":null, ";
      }
      if(siteId) {
        str += "\"siteId\":\"" + siteId + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"siteId\":null, ";
      }
      if(creator) {
        str += "\"creator\":\"" + creator + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"creator\":null, ";
      }
      if(creatorId) {
        str += "\"creatorId\":\"" + creatorId + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"creatorId\":null, ";
      }
      if(image0) {
        str += "\"image0\":\"" + image0 + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"image0\":null, ";
      }
      if(image1) {
        str += "\"image1\":\"" + image1 + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"image1\":null, ";
      }
      if(image2) {
        str += "\"image2\":\"" + image2 + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"image2\":null, ";
      }
      if(image3) {
        str += "\"image3\":\"" + image3 + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"image3\":null, ";
      }
      if(createdTime) {
        str += "\"createdTime\":" + createdTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"createdTime\":null, ";
      }
      if(modifiedTime) {
        str += "\"modifiedTime\":" + modifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"modifiedTime\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "guid:" + guid + ", ";
      str += "card:" + card + ", ";
      str += "url:" + url + ", ";
      str += "title:" + title + ", ";
      str += "description:" + description + ", ";
      str += "site:" + site + ", ";
      str += "siteId:" + siteId + ", ";
      str += "creator:" + creator + ", ";
      str += "creatorId:" + creatorId + ", ";
      str += "image0:" + image0 + ", ";
      str += "image1:" + image1 + ", ";
      str += "image2:" + image2 + ", ";
      str += "image3:" + image3 + ", ";
      str += "createdTime:" + createdTime + ", ";
      str += "modifiedTime:" + modifiedTime + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

cannyurl.wa.bean.TwitterGalleryCardJsBean.create = function(obj) {
  var o = new cannyurl.wa.bean.TwitterGalleryCardJsBean();

  if(obj.guid !== undefined && obj.guid != null) {
    o.setGuid(obj.guid);
  }
  if(obj.card !== undefined && obj.card != null) {
    o.setCard(obj.card);
  }
  if(obj.url !== undefined && obj.url != null) {
    o.setUrl(obj.url);
  }
  if(obj.title !== undefined && obj.title != null) {
    o.setTitle(obj.title);
  }
  if(obj.description !== undefined && obj.description != null) {
    o.setDescription(obj.description);
  }
  if(obj.site !== undefined && obj.site != null) {
    o.setSite(obj.site);
  }
  if(obj.siteId !== undefined && obj.siteId != null) {
    o.setSiteId(obj.siteId);
  }
  if(obj.creator !== undefined && obj.creator != null) {
    o.setCreator(obj.creator);
  }
  if(obj.creatorId !== undefined && obj.creatorId != null) {
    o.setCreatorId(obj.creatorId);
  }
  if(obj.image0 !== undefined && obj.image0 != null) {
    o.setImage0(obj.image0);
  }
  if(obj.image1 !== undefined && obj.image1 != null) {
    o.setImage1(obj.image1);
  }
  if(obj.image2 !== undefined && obj.image2 != null) {
    o.setImage2(obj.image2);
  }
  if(obj.image3 !== undefined && obj.image3 != null) {
    o.setImage3(obj.image3);
  }
  if(obj.createdTime !== undefined && obj.createdTime != null) {
    o.setCreatedTime(obj.createdTime);
  }
  if(obj.modifiedTime !== undefined && obj.modifiedTime != null) {
    o.setModifiedTime(obj.modifiedTime);
  }
    
  return o;
};

cannyurl.wa.bean.TwitterGalleryCardJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = cannyurl.wa.bean.TwitterGalleryCardJsBean.create(jsonObj);
  return obj;
};
