//////////////////////////////////////////////////////////
// <script src="/js/bean/twitterplayercardjsbean-1.0.js"></script>
// Last modified time: 1365474465195.
//////////////////////////////////////////////////////////

var cannyurl = cannyurl || {};
cannyurl.wa = cannyurl.wa || {};
cannyurl.wa.bean = cannyurl.wa.bean || {};
cannyurl.wa.bean.TwitterPlayerCardJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var guid = generateUuid();
    var card;
    var url;
    var title;
    var description;
    var site;
    var siteId;
    var creator;
    var creatorId;
    var image;
    var imageWidth;
    var imageHeight;
    var player;
    var playerWidth;
    var playerHeight;
    var playerStream;
    var playerStreamContentType;
    var createdTime = getCurrentTime();
    var modifiedTime;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getGuid = function() { return guid; };
    this.setGuid = function(value) { guid = value; };
    this.getCard = function() { return card; };
    this.setCard = function(value) { card = value; };
    this.getUrl = function() { return url; };
    this.setUrl = function(value) { url = value; };
    this.getTitle = function() { return title; };
    this.setTitle = function(value) { title = value; };
    this.getDescription = function() { return description; };
    this.setDescription = function(value) { description = value; };
    this.getSite = function() { return site; };
    this.setSite = function(value) { site = value; };
    this.getSiteId = function() { return siteId; };
    this.setSiteId = function(value) { siteId = value; };
    this.getCreator = function() { return creator; };
    this.setCreator = function(value) { creator = value; };
    this.getCreatorId = function() { return creatorId; };
    this.setCreatorId = function(value) { creatorId = value; };
    this.getImage = function() { return image; };
    this.setImage = function(value) { image = value; };
    this.getImageWidth = function() { return imageWidth; };
    this.setImageWidth = function(value) { imageWidth = value; };
    this.getImageHeight = function() { return imageHeight; };
    this.setImageHeight = function(value) { imageHeight = value; };
    this.getPlayer = function() { return player; };
    this.setPlayer = function(value) { player = value; };
    this.getPlayerWidth = function() { return playerWidth; };
    this.setPlayerWidth = function(value) { playerWidth = value; };
    this.getPlayerHeight = function() { return playerHeight; };
    this.setPlayerHeight = function(value) { playerHeight = value; };
    this.getPlayerStream = function() { return playerStream; };
    this.setPlayerStream = function(value) { playerStream = value; };
    this.getPlayerStreamContentType = function() { return playerStreamContentType; };
    this.setPlayerStreamContentType = function(value) { playerStreamContentType = value; };
    this.getCreatedTime = function() { return createdTime; };
    this.setCreatedTime = function(value) { createdTime = value; };
    this.getModifiedTime = function() { return modifiedTime; };
    this.setModifiedTime = function(value) { modifiedTime = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new cannyurl.wa.bean.TwitterPlayerCardJsBean();

      o.setGuid(generateUuid());
      if(card !== undefined && card != null) {
        o.setCard(card);
      }
      if(url !== undefined && url != null) {
        o.setUrl(url);
      }
      if(title !== undefined && title != null) {
        o.setTitle(title);
      }
      if(description !== undefined && description != null) {
        o.setDescription(description);
      }
      if(site !== undefined && site != null) {
        o.setSite(site);
      }
      if(siteId !== undefined && siteId != null) {
        o.setSiteId(siteId);
      }
      if(creator !== undefined && creator != null) {
        o.setCreator(creator);
      }
      if(creatorId !== undefined && creatorId != null) {
        o.setCreatorId(creatorId);
      }
      if(image !== undefined && image != null) {
        o.setImage(image);
      }
      if(imageWidth !== undefined && imageWidth != null) {
        o.setImageWidth(imageWidth);
      }
      if(imageHeight !== undefined && imageHeight != null) {
        o.setImageHeight(imageHeight);
      }
      if(player !== undefined && player != null) {
        o.setPlayer(player);
      }
      if(playerWidth !== undefined && playerWidth != null) {
        o.setPlayerWidth(playerWidth);
      }
      if(playerHeight !== undefined && playerHeight != null) {
        o.setPlayerHeight(playerHeight);
      }
      if(playerStream !== undefined && playerStream != null) {
        o.setPlayerStream(playerStream);
      }
      if(playerStreamContentType !== undefined && playerStreamContentType != null) {
        o.setPlayerStreamContentType(playerStreamContentType);
      }
      o.setCreatedTime(getCurrentTime());
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(guid !== undefined && guid != null) {
        jsonObj.guid = guid;
      } // Otherwise ignore...
      if(card !== undefined && card != null) {
        jsonObj.card = card;
      } // Otherwise ignore...
      if(url !== undefined && url != null) {
        jsonObj.url = url;
      } // Otherwise ignore...
      if(title !== undefined && title != null) {
        jsonObj.title = title;
      } // Otherwise ignore...
      if(description !== undefined && description != null) {
        jsonObj.description = description;
      } // Otherwise ignore...
      if(site !== undefined && site != null) {
        jsonObj.site = site;
      } // Otherwise ignore...
      if(siteId !== undefined && siteId != null) {
        jsonObj.siteId = siteId;
      } // Otherwise ignore...
      if(creator !== undefined && creator != null) {
        jsonObj.creator = creator;
      } // Otherwise ignore...
      if(creatorId !== undefined && creatorId != null) {
        jsonObj.creatorId = creatorId;
      } // Otherwise ignore...
      if(image !== undefined && image != null) {
        jsonObj.image = image;
      } // Otherwise ignore...
      if(imageWidth !== undefined && imageWidth != null) {
        jsonObj.imageWidth = imageWidth;
      } // Otherwise ignore...
      if(imageHeight !== undefined && imageHeight != null) {
        jsonObj.imageHeight = imageHeight;
      } // Otherwise ignore...
      if(player !== undefined && player != null) {
        jsonObj.player = player;
      } // Otherwise ignore...
      if(playerWidth !== undefined && playerWidth != null) {
        jsonObj.playerWidth = playerWidth;
      } // Otherwise ignore...
      if(playerHeight !== undefined && playerHeight != null) {
        jsonObj.playerHeight = playerHeight;
      } // Otherwise ignore...
      if(playerStream !== undefined && playerStream != null) {
        jsonObj.playerStream = playerStream;
      } // Otherwise ignore...
      if(playerStreamContentType !== undefined && playerStreamContentType != null) {
        jsonObj.playerStreamContentType = playerStreamContentType;
      } // Otherwise ignore...
      if(createdTime !== undefined && createdTime != null) {
        jsonObj.createdTime = createdTime;
      } // Otherwise ignore...
      if(modifiedTime !== undefined && modifiedTime != null) {
        jsonObj.modifiedTime = modifiedTime;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(guid) {
        str += "\"guid\":\"" + guid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"guid\":null, ";
      }
      if(card) {
        str += "\"card\":\"" + card + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"card\":null, ";
      }
      if(url) {
        str += "\"url\":\"" + url + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"url\":null, ";
      }
      if(title) {
        str += "\"title\":\"" + title + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"title\":null, ";
      }
      if(description) {
        str += "\"description\":\"" + description + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"description\":null, ";
      }
      if(site) {
        str += "\"site\":\"" + site + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"site\":null, ";
      }
      if(siteId) {
        str += "\"siteId\":\"" + siteId + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"siteId\":null, ";
      }
      if(creator) {
        str += "\"creator\":\"" + creator + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"creator\":null, ";
      }
      if(creatorId) {
        str += "\"creatorId\":\"" + creatorId + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"creatorId\":null, ";
      }
      if(image) {
        str += "\"image\":\"" + image + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"image\":null, ";
      }
      if(imageWidth) {
        str += "\"imageWidth\":" + imageWidth + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"imageWidth\":null, ";
      }
      if(imageHeight) {
        str += "\"imageHeight\":" + imageHeight + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"imageHeight\":null, ";
      }
      if(player) {
        str += "\"player\":\"" + player + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"player\":null, ";
      }
      if(playerWidth) {
        str += "\"playerWidth\":" + playerWidth + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"playerWidth\":null, ";
      }
      if(playerHeight) {
        str += "\"playerHeight\":" + playerHeight + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"playerHeight\":null, ";
      }
      if(playerStream) {
        str += "\"playerStream\":\"" + playerStream + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"playerStream\":null, ";
      }
      if(playerStreamContentType) {
        str += "\"playerStreamContentType\":\"" + playerStreamContentType + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"playerStreamContentType\":null, ";
      }
      if(createdTime) {
        str += "\"createdTime\":" + createdTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"createdTime\":null, ";
      }
      if(modifiedTime) {
        str += "\"modifiedTime\":" + modifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"modifiedTime\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "guid:" + guid + ", ";
      str += "card:" + card + ", ";
      str += "url:" + url + ", ";
      str += "title:" + title + ", ";
      str += "description:" + description + ", ";
      str += "site:" + site + ", ";
      str += "siteId:" + siteId + ", ";
      str += "creator:" + creator + ", ";
      str += "creatorId:" + creatorId + ", ";
      str += "image:" + image + ", ";
      str += "imageWidth:" + imageWidth + ", ";
      str += "imageHeight:" + imageHeight + ", ";
      str += "player:" + player + ", ";
      str += "playerWidth:" + playerWidth + ", ";
      str += "playerHeight:" + playerHeight + ", ";
      str += "playerStream:" + playerStream + ", ";
      str += "playerStreamContentType:" + playerStreamContentType + ", ";
      str += "createdTime:" + createdTime + ", ";
      str += "modifiedTime:" + modifiedTime + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

cannyurl.wa.bean.TwitterPlayerCardJsBean.create = function(obj) {
  var o = new cannyurl.wa.bean.TwitterPlayerCardJsBean();

  if(obj.guid !== undefined && obj.guid != null) {
    o.setGuid(obj.guid);
  }
  if(obj.card !== undefined && obj.card != null) {
    o.setCard(obj.card);
  }
  if(obj.url !== undefined && obj.url != null) {
    o.setUrl(obj.url);
  }
  if(obj.title !== undefined && obj.title != null) {
    o.setTitle(obj.title);
  }
  if(obj.description !== undefined && obj.description != null) {
    o.setDescription(obj.description);
  }
  if(obj.site !== undefined && obj.site != null) {
    o.setSite(obj.site);
  }
  if(obj.siteId !== undefined && obj.siteId != null) {
    o.setSiteId(obj.siteId);
  }
  if(obj.creator !== undefined && obj.creator != null) {
    o.setCreator(obj.creator);
  }
  if(obj.creatorId !== undefined && obj.creatorId != null) {
    o.setCreatorId(obj.creatorId);
  }
  if(obj.image !== undefined && obj.image != null) {
    o.setImage(obj.image);
  }
  if(obj.imageWidth !== undefined && obj.imageWidth != null) {
    o.setImageWidth(obj.imageWidth);
  }
  if(obj.imageHeight !== undefined && obj.imageHeight != null) {
    o.setImageHeight(obj.imageHeight);
  }
  if(obj.player !== undefined && obj.player != null) {
    o.setPlayer(obj.player);
  }
  if(obj.playerWidth !== undefined && obj.playerWidth != null) {
    o.setPlayerWidth(obj.playerWidth);
  }
  if(obj.playerHeight !== undefined && obj.playerHeight != null) {
    o.setPlayerHeight(obj.playerHeight);
  }
  if(obj.playerStream !== undefined && obj.playerStream != null) {
    o.setPlayerStream(obj.playerStream);
  }
  if(obj.playerStreamContentType !== undefined && obj.playerStreamContentType != null) {
    o.setPlayerStreamContentType(obj.playerStreamContentType);
  }
  if(obj.createdTime !== undefined && obj.createdTime != null) {
    o.setCreatedTime(obj.createdTime);
  }
  if(obj.modifiedTime !== undefined && obj.modifiedTime != null) {
    o.setModifiedTime(obj.modifiedTime);
  }
    
  return o;
};

cannyurl.wa.bean.TwitterPlayerCardJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = cannyurl.wa.bean.TwitterPlayerCardJsBean.create(jsonObj);
  return obj;
};
