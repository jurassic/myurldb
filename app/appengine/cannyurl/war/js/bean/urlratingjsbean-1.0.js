//////////////////////////////////////////////////////////
// <script src="/js/bean/urlratingjsbean-1.0.js"></script>
// Last modified time: 1365474465663.
//////////////////////////////////////////////////////////

var cannyurl = cannyurl || {};
cannyurl.wa = cannyurl.wa || {};
cannyurl.wa.bean = cannyurl.wa.bean || {};
cannyurl.wa.bean.UrlRatingJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var guid = generateUuid();
    var domain;
    var longUrl;
    var longUrlHash;
    var preview;
    var flag;
    var rating;
    var note;
    var ratedTime;
    var createdTime = getCurrentTime();
    var modifiedTime;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getGuid = function() { return guid; };
    this.setGuid = function(value) { guid = value; };
    this.getDomain = function() { return domain; };
    this.setDomain = function(value) { domain = value; };
    this.getLongUrl = function() { return longUrl; };
    this.setLongUrl = function(value) { longUrl = value; };
    this.getLongUrlHash = function() { return longUrlHash; };
    this.setLongUrlHash = function(value) { longUrlHash = value; };
    this.getPreview = function() { return preview; };
    this.setPreview = function(value) { preview = value; };
    this.getFlag = function() { return flag; };
    this.setFlag = function(value) { flag = value; };
    this.getRating = function() { return rating; };
    this.setRating = function(value) { rating = value; };
    this.getNote = function() { return note; };
    this.setNote = function(value) { note = value; };
    this.getRatedTime = function() { return ratedTime; };
    this.setRatedTime = function(value) { ratedTime = value; };
    this.getCreatedTime = function() { return createdTime; };
    this.setCreatedTime = function(value) { createdTime = value; };
    this.getModifiedTime = function() { return modifiedTime; };
    this.setModifiedTime = function(value) { modifiedTime = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new cannyurl.wa.bean.UrlRatingJsBean();

      o.setGuid(generateUuid());
      if(domain !== undefined && domain != null) {
        o.setDomain(domain);
      }
      if(longUrl !== undefined && longUrl != null) {
        o.setLongUrl(longUrl);
      }
      if(longUrlHash !== undefined && longUrlHash != null) {
        o.setLongUrlHash(longUrlHash);
      }
      if(preview !== undefined && preview != null) {
        o.setPreview(preview);
      }
      if(flag !== undefined && flag != null) {
        o.setFlag(flag);
      }
      if(rating !== undefined && rating != null) {
        o.setRating(rating);
      }
      if(note !== undefined && note != null) {
        o.setNote(note);
      }
      if(ratedTime !== undefined && ratedTime != null) {
        o.setRatedTime(ratedTime);
      }
      o.setCreatedTime(getCurrentTime());
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(guid !== undefined && guid != null) {
        jsonObj.guid = guid;
      } // Otherwise ignore...
      if(domain !== undefined && domain != null) {
        jsonObj.domain = domain;
      } // Otherwise ignore...
      if(longUrl !== undefined && longUrl != null) {
        jsonObj.longUrl = longUrl;
      } // Otherwise ignore...
      if(longUrlHash !== undefined && longUrlHash != null) {
        jsonObj.longUrlHash = longUrlHash;
      } // Otherwise ignore...
      if(preview !== undefined && preview != null) {
        jsonObj.preview = preview;
      } // Otherwise ignore...
      if(flag !== undefined && flag != null) {
        jsonObj.flag = flag;
      } // Otherwise ignore...
      if(rating !== undefined && rating != null) {
        jsonObj.rating = rating;
      } // Otherwise ignore...
      if(note !== undefined && note != null) {
        jsonObj.note = note;
      } // Otherwise ignore...
      if(ratedTime !== undefined && ratedTime != null) {
        jsonObj.ratedTime = ratedTime;
      } // Otherwise ignore...
      if(createdTime !== undefined && createdTime != null) {
        jsonObj.createdTime = createdTime;
      } // Otherwise ignore...
      if(modifiedTime !== undefined && modifiedTime != null) {
        jsonObj.modifiedTime = modifiedTime;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(guid) {
        str += "\"guid\":\"" + guid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"guid\":null, ";
      }
      if(domain) {
        str += "\"domain\":\"" + domain + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"domain\":null, ";
      }
      if(longUrl) {
        str += "\"longUrl\":\"" + longUrl + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"longUrl\":null, ";
      }
      if(longUrlHash) {
        str += "\"longUrlHash\":\"" + longUrlHash + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"longUrlHash\":null, ";
      }
      if(preview) {
        str += "\"preview\":\"" + preview + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"preview\":null, ";
      }
      if(flag) {
        str += "\"flag\":\"" + flag + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"flag\":null, ";
      }
      if(rating) {
        str += "\"rating\":" + rating + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"rating\":null, ";
      }
      if(note) {
        str += "\"note\":\"" + note + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"note\":null, ";
      }
      if(ratedTime) {
        str += "\"ratedTime\":" + ratedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"ratedTime\":null, ";
      }
      if(createdTime) {
        str += "\"createdTime\":" + createdTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"createdTime\":null, ";
      }
      if(modifiedTime) {
        str += "\"modifiedTime\":" + modifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"modifiedTime\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "guid:" + guid + ", ";
      str += "domain:" + domain + ", ";
      str += "longUrl:" + longUrl + ", ";
      str += "longUrlHash:" + longUrlHash + ", ";
      str += "preview:" + preview + ", ";
      str += "flag:" + flag + ", ";
      str += "rating:" + rating + ", ";
      str += "note:" + note + ", ";
      str += "ratedTime:" + ratedTime + ", ";
      str += "createdTime:" + createdTime + ", ";
      str += "modifiedTime:" + modifiedTime + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

cannyurl.wa.bean.UrlRatingJsBean.create = function(obj) {
  var o = new cannyurl.wa.bean.UrlRatingJsBean();

  if(obj.guid !== undefined && obj.guid != null) {
    o.setGuid(obj.guid);
  }
  if(obj.domain !== undefined && obj.domain != null) {
    o.setDomain(obj.domain);
  }
  if(obj.longUrl !== undefined && obj.longUrl != null) {
    o.setLongUrl(obj.longUrl);
  }
  if(obj.longUrlHash !== undefined && obj.longUrlHash != null) {
    o.setLongUrlHash(obj.longUrlHash);
  }
  if(obj.preview !== undefined && obj.preview != null) {
    o.setPreview(obj.preview);
  }
  if(obj.flag !== undefined && obj.flag != null) {
    o.setFlag(obj.flag);
  }
  if(obj.rating !== undefined && obj.rating != null) {
    o.setRating(obj.rating);
  }
  if(obj.note !== undefined && obj.note != null) {
    o.setNote(obj.note);
  }
  if(obj.ratedTime !== undefined && obj.ratedTime != null) {
    o.setRatedTime(obj.ratedTime);
  }
  if(obj.createdTime !== undefined && obj.createdTime != null) {
    o.setCreatedTime(obj.createdTime);
  }
  if(obj.modifiedTime !== undefined && obj.modifiedTime != null) {
    o.setModifiedTime(obj.modifiedTime);
  }
    
  return o;
};

cannyurl.wa.bean.UrlRatingJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = cannyurl.wa.bean.UrlRatingJsBean.create(jsonObj);
  return obj;
};
