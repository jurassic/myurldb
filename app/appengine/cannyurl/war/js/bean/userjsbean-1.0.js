//////////////////////////////////////////////////////////
// <script src="/js/bean/userjsbean-1.0.js"></script>
// Last modified time: 1365474464973.
//////////////////////////////////////////////////////////

var cannyurl = cannyurl || {};
cannyurl.wa = cannyurl.wa || {};
cannyurl.wa.bean = cannyurl.wa.bean || {};
cannyurl.wa.bean.UserJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var guid = generateUuid();
    var managerApp;
    var appAcl;
    var gaeApp;
    var aeryId;
    var sessionId;
    var ancestorGuid;
    var name;
    var usercode;
    var username;
    var nickname;
    var avatar;
    var email;
    var openId;
    var gaeUser;
    var entityType;
    var surrogate;
    var obsolete;
    var timeZone;
    var location;
    var streetAddress;
    var geoPoint;
    var ipAddress;
    var referer;
    var status;
    var emailVerifiedTime;
    var openIdVerifiedTime;
    var authenticatedTime;
    var createdTime = getCurrentTime();
    var modifiedTime;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getGuid = function() { return guid; };
    this.setGuid = function(value) { guid = value; };
    this.getManagerApp = function() { return managerApp; };
    this.setManagerApp = function(value) { managerApp = value; };
    this.getAppAcl = function() { return appAcl; };
    this.setAppAcl = function(value) { appAcl = value; };
    this.getGaeApp = function() { return gaeApp; };
    this.setGaeApp = function(value) { gaeApp = value; };
    this.getAeryId = function() { return aeryId; };
    this.setAeryId = function(value) { aeryId = value; };
    this.getSessionId = function() { return sessionId; };
    this.setSessionId = function(value) { sessionId = value; };
    this.getAncestorGuid = function() { return ancestorGuid; };
    this.setAncestorGuid = function(value) { ancestorGuid = value; };
    this.getName = function() { return name; };
    this.setName = function(value) { name = value; };
    this.getUsercode = function() { return usercode; };
    this.setUsercode = function(value) { usercode = value; };
    this.getUsername = function() { return username; };
    this.setUsername = function(value) { username = value; };
    this.getNickname = function() { return nickname; };
    this.setNickname = function(value) { nickname = value; };
    this.getAvatar = function() { return avatar; };
    this.setAvatar = function(value) { avatar = value; };
    this.getEmail = function() { return email; };
    this.setEmail = function(value) { email = value; };
    this.getOpenId = function() { return openId; };
    this.setOpenId = function(value) { openId = value; };
    this.getGaeUser = function() { return gaeUser; };
    this.setGaeUser = function(value) { gaeUser = value; };
    this.getEntityType = function() { return entityType; };
    this.setEntityType = function(value) { entityType = value; };
    this.getSurrogate = function() { return surrogate; };
    this.setSurrogate = function(value) { surrogate = value; };
    this.getObsolete = function() { return obsolete; };
    this.setObsolete = function(value) { obsolete = value; };
    this.getTimeZone = function() { return timeZone; };
    this.setTimeZone = function(value) { timeZone = value; };
    this.getLocation = function() { return location; };
    this.setLocation = function(value) { location = value; };
    this.getStreetAddress = function() { return streetAddress; };
    this.setStreetAddress = function(value) { streetAddress = value; };
    this.getGeoPoint = function() { return geoPoint; };
    this.setGeoPoint = function(value) { geoPoint = value; };
    this.getIpAddress = function() { return ipAddress; };
    this.setIpAddress = function(value) { ipAddress = value; };
    this.getReferer = function() { return referer; };
    this.setReferer = function(value) { referer = value; };
    this.getStatus = function() { return status; };
    this.setStatus = function(value) { status = value; };
    this.getEmailVerifiedTime = function() { return emailVerifiedTime; };
    this.setEmailVerifiedTime = function(value) { emailVerifiedTime = value; };
    this.getOpenIdVerifiedTime = function() { return openIdVerifiedTime; };
    this.setOpenIdVerifiedTime = function(value) { openIdVerifiedTime = value; };
    this.getAuthenticatedTime = function() { return authenticatedTime; };
    this.setAuthenticatedTime = function(value) { authenticatedTime = value; };
    this.getCreatedTime = function() { return createdTime; };
    this.setCreatedTime = function(value) { createdTime = value; };
    this.getModifiedTime = function() { return modifiedTime; };
    this.setModifiedTime = function(value) { modifiedTime = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new cannyurl.wa.bean.UserJsBean();

      o.setGuid(generateUuid());
      if(managerApp !== undefined && managerApp != null) {
        o.setManagerApp(managerApp);
      }
      if(appAcl !== undefined && appAcl != null) {
        o.setAppAcl(appAcl);
      }
      //o.setGaeApp(gaeApp.clone());
      if(gaeApp !== undefined && gaeApp != null) {
        o.setGaeApp(gaeApp);
      }
      if(aeryId !== undefined && aeryId != null) {
        o.setAeryId(aeryId);
      }
      if(sessionId !== undefined && sessionId != null) {
        o.setSessionId(sessionId);
      }
      if(ancestorGuid !== undefined && ancestorGuid != null) {
        o.setAncestorGuid(ancestorGuid);
      }
      //o.setName(name.clone());
      if(name !== undefined && name != null) {
        o.setName(name);
      }
      if(usercode !== undefined && usercode != null) {
        o.setUsercode(usercode);
      }
      if(username !== undefined && username != null) {
        o.setUsername(username);
      }
      if(nickname !== undefined && nickname != null) {
        o.setNickname(nickname);
      }
      if(avatar !== undefined && avatar != null) {
        o.setAvatar(avatar);
      }
      if(email !== undefined && email != null) {
        o.setEmail(email);
      }
      if(openId !== undefined && openId != null) {
        o.setOpenId(openId);
      }
      //o.setGaeUser(gaeUser.clone());
      if(gaeUser !== undefined && gaeUser != null) {
        o.setGaeUser(gaeUser);
      }
      if(entityType !== undefined && entityType != null) {
        o.setEntityType(entityType);
      }
      if(surrogate !== undefined && surrogate != null) {
        o.setSurrogate(surrogate);
      }
      if(obsolete !== undefined && obsolete != null) {
        o.setObsolete(obsolete);
      }
      if(timeZone !== undefined && timeZone != null) {
        o.setTimeZone(timeZone);
      }
      if(location !== undefined && location != null) {
        o.setLocation(location);
      }
      //o.setStreetAddress(streetAddress.clone());
      if(streetAddress !== undefined && streetAddress != null) {
        o.setStreetAddress(streetAddress);
      }
      //o.setGeoPoint(geoPoint.clone());
      if(geoPoint !== undefined && geoPoint != null) {
        o.setGeoPoint(geoPoint);
      }
      if(ipAddress !== undefined && ipAddress != null) {
        o.setIpAddress(ipAddress);
      }
      if(referer !== undefined && referer != null) {
        o.setReferer(referer);
      }
      if(status !== undefined && status != null) {
        o.setStatus(status);
      }
      if(emailVerifiedTime !== undefined && emailVerifiedTime != null) {
        o.setEmailVerifiedTime(emailVerifiedTime);
      }
      if(openIdVerifiedTime !== undefined && openIdVerifiedTime != null) {
        o.setOpenIdVerifiedTime(openIdVerifiedTime);
      }
      if(authenticatedTime !== undefined && authenticatedTime != null) {
        o.setAuthenticatedTime(authenticatedTime);
      }
      o.setCreatedTime(getCurrentTime());
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(guid !== undefined && guid != null) {
        jsonObj.guid = guid;
      } // Otherwise ignore...
      if(managerApp !== undefined && managerApp != null) {
        jsonObj.managerApp = managerApp;
      } // Otherwise ignore...
      if(appAcl !== undefined && appAcl != null) {
        jsonObj.appAcl = appAcl;
      } // Otherwise ignore...
      if(gaeApp !== undefined && gaeApp != null) {
        jsonObj.gaeApp = gaeApp;
      } // Otherwise ignore...
      if(aeryId !== undefined && aeryId != null) {
        jsonObj.aeryId = aeryId;
      } // Otherwise ignore...
      if(sessionId !== undefined && sessionId != null) {
        jsonObj.sessionId = sessionId;
      } // Otherwise ignore...
      if(ancestorGuid !== undefined && ancestorGuid != null) {
        jsonObj.ancestorGuid = ancestorGuid;
      } // Otherwise ignore...
      if(name !== undefined && name != null) {
        jsonObj.name = name;
      } // Otherwise ignore...
      if(usercode !== undefined && usercode != null) {
        jsonObj.usercode = usercode;
      } // Otherwise ignore...
      if(username !== undefined && username != null) {
        jsonObj.username = username;
      } // Otherwise ignore...
      if(nickname !== undefined && nickname != null) {
        jsonObj.nickname = nickname;
      } // Otherwise ignore...
      if(avatar !== undefined && avatar != null) {
        jsonObj.avatar = avatar;
      } // Otherwise ignore...
      if(email !== undefined && email != null) {
        jsonObj.email = email;
      } // Otherwise ignore...
      if(openId !== undefined && openId != null) {
        jsonObj.openId = openId;
      } // Otherwise ignore...
      if(gaeUser !== undefined && gaeUser != null) {
        jsonObj.gaeUser = gaeUser;
      } // Otherwise ignore...
      if(entityType !== undefined && entityType != null) {
        jsonObj.entityType = entityType;
      } // Otherwise ignore...
      if(surrogate !== undefined && surrogate != null) {
        jsonObj.surrogate = surrogate;
      } // Otherwise ignore...
      if(obsolete !== undefined && obsolete != null) {
        jsonObj.obsolete = obsolete;
      } // Otherwise ignore...
      if(timeZone !== undefined && timeZone != null) {
        jsonObj.timeZone = timeZone;
      } // Otherwise ignore...
      if(location !== undefined && location != null) {
        jsonObj.location = location;
      } // Otherwise ignore...
      if(streetAddress !== undefined && streetAddress != null) {
        jsonObj.streetAddress = streetAddress;
      } // Otherwise ignore...
      if(geoPoint !== undefined && geoPoint != null) {
        jsonObj.geoPoint = geoPoint;
      } // Otherwise ignore...
      if(ipAddress !== undefined && ipAddress != null) {
        jsonObj.ipAddress = ipAddress;
      } // Otherwise ignore...
      if(referer !== undefined && referer != null) {
        jsonObj.referer = referer;
      } // Otherwise ignore...
      if(status !== undefined && status != null) {
        jsonObj.status = status;
      } // Otherwise ignore...
      if(emailVerifiedTime !== undefined && emailVerifiedTime != null) {
        jsonObj.emailVerifiedTime = emailVerifiedTime;
      } // Otherwise ignore...
      if(openIdVerifiedTime !== undefined && openIdVerifiedTime != null) {
        jsonObj.openIdVerifiedTime = openIdVerifiedTime;
      } // Otherwise ignore...
      if(authenticatedTime !== undefined && authenticatedTime != null) {
        jsonObj.authenticatedTime = authenticatedTime;
      } // Otherwise ignore...
      if(createdTime !== undefined && createdTime != null) {
        jsonObj.createdTime = createdTime;
      } // Otherwise ignore...
      if(modifiedTime !== undefined && modifiedTime != null) {
        jsonObj.modifiedTime = modifiedTime;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(guid) {
        str += "\"guid\":\"" + guid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"guid\":null, ";
      }
      if(managerApp) {
        str += "\"managerApp\":\"" + managerApp + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"managerApp\":null, ";
      }
      if(appAcl) {
        str += "\"appAcl\":" + appAcl + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"appAcl\":null, ";
      }
      str += "\"gaeApp\":" + gaeApp.toJsonString() + ", ";
      if(aeryId) {
        str += "\"aeryId\":\"" + aeryId + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"aeryId\":null, ";
      }
      if(sessionId) {
        str += "\"sessionId\":\"" + sessionId + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"sessionId\":null, ";
      }
      if(ancestorGuid) {
        str += "\"ancestorGuid\":\"" + ancestorGuid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"ancestorGuid\":null, ";
      }
      str += "\"name\":" + name.toJsonString() + ", ";
      if(usercode) {
        str += "\"usercode\":\"" + usercode + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"usercode\":null, ";
      }
      if(username) {
        str += "\"username\":\"" + username + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"username\":null, ";
      }
      if(nickname) {
        str += "\"nickname\":\"" + nickname + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"nickname\":null, ";
      }
      if(avatar) {
        str += "\"avatar\":\"" + avatar + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"avatar\":null, ";
      }
      if(email) {
        str += "\"email\":\"" + email + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"email\":null, ";
      }
      if(openId) {
        str += "\"openId\":\"" + openId + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"openId\":null, ";
      }
      str += "\"gaeUser\":" + gaeUser.toJsonString() + ", ";
      if(entityType) {
        str += "\"entityType\":\"" + entityType + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"entityType\":null, ";
      }
      if(surrogate) {
        str += "\"surrogate\":" + surrogate + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"surrogate\":null, ";
      }
      if(obsolete) {
        str += "\"obsolete\":" + obsolete + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"obsolete\":null, ";
      }
      if(timeZone) {
        str += "\"timeZone\":\"" + timeZone + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"timeZone\":null, ";
      }
      if(location) {
        str += "\"location\":\"" + location + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"location\":null, ";
      }
      str += "\"streetAddress\":" + streetAddress.toJsonString() + ", ";
      str += "\"geoPoint\":" + geoPoint.toJsonString() + ", ";
      if(ipAddress) {
        str += "\"ipAddress\":\"" + ipAddress + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"ipAddress\":null, ";
      }
      if(referer) {
        str += "\"referer\":\"" + referer + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"referer\":null, ";
      }
      if(status) {
        str += "\"status\":\"" + status + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"status\":null, ";
      }
      if(emailVerifiedTime) {
        str += "\"emailVerifiedTime\":" + emailVerifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"emailVerifiedTime\":null, ";
      }
      if(openIdVerifiedTime) {
        str += "\"openIdVerifiedTime\":" + openIdVerifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"openIdVerifiedTime\":null, ";
      }
      if(authenticatedTime) {
        str += "\"authenticatedTime\":" + authenticatedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"authenticatedTime\":null, ";
      }
      if(createdTime) {
        str += "\"createdTime\":" + createdTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"createdTime\":null, ";
      }
      if(modifiedTime) {
        str += "\"modifiedTime\":" + modifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"modifiedTime\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "guid:" + guid + ", ";
      str += "managerApp:" + managerApp + ", ";
      str += "appAcl:" + appAcl + ", ";
      str += "gaeApp:" + gaeApp + ", ";
      str += "aeryId:" + aeryId + ", ";
      str += "sessionId:" + sessionId + ", ";
      str += "ancestorGuid:" + ancestorGuid + ", ";
      str += "name:" + name + ", ";
      str += "usercode:" + usercode + ", ";
      str += "username:" + username + ", ";
      str += "nickname:" + nickname + ", ";
      str += "avatar:" + avatar + ", ";
      str += "email:" + email + ", ";
      str += "openId:" + openId + ", ";
      str += "gaeUser:" + gaeUser + ", ";
      str += "entityType:" + entityType + ", ";
      str += "surrogate:" + surrogate + ", ";
      str += "obsolete:" + obsolete + ", ";
      str += "timeZone:" + timeZone + ", ";
      str += "location:" + location + ", ";
      str += "streetAddress:" + streetAddress + ", ";
      str += "geoPoint:" + geoPoint + ", ";
      str += "ipAddress:" + ipAddress + ", ";
      str += "referer:" + referer + ", ";
      str += "status:" + status + ", ";
      str += "emailVerifiedTime:" + emailVerifiedTime + ", ";
      str += "openIdVerifiedTime:" + openIdVerifiedTime + ", ";
      str += "authenticatedTime:" + authenticatedTime + ", ";
      str += "createdTime:" + createdTime + ", ";
      str += "modifiedTime:" + modifiedTime + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

cannyurl.wa.bean.UserJsBean.create = function(obj) {
  var o = new cannyurl.wa.bean.UserJsBean();

  if(obj.guid !== undefined && obj.guid != null) {
    o.setGuid(obj.guid);
  }
  if(obj.managerApp !== undefined && obj.managerApp != null) {
    o.setManagerApp(obj.managerApp);
  }
  if(obj.appAcl !== undefined && obj.appAcl != null) {
    o.setAppAcl(obj.appAcl);
  }
  if(obj.gaeApp !== undefined && obj.gaeApp != null) {
    o.setGaeApp(obj.gaeApp);
  }
  if(obj.aeryId !== undefined && obj.aeryId != null) {
    o.setAeryId(obj.aeryId);
  }
  if(obj.sessionId !== undefined && obj.sessionId != null) {
    o.setSessionId(obj.sessionId);
  }
  if(obj.ancestorGuid !== undefined && obj.ancestorGuid != null) {
    o.setAncestorGuid(obj.ancestorGuid);
  }
  if(obj.name !== undefined && obj.name != null) {
    o.setName(obj.name);
  }
  if(obj.usercode !== undefined && obj.usercode != null) {
    o.setUsercode(obj.usercode);
  }
  if(obj.username !== undefined && obj.username != null) {
    o.setUsername(obj.username);
  }
  if(obj.nickname !== undefined && obj.nickname != null) {
    o.setNickname(obj.nickname);
  }
  if(obj.avatar !== undefined && obj.avatar != null) {
    o.setAvatar(obj.avatar);
  }
  if(obj.email !== undefined && obj.email != null) {
    o.setEmail(obj.email);
  }
  if(obj.openId !== undefined && obj.openId != null) {
    o.setOpenId(obj.openId);
  }
  if(obj.gaeUser !== undefined && obj.gaeUser != null) {
    o.setGaeUser(obj.gaeUser);
  }
  if(obj.entityType !== undefined && obj.entityType != null) {
    o.setEntityType(obj.entityType);
  }
  if(obj.surrogate !== undefined && obj.surrogate != null) {
    o.setSurrogate(obj.surrogate);
  }
  if(obj.obsolete !== undefined && obj.obsolete != null) {
    o.setObsolete(obj.obsolete);
  }
  if(obj.timeZone !== undefined && obj.timeZone != null) {
    o.setTimeZone(obj.timeZone);
  }
  if(obj.location !== undefined && obj.location != null) {
    o.setLocation(obj.location);
  }
  if(obj.streetAddress !== undefined && obj.streetAddress != null) {
    o.setStreetAddress(obj.streetAddress);
  }
  if(obj.geoPoint !== undefined && obj.geoPoint != null) {
    o.setGeoPoint(obj.geoPoint);
  }
  if(obj.ipAddress !== undefined && obj.ipAddress != null) {
    o.setIpAddress(obj.ipAddress);
  }
  if(obj.referer !== undefined && obj.referer != null) {
    o.setReferer(obj.referer);
  }
  if(obj.status !== undefined && obj.status != null) {
    o.setStatus(obj.status);
  }
  if(obj.emailVerifiedTime !== undefined && obj.emailVerifiedTime != null) {
    o.setEmailVerifiedTime(obj.emailVerifiedTime);
  }
  if(obj.openIdVerifiedTime !== undefined && obj.openIdVerifiedTime != null) {
    o.setOpenIdVerifiedTime(obj.openIdVerifiedTime);
  }
  if(obj.authenticatedTime !== undefined && obj.authenticatedTime != null) {
    o.setAuthenticatedTime(obj.authenticatedTime);
  }
  if(obj.createdTime !== undefined && obj.createdTime != null) {
    o.setCreatedTime(obj.createdTime);
  }
  if(obj.modifiedTime !== undefined && obj.modifiedTime != null) {
    o.setModifiedTime(obj.modifiedTime);
  }
    
  return o;
};

cannyurl.wa.bean.UserJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = cannyurl.wa.bean.UserJsBean.create(jsonObj);
  return obj;
};
