//////////////////////////////////////////////////////////
// <script src="/js/bean/userpasswordjsbean-1.0.js"></script>
// Last modified time: 1365474465000.
//////////////////////////////////////////////////////////

var cannyurl = cannyurl || {};
cannyurl.wa = cannyurl.wa || {};
cannyurl.wa.bean = cannyurl.wa.bean || {};
cannyurl.wa.bean.UserPasswordJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var guid = generateUuid();
    var admin;
    var user;
    var username;
    var email;
    var openId;
    var password;
    var resetRequired;
    var challengeQuestion;
    var challengeAnswer;
    var status;
    var lastResetTime;
    var expirationTime;
    var createdTime = getCurrentTime();
    var modifiedTime;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getGuid = function() { return guid; };
    this.setGuid = function(value) { guid = value; };
    this.getAdmin = function() { return admin; };
    this.setAdmin = function(value) { admin = value; };
    this.getUser = function() { return user; };
    this.setUser = function(value) { user = value; };
    this.getUsername = function() { return username; };
    this.setUsername = function(value) { username = value; };
    this.getEmail = function() { return email; };
    this.setEmail = function(value) { email = value; };
    this.getOpenId = function() { return openId; };
    this.setOpenId = function(value) { openId = value; };
    this.getPassword = function() { return password; };
    this.setPassword = function(value) { password = value; };
    this.getResetRequired = function() { return resetRequired; };
    this.setResetRequired = function(value) { resetRequired = value; };
    this.getChallengeQuestion = function() { return challengeQuestion; };
    this.setChallengeQuestion = function(value) { challengeQuestion = value; };
    this.getChallengeAnswer = function() { return challengeAnswer; };
    this.setChallengeAnswer = function(value) { challengeAnswer = value; };
    this.getStatus = function() { return status; };
    this.setStatus = function(value) { status = value; };
    this.getLastResetTime = function() { return lastResetTime; };
    this.setLastResetTime = function(value) { lastResetTime = value; };
    this.getExpirationTime = function() { return expirationTime; };
    this.setExpirationTime = function(value) { expirationTime = value; };
    this.getCreatedTime = function() { return createdTime; };
    this.setCreatedTime = function(value) { createdTime = value; };
    this.getModifiedTime = function() { return modifiedTime; };
    this.setModifiedTime = function(value) { modifiedTime = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new cannyurl.wa.bean.UserPasswordJsBean();

      o.setGuid(generateUuid());
      if(admin !== undefined && admin != null) {
        o.setAdmin(admin);
      }
      if(user !== undefined && user != null) {
        o.setUser(user);
      }
      if(username !== undefined && username != null) {
        o.setUsername(username);
      }
      if(email !== undefined && email != null) {
        o.setEmail(email);
      }
      if(openId !== undefined && openId != null) {
        o.setOpenId(openId);
      }
      //o.setPassword(password.clone());
      if(password !== undefined && password != null) {
        o.setPassword(password);
      }
      if(resetRequired !== undefined && resetRequired != null) {
        o.setResetRequired(resetRequired);
      }
      if(challengeQuestion !== undefined && challengeQuestion != null) {
        o.setChallengeQuestion(challengeQuestion);
      }
      if(challengeAnswer !== undefined && challengeAnswer != null) {
        o.setChallengeAnswer(challengeAnswer);
      }
      if(status !== undefined && status != null) {
        o.setStatus(status);
      }
      if(lastResetTime !== undefined && lastResetTime != null) {
        o.setLastResetTime(lastResetTime);
      }
      if(expirationTime !== undefined && expirationTime != null) {
        o.setExpirationTime(expirationTime);
      }
      o.setCreatedTime(getCurrentTime());
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(guid !== undefined && guid != null) {
        jsonObj.guid = guid;
      } // Otherwise ignore...
      if(admin !== undefined && admin != null) {
        jsonObj.admin = admin;
      } // Otherwise ignore...
      if(user !== undefined && user != null) {
        jsonObj.user = user;
      } // Otherwise ignore...
      if(username !== undefined && username != null) {
        jsonObj.username = username;
      } // Otherwise ignore...
      if(email !== undefined && email != null) {
        jsonObj.email = email;
      } // Otherwise ignore...
      if(openId !== undefined && openId != null) {
        jsonObj.openId = openId;
      } // Otherwise ignore...
      if(password !== undefined && password != null) {
        jsonObj.password = password;
      } // Otherwise ignore...
      if(resetRequired !== undefined && resetRequired != null) {
        jsonObj.resetRequired = resetRequired;
      } // Otherwise ignore...
      if(challengeQuestion !== undefined && challengeQuestion != null) {
        jsonObj.challengeQuestion = challengeQuestion;
      } // Otherwise ignore...
      if(challengeAnswer !== undefined && challengeAnswer != null) {
        jsonObj.challengeAnswer = challengeAnswer;
      } // Otherwise ignore...
      if(status !== undefined && status != null) {
        jsonObj.status = status;
      } // Otherwise ignore...
      if(lastResetTime !== undefined && lastResetTime != null) {
        jsonObj.lastResetTime = lastResetTime;
      } // Otherwise ignore...
      if(expirationTime !== undefined && expirationTime != null) {
        jsonObj.expirationTime = expirationTime;
      } // Otherwise ignore...
      if(createdTime !== undefined && createdTime != null) {
        jsonObj.createdTime = createdTime;
      } // Otherwise ignore...
      if(modifiedTime !== undefined && modifiedTime != null) {
        jsonObj.modifiedTime = modifiedTime;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(guid) {
        str += "\"guid\":\"" + guid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"guid\":null, ";
      }
      if(admin) {
        str += "\"admin\":\"" + admin + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"admin\":null, ";
      }
      if(user) {
        str += "\"user\":\"" + user + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"user\":null, ";
      }
      if(username) {
        str += "\"username\":\"" + username + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"username\":null, ";
      }
      if(email) {
        str += "\"email\":\"" + email + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"email\":null, ";
      }
      if(openId) {
        str += "\"openId\":\"" + openId + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"openId\":null, ";
      }
      str += "\"password\":" + password.toJsonString() + ", ";
      if(resetRequired) {
        str += "\"resetRequired\":" + resetRequired + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"resetRequired\":null, ";
      }
      if(challengeQuestion) {
        str += "\"challengeQuestion\":\"" + challengeQuestion + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"challengeQuestion\":null, ";
      }
      if(challengeAnswer) {
        str += "\"challengeAnswer\":\"" + challengeAnswer + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"challengeAnswer\":null, ";
      }
      if(status) {
        str += "\"status\":\"" + status + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"status\":null, ";
      }
      if(lastResetTime) {
        str += "\"lastResetTime\":" + lastResetTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"lastResetTime\":null, ";
      }
      if(expirationTime) {
        str += "\"expirationTime\":" + expirationTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"expirationTime\":null, ";
      }
      if(createdTime) {
        str += "\"createdTime\":" + createdTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"createdTime\":null, ";
      }
      if(modifiedTime) {
        str += "\"modifiedTime\":" + modifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"modifiedTime\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "guid:" + guid + ", ";
      str += "admin:" + admin + ", ";
      str += "user:" + user + ", ";
      str += "username:" + username + ", ";
      str += "email:" + email + ", ";
      str += "openId:" + openId + ", ";
      str += "password:" + password + ", ";
      str += "resetRequired:" + resetRequired + ", ";
      str += "challengeQuestion:" + challengeQuestion + ", ";
      str += "challengeAnswer:" + challengeAnswer + ", ";
      str += "status:" + status + ", ";
      str += "lastResetTime:" + lastResetTime + ", ";
      str += "expirationTime:" + expirationTime + ", ";
      str += "createdTime:" + createdTime + ", ";
      str += "modifiedTime:" + modifiedTime + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

cannyurl.wa.bean.UserPasswordJsBean.create = function(obj) {
  var o = new cannyurl.wa.bean.UserPasswordJsBean();

  if(obj.guid !== undefined && obj.guid != null) {
    o.setGuid(obj.guid);
  }
  if(obj.admin !== undefined && obj.admin != null) {
    o.setAdmin(obj.admin);
  }
  if(obj.user !== undefined && obj.user != null) {
    o.setUser(obj.user);
  }
  if(obj.username !== undefined && obj.username != null) {
    o.setUsername(obj.username);
  }
  if(obj.email !== undefined && obj.email != null) {
    o.setEmail(obj.email);
  }
  if(obj.openId !== undefined && obj.openId != null) {
    o.setOpenId(obj.openId);
  }
  if(obj.password !== undefined && obj.password != null) {
    o.setPassword(obj.password);
  }
  if(obj.resetRequired !== undefined && obj.resetRequired != null) {
    o.setResetRequired(obj.resetRequired);
  }
  if(obj.challengeQuestion !== undefined && obj.challengeQuestion != null) {
    o.setChallengeQuestion(obj.challengeQuestion);
  }
  if(obj.challengeAnswer !== undefined && obj.challengeAnswer != null) {
    o.setChallengeAnswer(obj.challengeAnswer);
  }
  if(obj.status !== undefined && obj.status != null) {
    o.setStatus(obj.status);
  }
  if(obj.lastResetTime !== undefined && obj.lastResetTime != null) {
    o.setLastResetTime(obj.lastResetTime);
  }
  if(obj.expirationTime !== undefined && obj.expirationTime != null) {
    o.setExpirationTime(obj.expirationTime);
  }
  if(obj.createdTime !== undefined && obj.createdTime != null) {
    o.setCreatedTime(obj.createdTime);
  }
  if(obj.modifiedTime !== undefined && obj.modifiedTime != null) {
    o.setModifiedTime(obj.modifiedTime);
  }
    
  return o;
};

cannyurl.wa.bean.UserPasswordJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = cannyurl.wa.bean.UserPasswordJsBean.create(jsonObj);
  return obj;
};
