//////////////////////////////////////////////////////////
// <script src="/js/bean/userresourcepermissionjsbean-1.0.js"></script>
// Last modified time: 1365474465061.
//////////////////////////////////////////////////////////

var cannyurl = cannyurl || {};
cannyurl.wa = cannyurl.wa || {};
cannyurl.wa.bean = cannyurl.wa.bean || {};
cannyurl.wa.bean.UserResourcePermissionJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var guid = generateUuid();
    var user;
    var permissionName;
    var resource;
    var instance;
    var action;
    var permitted;
    var status;
    var createdTime = getCurrentTime();
    var modifiedTime;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getGuid = function() { return guid; };
    this.setGuid = function(value) { guid = value; };
    this.getUser = function() { return user; };
    this.setUser = function(value) { user = value; };
    this.getPermissionName = function() { return permissionName; };
    this.setPermissionName = function(value) { permissionName = value; };
    this.getResource = function() { return resource; };
    this.setResource = function(value) { resource = value; };
    this.getInstance = function() { return instance; };
    this.setInstance = function(value) { instance = value; };
    this.getAction = function() { return action; };
    this.setAction = function(value) { action = value; };
    this.getPermitted = function() { return permitted; };
    this.setPermitted = function(value) { permitted = value; };
    this.getStatus = function() { return status; };
    this.setStatus = function(value) { status = value; };
    this.getCreatedTime = function() { return createdTime; };
    this.setCreatedTime = function(value) { createdTime = value; };
    this.getModifiedTime = function() { return modifiedTime; };
    this.setModifiedTime = function(value) { modifiedTime = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new cannyurl.wa.bean.UserResourcePermissionJsBean();

      o.setGuid(generateUuid());
      if(user !== undefined && user != null) {
        o.setUser(user);
      }
      if(permissionName !== undefined && permissionName != null) {
        o.setPermissionName(permissionName);
      }
      if(resource !== undefined && resource != null) {
        o.setResource(resource);
      }
      if(instance !== undefined && instance != null) {
        o.setInstance(instance);
      }
      if(action !== undefined && action != null) {
        o.setAction(action);
      }
      if(permitted !== undefined && permitted != null) {
        o.setPermitted(permitted);
      }
      if(status !== undefined && status != null) {
        o.setStatus(status);
      }
      o.setCreatedTime(getCurrentTime());
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(guid !== undefined && guid != null) {
        jsonObj.guid = guid;
      } // Otherwise ignore...
      if(user !== undefined && user != null) {
        jsonObj.user = user;
      } // Otherwise ignore...
      if(permissionName !== undefined && permissionName != null) {
        jsonObj.permissionName = permissionName;
      } // Otherwise ignore...
      if(resource !== undefined && resource != null) {
        jsonObj.resource = resource;
      } // Otherwise ignore...
      if(instance !== undefined && instance != null) {
        jsonObj.instance = instance;
      } // Otherwise ignore...
      if(action !== undefined && action != null) {
        jsonObj.action = action;
      } // Otherwise ignore...
      if(permitted !== undefined && permitted != null) {
        jsonObj.permitted = permitted;
      } // Otherwise ignore...
      if(status !== undefined && status != null) {
        jsonObj.status = status;
      } // Otherwise ignore...
      if(createdTime !== undefined && createdTime != null) {
        jsonObj.createdTime = createdTime;
      } // Otherwise ignore...
      if(modifiedTime !== undefined && modifiedTime != null) {
        jsonObj.modifiedTime = modifiedTime;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(guid) {
        str += "\"guid\":\"" + guid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"guid\":null, ";
      }
      if(user) {
        str += "\"user\":\"" + user + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"user\":null, ";
      }
      if(permissionName) {
        str += "\"permissionName\":\"" + permissionName + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"permissionName\":null, ";
      }
      if(resource) {
        str += "\"resource\":\"" + resource + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"resource\":null, ";
      }
      if(instance) {
        str += "\"instance\":\"" + instance + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"instance\":null, ";
      }
      if(action) {
        str += "\"action\":\"" + action + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"action\":null, ";
      }
      if(permitted) {
        str += "\"permitted\":" + permitted + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"permitted\":null, ";
      }
      if(status) {
        str += "\"status\":\"" + status + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"status\":null, ";
      }
      if(createdTime) {
        str += "\"createdTime\":" + createdTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"createdTime\":null, ";
      }
      if(modifiedTime) {
        str += "\"modifiedTime\":" + modifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"modifiedTime\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "guid:" + guid + ", ";
      str += "user:" + user + ", ";
      str += "permissionName:" + permissionName + ", ";
      str += "resource:" + resource + ", ";
      str += "instance:" + instance + ", ";
      str += "action:" + action + ", ";
      str += "permitted:" + permitted + ", ";
      str += "status:" + status + ", ";
      str += "createdTime:" + createdTime + ", ";
      str += "modifiedTime:" + modifiedTime + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

cannyurl.wa.bean.UserResourcePermissionJsBean.create = function(obj) {
  var o = new cannyurl.wa.bean.UserResourcePermissionJsBean();

  if(obj.guid !== undefined && obj.guid != null) {
    o.setGuid(obj.guid);
  }
  if(obj.user !== undefined && obj.user != null) {
    o.setUser(obj.user);
  }
  if(obj.permissionName !== undefined && obj.permissionName != null) {
    o.setPermissionName(obj.permissionName);
  }
  if(obj.resource !== undefined && obj.resource != null) {
    o.setResource(obj.resource);
  }
  if(obj.instance !== undefined && obj.instance != null) {
    o.setInstance(obj.instance);
  }
  if(obj.action !== undefined && obj.action != null) {
    o.setAction(obj.action);
  }
  if(obj.permitted !== undefined && obj.permitted != null) {
    o.setPermitted(obj.permitted);
  }
  if(obj.status !== undefined && obj.status != null) {
    o.setStatus(obj.status);
  }
  if(obj.createdTime !== undefined && obj.createdTime != null) {
    o.setCreatedTime(obj.createdTime);
  }
  if(obj.modifiedTime !== undefined && obj.modifiedTime != null) {
    o.setModifiedTime(obj.modifiedTime);
  }
    
  return o;
};

cannyurl.wa.bean.UserResourcePermissionJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = cannyurl.wa.bean.UserResourcePermissionJsBean.create(jsonObj);
  return obj;
};
