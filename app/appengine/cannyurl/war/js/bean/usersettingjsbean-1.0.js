//////////////////////////////////////////////////////////
// <script src="/js/bean/usersettingjsbean-1.0.js"></script>
// Last modified time: 1365474465124.
//////////////////////////////////////////////////////////

var cannyurl = cannyurl || {};
cannyurl.wa = cannyurl.wa || {};
cannyurl.wa.bean = cannyurl.wa.bean || {};
cannyurl.wa.bean.UserSettingJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var guid = generateUuid();
    var user;
    var homePage;
    var selfBio;
    var userUrlDomain;
    var userUrlDomainNormalized;
    var autoRedirectEnabled;
    var viewEnabled;
    var shareEnabled;
    var defaultDomain;
    var defaultTokenType;
    var defaultRedirectType;
    var defaultFlashDuration;
    var defaultAccessType;
    var defaultViewType;
    var defaultShareType;
    var defaultPassphrase;
    var defaultSignature;
    var useSignature;
    var defaultEmblem;
    var useEmblem;
    var defaultBackground;
    var useBackground;
    var sponsorUrl;
    var sponsorBanner;
    var sponsorNote;
    var useSponsor;
    var extraParams;
    var expirationDuration;
    var createdTime = getCurrentTime();
    var modifiedTime;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getGuid = function() { return guid; };
    this.setGuid = function(value) { guid = value; };
    this.getUser = function() { return user; };
    this.setUser = function(value) { user = value; };
    this.getHomePage = function() { return homePage; };
    this.setHomePage = function(value) { homePage = value; };
    this.getSelfBio = function() { return selfBio; };
    this.setSelfBio = function(value) { selfBio = value; };
    this.getUserUrlDomain = function() { return userUrlDomain; };
    this.setUserUrlDomain = function(value) { userUrlDomain = value; };
    this.getUserUrlDomainNormalized = function() { return userUrlDomainNormalized; };
    this.setUserUrlDomainNormalized = function(value) { userUrlDomainNormalized = value; };
    this.getAutoRedirectEnabled = function() { return autoRedirectEnabled; };
    this.setAutoRedirectEnabled = function(value) { autoRedirectEnabled = value; };
    this.getViewEnabled = function() { return viewEnabled; };
    this.setViewEnabled = function(value) { viewEnabled = value; };
    this.getShareEnabled = function() { return shareEnabled; };
    this.setShareEnabled = function(value) { shareEnabled = value; };
    this.getDefaultDomain = function() { return defaultDomain; };
    this.setDefaultDomain = function(value) { defaultDomain = value; };
    this.getDefaultTokenType = function() { return defaultTokenType; };
    this.setDefaultTokenType = function(value) { defaultTokenType = value; };
    this.getDefaultRedirectType = function() { return defaultRedirectType; };
    this.setDefaultRedirectType = function(value) { defaultRedirectType = value; };
    this.getDefaultFlashDuration = function() { return defaultFlashDuration; };
    this.setDefaultFlashDuration = function(value) { defaultFlashDuration = value; };
    this.getDefaultAccessType = function() { return defaultAccessType; };
    this.setDefaultAccessType = function(value) { defaultAccessType = value; };
    this.getDefaultViewType = function() { return defaultViewType; };
    this.setDefaultViewType = function(value) { defaultViewType = value; };
    this.getDefaultShareType = function() { return defaultShareType; };
    this.setDefaultShareType = function(value) { defaultShareType = value; };
    this.getDefaultPassphrase = function() { return defaultPassphrase; };
    this.setDefaultPassphrase = function(value) { defaultPassphrase = value; };
    this.getDefaultSignature = function() { return defaultSignature; };
    this.setDefaultSignature = function(value) { defaultSignature = value; };
    this.getUseSignature = function() { return useSignature; };
    this.setUseSignature = function(value) { useSignature = value; };
    this.getDefaultEmblem = function() { return defaultEmblem; };
    this.setDefaultEmblem = function(value) { defaultEmblem = value; };
    this.getUseEmblem = function() { return useEmblem; };
    this.setUseEmblem = function(value) { useEmblem = value; };
    this.getDefaultBackground = function() { return defaultBackground; };
    this.setDefaultBackground = function(value) { defaultBackground = value; };
    this.getUseBackground = function() { return useBackground; };
    this.setUseBackground = function(value) { useBackground = value; };
    this.getSponsorUrl = function() { return sponsorUrl; };
    this.setSponsorUrl = function(value) { sponsorUrl = value; };
    this.getSponsorBanner = function() { return sponsorBanner; };
    this.setSponsorBanner = function(value) { sponsorBanner = value; };
    this.getSponsorNote = function() { return sponsorNote; };
    this.setSponsorNote = function(value) { sponsorNote = value; };
    this.getUseSponsor = function() { return useSponsor; };
    this.setUseSponsor = function(value) { useSponsor = value; };
    this.getExtraParams = function() { return extraParams; };
    this.setExtraParams = function(value) { extraParams = value; };
    this.getExpirationDuration = function() { return expirationDuration; };
    this.setExpirationDuration = function(value) { expirationDuration = value; };
    this.getCreatedTime = function() { return createdTime; };
    this.setCreatedTime = function(value) { createdTime = value; };
    this.getModifiedTime = function() { return modifiedTime; };
    this.setModifiedTime = function(value) { modifiedTime = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new cannyurl.wa.bean.UserSettingJsBean();

      o.setGuid(generateUuid());
      if(user !== undefined && user != null) {
        o.setUser(user);
      }
      if(homePage !== undefined && homePage != null) {
        o.setHomePage(homePage);
      }
      if(selfBio !== undefined && selfBio != null) {
        o.setSelfBio(selfBio);
      }
      if(userUrlDomain !== undefined && userUrlDomain != null) {
        o.setUserUrlDomain(userUrlDomain);
      }
      if(userUrlDomainNormalized !== undefined && userUrlDomainNormalized != null) {
        o.setUserUrlDomainNormalized(userUrlDomainNormalized);
      }
      if(autoRedirectEnabled !== undefined && autoRedirectEnabled != null) {
        o.setAutoRedirectEnabled(autoRedirectEnabled);
      }
      if(viewEnabled !== undefined && viewEnabled != null) {
        o.setViewEnabled(viewEnabled);
      }
      if(shareEnabled !== undefined && shareEnabled != null) {
        o.setShareEnabled(shareEnabled);
      }
      if(defaultDomain !== undefined && defaultDomain != null) {
        o.setDefaultDomain(defaultDomain);
      }
      if(defaultTokenType !== undefined && defaultTokenType != null) {
        o.setDefaultTokenType(defaultTokenType);
      }
      if(defaultRedirectType !== undefined && defaultRedirectType != null) {
        o.setDefaultRedirectType(defaultRedirectType);
      }
      if(defaultFlashDuration !== undefined && defaultFlashDuration != null) {
        o.setDefaultFlashDuration(defaultFlashDuration);
      }
      if(defaultAccessType !== undefined && defaultAccessType != null) {
        o.setDefaultAccessType(defaultAccessType);
      }
      if(defaultViewType !== undefined && defaultViewType != null) {
        o.setDefaultViewType(defaultViewType);
      }
      if(defaultShareType !== undefined && defaultShareType != null) {
        o.setDefaultShareType(defaultShareType);
      }
      if(defaultPassphrase !== undefined && defaultPassphrase != null) {
        o.setDefaultPassphrase(defaultPassphrase);
      }
      if(defaultSignature !== undefined && defaultSignature != null) {
        o.setDefaultSignature(defaultSignature);
      }
      if(useSignature !== undefined && useSignature != null) {
        o.setUseSignature(useSignature);
      }
      if(defaultEmblem !== undefined && defaultEmblem != null) {
        o.setDefaultEmblem(defaultEmblem);
      }
      if(useEmblem !== undefined && useEmblem != null) {
        o.setUseEmblem(useEmblem);
      }
      if(defaultBackground !== undefined && defaultBackground != null) {
        o.setDefaultBackground(defaultBackground);
      }
      if(useBackground !== undefined && useBackground != null) {
        o.setUseBackground(useBackground);
      }
      if(sponsorUrl !== undefined && sponsorUrl != null) {
        o.setSponsorUrl(sponsorUrl);
      }
      if(sponsorBanner !== undefined && sponsorBanner != null) {
        o.setSponsorBanner(sponsorBanner);
      }
      if(sponsorNote !== undefined && sponsorNote != null) {
        o.setSponsorNote(sponsorNote);
      }
      if(useSponsor !== undefined && useSponsor != null) {
        o.setUseSponsor(useSponsor);
      }
      if(extraParams !== undefined && extraParams != null) {
        o.setExtraParams(extraParams);
      }
      if(expirationDuration !== undefined && expirationDuration != null) {
        o.setExpirationDuration(expirationDuration);
      }
      o.setCreatedTime(getCurrentTime());
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(guid !== undefined && guid != null) {
        jsonObj.guid = guid;
      } // Otherwise ignore...
      if(user !== undefined && user != null) {
        jsonObj.user = user;
      } // Otherwise ignore...
      if(homePage !== undefined && homePage != null) {
        jsonObj.homePage = homePage;
      } // Otherwise ignore...
      if(selfBio !== undefined && selfBio != null) {
        jsonObj.selfBio = selfBio;
      } // Otherwise ignore...
      if(userUrlDomain !== undefined && userUrlDomain != null) {
        jsonObj.userUrlDomain = userUrlDomain;
      } // Otherwise ignore...
      if(userUrlDomainNormalized !== undefined && userUrlDomainNormalized != null) {
        jsonObj.userUrlDomainNormalized = userUrlDomainNormalized;
      } // Otherwise ignore...
      if(autoRedirectEnabled !== undefined && autoRedirectEnabled != null) {
        jsonObj.autoRedirectEnabled = autoRedirectEnabled;
      } // Otherwise ignore...
      if(viewEnabled !== undefined && viewEnabled != null) {
        jsonObj.viewEnabled = viewEnabled;
      } // Otherwise ignore...
      if(shareEnabled !== undefined && shareEnabled != null) {
        jsonObj.shareEnabled = shareEnabled;
      } // Otherwise ignore...
      if(defaultDomain !== undefined && defaultDomain != null) {
        jsonObj.defaultDomain = defaultDomain;
      } // Otherwise ignore...
      if(defaultTokenType !== undefined && defaultTokenType != null) {
        jsonObj.defaultTokenType = defaultTokenType;
      } // Otherwise ignore...
      if(defaultRedirectType !== undefined && defaultRedirectType != null) {
        jsonObj.defaultRedirectType = defaultRedirectType;
      } // Otherwise ignore...
      if(defaultFlashDuration !== undefined && defaultFlashDuration != null) {
        jsonObj.defaultFlashDuration = defaultFlashDuration;
      } // Otherwise ignore...
      if(defaultAccessType !== undefined && defaultAccessType != null) {
        jsonObj.defaultAccessType = defaultAccessType;
      } // Otherwise ignore...
      if(defaultViewType !== undefined && defaultViewType != null) {
        jsonObj.defaultViewType = defaultViewType;
      } // Otherwise ignore...
      if(defaultShareType !== undefined && defaultShareType != null) {
        jsonObj.defaultShareType = defaultShareType;
      } // Otherwise ignore...
      if(defaultPassphrase !== undefined && defaultPassphrase != null) {
        jsonObj.defaultPassphrase = defaultPassphrase;
      } // Otherwise ignore...
      if(defaultSignature !== undefined && defaultSignature != null) {
        jsonObj.defaultSignature = defaultSignature;
      } // Otherwise ignore...
      if(useSignature !== undefined && useSignature != null) {
        jsonObj.useSignature = useSignature;
      } // Otherwise ignore...
      if(defaultEmblem !== undefined && defaultEmblem != null) {
        jsonObj.defaultEmblem = defaultEmblem;
      } // Otherwise ignore...
      if(useEmblem !== undefined && useEmblem != null) {
        jsonObj.useEmblem = useEmblem;
      } // Otherwise ignore...
      if(defaultBackground !== undefined && defaultBackground != null) {
        jsonObj.defaultBackground = defaultBackground;
      } // Otherwise ignore...
      if(useBackground !== undefined && useBackground != null) {
        jsonObj.useBackground = useBackground;
      } // Otherwise ignore...
      if(sponsorUrl !== undefined && sponsorUrl != null) {
        jsonObj.sponsorUrl = sponsorUrl;
      } // Otherwise ignore...
      if(sponsorBanner !== undefined && sponsorBanner != null) {
        jsonObj.sponsorBanner = sponsorBanner;
      } // Otherwise ignore...
      if(sponsorNote !== undefined && sponsorNote != null) {
        jsonObj.sponsorNote = sponsorNote;
      } // Otherwise ignore...
      if(useSponsor !== undefined && useSponsor != null) {
        jsonObj.useSponsor = useSponsor;
      } // Otherwise ignore...
      if(extraParams !== undefined && extraParams != null) {
        jsonObj.extraParams = extraParams;
      } // Otherwise ignore...
      if(expirationDuration !== undefined && expirationDuration != null) {
        jsonObj.expirationDuration = expirationDuration;
      } // Otherwise ignore...
      if(createdTime !== undefined && createdTime != null) {
        jsonObj.createdTime = createdTime;
      } // Otherwise ignore...
      if(modifiedTime !== undefined && modifiedTime != null) {
        jsonObj.modifiedTime = modifiedTime;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(guid) {
        str += "\"guid\":\"" + guid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"guid\":null, ";
      }
      if(user) {
        str += "\"user\":\"" + user + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"user\":null, ";
      }
      if(homePage) {
        str += "\"homePage\":\"" + homePage + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"homePage\":null, ";
      }
      if(selfBio) {
        str += "\"selfBio\":\"" + selfBio + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"selfBio\":null, ";
      }
      if(userUrlDomain) {
        str += "\"userUrlDomain\":\"" + userUrlDomain + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"userUrlDomain\":null, ";
      }
      if(userUrlDomainNormalized) {
        str += "\"userUrlDomainNormalized\":\"" + userUrlDomainNormalized + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"userUrlDomainNormalized\":null, ";
      }
      if(autoRedirectEnabled) {
        str += "\"autoRedirectEnabled\":" + autoRedirectEnabled + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"autoRedirectEnabled\":null, ";
      }
      if(viewEnabled) {
        str += "\"viewEnabled\":" + viewEnabled + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"viewEnabled\":null, ";
      }
      if(shareEnabled) {
        str += "\"shareEnabled\":" + shareEnabled + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"shareEnabled\":null, ";
      }
      if(defaultDomain) {
        str += "\"defaultDomain\":\"" + defaultDomain + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"defaultDomain\":null, ";
      }
      if(defaultTokenType) {
        str += "\"defaultTokenType\":\"" + defaultTokenType + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"defaultTokenType\":null, ";
      }
      if(defaultRedirectType) {
        str += "\"defaultRedirectType\":\"" + defaultRedirectType + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"defaultRedirectType\":null, ";
      }
      if(defaultFlashDuration) {
        str += "\"defaultFlashDuration\":" + defaultFlashDuration + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"defaultFlashDuration\":null, ";
      }
      if(defaultAccessType) {
        str += "\"defaultAccessType\":\"" + defaultAccessType + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"defaultAccessType\":null, ";
      }
      if(defaultViewType) {
        str += "\"defaultViewType\":\"" + defaultViewType + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"defaultViewType\":null, ";
      }
      if(defaultShareType) {
        str += "\"defaultShareType\":\"" + defaultShareType + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"defaultShareType\":null, ";
      }
      if(defaultPassphrase) {
        str += "\"defaultPassphrase\":\"" + defaultPassphrase + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"defaultPassphrase\":null, ";
      }
      if(defaultSignature) {
        str += "\"defaultSignature\":\"" + defaultSignature + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"defaultSignature\":null, ";
      }
      if(useSignature) {
        str += "\"useSignature\":" + useSignature + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"useSignature\":null, ";
      }
      if(defaultEmblem) {
        str += "\"defaultEmblem\":\"" + defaultEmblem + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"defaultEmblem\":null, ";
      }
      if(useEmblem) {
        str += "\"useEmblem\":" + useEmblem + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"useEmblem\":null, ";
      }
      if(defaultBackground) {
        str += "\"defaultBackground\":\"" + defaultBackground + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"defaultBackground\":null, ";
      }
      if(useBackground) {
        str += "\"useBackground\":" + useBackground + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"useBackground\":null, ";
      }
      if(sponsorUrl) {
        str += "\"sponsorUrl\":\"" + sponsorUrl + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"sponsorUrl\":null, ";
      }
      if(sponsorBanner) {
        str += "\"sponsorBanner\":\"" + sponsorBanner + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"sponsorBanner\":null, ";
      }
      if(sponsorNote) {
        str += "\"sponsorNote\":\"" + sponsorNote + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"sponsorNote\":null, ";
      }
      if(useSponsor) {
        str += "\"useSponsor\":" + useSponsor + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"useSponsor\":null, ";
      }
      if(extraParams) {
        str += "\"extraParams\":\"" + extraParams + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"extraParams\":null, ";
      }
      if(expirationDuration) {
        str += "\"expirationDuration\":" + expirationDuration + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"expirationDuration\":null, ";
      }
      if(createdTime) {
        str += "\"createdTime\":" + createdTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"createdTime\":null, ";
      }
      if(modifiedTime) {
        str += "\"modifiedTime\":" + modifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"modifiedTime\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "guid:" + guid + ", ";
      str += "user:" + user + ", ";
      str += "homePage:" + homePage + ", ";
      str += "selfBio:" + selfBio + ", ";
      str += "userUrlDomain:" + userUrlDomain + ", ";
      str += "userUrlDomainNormalized:" + userUrlDomainNormalized + ", ";
      str += "autoRedirectEnabled:" + autoRedirectEnabled + ", ";
      str += "viewEnabled:" + viewEnabled + ", ";
      str += "shareEnabled:" + shareEnabled + ", ";
      str += "defaultDomain:" + defaultDomain + ", ";
      str += "defaultTokenType:" + defaultTokenType + ", ";
      str += "defaultRedirectType:" + defaultRedirectType + ", ";
      str += "defaultFlashDuration:" + defaultFlashDuration + ", ";
      str += "defaultAccessType:" + defaultAccessType + ", ";
      str += "defaultViewType:" + defaultViewType + ", ";
      str += "defaultShareType:" + defaultShareType + ", ";
      str += "defaultPassphrase:" + defaultPassphrase + ", ";
      str += "defaultSignature:" + defaultSignature + ", ";
      str += "useSignature:" + useSignature + ", ";
      str += "defaultEmblem:" + defaultEmblem + ", ";
      str += "useEmblem:" + useEmblem + ", ";
      str += "defaultBackground:" + defaultBackground + ", ";
      str += "useBackground:" + useBackground + ", ";
      str += "sponsorUrl:" + sponsorUrl + ", ";
      str += "sponsorBanner:" + sponsorBanner + ", ";
      str += "sponsorNote:" + sponsorNote + ", ";
      str += "useSponsor:" + useSponsor + ", ";
      str += "extraParams:" + extraParams + ", ";
      str += "expirationDuration:" + expirationDuration + ", ";
      str += "createdTime:" + createdTime + ", ";
      str += "modifiedTime:" + modifiedTime + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

cannyurl.wa.bean.UserSettingJsBean.create = function(obj) {
  var o = new cannyurl.wa.bean.UserSettingJsBean();

  if(obj.guid !== undefined && obj.guid != null) {
    o.setGuid(obj.guid);
  }
  if(obj.user !== undefined && obj.user != null) {
    o.setUser(obj.user);
  }
  if(obj.homePage !== undefined && obj.homePage != null) {
    o.setHomePage(obj.homePage);
  }
  if(obj.selfBio !== undefined && obj.selfBio != null) {
    o.setSelfBio(obj.selfBio);
  }
  if(obj.userUrlDomain !== undefined && obj.userUrlDomain != null) {
    o.setUserUrlDomain(obj.userUrlDomain);
  }
  if(obj.userUrlDomainNormalized !== undefined && obj.userUrlDomainNormalized != null) {
    o.setUserUrlDomainNormalized(obj.userUrlDomainNormalized);
  }
  if(obj.autoRedirectEnabled !== undefined && obj.autoRedirectEnabled != null) {
    o.setAutoRedirectEnabled(obj.autoRedirectEnabled);
  }
  if(obj.viewEnabled !== undefined && obj.viewEnabled != null) {
    o.setViewEnabled(obj.viewEnabled);
  }
  if(obj.shareEnabled !== undefined && obj.shareEnabled != null) {
    o.setShareEnabled(obj.shareEnabled);
  }
  if(obj.defaultDomain !== undefined && obj.defaultDomain != null) {
    o.setDefaultDomain(obj.defaultDomain);
  }
  if(obj.defaultTokenType !== undefined && obj.defaultTokenType != null) {
    o.setDefaultTokenType(obj.defaultTokenType);
  }
  if(obj.defaultRedirectType !== undefined && obj.defaultRedirectType != null) {
    o.setDefaultRedirectType(obj.defaultRedirectType);
  }
  if(obj.defaultFlashDuration !== undefined && obj.defaultFlashDuration != null) {
    o.setDefaultFlashDuration(obj.defaultFlashDuration);
  }
  if(obj.defaultAccessType !== undefined && obj.defaultAccessType != null) {
    o.setDefaultAccessType(obj.defaultAccessType);
  }
  if(obj.defaultViewType !== undefined && obj.defaultViewType != null) {
    o.setDefaultViewType(obj.defaultViewType);
  }
  if(obj.defaultShareType !== undefined && obj.defaultShareType != null) {
    o.setDefaultShareType(obj.defaultShareType);
  }
  if(obj.defaultPassphrase !== undefined && obj.defaultPassphrase != null) {
    o.setDefaultPassphrase(obj.defaultPassphrase);
  }
  if(obj.defaultSignature !== undefined && obj.defaultSignature != null) {
    o.setDefaultSignature(obj.defaultSignature);
  }
  if(obj.useSignature !== undefined && obj.useSignature != null) {
    o.setUseSignature(obj.useSignature);
  }
  if(obj.defaultEmblem !== undefined && obj.defaultEmblem != null) {
    o.setDefaultEmblem(obj.defaultEmblem);
  }
  if(obj.useEmblem !== undefined && obj.useEmblem != null) {
    o.setUseEmblem(obj.useEmblem);
  }
  if(obj.defaultBackground !== undefined && obj.defaultBackground != null) {
    o.setDefaultBackground(obj.defaultBackground);
  }
  if(obj.useBackground !== undefined && obj.useBackground != null) {
    o.setUseBackground(obj.useBackground);
  }
  if(obj.sponsorUrl !== undefined && obj.sponsorUrl != null) {
    o.setSponsorUrl(obj.sponsorUrl);
  }
  if(obj.sponsorBanner !== undefined && obj.sponsorBanner != null) {
    o.setSponsorBanner(obj.sponsorBanner);
  }
  if(obj.sponsorNote !== undefined && obj.sponsorNote != null) {
    o.setSponsorNote(obj.sponsorNote);
  }
  if(obj.useSponsor !== undefined && obj.useSponsor != null) {
    o.setUseSponsor(obj.useSponsor);
  }
  if(obj.extraParams !== undefined && obj.extraParams != null) {
    o.setExtraParams(obj.extraParams);
  }
  if(obj.expirationDuration !== undefined && obj.expirationDuration != null) {
    o.setExpirationDuration(obj.expirationDuration);
  }
  if(obj.createdTime !== undefined && obj.createdTime != null) {
    o.setCreatedTime(obj.createdTime);
  }
  if(obj.modifiedTime !== undefined && obj.modifiedTime != null) {
    o.setModifiedTime(obj.modifiedTime);
  }
    
  return o;
};

cannyurl.wa.bean.UserSettingJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = cannyurl.wa.bean.UserSettingJsBean.create(jsonObj);
  return obj;
};
