//////////////////////////////////////////////////////////
// <script src="/js/bean/visitorsettingjsbean-1.0.js"></script>
// Last modified time: 1365474465136.
//////////////////////////////////////////////////////////

var cannyurl = cannyurl || {};
cannyurl.wa = cannyurl.wa || {};
cannyurl.wa.bean = cannyurl.wa.bean || {};
cannyurl.wa.bean.VisitorSettingJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var guid = generateUuid();
    var user;
    var redirectType;
    var flashDuration;
    var privacyType;
    var createdTime = getCurrentTime();
    var modifiedTime;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getGuid = function() { return guid; };
    this.setGuid = function(value) { guid = value; };
    this.getUser = function() { return user; };
    this.setUser = function(value) { user = value; };
    this.getRedirectType = function() { return redirectType; };
    this.setRedirectType = function(value) { redirectType = value; };
    this.getFlashDuration = function() { return flashDuration; };
    this.setFlashDuration = function(value) { flashDuration = value; };
    this.getPrivacyType = function() { return privacyType; };
    this.setPrivacyType = function(value) { privacyType = value; };
    this.getCreatedTime = function() { return createdTime; };
    this.setCreatedTime = function(value) { createdTime = value; };
    this.getModifiedTime = function() { return modifiedTime; };
    this.setModifiedTime = function(value) { modifiedTime = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new cannyurl.wa.bean.VisitorSettingJsBean();

      o.setGuid(generateUuid());
      if(user !== undefined && user != null) {
        o.setUser(user);
      }
      if(redirectType !== undefined && redirectType != null) {
        o.setRedirectType(redirectType);
      }
      if(flashDuration !== undefined && flashDuration != null) {
        o.setFlashDuration(flashDuration);
      }
      if(privacyType !== undefined && privacyType != null) {
        o.setPrivacyType(privacyType);
      }
      o.setCreatedTime(getCurrentTime());
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(guid !== undefined && guid != null) {
        jsonObj.guid = guid;
      } // Otherwise ignore...
      if(user !== undefined && user != null) {
        jsonObj.user = user;
      } // Otherwise ignore...
      if(redirectType !== undefined && redirectType != null) {
        jsonObj.redirectType = redirectType;
      } // Otherwise ignore...
      if(flashDuration !== undefined && flashDuration != null) {
        jsonObj.flashDuration = flashDuration;
      } // Otherwise ignore...
      if(privacyType !== undefined && privacyType != null) {
        jsonObj.privacyType = privacyType;
      } // Otherwise ignore...
      if(createdTime !== undefined && createdTime != null) {
        jsonObj.createdTime = createdTime;
      } // Otherwise ignore...
      if(modifiedTime !== undefined && modifiedTime != null) {
        jsonObj.modifiedTime = modifiedTime;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(guid) {
        str += "\"guid\":\"" + guid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"guid\":null, ";
      }
      if(user) {
        str += "\"user\":\"" + user + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"user\":null, ";
      }
      if(redirectType) {
        str += "\"redirectType\":\"" + redirectType + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"redirectType\":null, ";
      }
      if(flashDuration) {
        str += "\"flashDuration\":" + flashDuration + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"flashDuration\":null, ";
      }
      if(privacyType) {
        str += "\"privacyType\":\"" + privacyType + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"privacyType\":null, ";
      }
      if(createdTime) {
        str += "\"createdTime\":" + createdTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"createdTime\":null, ";
      }
      if(modifiedTime) {
        str += "\"modifiedTime\":" + modifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"modifiedTime\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "guid:" + guid + ", ";
      str += "user:" + user + ", ";
      str += "redirectType:" + redirectType + ", ";
      str += "flashDuration:" + flashDuration + ", ";
      str += "privacyType:" + privacyType + ", ";
      str += "createdTime:" + createdTime + ", ";
      str += "modifiedTime:" + modifiedTime + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

cannyurl.wa.bean.VisitorSettingJsBean.create = function(obj) {
  var o = new cannyurl.wa.bean.VisitorSettingJsBean();

  if(obj.guid !== undefined && obj.guid != null) {
    o.setGuid(obj.guid);
  }
  if(obj.user !== undefined && obj.user != null) {
    o.setUser(obj.user);
  }
  if(obj.redirectType !== undefined && obj.redirectType != null) {
    o.setRedirectType(obj.redirectType);
  }
  if(obj.flashDuration !== undefined && obj.flashDuration != null) {
    o.setFlashDuration(obj.flashDuration);
  }
  if(obj.privacyType !== undefined && obj.privacyType != null) {
    o.setPrivacyType(obj.privacyType);
  }
  if(obj.createdTime !== undefined && obj.createdTime != null) {
    o.setCreatedTime(obj.createdTime);
  }
  if(obj.modifiedTime !== undefined && obj.modifiedTime != null) {
    o.setModifiedTime(obj.modifiedTime);
  }
    
  return o;
};

cannyurl.wa.bean.VisitorSettingJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = cannyurl.wa.bean.VisitorSettingJsBean.create(jsonObj);
  return obj;
};
