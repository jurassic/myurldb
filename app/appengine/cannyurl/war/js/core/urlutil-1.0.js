/*
URL-related utility functions.
*/

// temporary
// Err on the safer side, which is returning false.
// Returns true only if the arg is clearly invalid....
// Return value, false, does not mean the URL is valid...
// (TBD: Use regex ???)
function isUrlInvalid(url) {
    var isInvalid = false;
    if(! url) {
        isInvalid = true;
    } else {
        var trimmed = url.replace(/^\s+|\s+$/g, '');
        var len = trimmed.length;
        if(len < 11) {    // 11? 12? ???
            isInvalid = true;
        } else {
            if(trimmed.indexOf("http://") !== 0 && trimmed.indexOf("https://") !== 0) {
                isInvalid = true;
            } else {
                // TBD: Check domain part?
                // TBD: Check if the url contains invalid characters (e.g., space)
                // etc.. ???
            }
        }
    }
    return isInvalid;
}
