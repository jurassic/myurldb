/*
For war/fragment/PageFooter.jsp
*/

    // "Init"
    $(function() {
    	$("#app_info_dialog").hide();
    	$("#app_help_dialog").hide();
    	$("#app_about_dialog").hide();
    	$("#app_powered_dialog").hide();
    	$("#app_faqs_dialog").hide();
    });

    // Event handlers
    $(function() {
	    $("#app_info_anchor").click(function() {
		    if(DEBUG_ENABLED) console.log("Info anchor clicked.");
		    
			$( "#app_info_dialog" ).dialog({
				height: 350,
				width: 450,
				modal: true
			});
		    
		    return false;
	    });
	    $("#app_help_anchor").click(function() {
		    if(DEBUG_ENABLED) console.log("Help anchor clicked.");
		    
			$( "#app_help_dialog" ).dialog({
				height: 350,
				width: 450,
				modal: true
			});
		    
		    return false;
	    });
	    $("#app_about_anchor").click(function() {
		    if(DEBUG_ENABLED) console.log("About anchor clicked.");
		    
			$( "#app_about_dialog" ).dialog({
				height: 250,
				width: 450,
				modal: true
			});
		    
		    return false;
	    });
	    $("#app_powered_anchor").click(function() {
		    if(DEBUG_ENABLED) console.log("Powered anchor clicked.");
		    
			$( "#app_powered_dialog" ).dialog({
				height: 320,
				width: 450,
				modal: true
			});
		    
		    return false;
	    });
	    $("#app_faqs_anchor").click(function() {
		    if(DEBUG_ENABLED) console.log("FAQs anchor clicked.");
		    
			$( "#app_faqs_dialog" ).dialog({
				height: 350,
				width: 450,
				modal: true
			});
		    
		    return false;
	    });
    });
