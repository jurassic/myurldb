/*
Email through MailMor.
*/


//$(function() {
    var $emailDialog = $('<div id="emailhelper_email_dialog" title="Send Email Dialog" style="display: none;"><div id="emailhelper_email_main" style="width:100%;"><form id="emailhelper_email_form">' 
    		+ '<div id="emailhelper_email_head" style="width:100%;padding:4px 3px;"><span id="emailhelper_email_subject" style="font-weight:bold;font-size:1.2em;">Subject</span></div>' 
    		+ '<div id="emailhelper_email_body1" style="width:100%;padding:4px 3px;"><label for="emailhelper_email_sender_name" style="font-size:0.9em;">Your Name (optional):</label><input id="emailhelper_email_sender_name" type="text" style="width:400px;"></input></div>' 
    		+ '<div id="emailhelper_email_body2" style="width:100%;padding:4px 3px;"><label for="emailhelper_email_recipient_email" style="font-size:0.9em;">Recipient Email Address:</label><input id="emailhelper_email_recipient_email" type="email" style="width:400px;"></input></div>' 
    		+ '<div id="emailhelper_email_body3" style="width:100%;padding:4px 3px;"><label for="emailhelper_email_message" style="font-size:0.9em;">Message:</label><textarea id="emailhelper_email_message" cols="40" rows="8" style="width:400px;"></textarea>' 
    		+ '<input type="hidden" id="emailhelper_email_sender_email"></input><input type="hidden" id="emailhelper_email_default_sender_name"></input></div>'
            + '</form></div></div>');
    $('body').append($emailDialog);
//});


// EmailHelper object.
var webstoa = webstoa || {};
webstoa.EmailHelper = ( function() {

  var onEmailSuccess = function(result) {
      if(DEBUG_ENABLED) console.log("onEmailSuccess(): result = " + result);
      //updateStatus('Your message has been successfully emailed', 'info', 5550);
  	  $( "#emailhelper_email_dialog" ).dialog('close');
  };

  var cls = function(senderEmail, subject, defaultMessage, defaultSenderName) {
      this.senderEmail = senderEmail ? senderEmail : '';
      this.subject = subject ? subject : '';
      this.defaultMessage = defaultMessage ? defaultMessage : '';
      this.defaultSenderName = defaultSenderName ? defaultSenderName : '';

      this.email = function() {
    	if(DEBUG_ENABLED) console.log("Email() called.");

    	$('#emailhelper_email_sender_email').val(this.senderEmail);
    	$('#emailhelper_email_default_sender_name').val(this.defaultSenderName);
    	$('#emailhelper_email_subject').text(this.subject);
    	// ...    	
    	
    	// Reset user input values?
        $('#emailhelper_email_sender_name').val('');
        $('#emailhelper_email_recipient_email').val('');
        $('#emailhelper_email_message').val(this.defaultMessage);

	    $( "#emailhelper_email_dialog" ).dialog({
            height: 450,
            width: 460,
            modal: true,
            buttons: {
                Send: function() {
                    // ... 
                	var senderEmail = $('#emailhelper_email_sender_email').val();
                	var defaultSenderName = $('#emailhelper_email_default_sender_name').val();
                	var senderName = $('#emailhelper_email_sender_name').val().trim();
                	var recipientEmail = $('#emailhelper_email_recipient_email').val().trim();
                	var subject = $('#emailhelper_email_subject').text();
                	var message = $('#emailhelper_email_message').val().trim();
                	if(DEBUG_ENABLED) console.log("Email: senderName = " + senderName + "; recipientEmail = " + recipientEmail + "; message = " + message);
                	//var messageLen = message.length;
                	//if(messageLen > 500) {  // Max 500 chars
                	//	message = message.substring(0, 500);
                    //	if(DEBUG_ENABLED) console.log("Comment has been truncated to: " + message);
                	//}
                	
                	if(!recipientEmail) {  // Or, invalid
                		// TBD:
                		// Error...
                		window.alert("Please specify email address.");
                		return; // ???
                	}

                	var emailObj = {};
                	var sender = senderEmail;
                	if(senderName) {
                		sender += ", " + senderName;
                	} else if(defaultSenderName) {
                		sender += ", " + defaultSenderName;
                	}
                	emailObj.sender = sender;
                	var recipient = recipientEmail;
                	var toRecipients = [];
                	toRecipients[0] = recipient;
                	emailObj.toRecipients = toRecipients;
                	if(senderName) {
                		subject += " - Message from " + senderName;
                	}
                	emailObj.subject = subject;
                	//if(senderName) {
                	//	message += "-" + senderName + "\n";
                	//}
                	emailObj.body = message;
                	emailObj.deferred = false;
                	//emailObj.deferred = true;
                	//emailObj.scheduledTime = (new Date()).getTime() + 60 * 1000;
                	
                    var referrerInfo = {};
                    referrerInfo.referer = document.referrer;
                    referrerInfo.userAgent = navigator.userAgent;
                    referrerInfo.language = navigator.language;
                    //referrerInfo.hostname = "";
                    //referrerInfo.ipAddress = "";
                    emailObj.referrerInfo = referrerInfo;
                	
                	// temporary
                	//var mailMorUrl = 'http://localhost:8899/wp/mailmessage';
                	var mailMorUrl = 'http://beta.mailmor.com/wp/mailmessage';
                	var payload = JSON.stringify(emailObj);
                	var payloadParam = '?payload=' + payload
                	mailMorUrl += payloadParam;
                	$.ajax({
                  	    type: 'GET',
                  	    url: mailMorUrl,
                  	    dataType: "jsonp",
                  	    success: function(data, textStatus) {
                  	      // TBD
                  	      if(DEBUG_ENABLED) console.log("Email successfully sent. textStatus = " + textStatus);
                  	      if(DEBUG_ENABLED) console.log("data = " + data);
                  	      if(DEBUG_ENABLED) console.log(data);

            	          //updateStatus('Thanks for signing up.', 'info', 5550);

                  	      // TBD:
                  		  window.alert("Email successfully sent.");
                  	    },
                  	    error: function(req, textStatus) {
                   	      if(DEBUG_ENABLED) console.log("Failed to send email. textStatus = " + textStatus);

              	          //updateStatus('Failed to sign up: status = ' + textStatus, 'error', 5550);

                  	      // TBD:
                  		  window.alert("Emailing failed. Please try again.");
                  	    }
              	   	  });

                      // Close the dialog....
                      $(this).dialog('close');
                },
                Cancel: function() {
                    $(this).dialog('close'); 
                }
            }
        });    	
    };
  };

  return cls;
})();

