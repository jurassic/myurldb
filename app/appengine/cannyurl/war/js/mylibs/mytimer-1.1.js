/*
Simple countdown timer.
*/

var mytimer = mytimer || {};
mytimer.Timer = ( function() {

  var getCurrentTime = function() {
	return (new Date()).getTime();
  }

  var cls = function(total, delta) {
      var totalMillis = total || 10000;   // ??? 10 seconds
	  var deltaMillis = delta || 1000;   // 1 second interval by default.
//	  if(total) {
//		  totalMillis = total;
//	  } else {
//		  // ???
//		  totalMillis = 10000;  // 10 seconds
//	  }
//	  if(delta) {
//		  deltaMillis = delta;
//	  } else {
//		  deltaMillis = 1000;   // 1 second by default
//	  }

	  var remainingMillis;
	  var countdown;        // timer.
	  
      // TBD
	  var counterTextElement;   // jquery object
	  var hideTargetElement;    // jquery object
	  this.setCounterTextElement = function(jqObj) {
		  counterTextElement = jqObj;
	  }
	  this.setHideTargetElement = function(jqObj) {
		  hideTargetElement = jqObj;
	  }
	  
	  // TBD
	  var deltaAction;         // function pointer.
	  var deltaActionParam;
	  var countdownAction;     // function pointer.
	  var countdownActionParam;
	  this.setDeltaAction = function(ftn, p) {
		  deltaAction = ftn;
		  deltaActionParam = p;
	  }
	  this.setCountdownAction = function(ftn, p) {
		  countdownAction = ftn;
		  countdownActionParam = p;
	  }
    
      // progress bar..
      var progressBar;
      this.setProgressBar = function(pbar) {
          progressBar = pbar;  // ???
      };
      this.getProgressBar = function() {
          return progressBar;
      };

	  
	  this.getRemainingMillis = function() {
		  return remainingMillis || 0;
	  }
	  this.getRemainingSeconds = function() {
		  var secs = 0;
		  if(remainingMillis && remainingMillis > 0) {
			  secs = Math.floor(remainingMillis / 1000);
		  }
		  return secs;
	  }

      this.start = function() {
    	  remainingMillis = totalMillis;

          if(progressBar) {
              // progressBar.start(true);
              progressBar.start(true);
          }
          
    	  var that = this;
    	  countdown = setInterval(function() {
    		  remainingMillis -= deltaMillis;

    		  if(counterTextElement) {
        		  counterTextElement.text(that.getRemainingSeconds());    			  
    		  }
    		  if(deltaAction) {
    			  deltaAction(deltaActionParam);
    		  }
    		  if (remainingMillis <= 0) {
    			  if(hideTargetElement) {
    				  hideTargetElement.fadeOut('fast');
    			  }
        		  if(countdownAction) {
        			  countdownAction(countdownActionParam);
        		  }
        		  clearInterval(countdown);
    		  } 
    	  }, deltaMillis);
      };

      this.stop = function() {
    	  if(countdown) {
    		  clearInterval(countdown);
        	  // delete countdown;   // ???
    	  }
          if(progressBar) {
              // progressBar.reset();
              progressBar.pause();
          }
      };

      // TBD:
      this.pause = function() {
      };
      this.resume = function() {
      };
      
  };

  return cls;
})();

