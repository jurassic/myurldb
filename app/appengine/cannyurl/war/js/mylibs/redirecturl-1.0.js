/*
Short URL helper routines.
*/

var cannyurl = cannyurl || {};
cannyurl.redirecturl = cannyurl.redirecturl || {};
cannyurl.redirecturl.RedirectUrlHelper = ( function() {

  var PATH_VERIFY = "v";
  var PATH_INFO = "i";
  var PATH_CONFIRM = "c";
  var PATH_FLASH = "f";
  var PATH_PERMANENT = "p";
  var PATH_TEMPORARY = "t";

  var cls = function(shortLink) {
    //var shortLinkBean;
	var domain;
	var token;   // token == "real token" + (optional) query string.
    if(shortLink) {
    	//shortLinkBean = shortLink;
    	domain = shortLink.getDomain();
    	token = shortLink.getToken();
    } else {
    	// error ???
    }

    // Note: This is a duplication of Java code: app.util.ShortLinkUtil.parseFullUrl()
    // TBD: We should probably use ajax implementation relying on the same server side code.
    // For now, keep this in sync with the server side implementation.
    // TBD: How to do the error handling???
    // TBD: Query string???
    //      Currently, parsed "token" includes the query string ????
    // .....
    this.parseShortUrl = function(shortUrl) {
    	if(DEBUG_ENABLED) console.log("parseShortUrl() called with shortUrl = " + shortUrl);
    	
    	if(!shortUrl) {
    		return;
    	}
    	var length = shortUrl.length;
    	if(length < 10) {
    		return;
    	}
    	var idx1 = shortUrl.indexOf("/", 8);
    	if(idx1 < 0 || idx1 >= length - 2) {
    		return;
    	}

        var prefix = shortUrl.charAt(idx1+1);
        if(prefix == '!' || prefix == '@' || prefix == '~') {
            var idxU = shortUrl.indexOf("/", idx1+2);
            if(idxU < 0) {
                return;
            } else if(idxU >= length - 2) {
                return; 
            }
            idx1 = idxU;            
        }

        domain = shortUrl.substring(0, idx1+1);

        var idx2 = shortUrl.indexOf("/", idx1+1);
        if(idx2 == idx1+1) {
            return; 
        } else if(idx2 == idx1+2) {
            if(idx2 < length - 2) {
                idx1 = idx2;
            } else {
                return;
            }
        }
        
        // Note that
        // this token is not real "token" as defined in the server implementation...
        //    This "token" can potentially include query string (with ? sign), etc...
        token = shortUrl.substring(idx1+1);
    };

    this.isReady = function() {
    	return (domain && token);
    };

    this.verifyUrl = function() {
    	//if(DEBUG_ENABLED) console.log("verifyUrl() called.");
        var url = domain + PATH_VERIFY + '/' + token;
        return url;
    };
    this.infoUrl = function() {
    	//if(DEBUG_ENABLED) console.log("infoUrl() called.");
        var url = domain + PATH_INFO + '/' + token;
        return url;
    };
    this.confirmUrl = function() {
    	//if(DEBUG_ENABLED) console.log("confirmUrl() called.");
        var url = domain + PATH_CONFIRM + '/' + token;
        return url;
    };
    this.flashUrl = function(flash) {
    	//if(DEBUG_ENABLED) console.log("flashUrl() called.");
    	var url;
    	if(!isNaN(flash) && flash >=0 && flash < 10) {
        	url = domain + flash + '/' + token;    		
    	} else {
        	url = domain + PATH_FLASH + '/' + token;
    	}
        return url;
    };
    this.permanentUrl = function() {
    	//if(DEBUG_ENABLED) console.log("permanentUrl() called.");
        var url = domain + PATH_PERMANENT + '/' + token;
        return url;
    };
    this.temporaryUrl = function() {
    	//if(DEBUG_ENABLED) console.log("temporaryUrl() called.");
        var url = domain + PATH_TEMPORARY + '/' + token;
        return url;
    };


  };

  return cls;
})();

