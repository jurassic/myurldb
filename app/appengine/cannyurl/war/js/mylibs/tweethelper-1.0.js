/*
Twitter Anywhere API wrapper.
*/


//$(function() {
    var $tweetDialog = $('<div id="tweethelper_tweet_dialog" title="Tweet" style="display: none;"><div id="tweethelper_tweet_body"><div id="tweethelper_tweet_body_tweetbox"></div></div></div>');
    $('body').append($tweetDialog);
//});

$(function() {
    // Hack!!!!
    $("#tweethelper_tweet_dialog").bind('dialogclose', function(event) { 
        if($("#tweethelper_tweet_body_tweetbox_dynamic")) {
     	    $("#tweethelper_tweet_body_tweetbox_dynamic").remove();
        }
    });
});


// TweetHelper object.
var webstoa = webstoa || {};
webstoa.TweetHelper = ( function() {

  var onTweetSuccess = function(plainTwt, htmlTwt) {
      if(DEBUG_ENABLED) console.log("onTweetSuccess(): plainTwt = " + plainTwt);
      //updateStatus('Your message has been successfully tweeted', 'info', 5550);
  	  $( "#tweethelper_tweet_dialog" ).dialog('close');
  };

  var cls = function() {

    this.tweet = function(title, message) {
    	if(DEBUG_ENABLED) console.log("Tweeting: title = " + title + "; message = " + message);

    	if(!twttr) {
    		if(DEBUG_ENABLED) console.log("Twitter Anywwhere is not initialized");
    		//updateStatus("Twitter Anywhere is not initialized");
    		return;
        }
    	
    	if(!title) {
    		title = 'Tweet this message';
    	}

    	// Hack!!!!
        $("#tweethelper_tweet_body").append('<div id="tweethelper_tweet_body_tweetbox_dynamic"></div>');
        twttr.anywhere(function (T) {
            T("#tweethelper_tweet_body_tweetbox_dynamic").tweetBox({
              height: 80,
              width: 400,
              label: title,
              defaultContent: message,
              onTweet: onTweetSuccess
            });
        });

	    $( "#tweethelper_tweet_dialog" ).dialog({
            height: 205,
            width: 440,
            modal: true,
            buttons: {
                //Close: function() {
                //    $(this).dialog('close'); 
                //}
            }
        });    	
    };
  };

  return cls;
})();

