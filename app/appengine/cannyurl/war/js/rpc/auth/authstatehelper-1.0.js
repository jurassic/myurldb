//////////////////////////////////////////////////////////
// Server side: .../servlet/AuthStateServlet.jsva
//////////////////////////////////////////////////////////

var webstoa = webstoa || {};
webstoa.rpc = webstoa.rpc || {};
webstoa.rpc.auth = webstoa.rpc.auth || {};
webstoa.rpc.auth.AuthStateHelper = ( function() {

    /////////////////////////////
    // Utility methods
    /////////////////////////////

    var generateUuid = function() 
    {
        var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
            return v.toString(16);
        });
        return uuid;
    };
    
    var getCurrentTime = function() 
    {
        return (new Date()).getTime();
    };

    var buildWebServiceUrl = function(wsEndPoint, wsServletPath, field, comebackUrl, fallbackUrl)
    {
        var url = wsEndPoint + wsServletPath;
        if(field) {
            url += '/' + field;
        }
        url += "?" + "format=json";
        if(comebackUrl) {
             url += "&comebackUrl=" + comebackUrl;
        }
        if(fallbackUrl) {
             url += "&fallbackUrl=" + fallbackUrl;
        }
        return url;
    }

    /////////////////////////////
    // Constructor
    /////////////////////////////

    var cls = function(endPoint, servletPath, comebackUrl, fallbackUrl, callback) {

        // States.
        // wsEndPoint could an absolute URL (e.g., http://www.filestoa.com/) or an empty string for relative URL.
        var wsEndPoint;
        if(endPoint) {
            wsEndPoint = endPoint;
        } else {
            wsEndPoint = "";   // ???
        }
        // servletPath starts wtih "/".
        var wsServletPath;
        if(servletPath) {
            wsServletPath = servletPath;
        } else {
            wsServletPath = "/ajax/authstate";
        }

        // Private vars, for "caching"
        var authStateBean;
        var authenticated;
        var loginUrl;
        var logoutUrl;

        // comebackUrl
        var defaultComebackUrl;
        if(comebackUrl) {
            defaultComebackUrl = comebackUrl;
        }
        this.getComebackUrl = function()
        {
            return defaultComebackUrl;
        }
        // this.setComebackUrl = function(comebackUrl)
        // {
        //     defaultComebackUrl = comebackUrl;
        // }
        // fallbackUrl
        var defaultFallbackUrl;
        if(fallbackUrl) {
            defaultFallbackUrl = fallbackUrl;
        }
        this.getFallbackUrl = function()
        {
            return defaultFallbackUrl;
        }
        // this.setFallbackUrl = function(fallbackUrl)
        // {
        //     defaultFallbackUrl = fallbackUrl;
        // }

        // Callback
        var defaultCallback;
        if(callback) {
            defaultCallback = callback;
        }
        this.getCallback = function()
        {
            return defaultCallback;
        }
        this.setCallback = function(callback)
        {
            defaultCallback = callback;
        }


        /////////////////////////////
        // Convenience methods
        /////////////////////////////

        this.getAuthStateBean = function() 
        {
            return authStateBean;
        }
        this.fetchAuthStateBean = function(callback, refresh) 
        {
            var activeCallback;
            if(callback) {
                activeCallback = callback;
            } else {
                activeCallback = defaultCallback;
            }

            if(!refresh && authStateBean) {
                // Call callback
                activeCallback(authStateBean);
            } else {
                var that = this;
                // var url = buildWebServiceUrl(wsEndPoint, wsServletPath, "authstruct", defaultComebackUrl, defaultFallbackUrl);
                var url = buildWebServiceUrl(wsEndPoint, wsServletPath, null, defaultComebackUrl, defaultFallbackUrl);
                $.getJSON(url, function(data) {
                    var tis = that;
                    if(data) {
                        authStateBean = data;
                        var t = authStateBean['authenticated'];
                        if(t !== undefined && t != null) {
                            authenticated = t;
                        }
                        var u = authStateBean['loginUrl'];
                        if(u) {
                            loginUrl = u;
                        }
                        var s = authStateBean['logoutUrl'];
                        if(s) {
                            logoutUrl = s;
                        }
                        // Call callback
                        activeCallback(authStateBean);
                    } else {
                        // ???
                    }
                });
            }
        };

        this.isAuthenticated = function() 
        {
            return authenticated;
        }
        this.fetchAuthenticated = function(callback, refresh) 
        {
            var activeCallback;
            if(callback) {
                activeCallback = callback;
            } else {
                activeCallback = defaultCallback;
            }

            if(!refresh && authenticated) {
                // Call callback
                activeCallback(authenticated);
            } else {
                var that = this;
                var url = buildWebServiceUrl(wsEndPoint, wsServletPath, "authenticated", defaultComebackUrl, defaultFallbackUrl);
                $.getJSON(url, function(data) {
                    var tis = that;
                    if(data) {
                        var t = data['authenticated'];
                        if(t !== undefined && t != null) {
                            authenticated = t;
                            // Call callback
                            activeCallback(authenticated);
                        }
                    } else {
                        // ???
                    }
                });
            }
        };

        this.getLoginUrl = function() 
        {
            return loginUrl;
        }
        this.fetchLoginUrl = function(callback, refresh) 
        {
            var activeCallback;
            if(callback) {
                activeCallback = callback;
            } else {
                activeCallback = defaultCallback;
            }

            if(!refresh && loginUrl) {
                // Call callback
                activeCallback(loginUrl);
            } else {
                var that = this;
                var url = buildWebServiceUrl(wsEndPoint, wsServletPath, "loginurl", defaultComebackUrl, defaultFallbackUrl);
                $.getJSON(url, function(data) {
                    var tis = that;
                    if(data) {
                        var u = data['loginurl'];
                        if(u) {
                            loginUrl = u;
                            // Call callback
                            activeCallback(loginUrl);
                        }
                    } else {
                        // ???
                    }
                });
            }
        };

        this.getLogoutUrl = function() 
        {
            return logoutUrl;
        }
        this.fetchLogoutUrl = function(callback, refresh) 
        {
            var activeCallback;
            if(callback) {
                activeCallback = callback;
            } else {
                activeCallback = defaultCallback;
            }

            if(!refresh && logoutUrl) {
                // Call callback
                activeCallback(logoutUrl);
            } else {
                var that = this;
                var url = buildWebServiceUrl(wsEndPoint, wsServletPath, "logouturl", defaultComebackUrl, defaultFallbackUrl);
                $.getJSON(url, function(data) {
                    var tis = that;
                    if(data) {
                        var s = data['logouturl'];
                        if(s) {
                            logoutUrl = s;
                            // Call callback
                            activeCallback(logoutUrl);
                        }
                    } else {
                        // ???
                    }
                });
            }
        };

    };

    return cls;
})();

