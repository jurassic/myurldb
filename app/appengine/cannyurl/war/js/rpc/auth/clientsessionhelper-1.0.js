//////////////////////////////////////////////////////////
// Server side: .../servlet/ClientSessionServlet.jsva
//////////////////////////////////////////////////////////

var webstoa = webstoa || {};
webstoa.rpc = webstoa.rpc || {};
webstoa.rpc.auth = webstoa.rpc.auth || {};
webstoa.rpc.auth.ClientSessionHelper = ( function() {

    /////////////////////////////
    // Utility methods
    /////////////////////////////

    var generateUuid = function() 
    {
        var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
            return v.toString(16);
        });
        return uuid;
    };
    
    var getCurrentTime = function() 
    {
        return (new Date()).getTime();
    };

    var buildMapKey = function(attrKey, valType)
    {
        var mapKey = attrKey;
        if(valType) {
            mapKey += "-" + valType;
        }
        return mapKey;
    }

    var buildWebServiceUrl = function(wsEndPoint, wsServletPath, attrKey, valType)
    {
        var url = wsEndPoint + wsServletPath;
        url += '/' + attrKey;
        if(valType) {
            url += "/" + valType;
        }
        url += "?" + "format=json";
        return url;
    }

    /////////////////////////////
    // Constructor
    /////////////////////////////

    var cls = function(endPoint, servletPath, callback) {

        // States.
        // wsEndPoint could an absolute URL (e.g., http://www.filestoa.com/) or an empty string for relative URL.
        var wsEndPoint;
        if(endPoint) {
            wsEndPoint = endPoint;
        } else {
            wsEndPoint = "";   // ???
        }
        // servletPath starts wtih "/".
        var wsServletPath;
        if(servletPath) {
            wsServletPath = servletPath;
        } else {
            wsServletPath = "/ajax/clientsession";
        }

        // Private vars, for "caching"
        var attributeMap = {};

        // Callback
        var defaultCallback;
        if(callback) {
            defaultCallback = callback;
        }
        this.getCallback = function()
        {
            return defaultCallback;
        }
        this.setCallback = function(callback)
        {
            defaultCallback = callback;
        }


        /////////////////////////////
        // Convenience methods
        /////////////////////////////

        this.getAttribute = function(attrKey, valType) 
        {
            var attrValue;
            var mapKey = buildMapKey(attrKey, valType);
            if(mapKey in attributeMap) {
                attrValue = attributeMap[mapKey];
            }
            return attrValue;
        };
        this.fetchAttribute = function(attrKey, valType, callback, refresh) 
        {
            var activeCallback;
            if(callback) {
                activeCallback = callback;
            } else {
                activeCallback = defaultCallback;
            }

            var attrValue;
            var mapKey = buildMapKey(attrKey, valType);
            if(!refresh && (mapKey in attributeMap)) {
                attrValue = attributeMap[mapKey];
                // if(attrValue != null) {
                    // Call callback
                    activeCallback(attrValue);
                // }
            } else {
                var that = this;
                var url = buildWebServiceUrl(wsEndPoint, wsServletPath, attrKey, valType);
                $.getJSON(url, function(data) {
                    var tis = that;
                    if(data) {
                        var val = data[attrKey];
                        if(val !== undefined && val != null) {
                            attrValue = val;
                            attributeMap[mapKey] = attrValue;
                            // Call callback
                            activeCallback(attrValue);
                        } else {
                            // ???
                        }
                    } else {
                        // ???
                    }
                });            
            }
        };

    };

    return cls;
})();

