//////////////////////////////////////////////////////////
// Server side: .../servlet/SessionBeanServlet.jsva
//////////////////////////////////////////////////////////

var webstoa = webstoa || {};
webstoa.rpc = webstoa.rpc || {};
webstoa.rpc.auth = webstoa.rpc.auth || {};
webstoa.rpc.auth.SessionBeanHelper = ( function() {

    /////////////////////////////
    // Utility methods
    /////////////////////////////

    var generateUuid = function() 
    {
        var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
            return v.toString(16);
        });
        return uuid;
    };
    
    var getCurrentTime = function() 
    {
        return (new Date()).getTime();
    };

    var buildWebServiceUrl = function(wsEndPoint, wsServletPath, field)
    {
        var url = wsEndPoint + wsServletPath;
        if(field) {
            url += '/' + field;
        }
        url += "?" + "format=json";
        return url;
    }

    /////////////////////////////
    // Constructor
    /////////////////////////////

    var cls = function(endPoint, servletPath, callback) {

        // States.
        // wsEndPoint could an absolute URL (e.g., http://www.filestoa.com/) or an empty string for relative URL.
        var wsEndPoint;
        if(endPoint) {
            wsEndPoint = endPoint;
        } else {
            wsEndPoint = "";   // ???
        }
        // servletPath starts wtih "/".
        var wsServletPath;
        if(servletPath) {
            wsServletPath = servletPath;
        } else {
            wsServletPath = "/ajax/sessionbean";
        }

        // Private vars, for "caching"
        var sessionBean;
        var token;
        var userId;
        var status;

        // Callback
        var defaultCallback;
        if(callback) {
            defaultCallback = callback;
        }
        this.getCallback = function()
        {
            return defaultCallback;
        }
        this.setCallback = function(callback)
        {
            defaultCallback = callback;
        }


        /////////////////////////////
        // Convenience methods
        /////////////////////////////

        this.getSessionBean = function() 
        {
            return sessionBean;
        }
        this.fetchSessionBean = function(callback, refresh) 
        {
            var activeCallback;
            if(callback) {
                activeCallback = callback;
            } else {
                activeCallback = defaultCallback;
            }

            if(!refresh && sessionBean) {
                // Call callback
                activeCallback(sessionBean);
            } else {
                var that = this;
                // var url = buildWebServiceUrl(wsEndPoint, wsServletPath, "sessionstruct");
                var url = buildWebServiceUrl(wsEndPoint, wsServletPath);
                $.getJSON(url, function(data) {
                    var tis = that;
                    if(data) {
                        sessionBean = data;
                        var t = sessionBean['token'];
                        if(t) {
                            token = t;
                        }
                        var u = sessionBean['userId'];
                        if(u) {
                            userId = u;
                        }
                        var s = sessionBean['status'];
                        if(s) {
                            status = s;
                        }
                        // Call callback
                        activeCallback(sessionBean);
                    } else {
                        // ???
                    }
                });
            }
            return sessionBean;
        };

        this.getToken = function() 
        {
            return token;
        }
        this.fetchToken = function(callback, refresh) 
        {
            var activeCallback;
            if(callback) {
                activeCallback = callback;
            } else {
                activeCallback = defaultCallback;
            }

            if(!refresh && token) {
                // Call callback
                activeCallback(token);
            } else {
                var that = this;
                var url = buildWebServiceUrl(wsEndPoint, wsServletPath, "token");
                $.getJSON(url, function(data) {
                    var tis = that;
                    if(data) {
                        var t = data['token'];
                        if(t) {
                            token = t;
                            // Call callback
                            activeCallback(token);
                        }
                    } else {
                        // ???
                    }
                });
            }
        };

        this.getUserId = function() 
        {
            return userId;
        }
        this.fetchUserId = function(callback, refresh) 
        {
            var activeCallback;
            if(callback) {
                activeCallback = callback;
            } else {
                activeCallback = defaultCallback;
            }

            if(!refresh && userId) {
                // Call callback
                activeCallback(userId);
            } else {
                var that = this;
                var url = buildWebServiceUrl(wsEndPoint, wsServletPath, "userid");
                $.getJSON(url, function(data) {
                    var tis = that;
                    if(data) {
                        var u = data['userid'];
                        if(u) {
                            userId = u;
                            // Call callback
                            activeCallback(userId);
                        }
                    } else {
                        // ???
                    }
                });
            }
        };

        this.getStatus = function() 
        {
            return status;
        }
        this.fetchStatus = function(callback, refresh) 
        {
            var activeCallback;
            if(callback) {
                activeCallback = callback;
            } else {
                activeCallback = defaultCallback;
            }

            if(!refresh && status) {
                // Call callback
                activeCallback(status);
            } else {
                var that = this;
                var url = buildWebServiceUrl(wsEndPoint, wsServletPath, "status");
                $.getJSON(url, function(data) {
                    var tis = that;
                    if(data) {
                        var s = data['status'];
                        if(s) {
                            status = s;
                            // Call callback
                            activeCallback(status);
                        }
                    } else {
                        // ???
                    }
                });
            }
        };

    };

    return cls;
})();

