//////////////////////////////////////////////////////////
// Server side: .../servlet/BrandingUtilServlet.jsva
//////////////////////////////////////////////////////////

var webstoa = webstoa || {};
webstoa.rpc = webstoa.rpc || {};
webstoa.rpc.core = webstoa.rpc.core || {};
webstoa.rpc.core.BrandingUtilHelper = ( function() {

    /////////////////////////////
    // Utility methods
    /////////////////////////////

    var generateUuid = function() 
    {
        var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
            return v.toString(16);
        });
        return uuid;
    };
    
    var getCurrentTime = function() 
    {
        return (new Date()).getTime();
    };

    var buildWebServiceUrl = function(wsEndPoint, wsServletPath, field)
    {
        var url = wsEndPoint + wsServletPath;
        if(field) {
            url += '/' + field;
        }
        url += "?" + "format=json";
        return url;
    }

    /////////////////////////////
    // Constructor
    /////////////////////////////

    var cls = function(endPoint, servletPath, callback) {

        // States.
        // wsEndPoint could an absolute URL (e.g., http://www.filestoa.com/) or an empty string for relative URL.
        var wsEndPoint;
        if(endPoint) {
            wsEndPoint = endPoint;
        } else {
            wsEndPoint = "";   // ???
        }
        // servletPath starts wtih "/".
        var wsServletPath;
        if(servletPath) {
            wsServletPath = servletPath;
        } else {
            wsServletPath = "/ajax/brandingutil";
        }

        // Private vars, for "caching"
        var brandStruct;
        var appBrand;
        var displayName;
        var description;

        // Callback
        var defaultCallback;
        if(callback) {
            defaultCallback = callback;
        }
        this.getCallback = function()
        {
            return defaultCallback;
        }
        this.setCallback = function(callback)
        {
            defaultCallback = callback;
        }


        /////////////////////////////
        // Convenience methods
        /////////////////////////////

        this.getBrandStruct = function() 
        {
            return brandStruct;
        }
        this.fetchBrandStruct = function(callback, refresh) 
        {
            var activeCallback;
            if(callback) {
                activeCallback = callback;
            } else {
                activeCallback = defaultCallback;
            }

            if(!refresh && brandStruct) {
                // Call callback
                activeCallback(brandStruct);
            } else {
                var that = this;
                //  var url = buildWebServiceUrl(wsEndPoint, wsServletPath, "brandstruct");
                var url = buildWebServiceUrl(wsEndPoint, wsServletPath);
                $.getJSON(url, function(data) {
                    var tis = that;
                    if(data) {
                        brandStruct = data;
                        var brand = brandStruct['brand'];
                        if(brand) {
                            appBrand = brand;
                        }
                        var name = brandStruct['name'];
                        if(name) {
                            displayName = name;
                        }
                        var desc = brandStruct['description'];
                        if(desc) {
                            description = desc;
                        }
                        // Call callback
                        activeCallback(brandStruct);
                    } else {
                        // ???
                    }
                });
            }
        };

        this.getAppBrand = function() 
        {
            return appBrand;
        }
        this.fetchAppBrand = function(callback, refresh) 
        {
            var activeCallback;
            if(callback) {
                activeCallback = callback;
            } else {
                activeCallback = defaultCallback;
            }

            if(!refresh && appBrand) {
                // Call callback
                activeCallback(appBrand);
            } else {
                var that = this;
                var url = buildWebServiceUrl(wsEndPoint, wsServletPath, "appbrand");
                $.getJSON(url, function(data) {
                    var tis = that;
                    if(data) {
                        var brand = data['appbrand'];
                        if(brand) {
                            appBrand = brand;
                            // Call callback
                            activeCallback(appBrand);
                        }
                    } else {
                        // ???
                    }
                });
            }
        };

        this.getDisplayName = function() 
        {
            return displayName;
        }
        this.fetchDisplayName = function(callback, refresh) 
        {
            var activeCallback;
            if(callback) {
                activeCallback = callback;
            } else {
                activeCallback = defaultCallback;
            }

            if(!refresh && displayName) {
                // Call callback
                activeCallback(displayName);
            } else {
                var that = this;
                var url = buildWebServiceUrl(wsEndPoint, wsServletPath, "dsiplayname");
                $.getJSON(url, function(data) {
                    var tis = that;
                    if(data) {
                        var name = data['dsiplayname'];
                        if(name) {
                            displayName = name;
                            // Call callback
                            activeCallback(displayName);
                        }
                    } else {
                        // ???
                    }
                });
            }
        };

        this.getDescription = function() 
        {
            return description;
        }
        this.fetchDescription = function(callback, refresh) 
        {
            var activeCallback;
            if(callback) {
                activeCallback = callback;
            } else {
                activeCallback = defaultCallback;
            }

            if(!refresh && description) {
                // Call callback
                activeCallback(description);
            } else {
                var that = this;
                var url = buildWebServiceUrl(wsEndPoint, wsServletPath, "description");
                $.getJSON(url, function(data) {
                    var tis = that;
                    if(data) {
                        var desc = data['description'];
                        if(desc) {
                            description = desc;
                            // Call callback
                            activeCallback(description);
                        }
                    } else {
                        // ???
                    }
                });
            }
        };

    };

    return cls;
})();

