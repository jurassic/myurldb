//////////////////////////////////////////////////////////
// Server side: .../servlet/ClientInfoServlet.jsva
//////////////////////////////////////////////////////////

var webstoa = webstoa || {};
webstoa.rpc = webstoa.rpc || {};
webstoa.rpc.core = webstoa.rpc.auth || {};
webstoa.rpc.core.ClientInfoHelper = ( function() {

    /////////////////////////////
    // Utility methods
    /////////////////////////////

    var generateUuid = function() 
    {
        var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
            return v.toString(16);
        });
        return uuid;
    };
    
    var getCurrentTime = function() 
    {
        return (new Date()).getTime();
    };

    var buildMapKey = function(attrKey, valType)
    {
        var mapKey = attrKey;
        if(valType) {
            mapKey += "-" + valType;
        }
        return mapKey;
    }

    var buildWebServiceUrl = function(wsEndPoint, wsServletPath, attrKey, valType)
    {
        var url = wsEndPoint + wsServletPath;
        url += '/' + attrKey;
        if(valType) {
            url += "/" + valType;
        }
        url += "?" + "format=json";
        return url;
    }

    /////////////////////////////
    // Constructor
    /////////////////////////////

    var cls = function(endPoint, servletPath, callback) {

        // States.
        // wsEndPoint could an absolute URL (e.g., http://www.filestoa.com/) or an empty string for relative URL.
        var wsEndPoint;
        if(endPoint) {
            wsEndPoint = endPoint;
        } else {
            wsEndPoint = "";   // ???
        }
        // servletPath starts wtih "/".
        var wsServletPath;
        if(servletPath) {
            wsServletPath = servletPath;
        } else {
            wsServletPath = "/ajax/clientinfo";
        }

        // Private vars, for "caching"
        var ipAddress;
        var hostname;

        // Callback
        var defaultCallback;
        if(callback) {
            defaultCallback = callback;
        }
        this.getCallback = function()
        {
            return defaultCallback;
        }
        this.setCallback = function(callback)
        {
            defaultCallback = callback;
        }


        /////////////////////////////
        // Convenience methods
        /////////////////////////////

        this.getIpAddress = function() 
        {
            return ipAddress;
        }
        this.fetchIpAddress = function(callback, refresh) 
        {
            var activeCallback;
            if(callback) {
                activeCallback = callback;
            } else {
                activeCallback = defaultCallback;
            }

            if(!refresh && ipAddress) {
                // Call callback
                activeCallback(ipAddress);
            } else {
                var that = this;
                var url = buildWebServiceUrl(wsEndPoint, wsServletPath, "ipaddress");
                $.getJSON(url, function(data) {
                    var tis = that;
                    if(data) {
                        var brand = data['ipaddress'];
                        if(brand) {
                            ipAddress = brand;
                            // Call callback
                            activeCallback(ipAddress);
                        }
                    } else {
                        // ???
                    }
                });
            }
        };

        this.getHostname = function() 
        {
            return hostname;
        }
        this.fetchHostname = function(callback, refresh) 
        {
            var activeCallback;
            if(callback) {
                activeCallback = callback;
            } else {
                activeCallback = defaultCallback;
            }

            if(!refresh && hostname) {
                // Call callback
                activeCallback(hostname);
            } else {
                var that = this;
                var url = buildWebServiceUrl(wsEndPoint, wsServletPath, "hostname");
                $.getJSON(url, function(data) {
                    var tis = that;
                    if(data) {
                        var brand = data['hostname'];
                        if(brand) {
                            hostname = brand;
                            // Call callback
                            activeCallback(hostname);
                        }
                    } else {
                        // ???
                    }
                });
            }
        };

    };

    return cls;
})();

