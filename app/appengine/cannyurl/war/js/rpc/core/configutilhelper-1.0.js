//////////////////////////////////////////////////////////
// Server side: .../servlet/ConfigUtilServlet.jsva
//////////////////////////////////////////////////////////

var webstoa = webstoa || {};
webstoa.rpc = webstoa.rpc || {};
webstoa.rpc.core = webstoa.rpc.core || {};
webstoa.rpc.core.ConfigUtilHelper = ( function() {

    /////////////////////////////
    // Utility methods
    /////////////////////////////

    var generateUuid = function() 
    {
        var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
            return v.toString(16);
        });
        return uuid;
    };
    
    var getCurrentTime = function() 
    {
        return (new Date()).getTime();
    };

    var buildMapKey = function(configKey, valType, defaultVal)
    {
        var mapKey = configKey;
        if(valType) {
            mapKey += "-" + valType;
        } else {
            mapKey += "-";
        }
        if(defaultVal !== undefined && defaultVal != null) {
            mapKey += "-" + defaultVal;
        }
        return mapKey;
    }

    var buildWebServiceUrl = function(wsEndPoint, wsServletPath, configKey, valType, defaultVal)
    {
        var url = wsEndPoint + wsServletPath;
        url += '/' + configKey;
        if(valType) {
            url += "/" + valType;
            if(defaultVal !== undefined && defaultVal != null) {
                url += "/" + defaultVal;
            }
        }
        url += "?" + "format=json";
        return url;
    }

    /////////////////////////////
    // Constructor
    /////////////////////////////

    var cls = function(endPoint, servletPath, callback) {

        // States.
        // wsEndPoint could an absolute URL (e.g., http://www.filestoa.com/) or an empty string for relative URL.
        var wsEndPoint;
        if(endPoint) {
            wsEndPoint = endPoint;
        } else {
            wsEndPoint = "";   // ???
        }
        // servletPath starts wtih "/".
        var wsServletPath;
        if(servletPath) {
            wsServletPath = servletPath;
        } else {
            wsServletPath = "/ajax/configutil";
        }

        // Private vars, for "caching"
        var variableMap = {};

        // Callback
        var defaultCallback;
        if(callback) {
            defaultCallback = callback;
        }
        this.getCallback = function()
        {
            return defaultCallback;
        }
        this.setCallback = function(callback)
        {
            defaultCallback = callback;
        }

        /////////////////////////////
        // Convenience methods
        /////////////////////////////

        this.getConfigValue = function(configKey, valType, defaultVal) 
        {
            var configValue;
            var mapKey = buildMapKey(configKey, valType, defaultVal);
            if(mapKey in variableMap) {
                configValue = variableMap[mapKey];
            }
            return configValue;
        };
        this.fetchConfigValue = function(configKey, valType, defaultVal, callback, refresh) 
        {
            var activeCallback;
            if(callback) {
                activeCallback = callback;
            } else {
                activeCallback = defaultCallback;
            }

            var configValue;
            var mapKey = buildMapKey(configKey, valType, defaultVal);
            if(!refresh && (mapKey in variableMap)) {
                configValue = variableMap[mapKey];
                // if(configValue != null) {
                    // Call callback
                    activeCallback(configValue);
                // }
            } else {
                var that = this;
                var url = buildWebServiceUrl(wsEndPoint, wsServletPath, configKey, valType, defaultVal);
                $.getJSON(url, function(data) {
                    var tis = that;
                    if(data) {
                        var val = data[configKey];
                        // console.log("val = " + val);
                        if(val !== undefined && val != null) {
                            configValue = val;
                            variableMap[mapKey] = configValue;
                            // Call callback
                            activeCallback(configValue);
                        } else {
                            // ???
                        }
                    } else {
                        // ???
                    }
                });            
            }
        };

    };

    return cls;
})();

