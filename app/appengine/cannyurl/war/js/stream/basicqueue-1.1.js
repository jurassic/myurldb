/*

*/

var webstoa = webstoa || {};
webstoa.stream = webstoa.stream || {};
webstoa.stream.BasicQueue = ( function() {

  var getCurrentTime = function() {
	return (new Date()).getTime();
  }

  var cls = function(maxLen) {
    
    var maxSize;
    var isCircular;
    // TBD: validation ?
    if(maxLen && maxLen > 0) {
        maxSize = maxLen + 1;  // Keep one cell as "buffer"
        isCircular = true;
    } else {
    	// ???
        maxSize = undefined;
        isCircular = false;
    }

    // [...)
    var _startPointer = 0;
    var _endPointer = 0;
    var _queue = [];

    this.size = function() {
        var end = _endPointer;
        if(isCircular && end < _startPointer) {
            end = _endPointer + maxSize;
        }
        var len = end - _startPointer;
        return len;
    };
    this.maxCapacity = function() {
        return maxSize - 1;   // One cell is not being used...
    };
    this.freeCapacity = function() {
        var len = (maxSize - 1) - this.size();
        return len;
    };
    this.isEmpty = function() {
        if(_startPointer == _endPointer) {
        	return true;
        } else {
        	return false;
        }
    };
    
    this.reset = function() {
    	_startPointer = 0;
    	_endPointer = 0;
    };

    this.enqueue = function(item) {
        if(DEBUG_ENABLED) console.log("enqueue(): item = " + item);
        // _queue.push(item);
        _queue[_endPointer] = item;
        _endPointer++;
        if(isCircular) {
            _endPointer = _endPointer % maxSize;
        }
        if(_startPointer == _endPointer) {   
            // _endPointer has overtaken _startPointer...
            // (Note that the current queue size > 0 at this point.)
            // What to do???
            // Remove the old item and keep the new one... (which is already done above)
            // Just reset the _startPointer;
            _startPointer++;
            if(isCircular) {
                _startPointer = _startPointer % maxSize;
            }
            if(DEBUG_ENABLED) console.log("Queue is over-capacity. Resetting _startPointer: " + _startPointer);            
        }
    };

    this.dequeue = function() {
        // var item = _queue.shift();
    	if(_startPointer == _endPointer) {
    		return null;
    	}
        var item = _queue[_startPointer];
        _startPointer++;
        if(isCircular) {
            _startPointer = _startPointer % maxSize;
        }
        if(DEBUG_ENABLED) console.log("dequeue(): item = " + item);
        return item;
    };

    this.peek = function() {
        var item = _queue[_startPointer];
        if(DEBUG_ENABLED) console.log("peek(): item = " + item);
        return item;
    };

  };

  return cls;
})();

