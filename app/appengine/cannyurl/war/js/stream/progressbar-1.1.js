/*

*/

var webstoa = webstoa || {};
webstoa.stream = webstoa.stream || {};
webstoa.stream.ProgressBar = ( function() {

  var getCurrentTime = function() {
	return (new Date()).getTime();
  }

  // If first < second, then increment
  // If first > second, then decrement
  var cls = function(duration, delta, first, second) {
    // Default range [0, 100)
    var firstValue;  
    var secondValue;  
    if(first != null) {
    	firstValue = first;    // TBD: validation?	
    } else {
    	firstValue = 0.0;
    }
    if(second != null) {
    	secondValue = second;  // TBD: validation?	
    } else {
    	secondValue = 10.0;      // 0 ~> 9 by default...
    }

    // Just use (valueDelta > 0) or not....
    // var isIncrement = true;
    // if(secondValue < firstValue) {
    //    isIncrement = false;
    // }

    var durationMillis;
    if(duration && duration > 0) {
        durationMillis = duration;
    } else {
        durationMillis = 10 * 1000;   // 10 seconds...
    }   

    var intervalDelta;
    if(delta && delta > 0) {
        intervalDelta = delta;
    } else {
        // ???
        intervalDelta = durationMillis / Math.abs(secondValue - firstValue);
    }
    
    var totalSteps = Math.ceil(durationMillis / intervalDelta);
    intervalDelta = durationMillis / totalSteps;
    var valueDelta = (secondValue - firstValue) / totalSteps;   // >0 -> Increment, <0 -> decrement...
    
    
    this.setDurationMillis = function(duration, delta, first, second) {
        if(first != null) {
        	firstValue = first;    // TBD: validation?	
        }
        if(second != null) {
        	secondValue = second;  // TBD: validation?	
        }
        durationMillis = duration;  // ???
    	if(delta && delta > 0) {
    		intervalDelta = delta;
        } else {
            // ???
            intervalDelta = durationMillis / Math.abs(secondValue - firstValue);
    	}
    	var numSteps = Math.ceil(durationMillis / intervalDelta);
    	intervalDelta = durationMillis / numSteps;
    	valueDelta = (secondValue - firstValue) / numSteps;
    };
    this.getDurationMillis = function() {
    	return durationMillis;
    };

    
    // Callback function for display/rendering the progressBar
    // This function takes one arg, currentValue... (the caller should know first/second values...)
    var displayCallback;
    this.setDisplayCallback = function(callback) {
    	displayCallback = callback;  // ???
    };
    this.getDisplayCallback = function() {
    	return displayCallback;
    };

    // In order to be able to "stop" the timer...
    var progressTimerID;

    // The "counter".
    // Initially unset..
    // var currentValue = firstValue;
    var currentValue;

    
    this.getCurrentValue = function() {
        return currentValue;
    };
    
    this.reset = function() {
        currentValue = null;   // ???
        if(progressTimerID) {
            window.clearTimeout(progressTimerID);
        }
        if(displayCallback) {
            // if(DEBUG_ENABLED) console.log("Calling the callback function with currentValue = " + currentValue);
            displayCallback(secondValue);
        }
    };
    
    this.pause = function() {
        // Does this work??? What if it's called when progress() is being executed...
        if(progressTimerID) {
            window.clearTimeout(progressTimerID);
        }
    };

    // Just use start()
    // this.continue = function() {
    //	this.progress();
    // };

    this.start = function(restart) {
    	if(DEBUG_ENABLED) console.log("Starting progress: durationMillis = " + durationMillis + "; intervalDelta = " + intervalDelta + "; currentValue = " + currentValue);

        // ????
        // restart==true -> restart from the beginning...
        if(restart || ! currentValue) {   // first time...
            currentValue = firstValue - valueDelta;  // ???
            //currentValue = firstValue; // ???
        }
        if(progressTimerID) {
            window.clearTimeout(progressTimerID);
        }
    	this.progress();
    };

    this.progress = function() {
        var that = this;
    	if(((valueDelta > 0) && (currentValue > secondValue)) || ((valueDelta < 0) && (currentValue < secondValue))) {
    		//currentValue = secondValue + valueDelta;
	        //currentValue += valueDelta;
        	if(DEBUG_ENABLED) console.log(">>>>>>>>>>>>>>>>>>>>>>> currentValue = " + currentValue + "; firstValue = " + firstValue + "; secondValue = " + secondValue);
            if(progressTimerID) {
            	//if(DEBUG_ENABLED) console.log(">>>>>>>>>>>>>>>>>>>>>>> progressTimerID = " + progressTimerID);
                window.clearTimeout(progressTimerID);
            } else {
            	// if(DEBUG_ENABLED) console.log(">>>>>>>>>>>>>>>>>>>>>>> progressTimerID is not set");            	
            }
            // return;
    	} else {
	        currentValue += valueDelta;
	    	progressTimerID = window.setTimeout(function() { that.progress(); }, intervalDelta);
    	}
    	
        if(displayCallback) {
            // if(DEBUG_ENABLED) console.log("Calling the callback function with currentValue = " + currentValue);
            displayCallback(currentValue);
        } else {
            if(DEBUG_ENABLED) console.log("Callback function has not been set.");
        }
    };

  };

  return cls;
})();

