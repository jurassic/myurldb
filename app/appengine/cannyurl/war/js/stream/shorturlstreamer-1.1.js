/*

*/

var cannyurl = cannyurl || {};
cannyurl.stream = cannyurl.stream || {};
cannyurl.stream.ShortUrlStreamer = ( function() {

  var getCurrentTime = function() {
	return (new Date()).getTime();
  }

  var convertGeoCellToString = function(geoCellStruct) {
    var geoCellString;
    if(geoCellStruct) {
       var scale = geoCellStruct.scale;
       var latitude = geoCellStruct.latitude;
       var longitude = geoCellStruct.longitude;
       geoCellString = scale + ',' + latitude + ',' + longitude;
    }
    return geoCellString;
  }

  
  var cls = function(geoCell, termT, termM, fMillis, dMillis, maxF, maxD) {
    var geoCellStruct;
	var fetchCounter = 0;
    var displayCounter = 0;
    var termType;
    var termDelta;
//    var termDayHour;
    var termMillis
    var fetchMillis;
    var displayMillis;
    var maxFetches;
    var maxDisplays;
    if(geoCell) {
    	geoCellStruct = geoCell;
    } else {
        // ???
        // geoCellStruct = null;   // ???
    }
    if(termT) {
        termType = termT;
    } else {
        termType = "tenmins";
    }
    if(termType == 'minute') {
    	termDelta = 60 * 1000;
    } else if(termType == 'tenmins') {
    	termDelta = 10 * 60 * 1000;
    } else if(termType == 'hourly') {
    	termDelta = 60 * 60 * 1000;
    } else if(termType == 'daily') {
    	termDelta = 24 * 60 * 60 * 1000;
    } else {
    	// ???
    	termDelta = 24 * 60 * 60 * 1000;  // ???
    }
//    if(termD) {
//        // sanitized? ( " " -> "+" ) ???  Otherwise we need to url encode it ???
//        // termDayHour = termD;
//        termDayHour = termD.replace(" ", "+");
//    } else {
//        // ????
//        if(DEBUG_ENABLED) console.log("Invalid termDayHour.");
//        // Bail out ???
//    }
    if(termM && termM > 0) {
    	termMillis = termM;
    } else {
        // ????
        if(DEBUG_ENABLED) console.log("Invalid termMillis.");
        // Bail out ???
    }
    // Note: Actual fetch is done based on termType...
    // fetchMillis is a "checking" interval of a timer..
    if(fMillis && fMillis > 0) {
    	fetchMillis = fMillis;           // TBD: validation?	
    } else {
    	fetchMillis = 2 * 60 * 1000;    // 2 mins by default.
    }
    // displayMillis actually defines the refresh interval..
    // Note that if there are more than 40 messages per 10 mins,
    // the queue will grow and grow (based on 15 sec refresh)....
    if(dMillis && dMillis > 0) {
    	displayMillis = dMillis;         // TBD: validation?	
    } else {
    	displayMillis = 15 * 1000;       // 15 secs by default
    }
    if(maxF && maxF > 0) {
    	maxFetches = maxF;     // TBD: validation?
    } else {
    	maxFetches = 120;      // 120 times by default. ( 120 x 10 mins = 20 hours )
    }
    if(maxD && maxD > 0) {
    	maxDisplays = maxD;     // TBD: validation?
    } else {
    	maxDisplays = 2000;     // 2000 message displays by default.
    }
    
    
    // Timer IDs
    var fetchTimerID = null;
    var displayTimerID = null;

    // Message Queue
    var maxLen = 100;  // arbitrary
    var messageQueue = new webstoa.stream.BasicQueue(maxLen);
    // ...
    
    this.setMessageQueue = function(queue) {
    	messageQueue = queue;  // ???
    };
    this.getMessageQueue = function() {
    	return messageQueue;  // ???
    };  
    
    // progress bar..
    var progressBar;
    this.setProgressBar = function(pbar) {
    	progressBar = pbar;  // ???
    };
    this.getProgressBar = function() {
    	return progressBar;
    };
    
    // Display...
    var displayCallback;
    this.setDisplayCallback = function(callback) {
    	displayCallback = callback;  // ???
    };
    this.getDisplayCallback = function() {
    	return displayCallback;
    };
    

    this.getMessageCount = function() {
        if(messageQueue) {
        	return messageQueue.size();
        } else {
        	return 0;
        }
    };


    this.reset = function() {
        fetchCounter = maxFetches;  // ???
        displayCounter = maxDisplays;
    	if(fetchTimerID) {
    		window.clearTimeout(fetchTimerID);
    	}
    	if(displayTimerID) {
    		window.clearTimeout(displayTimerID);
    	}
    };

    this.pause = function() {
    	if(displayTimerID) {
    		window.clearTimeout(displayTimerID);
    	}
    };
    this.restart = function() {
    	this.display();
    };

    this.displayNow = function() {
    	// First stop the currently running display() chain (e.g., through setTimeout()...)
    	if(displayTimerID) {
    		window.clearTimeout(displayTimerID);
    	}
    	this.display();
    };

    this.start = function(queue) {
    	if(DEBUG_ENABLED) console.log("Starting fetch: fetchMillis = " + fetchMillis + "; maxFetches = " + maxFetches);

    	// Stop the timers, etc...
    	this.reset();
    	
    	if(queue) {
    		messageQueue = queue;
    	} else {
    		if(messageQueue) {
    			// Just use the current queu...
    		} else {
    			// ???
    			messageQueue = new webstoa.stream.BasicQueue();  // MaxLen ????
    		}
    	}

    	// Then, "restart"
    	fetchCounter = 0;
    	displayCounter = 0;
    	this.fetch();
    	this.display();
    };

    this.fetch = function() {
        ++fetchCounter;
    	if(fetchCounter > maxFetches) {
    		// TBD: Kill the timer...
        	if(fetchTimerID) {
        		window.clearTimeout(fetchTimerID);
        	}
    		return;
    	}
        var that = this;
        
		if(DEBUG_ENABLED) console.log("fetch(): fetchCounter = " + fetchCounter);
		fetchTimerID = window.setTimeout(function() { that.fetch(); }, fetchMillis);	

		var bufferMillis = 60 * 1000;   // 1 minute
        var nowPadded = getCurrentTime() + bufferMillis;
        if(termMillis + termDelta > nowPadded) {
        	// Not yet...
    		if(DEBUG_ENABLED) console.log("fetch(): termMillis not yet reached. termMillis+termDelta = " + (termMillis + termDelta) + "; nowPadded = " + nowPadded);
    		return;
        }
        
        var messageListUrl;
        if(geoCellStruct) {
            messageListUrl = '/ajax/stream/geocell/' + convertGeoCellToString(geoCellStruct) + '/' + termType + '/' + termMillis; 
        } else {
            messageListUrl = '/ajax/stream/' + termType + '/' + termMillis; 
        }

        $.ajax({
        	type: "GET",
            url: messageListUrl,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(data, textStatus) {
	    		var tis = that;
	    		if(DEBUG_ENABLED) console.log("fetch(): fetchCounter = " + fetchCounter + "; textStatus = " + textStatus);
	    		// fetchTimerID = window.setTimeout(function() { tis.fetch(); }, fetchMillis);	
	            
	            if(data) {
	            	var len = data.length;
	            	if(DEBUG_ENABLED) console.log("fetch() len = " + len + " for messageListUrl = " + messageListUrl);
	
	            	// ???
	            	// This does not really make sense...
	            	// But, this seems to be the only way we can start a timer when the queue has been empty for a while...
	                if(progressBar) {
	                	if(messageQueue.size() == 0 && len > 0) {
	                		progressBar.start(true);
	                	}
	                }
	            	
	                for(var i=0; i<len; i++) {
	                	var key = data[i];
	                	messageQueue.enqueue(key);
	                }
	                
	                // Update termMillis only when the current request is successful.... (emtpy list is considered success)
	                // TBD: This can potentially cause the fetch stuck at one timestamp...
	                termMillis += termDelta;
	                if(DEBUG_ENABLED) console.log("fetch(): Updated termMillis = " + termMillis);
	            } else {
	            	// ???
	            	if(DEBUG_ENABLED) console.log("fetch() failed: messageListUrl = " + messageListUrl);
	            }
            },
            error: function(req, textStatus) {
                // ???
           	    if(DEBUG_ENABLED) console.log("fetch() error: status = ' + textStatus + '; messageListUrl = " + messageListUrl);
            }
        });
    
    };
    
    this.display = function() {
        ++displayCounter;
    	if(displayCounter > maxDisplays) {
    		// TBD: Kill the timer...
        	if(displayTimerID) {
        		window.clearTimeout(displayTimerID);
        	}
    		return;
    	}
        var that = this;

        if(DEBUG_ENABLED) console.log("display(): displayCounter = " + displayCounter);
        displayTimerID = window.setTimeout(function() { that.display(); }, displayMillis);	
    
        if(progressBar) {
        	progressBar.reset();
        } else {
        	// How to get the caller's callback function??
        	// progressBar = new webstoa.stream.ProgressBar(15000, 1500, 100, 0);
        	// progressBar.setDisplayCallback(refreshProgressBar);
        }

    	var messageKey = messageQueue.dequeue();
    	if(messageKey) {
    		if(DEBUG_ENABLED) console.log("display(): messageKey = " + messageKey);

            var messageFetchUrl = '/ajax/stream/key/' + messageKey;

            $.ajax({
            	type: "GET",
                url: messageFetchUrl,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(data, textStatus) {
    	    		var tis = that;
	        		if(DEBUG_ENABLED) console.log("display(): messageKey = " + messageKey + "; textStatus = " + textStatus);
	                
	                if(data) {
	                	var guid = data.guid;
	                	//alert("TBD: Display() guid = " + guid);
	                	//.....
	                	
	                	var fhortLink = cannyurl.wa.bean.ShortLinkJsBean.create(data);
	                	
	                	// tBD
	                	// if(fhortLink) {
	                	//	if(DEBUG_ENABLED) console.log(">>>>>>>>>>>>>>>>>>>> data = " + data);
	                	//	if(DEBUG_ENABLED) console.log(">>>>>>>>>>>>>>>>>>>> data.title = " + data.title);
	                	//	if(DEBUG_ENABLED) console.log(">>>>>>>>>>>>>>>>>>>> data.textTitle = " + data.textTitle);
	                	//	if(DEBUG_ENABLED) console.log(">>>>>>>>>>>>>>>>>>>> fhortLink = " + fhortLink);
	                	// }
	
	                	if(displayCallback) {
	                		// displayCallback(data);
	                		displayCallback(fhortLink);
	                	} else {
	                		if(DEBUG_ENABLED) console.log("display(): displayCallback is not set.");
	                	}
	                	
	                    if(progressBar) {
	                    	if(messageQueue.size() > 0) {
	                    		progressBar.start(true);
	                    	} else {
	                    		progressBar.reset();
	                    		if(DEBUG_ENABLED) console.log("display(): progressBar not started because messageQueue is empty.");                    		
	                    	}
	                    } else {
	                		if(DEBUG_ENABLED) console.log("display(): progressBar is not set.");                    	
	                    }
	
	                	
	                } else {
	                	// ???
	            		if(DEBUG_ENABLED) console.log("display() failed because fetch failed: messageFetchUrl = " + messageFetchUrl);
	                }
                },
                error: function(req, textStatus) {
                    // ???
               	    if(DEBUG_ENABLED) console.log("display() fetch error: status = ' + textStatus + '; messageFetchUrl = " + messageFetchUrl);
                }
            });

    	} else {
    		// ???
    		if(DEBUG_ENABLED) console.log("display(): No items found in the message queue");
    	}
    	
    };

  };

  return cls;
})();

