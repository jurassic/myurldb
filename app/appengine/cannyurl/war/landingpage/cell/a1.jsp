<%@ page import="com.cannyurl.af.util.*, com.cannyurl.af.auth.*, com.cannyurl.af.auth.user.*, com.cannyurl.util.*, com.cannyurl.app.util.*, com.cannyurl.helper.*"
%><%@ page contentType="text/html; charset=UTF-8" 
%><%
// ...
%><%
String brandDisplayName = request.getParameter("brandDisplayName");
%>
<h2>Tech News in 2 Minutes</h2>
<p>
In a generation where new apps pop up practically every minute 
and Facebook is in the news as often as the presidential campaign,
it’s tough to keep up. 
To really be on top of the latest in technology you have to slog through site after site 
and read pages of info. 
Instead, why not check out <%=brandDisplayName%>? 
<%=brandDisplayName%> features brief, easy to follow blog summaries to give you just enough news without bogging you down. 
Use it on your lunch break, your commute, or just when you have two minutes to kill. 
Let us make the news easy for you!
</p>
