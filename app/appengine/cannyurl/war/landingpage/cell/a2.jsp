<%@ page import="com.cannyurl.af.util.*, com.cannyurl.af.auth.*, com.cannyurl.af.auth.user.*, com.cannyurl.util.*, com.cannyurl.app.util.*, com.cannyurl.helper.*"
%><%@ page contentType="text/html; charset=UTF-8" 
%><%
// ...
%><%
String brandDisplayName = request.getParameter("brandDisplayName");
%>
<h2>Preview Your Favorite Blogs</h2>
<p>     
Glance through a few summaries on <%=brandDisplayName%>, 
and if something catches your eye you can easily link to the original article for the full story. 
If not, you haven’t wasted your time on stories you’re not interested in. 
Either way, it’s a great way to stay on top of the most important developments as they happen 
and even impress your friends with your tech savvy! 
Even a little knowledge on the latest releases can go a long way, 
and you can always find the original blog, straight from our site, on your computer or mobile device.       
</p>
