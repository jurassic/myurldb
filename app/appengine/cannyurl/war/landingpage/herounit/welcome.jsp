<%@ page import="com.cannyurl.af.util.*, com.cannyurl.af.auth.*, com.cannyurl.af.auth.user.*, com.cannyurl.util.*, com.cannyurl.app.util.*, com.cannyurl.helper.*"
%><%@ page contentType="text/html; charset=UTF-8" 
%><%
// ...
%><%
String brandDisplayName = request.getParameter("brandDisplayName");
%>
<h1>Blog Digest Service</h1>
<p> 
Too many interesting blogs to follow?
Too many good articles to read?
But, too little time to read them all?
If so, then you've come to the right place.
<%=brandDisplayName%> is a blog digest service.
We provide daily digest of select tech blog articles, written by professional writers.
Please sign up now to be the first users of <%=brandDisplayName%>.
We are currently in "private beta",
and we'd appreciate your feedback.
</p>
<p>
<a id="anchor_mainmenu_signup" class="btn btn-primary btn-large" style="width:85px;" title="Please sign up now, and we will keep you posted on our progress at <%=brandDisplayName%>.">Sign up &raquo;</a>
<a id="anchor_mainmenu_showsample" class="btn btn-primary btn-large" style="width:85px;" title="View sample article summaries.">Samples &raquo;</a>
</p>
