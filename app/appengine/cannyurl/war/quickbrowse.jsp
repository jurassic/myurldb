<%@ page import="com.myurldb.ws.core.*, com.cannyurl.af.util.*, com.cannyurl.af.auth.*, com.cannyurl.fe.*, com.cannyurl.fe.core.*, com.cannyurl.fe.bean.*, com.cannyurl.wa.service.*, com.cannyurl.app.util.*, com.cannyurl.util.*, com.cannyurl.helper.*"
%><%@ page contentType="text/html; charset=UTF-8" 
%><%String requestUrl = request.getRequestURL().toString();
String topLevelUrl = UrlHelper.getInstance().getTopLevelURLFromRequestURL(requestUrl);
Long paramOffset = QueryParamUtil.getParamOffset(request);
Integer paramCount = QueryParamUtil.getParamCount(request);%><!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Short URL List (TBD)</title>

<link rel="stylesheet" type="text/css" href="/css/bootstrap/bootstrap/bootstrap-2.0.css">
<style>
#quickbrowse_table {
  border: none;
  margin: 8px;
  padding: 2px;
}
#quickbrowse_table tr {
  border: 1px solid;
  padding: 0px;
}
#quickbrowse_table tr td {
  border: 1px solid;
  padding: 4px;
}
</style>
</head>
<body>

<table id="quickbrowse_table" class="table table-striped">
<tr><td>Last&nbsp;Modified&nbsp;Time&nbsp;</td><td>Short URL</td><td>Long URL</td></tr>
<%
java.util.List<ShortLinkJsBean> signups = SitemapHelper.getInstance().findRecentShortLinks(paramOffset, paramCount);
if(signups != null && !signups.isEmpty()) {
    for(ShortLinkJsBean m : signups) {
        String domain = m.getDomain();
        String token = m.getToken();
        String permalink = ShortLinkUtil.buildShortUrl(domain, ShortLinkUtil.REDIRECT_PATH_CONFIRM, token);
        String longUrl = m.getLongUrl();
        String truncLongUrl = longUrl;
        int longLen = longUrl.length();
        if(longLen > 100) {   // arbitrary
        	truncLongUrl = longUrl.substring(0, 100 - 3) + "...";
        }
        if(permalink != null && permalink.length() > 0) {
                Long modifiedTime = m.getModifiedTime();
                if(modifiedTime == null || modifiedTime == 0L) {
                    modifiedTime = m.getCreatedTime();
                }
                String modifiedDateTime = DateUtil.formatDate(modifiedTime);
%>
   <tr>
      <td><%=modifiedDateTime%></td>
      <td><a href="<%=permalink%>"><%=permalink%></a></td>
      <td><a href="<%=longUrl%>"><%=truncLongUrl%></a></td>
   </tr>
<%
        }
    }
}
%>
</table>

</body>
</html>

