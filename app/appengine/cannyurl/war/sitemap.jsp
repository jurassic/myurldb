<%@ page import="com.myurldb.ws.core.*, com.cannyurl.af.auth.*, com.cannyurl.fe.*, com.cannyurl.fe.core.*, com.cannyurl.fe.bean.*, com.cannyurl.fe.util.*, com.cannyurl.wa.service.*, com.cannyurl.util.*, com.cannyurl.app.util.*, com.cannyurl.helper.*" 
%><%@ page contentType="application/xml; charset=UTF-8" 
%><%
// "Branding"
String appBrand = BrandingHelper.getInstance().getAppBrand();
String brandDisplayName = BrandingHelper.getInstance().getBrandDisplayName();
// ...
%><?xml version="1.0" encoding="UTF-8"?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">

<%
String requestUrl = request.getRequestURL().toString();
String topLevelUrl = UrlHelper.getInstance().getTopLevelURLFromRequestURL(requestUrl);
if(topLevelUrl != null && !topLevelUrl.isEmpty()) {
    String yesterday = SitemapHelper.formatDate(new java.util.Date().getTime() - 24*3600000L);
    if(yesterday == null || yesterday.isEmpty()) {  // This should not happen.
        yesterday = "2013-03-10";   // Just use an arbitrary date...
    }
    String weekAgo = SitemapHelper.formatDate(new java.util.Date().getTime() - 24*7*3600000L);
    if(weekAgo == null || weekAgo.isEmpty()) {  // This should not happen.
       weekAgo = "2013-03-10";   // Just use an arbitrary date...
    }
%>
   <sitemap>
      <loc><%=topLevelUrl%>sitemap0.xml</loc>
      <lastmod><%=weekAgo%></lastmod>
   </sitemap>
   <sitemap>
      <loc><%=topLevelUrl%>sitemap1.xml</loc>
      <lastmod><%=yesterday%></lastmod>
   </sitemap>
   <sitemap>
      <loc><%=topLevelUrl%>sitemap2.xml</loc>
      <lastmod><%=yesterday%></lastmod>
   </sitemap>
<%
}
%>

</sitemapindex>
