<%@ page import="com.myurldb.ws.core.*, com.cannyurl.af.auth.*, com.cannyurl.fe.*, com.cannyurl.fe.core.*, com.cannyurl.fe.bean.*, com.cannyurl.fe.util.*, com.cannyurl.wa.service.*, com.cannyurl.util.*, com.cannyurl.app.util.*, com.cannyurl.helper.*" 
%><%@ page contentType="application/xml; charset=UTF-8" 
%><?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">

<%
String requestUrl = request.getRequestURL().toString();
String topLevelUrl = UrlHelper.getInstance().getTopLevelURLFromRequestURL(requestUrl);
if(topLevelUrl != null && !topLevelUrl.isEmpty()) {
    String homePageUrl = topLevelUrl + "home";
    String aboutPageUrl = topLevelUrl + "about";
    String contactPageUrl = topLevelUrl + "contact";   // TBD...
    String helpPageUrl = topLevelUrl + "help";

    String editPageUrl = topLevelUrl + "shorten";
    String verifyPageUrl = topLevelUrl + "verify";

    String streamPageUrl = topLevelUrl + "stream";

    String frontPageUrl = ConfigUtil.getUiWebAppFrontPage();

    String yesterday = SitemapHelper.formatDate(new java.util.Date().getTime() - 24*3600000L);
    if(yesterday == null || yesterday.isEmpty()) {  // This should not happen.
        yesterday = "2013-03-10";   // Just use an arbitrary date...
    }
    String weekAgo = SitemapHelper.formatDate(new java.util.Date().getTime() - 24*7*3600000L);
    if(weekAgo == null || weekAgo.isEmpty()) {  // This should not happen.
       weekAgo = "2013-03-10";   // Just use an arbitrary date...
    }
    String monthAgo = SitemapHelper.formatDate(new java.util.Date().getTime() - 24*30*3600000L);
    if(monthAgo == null || monthAgo.isEmpty()) {  // This should not happen.
       monthAgo = "2013-03-10";   // Just use an arbitrary date...
    }
%>
<% if(frontPageUrl == null || frontPageUrl.isEmpty() || frontPageUrl.equals("/home")) { %>
   <url>
      <loc><%=homePageUrl%></loc>
      <lastmod><%=monthAgo%></lastmod>
      <changefreq>monthly</changefreq>
      <priority>0.4</priority>
   </url>
<% } %>
   <url>
      <loc><%=aboutPageUrl%></loc>
      <lastmod><%=monthAgo%></lastmod>
      <changefreq>monthly</changefreq>
      <priority>0.6</priority>
   </url>
<!--
   <url>
      <loc><%=contactPageUrl%></loc>
      <lastmod><%=monthAgo%></lastmod>
      <changefreq>monthly</changefreq>
      <priority>0.5</priority>
   </url>
-->
   <url>
      <loc><%=helpPageUrl%></loc>
      <lastmod><%=monthAgo%></lastmod>
      <changefreq>monthly</changefreq>
      <priority>0.6</priority>
   </url>

   <url>
      <loc><%=editPageUrl%></loc>
      <lastmod><%=monthAgo%></lastmod>
      <changefreq>monthly</changefreq>
      <priority>0.5</priority>
   </url>
   <url>
      <loc><%=verifyPageUrl%></loc>
      <lastmod><%=monthAgo%></lastmod>
      <changefreq>monthly</changefreq>
      <priority>0.5</priority>
   </url>

   <url>
      <loc><%=streamPageUrl%></loc>
      <lastmod><%=yesterday%></lastmod>
      <changefreq>daily</changefreq>
      <priority>0.3</priority>
   </url>
<%
}
%>

</urlset>
