<%@ page import="com.myurldb.ws.core.*, com.cannyurl.af.auth.*, com.cannyurl.fe.*, com.cannyurl.fe.core.*, com.cannyurl.fe.bean.*, com.cannyurl.wa.service.*, com.cannyurl.app.util.*, com.cannyurl.util.*, com.cannyurl.helper.*"
%><%@ page contentType="application/xml; charset=UTF-8" 
%><?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">

<%
String requestUrl = request.getRequestURL().toString();
String topLevelUrl = UrlHelper.getInstance().getTopLevelURLFromRequestURL(requestUrl);
%>
<%
String yesterday = SitemapHelper.formatDate(new java.util.Date().getTime() - 24*3600000L);
if(yesterday == null || yesterday.isEmpty()) {  // This should not happen.
    yesterday = "2013-03-10";   // Just use an arbitrary date...
}
String aWeekAgo = SitemapHelper.formatDate(new java.util.Date().getTime() - 7 * 24*3600000L);
if(aWeekAgo == null || aWeekAgo.isEmpty()) {  // This should not happen.
    aWeekAgo = "2013-03-10";   // Just use an arbitrary date...
}
%>

<%
// TBD: Read the count from the config file???
// Testing. Start with a small number.... (Note the 30 sec limit for request....)
// One easy way to circumvent this is make multiple calls. e.g., from 0~499, 500~999, 1000~1499, etc...
// Increase this number after enabling billing...
// For now, just use 100....
int maxCount = 100;
java.util.List<ShortLinkJsBean> signups = SitemapHelper.getInstance().findRecentShortLinks(maxCount);  // ?????
if(signups != null && !signups.isEmpty()) {
    for(ShortLinkJsBean m : signups) {
        String domain = m.getDomain();
        String token = m.getToken();
        String permalink = ShortLinkUtil.buildShortUrl(domain, ShortLinkUtil.REDIRECT_PATH_CONFIRM, token);
        // TBD:
        // Filter out urls based on domain and topLevelUrl ????
        // ....
        if(permalink != null && permalink.length() > 0) {
            if(permalink.startsWith(topLevelUrl)) {
                Long modifiedTime = m.getModifiedTime();
                if(modifiedTime == null || modifiedTime == 0L) {
                    modifiedTime = m.getCreatedTime();
                }
                String lastModified = SitemapHelper.formatDate(modifiedTime);
%>
   <url>
      <loc><%=permalink%></loc>
<%
                if(lastModified != null && !lastModified.isEmpty()) {
%>
      <lastmod><%=lastModified%></lastmod>
<%
                }
%>
      <changefreq>monthly</changefreq>
   </url>
<%
            }
        }
    }
}
%>

</urlset>
