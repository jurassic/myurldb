<%@ page import="com.myurldb.ws.core.*, com.cannyurl.af.auth.*, com.cannyurl.fe.*, com.cannyurl.fe.core.*, com.cannyurl.fe.bean.*, com.cannyurl.fe.util.*, com.cannyurl.wa.service.*, com.cannyurl.util.*, com.cannyurl.app.util.*, com.cannyurl.helper.*" 
%><%@ page contentType="application/xml; charset=UTF-8" 
%><?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">

<%
String requestUrl = request.getRequestURL().toString();
String topLevelUrl = UrlHelper.getInstance().getTopLevelURLFromRequestURL(requestUrl);
if(topLevelUrl != null && !topLevelUrl.isEmpty()) {
    // TBD:
    String landingPage1 = topLevelUrl + "lp/welcome?t=Tech+Blog+Digest+Service+-+Article+Summaries+by+Professional+Writers";
    String pressRelease1 = topLevelUrl + "pr/20120925";
    String siteHelp1 = topLevelUrl + "sitehelp/Tech+News+Sites";

    // TBD: 
    String siteLogList = topLevelUrl + "blog";
    // TBD: Each blog/sitelog posts???
    
    String yesterday = SitemapHelper.formatDate(new java.util.Date().getTime() - 24*3600000L);
    if(yesterday == null || yesterday.isEmpty()) {  // This should not happen.
        yesterday = "2012-09-10";   // Just use an arbitrary date...
    }
    String weekAgo = SitemapHelper.formatDate(new java.util.Date().getTime() - 24*7*3600000L);
    if(weekAgo == null || weekAgo.isEmpty()) {  // This should not happen.
       weekAgo = "2012-09-10";   // Just use an arbitrary date...
    }
    String monthAgo = SitemapHelper.formatDate(new java.util.Date().getTime() - 24*30*3600000L);
    if(monthAgo == null || monthAgo.isEmpty()) {  // This should not happen.
       monthAgo = "2012-09-10";   // Just use an arbitrary date...
    }
%>
<!--
   <url>
      <loc><%=landingPage1%></loc>
      <lastmod><%=weekAgo%></lastmod>
      <changefreq>weekly</changefreq>
      <priority>0.6</priority>
   </url>
   <url>
      <loc><%=pressRelease1%></loc>
      <lastmod><%=weekAgo%></lastmod>
      <changefreq>monthly</changefreq>
      <priority>0.4</priority>
   </url>
   <url>
      <loc><%=siteHelp1%></loc>
      <lastmod><%=monthAgo%></lastmod>
      <changefreq>monthly</changefreq>
      <priority>0.1</priority>
   </url>
-->
   <url>
      <loc><%=siteLogList%></loc>
      <lastmod><%=weekAgo%></lastmod>
      <changefreq>weekly</changefreq>
      <priority>0.7</priority>
   </url>
<%
}
%>

</urlset>
