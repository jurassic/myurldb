/*
"Brand"
*/

// Global var
var APP_BRAND = "cannyurl";

function getWebServiceEndPoint() {
    return getCannyUrlServiceEndPoint();
}
function getCannyUrlServiceEndPoint() {
    var wsEndpoint = 'http://www.su2.us/wa/shortlink/';
    return wsEndpoint;
}

function getShortLinkRootDomain() {
    return getCannyUrlShortLinkRootDomain();
}
function getCannyUrlShortLinkRootDomain() {
    var rootDomain = 'su2.us';
    return rootDomain;
}
