/*
Mapping event handlers to events.
*/


$(function() {

    $("#createNewButton").live('click', function() {
        // document.getElementById("createNewButton").addEventListener("click",createNewShortUrl, false);
        createNewShortUrl();
        return false;
    });

    $("#cancelButton").click(function() {
        cancelShortUrl();
        return false;
    });


    $("#copyButton").click(function() {
        copyShortUrl();
        return false;
    });

    $("#tweetButton").click(function() {
        openRemoteTweetPage();
        return false;
    });

});
