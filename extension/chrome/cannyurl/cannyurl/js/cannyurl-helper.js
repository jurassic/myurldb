/*
*/


function createBrandedShortUrl(tabUrl, message) {
    return createCannyUrl(tabUrl, message);
}
function saveBrandedShortLink(shortLink) {
    saveCannyUrlShortLink(shortLink);
}
function refreshBrandedShortLink(shortLink) {
    refreshCannyUrlShortLink(shortLink);
}

function createCannyUrl(tabUrl, message) {
    return createShortUrl(tabUrl, 'short', 'confirm', message);
}
function saveCannyUrlShortLink(shortLink) {
    saveShortLink(shortLink, getCannyUrlServiceEndPoint());
}
function refreshCannyUrlShortLink(shortLink) {
    refreshShortLink(shortLink, getCannyUrlServiceEndPoint());
}
