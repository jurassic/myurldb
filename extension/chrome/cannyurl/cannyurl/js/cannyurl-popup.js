/*
*/

// Global var!
var shortLinkObj;

// $(document).ready(
// windowLoaded();
// );

    window.addEventListener("load", windowLoaded, false);
    function windowLoaded() {
      chrome.tabs.getSelected(null, function(tab) {
	    var bkg = chrome.extension.getBackgroundPage();
	    var tabUrl = tab.url;

		// TBD: Validation....
		if(! isUrl(tabUrl) ) {   // temporary
			alert("Current tab is not a Web page: long URL = " + tabUrl);
			window.close();
            return;
		}
		if( tabUrl.length > 400 ) {   // temporary
			alert("Current page URL is too long: long URL = " + tabUrl);
			window.close();
            return;
		}

		// Call the background page function.
		shortLinkObj = bkg.findShortUrlEntry(tabUrl, true);

		// Duplicate codes...
		// TBD: validation, etc.
        if(shortLinkObj) {
            var domain = shortLinkObj.domain;
            var token = shortLinkObj.token;
            var shortUrl = makeShortUrl(domain, token);
            $( "#shortUrlText" ).val( shortUrl );
            var message = shortLinkObj.displayMessage;
            $( "#displayMessageText" ).val( message );
            if(! shortLinkObj.readOnly || shortLinkObj.readOnly != true) {   // ????
                $('#displayMessageText').removeAttr('readonly');
            } else {
                $('#displayMessageText').attr('readonly','readonly');
            }
        }

        // Ping PageSynopsis if necessary.
        bkg.refreshPageInfoMap(tabUrl);
        // ...
      });
    }

    
$(function() {
    // select() ????
    var currentVal = $("#displayMessageText").val();
    if(currentVal) {
        $("#displayMessageText").focus().val('').val(currentVal);
    } else {
        $("#displayMessageText").focus();
    }
});


    function disableCreateNewButton(delay)
    {
    	$("#createNewButton").attr('disabled','disabled');
    	var milli;
    	if(delay) {
    		milli = delay;
    	} else {
    		milli = 2500;  // ???
    	}
        // TBD:
    	// setTimeout(resetAndEnableCreateNewButton, milli);
        // ...
    }
    function enableCreateNewButton()
    {
        // TBD:
    	// setTimeout(resetAndEnableCreateNewButton, 1250);  // ???
        // ....
        
        // ...
        resetAndEnableCreateNewButton();
    }
    function resetAndEnableCreateNewButton()
    {
    	$("#createNewButton").removeAttr('disabled');
    }

function updateChanges() 
{
  if(! shortLinkObj) {
    // This should not happen.
    return;
  }
  if(! shortLinkObj.readOnly || shortLinkObj.readOnly != true) {   // ????
    shortLinkObj.readOnly = true;
	$('#displayMessageText').attr('readonly','readonly');

    // TBD: validation, etc.
    var msg = $( "#displayMessageText" ).val();
	//alert("msg = " + msg);
    var msgLength = msg.length;
    if(msgLength > 500) {
        msg = msg.substring(0, 500);
    }
    shortLinkObj.displayMessage = msg;

    var bkg = chrome.extension.getBackgroundPage();
    shortLinkObj = bkg.updateShortUrl(shortLinkObj);
  }
}
function copyShortUrl() 
{
  if(! shortLinkObj) {
    // This should not happen.
    alert("Unexpected error: cannot copy the short URL.");
    window.close();
    return;
  }
  
  updateChanges();

  $( "#shortUrlText" ).focus();
  $( "#shortUrlText" ).select();
  document.execCommand("Copy");
  
  window.close();
}
function cancelShortUrl() {
  // We don't actaully cancel anything (bc the shortLink has already been created).
  // Note: If the displayMessage field has been updated, that will be "canceled".
  // Just dismiss the popup window, for now.
  window.close();
}

// Note: to avoid abuse.
// if the user reaches the max, he/she needs to close/reopen the popup window.
var MAX_SHORTLINK_COUNT = 3;
var shortlink_create_counter = 0;
function createNewShortUrl() 
{
  // TBD: Long url (tab.url) could have been saved in windowLoaded().....
  if(! shortLinkObj) {
    // This should not happen.
    alert("Unexpected error: cannot create a new short URL.");
    window.close();
    return;
  }

  var longUrl = shortLinkObj.longUrl;  
  //var message = shortLinkObj.displayMessage;
  var message = $( "#displayMessageText" ).val();
  var messageLength = message.length;
  if(messageLength > 500) {
      message = message.substring(0, 500);
  }

  var bkg = chrome.extension.getBackgroundPage();
  shortLinkObj = bkg.createBrandedShortUrl(longUrl, message);
  // shortLinkObj = createBrandedShortUrl(longUrl, message);

  // Duplicate codes...
  // TBD: validation, etc.
  var domain = shortLinkObj.domain;
  var token = shortLinkObj.token;
  var shortUrl = makeShortUrl(domain, token);
  $( "#shortLink" ).html( shortUrl );
  $( "#shortUrlText" ).val( shortUrl );
  var message = shortLinkObj.displayMessage;
  $( "#displayMessageText" ).val( message );
  if(! shortLinkObj.readOnly || shortLinkObj.readOnly != true) {   // ????
    $('#displayMessageText').removeAttr('readonly');
  } else {
    $('#displayMessageText').attr('readonly','readonly');
  }
  
  // Prevent abuse
  // TBD:
  shortlink_create_counter++;
  if(shortlink_create_counter >= MAX_SHORTLINK_COUNT) {
    disableCreateNewButton();
  }
  // ...
}


// Canny URL / Flash URL version
function openRemoteTweetPage()
{
  // Save any changes first...
  updateChanges();

  // TBD
  var shortUrl = $( "#shortUrlText" ).val();
  var displayMessage = $( "#displayMessageText" ).val().trim();
  var mainMessage = displayMessage;
  // if(! mainMessage) {
  //   if(shortLinkObj) {
  //     var longUrl = shortLinkObj.longUrl;
  //     var bkg = chrome.extension.getBackgroundPage();
  //     var pageDescription = bkg.getWebPageDescription(longUrl);
  //     if(pageDescription) {
  //       mainMessage = pageDescription.trim();
  //     } else {
  //       var pageTitle = bkg.getWebPageTitle(longUrl);
  //       if(pageTitle) {
  //         mainMessage = pageTitle.trim();
  //       }
  //     }
  //   }
  // }
  if(mainMessage) {
    var remoteTweetPageWithMessage = 'http://beta.tweetmor.com/status/edit?message=';

    var tweetMessage = ' - ' + shortUrl;
    var tmLen = tweetMessage.length;
    var dmLen = mainMessage.length;
    var remaining = 140 - tmLen - 21;  // 21-3=18: buffer...
    if(remaining >= dmLen) {
      tweetMessage = mainMessage + tweetMessage;
    } else {
      tweetMessage = mainMessage.substring(0, remaining) + '...' + tweetMessage;
    }

    var remoteTweetPageUrl = remoteTweetPageWithMessage + encodeURIComponent(tweetMessage);
    chrome.tabs.create({'url':remoteTweetPageUrl}, function(tab) {}); 
  } else {
    var remoteTweetPageWithUrl = 'http://beta.tweetmor.com/status/edit?webpage=';

    var webPageUrl;
    if(shortLinkObj) {
      var longUrl = shortLinkObj.longUrl;
      webPageUrl = longUrl;
    }

    if(webPageUrl) {
      var remoteTweetPageUrl = remoteTweetPageWithUrl + encodeURIComponent(webPageUrl);
      chrome.tabs.create({'url':remoteTweetPageUrl}, function(tab) {}); 
    }
  }
}

/*
// TBD:
function openRemoteTweetPage()
{
  // Save any changes first...
  updateChanges();

  var remoteTweetPage = 'http://www.su2.us/tweet?message=';
  var shortUrl = $( "#shortUrlText" ).val();
  var displayMessage = $( "#displayMessageText" ).val().trim();

  var mainMessage = displayMessage;
  if(!mainMessage) {
    var bkg = chrome.extension.getBackgroundPage();
    var longUrl = shortLinkObj.longUrl;
    var pageDescription = bkg.getWebPageDescription(longUrl);
    if(pageDescription) {
      mainMessage = pageDescription;
    } else {
      var pageTitle = bkg.getWebPageTitle(longUrl);
      if(pageTitle) {
        mainMessage = pageTitle;
      }
    }
  }

  var tweetMessage = ' - ' + shortUrl;
  if(mainMessage) {
    var tmLen = tweetMessage.length;
    var dmLen = mainMessage.length;
    var remaining = 140 - tmLen - 21;  // 21-3=18: buffer...
    if(remaining >= dmLen) {
       tweetMessage = mainMessage + tweetMessage;
    } else {
       tweetMessage = mainMessage.substring(0, remaining) + '...' + tweetMessage;
    }
  }
  var remoteTweetPageUrl = remoteTweetPage + encodeURIComponent(tweetMessage);
  chrome.tabs.create({'url':remoteTweetPageUrl}, function(tab) {}); 
}
*/
