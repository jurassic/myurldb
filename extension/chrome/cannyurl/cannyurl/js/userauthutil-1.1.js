/*
User/Auth-related utiliity functions.
*/

// TBD: Do away with a global var ???
// Valid throughout a browser session ????
var userGuid;
function getUserGuid() 
{
  if(userGuid === undefined) {
	// TBD:
	// Get the new user from the User DB/table???
	// ...    
    // for now, get it from the "local storage"
    if(window.localStorage != null) {
       if(window.localStorage.userGuid) {
           userGuid = window.localStorage.userGuid;
       }
    }
    
    if(! userGuid) {
        userGuid = generateUuid();        
        // TBD:
        // Insert the new user into the User DB/table???
        // ...        
        // for now, store it on the "local storage"
        if(window.localStorage != null) {
            window.localStorage.userGuid = userGuid;
        }
    }
  }
  return userGuid;
}
