/*
"Brand"
*/

// Global var
var APP_BRAND = "femtourl";

function getWebServiceEndPoint() {
    return getFemtoUrlServiceEndPoint();
}
function getFemtoUrlServiceEndPoint() {
    var wsEndpoint = 'http://www.fm.gs/wa/shortlink/';
    return wsEndpoint;
}

function getShortLinkRootDomain() {
    return getFemtoUrlShortLinkRootDomain();
}
function getFemtoUrlShortLinkRootDomain() {
    var rootDomain = 'fm.gs';
    return rootDomain;
}
