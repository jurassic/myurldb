/*
Mapping event handlers to events.
*/


$(function() {

    $("#createNewButton").click(function() {
        createNewShortUrl();
        return false;
    });

    $("#cancelButton").click(function() {
        cancelShortUrl();
        return false;
    });

    $("#copyButton").click(function() {
        copyShortUrl();
        return false;
    });

    $("#tweetButton").click(function() {
        openRemoteTweetPage();
        return false;
    });

    $("#copy_femtourl_cannyurl").click(function() {
        copyShortUrl('cannyurl');
        return false;
    });

    $("#tweet_femtourl_cannyurl").click(function() {
        openRemoteTweetPage('cannyurl');
        return false;
    });

    $("#copy_femtourl_flashurl").click(function() {
        copyShortUrl('flashurl');
        return false;
    });

    $("#tweet_femtourl_flashurl").click(function() {
        openRemoteTweetPage('flashurl');
        return false;
    });

});
