/*
*/

function createBrandedShortUrl(tabUrl, message) {
    return createFemtoUrl(tabUrl, message);
}
function saveBrandedShortLink(shortLink) {
    saveFemtoUrlShortLink(shortLink);
}
function refreshBrandedShortLink(shortLink) {
    refreshFemtoUrlShortLink(shortLink);
}

function createFemtoUrl(tabUrl) {
    return createShortUrl(tabUrl, 'medium', 'permanent');
}
function saveFemtoUrlShortLink(shortLink) {
    saveShortLink(shortLink, getFemtoUrlServiceEndPoint());
}
function refreshFemtoUrlShortLink(shortLink) {
    refreshShortLink(shortLink, getFemtoUrlServiceEndPoint());
}
