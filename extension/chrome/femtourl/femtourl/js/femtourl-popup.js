/*
*/

// Global var!
var shortLinkObj;

    window.addEventListener("load", windowLoaded, false);
    function windowLoaded() {
      chrome.tabs.getSelected(null, function(tab) {
	    var bkg = chrome.extension.getBackgroundPage();
	    var tabUrl = tab.url;

		// TBD: Validation....
		if(! isUrl(tabUrl) ) {   // temporary
			alert("Current tab is not a Web page: long URL = " + tabUrl);
			window.close();
            return;
		}
		if( tabUrl.length > 400 ) {   // temporary
			alert("Current page URL is too long: long URL = " + tabUrl);
			window.close();
            return;
		}

		// Call the background page function.
		shortLinkObj = bkg.findShortUrlEntry(tabUrl, true);

		// Duplicate codes...
		// TBD: validation, etc.
        if(shortLinkObj) {
            var domain = shortLinkObj.domain;
            var token = shortLinkObj.token;

            var shortUrl = makeShortUrl(domain, token);
            $( "#shortUrlText" ).val( shortUrl );
            var cannyUrl = makeCannyUrl(domain, token);
            $( "#input_femtourl_cannyurl" ).val( cannyUrl );
            var flashUrl = makeFlashUrl(domain, token);
            $( "#input_femtourl_flashurl" ).val( flashUrl );
        }
        
        // Ping PageSynopsis if necessary.
        bkg.refreshPageInfoMap(tabUrl);
        // ...
      });
    }

    function disableCreateNewButton(delay)
    {
    	$("#createNewButton").attr('disabled','disabled');
    	var milli;
    	if(delay) {
    		milli = delay;
    	} else {
    		milli = 2500;  // ???
    	}
        // TBD:
    	// setTimeout(resetAndEnableCreateNewButton, milli);
        // ...
    }
    function enableCreateNewButton()
    {
        // TBD:
    	// setTimeout(resetAndEnableCreateNewButton, 1250);  // ???
        // ....
        
        // ...
        resetAndEnableCreateNewButton();
    }
    function resetAndEnableCreateNewButton()
    {
    	$("#createNewButton").removeAttr('disabled');
    }

function updateChanges() 
{
  if(! shortLinkObj) {
    // This should not happen.
    return;
  }
  if(! shortLinkObj.readOnly || shortLinkObj.readOnly != true) {   // ????
    shortLinkObj.readOnly = true;

    var bkg = chrome.extension.getBackgroundPage();
    shortLinkObj = bkg.updateShortUrl(shortLinkObj);
  }
}
function cancelShortUrl() {
  // We don't actaully cancel anything (bc the shortLink has already been created).
  // Just dismiss the popup window, for now.
  window.close();
}

// Note: to avoid abuse.
// if the user reaches the max, he/she needs to close/reopen the popup window.
var MAX_SHORTLINK_COUNT = 3;
var shortlink_create_counter = 0;
function createNewShortUrl() 
{
  // TBD: Long url (tab.url) could have been saved in windowLoaded().....
  if(! shortLinkObj) {
    // This should not happen.
    alert("Unexpected error: cannot create a new short URL.");
    window.close();
    return;
  }

  var longUrl = shortLinkObj.longUrl;  
  var bkg = chrome.extension.getBackgroundPage();
  shortLinkObj = bkg.createBrandedShortUrl(longUrl);
  // shortLinkObj = createBrandedShortUrl(longUrl);

  // Duplicate codes...
  // TBD: validation, etc.
  var domain = shortLinkObj.domain;
  var token = shortLinkObj.token;
  var shortUrl = makeShortUrl(domain, token);
  $( "#shortUrlText" ).val( shortUrl );
  var cannyUrl = makeCannyUrl(domain, token);
  $( "#input_femtourl_cannyurl" ).val( cannyUrl );
  var flashUrl = makeFlashUrl(domain, token);
  $( "#input_femtourl_flashurl" ).val( flashUrl );
  
  // Prevent abuse
  // TBD:
  shortlink_create_counter++;
  if(shortlink_create_counter >= MAX_SHORTLINK_COUNT) {
    disableCreateNewButton();
  }
  // ...
}

// TBD:
function copyShortUrl(mode) {
  if(! shortLinkObj) {
    // This should not happen.
    alert("Unexpected error: cannot copy the short URL.");
    window.close();
    return;
  }
  
  updateChanges();

  if(mode && mode == 'cannyurl') {
    $( "#input_femtourl_cannyurl" ).focus();
    $( "#input_femtourl_cannyurl" ).select();
    document.execCommand("Copy");
  } else if(mode && mode == 'flashurl') {
    $( "#input_femtourl_flashurl" ).focus();
    $( "#input_femtourl_flashurl" ).select();
    document.execCommand("Copy");
  } else {
    $( "#shortUrlText" ).focus();
    $( "#shortUrlText" ).select();
    document.execCommand("Copy");
  }
  
  window.close();
}


// Femto URL version.
function openRemoteTweetPage(mode)
{
  // Save any changes first...
  updateChanges();

  // TBD
  var remoteTweetPage = 'http://beta.tweetmor.com/status/edit?webpage=';
  var shortUrl = $( "#shortUrlText" ).val();
  if(mode) {
     if(mode == 'cannyurl') {
         shortUrl = $( "#input_femtourl_cannyurl" ).val();
     } else if(mode == 'flashurl') {
         shortUrl = $( "#input_femtourl_flashurl" ).val();
     } else {
        // ignore
     }
  }

  var webPageUrl;
  if(shortLinkObj) {
    var domain = shortLinkObj.domain;
    var token = shortLinkObj.token;
    var shortUrl;
    if(mode == 'cannyurl' || mode == 'flashurl') {
       shortUrl = makePermanentUrl(domain, token);
    } else {
       shortUrl = makeShortUrl(domain, token);
    }
    webPageUrl = shortUrl;
  }
  if(! webPageUrl) {    // This cannot happen... ???
    if(shortLinkObj) {
      var longUrl = shortLinkObj.longUrl;
      webPageUrl = longUrl;
    }
  }

  if(webPageUrl) {
    var remoteTweetPageUrl = remoteTweetPage + encodeURIComponent(webPageUrl);
    chrome.tabs.create({'url':remoteTweetPageUrl}, function(tab) {}); 
  }
}

/*
function openRemoteTweetPage(mode)
{
  // Save any changes first...
  updateChanges();

  var remoteTweetPage = 'http://www.fm.gs/tweet?message=';
  var shortUrl = $( "#shortUrlText" ).val();
  if(mode) {
     if(mode == 'cannyurl') {
         shortUrl = $( "#input_femtourl_cannyurl" ).val();
     } else if(mode == 'flashurl') {
         shortUrl = $( "#input_femtourl_flashurl" ).val();
     } else {
        // ignore
     }
  }
  
  var mainMessage = '';
  var bkg = chrome.extension.getBackgroundPage();
  var longUrl = shortLinkObj.longUrl;
  var pageDescription = bkg.getWebPageDescription(longUrl);
  if(pageDescription) {
      mainMessage = pageDescription;
  } else {
      var pageTitle = bkg.getWebPageTitle(longUrl);
      if(pageTitle) {
        mainMessage = pageTitle;
      }
  }

  var tweetMessage = ' - ' + shortUrl;
  if(mainMessage) {
    var tmLen = tweetMessage.length;
    var dmLen = mainMessage.length;
    var remaining = 140 - tmLen - 21;  // 21-3=18: buffer...
    if(remaining >= dmLen) {
       tweetMessage = mainMessage + tweetMessage;
    } else {
       tweetMessage = mainMessage.substring(0, remaining) + '...' + tweetMessage;
    }
  }
  var remoteTweetPageUrl = remoteTweetPage + encodeURIComponent(tweetMessage);
  chrome.tabs.create({'url':remoteTweetPageUrl}, function(tab) {}); 
}
*/
