/*
Maintains page/tab URL map.
Defines AJAX methods for ShortLink service.
*/

// Cache: { longUrl -> shortLink }
var UrlMap = {};
// Cache: { longUrl -> { title: xx, description: yy } }
var PageInfoMap = {};

function refreshPageInfoMap(longUrl)
{
  // TBD: Ping PageSynopsis
  if(!PageInfoMap[longUrl]) { // ???
    pingPageSynopsis(longUrl);
  }
}

function findShortUrlEntry(tabUrl, create) 
{
    if(! tabUrl) {
        // ????
        return;
    }

    var shortLink;
    if(tabUrl in UrlMap) {
        if(UrlMap[tabUrl].status === 'saved') {
           shortLink = UrlMap[tabUrl];
        } else {
           // ???
           // Resend the request???
           shortLink = UrlMap[tabUrl];
        }
    } else {
        // TBD:
        // Check the local storage...
        if(window.localStorage != null) {
            var localUrlMapStrVal = window.localStorage.localUrlMap;
            if(localUrlMapStrVal) {
                var localUrlMap = JSON.parse(localUrlMapStrVal);
                if(localUrlMap) {
                    shortLink = localUrlMap[tabUrl];
                    if(shortLink) {
                        UrlMap[tabUrl] = shortLink;
                    }
                }
            }
        }

        if(! shortLink) {
            if(create === true) {
                // Create a new one...
                shortLink = createBrandedShortUrl(tabUrl);
                // Note: createBrandedShortUrl() calls storeShortUrlEntry()...
            }
        }
    }
    return shortLink;
}

function storeShortUrlEntry(tabUrl, shortLink) 
{
    if(! tabUrl) {
        // ???
        return;
    }

    // Always overwrite an existing short url, if any...
    UrlMap[tabUrl] = shortLink;
    if(window.localStorage != null) {
        var localUrlMap;
        var localUrlMapStrVal = window.localStorage.localUrlMap;
        if(localUrlMapStrVal) {
            localUrlMap = JSON.parse(localUrlMapStrVal);
        } else {
            localUrlMap = {};
        }
        // if(localUrlMap) {
            localUrlMap[tabUrl] = shortLink;
            window.localStorage.localUrlMap = JSON.stringify(localUrlMap);
        // }
    }

}

function deleteShortUrlEntry(tabUrl) 
{
    if(! tabUrl) {
        // ????
        return;
    }

    delete UrlMap[tabUrl]; 
    if(window.localStorage != null) {
        var localUrlMap;
        var localUrlMapStrVal = window.localStorage.localUrlMap;
        if(localUrlMapStrVal) {
            localUrlMap = JSON.parse(localUrlMapStrVal);
        } else {
            localUrlMap = {};
        }
        // if(localUrlMap) {
            delete localUrlMap[tabUrl];
            window.localStorage.localUrlMap = JSON.stringify(localUrlMap);
        // }
    }
}


function createShortUrl(tabUrl, tokenType, redirectType, message, flashDuration, wsEndpoint) 
{
    // TBD: Duplicate codes.....
    var shortLink = {};
    shortLink.guid = generateUuid();
	shortLink.owner = getUserGuid();  // Note that this user may not exist in the User table....
    shortLink.longUrl = tabUrl;
    shortLink.domain = getShortUrlDomain();
    if(tokenType) {
        shortLink.tokenType = tokenType;
    } else {
        shortLink.tokenType = 'medium';
    }
    // shortLink.token = generateToken();
    if(shortLink.tokenType == 'short') {
        shortLink.token = generateToken(3);
    } else if(shortLink.tokenType == 'medium') {
        shortLink.token = generateToken(4);
    } else if(shortLink.tokenType == 'sassy') {
        shortLink.token = generateToken(5);
    } else {
        // ????
        shortLink.token = generateToken(5);
    }
    if(redirectType) {
        shortLink.redirectType = redirectType;
    } else {
        shortLink.redirectType = 'flash';
    }
    if(message) {
      shortLink.displayMessage = message;
    } else {
      shortLink.displayMessage = '';
    }
    if(flashDuration) {
      shortLink.flashDuration = flashDuration;
    } // else ???
    shortLink.readOnly = false;

    // Note:    
    storeShortUrlEntry(tabUrl, shortLink);
    saveShortLink(shortLink, wsEndpoint);

    return shortLink;
}

function updateShortUrl(shortLink, wsEndpoint) 
{
  // TBD
  if(shortLink) {
      var tabUrl = shortLink.longUrl;
      if(tabUrl) {
          storeShortUrlEntry(tabUrl, shortLink);
      }
      refreshShortLink(shortLink, wsEndpoint);
  }
  return shortLink;
}

function saveShortLink(shortLink, wsEndpoint) 
{
  var longUrl = shortLink.longUrl;  
  var payloadData = shortLink;

  if(! wsEndpoint) {
      wsEndpoint = getWebServiceEndPoint();
  }
  
  $.ajax(
    {
       url: wsEndpoint,
       type: "POST",
       async: true,
       timeout: 20000,
       dataType: "json",
       data: JSON.stringify(payloadData),
       contentType: "application/json; charset=UTF-8",
       success: function (result)
       {
          //alert("Success Response: " + JSON.stringify(result));
          //var shortUrl = result.domain + result.token;
          //$( "#shortLink" ).html( shortUrl ); 
          //alert("Success: Short URL = " + shortUrl);

          //var longUrl = result.longUrl;
          result.status = 'saved';
		  // TBD: Should it be saved back (bc attr has changed)???
          storeShortUrlEntry(longUrl, result);
          //alert("ShortLink successfully saved for longUrl = " + longUrl);
            
          //chrome.tabs.getCurrent(function(tab){
          //  view.alert("ShortLink successfully saved for longUrl = " + longUrl);
          //});          
        },
        error: function()
       {
          // TBD: Retry???
          // ...
          // Clear the cache...
          deleteShortUrlEntry(longUrl);   // ???????
          // This is kind of annoying...
          // How to display errors? Use "lastError" type flag (e.g., in Bkg page)
          //      and display the error on the popup page when it is open the next time ????
          // alert("Error occurred while saving longUrl = " + longUrl);
          // ...
		  // TBD: Retry???
		  // ...
       }
    });
  
  // Update the PageInfoMap entry
  refreshPageInfoMap(longUrl);
  // ....
}

function refreshShortLink(shortLink, wsEndpoint) 
{
  var guid = shortLink.guid;
  var longUrl = shortLink.longUrl;  
  var payloadData = shortLink;

  if(! wsEndpoint) {
      wsEndpoint = getWebServiceEndPoint();
  }

  $.ajax(
    {
       url: wsEndpoint + guid,
       type: "PUT",
       async: true,
       timeout: 20000,
       dataType: "json",
       data: JSON.stringify(payloadData),
       contentType: "application/json; charset=UTF-8",
       success: function (result)
       {
          // PUT currently does not return anything....
          //result.status = 'saved';
          //result.readOnly = true;  
          //UrlMap[longUrl] = result;
          
          var shortLink = findShortUrlEntry(longUrl, false);
          if(shortLink) {
              shortLink.status = 'saved';
              shortLink.readOnly = true;  
              storeShortUrlEntry(longUrl, shortLink);   // ????
              // alert("ShortLink successfully updated for longUrl = " + longUrl);
          } else {
              // ????
              // alert("ShortLink not updated for longUrl = " + longUrl);
          }
          // alert("ShortLink successfully updated for longUrl = " + longUrl);
       },
        error: function()
       {
          // TBD: Retry???
          // ...
          // Clear the cache... ???
          deleteShortUrlEntry(longUrl);   // ???????
          // This is kind of annoying...
          // How to display errors? Use "lastError" type flag (e.g., in Bkg page)
          //      and display the error on the popup page when it is open the next time ????
          // alert("Error occurred while updating longUrl = " + longUrl);
          // ....
		  // TBD: Retry???
		  // ...
       }
    });
  
  // Update the PageInfoMap entry
  refreshPageInfoMap(longUrl);
  // ....
}


function getWebPageTitle(longUrl) {
   var pageTitle = '';
   var pageInfo = PageInfoMap[longUrl];
   if(pageInfo) {
     pageTitle = pageInfo.title;
   }
   return pageTitle;
}
function getWebPageDescription(longUrl) {
   var pageDescription = '';
   var pageInfo = PageInfoMap[longUrl];
   if(pageInfo) {
     pageDescription = pageInfo.description;
   }
   return pageDescription;
}

