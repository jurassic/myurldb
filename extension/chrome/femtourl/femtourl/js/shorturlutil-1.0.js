/*
Short URL related utility functions.
*/

function getBase62Char(i)
{
	var c;
	if(i >= 0 && i < 26) {
		c = String.fromCharCode(i + 65);        // A == 65
	} else if(i >= 26 && i < 52) {
		c = String.fromCharCode(i - 26 + 97);   // a == 97
	} else {  // if(i >= 52 && i < 62) {
		c = String.fromCharCode(i - 52 + 48);   // 0 == 48
	}
	return c;
}
function getBase36Char(i)
{
	var c;
	if(i >= 0 && i < 26) {
		c = String.fromCharCode(i + 97);        // a == 97
	} else {  // if(i >= 26 && i < 36) {
		c = String.fromCharCode(i - 26 + 48);   // 0 == 48
	}
	return c;
}

function generateShortUrlToken62(length)
{
    var uuid = createUuidString();
	var firstpart = uuid.substr(0,8);   // First eight chars. (enough up to 5 char token)
	var base = parseInt(firstpart, 16); // ???

	var shortUrlToken = '';
	for(j=0; j<length; j++) {
		var tmp = Math.floor(base / 62);
		var rem = Math.floor(base - tmp * 62);
		base = tmp;
		shortUrlToken += getBase62Char(rem);
	}

	return shortUrlToken;
}

function generateShortUrlToken36(length)
{
    var uuid = createUuidString();
	var firstpart = uuid.substr(0,8);   // First eight chars. (enough up to 5 char token)
	var base = parseInt(firstpart, 16); // ???

	var shortUrlToken = '';
	for(j=0; j<length; j++) {
		var tmp = Math.floor(base / 36);
		var rem = Math.floor(base - tmp * 36);
		base = tmp;
		shortUrlToken += getBase36Char(rem);
	}

	return shortUrlToken;
}

function generateToken(length) 
{
    var DEFAULT_TOKEN_LENGTH = 4;  // temporary
    if(!length) {
        length = DEFAULT_TOKEN_LENGTH;
    }

    var token;
    if(length <= 3) {
        token = generateShortUrlToken62(3);
    } else {
        token = generateShortUrlToken36(length);
    }
    return token;
}

// Always include the trailing slash.
function getShortUrlDomain() {
  // temporary
  // var idx = Math.floor(Math.random()*26) + 97;  // 'a' == 97
  var idx = Math.floor(Math.random()*5) + 97 + 5;  // f ~ j
  return 'http://' + String.fromCharCode(idx) + '.' + getShortLinkRootDomain() + '/';
  //return 'http://flashurl3.appspot.com/';
}


function makeShortUrl(domain, token) {
  var shortUrl;
  if( domain.substr(-1) === "/" ) {
     shortUrl = domain + token;
  } else {
     shortUrl = domain + '/' + token;
  }
  return shortUrl;
}
function makeCannyUrl(domain, token) {
  var shortUrl;
  if( domain.substr(-1) === "/" ) {
     shortUrl = domain + "c/" + token;
  } else {
     shortUrl = domain + '/c/' + token;
  }
  return shortUrl;
}
function makeFlashUrl(domain, token) {
  var shortUrl;
  if( domain.substr(-1) === "/" ) {
     shortUrl = domain + "f/" + token;
  } else {
     shortUrl = domain + '/f/' + token;
  }
  return shortUrl;
}
function makePermanentUrl(domain, token) {
  var shortUrl;
  if( domain.substr(-1) === "/" ) {
     shortUrl = domain + "p/" + token;
  } else {
     shortUrl = domain + '/p/' + token;
  }
  return shortUrl;
}

