/*
"Brand"
*/

// Global var
var APP_BRAND = "flashurl";

function getWebServiceEndPoint() {
    return getFlashUrlServiceEndPoint();
}
function getFlashUrlServiceEndPoint() {
    var wsEndpoint = 'http://www.mq.ms/wa/shortlink/';
    return wsEndpoint;
}

function getShortLinkRootDomain() {
    return getFlashUrlShortLinkRootDomain();
}
function getFlashUrlShortLinkRootDomain() {
    var rootDomain = 'mq.ms';
    return rootDomain;
}
