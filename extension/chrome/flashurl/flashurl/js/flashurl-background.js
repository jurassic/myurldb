/*
*/

// Global vars...
var menuId1;
var menuId2a;
var menuId2b;
var menuId3;
var menuId4;
var menuId5a;
var menuId5b;
var menuId5c;
// for "verify page"
var menuId7;

function createShortLinkOnClick(info, tab)
{
  var infoMenuItemId = info.menuItemId;

  var shortUrl;
  if(infoMenuItemId == menuId1) {
    var linkUrl = info.linkUrl;
    //alert('linkUrl = ' + linkUrl);
    shortUrl = linkUrl
  } else if(infoMenuItemId == menuId2a || infoMenuItemId == menuId2b) {
    var mediaType  = info.mediaType;
    var srcUrl = info.srcUrl;
    //alert('mediaType = ' + mediaType);
    //alert('srcUrl = ' + srcUrl);
    shortUrl = srcUrl
  } else if(infoMenuItemId == menuId3) {
    var frameUrl = info.frameUrl;
    //alert('frameUrl = ' + frameUrl);
    shortUrl = frameUrl
  } else if(infoMenuItemId == menuId4) {
    var pageUrl = info.pageUrl;
    //alert('pageUrl = ' + pageUrl);
    shortUrl = pageUrl
  } else if(infoMenuItemId == menuId5a || infoMenuItemId == menuId5b || infoMenuItemId == menuId5c) {
     // selection..
     var selectionText = info.selectionText;
     // TBD...
  } else {
     //alert('Incorrect menuId');
  }

  if(shortUrl) {
    // TBD: Validation?
    if(isUrl(shortUrl)) {  // temporary
      var shortLink = findShortUrlEntry(shortUrl, true);
      if(shortLink) {
        var domain = shortLink.domain;
        var token = shortLink.token;
        var shortUrl = makeShortUrl(domain, token);
        //alert('shortUrl = ' + shortUrl);
        copyToClipboard(shortUrl);      

        refreshPageInfoMap(shortUrl);
      }
    } else {
      // ???
      // alert ???
      alert("The target is not a Web page URL: Link = " + shortUrl);
      //copyToClipboard('');  // Clear the clipboard???
    }
  } else {
    // TBD:
    // selection ???
  }
}
// Hack
function copyToClipboard(shortUrl)
{
  document.body.innerHTML = '<p><a href="'+shortUrl+'" target="_blank" >'+shortUrl+'</a><form style="margin-top: -35px; margin-left: -500px;"><input type="text" id="clipholder-flashurl-shorturl" value="'+shortUrl+'"></form></p>'
  document.getElementById("clipholder-flashurl-shorturl").select()
  document.execCommand("Copy") 
}

// TBD
function verifyShortUrlOnClick(info, tab)
{
  var infoMenuItemId = info.menuItemId;

  var shortUrl;
  if(infoMenuItemId == menuId7) {
    var linkUrl = info.linkUrl;
    //alert('linkUrl = ' + linkUrl);
    shortUrl = linkUrl
  }

  if(shortUrl) {
    var verifyPage = 'http://www.mq.ms/verify?pageurl=';
    var verifyPageUrl = verifyPage + encodeURIComponent(shortUrl);
    chrome.tabs.create({'url':verifyPageUrl}, function(tab) {}); 
  } else {
    // ???
  }
}


var contexts1 = ['link'];
//var urlPatterns = ['http://*/*'];
var menuTitle1 = 'Shorten link and copy';
menuId1 = chrome.contextMenus.create({"title": menuTitle1, "contexts": contexts1, "onclick": createShortLinkOnClick});

var contexts2a = ['image'];
var menuTitle2a = 'Shorten image URL and copy';
menuId2a = chrome.contextMenus.create({"title": menuTitle2a, "contexts": contexts2a, "onclick": createShortLinkOnClick});
var contexts2b = ['video', 'audio'];
var menuTitle2b = 'Shorten media URL and copy';
menuId2b = chrome.contextMenus.create({"title": menuTitle2b, "contexts": contexts2b, "onclick": createShortLinkOnClick});

var contexts3 = ['frame'];
var menuTitle3 = 'Shorten frame URL and copy';
menuId3 = chrome.contextMenus.create({"title": menuTitle3, "contexts": contexts3, "onclick": createShortLinkOnClick});

var contexts4 = ['page'];
var menuTitle4 = 'Shorten page URL and copy';
menuId4 = chrome.contextMenus.create({"title": menuTitle4, "contexts": contexts4, "onclick": createShortLinkOnClick});

/*
var contexts5 = ['selection'];
var menuTitle5a = 'Shorten links in the selelection';
menuId5a = chrome.contextMenus.create({"title": menuTitle5a, "contexts": contexts5, "onclick": createShortLinkOnClick});
var menuTitle5b = 'Copy selection with shortened links';
menuId5b = chrome.contextMenus.create({"title": menuTitle5b, "contexts": contexts5, "onclick": createShortLinkOnClick});
var menuTitle5c = 'Create a Scribble Memo and shorten its URL';
menuId5c = chrome.contextMenus.create({"title": menuTitle5c, "contexts": contexts5, "onclick": createShortLinkOnClick});
*/

var contexts7 = ['link'];
//var urlPatterns = ['http://*/*'];
var menuTitle7 = 'Verify link/short URL';
menuId7 = chrome.contextMenus.create({"title": menuTitle7, "contexts": contexts7, "onclick": verifyShortUrlOnClick});

