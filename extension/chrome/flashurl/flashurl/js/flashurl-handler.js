/*
Mapping event handlers to events.
*/


$(function() {

    $("#createNewButton").click(function() {
        createNewShortUrl();
        return false;
    });

    $("#cancelButton").click(function() {
        cancelShortUrl();
        return false;
    });

    $("#copyButton").click(function() {
        copyShortUrl();
        return false;
    });

    $("#tweetButton").click(function() {
        openRemoteTweetPage();
        return false;
    });

});
