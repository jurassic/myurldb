/*
*/

function createBrandedShortUrl(tabUrl, message) {
    return createFlashUrl(tabUrl, message);
}
function saveBrandedShortLink(shortLink) {
    saveFlashUrlShortLink(shortLink);
}
function refreshBrandedShortLink(shortLink) {
    refreshFlashUrlShortLink(shortLink);
}

function createFlashUrl(tabUrl, message, flashDuration) {
    return createShortUrl(tabUrl, 'medium', 'flash', message, flashDuration);
}
function saveFlashUrlShortLink(shortLink) {
    saveShortLink(shortLink, getFlashUrlServiceEndPoint());
}
function refreshFlashUrlShortLink(shortLink) {
    refreshShortLink(shortLink, getFlashUrlServiceEndPoint());
}
