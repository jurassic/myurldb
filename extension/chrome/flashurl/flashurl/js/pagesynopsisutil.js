/*
PageSynopsis utility functions.
Note that JSONP does not work. (Requires https and CSP)
*/

function pingPageSynopsis(longUrl)
{
/* */
  // TBD: Ping PageSynopsis
  var encodedLongUrl = encodeURIComponent(longUrl);
  // var pageInfoUrl = 'https://www.pagesynopsis.com/pageinfo?targetUrl=' +  encodedLongUrl + "&callback=?";
  var pageInfoUrl = 'http://www.pagesynopsis.com/pageinfo?targetUrl=' +  encodedLongUrl;
  $.getJSON(pageInfoUrl, function(data) {
     if(data) {
       var pageInfo = {};
       if(data.pageTitle) {
         pageInfo.title = data.pageTitle;
       } else {
         pageInfo.title = '';
       }
       if(data.pageDescription) {
         pageInfo.description = data.pageDescription;
       } else {
         pageInfo.description = '';
       }
       PageInfoMap[longUrl] = pageInfo;
       //alert("page info updated....");
     } else {
       // ???
     }
  }); 
  // ....
  //alert("pagesynopsis called....");
/* */
}
