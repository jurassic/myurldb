/*
"Brand"
*/

// Global var
var APP_BRAND = "sassyurl";

function getWebServiceEndPoint() {
    return getSassyUrlServiceEndPoint();
}
function getSassyUrlServiceEndPoint() {
    var wsEndpoint = 'http://www.oc.gs/wa/shortlink/';
    return wsEndpoint;
}

function getShortLinkRootDomain() {
    return getSassyUrlShortLinkRootDomain();
}
function getSassyUrlShortLinkRootDomain() {
    var rootDomain = 'oc.gs';
    return rootDomain;
}
