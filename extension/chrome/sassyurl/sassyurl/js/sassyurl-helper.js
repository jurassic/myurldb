/*
*/

function createBrandedShortUrl(tabUrl, message) {
    return createSassyUrl(tabUrl, message);
}
function saveBrandedShortLink(shortLink) {
    saveSassyUrlShortLink(shortLink);
}
function refreshBrandedShortLink(shortLink) {
    refreshSassyUrlShortLink(shortLink);
}

function createSassyUrl(tabUrl) {
    return createShortUrl(tabUrl, 'sassy', 'permanent');
}
function saveSassyUrlShortLink(shortLink) {
    saveShortLink(shortLink, getSassyUrlServiceEndPoint());
}
function refreshSassyUrlShortLink(shortLink) {
    refreshShortLink(shortLink, getSassyUrlServiceEndPoint());
}

