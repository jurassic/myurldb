package com.myurldb;

import java.io.IOException;
import javax.servlet.http.*;

@SuppressWarnings("serial")
public class MyUrlDBServlet extends HttpServlet
{
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws IOException
    {
        resp.setContentType("text/plain");
        resp.getWriter().println("Hello, world");
    }
}
