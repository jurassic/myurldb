package com.myurldb.cert.ws;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;


// OAuth consumer key registry.
public abstract class BaseConsumerRegistry
{
    private static final Logger log = Logger.getLogger(BaseConsumerRegistry.class.getName());

    // Consumer key-secret map.
    // TBD: Use a better data structure which reflects OAuthConsumerInfo.
    private Map<String, String> consumerSecretMap;
    
    protected Map<String, String> getBaseConsumerSecretMap()
    {
        consumerSecretMap = new HashMap<String, String>();

        // TBD...
        consumerSecretMap.put("b15e112d-746c-46ce-bba8-e3db05af0561", "eb339947-dceb-451b-8143-f740730f8cb5");  // MyUrlDB + MyUrlDBApp
        consumerSecretMap.put("2c158880-4f81-43db-9d8d-f9b3eee9bac9", "43d08e3d-853d-416d-b992-e2b3ca085116");  // MyUrlDB + QueryClient
        consumerSecretMap.put("aae7f75a-7127-405c-9e3d-c439131930f3", "872c3832-9eb0-4749-ad41-7adb88798893");  // MyUrlDB + CannyUrlApp
        consumerSecretMap.put("e4f928c1-3bc2-46d0-9854-e2f3fd50c532", "e931063a-62b3-470c-8159-7b62febcea98");  // MyUrlDB + FlashUrlApp
        consumerSecretMap.put("eb5ef7b8-dada-457f-9b55-f3ab8be0a95c", "3035d892-ba3b-4849-87c8-75c31b69b6b0");  // MyUrlDB + FemtoUrlApp
        consumerSecretMap.put("15e956e9-3d16-4f92-a2f0-0a95e78a69b3", "e7eab7c9-73eb-4cbb-98fb-a74f2d2e13d3");  // MyUrlDB + UserUrlApp
        // ...

        return consumerSecretMap;
    }
    protected Map<String, String> getConsumerSecretMap()
    {
        return consumerSecretMap;
    }

}
