package com.myurldb.ws;



public interface AbuseTag 
{
    String  getGuid();
    String  getShortLink();
    String  getShortUrl();
    String  getLongUrl();
    String  getLongUrlHash();
    String  getUser();
    String  getIpAddress();
    Integer  getTag();
    String  getComment();
    String  getStatus();
    String  getReviewer();
    String  getAction();
    String  getNote();
    String  getPastActions();
    Long  getReviewedTime();
    Long  getActionTime();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
