package com.myurldb.ws;



public interface AlbumShortLink 
{
    String  getGuid();
    String  getUser();
    String  getLinkAlbum();
    String  getShortLink();
    String  getShortUrl();
    String  getLongUrl();
    String  getNote();
    String  getStatus();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
