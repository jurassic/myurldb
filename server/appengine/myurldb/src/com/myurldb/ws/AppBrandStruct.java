package com.myurldb.ws;



public interface AppBrandStruct 
{
    String  getBrand();
    String  getName();
    String  getDescription();
    boolean isEmpty();
}
