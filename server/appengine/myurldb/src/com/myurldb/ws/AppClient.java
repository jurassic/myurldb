package com.myurldb.ws;



public interface AppClient 
{
    String  getGuid();
    String  getOwner();
    String  getAdmin();
    String  getName();
    String  getClientId();
    String  getClientCode();
    String  getRootDomain();
    String  getAppDomain();
    Boolean  isUseAppDomain();
    Boolean  isEnabled();
    String  getLicenseMode();
    String  getStatus();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
