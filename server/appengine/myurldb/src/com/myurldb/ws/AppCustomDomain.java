package com.myurldb.ws;



public interface AppCustomDomain 
{
    String  getGuid();
    String  getAppId();
    String  getSiteDomain();
    String  getDomain();
    Boolean  isVerified();
    String  getStatus();
    Long  getVerifiedTime();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
