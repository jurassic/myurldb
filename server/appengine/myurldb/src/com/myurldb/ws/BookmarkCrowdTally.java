package com.myurldb.ws;



public interface BookmarkCrowdTally extends CrowdTallyBase
{
    String  getBookmarkFolder();
    String  getContentTag();
    String  getReferenceElement();
    String  getElementType();
    String  getKeywordLink();
}
