package com.myurldb.ws;



public interface BookmarkFolderImport extends FolderImportBase
{
    String  getBookmarkFolder();
}
