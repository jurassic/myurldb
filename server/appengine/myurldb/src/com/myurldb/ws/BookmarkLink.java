package com.myurldb.ws;



public interface BookmarkLink extends NavLinkBase
{
    String  getBookmarkFolder();
    String  getContentTag();
    String  getReferenceElement();
    String  getElementType();
    String  getKeywordLink();
}
