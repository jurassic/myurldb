package com.myurldb.ws;



public interface CellLatitudeLongitude 
{
    Integer  getScale();
    Integer  getLatitude();
    Integer  getLongitude();
    boolean isEmpty();
}
