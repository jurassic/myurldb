package com.myurldb.ws;



public interface ClientSetting extends PersonalSetting
{
    String  getAppClient();
    String  getAdmin();
    Boolean  isAutoRedirectEnabled();
    Boolean  isViewEnabled();
    Boolean  isShareEnabled();
    String  getDefaultBaseDomain();
    String  getDefaultDomainType();
    String  getDefaultTokenType();
    String  getDefaultRedirectType();
    Long  getDefaultFlashDuration();
    String  getDefaultAccessType();
    String  getDefaultViewType();
}
