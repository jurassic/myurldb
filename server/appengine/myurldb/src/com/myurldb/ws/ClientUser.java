package com.myurldb.ws;



public interface ClientUser 
{
    String  getGuid();
    String  getAppClient();
    String  getUser();
    String  getRole();
    Boolean  isProvisioned();
    Boolean  isLicensed();
    String  getStatus();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
