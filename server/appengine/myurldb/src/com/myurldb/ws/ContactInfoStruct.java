package com.myurldb.ws;



public interface ContactInfoStruct 
{
    String  getUuid();
    String  getStreetAddress();
    String  getLocality();
    String  getRegion();
    String  getPostalCode();
    String  getCountryName();
    String  getEmailAddress();
    String  getPhoneNumber();
    String  getFaxNumber();
    String  getWebsite();
    String  getNote();
    boolean isEmpty();
}
