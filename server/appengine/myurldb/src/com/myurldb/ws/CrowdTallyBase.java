package com.myurldb.ws;



public interface CrowdTallyBase 
{
    String  getGuid();
    String  getUser();
    String  getShortLink();
    String  getDomain();
    String  getToken();
    String  getLongUrl();
    String  getShortUrl();
    String  getTallyDate();
    String  getStatus();
    String  getNote();
    Long  getExpirationTime();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
