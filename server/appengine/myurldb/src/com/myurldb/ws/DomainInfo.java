package com.myurldb.ws;



public interface DomainInfo 
{
    String  getGuid();
    String  getDomain();
    Boolean  isBanned();
    Boolean  isUrlShortener();
    String  getCategory();
    String  getReputation();
    String  getAuthority();
    String  getNote();
    Long  getVerifiedTime();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
