package com.myurldb.ws;



public interface ExternalServiceApiKeyStruct 
{
    String  getUuid();
    String  getService();
    String  getKey();
    String  getSecret();
    String  getNote();
    boolean isEmpty();
}
