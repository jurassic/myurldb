package com.myurldb.ws;



public interface FiveTen 
{
    String  getGuid();
    Integer  getCounter();
    String  getRequesterIpAddress();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
