package com.myurldb.ws;



public interface FolderBase 
{
    String  getGuid();
    String  getAppClient();
    String  getClientRootDomain();
    String  getUser();
    String  getTitle();
    String  getDescription();
    String  getType();
    String  getCategory();
    String  getParent();
    String  getAggregate();
    String  getAcl();
    Boolean  isExportable();
    String  getStatus();
    String  getNote();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
