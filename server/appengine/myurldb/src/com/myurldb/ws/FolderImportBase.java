package com.myurldb.ws;



public interface FolderImportBase 
{
    String  getGuid();
    String  getUser();
    Integer  getPrecedence();
    String  getImportType();
    String  getImportedFolder();
    String  getImportedFolderUser();
    String  getImportedFolderTitle();
    String  getImportedFolderPath();
    String  getStatus();
    String  getNote();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
