package com.myurldb.ws;



public interface FullNameStruct 
{
    String  getUuid();
    String  getDisplayName();
    String  getLastName();
    String  getFirstName();
    String  getMiddleName1();
    String  getMiddleName2();
    String  getMiddleInitial();
    String  getSalutation();
    String  getSuffix();
    String  getNote();
    boolean isEmpty();
}
