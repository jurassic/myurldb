package com.myurldb.ws;



public interface GaeAppStruct 
{
    String  getGroupId();
    String  getAppId();
    String  getAppDomain();
    String  getNamespace();
    Long  getAcl();
    String  getNote();
    boolean isEmpty();
}
