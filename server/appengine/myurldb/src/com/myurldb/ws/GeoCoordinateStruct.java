package com.myurldb.ws;



public interface GeoCoordinateStruct extends GeoPointStruct
{
    String  getAccuracy();
    String  getAltitudeAccuracy();
    Double  getHeading();
    Double  getSpeed();
    String  getNote();
    boolean isEmpty();
}
