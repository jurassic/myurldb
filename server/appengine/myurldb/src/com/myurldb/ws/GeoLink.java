package com.myurldb.ws;



public interface GeoLink 
{
    String  getGuid();
    String  getShortLink();
    String  getShortUrl();
    GeoCoordinateStruct  getGeoCoordinate();
    CellLatitudeLongitude  getGeoCell();
    String  getStatus();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
