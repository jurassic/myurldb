package com.myurldb.ws;



public interface HashedPasswordStruct 
{
    String  getUuid();
    String  getPlainText();
    String  getHashedText();
    String  getSalt();
    String  getAlgorithm();
    boolean isEmpty();
}
