package com.myurldb.ws;



public interface HelpNotice 
{
    String  getUuid();
    String  getTitle();
    String  getContent();
    String  getFormat();
    String  getNote();
    String  getStatus();
    boolean isEmpty();
}
