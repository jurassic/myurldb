package com.myurldb.ws;



public interface KeyValueRelationStruct extends KeyValuePairStruct
{
    String  getRelation();
    boolean isEmpty();
}
