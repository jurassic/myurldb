package com.myurldb.ws;



public interface KeywordCrowdTally extends CrowdTallyBase
{
    String  getKeywordFolder();
    String  getFolderPath();
    String  getKeyword();
    String  getQueryKey();
    String  getScope();
    Boolean  isCaseSensitive();
}
