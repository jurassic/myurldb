package com.myurldb.ws;



public interface KeywordFolderImport extends FolderImportBase
{
    String  getKeywordFolder();
}
