package com.myurldb.ws;



public interface KeywordLink extends NavLinkBase
{
    String  getKeywordFolder();
    String  getFolderPath();
    String  getKeyword();
    String  getQueryKey();
    String  getScope();
    Boolean  isDynamic();
    Boolean  isCaseSensitive();
}
