package com.myurldb.ws;



public interface LinkMessage 
{
    String  getGuid();
    String  getShortLink();
    String  getMessage();
    String  getPassword();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
