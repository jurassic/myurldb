package com.myurldb.ws;



public interface LinkPassphrase 
{
    String  getGuid();
    String  getShortLink();
    String  getPassphrase();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
