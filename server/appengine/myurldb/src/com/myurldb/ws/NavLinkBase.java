package com.myurldb.ws;



public interface NavLinkBase 
{
    String  getGuid();
    String  getAppClient();
    String  getClientRootDomain();
    String  getUser();
    String  getShortLink();
    String  getDomain();
    String  getToken();
    String  getLongUrl();
    String  getShortUrl();
    Boolean  isInternal();
    Boolean  isCaching();
    String  getMemo();
    String  getStatus();
    String  getNote();
    Long  getExpirationTime();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
