package com.myurldb.ws;



public interface PersonalSetting 
{
    String  getGuid();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
