package com.myurldb.ws;



public interface QrCode 
{
    String  getGuid();
    String  getShortLink();
    String  getImageLink();
    String  getImageUrl();
    String  getType();
    String  getStatus();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
