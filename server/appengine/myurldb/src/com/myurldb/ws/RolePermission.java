package com.myurldb.ws;



public interface RolePermission 
{
    String  getGuid();
    String  getRole();
    String  getPermissionName();
    String  getResource();
    String  getInstance();
    String  getAction();
    String  getStatus();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
