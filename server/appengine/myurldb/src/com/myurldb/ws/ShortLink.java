package com.myurldb.ws;



public interface ShortLink 
{
    String  getGuid();
    String  getAppClient();
    String  getClientRootDomain();
    String  getOwner();
    String  getLongUrlDomain();
    String  getLongUrl();
    String  getLongUrlFull();
    String  getLongUrlHash();
    Boolean  isReuseExistingShortUrl();
    Boolean  isFailIfShortUrlExists();
    String  getDomain();
    String  getDomainType();
    String  getUsercode();
    String  getUsername();
    String  getTokenPrefix();
    String  getToken();
    String  getTokenType();
    String  getSassyTokenType();
    String  getShortUrl();
    String  getShortPassage();
    String  getRedirectType();
    Long  getFlashDuration();
    String  getAccessType();
    String  getViewType();
    String  getShareType();
    Boolean  isReadOnly();
    String  getDisplayMessage();
    String  getShortMessage();
    Boolean  isKeywordEnabled();
    Boolean  isBookmarkEnabled();
    ReferrerInfoStruct  getReferrerInfo();
    String  getStatus();
    String  getNote();
    Long  getExpirationTime();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
