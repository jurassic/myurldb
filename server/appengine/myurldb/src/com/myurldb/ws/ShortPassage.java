package com.myurldb.ws;



public interface ShortPassage 
{
    String  getGuid();
    String  getOwner();
    String  getLongText();
    String  getShortText();
    ShortPassageAttribute  getAttribute();
    Boolean  isReadOnly();
    String  getStatus();
    String  getNote();
    Long  getExpirationTime();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
