package com.myurldb.ws;



public interface ShortPassageAttribute 
{
    String  getDomain();
    String  getTokenType();
    String  getDisplayMessage();
    String  getRedirectType();
    Long  getFlashDuration();
    String  getAccessType();
    String  getViewType();
    String  getShareType();
    boolean isEmpty();
}
