package com.myurldb.ws;



public interface SiteCustomDomain 
{
    String  getGuid();
    String  getSiteDomain();
    String  getDomain();
    String  getStatus();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
