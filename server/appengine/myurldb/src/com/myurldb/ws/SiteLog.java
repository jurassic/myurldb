package com.myurldb.ws;



public interface SiteLog 
{
    String  getUuid();
    String  getTitle();
    String  getPubDate();
    String  getTag();
    String  getContent();
    String  getFormat();
    String  getNote();
    String  getStatus();
    boolean isEmpty();
}
