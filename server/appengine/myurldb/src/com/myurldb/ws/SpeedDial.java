package com.myurldb.ws;



public interface SpeedDial 
{
    String  getGuid();
    String  getUser();
    String  getCode();
    String  getShortcut();
    String  getShortLink();
    String  getKeywordLink();
    String  getBookmarkLink();
    String  getLongUrl();
    String  getShortUrl();
    Boolean  isCaseSensitive();
    String  getStatus();
    String  getNote();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
