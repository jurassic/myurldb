package com.myurldb.ws;



public interface StreetAddressStruct 
{
    String  getUuid();
    String  getStreet1();
    String  getStreet2();
    String  getCity();
    String  getCounty();
    String  getPostalCode();
    String  getState();
    String  getProvince();
    String  getCountry();
    String  getCountryName();
    String  getNote();
    boolean isEmpty();
}
