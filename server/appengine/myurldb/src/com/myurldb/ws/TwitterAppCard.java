package com.myurldb.ws;



public interface TwitterAppCard extends TwitterCardBase
{
    String  getImage();
    Integer  getImageWidth();
    Integer  getImageHeight();
    TwitterCardAppInfo  getIphoneApp();
    TwitterCardAppInfo  getIpadApp();
    TwitterCardAppInfo  getGooglePlayApp();
}
