package com.myurldb.ws;



public interface TwitterCardBase 
{
    String  getGuid();
    String  getCard();
    String  getUrl();
    String  getTitle();
    String  getDescription();
    String  getSite();
    String  getSiteId();
    String  getCreator();
    String  getCreatorId();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
