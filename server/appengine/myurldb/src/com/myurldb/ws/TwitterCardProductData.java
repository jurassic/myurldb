package com.myurldb.ws;



public interface TwitterCardProductData 
{
    String  getData();
    String  getLabel();
    boolean isEmpty();
}
