package com.myurldb.ws;



public interface TwitterGalleryCard extends TwitterCardBase
{
    String  getImage0();
    String  getImage1();
    String  getImage2();
    String  getImage3();
}
