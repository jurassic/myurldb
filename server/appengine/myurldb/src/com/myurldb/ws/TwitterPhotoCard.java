package com.myurldb.ws;



public interface TwitterPhotoCard extends TwitterCardBase
{
    String  getImage();
    Integer  getImageWidth();
    Integer  getImageHeight();
}
