package com.myurldb.ws;



public interface TwitterPlayerCard extends TwitterCardBase
{
    String  getImage();
    Integer  getImageWidth();
    Integer  getImageHeight();
    String  getPlayer();
    Integer  getPlayerWidth();
    Integer  getPlayerHeight();
    String  getPlayerStream();
    String  getPlayerStreamContentType();
}
