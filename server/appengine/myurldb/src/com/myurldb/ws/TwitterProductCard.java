package com.myurldb.ws;



public interface TwitterProductCard extends TwitterCardBase
{
    String  getImage();
    Integer  getImageWidth();
    Integer  getImageHeight();
    TwitterCardProductData  getData1();
    TwitterCardProductData  getData2();
}
