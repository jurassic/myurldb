package com.myurldb.ws;



public interface TwitterSummaryCard extends TwitterCardBase
{
    String  getImage();
    Integer  getImageWidth();
    Integer  getImageHeight();
}
