package com.myurldb.ws;



public interface UrlRating 
{
    String  getGuid();
    String  getDomain();
    String  getLongUrl();
    String  getLongUrlHash();
    String  getPreview();
    String  getFlag();
    Double  getRating();
    String  getNote();
    Long  getRatedTime();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
