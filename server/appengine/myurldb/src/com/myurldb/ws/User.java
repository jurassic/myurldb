package com.myurldb.ws;



public interface User 
{
    String  getGuid();
    String  getManagerApp();
    Long  getAppAcl();
    GaeAppStruct  getGaeApp();
    String  getAeryId();
    String  getSessionId();
    String  getAncestorGuid();
    FullNameStruct  getName();
    String  getUsercode();
    String  getUsername();
    String  getNickname();
    String  getAvatar();
    String  getEmail();
    String  getOpenId();
    GaeUserStruct  getGaeUser();
    String  getEntityType();
    Boolean  isSurrogate();
    Boolean  isObsolete();
    String  getTimeZone();
    String  getLocation();
    StreetAddressStruct  getStreetAddress();
    GeoPointStruct  getGeoPoint();
    String  getIpAddress();
    String  getReferer();
    String  getStatus();
    Long  getEmailVerifiedTime();
    Long  getOpenIdVerifiedTime();
    Long  getAuthenticatedTime();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
