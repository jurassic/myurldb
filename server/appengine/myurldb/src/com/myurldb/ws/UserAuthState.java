package com.myurldb.ws;



public interface UserAuthState 
{
    String  getGuid();
    String  getProviderId();
    String  getUser();
    String  getUsername();
    String  getEmail();
    String  getOpenId();
    String  getDeviceId();
    String  getSessionId();
    String  getAuthToken();
    String  getAuthStatus();
    String  getExternalAuth();
    ExternalUserIdStruct  getExternalId();
    String  getStatus();
    Long  getFirstAuthTime();
    Long  getLastAuthTime();
    Long  getExpirationTime();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
