package com.myurldb.ws;



public interface UserCustomDomain 
{
    String  getGuid();
    String  getOwner();
    String  getDomain();
    Boolean  isVerified();
    String  getStatus();
    Long  getVerifiedTime();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
