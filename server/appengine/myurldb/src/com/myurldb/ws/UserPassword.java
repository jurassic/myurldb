package com.myurldb.ws;



public interface UserPassword 
{
    String  getGuid();
    String  getAdmin();
    String  getUser();
    String  getUsername();
    String  getEmail();
    String  getOpenId();
    HashedPasswordStruct  getPassword();
    Boolean  isResetRequired();
    String  getChallengeQuestion();
    String  getChallengeAnswer();
    String  getStatus();
    Long  getLastResetTime();
    Long  getExpirationTime();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
