package com.myurldb.ws;



public interface UserRating 
{
    String  getGuid();
    String  getUser();
    Double  getRating();
    String  getNote();
    Long  getRatedTime();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
