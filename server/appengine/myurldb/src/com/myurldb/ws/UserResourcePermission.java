package com.myurldb.ws;



public interface UserResourcePermission 
{
    String  getGuid();
    String  getUser();
    String  getPermissionName();
    String  getResource();
    String  getInstance();
    String  getAction();
    Boolean  isPermitted();
    String  getStatus();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
