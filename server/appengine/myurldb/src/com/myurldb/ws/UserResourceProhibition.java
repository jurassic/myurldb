package com.myurldb.ws;



public interface UserResourceProhibition 
{
    String  getGuid();
    String  getUser();
    String  getPermissionName();
    String  getResource();
    String  getInstance();
    String  getAction();
    Boolean  isProhibited();
    String  getStatus();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
