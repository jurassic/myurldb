package com.myurldb.ws;



public interface UserRole 
{
    String  getGuid();
    String  getUser();
    String  getRole();
    String  getStatus();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
