package com.myurldb.ws;



public interface UserSetting extends PersonalSetting
{
    String  getUser();
    String  getHomePage();
    String  getSelfBio();
    String  getUserUrlDomain();
    String  getUserUrlDomainNormalized();
    Boolean  isAutoRedirectEnabled();
    Boolean  isViewEnabled();
    Boolean  isShareEnabled();
    String  getDefaultDomain();
    String  getDefaultTokenType();
    String  getDefaultRedirectType();
    Long  getDefaultFlashDuration();
    String  getDefaultAccessType();
    String  getDefaultViewType();
    String  getDefaultShareType();
    String  getDefaultPassphrase();
    String  getDefaultSignature();
    Boolean  isUseSignature();
    String  getDefaultEmblem();
    Boolean  isUseEmblem();
    String  getDefaultBackground();
    Boolean  isUseBackground();
    String  getSponsorUrl();
    String  getSponsorBanner();
    String  getSponsorNote();
    Boolean  isUseSponsor();
    String  getExtraParams();
    Long  getExpirationDuration();
}
