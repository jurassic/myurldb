package com.myurldb.ws;



public interface UserUsercode 
{
    String  getGuid();
    String  getAdmin();
    String  getUser();
    String  getUsername();
    String  getUsercode();
    String  getStatus();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
