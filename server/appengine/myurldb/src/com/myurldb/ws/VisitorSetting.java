package com.myurldb.ws;



public interface VisitorSetting extends PersonalSetting
{
    String  getUser();
    String  getRedirectType();
    Long  getFlashDuration();
    String  getPrivacyType();
}
