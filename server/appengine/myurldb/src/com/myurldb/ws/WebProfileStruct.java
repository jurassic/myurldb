package com.myurldb.ws;



public interface WebProfileStruct 
{
    String  getUuid();
    String  getType();
    String  getServiceName();
    String  getServiceUrl();
    String  getProfileUrl();
    String  getNote();
    boolean isEmpty();
}
