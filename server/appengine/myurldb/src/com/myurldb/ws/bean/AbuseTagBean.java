package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.AbuseTag;
import com.myurldb.ws.data.AbuseTagDataObject;

public class AbuseTagBean extends BeanBase implements AbuseTag
{
    private static final Logger log = Logger.getLogger(AbuseTagBean.class.getName());

    // Embedded data object.
    private AbuseTagDataObject dobj = null;

    public AbuseTagBean()
    {
        this(new AbuseTagDataObject());
    }
    public AbuseTagBean(String guid)
    {
        this(new AbuseTagDataObject(guid));
    }
    public AbuseTagBean(AbuseTagDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public AbuseTagDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AbuseTagDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            if(log.isLoggable(Level.INFO)) log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AbuseTagDataObject is null!");
        }
    }

    public String getShortLink()
    {
        if(getDataObject() != null) {
            return getDataObject().getShortLink();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AbuseTagDataObject is null!");
            return null;   // ???
        }
    }
    public void setShortLink(String shortLink)
    {
        if(getDataObject() != null) {
            getDataObject().setShortLink(shortLink);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AbuseTagDataObject is null!");
        }
    }

    public String getShortUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getShortUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AbuseTagDataObject is null!");
            return null;   // ???
        }
    }
    public void setShortUrl(String shortUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setShortUrl(shortUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AbuseTagDataObject is null!");
        }
    }

    public String getLongUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getLongUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AbuseTagDataObject is null!");
            return null;   // ???
        }
    }
    public void setLongUrl(String longUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setLongUrl(longUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AbuseTagDataObject is null!");
        }
    }

    public String getLongUrlHash()
    {
        if(getDataObject() != null) {
            return getDataObject().getLongUrlHash();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AbuseTagDataObject is null!");
            return null;   // ???
        }
    }
    public void setLongUrlHash(String longUrlHash)
    {
        if(getDataObject() != null) {
            getDataObject().setLongUrlHash(longUrlHash);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AbuseTagDataObject is null!");
        }
    }

    public String getUser()
    {
        if(getDataObject() != null) {
            return getDataObject().getUser();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AbuseTagDataObject is null!");
            return null;   // ???
        }
    }
    public void setUser(String user)
    {
        if(getDataObject() != null) {
            getDataObject().setUser(user);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AbuseTagDataObject is null!");
        }
    }

    public String getIpAddress()
    {
        if(getDataObject() != null) {
            return getDataObject().getIpAddress();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AbuseTagDataObject is null!");
            return null;   // ???
        }
    }
    public void setIpAddress(String ipAddress)
    {
        if(getDataObject() != null) {
            getDataObject().setIpAddress(ipAddress);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AbuseTagDataObject is null!");
        }
    }

    public Integer getTag()
    {
        if(getDataObject() != null) {
            return getDataObject().getTag();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AbuseTagDataObject is null!");
            return null;   // ???
        }
    }
    public void setTag(Integer tag)
    {
        if(getDataObject() != null) {
            getDataObject().setTag(tag);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AbuseTagDataObject is null!");
        }
    }

    public String getComment()
    {
        if(getDataObject() != null) {
            return getDataObject().getComment();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AbuseTagDataObject is null!");
            return null;   // ???
        }
    }
    public void setComment(String comment)
    {
        if(getDataObject() != null) {
            getDataObject().setComment(comment);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AbuseTagDataObject is null!");
        }
    }

    public String getStatus()
    {
        if(getDataObject() != null) {
            return getDataObject().getStatus();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AbuseTagDataObject is null!");
            return null;   // ???
        }
    }
    public void setStatus(String status)
    {
        if(getDataObject() != null) {
            getDataObject().setStatus(status);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AbuseTagDataObject is null!");
        }
    }

    public String getReviewer()
    {
        if(getDataObject() != null) {
            return getDataObject().getReviewer();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AbuseTagDataObject is null!");
            return null;   // ???
        }
    }
    public void setReviewer(String reviewer)
    {
        if(getDataObject() != null) {
            getDataObject().setReviewer(reviewer);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AbuseTagDataObject is null!");
        }
    }

    public String getAction()
    {
        if(getDataObject() != null) {
            return getDataObject().getAction();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AbuseTagDataObject is null!");
            return null;   // ???
        }
    }
    public void setAction(String action)
    {
        if(getDataObject() != null) {
            getDataObject().setAction(action);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AbuseTagDataObject is null!");
        }
    }

    public String getNote()
    {
        if(getDataObject() != null) {
            return getDataObject().getNote();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AbuseTagDataObject is null!");
            return null;   // ???
        }
    }
    public void setNote(String note)
    {
        if(getDataObject() != null) {
            getDataObject().setNote(note);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AbuseTagDataObject is null!");
        }
    }

    public String getPastActions()
    {
        if(getDataObject() != null) {
            return getDataObject().getPastActions();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AbuseTagDataObject is null!");
            return null;   // ???
        }
    }
    public void setPastActions(String pastActions)
    {
        if(getDataObject() != null) {
            getDataObject().setPastActions(pastActions);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AbuseTagDataObject is null!");
        }
    }

    public Long getReviewedTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getReviewedTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AbuseTagDataObject is null!");
            return null;   // ???
        }
    }
    public void setReviewedTime(Long reviewedTime)
    {
        if(getDataObject() != null) {
            getDataObject().setReviewedTime(reviewedTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AbuseTagDataObject is null!");
        }
    }

    public Long getActionTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getActionTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AbuseTagDataObject is null!");
            return null;   // ???
        }
    }
    public void setActionTime(Long actionTime)
    {
        if(getDataObject() != null) {
            getDataObject().setActionTime(actionTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AbuseTagDataObject is null!");
        }
    }


    // TBD
    public AbuseTagDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
