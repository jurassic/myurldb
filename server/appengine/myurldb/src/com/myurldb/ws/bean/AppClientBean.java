package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.AppClient;
import com.myurldb.ws.data.AppClientDataObject;

public class AppClientBean extends BeanBase implements AppClient
{
    private static final Logger log = Logger.getLogger(AppClientBean.class.getName());

    // Embedded data object.
    private AppClientDataObject dobj = null;

    public AppClientBean()
    {
        this(new AppClientDataObject());
    }
    public AppClientBean(String guid)
    {
        this(new AppClientDataObject(guid));
    }
    public AppClientBean(AppClientDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public AppClientDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AppClientDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            if(log.isLoggable(Level.INFO)) log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AppClientDataObject is null!");
        }
    }

    public String getOwner()
    {
        if(getDataObject() != null) {
            return getDataObject().getOwner();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AppClientDataObject is null!");
            return null;   // ???
        }
    }
    public void setOwner(String owner)
    {
        if(getDataObject() != null) {
            getDataObject().setOwner(owner);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AppClientDataObject is null!");
        }
    }

    public String getAdmin()
    {
        if(getDataObject() != null) {
            return getDataObject().getAdmin();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AppClientDataObject is null!");
            return null;   // ???
        }
    }
    public void setAdmin(String admin)
    {
        if(getDataObject() != null) {
            getDataObject().setAdmin(admin);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AppClientDataObject is null!");
        }
    }

    public String getName()
    {
        if(getDataObject() != null) {
            return getDataObject().getName();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AppClientDataObject is null!");
            return null;   // ???
        }
    }
    public void setName(String name)
    {
        if(getDataObject() != null) {
            getDataObject().setName(name);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AppClientDataObject is null!");
        }
    }

    public String getClientId()
    {
        if(getDataObject() != null) {
            return getDataObject().getClientId();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AppClientDataObject is null!");
            return null;   // ???
        }
    }
    public void setClientId(String clientId)
    {
        if(getDataObject() != null) {
            getDataObject().setClientId(clientId);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AppClientDataObject is null!");
        }
    }

    public String getClientCode()
    {
        if(getDataObject() != null) {
            return getDataObject().getClientCode();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AppClientDataObject is null!");
            return null;   // ???
        }
    }
    public void setClientCode(String clientCode)
    {
        if(getDataObject() != null) {
            getDataObject().setClientCode(clientCode);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AppClientDataObject is null!");
        }
    }

    public String getRootDomain()
    {
        if(getDataObject() != null) {
            return getDataObject().getRootDomain();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AppClientDataObject is null!");
            return null;   // ???
        }
    }
    public void setRootDomain(String rootDomain)
    {
        if(getDataObject() != null) {
            getDataObject().setRootDomain(rootDomain);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AppClientDataObject is null!");
        }
    }

    public String getAppDomain()
    {
        if(getDataObject() != null) {
            return getDataObject().getAppDomain();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AppClientDataObject is null!");
            return null;   // ???
        }
    }
    public void setAppDomain(String appDomain)
    {
        if(getDataObject() != null) {
            getDataObject().setAppDomain(appDomain);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AppClientDataObject is null!");
        }
    }

    public Boolean isUseAppDomain()
    {
        if(getDataObject() != null) {
            return getDataObject().isUseAppDomain();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AppClientDataObject is null!");
            return null;   // ???
        }
    }
    public void setUseAppDomain(Boolean useAppDomain)
    {
        if(getDataObject() != null) {
            getDataObject().setUseAppDomain(useAppDomain);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AppClientDataObject is null!");
        }
    }

    public Boolean isEnabled()
    {
        if(getDataObject() != null) {
            return getDataObject().isEnabled();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AppClientDataObject is null!");
            return null;   // ???
        }
    }
    public void setEnabled(Boolean enabled)
    {
        if(getDataObject() != null) {
            getDataObject().setEnabled(enabled);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AppClientDataObject is null!");
        }
    }

    public String getLicenseMode()
    {
        if(getDataObject() != null) {
            return getDataObject().getLicenseMode();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AppClientDataObject is null!");
            return null;   // ???
        }
    }
    public void setLicenseMode(String licenseMode)
    {
        if(getDataObject() != null) {
            getDataObject().setLicenseMode(licenseMode);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AppClientDataObject is null!");
        }
    }

    public String getStatus()
    {
        if(getDataObject() != null) {
            return getDataObject().getStatus();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AppClientDataObject is null!");
            return null;   // ???
        }
    }
    public void setStatus(String status)
    {
        if(getDataObject() != null) {
            getDataObject().setStatus(status);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AppClientDataObject is null!");
        }
    }


    // TBD
    public AppClientDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
