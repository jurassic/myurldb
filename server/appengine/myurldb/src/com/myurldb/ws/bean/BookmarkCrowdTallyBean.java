package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.BookmarkCrowdTally;
import com.myurldb.ws.data.BookmarkCrowdTallyDataObject;

public class BookmarkCrowdTallyBean extends CrowdTallyBaseBean implements BookmarkCrowdTally
{
    private static final Logger log = Logger.getLogger(BookmarkCrowdTallyBean.class.getName());

    // Embedded data object.
    private BookmarkCrowdTallyDataObject dobj = null;

    public BookmarkCrowdTallyBean()
    {
        this(new BookmarkCrowdTallyDataObject());
    }
    public BookmarkCrowdTallyBean(String guid)
    {
        this(new BookmarkCrowdTallyDataObject(guid));
    }
    public BookmarkCrowdTallyBean(BookmarkCrowdTallyDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public BookmarkCrowdTallyDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getBookmarkFolder()
    {
        if(getDataObject() != null) {
            return getDataObject().getBookmarkFolder();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded BookmarkCrowdTallyDataObject is null!");
            return null;   // ???
        }
    }
    public void setBookmarkFolder(String bookmarkFolder)
    {
        if(getDataObject() != null) {
            getDataObject().setBookmarkFolder(bookmarkFolder);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded BookmarkCrowdTallyDataObject is null!");
        }
    }

    public String getContentTag()
    {
        if(getDataObject() != null) {
            return getDataObject().getContentTag();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded BookmarkCrowdTallyDataObject is null!");
            return null;   // ???
        }
    }
    public void setContentTag(String contentTag)
    {
        if(getDataObject() != null) {
            getDataObject().setContentTag(contentTag);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded BookmarkCrowdTallyDataObject is null!");
        }
    }

    public String getReferenceElement()
    {
        if(getDataObject() != null) {
            return getDataObject().getReferenceElement();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded BookmarkCrowdTallyDataObject is null!");
            return null;   // ???
        }
    }
    public void setReferenceElement(String referenceElement)
    {
        if(getDataObject() != null) {
            getDataObject().setReferenceElement(referenceElement);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded BookmarkCrowdTallyDataObject is null!");
        }
    }

    public String getElementType()
    {
        if(getDataObject() != null) {
            return getDataObject().getElementType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded BookmarkCrowdTallyDataObject is null!");
            return null;   // ???
        }
    }
    public void setElementType(String elementType)
    {
        if(getDataObject() != null) {
            getDataObject().setElementType(elementType);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded BookmarkCrowdTallyDataObject is null!");
        }
    }

    public String getKeywordLink()
    {
        if(getDataObject() != null) {
            return getDataObject().getKeywordLink();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded BookmarkCrowdTallyDataObject is null!");
            return null;   // ???
        }
    }
    public void setKeywordLink(String keywordLink)
    {
        if(getDataObject() != null) {
            getDataObject().setKeywordLink(keywordLink);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded BookmarkCrowdTallyDataObject is null!");
        }
    }


    // TBD
    public BookmarkCrowdTallyDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
