package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.BookmarkFolder;
import com.myurldb.ws.data.BookmarkFolderDataObject;

public class BookmarkFolderBean extends FolderBaseBean implements BookmarkFolder
{
    private static final Logger log = Logger.getLogger(BookmarkFolderBean.class.getName());

    // Embedded data object.
    private BookmarkFolderDataObject dobj = null;

    public BookmarkFolderBean()
    {
        this(new BookmarkFolderDataObject());
    }
    public BookmarkFolderBean(String guid)
    {
        this(new BookmarkFolderDataObject(guid));
    }
    public BookmarkFolderBean(BookmarkFolderDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public BookmarkFolderDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getKeywordFolder()
    {
        if(getDataObject() != null) {
            return getDataObject().getKeywordFolder();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded BookmarkFolderDataObject is null!");
            return null;   // ???
        }
    }
    public void setKeywordFolder(String keywordFolder)
    {
        if(getDataObject() != null) {
            getDataObject().setKeywordFolder(keywordFolder);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded BookmarkFolderDataObject is null!");
        }
    }


    // TBD
    public BookmarkFolderDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
