package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.BookmarkFolderImport;
import com.myurldb.ws.data.BookmarkFolderImportDataObject;

public class BookmarkFolderImportBean extends FolderImportBaseBean implements BookmarkFolderImport
{
    private static final Logger log = Logger.getLogger(BookmarkFolderImportBean.class.getName());

    // Embedded data object.
    private BookmarkFolderImportDataObject dobj = null;

    public BookmarkFolderImportBean()
    {
        this(new BookmarkFolderImportDataObject());
    }
    public BookmarkFolderImportBean(String guid)
    {
        this(new BookmarkFolderImportDataObject(guid));
    }
    public BookmarkFolderImportBean(BookmarkFolderImportDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public BookmarkFolderImportDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getBookmarkFolder()
    {
        if(getDataObject() != null) {
            return getDataObject().getBookmarkFolder();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded BookmarkFolderImportDataObject is null!");
            return null;   // ???
        }
    }
    public void setBookmarkFolder(String bookmarkFolder)
    {
        if(getDataObject() != null) {
            getDataObject().setBookmarkFolder(bookmarkFolder);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded BookmarkFolderImportDataObject is null!");
        }
    }


    // TBD
    public BookmarkFolderImportDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
