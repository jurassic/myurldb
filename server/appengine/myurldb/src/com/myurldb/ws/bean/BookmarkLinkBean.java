package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.BookmarkLink;
import com.myurldb.ws.data.BookmarkLinkDataObject;

public class BookmarkLinkBean extends NavLinkBaseBean implements BookmarkLink
{
    private static final Logger log = Logger.getLogger(BookmarkLinkBean.class.getName());

    // Embedded data object.
    private BookmarkLinkDataObject dobj = null;

    public BookmarkLinkBean()
    {
        this(new BookmarkLinkDataObject());
    }
    public BookmarkLinkBean(String guid)
    {
        this(new BookmarkLinkDataObject(guid));
    }
    public BookmarkLinkBean(BookmarkLinkDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public BookmarkLinkDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getBookmarkFolder()
    {
        if(getDataObject() != null) {
            return getDataObject().getBookmarkFolder();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded BookmarkLinkDataObject is null!");
            return null;   // ???
        }
    }
    public void setBookmarkFolder(String bookmarkFolder)
    {
        if(getDataObject() != null) {
            getDataObject().setBookmarkFolder(bookmarkFolder);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded BookmarkLinkDataObject is null!");
        }
    }

    public String getContentTag()
    {
        if(getDataObject() != null) {
            return getDataObject().getContentTag();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded BookmarkLinkDataObject is null!");
            return null;   // ???
        }
    }
    public void setContentTag(String contentTag)
    {
        if(getDataObject() != null) {
            getDataObject().setContentTag(contentTag);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded BookmarkLinkDataObject is null!");
        }
    }

    public String getReferenceElement()
    {
        if(getDataObject() != null) {
            return getDataObject().getReferenceElement();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded BookmarkLinkDataObject is null!");
            return null;   // ???
        }
    }
    public void setReferenceElement(String referenceElement)
    {
        if(getDataObject() != null) {
            getDataObject().setReferenceElement(referenceElement);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded BookmarkLinkDataObject is null!");
        }
    }

    public String getElementType()
    {
        if(getDataObject() != null) {
            return getDataObject().getElementType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded BookmarkLinkDataObject is null!");
            return null;   // ???
        }
    }
    public void setElementType(String elementType)
    {
        if(getDataObject() != null) {
            getDataObject().setElementType(elementType);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded BookmarkLinkDataObject is null!");
        }
    }

    public String getKeywordLink()
    {
        if(getDataObject() != null) {
            return getDataObject().getKeywordLink();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded BookmarkLinkDataObject is null!");
            return null;   // ???
        }
    }
    public void setKeywordLink(String keywordLink)
    {
        if(getDataObject() != null) {
            getDataObject().setKeywordLink(keywordLink);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded BookmarkLinkDataObject is null!");
        }
    }


    // TBD
    public BookmarkLinkDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
