package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.CellLatitudeLongitude;
import com.myurldb.ws.data.CellLatitudeLongitudeDataObject;

public class CellLatitudeLongitudeBean implements CellLatitudeLongitude
{
    private static final Logger log = Logger.getLogger(CellLatitudeLongitudeBean.class.getName());

    // Embedded data object.
    private CellLatitudeLongitudeDataObject dobj = null;

    public CellLatitudeLongitudeBean()
    {
        this(new CellLatitudeLongitudeDataObject());
    }
    public CellLatitudeLongitudeBean(CellLatitudeLongitudeDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    public CellLatitudeLongitudeDataObject getDataObject()
    {
        return this.dobj;
    }

    public Integer getScale()
    {
        if(getDataObject() != null) {
            return getDataObject().getScale();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CellLatitudeLongitudeDataObject is null!");
            return null;   // ???
        }
    }
    public void setScale(Integer scale)
    {
        if(getDataObject() != null) {
            getDataObject().setScale(scale);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CellLatitudeLongitudeDataObject is null!");
        }
    }

    public Integer getLatitude()
    {
        if(getDataObject() != null) {
            return getDataObject().getLatitude();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CellLatitudeLongitudeDataObject is null!");
            return null;   // ???
        }
    }
    public void setLatitude(Integer latitude)
    {
        if(getDataObject() != null) {
            getDataObject().setLatitude(latitude);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CellLatitudeLongitudeDataObject is null!");
        }
    }

    public Integer getLongitude()
    {
        if(getDataObject() != null) {
            return getDataObject().getLongitude();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CellLatitudeLongitudeDataObject is null!");
            return null;   // ???
        }
    }
    public void setLongitude(Integer longitude)
    {
        if(getDataObject() != null) {
            getDataObject().setLongitude(longitude);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CellLatitudeLongitudeDataObject is null!");
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        if(getDataObject() != null) {
            return getDataObject().isEmpty();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CellLatitudeLongitudeDataObject is null!");
            boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getScale() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLatitude() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLongitude() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
            return (atLeastOneFieldNonEmpty == false);
        }
    }

    // TBD
    public CellLatitudeLongitudeDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
