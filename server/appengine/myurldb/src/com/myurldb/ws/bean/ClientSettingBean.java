package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.ClientSetting;
import com.myurldb.ws.data.ClientSettingDataObject;

public class ClientSettingBean extends PersonalSettingBean implements ClientSetting
{
    private static final Logger log = Logger.getLogger(ClientSettingBean.class.getName());

    // Embedded data object.
    private ClientSettingDataObject dobj = null;

    public ClientSettingBean()
    {
        this(new ClientSettingDataObject());
    }
    public ClientSettingBean(String guid)
    {
        this(new ClientSettingDataObject(guid));
    }
    public ClientSettingBean(ClientSettingDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public ClientSettingDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getAppClient()
    {
        if(getDataObject() != null) {
            return getDataObject().getAppClient();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ClientSettingDataObject is null!");
            return null;   // ???
        }
    }
    public void setAppClient(String appClient)
    {
        if(getDataObject() != null) {
            getDataObject().setAppClient(appClient);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ClientSettingDataObject is null!");
        }
    }

    public String getAdmin()
    {
        if(getDataObject() != null) {
            return getDataObject().getAdmin();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ClientSettingDataObject is null!");
            return null;   // ???
        }
    }
    public void setAdmin(String admin)
    {
        if(getDataObject() != null) {
            getDataObject().setAdmin(admin);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ClientSettingDataObject is null!");
        }
    }

    public Boolean isAutoRedirectEnabled()
    {
        if(getDataObject() != null) {
            return getDataObject().isAutoRedirectEnabled();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ClientSettingDataObject is null!");
            return null;   // ???
        }
    }
    public void setAutoRedirectEnabled(Boolean autoRedirectEnabled)
    {
        if(getDataObject() != null) {
            getDataObject().setAutoRedirectEnabled(autoRedirectEnabled);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ClientSettingDataObject is null!");
        }
    }

    public Boolean isViewEnabled()
    {
        if(getDataObject() != null) {
            return getDataObject().isViewEnabled();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ClientSettingDataObject is null!");
            return null;   // ???
        }
    }
    public void setViewEnabled(Boolean viewEnabled)
    {
        if(getDataObject() != null) {
            getDataObject().setViewEnabled(viewEnabled);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ClientSettingDataObject is null!");
        }
    }

    public Boolean isShareEnabled()
    {
        if(getDataObject() != null) {
            return getDataObject().isShareEnabled();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ClientSettingDataObject is null!");
            return null;   // ???
        }
    }
    public void setShareEnabled(Boolean shareEnabled)
    {
        if(getDataObject() != null) {
            getDataObject().setShareEnabled(shareEnabled);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ClientSettingDataObject is null!");
        }
    }

    public String getDefaultBaseDomain()
    {
        if(getDataObject() != null) {
            return getDataObject().getDefaultBaseDomain();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ClientSettingDataObject is null!");
            return null;   // ???
        }
    }
    public void setDefaultBaseDomain(String defaultBaseDomain)
    {
        if(getDataObject() != null) {
            getDataObject().setDefaultBaseDomain(defaultBaseDomain);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ClientSettingDataObject is null!");
        }
    }

    public String getDefaultDomainType()
    {
        if(getDataObject() != null) {
            return getDataObject().getDefaultDomainType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ClientSettingDataObject is null!");
            return null;   // ???
        }
    }
    public void setDefaultDomainType(String defaultDomainType)
    {
        if(getDataObject() != null) {
            getDataObject().setDefaultDomainType(defaultDomainType);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ClientSettingDataObject is null!");
        }
    }

    public String getDefaultTokenType()
    {
        if(getDataObject() != null) {
            return getDataObject().getDefaultTokenType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ClientSettingDataObject is null!");
            return null;   // ???
        }
    }
    public void setDefaultTokenType(String defaultTokenType)
    {
        if(getDataObject() != null) {
            getDataObject().setDefaultTokenType(defaultTokenType);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ClientSettingDataObject is null!");
        }
    }

    public String getDefaultRedirectType()
    {
        if(getDataObject() != null) {
            return getDataObject().getDefaultRedirectType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ClientSettingDataObject is null!");
            return null;   // ???
        }
    }
    public void setDefaultRedirectType(String defaultRedirectType)
    {
        if(getDataObject() != null) {
            getDataObject().setDefaultRedirectType(defaultRedirectType);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ClientSettingDataObject is null!");
        }
    }

    public Long getDefaultFlashDuration()
    {
        if(getDataObject() != null) {
            return getDataObject().getDefaultFlashDuration();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ClientSettingDataObject is null!");
            return null;   // ???
        }
    }
    public void setDefaultFlashDuration(Long defaultFlashDuration)
    {
        if(getDataObject() != null) {
            getDataObject().setDefaultFlashDuration(defaultFlashDuration);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ClientSettingDataObject is null!");
        }
    }

    public String getDefaultAccessType()
    {
        if(getDataObject() != null) {
            return getDataObject().getDefaultAccessType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ClientSettingDataObject is null!");
            return null;   // ???
        }
    }
    public void setDefaultAccessType(String defaultAccessType)
    {
        if(getDataObject() != null) {
            getDataObject().setDefaultAccessType(defaultAccessType);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ClientSettingDataObject is null!");
        }
    }

    public String getDefaultViewType()
    {
        if(getDataObject() != null) {
            return getDataObject().getDefaultViewType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ClientSettingDataObject is null!");
            return null;   // ???
        }
    }
    public void setDefaultViewType(String defaultViewType)
    {
        if(getDataObject() != null) {
            getDataObject().setDefaultViewType(defaultViewType);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ClientSettingDataObject is null!");
        }
    }


    // TBD
    public ClientSettingDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
