package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.ClientUser;
import com.myurldb.ws.data.ClientUserDataObject;

public class ClientUserBean extends BeanBase implements ClientUser
{
    private static final Logger log = Logger.getLogger(ClientUserBean.class.getName());

    // Embedded data object.
    private ClientUserDataObject dobj = null;

    public ClientUserBean()
    {
        this(new ClientUserDataObject());
    }
    public ClientUserBean(String guid)
    {
        this(new ClientUserDataObject(guid));
    }
    public ClientUserBean(ClientUserDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public ClientUserDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ClientUserDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            if(log.isLoggable(Level.INFO)) log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ClientUserDataObject is null!");
        }
    }

    public String getAppClient()
    {
        if(getDataObject() != null) {
            return getDataObject().getAppClient();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ClientUserDataObject is null!");
            return null;   // ???
        }
    }
    public void setAppClient(String appClient)
    {
        if(getDataObject() != null) {
            getDataObject().setAppClient(appClient);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ClientUserDataObject is null!");
        }
    }

    public String getUser()
    {
        if(getDataObject() != null) {
            return getDataObject().getUser();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ClientUserDataObject is null!");
            return null;   // ???
        }
    }
    public void setUser(String user)
    {
        if(getDataObject() != null) {
            getDataObject().setUser(user);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ClientUserDataObject is null!");
        }
    }

    public String getRole()
    {
        if(getDataObject() != null) {
            return getDataObject().getRole();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ClientUserDataObject is null!");
            return null;   // ???
        }
    }
    public void setRole(String role)
    {
        if(getDataObject() != null) {
            getDataObject().setRole(role);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ClientUserDataObject is null!");
        }
    }

    public Boolean isProvisioned()
    {
        if(getDataObject() != null) {
            return getDataObject().isProvisioned();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ClientUserDataObject is null!");
            return null;   // ???
        }
    }
    public void setProvisioned(Boolean provisioned)
    {
        if(getDataObject() != null) {
            getDataObject().setProvisioned(provisioned);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ClientUserDataObject is null!");
        }
    }

    public Boolean isLicensed()
    {
        if(getDataObject() != null) {
            return getDataObject().isLicensed();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ClientUserDataObject is null!");
            return null;   // ???
        }
    }
    public void setLicensed(Boolean licensed)
    {
        if(getDataObject() != null) {
            getDataObject().setLicensed(licensed);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ClientUserDataObject is null!");
        }
    }

    public String getStatus()
    {
        if(getDataObject() != null) {
            return getDataObject().getStatus();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ClientUserDataObject is null!");
            return null;   // ???
        }
    }
    public void setStatus(String status)
    {
        if(getDataObject() != null) {
            getDataObject().setStatus(status);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ClientUserDataObject is null!");
        }
    }


    // TBD
    public ClientUserDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
