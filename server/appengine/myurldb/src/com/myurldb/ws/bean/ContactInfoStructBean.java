package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.ContactInfoStruct;
import com.myurldb.ws.data.ContactInfoStructDataObject;

public class ContactInfoStructBean implements ContactInfoStruct
{
    private static final Logger log = Logger.getLogger(ContactInfoStructBean.class.getName());

    // Embedded data object.
    private ContactInfoStructDataObject dobj = null;

    public ContactInfoStructBean()
    {
        this(new ContactInfoStructDataObject());
    }
    public ContactInfoStructBean(ContactInfoStructDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    public ContactInfoStructDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getUuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getUuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ContactInfoStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setUuid(String uuid)
    {
        if(getDataObject() != null) {
            getDataObject().setUuid(uuid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ContactInfoStructDataObject is null!");
        }
    }

    public String getStreetAddress()
    {
        if(getDataObject() != null) {
            return getDataObject().getStreetAddress();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ContactInfoStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setStreetAddress(String streetAddress)
    {
        if(getDataObject() != null) {
            getDataObject().setStreetAddress(streetAddress);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ContactInfoStructDataObject is null!");
        }
    }

    public String getLocality()
    {
        if(getDataObject() != null) {
            return getDataObject().getLocality();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ContactInfoStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setLocality(String locality)
    {
        if(getDataObject() != null) {
            getDataObject().setLocality(locality);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ContactInfoStructDataObject is null!");
        }
    }

    public String getRegion()
    {
        if(getDataObject() != null) {
            return getDataObject().getRegion();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ContactInfoStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setRegion(String region)
    {
        if(getDataObject() != null) {
            getDataObject().setRegion(region);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ContactInfoStructDataObject is null!");
        }
    }

    public String getPostalCode()
    {
        if(getDataObject() != null) {
            return getDataObject().getPostalCode();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ContactInfoStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setPostalCode(String postalCode)
    {
        if(getDataObject() != null) {
            getDataObject().setPostalCode(postalCode);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ContactInfoStructDataObject is null!");
        }
    }

    public String getCountryName()
    {
        if(getDataObject() != null) {
            return getDataObject().getCountryName();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ContactInfoStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setCountryName(String countryName)
    {
        if(getDataObject() != null) {
            getDataObject().setCountryName(countryName);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ContactInfoStructDataObject is null!");
        }
    }

    public String getEmailAddress()
    {
        if(getDataObject() != null) {
            return getDataObject().getEmailAddress();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ContactInfoStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setEmailAddress(String emailAddress)
    {
        if(getDataObject() != null) {
            getDataObject().setEmailAddress(emailAddress);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ContactInfoStructDataObject is null!");
        }
    }

    public String getPhoneNumber()
    {
        if(getDataObject() != null) {
            return getDataObject().getPhoneNumber();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ContactInfoStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setPhoneNumber(String phoneNumber)
    {
        if(getDataObject() != null) {
            getDataObject().setPhoneNumber(phoneNumber);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ContactInfoStructDataObject is null!");
        }
    }

    public String getFaxNumber()
    {
        if(getDataObject() != null) {
            return getDataObject().getFaxNumber();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ContactInfoStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setFaxNumber(String faxNumber)
    {
        if(getDataObject() != null) {
            getDataObject().setFaxNumber(faxNumber);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ContactInfoStructDataObject is null!");
        }
    }

    public String getWebsite()
    {
        if(getDataObject() != null) {
            return getDataObject().getWebsite();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ContactInfoStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setWebsite(String website)
    {
        if(getDataObject() != null) {
            getDataObject().setWebsite(website);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ContactInfoStructDataObject is null!");
        }
    }

    public String getNote()
    {
        if(getDataObject() != null) {
            return getDataObject().getNote();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ContactInfoStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setNote(String note)
    {
        if(getDataObject() != null) {
            getDataObject().setNote(note);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ContactInfoStructDataObject is null!");
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        if(getDataObject() != null) {
            return getDataObject().isEmpty();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ContactInfoStructDataObject is null!");
            boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getStreetAddress() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLocality() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getRegion() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPostalCode() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getCountryName() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getEmailAddress() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPhoneNumber() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getFaxNumber() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getWebsite() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
            return (atLeastOneFieldNonEmpty == false);
        }
    }

    // TBD
    public ContactInfoStructDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
