package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.DomainInfo;
import com.myurldb.ws.data.DomainInfoDataObject;

public class DomainInfoBean extends BeanBase implements DomainInfo
{
    private static final Logger log = Logger.getLogger(DomainInfoBean.class.getName());

    // Embedded data object.
    private DomainInfoDataObject dobj = null;

    public DomainInfoBean()
    {
        this(new DomainInfoDataObject());
    }
    public DomainInfoBean(String guid)
    {
        this(new DomainInfoDataObject(guid));
    }
    public DomainInfoBean(DomainInfoDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public DomainInfoDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DomainInfoDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            if(log.isLoggable(Level.INFO)) log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DomainInfoDataObject is null!");
        }
    }

    public String getDomain()
    {
        if(getDataObject() != null) {
            return getDataObject().getDomain();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DomainInfoDataObject is null!");
            return null;   // ???
        }
    }
    public void setDomain(String domain)
    {
        if(getDataObject() != null) {
            getDataObject().setDomain(domain);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DomainInfoDataObject is null!");
        }
    }

    public Boolean isBanned()
    {
        if(getDataObject() != null) {
            return getDataObject().isBanned();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DomainInfoDataObject is null!");
            return null;   // ???
        }
    }
    public void setBanned(Boolean banned)
    {
        if(getDataObject() != null) {
            getDataObject().setBanned(banned);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DomainInfoDataObject is null!");
        }
    }

    public Boolean isUrlShortener()
    {
        if(getDataObject() != null) {
            return getDataObject().isUrlShortener();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DomainInfoDataObject is null!");
            return null;   // ???
        }
    }
    public void setUrlShortener(Boolean urlShortener)
    {
        if(getDataObject() != null) {
            getDataObject().setUrlShortener(urlShortener);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DomainInfoDataObject is null!");
        }
    }

    public String getCategory()
    {
        if(getDataObject() != null) {
            return getDataObject().getCategory();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DomainInfoDataObject is null!");
            return null;   // ???
        }
    }
    public void setCategory(String category)
    {
        if(getDataObject() != null) {
            getDataObject().setCategory(category);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DomainInfoDataObject is null!");
        }
    }

    public String getReputation()
    {
        if(getDataObject() != null) {
            return getDataObject().getReputation();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DomainInfoDataObject is null!");
            return null;   // ???
        }
    }
    public void setReputation(String reputation)
    {
        if(getDataObject() != null) {
            getDataObject().setReputation(reputation);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DomainInfoDataObject is null!");
        }
    }

    public String getAuthority()
    {
        if(getDataObject() != null) {
            return getDataObject().getAuthority();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DomainInfoDataObject is null!");
            return null;   // ???
        }
    }
    public void setAuthority(String authority)
    {
        if(getDataObject() != null) {
            getDataObject().setAuthority(authority);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DomainInfoDataObject is null!");
        }
    }

    public String getNote()
    {
        if(getDataObject() != null) {
            return getDataObject().getNote();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DomainInfoDataObject is null!");
            return null;   // ???
        }
    }
    public void setNote(String note)
    {
        if(getDataObject() != null) {
            getDataObject().setNote(note);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DomainInfoDataObject is null!");
        }
    }

    public Long getVerifiedTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getVerifiedTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DomainInfoDataObject is null!");
            return null;   // ???
        }
    }
    public void setVerifiedTime(Long verifiedTime)
    {
        if(getDataObject() != null) {
            getDataObject().setVerifiedTime(verifiedTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DomainInfoDataObject is null!");
        }
    }


    // TBD
    public DomainInfoDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
