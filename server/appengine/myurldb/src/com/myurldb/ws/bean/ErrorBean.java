package com.myurldb.ws.bean;

import java.util.Date;
import java.util.logging.Logger;

import com.myurldb.ws.Error;

public class ErrorBean implements Error 
{
    private static final Logger log = Logger.getLogger(ErrorBean.class.getName()); 

	//private String code;
	private String message = null;
	//private String detail; 
	private String resource = null;
    private String cause = null;
    //private Date time;
    
    public ErrorBean() 
    {
        this(null);
    }
    public ErrorBean(String message) 
    {
        this(message, null);
    }
    public ErrorBean(String message, String resource) 
    {
        this(message, resource, null);
    }
    public ErrorBean(String message, String resource, String cause)
    {
        this.message = message;
        this.resource = resource;
        this.cause = cause;
    }
    

    @Override
    public String getMessage()
    {
        return this.message;
    }
    public void setMessage(String message)
    {
        this.message = message;
    }

    @Override
    public String getResource()
    {
        return this.resource;
    }
    public void setResource(String resource)
    {
        this.resource = resource;
    }

    @Override
    public String getCause()
    {
        return this.cause;
    }
    public void setCause(String cause)
    {
        this.cause = cause;
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("message = ");
        sb.append(this.message);
        sb.append(";");
        sb.append("resource = ");
        sb.append(this.resource);
        sb.append(";");
        sb.append("cause = ");
        sb.append(this.cause);
        return sb.toString();
    }

}
