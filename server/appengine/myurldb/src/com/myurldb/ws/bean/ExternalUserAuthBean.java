package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.ExternalUserIdStruct;
import com.myurldb.ws.ExternalUserAuth;
import com.myurldb.ws.data.ExternalUserIdStructDataObject;
import com.myurldb.ws.data.ExternalUserAuthDataObject;

public class ExternalUserAuthBean extends BeanBase implements ExternalUserAuth
{
    private static final Logger log = Logger.getLogger(ExternalUserAuthBean.class.getName());

    // Embedded data object.
    private ExternalUserAuthDataObject dobj = null;

    public ExternalUserAuthBean()
    {
        this(new ExternalUserAuthDataObject());
    }
    public ExternalUserAuthBean(String guid)
    {
        this(new ExternalUserAuthDataObject(guid));
    }
    public ExternalUserAuthBean(ExternalUserAuthDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public ExternalUserAuthDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalUserAuthDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            if(log.isLoggable(Level.INFO)) log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalUserAuthDataObject is null!");
        }
    }

    public String getUser()
    {
        if(getDataObject() != null) {
            return getDataObject().getUser();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalUserAuthDataObject is null!");
            return null;   // ???
        }
    }
    public void setUser(String user)
    {
        if(getDataObject() != null) {
            getDataObject().setUser(user);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalUserAuthDataObject is null!");
        }
    }

    public String getAuthType()
    {
        if(getDataObject() != null) {
            return getDataObject().getAuthType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalUserAuthDataObject is null!");
            return null;   // ???
        }
    }
    public void setAuthType(String authType)
    {
        if(getDataObject() != null) {
            getDataObject().setAuthType(authType);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalUserAuthDataObject is null!");
        }
    }

    public String getProviderId()
    {
        if(getDataObject() != null) {
            return getDataObject().getProviderId();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalUserAuthDataObject is null!");
            return null;   // ???
        }
    }
    public void setProviderId(String providerId)
    {
        if(getDataObject() != null) {
            getDataObject().setProviderId(providerId);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalUserAuthDataObject is null!");
        }
    }

    public String getProviderDomain()
    {
        if(getDataObject() != null) {
            return getDataObject().getProviderDomain();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalUserAuthDataObject is null!");
            return null;   // ???
        }
    }
    public void setProviderDomain(String providerDomain)
    {
        if(getDataObject() != null) {
            getDataObject().setProviderDomain(providerDomain);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalUserAuthDataObject is null!");
        }
    }

    public ExternalUserIdStruct getExternalUserId()
    {
        if(getDataObject() != null) {
            ExternalUserIdStruct _field = getDataObject().getExternalUserId();
            if(_field == null) {
                log.log(Level.INFO, "externalUserId is null.");
                return null;
            } else {
                return new ExternalUserIdStructBean((ExternalUserIdStructDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalUserAuthDataObject is null!");
            return null;   // ???
        }
    }
    public void setExternalUserId(ExternalUserIdStruct externalUserId)
    {
        if(getDataObject() != null) {
            getDataObject().setExternalUserId(
                (externalUserId instanceof ExternalUserIdStructBean) ?
                ((ExternalUserIdStructBean) externalUserId).toDataObject() :
                ((externalUserId instanceof ExternalUserIdStructDataObject) ? externalUserId : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalUserAuthDataObject is null!");
        }
    }

    public String getRequestToken()
    {
        if(getDataObject() != null) {
            return getDataObject().getRequestToken();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalUserAuthDataObject is null!");
            return null;   // ???
        }
    }
    public void setRequestToken(String requestToken)
    {
        if(getDataObject() != null) {
            getDataObject().setRequestToken(requestToken);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalUserAuthDataObject is null!");
        }
    }

    public String getAccessToken()
    {
        if(getDataObject() != null) {
            return getDataObject().getAccessToken();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalUserAuthDataObject is null!");
            return null;   // ???
        }
    }
    public void setAccessToken(String accessToken)
    {
        if(getDataObject() != null) {
            getDataObject().setAccessToken(accessToken);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalUserAuthDataObject is null!");
        }
    }

    public String getAccessTokenSecret()
    {
        if(getDataObject() != null) {
            return getDataObject().getAccessTokenSecret();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalUserAuthDataObject is null!");
            return null;   // ???
        }
    }
    public void setAccessTokenSecret(String accessTokenSecret)
    {
        if(getDataObject() != null) {
            getDataObject().setAccessTokenSecret(accessTokenSecret);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalUserAuthDataObject is null!");
        }
    }

    public String getEmail()
    {
        if(getDataObject() != null) {
            return getDataObject().getEmail();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalUserAuthDataObject is null!");
            return null;   // ???
        }
    }
    public void setEmail(String email)
    {
        if(getDataObject() != null) {
            getDataObject().setEmail(email);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalUserAuthDataObject is null!");
        }
    }

    public String getFirstName()
    {
        if(getDataObject() != null) {
            return getDataObject().getFirstName();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalUserAuthDataObject is null!");
            return null;   // ???
        }
    }
    public void setFirstName(String firstName)
    {
        if(getDataObject() != null) {
            getDataObject().setFirstName(firstName);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalUserAuthDataObject is null!");
        }
    }

    public String getLastName()
    {
        if(getDataObject() != null) {
            return getDataObject().getLastName();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalUserAuthDataObject is null!");
            return null;   // ???
        }
    }
    public void setLastName(String lastName)
    {
        if(getDataObject() != null) {
            getDataObject().setLastName(lastName);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalUserAuthDataObject is null!");
        }
    }

    public String getFullName()
    {
        if(getDataObject() != null) {
            return getDataObject().getFullName();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalUserAuthDataObject is null!");
            return null;   // ???
        }
    }
    public void setFullName(String fullName)
    {
        if(getDataObject() != null) {
            getDataObject().setFullName(fullName);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalUserAuthDataObject is null!");
        }
    }

    public String getDisplayName()
    {
        if(getDataObject() != null) {
            return getDataObject().getDisplayName();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalUserAuthDataObject is null!");
            return null;   // ???
        }
    }
    public void setDisplayName(String displayName)
    {
        if(getDataObject() != null) {
            getDataObject().setDisplayName(displayName);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalUserAuthDataObject is null!");
        }
    }

    public String getDescription()
    {
        if(getDataObject() != null) {
            return getDataObject().getDescription();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalUserAuthDataObject is null!");
            return null;   // ???
        }
    }
    public void setDescription(String description)
    {
        if(getDataObject() != null) {
            getDataObject().setDescription(description);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalUserAuthDataObject is null!");
        }
    }

    public String getGender()
    {
        if(getDataObject() != null) {
            return getDataObject().getGender();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalUserAuthDataObject is null!");
            return null;   // ???
        }
    }
    public void setGender(String gender)
    {
        if(getDataObject() != null) {
            getDataObject().setGender(gender);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalUserAuthDataObject is null!");
        }
    }

    public String getDateOfBirth()
    {
        if(getDataObject() != null) {
            return getDataObject().getDateOfBirth();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalUserAuthDataObject is null!");
            return null;   // ???
        }
    }
    public void setDateOfBirth(String dateOfBirth)
    {
        if(getDataObject() != null) {
            getDataObject().setDateOfBirth(dateOfBirth);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalUserAuthDataObject is null!");
        }
    }

    public String getProfileImageUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getProfileImageUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalUserAuthDataObject is null!");
            return null;   // ???
        }
    }
    public void setProfileImageUrl(String profileImageUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setProfileImageUrl(profileImageUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalUserAuthDataObject is null!");
        }
    }

    public String getTimeZone()
    {
        if(getDataObject() != null) {
            return getDataObject().getTimeZone();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalUserAuthDataObject is null!");
            return null;   // ???
        }
    }
    public void setTimeZone(String timeZone)
    {
        if(getDataObject() != null) {
            getDataObject().setTimeZone(timeZone);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalUserAuthDataObject is null!");
        }
    }

    public String getPostalCode()
    {
        if(getDataObject() != null) {
            return getDataObject().getPostalCode();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalUserAuthDataObject is null!");
            return null;   // ???
        }
    }
    public void setPostalCode(String postalCode)
    {
        if(getDataObject() != null) {
            getDataObject().setPostalCode(postalCode);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalUserAuthDataObject is null!");
        }
    }

    public String getLocation()
    {
        if(getDataObject() != null) {
            return getDataObject().getLocation();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalUserAuthDataObject is null!");
            return null;   // ???
        }
    }
    public void setLocation(String location)
    {
        if(getDataObject() != null) {
            getDataObject().setLocation(location);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalUserAuthDataObject is null!");
        }
    }

    public String getCountry()
    {
        if(getDataObject() != null) {
            return getDataObject().getCountry();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalUserAuthDataObject is null!");
            return null;   // ???
        }
    }
    public void setCountry(String country)
    {
        if(getDataObject() != null) {
            getDataObject().setCountry(country);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalUserAuthDataObject is null!");
        }
    }

    public String getLanguage()
    {
        if(getDataObject() != null) {
            return getDataObject().getLanguage();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalUserAuthDataObject is null!");
            return null;   // ???
        }
    }
    public void setLanguage(String language)
    {
        if(getDataObject() != null) {
            getDataObject().setLanguage(language);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalUserAuthDataObject is null!");
        }
    }

    public String getStatus()
    {
        if(getDataObject() != null) {
            return getDataObject().getStatus();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalUserAuthDataObject is null!");
            return null;   // ???
        }
    }
    public void setStatus(String status)
    {
        if(getDataObject() != null) {
            getDataObject().setStatus(status);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalUserAuthDataObject is null!");
        }
    }

    public Long getAuthTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getAuthTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalUserAuthDataObject is null!");
            return null;   // ???
        }
    }
    public void setAuthTime(Long authTime)
    {
        if(getDataObject() != null) {
            getDataObject().setAuthTime(authTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalUserAuthDataObject is null!");
        }
    }

    public Long getExpirationTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getExpirationTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalUserAuthDataObject is null!");
            return null;   // ???
        }
    }
    public void setExpirationTime(Long expirationTime)
    {
        if(getDataObject() != null) {
            getDataObject().setExpirationTime(expirationTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ExternalUserAuthDataObject is null!");
        }
    }


    // TBD
    public ExternalUserAuthDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
