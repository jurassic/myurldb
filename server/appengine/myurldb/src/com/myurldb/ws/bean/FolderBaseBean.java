package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.FolderBase;
import com.myurldb.ws.data.FolderBaseDataObject;

public abstract class FolderBaseBean extends BeanBase implements FolderBase
{
    private static final Logger log = Logger.getLogger(FolderBaseBean.class.getName());

    public FolderBaseBean()
    {
        super();
    }

    @Override
    public abstract FolderBaseDataObject getDataObject();

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FolderBaseDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            if(log.isLoggable(Level.INFO)) log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FolderBaseDataObject is null!");
        }
    }

    public String getAppClient()
    {
        if(getDataObject() != null) {
            return getDataObject().getAppClient();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FolderBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setAppClient(String appClient)
    {
        if(getDataObject() != null) {
            getDataObject().setAppClient(appClient);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FolderBaseDataObject is null!");
        }
    }

    public String getClientRootDomain()
    {
        if(getDataObject() != null) {
            return getDataObject().getClientRootDomain();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FolderBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setClientRootDomain(String clientRootDomain)
    {
        if(getDataObject() != null) {
            getDataObject().setClientRootDomain(clientRootDomain);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FolderBaseDataObject is null!");
        }
    }

    public String getUser()
    {
        if(getDataObject() != null) {
            return getDataObject().getUser();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FolderBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setUser(String user)
    {
        if(getDataObject() != null) {
            getDataObject().setUser(user);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FolderBaseDataObject is null!");
        }
    }

    public String getTitle()
    {
        if(getDataObject() != null) {
            return getDataObject().getTitle();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FolderBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setTitle(String title)
    {
        if(getDataObject() != null) {
            getDataObject().setTitle(title);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FolderBaseDataObject is null!");
        }
    }

    public String getDescription()
    {
        if(getDataObject() != null) {
            return getDataObject().getDescription();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FolderBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setDescription(String description)
    {
        if(getDataObject() != null) {
            getDataObject().setDescription(description);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FolderBaseDataObject is null!");
        }
    }

    public String getType()
    {
        if(getDataObject() != null) {
            return getDataObject().getType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FolderBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setType(String type)
    {
        if(getDataObject() != null) {
            getDataObject().setType(type);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FolderBaseDataObject is null!");
        }
    }

    public String getCategory()
    {
        if(getDataObject() != null) {
            return getDataObject().getCategory();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FolderBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setCategory(String category)
    {
        if(getDataObject() != null) {
            getDataObject().setCategory(category);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FolderBaseDataObject is null!");
        }
    }

    public String getParent()
    {
        if(getDataObject() != null) {
            return getDataObject().getParent();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FolderBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setParent(String parent)
    {
        if(getDataObject() != null) {
            getDataObject().setParent(parent);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FolderBaseDataObject is null!");
        }
    }

    public String getAggregate()
    {
        if(getDataObject() != null) {
            return getDataObject().getAggregate();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FolderBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setAggregate(String aggregate)
    {
        if(getDataObject() != null) {
            getDataObject().setAggregate(aggregate);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FolderBaseDataObject is null!");
        }
    }

    public String getAcl()
    {
        if(getDataObject() != null) {
            return getDataObject().getAcl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FolderBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setAcl(String acl)
    {
        if(getDataObject() != null) {
            getDataObject().setAcl(acl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FolderBaseDataObject is null!");
        }
    }

    public Boolean isExportable()
    {
        if(getDataObject() != null) {
            return getDataObject().isExportable();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FolderBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setExportable(Boolean exportable)
    {
        if(getDataObject() != null) {
            getDataObject().setExportable(exportable);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FolderBaseDataObject is null!");
        }
    }

    public String getStatus()
    {
        if(getDataObject() != null) {
            return getDataObject().getStatus();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FolderBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setStatus(String status)
    {
        if(getDataObject() != null) {
            getDataObject().setStatus(status);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FolderBaseDataObject is null!");
        }
    }

    public String getNote()
    {
        if(getDataObject() != null) {
            return getDataObject().getNote();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FolderBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setNote(String note)
    {
        if(getDataObject() != null) {
            getDataObject().setNote(note);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FolderBaseDataObject is null!");
        }
    }



    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
