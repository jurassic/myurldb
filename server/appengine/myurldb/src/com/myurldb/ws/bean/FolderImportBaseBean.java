package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.FolderImportBase;
import com.myurldb.ws.data.FolderImportBaseDataObject;

public abstract class FolderImportBaseBean extends BeanBase implements FolderImportBase
{
    private static final Logger log = Logger.getLogger(FolderImportBaseBean.class.getName());

    public FolderImportBaseBean()
    {
        super();
    }

    @Override
    public abstract FolderImportBaseDataObject getDataObject();

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FolderImportBaseDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            if(log.isLoggable(Level.INFO)) log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FolderImportBaseDataObject is null!");
        }
    }

    public String getUser()
    {
        if(getDataObject() != null) {
            return getDataObject().getUser();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FolderImportBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setUser(String user)
    {
        if(getDataObject() != null) {
            getDataObject().setUser(user);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FolderImportBaseDataObject is null!");
        }
    }

    public Integer getPrecedence()
    {
        if(getDataObject() != null) {
            return getDataObject().getPrecedence();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FolderImportBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setPrecedence(Integer precedence)
    {
        if(getDataObject() != null) {
            getDataObject().setPrecedence(precedence);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FolderImportBaseDataObject is null!");
        }
    }

    public String getImportType()
    {
        if(getDataObject() != null) {
            return getDataObject().getImportType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FolderImportBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setImportType(String importType)
    {
        if(getDataObject() != null) {
            getDataObject().setImportType(importType);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FolderImportBaseDataObject is null!");
        }
    }

    public String getImportedFolder()
    {
        if(getDataObject() != null) {
            return getDataObject().getImportedFolder();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FolderImportBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setImportedFolder(String importedFolder)
    {
        if(getDataObject() != null) {
            getDataObject().setImportedFolder(importedFolder);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FolderImportBaseDataObject is null!");
        }
    }

    public String getImportedFolderUser()
    {
        if(getDataObject() != null) {
            return getDataObject().getImportedFolderUser();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FolderImportBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setImportedFolderUser(String importedFolderUser)
    {
        if(getDataObject() != null) {
            getDataObject().setImportedFolderUser(importedFolderUser);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FolderImportBaseDataObject is null!");
        }
    }

    public String getImportedFolderTitle()
    {
        if(getDataObject() != null) {
            return getDataObject().getImportedFolderTitle();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FolderImportBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setImportedFolderTitle(String importedFolderTitle)
    {
        if(getDataObject() != null) {
            getDataObject().setImportedFolderTitle(importedFolderTitle);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FolderImportBaseDataObject is null!");
        }
    }

    public String getImportedFolderPath()
    {
        if(getDataObject() != null) {
            return getDataObject().getImportedFolderPath();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FolderImportBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setImportedFolderPath(String importedFolderPath)
    {
        if(getDataObject() != null) {
            getDataObject().setImportedFolderPath(importedFolderPath);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FolderImportBaseDataObject is null!");
        }
    }

    public String getStatus()
    {
        if(getDataObject() != null) {
            return getDataObject().getStatus();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FolderImportBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setStatus(String status)
    {
        if(getDataObject() != null) {
            getDataObject().setStatus(status);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FolderImportBaseDataObject is null!");
        }
    }

    public String getNote()
    {
        if(getDataObject() != null) {
            return getDataObject().getNote();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FolderImportBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setNote(String note)
    {
        if(getDataObject() != null) {
            getDataObject().setNote(note);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FolderImportBaseDataObject is null!");
        }
    }



    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
