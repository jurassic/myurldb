package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.FullNameStruct;
import com.myurldb.ws.data.FullNameStructDataObject;

public class FullNameStructBean implements FullNameStruct
{
    private static final Logger log = Logger.getLogger(FullNameStructBean.class.getName());

    // Embedded data object.
    private FullNameStructDataObject dobj = null;

    public FullNameStructBean()
    {
        this(new FullNameStructDataObject());
    }
    public FullNameStructBean(FullNameStructDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    public FullNameStructDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getUuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getUuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FullNameStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setUuid(String uuid)
    {
        if(getDataObject() != null) {
            getDataObject().setUuid(uuid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FullNameStructDataObject is null!");
        }
    }

    public String getDisplayName()
    {
        if(getDataObject() != null) {
            return getDataObject().getDisplayName();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FullNameStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setDisplayName(String displayName)
    {
        if(getDataObject() != null) {
            getDataObject().setDisplayName(displayName);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FullNameStructDataObject is null!");
        }
    }

    public String getLastName()
    {
        if(getDataObject() != null) {
            return getDataObject().getLastName();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FullNameStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setLastName(String lastName)
    {
        if(getDataObject() != null) {
            getDataObject().setLastName(lastName);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FullNameStructDataObject is null!");
        }
    }

    public String getFirstName()
    {
        if(getDataObject() != null) {
            return getDataObject().getFirstName();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FullNameStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setFirstName(String firstName)
    {
        if(getDataObject() != null) {
            getDataObject().setFirstName(firstName);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FullNameStructDataObject is null!");
        }
    }

    public String getMiddleName1()
    {
        if(getDataObject() != null) {
            return getDataObject().getMiddleName1();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FullNameStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setMiddleName1(String middleName1)
    {
        if(getDataObject() != null) {
            getDataObject().setMiddleName1(middleName1);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FullNameStructDataObject is null!");
        }
    }

    public String getMiddleName2()
    {
        if(getDataObject() != null) {
            return getDataObject().getMiddleName2();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FullNameStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setMiddleName2(String middleName2)
    {
        if(getDataObject() != null) {
            getDataObject().setMiddleName2(middleName2);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FullNameStructDataObject is null!");
        }
    }

    public String getMiddleInitial()
    {
        if(getDataObject() != null) {
            return getDataObject().getMiddleInitial();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FullNameStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setMiddleInitial(String middleInitial)
    {
        if(getDataObject() != null) {
            getDataObject().setMiddleInitial(middleInitial);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FullNameStructDataObject is null!");
        }
    }

    public String getSalutation()
    {
        if(getDataObject() != null) {
            return getDataObject().getSalutation();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FullNameStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setSalutation(String salutation)
    {
        if(getDataObject() != null) {
            getDataObject().setSalutation(salutation);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FullNameStructDataObject is null!");
        }
    }

    public String getSuffix()
    {
        if(getDataObject() != null) {
            return getDataObject().getSuffix();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FullNameStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setSuffix(String suffix)
    {
        if(getDataObject() != null) {
            getDataObject().setSuffix(suffix);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FullNameStructDataObject is null!");
        }
    }

    public String getNote()
    {
        if(getDataObject() != null) {
            return getDataObject().getNote();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FullNameStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setNote(String note)
    {
        if(getDataObject() != null) {
            getDataObject().setNote(note);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FullNameStructDataObject is null!");
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        if(getDataObject() != null) {
            return getDataObject().isEmpty();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FullNameStructDataObject is null!");
            boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getDisplayName() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLastName() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getFirstName() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getMiddleName1() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getMiddleName2() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getMiddleInitial() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSalutation() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSuffix() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
            return (atLeastOneFieldNonEmpty == false);
        }
    }

    // TBD
    public FullNameStructDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
