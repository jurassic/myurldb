package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.GeoCoordinateStruct;
import com.myurldb.ws.data.GeoCoordinateStructDataObject;

public class GeoCoordinateStructBean implements GeoCoordinateStruct
{
    private static final Logger log = Logger.getLogger(GeoCoordinateStructBean.class.getName());

    // Embedded data object.
    private GeoCoordinateStructDataObject dobj = null;

    public GeoCoordinateStructBean()
    {
        this(new GeoCoordinateStructDataObject());
    }
    public GeoCoordinateStructBean(GeoCoordinateStructDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    public GeoCoordinateStructDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getUuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getUuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GeoCoordinateStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setUuid(String uuid)
    {
        if(getDataObject() != null) {
            getDataObject().setUuid(uuid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GeoCoordinateStructDataObject is null!");
        }
    }

    public Double getLatitude()
    {
        if(getDataObject() != null) {
            return getDataObject().getLatitude();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GeoCoordinateStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setLatitude(Double latitude)
    {
        if(getDataObject() != null) {
            getDataObject().setLatitude(latitude);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GeoCoordinateStructDataObject is null!");
        }
    }

    public Double getLongitude()
    {
        if(getDataObject() != null) {
            return getDataObject().getLongitude();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GeoCoordinateStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setLongitude(Double longitude)
    {
        if(getDataObject() != null) {
            getDataObject().setLongitude(longitude);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GeoCoordinateStructDataObject is null!");
        }
    }

    public Double getAltitude()
    {
        if(getDataObject() != null) {
            return getDataObject().getAltitude();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GeoCoordinateStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setAltitude(Double altitude)
    {
        if(getDataObject() != null) {
            getDataObject().setAltitude(altitude);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GeoCoordinateStructDataObject is null!");
        }
    }

    public Boolean isSensorUsed()
    {
        if(getDataObject() != null) {
            return getDataObject().isSensorUsed();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GeoCoordinateStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setSensorUsed(Boolean sensorUsed)
    {
        if(getDataObject() != null) {
            getDataObject().setSensorUsed(sensorUsed);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GeoCoordinateStructDataObject is null!");
        }
    }

    public String getAccuracy()
    {
        if(getDataObject() != null) {
            return getDataObject().getAccuracy();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GeoCoordinateStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setAccuracy(String accuracy)
    {
        if(getDataObject() != null) {
            getDataObject().setAccuracy(accuracy);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GeoCoordinateStructDataObject is null!");
        }
    }

    public String getAltitudeAccuracy()
    {
        if(getDataObject() != null) {
            return getDataObject().getAltitudeAccuracy();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GeoCoordinateStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setAltitudeAccuracy(String altitudeAccuracy)
    {
        if(getDataObject() != null) {
            getDataObject().setAltitudeAccuracy(altitudeAccuracy);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GeoCoordinateStructDataObject is null!");
        }
    }

    public Double getHeading()
    {
        if(getDataObject() != null) {
            return getDataObject().getHeading();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GeoCoordinateStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setHeading(Double heading)
    {
        if(getDataObject() != null) {
            getDataObject().setHeading(heading);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GeoCoordinateStructDataObject is null!");
        }
    }

    public Double getSpeed()
    {
        if(getDataObject() != null) {
            return getDataObject().getSpeed();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GeoCoordinateStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setSpeed(Double speed)
    {
        if(getDataObject() != null) {
            getDataObject().setSpeed(speed);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GeoCoordinateStructDataObject is null!");
        }
    }

    public String getNote()
    {
        if(getDataObject() != null) {
            return getDataObject().getNote();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GeoCoordinateStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setNote(String note)
    {
        if(getDataObject() != null) {
            getDataObject().setNote(note);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GeoCoordinateStructDataObject is null!");
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        if(getDataObject() != null) {
            return getDataObject().isEmpty();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GeoCoordinateStructDataObject is null!");
            boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getLatitude() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLongitude() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAltitude() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && isSensorUsed() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAccuracy() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAltitudeAccuracy() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHeading() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSpeed() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
            return (atLeastOneFieldNonEmpty == false);
        }
    }

    // TBD
    public GeoCoordinateStructDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
