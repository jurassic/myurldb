package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.CellLatitudeLongitude;
import com.myurldb.ws.GeoCoordinateStruct;
import com.myurldb.ws.GeoLink;
import com.myurldb.ws.data.CellLatitudeLongitudeDataObject;
import com.myurldb.ws.data.GeoCoordinateStructDataObject;
import com.myurldb.ws.data.GeoLinkDataObject;

public class GeoLinkBean extends BeanBase implements GeoLink
{
    private static final Logger log = Logger.getLogger(GeoLinkBean.class.getName());

    // Embedded data object.
    private GeoLinkDataObject dobj = null;

    public GeoLinkBean()
    {
        this(new GeoLinkDataObject());
    }
    public GeoLinkBean(String guid)
    {
        this(new GeoLinkDataObject(guid));
    }
    public GeoLinkBean(GeoLinkDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public GeoLinkDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GeoLinkDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            if(log.isLoggable(Level.INFO)) log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GeoLinkDataObject is null!");
        }
    }

    public String getShortLink()
    {
        if(getDataObject() != null) {
            return getDataObject().getShortLink();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GeoLinkDataObject is null!");
            return null;   // ???
        }
    }
    public void setShortLink(String shortLink)
    {
        if(getDataObject() != null) {
            getDataObject().setShortLink(shortLink);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GeoLinkDataObject is null!");
        }
    }

    public String getShortUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getShortUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GeoLinkDataObject is null!");
            return null;   // ???
        }
    }
    public void setShortUrl(String shortUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setShortUrl(shortUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GeoLinkDataObject is null!");
        }
    }

    public GeoCoordinateStruct getGeoCoordinate()
    {
        if(getDataObject() != null) {
            GeoCoordinateStruct _field = getDataObject().getGeoCoordinate();
            if(_field == null) {
                log.log(Level.INFO, "geoCoordinate is null.");
                return null;
            } else {
                return new GeoCoordinateStructBean((GeoCoordinateStructDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GeoLinkDataObject is null!");
            return null;   // ???
        }
    }
    public void setGeoCoordinate(GeoCoordinateStruct geoCoordinate)
    {
        if(getDataObject() != null) {
            getDataObject().setGeoCoordinate(
                (geoCoordinate instanceof GeoCoordinateStructBean) ?
                ((GeoCoordinateStructBean) geoCoordinate).toDataObject() :
                ((geoCoordinate instanceof GeoCoordinateStructDataObject) ? geoCoordinate : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GeoLinkDataObject is null!");
        }
    }

    public CellLatitudeLongitude getGeoCell()
    {
        if(getDataObject() != null) {
            CellLatitudeLongitude _field = getDataObject().getGeoCell();
            if(_field == null) {
                log.log(Level.INFO, "geoCell is null.");
                return null;
            } else {
                return new CellLatitudeLongitudeBean((CellLatitudeLongitudeDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GeoLinkDataObject is null!");
            return null;   // ???
        }
    }
    public void setGeoCell(CellLatitudeLongitude geoCell)
    {
        if(getDataObject() != null) {
            getDataObject().setGeoCell(
                (geoCell instanceof CellLatitudeLongitudeBean) ?
                ((CellLatitudeLongitudeBean) geoCell).toDataObject() :
                ((geoCell instanceof CellLatitudeLongitudeDataObject) ? geoCell : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GeoLinkDataObject is null!");
        }
    }

    public String getStatus()
    {
        if(getDataObject() != null) {
            return getDataObject().getStatus();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GeoLinkDataObject is null!");
            return null;   // ???
        }
    }
    public void setStatus(String status)
    {
        if(getDataObject() != null) {
            getDataObject().setStatus(status);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GeoLinkDataObject is null!");
        }
    }


    // TBD
    public GeoLinkDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
