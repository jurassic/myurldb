package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.GeoPointStruct;
import com.myurldb.ws.data.GeoPointStructDataObject;

public class GeoPointStructBean implements GeoPointStruct
{
    private static final Logger log = Logger.getLogger(GeoPointStructBean.class.getName());

    // Embedded data object.
    private GeoPointStructDataObject dobj = null;

    public GeoPointStructBean()
    {
        this(new GeoPointStructDataObject());
    }
    public GeoPointStructBean(GeoPointStructDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    public GeoPointStructDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getUuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getUuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GeoPointStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setUuid(String uuid)
    {
        if(getDataObject() != null) {
            getDataObject().setUuid(uuid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GeoPointStructDataObject is null!");
        }
    }

    public Double getLatitude()
    {
        if(getDataObject() != null) {
            return getDataObject().getLatitude();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GeoPointStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setLatitude(Double latitude)
    {
        if(getDataObject() != null) {
            getDataObject().setLatitude(latitude);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GeoPointStructDataObject is null!");
        }
    }

    public Double getLongitude()
    {
        if(getDataObject() != null) {
            return getDataObject().getLongitude();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GeoPointStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setLongitude(Double longitude)
    {
        if(getDataObject() != null) {
            getDataObject().setLongitude(longitude);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GeoPointStructDataObject is null!");
        }
    }

    public Double getAltitude()
    {
        if(getDataObject() != null) {
            return getDataObject().getAltitude();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GeoPointStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setAltitude(Double altitude)
    {
        if(getDataObject() != null) {
            getDataObject().setAltitude(altitude);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GeoPointStructDataObject is null!");
        }
    }

    public Boolean isSensorUsed()
    {
        if(getDataObject() != null) {
            return getDataObject().isSensorUsed();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GeoPointStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setSensorUsed(Boolean sensorUsed)
    {
        if(getDataObject() != null) {
            getDataObject().setSensorUsed(sensorUsed);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GeoPointStructDataObject is null!");
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        if(getDataObject() != null) {
            return getDataObject().isEmpty();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded GeoPointStructDataObject is null!");
            boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getLatitude() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLongitude() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAltitude() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && isSensorUsed() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
            return (atLeastOneFieldNonEmpty == false);
        }
    }

    // TBD
    public GeoPointStructDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
