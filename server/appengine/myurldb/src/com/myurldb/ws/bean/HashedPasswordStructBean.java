package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.HashedPasswordStruct;
import com.myurldb.ws.data.HashedPasswordStructDataObject;

public class HashedPasswordStructBean implements HashedPasswordStruct
{
    private static final Logger log = Logger.getLogger(HashedPasswordStructBean.class.getName());

    // Embedded data object.
    private HashedPasswordStructDataObject dobj = null;

    public HashedPasswordStructBean()
    {
        this(new HashedPasswordStructDataObject());
    }
    public HashedPasswordStructBean(HashedPasswordStructDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    public HashedPasswordStructDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getUuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getUuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded HashedPasswordStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setUuid(String uuid)
    {
        if(getDataObject() != null) {
            getDataObject().setUuid(uuid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded HashedPasswordStructDataObject is null!");
        }
    }

    public String getPlainText()
    {
        if(getDataObject() != null) {
            return getDataObject().getPlainText();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded HashedPasswordStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setPlainText(String plainText)
    {
        if(getDataObject() != null) {
            getDataObject().setPlainText(plainText);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded HashedPasswordStructDataObject is null!");
        }
    }

    public String getHashedText()
    {
        if(getDataObject() != null) {
            return getDataObject().getHashedText();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded HashedPasswordStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setHashedText(String hashedText)
    {
        if(getDataObject() != null) {
            getDataObject().setHashedText(hashedText);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded HashedPasswordStructDataObject is null!");
        }
    }

    public String getSalt()
    {
        if(getDataObject() != null) {
            return getDataObject().getSalt();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded HashedPasswordStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setSalt(String salt)
    {
        if(getDataObject() != null) {
            getDataObject().setSalt(salt);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded HashedPasswordStructDataObject is null!");
        }
    }

    public String getAlgorithm()
    {
        if(getDataObject() != null) {
            return getDataObject().getAlgorithm();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded HashedPasswordStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setAlgorithm(String algorithm)
    {
        if(getDataObject() != null) {
            getDataObject().setAlgorithm(algorithm);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded HashedPasswordStructDataObject is null!");
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        if(getDataObject() != null) {
            return getDataObject().isEmpty();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded HashedPasswordStructDataObject is null!");
            boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getPlainText() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHashedText() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSalt() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAlgorithm() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
            return (atLeastOneFieldNonEmpty == false);
        }
    }

    // TBD
    public HashedPasswordStructDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
