package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.KeywordCrowdTally;
import com.myurldb.ws.data.KeywordCrowdTallyDataObject;

public class KeywordCrowdTallyBean extends CrowdTallyBaseBean implements KeywordCrowdTally
{
    private static final Logger log = Logger.getLogger(KeywordCrowdTallyBean.class.getName());

    // Embedded data object.
    private KeywordCrowdTallyDataObject dobj = null;

    public KeywordCrowdTallyBean()
    {
        this(new KeywordCrowdTallyDataObject());
    }
    public KeywordCrowdTallyBean(String guid)
    {
        this(new KeywordCrowdTallyDataObject(guid));
    }
    public KeywordCrowdTallyBean(KeywordCrowdTallyDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public KeywordCrowdTallyDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getKeywordFolder()
    {
        if(getDataObject() != null) {
            return getDataObject().getKeywordFolder();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded KeywordCrowdTallyDataObject is null!");
            return null;   // ???
        }
    }
    public void setKeywordFolder(String keywordFolder)
    {
        if(getDataObject() != null) {
            getDataObject().setKeywordFolder(keywordFolder);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded KeywordCrowdTallyDataObject is null!");
        }
    }

    public String getFolderPath()
    {
        if(getDataObject() != null) {
            return getDataObject().getFolderPath();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded KeywordCrowdTallyDataObject is null!");
            return null;   // ???
        }
    }
    public void setFolderPath(String folderPath)
    {
        if(getDataObject() != null) {
            getDataObject().setFolderPath(folderPath);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded KeywordCrowdTallyDataObject is null!");
        }
    }

    public String getKeyword()
    {
        if(getDataObject() != null) {
            return getDataObject().getKeyword();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded KeywordCrowdTallyDataObject is null!");
            return null;   // ???
        }
    }
    public void setKeyword(String keyword)
    {
        if(getDataObject() != null) {
            getDataObject().setKeyword(keyword);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded KeywordCrowdTallyDataObject is null!");
        }
    }

    public String getQueryKey()
    {
        if(getDataObject() != null) {
            return getDataObject().getQueryKey();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded KeywordCrowdTallyDataObject is null!");
            return null;   // ???
        }
    }
    public void setQueryKey(String queryKey)
    {
        if(getDataObject() != null) {
            getDataObject().setQueryKey(queryKey);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded KeywordCrowdTallyDataObject is null!");
        }
    }

    public String getScope()
    {
        if(getDataObject() != null) {
            return getDataObject().getScope();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded KeywordCrowdTallyDataObject is null!");
            return null;   // ???
        }
    }
    public void setScope(String scope)
    {
        if(getDataObject() != null) {
            getDataObject().setScope(scope);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded KeywordCrowdTallyDataObject is null!");
        }
    }

    public Boolean isCaseSensitive()
    {
        if(getDataObject() != null) {
            return getDataObject().isCaseSensitive();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded KeywordCrowdTallyDataObject is null!");
            return null;   // ???
        }
    }
    public void setCaseSensitive(Boolean caseSensitive)
    {
        if(getDataObject() != null) {
            getDataObject().setCaseSensitive(caseSensitive);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded KeywordCrowdTallyDataObject is null!");
        }
    }


    // TBD
    public KeywordCrowdTallyDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
