package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.KeywordFolder;
import com.myurldb.ws.data.KeywordFolderDataObject;

public class KeywordFolderBean extends FolderBaseBean implements KeywordFolder
{
    private static final Logger log = Logger.getLogger(KeywordFolderBean.class.getName());

    // Embedded data object.
    private KeywordFolderDataObject dobj = null;

    public KeywordFolderBean()
    {
        this(new KeywordFolderDataObject());
    }
    public KeywordFolderBean(String guid)
    {
        this(new KeywordFolderDataObject(guid));
    }
    public KeywordFolderBean(KeywordFolderDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public KeywordFolderDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getFolderPath()
    {
        if(getDataObject() != null) {
            return getDataObject().getFolderPath();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded KeywordFolderDataObject is null!");
            return null;   // ???
        }
    }
    public void setFolderPath(String folderPath)
    {
        if(getDataObject() != null) {
            getDataObject().setFolderPath(folderPath);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded KeywordFolderDataObject is null!");
        }
    }


    // TBD
    public KeywordFolderDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
