package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.KeywordFolderImport;
import com.myurldb.ws.data.KeywordFolderImportDataObject;

public class KeywordFolderImportBean extends FolderImportBaseBean implements KeywordFolderImport
{
    private static final Logger log = Logger.getLogger(KeywordFolderImportBean.class.getName());

    // Embedded data object.
    private KeywordFolderImportDataObject dobj = null;

    public KeywordFolderImportBean()
    {
        this(new KeywordFolderImportDataObject());
    }
    public KeywordFolderImportBean(String guid)
    {
        this(new KeywordFolderImportDataObject(guid));
    }
    public KeywordFolderImportBean(KeywordFolderImportDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public KeywordFolderImportDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getKeywordFolder()
    {
        if(getDataObject() != null) {
            return getDataObject().getKeywordFolder();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded KeywordFolderImportDataObject is null!");
            return null;   // ???
        }
    }
    public void setKeywordFolder(String keywordFolder)
    {
        if(getDataObject() != null) {
            getDataObject().setKeywordFolder(keywordFolder);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded KeywordFolderImportDataObject is null!");
        }
    }


    // TBD
    public KeywordFolderImportDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
