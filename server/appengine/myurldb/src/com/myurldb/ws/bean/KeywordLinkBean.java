package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.KeywordLink;
import com.myurldb.ws.data.KeywordLinkDataObject;

public class KeywordLinkBean extends NavLinkBaseBean implements KeywordLink
{
    private static final Logger log = Logger.getLogger(KeywordLinkBean.class.getName());

    // Embedded data object.
    private KeywordLinkDataObject dobj = null;

    public KeywordLinkBean()
    {
        this(new KeywordLinkDataObject());
    }
    public KeywordLinkBean(String guid)
    {
        this(new KeywordLinkDataObject(guid));
    }
    public KeywordLinkBean(KeywordLinkDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public KeywordLinkDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getKeywordFolder()
    {
        if(getDataObject() != null) {
            return getDataObject().getKeywordFolder();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded KeywordLinkDataObject is null!");
            return null;   // ???
        }
    }
    public void setKeywordFolder(String keywordFolder)
    {
        if(getDataObject() != null) {
            getDataObject().setKeywordFolder(keywordFolder);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded KeywordLinkDataObject is null!");
        }
    }

    public String getFolderPath()
    {
        if(getDataObject() != null) {
            return getDataObject().getFolderPath();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded KeywordLinkDataObject is null!");
            return null;   // ???
        }
    }
    public void setFolderPath(String folderPath)
    {
        if(getDataObject() != null) {
            getDataObject().setFolderPath(folderPath);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded KeywordLinkDataObject is null!");
        }
    }

    public String getKeyword()
    {
        if(getDataObject() != null) {
            return getDataObject().getKeyword();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded KeywordLinkDataObject is null!");
            return null;   // ???
        }
    }
    public void setKeyword(String keyword)
    {
        if(getDataObject() != null) {
            getDataObject().setKeyword(keyword);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded KeywordLinkDataObject is null!");
        }
    }

    public String getQueryKey()
    {
        if(getDataObject() != null) {
            return getDataObject().getQueryKey();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded KeywordLinkDataObject is null!");
            return null;   // ???
        }
    }
    public void setQueryKey(String queryKey)
    {
        if(getDataObject() != null) {
            getDataObject().setQueryKey(queryKey);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded KeywordLinkDataObject is null!");
        }
    }

    public String getScope()
    {
        if(getDataObject() != null) {
            return getDataObject().getScope();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded KeywordLinkDataObject is null!");
            return null;   // ???
        }
    }
    public void setScope(String scope)
    {
        if(getDataObject() != null) {
            getDataObject().setScope(scope);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded KeywordLinkDataObject is null!");
        }
    }

    public Boolean isDynamic()
    {
        if(getDataObject() != null) {
            return getDataObject().isDynamic();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded KeywordLinkDataObject is null!");
            return null;   // ???
        }
    }
    public void setDynamic(Boolean dynamic)
    {
        if(getDataObject() != null) {
            getDataObject().setDynamic(dynamic);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded KeywordLinkDataObject is null!");
        }
    }

    public Boolean isCaseSensitive()
    {
        if(getDataObject() != null) {
            return getDataObject().isCaseSensitive();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded KeywordLinkDataObject is null!");
            return null;   // ???
        }
    }
    public void setCaseSensitive(Boolean caseSensitive)
    {
        if(getDataObject() != null) {
            getDataObject().setCaseSensitive(caseSensitive);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded KeywordLinkDataObject is null!");
        }
    }


    // TBD
    public KeywordLinkDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
