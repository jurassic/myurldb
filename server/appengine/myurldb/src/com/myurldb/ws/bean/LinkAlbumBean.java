package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.LinkAlbum;
import com.myurldb.ws.data.LinkAlbumDataObject;

public class LinkAlbumBean extends BeanBase implements LinkAlbum
{
    private static final Logger log = Logger.getLogger(LinkAlbumBean.class.getName());

    // Embedded data object.
    private LinkAlbumDataObject dobj = null;

    public LinkAlbumBean()
    {
        this(new LinkAlbumDataObject());
    }
    public LinkAlbumBean(String guid)
    {
        this(new LinkAlbumDataObject(guid));
    }
    public LinkAlbumBean(LinkAlbumDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public LinkAlbumDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkAlbumDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            if(log.isLoggable(Level.INFO)) log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkAlbumDataObject is null!");
        }
    }

    public String getAppClient()
    {
        if(getDataObject() != null) {
            return getDataObject().getAppClient();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkAlbumDataObject is null!");
            return null;   // ???
        }
    }
    public void setAppClient(String appClient)
    {
        if(getDataObject() != null) {
            getDataObject().setAppClient(appClient);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkAlbumDataObject is null!");
        }
    }

    public String getClientRootDomain()
    {
        if(getDataObject() != null) {
            return getDataObject().getClientRootDomain();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkAlbumDataObject is null!");
            return null;   // ???
        }
    }
    public void setClientRootDomain(String clientRootDomain)
    {
        if(getDataObject() != null) {
            getDataObject().setClientRootDomain(clientRootDomain);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkAlbumDataObject is null!");
        }
    }

    public String getOwner()
    {
        if(getDataObject() != null) {
            return getDataObject().getOwner();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkAlbumDataObject is null!");
            return null;   // ???
        }
    }
    public void setOwner(String owner)
    {
        if(getDataObject() != null) {
            getDataObject().setOwner(owner);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkAlbumDataObject is null!");
        }
    }

    public String getName()
    {
        if(getDataObject() != null) {
            return getDataObject().getName();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkAlbumDataObject is null!");
            return null;   // ???
        }
    }
    public void setName(String name)
    {
        if(getDataObject() != null) {
            getDataObject().setName(name);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkAlbumDataObject is null!");
        }
    }

    public String getSummary()
    {
        if(getDataObject() != null) {
            return getDataObject().getSummary();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkAlbumDataObject is null!");
            return null;   // ???
        }
    }
    public void setSummary(String summary)
    {
        if(getDataObject() != null) {
            getDataObject().setSummary(summary);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkAlbumDataObject is null!");
        }
    }

    public String getTokenPrefix()
    {
        if(getDataObject() != null) {
            return getDataObject().getTokenPrefix();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkAlbumDataObject is null!");
            return null;   // ???
        }
    }
    public void setTokenPrefix(String tokenPrefix)
    {
        if(getDataObject() != null) {
            getDataObject().setTokenPrefix(tokenPrefix);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkAlbumDataObject is null!");
        }
    }

    public String getPermalink()
    {
        if(getDataObject() != null) {
            return getDataObject().getPermalink();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkAlbumDataObject is null!");
            return null;   // ???
        }
    }
    public void setPermalink(String permalink)
    {
        if(getDataObject() != null) {
            getDataObject().setPermalink(permalink);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkAlbumDataObject is null!");
        }
    }

    public String getShortLink()
    {
        if(getDataObject() != null) {
            return getDataObject().getShortLink();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkAlbumDataObject is null!");
            return null;   // ???
        }
    }
    public void setShortLink(String shortLink)
    {
        if(getDataObject() != null) {
            getDataObject().setShortLink(shortLink);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkAlbumDataObject is null!");
        }
    }

    public String getShortUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getShortUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkAlbumDataObject is null!");
            return null;   // ???
        }
    }
    public void setShortUrl(String shortUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setShortUrl(shortUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkAlbumDataObject is null!");
        }
    }

    public String getSource()
    {
        if(getDataObject() != null) {
            return getDataObject().getSource();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkAlbumDataObject is null!");
            return null;   // ???
        }
    }
    public void setSource(String source)
    {
        if(getDataObject() != null) {
            getDataObject().setSource(source);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkAlbumDataObject is null!");
        }
    }

    public String getReferenceUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getReferenceUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkAlbumDataObject is null!");
            return null;   // ???
        }
    }
    public void setReferenceUrl(String referenceUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setReferenceUrl(referenceUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkAlbumDataObject is null!");
        }
    }

    public Boolean isAutoGenerated()
    {
        if(getDataObject() != null) {
            return getDataObject().isAutoGenerated();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkAlbumDataObject is null!");
            return null;   // ???
        }
    }
    public void setAutoGenerated(Boolean autoGenerated)
    {
        if(getDataObject() != null) {
            getDataObject().setAutoGenerated(autoGenerated);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkAlbumDataObject is null!");
        }
    }

    public Integer getMaxLinkCount()
    {
        if(getDataObject() != null) {
            return getDataObject().getMaxLinkCount();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkAlbumDataObject is null!");
            return null;   // ???
        }
    }
    public void setMaxLinkCount(Integer maxLinkCount)
    {
        if(getDataObject() != null) {
            getDataObject().setMaxLinkCount(maxLinkCount);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkAlbumDataObject is null!");
        }
    }

    public String getNote()
    {
        if(getDataObject() != null) {
            return getDataObject().getNote();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkAlbumDataObject is null!");
            return null;   // ???
        }
    }
    public void setNote(String note)
    {
        if(getDataObject() != null) {
            getDataObject().setNote(note);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkAlbumDataObject is null!");
        }
    }

    public String getStatus()
    {
        if(getDataObject() != null) {
            return getDataObject().getStatus();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkAlbumDataObject is null!");
            return null;   // ???
        }
    }
    public void setStatus(String status)
    {
        if(getDataObject() != null) {
            getDataObject().setStatus(status);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkAlbumDataObject is null!");
        }
    }


    // TBD
    public LinkAlbumDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
