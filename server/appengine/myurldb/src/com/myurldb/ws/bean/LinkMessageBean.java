package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.LinkMessage;
import com.myurldb.ws.data.LinkMessageDataObject;

public class LinkMessageBean extends BeanBase implements LinkMessage
{
    private static final Logger log = Logger.getLogger(LinkMessageBean.class.getName());

    // Embedded data object.
    private LinkMessageDataObject dobj = null;

    public LinkMessageBean()
    {
        this(new LinkMessageDataObject());
    }
    public LinkMessageBean(String guid)
    {
        this(new LinkMessageDataObject(guid));
    }
    public LinkMessageBean(LinkMessageDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public LinkMessageDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkMessageDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            if(log.isLoggable(Level.INFO)) log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkMessageDataObject is null!");
        }
    }

    public String getShortLink()
    {
        if(getDataObject() != null) {
            return getDataObject().getShortLink();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkMessageDataObject is null!");
            return null;   // ???
        }
    }
    public void setShortLink(String shortLink)
    {
        if(getDataObject() != null) {
            getDataObject().setShortLink(shortLink);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkMessageDataObject is null!");
        }
    }

    public String getMessage()
    {
        if(getDataObject() != null) {
            return getDataObject().getMessage();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkMessageDataObject is null!");
            return null;   // ???
        }
    }
    public void setMessage(String message)
    {
        if(getDataObject() != null) {
            getDataObject().setMessage(message);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkMessageDataObject is null!");
        }
    }

    public String getPassword()
    {
        if(getDataObject() != null) {
            return getDataObject().getPassword();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkMessageDataObject is null!");
            return null;   // ???
        }
    }
    public void setPassword(String password)
    {
        if(getDataObject() != null) {
            getDataObject().setPassword(password);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkMessageDataObject is null!");
        }
    }


    // TBD
    public LinkMessageDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
