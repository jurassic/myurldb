package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.LinkPassphrase;
import com.myurldb.ws.data.LinkPassphraseDataObject;

public class LinkPassphraseBean extends BeanBase implements LinkPassphrase
{
    private static final Logger log = Logger.getLogger(LinkPassphraseBean.class.getName());

    // Embedded data object.
    private LinkPassphraseDataObject dobj = null;

    public LinkPassphraseBean()
    {
        this(new LinkPassphraseDataObject());
    }
    public LinkPassphraseBean(String guid)
    {
        this(new LinkPassphraseDataObject(guid));
    }
    public LinkPassphraseBean(LinkPassphraseDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public LinkPassphraseDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkPassphraseDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            if(log.isLoggable(Level.INFO)) log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkPassphraseDataObject is null!");
        }
    }

    public String getShortLink()
    {
        if(getDataObject() != null) {
            return getDataObject().getShortLink();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkPassphraseDataObject is null!");
            return null;   // ???
        }
    }
    public void setShortLink(String shortLink)
    {
        if(getDataObject() != null) {
            getDataObject().setShortLink(shortLink);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkPassphraseDataObject is null!");
        }
    }

    public String getPassphrase()
    {
        if(getDataObject() != null) {
            return getDataObject().getPassphrase();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkPassphraseDataObject is null!");
            return null;   // ???
        }
    }
    public void setPassphrase(String passphrase)
    {
        if(getDataObject() != null) {
            getDataObject().setPassphrase(passphrase);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkPassphraseDataObject is null!");
        }
    }


    // TBD
    public LinkPassphraseDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
