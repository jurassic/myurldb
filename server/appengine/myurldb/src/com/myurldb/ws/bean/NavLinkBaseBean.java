package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.NavLinkBase;
import com.myurldb.ws.data.NavLinkBaseDataObject;

public abstract class NavLinkBaseBean extends BeanBase implements NavLinkBase
{
    private static final Logger log = Logger.getLogger(NavLinkBaseBean.class.getName());

    public NavLinkBaseBean()
    {
        super();
    }

    @Override
    public abstract NavLinkBaseDataObject getDataObject();

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded NavLinkBaseDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            if(log.isLoggable(Level.INFO)) log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded NavLinkBaseDataObject is null!");
        }
    }

    public String getAppClient()
    {
        if(getDataObject() != null) {
            return getDataObject().getAppClient();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded NavLinkBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setAppClient(String appClient)
    {
        if(getDataObject() != null) {
            getDataObject().setAppClient(appClient);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded NavLinkBaseDataObject is null!");
        }
    }

    public String getClientRootDomain()
    {
        if(getDataObject() != null) {
            return getDataObject().getClientRootDomain();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded NavLinkBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setClientRootDomain(String clientRootDomain)
    {
        if(getDataObject() != null) {
            getDataObject().setClientRootDomain(clientRootDomain);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded NavLinkBaseDataObject is null!");
        }
    }

    public String getUser()
    {
        if(getDataObject() != null) {
            return getDataObject().getUser();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded NavLinkBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setUser(String user)
    {
        if(getDataObject() != null) {
            getDataObject().setUser(user);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded NavLinkBaseDataObject is null!");
        }
    }

    public String getShortLink()
    {
        if(getDataObject() != null) {
            return getDataObject().getShortLink();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded NavLinkBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setShortLink(String shortLink)
    {
        if(getDataObject() != null) {
            getDataObject().setShortLink(shortLink);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded NavLinkBaseDataObject is null!");
        }
    }

    public String getDomain()
    {
        if(getDataObject() != null) {
            return getDataObject().getDomain();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded NavLinkBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setDomain(String domain)
    {
        if(getDataObject() != null) {
            getDataObject().setDomain(domain);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded NavLinkBaseDataObject is null!");
        }
    }

    public String getToken()
    {
        if(getDataObject() != null) {
            return getDataObject().getToken();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded NavLinkBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setToken(String token)
    {
        if(getDataObject() != null) {
            getDataObject().setToken(token);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded NavLinkBaseDataObject is null!");
        }
    }

    public String getLongUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getLongUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded NavLinkBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setLongUrl(String longUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setLongUrl(longUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded NavLinkBaseDataObject is null!");
        }
    }

    public String getShortUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getShortUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded NavLinkBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setShortUrl(String shortUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setShortUrl(shortUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded NavLinkBaseDataObject is null!");
        }
    }

    public Boolean isInternal()
    {
        if(getDataObject() != null) {
            return getDataObject().isInternal();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded NavLinkBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setInternal(Boolean internal)
    {
        if(getDataObject() != null) {
            getDataObject().setInternal(internal);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded NavLinkBaseDataObject is null!");
        }
    }

    public Boolean isCaching()
    {
        if(getDataObject() != null) {
            return getDataObject().isCaching();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded NavLinkBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setCaching(Boolean caching)
    {
        if(getDataObject() != null) {
            getDataObject().setCaching(caching);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded NavLinkBaseDataObject is null!");
        }
    }

    public String getMemo()
    {
        if(getDataObject() != null) {
            return getDataObject().getMemo();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded NavLinkBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setMemo(String memo)
    {
        if(getDataObject() != null) {
            getDataObject().setMemo(memo);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded NavLinkBaseDataObject is null!");
        }
    }

    public String getStatus()
    {
        if(getDataObject() != null) {
            return getDataObject().getStatus();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded NavLinkBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setStatus(String status)
    {
        if(getDataObject() != null) {
            getDataObject().setStatus(status);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded NavLinkBaseDataObject is null!");
        }
    }

    public String getNote()
    {
        if(getDataObject() != null) {
            return getDataObject().getNote();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded NavLinkBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setNote(String note)
    {
        if(getDataObject() != null) {
            getDataObject().setNote(note);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded NavLinkBaseDataObject is null!");
        }
    }

    public Long getExpirationTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getExpirationTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded NavLinkBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setExpirationTime(Long expirationTime)
    {
        if(getDataObject() != null) {
            getDataObject().setExpirationTime(expirationTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded NavLinkBaseDataObject is null!");
        }
    }



    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
