package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.QrCode;
import com.myurldb.ws.data.QrCodeDataObject;

public class QrCodeBean extends BeanBase implements QrCode
{
    private static final Logger log = Logger.getLogger(QrCodeBean.class.getName());

    // Embedded data object.
    private QrCodeDataObject dobj = null;

    public QrCodeBean()
    {
        this(new QrCodeDataObject());
    }
    public QrCodeBean(String guid)
    {
        this(new QrCodeDataObject(guid));
    }
    public QrCodeBean(QrCodeDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public QrCodeDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QrCodeDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            if(log.isLoggable(Level.INFO)) log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QrCodeDataObject is null!");
        }
    }

    public String getShortLink()
    {
        if(getDataObject() != null) {
            return getDataObject().getShortLink();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QrCodeDataObject is null!");
            return null;   // ???
        }
    }
    public void setShortLink(String shortLink)
    {
        if(getDataObject() != null) {
            getDataObject().setShortLink(shortLink);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QrCodeDataObject is null!");
        }
    }

    public String getImageLink()
    {
        if(getDataObject() != null) {
            return getDataObject().getImageLink();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QrCodeDataObject is null!");
            return null;   // ???
        }
    }
    public void setImageLink(String imageLink)
    {
        if(getDataObject() != null) {
            getDataObject().setImageLink(imageLink);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QrCodeDataObject is null!");
        }
    }

    public String getImageUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getImageUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QrCodeDataObject is null!");
            return null;   // ???
        }
    }
    public void setImageUrl(String imageUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setImageUrl(imageUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QrCodeDataObject is null!");
        }
    }

    public String getType()
    {
        if(getDataObject() != null) {
            return getDataObject().getType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QrCodeDataObject is null!");
            return null;   // ???
        }
    }
    public void setType(String type)
    {
        if(getDataObject() != null) {
            getDataObject().setType(type);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QrCodeDataObject is null!");
        }
    }

    public String getStatus()
    {
        if(getDataObject() != null) {
            return getDataObject().getStatus();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QrCodeDataObject is null!");
            return null;   // ???
        }
    }
    public void setStatus(String status)
    {
        if(getDataObject() != null) {
            getDataObject().setStatus(status);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded QrCodeDataObject is null!");
        }
    }


    // TBD
    public QrCodeDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
