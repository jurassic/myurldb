package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.RolePermission;
import com.myurldb.ws.data.RolePermissionDataObject;

public class RolePermissionBean extends BeanBase implements RolePermission
{
    private static final Logger log = Logger.getLogger(RolePermissionBean.class.getName());

    // Embedded data object.
    private RolePermissionDataObject dobj = null;

    public RolePermissionBean()
    {
        this(new RolePermissionDataObject());
    }
    public RolePermissionBean(String guid)
    {
        this(new RolePermissionDataObject(guid));
    }
    public RolePermissionBean(RolePermissionDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public RolePermissionDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RolePermissionDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            if(log.isLoggable(Level.INFO)) log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RolePermissionDataObject is null!");
        }
    }

    public String getRole()
    {
        if(getDataObject() != null) {
            return getDataObject().getRole();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RolePermissionDataObject is null!");
            return null;   // ???
        }
    }
    public void setRole(String role)
    {
        if(getDataObject() != null) {
            getDataObject().setRole(role);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RolePermissionDataObject is null!");
        }
    }

    public String getPermissionName()
    {
        if(getDataObject() != null) {
            return getDataObject().getPermissionName();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RolePermissionDataObject is null!");
            return null;   // ???
        }
    }
    public void setPermissionName(String permissionName)
    {
        if(getDataObject() != null) {
            getDataObject().setPermissionName(permissionName);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RolePermissionDataObject is null!");
        }
    }

    public String getResource()
    {
        if(getDataObject() != null) {
            return getDataObject().getResource();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RolePermissionDataObject is null!");
            return null;   // ???
        }
    }
    public void setResource(String resource)
    {
        if(getDataObject() != null) {
            getDataObject().setResource(resource);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RolePermissionDataObject is null!");
        }
    }

    public String getInstance()
    {
        if(getDataObject() != null) {
            return getDataObject().getInstance();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RolePermissionDataObject is null!");
            return null;   // ???
        }
    }
    public void setInstance(String instance)
    {
        if(getDataObject() != null) {
            getDataObject().setInstance(instance);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RolePermissionDataObject is null!");
        }
    }

    public String getAction()
    {
        if(getDataObject() != null) {
            return getDataObject().getAction();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RolePermissionDataObject is null!");
            return null;   // ???
        }
    }
    public void setAction(String action)
    {
        if(getDataObject() != null) {
            getDataObject().setAction(action);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RolePermissionDataObject is null!");
        }
    }

    public String getStatus()
    {
        if(getDataObject() != null) {
            return getDataObject().getStatus();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RolePermissionDataObject is null!");
            return null;   // ???
        }
    }
    public void setStatus(String status)
    {
        if(getDataObject() != null) {
            getDataObject().setStatus(status);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RolePermissionDataObject is null!");
        }
    }


    // TBD
    public RolePermissionDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
