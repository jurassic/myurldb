package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.ReferrerInfoStruct;
import com.myurldb.ws.ShortLink;
import com.myurldb.ws.data.ReferrerInfoStructDataObject;
import com.myurldb.ws.data.ShortLinkDataObject;

public class ShortLinkBean extends BeanBase implements ShortLink
{
    private static final Logger log = Logger.getLogger(ShortLinkBean.class.getName());

    // Embedded data object.
    private ShortLinkDataObject dobj = null;

    public ShortLinkBean()
    {
        this(new ShortLinkDataObject());
    }
    public ShortLinkBean(String guid)
    {
        this(new ShortLinkDataObject(guid));
    }
    public ShortLinkBean(ShortLinkDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public ShortLinkDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            if(log.isLoggable(Level.INFO)) log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
        }
    }

    public String getAppClient()
    {
        if(getDataObject() != null) {
            return getDataObject().getAppClient();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
            return null;   // ???
        }
    }
    public void setAppClient(String appClient)
    {
        if(getDataObject() != null) {
            getDataObject().setAppClient(appClient);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
        }
    }

    public String getClientRootDomain()
    {
        if(getDataObject() != null) {
            return getDataObject().getClientRootDomain();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
            return null;   // ???
        }
    }
    public void setClientRootDomain(String clientRootDomain)
    {
        if(getDataObject() != null) {
            getDataObject().setClientRootDomain(clientRootDomain);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
        }
    }

    public String getOwner()
    {
        if(getDataObject() != null) {
            return getDataObject().getOwner();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
            return null;   // ???
        }
    }
    public void setOwner(String owner)
    {
        if(getDataObject() != null) {
            getDataObject().setOwner(owner);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
        }
    }

    public String getLongUrlDomain()
    {
        if(getDataObject() != null) {
            return getDataObject().getLongUrlDomain();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
            return null;   // ???
        }
    }
    public void setLongUrlDomain(String longUrlDomain)
    {
        if(getDataObject() != null) {
            getDataObject().setLongUrlDomain(longUrlDomain);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
        }
    }

    public String getLongUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getLongUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
            return null;   // ???
        }
    }
    public void setLongUrl(String longUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setLongUrl(longUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
        }
    }

    public String getLongUrlFull()
    {
        if(getDataObject() != null) {
            return getDataObject().getLongUrlFull();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
            return null;   // ???
        }
    }
    public void setLongUrlFull(String longUrlFull)
    {
        if(getDataObject() != null) {
            getDataObject().setLongUrlFull(longUrlFull);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
        }
    }

    public String getLongUrlHash()
    {
        if(getDataObject() != null) {
            return getDataObject().getLongUrlHash();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
            return null;   // ???
        }
    }
    public void setLongUrlHash(String longUrlHash)
    {
        if(getDataObject() != null) {
            getDataObject().setLongUrlHash(longUrlHash);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
        }
    }

    public Boolean isReuseExistingShortUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().isReuseExistingShortUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
            return null;   // ???
        }
    }
    public void setReuseExistingShortUrl(Boolean reuseExistingShortUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setReuseExistingShortUrl(reuseExistingShortUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
        }
    }

    public Boolean isFailIfShortUrlExists()
    {
        if(getDataObject() != null) {
            return getDataObject().isFailIfShortUrlExists();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
            return null;   // ???
        }
    }
    public void setFailIfShortUrlExists(Boolean failIfShortUrlExists)
    {
        if(getDataObject() != null) {
            getDataObject().setFailIfShortUrlExists(failIfShortUrlExists);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
        }
    }

    public String getDomain()
    {
        if(getDataObject() != null) {
            return getDataObject().getDomain();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
            return null;   // ???
        }
    }
    public void setDomain(String domain)
    {
        if(getDataObject() != null) {
            getDataObject().setDomain(domain);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
        }
    }

    public String getDomainType()
    {
        if(getDataObject() != null) {
            return getDataObject().getDomainType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
            return null;   // ???
        }
    }
    public void setDomainType(String domainType)
    {
        if(getDataObject() != null) {
            getDataObject().setDomainType(domainType);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
        }
    }

    public String getUsercode()
    {
        if(getDataObject() != null) {
            return getDataObject().getUsercode();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
            return null;   // ???
        }
    }
    public void setUsercode(String usercode)
    {
        if(getDataObject() != null) {
            getDataObject().setUsercode(usercode);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
        }
    }

    public String getUsername()
    {
        if(getDataObject() != null) {
            return getDataObject().getUsername();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
            return null;   // ???
        }
    }
    public void setUsername(String username)
    {
        if(getDataObject() != null) {
            getDataObject().setUsername(username);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
        }
    }

    public String getTokenPrefix()
    {
        if(getDataObject() != null) {
            return getDataObject().getTokenPrefix();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
            return null;   // ???
        }
    }
    public void setTokenPrefix(String tokenPrefix)
    {
        if(getDataObject() != null) {
            getDataObject().setTokenPrefix(tokenPrefix);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
        }
    }

    public String getToken()
    {
        if(getDataObject() != null) {
            return getDataObject().getToken();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
            return null;   // ???
        }
    }
    public void setToken(String token)
    {
        if(getDataObject() != null) {
            getDataObject().setToken(token);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
        }
    }

    public String getTokenType()
    {
        if(getDataObject() != null) {
            return getDataObject().getTokenType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
            return null;   // ???
        }
    }
    public void setTokenType(String tokenType)
    {
        if(getDataObject() != null) {
            getDataObject().setTokenType(tokenType);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
        }
    }

    public String getSassyTokenType()
    {
        if(getDataObject() != null) {
            return getDataObject().getSassyTokenType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
            return null;   // ???
        }
    }
    public void setSassyTokenType(String sassyTokenType)
    {
        if(getDataObject() != null) {
            getDataObject().setSassyTokenType(sassyTokenType);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
        }
    }

    public String getShortUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getShortUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
            return null;   // ???
        }
    }
    public void setShortUrl(String shortUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setShortUrl(shortUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
        }
    }

    public String getShortPassage()
    {
        if(getDataObject() != null) {
            return getDataObject().getShortPassage();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
            return null;   // ???
        }
    }
    public void setShortPassage(String shortPassage)
    {
        if(getDataObject() != null) {
            getDataObject().setShortPassage(shortPassage);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
        }
    }

    public String getRedirectType()
    {
        if(getDataObject() != null) {
            return getDataObject().getRedirectType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
            return null;   // ???
        }
    }
    public void setRedirectType(String redirectType)
    {
        if(getDataObject() != null) {
            getDataObject().setRedirectType(redirectType);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
        }
    }

    public Long getFlashDuration()
    {
        if(getDataObject() != null) {
            return getDataObject().getFlashDuration();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
            return null;   // ???
        }
    }
    public void setFlashDuration(Long flashDuration)
    {
        if(getDataObject() != null) {
            getDataObject().setFlashDuration(flashDuration);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
        }
    }

    public String getAccessType()
    {
        if(getDataObject() != null) {
            return getDataObject().getAccessType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
            return null;   // ???
        }
    }
    public void setAccessType(String accessType)
    {
        if(getDataObject() != null) {
            getDataObject().setAccessType(accessType);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
        }
    }

    public String getViewType()
    {
        if(getDataObject() != null) {
            return getDataObject().getViewType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
            return null;   // ???
        }
    }
    public void setViewType(String viewType)
    {
        if(getDataObject() != null) {
            getDataObject().setViewType(viewType);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
        }
    }

    public String getShareType()
    {
        if(getDataObject() != null) {
            return getDataObject().getShareType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
            return null;   // ???
        }
    }
    public void setShareType(String shareType)
    {
        if(getDataObject() != null) {
            getDataObject().setShareType(shareType);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
        }
    }

    public Boolean isReadOnly()
    {
        if(getDataObject() != null) {
            return getDataObject().isReadOnly();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
            return null;   // ???
        }
    }
    public void setReadOnly(Boolean readOnly)
    {
        if(getDataObject() != null) {
            getDataObject().setReadOnly(readOnly);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
        }
    }

    public String getDisplayMessage()
    {
        if(getDataObject() != null) {
            return getDataObject().getDisplayMessage();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
            return null;   // ???
        }
    }
    public void setDisplayMessage(String displayMessage)
    {
        if(getDataObject() != null) {
            getDataObject().setDisplayMessage(displayMessage);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
        }
    }

    public String getShortMessage()
    {
        if(getDataObject() != null) {
            return getDataObject().getShortMessage();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
            return null;   // ???
        }
    }
    public void setShortMessage(String shortMessage)
    {
        if(getDataObject() != null) {
            getDataObject().setShortMessage(shortMessage);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
        }
    }

    public Boolean isKeywordEnabled()
    {
        if(getDataObject() != null) {
            return getDataObject().isKeywordEnabled();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
            return null;   // ???
        }
    }
    public void setKeywordEnabled(Boolean keywordEnabled)
    {
        if(getDataObject() != null) {
            getDataObject().setKeywordEnabled(keywordEnabled);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
        }
    }

    public Boolean isBookmarkEnabled()
    {
        if(getDataObject() != null) {
            return getDataObject().isBookmarkEnabled();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
            return null;   // ???
        }
    }
    public void setBookmarkEnabled(Boolean bookmarkEnabled)
    {
        if(getDataObject() != null) {
            getDataObject().setBookmarkEnabled(bookmarkEnabled);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
        }
    }

    public ReferrerInfoStruct getReferrerInfo()
    {
        if(getDataObject() != null) {
            ReferrerInfoStruct _field = getDataObject().getReferrerInfo();
            if(_field == null) {
                log.log(Level.INFO, "referrerInfo is null.");
                return null;
            } else {
                return new ReferrerInfoStructBean((ReferrerInfoStructDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
            return null;   // ???
        }
    }
    public void setReferrerInfo(ReferrerInfoStruct referrerInfo)
    {
        if(getDataObject() != null) {
            getDataObject().setReferrerInfo(
                (referrerInfo instanceof ReferrerInfoStructBean) ?
                ((ReferrerInfoStructBean) referrerInfo).toDataObject() :
                ((referrerInfo instanceof ReferrerInfoStructDataObject) ? referrerInfo : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
        }
    }

    public String getStatus()
    {
        if(getDataObject() != null) {
            return getDataObject().getStatus();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
            return null;   // ???
        }
    }
    public void setStatus(String status)
    {
        if(getDataObject() != null) {
            getDataObject().setStatus(status);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
        }
    }

    public String getNote()
    {
        if(getDataObject() != null) {
            return getDataObject().getNote();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
            return null;   // ???
        }
    }
    public void setNote(String note)
    {
        if(getDataObject() != null) {
            getDataObject().setNote(note);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
        }
    }

    public Long getExpirationTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getExpirationTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
            return null;   // ???
        }
    }
    public void setExpirationTime(Long expirationTime)
    {
        if(getDataObject() != null) {
            getDataObject().setExpirationTime(expirationTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortLinkDataObject is null!");
        }
    }


    // TBD
    public ShortLinkDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
