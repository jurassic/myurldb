package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.ShortPassageAttribute;
import com.myurldb.ws.data.ShortPassageAttributeDataObject;

public class ShortPassageAttributeBean implements ShortPassageAttribute
{
    private static final Logger log = Logger.getLogger(ShortPassageAttributeBean.class.getName());

    // Embedded data object.
    private ShortPassageAttributeDataObject dobj = null;

    public ShortPassageAttributeBean()
    {
        this(new ShortPassageAttributeDataObject());
    }
    public ShortPassageAttributeBean(ShortPassageAttributeDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    public ShortPassageAttributeDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getDomain()
    {
        if(getDataObject() != null) {
            return getDataObject().getDomain();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortPassageAttributeDataObject is null!");
            return null;   // ???
        }
    }
    public void setDomain(String domain)
    {
        if(getDataObject() != null) {
            getDataObject().setDomain(domain);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortPassageAttributeDataObject is null!");
        }
    }

    public String getTokenType()
    {
        if(getDataObject() != null) {
            return getDataObject().getTokenType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortPassageAttributeDataObject is null!");
            return null;   // ???
        }
    }
    public void setTokenType(String tokenType)
    {
        if(getDataObject() != null) {
            getDataObject().setTokenType(tokenType);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortPassageAttributeDataObject is null!");
        }
    }

    public String getDisplayMessage()
    {
        if(getDataObject() != null) {
            return getDataObject().getDisplayMessage();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortPassageAttributeDataObject is null!");
            return null;   // ???
        }
    }
    public void setDisplayMessage(String displayMessage)
    {
        if(getDataObject() != null) {
            getDataObject().setDisplayMessage(displayMessage);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortPassageAttributeDataObject is null!");
        }
    }

    public String getRedirectType()
    {
        if(getDataObject() != null) {
            return getDataObject().getRedirectType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortPassageAttributeDataObject is null!");
            return null;   // ???
        }
    }
    public void setRedirectType(String redirectType)
    {
        if(getDataObject() != null) {
            getDataObject().setRedirectType(redirectType);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortPassageAttributeDataObject is null!");
        }
    }

    public Long getFlashDuration()
    {
        if(getDataObject() != null) {
            return getDataObject().getFlashDuration();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortPassageAttributeDataObject is null!");
            return null;   // ???
        }
    }
    public void setFlashDuration(Long flashDuration)
    {
        if(getDataObject() != null) {
            getDataObject().setFlashDuration(flashDuration);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortPassageAttributeDataObject is null!");
        }
    }

    public String getAccessType()
    {
        if(getDataObject() != null) {
            return getDataObject().getAccessType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortPassageAttributeDataObject is null!");
            return null;   // ???
        }
    }
    public void setAccessType(String accessType)
    {
        if(getDataObject() != null) {
            getDataObject().setAccessType(accessType);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortPassageAttributeDataObject is null!");
        }
    }

    public String getViewType()
    {
        if(getDataObject() != null) {
            return getDataObject().getViewType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortPassageAttributeDataObject is null!");
            return null;   // ???
        }
    }
    public void setViewType(String viewType)
    {
        if(getDataObject() != null) {
            getDataObject().setViewType(viewType);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortPassageAttributeDataObject is null!");
        }
    }

    public String getShareType()
    {
        if(getDataObject() != null) {
            return getDataObject().getShareType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortPassageAttributeDataObject is null!");
            return null;   // ???
        }
    }
    public void setShareType(String shareType)
    {
        if(getDataObject() != null) {
            getDataObject().setShareType(shareType);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortPassageAttributeDataObject is null!");
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        if(getDataObject() != null) {
            return getDataObject().isEmpty();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortPassageAttributeDataObject is null!");
            boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getDomain() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getTokenType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getDisplayMessage() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getRedirectType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getFlashDuration() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAccessType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getViewType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getShareType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
            return (atLeastOneFieldNonEmpty == false);
        }
    }

    // TBD
    public ShortPassageAttributeDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
