package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.ShortPassageAttribute;
import com.myurldb.ws.ShortPassage;
import com.myurldb.ws.data.ShortPassageAttributeDataObject;
import com.myurldb.ws.data.ShortPassageDataObject;

public class ShortPassageBean extends BeanBase implements ShortPassage
{
    private static final Logger log = Logger.getLogger(ShortPassageBean.class.getName());

    // Embedded data object.
    private ShortPassageDataObject dobj = null;

    public ShortPassageBean()
    {
        this(new ShortPassageDataObject());
    }
    public ShortPassageBean(String guid)
    {
        this(new ShortPassageDataObject(guid));
    }
    public ShortPassageBean(ShortPassageDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public ShortPassageDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortPassageDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            if(log.isLoggable(Level.INFO)) log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortPassageDataObject is null!");
        }
    }

    public String getOwner()
    {
        if(getDataObject() != null) {
            return getDataObject().getOwner();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortPassageDataObject is null!");
            return null;   // ???
        }
    }
    public void setOwner(String owner)
    {
        if(getDataObject() != null) {
            getDataObject().setOwner(owner);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortPassageDataObject is null!");
        }
    }

    public String getLongText()
    {
        if(getDataObject() != null) {
            return getDataObject().getLongText();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortPassageDataObject is null!");
            return null;   // ???
        }
    }
    public void setLongText(String longText)
    {
        if(getDataObject() != null) {
            getDataObject().setLongText(longText);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortPassageDataObject is null!");
        }
    }

    public String getShortText()
    {
        if(getDataObject() != null) {
            return getDataObject().getShortText();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortPassageDataObject is null!");
            return null;   // ???
        }
    }
    public void setShortText(String shortText)
    {
        if(getDataObject() != null) {
            getDataObject().setShortText(shortText);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortPassageDataObject is null!");
        }
    }

    public ShortPassageAttribute getAttribute()
    {
        if(getDataObject() != null) {
            ShortPassageAttribute _field = getDataObject().getAttribute();
            if(_field == null) {
                log.log(Level.INFO, "attribute is null.");
                return null;
            } else {
                return new ShortPassageAttributeBean((ShortPassageAttributeDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortPassageDataObject is null!");
            return null;   // ???
        }
    }
    public void setAttribute(ShortPassageAttribute attribute)
    {
        if(getDataObject() != null) {
            getDataObject().setAttribute(
                (attribute instanceof ShortPassageAttributeBean) ?
                ((ShortPassageAttributeBean) attribute).toDataObject() :
                ((attribute instanceof ShortPassageAttributeDataObject) ? attribute : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortPassageDataObject is null!");
        }
    }

    public Boolean isReadOnly()
    {
        if(getDataObject() != null) {
            return getDataObject().isReadOnly();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortPassageDataObject is null!");
            return null;   // ???
        }
    }
    public void setReadOnly(Boolean readOnly)
    {
        if(getDataObject() != null) {
            getDataObject().setReadOnly(readOnly);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortPassageDataObject is null!");
        }
    }

    public String getStatus()
    {
        if(getDataObject() != null) {
            return getDataObject().getStatus();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortPassageDataObject is null!");
            return null;   // ???
        }
    }
    public void setStatus(String status)
    {
        if(getDataObject() != null) {
            getDataObject().setStatus(status);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortPassageDataObject is null!");
        }
    }

    public String getNote()
    {
        if(getDataObject() != null) {
            return getDataObject().getNote();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortPassageDataObject is null!");
            return null;   // ???
        }
    }
    public void setNote(String note)
    {
        if(getDataObject() != null) {
            getDataObject().setNote(note);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortPassageDataObject is null!");
        }
    }

    public Long getExpirationTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getExpirationTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortPassageDataObject is null!");
            return null;   // ???
        }
    }
    public void setExpirationTime(Long expirationTime)
    {
        if(getDataObject() != null) {
            getDataObject().setExpirationTime(expirationTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortPassageDataObject is null!");
        }
    }


    // TBD
    public ShortPassageDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
