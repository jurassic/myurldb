package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.SiteCustomDomain;
import com.myurldb.ws.data.SiteCustomDomainDataObject;

public class SiteCustomDomainBean extends BeanBase implements SiteCustomDomain
{
    private static final Logger log = Logger.getLogger(SiteCustomDomainBean.class.getName());

    // Embedded data object.
    private SiteCustomDomainDataObject dobj = null;

    public SiteCustomDomainBean()
    {
        this(new SiteCustomDomainDataObject());
    }
    public SiteCustomDomainBean(String guid)
    {
        this(new SiteCustomDomainDataObject(guid));
    }
    public SiteCustomDomainBean(SiteCustomDomainDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public SiteCustomDomainDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded SiteCustomDomainDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            if(log.isLoggable(Level.INFO)) log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded SiteCustomDomainDataObject is null!");
        }
    }

    public String getSiteDomain()
    {
        if(getDataObject() != null) {
            return getDataObject().getSiteDomain();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded SiteCustomDomainDataObject is null!");
            return null;   // ???
        }
    }
    public void setSiteDomain(String siteDomain)
    {
        if(getDataObject() != null) {
            getDataObject().setSiteDomain(siteDomain);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded SiteCustomDomainDataObject is null!");
        }
    }

    public String getDomain()
    {
        if(getDataObject() != null) {
            return getDataObject().getDomain();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded SiteCustomDomainDataObject is null!");
            return null;   // ???
        }
    }
    public void setDomain(String domain)
    {
        if(getDataObject() != null) {
            getDataObject().setDomain(domain);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded SiteCustomDomainDataObject is null!");
        }
    }

    public String getStatus()
    {
        if(getDataObject() != null) {
            return getDataObject().getStatus();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded SiteCustomDomainDataObject is null!");
            return null;   // ???
        }
    }
    public void setStatus(String status)
    {
        if(getDataObject() != null) {
            getDataObject().setStatus(status);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded SiteCustomDomainDataObject is null!");
        }
    }


    // TBD
    public SiteCustomDomainDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
