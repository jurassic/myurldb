package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.SiteLog;
import com.myurldb.ws.data.SiteLogDataObject;

public class SiteLogBean implements SiteLog
{
    private static final Logger log = Logger.getLogger(SiteLogBean.class.getName());

    // Embedded data object.
    private SiteLogDataObject dobj = null;

    public SiteLogBean()
    {
        this(new SiteLogDataObject());
    }
    public SiteLogBean(SiteLogDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    public SiteLogDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getUuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getUuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded SiteLogDataObject is null!");
            return null;   // ???
        }
    }
    public void setUuid(String uuid)
    {
        if(getDataObject() != null) {
            getDataObject().setUuid(uuid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded SiteLogDataObject is null!");
        }
    }

    public String getTitle()
    {
        if(getDataObject() != null) {
            return getDataObject().getTitle();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded SiteLogDataObject is null!");
            return null;   // ???
        }
    }
    public void setTitle(String title)
    {
        if(getDataObject() != null) {
            getDataObject().setTitle(title);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded SiteLogDataObject is null!");
        }
    }

    public String getPubDate()
    {
        if(getDataObject() != null) {
            return getDataObject().getPubDate();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded SiteLogDataObject is null!");
            return null;   // ???
        }
    }
    public void setPubDate(String pubDate)
    {
        if(getDataObject() != null) {
            getDataObject().setPubDate(pubDate);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded SiteLogDataObject is null!");
        }
    }

    public String getTag()
    {
        if(getDataObject() != null) {
            return getDataObject().getTag();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded SiteLogDataObject is null!");
            return null;   // ???
        }
    }
    public void setTag(String tag)
    {
        if(getDataObject() != null) {
            getDataObject().setTag(tag);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded SiteLogDataObject is null!");
        }
    }

    public String getContent()
    {
        if(getDataObject() != null) {
            return getDataObject().getContent();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded SiteLogDataObject is null!");
            return null;   // ???
        }
    }
    public void setContent(String content)
    {
        if(getDataObject() != null) {
            getDataObject().setContent(content);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded SiteLogDataObject is null!");
        }
    }

    public String getFormat()
    {
        if(getDataObject() != null) {
            return getDataObject().getFormat();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded SiteLogDataObject is null!");
            return null;   // ???
        }
    }
    public void setFormat(String format)
    {
        if(getDataObject() != null) {
            getDataObject().setFormat(format);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded SiteLogDataObject is null!");
        }
    }

    public String getNote()
    {
        if(getDataObject() != null) {
            return getDataObject().getNote();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded SiteLogDataObject is null!");
            return null;   // ???
        }
    }
    public void setNote(String note)
    {
        if(getDataObject() != null) {
            getDataObject().setNote(note);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded SiteLogDataObject is null!");
        }
    }

    public String getStatus()
    {
        if(getDataObject() != null) {
            return getDataObject().getStatus();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded SiteLogDataObject is null!");
            return null;   // ???
        }
    }
    public void setStatus(String status)
    {
        if(getDataObject() != null) {
            getDataObject().setStatus(status);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded SiteLogDataObject is null!");
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        if(getDataObject() != null) {
            return getDataObject().isEmpty();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded SiteLogDataObject is null!");
            boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getTitle() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPubDate() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getTag() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getContent() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getFormat() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getStatus() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
            return (atLeastOneFieldNonEmpty == false);
        }
    }

    // TBD
    public SiteLogDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
