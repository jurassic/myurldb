package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.SpeedDial;
import com.myurldb.ws.data.SpeedDialDataObject;

public class SpeedDialBean extends BeanBase implements SpeedDial
{
    private static final Logger log = Logger.getLogger(SpeedDialBean.class.getName());

    // Embedded data object.
    private SpeedDialDataObject dobj = null;

    public SpeedDialBean()
    {
        this(new SpeedDialDataObject());
    }
    public SpeedDialBean(String guid)
    {
        this(new SpeedDialDataObject(guid));
    }
    public SpeedDialBean(SpeedDialDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public SpeedDialDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded SpeedDialDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            if(log.isLoggable(Level.INFO)) log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded SpeedDialDataObject is null!");
        }
    }

    public String getUser()
    {
        if(getDataObject() != null) {
            return getDataObject().getUser();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded SpeedDialDataObject is null!");
            return null;   // ???
        }
    }
    public void setUser(String user)
    {
        if(getDataObject() != null) {
            getDataObject().setUser(user);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded SpeedDialDataObject is null!");
        }
    }

    public String getCode()
    {
        if(getDataObject() != null) {
            return getDataObject().getCode();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded SpeedDialDataObject is null!");
            return null;   // ???
        }
    }
    public void setCode(String code)
    {
        if(getDataObject() != null) {
            getDataObject().setCode(code);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded SpeedDialDataObject is null!");
        }
    }

    public String getShortcut()
    {
        if(getDataObject() != null) {
            return getDataObject().getShortcut();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded SpeedDialDataObject is null!");
            return null;   // ???
        }
    }
    public void setShortcut(String shortcut)
    {
        if(getDataObject() != null) {
            getDataObject().setShortcut(shortcut);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded SpeedDialDataObject is null!");
        }
    }

    public String getShortLink()
    {
        if(getDataObject() != null) {
            return getDataObject().getShortLink();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded SpeedDialDataObject is null!");
            return null;   // ???
        }
    }
    public void setShortLink(String shortLink)
    {
        if(getDataObject() != null) {
            getDataObject().setShortLink(shortLink);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded SpeedDialDataObject is null!");
        }
    }

    public String getKeywordLink()
    {
        if(getDataObject() != null) {
            return getDataObject().getKeywordLink();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded SpeedDialDataObject is null!");
            return null;   // ???
        }
    }
    public void setKeywordLink(String keywordLink)
    {
        if(getDataObject() != null) {
            getDataObject().setKeywordLink(keywordLink);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded SpeedDialDataObject is null!");
        }
    }

    public String getBookmarkLink()
    {
        if(getDataObject() != null) {
            return getDataObject().getBookmarkLink();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded SpeedDialDataObject is null!");
            return null;   // ???
        }
    }
    public void setBookmarkLink(String bookmarkLink)
    {
        if(getDataObject() != null) {
            getDataObject().setBookmarkLink(bookmarkLink);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded SpeedDialDataObject is null!");
        }
    }

    public String getLongUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getLongUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded SpeedDialDataObject is null!");
            return null;   // ???
        }
    }
    public void setLongUrl(String longUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setLongUrl(longUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded SpeedDialDataObject is null!");
        }
    }

    public String getShortUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getShortUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded SpeedDialDataObject is null!");
            return null;   // ???
        }
    }
    public void setShortUrl(String shortUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setShortUrl(shortUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded SpeedDialDataObject is null!");
        }
    }

    public Boolean isCaseSensitive()
    {
        if(getDataObject() != null) {
            return getDataObject().isCaseSensitive();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded SpeedDialDataObject is null!");
            return null;   // ???
        }
    }
    public void setCaseSensitive(Boolean caseSensitive)
    {
        if(getDataObject() != null) {
            getDataObject().setCaseSensitive(caseSensitive);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded SpeedDialDataObject is null!");
        }
    }

    public String getStatus()
    {
        if(getDataObject() != null) {
            return getDataObject().getStatus();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded SpeedDialDataObject is null!");
            return null;   // ???
        }
    }
    public void setStatus(String status)
    {
        if(getDataObject() != null) {
            getDataObject().setStatus(status);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded SpeedDialDataObject is null!");
        }
    }

    public String getNote()
    {
        if(getDataObject() != null) {
            return getDataObject().getNote();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded SpeedDialDataObject is null!");
            return null;   // ???
        }
    }
    public void setNote(String note)
    {
        if(getDataObject() != null) {
            getDataObject().setNote(note);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded SpeedDialDataObject is null!");
        }
    }


    // TBD
    public SpeedDialDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
