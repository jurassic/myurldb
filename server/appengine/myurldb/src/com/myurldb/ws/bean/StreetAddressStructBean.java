package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.StreetAddressStruct;
import com.myurldb.ws.data.StreetAddressStructDataObject;

public class StreetAddressStructBean implements StreetAddressStruct
{
    private static final Logger log = Logger.getLogger(StreetAddressStructBean.class.getName());

    // Embedded data object.
    private StreetAddressStructDataObject dobj = null;

    public StreetAddressStructBean()
    {
        this(new StreetAddressStructDataObject());
    }
    public StreetAddressStructBean(StreetAddressStructDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    public StreetAddressStructDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getUuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getUuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded StreetAddressStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setUuid(String uuid)
    {
        if(getDataObject() != null) {
            getDataObject().setUuid(uuid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded StreetAddressStructDataObject is null!");
        }
    }

    public String getStreet1()
    {
        if(getDataObject() != null) {
            return getDataObject().getStreet1();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded StreetAddressStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setStreet1(String street1)
    {
        if(getDataObject() != null) {
            getDataObject().setStreet1(street1);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded StreetAddressStructDataObject is null!");
        }
    }

    public String getStreet2()
    {
        if(getDataObject() != null) {
            return getDataObject().getStreet2();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded StreetAddressStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setStreet2(String street2)
    {
        if(getDataObject() != null) {
            getDataObject().setStreet2(street2);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded StreetAddressStructDataObject is null!");
        }
    }

    public String getCity()
    {
        if(getDataObject() != null) {
            return getDataObject().getCity();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded StreetAddressStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setCity(String city)
    {
        if(getDataObject() != null) {
            getDataObject().setCity(city);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded StreetAddressStructDataObject is null!");
        }
    }

    public String getCounty()
    {
        if(getDataObject() != null) {
            return getDataObject().getCounty();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded StreetAddressStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setCounty(String county)
    {
        if(getDataObject() != null) {
            getDataObject().setCounty(county);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded StreetAddressStructDataObject is null!");
        }
    }

    public String getPostalCode()
    {
        if(getDataObject() != null) {
            return getDataObject().getPostalCode();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded StreetAddressStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setPostalCode(String postalCode)
    {
        if(getDataObject() != null) {
            getDataObject().setPostalCode(postalCode);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded StreetAddressStructDataObject is null!");
        }
    }

    public String getState()
    {
        if(getDataObject() != null) {
            return getDataObject().getState();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded StreetAddressStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setState(String state)
    {
        if(getDataObject() != null) {
            getDataObject().setState(state);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded StreetAddressStructDataObject is null!");
        }
    }

    public String getProvince()
    {
        if(getDataObject() != null) {
            return getDataObject().getProvince();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded StreetAddressStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setProvince(String province)
    {
        if(getDataObject() != null) {
            getDataObject().setProvince(province);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded StreetAddressStructDataObject is null!");
        }
    }

    public String getCountry()
    {
        if(getDataObject() != null) {
            return getDataObject().getCountry();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded StreetAddressStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setCountry(String country)
    {
        if(getDataObject() != null) {
            getDataObject().setCountry(country);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded StreetAddressStructDataObject is null!");
        }
    }

    public String getCountryName()
    {
        if(getDataObject() != null) {
            return getDataObject().getCountryName();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded StreetAddressStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setCountryName(String countryName)
    {
        if(getDataObject() != null) {
            getDataObject().setCountryName(countryName);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded StreetAddressStructDataObject is null!");
        }
    }

    public String getNote()
    {
        if(getDataObject() != null) {
            return getDataObject().getNote();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded StreetAddressStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setNote(String note)
    {
        if(getDataObject() != null) {
            getDataObject().setNote(note);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded StreetAddressStructDataObject is null!");
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        if(getDataObject() != null) {
            return getDataObject().isEmpty();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded StreetAddressStructDataObject is null!");
            boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getStreet1() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getStreet2() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getCity() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getCounty() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPostalCode() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getState() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getProvince() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getCountry() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getCountryName() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
            return (atLeastOneFieldNonEmpty == false);
        }
    }

    // TBD
    public StreetAddressStructDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
