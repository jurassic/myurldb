package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.TwitterAppCard;
import com.myurldb.ws.data.TwitterCardAppInfoDataObject;
import com.myurldb.ws.data.TwitterCardProductDataDataObject;
import com.myurldb.ws.data.TwitterAppCardDataObject;

public class TwitterAppCardBean extends TwitterCardBaseBean implements TwitterAppCard
{
    private static final Logger log = Logger.getLogger(TwitterAppCardBean.class.getName());

    // Embedded data object.
    private TwitterAppCardDataObject dobj = null;

    public TwitterAppCardBean()
    {
        this(new TwitterAppCardDataObject());
    }
    public TwitterAppCardBean(String guid)
    {
        this(new TwitterAppCardDataObject(guid));
    }
    public TwitterAppCardBean(TwitterAppCardDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public TwitterAppCardDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getImage()
    {
        if(getDataObject() != null) {
            return getDataObject().getImage();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterAppCardDataObject is null!");
            return null;   // ???
        }
    }
    public void setImage(String image)
    {
        if(getDataObject() != null) {
            getDataObject().setImage(image);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterAppCardDataObject is null!");
        }
    }

    public Integer getImageWidth()
    {
        if(getDataObject() != null) {
            return getDataObject().getImageWidth();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterAppCardDataObject is null!");
            return null;   // ???
        }
    }
    public void setImageWidth(Integer imageWidth)
    {
        if(getDataObject() != null) {
            getDataObject().setImageWidth(imageWidth);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterAppCardDataObject is null!");
        }
    }

    public Integer getImageHeight()
    {
        if(getDataObject() != null) {
            return getDataObject().getImageHeight();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterAppCardDataObject is null!");
            return null;   // ???
        }
    }
    public void setImageHeight(Integer imageHeight)
    {
        if(getDataObject() != null) {
            getDataObject().setImageHeight(imageHeight);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterAppCardDataObject is null!");
        }
    }

    public TwitterCardAppInfo getIphoneApp()
    {
        if(getDataObject() != null) {
            TwitterCardAppInfo _field = getDataObject().getIphoneApp();
            if(_field == null) {
                log.log(Level.INFO, "iphoneApp is null.");
                return null;
            } else {
                return new TwitterCardAppInfoBean((TwitterCardAppInfoDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterAppCardDataObject is null!");
            return null;   // ???
        }
    }
    public void setIphoneApp(TwitterCardAppInfo iphoneApp)
    {
        if(getDataObject() != null) {
            getDataObject().setIphoneApp(
                (iphoneApp instanceof TwitterCardAppInfoBean) ?
                ((TwitterCardAppInfoBean) iphoneApp).toDataObject() :
                ((iphoneApp instanceof TwitterCardAppInfoDataObject) ? iphoneApp : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterAppCardDataObject is null!");
        }
    }

    public TwitterCardAppInfo getIpadApp()
    {
        if(getDataObject() != null) {
            TwitterCardAppInfo _field = getDataObject().getIpadApp();
            if(_field == null) {
                log.log(Level.INFO, "ipadApp is null.");
                return null;
            } else {
                return new TwitterCardAppInfoBean((TwitterCardAppInfoDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterAppCardDataObject is null!");
            return null;   // ???
        }
    }
    public void setIpadApp(TwitterCardAppInfo ipadApp)
    {
        if(getDataObject() != null) {
            getDataObject().setIpadApp(
                (ipadApp instanceof TwitterCardAppInfoBean) ?
                ((TwitterCardAppInfoBean) ipadApp).toDataObject() :
                ((ipadApp instanceof TwitterCardAppInfoDataObject) ? ipadApp : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterAppCardDataObject is null!");
        }
    }

    public TwitterCardAppInfo getGooglePlayApp()
    {
        if(getDataObject() != null) {
            TwitterCardAppInfo _field = getDataObject().getGooglePlayApp();
            if(_field == null) {
                log.log(Level.INFO, "googlePlayApp is null.");
                return null;
            } else {
                return new TwitterCardAppInfoBean((TwitterCardAppInfoDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterAppCardDataObject is null!");
            return null;   // ???
        }
    }
    public void setGooglePlayApp(TwitterCardAppInfo googlePlayApp)
    {
        if(getDataObject() != null) {
            getDataObject().setGooglePlayApp(
                (googlePlayApp instanceof TwitterCardAppInfoBean) ?
                ((TwitterCardAppInfoBean) googlePlayApp).toDataObject() :
                ((googlePlayApp instanceof TwitterCardAppInfoDataObject) ? googlePlayApp : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterAppCardDataObject is null!");
        }
    }


    // TBD
    public TwitterAppCardDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
