package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.data.TwitterCardAppInfoDataObject;

public class TwitterCardAppInfoBean implements TwitterCardAppInfo
{
    private static final Logger log = Logger.getLogger(TwitterCardAppInfoBean.class.getName());

    // Embedded data object.
    private TwitterCardAppInfoDataObject dobj = null;

    public TwitterCardAppInfoBean()
    {
        this(new TwitterCardAppInfoDataObject());
    }
    public TwitterCardAppInfoBean(TwitterCardAppInfoDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    public TwitterCardAppInfoDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getName()
    {
        if(getDataObject() != null) {
            return getDataObject().getName();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterCardAppInfoDataObject is null!");
            return null;   // ???
        }
    }
    public void setName(String name)
    {
        if(getDataObject() != null) {
            getDataObject().setName(name);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterCardAppInfoDataObject is null!");
        }
    }

    public String getId()
    {
        if(getDataObject() != null) {
            return getDataObject().getId();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterCardAppInfoDataObject is null!");
            return null;   // ???
        }
    }
    public void setId(String id)
    {
        if(getDataObject() != null) {
            getDataObject().setId(id);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterCardAppInfoDataObject is null!");
        }
    }

    public String getUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterCardAppInfoDataObject is null!");
            return null;   // ???
        }
    }
    public void setUrl(String url)
    {
        if(getDataObject() != null) {
            getDataObject().setUrl(url);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterCardAppInfoDataObject is null!");
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        if(getDataObject() != null) {
            return getDataObject().isEmpty();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterCardAppInfoDataObject is null!");
            boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getName() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getId() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getUrl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
            return (atLeastOneFieldNonEmpty == false);
        }
    }

    // TBD
    public TwitterCardAppInfoDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
