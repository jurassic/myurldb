package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.TwitterCardBase;
import com.myurldb.ws.data.TwitterCardAppInfoDataObject;
import com.myurldb.ws.data.TwitterCardProductDataDataObject;
import com.myurldb.ws.data.TwitterCardBaseDataObject;

public abstract class TwitterCardBaseBean extends BeanBase implements TwitterCardBase
{
    private static final Logger log = Logger.getLogger(TwitterCardBaseBean.class.getName());

    public TwitterCardBaseBean()
    {
        super();
    }

    @Override
    public abstract TwitterCardBaseDataObject getDataObject();

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterCardBaseDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            if(log.isLoggable(Level.INFO)) log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterCardBaseDataObject is null!");
        }
    }

    public String getCard()
    {
        if(getDataObject() != null) {
            return getDataObject().getCard();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterCardBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setCard(String card)
    {
        if(getDataObject() != null) {
            getDataObject().setCard(card);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterCardBaseDataObject is null!");
        }
    }

    public String getUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterCardBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setUrl(String url)
    {
        if(getDataObject() != null) {
            getDataObject().setUrl(url);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterCardBaseDataObject is null!");
        }
    }

    public String getTitle()
    {
        if(getDataObject() != null) {
            return getDataObject().getTitle();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterCardBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setTitle(String title)
    {
        if(getDataObject() != null) {
            getDataObject().setTitle(title);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterCardBaseDataObject is null!");
        }
    }

    public String getDescription()
    {
        if(getDataObject() != null) {
            return getDataObject().getDescription();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterCardBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setDescription(String description)
    {
        if(getDataObject() != null) {
            getDataObject().setDescription(description);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterCardBaseDataObject is null!");
        }
    }

    public String getSite()
    {
        if(getDataObject() != null) {
            return getDataObject().getSite();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterCardBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setSite(String site)
    {
        if(getDataObject() != null) {
            getDataObject().setSite(site);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterCardBaseDataObject is null!");
        }
    }

    public String getSiteId()
    {
        if(getDataObject() != null) {
            return getDataObject().getSiteId();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterCardBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setSiteId(String siteId)
    {
        if(getDataObject() != null) {
            getDataObject().setSiteId(siteId);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterCardBaseDataObject is null!");
        }
    }

    public String getCreator()
    {
        if(getDataObject() != null) {
            return getDataObject().getCreator();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterCardBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setCreator(String creator)
    {
        if(getDataObject() != null) {
            getDataObject().setCreator(creator);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterCardBaseDataObject is null!");
        }
    }

    public String getCreatorId()
    {
        if(getDataObject() != null) {
            return getDataObject().getCreatorId();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterCardBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setCreatorId(String creatorId)
    {
        if(getDataObject() != null) {
            getDataObject().setCreatorId(creatorId);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterCardBaseDataObject is null!");
        }
    }



    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
