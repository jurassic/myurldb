package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.data.TwitterCardProductDataDataObject;

public class TwitterCardProductDataBean implements TwitterCardProductData
{
    private static final Logger log = Logger.getLogger(TwitterCardProductDataBean.class.getName());

    // Embedded data object.
    private TwitterCardProductDataDataObject dobj = null;

    public TwitterCardProductDataBean()
    {
        this(new TwitterCardProductDataDataObject());
    }
    public TwitterCardProductDataBean(TwitterCardProductDataDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    public TwitterCardProductDataDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getData()
    {
        if(getDataObject() != null) {
            return getDataObject().getData();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterCardProductDataDataObject is null!");
            return null;   // ???
        }
    }
    public void setData(String data)
    {
        if(getDataObject() != null) {
            getDataObject().setData(data);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterCardProductDataDataObject is null!");
        }
    }

    public String getLabel()
    {
        if(getDataObject() != null) {
            return getDataObject().getLabel();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterCardProductDataDataObject is null!");
            return null;   // ???
        }
    }
    public void setLabel(String label)
    {
        if(getDataObject() != null) {
            getDataObject().setLabel(label);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterCardProductDataDataObject is null!");
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        if(getDataObject() != null) {
            return getDataObject().isEmpty();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterCardProductDataDataObject is null!");
            boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getData() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLabel() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
            return (atLeastOneFieldNonEmpty == false);
        }
    }

    // TBD
    public TwitterCardProductDataDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
