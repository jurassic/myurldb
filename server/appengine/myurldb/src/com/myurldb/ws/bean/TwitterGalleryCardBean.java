package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.TwitterGalleryCard;
import com.myurldb.ws.data.TwitterCardAppInfoDataObject;
import com.myurldb.ws.data.TwitterCardProductDataDataObject;
import com.myurldb.ws.data.TwitterGalleryCardDataObject;

public class TwitterGalleryCardBean extends TwitterCardBaseBean implements TwitterGalleryCard
{
    private static final Logger log = Logger.getLogger(TwitterGalleryCardBean.class.getName());

    // Embedded data object.
    private TwitterGalleryCardDataObject dobj = null;

    public TwitterGalleryCardBean()
    {
        this(new TwitterGalleryCardDataObject());
    }
    public TwitterGalleryCardBean(String guid)
    {
        this(new TwitterGalleryCardDataObject(guid));
    }
    public TwitterGalleryCardBean(TwitterGalleryCardDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public TwitterGalleryCardDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getImage0()
    {
        if(getDataObject() != null) {
            return getDataObject().getImage0();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterGalleryCardDataObject is null!");
            return null;   // ???
        }
    }
    public void setImage0(String image0)
    {
        if(getDataObject() != null) {
            getDataObject().setImage0(image0);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterGalleryCardDataObject is null!");
        }
    }

    public String getImage1()
    {
        if(getDataObject() != null) {
            return getDataObject().getImage1();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterGalleryCardDataObject is null!");
            return null;   // ???
        }
    }
    public void setImage1(String image1)
    {
        if(getDataObject() != null) {
            getDataObject().setImage1(image1);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterGalleryCardDataObject is null!");
        }
    }

    public String getImage2()
    {
        if(getDataObject() != null) {
            return getDataObject().getImage2();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterGalleryCardDataObject is null!");
            return null;   // ???
        }
    }
    public void setImage2(String image2)
    {
        if(getDataObject() != null) {
            getDataObject().setImage2(image2);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterGalleryCardDataObject is null!");
        }
    }

    public String getImage3()
    {
        if(getDataObject() != null) {
            return getDataObject().getImage3();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterGalleryCardDataObject is null!");
            return null;   // ???
        }
    }
    public void setImage3(String image3)
    {
        if(getDataObject() != null) {
            getDataObject().setImage3(image3);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterGalleryCardDataObject is null!");
        }
    }


    // TBD
    public TwitterGalleryCardDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
