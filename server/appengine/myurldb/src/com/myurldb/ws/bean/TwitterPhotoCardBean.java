package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.TwitterPhotoCard;
import com.myurldb.ws.data.TwitterCardAppInfoDataObject;
import com.myurldb.ws.data.TwitterCardProductDataDataObject;
import com.myurldb.ws.data.TwitterPhotoCardDataObject;

public class TwitterPhotoCardBean extends TwitterCardBaseBean implements TwitterPhotoCard
{
    private static final Logger log = Logger.getLogger(TwitterPhotoCardBean.class.getName());

    // Embedded data object.
    private TwitterPhotoCardDataObject dobj = null;

    public TwitterPhotoCardBean()
    {
        this(new TwitterPhotoCardDataObject());
    }
    public TwitterPhotoCardBean(String guid)
    {
        this(new TwitterPhotoCardDataObject(guid));
    }
    public TwitterPhotoCardBean(TwitterPhotoCardDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public TwitterPhotoCardDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getImage()
    {
        if(getDataObject() != null) {
            return getDataObject().getImage();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterPhotoCardDataObject is null!");
            return null;   // ???
        }
    }
    public void setImage(String image)
    {
        if(getDataObject() != null) {
            getDataObject().setImage(image);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterPhotoCardDataObject is null!");
        }
    }

    public Integer getImageWidth()
    {
        if(getDataObject() != null) {
            return getDataObject().getImageWidth();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterPhotoCardDataObject is null!");
            return null;   // ???
        }
    }
    public void setImageWidth(Integer imageWidth)
    {
        if(getDataObject() != null) {
            getDataObject().setImageWidth(imageWidth);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterPhotoCardDataObject is null!");
        }
    }

    public Integer getImageHeight()
    {
        if(getDataObject() != null) {
            return getDataObject().getImageHeight();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterPhotoCardDataObject is null!");
            return null;   // ???
        }
    }
    public void setImageHeight(Integer imageHeight)
    {
        if(getDataObject() != null) {
            getDataObject().setImageHeight(imageHeight);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterPhotoCardDataObject is null!");
        }
    }


    // TBD
    public TwitterPhotoCardDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
