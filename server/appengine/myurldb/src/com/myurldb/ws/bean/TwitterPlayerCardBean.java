package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.TwitterPlayerCard;
import com.myurldb.ws.data.TwitterCardAppInfoDataObject;
import com.myurldb.ws.data.TwitterCardProductDataDataObject;
import com.myurldb.ws.data.TwitterPlayerCardDataObject;

public class TwitterPlayerCardBean extends TwitterCardBaseBean implements TwitterPlayerCard
{
    private static final Logger log = Logger.getLogger(TwitterPlayerCardBean.class.getName());

    // Embedded data object.
    private TwitterPlayerCardDataObject dobj = null;

    public TwitterPlayerCardBean()
    {
        this(new TwitterPlayerCardDataObject());
    }
    public TwitterPlayerCardBean(String guid)
    {
        this(new TwitterPlayerCardDataObject(guid));
    }
    public TwitterPlayerCardBean(TwitterPlayerCardDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public TwitterPlayerCardDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getImage()
    {
        if(getDataObject() != null) {
            return getDataObject().getImage();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterPlayerCardDataObject is null!");
            return null;   // ???
        }
    }
    public void setImage(String image)
    {
        if(getDataObject() != null) {
            getDataObject().setImage(image);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterPlayerCardDataObject is null!");
        }
    }

    public Integer getImageWidth()
    {
        if(getDataObject() != null) {
            return getDataObject().getImageWidth();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterPlayerCardDataObject is null!");
            return null;   // ???
        }
    }
    public void setImageWidth(Integer imageWidth)
    {
        if(getDataObject() != null) {
            getDataObject().setImageWidth(imageWidth);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterPlayerCardDataObject is null!");
        }
    }

    public Integer getImageHeight()
    {
        if(getDataObject() != null) {
            return getDataObject().getImageHeight();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterPlayerCardDataObject is null!");
            return null;   // ???
        }
    }
    public void setImageHeight(Integer imageHeight)
    {
        if(getDataObject() != null) {
            getDataObject().setImageHeight(imageHeight);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterPlayerCardDataObject is null!");
        }
    }

    public String getPlayer()
    {
        if(getDataObject() != null) {
            return getDataObject().getPlayer();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterPlayerCardDataObject is null!");
            return null;   // ???
        }
    }
    public void setPlayer(String player)
    {
        if(getDataObject() != null) {
            getDataObject().setPlayer(player);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterPlayerCardDataObject is null!");
        }
    }

    public Integer getPlayerWidth()
    {
        if(getDataObject() != null) {
            return getDataObject().getPlayerWidth();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterPlayerCardDataObject is null!");
            return null;   // ???
        }
    }
    public void setPlayerWidth(Integer playerWidth)
    {
        if(getDataObject() != null) {
            getDataObject().setPlayerWidth(playerWidth);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterPlayerCardDataObject is null!");
        }
    }

    public Integer getPlayerHeight()
    {
        if(getDataObject() != null) {
            return getDataObject().getPlayerHeight();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterPlayerCardDataObject is null!");
            return null;   // ???
        }
    }
    public void setPlayerHeight(Integer playerHeight)
    {
        if(getDataObject() != null) {
            getDataObject().setPlayerHeight(playerHeight);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterPlayerCardDataObject is null!");
        }
    }

    public String getPlayerStream()
    {
        if(getDataObject() != null) {
            return getDataObject().getPlayerStream();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterPlayerCardDataObject is null!");
            return null;   // ???
        }
    }
    public void setPlayerStream(String playerStream)
    {
        if(getDataObject() != null) {
            getDataObject().setPlayerStream(playerStream);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterPlayerCardDataObject is null!");
        }
    }

    public String getPlayerStreamContentType()
    {
        if(getDataObject() != null) {
            return getDataObject().getPlayerStreamContentType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterPlayerCardDataObject is null!");
            return null;   // ???
        }
    }
    public void setPlayerStreamContentType(String playerStreamContentType)
    {
        if(getDataObject() != null) {
            getDataObject().setPlayerStreamContentType(playerStreamContentType);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterPlayerCardDataObject is null!");
        }
    }


    // TBD
    public TwitterPlayerCardDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
