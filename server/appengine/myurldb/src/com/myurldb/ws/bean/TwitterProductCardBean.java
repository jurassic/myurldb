package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.TwitterProductCard;
import com.myurldb.ws.data.TwitterCardAppInfoDataObject;
import com.myurldb.ws.data.TwitterCardProductDataDataObject;
import com.myurldb.ws.data.TwitterProductCardDataObject;

public class TwitterProductCardBean extends TwitterCardBaseBean implements TwitterProductCard
{
    private static final Logger log = Logger.getLogger(TwitterProductCardBean.class.getName());

    // Embedded data object.
    private TwitterProductCardDataObject dobj = null;

    public TwitterProductCardBean()
    {
        this(new TwitterProductCardDataObject());
    }
    public TwitterProductCardBean(String guid)
    {
        this(new TwitterProductCardDataObject(guid));
    }
    public TwitterProductCardBean(TwitterProductCardDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public TwitterProductCardDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getImage()
    {
        if(getDataObject() != null) {
            return getDataObject().getImage();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterProductCardDataObject is null!");
            return null;   // ???
        }
    }
    public void setImage(String image)
    {
        if(getDataObject() != null) {
            getDataObject().setImage(image);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterProductCardDataObject is null!");
        }
    }

    public Integer getImageWidth()
    {
        if(getDataObject() != null) {
            return getDataObject().getImageWidth();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterProductCardDataObject is null!");
            return null;   // ???
        }
    }
    public void setImageWidth(Integer imageWidth)
    {
        if(getDataObject() != null) {
            getDataObject().setImageWidth(imageWidth);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterProductCardDataObject is null!");
        }
    }

    public Integer getImageHeight()
    {
        if(getDataObject() != null) {
            return getDataObject().getImageHeight();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterProductCardDataObject is null!");
            return null;   // ???
        }
    }
    public void setImageHeight(Integer imageHeight)
    {
        if(getDataObject() != null) {
            getDataObject().setImageHeight(imageHeight);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterProductCardDataObject is null!");
        }
    }

    public TwitterCardProductData getData1()
    {
        if(getDataObject() != null) {
            TwitterCardProductData _field = getDataObject().getData1();
            if(_field == null) {
                log.log(Level.INFO, "data1 is null.");
                return null;
            } else {
                return new TwitterCardProductDataBean((TwitterCardProductDataDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterProductCardDataObject is null!");
            return null;   // ???
        }
    }
    public void setData1(TwitterCardProductData data1)
    {
        if(getDataObject() != null) {
            getDataObject().setData1(
                (data1 instanceof TwitterCardProductDataBean) ?
                ((TwitterCardProductDataBean) data1).toDataObject() :
                ((data1 instanceof TwitterCardProductDataDataObject) ? data1 : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterProductCardDataObject is null!");
        }
    }

    public TwitterCardProductData getData2()
    {
        if(getDataObject() != null) {
            TwitterCardProductData _field = getDataObject().getData2();
            if(_field == null) {
                log.log(Level.INFO, "data2 is null.");
                return null;
            } else {
                return new TwitterCardProductDataBean((TwitterCardProductDataDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterProductCardDataObject is null!");
            return null;   // ???
        }
    }
    public void setData2(TwitterCardProductData data2)
    {
        if(getDataObject() != null) {
            getDataObject().setData2(
                (data2 instanceof TwitterCardProductDataBean) ?
                ((TwitterCardProductDataBean) data2).toDataObject() :
                ((data2 instanceof TwitterCardProductDataDataObject) ? data2 : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterProductCardDataObject is null!");
        }
    }


    // TBD
    public TwitterProductCardDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
