package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.TwitterSummaryCard;
import com.myurldb.ws.data.TwitterCardAppInfoDataObject;
import com.myurldb.ws.data.TwitterCardProductDataDataObject;
import com.myurldb.ws.data.TwitterSummaryCardDataObject;

public class TwitterSummaryCardBean extends TwitterCardBaseBean implements TwitterSummaryCard
{
    private static final Logger log = Logger.getLogger(TwitterSummaryCardBean.class.getName());

    // Embedded data object.
    private TwitterSummaryCardDataObject dobj = null;

    public TwitterSummaryCardBean()
    {
        this(new TwitterSummaryCardDataObject());
    }
    public TwitterSummaryCardBean(String guid)
    {
        this(new TwitterSummaryCardDataObject(guid));
    }
    public TwitterSummaryCardBean(TwitterSummaryCardDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public TwitterSummaryCardDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getImage()
    {
        if(getDataObject() != null) {
            return getDataObject().getImage();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterSummaryCardDataObject is null!");
            return null;   // ???
        }
    }
    public void setImage(String image)
    {
        if(getDataObject() != null) {
            getDataObject().setImage(image);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterSummaryCardDataObject is null!");
        }
    }

    public Integer getImageWidth()
    {
        if(getDataObject() != null) {
            return getDataObject().getImageWidth();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterSummaryCardDataObject is null!");
            return null;   // ???
        }
    }
    public void setImageWidth(Integer imageWidth)
    {
        if(getDataObject() != null) {
            getDataObject().setImageWidth(imageWidth);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterSummaryCardDataObject is null!");
        }
    }

    public Integer getImageHeight()
    {
        if(getDataObject() != null) {
            return getDataObject().getImageHeight();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterSummaryCardDataObject is null!");
            return null;   // ???
        }
    }
    public void setImageHeight(Integer imageHeight)
    {
        if(getDataObject() != null) {
            getDataObject().setImageHeight(imageHeight);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterSummaryCardDataObject is null!");
        }
    }


    // TBD
    public TwitterSummaryCardDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
