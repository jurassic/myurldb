package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.ExternalUserIdStruct;
import com.myurldb.ws.UserAuthState;
import com.myurldb.ws.data.ExternalUserIdStructDataObject;
import com.myurldb.ws.data.UserAuthStateDataObject;

public class UserAuthStateBean extends BeanBase implements UserAuthState
{
    private static final Logger log = Logger.getLogger(UserAuthStateBean.class.getName());

    // Embedded data object.
    private UserAuthStateDataObject dobj = null;

    public UserAuthStateBean()
    {
        this(new UserAuthStateDataObject());
    }
    public UserAuthStateBean(String guid)
    {
        this(new UserAuthStateDataObject(guid));
    }
    public UserAuthStateBean(UserAuthStateDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public UserAuthStateDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserAuthStateDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            if(log.isLoggable(Level.INFO)) log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserAuthStateDataObject is null!");
        }
    }

    public String getProviderId()
    {
        if(getDataObject() != null) {
            return getDataObject().getProviderId();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserAuthStateDataObject is null!");
            return null;   // ???
        }
    }
    public void setProviderId(String providerId)
    {
        if(getDataObject() != null) {
            getDataObject().setProviderId(providerId);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserAuthStateDataObject is null!");
        }
    }

    public String getUser()
    {
        if(getDataObject() != null) {
            return getDataObject().getUser();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserAuthStateDataObject is null!");
            return null;   // ???
        }
    }
    public void setUser(String user)
    {
        if(getDataObject() != null) {
            getDataObject().setUser(user);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserAuthStateDataObject is null!");
        }
    }

    public String getUsername()
    {
        if(getDataObject() != null) {
            return getDataObject().getUsername();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserAuthStateDataObject is null!");
            return null;   // ???
        }
    }
    public void setUsername(String username)
    {
        if(getDataObject() != null) {
            getDataObject().setUsername(username);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserAuthStateDataObject is null!");
        }
    }

    public String getEmail()
    {
        if(getDataObject() != null) {
            return getDataObject().getEmail();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserAuthStateDataObject is null!");
            return null;   // ???
        }
    }
    public void setEmail(String email)
    {
        if(getDataObject() != null) {
            getDataObject().setEmail(email);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserAuthStateDataObject is null!");
        }
    }

    public String getOpenId()
    {
        if(getDataObject() != null) {
            return getDataObject().getOpenId();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserAuthStateDataObject is null!");
            return null;   // ???
        }
    }
    public void setOpenId(String openId)
    {
        if(getDataObject() != null) {
            getDataObject().setOpenId(openId);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserAuthStateDataObject is null!");
        }
    }

    public String getDeviceId()
    {
        if(getDataObject() != null) {
            return getDataObject().getDeviceId();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserAuthStateDataObject is null!");
            return null;   // ???
        }
    }
    public void setDeviceId(String deviceId)
    {
        if(getDataObject() != null) {
            getDataObject().setDeviceId(deviceId);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserAuthStateDataObject is null!");
        }
    }

    public String getSessionId()
    {
        if(getDataObject() != null) {
            return getDataObject().getSessionId();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserAuthStateDataObject is null!");
            return null;   // ???
        }
    }
    public void setSessionId(String sessionId)
    {
        if(getDataObject() != null) {
            getDataObject().setSessionId(sessionId);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserAuthStateDataObject is null!");
        }
    }

    public String getAuthToken()
    {
        if(getDataObject() != null) {
            return getDataObject().getAuthToken();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserAuthStateDataObject is null!");
            return null;   // ???
        }
    }
    public void setAuthToken(String authToken)
    {
        if(getDataObject() != null) {
            getDataObject().setAuthToken(authToken);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserAuthStateDataObject is null!");
        }
    }

    public String getAuthStatus()
    {
        if(getDataObject() != null) {
            return getDataObject().getAuthStatus();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserAuthStateDataObject is null!");
            return null;   // ???
        }
    }
    public void setAuthStatus(String authStatus)
    {
        if(getDataObject() != null) {
            getDataObject().setAuthStatus(authStatus);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserAuthStateDataObject is null!");
        }
    }

    public String getExternalAuth()
    {
        if(getDataObject() != null) {
            return getDataObject().getExternalAuth();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserAuthStateDataObject is null!");
            return null;   // ???
        }
    }
    public void setExternalAuth(String externalAuth)
    {
        if(getDataObject() != null) {
            getDataObject().setExternalAuth(externalAuth);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserAuthStateDataObject is null!");
        }
    }

    public ExternalUserIdStruct getExternalId()
    {
        if(getDataObject() != null) {
            ExternalUserIdStruct _field = getDataObject().getExternalId();
            if(_field == null) {
                log.log(Level.INFO, "externalId is null.");
                return null;
            } else {
                return new ExternalUserIdStructBean((ExternalUserIdStructDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserAuthStateDataObject is null!");
            return null;   // ???
        }
    }
    public void setExternalId(ExternalUserIdStruct externalId)
    {
        if(getDataObject() != null) {
            getDataObject().setExternalId(
                (externalId instanceof ExternalUserIdStructBean) ?
                ((ExternalUserIdStructBean) externalId).toDataObject() :
                ((externalId instanceof ExternalUserIdStructDataObject) ? externalId : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserAuthStateDataObject is null!");
        }
    }

    public String getStatus()
    {
        if(getDataObject() != null) {
            return getDataObject().getStatus();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserAuthStateDataObject is null!");
            return null;   // ???
        }
    }
    public void setStatus(String status)
    {
        if(getDataObject() != null) {
            getDataObject().setStatus(status);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserAuthStateDataObject is null!");
        }
    }

    public Long getFirstAuthTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getFirstAuthTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserAuthStateDataObject is null!");
            return null;   // ???
        }
    }
    public void setFirstAuthTime(Long firstAuthTime)
    {
        if(getDataObject() != null) {
            getDataObject().setFirstAuthTime(firstAuthTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserAuthStateDataObject is null!");
        }
    }

    public Long getLastAuthTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getLastAuthTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserAuthStateDataObject is null!");
            return null;   // ???
        }
    }
    public void setLastAuthTime(Long lastAuthTime)
    {
        if(getDataObject() != null) {
            getDataObject().setLastAuthTime(lastAuthTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserAuthStateDataObject is null!");
        }
    }

    public Long getExpirationTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getExpirationTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserAuthStateDataObject is null!");
            return null;   // ???
        }
    }
    public void setExpirationTime(Long expirationTime)
    {
        if(getDataObject() != null) {
            getDataObject().setExpirationTime(expirationTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserAuthStateDataObject is null!");
        }
    }


    // TBD
    public UserAuthStateDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
