package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.GeoPointStruct;
import com.myurldb.ws.StreetAddressStruct;
import com.myurldb.ws.GaeAppStruct;
import com.myurldb.ws.FullNameStruct;
import com.myurldb.ws.GaeUserStruct;
import com.myurldb.ws.User;
import com.myurldb.ws.data.GeoPointStructDataObject;
import com.myurldb.ws.data.StreetAddressStructDataObject;
import com.myurldb.ws.data.GaeAppStructDataObject;
import com.myurldb.ws.data.FullNameStructDataObject;
import com.myurldb.ws.data.GaeUserStructDataObject;
import com.myurldb.ws.data.UserDataObject;

public class UserBean extends BeanBase implements User
{
    private static final Logger log = Logger.getLogger(UserBean.class.getName());

    // Embedded data object.
    private UserDataObject dobj = null;

    public UserBean()
    {
        this(new UserDataObject());
    }
    public UserBean(String guid)
    {
        this(new UserDataObject(guid));
    }
    public UserBean(UserDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public UserDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            if(log.isLoggable(Level.INFO)) log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
        }
    }

    public String getManagerApp()
    {
        if(getDataObject() != null) {
            return getDataObject().getManagerApp();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
            return null;   // ???
        }
    }
    public void setManagerApp(String managerApp)
    {
        if(getDataObject() != null) {
            getDataObject().setManagerApp(managerApp);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
        }
    }

    public Long getAppAcl()
    {
        if(getDataObject() != null) {
            return getDataObject().getAppAcl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
            return null;   // ???
        }
    }
    public void setAppAcl(Long appAcl)
    {
        if(getDataObject() != null) {
            getDataObject().setAppAcl(appAcl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
        }
    }

    public GaeAppStruct getGaeApp()
    {
        if(getDataObject() != null) {
            GaeAppStruct _field = getDataObject().getGaeApp();
            if(_field == null) {
                log.log(Level.INFO, "gaeApp is null.");
                return null;
            } else {
                return new GaeAppStructBean((GaeAppStructDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
            return null;   // ???
        }
    }
    public void setGaeApp(GaeAppStruct gaeApp)
    {
        if(getDataObject() != null) {
            getDataObject().setGaeApp(
                (gaeApp instanceof GaeAppStructBean) ?
                ((GaeAppStructBean) gaeApp).toDataObject() :
                ((gaeApp instanceof GaeAppStructDataObject) ? gaeApp : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
        }
    }

    public String getAeryId()
    {
        if(getDataObject() != null) {
            return getDataObject().getAeryId();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
            return null;   // ???
        }
    }
    public void setAeryId(String aeryId)
    {
        if(getDataObject() != null) {
            getDataObject().setAeryId(aeryId);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
        }
    }

    public String getSessionId()
    {
        if(getDataObject() != null) {
            return getDataObject().getSessionId();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
            return null;   // ???
        }
    }
    public void setSessionId(String sessionId)
    {
        if(getDataObject() != null) {
            getDataObject().setSessionId(sessionId);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
        }
    }

    public String getAncestorGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getAncestorGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
            return null;   // ???
        }
    }
    public void setAncestorGuid(String ancestorGuid)
    {
        if(getDataObject() != null) {
            getDataObject().setAncestorGuid(ancestorGuid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
        }
    }

    public FullNameStruct getName()
    {
        if(getDataObject() != null) {
            FullNameStruct _field = getDataObject().getName();
            if(_field == null) {
                log.log(Level.INFO, "name is null.");
                return null;
            } else {
                return new FullNameStructBean((FullNameStructDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
            return null;   // ???
        }
    }
    public void setName(FullNameStruct name)
    {
        if(getDataObject() != null) {
            getDataObject().setName(
                (name instanceof FullNameStructBean) ?
                ((FullNameStructBean) name).toDataObject() :
                ((name instanceof FullNameStructDataObject) ? name : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
        }
    }

    public String getUsercode()
    {
        if(getDataObject() != null) {
            return getDataObject().getUsercode();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
            return null;   // ???
        }
    }
    public void setUsercode(String usercode)
    {
        if(getDataObject() != null) {
            getDataObject().setUsercode(usercode);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
        }
    }

    public String getUsername()
    {
        if(getDataObject() != null) {
            return getDataObject().getUsername();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
            return null;   // ???
        }
    }
    public void setUsername(String username)
    {
        if(getDataObject() != null) {
            getDataObject().setUsername(username);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
        }
    }

    public String getNickname()
    {
        if(getDataObject() != null) {
            return getDataObject().getNickname();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
            return null;   // ???
        }
    }
    public void setNickname(String nickname)
    {
        if(getDataObject() != null) {
            getDataObject().setNickname(nickname);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
        }
    }

    public String getAvatar()
    {
        if(getDataObject() != null) {
            return getDataObject().getAvatar();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
            return null;   // ???
        }
    }
    public void setAvatar(String avatar)
    {
        if(getDataObject() != null) {
            getDataObject().setAvatar(avatar);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
        }
    }

    public String getEmail()
    {
        if(getDataObject() != null) {
            return getDataObject().getEmail();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
            return null;   // ???
        }
    }
    public void setEmail(String email)
    {
        if(getDataObject() != null) {
            getDataObject().setEmail(email);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
        }
    }

    public String getOpenId()
    {
        if(getDataObject() != null) {
            return getDataObject().getOpenId();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
            return null;   // ???
        }
    }
    public void setOpenId(String openId)
    {
        if(getDataObject() != null) {
            getDataObject().setOpenId(openId);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
        }
    }

    public GaeUserStruct getGaeUser()
    {
        if(getDataObject() != null) {
            GaeUserStruct _field = getDataObject().getGaeUser();
            if(_field == null) {
                log.log(Level.INFO, "gaeUser is null.");
                return null;
            } else {
                return new GaeUserStructBean((GaeUserStructDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
            return null;   // ???
        }
    }
    public void setGaeUser(GaeUserStruct gaeUser)
    {
        if(getDataObject() != null) {
            getDataObject().setGaeUser(
                (gaeUser instanceof GaeUserStructBean) ?
                ((GaeUserStructBean) gaeUser).toDataObject() :
                ((gaeUser instanceof GaeUserStructDataObject) ? gaeUser : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
        }
    }

    public String getEntityType()
    {
        if(getDataObject() != null) {
            return getDataObject().getEntityType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
            return null;   // ???
        }
    }
    public void setEntityType(String entityType)
    {
        if(getDataObject() != null) {
            getDataObject().setEntityType(entityType);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
        }
    }

    public Boolean isSurrogate()
    {
        if(getDataObject() != null) {
            return getDataObject().isSurrogate();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
            return null;   // ???
        }
    }
    public void setSurrogate(Boolean surrogate)
    {
        if(getDataObject() != null) {
            getDataObject().setSurrogate(surrogate);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
        }
    }

    public Boolean isObsolete()
    {
        if(getDataObject() != null) {
            return getDataObject().isObsolete();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
            return null;   // ???
        }
    }
    public void setObsolete(Boolean obsolete)
    {
        if(getDataObject() != null) {
            getDataObject().setObsolete(obsolete);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
        }
    }

    public String getTimeZone()
    {
        if(getDataObject() != null) {
            return getDataObject().getTimeZone();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
            return null;   // ???
        }
    }
    public void setTimeZone(String timeZone)
    {
        if(getDataObject() != null) {
            getDataObject().setTimeZone(timeZone);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
        }
    }

    public String getLocation()
    {
        if(getDataObject() != null) {
            return getDataObject().getLocation();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
            return null;   // ???
        }
    }
    public void setLocation(String location)
    {
        if(getDataObject() != null) {
            getDataObject().setLocation(location);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
        }
    }

    public StreetAddressStruct getStreetAddress()
    {
        if(getDataObject() != null) {
            StreetAddressStruct _field = getDataObject().getStreetAddress();
            if(_field == null) {
                log.log(Level.INFO, "streetAddress is null.");
                return null;
            } else {
                return new StreetAddressStructBean((StreetAddressStructDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
            return null;   // ???
        }
    }
    public void setStreetAddress(StreetAddressStruct streetAddress)
    {
        if(getDataObject() != null) {
            getDataObject().setStreetAddress(
                (streetAddress instanceof StreetAddressStructBean) ?
                ((StreetAddressStructBean) streetAddress).toDataObject() :
                ((streetAddress instanceof StreetAddressStructDataObject) ? streetAddress : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
        }
    }

    public GeoPointStruct getGeoPoint()
    {
        if(getDataObject() != null) {
            GeoPointStruct _field = getDataObject().getGeoPoint();
            if(_field == null) {
                log.log(Level.INFO, "geoPoint is null.");
                return null;
            } else {
                return new GeoPointStructBean((GeoPointStructDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
            return null;   // ???
        }
    }
    public void setGeoPoint(GeoPointStruct geoPoint)
    {
        if(getDataObject() != null) {
            getDataObject().setGeoPoint(
                (geoPoint instanceof GeoPointStructBean) ?
                ((GeoPointStructBean) geoPoint).toDataObject() :
                ((geoPoint instanceof GeoPointStructDataObject) ? geoPoint : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
        }
    }

    public String getIpAddress()
    {
        if(getDataObject() != null) {
            return getDataObject().getIpAddress();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
            return null;   // ???
        }
    }
    public void setIpAddress(String ipAddress)
    {
        if(getDataObject() != null) {
            getDataObject().setIpAddress(ipAddress);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
        }
    }

    public String getReferer()
    {
        if(getDataObject() != null) {
            return getDataObject().getReferer();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
            return null;   // ???
        }
    }
    public void setReferer(String referer)
    {
        if(getDataObject() != null) {
            getDataObject().setReferer(referer);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
        }
    }

    public String getStatus()
    {
        if(getDataObject() != null) {
            return getDataObject().getStatus();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
            return null;   // ???
        }
    }
    public void setStatus(String status)
    {
        if(getDataObject() != null) {
            getDataObject().setStatus(status);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
        }
    }

    public Long getEmailVerifiedTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getEmailVerifiedTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
            return null;   // ???
        }
    }
    public void setEmailVerifiedTime(Long emailVerifiedTime)
    {
        if(getDataObject() != null) {
            getDataObject().setEmailVerifiedTime(emailVerifiedTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
        }
    }

    public Long getOpenIdVerifiedTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getOpenIdVerifiedTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
            return null;   // ???
        }
    }
    public void setOpenIdVerifiedTime(Long openIdVerifiedTime)
    {
        if(getDataObject() != null) {
            getDataObject().setOpenIdVerifiedTime(openIdVerifiedTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
        }
    }

    public Long getAuthenticatedTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getAuthenticatedTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
            return null;   // ???
        }
    }
    public void setAuthenticatedTime(Long authenticatedTime)
    {
        if(getDataObject() != null) {
            getDataObject().setAuthenticatedTime(authenticatedTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserDataObject is null!");
        }
    }


    // TBD
    public UserDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
