package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.UserCustomDomain;
import com.myurldb.ws.data.UserCustomDomainDataObject;

public class UserCustomDomainBean extends BeanBase implements UserCustomDomain
{
    private static final Logger log = Logger.getLogger(UserCustomDomainBean.class.getName());

    // Embedded data object.
    private UserCustomDomainDataObject dobj = null;

    public UserCustomDomainBean()
    {
        this(new UserCustomDomainDataObject());
    }
    public UserCustomDomainBean(String guid)
    {
        this(new UserCustomDomainDataObject(guid));
    }
    public UserCustomDomainBean(UserCustomDomainDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public UserCustomDomainDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserCustomDomainDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            if(log.isLoggable(Level.INFO)) log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserCustomDomainDataObject is null!");
        }
    }

    public String getOwner()
    {
        if(getDataObject() != null) {
            return getDataObject().getOwner();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserCustomDomainDataObject is null!");
            return null;   // ???
        }
    }
    public void setOwner(String owner)
    {
        if(getDataObject() != null) {
            getDataObject().setOwner(owner);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserCustomDomainDataObject is null!");
        }
    }

    public String getDomain()
    {
        if(getDataObject() != null) {
            return getDataObject().getDomain();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserCustomDomainDataObject is null!");
            return null;   // ???
        }
    }
    public void setDomain(String domain)
    {
        if(getDataObject() != null) {
            getDataObject().setDomain(domain);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserCustomDomainDataObject is null!");
        }
    }

    public Boolean isVerified()
    {
        if(getDataObject() != null) {
            return getDataObject().isVerified();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserCustomDomainDataObject is null!");
            return null;   // ???
        }
    }
    public void setVerified(Boolean verified)
    {
        if(getDataObject() != null) {
            getDataObject().setVerified(verified);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserCustomDomainDataObject is null!");
        }
    }

    public String getStatus()
    {
        if(getDataObject() != null) {
            return getDataObject().getStatus();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserCustomDomainDataObject is null!");
            return null;   // ???
        }
    }
    public void setStatus(String status)
    {
        if(getDataObject() != null) {
            getDataObject().setStatus(status);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserCustomDomainDataObject is null!");
        }
    }

    public Long getVerifiedTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getVerifiedTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserCustomDomainDataObject is null!");
            return null;   // ???
        }
    }
    public void setVerifiedTime(Long verifiedTime)
    {
        if(getDataObject() != null) {
            getDataObject().setVerifiedTime(verifiedTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserCustomDomainDataObject is null!");
        }
    }


    // TBD
    public UserCustomDomainDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
