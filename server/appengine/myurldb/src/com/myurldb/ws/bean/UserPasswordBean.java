package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.HashedPasswordStruct;
import com.myurldb.ws.UserPassword;
import com.myurldb.ws.data.HashedPasswordStructDataObject;
import com.myurldb.ws.data.UserPasswordDataObject;

public class UserPasswordBean extends BeanBase implements UserPassword
{
    private static final Logger log = Logger.getLogger(UserPasswordBean.class.getName());

    // Embedded data object.
    private UserPasswordDataObject dobj = null;

    public UserPasswordBean()
    {
        this(new UserPasswordDataObject());
    }
    public UserPasswordBean(String guid)
    {
        this(new UserPasswordDataObject(guid));
    }
    public UserPasswordBean(UserPasswordDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public UserPasswordDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserPasswordDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            if(log.isLoggable(Level.INFO)) log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserPasswordDataObject is null!");
        }
    }

    public String getAdmin()
    {
        if(getDataObject() != null) {
            return getDataObject().getAdmin();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserPasswordDataObject is null!");
            return null;   // ???
        }
    }
    public void setAdmin(String admin)
    {
        if(getDataObject() != null) {
            getDataObject().setAdmin(admin);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserPasswordDataObject is null!");
        }
    }

    public String getUser()
    {
        if(getDataObject() != null) {
            return getDataObject().getUser();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserPasswordDataObject is null!");
            return null;   // ???
        }
    }
    public void setUser(String user)
    {
        if(getDataObject() != null) {
            getDataObject().setUser(user);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserPasswordDataObject is null!");
        }
    }

    public String getUsername()
    {
        if(getDataObject() != null) {
            return getDataObject().getUsername();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserPasswordDataObject is null!");
            return null;   // ???
        }
    }
    public void setUsername(String username)
    {
        if(getDataObject() != null) {
            getDataObject().setUsername(username);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserPasswordDataObject is null!");
        }
    }

    public String getEmail()
    {
        if(getDataObject() != null) {
            return getDataObject().getEmail();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserPasswordDataObject is null!");
            return null;   // ???
        }
    }
    public void setEmail(String email)
    {
        if(getDataObject() != null) {
            getDataObject().setEmail(email);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserPasswordDataObject is null!");
        }
    }

    public String getOpenId()
    {
        if(getDataObject() != null) {
            return getDataObject().getOpenId();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserPasswordDataObject is null!");
            return null;   // ???
        }
    }
    public void setOpenId(String openId)
    {
        if(getDataObject() != null) {
            getDataObject().setOpenId(openId);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserPasswordDataObject is null!");
        }
    }

    public HashedPasswordStruct getPassword()
    {
        if(getDataObject() != null) {
            HashedPasswordStruct _field = getDataObject().getPassword();
            if(_field == null) {
                log.log(Level.INFO, "password is null.");
                return null;
            } else {
                return new HashedPasswordStructBean((HashedPasswordStructDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserPasswordDataObject is null!");
            return null;   // ???
        }
    }
    public void setPassword(HashedPasswordStruct password)
    {
        if(getDataObject() != null) {
            getDataObject().setPassword(
                (password instanceof HashedPasswordStructBean) ?
                ((HashedPasswordStructBean) password).toDataObject() :
                ((password instanceof HashedPasswordStructDataObject) ? password : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserPasswordDataObject is null!");
        }
    }

    public Boolean isResetRequired()
    {
        if(getDataObject() != null) {
            return getDataObject().isResetRequired();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserPasswordDataObject is null!");
            return null;   // ???
        }
    }
    public void setResetRequired(Boolean resetRequired)
    {
        if(getDataObject() != null) {
            getDataObject().setResetRequired(resetRequired);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserPasswordDataObject is null!");
        }
    }

    public String getChallengeQuestion()
    {
        if(getDataObject() != null) {
            return getDataObject().getChallengeQuestion();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserPasswordDataObject is null!");
            return null;   // ???
        }
    }
    public void setChallengeQuestion(String challengeQuestion)
    {
        if(getDataObject() != null) {
            getDataObject().setChallengeQuestion(challengeQuestion);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserPasswordDataObject is null!");
        }
    }

    public String getChallengeAnswer()
    {
        if(getDataObject() != null) {
            return getDataObject().getChallengeAnswer();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserPasswordDataObject is null!");
            return null;   // ???
        }
    }
    public void setChallengeAnswer(String challengeAnswer)
    {
        if(getDataObject() != null) {
            getDataObject().setChallengeAnswer(challengeAnswer);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserPasswordDataObject is null!");
        }
    }

    public String getStatus()
    {
        if(getDataObject() != null) {
            return getDataObject().getStatus();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserPasswordDataObject is null!");
            return null;   // ???
        }
    }
    public void setStatus(String status)
    {
        if(getDataObject() != null) {
            getDataObject().setStatus(status);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserPasswordDataObject is null!");
        }
    }

    public Long getLastResetTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getLastResetTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserPasswordDataObject is null!");
            return null;   // ???
        }
    }
    public void setLastResetTime(Long lastResetTime)
    {
        if(getDataObject() != null) {
            getDataObject().setLastResetTime(lastResetTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserPasswordDataObject is null!");
        }
    }

    public Long getExpirationTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getExpirationTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserPasswordDataObject is null!");
            return null;   // ???
        }
    }
    public void setExpirationTime(Long expirationTime)
    {
        if(getDataObject() != null) {
            getDataObject().setExpirationTime(expirationTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserPasswordDataObject is null!");
        }
    }


    // TBD
    public UserPasswordDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
