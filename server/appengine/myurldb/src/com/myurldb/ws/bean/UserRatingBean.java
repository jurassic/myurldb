package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.UserRating;
import com.myurldb.ws.data.UserRatingDataObject;

public class UserRatingBean extends BeanBase implements UserRating
{
    private static final Logger log = Logger.getLogger(UserRatingBean.class.getName());

    // Embedded data object.
    private UserRatingDataObject dobj = null;

    public UserRatingBean()
    {
        this(new UserRatingDataObject());
    }
    public UserRatingBean(String guid)
    {
        this(new UserRatingDataObject(guid));
    }
    public UserRatingBean(UserRatingDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public UserRatingDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserRatingDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            if(log.isLoggable(Level.INFO)) log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserRatingDataObject is null!");
        }
    }

    public String getUser()
    {
        if(getDataObject() != null) {
            return getDataObject().getUser();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserRatingDataObject is null!");
            return null;   // ???
        }
    }
    public void setUser(String user)
    {
        if(getDataObject() != null) {
            getDataObject().setUser(user);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserRatingDataObject is null!");
        }
    }

    public Double getRating()
    {
        if(getDataObject() != null) {
            return getDataObject().getRating();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserRatingDataObject is null!");
            return null;   // ???
        }
    }
    public void setRating(Double rating)
    {
        if(getDataObject() != null) {
            getDataObject().setRating(rating);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserRatingDataObject is null!");
        }
    }

    public String getNote()
    {
        if(getDataObject() != null) {
            return getDataObject().getNote();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserRatingDataObject is null!");
            return null;   // ???
        }
    }
    public void setNote(String note)
    {
        if(getDataObject() != null) {
            getDataObject().setNote(note);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserRatingDataObject is null!");
        }
    }

    public Long getRatedTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getRatedTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserRatingDataObject is null!");
            return null;   // ???
        }
    }
    public void setRatedTime(Long ratedTime)
    {
        if(getDataObject() != null) {
            getDataObject().setRatedTime(ratedTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserRatingDataObject is null!");
        }
    }


    // TBD
    public UserRatingDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
