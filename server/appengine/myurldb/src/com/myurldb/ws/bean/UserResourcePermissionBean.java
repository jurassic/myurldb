package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.UserResourcePermission;
import com.myurldb.ws.data.UserResourcePermissionDataObject;

public class UserResourcePermissionBean extends BeanBase implements UserResourcePermission
{
    private static final Logger log = Logger.getLogger(UserResourcePermissionBean.class.getName());

    // Embedded data object.
    private UserResourcePermissionDataObject dobj = null;

    public UserResourcePermissionBean()
    {
        this(new UserResourcePermissionDataObject());
    }
    public UserResourcePermissionBean(String guid)
    {
        this(new UserResourcePermissionDataObject(guid));
    }
    public UserResourcePermissionBean(UserResourcePermissionDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public UserResourcePermissionDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserResourcePermissionDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            if(log.isLoggable(Level.INFO)) log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserResourcePermissionDataObject is null!");
        }
    }

    public String getUser()
    {
        if(getDataObject() != null) {
            return getDataObject().getUser();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserResourcePermissionDataObject is null!");
            return null;   // ???
        }
    }
    public void setUser(String user)
    {
        if(getDataObject() != null) {
            getDataObject().setUser(user);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserResourcePermissionDataObject is null!");
        }
    }

    public String getPermissionName()
    {
        if(getDataObject() != null) {
            return getDataObject().getPermissionName();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserResourcePermissionDataObject is null!");
            return null;   // ???
        }
    }
    public void setPermissionName(String permissionName)
    {
        if(getDataObject() != null) {
            getDataObject().setPermissionName(permissionName);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserResourcePermissionDataObject is null!");
        }
    }

    public String getResource()
    {
        if(getDataObject() != null) {
            return getDataObject().getResource();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserResourcePermissionDataObject is null!");
            return null;   // ???
        }
    }
    public void setResource(String resource)
    {
        if(getDataObject() != null) {
            getDataObject().setResource(resource);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserResourcePermissionDataObject is null!");
        }
    }

    public String getInstance()
    {
        if(getDataObject() != null) {
            return getDataObject().getInstance();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserResourcePermissionDataObject is null!");
            return null;   // ???
        }
    }
    public void setInstance(String instance)
    {
        if(getDataObject() != null) {
            getDataObject().setInstance(instance);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserResourcePermissionDataObject is null!");
        }
    }

    public String getAction()
    {
        if(getDataObject() != null) {
            return getDataObject().getAction();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserResourcePermissionDataObject is null!");
            return null;   // ???
        }
    }
    public void setAction(String action)
    {
        if(getDataObject() != null) {
            getDataObject().setAction(action);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserResourcePermissionDataObject is null!");
        }
    }

    public Boolean isPermitted()
    {
        if(getDataObject() != null) {
            return getDataObject().isPermitted();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserResourcePermissionDataObject is null!");
            return null;   // ???
        }
    }
    public void setPermitted(Boolean permitted)
    {
        if(getDataObject() != null) {
            getDataObject().setPermitted(permitted);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserResourcePermissionDataObject is null!");
        }
    }

    public String getStatus()
    {
        if(getDataObject() != null) {
            return getDataObject().getStatus();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserResourcePermissionDataObject is null!");
            return null;   // ???
        }
    }
    public void setStatus(String status)
    {
        if(getDataObject() != null) {
            getDataObject().setStatus(status);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserResourcePermissionDataObject is null!");
        }
    }


    // TBD
    public UserResourcePermissionDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
