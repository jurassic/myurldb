package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.UserResourceProhibition;
import com.myurldb.ws.data.UserResourceProhibitionDataObject;

public class UserResourceProhibitionBean extends BeanBase implements UserResourceProhibition
{
    private static final Logger log = Logger.getLogger(UserResourceProhibitionBean.class.getName());

    // Embedded data object.
    private UserResourceProhibitionDataObject dobj = null;

    public UserResourceProhibitionBean()
    {
        this(new UserResourceProhibitionDataObject());
    }
    public UserResourceProhibitionBean(String guid)
    {
        this(new UserResourceProhibitionDataObject(guid));
    }
    public UserResourceProhibitionBean(UserResourceProhibitionDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public UserResourceProhibitionDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserResourceProhibitionDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            if(log.isLoggable(Level.INFO)) log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserResourceProhibitionDataObject is null!");
        }
    }

    public String getUser()
    {
        if(getDataObject() != null) {
            return getDataObject().getUser();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserResourceProhibitionDataObject is null!");
            return null;   // ???
        }
    }
    public void setUser(String user)
    {
        if(getDataObject() != null) {
            getDataObject().setUser(user);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserResourceProhibitionDataObject is null!");
        }
    }

    public String getPermissionName()
    {
        if(getDataObject() != null) {
            return getDataObject().getPermissionName();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserResourceProhibitionDataObject is null!");
            return null;   // ???
        }
    }
    public void setPermissionName(String permissionName)
    {
        if(getDataObject() != null) {
            getDataObject().setPermissionName(permissionName);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserResourceProhibitionDataObject is null!");
        }
    }

    public String getResource()
    {
        if(getDataObject() != null) {
            return getDataObject().getResource();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserResourceProhibitionDataObject is null!");
            return null;   // ???
        }
    }
    public void setResource(String resource)
    {
        if(getDataObject() != null) {
            getDataObject().setResource(resource);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserResourceProhibitionDataObject is null!");
        }
    }

    public String getInstance()
    {
        if(getDataObject() != null) {
            return getDataObject().getInstance();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserResourceProhibitionDataObject is null!");
            return null;   // ???
        }
    }
    public void setInstance(String instance)
    {
        if(getDataObject() != null) {
            getDataObject().setInstance(instance);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserResourceProhibitionDataObject is null!");
        }
    }

    public String getAction()
    {
        if(getDataObject() != null) {
            return getDataObject().getAction();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserResourceProhibitionDataObject is null!");
            return null;   // ???
        }
    }
    public void setAction(String action)
    {
        if(getDataObject() != null) {
            getDataObject().setAction(action);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserResourceProhibitionDataObject is null!");
        }
    }

    public Boolean isProhibited()
    {
        if(getDataObject() != null) {
            return getDataObject().isProhibited();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserResourceProhibitionDataObject is null!");
            return null;   // ???
        }
    }
    public void setProhibited(Boolean prohibited)
    {
        if(getDataObject() != null) {
            getDataObject().setProhibited(prohibited);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserResourceProhibitionDataObject is null!");
        }
    }

    public String getStatus()
    {
        if(getDataObject() != null) {
            return getDataObject().getStatus();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserResourceProhibitionDataObject is null!");
            return null;   // ???
        }
    }
    public void setStatus(String status)
    {
        if(getDataObject() != null) {
            getDataObject().setStatus(status);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserResourceProhibitionDataObject is null!");
        }
    }


    // TBD
    public UserResourceProhibitionDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
