package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.UserRole;
import com.myurldb.ws.data.UserRoleDataObject;

public class UserRoleBean extends BeanBase implements UserRole
{
    private static final Logger log = Logger.getLogger(UserRoleBean.class.getName());

    // Embedded data object.
    private UserRoleDataObject dobj = null;

    public UserRoleBean()
    {
        this(new UserRoleDataObject());
    }
    public UserRoleBean(String guid)
    {
        this(new UserRoleDataObject(guid));
    }
    public UserRoleBean(UserRoleDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public UserRoleDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserRoleDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            if(log.isLoggable(Level.INFO)) log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserRoleDataObject is null!");
        }
    }

    public String getUser()
    {
        if(getDataObject() != null) {
            return getDataObject().getUser();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserRoleDataObject is null!");
            return null;   // ???
        }
    }
    public void setUser(String user)
    {
        if(getDataObject() != null) {
            getDataObject().setUser(user);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserRoleDataObject is null!");
        }
    }

    public String getRole()
    {
        if(getDataObject() != null) {
            return getDataObject().getRole();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserRoleDataObject is null!");
            return null;   // ???
        }
    }
    public void setRole(String role)
    {
        if(getDataObject() != null) {
            getDataObject().setRole(role);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserRoleDataObject is null!");
        }
    }

    public String getStatus()
    {
        if(getDataObject() != null) {
            return getDataObject().getStatus();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserRoleDataObject is null!");
            return null;   // ???
        }
    }
    public void setStatus(String status)
    {
        if(getDataObject() != null) {
            getDataObject().setStatus(status);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserRoleDataObject is null!");
        }
    }


    // TBD
    public UserRoleDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
