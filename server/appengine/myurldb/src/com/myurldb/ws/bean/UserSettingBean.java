package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.UserSetting;
import com.myurldb.ws.data.UserSettingDataObject;

public class UserSettingBean extends PersonalSettingBean implements UserSetting
{
    private static final Logger log = Logger.getLogger(UserSettingBean.class.getName());

    // Embedded data object.
    private UserSettingDataObject dobj = null;

    public UserSettingBean()
    {
        this(new UserSettingDataObject());
    }
    public UserSettingBean(String guid)
    {
        this(new UserSettingDataObject(guid));
    }
    public UserSettingBean(UserSettingDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public UserSettingDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getUser()
    {
        if(getDataObject() != null) {
            return getDataObject().getUser();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
            return null;   // ???
        }
    }
    public void setUser(String user)
    {
        if(getDataObject() != null) {
            getDataObject().setUser(user);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
        }
    }

    public String getHomePage()
    {
        if(getDataObject() != null) {
            return getDataObject().getHomePage();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
            return null;   // ???
        }
    }
    public void setHomePage(String homePage)
    {
        if(getDataObject() != null) {
            getDataObject().setHomePage(homePage);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
        }
    }

    public String getSelfBio()
    {
        if(getDataObject() != null) {
            return getDataObject().getSelfBio();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
            return null;   // ???
        }
    }
    public void setSelfBio(String selfBio)
    {
        if(getDataObject() != null) {
            getDataObject().setSelfBio(selfBio);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
        }
    }

    public String getUserUrlDomain()
    {
        if(getDataObject() != null) {
            return getDataObject().getUserUrlDomain();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
            return null;   // ???
        }
    }
    public void setUserUrlDomain(String userUrlDomain)
    {
        if(getDataObject() != null) {
            getDataObject().setUserUrlDomain(userUrlDomain);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
        }
    }

    public String getUserUrlDomainNormalized()
    {
        if(getDataObject() != null) {
            return getDataObject().getUserUrlDomainNormalized();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
            return null;   // ???
        }
    }
    public void setUserUrlDomainNormalized(String userUrlDomainNormalized)
    {
        if(getDataObject() != null) {
            getDataObject().setUserUrlDomainNormalized(userUrlDomainNormalized);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
        }
    }

    public Boolean isAutoRedirectEnabled()
    {
        if(getDataObject() != null) {
            return getDataObject().isAutoRedirectEnabled();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
            return null;   // ???
        }
    }
    public void setAutoRedirectEnabled(Boolean autoRedirectEnabled)
    {
        if(getDataObject() != null) {
            getDataObject().setAutoRedirectEnabled(autoRedirectEnabled);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
        }
    }

    public Boolean isViewEnabled()
    {
        if(getDataObject() != null) {
            return getDataObject().isViewEnabled();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
            return null;   // ???
        }
    }
    public void setViewEnabled(Boolean viewEnabled)
    {
        if(getDataObject() != null) {
            getDataObject().setViewEnabled(viewEnabled);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
        }
    }

    public Boolean isShareEnabled()
    {
        if(getDataObject() != null) {
            return getDataObject().isShareEnabled();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
            return null;   // ???
        }
    }
    public void setShareEnabled(Boolean shareEnabled)
    {
        if(getDataObject() != null) {
            getDataObject().setShareEnabled(shareEnabled);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
        }
    }

    public String getDefaultDomain()
    {
        if(getDataObject() != null) {
            return getDataObject().getDefaultDomain();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
            return null;   // ???
        }
    }
    public void setDefaultDomain(String defaultDomain)
    {
        if(getDataObject() != null) {
            getDataObject().setDefaultDomain(defaultDomain);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
        }
    }

    public String getDefaultTokenType()
    {
        if(getDataObject() != null) {
            return getDataObject().getDefaultTokenType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
            return null;   // ???
        }
    }
    public void setDefaultTokenType(String defaultTokenType)
    {
        if(getDataObject() != null) {
            getDataObject().setDefaultTokenType(defaultTokenType);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
        }
    }

    public String getDefaultRedirectType()
    {
        if(getDataObject() != null) {
            return getDataObject().getDefaultRedirectType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
            return null;   // ???
        }
    }
    public void setDefaultRedirectType(String defaultRedirectType)
    {
        if(getDataObject() != null) {
            getDataObject().setDefaultRedirectType(defaultRedirectType);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
        }
    }

    public Long getDefaultFlashDuration()
    {
        if(getDataObject() != null) {
            return getDataObject().getDefaultFlashDuration();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
            return null;   // ???
        }
    }
    public void setDefaultFlashDuration(Long defaultFlashDuration)
    {
        if(getDataObject() != null) {
            getDataObject().setDefaultFlashDuration(defaultFlashDuration);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
        }
    }

    public String getDefaultAccessType()
    {
        if(getDataObject() != null) {
            return getDataObject().getDefaultAccessType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
            return null;   // ???
        }
    }
    public void setDefaultAccessType(String defaultAccessType)
    {
        if(getDataObject() != null) {
            getDataObject().setDefaultAccessType(defaultAccessType);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
        }
    }

    public String getDefaultViewType()
    {
        if(getDataObject() != null) {
            return getDataObject().getDefaultViewType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
            return null;   // ???
        }
    }
    public void setDefaultViewType(String defaultViewType)
    {
        if(getDataObject() != null) {
            getDataObject().setDefaultViewType(defaultViewType);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
        }
    }

    public String getDefaultShareType()
    {
        if(getDataObject() != null) {
            return getDataObject().getDefaultShareType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
            return null;   // ???
        }
    }
    public void setDefaultShareType(String defaultShareType)
    {
        if(getDataObject() != null) {
            getDataObject().setDefaultShareType(defaultShareType);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
        }
    }

    public String getDefaultPassphrase()
    {
        if(getDataObject() != null) {
            return getDataObject().getDefaultPassphrase();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
            return null;   // ???
        }
    }
    public void setDefaultPassphrase(String defaultPassphrase)
    {
        if(getDataObject() != null) {
            getDataObject().setDefaultPassphrase(defaultPassphrase);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
        }
    }

    public String getDefaultSignature()
    {
        if(getDataObject() != null) {
            return getDataObject().getDefaultSignature();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
            return null;   // ???
        }
    }
    public void setDefaultSignature(String defaultSignature)
    {
        if(getDataObject() != null) {
            getDataObject().setDefaultSignature(defaultSignature);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
        }
    }

    public Boolean isUseSignature()
    {
        if(getDataObject() != null) {
            return getDataObject().isUseSignature();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
            return null;   // ???
        }
    }
    public void setUseSignature(Boolean useSignature)
    {
        if(getDataObject() != null) {
            getDataObject().setUseSignature(useSignature);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
        }
    }

    public String getDefaultEmblem()
    {
        if(getDataObject() != null) {
            return getDataObject().getDefaultEmblem();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
            return null;   // ???
        }
    }
    public void setDefaultEmblem(String defaultEmblem)
    {
        if(getDataObject() != null) {
            getDataObject().setDefaultEmblem(defaultEmblem);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
        }
    }

    public Boolean isUseEmblem()
    {
        if(getDataObject() != null) {
            return getDataObject().isUseEmblem();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
            return null;   // ???
        }
    }
    public void setUseEmblem(Boolean useEmblem)
    {
        if(getDataObject() != null) {
            getDataObject().setUseEmblem(useEmblem);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
        }
    }

    public String getDefaultBackground()
    {
        if(getDataObject() != null) {
            return getDataObject().getDefaultBackground();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
            return null;   // ???
        }
    }
    public void setDefaultBackground(String defaultBackground)
    {
        if(getDataObject() != null) {
            getDataObject().setDefaultBackground(defaultBackground);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
        }
    }

    public Boolean isUseBackground()
    {
        if(getDataObject() != null) {
            return getDataObject().isUseBackground();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
            return null;   // ???
        }
    }
    public void setUseBackground(Boolean useBackground)
    {
        if(getDataObject() != null) {
            getDataObject().setUseBackground(useBackground);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
        }
    }

    public String getSponsorUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getSponsorUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
            return null;   // ???
        }
    }
    public void setSponsorUrl(String sponsorUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setSponsorUrl(sponsorUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
        }
    }

    public String getSponsorBanner()
    {
        if(getDataObject() != null) {
            return getDataObject().getSponsorBanner();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
            return null;   // ???
        }
    }
    public void setSponsorBanner(String sponsorBanner)
    {
        if(getDataObject() != null) {
            getDataObject().setSponsorBanner(sponsorBanner);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
        }
    }

    public String getSponsorNote()
    {
        if(getDataObject() != null) {
            return getDataObject().getSponsorNote();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
            return null;   // ???
        }
    }
    public void setSponsorNote(String sponsorNote)
    {
        if(getDataObject() != null) {
            getDataObject().setSponsorNote(sponsorNote);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
        }
    }

    public Boolean isUseSponsor()
    {
        if(getDataObject() != null) {
            return getDataObject().isUseSponsor();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
            return null;   // ???
        }
    }
    public void setUseSponsor(Boolean useSponsor)
    {
        if(getDataObject() != null) {
            getDataObject().setUseSponsor(useSponsor);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
        }
    }

    public String getExtraParams()
    {
        if(getDataObject() != null) {
            return getDataObject().getExtraParams();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
            return null;   // ???
        }
    }
    public void setExtraParams(String extraParams)
    {
        if(getDataObject() != null) {
            getDataObject().setExtraParams(extraParams);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
        }
    }

    public Long getExpirationDuration()
    {
        if(getDataObject() != null) {
            return getDataObject().getExpirationDuration();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
            return null;   // ???
        }
    }
    public void setExpirationDuration(Long expirationDuration)
    {
        if(getDataObject() != null) {
            getDataObject().setExpirationDuration(expirationDuration);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserSettingDataObject is null!");
        }
    }


    // TBD
    public UserSettingDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
