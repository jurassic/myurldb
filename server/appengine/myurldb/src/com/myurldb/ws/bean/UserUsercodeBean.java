package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.UserUsercode;
import com.myurldb.ws.data.UserUsercodeDataObject;

public class UserUsercodeBean extends BeanBase implements UserUsercode
{
    private static final Logger log = Logger.getLogger(UserUsercodeBean.class.getName());

    // Embedded data object.
    private UserUsercodeDataObject dobj = null;

    public UserUsercodeBean()
    {
        this(new UserUsercodeDataObject());
    }
    public UserUsercodeBean(String guid)
    {
        this(new UserUsercodeDataObject(guid));
    }
    public UserUsercodeBean(UserUsercodeDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public UserUsercodeDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserUsercodeDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            if(log.isLoggable(Level.INFO)) log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserUsercodeDataObject is null!");
        }
    }

    public String getAdmin()
    {
        if(getDataObject() != null) {
            return getDataObject().getAdmin();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserUsercodeDataObject is null!");
            return null;   // ???
        }
    }
    public void setAdmin(String admin)
    {
        if(getDataObject() != null) {
            getDataObject().setAdmin(admin);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserUsercodeDataObject is null!");
        }
    }

    public String getUser()
    {
        if(getDataObject() != null) {
            return getDataObject().getUser();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserUsercodeDataObject is null!");
            return null;   // ???
        }
    }
    public void setUser(String user)
    {
        if(getDataObject() != null) {
            getDataObject().setUser(user);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserUsercodeDataObject is null!");
        }
    }

    public String getUsername()
    {
        if(getDataObject() != null) {
            return getDataObject().getUsername();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserUsercodeDataObject is null!");
            return null;   // ???
        }
    }
    public void setUsername(String username)
    {
        if(getDataObject() != null) {
            getDataObject().setUsername(username);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserUsercodeDataObject is null!");
        }
    }

    public String getUsercode()
    {
        if(getDataObject() != null) {
            return getDataObject().getUsercode();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserUsercodeDataObject is null!");
            return null;   // ???
        }
    }
    public void setUsercode(String usercode)
    {
        if(getDataObject() != null) {
            getDataObject().setUsercode(usercode);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserUsercodeDataObject is null!");
        }
    }

    public String getStatus()
    {
        if(getDataObject() != null) {
            return getDataObject().getStatus();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserUsercodeDataObject is null!");
            return null;   // ???
        }
    }
    public void setStatus(String status)
    {
        if(getDataObject() != null) {
            getDataObject().setStatus(status);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserUsercodeDataObject is null!");
        }
    }


    // TBD
    public UserUsercodeDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
