package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.VisitorSetting;
import com.myurldb.ws.data.VisitorSettingDataObject;

public class VisitorSettingBean extends PersonalSettingBean implements VisitorSetting
{
    private static final Logger log = Logger.getLogger(VisitorSettingBean.class.getName());

    // Embedded data object.
    private VisitorSettingDataObject dobj = null;

    public VisitorSettingBean()
    {
        this(new VisitorSettingDataObject());
    }
    public VisitorSettingBean(String guid)
    {
        this(new VisitorSettingDataObject(guid));
    }
    public VisitorSettingBean(VisitorSettingDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public VisitorSettingDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getUser()
    {
        if(getDataObject() != null) {
            return getDataObject().getUser();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded VisitorSettingDataObject is null!");
            return null;   // ???
        }
    }
    public void setUser(String user)
    {
        if(getDataObject() != null) {
            getDataObject().setUser(user);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded VisitorSettingDataObject is null!");
        }
    }

    public String getRedirectType()
    {
        if(getDataObject() != null) {
            return getDataObject().getRedirectType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded VisitorSettingDataObject is null!");
            return null;   // ???
        }
    }
    public void setRedirectType(String redirectType)
    {
        if(getDataObject() != null) {
            getDataObject().setRedirectType(redirectType);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded VisitorSettingDataObject is null!");
        }
    }

    public Long getFlashDuration()
    {
        if(getDataObject() != null) {
            return getDataObject().getFlashDuration();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded VisitorSettingDataObject is null!");
            return null;   // ???
        }
    }
    public void setFlashDuration(Long flashDuration)
    {
        if(getDataObject() != null) {
            getDataObject().setFlashDuration(flashDuration);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded VisitorSettingDataObject is null!");
        }
    }

    public String getPrivacyType()
    {
        if(getDataObject() != null) {
            return getDataObject().getPrivacyType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded VisitorSettingDataObject is null!");
            return null;   // ???
        }
    }
    public void setPrivacyType(String privacyType)
    {
        if(getDataObject() != null) {
            getDataObject().setPrivacyType(privacyType);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded VisitorSettingDataObject is null!");
        }
    }


    // TBD
    public VisitorSettingDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
