package com.myurldb.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.WebProfileStruct;
import com.myurldb.ws.data.WebProfileStructDataObject;

public class WebProfileStructBean implements WebProfileStruct
{
    private static final Logger log = Logger.getLogger(WebProfileStructBean.class.getName());

    // Embedded data object.
    private WebProfileStructDataObject dobj = null;

    public WebProfileStructBean()
    {
        this(new WebProfileStructDataObject());
    }
    public WebProfileStructBean(WebProfileStructDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    public WebProfileStructDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getUuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getUuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded WebProfileStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setUuid(String uuid)
    {
        if(getDataObject() != null) {
            getDataObject().setUuid(uuid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded WebProfileStructDataObject is null!");
        }
    }

    public String getType()
    {
        if(getDataObject() != null) {
            return getDataObject().getType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded WebProfileStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setType(String type)
    {
        if(getDataObject() != null) {
            getDataObject().setType(type);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded WebProfileStructDataObject is null!");
        }
    }

    public String getServiceName()
    {
        if(getDataObject() != null) {
            return getDataObject().getServiceName();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded WebProfileStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setServiceName(String serviceName)
    {
        if(getDataObject() != null) {
            getDataObject().setServiceName(serviceName);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded WebProfileStructDataObject is null!");
        }
    }

    public String getServiceUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getServiceUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded WebProfileStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setServiceUrl(String serviceUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setServiceUrl(serviceUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded WebProfileStructDataObject is null!");
        }
    }

    public String getProfileUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getProfileUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded WebProfileStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setProfileUrl(String profileUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setProfileUrl(profileUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded WebProfileStructDataObject is null!");
        }
    }

    public String getNote()
    {
        if(getDataObject() != null) {
            return getDataObject().getNote();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded WebProfileStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setNote(String note)
    {
        if(getDataObject() != null) {
            getDataObject().setNote(note);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded WebProfileStructDataObject is null!");
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        if(getDataObject() != null) {
            return getDataObject().isEmpty();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded WebProfileStructDataObject is null!");
            boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getServiceName() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getServiceUrl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getProfileUrl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
            return (atLeastOneFieldNonEmpty == false);
        }
    }

    // TBD
    public WebProfileStructDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
