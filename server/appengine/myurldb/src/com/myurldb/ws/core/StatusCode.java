package com.myurldb.ws.core;

// HTTP response codes.
public final class StatusCode
{
    // http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
    public final static int CONTINUE = 100;
    public final static int SWITCHING_PROTOCOLS = 101;
    public final static int OK = 200;
    public final static int CREATED = 201;
    public final static int ACCEPTED = 202;
    public final static int NON_AUTHORITATIVE_INFORMATION = 203;
    public final static int NO_CONTENT = 204;
    public final static int RESET_CONTENT = 205;
    public final static int PARTIAL_CONTENT = 206;
    public final static int MULTIPLE_CHOICES = 300;
    public final static int MOVED_PERMANENTLY = 301;
    public final static int FOUND = 302;
    public final static int SEE_OTHER = 303;
    public final static int NOT_MODIFIED = 304;
    public final static int USE_PROXY = 305;
    public final static int TEMPORARY_REDIRECT = 307;
    public final static int BAD_REQUEST = 400;
    public final static int UNAUTHORIZED = 401;
    public final static int PAYMENT_REQUIRED = 402;
    public final static int FORBIDDEN = 403;
    public final static int NOT_FOUND = 404;
    public final static int METHOD_NOT_ALLOWED = 405;
    public final static int NOT_ACCEPTABLE = 406;
    public final static int PROXY_AUTHENTICATION_REQUIRED = 407;
    public final static int REQUEST_TIMEOUT = 408;
    public final static int CONFLICT = 409;
    public final static int GONE = 410;
    public final static int LENGTH_REQUIRED = 411;
    public final static int PRECONDITION_FAILED = 412;
    public final static int REQUEST_ENTITY_TOO_LARGE = 413;
    public final static int REQUEST_URI_TOO_LONG = 414;
    public final static int UNSUPPORTED_MEDIA_TYPE = 415;
    public final static int REQUEST_RANGE_NOT_SATISFIABLE = 416;
    public final static int EXPECTATION_FAILED = 417;
    public final static int INTERNAL_SERVER_ERROR = 500;
    public final static int NOT_IMPLEMENTED = 501;
    public final static int BAD_GATEWAY = 502;
    public final static int SERVICE_UNAVAILABLE = 503;
    public final static int GATEWAY_TIMEOUT = 504;
    public final static int HTTP_VERSION_NOT_SUPPORTED = 505;

    // Static contants/methods only.
    private StatusCode() {}

    public static boolean isInformational(int code) 
    {
        if(code >= 100 && code < 200) {
            return true;
        } else {
            return false;
        }
    }
    
    public static boolean isSuccessful(int code) 
    {
        if(code >= 200 && code < 300) {
            return true;
        } else {
            return false;
        }
    }
    
    public static boolean isRedirection(int code)
    {
        if(code >= 300 && code < 400) {
            return true;
        } else {
            return false;
        }
    }
    
    public static boolean isClientError(int code)
    {
        if(code >= 400 && code < 500) {
            return true;
        } else {
            return false;
        }
    }
    
    public static boolean isServerError(int code)
    {
        if(code >= 500 && code < 600) {
            return true;
        } else {
            return false;
        }
    }

    // Note: isSuccessful(code) != !isError(code).
    public static boolean isError(int code)
    {
        return (isClientError(code) || isServerError(code));
    }

}
