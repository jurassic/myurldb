package com.myurldb.ws.dao;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.data.AbuseTagDataObject;

// TBD: Add offset/count to getAllAbuseTags() and findAbuseTags(), etc.
public interface AbuseTagDAO
{
    AbuseTagDataObject getAbuseTag(String guid) throws BaseException;
    List<AbuseTagDataObject> getAbuseTags(List<String> guids) throws BaseException;
    List<AbuseTagDataObject> getAllAbuseTags() throws BaseException;
    List<AbuseTagDataObject> getAllAbuseTags(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllAbuseTagKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<AbuseTagDataObject> findAbuseTags(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<AbuseTagDataObject> findAbuseTags(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    List<AbuseTagDataObject> findAbuseTags(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findAbuseTagKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createAbuseTag(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return AbuseTagDataObject?)
    String createAbuseTag(AbuseTagDataObject abuseTag) throws BaseException;          // Returns Guid.  (Return AbuseTagDataObject?)
    //Boolean updateAbuseTag(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateAbuseTag(AbuseTagDataObject abuseTag) throws BaseException;
    Boolean deleteAbuseTag(String guid) throws BaseException;
    Boolean deleteAbuseTag(AbuseTagDataObject abuseTag) throws BaseException;
    Long deleteAbuseTags(String filter, String params, List<String> values) throws BaseException;
}
