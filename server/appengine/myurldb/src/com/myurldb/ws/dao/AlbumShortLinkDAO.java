package com.myurldb.ws.dao;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.data.AlbumShortLinkDataObject;

// TBD: Add offset/count to getAllAlbumShortLinks() and findAlbumShortLinks(), etc.
public interface AlbumShortLinkDAO
{
    AlbumShortLinkDataObject getAlbumShortLink(String guid) throws BaseException;
    List<AlbumShortLinkDataObject> getAlbumShortLinks(List<String> guids) throws BaseException;
    List<AlbumShortLinkDataObject> getAllAlbumShortLinks() throws BaseException;
    List<AlbumShortLinkDataObject> getAllAlbumShortLinks(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllAlbumShortLinkKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<AlbumShortLinkDataObject> findAlbumShortLinks(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<AlbumShortLinkDataObject> findAlbumShortLinks(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    List<AlbumShortLinkDataObject> findAlbumShortLinks(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findAlbumShortLinkKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createAlbumShortLink(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return AlbumShortLinkDataObject?)
    String createAlbumShortLink(AlbumShortLinkDataObject albumShortLink) throws BaseException;          // Returns Guid.  (Return AlbumShortLinkDataObject?)
    //Boolean updateAlbumShortLink(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateAlbumShortLink(AlbumShortLinkDataObject albumShortLink) throws BaseException;
    Boolean deleteAlbumShortLink(String guid) throws BaseException;
    Boolean deleteAlbumShortLink(AlbumShortLinkDataObject albumShortLink) throws BaseException;
    Long deleteAlbumShortLinks(String filter, String params, List<String> values) throws BaseException;
}
