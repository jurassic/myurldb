package com.myurldb.ws.dao;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.data.AppClientDataObject;

// TBD: Add offset/count to getAllAppClients() and findAppClients(), etc.
public interface AppClientDAO
{
    AppClientDataObject getAppClient(String guid) throws BaseException;
    List<AppClientDataObject> getAppClients(List<String> guids) throws BaseException;
    List<AppClientDataObject> getAllAppClients() throws BaseException;
    List<AppClientDataObject> getAllAppClients(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllAppClientKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<AppClientDataObject> findAppClients(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<AppClientDataObject> findAppClients(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    List<AppClientDataObject> findAppClients(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findAppClientKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createAppClient(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return AppClientDataObject?)
    String createAppClient(AppClientDataObject appClient) throws BaseException;          // Returns Guid.  (Return AppClientDataObject?)
    //Boolean updateAppClient(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateAppClient(AppClientDataObject appClient) throws BaseException;
    Boolean deleteAppClient(String guid) throws BaseException;
    Boolean deleteAppClient(AppClientDataObject appClient) throws BaseException;
    Long deleteAppClients(String filter, String params, List<String> values) throws BaseException;
}
