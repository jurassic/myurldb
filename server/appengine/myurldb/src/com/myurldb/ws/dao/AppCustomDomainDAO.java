package com.myurldb.ws.dao;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.data.AppCustomDomainDataObject;

// TBD: Add offset/count to getAllAppCustomDomains() and findAppCustomDomains(), etc.
public interface AppCustomDomainDAO
{
    AppCustomDomainDataObject getAppCustomDomain(String guid) throws BaseException;
    List<AppCustomDomainDataObject> getAppCustomDomains(List<String> guids) throws BaseException;
    List<AppCustomDomainDataObject> getAllAppCustomDomains() throws BaseException;
    List<AppCustomDomainDataObject> getAllAppCustomDomains(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllAppCustomDomainKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<AppCustomDomainDataObject> findAppCustomDomains(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<AppCustomDomainDataObject> findAppCustomDomains(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    List<AppCustomDomainDataObject> findAppCustomDomains(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findAppCustomDomainKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createAppCustomDomain(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return AppCustomDomainDataObject?)
    String createAppCustomDomain(AppCustomDomainDataObject appCustomDomain) throws BaseException;          // Returns Guid.  (Return AppCustomDomainDataObject?)
    //Boolean updateAppCustomDomain(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateAppCustomDomain(AppCustomDomainDataObject appCustomDomain) throws BaseException;
    Boolean deleteAppCustomDomain(String guid) throws BaseException;
    Boolean deleteAppCustomDomain(AppCustomDomainDataObject appCustomDomain) throws BaseException;
    Long deleteAppCustomDomains(String filter, String params, List<String> values) throws BaseException;
}
