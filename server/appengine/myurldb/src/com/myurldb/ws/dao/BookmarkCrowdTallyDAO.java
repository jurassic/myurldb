package com.myurldb.ws.dao;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.data.BookmarkCrowdTallyDataObject;

// TBD: Add offset/count to getAllBookmarkCrowdTallies() and findBookmarkCrowdTallies(), etc.
public interface BookmarkCrowdTallyDAO
{
    BookmarkCrowdTallyDataObject getBookmarkCrowdTally(String guid) throws BaseException;
    List<BookmarkCrowdTallyDataObject> getBookmarkCrowdTallies(List<String> guids) throws BaseException;
    List<BookmarkCrowdTallyDataObject> getAllBookmarkCrowdTallies() throws BaseException;
    List<BookmarkCrowdTallyDataObject> getAllBookmarkCrowdTallies(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllBookmarkCrowdTallyKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<BookmarkCrowdTallyDataObject> findBookmarkCrowdTallies(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<BookmarkCrowdTallyDataObject> findBookmarkCrowdTallies(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    List<BookmarkCrowdTallyDataObject> findBookmarkCrowdTallies(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findBookmarkCrowdTallyKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createBookmarkCrowdTally(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return BookmarkCrowdTallyDataObject?)
    String createBookmarkCrowdTally(BookmarkCrowdTallyDataObject bookmarkCrowdTally) throws BaseException;          // Returns Guid.  (Return BookmarkCrowdTallyDataObject?)
    //Boolean updateBookmarkCrowdTally(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateBookmarkCrowdTally(BookmarkCrowdTallyDataObject bookmarkCrowdTally) throws BaseException;
    Boolean deleteBookmarkCrowdTally(String guid) throws BaseException;
    Boolean deleteBookmarkCrowdTally(BookmarkCrowdTallyDataObject bookmarkCrowdTally) throws BaseException;
    Long deleteBookmarkCrowdTallies(String filter, String params, List<String> values) throws BaseException;
}
