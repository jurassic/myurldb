package com.myurldb.ws.dao;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.data.BookmarkFolderDataObject;

// TBD: Add offset/count to getAllBookmarkFolders() and findBookmarkFolders(), etc.
public interface BookmarkFolderDAO
{
    BookmarkFolderDataObject getBookmarkFolder(String guid) throws BaseException;
    List<BookmarkFolderDataObject> getBookmarkFolders(List<String> guids) throws BaseException;
    List<BookmarkFolderDataObject> getAllBookmarkFolders() throws BaseException;
    List<BookmarkFolderDataObject> getAllBookmarkFolders(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllBookmarkFolderKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<BookmarkFolderDataObject> findBookmarkFolders(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<BookmarkFolderDataObject> findBookmarkFolders(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    List<BookmarkFolderDataObject> findBookmarkFolders(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findBookmarkFolderKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createBookmarkFolder(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return BookmarkFolderDataObject?)
    String createBookmarkFolder(BookmarkFolderDataObject bookmarkFolder) throws BaseException;          // Returns Guid.  (Return BookmarkFolderDataObject?)
    //Boolean updateBookmarkFolder(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateBookmarkFolder(BookmarkFolderDataObject bookmarkFolder) throws BaseException;
    Boolean deleteBookmarkFolder(String guid) throws BaseException;
    Boolean deleteBookmarkFolder(BookmarkFolderDataObject bookmarkFolder) throws BaseException;
    Long deleteBookmarkFolders(String filter, String params, List<String> values) throws BaseException;
}
