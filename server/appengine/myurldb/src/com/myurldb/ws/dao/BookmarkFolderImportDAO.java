package com.myurldb.ws.dao;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.data.BookmarkFolderImportDataObject;

// TBD: Add offset/count to getAllBookmarkFolderImports() and findBookmarkFolderImports(), etc.
public interface BookmarkFolderImportDAO
{
    BookmarkFolderImportDataObject getBookmarkFolderImport(String guid) throws BaseException;
    List<BookmarkFolderImportDataObject> getBookmarkFolderImports(List<String> guids) throws BaseException;
    List<BookmarkFolderImportDataObject> getAllBookmarkFolderImports() throws BaseException;
    List<BookmarkFolderImportDataObject> getAllBookmarkFolderImports(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllBookmarkFolderImportKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<BookmarkFolderImportDataObject> findBookmarkFolderImports(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<BookmarkFolderImportDataObject> findBookmarkFolderImports(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    List<BookmarkFolderImportDataObject> findBookmarkFolderImports(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findBookmarkFolderImportKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createBookmarkFolderImport(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return BookmarkFolderImportDataObject?)
    String createBookmarkFolderImport(BookmarkFolderImportDataObject bookmarkFolderImport) throws BaseException;          // Returns Guid.  (Return BookmarkFolderImportDataObject?)
    //Boolean updateBookmarkFolderImport(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateBookmarkFolderImport(BookmarkFolderImportDataObject bookmarkFolderImport) throws BaseException;
    Boolean deleteBookmarkFolderImport(String guid) throws BaseException;
    Boolean deleteBookmarkFolderImport(BookmarkFolderImportDataObject bookmarkFolderImport) throws BaseException;
    Long deleteBookmarkFolderImports(String filter, String params, List<String> values) throws BaseException;
}
