package com.myurldb.ws.dao;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.data.BookmarkLinkDataObject;

// TBD: Add offset/count to getAllBookmarkLinks() and findBookmarkLinks(), etc.
public interface BookmarkLinkDAO
{
    BookmarkLinkDataObject getBookmarkLink(String guid) throws BaseException;
    List<BookmarkLinkDataObject> getBookmarkLinks(List<String> guids) throws BaseException;
    List<BookmarkLinkDataObject> getAllBookmarkLinks() throws BaseException;
    List<BookmarkLinkDataObject> getAllBookmarkLinks(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllBookmarkLinkKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<BookmarkLinkDataObject> findBookmarkLinks(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<BookmarkLinkDataObject> findBookmarkLinks(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    List<BookmarkLinkDataObject> findBookmarkLinks(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findBookmarkLinkKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createBookmarkLink(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return BookmarkLinkDataObject?)
    String createBookmarkLink(BookmarkLinkDataObject bookmarkLink) throws BaseException;          // Returns Guid.  (Return BookmarkLinkDataObject?)
    //Boolean updateBookmarkLink(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateBookmarkLink(BookmarkLinkDataObject bookmarkLink) throws BaseException;
    Boolean deleteBookmarkLink(String guid) throws BaseException;
    Boolean deleteBookmarkLink(BookmarkLinkDataObject bookmarkLink) throws BaseException;
    Long deleteBookmarkLinks(String filter, String params, List<String> values) throws BaseException;
}
