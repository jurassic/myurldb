package com.myurldb.ws.dao;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.data.ClientSettingDataObject;

// TBD: Add offset/count to getAllClientSettings() and findClientSettings(), etc.
public interface ClientSettingDAO
{
    ClientSettingDataObject getClientSetting(String guid) throws BaseException;
    List<ClientSettingDataObject> getClientSettings(List<String> guids) throws BaseException;
    List<ClientSettingDataObject> getAllClientSettings() throws BaseException;
    List<ClientSettingDataObject> getAllClientSettings(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllClientSettingKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<ClientSettingDataObject> findClientSettings(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<ClientSettingDataObject> findClientSettings(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    List<ClientSettingDataObject> findClientSettings(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findClientSettingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createClientSetting(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return ClientSettingDataObject?)
    String createClientSetting(ClientSettingDataObject clientSetting) throws BaseException;          // Returns Guid.  (Return ClientSettingDataObject?)
    //Boolean updateClientSetting(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateClientSetting(ClientSettingDataObject clientSetting) throws BaseException;
    Boolean deleteClientSetting(String guid) throws BaseException;
    Boolean deleteClientSetting(ClientSettingDataObject clientSetting) throws BaseException;
    Long deleteClientSettings(String filter, String params, List<String> values) throws BaseException;
}
