package com.myurldb.ws.dao;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.data.ClientUserDataObject;

// TBD: Add offset/count to getAllClientUsers() and findClientUsers(), etc.
public interface ClientUserDAO
{
    ClientUserDataObject getClientUser(String guid) throws BaseException;
    List<ClientUserDataObject> getClientUsers(List<String> guids) throws BaseException;
    List<ClientUserDataObject> getAllClientUsers() throws BaseException;
    List<ClientUserDataObject> getAllClientUsers(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllClientUserKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<ClientUserDataObject> findClientUsers(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<ClientUserDataObject> findClientUsers(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    List<ClientUserDataObject> findClientUsers(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findClientUserKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createClientUser(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return ClientUserDataObject?)
    String createClientUser(ClientUserDataObject clientUser) throws BaseException;          // Returns Guid.  (Return ClientUserDataObject?)
    //Boolean updateClientUser(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateClientUser(ClientUserDataObject clientUser) throws BaseException;
    Boolean deleteClientUser(String guid) throws BaseException;
    Boolean deleteClientUser(ClientUserDataObject clientUser) throws BaseException;
    Long deleteClientUsers(String filter, String params, List<String> values) throws BaseException;
}
