package com.myurldb.ws.dao;

// Abstract factory.
public abstract class DAOFactory
{
    public abstract ApiConsumerDAO getApiConsumerDAO();
    public abstract AppCustomDomainDAO getAppCustomDomainDAO();
    public abstract SiteCustomDomainDAO getSiteCustomDomainDAO();
    public abstract UserDAO getUserDAO();
    public abstract UserUsercodeDAO getUserUsercodeDAO();
    public abstract UserPasswordDAO getUserPasswordDAO();
    public abstract ExternalUserAuthDAO getExternalUserAuthDAO();
    public abstract UserAuthStateDAO getUserAuthStateDAO();
    public abstract UserResourcePermissionDAO getUserResourcePermissionDAO();
    public abstract UserResourceProhibitionDAO getUserResourceProhibitionDAO();
    public abstract RolePermissionDAO getRolePermissionDAO();
    public abstract UserRoleDAO getUserRoleDAO();
    public abstract AppClientDAO getAppClientDAO();
    public abstract ClientUserDAO getClientUserDAO();
    public abstract UserCustomDomainDAO getUserCustomDomainDAO();
    public abstract ClientSettingDAO getClientSettingDAO();
    public abstract UserSettingDAO getUserSettingDAO();
    public abstract VisitorSettingDAO getVisitorSettingDAO();
    public abstract TwitterSummaryCardDAO getTwitterSummaryCardDAO();
    public abstract TwitterPhotoCardDAO getTwitterPhotoCardDAO();
    public abstract TwitterGalleryCardDAO getTwitterGalleryCardDAO();
    public abstract TwitterAppCardDAO getTwitterAppCardDAO();
    public abstract TwitterPlayerCardDAO getTwitterPlayerCardDAO();
    public abstract TwitterProductCardDAO getTwitterProductCardDAO();
    public abstract ShortPassageDAO getShortPassageDAO();
    public abstract ShortLinkDAO getShortLinkDAO();
    public abstract GeoLinkDAO getGeoLinkDAO();
    public abstract QrCodeDAO getQrCodeDAO();
    public abstract LinkPassphraseDAO getLinkPassphraseDAO();
    public abstract LinkMessageDAO getLinkMessageDAO();
    public abstract LinkAlbumDAO getLinkAlbumDAO();
    public abstract AlbumShortLinkDAO getAlbumShortLinkDAO();
    public abstract KeywordFolderDAO getKeywordFolderDAO();
    public abstract BookmarkFolderDAO getBookmarkFolderDAO();
    public abstract KeywordLinkDAO getKeywordLinkDAO();
    public abstract BookmarkLinkDAO getBookmarkLinkDAO();
    public abstract SpeedDialDAO getSpeedDialDAO();
    public abstract KeywordFolderImportDAO getKeywordFolderImportDAO();
    public abstract BookmarkFolderImportDAO getBookmarkFolderImportDAO();
    public abstract KeywordCrowdTallyDAO getKeywordCrowdTallyDAO();
    public abstract BookmarkCrowdTallyDAO getBookmarkCrowdTallyDAO();
    public abstract DomainInfoDAO getDomainInfoDAO();
    public abstract UrlRatingDAO getUrlRatingDAO();
    public abstract UserRatingDAO getUserRatingDAO();
    public abstract AbuseTagDAO getAbuseTagDAO();
    public abstract ServiceInfoDAO getServiceInfoDAO();
    public abstract FiveTenDAO getFiveTenDAO();
}
