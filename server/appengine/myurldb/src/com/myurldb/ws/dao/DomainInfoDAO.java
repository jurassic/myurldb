package com.myurldb.ws.dao;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.data.DomainInfoDataObject;

// TBD: Add offset/count to getAllDomainInfos() and findDomainInfos(), etc.
public interface DomainInfoDAO
{
    DomainInfoDataObject getDomainInfo(String guid) throws BaseException;
    List<DomainInfoDataObject> getDomainInfos(List<String> guids) throws BaseException;
    List<DomainInfoDataObject> getAllDomainInfos() throws BaseException;
    List<DomainInfoDataObject> getAllDomainInfos(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllDomainInfoKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<DomainInfoDataObject> findDomainInfos(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<DomainInfoDataObject> findDomainInfos(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    List<DomainInfoDataObject> findDomainInfos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findDomainInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createDomainInfo(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return DomainInfoDataObject?)
    String createDomainInfo(DomainInfoDataObject domainInfo) throws BaseException;          // Returns Guid.  (Return DomainInfoDataObject?)
    //Boolean updateDomainInfo(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateDomainInfo(DomainInfoDataObject domainInfo) throws BaseException;
    Boolean deleteDomainInfo(String guid) throws BaseException;
    Boolean deleteDomainInfo(DomainInfoDataObject domainInfo) throws BaseException;
    Long deleteDomainInfos(String filter, String params, List<String> values) throws BaseException;
}
