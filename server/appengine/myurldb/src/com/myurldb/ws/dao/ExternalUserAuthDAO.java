package com.myurldb.ws.dao;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.data.ExternalUserAuthDataObject;

// TBD: Add offset/count to getAllExternalUserAuths() and findExternalUserAuths(), etc.
public interface ExternalUserAuthDAO
{
    ExternalUserAuthDataObject getExternalUserAuth(String guid) throws BaseException;
    List<ExternalUserAuthDataObject> getExternalUserAuths(List<String> guids) throws BaseException;
    List<ExternalUserAuthDataObject> getAllExternalUserAuths() throws BaseException;
    List<ExternalUserAuthDataObject> getAllExternalUserAuths(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllExternalUserAuthKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<ExternalUserAuthDataObject> findExternalUserAuths(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<ExternalUserAuthDataObject> findExternalUserAuths(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    List<ExternalUserAuthDataObject> findExternalUserAuths(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findExternalUserAuthKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createExternalUserAuth(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return ExternalUserAuthDataObject?)
    String createExternalUserAuth(ExternalUserAuthDataObject externalUserAuth) throws BaseException;          // Returns Guid.  (Return ExternalUserAuthDataObject?)
    //Boolean updateExternalUserAuth(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateExternalUserAuth(ExternalUserAuthDataObject externalUserAuth) throws BaseException;
    Boolean deleteExternalUserAuth(String guid) throws BaseException;
    Boolean deleteExternalUserAuth(ExternalUserAuthDataObject externalUserAuth) throws BaseException;
    Long deleteExternalUserAuths(String filter, String params, List<String> values) throws BaseException;
}
