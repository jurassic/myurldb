package com.myurldb.ws.dao;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.data.GeoLinkDataObject;

// TBD: Add offset/count to getAllGeoLinks() and findGeoLinks(), etc.
public interface GeoLinkDAO
{
    GeoLinkDataObject getGeoLink(String guid) throws BaseException;
    List<GeoLinkDataObject> getGeoLinks(List<String> guids) throws BaseException;
    List<GeoLinkDataObject> getAllGeoLinks() throws BaseException;
    List<GeoLinkDataObject> getAllGeoLinks(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllGeoLinkKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<GeoLinkDataObject> findGeoLinks(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<GeoLinkDataObject> findGeoLinks(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    List<GeoLinkDataObject> findGeoLinks(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findGeoLinkKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createGeoLink(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return GeoLinkDataObject?)
    String createGeoLink(GeoLinkDataObject geoLink) throws BaseException;          // Returns Guid.  (Return GeoLinkDataObject?)
    //Boolean updateGeoLink(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateGeoLink(GeoLinkDataObject geoLink) throws BaseException;
    Boolean deleteGeoLink(String guid) throws BaseException;
    Boolean deleteGeoLink(GeoLinkDataObject geoLink) throws BaseException;
    Long deleteGeoLinks(String filter, String params, List<String> values) throws BaseException;
}
