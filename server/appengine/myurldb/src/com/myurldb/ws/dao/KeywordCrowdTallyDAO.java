package com.myurldb.ws.dao;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.data.KeywordCrowdTallyDataObject;

// TBD: Add offset/count to getAllKeywordCrowdTallies() and findKeywordCrowdTallies(), etc.
public interface KeywordCrowdTallyDAO
{
    KeywordCrowdTallyDataObject getKeywordCrowdTally(String guid) throws BaseException;
    List<KeywordCrowdTallyDataObject> getKeywordCrowdTallies(List<String> guids) throws BaseException;
    List<KeywordCrowdTallyDataObject> getAllKeywordCrowdTallies() throws BaseException;
    List<KeywordCrowdTallyDataObject> getAllKeywordCrowdTallies(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllKeywordCrowdTallyKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<KeywordCrowdTallyDataObject> findKeywordCrowdTallies(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<KeywordCrowdTallyDataObject> findKeywordCrowdTallies(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    List<KeywordCrowdTallyDataObject> findKeywordCrowdTallies(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findKeywordCrowdTallyKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createKeywordCrowdTally(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return KeywordCrowdTallyDataObject?)
    String createKeywordCrowdTally(KeywordCrowdTallyDataObject keywordCrowdTally) throws BaseException;          // Returns Guid.  (Return KeywordCrowdTallyDataObject?)
    //Boolean updateKeywordCrowdTally(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateKeywordCrowdTally(KeywordCrowdTallyDataObject keywordCrowdTally) throws BaseException;
    Boolean deleteKeywordCrowdTally(String guid) throws BaseException;
    Boolean deleteKeywordCrowdTally(KeywordCrowdTallyDataObject keywordCrowdTally) throws BaseException;
    Long deleteKeywordCrowdTallies(String filter, String params, List<String> values) throws BaseException;
}
