package com.myurldb.ws.dao;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.data.KeywordFolderDataObject;

// TBD: Add offset/count to getAllKeywordFolders() and findKeywordFolders(), etc.
public interface KeywordFolderDAO
{
    KeywordFolderDataObject getKeywordFolder(String guid) throws BaseException;
    List<KeywordFolderDataObject> getKeywordFolders(List<String> guids) throws BaseException;
    List<KeywordFolderDataObject> getAllKeywordFolders() throws BaseException;
    List<KeywordFolderDataObject> getAllKeywordFolders(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllKeywordFolderKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<KeywordFolderDataObject> findKeywordFolders(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<KeywordFolderDataObject> findKeywordFolders(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    List<KeywordFolderDataObject> findKeywordFolders(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findKeywordFolderKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createKeywordFolder(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return KeywordFolderDataObject?)
    String createKeywordFolder(KeywordFolderDataObject keywordFolder) throws BaseException;          // Returns Guid.  (Return KeywordFolderDataObject?)
    //Boolean updateKeywordFolder(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateKeywordFolder(KeywordFolderDataObject keywordFolder) throws BaseException;
    Boolean deleteKeywordFolder(String guid) throws BaseException;
    Boolean deleteKeywordFolder(KeywordFolderDataObject keywordFolder) throws BaseException;
    Long deleteKeywordFolders(String filter, String params, List<String> values) throws BaseException;
}
