package com.myurldb.ws.dao;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.data.KeywordFolderImportDataObject;

// TBD: Add offset/count to getAllKeywordFolderImports() and findKeywordFolderImports(), etc.
public interface KeywordFolderImportDAO
{
    KeywordFolderImportDataObject getKeywordFolderImport(String guid) throws BaseException;
    List<KeywordFolderImportDataObject> getKeywordFolderImports(List<String> guids) throws BaseException;
    List<KeywordFolderImportDataObject> getAllKeywordFolderImports() throws BaseException;
    List<KeywordFolderImportDataObject> getAllKeywordFolderImports(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllKeywordFolderImportKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<KeywordFolderImportDataObject> findKeywordFolderImports(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<KeywordFolderImportDataObject> findKeywordFolderImports(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    List<KeywordFolderImportDataObject> findKeywordFolderImports(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findKeywordFolderImportKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createKeywordFolderImport(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return KeywordFolderImportDataObject?)
    String createKeywordFolderImport(KeywordFolderImportDataObject keywordFolderImport) throws BaseException;          // Returns Guid.  (Return KeywordFolderImportDataObject?)
    //Boolean updateKeywordFolderImport(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateKeywordFolderImport(KeywordFolderImportDataObject keywordFolderImport) throws BaseException;
    Boolean deleteKeywordFolderImport(String guid) throws BaseException;
    Boolean deleteKeywordFolderImport(KeywordFolderImportDataObject keywordFolderImport) throws BaseException;
    Long deleteKeywordFolderImports(String filter, String params, List<String> values) throws BaseException;
}
