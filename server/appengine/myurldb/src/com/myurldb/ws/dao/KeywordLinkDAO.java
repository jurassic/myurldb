package com.myurldb.ws.dao;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.data.KeywordLinkDataObject;

// TBD: Add offset/count to getAllKeywordLinks() and findKeywordLinks(), etc.
public interface KeywordLinkDAO
{
    KeywordLinkDataObject getKeywordLink(String guid) throws BaseException;
    List<KeywordLinkDataObject> getKeywordLinks(List<String> guids) throws BaseException;
    List<KeywordLinkDataObject> getAllKeywordLinks() throws BaseException;
    List<KeywordLinkDataObject> getAllKeywordLinks(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllKeywordLinkKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<KeywordLinkDataObject> findKeywordLinks(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<KeywordLinkDataObject> findKeywordLinks(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    List<KeywordLinkDataObject> findKeywordLinks(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findKeywordLinkKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createKeywordLink(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return KeywordLinkDataObject?)
    String createKeywordLink(KeywordLinkDataObject keywordLink) throws BaseException;          // Returns Guid.  (Return KeywordLinkDataObject?)
    //Boolean updateKeywordLink(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateKeywordLink(KeywordLinkDataObject keywordLink) throws BaseException;
    Boolean deleteKeywordLink(String guid) throws BaseException;
    Boolean deleteKeywordLink(KeywordLinkDataObject keywordLink) throws BaseException;
    Long deleteKeywordLinks(String filter, String params, List<String> values) throws BaseException;
}
