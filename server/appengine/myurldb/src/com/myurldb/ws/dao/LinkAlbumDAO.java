package com.myurldb.ws.dao;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.data.LinkAlbumDataObject;

// TBD: Add offset/count to getAllLinkAlbums() and findLinkAlbums(), etc.
public interface LinkAlbumDAO
{
    LinkAlbumDataObject getLinkAlbum(String guid) throws BaseException;
    List<LinkAlbumDataObject> getLinkAlbums(List<String> guids) throws BaseException;
    List<LinkAlbumDataObject> getAllLinkAlbums() throws BaseException;
    List<LinkAlbumDataObject> getAllLinkAlbums(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllLinkAlbumKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<LinkAlbumDataObject> findLinkAlbums(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<LinkAlbumDataObject> findLinkAlbums(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    List<LinkAlbumDataObject> findLinkAlbums(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findLinkAlbumKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createLinkAlbum(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return LinkAlbumDataObject?)
    String createLinkAlbum(LinkAlbumDataObject linkAlbum) throws BaseException;          // Returns Guid.  (Return LinkAlbumDataObject?)
    //Boolean updateLinkAlbum(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateLinkAlbum(LinkAlbumDataObject linkAlbum) throws BaseException;
    Boolean deleteLinkAlbum(String guid) throws BaseException;
    Boolean deleteLinkAlbum(LinkAlbumDataObject linkAlbum) throws BaseException;
    Long deleteLinkAlbums(String filter, String params, List<String> values) throws BaseException;
}
