package com.myurldb.ws.dao;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.data.LinkMessageDataObject;

// TBD: Add offset/count to getAllLinkMessages() and findLinkMessages(), etc.
public interface LinkMessageDAO
{
    LinkMessageDataObject getLinkMessage(String guid) throws BaseException;
    List<LinkMessageDataObject> getLinkMessages(List<String> guids) throws BaseException;
    List<LinkMessageDataObject> getAllLinkMessages() throws BaseException;
    List<LinkMessageDataObject> getAllLinkMessages(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllLinkMessageKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<LinkMessageDataObject> findLinkMessages(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<LinkMessageDataObject> findLinkMessages(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    List<LinkMessageDataObject> findLinkMessages(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findLinkMessageKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createLinkMessage(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return LinkMessageDataObject?)
    String createLinkMessage(LinkMessageDataObject linkMessage) throws BaseException;          // Returns Guid.  (Return LinkMessageDataObject?)
    //Boolean updateLinkMessage(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateLinkMessage(LinkMessageDataObject linkMessage) throws BaseException;
    Boolean deleteLinkMessage(String guid) throws BaseException;
    Boolean deleteLinkMessage(LinkMessageDataObject linkMessage) throws BaseException;
    Long deleteLinkMessages(String filter, String params, List<String> values) throws BaseException;
}
