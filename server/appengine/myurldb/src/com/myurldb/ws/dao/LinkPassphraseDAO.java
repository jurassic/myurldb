package com.myurldb.ws.dao;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.data.LinkPassphraseDataObject;

// TBD: Add offset/count to getAllLinkPassphrases() and findLinkPassphrases(), etc.
public interface LinkPassphraseDAO
{
    LinkPassphraseDataObject getLinkPassphrase(String guid) throws BaseException;
    List<LinkPassphraseDataObject> getLinkPassphrases(List<String> guids) throws BaseException;
    List<LinkPassphraseDataObject> getAllLinkPassphrases() throws BaseException;
    List<LinkPassphraseDataObject> getAllLinkPassphrases(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllLinkPassphraseKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<LinkPassphraseDataObject> findLinkPassphrases(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<LinkPassphraseDataObject> findLinkPassphrases(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    List<LinkPassphraseDataObject> findLinkPassphrases(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findLinkPassphraseKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createLinkPassphrase(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return LinkPassphraseDataObject?)
    String createLinkPassphrase(LinkPassphraseDataObject linkPassphrase) throws BaseException;          // Returns Guid.  (Return LinkPassphraseDataObject?)
    //Boolean updateLinkPassphrase(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateLinkPassphrase(LinkPassphraseDataObject linkPassphrase) throws BaseException;
    Boolean deleteLinkPassphrase(String guid) throws BaseException;
    Boolean deleteLinkPassphrase(LinkPassphraseDataObject linkPassphrase) throws BaseException;
    Long deleteLinkPassphrases(String filter, String params, List<String> values) throws BaseException;
}
