package com.myurldb.ws.dao;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.data.QrCodeDataObject;

// TBD: Add offset/count to getAllQrCodes() and findQrCodes(), etc.
public interface QrCodeDAO
{
    QrCodeDataObject getQrCode(String guid) throws BaseException;
    List<QrCodeDataObject> getQrCodes(List<String> guids) throws BaseException;
    List<QrCodeDataObject> getAllQrCodes() throws BaseException;
    List<QrCodeDataObject> getAllQrCodes(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllQrCodeKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<QrCodeDataObject> findQrCodes(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<QrCodeDataObject> findQrCodes(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    List<QrCodeDataObject> findQrCodes(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findQrCodeKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createQrCode(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return QrCodeDataObject?)
    String createQrCode(QrCodeDataObject qrCode) throws BaseException;          // Returns Guid.  (Return QrCodeDataObject?)
    //Boolean updateQrCode(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateQrCode(QrCodeDataObject qrCode) throws BaseException;
    Boolean deleteQrCode(String guid) throws BaseException;
    Boolean deleteQrCode(QrCodeDataObject qrCode) throws BaseException;
    Long deleteQrCodes(String filter, String params, List<String> values) throws BaseException;
}
