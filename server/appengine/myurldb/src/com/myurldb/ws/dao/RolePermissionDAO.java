package com.myurldb.ws.dao;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.data.RolePermissionDataObject;

// TBD: Add offset/count to getAllRolePermissions() and findRolePermissions(), etc.
public interface RolePermissionDAO
{
    RolePermissionDataObject getRolePermission(String guid) throws BaseException;
    List<RolePermissionDataObject> getRolePermissions(List<String> guids) throws BaseException;
    List<RolePermissionDataObject> getAllRolePermissions() throws BaseException;
    List<RolePermissionDataObject> getAllRolePermissions(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllRolePermissionKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<RolePermissionDataObject> findRolePermissions(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<RolePermissionDataObject> findRolePermissions(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    List<RolePermissionDataObject> findRolePermissions(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findRolePermissionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createRolePermission(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return RolePermissionDataObject?)
    String createRolePermission(RolePermissionDataObject rolePermission) throws BaseException;          // Returns Guid.  (Return RolePermissionDataObject?)
    //Boolean updateRolePermission(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateRolePermission(RolePermissionDataObject rolePermission) throws BaseException;
    Boolean deleteRolePermission(String guid) throws BaseException;
    Boolean deleteRolePermission(RolePermissionDataObject rolePermission) throws BaseException;
    Long deleteRolePermissions(String filter, String params, List<String> values) throws BaseException;
}
