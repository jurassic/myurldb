package com.myurldb.ws.dao;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.data.ShortLinkDataObject;

// TBD: Add offset/count to getAllShortLinks() and findShortLinks(), etc.
public interface ShortLinkDAO
{
    ShortLinkDataObject getShortLink(String guid) throws BaseException;
    List<ShortLinkDataObject> getShortLinks(List<String> guids) throws BaseException;
    List<ShortLinkDataObject> getAllShortLinks() throws BaseException;
    List<ShortLinkDataObject> getAllShortLinks(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllShortLinkKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<ShortLinkDataObject> findShortLinks(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<ShortLinkDataObject> findShortLinks(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    List<ShortLinkDataObject> findShortLinks(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findShortLinkKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createShortLink(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return ShortLinkDataObject?)
    String createShortLink(ShortLinkDataObject shortLink) throws BaseException;          // Returns Guid.  (Return ShortLinkDataObject?)
    //Boolean updateShortLink(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateShortLink(ShortLinkDataObject shortLink) throws BaseException;
    Boolean deleteShortLink(String guid) throws BaseException;
    Boolean deleteShortLink(ShortLinkDataObject shortLink) throws BaseException;
    Long deleteShortLinks(String filter, String params, List<String> values) throws BaseException;
}
