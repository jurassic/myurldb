package com.myurldb.ws.dao;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.data.ShortPassageDataObject;

// TBD: Add offset/count to getAllShortPassages() and findShortPassages(), etc.
public interface ShortPassageDAO
{
    ShortPassageDataObject getShortPassage(String guid) throws BaseException;
    List<ShortPassageDataObject> getShortPassages(List<String> guids) throws BaseException;
    List<ShortPassageDataObject> getAllShortPassages() throws BaseException;
    List<ShortPassageDataObject> getAllShortPassages(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllShortPassageKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<ShortPassageDataObject> findShortPassages(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<ShortPassageDataObject> findShortPassages(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    List<ShortPassageDataObject> findShortPassages(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findShortPassageKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createShortPassage(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return ShortPassageDataObject?)
    String createShortPassage(ShortPassageDataObject shortPassage) throws BaseException;          // Returns Guid.  (Return ShortPassageDataObject?)
    //Boolean updateShortPassage(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateShortPassage(ShortPassageDataObject shortPassage) throws BaseException;
    Boolean deleteShortPassage(String guid) throws BaseException;
    Boolean deleteShortPassage(ShortPassageDataObject shortPassage) throws BaseException;
    Long deleteShortPassages(String filter, String params, List<String> values) throws BaseException;
}
