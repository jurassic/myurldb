package com.myurldb.ws.dao;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.data.SiteCustomDomainDataObject;

// TBD: Add offset/count to getAllSiteCustomDomains() and findSiteCustomDomains(), etc.
public interface SiteCustomDomainDAO
{
    SiteCustomDomainDataObject getSiteCustomDomain(String guid) throws BaseException;
    List<SiteCustomDomainDataObject> getSiteCustomDomains(List<String> guids) throws BaseException;
    List<SiteCustomDomainDataObject> getAllSiteCustomDomains() throws BaseException;
    List<SiteCustomDomainDataObject> getAllSiteCustomDomains(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllSiteCustomDomainKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<SiteCustomDomainDataObject> findSiteCustomDomains(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<SiteCustomDomainDataObject> findSiteCustomDomains(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    List<SiteCustomDomainDataObject> findSiteCustomDomains(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findSiteCustomDomainKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createSiteCustomDomain(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return SiteCustomDomainDataObject?)
    String createSiteCustomDomain(SiteCustomDomainDataObject siteCustomDomain) throws BaseException;          // Returns Guid.  (Return SiteCustomDomainDataObject?)
    //Boolean updateSiteCustomDomain(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateSiteCustomDomain(SiteCustomDomainDataObject siteCustomDomain) throws BaseException;
    Boolean deleteSiteCustomDomain(String guid) throws BaseException;
    Boolean deleteSiteCustomDomain(SiteCustomDomainDataObject siteCustomDomain) throws BaseException;
    Long deleteSiteCustomDomains(String filter, String params, List<String> values) throws BaseException;
}
