package com.myurldb.ws.dao;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.data.SpeedDialDataObject;

// TBD: Add offset/count to getAllSpeedDials() and findSpeedDials(), etc.
public interface SpeedDialDAO
{
    SpeedDialDataObject getSpeedDial(String guid) throws BaseException;
    List<SpeedDialDataObject> getSpeedDials(List<String> guids) throws BaseException;
    List<SpeedDialDataObject> getAllSpeedDials() throws BaseException;
    List<SpeedDialDataObject> getAllSpeedDials(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllSpeedDialKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<SpeedDialDataObject> findSpeedDials(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<SpeedDialDataObject> findSpeedDials(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    List<SpeedDialDataObject> findSpeedDials(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findSpeedDialKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createSpeedDial(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return SpeedDialDataObject?)
    String createSpeedDial(SpeedDialDataObject speedDial) throws BaseException;          // Returns Guid.  (Return SpeedDialDataObject?)
    //Boolean updateSpeedDial(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateSpeedDial(SpeedDialDataObject speedDial) throws BaseException;
    Boolean deleteSpeedDial(String guid) throws BaseException;
    Boolean deleteSpeedDial(SpeedDialDataObject speedDial) throws BaseException;
    Long deleteSpeedDials(String filter, String params, List<String> values) throws BaseException;
}
