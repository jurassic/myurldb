package com.myurldb.ws.dao;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.data.TwitterAppCardDataObject;

// TBD: Add offset/count to getAllTwitterAppCards() and findTwitterAppCards(), etc.
public interface TwitterAppCardDAO
{
    TwitterAppCardDataObject getTwitterAppCard(String guid) throws BaseException;
    List<TwitterAppCardDataObject> getTwitterAppCards(List<String> guids) throws BaseException;
    List<TwitterAppCardDataObject> getAllTwitterAppCards() throws BaseException;
    List<TwitterAppCardDataObject> getAllTwitterAppCards(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllTwitterAppCardKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<TwitterAppCardDataObject> findTwitterAppCards(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<TwitterAppCardDataObject> findTwitterAppCards(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    List<TwitterAppCardDataObject> findTwitterAppCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findTwitterAppCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createTwitterAppCard(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return TwitterAppCardDataObject?)
    String createTwitterAppCard(TwitterAppCardDataObject twitterAppCard) throws BaseException;          // Returns Guid.  (Return TwitterAppCardDataObject?)
    //Boolean updateTwitterAppCard(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateTwitterAppCard(TwitterAppCardDataObject twitterAppCard) throws BaseException;
    Boolean deleteTwitterAppCard(String guid) throws BaseException;
    Boolean deleteTwitterAppCard(TwitterAppCardDataObject twitterAppCard) throws BaseException;
    Long deleteTwitterAppCards(String filter, String params, List<String> values) throws BaseException;
}
