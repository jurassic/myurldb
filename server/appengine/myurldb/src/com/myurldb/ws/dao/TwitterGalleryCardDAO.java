package com.myurldb.ws.dao;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.data.TwitterGalleryCardDataObject;

// TBD: Add offset/count to getAllTwitterGalleryCards() and findTwitterGalleryCards(), etc.
public interface TwitterGalleryCardDAO
{
    TwitterGalleryCardDataObject getTwitterGalleryCard(String guid) throws BaseException;
    List<TwitterGalleryCardDataObject> getTwitterGalleryCards(List<String> guids) throws BaseException;
    List<TwitterGalleryCardDataObject> getAllTwitterGalleryCards() throws BaseException;
    List<TwitterGalleryCardDataObject> getAllTwitterGalleryCards(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllTwitterGalleryCardKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<TwitterGalleryCardDataObject> findTwitterGalleryCards(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<TwitterGalleryCardDataObject> findTwitterGalleryCards(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    List<TwitterGalleryCardDataObject> findTwitterGalleryCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findTwitterGalleryCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createTwitterGalleryCard(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return TwitterGalleryCardDataObject?)
    String createTwitterGalleryCard(TwitterGalleryCardDataObject twitterGalleryCard) throws BaseException;          // Returns Guid.  (Return TwitterGalleryCardDataObject?)
    //Boolean updateTwitterGalleryCard(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateTwitterGalleryCard(TwitterGalleryCardDataObject twitterGalleryCard) throws BaseException;
    Boolean deleteTwitterGalleryCard(String guid) throws BaseException;
    Boolean deleteTwitterGalleryCard(TwitterGalleryCardDataObject twitterGalleryCard) throws BaseException;
    Long deleteTwitterGalleryCards(String filter, String params, List<String> values) throws BaseException;
}
