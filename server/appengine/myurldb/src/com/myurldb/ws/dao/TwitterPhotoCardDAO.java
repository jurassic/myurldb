package com.myurldb.ws.dao;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.data.TwitterPhotoCardDataObject;

// TBD: Add offset/count to getAllTwitterPhotoCards() and findTwitterPhotoCards(), etc.
public interface TwitterPhotoCardDAO
{
    TwitterPhotoCardDataObject getTwitterPhotoCard(String guid) throws BaseException;
    List<TwitterPhotoCardDataObject> getTwitterPhotoCards(List<String> guids) throws BaseException;
    List<TwitterPhotoCardDataObject> getAllTwitterPhotoCards() throws BaseException;
    List<TwitterPhotoCardDataObject> getAllTwitterPhotoCards(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllTwitterPhotoCardKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<TwitterPhotoCardDataObject> findTwitterPhotoCards(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<TwitterPhotoCardDataObject> findTwitterPhotoCards(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    List<TwitterPhotoCardDataObject> findTwitterPhotoCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findTwitterPhotoCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createTwitterPhotoCard(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return TwitterPhotoCardDataObject?)
    String createTwitterPhotoCard(TwitterPhotoCardDataObject twitterPhotoCard) throws BaseException;          // Returns Guid.  (Return TwitterPhotoCardDataObject?)
    //Boolean updateTwitterPhotoCard(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateTwitterPhotoCard(TwitterPhotoCardDataObject twitterPhotoCard) throws BaseException;
    Boolean deleteTwitterPhotoCard(String guid) throws BaseException;
    Boolean deleteTwitterPhotoCard(TwitterPhotoCardDataObject twitterPhotoCard) throws BaseException;
    Long deleteTwitterPhotoCards(String filter, String params, List<String> values) throws BaseException;
}
