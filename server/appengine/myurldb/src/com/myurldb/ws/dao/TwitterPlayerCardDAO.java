package com.myurldb.ws.dao;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.data.TwitterPlayerCardDataObject;

// TBD: Add offset/count to getAllTwitterPlayerCards() and findTwitterPlayerCards(), etc.
public interface TwitterPlayerCardDAO
{
    TwitterPlayerCardDataObject getTwitterPlayerCard(String guid) throws BaseException;
    List<TwitterPlayerCardDataObject> getTwitterPlayerCards(List<String> guids) throws BaseException;
    List<TwitterPlayerCardDataObject> getAllTwitterPlayerCards() throws BaseException;
    List<TwitterPlayerCardDataObject> getAllTwitterPlayerCards(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllTwitterPlayerCardKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<TwitterPlayerCardDataObject> findTwitterPlayerCards(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<TwitterPlayerCardDataObject> findTwitterPlayerCards(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    List<TwitterPlayerCardDataObject> findTwitterPlayerCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findTwitterPlayerCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createTwitterPlayerCard(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return TwitterPlayerCardDataObject?)
    String createTwitterPlayerCard(TwitterPlayerCardDataObject twitterPlayerCard) throws BaseException;          // Returns Guid.  (Return TwitterPlayerCardDataObject?)
    //Boolean updateTwitterPlayerCard(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateTwitterPlayerCard(TwitterPlayerCardDataObject twitterPlayerCard) throws BaseException;
    Boolean deleteTwitterPlayerCard(String guid) throws BaseException;
    Boolean deleteTwitterPlayerCard(TwitterPlayerCardDataObject twitterPlayerCard) throws BaseException;
    Long deleteTwitterPlayerCards(String filter, String params, List<String> values) throws BaseException;
}
