package com.myurldb.ws.dao;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.data.TwitterProductCardDataObject;

// TBD: Add offset/count to getAllTwitterProductCards() and findTwitterProductCards(), etc.
public interface TwitterProductCardDAO
{
    TwitterProductCardDataObject getTwitterProductCard(String guid) throws BaseException;
    List<TwitterProductCardDataObject> getTwitterProductCards(List<String> guids) throws BaseException;
    List<TwitterProductCardDataObject> getAllTwitterProductCards() throws BaseException;
    List<TwitterProductCardDataObject> getAllTwitterProductCards(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllTwitterProductCardKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<TwitterProductCardDataObject> findTwitterProductCards(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<TwitterProductCardDataObject> findTwitterProductCards(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    List<TwitterProductCardDataObject> findTwitterProductCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findTwitterProductCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createTwitterProductCard(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return TwitterProductCardDataObject?)
    String createTwitterProductCard(TwitterProductCardDataObject twitterProductCard) throws BaseException;          // Returns Guid.  (Return TwitterProductCardDataObject?)
    //Boolean updateTwitterProductCard(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateTwitterProductCard(TwitterProductCardDataObject twitterProductCard) throws BaseException;
    Boolean deleteTwitterProductCard(String guid) throws BaseException;
    Boolean deleteTwitterProductCard(TwitterProductCardDataObject twitterProductCard) throws BaseException;
    Long deleteTwitterProductCards(String filter, String params, List<String> values) throws BaseException;
}
