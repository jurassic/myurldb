package com.myurldb.ws.dao;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.data.TwitterSummaryCardDataObject;

// TBD: Add offset/count to getAllTwitterSummaryCards() and findTwitterSummaryCards(), etc.
public interface TwitterSummaryCardDAO
{
    TwitterSummaryCardDataObject getTwitterSummaryCard(String guid) throws BaseException;
    List<TwitterSummaryCardDataObject> getTwitterSummaryCards(List<String> guids) throws BaseException;
    List<TwitterSummaryCardDataObject> getAllTwitterSummaryCards() throws BaseException;
    List<TwitterSummaryCardDataObject> getAllTwitterSummaryCards(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllTwitterSummaryCardKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<TwitterSummaryCardDataObject> findTwitterSummaryCards(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<TwitterSummaryCardDataObject> findTwitterSummaryCards(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    List<TwitterSummaryCardDataObject> findTwitterSummaryCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findTwitterSummaryCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createTwitterSummaryCard(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return TwitterSummaryCardDataObject?)
    String createTwitterSummaryCard(TwitterSummaryCardDataObject twitterSummaryCard) throws BaseException;          // Returns Guid.  (Return TwitterSummaryCardDataObject?)
    //Boolean updateTwitterSummaryCard(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateTwitterSummaryCard(TwitterSummaryCardDataObject twitterSummaryCard) throws BaseException;
    Boolean deleteTwitterSummaryCard(String guid) throws BaseException;
    Boolean deleteTwitterSummaryCard(TwitterSummaryCardDataObject twitterSummaryCard) throws BaseException;
    Long deleteTwitterSummaryCards(String filter, String params, List<String> values) throws BaseException;
}
