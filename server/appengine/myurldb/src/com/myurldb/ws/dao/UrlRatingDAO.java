package com.myurldb.ws.dao;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.data.UrlRatingDataObject;

// TBD: Add offset/count to getAllUrlRatings() and findUrlRatings(), etc.
public interface UrlRatingDAO
{
    UrlRatingDataObject getUrlRating(String guid) throws BaseException;
    List<UrlRatingDataObject> getUrlRatings(List<String> guids) throws BaseException;
    List<UrlRatingDataObject> getAllUrlRatings() throws BaseException;
    List<UrlRatingDataObject> getAllUrlRatings(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllUrlRatingKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<UrlRatingDataObject> findUrlRatings(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<UrlRatingDataObject> findUrlRatings(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    List<UrlRatingDataObject> findUrlRatings(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findUrlRatingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createUrlRating(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return UrlRatingDataObject?)
    String createUrlRating(UrlRatingDataObject urlRating) throws BaseException;          // Returns Guid.  (Return UrlRatingDataObject?)
    //Boolean updateUrlRating(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateUrlRating(UrlRatingDataObject urlRating) throws BaseException;
    Boolean deleteUrlRating(String guid) throws BaseException;
    Boolean deleteUrlRating(UrlRatingDataObject urlRating) throws BaseException;
    Long deleteUrlRatings(String filter, String params, List<String> values) throws BaseException;
}
