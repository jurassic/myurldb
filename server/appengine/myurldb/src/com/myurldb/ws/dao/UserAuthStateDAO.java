package com.myurldb.ws.dao;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.data.UserAuthStateDataObject;

// TBD: Add offset/count to getAllUserAuthStates() and findUserAuthStates(), etc.
public interface UserAuthStateDAO
{
    UserAuthStateDataObject getUserAuthState(String guid) throws BaseException;
    List<UserAuthStateDataObject> getUserAuthStates(List<String> guids) throws BaseException;
    List<UserAuthStateDataObject> getAllUserAuthStates() throws BaseException;
    List<UserAuthStateDataObject> getAllUserAuthStates(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllUserAuthStateKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<UserAuthStateDataObject> findUserAuthStates(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<UserAuthStateDataObject> findUserAuthStates(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    List<UserAuthStateDataObject> findUserAuthStates(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findUserAuthStateKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createUserAuthState(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return UserAuthStateDataObject?)
    String createUserAuthState(UserAuthStateDataObject userAuthState) throws BaseException;          // Returns Guid.  (Return UserAuthStateDataObject?)
    //Boolean updateUserAuthState(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateUserAuthState(UserAuthStateDataObject userAuthState) throws BaseException;
    Boolean deleteUserAuthState(String guid) throws BaseException;
    Boolean deleteUserAuthState(UserAuthStateDataObject userAuthState) throws BaseException;
    Long deleteUserAuthStates(String filter, String params, List<String> values) throws BaseException;
}
