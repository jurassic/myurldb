package com.myurldb.ws.dao;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.data.UserCustomDomainDataObject;

// TBD: Add offset/count to getAllUserCustomDomains() and findUserCustomDomains(), etc.
public interface UserCustomDomainDAO
{
    UserCustomDomainDataObject getUserCustomDomain(String guid) throws BaseException;
    List<UserCustomDomainDataObject> getUserCustomDomains(List<String> guids) throws BaseException;
    List<UserCustomDomainDataObject> getAllUserCustomDomains() throws BaseException;
    List<UserCustomDomainDataObject> getAllUserCustomDomains(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllUserCustomDomainKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<UserCustomDomainDataObject> findUserCustomDomains(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<UserCustomDomainDataObject> findUserCustomDomains(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    List<UserCustomDomainDataObject> findUserCustomDomains(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findUserCustomDomainKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createUserCustomDomain(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return UserCustomDomainDataObject?)
    String createUserCustomDomain(UserCustomDomainDataObject userCustomDomain) throws BaseException;          // Returns Guid.  (Return UserCustomDomainDataObject?)
    //Boolean updateUserCustomDomain(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateUserCustomDomain(UserCustomDomainDataObject userCustomDomain) throws BaseException;
    Boolean deleteUserCustomDomain(String guid) throws BaseException;
    Boolean deleteUserCustomDomain(UserCustomDomainDataObject userCustomDomain) throws BaseException;
    Long deleteUserCustomDomains(String filter, String params, List<String> values) throws BaseException;
}
