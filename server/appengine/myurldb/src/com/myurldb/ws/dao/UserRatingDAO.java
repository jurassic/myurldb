package com.myurldb.ws.dao;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.data.UserRatingDataObject;

// TBD: Add offset/count to getAllUserRatings() and findUserRatings(), etc.
public interface UserRatingDAO
{
    UserRatingDataObject getUserRating(String guid) throws BaseException;
    List<UserRatingDataObject> getUserRatings(List<String> guids) throws BaseException;
    List<UserRatingDataObject> getAllUserRatings() throws BaseException;
    List<UserRatingDataObject> getAllUserRatings(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllUserRatingKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<UserRatingDataObject> findUserRatings(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<UserRatingDataObject> findUserRatings(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    List<UserRatingDataObject> findUserRatings(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findUserRatingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createUserRating(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return UserRatingDataObject?)
    String createUserRating(UserRatingDataObject userRating) throws BaseException;          // Returns Guid.  (Return UserRatingDataObject?)
    //Boolean updateUserRating(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateUserRating(UserRatingDataObject userRating) throws BaseException;
    Boolean deleteUserRating(String guid) throws BaseException;
    Boolean deleteUserRating(UserRatingDataObject userRating) throws BaseException;
    Long deleteUserRatings(String filter, String params, List<String> values) throws BaseException;
}
