package com.myurldb.ws.dao;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.data.UserResourcePermissionDataObject;

// TBD: Add offset/count to getAllUserResourcePermissions() and findUserResourcePermissions(), etc.
public interface UserResourcePermissionDAO
{
    UserResourcePermissionDataObject getUserResourcePermission(String guid) throws BaseException;
    List<UserResourcePermissionDataObject> getUserResourcePermissions(List<String> guids) throws BaseException;
    List<UserResourcePermissionDataObject> getAllUserResourcePermissions() throws BaseException;
    List<UserResourcePermissionDataObject> getAllUserResourcePermissions(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllUserResourcePermissionKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<UserResourcePermissionDataObject> findUserResourcePermissions(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<UserResourcePermissionDataObject> findUserResourcePermissions(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    List<UserResourcePermissionDataObject> findUserResourcePermissions(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findUserResourcePermissionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createUserResourcePermission(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return UserResourcePermissionDataObject?)
    String createUserResourcePermission(UserResourcePermissionDataObject userResourcePermission) throws BaseException;          // Returns Guid.  (Return UserResourcePermissionDataObject?)
    //Boolean updateUserResourcePermission(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateUserResourcePermission(UserResourcePermissionDataObject userResourcePermission) throws BaseException;
    Boolean deleteUserResourcePermission(String guid) throws BaseException;
    Boolean deleteUserResourcePermission(UserResourcePermissionDataObject userResourcePermission) throws BaseException;
    Long deleteUserResourcePermissions(String filter, String params, List<String> values) throws BaseException;
}
