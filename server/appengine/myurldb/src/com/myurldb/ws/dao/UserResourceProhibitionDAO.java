package com.myurldb.ws.dao;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.data.UserResourceProhibitionDataObject;

// TBD: Add offset/count to getAllUserResourceProhibitions() and findUserResourceProhibitions(), etc.
public interface UserResourceProhibitionDAO
{
    UserResourceProhibitionDataObject getUserResourceProhibition(String guid) throws BaseException;
    List<UserResourceProhibitionDataObject> getUserResourceProhibitions(List<String> guids) throws BaseException;
    List<UserResourceProhibitionDataObject> getAllUserResourceProhibitions() throws BaseException;
    List<UserResourceProhibitionDataObject> getAllUserResourceProhibitions(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllUserResourceProhibitionKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<UserResourceProhibitionDataObject> findUserResourceProhibitions(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<UserResourceProhibitionDataObject> findUserResourceProhibitions(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    List<UserResourceProhibitionDataObject> findUserResourceProhibitions(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findUserResourceProhibitionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createUserResourceProhibition(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return UserResourceProhibitionDataObject?)
    String createUserResourceProhibition(UserResourceProhibitionDataObject userResourceProhibition) throws BaseException;          // Returns Guid.  (Return UserResourceProhibitionDataObject?)
    //Boolean updateUserResourceProhibition(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateUserResourceProhibition(UserResourceProhibitionDataObject userResourceProhibition) throws BaseException;
    Boolean deleteUserResourceProhibition(String guid) throws BaseException;
    Boolean deleteUserResourceProhibition(UserResourceProhibitionDataObject userResourceProhibition) throws BaseException;
    Long deleteUserResourceProhibitions(String filter, String params, List<String> values) throws BaseException;
}
