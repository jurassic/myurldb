package com.myurldb.ws.dao;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.data.UserRoleDataObject;

// TBD: Add offset/count to getAllUserRoles() and findUserRoles(), etc.
public interface UserRoleDAO
{
    UserRoleDataObject getUserRole(String guid) throws BaseException;
    List<UserRoleDataObject> getUserRoles(List<String> guids) throws BaseException;
    List<UserRoleDataObject> getAllUserRoles() throws BaseException;
    List<UserRoleDataObject> getAllUserRoles(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllUserRoleKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<UserRoleDataObject> findUserRoles(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<UserRoleDataObject> findUserRoles(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    List<UserRoleDataObject> findUserRoles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findUserRoleKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createUserRole(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return UserRoleDataObject?)
    String createUserRole(UserRoleDataObject userRole) throws BaseException;          // Returns Guid.  (Return UserRoleDataObject?)
    //Boolean updateUserRole(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateUserRole(UserRoleDataObject userRole) throws BaseException;
    Boolean deleteUserRole(String guid) throws BaseException;
    Boolean deleteUserRole(UserRoleDataObject userRole) throws BaseException;
    Long deleteUserRoles(String filter, String params, List<String> values) throws BaseException;
}
