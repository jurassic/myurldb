package com.myurldb.ws.dao;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.data.UserSettingDataObject;

// TBD: Add offset/count to getAllUserSettings() and findUserSettings(), etc.
public interface UserSettingDAO
{
    UserSettingDataObject getUserSetting(String guid) throws BaseException;
    List<UserSettingDataObject> getUserSettings(List<String> guids) throws BaseException;
    List<UserSettingDataObject> getAllUserSettings() throws BaseException;
    List<UserSettingDataObject> getAllUserSettings(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllUserSettingKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<UserSettingDataObject> findUserSettings(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<UserSettingDataObject> findUserSettings(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    List<UserSettingDataObject> findUserSettings(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findUserSettingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createUserSetting(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return UserSettingDataObject?)
    String createUserSetting(UserSettingDataObject userSetting) throws BaseException;          // Returns Guid.  (Return UserSettingDataObject?)
    //Boolean updateUserSetting(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateUserSetting(UserSettingDataObject userSetting) throws BaseException;
    Boolean deleteUserSetting(String guid) throws BaseException;
    Boolean deleteUserSetting(UserSettingDataObject userSetting) throws BaseException;
    Long deleteUserSettings(String filter, String params, List<String> values) throws BaseException;
}
