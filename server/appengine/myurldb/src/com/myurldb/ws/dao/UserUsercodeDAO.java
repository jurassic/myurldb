package com.myurldb.ws.dao;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.data.UserUsercodeDataObject;

// TBD: Add offset/count to getAllUserUsercodes() and findUserUsercodes(), etc.
public interface UserUsercodeDAO
{
    UserUsercodeDataObject getUserUsercode(String guid) throws BaseException;
    List<UserUsercodeDataObject> getUserUsercodes(List<String> guids) throws BaseException;
    List<UserUsercodeDataObject> getAllUserUsercodes() throws BaseException;
    List<UserUsercodeDataObject> getAllUserUsercodes(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllUserUsercodeKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<UserUsercodeDataObject> findUserUsercodes(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<UserUsercodeDataObject> findUserUsercodes(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    List<UserUsercodeDataObject> findUserUsercodes(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findUserUsercodeKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createUserUsercode(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return UserUsercodeDataObject?)
    String createUserUsercode(UserUsercodeDataObject userUsercode) throws BaseException;          // Returns Guid.  (Return UserUsercodeDataObject?)
    //Boolean updateUserUsercode(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateUserUsercode(UserUsercodeDataObject userUsercode) throws BaseException;
    Boolean deleteUserUsercode(String guid) throws BaseException;
    Boolean deleteUserUsercode(UserUsercodeDataObject userUsercode) throws BaseException;
    Long deleteUserUsercodes(String filter, String params, List<String> values) throws BaseException;
}
