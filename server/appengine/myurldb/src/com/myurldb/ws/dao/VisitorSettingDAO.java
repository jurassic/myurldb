package com.myurldb.ws.dao;

import java.util.List;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.data.VisitorSettingDataObject;

// TBD: Add offset/count to getAllVisitorSettings() and findVisitorSettings(), etc.
public interface VisitorSettingDAO
{
    VisitorSettingDataObject getVisitorSetting(String guid) throws BaseException;
    List<VisitorSettingDataObject> getVisitorSettings(List<String> guids) throws BaseException;
    List<VisitorSettingDataObject> getAllVisitorSettings() throws BaseException;
    List<VisitorSettingDataObject> getAllVisitorSettings(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllVisitorSettingKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<VisitorSettingDataObject> findVisitorSettings(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<VisitorSettingDataObject> findVisitorSettings(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    List<VisitorSettingDataObject> findVisitorSettings(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findVisitorSettingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createVisitorSetting(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return VisitorSettingDataObject?)
    String createVisitorSetting(VisitorSettingDataObject visitorSetting) throws BaseException;          // Returns Guid.  (Return VisitorSettingDataObject?)
    //Boolean updateVisitorSetting(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateVisitorSetting(VisitorSettingDataObject visitorSetting) throws BaseException;
    Boolean deleteVisitorSetting(String guid) throws BaseException;
    Boolean deleteVisitorSetting(VisitorSettingDataObject visitorSetting) throws BaseException;
    Long deleteVisitorSettings(String filter, String params, List<String> values) throws BaseException;
}
