package com.myurldb.ws.dao.base;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

//import com.google.appengine.api.datastore.Key;

import com.myurldb.ws.core.GUID;
import com.myurldb.ws.config.Config;
import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.DataStoreException;
import com.myurldb.ws.exception.ServiceUnavailableException;
import com.myurldb.ws.exception.RequestConflictException;
import com.myurldb.ws.exception.ResourceNotFoundException;
import com.myurldb.ws.exception.NotImplementedException;
import com.myurldb.ws.dao.AlbumShortLinkDAO;
import com.myurldb.ws.data.AlbumShortLinkDataObject;
import com.myurldb.ws.search.gae.AlbumShortLinkIndexBuilder;


public class DefaultAlbumShortLinkDAO extends DefaultDAOBase implements AlbumShortLinkDAO
{
    private static final Logger log = Logger.getLogger(DefaultAlbumShortLinkDAO.class.getName()); 

    // Returns the albumShortLink for the given guid/key.
    // Returns null if none is found in the datastore.
    @Override
    public AlbumShortLinkDataObject getAlbumShortLink(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: guid = " + guid);

        AlbumShortLinkDataObject albumShortLink = null;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            //Transaction tx = pm.currentTransaction();
            try {
                //tx.setOptimistic(true);
                //tx.begin();
                //Key key = AlbumShortLinkDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = AlbumShortLinkDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                albumShortLink = pm.getObjectById(AlbumShortLinkDataObject.class, key);
                //tx.commit();
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve albumShortLink for guid = " + guid, ex);
                throw new ResourceNotFoundException(ex);
            } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        log.finer("END");
	    return albumShortLink;
	}

    @Override
    public List<AlbumShortLinkDataObject> getAlbumShortLinks(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // temporary implementation
        List<AlbumShortLinkDataObject> albumShortLinks = null;
        if(guids != null && !guids.isEmpty()) {
            albumShortLinks = new ArrayList<AlbumShortLinkDataObject>();
            for(String guid : guids) {
                AlbumShortLinkDataObject obj = getAlbumShortLink(guid); 
                albumShortLinks.add(obj);
            }
	    }

        log.finer("END");
        return albumShortLinks;
    }

    @Override
    public List<AlbumShortLinkDataObject> getAllAlbumShortLinks() throws BaseException
	{
	    return getAllAlbumShortLinks(null, null, null);
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<AlbumShortLinkDataObject> getAllAlbumShortLinks(String ordering, Long offset, Integer count) throws BaseException
	{
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count);

    	List<AlbumShortLinkDataObject> albumShortLinks = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(AlbumShortLinkDataObject.class);
            if(ordering != null) {
                q.setOrdering(ordering);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            albumShortLinks = (List<AlbumShortLinkDataObject>) q.execute();
            if(albumShortLinks != null) {
                albumShortLinks.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            /*
            // ???
            Collection<AlbumShortLinkDataObject> rs_albumShortLinks = (Collection<AlbumShortLinkDataObject>) q.execute();
            if(rs_albumShortLinks == null) {
                log.log(Level.WARNING, "Failed to retrieve all albumShortLinks.");
                albumShortLinks = new ArrayList<AlbumShortLinkDataObject>();  // ???           
            } else {
                albumShortLinks = new ArrayList<AlbumShortLinkDataObject>(pm.detachCopyAll(rs_albumShortLinks));
            }
            */
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all albumShortLinks.", ex);
            //albumShortLinks = new ArrayList<AlbumShortLinkDataObject>();  // ???
            throw new DataStoreException("Failed to retrieve all albumShortLinks.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return albumShortLinks;
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<String> getAllAlbumShortLinkKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count);

    	List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery("select key from " + AlbumShortLinkDataObject.class.getName());  // ???
            if(ordering != null) {
                q.setOrdering(ordering);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            keys = (List<String>) q.execute();
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all AlbumShortLink keys.", ex);
            throw new DataStoreException("Failed to retrieve all AlbumShortLink keys.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return keys;
    }

    @Override
	public List<AlbumShortLinkDataObject> findAlbumShortLinks(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findAlbumShortLinks(filter, ordering, params, values, null, null);
    }

    @Override
	public List<AlbumShortLinkDataObject> findAlbumShortLinks(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findAlbumShortLinks(filter, ordering, params, values, null, null, offset, count);
	}

    @SuppressWarnings("unchecked")
    @Override
	public List<AlbumShortLinkDataObject> findAlbumShortLinks(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
        if(log.isLoggable(Level.INFO)) log.info("DefaultAlbumShortLinkDAO.findAlbumShortLinks(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("findAlbumShortLinks() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        List<AlbumShortLinkDataObject> albumShortLinks = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(AlbumShortLinkDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                albumShortLinks = (List<AlbumShortLinkDataObject>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                albumShortLinks = (List<AlbumShortLinkDataObject>) q.execute();
            }
            if(albumShortLinks != null) {
                albumShortLinks.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //while($TypeUtil.deCapitalizePluralize($typeName).iterator().hasNext()) {
            //    for(AlbumShortLinkDataObject dobj : albumShortLinks) {
            //        //
            //    }
            //}
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find albumShortLinks because index is missing.", ex);
            //albumShortLinks = new ArrayList<AlbumShortLinkDataObject>();  // ???
            throw new DataStoreException("Failed to find albumShortLinks because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find albumShortLinks meeting the criterion.", ex);
            //albumShortLinks = new ArrayList<AlbumShortLinkDataObject>();  // ???
            throw new DataStoreException("Failed to find albumShortLinks meeting the criterion.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return albumShortLinks;
	}

    @SuppressWarnings("unchecked")
    @Override
    public List<String> findAlbumShortLinkKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultAlbumShortLinkDAO.findAlbumShortLinkKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery("select key from " + AlbumShortLinkDataObject.class.getName());  // ???
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                keys = (List<String>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                keys = (List<String>) q.execute();
            }
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find AlbumShortLink keys because index is missing.", ex);
            throw new DataStoreException("Failed to find AlbumShortLink keys because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find AlbumShortLink keys meeting the criterion.", ex);
            throw new DataStoreException("Failed to find AlbumShortLink keys meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END");
        return keys;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultAlbumShortLinkDAO.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("getCount() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        Long count = 0L; 
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(AlbumShortLinkDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(aggregate == null || aggregate.isEmpty()) {
                aggregate = "count(this)";  // By default, count. Other examples: "max(this.createdTime)", avg, sum, etc. (Note: These are currently not supported by Google App Engine.)
            }
            q.setResult(aggregate);
            //q.setResultClass(Long.class);
            // For now, we only support a single aggregate function. (Eg, no comma-separated list.)
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                count = (Long) q.execute();
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to get albumShortLink count because index is missing.", ex);
            throw new DataStoreException("Failed to get albumShortLink count because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to get albumShortLink count meeting the criterion.", ex);
            throw new DataStoreException("Failed to get albumShortLink count meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(count == null) {
            count = 0L;
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

	// Stores the albumShortLink in the datastore and returns its guid.
	// Returns null if the operation fails.
    private String storeAlbumShortLink(AlbumShortLinkDataObject albumShortLink) throws BaseException
    {
        log.fine("storeAlbumShortLink() called.");

        String guid = null;
        PersistenceManager pm = getPersistenceManager();
        try {
            // TBD: If the object on the datastore has non-null created date and the arg shortLink has no created date timestamp,
            //      do not overwrite the timestamp. How to do that???
            // For now, this is ok since we use "overwrite/replace" semantics (not partial update).
            Long createdTime = albumShortLink.getCreatedTime();
            if(createdTime == null) {
                createdTime = System.currentTimeMillis();
                albumShortLink.setCreatedTime(createdTime);
            }
            Long modifiedTime = albumShortLink.getModifiedTime();
            if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
                albumShortLink.setModifiedTime(createdTime);
            }
            pm.makePersistent(albumShortLink); 
            // TBD: How do you know the makePersistent() call was successful???
            guid = albumShortLink.getGuid();
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to store albumShortLink because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to store albumShortLink because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to store albumShortLink.", ex);
            throw new DataStoreException("Failed to store albumShortLink.", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

	    // TBD:
	    if(Config.getInstance().isSearchEnabled()) {
            log.fine("Calling addDocument()... for AlbumShortLink.");
	    	AlbumShortLinkIndexBuilder builder = new AlbumShortLinkIndexBuilder();
	    	builder.addDocument(albumShortLink);
	    }

        if(log.isLoggable(Level.FINE)) log.fine("storeAlbumShortLink(): guid = " + guid);
        return guid;
    }

    @Override
    public String createAlbumShortLink(AlbumShortLinkDataObject albumShortLink) throws BaseException
    {
        // The createdTime field will be automatically set in storeAlbumShortLink().
        //Long createdTime = albumShortLink.getCreatedTime();
        //if(createdTime == null) {
        //    createdTime = System.currentTimeMillis();
        //    albumShortLink.setCreatedTime(createdTime);
        //}
        //Long modifiedTime = albumShortLink.getModifiedTime();
        //if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
        //    albumShortLink.setModifiedTime(createdTime);
        //}
        return storeAlbumShortLink(albumShortLink);
    }

    @Override
	public Boolean updateAlbumShortLink(AlbumShortLinkDataObject albumShortLink) throws BaseException
	{
	    // Modify the modifiedTime timestamp.
	    // CreatedTime should, normally, be non-null.
	    // If null, however, it will be automatically set in storeAlbumShortLink()
	    // (in which case modifiedTime might be updated again).
	    albumShortLink.setModifiedTime(System.currentTimeMillis());
	    String guid = storeAlbumShortLink(albumShortLink);
	    if(guid != null) {
	        return true;
	    } else {
	        return false;
	    }
	}
	
    @Override
    public Boolean deleteAlbumShortLink(AlbumShortLinkDataObject albumShortLink) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        PersistenceManager pm = getPersistenceManager();
        try {
            pm.deletePersistent(albumShortLink);
            // TBD: How do you know the deletePersistent() call was successful???
            suc = true;
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to delete albumShortLink because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete albumShortLink because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete albumShortLink.", ex);
            throw new DataStoreException("Failed to delete albumShortLink.", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

	    // TBD:
	    if(Config.getInstance().isSearchEnabled()) {
            log.fine("Calling removeDocument()... for AlbumShortLink.");
	    	AlbumShortLinkIndexBuilder builder = new AlbumShortLinkIndexBuilder();
	    	builder.removeDocument(albumShortLink.getGuid());
	    }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteAlbumShortLink(String guid) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            try {
                //Key key = AlbumShortLinkDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = AlbumShortLinkDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                AlbumShortLinkDataObject albumShortLink = pm.getObjectById(AlbumShortLinkDataObject.class, key);
                pm.deletePersistent(albumShortLink);
                // TBD: How do you know the deletePersistent() call was successful???
                suc = true;
            } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
                // Datastore is read-only, possibly due to the Google App Engine maintenance.
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete albumShortLink because the datastore is currently read-only. guid = " + guid, ex);
                throw new ServiceUnavailableException("Failed to delete albumShortLink because the datastore is currently read-only.", ex);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete albumShortLink for guid = " + guid, ex);
                throw new DataStoreException("Failed to delete albumShortLink for guid = " + guid, ex);
            } finally {
                try {
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

	    // TBD:
	    if(Config.getInstance().isSearchEnabled()) {
            log.fine("Calling removeDocument()... for AlbumShortLink.");
	    	AlbumShortLinkIndexBuilder builder = new AlbumShortLinkIndexBuilder();
	    	builder.removeDocument(guid);
	    }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
	}

    @Override
    public Long deleteAlbumShortLinks(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultAlbumShortLinkDAO.deleteAlbumShortLinks(): filter=" + filter + "; params=" + params);

        // TBD: Delete-by-query requests should be carefully checked because of its potential harms.
        if(filter == null || filter.isEmpty()) {
            throw new RequestConflictException("deleteAll() without filter is not supported for performance/safety reasons");
        }

        Long count = 0L;
        PersistenceManager pm = getPersistenceManager();
        try {
            Query q = pm.newQuery(AlbumShortLinkDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.deletePersistentAll(values.toArray(new Object[0]));
            } else {
                count = (Long) q.deletePersistentAll();
            }
            // TBD: How do you know the deletePersistent() call was successful???
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to deletealbumShortLinks because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete albumShortLinks because the datastore is currently read-only.", ex);
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to delete albumShortLinks because index is missing", ex);
            throw new DataStoreException("Failed to delete albumShortLinks because index is missing", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete albumShortLinks", ex);
            throw new DataStoreException("Failed to delete albumShortLinks", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

	    // TBD:
	    //if(Config.getInstance().isSearchEnabled()) {
        //    log.fine("Calling removeDocument()... for AlbumShortLink.");
	    //    AlbumShortLinkIndexBuilder builder = new AlbumShortLinkIndexBuilder();
	    //    builder.removeDocument(guids);
	    //}

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
