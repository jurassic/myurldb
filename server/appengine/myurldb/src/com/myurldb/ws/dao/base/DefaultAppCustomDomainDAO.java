package com.myurldb.ws.dao.base;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

//import com.google.appengine.api.datastore.Key;

import com.myurldb.ws.core.GUID;
import com.myurldb.ws.config.Config;
import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.DataStoreException;
import com.myurldb.ws.exception.ServiceUnavailableException;
import com.myurldb.ws.exception.RequestConflictException;
import com.myurldb.ws.exception.ResourceNotFoundException;
import com.myurldb.ws.exception.NotImplementedException;
import com.myurldb.ws.dao.AppCustomDomainDAO;
import com.myurldb.ws.data.AppCustomDomainDataObject;


public class DefaultAppCustomDomainDAO extends DefaultDAOBase implements AppCustomDomainDAO
{
    private static final Logger log = Logger.getLogger(DefaultAppCustomDomainDAO.class.getName()); 

    // Returns the appCustomDomain for the given guid/key.
    // Returns null if none is found in the datastore.
    @Override
    public AppCustomDomainDataObject getAppCustomDomain(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: guid = " + guid);

        AppCustomDomainDataObject appCustomDomain = null;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            //Transaction tx = pm.currentTransaction();
            try {
                //tx.setOptimistic(true);
                //tx.begin();
                //Key key = AppCustomDomainDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = AppCustomDomainDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                appCustomDomain = pm.getObjectById(AppCustomDomainDataObject.class, key);
                //tx.commit();
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve appCustomDomain for guid = " + guid, ex);
                throw new ResourceNotFoundException(ex);
            } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        log.finer("END");
	    return appCustomDomain;
	}

    @Override
    public List<AppCustomDomainDataObject> getAppCustomDomains(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // temporary implementation
        List<AppCustomDomainDataObject> appCustomDomains = null;
        if(guids != null && !guids.isEmpty()) {
            appCustomDomains = new ArrayList<AppCustomDomainDataObject>();
            for(String guid : guids) {
                AppCustomDomainDataObject obj = getAppCustomDomain(guid); 
                appCustomDomains.add(obj);
            }
	    }

        log.finer("END");
        return appCustomDomains;
    }

    @Override
    public List<AppCustomDomainDataObject> getAllAppCustomDomains() throws BaseException
	{
	    return getAllAppCustomDomains(null, null, null);
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<AppCustomDomainDataObject> getAllAppCustomDomains(String ordering, Long offset, Integer count) throws BaseException
	{
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count);

    	List<AppCustomDomainDataObject> appCustomDomains = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(AppCustomDomainDataObject.class);
            if(ordering != null) {
                q.setOrdering(ordering);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            appCustomDomains = (List<AppCustomDomainDataObject>) q.execute();
            if(appCustomDomains != null) {
                appCustomDomains.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            /*
            // ???
            Collection<AppCustomDomainDataObject> rs_appCustomDomains = (Collection<AppCustomDomainDataObject>) q.execute();
            if(rs_appCustomDomains == null) {
                log.log(Level.WARNING, "Failed to retrieve all appCustomDomains.");
                appCustomDomains = new ArrayList<AppCustomDomainDataObject>();  // ???           
            } else {
                appCustomDomains = new ArrayList<AppCustomDomainDataObject>(pm.detachCopyAll(rs_appCustomDomains));
            }
            */
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all appCustomDomains.", ex);
            //appCustomDomains = new ArrayList<AppCustomDomainDataObject>();  // ???
            throw new DataStoreException("Failed to retrieve all appCustomDomains.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return appCustomDomains;
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<String> getAllAppCustomDomainKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count);

    	List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery("select key from " + AppCustomDomainDataObject.class.getName());  // ???
            if(ordering != null) {
                q.setOrdering(ordering);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            keys = (List<String>) q.execute();
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all AppCustomDomain keys.", ex);
            throw new DataStoreException("Failed to retrieve all AppCustomDomain keys.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return keys;
    }

    @Override
	public List<AppCustomDomainDataObject> findAppCustomDomains(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findAppCustomDomains(filter, ordering, params, values, null, null);
    }

    @Override
	public List<AppCustomDomainDataObject> findAppCustomDomains(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findAppCustomDomains(filter, ordering, params, values, null, null, offset, count);
	}

    @SuppressWarnings("unchecked")
    @Override
	public List<AppCustomDomainDataObject> findAppCustomDomains(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
        if(log.isLoggable(Level.INFO)) log.info("DefaultAppCustomDomainDAO.findAppCustomDomains(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("findAppCustomDomains() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        List<AppCustomDomainDataObject> appCustomDomains = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(AppCustomDomainDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                appCustomDomains = (List<AppCustomDomainDataObject>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                appCustomDomains = (List<AppCustomDomainDataObject>) q.execute();
            }
            if(appCustomDomains != null) {
                appCustomDomains.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //while($TypeUtil.deCapitalizePluralize($typeName).iterator().hasNext()) {
            //    for(AppCustomDomainDataObject dobj : appCustomDomains) {
            //        //
            //    }
            //}
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find appCustomDomains because index is missing.", ex);
            //appCustomDomains = new ArrayList<AppCustomDomainDataObject>();  // ???
            throw new DataStoreException("Failed to find appCustomDomains because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find appCustomDomains meeting the criterion.", ex);
            //appCustomDomains = new ArrayList<AppCustomDomainDataObject>();  // ???
            throw new DataStoreException("Failed to find appCustomDomains meeting the criterion.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return appCustomDomains;
	}

    @SuppressWarnings("unchecked")
    @Override
    public List<String> findAppCustomDomainKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultAppCustomDomainDAO.findAppCustomDomainKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery("select key from " + AppCustomDomainDataObject.class.getName());  // ???
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                keys = (List<String>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                keys = (List<String>) q.execute();
            }
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find AppCustomDomain keys because index is missing.", ex);
            throw new DataStoreException("Failed to find AppCustomDomain keys because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find AppCustomDomain keys meeting the criterion.", ex);
            throw new DataStoreException("Failed to find AppCustomDomain keys meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END");
        return keys;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultAppCustomDomainDAO.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("getCount() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        Long count = 0L; 
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(AppCustomDomainDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(aggregate == null || aggregate.isEmpty()) {
                aggregate = "count(this)";  // By default, count. Other examples: "max(this.createdTime)", avg, sum, etc. (Note: These are currently not supported by Google App Engine.)
            }
            q.setResult(aggregate);
            //q.setResultClass(Long.class);
            // For now, we only support a single aggregate function. (Eg, no comma-separated list.)
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                count = (Long) q.execute();
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to get appCustomDomain count because index is missing.", ex);
            throw new DataStoreException("Failed to get appCustomDomain count because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to get appCustomDomain count meeting the criterion.", ex);
            throw new DataStoreException("Failed to get appCustomDomain count meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(count == null) {
            count = 0L;
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

	// Stores the appCustomDomain in the datastore and returns its guid.
	// Returns null if the operation fails.
    private String storeAppCustomDomain(AppCustomDomainDataObject appCustomDomain) throws BaseException
    {
        log.fine("storeAppCustomDomain() called.");

        String guid = null;
        PersistenceManager pm = getPersistenceManager();
        try {
            // TBD: If the object on the datastore has non-null created date and the arg shortLink has no created date timestamp,
            //      do not overwrite the timestamp. How to do that???
            // For now, this is ok since we use "overwrite/replace" semantics (not partial update).
            Long createdTime = appCustomDomain.getCreatedTime();
            if(createdTime == null) {
                createdTime = System.currentTimeMillis();
                appCustomDomain.setCreatedTime(createdTime);
            }
            Long modifiedTime = appCustomDomain.getModifiedTime();
            if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
                appCustomDomain.setModifiedTime(createdTime);
            }
            pm.makePersistent(appCustomDomain); 
            // TBD: How do you know the makePersistent() call was successful???
            guid = appCustomDomain.getGuid();
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to store appCustomDomain because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to store appCustomDomain because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to store appCustomDomain.", ex);
            throw new DataStoreException("Failed to store appCustomDomain.", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINE)) log.fine("storeAppCustomDomain(): guid = " + guid);
        return guid;
    }

    @Override
    public String createAppCustomDomain(AppCustomDomainDataObject appCustomDomain) throws BaseException
    {
        // The createdTime field will be automatically set in storeAppCustomDomain().
        //Long createdTime = appCustomDomain.getCreatedTime();
        //if(createdTime == null) {
        //    createdTime = System.currentTimeMillis();
        //    appCustomDomain.setCreatedTime(createdTime);
        //}
        //Long modifiedTime = appCustomDomain.getModifiedTime();
        //if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
        //    appCustomDomain.setModifiedTime(createdTime);
        //}
        return storeAppCustomDomain(appCustomDomain);
    }

    @Override
	public Boolean updateAppCustomDomain(AppCustomDomainDataObject appCustomDomain) throws BaseException
	{
	    // Modify the modifiedTime timestamp.
	    // CreatedTime should, normally, be non-null.
	    // If null, however, it will be automatically set in storeAppCustomDomain()
	    // (in which case modifiedTime might be updated again).
	    appCustomDomain.setModifiedTime(System.currentTimeMillis());
	    String guid = storeAppCustomDomain(appCustomDomain);
	    if(guid != null) {
	        return true;
	    } else {
	        return false;
	    }
	}
	
    @Override
    public Boolean deleteAppCustomDomain(AppCustomDomainDataObject appCustomDomain) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        PersistenceManager pm = getPersistenceManager();
        try {
            pm.deletePersistent(appCustomDomain);
            // TBD: How do you know the deletePersistent() call was successful???
            suc = true;
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to delete appCustomDomain because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete appCustomDomain because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete appCustomDomain.", ex);
            throw new DataStoreException("Failed to delete appCustomDomain.", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteAppCustomDomain(String guid) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            try {
                //Key key = AppCustomDomainDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = AppCustomDomainDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                AppCustomDomainDataObject appCustomDomain = pm.getObjectById(AppCustomDomainDataObject.class, key);
                pm.deletePersistent(appCustomDomain);
                // TBD: How do you know the deletePersistent() call was successful???
                suc = true;
            } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
                // Datastore is read-only, possibly due to the Google App Engine maintenance.
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete appCustomDomain because the datastore is currently read-only. guid = " + guid, ex);
                throw new ServiceUnavailableException("Failed to delete appCustomDomain because the datastore is currently read-only.", ex);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete appCustomDomain for guid = " + guid, ex);
                throw new DataStoreException("Failed to delete appCustomDomain for guid = " + guid, ex);
            } finally {
                try {
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
	}

    @Override
    public Long deleteAppCustomDomains(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultAppCustomDomainDAO.deleteAppCustomDomains(): filter=" + filter + "; params=" + params);

        // TBD: Delete-by-query requests should be carefully checked because of its potential harms.
        if(filter == null || filter.isEmpty()) {
            throw new RequestConflictException("deleteAll() without filter is not supported for performance/safety reasons");
        }

        Long count = 0L;
        PersistenceManager pm = getPersistenceManager();
        try {
            Query q = pm.newQuery(AppCustomDomainDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.deletePersistentAll(values.toArray(new Object[0]));
            } else {
                count = (Long) q.deletePersistentAll();
            }
            // TBD: How do you know the deletePersistent() call was successful???
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to deleteappCustomDomains because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete appCustomDomains because the datastore is currently read-only.", ex);
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to delete appCustomDomains because index is missing", ex);
            throw new DataStoreException("Failed to delete appCustomDomains because index is missing", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete appCustomDomains", ex);
            throw new DataStoreException("Failed to delete appCustomDomains", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
