package com.myurldb.ws.dao.base;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

//import com.google.appengine.api.datastore.Key;

import com.myurldb.ws.core.GUID;
import com.myurldb.ws.config.Config;
import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.DataStoreException;
import com.myurldb.ws.exception.ServiceUnavailableException;
import com.myurldb.ws.exception.RequestConflictException;
import com.myurldb.ws.exception.ResourceNotFoundException;
import com.myurldb.ws.exception.NotImplementedException;
import com.myurldb.ws.dao.BookmarkLinkDAO;
import com.myurldb.ws.data.BookmarkLinkDataObject;
import com.myurldb.ws.search.gae.BookmarkLinkIndexBuilder;


public class DefaultBookmarkLinkDAO extends DefaultDAOBase implements BookmarkLinkDAO
{
    private static final Logger log = Logger.getLogger(DefaultBookmarkLinkDAO.class.getName()); 

    // Returns the bookmarkLink for the given guid/key.
    // Returns null if none is found in the datastore.
    @Override
    public BookmarkLinkDataObject getBookmarkLink(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: guid = " + guid);

        BookmarkLinkDataObject bookmarkLink = null;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            //Transaction tx = pm.currentTransaction();
            try {
                //tx.setOptimistic(true);
                //tx.begin();
                //Key key = BookmarkLinkDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = BookmarkLinkDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                bookmarkLink = pm.getObjectById(BookmarkLinkDataObject.class, key);
                //tx.commit();
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve bookmarkLink for guid = " + guid, ex);
                throw new ResourceNotFoundException(ex);
            } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        log.finer("END");
	    return bookmarkLink;
	}

    @Override
    public List<BookmarkLinkDataObject> getBookmarkLinks(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // temporary implementation
        List<BookmarkLinkDataObject> bookmarkLinks = null;
        if(guids != null && !guids.isEmpty()) {
            bookmarkLinks = new ArrayList<BookmarkLinkDataObject>();
            for(String guid : guids) {
                BookmarkLinkDataObject obj = getBookmarkLink(guid); 
                bookmarkLinks.add(obj);
            }
	    }

        log.finer("END");
        return bookmarkLinks;
    }

    @Override
    public List<BookmarkLinkDataObject> getAllBookmarkLinks() throws BaseException
	{
	    return getAllBookmarkLinks(null, null, null);
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<BookmarkLinkDataObject> getAllBookmarkLinks(String ordering, Long offset, Integer count) throws BaseException
	{
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count);

    	List<BookmarkLinkDataObject> bookmarkLinks = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(BookmarkLinkDataObject.class);
            if(ordering != null) {
                q.setOrdering(ordering);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            bookmarkLinks = (List<BookmarkLinkDataObject>) q.execute();
            if(bookmarkLinks != null) {
                bookmarkLinks.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            /*
            // ???
            Collection<BookmarkLinkDataObject> rs_bookmarkLinks = (Collection<BookmarkLinkDataObject>) q.execute();
            if(rs_bookmarkLinks == null) {
                log.log(Level.WARNING, "Failed to retrieve all bookmarkLinks.");
                bookmarkLinks = new ArrayList<BookmarkLinkDataObject>();  // ???           
            } else {
                bookmarkLinks = new ArrayList<BookmarkLinkDataObject>(pm.detachCopyAll(rs_bookmarkLinks));
            }
            */
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all bookmarkLinks.", ex);
            //bookmarkLinks = new ArrayList<BookmarkLinkDataObject>();  // ???
            throw new DataStoreException("Failed to retrieve all bookmarkLinks.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return bookmarkLinks;
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<String> getAllBookmarkLinkKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count);

    	List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery("select key from " + BookmarkLinkDataObject.class.getName());  // ???
            if(ordering != null) {
                q.setOrdering(ordering);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            keys = (List<String>) q.execute();
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all BookmarkLink keys.", ex);
            throw new DataStoreException("Failed to retrieve all BookmarkLink keys.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return keys;
    }

    @Override
	public List<BookmarkLinkDataObject> findBookmarkLinks(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findBookmarkLinks(filter, ordering, params, values, null, null);
    }

    @Override
	public List<BookmarkLinkDataObject> findBookmarkLinks(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findBookmarkLinks(filter, ordering, params, values, null, null, offset, count);
	}

    @SuppressWarnings("unchecked")
    @Override
	public List<BookmarkLinkDataObject> findBookmarkLinks(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
        if(log.isLoggable(Level.INFO)) log.info("DefaultBookmarkLinkDAO.findBookmarkLinks(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("findBookmarkLinks() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        List<BookmarkLinkDataObject> bookmarkLinks = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(BookmarkLinkDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                bookmarkLinks = (List<BookmarkLinkDataObject>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                bookmarkLinks = (List<BookmarkLinkDataObject>) q.execute();
            }
            if(bookmarkLinks != null) {
                bookmarkLinks.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //while($TypeUtil.deCapitalizePluralize($typeName).iterator().hasNext()) {
            //    for(BookmarkLinkDataObject dobj : bookmarkLinks) {
            //        //
            //    }
            //}
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find bookmarkLinks because index is missing.", ex);
            //bookmarkLinks = new ArrayList<BookmarkLinkDataObject>();  // ???
            throw new DataStoreException("Failed to find bookmarkLinks because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find bookmarkLinks meeting the criterion.", ex);
            //bookmarkLinks = new ArrayList<BookmarkLinkDataObject>();  // ???
            throw new DataStoreException("Failed to find bookmarkLinks meeting the criterion.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return bookmarkLinks;
	}

    @SuppressWarnings("unchecked")
    @Override
    public List<String> findBookmarkLinkKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultBookmarkLinkDAO.findBookmarkLinkKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery("select key from " + BookmarkLinkDataObject.class.getName());  // ???
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                keys = (List<String>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                keys = (List<String>) q.execute();
            }
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find BookmarkLink keys because index is missing.", ex);
            throw new DataStoreException("Failed to find BookmarkLink keys because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find BookmarkLink keys meeting the criterion.", ex);
            throw new DataStoreException("Failed to find BookmarkLink keys meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END");
        return keys;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultBookmarkLinkDAO.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("getCount() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        Long count = 0L; 
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(BookmarkLinkDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(aggregate == null || aggregate.isEmpty()) {
                aggregate = "count(this)";  // By default, count. Other examples: "max(this.createdTime)", avg, sum, etc. (Note: These are currently not supported by Google App Engine.)
            }
            q.setResult(aggregate);
            //q.setResultClass(Long.class);
            // For now, we only support a single aggregate function. (Eg, no comma-separated list.)
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                count = (Long) q.execute();
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to get bookmarkLink count because index is missing.", ex);
            throw new DataStoreException("Failed to get bookmarkLink count because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to get bookmarkLink count meeting the criterion.", ex);
            throw new DataStoreException("Failed to get bookmarkLink count meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(count == null) {
            count = 0L;
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

	// Stores the bookmarkLink in the datastore and returns its guid.
	// Returns null if the operation fails.
    private String storeBookmarkLink(BookmarkLinkDataObject bookmarkLink) throws BaseException
    {
        log.fine("storeBookmarkLink() called.");

        String guid = null;
        PersistenceManager pm = getPersistenceManager();
        try {
            // TBD: If the object on the datastore has non-null created date and the arg shortLink has no created date timestamp,
            //      do not overwrite the timestamp. How to do that???
            // For now, this is ok since we use "overwrite/replace" semantics (not partial update).
            Long createdTime = bookmarkLink.getCreatedTime();
            if(createdTime == null) {
                createdTime = System.currentTimeMillis();
                bookmarkLink.setCreatedTime(createdTime);
            }
            Long modifiedTime = bookmarkLink.getModifiedTime();
            if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
                bookmarkLink.setModifiedTime(createdTime);
            }
            pm.makePersistent(bookmarkLink); 
            // TBD: How do you know the makePersistent() call was successful???
            guid = bookmarkLink.getGuid();
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to store bookmarkLink because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to store bookmarkLink because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to store bookmarkLink.", ex);
            throw new DataStoreException("Failed to store bookmarkLink.", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

	    // TBD:
	    if(Config.getInstance().isSearchEnabled()) {
            log.fine("Calling addDocument()... for BookmarkLink.");
	    	BookmarkLinkIndexBuilder builder = new BookmarkLinkIndexBuilder();
	    	builder.addDocument(bookmarkLink);
	    }

        if(log.isLoggable(Level.FINE)) log.fine("storeBookmarkLink(): guid = " + guid);
        return guid;
    }

    @Override
    public String createBookmarkLink(BookmarkLinkDataObject bookmarkLink) throws BaseException
    {
        // The createdTime field will be automatically set in storeBookmarkLink().
        //Long createdTime = bookmarkLink.getCreatedTime();
        //if(createdTime == null) {
        //    createdTime = System.currentTimeMillis();
        //    bookmarkLink.setCreatedTime(createdTime);
        //}
        //Long modifiedTime = bookmarkLink.getModifiedTime();
        //if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
        //    bookmarkLink.setModifiedTime(createdTime);
        //}
        return storeBookmarkLink(bookmarkLink);
    }

    @Override
	public Boolean updateBookmarkLink(BookmarkLinkDataObject bookmarkLink) throws BaseException
	{
	    // Modify the modifiedTime timestamp.
	    // CreatedTime should, normally, be non-null.
	    // If null, however, it will be automatically set in storeBookmarkLink()
	    // (in which case modifiedTime might be updated again).
	    bookmarkLink.setModifiedTime(System.currentTimeMillis());
	    String guid = storeBookmarkLink(bookmarkLink);
	    if(guid != null) {
	        return true;
	    } else {
	        return false;
	    }
	}
	
    @Override
    public Boolean deleteBookmarkLink(BookmarkLinkDataObject bookmarkLink) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        PersistenceManager pm = getPersistenceManager();
        try {
            pm.deletePersistent(bookmarkLink);
            // TBD: How do you know the deletePersistent() call was successful???
            suc = true;
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to delete bookmarkLink because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete bookmarkLink because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete bookmarkLink.", ex);
            throw new DataStoreException("Failed to delete bookmarkLink.", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

	    // TBD:
	    if(Config.getInstance().isSearchEnabled()) {
            log.fine("Calling removeDocument()... for BookmarkLink.");
	    	BookmarkLinkIndexBuilder builder = new BookmarkLinkIndexBuilder();
	    	builder.removeDocument(bookmarkLink.getGuid());
	    }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteBookmarkLink(String guid) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            try {
                //Key key = BookmarkLinkDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = BookmarkLinkDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                BookmarkLinkDataObject bookmarkLink = pm.getObjectById(BookmarkLinkDataObject.class, key);
                pm.deletePersistent(bookmarkLink);
                // TBD: How do you know the deletePersistent() call was successful???
                suc = true;
            } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
                // Datastore is read-only, possibly due to the Google App Engine maintenance.
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete bookmarkLink because the datastore is currently read-only. guid = " + guid, ex);
                throw new ServiceUnavailableException("Failed to delete bookmarkLink because the datastore is currently read-only.", ex);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete bookmarkLink for guid = " + guid, ex);
                throw new DataStoreException("Failed to delete bookmarkLink for guid = " + guid, ex);
            } finally {
                try {
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

	    // TBD:
	    if(Config.getInstance().isSearchEnabled()) {
            log.fine("Calling removeDocument()... for BookmarkLink.");
	    	BookmarkLinkIndexBuilder builder = new BookmarkLinkIndexBuilder();
	    	builder.removeDocument(guid);
	    }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
	}

    @Override
    public Long deleteBookmarkLinks(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultBookmarkLinkDAO.deleteBookmarkLinks(): filter=" + filter + "; params=" + params);

        // TBD: Delete-by-query requests should be carefully checked because of its potential harms.
        if(filter == null || filter.isEmpty()) {
            throw new RequestConflictException("deleteAll() without filter is not supported for performance/safety reasons");
        }

        Long count = 0L;
        PersistenceManager pm = getPersistenceManager();
        try {
            Query q = pm.newQuery(BookmarkLinkDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.deletePersistentAll(values.toArray(new Object[0]));
            } else {
                count = (Long) q.deletePersistentAll();
            }
            // TBD: How do you know the deletePersistent() call was successful???
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to deletebookmarkLinks because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete bookmarkLinks because the datastore is currently read-only.", ex);
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to delete bookmarkLinks because index is missing", ex);
            throw new DataStoreException("Failed to delete bookmarkLinks because index is missing", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete bookmarkLinks", ex);
            throw new DataStoreException("Failed to delete bookmarkLinks", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

	    // TBD:
	    //if(Config.getInstance().isSearchEnabled()) {
        //    log.fine("Calling removeDocument()... for BookmarkLink.");
	    //    BookmarkLinkIndexBuilder builder = new BookmarkLinkIndexBuilder();
	    //    builder.removeDocument(guids);
	    //}

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
