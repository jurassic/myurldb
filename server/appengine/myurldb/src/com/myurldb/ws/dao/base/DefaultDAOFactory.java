package com.myurldb.ws.dao.base;

import java.util.logging.Logger;

import com.myurldb.ws.dao.DAOFactory;
import com.myurldb.ws.dao.ApiConsumerDAO;
import com.myurldb.ws.dao.AppCustomDomainDAO;
import com.myurldb.ws.dao.SiteCustomDomainDAO;
import com.myurldb.ws.dao.UserDAO;
import com.myurldb.ws.dao.UserUsercodeDAO;
import com.myurldb.ws.dao.UserPasswordDAO;
import com.myurldb.ws.dao.ExternalUserAuthDAO;
import com.myurldb.ws.dao.UserAuthStateDAO;
import com.myurldb.ws.dao.UserResourcePermissionDAO;
import com.myurldb.ws.dao.UserResourceProhibitionDAO;
import com.myurldb.ws.dao.RolePermissionDAO;
import com.myurldb.ws.dao.UserRoleDAO;
import com.myurldb.ws.dao.AppClientDAO;
import com.myurldb.ws.dao.ClientUserDAO;
import com.myurldb.ws.dao.UserCustomDomainDAO;
import com.myurldb.ws.dao.ClientSettingDAO;
import com.myurldb.ws.dao.UserSettingDAO;
import com.myurldb.ws.dao.VisitorSettingDAO;
import com.myurldb.ws.dao.TwitterSummaryCardDAO;
import com.myurldb.ws.dao.TwitterPhotoCardDAO;
import com.myurldb.ws.dao.TwitterGalleryCardDAO;
import com.myurldb.ws.dao.TwitterAppCardDAO;
import com.myurldb.ws.dao.TwitterPlayerCardDAO;
import com.myurldb.ws.dao.TwitterProductCardDAO;
import com.myurldb.ws.dao.ShortPassageDAO;
import com.myurldb.ws.dao.ShortLinkDAO;
import com.myurldb.ws.dao.GeoLinkDAO;
import com.myurldb.ws.dao.QrCodeDAO;
import com.myurldb.ws.dao.LinkPassphraseDAO;
import com.myurldb.ws.dao.LinkMessageDAO;
import com.myurldb.ws.dao.LinkAlbumDAO;
import com.myurldb.ws.dao.AlbumShortLinkDAO;
import com.myurldb.ws.dao.KeywordFolderDAO;
import com.myurldb.ws.dao.BookmarkFolderDAO;
import com.myurldb.ws.dao.KeywordLinkDAO;
import com.myurldb.ws.dao.BookmarkLinkDAO;
import com.myurldb.ws.dao.SpeedDialDAO;
import com.myurldb.ws.dao.KeywordFolderImportDAO;
import com.myurldb.ws.dao.BookmarkFolderImportDAO;
import com.myurldb.ws.dao.KeywordCrowdTallyDAO;
import com.myurldb.ws.dao.BookmarkCrowdTallyDAO;
import com.myurldb.ws.dao.DomainInfoDAO;
import com.myurldb.ws.dao.UrlRatingDAO;
import com.myurldb.ws.dao.UserRatingDAO;
import com.myurldb.ws.dao.AbuseTagDAO;
import com.myurldb.ws.dao.ServiceInfoDAO;
import com.myurldb.ws.dao.FiveTenDAO;

// Default DAO factory uses JDO on Google AppEngine.
public class DefaultDAOFactory extends DAOFactory
{
    private static final Logger log = Logger.getLogger(DefaultDAOFactory.class.getName());

    // Ctor. Prevents instantiation.
    private DefaultDAOFactory()
    {
        // ...
    }

    // Initialization-on-demand holder.
    private static class DefaultDAOFactoryHolder
    {
        private static final DefaultDAOFactory INSTANCE = new DefaultDAOFactory();
    }

    // Singleton method
    public static DefaultDAOFactory getInstance()
    {
        return DefaultDAOFactoryHolder.INSTANCE;
    }

    @Override
    public ApiConsumerDAO getApiConsumerDAO()
    {
        return new DefaultApiConsumerDAO();
    }

    @Override
    public AppCustomDomainDAO getAppCustomDomainDAO()
    {
        return new DefaultAppCustomDomainDAO();
    }

    @Override
    public SiteCustomDomainDAO getSiteCustomDomainDAO()
    {
        return new DefaultSiteCustomDomainDAO();
    }

    @Override
    public UserDAO getUserDAO()
    {
        return new DefaultUserDAO();
    }

    @Override
    public UserUsercodeDAO getUserUsercodeDAO()
    {
        return new DefaultUserUsercodeDAO();
    }

    @Override
    public UserPasswordDAO getUserPasswordDAO()
    {
        return new DefaultUserPasswordDAO();
    }

    @Override
    public ExternalUserAuthDAO getExternalUserAuthDAO()
    {
        return new DefaultExternalUserAuthDAO();
    }

    @Override
    public UserAuthStateDAO getUserAuthStateDAO()
    {
        return new DefaultUserAuthStateDAO();
    }

    @Override
    public UserResourcePermissionDAO getUserResourcePermissionDAO()
    {
        return new DefaultUserResourcePermissionDAO();
    }

    @Override
    public UserResourceProhibitionDAO getUserResourceProhibitionDAO()
    {
        return new DefaultUserResourceProhibitionDAO();
    }

    @Override
    public RolePermissionDAO getRolePermissionDAO()
    {
        return new DefaultRolePermissionDAO();
    }

    @Override
    public UserRoleDAO getUserRoleDAO()
    {
        return new DefaultUserRoleDAO();
    }

    @Override
    public AppClientDAO getAppClientDAO()
    {
        return new DefaultAppClientDAO();
    }

    @Override
    public ClientUserDAO getClientUserDAO()
    {
        return new DefaultClientUserDAO();
    }

    @Override
    public UserCustomDomainDAO getUserCustomDomainDAO()
    {
        return new DefaultUserCustomDomainDAO();
    }

    @Override
    public ClientSettingDAO getClientSettingDAO()
    {
        return new DefaultClientSettingDAO();
    }

    @Override
    public UserSettingDAO getUserSettingDAO()
    {
        return new DefaultUserSettingDAO();
    }

    @Override
    public VisitorSettingDAO getVisitorSettingDAO()
    {
        return new DefaultVisitorSettingDAO();
    }

    @Override
    public TwitterSummaryCardDAO getTwitterSummaryCardDAO()
    {
        return new DefaultTwitterSummaryCardDAO();
    }

    @Override
    public TwitterPhotoCardDAO getTwitterPhotoCardDAO()
    {
        return new DefaultTwitterPhotoCardDAO();
    }

    @Override
    public TwitterGalleryCardDAO getTwitterGalleryCardDAO()
    {
        return new DefaultTwitterGalleryCardDAO();
    }

    @Override
    public TwitterAppCardDAO getTwitterAppCardDAO()
    {
        return new DefaultTwitterAppCardDAO();
    }

    @Override
    public TwitterPlayerCardDAO getTwitterPlayerCardDAO()
    {
        return new DefaultTwitterPlayerCardDAO();
    }

    @Override
    public TwitterProductCardDAO getTwitterProductCardDAO()
    {
        return new DefaultTwitterProductCardDAO();
    }

    @Override
    public ShortPassageDAO getShortPassageDAO()
    {
        return new DefaultShortPassageDAO();
    }

    @Override
    public ShortLinkDAO getShortLinkDAO()
    {
        return new DefaultShortLinkDAO();
    }

    @Override
    public GeoLinkDAO getGeoLinkDAO()
    {
        return new DefaultGeoLinkDAO();
    }

    @Override
    public QrCodeDAO getQrCodeDAO()
    {
        return new DefaultQrCodeDAO();
    }

    @Override
    public LinkPassphraseDAO getLinkPassphraseDAO()
    {
        return new DefaultLinkPassphraseDAO();
    }

    @Override
    public LinkMessageDAO getLinkMessageDAO()
    {
        return new DefaultLinkMessageDAO();
    }

    @Override
    public LinkAlbumDAO getLinkAlbumDAO()
    {
        return new DefaultLinkAlbumDAO();
    }

    @Override
    public AlbumShortLinkDAO getAlbumShortLinkDAO()
    {
        return new DefaultAlbumShortLinkDAO();
    }

    @Override
    public KeywordFolderDAO getKeywordFolderDAO()
    {
        return new DefaultKeywordFolderDAO();
    }

    @Override
    public BookmarkFolderDAO getBookmarkFolderDAO()
    {
        return new DefaultBookmarkFolderDAO();
    }

    @Override
    public KeywordLinkDAO getKeywordLinkDAO()
    {
        return new DefaultKeywordLinkDAO();
    }

    @Override
    public BookmarkLinkDAO getBookmarkLinkDAO()
    {
        return new DefaultBookmarkLinkDAO();
    }

    @Override
    public SpeedDialDAO getSpeedDialDAO()
    {
        return new DefaultSpeedDialDAO();
    }

    @Override
    public KeywordFolderImportDAO getKeywordFolderImportDAO()
    {
        return new DefaultKeywordFolderImportDAO();
    }

    @Override
    public BookmarkFolderImportDAO getBookmarkFolderImportDAO()
    {
        return new DefaultBookmarkFolderImportDAO();
    }

    @Override
    public KeywordCrowdTallyDAO getKeywordCrowdTallyDAO()
    {
        return new DefaultKeywordCrowdTallyDAO();
    }

    @Override
    public BookmarkCrowdTallyDAO getBookmarkCrowdTallyDAO()
    {
        return new DefaultBookmarkCrowdTallyDAO();
    }

    @Override
    public DomainInfoDAO getDomainInfoDAO()
    {
        return new DefaultDomainInfoDAO();
    }

    @Override
    public UrlRatingDAO getUrlRatingDAO()
    {
        return new DefaultUrlRatingDAO();
    }

    @Override
    public UserRatingDAO getUserRatingDAO()
    {
        return new DefaultUserRatingDAO();
    }

    @Override
    public AbuseTagDAO getAbuseTagDAO()
    {
        return new DefaultAbuseTagDAO();
    }

    @Override
    public ServiceInfoDAO getServiceInfoDAO()
    {
        return new DefaultServiceInfoDAO();
    }

    @Override
    public FiveTenDAO getFiveTenDAO()
    {
        return new DefaultFiveTenDAO();
    }

}
