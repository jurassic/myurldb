package com.myurldb.ws.dao.base;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

//import com.google.appengine.api.datastore.Key;

import com.myurldb.ws.core.GUID;
import com.myurldb.ws.config.Config;
import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.DataStoreException;
import com.myurldb.ws.exception.ServiceUnavailableException;
import com.myurldb.ws.exception.RequestConflictException;
import com.myurldb.ws.exception.ResourceNotFoundException;
import com.myurldb.ws.exception.NotImplementedException;
import com.myurldb.ws.dao.DomainInfoDAO;
import com.myurldb.ws.data.DomainInfoDataObject;
import com.myurldb.ws.search.gae.DomainInfoIndexBuilder;


public class DefaultDomainInfoDAO extends DefaultDAOBase implements DomainInfoDAO
{
    private static final Logger log = Logger.getLogger(DefaultDomainInfoDAO.class.getName()); 

    // Returns the domainInfo for the given guid/key.
    // Returns null if none is found in the datastore.
    @Override
    public DomainInfoDataObject getDomainInfo(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: guid = " + guid);

        DomainInfoDataObject domainInfo = null;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            //Transaction tx = pm.currentTransaction();
            try {
                //tx.setOptimistic(true);
                //tx.begin();
                //Key key = DomainInfoDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = DomainInfoDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                domainInfo = pm.getObjectById(DomainInfoDataObject.class, key);
                //tx.commit();
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve domainInfo for guid = " + guid, ex);
                throw new ResourceNotFoundException(ex);
            } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        log.finer("END");
	    return domainInfo;
	}

    @Override
    public List<DomainInfoDataObject> getDomainInfos(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // temporary implementation
        List<DomainInfoDataObject> domainInfos = null;
        if(guids != null && !guids.isEmpty()) {
            domainInfos = new ArrayList<DomainInfoDataObject>();
            for(String guid : guids) {
                DomainInfoDataObject obj = getDomainInfo(guid); 
                domainInfos.add(obj);
            }
	    }

        log.finer("END");
        return domainInfos;
    }

    @Override
    public List<DomainInfoDataObject> getAllDomainInfos() throws BaseException
	{
	    return getAllDomainInfos(null, null, null);
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<DomainInfoDataObject> getAllDomainInfos(String ordering, Long offset, Integer count) throws BaseException
	{
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count);

    	List<DomainInfoDataObject> domainInfos = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(DomainInfoDataObject.class);
            if(ordering != null) {
                q.setOrdering(ordering);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            domainInfos = (List<DomainInfoDataObject>) q.execute();
            if(domainInfos != null) {
                domainInfos.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            /*
            // ???
            Collection<DomainInfoDataObject> rs_domainInfos = (Collection<DomainInfoDataObject>) q.execute();
            if(rs_domainInfos == null) {
                log.log(Level.WARNING, "Failed to retrieve all domainInfos.");
                domainInfos = new ArrayList<DomainInfoDataObject>();  // ???           
            } else {
                domainInfos = new ArrayList<DomainInfoDataObject>(pm.detachCopyAll(rs_domainInfos));
            }
            */
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all domainInfos.", ex);
            //domainInfos = new ArrayList<DomainInfoDataObject>();  // ???
            throw new DataStoreException("Failed to retrieve all domainInfos.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return domainInfos;
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<String> getAllDomainInfoKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count);

    	List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery("select key from " + DomainInfoDataObject.class.getName());  // ???
            if(ordering != null) {
                q.setOrdering(ordering);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            keys = (List<String>) q.execute();
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all DomainInfo keys.", ex);
            throw new DataStoreException("Failed to retrieve all DomainInfo keys.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return keys;
    }

    @Override
	public List<DomainInfoDataObject> findDomainInfos(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findDomainInfos(filter, ordering, params, values, null, null);
    }

    @Override
	public List<DomainInfoDataObject> findDomainInfos(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findDomainInfos(filter, ordering, params, values, null, null, offset, count);
	}

    @SuppressWarnings("unchecked")
    @Override
	public List<DomainInfoDataObject> findDomainInfos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
        if(log.isLoggable(Level.INFO)) log.info("DefaultDomainInfoDAO.findDomainInfos(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("findDomainInfos() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        List<DomainInfoDataObject> domainInfos = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(DomainInfoDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                domainInfos = (List<DomainInfoDataObject>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                domainInfos = (List<DomainInfoDataObject>) q.execute();
            }
            if(domainInfos != null) {
                domainInfos.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //while($TypeUtil.deCapitalizePluralize($typeName).iterator().hasNext()) {
            //    for(DomainInfoDataObject dobj : domainInfos) {
            //        //
            //    }
            //}
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find domainInfos because index is missing.", ex);
            //domainInfos = new ArrayList<DomainInfoDataObject>();  // ???
            throw new DataStoreException("Failed to find domainInfos because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find domainInfos meeting the criterion.", ex);
            //domainInfos = new ArrayList<DomainInfoDataObject>();  // ???
            throw new DataStoreException("Failed to find domainInfos meeting the criterion.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return domainInfos;
	}

    @SuppressWarnings("unchecked")
    @Override
    public List<String> findDomainInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultDomainInfoDAO.findDomainInfoKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery("select key from " + DomainInfoDataObject.class.getName());  // ???
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                keys = (List<String>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                keys = (List<String>) q.execute();
            }
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find DomainInfo keys because index is missing.", ex);
            throw new DataStoreException("Failed to find DomainInfo keys because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find DomainInfo keys meeting the criterion.", ex);
            throw new DataStoreException("Failed to find DomainInfo keys meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END");
        return keys;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultDomainInfoDAO.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("getCount() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        Long count = 0L; 
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(DomainInfoDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(aggregate == null || aggregate.isEmpty()) {
                aggregate = "count(this)";  // By default, count. Other examples: "max(this.createdTime)", avg, sum, etc. (Note: These are currently not supported by Google App Engine.)
            }
            q.setResult(aggregate);
            //q.setResultClass(Long.class);
            // For now, we only support a single aggregate function. (Eg, no comma-separated list.)
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                count = (Long) q.execute();
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to get domainInfo count because index is missing.", ex);
            throw new DataStoreException("Failed to get domainInfo count because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to get domainInfo count meeting the criterion.", ex);
            throw new DataStoreException("Failed to get domainInfo count meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(count == null) {
            count = 0L;
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

	// Stores the domainInfo in the datastore and returns its guid.
	// Returns null if the operation fails.
    private String storeDomainInfo(DomainInfoDataObject domainInfo) throws BaseException
    {
        log.fine("storeDomainInfo() called.");

        String guid = null;
        PersistenceManager pm = getPersistenceManager();
        try {
            // TBD: If the object on the datastore has non-null created date and the arg shortLink has no created date timestamp,
            //      do not overwrite the timestamp. How to do that???
            // For now, this is ok since we use "overwrite/replace" semantics (not partial update).
            Long createdTime = domainInfo.getCreatedTime();
            if(createdTime == null) {
                createdTime = System.currentTimeMillis();
                domainInfo.setCreatedTime(createdTime);
            }
            Long modifiedTime = domainInfo.getModifiedTime();
            if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
                domainInfo.setModifiedTime(createdTime);
            }
            pm.makePersistent(domainInfo); 
            // TBD: How do you know the makePersistent() call was successful???
            guid = domainInfo.getGuid();
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to store domainInfo because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to store domainInfo because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to store domainInfo.", ex);
            throw new DataStoreException("Failed to store domainInfo.", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

	    // TBD:
	    if(Config.getInstance().isSearchEnabled()) {
            log.fine("Calling addDocument()... for DomainInfo.");
	    	DomainInfoIndexBuilder builder = new DomainInfoIndexBuilder();
	    	builder.addDocument(domainInfo);
	    }

        if(log.isLoggable(Level.FINE)) log.fine("storeDomainInfo(): guid = " + guid);
        return guid;
    }

    @Override
    public String createDomainInfo(DomainInfoDataObject domainInfo) throws BaseException
    {
        // The createdTime field will be automatically set in storeDomainInfo().
        //Long createdTime = domainInfo.getCreatedTime();
        //if(createdTime == null) {
        //    createdTime = System.currentTimeMillis();
        //    domainInfo.setCreatedTime(createdTime);
        //}
        //Long modifiedTime = domainInfo.getModifiedTime();
        //if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
        //    domainInfo.setModifiedTime(createdTime);
        //}
        return storeDomainInfo(domainInfo);
    }

    @Override
	public Boolean updateDomainInfo(DomainInfoDataObject domainInfo) throws BaseException
	{
	    // Modify the modifiedTime timestamp.
	    // CreatedTime should, normally, be non-null.
	    // If null, however, it will be automatically set in storeDomainInfo()
	    // (in which case modifiedTime might be updated again).
	    domainInfo.setModifiedTime(System.currentTimeMillis());
	    String guid = storeDomainInfo(domainInfo);
	    if(guid != null) {
	        return true;
	    } else {
	        return false;
	    }
	}
	
    @Override
    public Boolean deleteDomainInfo(DomainInfoDataObject domainInfo) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        PersistenceManager pm = getPersistenceManager();
        try {
            pm.deletePersistent(domainInfo);
            // TBD: How do you know the deletePersistent() call was successful???
            suc = true;
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to delete domainInfo because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete domainInfo because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete domainInfo.", ex);
            throw new DataStoreException("Failed to delete domainInfo.", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

	    // TBD:
	    if(Config.getInstance().isSearchEnabled()) {
            log.fine("Calling removeDocument()... for DomainInfo.");
	    	DomainInfoIndexBuilder builder = new DomainInfoIndexBuilder();
	    	builder.removeDocument(domainInfo.getGuid());
	    }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteDomainInfo(String guid) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            try {
                //Key key = DomainInfoDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = DomainInfoDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                DomainInfoDataObject domainInfo = pm.getObjectById(DomainInfoDataObject.class, key);
                pm.deletePersistent(domainInfo);
                // TBD: How do you know the deletePersistent() call was successful???
                suc = true;
            } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
                // Datastore is read-only, possibly due to the Google App Engine maintenance.
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete domainInfo because the datastore is currently read-only. guid = " + guid, ex);
                throw new ServiceUnavailableException("Failed to delete domainInfo because the datastore is currently read-only.", ex);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete domainInfo for guid = " + guid, ex);
                throw new DataStoreException("Failed to delete domainInfo for guid = " + guid, ex);
            } finally {
                try {
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

	    // TBD:
	    if(Config.getInstance().isSearchEnabled()) {
            log.fine("Calling removeDocument()... for DomainInfo.");
	    	DomainInfoIndexBuilder builder = new DomainInfoIndexBuilder();
	    	builder.removeDocument(guid);
	    }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
	}

    @Override
    public Long deleteDomainInfos(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultDomainInfoDAO.deleteDomainInfos(): filter=" + filter + "; params=" + params);

        // TBD: Delete-by-query requests should be carefully checked because of its potential harms.
        if(filter == null || filter.isEmpty()) {
            throw new RequestConflictException("deleteAll() without filter is not supported for performance/safety reasons");
        }

        Long count = 0L;
        PersistenceManager pm = getPersistenceManager();
        try {
            Query q = pm.newQuery(DomainInfoDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.deletePersistentAll(values.toArray(new Object[0]));
            } else {
                count = (Long) q.deletePersistentAll();
            }
            // TBD: How do you know the deletePersistent() call was successful???
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to deletedomainInfos because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete domainInfos because the datastore is currently read-only.", ex);
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to delete domainInfos because index is missing", ex);
            throw new DataStoreException("Failed to delete domainInfos because index is missing", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete domainInfos", ex);
            throw new DataStoreException("Failed to delete domainInfos", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

	    // TBD:
	    //if(Config.getInstance().isSearchEnabled()) {
        //    log.fine("Calling removeDocument()... for DomainInfo.");
	    //    DomainInfoIndexBuilder builder = new DomainInfoIndexBuilder();
	    //    builder.removeDocument(guids);
	    //}

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
