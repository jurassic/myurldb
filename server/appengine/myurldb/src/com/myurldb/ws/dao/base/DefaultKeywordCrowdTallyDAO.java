package com.myurldb.ws.dao.base;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

//import com.google.appengine.api.datastore.Key;

import com.myurldb.ws.core.GUID;
import com.myurldb.ws.config.Config;
import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.DataStoreException;
import com.myurldb.ws.exception.ServiceUnavailableException;
import com.myurldb.ws.exception.RequestConflictException;
import com.myurldb.ws.exception.ResourceNotFoundException;
import com.myurldb.ws.exception.NotImplementedException;
import com.myurldb.ws.dao.KeywordCrowdTallyDAO;
import com.myurldb.ws.data.KeywordCrowdTallyDataObject;
import com.myurldb.ws.search.gae.KeywordCrowdTallyIndexBuilder;


public class DefaultKeywordCrowdTallyDAO extends DefaultDAOBase implements KeywordCrowdTallyDAO
{
    private static final Logger log = Logger.getLogger(DefaultKeywordCrowdTallyDAO.class.getName()); 

    // Returns the keywordCrowdTally for the given guid/key.
    // Returns null if none is found in the datastore.
    @Override
    public KeywordCrowdTallyDataObject getKeywordCrowdTally(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: guid = " + guid);

        KeywordCrowdTallyDataObject keywordCrowdTally = null;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            //Transaction tx = pm.currentTransaction();
            try {
                //tx.setOptimistic(true);
                //tx.begin();
                //Key key = KeywordCrowdTallyDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = KeywordCrowdTallyDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                keywordCrowdTally = pm.getObjectById(KeywordCrowdTallyDataObject.class, key);
                //tx.commit();
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve keywordCrowdTally for guid = " + guid, ex);
                throw new ResourceNotFoundException(ex);
            } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        log.finer("END");
	    return keywordCrowdTally;
	}

    @Override
    public List<KeywordCrowdTallyDataObject> getKeywordCrowdTallies(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // temporary implementation
        List<KeywordCrowdTallyDataObject> keywordCrowdTallies = null;
        if(guids != null && !guids.isEmpty()) {
            keywordCrowdTallies = new ArrayList<KeywordCrowdTallyDataObject>();
            for(String guid : guids) {
                KeywordCrowdTallyDataObject obj = getKeywordCrowdTally(guid); 
                keywordCrowdTallies.add(obj);
            }
	    }

        log.finer("END");
        return keywordCrowdTallies;
    }

    @Override
    public List<KeywordCrowdTallyDataObject> getAllKeywordCrowdTallies() throws BaseException
	{
	    return getAllKeywordCrowdTallies(null, null, null);
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<KeywordCrowdTallyDataObject> getAllKeywordCrowdTallies(String ordering, Long offset, Integer count) throws BaseException
	{
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count);

    	List<KeywordCrowdTallyDataObject> keywordCrowdTallies = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(KeywordCrowdTallyDataObject.class);
            if(ordering != null) {
                q.setOrdering(ordering);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            keywordCrowdTallies = (List<KeywordCrowdTallyDataObject>) q.execute();
            if(keywordCrowdTallies != null) {
                keywordCrowdTallies.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            /*
            // ???
            Collection<KeywordCrowdTallyDataObject> rs_keywordCrowdTallies = (Collection<KeywordCrowdTallyDataObject>) q.execute();
            if(rs_keywordCrowdTallies == null) {
                log.log(Level.WARNING, "Failed to retrieve all keywordCrowdTallies.");
                keywordCrowdTallies = new ArrayList<KeywordCrowdTallyDataObject>();  // ???           
            } else {
                keywordCrowdTallies = new ArrayList<KeywordCrowdTallyDataObject>(pm.detachCopyAll(rs_keywordCrowdTallies));
            }
            */
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all keywordCrowdTallies.", ex);
            //keywordCrowdTallies = new ArrayList<KeywordCrowdTallyDataObject>();  // ???
            throw new DataStoreException("Failed to retrieve all keywordCrowdTallies.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return keywordCrowdTallies;
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<String> getAllKeywordCrowdTallyKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count);

    	List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery("select key from " + KeywordCrowdTallyDataObject.class.getName());  // ???
            if(ordering != null) {
                q.setOrdering(ordering);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            keys = (List<String>) q.execute();
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all KeywordCrowdTally keys.", ex);
            throw new DataStoreException("Failed to retrieve all KeywordCrowdTally keys.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return keys;
    }

    @Override
	public List<KeywordCrowdTallyDataObject> findKeywordCrowdTallies(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findKeywordCrowdTallies(filter, ordering, params, values, null, null);
    }

    @Override
	public List<KeywordCrowdTallyDataObject> findKeywordCrowdTallies(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findKeywordCrowdTallies(filter, ordering, params, values, null, null, offset, count);
	}

    @SuppressWarnings("unchecked")
    @Override
	public List<KeywordCrowdTallyDataObject> findKeywordCrowdTallies(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
        if(log.isLoggable(Level.INFO)) log.info("DefaultKeywordCrowdTallyDAO.findKeywordCrowdTallies(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("findKeywordCrowdTallies() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        List<KeywordCrowdTallyDataObject> keywordCrowdTallies = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(KeywordCrowdTallyDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                keywordCrowdTallies = (List<KeywordCrowdTallyDataObject>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                keywordCrowdTallies = (List<KeywordCrowdTallyDataObject>) q.execute();
            }
            if(keywordCrowdTallies != null) {
                keywordCrowdTallies.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //while($TypeUtil.deCapitalizePluralize($typeName).iterator().hasNext()) {
            //    for(KeywordCrowdTallyDataObject dobj : keywordCrowdTallies) {
            //        //
            //    }
            //}
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find keywordCrowdTallies because index is missing.", ex);
            //keywordCrowdTallies = new ArrayList<KeywordCrowdTallyDataObject>();  // ???
            throw new DataStoreException("Failed to find keywordCrowdTallies because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find keywordCrowdTallies meeting the criterion.", ex);
            //keywordCrowdTallies = new ArrayList<KeywordCrowdTallyDataObject>();  // ???
            throw new DataStoreException("Failed to find keywordCrowdTallies meeting the criterion.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return keywordCrowdTallies;
	}

    @SuppressWarnings("unchecked")
    @Override
    public List<String> findKeywordCrowdTallyKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultKeywordCrowdTallyDAO.findKeywordCrowdTallyKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery("select key from " + KeywordCrowdTallyDataObject.class.getName());  // ???
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                keys = (List<String>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                keys = (List<String>) q.execute();
            }
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find KeywordCrowdTally keys because index is missing.", ex);
            throw new DataStoreException("Failed to find KeywordCrowdTally keys because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find KeywordCrowdTally keys meeting the criterion.", ex);
            throw new DataStoreException("Failed to find KeywordCrowdTally keys meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END");
        return keys;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultKeywordCrowdTallyDAO.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("getCount() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        Long count = 0L; 
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(KeywordCrowdTallyDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(aggregate == null || aggregate.isEmpty()) {
                aggregate = "count(this)";  // By default, count. Other examples: "max(this.createdTime)", avg, sum, etc. (Note: These are currently not supported by Google App Engine.)
            }
            q.setResult(aggregate);
            //q.setResultClass(Long.class);
            // For now, we only support a single aggregate function. (Eg, no comma-separated list.)
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                count = (Long) q.execute();
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to get keywordCrowdTally count because index is missing.", ex);
            throw new DataStoreException("Failed to get keywordCrowdTally count because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to get keywordCrowdTally count meeting the criterion.", ex);
            throw new DataStoreException("Failed to get keywordCrowdTally count meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(count == null) {
            count = 0L;
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

	// Stores the keywordCrowdTally in the datastore and returns its guid.
	// Returns null if the operation fails.
    private String storeKeywordCrowdTally(KeywordCrowdTallyDataObject keywordCrowdTally) throws BaseException
    {
        log.fine("storeKeywordCrowdTally() called.");

        String guid = null;
        PersistenceManager pm = getPersistenceManager();
        try {
            // TBD: If the object on the datastore has non-null created date and the arg shortLink has no created date timestamp,
            //      do not overwrite the timestamp. How to do that???
            // For now, this is ok since we use "overwrite/replace" semantics (not partial update).
            Long createdTime = keywordCrowdTally.getCreatedTime();
            if(createdTime == null) {
                createdTime = System.currentTimeMillis();
                keywordCrowdTally.setCreatedTime(createdTime);
            }
            Long modifiedTime = keywordCrowdTally.getModifiedTime();
            if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
                keywordCrowdTally.setModifiedTime(createdTime);
            }
            pm.makePersistent(keywordCrowdTally); 
            // TBD: How do you know the makePersistent() call was successful???
            guid = keywordCrowdTally.getGuid();
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to store keywordCrowdTally because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to store keywordCrowdTally because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to store keywordCrowdTally.", ex);
            throw new DataStoreException("Failed to store keywordCrowdTally.", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

	    // TBD:
	    if(Config.getInstance().isSearchEnabled()) {
            log.fine("Calling addDocument()... for KeywordCrowdTally.");
	    	KeywordCrowdTallyIndexBuilder builder = new KeywordCrowdTallyIndexBuilder();
	    	builder.addDocument(keywordCrowdTally);
	    }

        if(log.isLoggable(Level.FINE)) log.fine("storeKeywordCrowdTally(): guid = " + guid);
        return guid;
    }

    @Override
    public String createKeywordCrowdTally(KeywordCrowdTallyDataObject keywordCrowdTally) throws BaseException
    {
        // The createdTime field will be automatically set in storeKeywordCrowdTally().
        //Long createdTime = keywordCrowdTally.getCreatedTime();
        //if(createdTime == null) {
        //    createdTime = System.currentTimeMillis();
        //    keywordCrowdTally.setCreatedTime(createdTime);
        //}
        //Long modifiedTime = keywordCrowdTally.getModifiedTime();
        //if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
        //    keywordCrowdTally.setModifiedTime(createdTime);
        //}
        return storeKeywordCrowdTally(keywordCrowdTally);
    }

    @Override
	public Boolean updateKeywordCrowdTally(KeywordCrowdTallyDataObject keywordCrowdTally) throws BaseException
	{
	    // Modify the modifiedTime timestamp.
	    // CreatedTime should, normally, be non-null.
	    // If null, however, it will be automatically set in storeKeywordCrowdTally()
	    // (in which case modifiedTime might be updated again).
	    keywordCrowdTally.setModifiedTime(System.currentTimeMillis());
	    String guid = storeKeywordCrowdTally(keywordCrowdTally);
	    if(guid != null) {
	        return true;
	    } else {
	        return false;
	    }
	}
	
    @Override
    public Boolean deleteKeywordCrowdTally(KeywordCrowdTallyDataObject keywordCrowdTally) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        PersistenceManager pm = getPersistenceManager();
        try {
            pm.deletePersistent(keywordCrowdTally);
            // TBD: How do you know the deletePersistent() call was successful???
            suc = true;
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to delete keywordCrowdTally because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete keywordCrowdTally because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete keywordCrowdTally.", ex);
            throw new DataStoreException("Failed to delete keywordCrowdTally.", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

	    // TBD:
	    if(Config.getInstance().isSearchEnabled()) {
            log.fine("Calling removeDocument()... for KeywordCrowdTally.");
	    	KeywordCrowdTallyIndexBuilder builder = new KeywordCrowdTallyIndexBuilder();
	    	builder.removeDocument(keywordCrowdTally.getGuid());
	    }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteKeywordCrowdTally(String guid) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            try {
                //Key key = KeywordCrowdTallyDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = KeywordCrowdTallyDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                KeywordCrowdTallyDataObject keywordCrowdTally = pm.getObjectById(KeywordCrowdTallyDataObject.class, key);
                pm.deletePersistent(keywordCrowdTally);
                // TBD: How do you know the deletePersistent() call was successful???
                suc = true;
            } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
                // Datastore is read-only, possibly due to the Google App Engine maintenance.
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete keywordCrowdTally because the datastore is currently read-only. guid = " + guid, ex);
                throw new ServiceUnavailableException("Failed to delete keywordCrowdTally because the datastore is currently read-only.", ex);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete keywordCrowdTally for guid = " + guid, ex);
                throw new DataStoreException("Failed to delete keywordCrowdTally for guid = " + guid, ex);
            } finally {
                try {
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

	    // TBD:
	    if(Config.getInstance().isSearchEnabled()) {
            log.fine("Calling removeDocument()... for KeywordCrowdTally.");
	    	KeywordCrowdTallyIndexBuilder builder = new KeywordCrowdTallyIndexBuilder();
	    	builder.removeDocument(guid);
	    }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
	}

    @Override
    public Long deleteKeywordCrowdTallies(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultKeywordCrowdTallyDAO.deleteKeywordCrowdTallies(): filter=" + filter + "; params=" + params);

        // TBD: Delete-by-query requests should be carefully checked because of its potential harms.
        if(filter == null || filter.isEmpty()) {
            throw new RequestConflictException("deleteAll() without filter is not supported for performance/safety reasons");
        }

        Long count = 0L;
        PersistenceManager pm = getPersistenceManager();
        try {
            Query q = pm.newQuery(KeywordCrowdTallyDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.deletePersistentAll(values.toArray(new Object[0]));
            } else {
                count = (Long) q.deletePersistentAll();
            }
            // TBD: How do you know the deletePersistent() call was successful???
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to deletekeywordCrowdTallies because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete keywordCrowdTallies because the datastore is currently read-only.", ex);
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to delete keywordCrowdTallies because index is missing", ex);
            throw new DataStoreException("Failed to delete keywordCrowdTallies because index is missing", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete keywordCrowdTallies", ex);
            throw new DataStoreException("Failed to delete keywordCrowdTallies", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

	    // TBD:
	    //if(Config.getInstance().isSearchEnabled()) {
        //    log.fine("Calling removeDocument()... for KeywordCrowdTally.");
	    //    KeywordCrowdTallyIndexBuilder builder = new KeywordCrowdTallyIndexBuilder();
	    //    builder.removeDocument(guids);
	    //}

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
