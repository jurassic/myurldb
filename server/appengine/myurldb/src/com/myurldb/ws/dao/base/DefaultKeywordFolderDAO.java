package com.myurldb.ws.dao.base;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

//import com.google.appengine.api.datastore.Key;

import com.myurldb.ws.core.GUID;
import com.myurldb.ws.config.Config;
import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.DataStoreException;
import com.myurldb.ws.exception.ServiceUnavailableException;
import com.myurldb.ws.exception.RequestConflictException;
import com.myurldb.ws.exception.ResourceNotFoundException;
import com.myurldb.ws.exception.NotImplementedException;
import com.myurldb.ws.dao.KeywordFolderDAO;
import com.myurldb.ws.data.KeywordFolderDataObject;
import com.myurldb.ws.search.gae.KeywordFolderIndexBuilder;


public class DefaultKeywordFolderDAO extends DefaultDAOBase implements KeywordFolderDAO
{
    private static final Logger log = Logger.getLogger(DefaultKeywordFolderDAO.class.getName()); 

    // Returns the keywordFolder for the given guid/key.
    // Returns null if none is found in the datastore.
    @Override
    public KeywordFolderDataObject getKeywordFolder(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: guid = " + guid);

        KeywordFolderDataObject keywordFolder = null;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            //Transaction tx = pm.currentTransaction();
            try {
                //tx.setOptimistic(true);
                //tx.begin();
                //Key key = KeywordFolderDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = KeywordFolderDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                keywordFolder = pm.getObjectById(KeywordFolderDataObject.class, key);
                //tx.commit();
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve keywordFolder for guid = " + guid, ex);
                throw new ResourceNotFoundException(ex);
            } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        log.finer("END");
	    return keywordFolder;
	}

    @Override
    public List<KeywordFolderDataObject> getKeywordFolders(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // temporary implementation
        List<KeywordFolderDataObject> keywordFolders = null;
        if(guids != null && !guids.isEmpty()) {
            keywordFolders = new ArrayList<KeywordFolderDataObject>();
            for(String guid : guids) {
                KeywordFolderDataObject obj = getKeywordFolder(guid); 
                keywordFolders.add(obj);
            }
	    }

        log.finer("END");
        return keywordFolders;
    }

    @Override
    public List<KeywordFolderDataObject> getAllKeywordFolders() throws BaseException
	{
	    return getAllKeywordFolders(null, null, null);
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<KeywordFolderDataObject> getAllKeywordFolders(String ordering, Long offset, Integer count) throws BaseException
	{
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count);

    	List<KeywordFolderDataObject> keywordFolders = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(KeywordFolderDataObject.class);
            if(ordering != null) {
                q.setOrdering(ordering);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            keywordFolders = (List<KeywordFolderDataObject>) q.execute();
            if(keywordFolders != null) {
                keywordFolders.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            /*
            // ???
            Collection<KeywordFolderDataObject> rs_keywordFolders = (Collection<KeywordFolderDataObject>) q.execute();
            if(rs_keywordFolders == null) {
                log.log(Level.WARNING, "Failed to retrieve all keywordFolders.");
                keywordFolders = new ArrayList<KeywordFolderDataObject>();  // ???           
            } else {
                keywordFolders = new ArrayList<KeywordFolderDataObject>(pm.detachCopyAll(rs_keywordFolders));
            }
            */
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all keywordFolders.", ex);
            //keywordFolders = new ArrayList<KeywordFolderDataObject>();  // ???
            throw new DataStoreException("Failed to retrieve all keywordFolders.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return keywordFolders;
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<String> getAllKeywordFolderKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count);

    	List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery("select key from " + KeywordFolderDataObject.class.getName());  // ???
            if(ordering != null) {
                q.setOrdering(ordering);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            keys = (List<String>) q.execute();
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all KeywordFolder keys.", ex);
            throw new DataStoreException("Failed to retrieve all KeywordFolder keys.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return keys;
    }

    @Override
	public List<KeywordFolderDataObject> findKeywordFolders(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findKeywordFolders(filter, ordering, params, values, null, null);
    }

    @Override
	public List<KeywordFolderDataObject> findKeywordFolders(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findKeywordFolders(filter, ordering, params, values, null, null, offset, count);
	}

    @SuppressWarnings("unchecked")
    @Override
	public List<KeywordFolderDataObject> findKeywordFolders(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
        if(log.isLoggable(Level.INFO)) log.info("DefaultKeywordFolderDAO.findKeywordFolders(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("findKeywordFolders() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        List<KeywordFolderDataObject> keywordFolders = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(KeywordFolderDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                keywordFolders = (List<KeywordFolderDataObject>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                keywordFolders = (List<KeywordFolderDataObject>) q.execute();
            }
            if(keywordFolders != null) {
                keywordFolders.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //while($TypeUtil.deCapitalizePluralize($typeName).iterator().hasNext()) {
            //    for(KeywordFolderDataObject dobj : keywordFolders) {
            //        //
            //    }
            //}
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find keywordFolders because index is missing.", ex);
            //keywordFolders = new ArrayList<KeywordFolderDataObject>();  // ???
            throw new DataStoreException("Failed to find keywordFolders because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find keywordFolders meeting the criterion.", ex);
            //keywordFolders = new ArrayList<KeywordFolderDataObject>();  // ???
            throw new DataStoreException("Failed to find keywordFolders meeting the criterion.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return keywordFolders;
	}

    @SuppressWarnings("unchecked")
    @Override
    public List<String> findKeywordFolderKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultKeywordFolderDAO.findKeywordFolderKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery("select key from " + KeywordFolderDataObject.class.getName());  // ???
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                keys = (List<String>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                keys = (List<String>) q.execute();
            }
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find KeywordFolder keys because index is missing.", ex);
            throw new DataStoreException("Failed to find KeywordFolder keys because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find KeywordFolder keys meeting the criterion.", ex);
            throw new DataStoreException("Failed to find KeywordFolder keys meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END");
        return keys;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultKeywordFolderDAO.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("getCount() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        Long count = 0L; 
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(KeywordFolderDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(aggregate == null || aggregate.isEmpty()) {
                aggregate = "count(this)";  // By default, count. Other examples: "max(this.createdTime)", avg, sum, etc. (Note: These are currently not supported by Google App Engine.)
            }
            q.setResult(aggregate);
            //q.setResultClass(Long.class);
            // For now, we only support a single aggregate function. (Eg, no comma-separated list.)
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                count = (Long) q.execute();
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to get keywordFolder count because index is missing.", ex);
            throw new DataStoreException("Failed to get keywordFolder count because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to get keywordFolder count meeting the criterion.", ex);
            throw new DataStoreException("Failed to get keywordFolder count meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(count == null) {
            count = 0L;
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

	// Stores the keywordFolder in the datastore and returns its guid.
	// Returns null if the operation fails.
    private String storeKeywordFolder(KeywordFolderDataObject keywordFolder) throws BaseException
    {
        log.fine("storeKeywordFolder() called.");

        String guid = null;
        PersistenceManager pm = getPersistenceManager();
        try {
            // TBD: If the object on the datastore has non-null created date and the arg shortLink has no created date timestamp,
            //      do not overwrite the timestamp. How to do that???
            // For now, this is ok since we use "overwrite/replace" semantics (not partial update).
            Long createdTime = keywordFolder.getCreatedTime();
            if(createdTime == null) {
                createdTime = System.currentTimeMillis();
                keywordFolder.setCreatedTime(createdTime);
            }
            Long modifiedTime = keywordFolder.getModifiedTime();
            if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
                keywordFolder.setModifiedTime(createdTime);
            }
            pm.makePersistent(keywordFolder); 
            // TBD: How do you know the makePersistent() call was successful???
            guid = keywordFolder.getGuid();
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to store keywordFolder because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to store keywordFolder because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to store keywordFolder.", ex);
            throw new DataStoreException("Failed to store keywordFolder.", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

	    // TBD:
	    if(Config.getInstance().isSearchEnabled()) {
            log.fine("Calling addDocument()... for KeywordFolder.");
	    	KeywordFolderIndexBuilder builder = new KeywordFolderIndexBuilder();
	    	builder.addDocument(keywordFolder);
	    }

        if(log.isLoggable(Level.FINE)) log.fine("storeKeywordFolder(): guid = " + guid);
        return guid;
    }

    @Override
    public String createKeywordFolder(KeywordFolderDataObject keywordFolder) throws BaseException
    {
        // The createdTime field will be automatically set in storeKeywordFolder().
        //Long createdTime = keywordFolder.getCreatedTime();
        //if(createdTime == null) {
        //    createdTime = System.currentTimeMillis();
        //    keywordFolder.setCreatedTime(createdTime);
        //}
        //Long modifiedTime = keywordFolder.getModifiedTime();
        //if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
        //    keywordFolder.setModifiedTime(createdTime);
        //}
        return storeKeywordFolder(keywordFolder);
    }

    @Override
	public Boolean updateKeywordFolder(KeywordFolderDataObject keywordFolder) throws BaseException
	{
	    // Modify the modifiedTime timestamp.
	    // CreatedTime should, normally, be non-null.
	    // If null, however, it will be automatically set in storeKeywordFolder()
	    // (in which case modifiedTime might be updated again).
	    keywordFolder.setModifiedTime(System.currentTimeMillis());
	    String guid = storeKeywordFolder(keywordFolder);
	    if(guid != null) {
	        return true;
	    } else {
	        return false;
	    }
	}
	
    @Override
    public Boolean deleteKeywordFolder(KeywordFolderDataObject keywordFolder) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        PersistenceManager pm = getPersistenceManager();
        try {
            pm.deletePersistent(keywordFolder);
            // TBD: How do you know the deletePersistent() call was successful???
            suc = true;
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to delete keywordFolder because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete keywordFolder because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete keywordFolder.", ex);
            throw new DataStoreException("Failed to delete keywordFolder.", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

	    // TBD:
	    if(Config.getInstance().isSearchEnabled()) {
            log.fine("Calling removeDocument()... for KeywordFolder.");
	    	KeywordFolderIndexBuilder builder = new KeywordFolderIndexBuilder();
	    	builder.removeDocument(keywordFolder.getGuid());
	    }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteKeywordFolder(String guid) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            try {
                //Key key = KeywordFolderDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = KeywordFolderDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                KeywordFolderDataObject keywordFolder = pm.getObjectById(KeywordFolderDataObject.class, key);
                pm.deletePersistent(keywordFolder);
                // TBD: How do you know the deletePersistent() call was successful???
                suc = true;
            } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
                // Datastore is read-only, possibly due to the Google App Engine maintenance.
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete keywordFolder because the datastore is currently read-only. guid = " + guid, ex);
                throw new ServiceUnavailableException("Failed to delete keywordFolder because the datastore is currently read-only.", ex);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete keywordFolder for guid = " + guid, ex);
                throw new DataStoreException("Failed to delete keywordFolder for guid = " + guid, ex);
            } finally {
                try {
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

	    // TBD:
	    if(Config.getInstance().isSearchEnabled()) {
            log.fine("Calling removeDocument()... for KeywordFolder.");
	    	KeywordFolderIndexBuilder builder = new KeywordFolderIndexBuilder();
	    	builder.removeDocument(guid);
	    }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
	}

    @Override
    public Long deleteKeywordFolders(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultKeywordFolderDAO.deleteKeywordFolders(): filter=" + filter + "; params=" + params);

        // TBD: Delete-by-query requests should be carefully checked because of its potential harms.
        if(filter == null || filter.isEmpty()) {
            throw new RequestConflictException("deleteAll() without filter is not supported for performance/safety reasons");
        }

        Long count = 0L;
        PersistenceManager pm = getPersistenceManager();
        try {
            Query q = pm.newQuery(KeywordFolderDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.deletePersistentAll(values.toArray(new Object[0]));
            } else {
                count = (Long) q.deletePersistentAll();
            }
            // TBD: How do you know the deletePersistent() call was successful???
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to deletekeywordFolders because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete keywordFolders because the datastore is currently read-only.", ex);
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to delete keywordFolders because index is missing", ex);
            throw new DataStoreException("Failed to delete keywordFolders because index is missing", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete keywordFolders", ex);
            throw new DataStoreException("Failed to delete keywordFolders", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

	    // TBD:
	    //if(Config.getInstance().isSearchEnabled()) {
        //    log.fine("Calling removeDocument()... for KeywordFolder.");
	    //    KeywordFolderIndexBuilder builder = new KeywordFolderIndexBuilder();
	    //    builder.removeDocument(guids);
	    //}

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
