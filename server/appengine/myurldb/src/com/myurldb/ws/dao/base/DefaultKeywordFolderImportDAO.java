package com.myurldb.ws.dao.base;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

//import com.google.appengine.api.datastore.Key;

import com.myurldb.ws.core.GUID;
import com.myurldb.ws.config.Config;
import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.DataStoreException;
import com.myurldb.ws.exception.ServiceUnavailableException;
import com.myurldb.ws.exception.RequestConflictException;
import com.myurldb.ws.exception.ResourceNotFoundException;
import com.myurldb.ws.exception.NotImplementedException;
import com.myurldb.ws.dao.KeywordFolderImportDAO;
import com.myurldb.ws.data.KeywordFolderImportDataObject;


public class DefaultKeywordFolderImportDAO extends DefaultDAOBase implements KeywordFolderImportDAO
{
    private static final Logger log = Logger.getLogger(DefaultKeywordFolderImportDAO.class.getName()); 

    // Returns the keywordFolderImport for the given guid/key.
    // Returns null if none is found in the datastore.
    @Override
    public KeywordFolderImportDataObject getKeywordFolderImport(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: guid = " + guid);

        KeywordFolderImportDataObject keywordFolderImport = null;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            //Transaction tx = pm.currentTransaction();
            try {
                //tx.setOptimistic(true);
                //tx.begin();
                //Key key = KeywordFolderImportDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = KeywordFolderImportDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                keywordFolderImport = pm.getObjectById(KeywordFolderImportDataObject.class, key);
                //tx.commit();
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve keywordFolderImport for guid = " + guid, ex);
                throw new ResourceNotFoundException(ex);
            } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        log.finer("END");
	    return keywordFolderImport;
	}

    @Override
    public List<KeywordFolderImportDataObject> getKeywordFolderImports(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // temporary implementation
        List<KeywordFolderImportDataObject> keywordFolderImports = null;
        if(guids != null && !guids.isEmpty()) {
            keywordFolderImports = new ArrayList<KeywordFolderImportDataObject>();
            for(String guid : guids) {
                KeywordFolderImportDataObject obj = getKeywordFolderImport(guid); 
                keywordFolderImports.add(obj);
            }
	    }

        log.finer("END");
        return keywordFolderImports;
    }

    @Override
    public List<KeywordFolderImportDataObject> getAllKeywordFolderImports() throws BaseException
	{
	    return getAllKeywordFolderImports(null, null, null);
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<KeywordFolderImportDataObject> getAllKeywordFolderImports(String ordering, Long offset, Integer count) throws BaseException
	{
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count);

    	List<KeywordFolderImportDataObject> keywordFolderImports = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(KeywordFolderImportDataObject.class);
            if(ordering != null) {
                q.setOrdering(ordering);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            keywordFolderImports = (List<KeywordFolderImportDataObject>) q.execute();
            if(keywordFolderImports != null) {
                keywordFolderImports.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            /*
            // ???
            Collection<KeywordFolderImportDataObject> rs_keywordFolderImports = (Collection<KeywordFolderImportDataObject>) q.execute();
            if(rs_keywordFolderImports == null) {
                log.log(Level.WARNING, "Failed to retrieve all keywordFolderImports.");
                keywordFolderImports = new ArrayList<KeywordFolderImportDataObject>();  // ???           
            } else {
                keywordFolderImports = new ArrayList<KeywordFolderImportDataObject>(pm.detachCopyAll(rs_keywordFolderImports));
            }
            */
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all keywordFolderImports.", ex);
            //keywordFolderImports = new ArrayList<KeywordFolderImportDataObject>();  // ???
            throw new DataStoreException("Failed to retrieve all keywordFolderImports.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return keywordFolderImports;
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<String> getAllKeywordFolderImportKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count);

    	List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery("select key from " + KeywordFolderImportDataObject.class.getName());  // ???
            if(ordering != null) {
                q.setOrdering(ordering);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            keys = (List<String>) q.execute();
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all KeywordFolderImport keys.", ex);
            throw new DataStoreException("Failed to retrieve all KeywordFolderImport keys.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return keys;
    }

    @Override
	public List<KeywordFolderImportDataObject> findKeywordFolderImports(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findKeywordFolderImports(filter, ordering, params, values, null, null);
    }

    @Override
	public List<KeywordFolderImportDataObject> findKeywordFolderImports(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findKeywordFolderImports(filter, ordering, params, values, null, null, offset, count);
	}

    @SuppressWarnings("unchecked")
    @Override
	public List<KeywordFolderImportDataObject> findKeywordFolderImports(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
        if(log.isLoggable(Level.INFO)) log.info("DefaultKeywordFolderImportDAO.findKeywordFolderImports(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("findKeywordFolderImports() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        List<KeywordFolderImportDataObject> keywordFolderImports = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(KeywordFolderImportDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                keywordFolderImports = (List<KeywordFolderImportDataObject>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                keywordFolderImports = (List<KeywordFolderImportDataObject>) q.execute();
            }
            if(keywordFolderImports != null) {
                keywordFolderImports.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //while($TypeUtil.deCapitalizePluralize($typeName).iterator().hasNext()) {
            //    for(KeywordFolderImportDataObject dobj : keywordFolderImports) {
            //        //
            //    }
            //}
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find keywordFolderImports because index is missing.", ex);
            //keywordFolderImports = new ArrayList<KeywordFolderImportDataObject>();  // ???
            throw new DataStoreException("Failed to find keywordFolderImports because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find keywordFolderImports meeting the criterion.", ex);
            //keywordFolderImports = new ArrayList<KeywordFolderImportDataObject>();  // ???
            throw new DataStoreException("Failed to find keywordFolderImports meeting the criterion.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return keywordFolderImports;
	}

    @SuppressWarnings("unchecked")
    @Override
    public List<String> findKeywordFolderImportKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultKeywordFolderImportDAO.findKeywordFolderImportKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery("select key from " + KeywordFolderImportDataObject.class.getName());  // ???
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                keys = (List<String>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                keys = (List<String>) q.execute();
            }
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find KeywordFolderImport keys because index is missing.", ex);
            throw new DataStoreException("Failed to find KeywordFolderImport keys because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find KeywordFolderImport keys meeting the criterion.", ex);
            throw new DataStoreException("Failed to find KeywordFolderImport keys meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END");
        return keys;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultKeywordFolderImportDAO.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("getCount() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        Long count = 0L; 
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(KeywordFolderImportDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(aggregate == null || aggregate.isEmpty()) {
                aggregate = "count(this)";  // By default, count. Other examples: "max(this.createdTime)", avg, sum, etc. (Note: These are currently not supported by Google App Engine.)
            }
            q.setResult(aggregate);
            //q.setResultClass(Long.class);
            // For now, we only support a single aggregate function. (Eg, no comma-separated list.)
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                count = (Long) q.execute();
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to get keywordFolderImport count because index is missing.", ex);
            throw new DataStoreException("Failed to get keywordFolderImport count because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to get keywordFolderImport count meeting the criterion.", ex);
            throw new DataStoreException("Failed to get keywordFolderImport count meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(count == null) {
            count = 0L;
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

	// Stores the keywordFolderImport in the datastore and returns its guid.
	// Returns null if the operation fails.
    private String storeKeywordFolderImport(KeywordFolderImportDataObject keywordFolderImport) throws BaseException
    {
        log.fine("storeKeywordFolderImport() called.");

        String guid = null;
        PersistenceManager pm = getPersistenceManager();
        try {
            // TBD: If the object on the datastore has non-null created date and the arg shortLink has no created date timestamp,
            //      do not overwrite the timestamp. How to do that???
            // For now, this is ok since we use "overwrite/replace" semantics (not partial update).
            Long createdTime = keywordFolderImport.getCreatedTime();
            if(createdTime == null) {
                createdTime = System.currentTimeMillis();
                keywordFolderImport.setCreatedTime(createdTime);
            }
            Long modifiedTime = keywordFolderImport.getModifiedTime();
            if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
                keywordFolderImport.setModifiedTime(createdTime);
            }
            pm.makePersistent(keywordFolderImport); 
            // TBD: How do you know the makePersistent() call was successful???
            guid = keywordFolderImport.getGuid();
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to store keywordFolderImport because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to store keywordFolderImport because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to store keywordFolderImport.", ex);
            throw new DataStoreException("Failed to store keywordFolderImport.", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINE)) log.fine("storeKeywordFolderImport(): guid = " + guid);
        return guid;
    }

    @Override
    public String createKeywordFolderImport(KeywordFolderImportDataObject keywordFolderImport) throws BaseException
    {
        // The createdTime field will be automatically set in storeKeywordFolderImport().
        //Long createdTime = keywordFolderImport.getCreatedTime();
        //if(createdTime == null) {
        //    createdTime = System.currentTimeMillis();
        //    keywordFolderImport.setCreatedTime(createdTime);
        //}
        //Long modifiedTime = keywordFolderImport.getModifiedTime();
        //if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
        //    keywordFolderImport.setModifiedTime(createdTime);
        //}
        return storeKeywordFolderImport(keywordFolderImport);
    }

    @Override
	public Boolean updateKeywordFolderImport(KeywordFolderImportDataObject keywordFolderImport) throws BaseException
	{
	    // Modify the modifiedTime timestamp.
	    // CreatedTime should, normally, be non-null.
	    // If null, however, it will be automatically set in storeKeywordFolderImport()
	    // (in which case modifiedTime might be updated again).
	    keywordFolderImport.setModifiedTime(System.currentTimeMillis());
	    String guid = storeKeywordFolderImport(keywordFolderImport);
	    if(guid != null) {
	        return true;
	    } else {
	        return false;
	    }
	}
	
    @Override
    public Boolean deleteKeywordFolderImport(KeywordFolderImportDataObject keywordFolderImport) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        PersistenceManager pm = getPersistenceManager();
        try {
            pm.deletePersistent(keywordFolderImport);
            // TBD: How do you know the deletePersistent() call was successful???
            suc = true;
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to delete keywordFolderImport because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete keywordFolderImport because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete keywordFolderImport.", ex);
            throw new DataStoreException("Failed to delete keywordFolderImport.", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteKeywordFolderImport(String guid) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            try {
                //Key key = KeywordFolderImportDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = KeywordFolderImportDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                KeywordFolderImportDataObject keywordFolderImport = pm.getObjectById(KeywordFolderImportDataObject.class, key);
                pm.deletePersistent(keywordFolderImport);
                // TBD: How do you know the deletePersistent() call was successful???
                suc = true;
            } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
                // Datastore is read-only, possibly due to the Google App Engine maintenance.
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete keywordFolderImport because the datastore is currently read-only. guid = " + guid, ex);
                throw new ServiceUnavailableException("Failed to delete keywordFolderImport because the datastore is currently read-only.", ex);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete keywordFolderImport for guid = " + guid, ex);
                throw new DataStoreException("Failed to delete keywordFolderImport for guid = " + guid, ex);
            } finally {
                try {
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
	}

    @Override
    public Long deleteKeywordFolderImports(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultKeywordFolderImportDAO.deleteKeywordFolderImports(): filter=" + filter + "; params=" + params);

        // TBD: Delete-by-query requests should be carefully checked because of its potential harms.
        if(filter == null || filter.isEmpty()) {
            throw new RequestConflictException("deleteAll() without filter is not supported for performance/safety reasons");
        }

        Long count = 0L;
        PersistenceManager pm = getPersistenceManager();
        try {
            Query q = pm.newQuery(KeywordFolderImportDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.deletePersistentAll(values.toArray(new Object[0]));
            } else {
                count = (Long) q.deletePersistentAll();
            }
            // TBD: How do you know the deletePersistent() call was successful???
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to deletekeywordFolderImports because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete keywordFolderImports because the datastore is currently read-only.", ex);
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to delete keywordFolderImports because index is missing", ex);
            throw new DataStoreException("Failed to delete keywordFolderImports because index is missing", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete keywordFolderImports", ex);
            throw new DataStoreException("Failed to delete keywordFolderImports", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
