package com.myurldb.ws.dao.base;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

//import com.google.appengine.api.datastore.Key;

import com.myurldb.ws.core.GUID;
import com.myurldb.ws.config.Config;
import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.DataStoreException;
import com.myurldb.ws.exception.ServiceUnavailableException;
import com.myurldb.ws.exception.RequestConflictException;
import com.myurldb.ws.exception.ResourceNotFoundException;
import com.myurldb.ws.exception.NotImplementedException;
import com.myurldb.ws.dao.KeywordLinkDAO;
import com.myurldb.ws.data.KeywordLinkDataObject;
import com.myurldb.ws.search.gae.KeywordLinkIndexBuilder;


public class DefaultKeywordLinkDAO extends DefaultDAOBase implements KeywordLinkDAO
{
    private static final Logger log = Logger.getLogger(DefaultKeywordLinkDAO.class.getName()); 

    // Returns the keywordLink for the given guid/key.
    // Returns null if none is found in the datastore.
    @Override
    public KeywordLinkDataObject getKeywordLink(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: guid = " + guid);

        KeywordLinkDataObject keywordLink = null;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            //Transaction tx = pm.currentTransaction();
            try {
                //tx.setOptimistic(true);
                //tx.begin();
                //Key key = KeywordLinkDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = KeywordLinkDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                keywordLink = pm.getObjectById(KeywordLinkDataObject.class, key);
                //tx.commit();
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve keywordLink for guid = " + guid, ex);
                throw new ResourceNotFoundException(ex);
            } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        log.finer("END");
	    return keywordLink;
	}

    @Override
    public List<KeywordLinkDataObject> getKeywordLinks(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // temporary implementation
        List<KeywordLinkDataObject> keywordLinks = null;
        if(guids != null && !guids.isEmpty()) {
            keywordLinks = new ArrayList<KeywordLinkDataObject>();
            for(String guid : guids) {
                KeywordLinkDataObject obj = getKeywordLink(guid); 
                keywordLinks.add(obj);
            }
	    }

        log.finer("END");
        return keywordLinks;
    }

    @Override
    public List<KeywordLinkDataObject> getAllKeywordLinks() throws BaseException
	{
	    return getAllKeywordLinks(null, null, null);
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<KeywordLinkDataObject> getAllKeywordLinks(String ordering, Long offset, Integer count) throws BaseException
	{
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count);

    	List<KeywordLinkDataObject> keywordLinks = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(KeywordLinkDataObject.class);
            if(ordering != null) {
                q.setOrdering(ordering);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            keywordLinks = (List<KeywordLinkDataObject>) q.execute();
            if(keywordLinks != null) {
                keywordLinks.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            /*
            // ???
            Collection<KeywordLinkDataObject> rs_keywordLinks = (Collection<KeywordLinkDataObject>) q.execute();
            if(rs_keywordLinks == null) {
                log.log(Level.WARNING, "Failed to retrieve all keywordLinks.");
                keywordLinks = new ArrayList<KeywordLinkDataObject>();  // ???           
            } else {
                keywordLinks = new ArrayList<KeywordLinkDataObject>(pm.detachCopyAll(rs_keywordLinks));
            }
            */
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all keywordLinks.", ex);
            //keywordLinks = new ArrayList<KeywordLinkDataObject>();  // ???
            throw new DataStoreException("Failed to retrieve all keywordLinks.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return keywordLinks;
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<String> getAllKeywordLinkKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count);

    	List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery("select key from " + KeywordLinkDataObject.class.getName());  // ???
            if(ordering != null) {
                q.setOrdering(ordering);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            keys = (List<String>) q.execute();
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all KeywordLink keys.", ex);
            throw new DataStoreException("Failed to retrieve all KeywordLink keys.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return keys;
    }

    @Override
	public List<KeywordLinkDataObject> findKeywordLinks(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findKeywordLinks(filter, ordering, params, values, null, null);
    }

    @Override
	public List<KeywordLinkDataObject> findKeywordLinks(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findKeywordLinks(filter, ordering, params, values, null, null, offset, count);
	}

    @SuppressWarnings("unchecked")
    @Override
	public List<KeywordLinkDataObject> findKeywordLinks(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
        if(log.isLoggable(Level.INFO)) log.info("DefaultKeywordLinkDAO.findKeywordLinks(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("findKeywordLinks() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        List<KeywordLinkDataObject> keywordLinks = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(KeywordLinkDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                keywordLinks = (List<KeywordLinkDataObject>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                keywordLinks = (List<KeywordLinkDataObject>) q.execute();
            }
            if(keywordLinks != null) {
                keywordLinks.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //while($TypeUtil.deCapitalizePluralize($typeName).iterator().hasNext()) {
            //    for(KeywordLinkDataObject dobj : keywordLinks) {
            //        //
            //    }
            //}
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find keywordLinks because index is missing.", ex);
            //keywordLinks = new ArrayList<KeywordLinkDataObject>();  // ???
            throw new DataStoreException("Failed to find keywordLinks because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find keywordLinks meeting the criterion.", ex);
            //keywordLinks = new ArrayList<KeywordLinkDataObject>();  // ???
            throw new DataStoreException("Failed to find keywordLinks meeting the criterion.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return keywordLinks;
	}

    @SuppressWarnings("unchecked")
    @Override
    public List<String> findKeywordLinkKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultKeywordLinkDAO.findKeywordLinkKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery("select key from " + KeywordLinkDataObject.class.getName());  // ???
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                keys = (List<String>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                keys = (List<String>) q.execute();
            }
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find KeywordLink keys because index is missing.", ex);
            throw new DataStoreException("Failed to find KeywordLink keys because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find KeywordLink keys meeting the criterion.", ex);
            throw new DataStoreException("Failed to find KeywordLink keys meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END");
        return keys;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultKeywordLinkDAO.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("getCount() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        Long count = 0L; 
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(KeywordLinkDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(aggregate == null || aggregate.isEmpty()) {
                aggregate = "count(this)";  // By default, count. Other examples: "max(this.createdTime)", avg, sum, etc. (Note: These are currently not supported by Google App Engine.)
            }
            q.setResult(aggregate);
            //q.setResultClass(Long.class);
            // For now, we only support a single aggregate function. (Eg, no comma-separated list.)
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                count = (Long) q.execute();
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to get keywordLink count because index is missing.", ex);
            throw new DataStoreException("Failed to get keywordLink count because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to get keywordLink count meeting the criterion.", ex);
            throw new DataStoreException("Failed to get keywordLink count meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(count == null) {
            count = 0L;
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

	// Stores the keywordLink in the datastore and returns its guid.
	// Returns null if the operation fails.
    private String storeKeywordLink(KeywordLinkDataObject keywordLink) throws BaseException
    {
        log.fine("storeKeywordLink() called.");

        String guid = null;
        PersistenceManager pm = getPersistenceManager();
        try {
            // TBD: If the object on the datastore has non-null created date and the arg shortLink has no created date timestamp,
            //      do not overwrite the timestamp. How to do that???
            // For now, this is ok since we use "overwrite/replace" semantics (not partial update).
            Long createdTime = keywordLink.getCreatedTime();
            if(createdTime == null) {
                createdTime = System.currentTimeMillis();
                keywordLink.setCreatedTime(createdTime);
            }
            Long modifiedTime = keywordLink.getModifiedTime();
            if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
                keywordLink.setModifiedTime(createdTime);
            }
            pm.makePersistent(keywordLink); 
            // TBD: How do you know the makePersistent() call was successful???
            guid = keywordLink.getGuid();
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to store keywordLink because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to store keywordLink because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to store keywordLink.", ex);
            throw new DataStoreException("Failed to store keywordLink.", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

	    // TBD:
	    if(Config.getInstance().isSearchEnabled()) {
            log.fine("Calling addDocument()... for KeywordLink.");
	    	KeywordLinkIndexBuilder builder = new KeywordLinkIndexBuilder();
	    	builder.addDocument(keywordLink);
	    }

        if(log.isLoggable(Level.FINE)) log.fine("storeKeywordLink(): guid = " + guid);
        return guid;
    }

    @Override
    public String createKeywordLink(KeywordLinkDataObject keywordLink) throws BaseException
    {
        // The createdTime field will be automatically set in storeKeywordLink().
        //Long createdTime = keywordLink.getCreatedTime();
        //if(createdTime == null) {
        //    createdTime = System.currentTimeMillis();
        //    keywordLink.setCreatedTime(createdTime);
        //}
        //Long modifiedTime = keywordLink.getModifiedTime();
        //if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
        //    keywordLink.setModifiedTime(createdTime);
        //}
        return storeKeywordLink(keywordLink);
    }

    @Override
	public Boolean updateKeywordLink(KeywordLinkDataObject keywordLink) throws BaseException
	{
	    // Modify the modifiedTime timestamp.
	    // CreatedTime should, normally, be non-null.
	    // If null, however, it will be automatically set in storeKeywordLink()
	    // (in which case modifiedTime might be updated again).
	    keywordLink.setModifiedTime(System.currentTimeMillis());
	    String guid = storeKeywordLink(keywordLink);
	    if(guid != null) {
	        return true;
	    } else {
	        return false;
	    }
	}
	
    @Override
    public Boolean deleteKeywordLink(KeywordLinkDataObject keywordLink) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        PersistenceManager pm = getPersistenceManager();
        try {
            pm.deletePersistent(keywordLink);
            // TBD: How do you know the deletePersistent() call was successful???
            suc = true;
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to delete keywordLink because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete keywordLink because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete keywordLink.", ex);
            throw new DataStoreException("Failed to delete keywordLink.", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

	    // TBD:
	    if(Config.getInstance().isSearchEnabled()) {
            log.fine("Calling removeDocument()... for KeywordLink.");
	    	KeywordLinkIndexBuilder builder = new KeywordLinkIndexBuilder();
	    	builder.removeDocument(keywordLink.getGuid());
	    }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteKeywordLink(String guid) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            try {
                //Key key = KeywordLinkDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = KeywordLinkDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                KeywordLinkDataObject keywordLink = pm.getObjectById(KeywordLinkDataObject.class, key);
                pm.deletePersistent(keywordLink);
                // TBD: How do you know the deletePersistent() call was successful???
                suc = true;
            } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
                // Datastore is read-only, possibly due to the Google App Engine maintenance.
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete keywordLink because the datastore is currently read-only. guid = " + guid, ex);
                throw new ServiceUnavailableException("Failed to delete keywordLink because the datastore is currently read-only.", ex);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete keywordLink for guid = " + guid, ex);
                throw new DataStoreException("Failed to delete keywordLink for guid = " + guid, ex);
            } finally {
                try {
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

	    // TBD:
	    if(Config.getInstance().isSearchEnabled()) {
            log.fine("Calling removeDocument()... for KeywordLink.");
	    	KeywordLinkIndexBuilder builder = new KeywordLinkIndexBuilder();
	    	builder.removeDocument(guid);
	    }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
	}

    @Override
    public Long deleteKeywordLinks(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultKeywordLinkDAO.deleteKeywordLinks(): filter=" + filter + "; params=" + params);

        // TBD: Delete-by-query requests should be carefully checked because of its potential harms.
        if(filter == null || filter.isEmpty()) {
            throw new RequestConflictException("deleteAll() without filter is not supported for performance/safety reasons");
        }

        Long count = 0L;
        PersistenceManager pm = getPersistenceManager();
        try {
            Query q = pm.newQuery(KeywordLinkDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.deletePersistentAll(values.toArray(new Object[0]));
            } else {
                count = (Long) q.deletePersistentAll();
            }
            // TBD: How do you know the deletePersistent() call was successful???
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to deletekeywordLinks because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete keywordLinks because the datastore is currently read-only.", ex);
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to delete keywordLinks because index is missing", ex);
            throw new DataStoreException("Failed to delete keywordLinks because index is missing", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete keywordLinks", ex);
            throw new DataStoreException("Failed to delete keywordLinks", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

	    // TBD:
	    //if(Config.getInstance().isSearchEnabled()) {
        //    log.fine("Calling removeDocument()... for KeywordLink.");
	    //    KeywordLinkIndexBuilder builder = new KeywordLinkIndexBuilder();
	    //    builder.removeDocument(guids);
	    //}

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
