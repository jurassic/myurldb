package com.myurldb.ws.dao.base;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

//import com.google.appengine.api.datastore.Key;

import com.myurldb.ws.core.GUID;
import com.myurldb.ws.config.Config;
import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.DataStoreException;
import com.myurldb.ws.exception.ServiceUnavailableException;
import com.myurldb.ws.exception.RequestConflictException;
import com.myurldb.ws.exception.ResourceNotFoundException;
import com.myurldb.ws.exception.NotImplementedException;
import com.myurldb.ws.dao.LinkAlbumDAO;
import com.myurldb.ws.data.LinkAlbumDataObject;
import com.myurldb.ws.search.gae.LinkAlbumIndexBuilder;


public class DefaultLinkAlbumDAO extends DefaultDAOBase implements LinkAlbumDAO
{
    private static final Logger log = Logger.getLogger(DefaultLinkAlbumDAO.class.getName()); 

    // Returns the linkAlbum for the given guid/key.
    // Returns null if none is found in the datastore.
    @Override
    public LinkAlbumDataObject getLinkAlbum(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: guid = " + guid);

        LinkAlbumDataObject linkAlbum = null;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            //Transaction tx = pm.currentTransaction();
            try {
                //tx.setOptimistic(true);
                //tx.begin();
                //Key key = LinkAlbumDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = LinkAlbumDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                linkAlbum = pm.getObjectById(LinkAlbumDataObject.class, key);
                //tx.commit();
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve linkAlbum for guid = " + guid, ex);
                throw new ResourceNotFoundException(ex);
            } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        log.finer("END");
	    return linkAlbum;
	}

    @Override
    public List<LinkAlbumDataObject> getLinkAlbums(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // temporary implementation
        List<LinkAlbumDataObject> linkAlbums = null;
        if(guids != null && !guids.isEmpty()) {
            linkAlbums = new ArrayList<LinkAlbumDataObject>();
            for(String guid : guids) {
                LinkAlbumDataObject obj = getLinkAlbum(guid); 
                linkAlbums.add(obj);
            }
	    }

        log.finer("END");
        return linkAlbums;
    }

    @Override
    public List<LinkAlbumDataObject> getAllLinkAlbums() throws BaseException
	{
	    return getAllLinkAlbums(null, null, null);
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<LinkAlbumDataObject> getAllLinkAlbums(String ordering, Long offset, Integer count) throws BaseException
	{
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count);

    	List<LinkAlbumDataObject> linkAlbums = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(LinkAlbumDataObject.class);
            if(ordering != null) {
                q.setOrdering(ordering);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            linkAlbums = (List<LinkAlbumDataObject>) q.execute();
            if(linkAlbums != null) {
                linkAlbums.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            /*
            // ???
            Collection<LinkAlbumDataObject> rs_linkAlbums = (Collection<LinkAlbumDataObject>) q.execute();
            if(rs_linkAlbums == null) {
                log.log(Level.WARNING, "Failed to retrieve all linkAlbums.");
                linkAlbums = new ArrayList<LinkAlbumDataObject>();  // ???           
            } else {
                linkAlbums = new ArrayList<LinkAlbumDataObject>(pm.detachCopyAll(rs_linkAlbums));
            }
            */
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all linkAlbums.", ex);
            //linkAlbums = new ArrayList<LinkAlbumDataObject>();  // ???
            throw new DataStoreException("Failed to retrieve all linkAlbums.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return linkAlbums;
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<String> getAllLinkAlbumKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count);

    	List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery("select key from " + LinkAlbumDataObject.class.getName());  // ???
            if(ordering != null) {
                q.setOrdering(ordering);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            keys = (List<String>) q.execute();
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all LinkAlbum keys.", ex);
            throw new DataStoreException("Failed to retrieve all LinkAlbum keys.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return keys;
    }

    @Override
	public List<LinkAlbumDataObject> findLinkAlbums(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findLinkAlbums(filter, ordering, params, values, null, null);
    }

    @Override
	public List<LinkAlbumDataObject> findLinkAlbums(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findLinkAlbums(filter, ordering, params, values, null, null, offset, count);
	}

    @SuppressWarnings("unchecked")
    @Override
	public List<LinkAlbumDataObject> findLinkAlbums(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
        if(log.isLoggable(Level.INFO)) log.info("DefaultLinkAlbumDAO.findLinkAlbums(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("findLinkAlbums() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        List<LinkAlbumDataObject> linkAlbums = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(LinkAlbumDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                linkAlbums = (List<LinkAlbumDataObject>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                linkAlbums = (List<LinkAlbumDataObject>) q.execute();
            }
            if(linkAlbums != null) {
                linkAlbums.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //while($TypeUtil.deCapitalizePluralize($typeName).iterator().hasNext()) {
            //    for(LinkAlbumDataObject dobj : linkAlbums) {
            //        //
            //    }
            //}
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find linkAlbums because index is missing.", ex);
            //linkAlbums = new ArrayList<LinkAlbumDataObject>();  // ???
            throw new DataStoreException("Failed to find linkAlbums because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find linkAlbums meeting the criterion.", ex);
            //linkAlbums = new ArrayList<LinkAlbumDataObject>();  // ???
            throw new DataStoreException("Failed to find linkAlbums meeting the criterion.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return linkAlbums;
	}

    @SuppressWarnings("unchecked")
    @Override
    public List<String> findLinkAlbumKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultLinkAlbumDAO.findLinkAlbumKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery("select key from " + LinkAlbumDataObject.class.getName());  // ???
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                keys = (List<String>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                keys = (List<String>) q.execute();
            }
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find LinkAlbum keys because index is missing.", ex);
            throw new DataStoreException("Failed to find LinkAlbum keys because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find LinkAlbum keys meeting the criterion.", ex);
            throw new DataStoreException("Failed to find LinkAlbum keys meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END");
        return keys;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultLinkAlbumDAO.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("getCount() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        Long count = 0L; 
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(LinkAlbumDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(aggregate == null || aggregate.isEmpty()) {
                aggregate = "count(this)";  // By default, count. Other examples: "max(this.createdTime)", avg, sum, etc. (Note: These are currently not supported by Google App Engine.)
            }
            q.setResult(aggregate);
            //q.setResultClass(Long.class);
            // For now, we only support a single aggregate function. (Eg, no comma-separated list.)
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                count = (Long) q.execute();
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to get linkAlbum count because index is missing.", ex);
            throw new DataStoreException("Failed to get linkAlbum count because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to get linkAlbum count meeting the criterion.", ex);
            throw new DataStoreException("Failed to get linkAlbum count meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(count == null) {
            count = 0L;
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

	// Stores the linkAlbum in the datastore and returns its guid.
	// Returns null if the operation fails.
    private String storeLinkAlbum(LinkAlbumDataObject linkAlbum) throws BaseException
    {
        log.fine("storeLinkAlbum() called.");

        String guid = null;
        PersistenceManager pm = getPersistenceManager();
        try {
            // TBD: If the object on the datastore has non-null created date and the arg shortLink has no created date timestamp,
            //      do not overwrite the timestamp. How to do that???
            // For now, this is ok since we use "overwrite/replace" semantics (not partial update).
            Long createdTime = linkAlbum.getCreatedTime();
            if(createdTime == null) {
                createdTime = System.currentTimeMillis();
                linkAlbum.setCreatedTime(createdTime);
            }
            Long modifiedTime = linkAlbum.getModifiedTime();
            if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
                linkAlbum.setModifiedTime(createdTime);
            }
            pm.makePersistent(linkAlbum); 
            // TBD: How do you know the makePersistent() call was successful???
            guid = linkAlbum.getGuid();
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to store linkAlbum because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to store linkAlbum because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to store linkAlbum.", ex);
            throw new DataStoreException("Failed to store linkAlbum.", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

	    // TBD:
	    if(Config.getInstance().isSearchEnabled()) {
            log.fine("Calling addDocument()... for LinkAlbum.");
	    	LinkAlbumIndexBuilder builder = new LinkAlbumIndexBuilder();
	    	builder.addDocument(linkAlbum);
	    }

        if(log.isLoggable(Level.FINE)) log.fine("storeLinkAlbum(): guid = " + guid);
        return guid;
    }

    @Override
    public String createLinkAlbum(LinkAlbumDataObject linkAlbum) throws BaseException
    {
        // The createdTime field will be automatically set in storeLinkAlbum().
        //Long createdTime = linkAlbum.getCreatedTime();
        //if(createdTime == null) {
        //    createdTime = System.currentTimeMillis();
        //    linkAlbum.setCreatedTime(createdTime);
        //}
        //Long modifiedTime = linkAlbum.getModifiedTime();
        //if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
        //    linkAlbum.setModifiedTime(createdTime);
        //}
        return storeLinkAlbum(linkAlbum);
    }

    @Override
	public Boolean updateLinkAlbum(LinkAlbumDataObject linkAlbum) throws BaseException
	{
	    // Modify the modifiedTime timestamp.
	    // CreatedTime should, normally, be non-null.
	    // If null, however, it will be automatically set in storeLinkAlbum()
	    // (in which case modifiedTime might be updated again).
	    linkAlbum.setModifiedTime(System.currentTimeMillis());
	    String guid = storeLinkAlbum(linkAlbum);
	    if(guid != null) {
	        return true;
	    } else {
	        return false;
	    }
	}
	
    @Override
    public Boolean deleteLinkAlbum(LinkAlbumDataObject linkAlbum) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        PersistenceManager pm = getPersistenceManager();
        try {
            pm.deletePersistent(linkAlbum);
            // TBD: How do you know the deletePersistent() call was successful???
            suc = true;
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to delete linkAlbum because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete linkAlbum because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete linkAlbum.", ex);
            throw new DataStoreException("Failed to delete linkAlbum.", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

	    // TBD:
	    if(Config.getInstance().isSearchEnabled()) {
            log.fine("Calling removeDocument()... for LinkAlbum.");
	    	LinkAlbumIndexBuilder builder = new LinkAlbumIndexBuilder();
	    	builder.removeDocument(linkAlbum.getGuid());
	    }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteLinkAlbum(String guid) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            try {
                //Key key = LinkAlbumDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = LinkAlbumDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                LinkAlbumDataObject linkAlbum = pm.getObjectById(LinkAlbumDataObject.class, key);
                pm.deletePersistent(linkAlbum);
                // TBD: How do you know the deletePersistent() call was successful???
                suc = true;
            } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
                // Datastore is read-only, possibly due to the Google App Engine maintenance.
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete linkAlbum because the datastore is currently read-only. guid = " + guid, ex);
                throw new ServiceUnavailableException("Failed to delete linkAlbum because the datastore is currently read-only.", ex);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete linkAlbum for guid = " + guid, ex);
                throw new DataStoreException("Failed to delete linkAlbum for guid = " + guid, ex);
            } finally {
                try {
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

	    // TBD:
	    if(Config.getInstance().isSearchEnabled()) {
            log.fine("Calling removeDocument()... for LinkAlbum.");
	    	LinkAlbumIndexBuilder builder = new LinkAlbumIndexBuilder();
	    	builder.removeDocument(guid);
	    }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
	}

    @Override
    public Long deleteLinkAlbums(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultLinkAlbumDAO.deleteLinkAlbums(): filter=" + filter + "; params=" + params);

        // TBD: Delete-by-query requests should be carefully checked because of its potential harms.
        if(filter == null || filter.isEmpty()) {
            throw new RequestConflictException("deleteAll() without filter is not supported for performance/safety reasons");
        }

        Long count = 0L;
        PersistenceManager pm = getPersistenceManager();
        try {
            Query q = pm.newQuery(LinkAlbumDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.deletePersistentAll(values.toArray(new Object[0]));
            } else {
                count = (Long) q.deletePersistentAll();
            }
            // TBD: How do you know the deletePersistent() call was successful???
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to deletelinkAlbums because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete linkAlbums because the datastore is currently read-only.", ex);
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to delete linkAlbums because index is missing", ex);
            throw new DataStoreException("Failed to delete linkAlbums because index is missing", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete linkAlbums", ex);
            throw new DataStoreException("Failed to delete linkAlbums", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

	    // TBD:
	    //if(Config.getInstance().isSearchEnabled()) {
        //    log.fine("Calling removeDocument()... for LinkAlbum.");
	    //    LinkAlbumIndexBuilder builder = new LinkAlbumIndexBuilder();
	    //    builder.removeDocument(guids);
	    //}

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
