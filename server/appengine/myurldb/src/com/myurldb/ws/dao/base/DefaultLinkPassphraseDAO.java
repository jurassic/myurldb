package com.myurldb.ws.dao.base;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

//import com.google.appengine.api.datastore.Key;

import com.myurldb.ws.core.GUID;
import com.myurldb.ws.config.Config;
import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.DataStoreException;
import com.myurldb.ws.exception.ServiceUnavailableException;
import com.myurldb.ws.exception.RequestConflictException;
import com.myurldb.ws.exception.ResourceNotFoundException;
import com.myurldb.ws.exception.NotImplementedException;
import com.myurldb.ws.dao.LinkPassphraseDAO;
import com.myurldb.ws.data.LinkPassphraseDataObject;


public class DefaultLinkPassphraseDAO extends DefaultDAOBase implements LinkPassphraseDAO
{
    private static final Logger log = Logger.getLogger(DefaultLinkPassphraseDAO.class.getName()); 

    // Returns the linkPassphrase for the given guid/key.
    // Returns null if none is found in the datastore.
    @Override
    public LinkPassphraseDataObject getLinkPassphrase(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: guid = " + guid);

        LinkPassphraseDataObject linkPassphrase = null;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            //Transaction tx = pm.currentTransaction();
            try {
                //tx.setOptimistic(true);
                //tx.begin();
                //Key key = LinkPassphraseDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = LinkPassphraseDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                linkPassphrase = pm.getObjectById(LinkPassphraseDataObject.class, key);
                //tx.commit();
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve linkPassphrase for guid = " + guid, ex);
                throw new ResourceNotFoundException(ex);
            } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        log.finer("END");
	    return linkPassphrase;
	}

    @Override
    public List<LinkPassphraseDataObject> getLinkPassphrases(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // temporary implementation
        List<LinkPassphraseDataObject> linkPassphrases = null;
        if(guids != null && !guids.isEmpty()) {
            linkPassphrases = new ArrayList<LinkPassphraseDataObject>();
            for(String guid : guids) {
                LinkPassphraseDataObject obj = getLinkPassphrase(guid); 
                linkPassphrases.add(obj);
            }
	    }

        log.finer("END");
        return linkPassphrases;
    }

    @Override
    public List<LinkPassphraseDataObject> getAllLinkPassphrases() throws BaseException
	{
	    return getAllLinkPassphrases(null, null, null);
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<LinkPassphraseDataObject> getAllLinkPassphrases(String ordering, Long offset, Integer count) throws BaseException
	{
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count);

    	List<LinkPassphraseDataObject> linkPassphrases = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(LinkPassphraseDataObject.class);
            if(ordering != null) {
                q.setOrdering(ordering);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            linkPassphrases = (List<LinkPassphraseDataObject>) q.execute();
            if(linkPassphrases != null) {
                linkPassphrases.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            /*
            // ???
            Collection<LinkPassphraseDataObject> rs_linkPassphrases = (Collection<LinkPassphraseDataObject>) q.execute();
            if(rs_linkPassphrases == null) {
                log.log(Level.WARNING, "Failed to retrieve all linkPassphrases.");
                linkPassphrases = new ArrayList<LinkPassphraseDataObject>();  // ???           
            } else {
                linkPassphrases = new ArrayList<LinkPassphraseDataObject>(pm.detachCopyAll(rs_linkPassphrases));
            }
            */
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all linkPassphrases.", ex);
            //linkPassphrases = new ArrayList<LinkPassphraseDataObject>();  // ???
            throw new DataStoreException("Failed to retrieve all linkPassphrases.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return linkPassphrases;
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<String> getAllLinkPassphraseKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count);

    	List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery("select key from " + LinkPassphraseDataObject.class.getName());  // ???
            if(ordering != null) {
                q.setOrdering(ordering);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            keys = (List<String>) q.execute();
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all LinkPassphrase keys.", ex);
            throw new DataStoreException("Failed to retrieve all LinkPassphrase keys.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return keys;
    }

    @Override
	public List<LinkPassphraseDataObject> findLinkPassphrases(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findLinkPassphrases(filter, ordering, params, values, null, null);
    }

    @Override
	public List<LinkPassphraseDataObject> findLinkPassphrases(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findLinkPassphrases(filter, ordering, params, values, null, null, offset, count);
	}

    @SuppressWarnings("unchecked")
    @Override
	public List<LinkPassphraseDataObject> findLinkPassphrases(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
        if(log.isLoggable(Level.INFO)) log.info("DefaultLinkPassphraseDAO.findLinkPassphrases(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("findLinkPassphrases() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        List<LinkPassphraseDataObject> linkPassphrases = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(LinkPassphraseDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                linkPassphrases = (List<LinkPassphraseDataObject>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                linkPassphrases = (List<LinkPassphraseDataObject>) q.execute();
            }
            if(linkPassphrases != null) {
                linkPassphrases.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //while($TypeUtil.deCapitalizePluralize($typeName).iterator().hasNext()) {
            //    for(LinkPassphraseDataObject dobj : linkPassphrases) {
            //        //
            //    }
            //}
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find linkPassphrases because index is missing.", ex);
            //linkPassphrases = new ArrayList<LinkPassphraseDataObject>();  // ???
            throw new DataStoreException("Failed to find linkPassphrases because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find linkPassphrases meeting the criterion.", ex);
            //linkPassphrases = new ArrayList<LinkPassphraseDataObject>();  // ???
            throw new DataStoreException("Failed to find linkPassphrases meeting the criterion.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return linkPassphrases;
	}

    @SuppressWarnings("unchecked")
    @Override
    public List<String> findLinkPassphraseKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultLinkPassphraseDAO.findLinkPassphraseKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery("select key from " + LinkPassphraseDataObject.class.getName());  // ???
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                keys = (List<String>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                keys = (List<String>) q.execute();
            }
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find LinkPassphrase keys because index is missing.", ex);
            throw new DataStoreException("Failed to find LinkPassphrase keys because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find LinkPassphrase keys meeting the criterion.", ex);
            throw new DataStoreException("Failed to find LinkPassphrase keys meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END");
        return keys;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultLinkPassphraseDAO.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("getCount() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        Long count = 0L; 
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(LinkPassphraseDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(aggregate == null || aggregate.isEmpty()) {
                aggregate = "count(this)";  // By default, count. Other examples: "max(this.createdTime)", avg, sum, etc. (Note: These are currently not supported by Google App Engine.)
            }
            q.setResult(aggregate);
            //q.setResultClass(Long.class);
            // For now, we only support a single aggregate function. (Eg, no comma-separated list.)
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                count = (Long) q.execute();
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to get linkPassphrase count because index is missing.", ex);
            throw new DataStoreException("Failed to get linkPassphrase count because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to get linkPassphrase count meeting the criterion.", ex);
            throw new DataStoreException("Failed to get linkPassphrase count meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(count == null) {
            count = 0L;
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

	// Stores the linkPassphrase in the datastore and returns its guid.
	// Returns null if the operation fails.
    private String storeLinkPassphrase(LinkPassphraseDataObject linkPassphrase) throws BaseException
    {
        log.fine("storeLinkPassphrase() called.");

        String guid = null;
        PersistenceManager pm = getPersistenceManager();
        try {
            // TBD: If the object on the datastore has non-null created date and the arg shortLink has no created date timestamp,
            //      do not overwrite the timestamp. How to do that???
            // For now, this is ok since we use "overwrite/replace" semantics (not partial update).
            Long createdTime = linkPassphrase.getCreatedTime();
            if(createdTime == null) {
                createdTime = System.currentTimeMillis();
                linkPassphrase.setCreatedTime(createdTime);
            }
            Long modifiedTime = linkPassphrase.getModifiedTime();
            if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
                linkPassphrase.setModifiedTime(createdTime);
            }
            pm.makePersistent(linkPassphrase); 
            // TBD: How do you know the makePersistent() call was successful???
            guid = linkPassphrase.getGuid();
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to store linkPassphrase because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to store linkPassphrase because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to store linkPassphrase.", ex);
            throw new DataStoreException("Failed to store linkPassphrase.", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINE)) log.fine("storeLinkPassphrase(): guid = " + guid);
        return guid;
    }

    @Override
    public String createLinkPassphrase(LinkPassphraseDataObject linkPassphrase) throws BaseException
    {
        // The createdTime field will be automatically set in storeLinkPassphrase().
        //Long createdTime = linkPassphrase.getCreatedTime();
        //if(createdTime == null) {
        //    createdTime = System.currentTimeMillis();
        //    linkPassphrase.setCreatedTime(createdTime);
        //}
        //Long modifiedTime = linkPassphrase.getModifiedTime();
        //if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
        //    linkPassphrase.setModifiedTime(createdTime);
        //}
        return storeLinkPassphrase(linkPassphrase);
    }

    @Override
	public Boolean updateLinkPassphrase(LinkPassphraseDataObject linkPassphrase) throws BaseException
	{
	    // Modify the modifiedTime timestamp.
	    // CreatedTime should, normally, be non-null.
	    // If null, however, it will be automatically set in storeLinkPassphrase()
	    // (in which case modifiedTime might be updated again).
	    linkPassphrase.setModifiedTime(System.currentTimeMillis());
	    String guid = storeLinkPassphrase(linkPassphrase);
	    if(guid != null) {
	        return true;
	    } else {
	        return false;
	    }
	}
	
    @Override
    public Boolean deleteLinkPassphrase(LinkPassphraseDataObject linkPassphrase) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        PersistenceManager pm = getPersistenceManager();
        try {
            pm.deletePersistent(linkPassphrase);
            // TBD: How do you know the deletePersistent() call was successful???
            suc = true;
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to delete linkPassphrase because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete linkPassphrase because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete linkPassphrase.", ex);
            throw new DataStoreException("Failed to delete linkPassphrase.", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteLinkPassphrase(String guid) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            try {
                //Key key = LinkPassphraseDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = LinkPassphraseDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                LinkPassphraseDataObject linkPassphrase = pm.getObjectById(LinkPassphraseDataObject.class, key);
                pm.deletePersistent(linkPassphrase);
                // TBD: How do you know the deletePersistent() call was successful???
                suc = true;
            } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
                // Datastore is read-only, possibly due to the Google App Engine maintenance.
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete linkPassphrase because the datastore is currently read-only. guid = " + guid, ex);
                throw new ServiceUnavailableException("Failed to delete linkPassphrase because the datastore is currently read-only.", ex);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete linkPassphrase for guid = " + guid, ex);
                throw new DataStoreException("Failed to delete linkPassphrase for guid = " + guid, ex);
            } finally {
                try {
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
	}

    @Override
    public Long deleteLinkPassphrases(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultLinkPassphraseDAO.deleteLinkPassphrases(): filter=" + filter + "; params=" + params);

        // TBD: Delete-by-query requests should be carefully checked because of its potential harms.
        if(filter == null || filter.isEmpty()) {
            throw new RequestConflictException("deleteAll() without filter is not supported for performance/safety reasons");
        }

        Long count = 0L;
        PersistenceManager pm = getPersistenceManager();
        try {
            Query q = pm.newQuery(LinkPassphraseDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.deletePersistentAll(values.toArray(new Object[0]));
            } else {
                count = (Long) q.deletePersistentAll();
            }
            // TBD: How do you know the deletePersistent() call was successful???
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to deletelinkPassphrases because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete linkPassphrases because the datastore is currently read-only.", ex);
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to delete linkPassphrases because index is missing", ex);
            throw new DataStoreException("Failed to delete linkPassphrases because index is missing", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete linkPassphrases", ex);
            throw new DataStoreException("Failed to delete linkPassphrases", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
