package com.myurldb.ws.dao.base;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

//import com.google.appengine.api.datastore.Key;

import com.myurldb.ws.core.GUID;
import com.myurldb.ws.config.Config;
import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.DataStoreException;
import com.myurldb.ws.exception.ServiceUnavailableException;
import com.myurldb.ws.exception.RequestConflictException;
import com.myurldb.ws.exception.ResourceNotFoundException;
import com.myurldb.ws.exception.NotImplementedException;
import com.myurldb.ws.dao.ShortPassageDAO;
import com.myurldb.ws.data.ShortPassageDataObject;
import com.myurldb.ws.search.gae.ShortPassageIndexBuilder;


public class DefaultShortPassageDAO extends DefaultDAOBase implements ShortPassageDAO
{
    private static final Logger log = Logger.getLogger(DefaultShortPassageDAO.class.getName()); 

    // Returns the shortPassage for the given guid/key.
    // Returns null if none is found in the datastore.
    @Override
    public ShortPassageDataObject getShortPassage(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: guid = " + guid);

        ShortPassageDataObject shortPassage = null;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            //Transaction tx = pm.currentTransaction();
            try {
                //tx.setOptimistic(true);
                //tx.begin();
                //Key key = ShortPassageDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = ShortPassageDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                shortPassage = pm.getObjectById(ShortPassageDataObject.class, key);
                shortPassage.getLongText();  // "Touch". Otherwise this field will not be fetched by default.
                shortPassage.getShortText();  // "Touch". Otherwise this field will not be fetched by default.
                shortPassage.getAttribute();  // "Touch". Otherwise this field will not be fetched by default.
                //tx.commit();
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve shortPassage for guid = " + guid, ex);
                throw new ResourceNotFoundException(ex);
            } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        log.finer("END");
	    return shortPassage;
	}

    @Override
    public List<ShortPassageDataObject> getShortPassages(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // temporary implementation
        List<ShortPassageDataObject> shortPassages = null;
        if(guids != null && !guids.isEmpty()) {
            shortPassages = new ArrayList<ShortPassageDataObject>();
            for(String guid : guids) {
                ShortPassageDataObject obj = getShortPassage(guid); 
                shortPassages.add(obj);
            }
	    }

        log.finer("END");
        return shortPassages;
    }

    @Override
    public List<ShortPassageDataObject> getAllShortPassages() throws BaseException
	{
	    return getAllShortPassages(null, null, null);
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<ShortPassageDataObject> getAllShortPassages(String ordering, Long offset, Integer count) throws BaseException
	{
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count);

    	List<ShortPassageDataObject> shortPassages = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(ShortPassageDataObject.class);
            if(ordering != null) {
                q.setOrdering(ordering);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            shortPassages = (List<ShortPassageDataObject>) q.execute();
            if(shortPassages != null) {
                shortPassages.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            /*
            // ???
            Collection<ShortPassageDataObject> rs_shortPassages = (Collection<ShortPassageDataObject>) q.execute();
            if(rs_shortPassages == null) {
                log.log(Level.WARNING, "Failed to retrieve all shortPassages.");
                shortPassages = new ArrayList<ShortPassageDataObject>();  // ???           
            } else {
                shortPassages = new ArrayList<ShortPassageDataObject>(pm.detachCopyAll(rs_shortPassages));
            }
            */
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all shortPassages.", ex);
            //shortPassages = new ArrayList<ShortPassageDataObject>();  // ???
            throw new DataStoreException("Failed to retrieve all shortPassages.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return shortPassages;
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<String> getAllShortPassageKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count);

    	List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery("select key from " + ShortPassageDataObject.class.getName());  // ???
            if(ordering != null) {
                q.setOrdering(ordering);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            keys = (List<String>) q.execute();
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all ShortPassage keys.", ex);
            throw new DataStoreException("Failed to retrieve all ShortPassage keys.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return keys;
    }

    @Override
	public List<ShortPassageDataObject> findShortPassages(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findShortPassages(filter, ordering, params, values, null, null);
    }

    @Override
	public List<ShortPassageDataObject> findShortPassages(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findShortPassages(filter, ordering, params, values, null, null, offset, count);
	}

    @SuppressWarnings("unchecked")
    @Override
	public List<ShortPassageDataObject> findShortPassages(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
        if(log.isLoggable(Level.INFO)) log.info("DefaultShortPassageDAO.findShortPassages(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("findShortPassages() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        List<ShortPassageDataObject> shortPassages = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(ShortPassageDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                shortPassages = (List<ShortPassageDataObject>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                shortPassages = (List<ShortPassageDataObject>) q.execute();
            }
            if(shortPassages != null) {
                shortPassages.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //while($TypeUtil.deCapitalizePluralize($typeName).iterator().hasNext()) {
            //    for(ShortPassageDataObject dobj : shortPassages) {
            //        //
            //    }
            //}
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find shortPassages because index is missing.", ex);
            //shortPassages = new ArrayList<ShortPassageDataObject>();  // ???
            throw new DataStoreException("Failed to find shortPassages because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find shortPassages meeting the criterion.", ex);
            //shortPassages = new ArrayList<ShortPassageDataObject>();  // ???
            throw new DataStoreException("Failed to find shortPassages meeting the criterion.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return shortPassages;
	}

    @SuppressWarnings("unchecked")
    @Override
    public List<String> findShortPassageKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultShortPassageDAO.findShortPassageKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery("select key from " + ShortPassageDataObject.class.getName());  // ???
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                keys = (List<String>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                keys = (List<String>) q.execute();
            }
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find ShortPassage keys because index is missing.", ex);
            throw new DataStoreException("Failed to find ShortPassage keys because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find ShortPassage keys meeting the criterion.", ex);
            throw new DataStoreException("Failed to find ShortPassage keys meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END");
        return keys;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultShortPassageDAO.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("getCount() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        Long count = 0L; 
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(ShortPassageDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(aggregate == null || aggregate.isEmpty()) {
                aggregate = "count(this)";  // By default, count. Other examples: "max(this.createdTime)", avg, sum, etc. (Note: These are currently not supported by Google App Engine.)
            }
            q.setResult(aggregate);
            //q.setResultClass(Long.class);
            // For now, we only support a single aggregate function. (Eg, no comma-separated list.)
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                count = (Long) q.execute();
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to get shortPassage count because index is missing.", ex);
            throw new DataStoreException("Failed to get shortPassage count because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to get shortPassage count meeting the criterion.", ex);
            throw new DataStoreException("Failed to get shortPassage count meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(count == null) {
            count = 0L;
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

	// Stores the shortPassage in the datastore and returns its guid.
	// Returns null if the operation fails.
    private String storeShortPassage(ShortPassageDataObject shortPassage) throws BaseException
    {
        log.fine("storeShortPassage() called.");

        String guid = null;
        PersistenceManager pm = getPersistenceManager();
        try {
            // TBD: If the object on the datastore has non-null created date and the arg shortLink has no created date timestamp,
            //      do not overwrite the timestamp. How to do that???
            // For now, this is ok since we use "overwrite/replace" semantics (not partial update).
            Long createdTime = shortPassage.getCreatedTime();
            if(createdTime == null) {
                createdTime = System.currentTimeMillis();
                shortPassage.setCreatedTime(createdTime);
            }
            Long modifiedTime = shortPassage.getModifiedTime();
            if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
                shortPassage.setModifiedTime(createdTime);
            }
            pm.makePersistent(shortPassage); 
            // TBD: How do you know the makePersistent() call was successful???
            guid = shortPassage.getGuid();
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to store shortPassage because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to store shortPassage because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to store shortPassage.", ex);
            throw new DataStoreException("Failed to store shortPassage.", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

	    // TBD:
	    if(Config.getInstance().isSearchEnabled()) {
            log.fine("Calling addDocument()... for ShortPassage.");
	    	ShortPassageIndexBuilder builder = new ShortPassageIndexBuilder();
	    	builder.addDocument(shortPassage);
	    }

        if(log.isLoggable(Level.FINE)) log.fine("storeShortPassage(): guid = " + guid);
        return guid;
    }

    @Override
    public String createShortPassage(ShortPassageDataObject shortPassage) throws BaseException
    {
        // The createdTime field will be automatically set in storeShortPassage().
        //Long createdTime = shortPassage.getCreatedTime();
        //if(createdTime == null) {
        //    createdTime = System.currentTimeMillis();
        //    shortPassage.setCreatedTime(createdTime);
        //}
        //Long modifiedTime = shortPassage.getModifiedTime();
        //if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
        //    shortPassage.setModifiedTime(createdTime);
        //}
        return storeShortPassage(shortPassage);
    }

    @Override
	public Boolean updateShortPassage(ShortPassageDataObject shortPassage) throws BaseException
	{
	    // Modify the modifiedTime timestamp.
	    // CreatedTime should, normally, be non-null.
	    // If null, however, it will be automatically set in storeShortPassage()
	    // (in which case modifiedTime might be updated again).
	    shortPassage.setModifiedTime(System.currentTimeMillis());
	    String guid = storeShortPassage(shortPassage);
	    if(guid != null) {
	        return true;
	    } else {
	        return false;
	    }
	}
	
    @Override
    public Boolean deleteShortPassage(ShortPassageDataObject shortPassage) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        PersistenceManager pm = getPersistenceManager();
        try {
            pm.deletePersistent(shortPassage);
            // TBD: How do you know the deletePersistent() call was successful???
            suc = true;
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to delete shortPassage because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete shortPassage because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete shortPassage.", ex);
            throw new DataStoreException("Failed to delete shortPassage.", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

	    // TBD:
	    if(Config.getInstance().isSearchEnabled()) {
            log.fine("Calling removeDocument()... for ShortPassage.");
	    	ShortPassageIndexBuilder builder = new ShortPassageIndexBuilder();
	    	builder.removeDocument(shortPassage.getGuid());
	    }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteShortPassage(String guid) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            try {
                //Key key = ShortPassageDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = ShortPassageDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                ShortPassageDataObject shortPassage = pm.getObjectById(ShortPassageDataObject.class, key);
                pm.deletePersistent(shortPassage);
                // TBD: How do you know the deletePersistent() call was successful???
                suc = true;
            } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
                // Datastore is read-only, possibly due to the Google App Engine maintenance.
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete shortPassage because the datastore is currently read-only. guid = " + guid, ex);
                throw new ServiceUnavailableException("Failed to delete shortPassage because the datastore is currently read-only.", ex);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete shortPassage for guid = " + guid, ex);
                throw new DataStoreException("Failed to delete shortPassage for guid = " + guid, ex);
            } finally {
                try {
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

	    // TBD:
	    if(Config.getInstance().isSearchEnabled()) {
            log.fine("Calling removeDocument()... for ShortPassage.");
	    	ShortPassageIndexBuilder builder = new ShortPassageIndexBuilder();
	    	builder.removeDocument(guid);
	    }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
	}

    @Override
    public Long deleteShortPassages(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultShortPassageDAO.deleteShortPassages(): filter=" + filter + "; params=" + params);

        // TBD: Delete-by-query requests should be carefully checked because of its potential harms.
        if(filter == null || filter.isEmpty()) {
            throw new RequestConflictException("deleteAll() without filter is not supported for performance/safety reasons");
        }

        Long count = 0L;
        PersistenceManager pm = getPersistenceManager();
        try {
            Query q = pm.newQuery(ShortPassageDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.deletePersistentAll(values.toArray(new Object[0]));
            } else {
                count = (Long) q.deletePersistentAll();
            }
            // TBD: How do you know the deletePersistent() call was successful???
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to deleteshortPassages because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete shortPassages because the datastore is currently read-only.", ex);
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to delete shortPassages because index is missing", ex);
            throw new DataStoreException("Failed to delete shortPassages because index is missing", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete shortPassages", ex);
            throw new DataStoreException("Failed to delete shortPassages", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

	    // TBD:
	    //if(Config.getInstance().isSearchEnabled()) {
        //    log.fine("Calling removeDocument()... for ShortPassage.");
	    //    ShortPassageIndexBuilder builder = new ShortPassageIndexBuilder();
	    //    builder.removeDocument(guids);
	    //}

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
