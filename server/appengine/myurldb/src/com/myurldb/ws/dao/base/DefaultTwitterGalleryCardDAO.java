package com.myurldb.ws.dao.base;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

//import com.google.appengine.api.datastore.Key;

import com.myurldb.ws.core.GUID;
import com.myurldb.ws.config.Config;
import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.DataStoreException;
import com.myurldb.ws.exception.ServiceUnavailableException;
import com.myurldb.ws.exception.RequestConflictException;
import com.myurldb.ws.exception.ResourceNotFoundException;
import com.myurldb.ws.exception.NotImplementedException;
import com.myurldb.ws.dao.TwitterGalleryCardDAO;
import com.myurldb.ws.data.TwitterGalleryCardDataObject;


public class DefaultTwitterGalleryCardDAO extends DefaultDAOBase implements TwitterGalleryCardDAO
{
    private static final Logger log = Logger.getLogger(DefaultTwitterGalleryCardDAO.class.getName()); 

    // Returns the twitterGalleryCard for the given guid/key.
    // Returns null if none is found in the datastore.
    @Override
    public TwitterGalleryCardDataObject getTwitterGalleryCard(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: guid = " + guid);

        TwitterGalleryCardDataObject twitterGalleryCard = null;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            //Transaction tx = pm.currentTransaction();
            try {
                //tx.setOptimistic(true);
                //tx.begin();
                //Key key = TwitterGalleryCardDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = TwitterGalleryCardDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                twitterGalleryCard = pm.getObjectById(TwitterGalleryCardDataObject.class, key);
                //tx.commit();
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve twitterGalleryCard for guid = " + guid, ex);
                throw new ResourceNotFoundException(ex);
            } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        log.finer("END");
	    return twitterGalleryCard;
	}

    @Override
    public List<TwitterGalleryCardDataObject> getTwitterGalleryCards(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // temporary implementation
        List<TwitterGalleryCardDataObject> twitterGalleryCards = null;
        if(guids != null && !guids.isEmpty()) {
            twitterGalleryCards = new ArrayList<TwitterGalleryCardDataObject>();
            for(String guid : guids) {
                TwitterGalleryCardDataObject obj = getTwitterGalleryCard(guid); 
                twitterGalleryCards.add(obj);
            }
	    }

        log.finer("END");
        return twitterGalleryCards;
    }

    @Override
    public List<TwitterGalleryCardDataObject> getAllTwitterGalleryCards() throws BaseException
	{
	    return getAllTwitterGalleryCards(null, null, null);
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<TwitterGalleryCardDataObject> getAllTwitterGalleryCards(String ordering, Long offset, Integer count) throws BaseException
	{
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count);

    	List<TwitterGalleryCardDataObject> twitterGalleryCards = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(TwitterGalleryCardDataObject.class);
            if(ordering != null) {
                q.setOrdering(ordering);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            twitterGalleryCards = (List<TwitterGalleryCardDataObject>) q.execute();
            if(twitterGalleryCards != null) {
                twitterGalleryCards.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            /*
            // ???
            Collection<TwitterGalleryCardDataObject> rs_twitterGalleryCards = (Collection<TwitterGalleryCardDataObject>) q.execute();
            if(rs_twitterGalleryCards == null) {
                log.log(Level.WARNING, "Failed to retrieve all twitterGalleryCards.");
                twitterGalleryCards = new ArrayList<TwitterGalleryCardDataObject>();  // ???           
            } else {
                twitterGalleryCards = new ArrayList<TwitterGalleryCardDataObject>(pm.detachCopyAll(rs_twitterGalleryCards));
            }
            */
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all twitterGalleryCards.", ex);
            //twitterGalleryCards = new ArrayList<TwitterGalleryCardDataObject>();  // ???
            throw new DataStoreException("Failed to retrieve all twitterGalleryCards.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return twitterGalleryCards;
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<String> getAllTwitterGalleryCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count);

    	List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery("select key from " + TwitterGalleryCardDataObject.class.getName());  // ???
            if(ordering != null) {
                q.setOrdering(ordering);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            keys = (List<String>) q.execute();
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all TwitterGalleryCard keys.", ex);
            throw new DataStoreException("Failed to retrieve all TwitterGalleryCard keys.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return keys;
    }

    @Override
	public List<TwitterGalleryCardDataObject> findTwitterGalleryCards(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findTwitterGalleryCards(filter, ordering, params, values, null, null);
    }

    @Override
	public List<TwitterGalleryCardDataObject> findTwitterGalleryCards(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findTwitterGalleryCards(filter, ordering, params, values, null, null, offset, count);
	}

    @SuppressWarnings("unchecked")
    @Override
	public List<TwitterGalleryCardDataObject> findTwitterGalleryCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
        if(log.isLoggable(Level.INFO)) log.info("DefaultTwitterGalleryCardDAO.findTwitterGalleryCards(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("findTwitterGalleryCards() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        List<TwitterGalleryCardDataObject> twitterGalleryCards = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(TwitterGalleryCardDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                twitterGalleryCards = (List<TwitterGalleryCardDataObject>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                twitterGalleryCards = (List<TwitterGalleryCardDataObject>) q.execute();
            }
            if(twitterGalleryCards != null) {
                twitterGalleryCards.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //while($TypeUtil.deCapitalizePluralize($typeName).iterator().hasNext()) {
            //    for(TwitterGalleryCardDataObject dobj : twitterGalleryCards) {
            //        //
            //    }
            //}
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find twitterGalleryCards because index is missing.", ex);
            //twitterGalleryCards = new ArrayList<TwitterGalleryCardDataObject>();  // ???
            throw new DataStoreException("Failed to find twitterGalleryCards because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find twitterGalleryCards meeting the criterion.", ex);
            //twitterGalleryCards = new ArrayList<TwitterGalleryCardDataObject>();  // ???
            throw new DataStoreException("Failed to find twitterGalleryCards meeting the criterion.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return twitterGalleryCards;
	}

    @SuppressWarnings("unchecked")
    @Override
    public List<String> findTwitterGalleryCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultTwitterGalleryCardDAO.findTwitterGalleryCardKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery("select key from " + TwitterGalleryCardDataObject.class.getName());  // ???
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                keys = (List<String>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                keys = (List<String>) q.execute();
            }
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find TwitterGalleryCard keys because index is missing.", ex);
            throw new DataStoreException("Failed to find TwitterGalleryCard keys because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find TwitterGalleryCard keys meeting the criterion.", ex);
            throw new DataStoreException("Failed to find TwitterGalleryCard keys meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END");
        return keys;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultTwitterGalleryCardDAO.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("getCount() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        Long count = 0L; 
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(TwitterGalleryCardDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(aggregate == null || aggregate.isEmpty()) {
                aggregate = "count(this)";  // By default, count. Other examples: "max(this.createdTime)", avg, sum, etc. (Note: These are currently not supported by Google App Engine.)
            }
            q.setResult(aggregate);
            //q.setResultClass(Long.class);
            // For now, we only support a single aggregate function. (Eg, no comma-separated list.)
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                count = (Long) q.execute();
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to get twitterGalleryCard count because index is missing.", ex);
            throw new DataStoreException("Failed to get twitterGalleryCard count because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to get twitterGalleryCard count meeting the criterion.", ex);
            throw new DataStoreException("Failed to get twitterGalleryCard count meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(count == null) {
            count = 0L;
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

	// Stores the twitterGalleryCard in the datastore and returns its guid.
	// Returns null if the operation fails.
    private String storeTwitterGalleryCard(TwitterGalleryCardDataObject twitterGalleryCard) throws BaseException
    {
        log.fine("storeTwitterGalleryCard() called.");

        String guid = null;
        PersistenceManager pm = getPersistenceManager();
        try {
            // TBD: If the object on the datastore has non-null created date and the arg shortLink has no created date timestamp,
            //      do not overwrite the timestamp. How to do that???
            // For now, this is ok since we use "overwrite/replace" semantics (not partial update).
            Long createdTime = twitterGalleryCard.getCreatedTime();
            if(createdTime == null) {
                createdTime = System.currentTimeMillis();
                twitterGalleryCard.setCreatedTime(createdTime);
            }
            Long modifiedTime = twitterGalleryCard.getModifiedTime();
            if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
                twitterGalleryCard.setModifiedTime(createdTime);
            }
            pm.makePersistent(twitterGalleryCard); 
            // TBD: How do you know the makePersistent() call was successful???
            guid = twitterGalleryCard.getGuid();
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to store twitterGalleryCard because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to store twitterGalleryCard because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to store twitterGalleryCard.", ex);
            throw new DataStoreException("Failed to store twitterGalleryCard.", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINE)) log.fine("storeTwitterGalleryCard(): guid = " + guid);
        return guid;
    }

    @Override
    public String createTwitterGalleryCard(TwitterGalleryCardDataObject twitterGalleryCard) throws BaseException
    {
        // The createdTime field will be automatically set in storeTwitterGalleryCard().
        //Long createdTime = twitterGalleryCard.getCreatedTime();
        //if(createdTime == null) {
        //    createdTime = System.currentTimeMillis();
        //    twitterGalleryCard.setCreatedTime(createdTime);
        //}
        //Long modifiedTime = twitterGalleryCard.getModifiedTime();
        //if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
        //    twitterGalleryCard.setModifiedTime(createdTime);
        //}
        return storeTwitterGalleryCard(twitterGalleryCard);
    }

    @Override
	public Boolean updateTwitterGalleryCard(TwitterGalleryCardDataObject twitterGalleryCard) throws BaseException
	{
	    // Modify the modifiedTime timestamp.
	    // CreatedTime should, normally, be non-null.
	    // If null, however, it will be automatically set in storeTwitterGalleryCard()
	    // (in which case modifiedTime might be updated again).
	    twitterGalleryCard.setModifiedTime(System.currentTimeMillis());
	    String guid = storeTwitterGalleryCard(twitterGalleryCard);
	    if(guid != null) {
	        return true;
	    } else {
	        return false;
	    }
	}
	
    @Override
    public Boolean deleteTwitterGalleryCard(TwitterGalleryCardDataObject twitterGalleryCard) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        PersistenceManager pm = getPersistenceManager();
        try {
            pm.deletePersistent(twitterGalleryCard);
            // TBD: How do you know the deletePersistent() call was successful???
            suc = true;
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to delete twitterGalleryCard because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete twitterGalleryCard because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete twitterGalleryCard.", ex);
            throw new DataStoreException("Failed to delete twitterGalleryCard.", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteTwitterGalleryCard(String guid) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            try {
                //Key key = TwitterGalleryCardDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = TwitterGalleryCardDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                TwitterGalleryCardDataObject twitterGalleryCard = pm.getObjectById(TwitterGalleryCardDataObject.class, key);
                pm.deletePersistent(twitterGalleryCard);
                // TBD: How do you know the deletePersistent() call was successful???
                suc = true;
            } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
                // Datastore is read-only, possibly due to the Google App Engine maintenance.
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete twitterGalleryCard because the datastore is currently read-only. guid = " + guid, ex);
                throw new ServiceUnavailableException("Failed to delete twitterGalleryCard because the datastore is currently read-only.", ex);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete twitterGalleryCard for guid = " + guid, ex);
                throw new DataStoreException("Failed to delete twitterGalleryCard for guid = " + guid, ex);
            } finally {
                try {
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
	}

    @Override
    public Long deleteTwitterGalleryCards(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultTwitterGalleryCardDAO.deleteTwitterGalleryCards(): filter=" + filter + "; params=" + params);

        // TBD: Delete-by-query requests should be carefully checked because of its potential harms.
        if(filter == null || filter.isEmpty()) {
            throw new RequestConflictException("deleteAll() without filter is not supported for performance/safety reasons");
        }

        Long count = 0L;
        PersistenceManager pm = getPersistenceManager();
        try {
            Query q = pm.newQuery(TwitterGalleryCardDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.deletePersistentAll(values.toArray(new Object[0]));
            } else {
                count = (Long) q.deletePersistentAll();
            }
            // TBD: How do you know the deletePersistent() call was successful???
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to deletetwitterGalleryCards because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete twitterGalleryCards because the datastore is currently read-only.", ex);
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to delete twitterGalleryCards because index is missing", ex);
            throw new DataStoreException("Failed to delete twitterGalleryCards because index is missing", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete twitterGalleryCards", ex);
            throw new DataStoreException("Failed to delete twitterGalleryCards", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
