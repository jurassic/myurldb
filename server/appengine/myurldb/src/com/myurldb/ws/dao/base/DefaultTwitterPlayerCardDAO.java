package com.myurldb.ws.dao.base;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

//import com.google.appengine.api.datastore.Key;

import com.myurldb.ws.core.GUID;
import com.myurldb.ws.config.Config;
import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.DataStoreException;
import com.myurldb.ws.exception.ServiceUnavailableException;
import com.myurldb.ws.exception.RequestConflictException;
import com.myurldb.ws.exception.ResourceNotFoundException;
import com.myurldb.ws.exception.NotImplementedException;
import com.myurldb.ws.dao.TwitterPlayerCardDAO;
import com.myurldb.ws.data.TwitterPlayerCardDataObject;


public class DefaultTwitterPlayerCardDAO extends DefaultDAOBase implements TwitterPlayerCardDAO
{
    private static final Logger log = Logger.getLogger(DefaultTwitterPlayerCardDAO.class.getName()); 

    // Returns the twitterPlayerCard for the given guid/key.
    // Returns null if none is found in the datastore.
    @Override
    public TwitterPlayerCardDataObject getTwitterPlayerCard(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: guid = " + guid);

        TwitterPlayerCardDataObject twitterPlayerCard = null;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            //Transaction tx = pm.currentTransaction();
            try {
                //tx.setOptimistic(true);
                //tx.begin();
                //Key key = TwitterPlayerCardDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = TwitterPlayerCardDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                twitterPlayerCard = pm.getObjectById(TwitterPlayerCardDataObject.class, key);
                //tx.commit();
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve twitterPlayerCard for guid = " + guid, ex);
                throw new ResourceNotFoundException(ex);
            } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        log.finer("END");
	    return twitterPlayerCard;
	}

    @Override
    public List<TwitterPlayerCardDataObject> getTwitterPlayerCards(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // temporary implementation
        List<TwitterPlayerCardDataObject> twitterPlayerCards = null;
        if(guids != null && !guids.isEmpty()) {
            twitterPlayerCards = new ArrayList<TwitterPlayerCardDataObject>();
            for(String guid : guids) {
                TwitterPlayerCardDataObject obj = getTwitterPlayerCard(guid); 
                twitterPlayerCards.add(obj);
            }
	    }

        log.finer("END");
        return twitterPlayerCards;
    }

    @Override
    public List<TwitterPlayerCardDataObject> getAllTwitterPlayerCards() throws BaseException
	{
	    return getAllTwitterPlayerCards(null, null, null);
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<TwitterPlayerCardDataObject> getAllTwitterPlayerCards(String ordering, Long offset, Integer count) throws BaseException
	{
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count);

    	List<TwitterPlayerCardDataObject> twitterPlayerCards = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(TwitterPlayerCardDataObject.class);
            if(ordering != null) {
                q.setOrdering(ordering);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            twitterPlayerCards = (List<TwitterPlayerCardDataObject>) q.execute();
            if(twitterPlayerCards != null) {
                twitterPlayerCards.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            /*
            // ???
            Collection<TwitterPlayerCardDataObject> rs_twitterPlayerCards = (Collection<TwitterPlayerCardDataObject>) q.execute();
            if(rs_twitterPlayerCards == null) {
                log.log(Level.WARNING, "Failed to retrieve all twitterPlayerCards.");
                twitterPlayerCards = new ArrayList<TwitterPlayerCardDataObject>();  // ???           
            } else {
                twitterPlayerCards = new ArrayList<TwitterPlayerCardDataObject>(pm.detachCopyAll(rs_twitterPlayerCards));
            }
            */
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all twitterPlayerCards.", ex);
            //twitterPlayerCards = new ArrayList<TwitterPlayerCardDataObject>();  // ???
            throw new DataStoreException("Failed to retrieve all twitterPlayerCards.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return twitterPlayerCards;
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<String> getAllTwitterPlayerCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count);

    	List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery("select key from " + TwitterPlayerCardDataObject.class.getName());  // ???
            if(ordering != null) {
                q.setOrdering(ordering);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            keys = (List<String>) q.execute();
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all TwitterPlayerCard keys.", ex);
            throw new DataStoreException("Failed to retrieve all TwitterPlayerCard keys.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return keys;
    }

    @Override
	public List<TwitterPlayerCardDataObject> findTwitterPlayerCards(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findTwitterPlayerCards(filter, ordering, params, values, null, null);
    }

    @Override
	public List<TwitterPlayerCardDataObject> findTwitterPlayerCards(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findTwitterPlayerCards(filter, ordering, params, values, null, null, offset, count);
	}

    @SuppressWarnings("unchecked")
    @Override
	public List<TwitterPlayerCardDataObject> findTwitterPlayerCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
        if(log.isLoggable(Level.INFO)) log.info("DefaultTwitterPlayerCardDAO.findTwitterPlayerCards(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("findTwitterPlayerCards() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        List<TwitterPlayerCardDataObject> twitterPlayerCards = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(TwitterPlayerCardDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                twitterPlayerCards = (List<TwitterPlayerCardDataObject>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                twitterPlayerCards = (List<TwitterPlayerCardDataObject>) q.execute();
            }
            if(twitterPlayerCards != null) {
                twitterPlayerCards.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //while($TypeUtil.deCapitalizePluralize($typeName).iterator().hasNext()) {
            //    for(TwitterPlayerCardDataObject dobj : twitterPlayerCards) {
            //        //
            //    }
            //}
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find twitterPlayerCards because index is missing.", ex);
            //twitterPlayerCards = new ArrayList<TwitterPlayerCardDataObject>();  // ???
            throw new DataStoreException("Failed to find twitterPlayerCards because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find twitterPlayerCards meeting the criterion.", ex);
            //twitterPlayerCards = new ArrayList<TwitterPlayerCardDataObject>();  // ???
            throw new DataStoreException("Failed to find twitterPlayerCards meeting the criterion.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return twitterPlayerCards;
	}

    @SuppressWarnings("unchecked")
    @Override
    public List<String> findTwitterPlayerCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultTwitterPlayerCardDAO.findTwitterPlayerCardKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery("select key from " + TwitterPlayerCardDataObject.class.getName());  // ???
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                keys = (List<String>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                keys = (List<String>) q.execute();
            }
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find TwitterPlayerCard keys because index is missing.", ex);
            throw new DataStoreException("Failed to find TwitterPlayerCard keys because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find TwitterPlayerCard keys meeting the criterion.", ex);
            throw new DataStoreException("Failed to find TwitterPlayerCard keys meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END");
        return keys;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultTwitterPlayerCardDAO.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("getCount() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        Long count = 0L; 
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(TwitterPlayerCardDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(aggregate == null || aggregate.isEmpty()) {
                aggregate = "count(this)";  // By default, count. Other examples: "max(this.createdTime)", avg, sum, etc. (Note: These are currently not supported by Google App Engine.)
            }
            q.setResult(aggregate);
            //q.setResultClass(Long.class);
            // For now, we only support a single aggregate function. (Eg, no comma-separated list.)
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                count = (Long) q.execute();
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to get twitterPlayerCard count because index is missing.", ex);
            throw new DataStoreException("Failed to get twitterPlayerCard count because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to get twitterPlayerCard count meeting the criterion.", ex);
            throw new DataStoreException("Failed to get twitterPlayerCard count meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(count == null) {
            count = 0L;
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

	// Stores the twitterPlayerCard in the datastore and returns its guid.
	// Returns null if the operation fails.
    private String storeTwitterPlayerCard(TwitterPlayerCardDataObject twitterPlayerCard) throws BaseException
    {
        log.fine("storeTwitterPlayerCard() called.");

        String guid = null;
        PersistenceManager pm = getPersistenceManager();
        try {
            // TBD: If the object on the datastore has non-null created date and the arg shortLink has no created date timestamp,
            //      do not overwrite the timestamp. How to do that???
            // For now, this is ok since we use "overwrite/replace" semantics (not partial update).
            Long createdTime = twitterPlayerCard.getCreatedTime();
            if(createdTime == null) {
                createdTime = System.currentTimeMillis();
                twitterPlayerCard.setCreatedTime(createdTime);
            }
            Long modifiedTime = twitterPlayerCard.getModifiedTime();
            if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
                twitterPlayerCard.setModifiedTime(createdTime);
            }
            pm.makePersistent(twitterPlayerCard); 
            // TBD: How do you know the makePersistent() call was successful???
            guid = twitterPlayerCard.getGuid();
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to store twitterPlayerCard because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to store twitterPlayerCard because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to store twitterPlayerCard.", ex);
            throw new DataStoreException("Failed to store twitterPlayerCard.", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINE)) log.fine("storeTwitterPlayerCard(): guid = " + guid);
        return guid;
    }

    @Override
    public String createTwitterPlayerCard(TwitterPlayerCardDataObject twitterPlayerCard) throws BaseException
    {
        // The createdTime field will be automatically set in storeTwitterPlayerCard().
        //Long createdTime = twitterPlayerCard.getCreatedTime();
        //if(createdTime == null) {
        //    createdTime = System.currentTimeMillis();
        //    twitterPlayerCard.setCreatedTime(createdTime);
        //}
        //Long modifiedTime = twitterPlayerCard.getModifiedTime();
        //if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
        //    twitterPlayerCard.setModifiedTime(createdTime);
        //}
        return storeTwitterPlayerCard(twitterPlayerCard);
    }

    @Override
	public Boolean updateTwitterPlayerCard(TwitterPlayerCardDataObject twitterPlayerCard) throws BaseException
	{
	    // Modify the modifiedTime timestamp.
	    // CreatedTime should, normally, be non-null.
	    // If null, however, it will be automatically set in storeTwitterPlayerCard()
	    // (in which case modifiedTime might be updated again).
	    twitterPlayerCard.setModifiedTime(System.currentTimeMillis());
	    String guid = storeTwitterPlayerCard(twitterPlayerCard);
	    if(guid != null) {
	        return true;
	    } else {
	        return false;
	    }
	}
	
    @Override
    public Boolean deleteTwitterPlayerCard(TwitterPlayerCardDataObject twitterPlayerCard) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        PersistenceManager pm = getPersistenceManager();
        try {
            pm.deletePersistent(twitterPlayerCard);
            // TBD: How do you know the deletePersistent() call was successful???
            suc = true;
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to delete twitterPlayerCard because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete twitterPlayerCard because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete twitterPlayerCard.", ex);
            throw new DataStoreException("Failed to delete twitterPlayerCard.", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteTwitterPlayerCard(String guid) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            try {
                //Key key = TwitterPlayerCardDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = TwitterPlayerCardDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                TwitterPlayerCardDataObject twitterPlayerCard = pm.getObjectById(TwitterPlayerCardDataObject.class, key);
                pm.deletePersistent(twitterPlayerCard);
                // TBD: How do you know the deletePersistent() call was successful???
                suc = true;
            } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
                // Datastore is read-only, possibly due to the Google App Engine maintenance.
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete twitterPlayerCard because the datastore is currently read-only. guid = " + guid, ex);
                throw new ServiceUnavailableException("Failed to delete twitterPlayerCard because the datastore is currently read-only.", ex);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete twitterPlayerCard for guid = " + guid, ex);
                throw new DataStoreException("Failed to delete twitterPlayerCard for guid = " + guid, ex);
            } finally {
                try {
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
	}

    @Override
    public Long deleteTwitterPlayerCards(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultTwitterPlayerCardDAO.deleteTwitterPlayerCards(): filter=" + filter + "; params=" + params);

        // TBD: Delete-by-query requests should be carefully checked because of its potential harms.
        if(filter == null || filter.isEmpty()) {
            throw new RequestConflictException("deleteAll() without filter is not supported for performance/safety reasons");
        }

        Long count = 0L;
        PersistenceManager pm = getPersistenceManager();
        try {
            Query q = pm.newQuery(TwitterPlayerCardDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.deletePersistentAll(values.toArray(new Object[0]));
            } else {
                count = (Long) q.deletePersistentAll();
            }
            // TBD: How do you know the deletePersistent() call was successful???
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to deletetwitterPlayerCards because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete twitterPlayerCards because the datastore is currently read-only.", ex);
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to delete twitterPlayerCards because index is missing", ex);
            throw new DataStoreException("Failed to delete twitterPlayerCards because index is missing", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete twitterPlayerCards", ex);
            throw new DataStoreException("Failed to delete twitterPlayerCards", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
