package com.myurldb.ws.dao.base;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

//import com.google.appengine.api.datastore.Key;

import com.myurldb.ws.core.GUID;
import com.myurldb.ws.config.Config;
import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.DataStoreException;
import com.myurldb.ws.exception.ServiceUnavailableException;
import com.myurldb.ws.exception.RequestConflictException;
import com.myurldb.ws.exception.ResourceNotFoundException;
import com.myurldb.ws.exception.NotImplementedException;
import com.myurldb.ws.dao.TwitterSummaryCardDAO;
import com.myurldb.ws.data.TwitterSummaryCardDataObject;


public class DefaultTwitterSummaryCardDAO extends DefaultDAOBase implements TwitterSummaryCardDAO
{
    private static final Logger log = Logger.getLogger(DefaultTwitterSummaryCardDAO.class.getName()); 

    // Returns the twitterSummaryCard for the given guid/key.
    // Returns null if none is found in the datastore.
    @Override
    public TwitterSummaryCardDataObject getTwitterSummaryCard(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: guid = " + guid);

        TwitterSummaryCardDataObject twitterSummaryCard = null;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            //Transaction tx = pm.currentTransaction();
            try {
                //tx.setOptimistic(true);
                //tx.begin();
                //Key key = TwitterSummaryCardDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = TwitterSummaryCardDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                twitterSummaryCard = pm.getObjectById(TwitterSummaryCardDataObject.class, key);
                //tx.commit();
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve twitterSummaryCard for guid = " + guid, ex);
                throw new ResourceNotFoundException(ex);
            } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        log.finer("END");
	    return twitterSummaryCard;
	}

    @Override
    public List<TwitterSummaryCardDataObject> getTwitterSummaryCards(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // temporary implementation
        List<TwitterSummaryCardDataObject> twitterSummaryCards = null;
        if(guids != null && !guids.isEmpty()) {
            twitterSummaryCards = new ArrayList<TwitterSummaryCardDataObject>();
            for(String guid : guids) {
                TwitterSummaryCardDataObject obj = getTwitterSummaryCard(guid); 
                twitterSummaryCards.add(obj);
            }
	    }

        log.finer("END");
        return twitterSummaryCards;
    }

    @Override
    public List<TwitterSummaryCardDataObject> getAllTwitterSummaryCards() throws BaseException
	{
	    return getAllTwitterSummaryCards(null, null, null);
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<TwitterSummaryCardDataObject> getAllTwitterSummaryCards(String ordering, Long offset, Integer count) throws BaseException
	{
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count);

    	List<TwitterSummaryCardDataObject> twitterSummaryCards = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(TwitterSummaryCardDataObject.class);
            if(ordering != null) {
                q.setOrdering(ordering);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            twitterSummaryCards = (List<TwitterSummaryCardDataObject>) q.execute();
            if(twitterSummaryCards != null) {
                twitterSummaryCards.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            /*
            // ???
            Collection<TwitterSummaryCardDataObject> rs_twitterSummaryCards = (Collection<TwitterSummaryCardDataObject>) q.execute();
            if(rs_twitterSummaryCards == null) {
                log.log(Level.WARNING, "Failed to retrieve all twitterSummaryCards.");
                twitterSummaryCards = new ArrayList<TwitterSummaryCardDataObject>();  // ???           
            } else {
                twitterSummaryCards = new ArrayList<TwitterSummaryCardDataObject>(pm.detachCopyAll(rs_twitterSummaryCards));
            }
            */
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all twitterSummaryCards.", ex);
            //twitterSummaryCards = new ArrayList<TwitterSummaryCardDataObject>();  // ???
            throw new DataStoreException("Failed to retrieve all twitterSummaryCards.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return twitterSummaryCards;
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<String> getAllTwitterSummaryCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count);

    	List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery("select key from " + TwitterSummaryCardDataObject.class.getName());  // ???
            if(ordering != null) {
                q.setOrdering(ordering);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            keys = (List<String>) q.execute();
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all TwitterSummaryCard keys.", ex);
            throw new DataStoreException("Failed to retrieve all TwitterSummaryCard keys.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return keys;
    }

    @Override
	public List<TwitterSummaryCardDataObject> findTwitterSummaryCards(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findTwitterSummaryCards(filter, ordering, params, values, null, null);
    }

    @Override
	public List<TwitterSummaryCardDataObject> findTwitterSummaryCards(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findTwitterSummaryCards(filter, ordering, params, values, null, null, offset, count);
	}

    @SuppressWarnings("unchecked")
    @Override
	public List<TwitterSummaryCardDataObject> findTwitterSummaryCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
        if(log.isLoggable(Level.INFO)) log.info("DefaultTwitterSummaryCardDAO.findTwitterSummaryCards(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("findTwitterSummaryCards() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        List<TwitterSummaryCardDataObject> twitterSummaryCards = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(TwitterSummaryCardDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                twitterSummaryCards = (List<TwitterSummaryCardDataObject>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                twitterSummaryCards = (List<TwitterSummaryCardDataObject>) q.execute();
            }
            if(twitterSummaryCards != null) {
                twitterSummaryCards.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //while($TypeUtil.deCapitalizePluralize($typeName).iterator().hasNext()) {
            //    for(TwitterSummaryCardDataObject dobj : twitterSummaryCards) {
            //        //
            //    }
            //}
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find twitterSummaryCards because index is missing.", ex);
            //twitterSummaryCards = new ArrayList<TwitterSummaryCardDataObject>();  // ???
            throw new DataStoreException("Failed to find twitterSummaryCards because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find twitterSummaryCards meeting the criterion.", ex);
            //twitterSummaryCards = new ArrayList<TwitterSummaryCardDataObject>();  // ???
            throw new DataStoreException("Failed to find twitterSummaryCards meeting the criterion.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return twitterSummaryCards;
	}

    @SuppressWarnings("unchecked")
    @Override
    public List<String> findTwitterSummaryCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultTwitterSummaryCardDAO.findTwitterSummaryCardKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery("select key from " + TwitterSummaryCardDataObject.class.getName());  // ???
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                keys = (List<String>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                keys = (List<String>) q.execute();
            }
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find TwitterSummaryCard keys because index is missing.", ex);
            throw new DataStoreException("Failed to find TwitterSummaryCard keys because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find TwitterSummaryCard keys meeting the criterion.", ex);
            throw new DataStoreException("Failed to find TwitterSummaryCard keys meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END");
        return keys;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultTwitterSummaryCardDAO.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("getCount() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        Long count = 0L; 
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(TwitterSummaryCardDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(aggregate == null || aggregate.isEmpty()) {
                aggregate = "count(this)";  // By default, count. Other examples: "max(this.createdTime)", avg, sum, etc. (Note: These are currently not supported by Google App Engine.)
            }
            q.setResult(aggregate);
            //q.setResultClass(Long.class);
            // For now, we only support a single aggregate function. (Eg, no comma-separated list.)
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                count = (Long) q.execute();
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to get twitterSummaryCard count because index is missing.", ex);
            throw new DataStoreException("Failed to get twitterSummaryCard count because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to get twitterSummaryCard count meeting the criterion.", ex);
            throw new DataStoreException("Failed to get twitterSummaryCard count meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(count == null) {
            count = 0L;
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

	// Stores the twitterSummaryCard in the datastore and returns its guid.
	// Returns null if the operation fails.
    private String storeTwitterSummaryCard(TwitterSummaryCardDataObject twitterSummaryCard) throws BaseException
    {
        log.fine("storeTwitterSummaryCard() called.");

        String guid = null;
        PersistenceManager pm = getPersistenceManager();
        try {
            // TBD: If the object on the datastore has non-null created date and the arg shortLink has no created date timestamp,
            //      do not overwrite the timestamp. How to do that???
            // For now, this is ok since we use "overwrite/replace" semantics (not partial update).
            Long createdTime = twitterSummaryCard.getCreatedTime();
            if(createdTime == null) {
                createdTime = System.currentTimeMillis();
                twitterSummaryCard.setCreatedTime(createdTime);
            }
            Long modifiedTime = twitterSummaryCard.getModifiedTime();
            if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
                twitterSummaryCard.setModifiedTime(createdTime);
            }
            pm.makePersistent(twitterSummaryCard); 
            // TBD: How do you know the makePersistent() call was successful???
            guid = twitterSummaryCard.getGuid();
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to store twitterSummaryCard because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to store twitterSummaryCard because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to store twitterSummaryCard.", ex);
            throw new DataStoreException("Failed to store twitterSummaryCard.", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINE)) log.fine("storeTwitterSummaryCard(): guid = " + guid);
        return guid;
    }

    @Override
    public String createTwitterSummaryCard(TwitterSummaryCardDataObject twitterSummaryCard) throws BaseException
    {
        // The createdTime field will be automatically set in storeTwitterSummaryCard().
        //Long createdTime = twitterSummaryCard.getCreatedTime();
        //if(createdTime == null) {
        //    createdTime = System.currentTimeMillis();
        //    twitterSummaryCard.setCreatedTime(createdTime);
        //}
        //Long modifiedTime = twitterSummaryCard.getModifiedTime();
        //if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
        //    twitterSummaryCard.setModifiedTime(createdTime);
        //}
        return storeTwitterSummaryCard(twitterSummaryCard);
    }

    @Override
	public Boolean updateTwitterSummaryCard(TwitterSummaryCardDataObject twitterSummaryCard) throws BaseException
	{
	    // Modify the modifiedTime timestamp.
	    // CreatedTime should, normally, be non-null.
	    // If null, however, it will be automatically set in storeTwitterSummaryCard()
	    // (in which case modifiedTime might be updated again).
	    twitterSummaryCard.setModifiedTime(System.currentTimeMillis());
	    String guid = storeTwitterSummaryCard(twitterSummaryCard);
	    if(guid != null) {
	        return true;
	    } else {
	        return false;
	    }
	}
	
    @Override
    public Boolean deleteTwitterSummaryCard(TwitterSummaryCardDataObject twitterSummaryCard) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        PersistenceManager pm = getPersistenceManager();
        try {
            pm.deletePersistent(twitterSummaryCard);
            // TBD: How do you know the deletePersistent() call was successful???
            suc = true;
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to delete twitterSummaryCard because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete twitterSummaryCard because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete twitterSummaryCard.", ex);
            throw new DataStoreException("Failed to delete twitterSummaryCard.", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteTwitterSummaryCard(String guid) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            try {
                //Key key = TwitterSummaryCardDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = TwitterSummaryCardDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                TwitterSummaryCardDataObject twitterSummaryCard = pm.getObjectById(TwitterSummaryCardDataObject.class, key);
                pm.deletePersistent(twitterSummaryCard);
                // TBD: How do you know the deletePersistent() call was successful???
                suc = true;
            } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
                // Datastore is read-only, possibly due to the Google App Engine maintenance.
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete twitterSummaryCard because the datastore is currently read-only. guid = " + guid, ex);
                throw new ServiceUnavailableException("Failed to delete twitterSummaryCard because the datastore is currently read-only.", ex);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete twitterSummaryCard for guid = " + guid, ex);
                throw new DataStoreException("Failed to delete twitterSummaryCard for guid = " + guid, ex);
            } finally {
                try {
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
	}

    @Override
    public Long deleteTwitterSummaryCards(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultTwitterSummaryCardDAO.deleteTwitterSummaryCards(): filter=" + filter + "; params=" + params);

        // TBD: Delete-by-query requests should be carefully checked because of its potential harms.
        if(filter == null || filter.isEmpty()) {
            throw new RequestConflictException("deleteAll() without filter is not supported for performance/safety reasons");
        }

        Long count = 0L;
        PersistenceManager pm = getPersistenceManager();
        try {
            Query q = pm.newQuery(TwitterSummaryCardDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.deletePersistentAll(values.toArray(new Object[0]));
            } else {
                count = (Long) q.deletePersistentAll();
            }
            // TBD: How do you know the deletePersistent() call was successful???
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to deletetwitterSummaryCards because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete twitterSummaryCards because the datastore is currently read-only.", ex);
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to delete twitterSummaryCards because index is missing", ex);
            throw new DataStoreException("Failed to delete twitterSummaryCards because index is missing", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete twitterSummaryCards", ex);
            throw new DataStoreException("Failed to delete twitterSummaryCards", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
