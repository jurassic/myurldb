package com.myurldb.ws.dao.base;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

//import com.google.appengine.api.datastore.Key;

import com.myurldb.ws.core.GUID;
import com.myurldb.ws.config.Config;
import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.DataStoreException;
import com.myurldb.ws.exception.ServiceUnavailableException;
import com.myurldb.ws.exception.RequestConflictException;
import com.myurldb.ws.exception.ResourceNotFoundException;
import com.myurldb.ws.exception.NotImplementedException;
import com.myurldb.ws.dao.UserResourcePermissionDAO;
import com.myurldb.ws.data.UserResourcePermissionDataObject;


public class DefaultUserResourcePermissionDAO extends DefaultDAOBase implements UserResourcePermissionDAO
{
    private static final Logger log = Logger.getLogger(DefaultUserResourcePermissionDAO.class.getName()); 

    // Returns the userResourcePermission for the given guid/key.
    // Returns null if none is found in the datastore.
    @Override
    public UserResourcePermissionDataObject getUserResourcePermission(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: guid = " + guid);

        UserResourcePermissionDataObject userResourcePermission = null;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            //Transaction tx = pm.currentTransaction();
            try {
                //tx.setOptimistic(true);
                //tx.begin();
                //Key key = UserResourcePermissionDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = UserResourcePermissionDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                userResourcePermission = pm.getObjectById(UserResourcePermissionDataObject.class, key);
                //tx.commit();
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve userResourcePermission for guid = " + guid, ex);
                throw new ResourceNotFoundException(ex);
            } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        log.finer("END");
	    return userResourcePermission;
	}

    @Override
    public List<UserResourcePermissionDataObject> getUserResourcePermissions(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // temporary implementation
        List<UserResourcePermissionDataObject> userResourcePermissions = null;
        if(guids != null && !guids.isEmpty()) {
            userResourcePermissions = new ArrayList<UserResourcePermissionDataObject>();
            for(String guid : guids) {
                UserResourcePermissionDataObject obj = getUserResourcePermission(guid); 
                userResourcePermissions.add(obj);
            }
	    }

        log.finer("END");
        return userResourcePermissions;
    }

    @Override
    public List<UserResourcePermissionDataObject> getAllUserResourcePermissions() throws BaseException
	{
	    return getAllUserResourcePermissions(null, null, null);
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<UserResourcePermissionDataObject> getAllUserResourcePermissions(String ordering, Long offset, Integer count) throws BaseException
	{
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count);

    	List<UserResourcePermissionDataObject> userResourcePermissions = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(UserResourcePermissionDataObject.class);
            if(ordering != null) {
                q.setOrdering(ordering);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            userResourcePermissions = (List<UserResourcePermissionDataObject>) q.execute();
            if(userResourcePermissions != null) {
                userResourcePermissions.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            /*
            // ???
            Collection<UserResourcePermissionDataObject> rs_userResourcePermissions = (Collection<UserResourcePermissionDataObject>) q.execute();
            if(rs_userResourcePermissions == null) {
                log.log(Level.WARNING, "Failed to retrieve all userResourcePermissions.");
                userResourcePermissions = new ArrayList<UserResourcePermissionDataObject>();  // ???           
            } else {
                userResourcePermissions = new ArrayList<UserResourcePermissionDataObject>(pm.detachCopyAll(rs_userResourcePermissions));
            }
            */
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all userResourcePermissions.", ex);
            //userResourcePermissions = new ArrayList<UserResourcePermissionDataObject>();  // ???
            throw new DataStoreException("Failed to retrieve all userResourcePermissions.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return userResourcePermissions;
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<String> getAllUserResourcePermissionKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count);

    	List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery("select key from " + UserResourcePermissionDataObject.class.getName());  // ???
            if(ordering != null) {
                q.setOrdering(ordering);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            keys = (List<String>) q.execute();
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all UserResourcePermission keys.", ex);
            throw new DataStoreException("Failed to retrieve all UserResourcePermission keys.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return keys;
    }

    @Override
	public List<UserResourcePermissionDataObject> findUserResourcePermissions(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findUserResourcePermissions(filter, ordering, params, values, null, null);
    }

    @Override
	public List<UserResourcePermissionDataObject> findUserResourcePermissions(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findUserResourcePermissions(filter, ordering, params, values, null, null, offset, count);
	}

    @SuppressWarnings("unchecked")
    @Override
	public List<UserResourcePermissionDataObject> findUserResourcePermissions(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
        if(log.isLoggable(Level.INFO)) log.info("DefaultUserResourcePermissionDAO.findUserResourcePermissions(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("findUserResourcePermissions() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        List<UserResourcePermissionDataObject> userResourcePermissions = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(UserResourcePermissionDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                userResourcePermissions = (List<UserResourcePermissionDataObject>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                userResourcePermissions = (List<UserResourcePermissionDataObject>) q.execute();
            }
            if(userResourcePermissions != null) {
                userResourcePermissions.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //while($TypeUtil.deCapitalizePluralize($typeName).iterator().hasNext()) {
            //    for(UserResourcePermissionDataObject dobj : userResourcePermissions) {
            //        //
            //    }
            //}
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find userResourcePermissions because index is missing.", ex);
            //userResourcePermissions = new ArrayList<UserResourcePermissionDataObject>();  // ???
            throw new DataStoreException("Failed to find userResourcePermissions because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find userResourcePermissions meeting the criterion.", ex);
            //userResourcePermissions = new ArrayList<UserResourcePermissionDataObject>();  // ???
            throw new DataStoreException("Failed to find userResourcePermissions meeting the criterion.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return userResourcePermissions;
	}

    @SuppressWarnings("unchecked")
    @Override
    public List<String> findUserResourcePermissionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultUserResourcePermissionDAO.findUserResourcePermissionKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery("select key from " + UserResourcePermissionDataObject.class.getName());  // ???
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                keys = (List<String>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                keys = (List<String>) q.execute();
            }
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find UserResourcePermission keys because index is missing.", ex);
            throw new DataStoreException("Failed to find UserResourcePermission keys because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find UserResourcePermission keys meeting the criterion.", ex);
            throw new DataStoreException("Failed to find UserResourcePermission keys meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END");
        return keys;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultUserResourcePermissionDAO.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("getCount() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        Long count = 0L; 
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(UserResourcePermissionDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(aggregate == null || aggregate.isEmpty()) {
                aggregate = "count(this)";  // By default, count. Other examples: "max(this.createdTime)", avg, sum, etc. (Note: These are currently not supported by Google App Engine.)
            }
            q.setResult(aggregate);
            //q.setResultClass(Long.class);
            // For now, we only support a single aggregate function. (Eg, no comma-separated list.)
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                count = (Long) q.execute();
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to get userResourcePermission count because index is missing.", ex);
            throw new DataStoreException("Failed to get userResourcePermission count because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to get userResourcePermission count meeting the criterion.", ex);
            throw new DataStoreException("Failed to get userResourcePermission count meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(count == null) {
            count = 0L;
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

	// Stores the userResourcePermission in the datastore and returns its guid.
	// Returns null if the operation fails.
    private String storeUserResourcePermission(UserResourcePermissionDataObject userResourcePermission) throws BaseException
    {
        log.fine("storeUserResourcePermission() called.");

        String guid = null;
        PersistenceManager pm = getPersistenceManager();
        try {
            // TBD: If the object on the datastore has non-null created date and the arg shortLink has no created date timestamp,
            //      do not overwrite the timestamp. How to do that???
            // For now, this is ok since we use "overwrite/replace" semantics (not partial update).
            Long createdTime = userResourcePermission.getCreatedTime();
            if(createdTime == null) {
                createdTime = System.currentTimeMillis();
                userResourcePermission.setCreatedTime(createdTime);
            }
            Long modifiedTime = userResourcePermission.getModifiedTime();
            if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
                userResourcePermission.setModifiedTime(createdTime);
            }
            pm.makePersistent(userResourcePermission); 
            // TBD: How do you know the makePersistent() call was successful???
            guid = userResourcePermission.getGuid();
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to store userResourcePermission because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to store userResourcePermission because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to store userResourcePermission.", ex);
            throw new DataStoreException("Failed to store userResourcePermission.", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINE)) log.fine("storeUserResourcePermission(): guid = " + guid);
        return guid;
    }

    @Override
    public String createUserResourcePermission(UserResourcePermissionDataObject userResourcePermission) throws BaseException
    {
        // The createdTime field will be automatically set in storeUserResourcePermission().
        //Long createdTime = userResourcePermission.getCreatedTime();
        //if(createdTime == null) {
        //    createdTime = System.currentTimeMillis();
        //    userResourcePermission.setCreatedTime(createdTime);
        //}
        //Long modifiedTime = userResourcePermission.getModifiedTime();
        //if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
        //    userResourcePermission.setModifiedTime(createdTime);
        //}
        return storeUserResourcePermission(userResourcePermission);
    }

    @Override
	public Boolean updateUserResourcePermission(UserResourcePermissionDataObject userResourcePermission) throws BaseException
	{
	    // Modify the modifiedTime timestamp.
	    // CreatedTime should, normally, be non-null.
	    // If null, however, it will be automatically set in storeUserResourcePermission()
	    // (in which case modifiedTime might be updated again).
	    userResourcePermission.setModifiedTime(System.currentTimeMillis());
	    String guid = storeUserResourcePermission(userResourcePermission);
	    if(guid != null) {
	        return true;
	    } else {
	        return false;
	    }
	}
	
    @Override
    public Boolean deleteUserResourcePermission(UserResourcePermissionDataObject userResourcePermission) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        PersistenceManager pm = getPersistenceManager();
        try {
            pm.deletePersistent(userResourcePermission);
            // TBD: How do you know the deletePersistent() call was successful???
            suc = true;
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to delete userResourcePermission because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete userResourcePermission because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete userResourcePermission.", ex);
            throw new DataStoreException("Failed to delete userResourcePermission.", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteUserResourcePermission(String guid) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            try {
                //Key key = UserResourcePermissionDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = UserResourcePermissionDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                UserResourcePermissionDataObject userResourcePermission = pm.getObjectById(UserResourcePermissionDataObject.class, key);
                pm.deletePersistent(userResourcePermission);
                // TBD: How do you know the deletePersistent() call was successful???
                suc = true;
            } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
                // Datastore is read-only, possibly due to the Google App Engine maintenance.
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete userResourcePermission because the datastore is currently read-only. guid = " + guid, ex);
                throw new ServiceUnavailableException("Failed to delete userResourcePermission because the datastore is currently read-only.", ex);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete userResourcePermission for guid = " + guid, ex);
                throw new DataStoreException("Failed to delete userResourcePermission for guid = " + guid, ex);
            } finally {
                try {
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
	}

    @Override
    public Long deleteUserResourcePermissions(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultUserResourcePermissionDAO.deleteUserResourcePermissions(): filter=" + filter + "; params=" + params);

        // TBD: Delete-by-query requests should be carefully checked because of its potential harms.
        if(filter == null || filter.isEmpty()) {
            throw new RequestConflictException("deleteAll() without filter is not supported for performance/safety reasons");
        }

        Long count = 0L;
        PersistenceManager pm = getPersistenceManager();
        try {
            Query q = pm.newQuery(UserResourcePermissionDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.deletePersistentAll(values.toArray(new Object[0]));
            } else {
                count = (Long) q.deletePersistentAll();
            }
            // TBD: How do you know the deletePersistent() call was successful???
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to deleteuserResourcePermissions because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete userResourcePermissions because the datastore is currently read-only.", ex);
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to delete userResourcePermissions because index is missing", ex);
            throw new DataStoreException("Failed to delete userResourcePermissions because index is missing", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete userResourcePermissions", ex);
            throw new DataStoreException("Failed to delete userResourcePermissions", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
