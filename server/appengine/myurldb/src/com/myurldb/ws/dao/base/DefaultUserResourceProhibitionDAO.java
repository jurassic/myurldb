package com.myurldb.ws.dao.base;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

//import com.google.appengine.api.datastore.Key;

import com.myurldb.ws.core.GUID;
import com.myurldb.ws.config.Config;
import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.DataStoreException;
import com.myurldb.ws.exception.ServiceUnavailableException;
import com.myurldb.ws.exception.RequestConflictException;
import com.myurldb.ws.exception.ResourceNotFoundException;
import com.myurldb.ws.exception.NotImplementedException;
import com.myurldb.ws.dao.UserResourceProhibitionDAO;
import com.myurldb.ws.data.UserResourceProhibitionDataObject;


public class DefaultUserResourceProhibitionDAO extends DefaultDAOBase implements UserResourceProhibitionDAO
{
    private static final Logger log = Logger.getLogger(DefaultUserResourceProhibitionDAO.class.getName()); 

    // Returns the userResourceProhibition for the given guid/key.
    // Returns null if none is found in the datastore.
    @Override
    public UserResourceProhibitionDataObject getUserResourceProhibition(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: guid = " + guid);

        UserResourceProhibitionDataObject userResourceProhibition = null;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            //Transaction tx = pm.currentTransaction();
            try {
                //tx.setOptimistic(true);
                //tx.begin();
                //Key key = UserResourceProhibitionDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = UserResourceProhibitionDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                userResourceProhibition = pm.getObjectById(UserResourceProhibitionDataObject.class, key);
                //tx.commit();
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve userResourceProhibition for guid = " + guid, ex);
                throw new ResourceNotFoundException(ex);
            } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        log.finer("END");
	    return userResourceProhibition;
	}

    @Override
    public List<UserResourceProhibitionDataObject> getUserResourceProhibitions(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // temporary implementation
        List<UserResourceProhibitionDataObject> userResourceProhibitions = null;
        if(guids != null && !guids.isEmpty()) {
            userResourceProhibitions = new ArrayList<UserResourceProhibitionDataObject>();
            for(String guid : guids) {
                UserResourceProhibitionDataObject obj = getUserResourceProhibition(guid); 
                userResourceProhibitions.add(obj);
            }
	    }

        log.finer("END");
        return userResourceProhibitions;
    }

    @Override
    public List<UserResourceProhibitionDataObject> getAllUserResourceProhibitions() throws BaseException
	{
	    return getAllUserResourceProhibitions(null, null, null);
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<UserResourceProhibitionDataObject> getAllUserResourceProhibitions(String ordering, Long offset, Integer count) throws BaseException
	{
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count);

    	List<UserResourceProhibitionDataObject> userResourceProhibitions = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(UserResourceProhibitionDataObject.class);
            if(ordering != null) {
                q.setOrdering(ordering);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            userResourceProhibitions = (List<UserResourceProhibitionDataObject>) q.execute();
            if(userResourceProhibitions != null) {
                userResourceProhibitions.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            /*
            // ???
            Collection<UserResourceProhibitionDataObject> rs_userResourceProhibitions = (Collection<UserResourceProhibitionDataObject>) q.execute();
            if(rs_userResourceProhibitions == null) {
                log.log(Level.WARNING, "Failed to retrieve all userResourceProhibitions.");
                userResourceProhibitions = new ArrayList<UserResourceProhibitionDataObject>();  // ???           
            } else {
                userResourceProhibitions = new ArrayList<UserResourceProhibitionDataObject>(pm.detachCopyAll(rs_userResourceProhibitions));
            }
            */
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all userResourceProhibitions.", ex);
            //userResourceProhibitions = new ArrayList<UserResourceProhibitionDataObject>();  // ???
            throw new DataStoreException("Failed to retrieve all userResourceProhibitions.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return userResourceProhibitions;
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<String> getAllUserResourceProhibitionKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count);

    	List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery("select key from " + UserResourceProhibitionDataObject.class.getName());  // ???
            if(ordering != null) {
                q.setOrdering(ordering);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            keys = (List<String>) q.execute();
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all UserResourceProhibition keys.", ex);
            throw new DataStoreException("Failed to retrieve all UserResourceProhibition keys.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return keys;
    }

    @Override
	public List<UserResourceProhibitionDataObject> findUserResourceProhibitions(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findUserResourceProhibitions(filter, ordering, params, values, null, null);
    }

    @Override
	public List<UserResourceProhibitionDataObject> findUserResourceProhibitions(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findUserResourceProhibitions(filter, ordering, params, values, null, null, offset, count);
	}

    @SuppressWarnings("unchecked")
    @Override
	public List<UserResourceProhibitionDataObject> findUserResourceProhibitions(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
        if(log.isLoggable(Level.INFO)) log.info("DefaultUserResourceProhibitionDAO.findUserResourceProhibitions(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("findUserResourceProhibitions() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        List<UserResourceProhibitionDataObject> userResourceProhibitions = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(UserResourceProhibitionDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                userResourceProhibitions = (List<UserResourceProhibitionDataObject>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                userResourceProhibitions = (List<UserResourceProhibitionDataObject>) q.execute();
            }
            if(userResourceProhibitions != null) {
                userResourceProhibitions.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //while($TypeUtil.deCapitalizePluralize($typeName).iterator().hasNext()) {
            //    for(UserResourceProhibitionDataObject dobj : userResourceProhibitions) {
            //        //
            //    }
            //}
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find userResourceProhibitions because index is missing.", ex);
            //userResourceProhibitions = new ArrayList<UserResourceProhibitionDataObject>();  // ???
            throw new DataStoreException("Failed to find userResourceProhibitions because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find userResourceProhibitions meeting the criterion.", ex);
            //userResourceProhibitions = new ArrayList<UserResourceProhibitionDataObject>();  // ???
            throw new DataStoreException("Failed to find userResourceProhibitions meeting the criterion.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return userResourceProhibitions;
	}

    @SuppressWarnings("unchecked")
    @Override
    public List<String> findUserResourceProhibitionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultUserResourceProhibitionDAO.findUserResourceProhibitionKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery("select key from " + UserResourceProhibitionDataObject.class.getName());  // ???
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                keys = (List<String>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                keys = (List<String>) q.execute();
            }
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find UserResourceProhibition keys because index is missing.", ex);
            throw new DataStoreException("Failed to find UserResourceProhibition keys because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find UserResourceProhibition keys meeting the criterion.", ex);
            throw new DataStoreException("Failed to find UserResourceProhibition keys meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END");
        return keys;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultUserResourceProhibitionDAO.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("getCount() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        Long count = 0L; 
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(UserResourceProhibitionDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(aggregate == null || aggregate.isEmpty()) {
                aggregate = "count(this)";  // By default, count. Other examples: "max(this.createdTime)", avg, sum, etc. (Note: These are currently not supported by Google App Engine.)
            }
            q.setResult(aggregate);
            //q.setResultClass(Long.class);
            // For now, we only support a single aggregate function. (Eg, no comma-separated list.)
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                count = (Long) q.execute();
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to get userResourceProhibition count because index is missing.", ex);
            throw new DataStoreException("Failed to get userResourceProhibition count because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to get userResourceProhibition count meeting the criterion.", ex);
            throw new DataStoreException("Failed to get userResourceProhibition count meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(count == null) {
            count = 0L;
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

	// Stores the userResourceProhibition in the datastore and returns its guid.
	// Returns null if the operation fails.
    private String storeUserResourceProhibition(UserResourceProhibitionDataObject userResourceProhibition) throws BaseException
    {
        log.fine("storeUserResourceProhibition() called.");

        String guid = null;
        PersistenceManager pm = getPersistenceManager();
        try {
            // TBD: If the object on the datastore has non-null created date and the arg shortLink has no created date timestamp,
            //      do not overwrite the timestamp. How to do that???
            // For now, this is ok since we use "overwrite/replace" semantics (not partial update).
            Long createdTime = userResourceProhibition.getCreatedTime();
            if(createdTime == null) {
                createdTime = System.currentTimeMillis();
                userResourceProhibition.setCreatedTime(createdTime);
            }
            Long modifiedTime = userResourceProhibition.getModifiedTime();
            if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
                userResourceProhibition.setModifiedTime(createdTime);
            }
            pm.makePersistent(userResourceProhibition); 
            // TBD: How do you know the makePersistent() call was successful???
            guid = userResourceProhibition.getGuid();
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to store userResourceProhibition because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to store userResourceProhibition because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to store userResourceProhibition.", ex);
            throw new DataStoreException("Failed to store userResourceProhibition.", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINE)) log.fine("storeUserResourceProhibition(): guid = " + guid);
        return guid;
    }

    @Override
    public String createUserResourceProhibition(UserResourceProhibitionDataObject userResourceProhibition) throws BaseException
    {
        // The createdTime field will be automatically set in storeUserResourceProhibition().
        //Long createdTime = userResourceProhibition.getCreatedTime();
        //if(createdTime == null) {
        //    createdTime = System.currentTimeMillis();
        //    userResourceProhibition.setCreatedTime(createdTime);
        //}
        //Long modifiedTime = userResourceProhibition.getModifiedTime();
        //if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
        //    userResourceProhibition.setModifiedTime(createdTime);
        //}
        return storeUserResourceProhibition(userResourceProhibition);
    }

    @Override
	public Boolean updateUserResourceProhibition(UserResourceProhibitionDataObject userResourceProhibition) throws BaseException
	{
	    // Modify the modifiedTime timestamp.
	    // CreatedTime should, normally, be non-null.
	    // If null, however, it will be automatically set in storeUserResourceProhibition()
	    // (in which case modifiedTime might be updated again).
	    userResourceProhibition.setModifiedTime(System.currentTimeMillis());
	    String guid = storeUserResourceProhibition(userResourceProhibition);
	    if(guid != null) {
	        return true;
	    } else {
	        return false;
	    }
	}
	
    @Override
    public Boolean deleteUserResourceProhibition(UserResourceProhibitionDataObject userResourceProhibition) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        PersistenceManager pm = getPersistenceManager();
        try {
            pm.deletePersistent(userResourceProhibition);
            // TBD: How do you know the deletePersistent() call was successful???
            suc = true;
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to delete userResourceProhibition because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete userResourceProhibition because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete userResourceProhibition.", ex);
            throw new DataStoreException("Failed to delete userResourceProhibition.", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteUserResourceProhibition(String guid) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            try {
                //Key key = UserResourceProhibitionDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = UserResourceProhibitionDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                UserResourceProhibitionDataObject userResourceProhibition = pm.getObjectById(UserResourceProhibitionDataObject.class, key);
                pm.deletePersistent(userResourceProhibition);
                // TBD: How do you know the deletePersistent() call was successful???
                suc = true;
            } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
                // Datastore is read-only, possibly due to the Google App Engine maintenance.
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete userResourceProhibition because the datastore is currently read-only. guid = " + guid, ex);
                throw new ServiceUnavailableException("Failed to delete userResourceProhibition because the datastore is currently read-only.", ex);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete userResourceProhibition for guid = " + guid, ex);
                throw new DataStoreException("Failed to delete userResourceProhibition for guid = " + guid, ex);
            } finally {
                try {
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
	}

    @Override
    public Long deleteUserResourceProhibitions(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultUserResourceProhibitionDAO.deleteUserResourceProhibitions(): filter=" + filter + "; params=" + params);

        // TBD: Delete-by-query requests should be carefully checked because of its potential harms.
        if(filter == null || filter.isEmpty()) {
            throw new RequestConflictException("deleteAll() without filter is not supported for performance/safety reasons");
        }

        Long count = 0L;
        PersistenceManager pm = getPersistenceManager();
        try {
            Query q = pm.newQuery(UserResourceProhibitionDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.deletePersistentAll(values.toArray(new Object[0]));
            } else {
                count = (Long) q.deletePersistentAll();
            }
            // TBD: How do you know the deletePersistent() call was successful???
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to deleteuserResourceProhibitions because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete userResourceProhibitions because the datastore is currently read-only.", ex);
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to delete userResourceProhibitions because index is missing", ex);
            throw new DataStoreException("Failed to delete userResourceProhibitions because index is missing", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete userResourceProhibitions", ex);
            throw new DataStoreException("Failed to delete userResourceProhibitions", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
