package com.myurldb.ws.dao.base;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

//import com.google.appengine.api.datastore.Key;

import com.myurldb.ws.core.GUID;
import com.myurldb.ws.config.Config;
import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.DataStoreException;
import com.myurldb.ws.exception.ServiceUnavailableException;
import com.myurldb.ws.exception.RequestConflictException;
import com.myurldb.ws.exception.ResourceNotFoundException;
import com.myurldb.ws.exception.NotImplementedException;
import com.myurldb.ws.dao.UserRoleDAO;
import com.myurldb.ws.data.UserRoleDataObject;


public class DefaultUserRoleDAO extends DefaultDAOBase implements UserRoleDAO
{
    private static final Logger log = Logger.getLogger(DefaultUserRoleDAO.class.getName()); 

    // Returns the userRole for the given guid/key.
    // Returns null if none is found in the datastore.
    @Override
    public UserRoleDataObject getUserRole(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: guid = " + guid);

        UserRoleDataObject userRole = null;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            //Transaction tx = pm.currentTransaction();
            try {
                //tx.setOptimistic(true);
                //tx.begin();
                //Key key = UserRoleDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = UserRoleDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                userRole = pm.getObjectById(UserRoleDataObject.class, key);
                //tx.commit();
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve userRole for guid = " + guid, ex);
                throw new ResourceNotFoundException(ex);
            } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        log.finer("END");
	    return userRole;
	}

    @Override
    public List<UserRoleDataObject> getUserRoles(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // temporary implementation
        List<UserRoleDataObject> userRoles = null;
        if(guids != null && !guids.isEmpty()) {
            userRoles = new ArrayList<UserRoleDataObject>();
            for(String guid : guids) {
                UserRoleDataObject obj = getUserRole(guid); 
                userRoles.add(obj);
            }
	    }

        log.finer("END");
        return userRoles;
    }

    @Override
    public List<UserRoleDataObject> getAllUserRoles() throws BaseException
	{
	    return getAllUserRoles(null, null, null);
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<UserRoleDataObject> getAllUserRoles(String ordering, Long offset, Integer count) throws BaseException
	{
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count);

    	List<UserRoleDataObject> userRoles = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(UserRoleDataObject.class);
            if(ordering != null) {
                q.setOrdering(ordering);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            userRoles = (List<UserRoleDataObject>) q.execute();
            if(userRoles != null) {
                userRoles.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            /*
            // ???
            Collection<UserRoleDataObject> rs_userRoles = (Collection<UserRoleDataObject>) q.execute();
            if(rs_userRoles == null) {
                log.log(Level.WARNING, "Failed to retrieve all userRoles.");
                userRoles = new ArrayList<UserRoleDataObject>();  // ???           
            } else {
                userRoles = new ArrayList<UserRoleDataObject>(pm.detachCopyAll(rs_userRoles));
            }
            */
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all userRoles.", ex);
            //userRoles = new ArrayList<UserRoleDataObject>();  // ???
            throw new DataStoreException("Failed to retrieve all userRoles.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return userRoles;
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<String> getAllUserRoleKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count);

    	List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery("select key from " + UserRoleDataObject.class.getName());  // ???
            if(ordering != null) {
                q.setOrdering(ordering);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            keys = (List<String>) q.execute();
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all UserRole keys.", ex);
            throw new DataStoreException("Failed to retrieve all UserRole keys.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return keys;
    }

    @Override
	public List<UserRoleDataObject> findUserRoles(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findUserRoles(filter, ordering, params, values, null, null);
    }

    @Override
	public List<UserRoleDataObject> findUserRoles(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findUserRoles(filter, ordering, params, values, null, null, offset, count);
	}

    @SuppressWarnings("unchecked")
    @Override
	public List<UserRoleDataObject> findUserRoles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
        if(log.isLoggable(Level.INFO)) log.info("DefaultUserRoleDAO.findUserRoles(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("findUserRoles() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        List<UserRoleDataObject> userRoles = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(UserRoleDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                userRoles = (List<UserRoleDataObject>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                userRoles = (List<UserRoleDataObject>) q.execute();
            }
            if(userRoles != null) {
                userRoles.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //while($TypeUtil.deCapitalizePluralize($typeName).iterator().hasNext()) {
            //    for(UserRoleDataObject dobj : userRoles) {
            //        //
            //    }
            //}
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find userRoles because index is missing.", ex);
            //userRoles = new ArrayList<UserRoleDataObject>();  // ???
            throw new DataStoreException("Failed to find userRoles because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find userRoles meeting the criterion.", ex);
            //userRoles = new ArrayList<UserRoleDataObject>();  // ???
            throw new DataStoreException("Failed to find userRoles meeting the criterion.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return userRoles;
	}

    @SuppressWarnings("unchecked")
    @Override
    public List<String> findUserRoleKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultUserRoleDAO.findUserRoleKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery("select key from " + UserRoleDataObject.class.getName());  // ???
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                keys = (List<String>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                keys = (List<String>) q.execute();
            }
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find UserRole keys because index is missing.", ex);
            throw new DataStoreException("Failed to find UserRole keys because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find UserRole keys meeting the criterion.", ex);
            throw new DataStoreException("Failed to find UserRole keys meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END");
        return keys;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultUserRoleDAO.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("getCount() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        Long count = 0L; 
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(UserRoleDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(aggregate == null || aggregate.isEmpty()) {
                aggregate = "count(this)";  // By default, count. Other examples: "max(this.createdTime)", avg, sum, etc. (Note: These are currently not supported by Google App Engine.)
            }
            q.setResult(aggregate);
            //q.setResultClass(Long.class);
            // For now, we only support a single aggregate function. (Eg, no comma-separated list.)
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                count = (Long) q.execute();
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to get userRole count because index is missing.", ex);
            throw new DataStoreException("Failed to get userRole count because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to get userRole count meeting the criterion.", ex);
            throw new DataStoreException("Failed to get userRole count meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(count == null) {
            count = 0L;
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

	// Stores the userRole in the datastore and returns its guid.
	// Returns null if the operation fails.
    private String storeUserRole(UserRoleDataObject userRole) throws BaseException
    {
        log.fine("storeUserRole() called.");

        String guid = null;
        PersistenceManager pm = getPersistenceManager();
        try {
            // TBD: If the object on the datastore has non-null created date and the arg shortLink has no created date timestamp,
            //      do not overwrite the timestamp. How to do that???
            // For now, this is ok since we use "overwrite/replace" semantics (not partial update).
            Long createdTime = userRole.getCreatedTime();
            if(createdTime == null) {
                createdTime = System.currentTimeMillis();
                userRole.setCreatedTime(createdTime);
            }
            Long modifiedTime = userRole.getModifiedTime();
            if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
                userRole.setModifiedTime(createdTime);
            }
            pm.makePersistent(userRole); 
            // TBD: How do you know the makePersistent() call was successful???
            guid = userRole.getGuid();
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to store userRole because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to store userRole because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to store userRole.", ex);
            throw new DataStoreException("Failed to store userRole.", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINE)) log.fine("storeUserRole(): guid = " + guid);
        return guid;
    }

    @Override
    public String createUserRole(UserRoleDataObject userRole) throws BaseException
    {
        // The createdTime field will be automatically set in storeUserRole().
        //Long createdTime = userRole.getCreatedTime();
        //if(createdTime == null) {
        //    createdTime = System.currentTimeMillis();
        //    userRole.setCreatedTime(createdTime);
        //}
        //Long modifiedTime = userRole.getModifiedTime();
        //if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
        //    userRole.setModifiedTime(createdTime);
        //}
        return storeUserRole(userRole);
    }

    @Override
	public Boolean updateUserRole(UserRoleDataObject userRole) throws BaseException
	{
	    // Modify the modifiedTime timestamp.
	    // CreatedTime should, normally, be non-null.
	    // If null, however, it will be automatically set in storeUserRole()
	    // (in which case modifiedTime might be updated again).
	    userRole.setModifiedTime(System.currentTimeMillis());
	    String guid = storeUserRole(userRole);
	    if(guid != null) {
	        return true;
	    } else {
	        return false;
	    }
	}
	
    @Override
    public Boolean deleteUserRole(UserRoleDataObject userRole) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        PersistenceManager pm = getPersistenceManager();
        try {
            pm.deletePersistent(userRole);
            // TBD: How do you know the deletePersistent() call was successful???
            suc = true;
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to delete userRole because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete userRole because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete userRole.", ex);
            throw new DataStoreException("Failed to delete userRole.", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteUserRole(String guid) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            try {
                //Key key = UserRoleDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = UserRoleDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                UserRoleDataObject userRole = pm.getObjectById(UserRoleDataObject.class, key);
                pm.deletePersistent(userRole);
                // TBD: How do you know the deletePersistent() call was successful???
                suc = true;
            } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
                // Datastore is read-only, possibly due to the Google App Engine maintenance.
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete userRole because the datastore is currently read-only. guid = " + guid, ex);
                throw new ServiceUnavailableException("Failed to delete userRole because the datastore is currently read-only.", ex);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete userRole for guid = " + guid, ex);
                throw new DataStoreException("Failed to delete userRole for guid = " + guid, ex);
            } finally {
                try {
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
	}

    @Override
    public Long deleteUserRoles(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultUserRoleDAO.deleteUserRoles(): filter=" + filter + "; params=" + params);

        // TBD: Delete-by-query requests should be carefully checked because of its potential harms.
        if(filter == null || filter.isEmpty()) {
            throw new RequestConflictException("deleteAll() without filter is not supported for performance/safety reasons");
        }

        Long count = 0L;
        PersistenceManager pm = getPersistenceManager();
        try {
            Query q = pm.newQuery(UserRoleDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.deletePersistentAll(values.toArray(new Object[0]));
            } else {
                count = (Long) q.deletePersistentAll();
            }
            // TBD: How do you know the deletePersistent() call was successful???
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to deleteuserRoles because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete userRoles because the datastore is currently read-only.", ex);
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to delete userRoles because index is missing", ex);
            throw new DataStoreException("Failed to delete userRoles because index is missing", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete userRoles", ex);
            throw new DataStoreException("Failed to delete userRoles", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
