package com.myurldb.ws.dao.base;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

//import com.google.appengine.api.datastore.Key;

import com.myurldb.ws.core.GUID;
import com.myurldb.ws.config.Config;
import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.DataStoreException;
import com.myurldb.ws.exception.ServiceUnavailableException;
import com.myurldb.ws.exception.RequestConflictException;
import com.myurldb.ws.exception.ResourceNotFoundException;
import com.myurldb.ws.exception.NotImplementedException;
import com.myurldb.ws.dao.VisitorSettingDAO;
import com.myurldb.ws.data.VisitorSettingDataObject;


public class DefaultVisitorSettingDAO extends DefaultDAOBase implements VisitorSettingDAO
{
    private static final Logger log = Logger.getLogger(DefaultVisitorSettingDAO.class.getName()); 

    // Returns the visitorSetting for the given guid/key.
    // Returns null if none is found in the datastore.
    @Override
    public VisitorSettingDataObject getVisitorSetting(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: guid = " + guid);

        VisitorSettingDataObject visitorSetting = null;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            //Transaction tx = pm.currentTransaction();
            try {
                //tx.setOptimistic(true);
                //tx.begin();
                //Key key = VisitorSettingDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = VisitorSettingDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                visitorSetting = pm.getObjectById(VisitorSettingDataObject.class, key);
                //tx.commit();
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve visitorSetting for guid = " + guid, ex);
                throw new ResourceNotFoundException(ex);
            } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        log.finer("END");
	    return visitorSetting;
	}

    @Override
    public List<VisitorSettingDataObject> getVisitorSettings(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // temporary implementation
        List<VisitorSettingDataObject> visitorSettings = null;
        if(guids != null && !guids.isEmpty()) {
            visitorSettings = new ArrayList<VisitorSettingDataObject>();
            for(String guid : guids) {
                VisitorSettingDataObject obj = getVisitorSetting(guid); 
                visitorSettings.add(obj);
            }
	    }

        log.finer("END");
        return visitorSettings;
    }

    @Override
    public List<VisitorSettingDataObject> getAllVisitorSettings() throws BaseException
	{
	    return getAllVisitorSettings(null, null, null);
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<VisitorSettingDataObject> getAllVisitorSettings(String ordering, Long offset, Integer count) throws BaseException
	{
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count);

    	List<VisitorSettingDataObject> visitorSettings = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(VisitorSettingDataObject.class);
            if(ordering != null) {
                q.setOrdering(ordering);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            visitorSettings = (List<VisitorSettingDataObject>) q.execute();
            if(visitorSettings != null) {
                visitorSettings.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            /*
            // ???
            Collection<VisitorSettingDataObject> rs_visitorSettings = (Collection<VisitorSettingDataObject>) q.execute();
            if(rs_visitorSettings == null) {
                log.log(Level.WARNING, "Failed to retrieve all visitorSettings.");
                visitorSettings = new ArrayList<VisitorSettingDataObject>();  // ???           
            } else {
                visitorSettings = new ArrayList<VisitorSettingDataObject>(pm.detachCopyAll(rs_visitorSettings));
            }
            */
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all visitorSettings.", ex);
            //visitorSettings = new ArrayList<VisitorSettingDataObject>();  // ???
            throw new DataStoreException("Failed to retrieve all visitorSettings.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return visitorSettings;
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<String> getAllVisitorSettingKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count);

    	List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery("select key from " + VisitorSettingDataObject.class.getName());  // ???
            if(ordering != null) {
                q.setOrdering(ordering);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            keys = (List<String>) q.execute();
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all VisitorSetting keys.", ex);
            throw new DataStoreException("Failed to retrieve all VisitorSetting keys.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return keys;
    }

    @Override
	public List<VisitorSettingDataObject> findVisitorSettings(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findVisitorSettings(filter, ordering, params, values, null, null);
    }

    @Override
	public List<VisitorSettingDataObject> findVisitorSettings(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findVisitorSettings(filter, ordering, params, values, null, null, offset, count);
	}

    @SuppressWarnings("unchecked")
    @Override
	public List<VisitorSettingDataObject> findVisitorSettings(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
        if(log.isLoggable(Level.INFO)) log.info("DefaultVisitorSettingDAO.findVisitorSettings(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("findVisitorSettings() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        List<VisitorSettingDataObject> visitorSettings = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(VisitorSettingDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                visitorSettings = (List<VisitorSettingDataObject>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                visitorSettings = (List<VisitorSettingDataObject>) q.execute();
            }
            if(visitorSettings != null) {
                visitorSettings.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //while($TypeUtil.deCapitalizePluralize($typeName).iterator().hasNext()) {
            //    for(VisitorSettingDataObject dobj : visitorSettings) {
            //        //
            //    }
            //}
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find visitorSettings because index is missing.", ex);
            //visitorSettings = new ArrayList<VisitorSettingDataObject>();  // ???
            throw new DataStoreException("Failed to find visitorSettings because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find visitorSettings meeting the criterion.", ex);
            //visitorSettings = new ArrayList<VisitorSettingDataObject>();  // ???
            throw new DataStoreException("Failed to find visitorSettings meeting the criterion.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return visitorSettings;
	}

    @SuppressWarnings("unchecked")
    @Override
    public List<String> findVisitorSettingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultVisitorSettingDAO.findVisitorSettingKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery("select key from " + VisitorSettingDataObject.class.getName());  // ???
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                keys = (List<String>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                keys = (List<String>) q.execute();
            }
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find VisitorSetting keys because index is missing.", ex);
            throw new DataStoreException("Failed to find VisitorSetting keys because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find VisitorSetting keys meeting the criterion.", ex);
            throw new DataStoreException("Failed to find VisitorSetting keys meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END");
        return keys;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultVisitorSettingDAO.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("getCount() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        Long count = 0L; 
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(VisitorSettingDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(aggregate == null || aggregate.isEmpty()) {
                aggregate = "count(this)";  // By default, count. Other examples: "max(this.createdTime)", avg, sum, etc. (Note: These are currently not supported by Google App Engine.)
            }
            q.setResult(aggregate);
            //q.setResultClass(Long.class);
            // For now, we only support a single aggregate function. (Eg, no comma-separated list.)
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                count = (Long) q.execute();
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to get visitorSetting count because index is missing.", ex);
            throw new DataStoreException("Failed to get visitorSetting count because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to get visitorSetting count meeting the criterion.", ex);
            throw new DataStoreException("Failed to get visitorSetting count meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(count == null) {
            count = 0L;
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

	// Stores the visitorSetting in the datastore and returns its guid.
	// Returns null if the operation fails.
    private String storeVisitorSetting(VisitorSettingDataObject visitorSetting) throws BaseException
    {
        log.fine("storeVisitorSetting() called.");

        String guid = null;
        PersistenceManager pm = getPersistenceManager();
        try {
            // TBD: If the object on the datastore has non-null created date and the arg shortLink has no created date timestamp,
            //      do not overwrite the timestamp. How to do that???
            // For now, this is ok since we use "overwrite/replace" semantics (not partial update).
            Long createdTime = visitorSetting.getCreatedTime();
            if(createdTime == null) {
                createdTime = System.currentTimeMillis();
                visitorSetting.setCreatedTime(createdTime);
            }
            Long modifiedTime = visitorSetting.getModifiedTime();
            if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
                visitorSetting.setModifiedTime(createdTime);
            }
            pm.makePersistent(visitorSetting); 
            // TBD: How do you know the makePersistent() call was successful???
            guid = visitorSetting.getGuid();
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to store visitorSetting because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to store visitorSetting because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to store visitorSetting.", ex);
            throw new DataStoreException("Failed to store visitorSetting.", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINE)) log.fine("storeVisitorSetting(): guid = " + guid);
        return guid;
    }

    @Override
    public String createVisitorSetting(VisitorSettingDataObject visitorSetting) throws BaseException
    {
        // The createdTime field will be automatically set in storeVisitorSetting().
        //Long createdTime = visitorSetting.getCreatedTime();
        //if(createdTime == null) {
        //    createdTime = System.currentTimeMillis();
        //    visitorSetting.setCreatedTime(createdTime);
        //}
        //Long modifiedTime = visitorSetting.getModifiedTime();
        //if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
        //    visitorSetting.setModifiedTime(createdTime);
        //}
        return storeVisitorSetting(visitorSetting);
    }

    @Override
	public Boolean updateVisitorSetting(VisitorSettingDataObject visitorSetting) throws BaseException
	{
	    // Modify the modifiedTime timestamp.
	    // CreatedTime should, normally, be non-null.
	    // If null, however, it will be automatically set in storeVisitorSetting()
	    // (in which case modifiedTime might be updated again).
	    visitorSetting.setModifiedTime(System.currentTimeMillis());
	    String guid = storeVisitorSetting(visitorSetting);
	    if(guid != null) {
	        return true;
	    } else {
	        return false;
	    }
	}
	
    @Override
    public Boolean deleteVisitorSetting(VisitorSettingDataObject visitorSetting) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        PersistenceManager pm = getPersistenceManager();
        try {
            pm.deletePersistent(visitorSetting);
            // TBD: How do you know the deletePersistent() call was successful???
            suc = true;
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to delete visitorSetting because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete visitorSetting because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete visitorSetting.", ex);
            throw new DataStoreException("Failed to delete visitorSetting.", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteVisitorSetting(String guid) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            try {
                //Key key = VisitorSettingDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = VisitorSettingDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                VisitorSettingDataObject visitorSetting = pm.getObjectById(VisitorSettingDataObject.class, key);
                pm.deletePersistent(visitorSetting);
                // TBD: How do you know the deletePersistent() call was successful???
                suc = true;
            } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
                // Datastore is read-only, possibly due to the Google App Engine maintenance.
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete visitorSetting because the datastore is currently read-only. guid = " + guid, ex);
                throw new ServiceUnavailableException("Failed to delete visitorSetting because the datastore is currently read-only.", ex);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete visitorSetting for guid = " + guid, ex);
                throw new DataStoreException("Failed to delete visitorSetting for guid = " + guid, ex);
            } finally {
                try {
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
	}

    @Override
    public Long deleteVisitorSettings(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultVisitorSettingDAO.deleteVisitorSettings(): filter=" + filter + "; params=" + params);

        // TBD: Delete-by-query requests should be carefully checked because of its potential harms.
        if(filter == null || filter.isEmpty()) {
            throw new RequestConflictException("deleteAll() without filter is not supported for performance/safety reasons");
        }

        Long count = 0L;
        PersistenceManager pm = getPersistenceManager();
        try {
            Query q = pm.newQuery(VisitorSettingDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.deletePersistentAll(values.toArray(new Object[0]));
            } else {
                count = (Long) q.deletePersistentAll();
            }
            // TBD: How do you know the deletePersistent() call was successful???
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to deletevisitorSettings because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete visitorSettings because the datastore is currently read-only.", ex);
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to delete visitorSettings because index is missing", ex);
            throw new DataStoreException("Failed to delete visitorSettings because index is missing", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete visitorSettings", ex);
            throw new DataStoreException("Failed to delete visitorSettings", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
