package com.myurldb.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.myurldb.ws.AbuseTag;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;


@PersistenceCapable(detachable="true")
public class AbuseTagDataObject extends KeyedDataObject implements AbuseTag
{
    private static final Logger log = Logger.getLogger(AbuseTagDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(AbuseTagDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(AbuseTagDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String guid;

    @Persistent(defaultFetchGroup = "true")
    private String shortLink;

    @Persistent(defaultFetchGroup = "true")
    private String shortUrl;

    @Persistent(defaultFetchGroup = "true")
    private String longUrl;

    @Persistent(defaultFetchGroup = "true")
    private String longUrlHash;

    @Persistent(defaultFetchGroup = "true")
    private String user;

    @Persistent(defaultFetchGroup = "true")
    private String ipAddress;

    @Persistent(defaultFetchGroup = "true")
    private Integer tag;

    @Persistent(defaultFetchGroup = "true")
    private String comment;

    @Persistent(defaultFetchGroup = "true")
    private String status;

    @Persistent(defaultFetchGroup = "true")
    private String reviewer;

    @Persistent(defaultFetchGroup = "true")
    private String action;

    @Persistent(defaultFetchGroup = "true")
    private String note;

    @Persistent(defaultFetchGroup = "true")
    private String pastActions;

    @Persistent(defaultFetchGroup = "true")
    private Long reviewedTime;

    @Persistent(defaultFetchGroup = "true")
    private Long actionTime;

    public AbuseTagDataObject()
    {
        this(null);
    }
    public AbuseTagDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public AbuseTagDataObject(String guid, String shortLink, String shortUrl, String longUrl, String longUrlHash, String user, String ipAddress, Integer tag, String comment, String status, String reviewer, String action, String note, String pastActions, Long reviewedTime, Long actionTime)
    {
        this(guid, shortLink, shortUrl, longUrl, longUrlHash, user, ipAddress, tag, comment, status, reviewer, action, note, pastActions, reviewedTime, actionTime, null, null);
    }
    public AbuseTagDataObject(String guid, String shortLink, String shortUrl, String longUrl, String longUrlHash, String user, String ipAddress, Integer tag, String comment, String status, String reviewer, String action, String note, String pastActions, Long reviewedTime, Long actionTime, Long createdTime, Long modifiedTime)
    {
        super(createdTime, modifiedTime);
        generatePK(guid);
        this.shortLink = shortLink;
        this.shortUrl = shortUrl;
        this.longUrl = longUrl;
        this.longUrlHash = longUrlHash;
        this.user = user;
        this.ipAddress = ipAddress;
        this.tag = tag;
        this.comment = comment;
        this.status = status;
        this.reviewer = reviewer;
        this.action = action;
        this.note = note;
        this.pastActions = pastActions;
        this.reviewedTime = reviewedTime;
        this.actionTime = actionTime;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        generatePK(guid);
    }
    protected void generatePK(String guid)
    {
        if(guid == null) {
            this.guid = GUID.generate();  // temporary
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to a new guid = " + this.guid);
        } else {
            this.guid = guid;
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to guid = " + this.guid);
        }
        rebuildKey();        
    }

//    @Override
//    protected Key createKey()
//    {
//        return AbuseTagDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return AbuseTagDataObject.composeKey(getGuid());
    }

    public String getShortLink()
    {
        return this.shortLink;
    }
    public void setShortLink(String shortLink)
    {
        this.shortLink = shortLink;
    }

    public String getShortUrl()
    {
        return this.shortUrl;
    }
    public void setShortUrl(String shortUrl)
    {
        this.shortUrl = shortUrl;
    }

    public String getLongUrl()
    {
        return this.longUrl;
    }
    public void setLongUrl(String longUrl)
    {
        this.longUrl = longUrl;
    }

    public String getLongUrlHash()
    {
        return this.longUrlHash;
    }
    public void setLongUrlHash(String longUrlHash)
    {
        this.longUrlHash = longUrlHash;
    }

    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    public String getIpAddress()
    {
        return this.ipAddress;
    }
    public void setIpAddress(String ipAddress)
    {
        this.ipAddress = ipAddress;
    }

    public Integer getTag()
    {
        return this.tag;
    }
    public void setTag(Integer tag)
    {
        this.tag = tag;
    }

    public String getComment()
    {
        return this.comment;
    }
    public void setComment(String comment)
    {
        this.comment = comment;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getReviewer()
    {
        return this.reviewer;
    }
    public void setReviewer(String reviewer)
    {
        this.reviewer = reviewer;
    }

    public String getAction()
    {
        return this.action;
    }
    public void setAction(String action)
    {
        this.action = action;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    public String getPastActions()
    {
        return this.pastActions;
    }
    public void setPastActions(String pastActions)
    {
        this.pastActions = pastActions;
    }

    public Long getReviewedTime()
    {
        return this.reviewedTime;
    }
    public void setReviewedTime(Long reviewedTime)
    {
        this.reviewedTime = reviewedTime;
    }

    public Long getActionTime()
    {
        return this.actionTime;
    }
    public void setActionTime(Long actionTime)
    {
        this.actionTime = actionTime;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("guid", this.guid);
        dataMap.put("shortLink", this.shortLink);
        dataMap.put("shortUrl", this.shortUrl);
        dataMap.put("longUrl", this.longUrl);
        dataMap.put("longUrlHash", this.longUrlHash);
        dataMap.put("user", this.user);
        dataMap.put("ipAddress", this.ipAddress);
        dataMap.put("tag", this.tag);
        dataMap.put("comment", this.comment);
        dataMap.put("status", this.status);
        dataMap.put("reviewer", this.reviewer);
        dataMap.put("action", this.action);
        dataMap.put("note", this.note);
        dataMap.put("pastActions", this.pastActions);
        dataMap.put("reviewedTime", this.reviewedTime);
        dataMap.put("actionTime", this.actionTime);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        AbuseTag thatObj = (AbuseTag) obj;
        if( (this.guid == null && thatObj.getGuid() != null)
            || (this.guid != null && thatObj.getGuid() == null)
            || !this.guid.equals(thatObj.getGuid()) ) {
            return false;
        }
        if( (this.shortLink == null && thatObj.getShortLink() != null)
            || (this.shortLink != null && thatObj.getShortLink() == null)
            || !this.shortLink.equals(thatObj.getShortLink()) ) {
            return false;
        }
        if( (this.shortUrl == null && thatObj.getShortUrl() != null)
            || (this.shortUrl != null && thatObj.getShortUrl() == null)
            || !this.shortUrl.equals(thatObj.getShortUrl()) ) {
            return false;
        }
        if( (this.longUrl == null && thatObj.getLongUrl() != null)
            || (this.longUrl != null && thatObj.getLongUrl() == null)
            || !this.longUrl.equals(thatObj.getLongUrl()) ) {
            return false;
        }
        if( (this.longUrlHash == null && thatObj.getLongUrlHash() != null)
            || (this.longUrlHash != null && thatObj.getLongUrlHash() == null)
            || !this.longUrlHash.equals(thatObj.getLongUrlHash()) ) {
            return false;
        }
        if( (this.user == null && thatObj.getUser() != null)
            || (this.user != null && thatObj.getUser() == null)
            || !this.user.equals(thatObj.getUser()) ) {
            return false;
        }
        if( (this.ipAddress == null && thatObj.getIpAddress() != null)
            || (this.ipAddress != null && thatObj.getIpAddress() == null)
            || !this.ipAddress.equals(thatObj.getIpAddress()) ) {
            return false;
        }
        if( (this.tag == null && thatObj.getTag() != null)
            || (this.tag != null && thatObj.getTag() == null)
            || !this.tag.equals(thatObj.getTag()) ) {
            return false;
        }
        if( (this.comment == null && thatObj.getComment() != null)
            || (this.comment != null && thatObj.getComment() == null)
            || !this.comment.equals(thatObj.getComment()) ) {
            return false;
        }
        if( (this.status == null && thatObj.getStatus() != null)
            || (this.status != null && thatObj.getStatus() == null)
            || !this.status.equals(thatObj.getStatus()) ) {
            return false;
        }
        if( (this.reviewer == null && thatObj.getReviewer() != null)
            || (this.reviewer != null && thatObj.getReviewer() == null)
            || !this.reviewer.equals(thatObj.getReviewer()) ) {
            return false;
        }
        if( (this.action == null && thatObj.getAction() != null)
            || (this.action != null && thatObj.getAction() == null)
            || !this.action.equals(thatObj.getAction()) ) {
            return false;
        }
        if( (this.note == null && thatObj.getNote() != null)
            || (this.note != null && thatObj.getNote() == null)
            || !this.note.equals(thatObj.getNote()) ) {
            return false;
        }
        if( (this.pastActions == null && thatObj.getPastActions() != null)
            || (this.pastActions != null && thatObj.getPastActions() == null)
            || !this.pastActions.equals(thatObj.getPastActions()) ) {
            return false;
        }
        if( (this.reviewedTime == null && thatObj.getReviewedTime() != null)
            || (this.reviewedTime != null && thatObj.getReviewedTime() == null)
            || !this.reviewedTime.equals(thatObj.getReviewedTime()) ) {
            return false;
        }
        if( (this.actionTime == null && thatObj.getActionTime() != null)
            || (this.actionTime != null && thatObj.getActionTime() == null)
            || !this.actionTime.equals(thatObj.getActionTime()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = shortLink == null ? 0 : shortLink.hashCode();
        _hash = 31 * _hash + delta;
        delta = shortUrl == null ? 0 : shortUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = longUrl == null ? 0 : longUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = longUrlHash == null ? 0 : longUrlHash.hashCode();
        _hash = 31 * _hash + delta;
        delta = user == null ? 0 : user.hashCode();
        _hash = 31 * _hash + delta;
        delta = ipAddress == null ? 0 : ipAddress.hashCode();
        _hash = 31 * _hash + delta;
        delta = tag == null ? 0 : tag.hashCode();
        _hash = 31 * _hash + delta;
        delta = comment == null ? 0 : comment.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = reviewer == null ? 0 : reviewer.hashCode();
        _hash = 31 * _hash + delta;
        delta = action == null ? 0 : action.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        delta = pastActions == null ? 0 : pastActions.hashCode();
        _hash = 31 * _hash + delta;
        delta = reviewedTime == null ? 0 : reviewedTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = actionTime == null ? 0 : actionTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
