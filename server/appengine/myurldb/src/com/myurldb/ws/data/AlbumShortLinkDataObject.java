package com.myurldb.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.myurldb.ws.AlbumShortLink;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;


@PersistenceCapable(detachable="true")
public class AlbumShortLinkDataObject extends KeyedDataObject implements AlbumShortLink
{
    private static final Logger log = Logger.getLogger(AlbumShortLinkDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(AlbumShortLinkDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(AlbumShortLinkDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String guid;

    @Persistent(defaultFetchGroup = "true")
    private String user;

    @Persistent(defaultFetchGroup = "true")
    private String linkAlbum;

    @Persistent(defaultFetchGroup = "true")
    private String shortLink;

    @Persistent(defaultFetchGroup = "true")
    private String shortUrl;

    @Persistent(defaultFetchGroup = "true")
    private String longUrl;

    @Persistent(defaultFetchGroup = "true")
    private String note;

    @Persistent(defaultFetchGroup = "true")
    private String status;

    public AlbumShortLinkDataObject()
    {
        this(null);
    }
    public AlbumShortLinkDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null);
    }
    public AlbumShortLinkDataObject(String guid, String user, String linkAlbum, String shortLink, String shortUrl, String longUrl, String note, String status)
    {
        this(guid, user, linkAlbum, shortLink, shortUrl, longUrl, note, status, null, null);
    }
    public AlbumShortLinkDataObject(String guid, String user, String linkAlbum, String shortLink, String shortUrl, String longUrl, String note, String status, Long createdTime, Long modifiedTime)
    {
        super(createdTime, modifiedTime);
        generatePK(guid);
        this.user = user;
        this.linkAlbum = linkAlbum;
        this.shortLink = shortLink;
        this.shortUrl = shortUrl;
        this.longUrl = longUrl;
        this.note = note;
        this.status = status;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        generatePK(guid);
    }
    protected void generatePK(String guid)
    {
        if(guid == null) {
            this.guid = GUID.generate();  // temporary
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to a new guid = " + this.guid);
        } else {
            this.guid = guid;
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to guid = " + this.guid);
        }
        rebuildKey();        
    }

//    @Override
//    protected Key createKey()
//    {
//        return AlbumShortLinkDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return AlbumShortLinkDataObject.composeKey(getGuid());
    }

    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    public String getLinkAlbum()
    {
        return this.linkAlbum;
    }
    public void setLinkAlbum(String linkAlbum)
    {
        this.linkAlbum = linkAlbum;
    }

    public String getShortLink()
    {
        return this.shortLink;
    }
    public void setShortLink(String shortLink)
    {
        this.shortLink = shortLink;
    }

    public String getShortUrl()
    {
        return this.shortUrl;
    }
    public void setShortUrl(String shortUrl)
    {
        this.shortUrl = shortUrl;
    }

    public String getLongUrl()
    {
        return this.longUrl;
    }
    public void setLongUrl(String longUrl)
    {
        this.longUrl = longUrl;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("guid", this.guid);
        dataMap.put("user", this.user);
        dataMap.put("linkAlbum", this.linkAlbum);
        dataMap.put("shortLink", this.shortLink);
        dataMap.put("shortUrl", this.shortUrl);
        dataMap.put("longUrl", this.longUrl);
        dataMap.put("note", this.note);
        dataMap.put("status", this.status);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        AlbumShortLink thatObj = (AlbumShortLink) obj;
        if( (this.guid == null && thatObj.getGuid() != null)
            || (this.guid != null && thatObj.getGuid() == null)
            || !this.guid.equals(thatObj.getGuid()) ) {
            return false;
        }
        if( (this.user == null && thatObj.getUser() != null)
            || (this.user != null && thatObj.getUser() == null)
            || !this.user.equals(thatObj.getUser()) ) {
            return false;
        }
        if( (this.linkAlbum == null && thatObj.getLinkAlbum() != null)
            || (this.linkAlbum != null && thatObj.getLinkAlbum() == null)
            || !this.linkAlbum.equals(thatObj.getLinkAlbum()) ) {
            return false;
        }
        if( (this.shortLink == null && thatObj.getShortLink() != null)
            || (this.shortLink != null && thatObj.getShortLink() == null)
            || !this.shortLink.equals(thatObj.getShortLink()) ) {
            return false;
        }
        if( (this.shortUrl == null && thatObj.getShortUrl() != null)
            || (this.shortUrl != null && thatObj.getShortUrl() == null)
            || !this.shortUrl.equals(thatObj.getShortUrl()) ) {
            return false;
        }
        if( (this.longUrl == null && thatObj.getLongUrl() != null)
            || (this.longUrl != null && thatObj.getLongUrl() == null)
            || !this.longUrl.equals(thatObj.getLongUrl()) ) {
            return false;
        }
        if( (this.note == null && thatObj.getNote() != null)
            || (this.note != null && thatObj.getNote() == null)
            || !this.note.equals(thatObj.getNote()) ) {
            return false;
        }
        if( (this.status == null && thatObj.getStatus() != null)
            || (this.status != null && thatObj.getStatus() == null)
            || !this.status.equals(thatObj.getStatus()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = user == null ? 0 : user.hashCode();
        _hash = 31 * _hash + delta;
        delta = linkAlbum == null ? 0 : linkAlbum.hashCode();
        _hash = 31 * _hash + delta;
        delta = shortLink == null ? 0 : shortLink.hashCode();
        _hash = 31 * _hash + delta;
        delta = shortUrl == null ? 0 : shortUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = longUrl == null ? 0 : longUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
