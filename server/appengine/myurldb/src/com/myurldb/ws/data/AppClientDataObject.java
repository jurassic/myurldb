package com.myurldb.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.myurldb.ws.AppClient;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;


@PersistenceCapable(detachable="true")
public class AppClientDataObject extends KeyedDataObject implements AppClient
{
    private static final Logger log = Logger.getLogger(AppClientDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(AppClientDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(AppClientDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String guid;

    @Persistent(defaultFetchGroup = "true")
    private String owner;

    @Persistent(defaultFetchGroup = "true")
    private String admin;

    @Persistent(defaultFetchGroup = "true")
    private String name;

    @Persistent(defaultFetchGroup = "true")
    private String clientId;

    @Persistent(defaultFetchGroup = "true")
    private String clientCode;

    @Persistent(defaultFetchGroup = "true")
    private String rootDomain;

    @Persistent(defaultFetchGroup = "true")
    private String appDomain;

    @Persistent(defaultFetchGroup = "true")
    private Boolean useAppDomain;

    @Persistent(defaultFetchGroup = "true")
    private Boolean enabled;

    @Persistent(defaultFetchGroup = "true")
    private String licenseMode;

    @Persistent(defaultFetchGroup = "true")
    private String status;

    public AppClientDataObject()
    {
        this(null);
    }
    public AppClientDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public AppClientDataObject(String guid, String owner, String admin, String name, String clientId, String clientCode, String rootDomain, String appDomain, Boolean useAppDomain, Boolean enabled, String licenseMode, String status)
    {
        this(guid, owner, admin, name, clientId, clientCode, rootDomain, appDomain, useAppDomain, enabled, licenseMode, status, null, null);
    }
    public AppClientDataObject(String guid, String owner, String admin, String name, String clientId, String clientCode, String rootDomain, String appDomain, Boolean useAppDomain, Boolean enabled, String licenseMode, String status, Long createdTime, Long modifiedTime)
    {
        super(createdTime, modifiedTime);
        generatePK(guid);
        this.owner = owner;
        this.admin = admin;
        this.name = name;
        this.clientId = clientId;
        this.clientCode = clientCode;
        this.rootDomain = rootDomain;
        this.appDomain = appDomain;
        this.useAppDomain = useAppDomain;
        this.enabled = enabled;
        this.licenseMode = licenseMode;
        this.status = status;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        generatePK(guid);
    }
    protected void generatePK(String guid)
    {
        if(guid == null) {
            this.guid = GUID.generate();  // temporary
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to a new guid = " + this.guid);
        } else {
            this.guid = guid;
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to guid = " + this.guid);
        }
        rebuildKey();        
    }

//    @Override
//    protected Key createKey()
//    {
//        return AppClientDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return AppClientDataObject.composeKey(getGuid());
    }

    public String getOwner()
    {
        return this.owner;
    }
    public void setOwner(String owner)
    {
        this.owner = owner;
    }

    public String getAdmin()
    {
        return this.admin;
    }
    public void setAdmin(String admin)
    {
        this.admin = admin;
    }

    public String getName()
    {
        return this.name;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getClientId()
    {
        return this.clientId;
    }
    public void setClientId(String clientId)
    {
        this.clientId = clientId;
    }

    public String getClientCode()
    {
        return this.clientCode;
    }
    public void setClientCode(String clientCode)
    {
        this.clientCode = clientCode;
    }

    public String getRootDomain()
    {
        return this.rootDomain;
    }
    public void setRootDomain(String rootDomain)
    {
        this.rootDomain = rootDomain;
    }

    public String getAppDomain()
    {
        return this.appDomain;
    }
    public void setAppDomain(String appDomain)
    {
        this.appDomain = appDomain;
    }

    public Boolean isUseAppDomain()
    {
        return this.useAppDomain;
    }
    public void setUseAppDomain(Boolean useAppDomain)
    {
        this.useAppDomain = useAppDomain;
    }

    public Boolean isEnabled()
    {
        return this.enabled;
    }
    public void setEnabled(Boolean enabled)
    {
        this.enabled = enabled;
    }

    public String getLicenseMode()
    {
        return this.licenseMode;
    }
    public void setLicenseMode(String licenseMode)
    {
        this.licenseMode = licenseMode;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("guid", this.guid);
        dataMap.put("owner", this.owner);
        dataMap.put("admin", this.admin);
        dataMap.put("name", this.name);
        dataMap.put("clientId", this.clientId);
        dataMap.put("clientCode", this.clientCode);
        dataMap.put("rootDomain", this.rootDomain);
        dataMap.put("appDomain", this.appDomain);
        dataMap.put("useAppDomain", this.useAppDomain);
        dataMap.put("enabled", this.enabled);
        dataMap.put("licenseMode", this.licenseMode);
        dataMap.put("status", this.status);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        AppClient thatObj = (AppClient) obj;
        if( (this.guid == null && thatObj.getGuid() != null)
            || (this.guid != null && thatObj.getGuid() == null)
            || !this.guid.equals(thatObj.getGuid()) ) {
            return false;
        }
        if( (this.owner == null && thatObj.getOwner() != null)
            || (this.owner != null && thatObj.getOwner() == null)
            || !this.owner.equals(thatObj.getOwner()) ) {
            return false;
        }
        if( (this.admin == null && thatObj.getAdmin() != null)
            || (this.admin != null && thatObj.getAdmin() == null)
            || !this.admin.equals(thatObj.getAdmin()) ) {
            return false;
        }
        if( (this.name == null && thatObj.getName() != null)
            || (this.name != null && thatObj.getName() == null)
            || !this.name.equals(thatObj.getName()) ) {
            return false;
        }
        if( (this.clientId == null && thatObj.getClientId() != null)
            || (this.clientId != null && thatObj.getClientId() == null)
            || !this.clientId.equals(thatObj.getClientId()) ) {
            return false;
        }
        if( (this.clientCode == null && thatObj.getClientCode() != null)
            || (this.clientCode != null && thatObj.getClientCode() == null)
            || !this.clientCode.equals(thatObj.getClientCode()) ) {
            return false;
        }
        if( (this.rootDomain == null && thatObj.getRootDomain() != null)
            || (this.rootDomain != null && thatObj.getRootDomain() == null)
            || !this.rootDomain.equals(thatObj.getRootDomain()) ) {
            return false;
        }
        if( (this.appDomain == null && thatObj.getAppDomain() != null)
            || (this.appDomain != null && thatObj.getAppDomain() == null)
            || !this.appDomain.equals(thatObj.getAppDomain()) ) {
            return false;
        }
        if( (this.useAppDomain == null && thatObj.isUseAppDomain() != null)
            || (this.useAppDomain != null && thatObj.isUseAppDomain() == null)
            || !this.useAppDomain.equals(thatObj.isUseAppDomain()) ) {
            return false;
        }
        if( (this.enabled == null && thatObj.isEnabled() != null)
            || (this.enabled != null && thatObj.isEnabled() == null)
            || !this.enabled.equals(thatObj.isEnabled()) ) {
            return false;
        }
        if( (this.licenseMode == null && thatObj.getLicenseMode() != null)
            || (this.licenseMode != null && thatObj.getLicenseMode() == null)
            || !this.licenseMode.equals(thatObj.getLicenseMode()) ) {
            return false;
        }
        if( (this.status == null && thatObj.getStatus() != null)
            || (this.status != null && thatObj.getStatus() == null)
            || !this.status.equals(thatObj.getStatus()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = owner == null ? 0 : owner.hashCode();
        _hash = 31 * _hash + delta;
        delta = admin == null ? 0 : admin.hashCode();
        _hash = 31 * _hash + delta;
        delta = name == null ? 0 : name.hashCode();
        _hash = 31 * _hash + delta;
        delta = clientId == null ? 0 : clientId.hashCode();
        _hash = 31 * _hash + delta;
        delta = clientCode == null ? 0 : clientCode.hashCode();
        _hash = 31 * _hash + delta;
        delta = rootDomain == null ? 0 : rootDomain.hashCode();
        _hash = 31 * _hash + delta;
        delta = appDomain == null ? 0 : appDomain.hashCode();
        _hash = 31 * _hash + delta;
        delta = useAppDomain == null ? 0 : useAppDomain.hashCode();
        _hash = 31 * _hash + delta;
        delta = enabled == null ? 0 : enabled.hashCode();
        _hash = 31 * _hash + delta;
        delta = licenseMode == null ? 0 : licenseMode.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
