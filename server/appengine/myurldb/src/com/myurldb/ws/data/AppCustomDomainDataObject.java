package com.myurldb.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.myurldb.ws.AppCustomDomain;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;


@PersistenceCapable(detachable="true")
public class AppCustomDomainDataObject extends KeyedDataObject implements AppCustomDomain
{
    private static final Logger log = Logger.getLogger(AppCustomDomainDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(AppCustomDomainDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(AppCustomDomainDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String guid;

    @Persistent(defaultFetchGroup = "true")
    private String appId;

    @Persistent(defaultFetchGroup = "true")
    private String siteDomain;

    @Persistent(defaultFetchGroup = "true")
    private String domain;

    @Persistent(defaultFetchGroup = "true")
    private Boolean verified;

    @Persistent(defaultFetchGroup = "true")
    private String status;

    @Persistent(defaultFetchGroup = "true")
    private Long verifiedTime;

    public AppCustomDomainDataObject()
    {
        this(null);
    }
    public AppCustomDomainDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null);
    }
    public AppCustomDomainDataObject(String guid, String appId, String siteDomain, String domain, Boolean verified, String status, Long verifiedTime)
    {
        this(guid, appId, siteDomain, domain, verified, status, verifiedTime, null, null);
    }
    public AppCustomDomainDataObject(String guid, String appId, String siteDomain, String domain, Boolean verified, String status, Long verifiedTime, Long createdTime, Long modifiedTime)
    {
        super(createdTime, modifiedTime);
        generatePK(guid);
        this.appId = appId;
        this.siteDomain = siteDomain;
        this.domain = domain;
        this.verified = verified;
        this.status = status;
        this.verifiedTime = verifiedTime;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        generatePK(guid);
    }
    protected void generatePK(String guid)
    {
        if(guid == null) {
            this.guid = GUID.generate();  // temporary
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to a new guid = " + this.guid);
        } else {
            this.guid = guid;
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to guid = " + this.guid);
        }
        rebuildKey();        
    }

//    @Override
//    protected Key createKey()
//    {
//        return AppCustomDomainDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return AppCustomDomainDataObject.composeKey(getGuid());
    }

    public String getAppId()
    {
        return this.appId;
    }
    public void setAppId(String appId)
    {
        this.appId = appId;
    }

    public String getSiteDomain()
    {
        return this.siteDomain;
    }
    public void setSiteDomain(String siteDomain)
    {
        this.siteDomain = siteDomain;
    }

    public String getDomain()
    {
        return this.domain;
    }
    public void setDomain(String domain)
    {
        this.domain = domain;
    }

    public Boolean isVerified()
    {
        return this.verified;
    }
    public void setVerified(Boolean verified)
    {
        this.verified = verified;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public Long getVerifiedTime()
    {
        return this.verifiedTime;
    }
    public void setVerifiedTime(Long verifiedTime)
    {
        this.verifiedTime = verifiedTime;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("guid", this.guid);
        dataMap.put("appId", this.appId);
        dataMap.put("siteDomain", this.siteDomain);
        dataMap.put("domain", this.domain);
        dataMap.put("verified", this.verified);
        dataMap.put("status", this.status);
        dataMap.put("verifiedTime", this.verifiedTime);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        AppCustomDomain thatObj = (AppCustomDomain) obj;
        if( (this.guid == null && thatObj.getGuid() != null)
            || (this.guid != null && thatObj.getGuid() == null)
            || !this.guid.equals(thatObj.getGuid()) ) {
            return false;
        }
        if( (this.appId == null && thatObj.getAppId() != null)
            || (this.appId != null && thatObj.getAppId() == null)
            || !this.appId.equals(thatObj.getAppId()) ) {
            return false;
        }
        if( (this.siteDomain == null && thatObj.getSiteDomain() != null)
            || (this.siteDomain != null && thatObj.getSiteDomain() == null)
            || !this.siteDomain.equals(thatObj.getSiteDomain()) ) {
            return false;
        }
        if( (this.domain == null && thatObj.getDomain() != null)
            || (this.domain != null && thatObj.getDomain() == null)
            || !this.domain.equals(thatObj.getDomain()) ) {
            return false;
        }
        if( (this.verified == null && thatObj.isVerified() != null)
            || (this.verified != null && thatObj.isVerified() == null)
            || !this.verified.equals(thatObj.isVerified()) ) {
            return false;
        }
        if( (this.status == null && thatObj.getStatus() != null)
            || (this.status != null && thatObj.getStatus() == null)
            || !this.status.equals(thatObj.getStatus()) ) {
            return false;
        }
        if( (this.verifiedTime == null && thatObj.getVerifiedTime() != null)
            || (this.verifiedTime != null && thatObj.getVerifiedTime() == null)
            || !this.verifiedTime.equals(thatObj.getVerifiedTime()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = appId == null ? 0 : appId.hashCode();
        _hash = 31 * _hash + delta;
        delta = siteDomain == null ? 0 : siteDomain.hashCode();
        _hash = 31 * _hash + delta;
        delta = domain == null ? 0 : domain.hashCode();
        _hash = 31 * _hash + delta;
        delta = verified == null ? 0 : verified.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = verifiedTime == null ? 0 : verifiedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
