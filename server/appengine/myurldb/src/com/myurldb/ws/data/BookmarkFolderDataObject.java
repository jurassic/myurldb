package com.myurldb.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.myurldb.ws.BookmarkFolder;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;


@PersistenceCapable(detachable="true")
@Inheritance(customStrategy = "complete-table")
public class BookmarkFolderDataObject extends FolderBaseDataObject implements BookmarkFolder
{
    private static final Logger log = Logger.getLogger(BookmarkFolderDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(BookmarkFolderDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(BookmarkFolderDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String keywordFolder;

    public BookmarkFolderDataObject()
    {
        this(null);
    }
    public BookmarkFolderDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BookmarkFolderDataObject(String guid, String appClient, String clientRootDomain, String user, String title, String description, String type, String category, String parent, String aggregate, String acl, Boolean exportable, String status, String note, String keywordFolder)
    {
        this(guid, appClient, clientRootDomain, user, title, description, type, category, parent, aggregate, acl, exportable, status, note, keywordFolder, null, null);
    }
    public BookmarkFolderDataObject(String guid, String appClient, String clientRootDomain, String user, String title, String description, String type, String category, String parent, String aggregate, String acl, Boolean exportable, String status, String note, String keywordFolder, Long createdTime, Long modifiedTime)
    {
        super(guid, appClient, clientRootDomain, user, title, description, type, category, parent, aggregate, acl, exportable, status, note, createdTime, modifiedTime);
        this.keywordFolder = keywordFolder;
    }

//    @Override
//    protected Key createKey()
//    {
//        return BookmarkFolderDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return BookmarkFolderDataObject.composeKey(getGuid());
    }

    public String getKeywordFolder()
    {
        return this.keywordFolder;
    }
    public void setKeywordFolder(String keywordFolder)
    {
        this.keywordFolder = keywordFolder;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("keywordFolder", this.keywordFolder);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        BookmarkFolder thatObj = (BookmarkFolder) obj;
        if( (this.keywordFolder == null && thatObj.getKeywordFolder() != null)
            || (this.keywordFolder != null && thatObj.getKeywordFolder() == null)
            || !this.keywordFolder.equals(thatObj.getKeywordFolder()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = keywordFolder == null ? 0 : keywordFolder.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
