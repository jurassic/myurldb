package com.myurldb.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.myurldb.ws.BookmarkFolderImport;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;


@PersistenceCapable(detachable="true")
@Inheritance(customStrategy = "complete-table")
public class BookmarkFolderImportDataObject extends FolderImportBaseDataObject implements BookmarkFolderImport
{
    private static final Logger log = Logger.getLogger(BookmarkFolderImportDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(BookmarkFolderImportDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(BookmarkFolderImportDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String bookmarkFolder;

    public BookmarkFolderImportDataObject()
    {
        this(null);
    }
    public BookmarkFolderImportDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BookmarkFolderImportDataObject(String guid, String user, Integer precedence, String importType, String importedFolder, String importedFolderUser, String importedFolderTitle, String importedFolderPath, String status, String note, String bookmarkFolder)
    {
        this(guid, user, precedence, importType, importedFolder, importedFolderUser, importedFolderTitle, importedFolderPath, status, note, bookmarkFolder, null, null);
    }
    public BookmarkFolderImportDataObject(String guid, String user, Integer precedence, String importType, String importedFolder, String importedFolderUser, String importedFolderTitle, String importedFolderPath, String status, String note, String bookmarkFolder, Long createdTime, Long modifiedTime)
    {
        super(guid, user, precedence, importType, importedFolder, importedFolderUser, importedFolderTitle, importedFolderPath, status, note, createdTime, modifiedTime);
        this.bookmarkFolder = bookmarkFolder;
    }

//    @Override
//    protected Key createKey()
//    {
//        return BookmarkFolderImportDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return BookmarkFolderImportDataObject.composeKey(getGuid());
    }

    public String getBookmarkFolder()
    {
        return this.bookmarkFolder;
    }
    public void setBookmarkFolder(String bookmarkFolder)
    {
        this.bookmarkFolder = bookmarkFolder;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("bookmarkFolder", this.bookmarkFolder);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        BookmarkFolderImport thatObj = (BookmarkFolderImport) obj;
        if( (this.bookmarkFolder == null && thatObj.getBookmarkFolder() != null)
            || (this.bookmarkFolder != null && thatObj.getBookmarkFolder() == null)
            || !this.bookmarkFolder.equals(thatObj.getBookmarkFolder()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = bookmarkFolder == null ? 0 : bookmarkFolder.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
