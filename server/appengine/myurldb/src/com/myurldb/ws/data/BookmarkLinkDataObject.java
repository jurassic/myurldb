package com.myurldb.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.myurldb.ws.BookmarkLink;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;


@PersistenceCapable(detachable="true")
@Inheritance(customStrategy = "complete-table")
public class BookmarkLinkDataObject extends NavLinkBaseDataObject implements BookmarkLink
{
    private static final Logger log = Logger.getLogger(BookmarkLinkDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(BookmarkLinkDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(BookmarkLinkDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String bookmarkFolder;

    @Persistent(defaultFetchGroup = "true")
    private String contentTag;

    @Persistent(defaultFetchGroup = "true")
    private String referenceElement;

    @Persistent(defaultFetchGroup = "true")
    private String elementType;

    @Persistent(defaultFetchGroup = "true")
    private String keywordLink;

    public BookmarkLinkDataObject()
    {
        this(null);
    }
    public BookmarkLinkDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BookmarkLinkDataObject(String guid, String appClient, String clientRootDomain, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, Boolean internal, Boolean caching, String memo, String status, String note, Long expirationTime, String bookmarkFolder, String contentTag, String referenceElement, String elementType, String keywordLink)
    {
        this(guid, appClient, clientRootDomain, user, shortLink, domain, token, longUrl, shortUrl, internal, caching, memo, status, note, expirationTime, bookmarkFolder, contentTag, referenceElement, elementType, keywordLink, null, null);
    }
    public BookmarkLinkDataObject(String guid, String appClient, String clientRootDomain, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, Boolean internal, Boolean caching, String memo, String status, String note, Long expirationTime, String bookmarkFolder, String contentTag, String referenceElement, String elementType, String keywordLink, Long createdTime, Long modifiedTime)
    {
        super(guid, appClient, clientRootDomain, user, shortLink, domain, token, longUrl, shortUrl, internal, caching, memo, status, note, expirationTime, createdTime, modifiedTime);
        this.bookmarkFolder = bookmarkFolder;
        this.contentTag = contentTag;
        this.referenceElement = referenceElement;
        this.elementType = elementType;
        this.keywordLink = keywordLink;
    }

//    @Override
//    protected Key createKey()
//    {
//        return BookmarkLinkDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return BookmarkLinkDataObject.composeKey(getGuid());
    }

    public String getBookmarkFolder()
    {
        return this.bookmarkFolder;
    }
    public void setBookmarkFolder(String bookmarkFolder)
    {
        this.bookmarkFolder = bookmarkFolder;
    }

    public String getContentTag()
    {
        return this.contentTag;
    }
    public void setContentTag(String contentTag)
    {
        this.contentTag = contentTag;
    }

    public String getReferenceElement()
    {
        return this.referenceElement;
    }
    public void setReferenceElement(String referenceElement)
    {
        this.referenceElement = referenceElement;
    }

    public String getElementType()
    {
        return this.elementType;
    }
    public void setElementType(String elementType)
    {
        this.elementType = elementType;
    }

    public String getKeywordLink()
    {
        return this.keywordLink;
    }
    public void setKeywordLink(String keywordLink)
    {
        this.keywordLink = keywordLink;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("bookmarkFolder", this.bookmarkFolder);
        dataMap.put("contentTag", this.contentTag);
        dataMap.put("referenceElement", this.referenceElement);
        dataMap.put("elementType", this.elementType);
        dataMap.put("keywordLink", this.keywordLink);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        BookmarkLink thatObj = (BookmarkLink) obj;
        if( (this.bookmarkFolder == null && thatObj.getBookmarkFolder() != null)
            || (this.bookmarkFolder != null && thatObj.getBookmarkFolder() == null)
            || !this.bookmarkFolder.equals(thatObj.getBookmarkFolder()) ) {
            return false;
        }
        if( (this.contentTag == null && thatObj.getContentTag() != null)
            || (this.contentTag != null && thatObj.getContentTag() == null)
            || !this.contentTag.equals(thatObj.getContentTag()) ) {
            return false;
        }
        if( (this.referenceElement == null && thatObj.getReferenceElement() != null)
            || (this.referenceElement != null && thatObj.getReferenceElement() == null)
            || !this.referenceElement.equals(thatObj.getReferenceElement()) ) {
            return false;
        }
        if( (this.elementType == null && thatObj.getElementType() != null)
            || (this.elementType != null && thatObj.getElementType() == null)
            || !this.elementType.equals(thatObj.getElementType()) ) {
            return false;
        }
        if( (this.keywordLink == null && thatObj.getKeywordLink() != null)
            || (this.keywordLink != null && thatObj.getKeywordLink() == null)
            || !this.keywordLink.equals(thatObj.getKeywordLink()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = bookmarkFolder == null ? 0 : bookmarkFolder.hashCode();
        _hash = 31 * _hash + delta;
        delta = contentTag == null ? 0 : contentTag.hashCode();
        _hash = 31 * _hash + delta;
        delta = referenceElement == null ? 0 : referenceElement.hashCode();
        _hash = 31 * _hash + delta;
        delta = elementType == null ? 0 : elementType.hashCode();
        _hash = 31 * _hash + delta;
        delta = keywordLink == null ? 0 : keywordLink.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
