package com.myurldb.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.Extension;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PrimaryKey;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.myurldb.ws.CellLatitudeLongitude;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;


@PersistenceCapable(detachable="true")
// @EmbeddedOnly
public class CellLatitudeLongitudeDataObject implements CellLatitudeLongitude, Serializable
{
    private static final Logger log = Logger.getLogger(CellLatitudeLongitudeDataObject.class.getName());

    @PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    private Long _celllatitudelongitude_auto_id;         // Note: Object with long PK cannot be a parent...

    @Persistent(defaultFetchGroup = "true")
    private Integer scale;

    @Persistent(defaultFetchGroup = "true")
    private Integer latitude;

    @Persistent(defaultFetchGroup = "true")
    private Integer longitude;

    public CellLatitudeLongitudeDataObject()
    {
        // ???
        // this(null, null, null);
    }
    public CellLatitudeLongitudeDataObject(Integer scale, Integer latitude, Integer longitude)
    {
        setScale(scale);
        setLatitude(latitude);
        setLongitude(longitude);
    }

    private void resetEncodedKey()
    {
    }

    public Integer getScale()
    {
        return this.scale;
    }
    public void setScale(Integer scale)
    {
        this.scale = scale;
        if(this.scale != null) {
            resetEncodedKey();
        }
    }

    public Integer getLatitude()
    {
        return this.latitude;
    }
    public void setLatitude(Integer latitude)
    {
        this.latitude = latitude;
        if(this.latitude != null) {
            resetEncodedKey();
        }
    }

    public Integer getLongitude()
    {
        return this.longitude;
    }
    public void setLongitude(Integer longitude)
    {
        this.longitude = longitude;
        if(this.longitude != null) {
            resetEncodedKey();
        }
    }


    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getScale() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLatitude() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLongitude() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("scale", this.scale);
        dataMap.put("latitude", this.latitude);
        dataMap.put("longitude", this.longitude);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        CellLatitudeLongitude thatObj = (CellLatitudeLongitude) obj;
        if( (this.scale == null && thatObj.getScale() != null)
            || (this.scale != null && thatObj.getScale() == null)
            || !this.scale.equals(thatObj.getScale()) ) {
            return false;
        }
        if( (this.latitude == null && thatObj.getLatitude() != null)
            || (this.latitude != null && thatObj.getLatitude() == null)
            || !this.latitude.equals(thatObj.getLatitude()) ) {
            return false;
        }
        if( (this.longitude == null && thatObj.getLongitude() != null)
            || (this.longitude != null && thatObj.getLongitude() == null)
            || !this.longitude.equals(thatObj.getLongitude()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = scale == null ? 0 : scale.hashCode();
        _hash = 31 * _hash + delta;
        delta = latitude == null ? 0 : latitude.hashCode();
        _hash = 31 * _hash + delta;
        delta = longitude == null ? 0 : longitude.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public String toString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuffer sb = new StringBuffer();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString(); // ???
            sb.append(k).append(":").append(v).append(";");
        }
        return sb.toString();
    }

    // TBD: Serialization methods?
    // ...

}
