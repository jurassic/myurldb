package com.myurldb.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.myurldb.ws.ClientSetting;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;


@PersistenceCapable(detachable="true")
@Inheritance(customStrategy = "complete-table")
public class ClientSettingDataObject extends PersonalSettingDataObject implements ClientSetting
{
    private static final Logger log = Logger.getLogger(ClientSettingDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(ClientSettingDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(ClientSettingDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String appClient;

    @Persistent(defaultFetchGroup = "true")
    private String admin;

    @Persistent(defaultFetchGroup = "true")
    private Boolean autoRedirectEnabled;

    @Persistent(defaultFetchGroup = "true")
    private Boolean viewEnabled;

    @Persistent(defaultFetchGroup = "true")
    private Boolean shareEnabled;

    @Persistent(defaultFetchGroup = "true")
    private String defaultBaseDomain;

    @Persistent(defaultFetchGroup = "true")
    private String defaultDomainType;

    @Persistent(defaultFetchGroup = "true")
    private String defaultTokenType;

    @Persistent(defaultFetchGroup = "true")
    private String defaultRedirectType;

    @Persistent(defaultFetchGroup = "true")
    private Long defaultFlashDuration;

    @Persistent(defaultFetchGroup = "true")
    private String defaultAccessType;

    @Persistent(defaultFetchGroup = "true")
    private String defaultViewType;

    public ClientSettingDataObject()
    {
        this(null);
    }
    public ClientSettingDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public ClientSettingDataObject(String guid, String appClient, String admin, Boolean autoRedirectEnabled, Boolean viewEnabled, Boolean shareEnabled, String defaultBaseDomain, String defaultDomainType, String defaultTokenType, String defaultRedirectType, Long defaultFlashDuration, String defaultAccessType, String defaultViewType)
    {
        this(guid, appClient, admin, autoRedirectEnabled, viewEnabled, shareEnabled, defaultBaseDomain, defaultDomainType, defaultTokenType, defaultRedirectType, defaultFlashDuration, defaultAccessType, defaultViewType, null, null);
    }
    public ClientSettingDataObject(String guid, String appClient, String admin, Boolean autoRedirectEnabled, Boolean viewEnabled, Boolean shareEnabled, String defaultBaseDomain, String defaultDomainType, String defaultTokenType, String defaultRedirectType, Long defaultFlashDuration, String defaultAccessType, String defaultViewType, Long createdTime, Long modifiedTime)
    {
        super(guid, createdTime, modifiedTime);
        this.appClient = appClient;
        this.admin = admin;
        this.autoRedirectEnabled = autoRedirectEnabled;
        this.viewEnabled = viewEnabled;
        this.shareEnabled = shareEnabled;
        this.defaultBaseDomain = defaultBaseDomain;
        this.defaultDomainType = defaultDomainType;
        this.defaultTokenType = defaultTokenType;
        this.defaultRedirectType = defaultRedirectType;
        this.defaultFlashDuration = defaultFlashDuration;
        this.defaultAccessType = defaultAccessType;
        this.defaultViewType = defaultViewType;
    }

//    @Override
//    protected Key createKey()
//    {
//        return ClientSettingDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return ClientSettingDataObject.composeKey(getGuid());
    }

    public String getAppClient()
    {
        return this.appClient;
    }
    public void setAppClient(String appClient)
    {
        this.appClient = appClient;
    }

    public String getAdmin()
    {
        return this.admin;
    }
    public void setAdmin(String admin)
    {
        this.admin = admin;
    }

    public Boolean isAutoRedirectEnabled()
    {
        return this.autoRedirectEnabled;
    }
    public void setAutoRedirectEnabled(Boolean autoRedirectEnabled)
    {
        this.autoRedirectEnabled = autoRedirectEnabled;
    }

    public Boolean isViewEnabled()
    {
        return this.viewEnabled;
    }
    public void setViewEnabled(Boolean viewEnabled)
    {
        this.viewEnabled = viewEnabled;
    }

    public Boolean isShareEnabled()
    {
        return this.shareEnabled;
    }
    public void setShareEnabled(Boolean shareEnabled)
    {
        this.shareEnabled = shareEnabled;
    }

    public String getDefaultBaseDomain()
    {
        return this.defaultBaseDomain;
    }
    public void setDefaultBaseDomain(String defaultBaseDomain)
    {
        this.defaultBaseDomain = defaultBaseDomain;
    }

    public String getDefaultDomainType()
    {
        return this.defaultDomainType;
    }
    public void setDefaultDomainType(String defaultDomainType)
    {
        this.defaultDomainType = defaultDomainType;
    }

    public String getDefaultTokenType()
    {
        return this.defaultTokenType;
    }
    public void setDefaultTokenType(String defaultTokenType)
    {
        this.defaultTokenType = defaultTokenType;
    }

    public String getDefaultRedirectType()
    {
        return this.defaultRedirectType;
    }
    public void setDefaultRedirectType(String defaultRedirectType)
    {
        this.defaultRedirectType = defaultRedirectType;
    }

    public Long getDefaultFlashDuration()
    {
        return this.defaultFlashDuration;
    }
    public void setDefaultFlashDuration(Long defaultFlashDuration)
    {
        this.defaultFlashDuration = defaultFlashDuration;
    }

    public String getDefaultAccessType()
    {
        return this.defaultAccessType;
    }
    public void setDefaultAccessType(String defaultAccessType)
    {
        this.defaultAccessType = defaultAccessType;
    }

    public String getDefaultViewType()
    {
        return this.defaultViewType;
    }
    public void setDefaultViewType(String defaultViewType)
    {
        this.defaultViewType = defaultViewType;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("appClient", this.appClient);
        dataMap.put("admin", this.admin);
        dataMap.put("autoRedirectEnabled", this.autoRedirectEnabled);
        dataMap.put("viewEnabled", this.viewEnabled);
        dataMap.put("shareEnabled", this.shareEnabled);
        dataMap.put("defaultBaseDomain", this.defaultBaseDomain);
        dataMap.put("defaultDomainType", this.defaultDomainType);
        dataMap.put("defaultTokenType", this.defaultTokenType);
        dataMap.put("defaultRedirectType", this.defaultRedirectType);
        dataMap.put("defaultFlashDuration", this.defaultFlashDuration);
        dataMap.put("defaultAccessType", this.defaultAccessType);
        dataMap.put("defaultViewType", this.defaultViewType);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        ClientSetting thatObj = (ClientSetting) obj;
        if( (this.appClient == null && thatObj.getAppClient() != null)
            || (this.appClient != null && thatObj.getAppClient() == null)
            || !this.appClient.equals(thatObj.getAppClient()) ) {
            return false;
        }
        if( (this.admin == null && thatObj.getAdmin() != null)
            || (this.admin != null && thatObj.getAdmin() == null)
            || !this.admin.equals(thatObj.getAdmin()) ) {
            return false;
        }
        if( (this.autoRedirectEnabled == null && thatObj.isAutoRedirectEnabled() != null)
            || (this.autoRedirectEnabled != null && thatObj.isAutoRedirectEnabled() == null)
            || !this.autoRedirectEnabled.equals(thatObj.isAutoRedirectEnabled()) ) {
            return false;
        }
        if( (this.viewEnabled == null && thatObj.isViewEnabled() != null)
            || (this.viewEnabled != null && thatObj.isViewEnabled() == null)
            || !this.viewEnabled.equals(thatObj.isViewEnabled()) ) {
            return false;
        }
        if( (this.shareEnabled == null && thatObj.isShareEnabled() != null)
            || (this.shareEnabled != null && thatObj.isShareEnabled() == null)
            || !this.shareEnabled.equals(thatObj.isShareEnabled()) ) {
            return false;
        }
        if( (this.defaultBaseDomain == null && thatObj.getDefaultBaseDomain() != null)
            || (this.defaultBaseDomain != null && thatObj.getDefaultBaseDomain() == null)
            || !this.defaultBaseDomain.equals(thatObj.getDefaultBaseDomain()) ) {
            return false;
        }
        if( (this.defaultDomainType == null && thatObj.getDefaultDomainType() != null)
            || (this.defaultDomainType != null && thatObj.getDefaultDomainType() == null)
            || !this.defaultDomainType.equals(thatObj.getDefaultDomainType()) ) {
            return false;
        }
        if( (this.defaultTokenType == null && thatObj.getDefaultTokenType() != null)
            || (this.defaultTokenType != null && thatObj.getDefaultTokenType() == null)
            || !this.defaultTokenType.equals(thatObj.getDefaultTokenType()) ) {
            return false;
        }
        if( (this.defaultRedirectType == null && thatObj.getDefaultRedirectType() != null)
            || (this.defaultRedirectType != null && thatObj.getDefaultRedirectType() == null)
            || !this.defaultRedirectType.equals(thatObj.getDefaultRedirectType()) ) {
            return false;
        }
        if( (this.defaultFlashDuration == null && thatObj.getDefaultFlashDuration() != null)
            || (this.defaultFlashDuration != null && thatObj.getDefaultFlashDuration() == null)
            || !this.defaultFlashDuration.equals(thatObj.getDefaultFlashDuration()) ) {
            return false;
        }
        if( (this.defaultAccessType == null && thatObj.getDefaultAccessType() != null)
            || (this.defaultAccessType != null && thatObj.getDefaultAccessType() == null)
            || !this.defaultAccessType.equals(thatObj.getDefaultAccessType()) ) {
            return false;
        }
        if( (this.defaultViewType == null && thatObj.getDefaultViewType() != null)
            || (this.defaultViewType != null && thatObj.getDefaultViewType() == null)
            || !this.defaultViewType.equals(thatObj.getDefaultViewType()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = appClient == null ? 0 : appClient.hashCode();
        _hash = 31 * _hash + delta;
        delta = admin == null ? 0 : admin.hashCode();
        _hash = 31 * _hash + delta;
        delta = autoRedirectEnabled == null ? 0 : autoRedirectEnabled.hashCode();
        _hash = 31 * _hash + delta;
        delta = viewEnabled == null ? 0 : viewEnabled.hashCode();
        _hash = 31 * _hash + delta;
        delta = shareEnabled == null ? 0 : shareEnabled.hashCode();
        _hash = 31 * _hash + delta;
        delta = defaultBaseDomain == null ? 0 : defaultBaseDomain.hashCode();
        _hash = 31 * _hash + delta;
        delta = defaultDomainType == null ? 0 : defaultDomainType.hashCode();
        _hash = 31 * _hash + delta;
        delta = defaultTokenType == null ? 0 : defaultTokenType.hashCode();
        _hash = 31 * _hash + delta;
        delta = defaultRedirectType == null ? 0 : defaultRedirectType.hashCode();
        _hash = 31 * _hash + delta;
        delta = defaultFlashDuration == null ? 0 : defaultFlashDuration.hashCode();
        _hash = 31 * _hash + delta;
        delta = defaultAccessType == null ? 0 : defaultAccessType.hashCode();
        _hash = 31 * _hash + delta;
        delta = defaultViewType == null ? 0 : defaultViewType.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
