package com.myurldb.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.myurldb.ws.ClientUser;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;


@PersistenceCapable(detachable="true")
public class ClientUserDataObject extends KeyedDataObject implements ClientUser
{
    private static final Logger log = Logger.getLogger(ClientUserDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(ClientUserDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(ClientUserDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String guid;

    @Persistent(defaultFetchGroup = "true")
    private String appClient;

    @Persistent(defaultFetchGroup = "true")
    private String user;

    @Persistent(defaultFetchGroup = "true")
    private String role;

    @Persistent(defaultFetchGroup = "true")
    private Boolean provisioned;

    @Persistent(defaultFetchGroup = "true")
    private Boolean licensed;

    @Persistent(defaultFetchGroup = "true")
    private String status;

    public ClientUserDataObject()
    {
        this(null);
    }
    public ClientUserDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null);
    }
    public ClientUserDataObject(String guid, String appClient, String user, String role, Boolean provisioned, Boolean licensed, String status)
    {
        this(guid, appClient, user, role, provisioned, licensed, status, null, null);
    }
    public ClientUserDataObject(String guid, String appClient, String user, String role, Boolean provisioned, Boolean licensed, String status, Long createdTime, Long modifiedTime)
    {
        super(createdTime, modifiedTime);
        generatePK(guid);
        this.appClient = appClient;
        this.user = user;
        this.role = role;
        this.provisioned = provisioned;
        this.licensed = licensed;
        this.status = status;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        generatePK(guid);
    }
    protected void generatePK(String guid)
    {
        if(guid == null) {
            this.guid = GUID.generate();  // temporary
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to a new guid = " + this.guid);
        } else {
            this.guid = guid;
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to guid = " + this.guid);
        }
        rebuildKey();        
    }

//    @Override
//    protected Key createKey()
//    {
//        return ClientUserDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return ClientUserDataObject.composeKey(getGuid());
    }

    public String getAppClient()
    {
        return this.appClient;
    }
    public void setAppClient(String appClient)
    {
        this.appClient = appClient;
    }

    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    public String getRole()
    {
        return this.role;
    }
    public void setRole(String role)
    {
        this.role = role;
    }

    public Boolean isProvisioned()
    {
        return this.provisioned;
    }
    public void setProvisioned(Boolean provisioned)
    {
        this.provisioned = provisioned;
    }

    public Boolean isLicensed()
    {
        return this.licensed;
    }
    public void setLicensed(Boolean licensed)
    {
        this.licensed = licensed;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("guid", this.guid);
        dataMap.put("appClient", this.appClient);
        dataMap.put("user", this.user);
        dataMap.put("role", this.role);
        dataMap.put("provisioned", this.provisioned);
        dataMap.put("licensed", this.licensed);
        dataMap.put("status", this.status);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        ClientUser thatObj = (ClientUser) obj;
        if( (this.guid == null && thatObj.getGuid() != null)
            || (this.guid != null && thatObj.getGuid() == null)
            || !this.guid.equals(thatObj.getGuid()) ) {
            return false;
        }
        if( (this.appClient == null && thatObj.getAppClient() != null)
            || (this.appClient != null && thatObj.getAppClient() == null)
            || !this.appClient.equals(thatObj.getAppClient()) ) {
            return false;
        }
        if( (this.user == null && thatObj.getUser() != null)
            || (this.user != null && thatObj.getUser() == null)
            || !this.user.equals(thatObj.getUser()) ) {
            return false;
        }
        if( (this.role == null && thatObj.getRole() != null)
            || (this.role != null && thatObj.getRole() == null)
            || !this.role.equals(thatObj.getRole()) ) {
            return false;
        }
        if( (this.provisioned == null && thatObj.isProvisioned() != null)
            || (this.provisioned != null && thatObj.isProvisioned() == null)
            || !this.provisioned.equals(thatObj.isProvisioned()) ) {
            return false;
        }
        if( (this.licensed == null && thatObj.isLicensed() != null)
            || (this.licensed != null && thatObj.isLicensed() == null)
            || !this.licensed.equals(thatObj.isLicensed()) ) {
            return false;
        }
        if( (this.status == null && thatObj.getStatus() != null)
            || (this.status != null && thatObj.getStatus() == null)
            || !this.status.equals(thatObj.getStatus()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = appClient == null ? 0 : appClient.hashCode();
        _hash = 31 * _hash + delta;
        delta = user == null ? 0 : user.hashCode();
        _hash = 31 * _hash + delta;
        delta = role == null ? 0 : role.hashCode();
        _hash = 31 * _hash + delta;
        delta = provisioned == null ? 0 : provisioned.hashCode();
        _hash = 31 * _hash + delta;
        delta = licensed == null ? 0 : licensed.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
