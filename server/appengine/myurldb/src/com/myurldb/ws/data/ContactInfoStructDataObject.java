package com.myurldb.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.Extension;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PrimaryKey;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.myurldb.ws.ContactInfoStruct;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;


@PersistenceCapable(detachable="true")
// @EmbeddedOnly
public class ContactInfoStructDataObject implements ContactInfoStruct, Serializable
{
    private static final Logger log = Logger.getLogger(ContactInfoStructDataObject.class.getName());

    @PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    @Extension(vendorName="datanucleus", key="gae.encoded-pk", value="true")
    private String _contactinfostruct_encoded_key = null;   // Due to GAE limitation, the PK needs to be either Key or encoded string...

    @Persistent(defaultFetchGroup = "true")
    // @Extension(vendorName="datanucleus", key="gae.pk-name", value="true")
    private String uuid;

    @Persistent(defaultFetchGroup = "true")
    private String streetAddress;

    @Persistent(defaultFetchGroup = "true")
    private String locality;

    @Persistent(defaultFetchGroup = "true")
    private String region;

    @Persistent(defaultFetchGroup = "true")
    private String postalCode;

    @Persistent(defaultFetchGroup = "true")
    private String countryName;

    @Persistent(defaultFetchGroup = "true")
    private String emailAddress;

    @Persistent(defaultFetchGroup = "true")
    private String phoneNumber;

    @Persistent(defaultFetchGroup = "true")
    private String faxNumber;

    @Persistent(defaultFetchGroup = "true")
    private String website;

    @Persistent(defaultFetchGroup = "true")
    private String note;

    public ContactInfoStructDataObject()
    {
        // ???
        // this(null, null, null, null, null, null, null, null, null, null, null);
    }
    public ContactInfoStructDataObject(String uuid, String streetAddress, String locality, String region, String postalCode, String countryName, String emailAddress, String phoneNumber, String faxNumber, String website, String note)
    {
        setUuid(uuid);
        setStreetAddress(streetAddress);
        setLocality(locality);
        setRegion(region);
        setPostalCode(postalCode);
        setCountryName(countryName);
        setEmailAddress(emailAddress);
        setPhoneNumber(phoneNumber);
        setFaxNumber(faxNumber);
        setWebsite(website);
        setNote(note);
    }

    private void resetEncodedKey()
    {
        if(_contactinfostruct_encoded_key == null) {
            if(uuid == null || uuid.isEmpty()) {
                // Temporary solution. PK cannot be null.
                uuid = GUID.generate();
            }
            _contactinfostruct_encoded_key = KeyFactory.createKeyString(ContactInfoStructDataObject.class.getSimpleName(), uuid);
        }
    }

    public String getUuid()
    {
        return this.uuid;
    }
    public void setUuid(String uuid)
    {
        if(uuid == null || uuid.isEmpty()) {
            // Temporary solution. PK cannot be null. ???
            // this.uuid = GUID.generate();
            this.uuid = uuid;
            _contactinfostruct_encoded_key = null;   // ???
        } else {
            this.uuid = uuid;
            _contactinfostruct_encoded_key = KeyFactory.createKeyString(ContactInfoStructDataObject.class.getSimpleName(), this.uuid);
        }
    }

    public String getStreetAddress()
    {
        return this.streetAddress;
    }
    public void setStreetAddress(String streetAddress)
    {
        this.streetAddress = streetAddress;
        if(this.streetAddress != null) {
            resetEncodedKey();
        }
    }

    public String getLocality()
    {
        return this.locality;
    }
    public void setLocality(String locality)
    {
        this.locality = locality;
        if(this.locality != null) {
            resetEncodedKey();
        }
    }

    public String getRegion()
    {
        return this.region;
    }
    public void setRegion(String region)
    {
        this.region = region;
        if(this.region != null) {
            resetEncodedKey();
        }
    }

    public String getPostalCode()
    {
        return this.postalCode;
    }
    public void setPostalCode(String postalCode)
    {
        this.postalCode = postalCode;
        if(this.postalCode != null) {
            resetEncodedKey();
        }
    }

    public String getCountryName()
    {
        return this.countryName;
    }
    public void setCountryName(String countryName)
    {
        this.countryName = countryName;
        if(this.countryName != null) {
            resetEncodedKey();
        }
    }

    public String getEmailAddress()
    {
        return this.emailAddress;
    }
    public void setEmailAddress(String emailAddress)
    {
        this.emailAddress = emailAddress;
        if(this.emailAddress != null) {
            resetEncodedKey();
        }
    }

    public String getPhoneNumber()
    {
        return this.phoneNumber;
    }
    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
        if(this.phoneNumber != null) {
            resetEncodedKey();
        }
    }

    public String getFaxNumber()
    {
        return this.faxNumber;
    }
    public void setFaxNumber(String faxNumber)
    {
        this.faxNumber = faxNumber;
        if(this.faxNumber != null) {
            resetEncodedKey();
        }
    }

    public String getWebsite()
    {
        return this.website;
    }
    public void setWebsite(String website)
    {
        this.website = website;
        if(this.website != null) {
            resetEncodedKey();
        }
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
        if(this.note != null) {
            resetEncodedKey();
        }
    }


    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getStreetAddress() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLocality() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getRegion() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPostalCode() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getCountryName() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getEmailAddress() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPhoneNumber() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getFaxNumber() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getWebsite() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("uuid", this.uuid);
        dataMap.put("streetAddress", this.streetAddress);
        dataMap.put("locality", this.locality);
        dataMap.put("region", this.region);
        dataMap.put("postalCode", this.postalCode);
        dataMap.put("countryName", this.countryName);
        dataMap.put("emailAddress", this.emailAddress);
        dataMap.put("phoneNumber", this.phoneNumber);
        dataMap.put("faxNumber", this.faxNumber);
        dataMap.put("website", this.website);
        dataMap.put("note", this.note);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        ContactInfoStruct thatObj = (ContactInfoStruct) obj;
        if( (this.uuid == null && thatObj.getUuid() != null)
            || (this.uuid != null && thatObj.getUuid() == null)
            || !this.uuid.equals(thatObj.getUuid()) ) {
            return false;
        }
        if( (this.streetAddress == null && thatObj.getStreetAddress() != null)
            || (this.streetAddress != null && thatObj.getStreetAddress() == null)
            || !this.streetAddress.equals(thatObj.getStreetAddress()) ) {
            return false;
        }
        if( (this.locality == null && thatObj.getLocality() != null)
            || (this.locality != null && thatObj.getLocality() == null)
            || !this.locality.equals(thatObj.getLocality()) ) {
            return false;
        }
        if( (this.region == null && thatObj.getRegion() != null)
            || (this.region != null && thatObj.getRegion() == null)
            || !this.region.equals(thatObj.getRegion()) ) {
            return false;
        }
        if( (this.postalCode == null && thatObj.getPostalCode() != null)
            || (this.postalCode != null && thatObj.getPostalCode() == null)
            || !this.postalCode.equals(thatObj.getPostalCode()) ) {
            return false;
        }
        if( (this.countryName == null && thatObj.getCountryName() != null)
            || (this.countryName != null && thatObj.getCountryName() == null)
            || !this.countryName.equals(thatObj.getCountryName()) ) {
            return false;
        }
        if( (this.emailAddress == null && thatObj.getEmailAddress() != null)
            || (this.emailAddress != null && thatObj.getEmailAddress() == null)
            || !this.emailAddress.equals(thatObj.getEmailAddress()) ) {
            return false;
        }
        if( (this.phoneNumber == null && thatObj.getPhoneNumber() != null)
            || (this.phoneNumber != null && thatObj.getPhoneNumber() == null)
            || !this.phoneNumber.equals(thatObj.getPhoneNumber()) ) {
            return false;
        }
        if( (this.faxNumber == null && thatObj.getFaxNumber() != null)
            || (this.faxNumber != null && thatObj.getFaxNumber() == null)
            || !this.faxNumber.equals(thatObj.getFaxNumber()) ) {
            return false;
        }
        if( (this.website == null && thatObj.getWebsite() != null)
            || (this.website != null && thatObj.getWebsite() == null)
            || !this.website.equals(thatObj.getWebsite()) ) {
            return false;
        }
        if( (this.note == null && thatObj.getNote() != null)
            || (this.note != null && thatObj.getNote() == null)
            || !this.note.equals(thatObj.getNote()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = uuid == null ? 0 : uuid.hashCode();
        _hash = 31 * _hash + delta;
        delta = streetAddress == null ? 0 : streetAddress.hashCode();
        _hash = 31 * _hash + delta;
        delta = locality == null ? 0 : locality.hashCode();
        _hash = 31 * _hash + delta;
        delta = region == null ? 0 : region.hashCode();
        _hash = 31 * _hash + delta;
        delta = postalCode == null ? 0 : postalCode.hashCode();
        _hash = 31 * _hash + delta;
        delta = countryName == null ? 0 : countryName.hashCode();
        _hash = 31 * _hash + delta;
        delta = emailAddress == null ? 0 : emailAddress.hashCode();
        _hash = 31 * _hash + delta;
        delta = phoneNumber == null ? 0 : phoneNumber.hashCode();
        _hash = 31 * _hash + delta;
        delta = faxNumber == null ? 0 : faxNumber.hashCode();
        _hash = 31 * _hash + delta;
        delta = website == null ? 0 : website.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public String toString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuffer sb = new StringBuffer();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString(); // ???
            sb.append(k).append(":").append(v).append(";");
        }
        return sb.toString();
    }

    // TBD: Serialization methods?
    // ...

}
