package com.myurldb.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.myurldb.ws.DomainInfo;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;


@PersistenceCapable(detachable="true")
public class DomainInfoDataObject extends KeyedDataObject implements DomainInfo
{
    private static final Logger log = Logger.getLogger(DomainInfoDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(DomainInfoDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(DomainInfoDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String guid;

    @Persistent(defaultFetchGroup = "true")
    private String domain;

    @Persistent(defaultFetchGroup = "true")
    private Boolean banned;

    @Persistent(defaultFetchGroup = "true")
    private Boolean urlShortener;

    @Persistent(defaultFetchGroup = "true")
    private String category;

    @Persistent(defaultFetchGroup = "true")
    private String reputation;

    @Persistent(defaultFetchGroup = "true")
    private String authority;

    @Persistent(defaultFetchGroup = "true")
    private String note;

    @Persistent(defaultFetchGroup = "true")
    private Long verifiedTime;

    public DomainInfoDataObject()
    {
        this(null);
    }
    public DomainInfoDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null);
    }
    public DomainInfoDataObject(String guid, String domain, Boolean banned, Boolean urlShortener, String category, String reputation, String authority, String note, Long verifiedTime)
    {
        this(guid, domain, banned, urlShortener, category, reputation, authority, note, verifiedTime, null, null);
    }
    public DomainInfoDataObject(String guid, String domain, Boolean banned, Boolean urlShortener, String category, String reputation, String authority, String note, Long verifiedTime, Long createdTime, Long modifiedTime)
    {
        super(createdTime, modifiedTime);
        generatePK(guid);
        this.domain = domain;
        this.banned = banned;
        this.urlShortener = urlShortener;
        this.category = category;
        this.reputation = reputation;
        this.authority = authority;
        this.note = note;
        this.verifiedTime = verifiedTime;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        generatePK(guid);
    }
    protected void generatePK(String guid)
    {
        if(guid == null) {
            this.guid = GUID.generate();  // temporary
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to a new guid = " + this.guid);
        } else {
            this.guid = guid;
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to guid = " + this.guid);
        }
        rebuildKey();        
    }

//    @Override
//    protected Key createKey()
//    {
//        return DomainInfoDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return DomainInfoDataObject.composeKey(getGuid());
    }

    public String getDomain()
    {
        return this.domain;
    }
    public void setDomain(String domain)
    {
        this.domain = domain;
    }

    public Boolean isBanned()
    {
        return this.banned;
    }
    public void setBanned(Boolean banned)
    {
        this.banned = banned;
    }

    public Boolean isUrlShortener()
    {
        return this.urlShortener;
    }
    public void setUrlShortener(Boolean urlShortener)
    {
        this.urlShortener = urlShortener;
    }

    public String getCategory()
    {
        return this.category;
    }
    public void setCategory(String category)
    {
        this.category = category;
    }

    public String getReputation()
    {
        return this.reputation;
    }
    public void setReputation(String reputation)
    {
        this.reputation = reputation;
    }

    public String getAuthority()
    {
        return this.authority;
    }
    public void setAuthority(String authority)
    {
        this.authority = authority;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    public Long getVerifiedTime()
    {
        return this.verifiedTime;
    }
    public void setVerifiedTime(Long verifiedTime)
    {
        this.verifiedTime = verifiedTime;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("guid", this.guid);
        dataMap.put("domain", this.domain);
        dataMap.put("banned", this.banned);
        dataMap.put("urlShortener", this.urlShortener);
        dataMap.put("category", this.category);
        dataMap.put("reputation", this.reputation);
        dataMap.put("authority", this.authority);
        dataMap.put("note", this.note);
        dataMap.put("verifiedTime", this.verifiedTime);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        DomainInfo thatObj = (DomainInfo) obj;
        if( (this.guid == null && thatObj.getGuid() != null)
            || (this.guid != null && thatObj.getGuid() == null)
            || !this.guid.equals(thatObj.getGuid()) ) {
            return false;
        }
        if( (this.domain == null && thatObj.getDomain() != null)
            || (this.domain != null && thatObj.getDomain() == null)
            || !this.domain.equals(thatObj.getDomain()) ) {
            return false;
        }
        if( (this.banned == null && thatObj.isBanned() != null)
            || (this.banned != null && thatObj.isBanned() == null)
            || !this.banned.equals(thatObj.isBanned()) ) {
            return false;
        }
        if( (this.urlShortener == null && thatObj.isUrlShortener() != null)
            || (this.urlShortener != null && thatObj.isUrlShortener() == null)
            || !this.urlShortener.equals(thatObj.isUrlShortener()) ) {
            return false;
        }
        if( (this.category == null && thatObj.getCategory() != null)
            || (this.category != null && thatObj.getCategory() == null)
            || !this.category.equals(thatObj.getCategory()) ) {
            return false;
        }
        if( (this.reputation == null && thatObj.getReputation() != null)
            || (this.reputation != null && thatObj.getReputation() == null)
            || !this.reputation.equals(thatObj.getReputation()) ) {
            return false;
        }
        if( (this.authority == null && thatObj.getAuthority() != null)
            || (this.authority != null && thatObj.getAuthority() == null)
            || !this.authority.equals(thatObj.getAuthority()) ) {
            return false;
        }
        if( (this.note == null && thatObj.getNote() != null)
            || (this.note != null && thatObj.getNote() == null)
            || !this.note.equals(thatObj.getNote()) ) {
            return false;
        }
        if( (this.verifiedTime == null && thatObj.getVerifiedTime() != null)
            || (this.verifiedTime != null && thatObj.getVerifiedTime() == null)
            || !this.verifiedTime.equals(thatObj.getVerifiedTime()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = domain == null ? 0 : domain.hashCode();
        _hash = 31 * _hash + delta;
        delta = banned == null ? 0 : banned.hashCode();
        _hash = 31 * _hash + delta;
        delta = urlShortener == null ? 0 : urlShortener.hashCode();
        _hash = 31 * _hash + delta;
        delta = category == null ? 0 : category.hashCode();
        _hash = 31 * _hash + delta;
        delta = reputation == null ? 0 : reputation.hashCode();
        _hash = 31 * _hash + delta;
        delta = authority == null ? 0 : authority.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        delta = verifiedTime == null ? 0 : verifiedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
