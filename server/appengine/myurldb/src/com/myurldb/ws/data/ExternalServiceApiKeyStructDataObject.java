package com.myurldb.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.Extension;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PrimaryKey;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.myurldb.ws.ExternalServiceApiKeyStruct;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;


@PersistenceCapable(detachable="true")
// @EmbeddedOnly
public class ExternalServiceApiKeyStructDataObject implements ExternalServiceApiKeyStruct, Serializable
{
    private static final Logger log = Logger.getLogger(ExternalServiceApiKeyStructDataObject.class.getName());

    @PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    @Extension(vendorName="datanucleus", key="gae.encoded-pk", value="true")
    private String _externalserviceapikeystruct_encoded_key = null;   // Due to GAE limitation, the PK needs to be either Key or encoded string...

    @Persistent(defaultFetchGroup = "true")
    // @Extension(vendorName="datanucleus", key="gae.pk-name", value="true")
    private String uuid;

    @Persistent(defaultFetchGroup = "true")
    private String service;

    @Persistent(defaultFetchGroup = "true")
    private String key;

    @Persistent(defaultFetchGroup = "true")
    private String secret;

    @Persistent(defaultFetchGroup = "true")
    private String note;

    public ExternalServiceApiKeyStructDataObject()
    {
        // ???
        // this(null, null, null, null, null);
    }
    public ExternalServiceApiKeyStructDataObject(String uuid, String service, String key, String secret, String note)
    {
        setUuid(uuid);
        setService(service);
        setKey(key);
        setSecret(secret);
        setNote(note);
    }

    private void resetEncodedKey()
    {
        if(_externalserviceapikeystruct_encoded_key == null) {
            if(uuid == null || uuid.isEmpty()) {
                // Temporary solution. PK cannot be null.
                uuid = GUID.generate();
            }
            _externalserviceapikeystruct_encoded_key = KeyFactory.createKeyString(ExternalServiceApiKeyStructDataObject.class.getSimpleName(), uuid);
        }
    }

    public String getUuid()
    {
        return this.uuid;
    }
    public void setUuid(String uuid)
    {
        if(uuid == null || uuid.isEmpty()) {
            // Temporary solution. PK cannot be null. ???
            // this.uuid = GUID.generate();
            this.uuid = uuid;
            _externalserviceapikeystruct_encoded_key = null;   // ???
        } else {
            this.uuid = uuid;
            _externalserviceapikeystruct_encoded_key = KeyFactory.createKeyString(ExternalServiceApiKeyStructDataObject.class.getSimpleName(), this.uuid);
        }
    }

    public String getService()
    {
        return this.service;
    }
    public void setService(String service)
    {
        this.service = service;
        if(this.service != null) {
            resetEncodedKey();
        }
    }

    public String getKey()
    {
        return this.key;
    }
    public void setKey(String key)
    {
        this.key = key;
        if(this.key != null) {
            resetEncodedKey();
        }
    }

    public String getSecret()
    {
        return this.secret;
    }
    public void setSecret(String secret)
    {
        this.secret = secret;
        if(this.secret != null) {
            resetEncodedKey();
        }
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
        if(this.note != null) {
            resetEncodedKey();
        }
    }


    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getService() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getKey() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSecret() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("uuid", this.uuid);
        dataMap.put("service", this.service);
        dataMap.put("key", this.key);
        dataMap.put("secret", this.secret);
        dataMap.put("note", this.note);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        ExternalServiceApiKeyStruct thatObj = (ExternalServiceApiKeyStruct) obj;
        if( (this.uuid == null && thatObj.getUuid() != null)
            || (this.uuid != null && thatObj.getUuid() == null)
            || !this.uuid.equals(thatObj.getUuid()) ) {
            return false;
        }
        if( (this.service == null && thatObj.getService() != null)
            || (this.service != null && thatObj.getService() == null)
            || !this.service.equals(thatObj.getService()) ) {
            return false;
        }
        if( (this.key == null && thatObj.getKey() != null)
            || (this.key != null && thatObj.getKey() == null)
            || !this.key.equals(thatObj.getKey()) ) {
            return false;
        }
        if( (this.secret == null && thatObj.getSecret() != null)
            || (this.secret != null && thatObj.getSecret() == null)
            || !this.secret.equals(thatObj.getSecret()) ) {
            return false;
        }
        if( (this.note == null && thatObj.getNote() != null)
            || (this.note != null && thatObj.getNote() == null)
            || !this.note.equals(thatObj.getNote()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = uuid == null ? 0 : uuid.hashCode();
        _hash = 31 * _hash + delta;
        delta = service == null ? 0 : service.hashCode();
        _hash = 31 * _hash + delta;
        delta = key == null ? 0 : key.hashCode();
        _hash = 31 * _hash + delta;
        delta = secret == null ? 0 : secret.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public String toString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuffer sb = new StringBuffer();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString(); // ???
            sb.append(k).append(":").append(v).append(";");
        }
        return sb.toString();
    }

    // TBD: Serialization methods?
    // ...

}
