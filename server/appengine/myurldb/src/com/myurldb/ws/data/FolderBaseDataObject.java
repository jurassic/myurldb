package com.myurldb.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.myurldb.ws.FolderBase;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;


@PersistenceCapable(detachable="true")
@Inheritance(strategy = InheritanceStrategy.SUBCLASS_TABLE)
public abstract class FolderBaseDataObject extends KeyedDataObject implements FolderBase
{
    private static final Logger log = Logger.getLogger(FolderBaseDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(FolderBaseDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(FolderBaseDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String guid;

    @Persistent(defaultFetchGroup = "true")
    private String appClient;

    @Persistent(defaultFetchGroup = "true")
    private String clientRootDomain;

    @Persistent(defaultFetchGroup = "true")
    private String user;

    @Persistent(defaultFetchGroup = "true")
    private String title;

    @Persistent(defaultFetchGroup = "true")
    private String description;

    @Persistent(defaultFetchGroup = "true")
    private String type;

    @Persistent(defaultFetchGroup = "true")
    private String category;

    @Persistent(defaultFetchGroup = "true")
    private String parent;

    @Persistent(defaultFetchGroup = "true")
    private String aggregate;

    @Persistent(defaultFetchGroup = "true")
    private String acl;

    @Persistent(defaultFetchGroup = "true")
    private Boolean exportable;

    @Persistent(defaultFetchGroup = "true")
    private String status;

    @Persistent(defaultFetchGroup = "true")
    private String note;

    public FolderBaseDataObject()
    {
        this(null);
    }
    public FolderBaseDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public FolderBaseDataObject(String guid, String appClient, String clientRootDomain, String user, String title, String description, String type, String category, String parent, String aggregate, String acl, Boolean exportable, String status, String note)
    {
        this(guid, appClient, clientRootDomain, user, title, description, type, category, parent, aggregate, acl, exportable, status, note, null, null);
    }
    public FolderBaseDataObject(String guid, String appClient, String clientRootDomain, String user, String title, String description, String type, String category, String parent, String aggregate, String acl, Boolean exportable, String status, String note, Long createdTime, Long modifiedTime)
    {
        super(createdTime, modifiedTime);
        generatePK(guid);
        this.appClient = appClient;
        this.clientRootDomain = clientRootDomain;
        this.user = user;
        this.title = title;
        this.description = description;
        this.type = type;
        this.category = category;
        this.parent = parent;
        this.aggregate = aggregate;
        this.acl = acl;
        this.exportable = exportable;
        this.status = status;
        this.note = note;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        generatePK(guid);
    }
    protected void generatePK(String guid)
    {
        if(guid == null) {
            this.guid = GUID.generate();  // temporary
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to a new guid = " + this.guid);
        } else {
            this.guid = guid;
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to guid = " + this.guid);
        }
        rebuildKey();        
    }

//    @Override
//    protected Key createKey()
//    {
//        return FolderBaseDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return FolderBaseDataObject.composeKey(getGuid());
    }

    public String getAppClient()
    {
        return this.appClient;
    }
    public void setAppClient(String appClient)
    {
        this.appClient = appClient;
    }

    public String getClientRootDomain()
    {
        return this.clientRootDomain;
    }
    public void setClientRootDomain(String clientRootDomain)
    {
        this.clientRootDomain = clientRootDomain;
    }

    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    public String getTitle()
    {
        return this.title;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getDescription()
    {
        return this.description;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getType()
    {
        return this.type;
    }
    public void setType(String type)
    {
        this.type = type;
    }

    public String getCategory()
    {
        return this.category;
    }
    public void setCategory(String category)
    {
        this.category = category;
    }

    public String getParent()
    {
        return this.parent;
    }
    public void setParent(String parent)
    {
        this.parent = parent;
    }

    public String getAggregate()
    {
        return this.aggregate;
    }
    public void setAggregate(String aggregate)
    {
        this.aggregate = aggregate;
    }

    public String getAcl()
    {
        return this.acl;
    }
    public void setAcl(String acl)
    {
        this.acl = acl;
    }

    public Boolean isExportable()
    {
        return this.exportable;
    }
    public void setExportable(Boolean exportable)
    {
        this.exportable = exportable;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("guid", this.guid);
        dataMap.put("appClient", this.appClient);
        dataMap.put("clientRootDomain", this.clientRootDomain);
        dataMap.put("user", this.user);
        dataMap.put("title", this.title);
        dataMap.put("description", this.description);
        dataMap.put("type", this.type);
        dataMap.put("category", this.category);
        dataMap.put("parent", this.parent);
        dataMap.put("aggregate", this.aggregate);
        dataMap.put("acl", this.acl);
        dataMap.put("exportable", this.exportable);
        dataMap.put("status", this.status);
        dataMap.put("note", this.note);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        FolderBase thatObj = (FolderBase) obj;
        if( (this.guid == null && thatObj.getGuid() != null)
            || (this.guid != null && thatObj.getGuid() == null)
            || !this.guid.equals(thatObj.getGuid()) ) {
            return false;
        }
        if( (this.appClient == null && thatObj.getAppClient() != null)
            || (this.appClient != null && thatObj.getAppClient() == null)
            || !this.appClient.equals(thatObj.getAppClient()) ) {
            return false;
        }
        if( (this.clientRootDomain == null && thatObj.getClientRootDomain() != null)
            || (this.clientRootDomain != null && thatObj.getClientRootDomain() == null)
            || !this.clientRootDomain.equals(thatObj.getClientRootDomain()) ) {
            return false;
        }
        if( (this.user == null && thatObj.getUser() != null)
            || (this.user != null && thatObj.getUser() == null)
            || !this.user.equals(thatObj.getUser()) ) {
            return false;
        }
        if( (this.title == null && thatObj.getTitle() != null)
            || (this.title != null && thatObj.getTitle() == null)
            || !this.title.equals(thatObj.getTitle()) ) {
            return false;
        }
        if( (this.description == null && thatObj.getDescription() != null)
            || (this.description != null && thatObj.getDescription() == null)
            || !this.description.equals(thatObj.getDescription()) ) {
            return false;
        }
        if( (this.type == null && thatObj.getType() != null)
            || (this.type != null && thatObj.getType() == null)
            || !this.type.equals(thatObj.getType()) ) {
            return false;
        }
        if( (this.category == null && thatObj.getCategory() != null)
            || (this.category != null && thatObj.getCategory() == null)
            || !this.category.equals(thatObj.getCategory()) ) {
            return false;
        }
        if( (this.parent == null && thatObj.getParent() != null)
            || (this.parent != null && thatObj.getParent() == null)
            || !this.parent.equals(thatObj.getParent()) ) {
            return false;
        }
        if( (this.aggregate == null && thatObj.getAggregate() != null)
            || (this.aggregate != null && thatObj.getAggregate() == null)
            || !this.aggregate.equals(thatObj.getAggregate()) ) {
            return false;
        }
        if( (this.acl == null && thatObj.getAcl() != null)
            || (this.acl != null && thatObj.getAcl() == null)
            || !this.acl.equals(thatObj.getAcl()) ) {
            return false;
        }
        if( (this.exportable == null && thatObj.isExportable() != null)
            || (this.exportable != null && thatObj.isExportable() == null)
            || !this.exportable.equals(thatObj.isExportable()) ) {
            return false;
        }
        if( (this.status == null && thatObj.getStatus() != null)
            || (this.status != null && thatObj.getStatus() == null)
            || !this.status.equals(thatObj.getStatus()) ) {
            return false;
        }
        if( (this.note == null && thatObj.getNote() != null)
            || (this.note != null && thatObj.getNote() == null)
            || !this.note.equals(thatObj.getNote()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = appClient == null ? 0 : appClient.hashCode();
        _hash = 31 * _hash + delta;
        delta = clientRootDomain == null ? 0 : clientRootDomain.hashCode();
        _hash = 31 * _hash + delta;
        delta = user == null ? 0 : user.hashCode();
        _hash = 31 * _hash + delta;
        delta = title == null ? 0 : title.hashCode();
        _hash = 31 * _hash + delta;
        delta = description == null ? 0 : description.hashCode();
        _hash = 31 * _hash + delta;
        delta = type == null ? 0 : type.hashCode();
        _hash = 31 * _hash + delta;
        delta = category == null ? 0 : category.hashCode();
        _hash = 31 * _hash + delta;
        delta = parent == null ? 0 : parent.hashCode();
        _hash = 31 * _hash + delta;
        delta = aggregate == null ? 0 : aggregate.hashCode();
        _hash = 31 * _hash + delta;
        delta = acl == null ? 0 : acl.hashCode();
        _hash = 31 * _hash + delta;
        delta = exportable == null ? 0 : exportable.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
