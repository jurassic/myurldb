package com.myurldb.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.myurldb.ws.FolderImportBase;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;


@PersistenceCapable(detachable="true")
@Inheritance(strategy = InheritanceStrategy.SUBCLASS_TABLE)
public abstract class FolderImportBaseDataObject extends KeyedDataObject implements FolderImportBase
{
    private static final Logger log = Logger.getLogger(FolderImportBaseDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(FolderImportBaseDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(FolderImportBaseDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String guid;

    @Persistent(defaultFetchGroup = "true")
    private String user;

    @Persistent(defaultFetchGroup = "true")
    private Integer precedence;

    @Persistent(defaultFetchGroup = "true")
    private String importType;

    @Persistent(defaultFetchGroup = "true")
    private String importedFolder;

    @Persistent(defaultFetchGroup = "true")
    private String importedFolderUser;

    @Persistent(defaultFetchGroup = "true")
    private String importedFolderTitle;

    @Persistent(defaultFetchGroup = "true")
    private String importedFolderPath;

    @Persistent(defaultFetchGroup = "true")
    private String status;

    @Persistent(defaultFetchGroup = "true")
    private String note;

    public FolderImportBaseDataObject()
    {
        this(null);
    }
    public FolderImportBaseDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null);
    }
    public FolderImportBaseDataObject(String guid, String user, Integer precedence, String importType, String importedFolder, String importedFolderUser, String importedFolderTitle, String importedFolderPath, String status, String note)
    {
        this(guid, user, precedence, importType, importedFolder, importedFolderUser, importedFolderTitle, importedFolderPath, status, note, null, null);
    }
    public FolderImportBaseDataObject(String guid, String user, Integer precedence, String importType, String importedFolder, String importedFolderUser, String importedFolderTitle, String importedFolderPath, String status, String note, Long createdTime, Long modifiedTime)
    {
        super(createdTime, modifiedTime);
        generatePK(guid);
        this.user = user;
        this.precedence = precedence;
        this.importType = importType;
        this.importedFolder = importedFolder;
        this.importedFolderUser = importedFolderUser;
        this.importedFolderTitle = importedFolderTitle;
        this.importedFolderPath = importedFolderPath;
        this.status = status;
        this.note = note;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        generatePK(guid);
    }
    protected void generatePK(String guid)
    {
        if(guid == null) {
            this.guid = GUID.generate();  // temporary
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to a new guid = " + this.guid);
        } else {
            this.guid = guid;
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to guid = " + this.guid);
        }
        rebuildKey();        
    }

//    @Override
//    protected Key createKey()
//    {
//        return FolderImportBaseDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return FolderImportBaseDataObject.composeKey(getGuid());
    }

    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    public Integer getPrecedence()
    {
        return this.precedence;
    }
    public void setPrecedence(Integer precedence)
    {
        this.precedence = precedence;
    }

    public String getImportType()
    {
        return this.importType;
    }
    public void setImportType(String importType)
    {
        this.importType = importType;
    }

    public String getImportedFolder()
    {
        return this.importedFolder;
    }
    public void setImportedFolder(String importedFolder)
    {
        this.importedFolder = importedFolder;
    }

    public String getImportedFolderUser()
    {
        return this.importedFolderUser;
    }
    public void setImportedFolderUser(String importedFolderUser)
    {
        this.importedFolderUser = importedFolderUser;
    }

    public String getImportedFolderTitle()
    {
        return this.importedFolderTitle;
    }
    public void setImportedFolderTitle(String importedFolderTitle)
    {
        this.importedFolderTitle = importedFolderTitle;
    }

    public String getImportedFolderPath()
    {
        return this.importedFolderPath;
    }
    public void setImportedFolderPath(String importedFolderPath)
    {
        this.importedFolderPath = importedFolderPath;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("guid", this.guid);
        dataMap.put("user", this.user);
        dataMap.put("precedence", this.precedence);
        dataMap.put("importType", this.importType);
        dataMap.put("importedFolder", this.importedFolder);
        dataMap.put("importedFolderUser", this.importedFolderUser);
        dataMap.put("importedFolderTitle", this.importedFolderTitle);
        dataMap.put("importedFolderPath", this.importedFolderPath);
        dataMap.put("status", this.status);
        dataMap.put("note", this.note);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        FolderImportBase thatObj = (FolderImportBase) obj;
        if( (this.guid == null && thatObj.getGuid() != null)
            || (this.guid != null && thatObj.getGuid() == null)
            || !this.guid.equals(thatObj.getGuid()) ) {
            return false;
        }
        if( (this.user == null && thatObj.getUser() != null)
            || (this.user != null && thatObj.getUser() == null)
            || !this.user.equals(thatObj.getUser()) ) {
            return false;
        }
        if( (this.precedence == null && thatObj.getPrecedence() != null)
            || (this.precedence != null && thatObj.getPrecedence() == null)
            || !this.precedence.equals(thatObj.getPrecedence()) ) {
            return false;
        }
        if( (this.importType == null && thatObj.getImportType() != null)
            || (this.importType != null && thatObj.getImportType() == null)
            || !this.importType.equals(thatObj.getImportType()) ) {
            return false;
        }
        if( (this.importedFolder == null && thatObj.getImportedFolder() != null)
            || (this.importedFolder != null && thatObj.getImportedFolder() == null)
            || !this.importedFolder.equals(thatObj.getImportedFolder()) ) {
            return false;
        }
        if( (this.importedFolderUser == null && thatObj.getImportedFolderUser() != null)
            || (this.importedFolderUser != null && thatObj.getImportedFolderUser() == null)
            || !this.importedFolderUser.equals(thatObj.getImportedFolderUser()) ) {
            return false;
        }
        if( (this.importedFolderTitle == null && thatObj.getImportedFolderTitle() != null)
            || (this.importedFolderTitle != null && thatObj.getImportedFolderTitle() == null)
            || !this.importedFolderTitle.equals(thatObj.getImportedFolderTitle()) ) {
            return false;
        }
        if( (this.importedFolderPath == null && thatObj.getImportedFolderPath() != null)
            || (this.importedFolderPath != null && thatObj.getImportedFolderPath() == null)
            || !this.importedFolderPath.equals(thatObj.getImportedFolderPath()) ) {
            return false;
        }
        if( (this.status == null && thatObj.getStatus() != null)
            || (this.status != null && thatObj.getStatus() == null)
            || !this.status.equals(thatObj.getStatus()) ) {
            return false;
        }
        if( (this.note == null && thatObj.getNote() != null)
            || (this.note != null && thatObj.getNote() == null)
            || !this.note.equals(thatObj.getNote()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = user == null ? 0 : user.hashCode();
        _hash = 31 * _hash + delta;
        delta = precedence == null ? 0 : precedence.hashCode();
        _hash = 31 * _hash + delta;
        delta = importType == null ? 0 : importType.hashCode();
        _hash = 31 * _hash + delta;
        delta = importedFolder == null ? 0 : importedFolder.hashCode();
        _hash = 31 * _hash + delta;
        delta = importedFolderUser == null ? 0 : importedFolderUser.hashCode();
        _hash = 31 * _hash + delta;
        delta = importedFolderTitle == null ? 0 : importedFolderTitle.hashCode();
        _hash = 31 * _hash + delta;
        delta = importedFolderPath == null ? 0 : importedFolderPath.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
