package com.myurldb.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.Extension;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PrimaryKey;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.myurldb.ws.FullNameStruct;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;


@PersistenceCapable(detachable="true")
// @EmbeddedOnly
public class FullNameStructDataObject implements FullNameStruct, Serializable
{
    private static final Logger log = Logger.getLogger(FullNameStructDataObject.class.getName());

    @PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    @Extension(vendorName="datanucleus", key="gae.encoded-pk", value="true")
    private String _fullnamestruct_encoded_key = null;   // Due to GAE limitation, the PK needs to be either Key or encoded string...

    @Persistent(defaultFetchGroup = "true")
    // @Extension(vendorName="datanucleus", key="gae.pk-name", value="true")
    private String uuid;

    @Persistent(defaultFetchGroup = "true")
    private String displayName;

    @Persistent(defaultFetchGroup = "true")
    private String lastName;

    @Persistent(defaultFetchGroup = "true")
    private String firstName;

    @Persistent(defaultFetchGroup = "true")
    private String middleName1;

    @Persistent(defaultFetchGroup = "true")
    private String middleName2;

    @Persistent(defaultFetchGroup = "true")
    private String middleInitial;

    @Persistent(defaultFetchGroup = "true")
    private String salutation;

    @Persistent(defaultFetchGroup = "true")
    private String suffix;

    @Persistent(defaultFetchGroup = "true")
    private String note;

    public FullNameStructDataObject()
    {
        // ???
        // this(null, null, null, null, null, null, null, null, null, null);
    }
    public FullNameStructDataObject(String uuid, String displayName, String lastName, String firstName, String middleName1, String middleName2, String middleInitial, String salutation, String suffix, String note)
    {
        setUuid(uuid);
        setDisplayName(displayName);
        setLastName(lastName);
        setFirstName(firstName);
        setMiddleName1(middleName1);
        setMiddleName2(middleName2);
        setMiddleInitial(middleInitial);
        setSalutation(salutation);
        setSuffix(suffix);
        setNote(note);
    }

    private void resetEncodedKey()
    {
        if(_fullnamestruct_encoded_key == null) {
            if(uuid == null || uuid.isEmpty()) {
                // Temporary solution. PK cannot be null.
                uuid = GUID.generate();
            }
            _fullnamestruct_encoded_key = KeyFactory.createKeyString(FullNameStructDataObject.class.getSimpleName(), uuid);
        }
    }

    public String getUuid()
    {
        return this.uuid;
    }
    public void setUuid(String uuid)
    {
        if(uuid == null || uuid.isEmpty()) {
            // Temporary solution. PK cannot be null. ???
            // this.uuid = GUID.generate();
            this.uuid = uuid;
            _fullnamestruct_encoded_key = null;   // ???
        } else {
            this.uuid = uuid;
            _fullnamestruct_encoded_key = KeyFactory.createKeyString(FullNameStructDataObject.class.getSimpleName(), this.uuid);
        }
    }

    public String getDisplayName()
    {
        return this.displayName;
    }
    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
        if(this.displayName != null) {
            resetEncodedKey();
        }
    }

    public String getLastName()
    {
        return this.lastName;
    }
    public void setLastName(String lastName)
    {
        this.lastName = lastName;
        if(this.lastName != null) {
            resetEncodedKey();
        }
    }

    public String getFirstName()
    {
        return this.firstName;
    }
    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
        if(this.firstName != null) {
            resetEncodedKey();
        }
    }

    public String getMiddleName1()
    {
        return this.middleName1;
    }
    public void setMiddleName1(String middleName1)
    {
        this.middleName1 = middleName1;
        if(this.middleName1 != null) {
            resetEncodedKey();
        }
    }

    public String getMiddleName2()
    {
        return this.middleName2;
    }
    public void setMiddleName2(String middleName2)
    {
        this.middleName2 = middleName2;
        if(this.middleName2 != null) {
            resetEncodedKey();
        }
    }

    public String getMiddleInitial()
    {
        return this.middleInitial;
    }
    public void setMiddleInitial(String middleInitial)
    {
        this.middleInitial = middleInitial;
        if(this.middleInitial != null) {
            resetEncodedKey();
        }
    }

    public String getSalutation()
    {
        return this.salutation;
    }
    public void setSalutation(String salutation)
    {
        this.salutation = salutation;
        if(this.salutation != null) {
            resetEncodedKey();
        }
    }

    public String getSuffix()
    {
        return this.suffix;
    }
    public void setSuffix(String suffix)
    {
        this.suffix = suffix;
        if(this.suffix != null) {
            resetEncodedKey();
        }
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
        if(this.note != null) {
            resetEncodedKey();
        }
    }


    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getDisplayName() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLastName() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getFirstName() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getMiddleName1() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getMiddleName2() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getMiddleInitial() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSalutation() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSuffix() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("uuid", this.uuid);
        dataMap.put("displayName", this.displayName);
        dataMap.put("lastName", this.lastName);
        dataMap.put("firstName", this.firstName);
        dataMap.put("middleName1", this.middleName1);
        dataMap.put("middleName2", this.middleName2);
        dataMap.put("middleInitial", this.middleInitial);
        dataMap.put("salutation", this.salutation);
        dataMap.put("suffix", this.suffix);
        dataMap.put("note", this.note);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        FullNameStruct thatObj = (FullNameStruct) obj;
        if( (this.uuid == null && thatObj.getUuid() != null)
            || (this.uuid != null && thatObj.getUuid() == null)
            || !this.uuid.equals(thatObj.getUuid()) ) {
            return false;
        }
        if( (this.displayName == null && thatObj.getDisplayName() != null)
            || (this.displayName != null && thatObj.getDisplayName() == null)
            || !this.displayName.equals(thatObj.getDisplayName()) ) {
            return false;
        }
        if( (this.lastName == null && thatObj.getLastName() != null)
            || (this.lastName != null && thatObj.getLastName() == null)
            || !this.lastName.equals(thatObj.getLastName()) ) {
            return false;
        }
        if( (this.firstName == null && thatObj.getFirstName() != null)
            || (this.firstName != null && thatObj.getFirstName() == null)
            || !this.firstName.equals(thatObj.getFirstName()) ) {
            return false;
        }
        if( (this.middleName1 == null && thatObj.getMiddleName1() != null)
            || (this.middleName1 != null && thatObj.getMiddleName1() == null)
            || !this.middleName1.equals(thatObj.getMiddleName1()) ) {
            return false;
        }
        if( (this.middleName2 == null && thatObj.getMiddleName2() != null)
            || (this.middleName2 != null && thatObj.getMiddleName2() == null)
            || !this.middleName2.equals(thatObj.getMiddleName2()) ) {
            return false;
        }
        if( (this.middleInitial == null && thatObj.getMiddleInitial() != null)
            || (this.middleInitial != null && thatObj.getMiddleInitial() == null)
            || !this.middleInitial.equals(thatObj.getMiddleInitial()) ) {
            return false;
        }
        if( (this.salutation == null && thatObj.getSalutation() != null)
            || (this.salutation != null && thatObj.getSalutation() == null)
            || !this.salutation.equals(thatObj.getSalutation()) ) {
            return false;
        }
        if( (this.suffix == null && thatObj.getSuffix() != null)
            || (this.suffix != null && thatObj.getSuffix() == null)
            || !this.suffix.equals(thatObj.getSuffix()) ) {
            return false;
        }
        if( (this.note == null && thatObj.getNote() != null)
            || (this.note != null && thatObj.getNote() == null)
            || !this.note.equals(thatObj.getNote()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = uuid == null ? 0 : uuid.hashCode();
        _hash = 31 * _hash + delta;
        delta = displayName == null ? 0 : displayName.hashCode();
        _hash = 31 * _hash + delta;
        delta = lastName == null ? 0 : lastName.hashCode();
        _hash = 31 * _hash + delta;
        delta = firstName == null ? 0 : firstName.hashCode();
        _hash = 31 * _hash + delta;
        delta = middleName1 == null ? 0 : middleName1.hashCode();
        _hash = 31 * _hash + delta;
        delta = middleName2 == null ? 0 : middleName2.hashCode();
        _hash = 31 * _hash + delta;
        delta = middleInitial == null ? 0 : middleInitial.hashCode();
        _hash = 31 * _hash + delta;
        delta = salutation == null ? 0 : salutation.hashCode();
        _hash = 31 * _hash + delta;
        delta = suffix == null ? 0 : suffix.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public String toString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuffer sb = new StringBuffer();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString(); // ???
            sb.append(k).append(":").append(v).append(";");
        }
        return sb.toString();
    }

    // TBD: Serialization methods?
    // ...

}
