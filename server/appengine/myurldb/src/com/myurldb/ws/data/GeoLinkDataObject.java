package com.myurldb.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.myurldb.ws.CellLatitudeLongitude;
import com.myurldb.ws.GeoCoordinateStruct;
import com.myurldb.ws.GeoLink;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;


@PersistenceCapable(detachable="true")
public class GeoLinkDataObject extends KeyedDataObject implements GeoLink
{
    private static final Logger log = Logger.getLogger(GeoLinkDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(GeoLinkDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(GeoLinkDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String guid;

    @Persistent(defaultFetchGroup = "true")
    private String shortLink;

    @Persistent(defaultFetchGroup = "true")
    private String shortUrl;

    @Persistent(defaultFetchGroup = "true")
    @Embedded(members = {
        @Persistent(name="uuid", columns=@Column(name="geoCoordinateuuid")),
        @Persistent(name="latitude", columns=@Column(name="geoCoordinatelatitude")),
        @Persistent(name="longitude", columns=@Column(name="geoCoordinatelongitude")),
        @Persistent(name="altitude", columns=@Column(name="geoCoordinatealtitude")),
        @Persistent(name="sensorUsed", columns=@Column(name="geoCoordinatesensorUsed")),
        @Persistent(name="accuracy", columns=@Column(name="geoCoordinateaccuracy")),
        @Persistent(name="altitudeAccuracy", columns=@Column(name="geoCoordinatealtitudeAccuracy")),
        @Persistent(name="heading", columns=@Column(name="geoCoordinateheading")),
        @Persistent(name="speed", columns=@Column(name="geoCoordinatespeed")),
        @Persistent(name="note", columns=@Column(name="geoCoordinatenote")),
    })
    private GeoCoordinateStructDataObject geoCoordinate;

    @Persistent(defaultFetchGroup = "false")
    @Embedded(members = {
        @Persistent(name="scale", columns=@Column(name="geoCellscale")),
        @Persistent(name="latitude", columns=@Column(name="geoCelllatitude")),
        @Persistent(name="longitude", columns=@Column(name="geoCelllongitude")),
    })
    private CellLatitudeLongitudeDataObject geoCell;

    @Persistent(defaultFetchGroup = "true")
    private String status;

    public GeoLinkDataObject()
    {
        this(null);
    }
    public GeoLinkDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null);
    }
    public GeoLinkDataObject(String guid, String shortLink, String shortUrl, GeoCoordinateStruct geoCoordinate, CellLatitudeLongitude geoCell, String status)
    {
        this(guid, shortLink, shortUrl, geoCoordinate, geoCell, status, null, null);
    }
    public GeoLinkDataObject(String guid, String shortLink, String shortUrl, GeoCoordinateStruct geoCoordinate, CellLatitudeLongitude geoCell, String status, Long createdTime, Long modifiedTime)
    {
        super(createdTime, modifiedTime);
        generatePK(guid);
        this.shortLink = shortLink;
        this.shortUrl = shortUrl;
        if(geoCoordinate != null) {
            this.geoCoordinate = new GeoCoordinateStructDataObject(geoCoordinate.getUuid(), geoCoordinate.getLatitude(), geoCoordinate.getLongitude(), geoCoordinate.getAltitude(), geoCoordinate.isSensorUsed(), geoCoordinate.getAccuracy(), geoCoordinate.getAltitudeAccuracy(), geoCoordinate.getHeading(), geoCoordinate.getSpeed(), geoCoordinate.getNote());
        } else {
            this.geoCoordinate = null;
        }
        if(geoCell != null) {
            this.geoCell = new CellLatitudeLongitudeDataObject(geoCell.getScale(), geoCell.getLatitude(), geoCell.getLongitude());
        } else {
            this.geoCell = null;
        }
        this.status = status;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        generatePK(guid);
    }
    protected void generatePK(String guid)
    {
        if(guid == null) {
            this.guid = GUID.generate();  // temporary
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to a new guid = " + this.guid);
        } else {
            this.guid = guid;
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to guid = " + this.guid);
        }
        rebuildKey();        
    }

//    @Override
//    protected Key createKey()
//    {
//        return GeoLinkDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return GeoLinkDataObject.composeKey(getGuid());
    }

    public String getShortLink()
    {
        return this.shortLink;
    }
    public void setShortLink(String shortLink)
    {
        this.shortLink = shortLink;
    }

    public String getShortUrl()
    {
        return this.shortUrl;
    }
    public void setShortUrl(String shortUrl)
    {
        this.shortUrl = shortUrl;
    }

    public GeoCoordinateStruct getGeoCoordinate()
    {
        return this.geoCoordinate;
    }
    public void setGeoCoordinate(GeoCoordinateStruct geoCoordinate)
    {
        if(geoCoordinate == null) {
            this.geoCoordinate = null;
            log.log(Level.INFO, "GeoLinkDataObject.setGeoCoordinate(GeoCoordinateStruct geoCoordinate): Arg geoCoordinate is null.");            
        } else if(geoCoordinate instanceof GeoCoordinateStructDataObject) {
            this.geoCoordinate = (GeoCoordinateStructDataObject) geoCoordinate;
        } else if(geoCoordinate instanceof GeoCoordinateStruct) {
            this.geoCoordinate = new GeoCoordinateStructDataObject(geoCoordinate.getUuid(), geoCoordinate.getLatitude(), geoCoordinate.getLongitude(), geoCoordinate.getAltitude(), geoCoordinate.isSensorUsed(), geoCoordinate.getAccuracy(), geoCoordinate.getAltitudeAccuracy(), geoCoordinate.getHeading(), geoCoordinate.getSpeed(), geoCoordinate.getNote());
        } else {
            this.geoCoordinate = new GeoCoordinateStructDataObject();   // ????
            log.log(Level.WARNING, "GeoLinkDataObject.setGeoCoordinate(GeoCoordinateStruct geoCoordinate): Arg geoCoordinate is of an invalid type.");
        }
    }

    public CellLatitudeLongitude getGeoCell()
    {
        return this.geoCell;
    }
    public void setGeoCell(CellLatitudeLongitude geoCell)
    {
        if(geoCell == null) {
            this.geoCell = null;
            log.log(Level.INFO, "GeoLinkDataObject.setGeoCell(CellLatitudeLongitude geoCell): Arg geoCell is null.");            
        } else if(geoCell instanceof CellLatitudeLongitudeDataObject) {
            this.geoCell = (CellLatitudeLongitudeDataObject) geoCell;
        } else if(geoCell instanceof CellLatitudeLongitude) {
            this.geoCell = new CellLatitudeLongitudeDataObject(geoCell.getScale(), geoCell.getLatitude(), geoCell.getLongitude());
        } else {
            this.geoCell = new CellLatitudeLongitudeDataObject();   // ????
            log.log(Level.WARNING, "GeoLinkDataObject.setGeoCell(CellLatitudeLongitude geoCell): Arg geoCell is of an invalid type.");
        }
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("guid", this.guid);
        dataMap.put("shortLink", this.shortLink);
        dataMap.put("shortUrl", this.shortUrl);
        dataMap.put("geoCoordinate", this.geoCoordinate);
        dataMap.put("geoCell", this.geoCell);
        dataMap.put("status", this.status);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        GeoLink thatObj = (GeoLink) obj;
        if( (this.guid == null && thatObj.getGuid() != null)
            || (this.guid != null && thatObj.getGuid() == null)
            || !this.guid.equals(thatObj.getGuid()) ) {
            return false;
        }
        if( (this.shortLink == null && thatObj.getShortLink() != null)
            || (this.shortLink != null && thatObj.getShortLink() == null)
            || !this.shortLink.equals(thatObj.getShortLink()) ) {
            return false;
        }
        if( (this.shortUrl == null && thatObj.getShortUrl() != null)
            || (this.shortUrl != null && thatObj.getShortUrl() == null)
            || !this.shortUrl.equals(thatObj.getShortUrl()) ) {
            return false;
        }
        if( (this.geoCoordinate == null && thatObj.getGeoCoordinate() != null)
            || (this.geoCoordinate != null && thatObj.getGeoCoordinate() == null)
            || !this.geoCoordinate.equals(thatObj.getGeoCoordinate()) ) {
            return false;
        }
        if( (this.geoCell == null && thatObj.getGeoCell() != null)
            || (this.geoCell != null && thatObj.getGeoCell() == null)
            || !this.geoCell.equals(thatObj.getGeoCell()) ) {
            return false;
        }
        if( (this.status == null && thatObj.getStatus() != null)
            || (this.status != null && thatObj.getStatus() == null)
            || !this.status.equals(thatObj.getStatus()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = shortLink == null ? 0 : shortLink.hashCode();
        _hash = 31 * _hash + delta;
        delta = shortUrl == null ? 0 : shortUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = geoCoordinate == null ? 0 : geoCoordinate.hashCode();
        _hash = 31 * _hash + delta;
        delta = geoCell == null ? 0 : geoCell.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
