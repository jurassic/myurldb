package com.myurldb.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.Extension;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PrimaryKey;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.myurldb.ws.GeoPointStruct;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;


@PersistenceCapable(detachable="true")
// @EmbeddedOnly
public class GeoPointStructDataObject implements GeoPointStruct, Serializable
{
    private static final Logger log = Logger.getLogger(GeoPointStructDataObject.class.getName());

    @PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    @Extension(vendorName="datanucleus", key="gae.encoded-pk", value="true")
    private String _geopointstruct_encoded_key = null;   // Due to GAE limitation, the PK needs to be either Key or encoded string...

    @Persistent(defaultFetchGroup = "true")
    // @Extension(vendorName="datanucleus", key="gae.pk-name", value="true")
    private String uuid;

    @Persistent(defaultFetchGroup = "true")
    private Double latitude;

    @Persistent(defaultFetchGroup = "true")
    private Double longitude;

    @Persistent(defaultFetchGroup = "true")
    private Double altitude;

    @Persistent(defaultFetchGroup = "true")
    private Boolean sensorUsed;

    public GeoPointStructDataObject()
    {
        // ???
        // this(null, null, null, null, null);
    }
    public GeoPointStructDataObject(String uuid, Double latitude, Double longitude, Double altitude, Boolean sensorUsed)
    {
        setUuid(uuid);
        setLatitude(latitude);
        setLongitude(longitude);
        setAltitude(altitude);
        setSensorUsed(sensorUsed);
    }

    private void resetEncodedKey()
    {
        if(_geopointstruct_encoded_key == null) {
            if(uuid == null || uuid.isEmpty()) {
                // Temporary solution. PK cannot be null.
                uuid = GUID.generate();
            }
            _geopointstruct_encoded_key = KeyFactory.createKeyString(GeoPointStructDataObject.class.getSimpleName(), uuid);
        }
    }

    public String getUuid()
    {
        return this.uuid;
    }
    public void setUuid(String uuid)
    {
        if(uuid == null || uuid.isEmpty()) {
            // Temporary solution. PK cannot be null. ???
            // this.uuid = GUID.generate();
            this.uuid = uuid;
            _geopointstruct_encoded_key = null;   // ???
        } else {
            this.uuid = uuid;
            _geopointstruct_encoded_key = KeyFactory.createKeyString(GeoPointStructDataObject.class.getSimpleName(), this.uuid);
        }
    }

    public Double getLatitude()
    {
        return this.latitude;
    }
    public void setLatitude(Double latitude)
    {
        this.latitude = latitude;
        if(this.latitude != null) {
            resetEncodedKey();
        }
    }

    public Double getLongitude()
    {
        return this.longitude;
    }
    public void setLongitude(Double longitude)
    {
        this.longitude = longitude;
        if(this.longitude != null) {
            resetEncodedKey();
        }
    }

    public Double getAltitude()
    {
        return this.altitude;
    }
    public void setAltitude(Double altitude)
    {
        this.altitude = altitude;
        if(this.altitude != null) {
            resetEncodedKey();
        }
    }

    public Boolean isSensorUsed()
    {
        return this.sensorUsed;
    }
    public void setSensorUsed(Boolean sensorUsed)
    {
        this.sensorUsed = sensorUsed;
        if(this.sensorUsed != null) {
            resetEncodedKey();
        }
    }


    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getLatitude() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLongitude() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAltitude() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && isSensorUsed() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("uuid", this.uuid);
        dataMap.put("latitude", this.latitude);
        dataMap.put("longitude", this.longitude);
        dataMap.put("altitude", this.altitude);
        dataMap.put("sensorUsed", this.sensorUsed);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        GeoPointStruct thatObj = (GeoPointStruct) obj;
        if( (this.uuid == null && thatObj.getUuid() != null)
            || (this.uuid != null && thatObj.getUuid() == null)
            || !this.uuid.equals(thatObj.getUuid()) ) {
            return false;
        }
        if( (this.latitude == null && thatObj.getLatitude() != null)
            || (this.latitude != null && thatObj.getLatitude() == null)
            || !this.latitude.equals(thatObj.getLatitude()) ) {
            return false;
        }
        if( (this.longitude == null && thatObj.getLongitude() != null)
            || (this.longitude != null && thatObj.getLongitude() == null)
            || !this.longitude.equals(thatObj.getLongitude()) ) {
            return false;
        }
        if( (this.altitude == null && thatObj.getAltitude() != null)
            || (this.altitude != null && thatObj.getAltitude() == null)
            || !this.altitude.equals(thatObj.getAltitude()) ) {
            return false;
        }
        if( (this.sensorUsed == null && thatObj.isSensorUsed() != null)
            || (this.sensorUsed != null && thatObj.isSensorUsed() == null)
            || !this.sensorUsed.equals(thatObj.isSensorUsed()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = uuid == null ? 0 : uuid.hashCode();
        _hash = 31 * _hash + delta;
        delta = latitude == null ? 0 : latitude.hashCode();
        _hash = 31 * _hash + delta;
        delta = longitude == null ? 0 : longitude.hashCode();
        _hash = 31 * _hash + delta;
        delta = altitude == null ? 0 : altitude.hashCode();
        _hash = 31 * _hash + delta;
        delta = sensorUsed == null ? 0 : sensorUsed.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public String toString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuffer sb = new StringBuffer();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString(); // ???
            sb.append(k).append(":").append(v).append(";");
        }
        return sb.toString();
    }

    // TBD: Serialization methods?
    // ...

}
