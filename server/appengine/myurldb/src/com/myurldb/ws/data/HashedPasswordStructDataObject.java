package com.myurldb.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.Extension;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PrimaryKey;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.myurldb.ws.HashedPasswordStruct;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;


@PersistenceCapable(detachable="true")
// @EmbeddedOnly
public class HashedPasswordStructDataObject implements HashedPasswordStruct, Serializable
{
    private static final Logger log = Logger.getLogger(HashedPasswordStructDataObject.class.getName());

    @PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    @Extension(vendorName="datanucleus", key="gae.encoded-pk", value="true")
    private String _hashedpasswordstruct_encoded_key = null;   // Due to GAE limitation, the PK needs to be either Key or encoded string...

    @Persistent(defaultFetchGroup = "true")
    // @Extension(vendorName="datanucleus", key="gae.pk-name", value="true")
    private String uuid;

    @Persistent(defaultFetchGroup = "true")
    private String plainText;

    @Persistent(defaultFetchGroup = "true")
    private String hashedText;

    @Persistent(defaultFetchGroup = "true")
    private String salt;

    @Persistent(defaultFetchGroup = "true")
    private String algorithm;

    public HashedPasswordStructDataObject()
    {
        // ???
        // this(null, null, null, null, null);
    }
    public HashedPasswordStructDataObject(String uuid, String plainText, String hashedText, String salt, String algorithm)
    {
        setUuid(uuid);
        setPlainText(plainText);
        setHashedText(hashedText);
        setSalt(salt);
        setAlgorithm(algorithm);
    }

    private void resetEncodedKey()
    {
        if(_hashedpasswordstruct_encoded_key == null) {
            if(uuid == null || uuid.isEmpty()) {
                // Temporary solution. PK cannot be null.
                uuid = GUID.generate();
            }
            _hashedpasswordstruct_encoded_key = KeyFactory.createKeyString(HashedPasswordStructDataObject.class.getSimpleName(), uuid);
        }
    }

    public String getUuid()
    {
        return this.uuid;
    }
    public void setUuid(String uuid)
    {
        if(uuid == null || uuid.isEmpty()) {
            // Temporary solution. PK cannot be null. ???
            // this.uuid = GUID.generate();
            this.uuid = uuid;
            _hashedpasswordstruct_encoded_key = null;   // ???
        } else {
            this.uuid = uuid;
            _hashedpasswordstruct_encoded_key = KeyFactory.createKeyString(HashedPasswordStructDataObject.class.getSimpleName(), this.uuid);
        }
    }

    public String getPlainText()
    {
        return this.plainText;
    }
    public void setPlainText(String plainText)
    {
        this.plainText = plainText;
        if(this.plainText != null) {
            resetEncodedKey();
        }
    }

    public String getHashedText()
    {
        return this.hashedText;
    }
    public void setHashedText(String hashedText)
    {
        this.hashedText = hashedText;
        if(this.hashedText != null) {
            resetEncodedKey();
        }
    }

    public String getSalt()
    {
        return this.salt;
    }
    public void setSalt(String salt)
    {
        this.salt = salt;
        if(this.salt != null) {
            resetEncodedKey();
        }
    }

    public String getAlgorithm()
    {
        return this.algorithm;
    }
    public void setAlgorithm(String algorithm)
    {
        this.algorithm = algorithm;
        if(this.algorithm != null) {
            resetEncodedKey();
        }
    }


    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getPlainText() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHashedText() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSalt() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAlgorithm() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("uuid", this.uuid);
        dataMap.put("plainText", this.plainText);
        dataMap.put("hashedText", this.hashedText);
        dataMap.put("salt", this.salt);
        dataMap.put("algorithm", this.algorithm);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        HashedPasswordStruct thatObj = (HashedPasswordStruct) obj;
        if( (this.uuid == null && thatObj.getUuid() != null)
            || (this.uuid != null && thatObj.getUuid() == null)
            || !this.uuid.equals(thatObj.getUuid()) ) {
            return false;
        }
        if( (this.plainText == null && thatObj.getPlainText() != null)
            || (this.plainText != null && thatObj.getPlainText() == null)
            || !this.plainText.equals(thatObj.getPlainText()) ) {
            return false;
        }
        if( (this.hashedText == null && thatObj.getHashedText() != null)
            || (this.hashedText != null && thatObj.getHashedText() == null)
            || !this.hashedText.equals(thatObj.getHashedText()) ) {
            return false;
        }
        if( (this.salt == null && thatObj.getSalt() != null)
            || (this.salt != null && thatObj.getSalt() == null)
            || !this.salt.equals(thatObj.getSalt()) ) {
            return false;
        }
        if( (this.algorithm == null && thatObj.getAlgorithm() != null)
            || (this.algorithm != null && thatObj.getAlgorithm() == null)
            || !this.algorithm.equals(thatObj.getAlgorithm()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = uuid == null ? 0 : uuid.hashCode();
        _hash = 31 * _hash + delta;
        delta = plainText == null ? 0 : plainText.hashCode();
        _hash = 31 * _hash + delta;
        delta = hashedText == null ? 0 : hashedText.hashCode();
        _hash = 31 * _hash + delta;
        delta = salt == null ? 0 : salt.hashCode();
        _hash = 31 * _hash + delta;
        delta = algorithm == null ? 0 : algorithm.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public String toString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuffer sb = new StringBuffer();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString(); // ???
            sb.append(k).append(":").append(v).append(";");
        }
        return sb.toString();
    }

    // TBD: Serialization methods?
    // ...

}
