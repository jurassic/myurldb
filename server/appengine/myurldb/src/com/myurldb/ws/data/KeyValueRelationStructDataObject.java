package com.myurldb.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.Extension;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PrimaryKey;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.myurldb.ws.KeyValueRelationStruct;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;


@PersistenceCapable(detachable="true")
// @EmbeddedOnly
public class KeyValueRelationStructDataObject implements KeyValueRelationStruct, Serializable
{
    private static final Logger log = Logger.getLogger(KeyValueRelationStructDataObject.class.getName());

    @PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    @Extension(vendorName="datanucleus", key="gae.encoded-pk", value="true")
    private String _keyvaluerelationstruct_encoded_key = null;   // Due to GAE limitation, the PK needs to be either Key or encoded string...

    @Persistent(defaultFetchGroup = "true")
    // @Extension(vendorName="datanucleus", key="gae.pk-name", value="true")
    private String uuid;

    @Persistent(defaultFetchGroup = "true")
    private String key;

    @Persistent(defaultFetchGroup = "true")
    private String value;

    @Persistent(defaultFetchGroup = "true")
    private String note;

    @Persistent(defaultFetchGroup = "true")
    private String relation;

    public KeyValueRelationStructDataObject()
    {
        // ???
        // this(null, null, null, null, null);
    }
    public KeyValueRelationStructDataObject(String uuid, String key, String value, String note, String relation)
    {
        setUuid(uuid);
        setKey(key);
        setValue(value);
        setNote(note);
        setRelation(relation);
    }

    private void resetEncodedKey()
    {
        if(_keyvaluerelationstruct_encoded_key == null) {
            if(uuid == null || uuid.isEmpty()) {
                // Temporary solution. PK cannot be null.
                uuid = GUID.generate();
            }
            _keyvaluerelationstruct_encoded_key = KeyFactory.createKeyString(KeyValueRelationStructDataObject.class.getSimpleName(), uuid);
        }
    }

    public String getUuid()
    {
        return this.uuid;
    }
    public void setUuid(String uuid)
    {
        if(uuid == null || uuid.isEmpty()) {
            // Temporary solution. PK cannot be null. ???
            // this.uuid = GUID.generate();
            this.uuid = uuid;
            _keyvaluerelationstruct_encoded_key = null;   // ???
        } else {
            this.uuid = uuid;
            _keyvaluerelationstruct_encoded_key = KeyFactory.createKeyString(KeyValueRelationStructDataObject.class.getSimpleName(), this.uuid);
        }
    }

    public String getKey()
    {
        return this.key;
    }
    public void setKey(String key)
    {
        this.key = key;
        if(this.key != null) {
            resetEncodedKey();
        }
    }

    public String getValue()
    {
        return this.value;
    }
    public void setValue(String value)
    {
        this.value = value;
        if(this.value != null) {
            resetEncodedKey();
        }
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
        if(this.note != null) {
            resetEncodedKey();
        }
    }

    public String getRelation()
    {
        return this.relation;
    }
    public void setRelation(String relation)
    {
        this.relation = relation;
        if(this.relation != null) {
            resetEncodedKey();
        }
    }


    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getKey() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getValue() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getRelation() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("uuid", this.uuid);
        dataMap.put("key", this.key);
        dataMap.put("value", this.value);
        dataMap.put("note", this.note);
        dataMap.put("relation", this.relation);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        KeyValueRelationStruct thatObj = (KeyValueRelationStruct) obj;
        if( (this.uuid == null && thatObj.getUuid() != null)
            || (this.uuid != null && thatObj.getUuid() == null)
            || !this.uuid.equals(thatObj.getUuid()) ) {
            return false;
        }
        if( (this.key == null && thatObj.getKey() != null)
            || (this.key != null && thatObj.getKey() == null)
            || !this.key.equals(thatObj.getKey()) ) {
            return false;
        }
        if( (this.value == null && thatObj.getValue() != null)
            || (this.value != null && thatObj.getValue() == null)
            || !this.value.equals(thatObj.getValue()) ) {
            return false;
        }
        if( (this.note == null && thatObj.getNote() != null)
            || (this.note != null && thatObj.getNote() == null)
            || !this.note.equals(thatObj.getNote()) ) {
            return false;
        }
        if( (this.relation == null && thatObj.getRelation() != null)
            || (this.relation != null && thatObj.getRelation() == null)
            || !this.relation.equals(thatObj.getRelation()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = uuid == null ? 0 : uuid.hashCode();
        _hash = 31 * _hash + delta;
        delta = key == null ? 0 : key.hashCode();
        _hash = 31 * _hash + delta;
        delta = value == null ? 0 : value.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        delta = relation == null ? 0 : relation.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public String toString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuffer sb = new StringBuffer();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString(); // ???
            sb.append(k).append(":").append(v).append(";");
        }
        return sb.toString();
    }

    // TBD: Serialization methods?
    // ...

}
