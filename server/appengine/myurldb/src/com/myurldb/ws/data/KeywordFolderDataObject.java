package com.myurldb.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.myurldb.ws.KeywordFolder;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;


@PersistenceCapable(detachable="true")
@Inheritance(customStrategy = "complete-table")
public class KeywordFolderDataObject extends FolderBaseDataObject implements KeywordFolder
{
    private static final Logger log = Logger.getLogger(KeywordFolderDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(KeywordFolderDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(KeywordFolderDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String folderPath;

    public KeywordFolderDataObject()
    {
        this(null);
    }
    public KeywordFolderDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public KeywordFolderDataObject(String guid, String appClient, String clientRootDomain, String user, String title, String description, String type, String category, String parent, String aggregate, String acl, Boolean exportable, String status, String note, String folderPath)
    {
        this(guid, appClient, clientRootDomain, user, title, description, type, category, parent, aggregate, acl, exportable, status, note, folderPath, null, null);
    }
    public KeywordFolderDataObject(String guid, String appClient, String clientRootDomain, String user, String title, String description, String type, String category, String parent, String aggregate, String acl, Boolean exportable, String status, String note, String folderPath, Long createdTime, Long modifiedTime)
    {
        super(guid, appClient, clientRootDomain, user, title, description, type, category, parent, aggregate, acl, exportable, status, note, createdTime, modifiedTime);
        this.folderPath = folderPath;
    }

//    @Override
//    protected Key createKey()
//    {
//        return KeywordFolderDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return KeywordFolderDataObject.composeKey(getGuid());
    }

    public String getFolderPath()
    {
        return this.folderPath;
    }
    public void setFolderPath(String folderPath)
    {
        this.folderPath = folderPath;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("folderPath", this.folderPath);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        KeywordFolder thatObj = (KeywordFolder) obj;
        if( (this.folderPath == null && thatObj.getFolderPath() != null)
            || (this.folderPath != null && thatObj.getFolderPath() == null)
            || !this.folderPath.equals(thatObj.getFolderPath()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = folderPath == null ? 0 : folderPath.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
