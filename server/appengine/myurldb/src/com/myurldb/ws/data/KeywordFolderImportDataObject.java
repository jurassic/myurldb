package com.myurldb.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.myurldb.ws.KeywordFolderImport;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;


@PersistenceCapable(detachable="true")
@Inheritance(customStrategy = "complete-table")
public class KeywordFolderImportDataObject extends FolderImportBaseDataObject implements KeywordFolderImport
{
    private static final Logger log = Logger.getLogger(KeywordFolderImportDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(KeywordFolderImportDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(KeywordFolderImportDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String keywordFolder;

    public KeywordFolderImportDataObject()
    {
        this(null);
    }
    public KeywordFolderImportDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public KeywordFolderImportDataObject(String guid, String user, Integer precedence, String importType, String importedFolder, String importedFolderUser, String importedFolderTitle, String importedFolderPath, String status, String note, String keywordFolder)
    {
        this(guid, user, precedence, importType, importedFolder, importedFolderUser, importedFolderTitle, importedFolderPath, status, note, keywordFolder, null, null);
    }
    public KeywordFolderImportDataObject(String guid, String user, Integer precedence, String importType, String importedFolder, String importedFolderUser, String importedFolderTitle, String importedFolderPath, String status, String note, String keywordFolder, Long createdTime, Long modifiedTime)
    {
        super(guid, user, precedence, importType, importedFolder, importedFolderUser, importedFolderTitle, importedFolderPath, status, note, createdTime, modifiedTime);
        this.keywordFolder = keywordFolder;
    }

//    @Override
//    protected Key createKey()
//    {
//        return KeywordFolderImportDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return KeywordFolderImportDataObject.composeKey(getGuid());
    }

    public String getKeywordFolder()
    {
        return this.keywordFolder;
    }
    public void setKeywordFolder(String keywordFolder)
    {
        this.keywordFolder = keywordFolder;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("keywordFolder", this.keywordFolder);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        KeywordFolderImport thatObj = (KeywordFolderImport) obj;
        if( (this.keywordFolder == null && thatObj.getKeywordFolder() != null)
            || (this.keywordFolder != null && thatObj.getKeywordFolder() == null)
            || !this.keywordFolder.equals(thatObj.getKeywordFolder()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = keywordFolder == null ? 0 : keywordFolder.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
