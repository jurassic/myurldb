package com.myurldb.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.myurldb.ws.KeywordLink;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;


@PersistenceCapable(detachable="true")
@Inheritance(customStrategy = "complete-table")
public class KeywordLinkDataObject extends NavLinkBaseDataObject implements KeywordLink
{
    private static final Logger log = Logger.getLogger(KeywordLinkDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(KeywordLinkDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(KeywordLinkDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String keywordFolder;

    @Persistent(defaultFetchGroup = "true")
    private String folderPath;

    @Persistent(defaultFetchGroup = "true")
    private String keyword;

    @Persistent(defaultFetchGroup = "true")
    private String queryKey;

    @Persistent(defaultFetchGroup = "true")
    private String scope;

    @Persistent(defaultFetchGroup = "true")
    private Boolean dynamic;

    @Persistent(defaultFetchGroup = "true")
    private Boolean caseSensitive;

    public KeywordLinkDataObject()
    {
        this(null);
    }
    public KeywordLinkDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public KeywordLinkDataObject(String guid, String appClient, String clientRootDomain, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, Boolean internal, Boolean caching, String memo, String status, String note, Long expirationTime, String keywordFolder, String folderPath, String keyword, String queryKey, String scope, Boolean dynamic, Boolean caseSensitive)
    {
        this(guid, appClient, clientRootDomain, user, shortLink, domain, token, longUrl, shortUrl, internal, caching, memo, status, note, expirationTime, keywordFolder, folderPath, keyword, queryKey, scope, dynamic, caseSensitive, null, null);
    }
    public KeywordLinkDataObject(String guid, String appClient, String clientRootDomain, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, Boolean internal, Boolean caching, String memo, String status, String note, Long expirationTime, String keywordFolder, String folderPath, String keyword, String queryKey, String scope, Boolean dynamic, Boolean caseSensitive, Long createdTime, Long modifiedTime)
    {
        super(guid, appClient, clientRootDomain, user, shortLink, domain, token, longUrl, shortUrl, internal, caching, memo, status, note, expirationTime, createdTime, modifiedTime);
        this.keywordFolder = keywordFolder;
        this.folderPath = folderPath;
        this.keyword = keyword;
        this.queryKey = queryKey;
        this.scope = scope;
        this.dynamic = dynamic;
        this.caseSensitive = caseSensitive;
    }

//    @Override
//    protected Key createKey()
//    {
//        return KeywordLinkDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return KeywordLinkDataObject.composeKey(getGuid());
    }

    public String getKeywordFolder()
    {
        return this.keywordFolder;
    }
    public void setKeywordFolder(String keywordFolder)
    {
        this.keywordFolder = keywordFolder;
    }

    public String getFolderPath()
    {
        return this.folderPath;
    }
    public void setFolderPath(String folderPath)
    {
        this.folderPath = folderPath;
    }

    public String getKeyword()
    {
        return this.keyword;
    }
    public void setKeyword(String keyword)
    {
        this.keyword = keyword;
    }

    public String getQueryKey()
    {
        return this.queryKey;
    }
    public void setQueryKey(String queryKey)
    {
        this.queryKey = queryKey;
    }

    public String getScope()
    {
        return this.scope;
    }
    public void setScope(String scope)
    {
        this.scope = scope;
    }

    public Boolean isDynamic()
    {
        return this.dynamic;
    }
    public void setDynamic(Boolean dynamic)
    {
        this.dynamic = dynamic;
    }

    public Boolean isCaseSensitive()
    {
        return this.caseSensitive;
    }
    public void setCaseSensitive(Boolean caseSensitive)
    {
        this.caseSensitive = caseSensitive;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("keywordFolder", this.keywordFolder);
        dataMap.put("folderPath", this.folderPath);
        dataMap.put("keyword", this.keyword);
        dataMap.put("queryKey", this.queryKey);
        dataMap.put("scope", this.scope);
        dataMap.put("dynamic", this.dynamic);
        dataMap.put("caseSensitive", this.caseSensitive);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        KeywordLink thatObj = (KeywordLink) obj;
        if( (this.keywordFolder == null && thatObj.getKeywordFolder() != null)
            || (this.keywordFolder != null && thatObj.getKeywordFolder() == null)
            || !this.keywordFolder.equals(thatObj.getKeywordFolder()) ) {
            return false;
        }
        if( (this.folderPath == null && thatObj.getFolderPath() != null)
            || (this.folderPath != null && thatObj.getFolderPath() == null)
            || !this.folderPath.equals(thatObj.getFolderPath()) ) {
            return false;
        }
        if( (this.keyword == null && thatObj.getKeyword() != null)
            || (this.keyword != null && thatObj.getKeyword() == null)
            || !this.keyword.equals(thatObj.getKeyword()) ) {
            return false;
        }
        if( (this.queryKey == null && thatObj.getQueryKey() != null)
            || (this.queryKey != null && thatObj.getQueryKey() == null)
            || !this.queryKey.equals(thatObj.getQueryKey()) ) {
            return false;
        }
        if( (this.scope == null && thatObj.getScope() != null)
            || (this.scope != null && thatObj.getScope() == null)
            || !this.scope.equals(thatObj.getScope()) ) {
            return false;
        }
        if( (this.dynamic == null && thatObj.isDynamic() != null)
            || (this.dynamic != null && thatObj.isDynamic() == null)
            || !this.dynamic.equals(thatObj.isDynamic()) ) {
            return false;
        }
        if( (this.caseSensitive == null && thatObj.isCaseSensitive() != null)
            || (this.caseSensitive != null && thatObj.isCaseSensitive() == null)
            || !this.caseSensitive.equals(thatObj.isCaseSensitive()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = keywordFolder == null ? 0 : keywordFolder.hashCode();
        _hash = 31 * _hash + delta;
        delta = folderPath == null ? 0 : folderPath.hashCode();
        _hash = 31 * _hash + delta;
        delta = keyword == null ? 0 : keyword.hashCode();
        _hash = 31 * _hash + delta;
        delta = queryKey == null ? 0 : queryKey.hashCode();
        _hash = 31 * _hash + delta;
        delta = scope == null ? 0 : scope.hashCode();
        _hash = 31 * _hash + delta;
        delta = dynamic == null ? 0 : dynamic.hashCode();
        _hash = 31 * _hash + delta;
        delta = caseSensitive == null ? 0 : caseSensitive.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
