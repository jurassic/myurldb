package com.myurldb.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.myurldb.ws.LinkMessage;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;


@PersistenceCapable(detachable="true")
public class LinkMessageDataObject extends KeyedDataObject implements LinkMessage
{
    private static final Logger log = Logger.getLogger(LinkMessageDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(LinkMessageDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(LinkMessageDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String guid;

    @Persistent(defaultFetchGroup = "true")
    private String shortLink;

    @Persistent(defaultFetchGroup = "true")
    private String message;

    @Persistent(defaultFetchGroup = "true")
    private String password;

    public LinkMessageDataObject()
    {
        this(null);
    }
    public LinkMessageDataObject(String guid)
    {
        this(guid, null, null, null, null, null);
    }
    public LinkMessageDataObject(String guid, String shortLink, String message, String password)
    {
        this(guid, shortLink, message, password, null, null);
    }
    public LinkMessageDataObject(String guid, String shortLink, String message, String password, Long createdTime, Long modifiedTime)
    {
        super(createdTime, modifiedTime);
        generatePK(guid);
        this.shortLink = shortLink;
        this.message = message;
        this.password = password;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        generatePK(guid);
    }
    protected void generatePK(String guid)
    {
        if(guid == null) {
            this.guid = GUID.generate();  // temporary
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to a new guid = " + this.guid);
        } else {
            this.guid = guid;
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to guid = " + this.guid);
        }
        rebuildKey();        
    }

//    @Override
//    protected Key createKey()
//    {
//        return LinkMessageDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return LinkMessageDataObject.composeKey(getGuid());
    }

    public String getShortLink()
    {
        return this.shortLink;
    }
    public void setShortLink(String shortLink)
    {
        this.shortLink = shortLink;
    }

    public String getMessage()
    {
        return this.message;
    }
    public void setMessage(String message)
    {
        this.message = message;
    }

    public String getPassword()
    {
        return this.password;
    }
    public void setPassword(String password)
    {
        this.password = password;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("guid", this.guid);
        dataMap.put("shortLink", this.shortLink);
        dataMap.put("message", this.message);
        dataMap.put("password", this.password);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        LinkMessage thatObj = (LinkMessage) obj;
        if( (this.guid == null && thatObj.getGuid() != null)
            || (this.guid != null && thatObj.getGuid() == null)
            || !this.guid.equals(thatObj.getGuid()) ) {
            return false;
        }
        if( (this.shortLink == null && thatObj.getShortLink() != null)
            || (this.shortLink != null && thatObj.getShortLink() == null)
            || !this.shortLink.equals(thatObj.getShortLink()) ) {
            return false;
        }
        if( (this.message == null && thatObj.getMessage() != null)
            || (this.message != null && thatObj.getMessage() == null)
            || !this.message.equals(thatObj.getMessage()) ) {
            return false;
        }
        if( (this.password == null && thatObj.getPassword() != null)
            || (this.password != null && thatObj.getPassword() == null)
            || !this.password.equals(thatObj.getPassword()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = shortLink == null ? 0 : shortLink.hashCode();
        _hash = 31 * _hash + delta;
        delta = message == null ? 0 : message.hashCode();
        _hash = 31 * _hash + delta;
        delta = password == null ? 0 : password.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
