package com.myurldb.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.myurldb.ws.NavLinkBase;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;


@PersistenceCapable(detachable="true")
@Inheritance(strategy = InheritanceStrategy.SUBCLASS_TABLE)
public abstract class NavLinkBaseDataObject extends KeyedDataObject implements NavLinkBase
{
    private static final Logger log = Logger.getLogger(NavLinkBaseDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(NavLinkBaseDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(NavLinkBaseDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String guid;

    @Persistent(defaultFetchGroup = "true")
    private String appClient;

    @Persistent(defaultFetchGroup = "true")
    private String clientRootDomain;

    @Persistent(defaultFetchGroup = "true")
    private String user;

    @Persistent(defaultFetchGroup = "true")
    private String shortLink;

    @Persistent(defaultFetchGroup = "true")
    private String domain;

    @Persistent(defaultFetchGroup = "true")
    private String token;

    @Persistent(defaultFetchGroup = "true")
    private String longUrl;

    @Persistent(defaultFetchGroup = "true")
    private String shortUrl;

    @Persistent(defaultFetchGroup = "true")
    private Boolean internal;

    @Persistent(defaultFetchGroup = "true")
    private Boolean caching;

    @Persistent(defaultFetchGroup = "true")
    private Text memo;

    @Persistent(defaultFetchGroup = "true")
    private String status;

    @Persistent(defaultFetchGroup = "true")
    private String note;

    @Persistent(defaultFetchGroup = "true")
    private Long expirationTime;

    public NavLinkBaseDataObject()
    {
        this(null);
    }
    public NavLinkBaseDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public NavLinkBaseDataObject(String guid, String appClient, String clientRootDomain, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, Boolean internal, Boolean caching, String memo, String status, String note, Long expirationTime)
    {
        this(guid, appClient, clientRootDomain, user, shortLink, domain, token, longUrl, shortUrl, internal, caching, memo, status, note, expirationTime, null, null);
    }
    public NavLinkBaseDataObject(String guid, String appClient, String clientRootDomain, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, Boolean internal, Boolean caching, String memo, String status, String note, Long expirationTime, Long createdTime, Long modifiedTime)
    {
        super(createdTime, modifiedTime);
        generatePK(guid);
        this.appClient = appClient;
        this.clientRootDomain = clientRootDomain;
        this.user = user;
        this.shortLink = shortLink;
        this.domain = domain;
        this.token = token;
        this.longUrl = longUrl;
        this.shortUrl = shortUrl;
        this.internal = internal;
        this.caching = caching;
        setMemo(memo);
        this.status = status;
        this.note = note;
        this.expirationTime = expirationTime;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        generatePK(guid);
    }
    protected void generatePK(String guid)
    {
        if(guid == null) {
            this.guid = GUID.generate();  // temporary
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to a new guid = " + this.guid);
        } else {
            this.guid = guid;
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to guid = " + this.guid);
        }
        rebuildKey();        
    }

//    @Override
//    protected Key createKey()
//    {
//        return NavLinkBaseDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return NavLinkBaseDataObject.composeKey(getGuid());
    }

    public String getAppClient()
    {
        return this.appClient;
    }
    public void setAppClient(String appClient)
    {
        this.appClient = appClient;
    }

    public String getClientRootDomain()
    {
        return this.clientRootDomain;
    }
    public void setClientRootDomain(String clientRootDomain)
    {
        this.clientRootDomain = clientRootDomain;
    }

    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    public String getShortLink()
    {
        return this.shortLink;
    }
    public void setShortLink(String shortLink)
    {
        this.shortLink = shortLink;
    }

    public String getDomain()
    {
        return this.domain;
    }
    public void setDomain(String domain)
    {
        this.domain = domain;
    }

    public String getToken()
    {
        return this.token;
    }
    public void setToken(String token)
    {
        this.token = token;
    }

    public String getLongUrl()
    {
        return this.longUrl;
    }
    public void setLongUrl(String longUrl)
    {
        this.longUrl = longUrl;
    }

    public String getShortUrl()
    {
        return this.shortUrl;
    }
    public void setShortUrl(String shortUrl)
    {
        this.shortUrl = shortUrl;
    }

    public Boolean isInternal()
    {
        return this.internal;
    }
    public void setInternal(Boolean internal)
    {
        this.internal = internal;
    }

    public Boolean isCaching()
    {
        return this.caching;
    }
    public void setCaching(Boolean caching)
    {
        this.caching = caching;
    }

    public String getMemo()
    {
        if(this.memo == null) {
            return null;
        }    
        return this.memo.getValue();
    }
    public void setMemo(String memo)
    {
        if(memo == null) {
            this.memo = null;
        } else {
            this.memo = new Text(memo);
        }
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    public Long getExpirationTime()
    {
        return this.expirationTime;
    }
    public void setExpirationTime(Long expirationTime)
    {
        this.expirationTime = expirationTime;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("guid", this.guid);
        dataMap.put("appClient", this.appClient);
        dataMap.put("clientRootDomain", this.clientRootDomain);
        dataMap.put("user", this.user);
        dataMap.put("shortLink", this.shortLink);
        dataMap.put("domain", this.domain);
        dataMap.put("token", this.token);
        dataMap.put("longUrl", this.longUrl);
        dataMap.put("shortUrl", this.shortUrl);
        dataMap.put("internal", this.internal);
        dataMap.put("caching", this.caching);
        dataMap.put("memo", this.memo);
        dataMap.put("status", this.status);
        dataMap.put("note", this.note);
        dataMap.put("expirationTime", this.expirationTime);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        NavLinkBase thatObj = (NavLinkBase) obj;
        if( (this.guid == null && thatObj.getGuid() != null)
            || (this.guid != null && thatObj.getGuid() == null)
            || !this.guid.equals(thatObj.getGuid()) ) {
            return false;
        }
        if( (this.appClient == null && thatObj.getAppClient() != null)
            || (this.appClient != null && thatObj.getAppClient() == null)
            || !this.appClient.equals(thatObj.getAppClient()) ) {
            return false;
        }
        if( (this.clientRootDomain == null && thatObj.getClientRootDomain() != null)
            || (this.clientRootDomain != null && thatObj.getClientRootDomain() == null)
            || !this.clientRootDomain.equals(thatObj.getClientRootDomain()) ) {
            return false;
        }
        if( (this.user == null && thatObj.getUser() != null)
            || (this.user != null && thatObj.getUser() == null)
            || !this.user.equals(thatObj.getUser()) ) {
            return false;
        }
        if( (this.shortLink == null && thatObj.getShortLink() != null)
            || (this.shortLink != null && thatObj.getShortLink() == null)
            || !this.shortLink.equals(thatObj.getShortLink()) ) {
            return false;
        }
        if( (this.domain == null && thatObj.getDomain() != null)
            || (this.domain != null && thatObj.getDomain() == null)
            || !this.domain.equals(thatObj.getDomain()) ) {
            return false;
        }
        if( (this.token == null && thatObj.getToken() != null)
            || (this.token != null && thatObj.getToken() == null)
            || !this.token.equals(thatObj.getToken()) ) {
            return false;
        }
        if( (this.longUrl == null && thatObj.getLongUrl() != null)
            || (this.longUrl != null && thatObj.getLongUrl() == null)
            || !this.longUrl.equals(thatObj.getLongUrl()) ) {
            return false;
        }
        if( (this.shortUrl == null && thatObj.getShortUrl() != null)
            || (this.shortUrl != null && thatObj.getShortUrl() == null)
            || !this.shortUrl.equals(thatObj.getShortUrl()) ) {
            return false;
        }
        if( (this.internal == null && thatObj.isInternal() != null)
            || (this.internal != null && thatObj.isInternal() == null)
            || !this.internal.equals(thatObj.isInternal()) ) {
            return false;
        }
        if( (this.caching == null && thatObj.isCaching() != null)
            || (this.caching != null && thatObj.isCaching() == null)
            || !this.caching.equals(thatObj.isCaching()) ) {
            return false;
        }
        if( (this.memo == null && thatObj.getMemo() != null)
            || (this.memo != null && thatObj.getMemo() == null)
            || !this.memo.equals(thatObj.getMemo()) ) {
            return false;
        }
        if( (this.status == null && thatObj.getStatus() != null)
            || (this.status != null && thatObj.getStatus() == null)
            || !this.status.equals(thatObj.getStatus()) ) {
            return false;
        }
        if( (this.note == null && thatObj.getNote() != null)
            || (this.note != null && thatObj.getNote() == null)
            || !this.note.equals(thatObj.getNote()) ) {
            return false;
        }
        if( (this.expirationTime == null && thatObj.getExpirationTime() != null)
            || (this.expirationTime != null && thatObj.getExpirationTime() == null)
            || !this.expirationTime.equals(thatObj.getExpirationTime()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = appClient == null ? 0 : appClient.hashCode();
        _hash = 31 * _hash + delta;
        delta = clientRootDomain == null ? 0 : clientRootDomain.hashCode();
        _hash = 31 * _hash + delta;
        delta = user == null ? 0 : user.hashCode();
        _hash = 31 * _hash + delta;
        delta = shortLink == null ? 0 : shortLink.hashCode();
        _hash = 31 * _hash + delta;
        delta = domain == null ? 0 : domain.hashCode();
        _hash = 31 * _hash + delta;
        delta = token == null ? 0 : token.hashCode();
        _hash = 31 * _hash + delta;
        delta = longUrl == null ? 0 : longUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = shortUrl == null ? 0 : shortUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = internal == null ? 0 : internal.hashCode();
        _hash = 31 * _hash + delta;
        delta = caching == null ? 0 : caching.hashCode();
        _hash = 31 * _hash + delta;
        delta = memo == null ? 0 : memo.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        delta = expirationTime == null ? 0 : expirationTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
