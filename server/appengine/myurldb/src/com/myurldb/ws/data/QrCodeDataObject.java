package com.myurldb.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.myurldb.ws.QrCode;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;


@PersistenceCapable(detachable="true")
public class QrCodeDataObject extends KeyedDataObject implements QrCode
{
    private static final Logger log = Logger.getLogger(QrCodeDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(QrCodeDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(QrCodeDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String guid;

    @Persistent(defaultFetchGroup = "true")
    private String shortLink;

    @Persistent(defaultFetchGroup = "true")
    private String imageLink;

    @Persistent(defaultFetchGroup = "true")
    private String imageUrl;

    @Persistent(defaultFetchGroup = "true")
    private String type;

    @Persistent(defaultFetchGroup = "true")
    private String status;

    public QrCodeDataObject()
    {
        this(null);
    }
    public QrCodeDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null);
    }
    public QrCodeDataObject(String guid, String shortLink, String imageLink, String imageUrl, String type, String status)
    {
        this(guid, shortLink, imageLink, imageUrl, type, status, null, null);
    }
    public QrCodeDataObject(String guid, String shortLink, String imageLink, String imageUrl, String type, String status, Long createdTime, Long modifiedTime)
    {
        super(createdTime, modifiedTime);
        generatePK(guid);
        this.shortLink = shortLink;
        this.imageLink = imageLink;
        this.imageUrl = imageUrl;
        this.type = type;
        this.status = status;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        generatePK(guid);
    }
    protected void generatePK(String guid)
    {
        if(guid == null) {
            this.guid = GUID.generate();  // temporary
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to a new guid = " + this.guid);
        } else {
            this.guid = guid;
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to guid = " + this.guid);
        }
        rebuildKey();        
    }

//    @Override
//    protected Key createKey()
//    {
//        return QrCodeDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return QrCodeDataObject.composeKey(getGuid());
    }

    public String getShortLink()
    {
        return this.shortLink;
    }
    public void setShortLink(String shortLink)
    {
        this.shortLink = shortLink;
    }

    public String getImageLink()
    {
        return this.imageLink;
    }
    public void setImageLink(String imageLink)
    {
        this.imageLink = imageLink;
    }

    public String getImageUrl()
    {
        return this.imageUrl;
    }
    public void setImageUrl(String imageUrl)
    {
        this.imageUrl = imageUrl;
    }

    public String getType()
    {
        return this.type;
    }
    public void setType(String type)
    {
        this.type = type;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("guid", this.guid);
        dataMap.put("shortLink", this.shortLink);
        dataMap.put("imageLink", this.imageLink);
        dataMap.put("imageUrl", this.imageUrl);
        dataMap.put("type", this.type);
        dataMap.put("status", this.status);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        QrCode thatObj = (QrCode) obj;
        if( (this.guid == null && thatObj.getGuid() != null)
            || (this.guid != null && thatObj.getGuid() == null)
            || !this.guid.equals(thatObj.getGuid()) ) {
            return false;
        }
        if( (this.shortLink == null && thatObj.getShortLink() != null)
            || (this.shortLink != null && thatObj.getShortLink() == null)
            || !this.shortLink.equals(thatObj.getShortLink()) ) {
            return false;
        }
        if( (this.imageLink == null && thatObj.getImageLink() != null)
            || (this.imageLink != null && thatObj.getImageLink() == null)
            || !this.imageLink.equals(thatObj.getImageLink()) ) {
            return false;
        }
        if( (this.imageUrl == null && thatObj.getImageUrl() != null)
            || (this.imageUrl != null && thatObj.getImageUrl() == null)
            || !this.imageUrl.equals(thatObj.getImageUrl()) ) {
            return false;
        }
        if( (this.type == null && thatObj.getType() != null)
            || (this.type != null && thatObj.getType() == null)
            || !this.type.equals(thatObj.getType()) ) {
            return false;
        }
        if( (this.status == null && thatObj.getStatus() != null)
            || (this.status != null && thatObj.getStatus() == null)
            || !this.status.equals(thatObj.getStatus()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = shortLink == null ? 0 : shortLink.hashCode();
        _hash = 31 * _hash + delta;
        delta = imageLink == null ? 0 : imageLink.hashCode();
        _hash = 31 * _hash + delta;
        delta = imageUrl == null ? 0 : imageUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = type == null ? 0 : type.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
