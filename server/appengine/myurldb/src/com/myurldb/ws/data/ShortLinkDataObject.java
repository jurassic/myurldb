package com.myurldb.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.myurldb.ws.ReferrerInfoStruct;
import com.myurldb.ws.ShortLink;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;


@PersistenceCapable(detachable="true")
public class ShortLinkDataObject extends KeyedDataObject implements ShortLink
{
    private static final Logger log = Logger.getLogger(ShortLinkDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(ShortLinkDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(ShortLinkDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String guid;

    @Persistent(defaultFetchGroup = "true")
    private String appClient;

    @Persistent(defaultFetchGroup = "true")
    private String clientRootDomain;

    @Persistent(defaultFetchGroup = "true")
    private String owner;

    @Persistent(defaultFetchGroup = "true")
    private String longUrlDomain;

    @Persistent(defaultFetchGroup = "true")
    private String longUrl;

    @Persistent(defaultFetchGroup = "true")
    private Text longUrlFull;

    @Persistent(defaultFetchGroup = "true")
    private String longUrlHash;

    @Persistent(defaultFetchGroup = "true")
    private Boolean reuseExistingShortUrl;

    @Persistent(defaultFetchGroup = "true")
    private Boolean failIfShortUrlExists;

    @Persistent(defaultFetchGroup = "true")
    private String domain;

    @Persistent(defaultFetchGroup = "true")
    private String domainType;

    @Persistent(defaultFetchGroup = "true")
    private String usercode;

    @Persistent(defaultFetchGroup = "true")
    private String username;

    @Persistent(defaultFetchGroup = "true")
    private String tokenPrefix;

    @Persistent(defaultFetchGroup = "true")
    private String token;

    @Persistent(defaultFetchGroup = "true")
    private String tokenType;

    @Persistent(defaultFetchGroup = "true")
    private String sassyTokenType;

    @Persistent(defaultFetchGroup = "true")
    private String shortUrl;

    @Persistent(defaultFetchGroup = "true")
    private String shortPassage;

    @Persistent(defaultFetchGroup = "true")
    private String redirectType;

    @Persistent(defaultFetchGroup = "true")
    private Long flashDuration;

    @Persistent(defaultFetchGroup = "true")
    private String accessType;

    @Persistent(defaultFetchGroup = "true")
    private String viewType;

    @Persistent(defaultFetchGroup = "true")
    private String shareType;

    @Persistent(defaultFetchGroup = "true")
    private Boolean readOnly;

    @Persistent(defaultFetchGroup = "true")
    private List<String> displayMessage;

    @Persistent(defaultFetchGroup = "true")
    private String shortMessage;

    @Persistent(defaultFetchGroup = "true")
    private Boolean keywordEnabled;

    @Persistent(defaultFetchGroup = "true")
    private Boolean bookmarkEnabled;

    @Persistent(defaultFetchGroup = "false")
    @Embedded(members = {
        @Persistent(name="referer", columns=@Column(name="referrerInforeferer")),
        @Persistent(name="userAgent", columns=@Column(name="referrerInfouserAgent")),
        @Persistent(name="language", columns=@Column(name="referrerInfolanguage")),
        @Persistent(name="hostname", columns=@Column(name="referrerInfohostname")),
        @Persistent(name="ipAddress", columns=@Column(name="referrerInfoipAddress")),
        @Persistent(name="note", columns=@Column(name="referrerInfonote")),
    })
    private ReferrerInfoStructDataObject referrerInfo;

    @Persistent(defaultFetchGroup = "true")
    private String status;

    @Persistent(defaultFetchGroup = "true")
    private String note;

    @Persistent(defaultFetchGroup = "true")
    private Long expirationTime;

    public ShortLinkDataObject()
    {
        this(null);
    }
    public ShortLinkDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public ShortLinkDataObject(String guid, String appClient, String clientRootDomain, String owner, String longUrlDomain, String longUrl, String longUrlFull, String longUrlHash, Boolean reuseExistingShortUrl, Boolean failIfShortUrlExists, String domain, String domainType, String usercode, String username, String tokenPrefix, String token, String tokenType, String sassyTokenType, String shortUrl, String shortPassage, String redirectType, Long flashDuration, String accessType, String viewType, String shareType, Boolean readOnly, String displayMessage, String shortMessage, Boolean keywordEnabled, Boolean bookmarkEnabled, ReferrerInfoStruct referrerInfo, String status, String note, Long expirationTime)
    {
        this(guid, appClient, clientRootDomain, owner, longUrlDomain, longUrl, longUrlFull, longUrlHash, reuseExistingShortUrl, failIfShortUrlExists, domain, domainType, usercode, username, tokenPrefix, token, tokenType, sassyTokenType, shortUrl, shortPassage, redirectType, flashDuration, accessType, viewType, shareType, readOnly, displayMessage, shortMessage, keywordEnabled, bookmarkEnabled, referrerInfo, status, note, expirationTime, null, null);
    }
    public ShortLinkDataObject(String guid, String appClient, String clientRootDomain, String owner, String longUrlDomain, String longUrl, String longUrlFull, String longUrlHash, Boolean reuseExistingShortUrl, Boolean failIfShortUrlExists, String domain, String domainType, String usercode, String username, String tokenPrefix, String token, String tokenType, String sassyTokenType, String shortUrl, String shortPassage, String redirectType, Long flashDuration, String accessType, String viewType, String shareType, Boolean readOnly, String displayMessage, String shortMessage, Boolean keywordEnabled, Boolean bookmarkEnabled, ReferrerInfoStruct referrerInfo, String status, String note, Long expirationTime, Long createdTime, Long modifiedTime)
    {
        super(createdTime, modifiedTime);
        generatePK(guid);
        this.appClient = appClient;
        this.clientRootDomain = clientRootDomain;
        this.owner = owner;
        this.longUrlDomain = longUrlDomain;
        this.longUrl = longUrl;
        setLongUrlFull(longUrlFull);
        this.longUrlHash = longUrlHash;
        this.reuseExistingShortUrl = reuseExistingShortUrl;
        this.failIfShortUrlExists = failIfShortUrlExists;
        this.domain = domain;
        this.domainType = domainType;
        this.usercode = usercode;
        this.username = username;
        this.tokenPrefix = tokenPrefix;
        this.token = token;
        this.tokenType = tokenType;
        this.sassyTokenType = sassyTokenType;
        this.shortUrl = shortUrl;
        this.shortPassage = shortPassage;
        this.redirectType = redirectType;
        this.flashDuration = flashDuration;
        this.accessType = accessType;
        this.viewType = viewType;
        this.shareType = shareType;
        this.readOnly = readOnly;
        setDisplayMessage(displayMessage);
        this.shortMessage = shortMessage;
        this.keywordEnabled = keywordEnabled;
        this.bookmarkEnabled = bookmarkEnabled;
        if(referrerInfo != null) {
            this.referrerInfo = new ReferrerInfoStructDataObject(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            this.referrerInfo = null;
        }
        this.status = status;
        this.note = note;
        this.expirationTime = expirationTime;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        generatePK(guid);
    }
    protected void generatePK(String guid)
    {
        if(guid == null) {
            this.guid = GUID.generate();  // temporary
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to a new guid = " + this.guid);
        } else {
            this.guid = guid;
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to guid = " + this.guid);
        }
        rebuildKey();        
    }

//    @Override
//    protected Key createKey()
//    {
//        return ShortLinkDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return ShortLinkDataObject.composeKey(getGuid());
    }

    public String getAppClient()
    {
        return this.appClient;
    }
    public void setAppClient(String appClient)
    {
        this.appClient = appClient;
    }

    public String getClientRootDomain()
    {
        return this.clientRootDomain;
    }
    public void setClientRootDomain(String clientRootDomain)
    {
        this.clientRootDomain = clientRootDomain;
    }

    public String getOwner()
    {
        return this.owner;
    }
    public void setOwner(String owner)
    {
        this.owner = owner;
    }

    public String getLongUrlDomain()
    {
        return this.longUrlDomain;
    }
    public void setLongUrlDomain(String longUrlDomain)
    {
        this.longUrlDomain = longUrlDomain;
    }

    public String getLongUrl()
    {
        return this.longUrl;
    }
    public void setLongUrl(String longUrl)
    {
        this.longUrl = longUrl;
    }

    public String getLongUrlFull()
    {
        if(this.longUrlFull == null) {
            return null;
        }    
        return this.longUrlFull.getValue();
    }
    public void setLongUrlFull(String longUrlFull)
    {
        if(longUrlFull == null) {
            this.longUrlFull = null;
        } else {
            this.longUrlFull = new Text(longUrlFull);
        }
    }

    public String getLongUrlHash()
    {
        return this.longUrlHash;
    }
    public void setLongUrlHash(String longUrlHash)
    {
        this.longUrlHash = longUrlHash;
    }

    public Boolean isReuseExistingShortUrl()
    {
        return this.reuseExistingShortUrl;
    }
    public void setReuseExistingShortUrl(Boolean reuseExistingShortUrl)
    {
        this.reuseExistingShortUrl = reuseExistingShortUrl;
    }

    public Boolean isFailIfShortUrlExists()
    {
        return this.failIfShortUrlExists;
    }
    public void setFailIfShortUrlExists(Boolean failIfShortUrlExists)
    {
        this.failIfShortUrlExists = failIfShortUrlExists;
    }

    public String getDomain()
    {
        return this.domain;
    }
    public void setDomain(String domain)
    {
        this.domain = domain;
    }

    public String getDomainType()
    {
        return this.domainType;
    }
    public void setDomainType(String domainType)
    {
        this.domainType = domainType;
    }

    public String getUsercode()
    {
        return this.usercode;
    }
    public void setUsercode(String usercode)
    {
        this.usercode = usercode;
    }

    public String getUsername()
    {
        return this.username;
    }
    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getTokenPrefix()
    {
        return this.tokenPrefix;
    }
    public void setTokenPrefix(String tokenPrefix)
    {
        this.tokenPrefix = tokenPrefix;
    }

    public String getToken()
    {
        return this.token;
    }
    public void setToken(String token)
    {
        this.token = token;
    }

    public String getTokenType()
    {
        return this.tokenType;
    }
    public void setTokenType(String tokenType)
    {
        this.tokenType = tokenType;
    }

    public String getSassyTokenType()
    {
        return this.sassyTokenType;
    }
    public void setSassyTokenType(String sassyTokenType)
    {
        this.sassyTokenType = sassyTokenType;
    }

    public String getShortUrl()
    {
        return this.shortUrl;
    }
    public void setShortUrl(String shortUrl)
    {
        this.shortUrl = shortUrl;
    }

    public String getShortPassage()
    {
        return this.shortPassage;
    }
    public void setShortPassage(String shortPassage)
    {
        this.shortPassage = shortPassage;
    }

    public String getRedirectType()
    {
        return this.redirectType;
    }
    public void setRedirectType(String redirectType)
    {
        this.redirectType = redirectType;
    }

    public Long getFlashDuration()
    {
        return this.flashDuration;
    }
    public void setFlashDuration(Long flashDuration)
    {
        this.flashDuration = flashDuration;
    }

    public String getAccessType()
    {
        return this.accessType;
    }
    public void setAccessType(String accessType)
    {
        this.accessType = accessType;
    }

    public String getViewType()
    {
        return this.viewType;
    }
    public void setViewType(String viewType)
    {
        this.viewType = viewType;
    }

    public String getShareType()
    {
        return this.shareType;
    }
    public void setShareType(String shareType)
    {
        this.shareType = shareType;
    }

    public Boolean isReadOnly()
    {
        return this.readOnly;
    }
    public void setReadOnly(Boolean readOnly)
    {
        this.readOnly = readOnly;
    }

    public String getDisplayMessage()
    {
        if(this.displayMessage == null) {
            return null;
        }
        StringBuffer sb = new StringBuffer();
        for(String v : this.displayMessage) {
            sb.append(v);
        }
        return sb.toString(); 
    }
    public void setDisplayMessage(String displayMessage)
    {
        if(displayMessage == null) {
            this.displayMessage = null;
        } else {
            List<String> _list = new ArrayList<String>();
            int _beg = 0;
            int _rem = displayMessage.length();
            while(_rem > 0) {
                int _sz;
                if((_rem - 500) >= 0) {
                    _sz = 500;
                    _rem -= 500;
                } else {
                    _sz = _rem;
                    _rem = 0;
                }
                _list.add(displayMessage.substring(_beg, _beg+_sz));
                _beg += _sz;
            }
            this.displayMessage = _list;
        }
    }

    public String getShortMessage()
    {
        return this.shortMessage;
    }
    public void setShortMessage(String shortMessage)
    {
        this.shortMessage = shortMessage;
    }

    public Boolean isKeywordEnabled()
    {
        return this.keywordEnabled;
    }
    public void setKeywordEnabled(Boolean keywordEnabled)
    {
        this.keywordEnabled = keywordEnabled;
    }

    public Boolean isBookmarkEnabled()
    {
        return this.bookmarkEnabled;
    }
    public void setBookmarkEnabled(Boolean bookmarkEnabled)
    {
        this.bookmarkEnabled = bookmarkEnabled;
    }

    public ReferrerInfoStruct getReferrerInfo()
    {
        return this.referrerInfo;
    }
    public void setReferrerInfo(ReferrerInfoStruct referrerInfo)
    {
        if(referrerInfo == null) {
            this.referrerInfo = null;
            log.log(Level.INFO, "ShortLinkDataObject.setReferrerInfo(ReferrerInfoStruct referrerInfo): Arg referrerInfo is null.");            
        } else if(referrerInfo instanceof ReferrerInfoStructDataObject) {
            this.referrerInfo = (ReferrerInfoStructDataObject) referrerInfo;
        } else if(referrerInfo instanceof ReferrerInfoStruct) {
            this.referrerInfo = new ReferrerInfoStructDataObject(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            this.referrerInfo = new ReferrerInfoStructDataObject();   // ????
            log.log(Level.WARNING, "ShortLinkDataObject.setReferrerInfo(ReferrerInfoStruct referrerInfo): Arg referrerInfo is of an invalid type.");
        }
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    public Long getExpirationTime()
    {
        return this.expirationTime;
    }
    public void setExpirationTime(Long expirationTime)
    {
        this.expirationTime = expirationTime;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("guid", this.guid);
        dataMap.put("appClient", this.appClient);
        dataMap.put("clientRootDomain", this.clientRootDomain);
        dataMap.put("owner", this.owner);
        dataMap.put("longUrlDomain", this.longUrlDomain);
        dataMap.put("longUrl", this.longUrl);
        dataMap.put("longUrlFull", this.longUrlFull);
        dataMap.put("longUrlHash", this.longUrlHash);
        dataMap.put("reuseExistingShortUrl", this.reuseExistingShortUrl);
        dataMap.put("failIfShortUrlExists", this.failIfShortUrlExists);
        dataMap.put("domain", this.domain);
        dataMap.put("domainType", this.domainType);
        dataMap.put("usercode", this.usercode);
        dataMap.put("username", this.username);
        dataMap.put("tokenPrefix", this.tokenPrefix);
        dataMap.put("token", this.token);
        dataMap.put("tokenType", this.tokenType);
        dataMap.put("sassyTokenType", this.sassyTokenType);
        dataMap.put("shortUrl", this.shortUrl);
        dataMap.put("shortPassage", this.shortPassage);
        dataMap.put("redirectType", this.redirectType);
        dataMap.put("flashDuration", this.flashDuration);
        dataMap.put("accessType", this.accessType);
        dataMap.put("viewType", this.viewType);
        dataMap.put("shareType", this.shareType);
        dataMap.put("readOnly", this.readOnly);
        dataMap.put("displayMessage", this.displayMessage);
        dataMap.put("shortMessage", this.shortMessage);
        dataMap.put("keywordEnabled", this.keywordEnabled);
        dataMap.put("bookmarkEnabled", this.bookmarkEnabled);
        dataMap.put("referrerInfo", this.referrerInfo);
        dataMap.put("status", this.status);
        dataMap.put("note", this.note);
        dataMap.put("expirationTime", this.expirationTime);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        ShortLink thatObj = (ShortLink) obj;
        if( (this.guid == null && thatObj.getGuid() != null)
            || (this.guid != null && thatObj.getGuid() == null)
            || !this.guid.equals(thatObj.getGuid()) ) {
            return false;
        }
        if( (this.appClient == null && thatObj.getAppClient() != null)
            || (this.appClient != null && thatObj.getAppClient() == null)
            || !this.appClient.equals(thatObj.getAppClient()) ) {
            return false;
        }
        if( (this.clientRootDomain == null && thatObj.getClientRootDomain() != null)
            || (this.clientRootDomain != null && thatObj.getClientRootDomain() == null)
            || !this.clientRootDomain.equals(thatObj.getClientRootDomain()) ) {
            return false;
        }
        if( (this.owner == null && thatObj.getOwner() != null)
            || (this.owner != null && thatObj.getOwner() == null)
            || !this.owner.equals(thatObj.getOwner()) ) {
            return false;
        }
        if( (this.longUrlDomain == null && thatObj.getLongUrlDomain() != null)
            || (this.longUrlDomain != null && thatObj.getLongUrlDomain() == null)
            || !this.longUrlDomain.equals(thatObj.getLongUrlDomain()) ) {
            return false;
        }
        if( (this.longUrl == null && thatObj.getLongUrl() != null)
            || (this.longUrl != null && thatObj.getLongUrl() == null)
            || !this.longUrl.equals(thatObj.getLongUrl()) ) {
            return false;
        }
        if( (this.longUrlFull == null && thatObj.getLongUrlFull() != null)
            || (this.longUrlFull != null && thatObj.getLongUrlFull() == null)
            || !this.longUrlFull.equals(thatObj.getLongUrlFull()) ) {
            return false;
        }
        if( (this.longUrlHash == null && thatObj.getLongUrlHash() != null)
            || (this.longUrlHash != null && thatObj.getLongUrlHash() == null)
            || !this.longUrlHash.equals(thatObj.getLongUrlHash()) ) {
            return false;
        }
        if( (this.reuseExistingShortUrl == null && thatObj.isReuseExistingShortUrl() != null)
            || (this.reuseExistingShortUrl != null && thatObj.isReuseExistingShortUrl() == null)
            || !this.reuseExistingShortUrl.equals(thatObj.isReuseExistingShortUrl()) ) {
            return false;
        }
        if( (this.failIfShortUrlExists == null && thatObj.isFailIfShortUrlExists() != null)
            || (this.failIfShortUrlExists != null && thatObj.isFailIfShortUrlExists() == null)
            || !this.failIfShortUrlExists.equals(thatObj.isFailIfShortUrlExists()) ) {
            return false;
        }
        if( (this.domain == null && thatObj.getDomain() != null)
            || (this.domain != null && thatObj.getDomain() == null)
            || !this.domain.equals(thatObj.getDomain()) ) {
            return false;
        }
        if( (this.domainType == null && thatObj.getDomainType() != null)
            || (this.domainType != null && thatObj.getDomainType() == null)
            || !this.domainType.equals(thatObj.getDomainType()) ) {
            return false;
        }
        if( (this.usercode == null && thatObj.getUsercode() != null)
            || (this.usercode != null && thatObj.getUsercode() == null)
            || !this.usercode.equals(thatObj.getUsercode()) ) {
            return false;
        }
        if( (this.username == null && thatObj.getUsername() != null)
            || (this.username != null && thatObj.getUsername() == null)
            || !this.username.equals(thatObj.getUsername()) ) {
            return false;
        }
        if( (this.tokenPrefix == null && thatObj.getTokenPrefix() != null)
            || (this.tokenPrefix != null && thatObj.getTokenPrefix() == null)
            || !this.tokenPrefix.equals(thatObj.getTokenPrefix()) ) {
            return false;
        }
        if( (this.token == null && thatObj.getToken() != null)
            || (this.token != null && thatObj.getToken() == null)
            || !this.token.equals(thatObj.getToken()) ) {
            return false;
        }
        if( (this.tokenType == null && thatObj.getTokenType() != null)
            || (this.tokenType != null && thatObj.getTokenType() == null)
            || !this.tokenType.equals(thatObj.getTokenType()) ) {
            return false;
        }
        if( (this.sassyTokenType == null && thatObj.getSassyTokenType() != null)
            || (this.sassyTokenType != null && thatObj.getSassyTokenType() == null)
            || !this.sassyTokenType.equals(thatObj.getSassyTokenType()) ) {
            return false;
        }
        if( (this.shortUrl == null && thatObj.getShortUrl() != null)
            || (this.shortUrl != null && thatObj.getShortUrl() == null)
            || !this.shortUrl.equals(thatObj.getShortUrl()) ) {
            return false;
        }
        if( (this.shortPassage == null && thatObj.getShortPassage() != null)
            || (this.shortPassage != null && thatObj.getShortPassage() == null)
            || !this.shortPassage.equals(thatObj.getShortPassage()) ) {
            return false;
        }
        if( (this.redirectType == null && thatObj.getRedirectType() != null)
            || (this.redirectType != null && thatObj.getRedirectType() == null)
            || !this.redirectType.equals(thatObj.getRedirectType()) ) {
            return false;
        }
        if( (this.flashDuration == null && thatObj.getFlashDuration() != null)
            || (this.flashDuration != null && thatObj.getFlashDuration() == null)
            || !this.flashDuration.equals(thatObj.getFlashDuration()) ) {
            return false;
        }
        if( (this.accessType == null && thatObj.getAccessType() != null)
            || (this.accessType != null && thatObj.getAccessType() == null)
            || !this.accessType.equals(thatObj.getAccessType()) ) {
            return false;
        }
        if( (this.viewType == null && thatObj.getViewType() != null)
            || (this.viewType != null && thatObj.getViewType() == null)
            || !this.viewType.equals(thatObj.getViewType()) ) {
            return false;
        }
        if( (this.shareType == null && thatObj.getShareType() != null)
            || (this.shareType != null && thatObj.getShareType() == null)
            || !this.shareType.equals(thatObj.getShareType()) ) {
            return false;
        }
        if( (this.readOnly == null && thatObj.isReadOnly() != null)
            || (this.readOnly != null && thatObj.isReadOnly() == null)
            || !this.readOnly.equals(thatObj.isReadOnly()) ) {
            return false;
        }
        if( (this.displayMessage == null && thatObj.getDisplayMessage() != null)
            || (this.displayMessage != null && thatObj.getDisplayMessage() == null)
            || !this.displayMessage.equals(thatObj.getDisplayMessage()) ) {
            return false;
        }
        if( (this.shortMessage == null && thatObj.getShortMessage() != null)
            || (this.shortMessage != null && thatObj.getShortMessage() == null)
            || !this.shortMessage.equals(thatObj.getShortMessage()) ) {
            return false;
        }
        if( (this.keywordEnabled == null && thatObj.isKeywordEnabled() != null)
            || (this.keywordEnabled != null && thatObj.isKeywordEnabled() == null)
            || !this.keywordEnabled.equals(thatObj.isKeywordEnabled()) ) {
            return false;
        }
        if( (this.bookmarkEnabled == null && thatObj.isBookmarkEnabled() != null)
            || (this.bookmarkEnabled != null && thatObj.isBookmarkEnabled() == null)
            || !this.bookmarkEnabled.equals(thatObj.isBookmarkEnabled()) ) {
            return false;
        }
        if( (this.referrerInfo == null && thatObj.getReferrerInfo() != null)
            || (this.referrerInfo != null && thatObj.getReferrerInfo() == null)
            || !this.referrerInfo.equals(thatObj.getReferrerInfo()) ) {
            return false;
        }
        if( (this.status == null && thatObj.getStatus() != null)
            || (this.status != null && thatObj.getStatus() == null)
            || !this.status.equals(thatObj.getStatus()) ) {
            return false;
        }
        if( (this.note == null && thatObj.getNote() != null)
            || (this.note != null && thatObj.getNote() == null)
            || !this.note.equals(thatObj.getNote()) ) {
            return false;
        }
        if( (this.expirationTime == null && thatObj.getExpirationTime() != null)
            || (this.expirationTime != null && thatObj.getExpirationTime() == null)
            || !this.expirationTime.equals(thatObj.getExpirationTime()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = appClient == null ? 0 : appClient.hashCode();
        _hash = 31 * _hash + delta;
        delta = clientRootDomain == null ? 0 : clientRootDomain.hashCode();
        _hash = 31 * _hash + delta;
        delta = owner == null ? 0 : owner.hashCode();
        _hash = 31 * _hash + delta;
        delta = longUrlDomain == null ? 0 : longUrlDomain.hashCode();
        _hash = 31 * _hash + delta;
        delta = longUrl == null ? 0 : longUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = longUrlFull == null ? 0 : longUrlFull.hashCode();
        _hash = 31 * _hash + delta;
        delta = longUrlHash == null ? 0 : longUrlHash.hashCode();
        _hash = 31 * _hash + delta;
        delta = reuseExistingShortUrl == null ? 0 : reuseExistingShortUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = failIfShortUrlExists == null ? 0 : failIfShortUrlExists.hashCode();
        _hash = 31 * _hash + delta;
        delta = domain == null ? 0 : domain.hashCode();
        _hash = 31 * _hash + delta;
        delta = domainType == null ? 0 : domainType.hashCode();
        _hash = 31 * _hash + delta;
        delta = usercode == null ? 0 : usercode.hashCode();
        _hash = 31 * _hash + delta;
        delta = username == null ? 0 : username.hashCode();
        _hash = 31 * _hash + delta;
        delta = tokenPrefix == null ? 0 : tokenPrefix.hashCode();
        _hash = 31 * _hash + delta;
        delta = token == null ? 0 : token.hashCode();
        _hash = 31 * _hash + delta;
        delta = tokenType == null ? 0 : tokenType.hashCode();
        _hash = 31 * _hash + delta;
        delta = sassyTokenType == null ? 0 : sassyTokenType.hashCode();
        _hash = 31 * _hash + delta;
        delta = shortUrl == null ? 0 : shortUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = shortPassage == null ? 0 : shortPassage.hashCode();
        _hash = 31 * _hash + delta;
        delta = redirectType == null ? 0 : redirectType.hashCode();
        _hash = 31 * _hash + delta;
        delta = flashDuration == null ? 0 : flashDuration.hashCode();
        _hash = 31 * _hash + delta;
        delta = accessType == null ? 0 : accessType.hashCode();
        _hash = 31 * _hash + delta;
        delta = viewType == null ? 0 : viewType.hashCode();
        _hash = 31 * _hash + delta;
        delta = shareType == null ? 0 : shareType.hashCode();
        _hash = 31 * _hash + delta;
        delta = readOnly == null ? 0 : readOnly.hashCode();
        _hash = 31 * _hash + delta;
        delta = displayMessage == null ? 0 : displayMessage.hashCode();
        _hash = 31 * _hash + delta;
        delta = shortMessage == null ? 0 : shortMessage.hashCode();
        _hash = 31 * _hash + delta;
        delta = keywordEnabled == null ? 0 : keywordEnabled.hashCode();
        _hash = 31 * _hash + delta;
        delta = bookmarkEnabled == null ? 0 : bookmarkEnabled.hashCode();
        _hash = 31 * _hash + delta;
        delta = referrerInfo == null ? 0 : referrerInfo.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        delta = expirationTime == null ? 0 : expirationTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
