package com.myurldb.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.Extension;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PrimaryKey;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.myurldb.ws.ShortPassageAttribute;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;


@PersistenceCapable(detachable="true")
// @EmbeddedOnly
public class ShortPassageAttributeDataObject implements ShortPassageAttribute, Serializable
{
    private static final Logger log = Logger.getLogger(ShortPassageAttributeDataObject.class.getName());

    @PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    private Long _shortpassageattribute_auto_id;         // Note: Object with long PK cannot be a parent...

    @Persistent(defaultFetchGroup = "true")
    private String domain;

    @Persistent(defaultFetchGroup = "true")
    private String tokenType;

    @Persistent(defaultFetchGroup = "true")
    private String displayMessage;

    @Persistent(defaultFetchGroup = "true")
    private String redirectType;

    @Persistent(defaultFetchGroup = "true")
    private Long flashDuration;

    @Persistent(defaultFetchGroup = "true")
    private String accessType;

    @Persistent(defaultFetchGroup = "true")
    private String viewType;

    @Persistent(defaultFetchGroup = "true")
    private String shareType;

    public ShortPassageAttributeDataObject()
    {
        // ???
        // this(null, null, null, null, null, null, null, null);
    }
    public ShortPassageAttributeDataObject(String domain, String tokenType, String displayMessage, String redirectType, Long flashDuration, String accessType, String viewType, String shareType)
    {
        setDomain(domain);
        setTokenType(tokenType);
        setDisplayMessage(displayMessage);
        setRedirectType(redirectType);
        setFlashDuration(flashDuration);
        setAccessType(accessType);
        setViewType(viewType);
        setShareType(shareType);
    }

    private void resetEncodedKey()
    {
    }

    public String getDomain()
    {
        return this.domain;
    }
    public void setDomain(String domain)
    {
        this.domain = domain;
        if(this.domain != null) {
            resetEncodedKey();
        }
    }

    public String getTokenType()
    {
        return this.tokenType;
    }
    public void setTokenType(String tokenType)
    {
        this.tokenType = tokenType;
        if(this.tokenType != null) {
            resetEncodedKey();
        }
    }

    public String getDisplayMessage()
    {
        return this.displayMessage;
    }
    public void setDisplayMessage(String displayMessage)
    {
        this.displayMessage = displayMessage;
        if(this.displayMessage != null) {
            resetEncodedKey();
        }
    }

    public String getRedirectType()
    {
        return this.redirectType;
    }
    public void setRedirectType(String redirectType)
    {
        this.redirectType = redirectType;
        if(this.redirectType != null) {
            resetEncodedKey();
        }
    }

    public Long getFlashDuration()
    {
        return this.flashDuration;
    }
    public void setFlashDuration(Long flashDuration)
    {
        this.flashDuration = flashDuration;
        if(this.flashDuration != null) {
            resetEncodedKey();
        }
    }

    public String getAccessType()
    {
        return this.accessType;
    }
    public void setAccessType(String accessType)
    {
        this.accessType = accessType;
        if(this.accessType != null) {
            resetEncodedKey();
        }
    }

    public String getViewType()
    {
        return this.viewType;
    }
    public void setViewType(String viewType)
    {
        this.viewType = viewType;
        if(this.viewType != null) {
            resetEncodedKey();
        }
    }

    public String getShareType()
    {
        return this.shareType;
    }
    public void setShareType(String shareType)
    {
        this.shareType = shareType;
        if(this.shareType != null) {
            resetEncodedKey();
        }
    }


    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getDomain() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getTokenType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getDisplayMessage() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getRedirectType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getFlashDuration() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAccessType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getViewType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getShareType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("domain", this.domain);
        dataMap.put("tokenType", this.tokenType);
        dataMap.put("displayMessage", this.displayMessage);
        dataMap.put("redirectType", this.redirectType);
        dataMap.put("flashDuration", this.flashDuration);
        dataMap.put("accessType", this.accessType);
        dataMap.put("viewType", this.viewType);
        dataMap.put("shareType", this.shareType);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        ShortPassageAttribute thatObj = (ShortPassageAttribute) obj;
        if( (this.domain == null && thatObj.getDomain() != null)
            || (this.domain != null && thatObj.getDomain() == null)
            || !this.domain.equals(thatObj.getDomain()) ) {
            return false;
        }
        if( (this.tokenType == null && thatObj.getTokenType() != null)
            || (this.tokenType != null && thatObj.getTokenType() == null)
            || !this.tokenType.equals(thatObj.getTokenType()) ) {
            return false;
        }
        if( (this.displayMessage == null && thatObj.getDisplayMessage() != null)
            || (this.displayMessage != null && thatObj.getDisplayMessage() == null)
            || !this.displayMessage.equals(thatObj.getDisplayMessage()) ) {
            return false;
        }
        if( (this.redirectType == null && thatObj.getRedirectType() != null)
            || (this.redirectType != null && thatObj.getRedirectType() == null)
            || !this.redirectType.equals(thatObj.getRedirectType()) ) {
            return false;
        }
        if( (this.flashDuration == null && thatObj.getFlashDuration() != null)
            || (this.flashDuration != null && thatObj.getFlashDuration() == null)
            || !this.flashDuration.equals(thatObj.getFlashDuration()) ) {
            return false;
        }
        if( (this.accessType == null && thatObj.getAccessType() != null)
            || (this.accessType != null && thatObj.getAccessType() == null)
            || !this.accessType.equals(thatObj.getAccessType()) ) {
            return false;
        }
        if( (this.viewType == null && thatObj.getViewType() != null)
            || (this.viewType != null && thatObj.getViewType() == null)
            || !this.viewType.equals(thatObj.getViewType()) ) {
            return false;
        }
        if( (this.shareType == null && thatObj.getShareType() != null)
            || (this.shareType != null && thatObj.getShareType() == null)
            || !this.shareType.equals(thatObj.getShareType()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = domain == null ? 0 : domain.hashCode();
        _hash = 31 * _hash + delta;
        delta = tokenType == null ? 0 : tokenType.hashCode();
        _hash = 31 * _hash + delta;
        delta = displayMessage == null ? 0 : displayMessage.hashCode();
        _hash = 31 * _hash + delta;
        delta = redirectType == null ? 0 : redirectType.hashCode();
        _hash = 31 * _hash + delta;
        delta = flashDuration == null ? 0 : flashDuration.hashCode();
        _hash = 31 * _hash + delta;
        delta = accessType == null ? 0 : accessType.hashCode();
        _hash = 31 * _hash + delta;
        delta = viewType == null ? 0 : viewType.hashCode();
        _hash = 31 * _hash + delta;
        delta = shareType == null ? 0 : shareType.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public String toString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuffer sb = new StringBuffer();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString(); // ???
            sb.append(k).append(":").append(v).append(";");
        }
        return sb.toString();
    }

    // TBD: Serialization methods?
    // ...

}
