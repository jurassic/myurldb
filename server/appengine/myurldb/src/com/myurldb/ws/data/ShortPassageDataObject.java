package com.myurldb.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.myurldb.ws.ShortPassageAttribute;
import com.myurldb.ws.ShortPassage;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;


@PersistenceCapable(detachable="true")
public class ShortPassageDataObject extends KeyedDataObject implements ShortPassage
{
    private static final Logger log = Logger.getLogger(ShortPassageDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(ShortPassageDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(ShortPassageDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String guid;

    @Persistent(defaultFetchGroup = "true")
    private String owner;

    @Persistent(defaultFetchGroup = "false")
    private Text longText;

    @Persistent(defaultFetchGroup = "false")
    private Text shortText;

    @Persistent(defaultFetchGroup = "false")
    @Embedded(members = {
        @Persistent(name="domain", columns=@Column(name="attributedomain")),
        @Persistent(name="tokenType", columns=@Column(name="attributetokenType")),
        @Persistent(name="displayMessage", columns=@Column(name="attributedisplayMessage")),
        @Persistent(name="redirectType", columns=@Column(name="attributeredirectType")),
        @Persistent(name="flashDuration", columns=@Column(name="attributeflashDuration")),
        @Persistent(name="accessType", columns=@Column(name="attributeaccessType")),
        @Persistent(name="viewType", columns=@Column(name="attributeviewType")),
        @Persistent(name="shareType", columns=@Column(name="attributeshareType")),
    })
    private ShortPassageAttributeDataObject attribute;

    @Persistent(defaultFetchGroup = "true")
    private Boolean readOnly;

    @Persistent(defaultFetchGroup = "true")
    private String status;

    @Persistent(defaultFetchGroup = "true")
    private String note;

    @Persistent(defaultFetchGroup = "true")
    private Long expirationTime;

    public ShortPassageDataObject()
    {
        this(null);
    }
    public ShortPassageDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null);
    }
    public ShortPassageDataObject(String guid, String owner, String longText, String shortText, ShortPassageAttribute attribute, Boolean readOnly, String status, String note, Long expirationTime)
    {
        this(guid, owner, longText, shortText, attribute, readOnly, status, note, expirationTime, null, null);
    }
    public ShortPassageDataObject(String guid, String owner, String longText, String shortText, ShortPassageAttribute attribute, Boolean readOnly, String status, String note, Long expirationTime, Long createdTime, Long modifiedTime)
    {
        super(createdTime, modifiedTime);
        generatePK(guid);
        this.owner = owner;
        setLongText(longText);
        setShortText(shortText);
        if(attribute != null) {
            this.attribute = new ShortPassageAttributeDataObject(attribute.getDomain(), attribute.getTokenType(), attribute.getDisplayMessage(), attribute.getRedirectType(), attribute.getFlashDuration(), attribute.getAccessType(), attribute.getViewType(), attribute.getShareType());
        } else {
            this.attribute = null;
        }
        this.readOnly = readOnly;
        this.status = status;
        this.note = note;
        this.expirationTime = expirationTime;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        generatePK(guid);
    }
    protected void generatePK(String guid)
    {
        if(guid == null) {
            this.guid = GUID.generate();  // temporary
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to a new guid = " + this.guid);
        } else {
            this.guid = guid;
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to guid = " + this.guid);
        }
        rebuildKey();        
    }

//    @Override
//    protected Key createKey()
//    {
//        return ShortPassageDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return ShortPassageDataObject.composeKey(getGuid());
    }

    public String getOwner()
    {
        return this.owner;
    }
    public void setOwner(String owner)
    {
        this.owner = owner;
    }

    public String getLongText()
    {
        if(this.longText == null) {
            return null;
        }    
        return this.longText.getValue();
    }
    public void setLongText(String longText)
    {
        if(longText == null) {
            this.longText = null;
        } else {
            this.longText = new Text(longText);
        }
    }

    public String getShortText()
    {
        if(this.shortText == null) {
            return null;
        }    
        return this.shortText.getValue();
    }
    public void setShortText(String shortText)
    {
        if(shortText == null) {
            this.shortText = null;
        } else {
            this.shortText = new Text(shortText);
        }
    }

    public ShortPassageAttribute getAttribute()
    {
        return this.attribute;
    }
    public void setAttribute(ShortPassageAttribute attribute)
    {
        if(attribute == null) {
            this.attribute = null;
            log.log(Level.INFO, "ShortPassageDataObject.setAttribute(ShortPassageAttribute attribute): Arg attribute is null.");            
        } else if(attribute instanceof ShortPassageAttributeDataObject) {
            this.attribute = (ShortPassageAttributeDataObject) attribute;
        } else if(attribute instanceof ShortPassageAttribute) {
            this.attribute = new ShortPassageAttributeDataObject(attribute.getDomain(), attribute.getTokenType(), attribute.getDisplayMessage(), attribute.getRedirectType(), attribute.getFlashDuration(), attribute.getAccessType(), attribute.getViewType(), attribute.getShareType());
        } else {
            this.attribute = new ShortPassageAttributeDataObject();   // ????
            log.log(Level.WARNING, "ShortPassageDataObject.setAttribute(ShortPassageAttribute attribute): Arg attribute is of an invalid type.");
        }
    }

    public Boolean isReadOnly()
    {
        return this.readOnly;
    }
    public void setReadOnly(Boolean readOnly)
    {
        this.readOnly = readOnly;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    public Long getExpirationTime()
    {
        return this.expirationTime;
    }
    public void setExpirationTime(Long expirationTime)
    {
        this.expirationTime = expirationTime;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("guid", this.guid);
        dataMap.put("owner", this.owner);
        dataMap.put("longText", this.longText);
        dataMap.put("shortText", this.shortText);
        dataMap.put("attribute", this.attribute);
        dataMap.put("readOnly", this.readOnly);
        dataMap.put("status", this.status);
        dataMap.put("note", this.note);
        dataMap.put("expirationTime", this.expirationTime);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        ShortPassage thatObj = (ShortPassage) obj;
        if( (this.guid == null && thatObj.getGuid() != null)
            || (this.guid != null && thatObj.getGuid() == null)
            || !this.guid.equals(thatObj.getGuid()) ) {
            return false;
        }
        if( (this.owner == null && thatObj.getOwner() != null)
            || (this.owner != null && thatObj.getOwner() == null)
            || !this.owner.equals(thatObj.getOwner()) ) {
            return false;
        }
        if( (this.longText == null && thatObj.getLongText() != null)
            || (this.longText != null && thatObj.getLongText() == null)
            || !this.longText.equals(thatObj.getLongText()) ) {
            return false;
        }
        if( (this.shortText == null && thatObj.getShortText() != null)
            || (this.shortText != null && thatObj.getShortText() == null)
            || !this.shortText.equals(thatObj.getShortText()) ) {
            return false;
        }
        if( (this.attribute == null && thatObj.getAttribute() != null)
            || (this.attribute != null && thatObj.getAttribute() == null)
            || !this.attribute.equals(thatObj.getAttribute()) ) {
            return false;
        }
        if( (this.readOnly == null && thatObj.isReadOnly() != null)
            || (this.readOnly != null && thatObj.isReadOnly() == null)
            || !this.readOnly.equals(thatObj.isReadOnly()) ) {
            return false;
        }
        if( (this.status == null && thatObj.getStatus() != null)
            || (this.status != null && thatObj.getStatus() == null)
            || !this.status.equals(thatObj.getStatus()) ) {
            return false;
        }
        if( (this.note == null && thatObj.getNote() != null)
            || (this.note != null && thatObj.getNote() == null)
            || !this.note.equals(thatObj.getNote()) ) {
            return false;
        }
        if( (this.expirationTime == null && thatObj.getExpirationTime() != null)
            || (this.expirationTime != null && thatObj.getExpirationTime() == null)
            || !this.expirationTime.equals(thatObj.getExpirationTime()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = owner == null ? 0 : owner.hashCode();
        _hash = 31 * _hash + delta;
        delta = longText == null ? 0 : longText.hashCode();
        _hash = 31 * _hash + delta;
        delta = shortText == null ? 0 : shortText.hashCode();
        _hash = 31 * _hash + delta;
        delta = attribute == null ? 0 : attribute.hashCode();
        _hash = 31 * _hash + delta;
        delta = readOnly == null ? 0 : readOnly.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        delta = expirationTime == null ? 0 : expirationTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
