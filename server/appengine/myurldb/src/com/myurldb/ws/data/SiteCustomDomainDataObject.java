package com.myurldb.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.myurldb.ws.SiteCustomDomain;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;


@PersistenceCapable(detachable="true")
public class SiteCustomDomainDataObject extends KeyedDataObject implements SiteCustomDomain
{
    private static final Logger log = Logger.getLogger(SiteCustomDomainDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(SiteCustomDomainDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(SiteCustomDomainDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String guid;

    @Persistent(defaultFetchGroup = "true")
    private String siteDomain;

    @Persistent(defaultFetchGroup = "true")
    private String domain;

    @Persistent(defaultFetchGroup = "true")
    private String status;

    public SiteCustomDomainDataObject()
    {
        this(null);
    }
    public SiteCustomDomainDataObject(String guid)
    {
        this(guid, null, null, null, null, null);
    }
    public SiteCustomDomainDataObject(String guid, String siteDomain, String domain, String status)
    {
        this(guid, siteDomain, domain, status, null, null);
    }
    public SiteCustomDomainDataObject(String guid, String siteDomain, String domain, String status, Long createdTime, Long modifiedTime)
    {
        super(createdTime, modifiedTime);
        generatePK(guid);
        this.siteDomain = siteDomain;
        this.domain = domain;
        this.status = status;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        generatePK(guid);
    }
    protected void generatePK(String guid)
    {
        if(guid == null) {
            this.guid = GUID.generate();  // temporary
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to a new guid = " + this.guid);
        } else {
            this.guid = guid;
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to guid = " + this.guid);
        }
        rebuildKey();        
    }

//    @Override
//    protected Key createKey()
//    {
//        return SiteCustomDomainDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return SiteCustomDomainDataObject.composeKey(getGuid());
    }

    public String getSiteDomain()
    {
        return this.siteDomain;
    }
    public void setSiteDomain(String siteDomain)
    {
        this.siteDomain = siteDomain;
    }

    public String getDomain()
    {
        return this.domain;
    }
    public void setDomain(String domain)
    {
        this.domain = domain;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("guid", this.guid);
        dataMap.put("siteDomain", this.siteDomain);
        dataMap.put("domain", this.domain);
        dataMap.put("status", this.status);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        SiteCustomDomain thatObj = (SiteCustomDomain) obj;
        if( (this.guid == null && thatObj.getGuid() != null)
            || (this.guid != null && thatObj.getGuid() == null)
            || !this.guid.equals(thatObj.getGuid()) ) {
            return false;
        }
        if( (this.siteDomain == null && thatObj.getSiteDomain() != null)
            || (this.siteDomain != null && thatObj.getSiteDomain() == null)
            || !this.siteDomain.equals(thatObj.getSiteDomain()) ) {
            return false;
        }
        if( (this.domain == null && thatObj.getDomain() != null)
            || (this.domain != null && thatObj.getDomain() == null)
            || !this.domain.equals(thatObj.getDomain()) ) {
            return false;
        }
        if( (this.status == null && thatObj.getStatus() != null)
            || (this.status != null && thatObj.getStatus() == null)
            || !this.status.equals(thatObj.getStatus()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = siteDomain == null ? 0 : siteDomain.hashCode();
        _hash = 31 * _hash + delta;
        delta = domain == null ? 0 : domain.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
