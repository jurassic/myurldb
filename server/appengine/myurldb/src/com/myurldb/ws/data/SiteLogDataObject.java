package com.myurldb.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.Extension;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PrimaryKey;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.myurldb.ws.SiteLog;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;


@PersistenceCapable(detachable="true")
// @EmbeddedOnly
public class SiteLogDataObject implements SiteLog, Serializable
{
    private static final Logger log = Logger.getLogger(SiteLogDataObject.class.getName());

    @PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    @Extension(vendorName="datanucleus", key="gae.encoded-pk", value="true")
    private String _sitelog_encoded_key = null;   // Due to GAE limitation, the PK needs to be either Key or encoded string...

    @Persistent(defaultFetchGroup = "true")
    // @Extension(vendorName="datanucleus", key="gae.pk-name", value="true")
    private String uuid;

    @Persistent(defaultFetchGroup = "true")
    private String title;

    @Persistent(defaultFetchGroup = "true")
    private String pubDate;

    @Persistent(defaultFetchGroup = "true")
    private String tag;

    @Persistent(defaultFetchGroup = "true")
    private Text content;

    @Persistent(defaultFetchGroup = "true")
    private String format;

    @Persistent(defaultFetchGroup = "true")
    private String note;

    @Persistent(defaultFetchGroup = "true")
    private String status;

    public SiteLogDataObject()
    {
        // ???
        // this(null, null, null, null, null, null, null, null);
    }
    public SiteLogDataObject(String uuid, String title, String pubDate, String tag, String content, String format, String note, String status)
    {
        setUuid(uuid);
        setTitle(title);
        setPubDate(pubDate);
        setTag(tag);
        setContent(content);
        setFormat(format);
        setNote(note);
        setStatus(status);
    }

    private void resetEncodedKey()
    {
        if(_sitelog_encoded_key == null) {
            if(uuid == null || uuid.isEmpty()) {
                // Temporary solution. PK cannot be null.
                uuid = GUID.generate();
            }
            _sitelog_encoded_key = KeyFactory.createKeyString(SiteLogDataObject.class.getSimpleName(), uuid);
        }
    }

    public String getUuid()
    {
        return this.uuid;
    }
    public void setUuid(String uuid)
    {
        if(uuid == null || uuid.isEmpty()) {
            // Temporary solution. PK cannot be null. ???
            // this.uuid = GUID.generate();
            this.uuid = uuid;
            _sitelog_encoded_key = null;   // ???
        } else {
            this.uuid = uuid;
            _sitelog_encoded_key = KeyFactory.createKeyString(SiteLogDataObject.class.getSimpleName(), this.uuid);
        }
    }

    public String getTitle()
    {
        return this.title;
    }
    public void setTitle(String title)
    {
        this.title = title;
        if(this.title != null) {
            resetEncodedKey();
        }
    }

    public String getPubDate()
    {
        return this.pubDate;
    }
    public void setPubDate(String pubDate)
    {
        this.pubDate = pubDate;
        if(this.pubDate != null) {
            resetEncodedKey();
        }
    }

    public String getTag()
    {
        return this.tag;
    }
    public void setTag(String tag)
    {
        this.tag = tag;
        if(this.tag != null) {
            resetEncodedKey();
        }
    }

    public String getContent()
    {
        if(this.content == null) {
            return null;
        }    
        return this.content.getValue();
    }
    public void setContent(String content)
    {
        if(content == null) {
            this.content = null;
        } else {
            this.content = new Text(content);
        }
    }

    public String getFormat()
    {
        return this.format;
    }
    public void setFormat(String format)
    {
        this.format = format;
        if(this.format != null) {
            resetEncodedKey();
        }
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
        if(this.note != null) {
            resetEncodedKey();
        }
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
        if(this.status != null) {
            resetEncodedKey();
        }
    }


    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getTitle() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPubDate() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getTag() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getContent() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getFormat() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getStatus() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("uuid", this.uuid);
        dataMap.put("title", this.title);
        dataMap.put("pubDate", this.pubDate);
        dataMap.put("tag", this.tag);
        dataMap.put("content", this.content);
        dataMap.put("format", this.format);
        dataMap.put("note", this.note);
        dataMap.put("status", this.status);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        SiteLog thatObj = (SiteLog) obj;
        if( (this.uuid == null && thatObj.getUuid() != null)
            || (this.uuid != null && thatObj.getUuid() == null)
            || !this.uuid.equals(thatObj.getUuid()) ) {
            return false;
        }
        if( (this.title == null && thatObj.getTitle() != null)
            || (this.title != null && thatObj.getTitle() == null)
            || !this.title.equals(thatObj.getTitle()) ) {
            return false;
        }
        if( (this.pubDate == null && thatObj.getPubDate() != null)
            || (this.pubDate != null && thatObj.getPubDate() == null)
            || !this.pubDate.equals(thatObj.getPubDate()) ) {
            return false;
        }
        if( (this.tag == null && thatObj.getTag() != null)
            || (this.tag != null && thatObj.getTag() == null)
            || !this.tag.equals(thatObj.getTag()) ) {
            return false;
        }
        if( (this.content == null && thatObj.getContent() != null)
            || (this.content != null && thatObj.getContent() == null)
            || !this.content.equals(thatObj.getContent()) ) {
            return false;
        }
        if( (this.format == null && thatObj.getFormat() != null)
            || (this.format != null && thatObj.getFormat() == null)
            || !this.format.equals(thatObj.getFormat()) ) {
            return false;
        }
        if( (this.note == null && thatObj.getNote() != null)
            || (this.note != null && thatObj.getNote() == null)
            || !this.note.equals(thatObj.getNote()) ) {
            return false;
        }
        if( (this.status == null && thatObj.getStatus() != null)
            || (this.status != null && thatObj.getStatus() == null)
            || !this.status.equals(thatObj.getStatus()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = uuid == null ? 0 : uuid.hashCode();
        _hash = 31 * _hash + delta;
        delta = title == null ? 0 : title.hashCode();
        _hash = 31 * _hash + delta;
        delta = pubDate == null ? 0 : pubDate.hashCode();
        _hash = 31 * _hash + delta;
        delta = tag == null ? 0 : tag.hashCode();
        _hash = 31 * _hash + delta;
        delta = content == null ? 0 : content.hashCode();
        _hash = 31 * _hash + delta;
        delta = format == null ? 0 : format.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public String toString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuffer sb = new StringBuffer();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString(); // ???
            sb.append(k).append(":").append(v).append(";");
        }
        return sb.toString();
    }

    // TBD: Serialization methods?
    // ...

}
