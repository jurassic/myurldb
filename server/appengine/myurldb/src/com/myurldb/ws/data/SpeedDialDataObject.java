package com.myurldb.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.myurldb.ws.SpeedDial;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;


@PersistenceCapable(detachable="true")
public class SpeedDialDataObject extends KeyedDataObject implements SpeedDial
{
    private static final Logger log = Logger.getLogger(SpeedDialDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(SpeedDialDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(SpeedDialDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String guid;

    @Persistent(defaultFetchGroup = "true")
    private String user;

    @Persistent(defaultFetchGroup = "true")
    private String code;

    @Persistent(defaultFetchGroup = "true")
    private String shortcut;

    @Persistent(defaultFetchGroup = "true")
    private String shortLink;

    @Persistent(defaultFetchGroup = "true")
    private String keywordLink;

    @Persistent(defaultFetchGroup = "true")
    private String bookmarkLink;

    @Persistent(defaultFetchGroup = "true")
    private String longUrl;

    @Persistent(defaultFetchGroup = "true")
    private String shortUrl;

    @Persistent(defaultFetchGroup = "true")
    private Boolean caseSensitive;

    @Persistent(defaultFetchGroup = "true")
    private String status;

    @Persistent(defaultFetchGroup = "true")
    private String note;

    public SpeedDialDataObject()
    {
        this(null);
    }
    public SpeedDialDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public SpeedDialDataObject(String guid, String user, String code, String shortcut, String shortLink, String keywordLink, String bookmarkLink, String longUrl, String shortUrl, Boolean caseSensitive, String status, String note)
    {
        this(guid, user, code, shortcut, shortLink, keywordLink, bookmarkLink, longUrl, shortUrl, caseSensitive, status, note, null, null);
    }
    public SpeedDialDataObject(String guid, String user, String code, String shortcut, String shortLink, String keywordLink, String bookmarkLink, String longUrl, String shortUrl, Boolean caseSensitive, String status, String note, Long createdTime, Long modifiedTime)
    {
        super(createdTime, modifiedTime);
        generatePK(guid);
        this.user = user;
        this.code = code;
        this.shortcut = shortcut;
        this.shortLink = shortLink;
        this.keywordLink = keywordLink;
        this.bookmarkLink = bookmarkLink;
        this.longUrl = longUrl;
        this.shortUrl = shortUrl;
        this.caseSensitive = caseSensitive;
        this.status = status;
        this.note = note;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        generatePK(guid);
    }
    protected void generatePK(String guid)
    {
        if(guid == null) {
            this.guid = GUID.generate();  // temporary
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to a new guid = " + this.guid);
        } else {
            this.guid = guid;
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to guid = " + this.guid);
        }
        rebuildKey();        
    }

//    @Override
//    protected Key createKey()
//    {
//        return SpeedDialDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return SpeedDialDataObject.composeKey(getGuid());
    }

    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    public String getCode()
    {
        return this.code;
    }
    public void setCode(String code)
    {
        this.code = code;
    }

    public String getShortcut()
    {
        return this.shortcut;
    }
    public void setShortcut(String shortcut)
    {
        this.shortcut = shortcut;
    }

    public String getShortLink()
    {
        return this.shortLink;
    }
    public void setShortLink(String shortLink)
    {
        this.shortLink = shortLink;
    }

    public String getKeywordLink()
    {
        return this.keywordLink;
    }
    public void setKeywordLink(String keywordLink)
    {
        this.keywordLink = keywordLink;
    }

    public String getBookmarkLink()
    {
        return this.bookmarkLink;
    }
    public void setBookmarkLink(String bookmarkLink)
    {
        this.bookmarkLink = bookmarkLink;
    }

    public String getLongUrl()
    {
        return this.longUrl;
    }
    public void setLongUrl(String longUrl)
    {
        this.longUrl = longUrl;
    }

    public String getShortUrl()
    {
        return this.shortUrl;
    }
    public void setShortUrl(String shortUrl)
    {
        this.shortUrl = shortUrl;
    }

    public Boolean isCaseSensitive()
    {
        return this.caseSensitive;
    }
    public void setCaseSensitive(Boolean caseSensitive)
    {
        this.caseSensitive = caseSensitive;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("guid", this.guid);
        dataMap.put("user", this.user);
        dataMap.put("code", this.code);
        dataMap.put("shortcut", this.shortcut);
        dataMap.put("shortLink", this.shortLink);
        dataMap.put("keywordLink", this.keywordLink);
        dataMap.put("bookmarkLink", this.bookmarkLink);
        dataMap.put("longUrl", this.longUrl);
        dataMap.put("shortUrl", this.shortUrl);
        dataMap.put("caseSensitive", this.caseSensitive);
        dataMap.put("status", this.status);
        dataMap.put("note", this.note);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        SpeedDial thatObj = (SpeedDial) obj;
        if( (this.guid == null && thatObj.getGuid() != null)
            || (this.guid != null && thatObj.getGuid() == null)
            || !this.guid.equals(thatObj.getGuid()) ) {
            return false;
        }
        if( (this.user == null && thatObj.getUser() != null)
            || (this.user != null && thatObj.getUser() == null)
            || !this.user.equals(thatObj.getUser()) ) {
            return false;
        }
        if( (this.code == null && thatObj.getCode() != null)
            || (this.code != null && thatObj.getCode() == null)
            || !this.code.equals(thatObj.getCode()) ) {
            return false;
        }
        if( (this.shortcut == null && thatObj.getShortcut() != null)
            || (this.shortcut != null && thatObj.getShortcut() == null)
            || !this.shortcut.equals(thatObj.getShortcut()) ) {
            return false;
        }
        if( (this.shortLink == null && thatObj.getShortLink() != null)
            || (this.shortLink != null && thatObj.getShortLink() == null)
            || !this.shortLink.equals(thatObj.getShortLink()) ) {
            return false;
        }
        if( (this.keywordLink == null && thatObj.getKeywordLink() != null)
            || (this.keywordLink != null && thatObj.getKeywordLink() == null)
            || !this.keywordLink.equals(thatObj.getKeywordLink()) ) {
            return false;
        }
        if( (this.bookmarkLink == null && thatObj.getBookmarkLink() != null)
            || (this.bookmarkLink != null && thatObj.getBookmarkLink() == null)
            || !this.bookmarkLink.equals(thatObj.getBookmarkLink()) ) {
            return false;
        }
        if( (this.longUrl == null && thatObj.getLongUrl() != null)
            || (this.longUrl != null && thatObj.getLongUrl() == null)
            || !this.longUrl.equals(thatObj.getLongUrl()) ) {
            return false;
        }
        if( (this.shortUrl == null && thatObj.getShortUrl() != null)
            || (this.shortUrl != null && thatObj.getShortUrl() == null)
            || !this.shortUrl.equals(thatObj.getShortUrl()) ) {
            return false;
        }
        if( (this.caseSensitive == null && thatObj.isCaseSensitive() != null)
            || (this.caseSensitive != null && thatObj.isCaseSensitive() == null)
            || !this.caseSensitive.equals(thatObj.isCaseSensitive()) ) {
            return false;
        }
        if( (this.status == null && thatObj.getStatus() != null)
            || (this.status != null && thatObj.getStatus() == null)
            || !this.status.equals(thatObj.getStatus()) ) {
            return false;
        }
        if( (this.note == null && thatObj.getNote() != null)
            || (this.note != null && thatObj.getNote() == null)
            || !this.note.equals(thatObj.getNote()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = user == null ? 0 : user.hashCode();
        _hash = 31 * _hash + delta;
        delta = code == null ? 0 : code.hashCode();
        _hash = 31 * _hash + delta;
        delta = shortcut == null ? 0 : shortcut.hashCode();
        _hash = 31 * _hash + delta;
        delta = shortLink == null ? 0 : shortLink.hashCode();
        _hash = 31 * _hash + delta;
        delta = keywordLink == null ? 0 : keywordLink.hashCode();
        _hash = 31 * _hash + delta;
        delta = bookmarkLink == null ? 0 : bookmarkLink.hashCode();
        _hash = 31 * _hash + delta;
        delta = longUrl == null ? 0 : longUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = shortUrl == null ? 0 : shortUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = caseSensitive == null ? 0 : caseSensitive.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
