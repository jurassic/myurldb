package com.myurldb.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.Extension;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PrimaryKey;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.myurldb.ws.StreetAddressStruct;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;


@PersistenceCapable(detachable="true")
// @EmbeddedOnly
public class StreetAddressStructDataObject implements StreetAddressStruct, Serializable
{
    private static final Logger log = Logger.getLogger(StreetAddressStructDataObject.class.getName());

    @PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    @Extension(vendorName="datanucleus", key="gae.encoded-pk", value="true")
    private String _streetaddressstruct_encoded_key = null;   // Due to GAE limitation, the PK needs to be either Key or encoded string...

    @Persistent(defaultFetchGroup = "true")
    // @Extension(vendorName="datanucleus", key="gae.pk-name", value="true")
    private String uuid;

    @Persistent(defaultFetchGroup = "true")
    private String street1;

    @Persistent(defaultFetchGroup = "true")
    private String street2;

    @Persistent(defaultFetchGroup = "true")
    private String city;

    @Persistent(defaultFetchGroup = "true")
    private String county;

    @Persistent(defaultFetchGroup = "true")
    private String postalCode;

    @Persistent(defaultFetchGroup = "true")
    private String state;

    @Persistent(defaultFetchGroup = "true")
    private String province;

    @Persistent(defaultFetchGroup = "true")
    private String country;

    @Persistent(defaultFetchGroup = "true")
    private String countryName;

    @Persistent(defaultFetchGroup = "true")
    private String note;

    public StreetAddressStructDataObject()
    {
        // ???
        // this(null, null, null, null, null, null, null, null, null, null, null);
    }
    public StreetAddressStructDataObject(String uuid, String street1, String street2, String city, String county, String postalCode, String state, String province, String country, String countryName, String note)
    {
        setUuid(uuid);
        setStreet1(street1);
        setStreet2(street2);
        setCity(city);
        setCounty(county);
        setPostalCode(postalCode);
        setState(state);
        setProvince(province);
        setCountry(country);
        setCountryName(countryName);
        setNote(note);
    }

    private void resetEncodedKey()
    {
        if(_streetaddressstruct_encoded_key == null) {
            if(uuid == null || uuid.isEmpty()) {
                // Temporary solution. PK cannot be null.
                uuid = GUID.generate();
            }
            _streetaddressstruct_encoded_key = KeyFactory.createKeyString(StreetAddressStructDataObject.class.getSimpleName(), uuid);
        }
    }

    public String getUuid()
    {
        return this.uuid;
    }
    public void setUuid(String uuid)
    {
        if(uuid == null || uuid.isEmpty()) {
            // Temporary solution. PK cannot be null. ???
            // this.uuid = GUID.generate();
            this.uuid = uuid;
            _streetaddressstruct_encoded_key = null;   // ???
        } else {
            this.uuid = uuid;
            _streetaddressstruct_encoded_key = KeyFactory.createKeyString(StreetAddressStructDataObject.class.getSimpleName(), this.uuid);
        }
    }

    public String getStreet1()
    {
        return this.street1;
    }
    public void setStreet1(String street1)
    {
        this.street1 = street1;
        if(this.street1 != null) {
            resetEncodedKey();
        }
    }

    public String getStreet2()
    {
        return this.street2;
    }
    public void setStreet2(String street2)
    {
        this.street2 = street2;
        if(this.street2 != null) {
            resetEncodedKey();
        }
    }

    public String getCity()
    {
        return this.city;
    }
    public void setCity(String city)
    {
        this.city = city;
        if(this.city != null) {
            resetEncodedKey();
        }
    }

    public String getCounty()
    {
        return this.county;
    }
    public void setCounty(String county)
    {
        this.county = county;
        if(this.county != null) {
            resetEncodedKey();
        }
    }

    public String getPostalCode()
    {
        return this.postalCode;
    }
    public void setPostalCode(String postalCode)
    {
        this.postalCode = postalCode;
        if(this.postalCode != null) {
            resetEncodedKey();
        }
    }

    public String getState()
    {
        return this.state;
    }
    public void setState(String state)
    {
        this.state = state;
        if(this.state != null) {
            resetEncodedKey();
        }
    }

    public String getProvince()
    {
        return this.province;
    }
    public void setProvince(String province)
    {
        this.province = province;
        if(this.province != null) {
            resetEncodedKey();
        }
    }

    public String getCountry()
    {
        return this.country;
    }
    public void setCountry(String country)
    {
        this.country = country;
        if(this.country != null) {
            resetEncodedKey();
        }
    }

    public String getCountryName()
    {
        return this.countryName;
    }
    public void setCountryName(String countryName)
    {
        this.countryName = countryName;
        if(this.countryName != null) {
            resetEncodedKey();
        }
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
        if(this.note != null) {
            resetEncodedKey();
        }
    }


    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getStreet1() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getStreet2() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getCity() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getCounty() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPostalCode() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getState() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getProvince() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getCountry() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getCountryName() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("uuid", this.uuid);
        dataMap.put("street1", this.street1);
        dataMap.put("street2", this.street2);
        dataMap.put("city", this.city);
        dataMap.put("county", this.county);
        dataMap.put("postalCode", this.postalCode);
        dataMap.put("state", this.state);
        dataMap.put("province", this.province);
        dataMap.put("country", this.country);
        dataMap.put("countryName", this.countryName);
        dataMap.put("note", this.note);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        StreetAddressStruct thatObj = (StreetAddressStruct) obj;
        if( (this.uuid == null && thatObj.getUuid() != null)
            || (this.uuid != null && thatObj.getUuid() == null)
            || !this.uuid.equals(thatObj.getUuid()) ) {
            return false;
        }
        if( (this.street1 == null && thatObj.getStreet1() != null)
            || (this.street1 != null && thatObj.getStreet1() == null)
            || !this.street1.equals(thatObj.getStreet1()) ) {
            return false;
        }
        if( (this.street2 == null && thatObj.getStreet2() != null)
            || (this.street2 != null && thatObj.getStreet2() == null)
            || !this.street2.equals(thatObj.getStreet2()) ) {
            return false;
        }
        if( (this.city == null && thatObj.getCity() != null)
            || (this.city != null && thatObj.getCity() == null)
            || !this.city.equals(thatObj.getCity()) ) {
            return false;
        }
        if( (this.county == null && thatObj.getCounty() != null)
            || (this.county != null && thatObj.getCounty() == null)
            || !this.county.equals(thatObj.getCounty()) ) {
            return false;
        }
        if( (this.postalCode == null && thatObj.getPostalCode() != null)
            || (this.postalCode != null && thatObj.getPostalCode() == null)
            || !this.postalCode.equals(thatObj.getPostalCode()) ) {
            return false;
        }
        if( (this.state == null && thatObj.getState() != null)
            || (this.state != null && thatObj.getState() == null)
            || !this.state.equals(thatObj.getState()) ) {
            return false;
        }
        if( (this.province == null && thatObj.getProvince() != null)
            || (this.province != null && thatObj.getProvince() == null)
            || !this.province.equals(thatObj.getProvince()) ) {
            return false;
        }
        if( (this.country == null && thatObj.getCountry() != null)
            || (this.country != null && thatObj.getCountry() == null)
            || !this.country.equals(thatObj.getCountry()) ) {
            return false;
        }
        if( (this.countryName == null && thatObj.getCountryName() != null)
            || (this.countryName != null && thatObj.getCountryName() == null)
            || !this.countryName.equals(thatObj.getCountryName()) ) {
            return false;
        }
        if( (this.note == null && thatObj.getNote() != null)
            || (this.note != null && thatObj.getNote() == null)
            || !this.note.equals(thatObj.getNote()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = uuid == null ? 0 : uuid.hashCode();
        _hash = 31 * _hash + delta;
        delta = street1 == null ? 0 : street1.hashCode();
        _hash = 31 * _hash + delta;
        delta = street2 == null ? 0 : street2.hashCode();
        _hash = 31 * _hash + delta;
        delta = city == null ? 0 : city.hashCode();
        _hash = 31 * _hash + delta;
        delta = county == null ? 0 : county.hashCode();
        _hash = 31 * _hash + delta;
        delta = postalCode == null ? 0 : postalCode.hashCode();
        _hash = 31 * _hash + delta;
        delta = state == null ? 0 : state.hashCode();
        _hash = 31 * _hash + delta;
        delta = province == null ? 0 : province.hashCode();
        _hash = 31 * _hash + delta;
        delta = country == null ? 0 : country.hashCode();
        _hash = 31 * _hash + delta;
        delta = countryName == null ? 0 : countryName.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public String toString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuffer sb = new StringBuffer();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString(); // ???
            sb.append(k).append(":").append(v).append(";");
        }
        return sb.toString();
    }

    // TBD: Serialization methods?
    // ...

}
