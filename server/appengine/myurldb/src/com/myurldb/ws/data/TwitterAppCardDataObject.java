package com.myurldb.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.TwitterAppCard;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;


@PersistenceCapable(detachable="true")
@Inheritance(customStrategy = "complete-table")
public class TwitterAppCardDataObject extends TwitterCardBaseDataObject implements TwitterAppCard
{
    private static final Logger log = Logger.getLogger(TwitterAppCardDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(TwitterAppCardDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(TwitterAppCardDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String image;

    @Persistent(defaultFetchGroup = "true")
    private Integer imageWidth;

    @Persistent(defaultFetchGroup = "true")
    private Integer imageHeight;

    @Persistent(defaultFetchGroup = "false")
    @Embedded(members = {
        @Persistent(name="name", columns=@Column(name="iphoneAppname")),
        @Persistent(name="id", columns=@Column(name="iphoneAppid")),
        @Persistent(name="url", columns=@Column(name="iphoneAppurl")),
    })
    private TwitterCardAppInfoDataObject iphoneApp;

    @Persistent(defaultFetchGroup = "false")
    @Embedded(members = {
        @Persistent(name="name", columns=@Column(name="ipadAppname")),
        @Persistent(name="id", columns=@Column(name="ipadAppid")),
        @Persistent(name="url", columns=@Column(name="ipadAppurl")),
    })
    private TwitterCardAppInfoDataObject ipadApp;

    @Persistent(defaultFetchGroup = "false")
    @Embedded(members = {
        @Persistent(name="name", columns=@Column(name="googlePlayAppname")),
        @Persistent(name="id", columns=@Column(name="googlePlayAppid")),
        @Persistent(name="url", columns=@Column(name="googlePlayAppurl")),
    })
    private TwitterCardAppInfoDataObject googlePlayApp;

    public TwitterAppCardDataObject()
    {
        this(null);
    }
    public TwitterAppCardDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public TwitterAppCardDataObject(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, TwitterCardAppInfo iphoneApp, TwitterCardAppInfo ipadApp, TwitterCardAppInfo googlePlayApp)
    {
        this(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, iphoneApp, ipadApp, googlePlayApp, null, null);
    }
    public TwitterAppCardDataObject(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, TwitterCardAppInfo iphoneApp, TwitterCardAppInfo ipadApp, TwitterCardAppInfo googlePlayApp, Long createdTime, Long modifiedTime)
    {
        super(guid, card, url, title, description, site, siteId, creator, creatorId, createdTime, modifiedTime);
        this.image = image;
        this.imageWidth = imageWidth;
        this.imageHeight = imageHeight;
        if(iphoneApp != null) {
            this.iphoneApp = new TwitterCardAppInfoDataObject(iphoneApp.getName(), iphoneApp.getId(), iphoneApp.getUrl());
        } else {
            this.iphoneApp = null;
        }
        if(ipadApp != null) {
            this.ipadApp = new TwitterCardAppInfoDataObject(ipadApp.getName(), ipadApp.getId(), ipadApp.getUrl());
        } else {
            this.ipadApp = null;
        }
        if(googlePlayApp != null) {
            this.googlePlayApp = new TwitterCardAppInfoDataObject(googlePlayApp.getName(), googlePlayApp.getId(), googlePlayApp.getUrl());
        } else {
            this.googlePlayApp = null;
        }
    }

//    @Override
//    protected Key createKey()
//    {
//        return TwitterAppCardDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return TwitterAppCardDataObject.composeKey(getGuid());
    }

    public String getImage()
    {
        return this.image;
    }
    public void setImage(String image)
    {
        this.image = image;
    }

    public Integer getImageWidth()
    {
        return this.imageWidth;
    }
    public void setImageWidth(Integer imageWidth)
    {
        this.imageWidth = imageWidth;
    }

    public Integer getImageHeight()
    {
        return this.imageHeight;
    }
    public void setImageHeight(Integer imageHeight)
    {
        this.imageHeight = imageHeight;
    }

    public TwitterCardAppInfo getIphoneApp()
    {
        return this.iphoneApp;
    }
    public void setIphoneApp(TwitterCardAppInfo iphoneApp)
    {
        if(iphoneApp == null) {
            this.iphoneApp = null;
            log.log(Level.INFO, "TwitterAppCardDataObject.setIphoneApp(TwitterCardAppInfo iphoneApp): Arg iphoneApp is null.");            
        } else if(iphoneApp instanceof TwitterCardAppInfoDataObject) {
            this.iphoneApp = (TwitterCardAppInfoDataObject) iphoneApp;
        } else if(iphoneApp instanceof TwitterCardAppInfo) {
            this.iphoneApp = new TwitterCardAppInfoDataObject(iphoneApp.getName(), iphoneApp.getId(), iphoneApp.getUrl());
        } else {
            this.iphoneApp = new TwitterCardAppInfoDataObject();   // ????
            log.log(Level.WARNING, "TwitterAppCardDataObject.setIphoneApp(TwitterCardAppInfo iphoneApp): Arg iphoneApp is of an invalid type.");
        }
    }

    public TwitterCardAppInfo getIpadApp()
    {
        return this.ipadApp;
    }
    public void setIpadApp(TwitterCardAppInfo ipadApp)
    {
        if(ipadApp == null) {
            this.ipadApp = null;
            log.log(Level.INFO, "TwitterAppCardDataObject.setIpadApp(TwitterCardAppInfo ipadApp): Arg ipadApp is null.");            
        } else if(ipadApp instanceof TwitterCardAppInfoDataObject) {
            this.ipadApp = (TwitterCardAppInfoDataObject) ipadApp;
        } else if(ipadApp instanceof TwitterCardAppInfo) {
            this.ipadApp = new TwitterCardAppInfoDataObject(ipadApp.getName(), ipadApp.getId(), ipadApp.getUrl());
        } else {
            this.ipadApp = new TwitterCardAppInfoDataObject();   // ????
            log.log(Level.WARNING, "TwitterAppCardDataObject.setIpadApp(TwitterCardAppInfo ipadApp): Arg ipadApp is of an invalid type.");
        }
    }

    public TwitterCardAppInfo getGooglePlayApp()
    {
        return this.googlePlayApp;
    }
    public void setGooglePlayApp(TwitterCardAppInfo googlePlayApp)
    {
        if(googlePlayApp == null) {
            this.googlePlayApp = null;
            log.log(Level.INFO, "TwitterAppCardDataObject.setGooglePlayApp(TwitterCardAppInfo googlePlayApp): Arg googlePlayApp is null.");            
        } else if(googlePlayApp instanceof TwitterCardAppInfoDataObject) {
            this.googlePlayApp = (TwitterCardAppInfoDataObject) googlePlayApp;
        } else if(googlePlayApp instanceof TwitterCardAppInfo) {
            this.googlePlayApp = new TwitterCardAppInfoDataObject(googlePlayApp.getName(), googlePlayApp.getId(), googlePlayApp.getUrl());
        } else {
            this.googlePlayApp = new TwitterCardAppInfoDataObject();   // ????
            log.log(Level.WARNING, "TwitterAppCardDataObject.setGooglePlayApp(TwitterCardAppInfo googlePlayApp): Arg googlePlayApp is of an invalid type.");
        }
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("image", this.image);
        dataMap.put("imageWidth", this.imageWidth);
        dataMap.put("imageHeight", this.imageHeight);
        dataMap.put("iphoneApp", this.iphoneApp);
        dataMap.put("ipadApp", this.ipadApp);
        dataMap.put("googlePlayApp", this.googlePlayApp);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        TwitterAppCard thatObj = (TwitterAppCard) obj;
        if( (this.image == null && thatObj.getImage() != null)
            || (this.image != null && thatObj.getImage() == null)
            || !this.image.equals(thatObj.getImage()) ) {
            return false;
        }
        if( (this.imageWidth == null && thatObj.getImageWidth() != null)
            || (this.imageWidth != null && thatObj.getImageWidth() == null)
            || !this.imageWidth.equals(thatObj.getImageWidth()) ) {
            return false;
        }
        if( (this.imageHeight == null && thatObj.getImageHeight() != null)
            || (this.imageHeight != null && thatObj.getImageHeight() == null)
            || !this.imageHeight.equals(thatObj.getImageHeight()) ) {
            return false;
        }
        if( (this.iphoneApp == null && thatObj.getIphoneApp() != null)
            || (this.iphoneApp != null && thatObj.getIphoneApp() == null)
            || !this.iphoneApp.equals(thatObj.getIphoneApp()) ) {
            return false;
        }
        if( (this.ipadApp == null && thatObj.getIpadApp() != null)
            || (this.ipadApp != null && thatObj.getIpadApp() == null)
            || !this.ipadApp.equals(thatObj.getIpadApp()) ) {
            return false;
        }
        if( (this.googlePlayApp == null && thatObj.getGooglePlayApp() != null)
            || (this.googlePlayApp != null && thatObj.getGooglePlayApp() == null)
            || !this.googlePlayApp.equals(thatObj.getGooglePlayApp()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = image == null ? 0 : image.hashCode();
        _hash = 31 * _hash + delta;
        delta = imageWidth == null ? 0 : imageWidth.hashCode();
        _hash = 31 * _hash + delta;
        delta = imageHeight == null ? 0 : imageHeight.hashCode();
        _hash = 31 * _hash + delta;
        delta = iphoneApp == null ? 0 : iphoneApp.hashCode();
        _hash = 31 * _hash + delta;
        delta = ipadApp == null ? 0 : ipadApp.hashCode();
        _hash = 31 * _hash + delta;
        delta = googlePlayApp == null ? 0 : googlePlayApp.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
