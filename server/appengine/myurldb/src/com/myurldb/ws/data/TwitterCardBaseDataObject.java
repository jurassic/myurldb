package com.myurldb.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.TwitterCardBase;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;


@PersistenceCapable(detachable="true")
@Inheritance(strategy = InheritanceStrategy.SUBCLASS_TABLE)
public abstract class TwitterCardBaseDataObject extends KeyedDataObject implements TwitterCardBase
{
    private static final Logger log = Logger.getLogger(TwitterCardBaseDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(TwitterCardBaseDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(TwitterCardBaseDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String guid;

    @Persistent(defaultFetchGroup = "true")
    private String card;

    @Persistent(defaultFetchGroup = "true")
    private String url;

    @Persistent(defaultFetchGroup = "true")
    private String title;

    @Persistent(defaultFetchGroup = "true")
    private String description;

    @Persistent(defaultFetchGroup = "true")
    private String site;

    @Persistent(defaultFetchGroup = "true")
    private String siteId;

    @Persistent(defaultFetchGroup = "true")
    private String creator;

    @Persistent(defaultFetchGroup = "true")
    private String creatorId;

    public TwitterCardBaseDataObject()
    {
        this(null);
    }
    public TwitterCardBaseDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null);
    }
    public TwitterCardBaseDataObject(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId)
    {
        this(guid, card, url, title, description, site, siteId, creator, creatorId, null, null);
    }
    public TwitterCardBaseDataObject(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, Long createdTime, Long modifiedTime)
    {
        super(createdTime, modifiedTime);
        generatePK(guid);
        this.card = card;
        this.url = url;
        this.title = title;
        this.description = description;
        this.site = site;
        this.siteId = siteId;
        this.creator = creator;
        this.creatorId = creatorId;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        generatePK(guid);
    }
    protected void generatePK(String guid)
    {
        if(guid == null) {
            this.guid = GUID.generate();  // temporary
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to a new guid = " + this.guid);
        } else {
            this.guid = guid;
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to guid = " + this.guid);
        }
        rebuildKey();        
    }

//    @Override
//    protected Key createKey()
//    {
//        return TwitterCardBaseDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return TwitterCardBaseDataObject.composeKey(getGuid());
    }

    public String getCard()
    {
        return this.card;
    }
    public void setCard(String card)
    {
        this.card = card;
    }

    public String getUrl()
    {
        return this.url;
    }
    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getTitle()
    {
        return this.title;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getDescription()
    {
        return this.description;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getSite()
    {
        return this.site;
    }
    public void setSite(String site)
    {
        this.site = site;
    }

    public String getSiteId()
    {
        return this.siteId;
    }
    public void setSiteId(String siteId)
    {
        this.siteId = siteId;
    }

    public String getCreator()
    {
        return this.creator;
    }
    public void setCreator(String creator)
    {
        this.creator = creator;
    }

    public String getCreatorId()
    {
        return this.creatorId;
    }
    public void setCreatorId(String creatorId)
    {
        this.creatorId = creatorId;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("guid", this.guid);
        dataMap.put("card", this.card);
        dataMap.put("url", this.url);
        dataMap.put("title", this.title);
        dataMap.put("description", this.description);
        dataMap.put("site", this.site);
        dataMap.put("siteId", this.siteId);
        dataMap.put("creator", this.creator);
        dataMap.put("creatorId", this.creatorId);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        TwitterCardBase thatObj = (TwitterCardBase) obj;
        if( (this.guid == null && thatObj.getGuid() != null)
            || (this.guid != null && thatObj.getGuid() == null)
            || !this.guid.equals(thatObj.getGuid()) ) {
            return false;
        }
        if( (this.card == null && thatObj.getCard() != null)
            || (this.card != null && thatObj.getCard() == null)
            || !this.card.equals(thatObj.getCard()) ) {
            return false;
        }
        if( (this.url == null && thatObj.getUrl() != null)
            || (this.url != null && thatObj.getUrl() == null)
            || !this.url.equals(thatObj.getUrl()) ) {
            return false;
        }
        if( (this.title == null && thatObj.getTitle() != null)
            || (this.title != null && thatObj.getTitle() == null)
            || !this.title.equals(thatObj.getTitle()) ) {
            return false;
        }
        if( (this.description == null && thatObj.getDescription() != null)
            || (this.description != null && thatObj.getDescription() == null)
            || !this.description.equals(thatObj.getDescription()) ) {
            return false;
        }
        if( (this.site == null && thatObj.getSite() != null)
            || (this.site != null && thatObj.getSite() == null)
            || !this.site.equals(thatObj.getSite()) ) {
            return false;
        }
        if( (this.siteId == null && thatObj.getSiteId() != null)
            || (this.siteId != null && thatObj.getSiteId() == null)
            || !this.siteId.equals(thatObj.getSiteId()) ) {
            return false;
        }
        if( (this.creator == null && thatObj.getCreator() != null)
            || (this.creator != null && thatObj.getCreator() == null)
            || !this.creator.equals(thatObj.getCreator()) ) {
            return false;
        }
        if( (this.creatorId == null && thatObj.getCreatorId() != null)
            || (this.creatorId != null && thatObj.getCreatorId() == null)
            || !this.creatorId.equals(thatObj.getCreatorId()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = card == null ? 0 : card.hashCode();
        _hash = 31 * _hash + delta;
        delta = url == null ? 0 : url.hashCode();
        _hash = 31 * _hash + delta;
        delta = title == null ? 0 : title.hashCode();
        _hash = 31 * _hash + delta;
        delta = description == null ? 0 : description.hashCode();
        _hash = 31 * _hash + delta;
        delta = site == null ? 0 : site.hashCode();
        _hash = 31 * _hash + delta;
        delta = siteId == null ? 0 : siteId.hashCode();
        _hash = 31 * _hash + delta;
        delta = creator == null ? 0 : creator.hashCode();
        _hash = 31 * _hash + delta;
        delta = creatorId == null ? 0 : creatorId.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
