package com.myurldb.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.TwitterGalleryCard;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;


@PersistenceCapable(detachable="true")
@Inheritance(customStrategy = "complete-table")
public class TwitterGalleryCardDataObject extends TwitterCardBaseDataObject implements TwitterGalleryCard
{
    private static final Logger log = Logger.getLogger(TwitterGalleryCardDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(TwitterGalleryCardDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(TwitterGalleryCardDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String image0;

    @Persistent(defaultFetchGroup = "true")
    private String image1;

    @Persistent(defaultFetchGroup = "true")
    private String image2;

    @Persistent(defaultFetchGroup = "true")
    private String image3;

    public TwitterGalleryCardDataObject()
    {
        this(null);
    }
    public TwitterGalleryCardDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public TwitterGalleryCardDataObject(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image0, String image1, String image2, String image3)
    {
        this(guid, card, url, title, description, site, siteId, creator, creatorId, image0, image1, image2, image3, null, null);
    }
    public TwitterGalleryCardDataObject(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image0, String image1, String image2, String image3, Long createdTime, Long modifiedTime)
    {
        super(guid, card, url, title, description, site, siteId, creator, creatorId, createdTime, modifiedTime);
        this.image0 = image0;
        this.image1 = image1;
        this.image2 = image2;
        this.image3 = image3;
    }

//    @Override
//    protected Key createKey()
//    {
//        return TwitterGalleryCardDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return TwitterGalleryCardDataObject.composeKey(getGuid());
    }

    public String getImage0()
    {
        return this.image0;
    }
    public void setImage0(String image0)
    {
        this.image0 = image0;
    }

    public String getImage1()
    {
        return this.image1;
    }
    public void setImage1(String image1)
    {
        this.image1 = image1;
    }

    public String getImage2()
    {
        return this.image2;
    }
    public void setImage2(String image2)
    {
        this.image2 = image2;
    }

    public String getImage3()
    {
        return this.image3;
    }
    public void setImage3(String image3)
    {
        this.image3 = image3;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("image0", this.image0);
        dataMap.put("image1", this.image1);
        dataMap.put("image2", this.image2);
        dataMap.put("image3", this.image3);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        TwitterGalleryCard thatObj = (TwitterGalleryCard) obj;
        if( (this.image0 == null && thatObj.getImage0() != null)
            || (this.image0 != null && thatObj.getImage0() == null)
            || !this.image0.equals(thatObj.getImage0()) ) {
            return false;
        }
        if( (this.image1 == null && thatObj.getImage1() != null)
            || (this.image1 != null && thatObj.getImage1() == null)
            || !this.image1.equals(thatObj.getImage1()) ) {
            return false;
        }
        if( (this.image2 == null && thatObj.getImage2() != null)
            || (this.image2 != null && thatObj.getImage2() == null)
            || !this.image2.equals(thatObj.getImage2()) ) {
            return false;
        }
        if( (this.image3 == null && thatObj.getImage3() != null)
            || (this.image3 != null && thatObj.getImage3() == null)
            || !this.image3.equals(thatObj.getImage3()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = image0 == null ? 0 : image0.hashCode();
        _hash = 31 * _hash + delta;
        delta = image1 == null ? 0 : image1.hashCode();
        _hash = 31 * _hash + delta;
        delta = image2 == null ? 0 : image2.hashCode();
        _hash = 31 * _hash + delta;
        delta = image3 == null ? 0 : image3.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
