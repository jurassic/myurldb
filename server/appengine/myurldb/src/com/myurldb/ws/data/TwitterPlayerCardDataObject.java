package com.myurldb.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.TwitterPlayerCard;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;


@PersistenceCapable(detachable="true")
@Inheritance(customStrategy = "complete-table")
public class TwitterPlayerCardDataObject extends TwitterCardBaseDataObject implements TwitterPlayerCard
{
    private static final Logger log = Logger.getLogger(TwitterPlayerCardDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(TwitterPlayerCardDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(TwitterPlayerCardDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String image;

    @Persistent(defaultFetchGroup = "true")
    private Integer imageWidth;

    @Persistent(defaultFetchGroup = "true")
    private Integer imageHeight;

    @Persistent(defaultFetchGroup = "true")
    private String player;

    @Persistent(defaultFetchGroup = "true")
    private Integer playerWidth;

    @Persistent(defaultFetchGroup = "true")
    private Integer playerHeight;

    @Persistent(defaultFetchGroup = "true")
    private String playerStream;

    @Persistent(defaultFetchGroup = "true")
    private String playerStreamContentType;

    public TwitterPlayerCardDataObject()
    {
        this(null);
    }
    public TwitterPlayerCardDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public TwitterPlayerCardDataObject(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, String player, Integer playerWidth, Integer playerHeight, String playerStream, String playerStreamContentType)
    {
        this(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, player, playerWidth, playerHeight, playerStream, playerStreamContentType, null, null);
    }
    public TwitterPlayerCardDataObject(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, String player, Integer playerWidth, Integer playerHeight, String playerStream, String playerStreamContentType, Long createdTime, Long modifiedTime)
    {
        super(guid, card, url, title, description, site, siteId, creator, creatorId, createdTime, modifiedTime);
        this.image = image;
        this.imageWidth = imageWidth;
        this.imageHeight = imageHeight;
        this.player = player;
        this.playerWidth = playerWidth;
        this.playerHeight = playerHeight;
        this.playerStream = playerStream;
        this.playerStreamContentType = playerStreamContentType;
    }

//    @Override
//    protected Key createKey()
//    {
//        return TwitterPlayerCardDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return TwitterPlayerCardDataObject.composeKey(getGuid());
    }

    public String getImage()
    {
        return this.image;
    }
    public void setImage(String image)
    {
        this.image = image;
    }

    public Integer getImageWidth()
    {
        return this.imageWidth;
    }
    public void setImageWidth(Integer imageWidth)
    {
        this.imageWidth = imageWidth;
    }

    public Integer getImageHeight()
    {
        return this.imageHeight;
    }
    public void setImageHeight(Integer imageHeight)
    {
        this.imageHeight = imageHeight;
    }

    public String getPlayer()
    {
        return this.player;
    }
    public void setPlayer(String player)
    {
        this.player = player;
    }

    public Integer getPlayerWidth()
    {
        return this.playerWidth;
    }
    public void setPlayerWidth(Integer playerWidth)
    {
        this.playerWidth = playerWidth;
    }

    public Integer getPlayerHeight()
    {
        return this.playerHeight;
    }
    public void setPlayerHeight(Integer playerHeight)
    {
        this.playerHeight = playerHeight;
    }

    public String getPlayerStream()
    {
        return this.playerStream;
    }
    public void setPlayerStream(String playerStream)
    {
        this.playerStream = playerStream;
    }

    public String getPlayerStreamContentType()
    {
        return this.playerStreamContentType;
    }
    public void setPlayerStreamContentType(String playerStreamContentType)
    {
        this.playerStreamContentType = playerStreamContentType;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("image", this.image);
        dataMap.put("imageWidth", this.imageWidth);
        dataMap.put("imageHeight", this.imageHeight);
        dataMap.put("player", this.player);
        dataMap.put("playerWidth", this.playerWidth);
        dataMap.put("playerHeight", this.playerHeight);
        dataMap.put("playerStream", this.playerStream);
        dataMap.put("playerStreamContentType", this.playerStreamContentType);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        TwitterPlayerCard thatObj = (TwitterPlayerCard) obj;
        if( (this.image == null && thatObj.getImage() != null)
            || (this.image != null && thatObj.getImage() == null)
            || !this.image.equals(thatObj.getImage()) ) {
            return false;
        }
        if( (this.imageWidth == null && thatObj.getImageWidth() != null)
            || (this.imageWidth != null && thatObj.getImageWidth() == null)
            || !this.imageWidth.equals(thatObj.getImageWidth()) ) {
            return false;
        }
        if( (this.imageHeight == null && thatObj.getImageHeight() != null)
            || (this.imageHeight != null && thatObj.getImageHeight() == null)
            || !this.imageHeight.equals(thatObj.getImageHeight()) ) {
            return false;
        }
        if( (this.player == null && thatObj.getPlayer() != null)
            || (this.player != null && thatObj.getPlayer() == null)
            || !this.player.equals(thatObj.getPlayer()) ) {
            return false;
        }
        if( (this.playerWidth == null && thatObj.getPlayerWidth() != null)
            || (this.playerWidth != null && thatObj.getPlayerWidth() == null)
            || !this.playerWidth.equals(thatObj.getPlayerWidth()) ) {
            return false;
        }
        if( (this.playerHeight == null && thatObj.getPlayerHeight() != null)
            || (this.playerHeight != null && thatObj.getPlayerHeight() == null)
            || !this.playerHeight.equals(thatObj.getPlayerHeight()) ) {
            return false;
        }
        if( (this.playerStream == null && thatObj.getPlayerStream() != null)
            || (this.playerStream != null && thatObj.getPlayerStream() == null)
            || !this.playerStream.equals(thatObj.getPlayerStream()) ) {
            return false;
        }
        if( (this.playerStreamContentType == null && thatObj.getPlayerStreamContentType() != null)
            || (this.playerStreamContentType != null && thatObj.getPlayerStreamContentType() == null)
            || !this.playerStreamContentType.equals(thatObj.getPlayerStreamContentType()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = image == null ? 0 : image.hashCode();
        _hash = 31 * _hash + delta;
        delta = imageWidth == null ? 0 : imageWidth.hashCode();
        _hash = 31 * _hash + delta;
        delta = imageHeight == null ? 0 : imageHeight.hashCode();
        _hash = 31 * _hash + delta;
        delta = player == null ? 0 : player.hashCode();
        _hash = 31 * _hash + delta;
        delta = playerWidth == null ? 0 : playerWidth.hashCode();
        _hash = 31 * _hash + delta;
        delta = playerHeight == null ? 0 : playerHeight.hashCode();
        _hash = 31 * _hash + delta;
        delta = playerStream == null ? 0 : playerStream.hashCode();
        _hash = 31 * _hash + delta;
        delta = playerStreamContentType == null ? 0 : playerStreamContentType.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
