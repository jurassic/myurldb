package com.myurldb.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.TwitterProductCard;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;


@PersistenceCapable(detachable="true")
@Inheritance(customStrategy = "complete-table")
public class TwitterProductCardDataObject extends TwitterCardBaseDataObject implements TwitterProductCard
{
    private static final Logger log = Logger.getLogger(TwitterProductCardDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(TwitterProductCardDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(TwitterProductCardDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String image;

    @Persistent(defaultFetchGroup = "true")
    private Integer imageWidth;

    @Persistent(defaultFetchGroup = "true")
    private Integer imageHeight;

    @Persistent(defaultFetchGroup = "false")
    @Embedded(members = {
        @Persistent(name="data", columns=@Column(name="data1data")),
        @Persistent(name="label", columns=@Column(name="data1label")),
    })
    private TwitterCardProductDataDataObject data1;

    @Persistent(defaultFetchGroup = "false")
    @Embedded(members = {
        @Persistent(name="data", columns=@Column(name="data2data")),
        @Persistent(name="label", columns=@Column(name="data2label")),
    })
    private TwitterCardProductDataDataObject data2;

    public TwitterProductCardDataObject()
    {
        this(null);
    }
    public TwitterProductCardDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public TwitterProductCardDataObject(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, TwitterCardProductData data1, TwitterCardProductData data2)
    {
        this(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, data1, data2, null, null);
    }
    public TwitterProductCardDataObject(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, TwitterCardProductData data1, TwitterCardProductData data2, Long createdTime, Long modifiedTime)
    {
        super(guid, card, url, title, description, site, siteId, creator, creatorId, createdTime, modifiedTime);
        this.image = image;
        this.imageWidth = imageWidth;
        this.imageHeight = imageHeight;
        if(data1 != null) {
            this.data1 = new TwitterCardProductDataDataObject(data1.getData(), data1.getLabel());
        } else {
            this.data1 = null;
        }
        if(data2 != null) {
            this.data2 = new TwitterCardProductDataDataObject(data2.getData(), data2.getLabel());
        } else {
            this.data2 = null;
        }
    }

//    @Override
//    protected Key createKey()
//    {
//        return TwitterProductCardDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return TwitterProductCardDataObject.composeKey(getGuid());
    }

    public String getImage()
    {
        return this.image;
    }
    public void setImage(String image)
    {
        this.image = image;
    }

    public Integer getImageWidth()
    {
        return this.imageWidth;
    }
    public void setImageWidth(Integer imageWidth)
    {
        this.imageWidth = imageWidth;
    }

    public Integer getImageHeight()
    {
        return this.imageHeight;
    }
    public void setImageHeight(Integer imageHeight)
    {
        this.imageHeight = imageHeight;
    }

    public TwitterCardProductData getData1()
    {
        return this.data1;
    }
    public void setData1(TwitterCardProductData data1)
    {
        if(data1 == null) {
            this.data1 = null;
            log.log(Level.INFO, "TwitterProductCardDataObject.setData1(TwitterCardProductData data1): Arg data1 is null.");            
        } else if(data1 instanceof TwitterCardProductDataDataObject) {
            this.data1 = (TwitterCardProductDataDataObject) data1;
        } else if(data1 instanceof TwitterCardProductData) {
            this.data1 = new TwitterCardProductDataDataObject(data1.getData(), data1.getLabel());
        } else {
            this.data1 = new TwitterCardProductDataDataObject();   // ????
            log.log(Level.WARNING, "TwitterProductCardDataObject.setData1(TwitterCardProductData data1): Arg data1 is of an invalid type.");
        }
    }

    public TwitterCardProductData getData2()
    {
        return this.data2;
    }
    public void setData2(TwitterCardProductData data2)
    {
        if(data2 == null) {
            this.data2 = null;
            log.log(Level.INFO, "TwitterProductCardDataObject.setData2(TwitterCardProductData data2): Arg data2 is null.");            
        } else if(data2 instanceof TwitterCardProductDataDataObject) {
            this.data2 = (TwitterCardProductDataDataObject) data2;
        } else if(data2 instanceof TwitterCardProductData) {
            this.data2 = new TwitterCardProductDataDataObject(data2.getData(), data2.getLabel());
        } else {
            this.data2 = new TwitterCardProductDataDataObject();   // ????
            log.log(Level.WARNING, "TwitterProductCardDataObject.setData2(TwitterCardProductData data2): Arg data2 is of an invalid type.");
        }
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("image", this.image);
        dataMap.put("imageWidth", this.imageWidth);
        dataMap.put("imageHeight", this.imageHeight);
        dataMap.put("data1", this.data1);
        dataMap.put("data2", this.data2);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        TwitterProductCard thatObj = (TwitterProductCard) obj;
        if( (this.image == null && thatObj.getImage() != null)
            || (this.image != null && thatObj.getImage() == null)
            || !this.image.equals(thatObj.getImage()) ) {
            return false;
        }
        if( (this.imageWidth == null && thatObj.getImageWidth() != null)
            || (this.imageWidth != null && thatObj.getImageWidth() == null)
            || !this.imageWidth.equals(thatObj.getImageWidth()) ) {
            return false;
        }
        if( (this.imageHeight == null && thatObj.getImageHeight() != null)
            || (this.imageHeight != null && thatObj.getImageHeight() == null)
            || !this.imageHeight.equals(thatObj.getImageHeight()) ) {
            return false;
        }
        if( (this.data1 == null && thatObj.getData1() != null)
            || (this.data1 != null && thatObj.getData1() == null)
            || !this.data1.equals(thatObj.getData1()) ) {
            return false;
        }
        if( (this.data2 == null && thatObj.getData2() != null)
            || (this.data2 != null && thatObj.getData2() == null)
            || !this.data2.equals(thatObj.getData2()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = image == null ? 0 : image.hashCode();
        _hash = 31 * _hash + delta;
        delta = imageWidth == null ? 0 : imageWidth.hashCode();
        _hash = 31 * _hash + delta;
        delta = imageHeight == null ? 0 : imageHeight.hashCode();
        _hash = 31 * _hash + delta;
        delta = data1 == null ? 0 : data1.hashCode();
        _hash = 31 * _hash + delta;
        delta = data2 == null ? 0 : data2.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
