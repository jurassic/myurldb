package com.myurldb.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.TwitterSummaryCard;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;


@PersistenceCapable(detachable="true")
@Inheritance(customStrategy = "complete-table")
public class TwitterSummaryCardDataObject extends TwitterCardBaseDataObject implements TwitterSummaryCard
{
    private static final Logger log = Logger.getLogger(TwitterSummaryCardDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(TwitterSummaryCardDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(TwitterSummaryCardDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String image;

    @Persistent(defaultFetchGroup = "true")
    private Integer imageWidth;

    @Persistent(defaultFetchGroup = "true")
    private Integer imageHeight;

    public TwitterSummaryCardDataObject()
    {
        this(null);
    }
    public TwitterSummaryCardDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public TwitterSummaryCardDataObject(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight)
    {
        this(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, null, null);
    }
    public TwitterSummaryCardDataObject(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, Long createdTime, Long modifiedTime)
    {
        super(guid, card, url, title, description, site, siteId, creator, creatorId, createdTime, modifiedTime);
        this.image = image;
        this.imageWidth = imageWidth;
        this.imageHeight = imageHeight;
    }

//    @Override
//    protected Key createKey()
//    {
//        return TwitterSummaryCardDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return TwitterSummaryCardDataObject.composeKey(getGuid());
    }

    public String getImage()
    {
        return this.image;
    }
    public void setImage(String image)
    {
        this.image = image;
    }

    public Integer getImageWidth()
    {
        return this.imageWidth;
    }
    public void setImageWidth(Integer imageWidth)
    {
        this.imageWidth = imageWidth;
    }

    public Integer getImageHeight()
    {
        return this.imageHeight;
    }
    public void setImageHeight(Integer imageHeight)
    {
        this.imageHeight = imageHeight;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("image", this.image);
        dataMap.put("imageWidth", this.imageWidth);
        dataMap.put("imageHeight", this.imageHeight);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        TwitterSummaryCard thatObj = (TwitterSummaryCard) obj;
        if( (this.image == null && thatObj.getImage() != null)
            || (this.image != null && thatObj.getImage() == null)
            || !this.image.equals(thatObj.getImage()) ) {
            return false;
        }
        if( (this.imageWidth == null && thatObj.getImageWidth() != null)
            || (this.imageWidth != null && thatObj.getImageWidth() == null)
            || !this.imageWidth.equals(thatObj.getImageWidth()) ) {
            return false;
        }
        if( (this.imageHeight == null && thatObj.getImageHeight() != null)
            || (this.imageHeight != null && thatObj.getImageHeight() == null)
            || !this.imageHeight.equals(thatObj.getImageHeight()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = image == null ? 0 : image.hashCode();
        _hash = 31 * _hash + delta;
        delta = imageWidth == null ? 0 : imageWidth.hashCode();
        _hash = 31 * _hash + delta;
        delta = imageHeight == null ? 0 : imageHeight.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
