package com.myurldb.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.myurldb.ws.HashedPasswordStruct;
import com.myurldb.ws.UserPassword;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;


@PersistenceCapable(detachable="true")
public class UserPasswordDataObject extends KeyedDataObject implements UserPassword
{
    private static final Logger log = Logger.getLogger(UserPasswordDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(UserPasswordDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(UserPasswordDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String guid;

    @Persistent(defaultFetchGroup = "true")
    private String admin;

    @Persistent(defaultFetchGroup = "true")
    private String user;

    @Persistent(defaultFetchGroup = "true")
    private String username;

    @Persistent(defaultFetchGroup = "true")
    private String email;

    @Persistent(defaultFetchGroup = "true")
    private String openId;

    @Persistent(defaultFetchGroup = "false")
    @Embedded(members = {
        @Persistent(name="uuid", columns=@Column(name="passworduuid")),
        @Persistent(name="plainText", columns=@Column(name="passwordplainText")),
        @Persistent(name="hashedText", columns=@Column(name="passwordhashedText")),
        @Persistent(name="salt", columns=@Column(name="passwordsalt")),
        @Persistent(name="algorithm", columns=@Column(name="passwordalgorithm")),
    })
    private HashedPasswordStructDataObject password;

    @Persistent(defaultFetchGroup = "true")
    private Boolean resetRequired;

    @Persistent(defaultFetchGroup = "true")
    private String challengeQuestion;

    @Persistent(defaultFetchGroup = "true")
    private String challengeAnswer;

    @Persistent(defaultFetchGroup = "true")
    private String status;

    @Persistent(defaultFetchGroup = "true")
    private Long lastResetTime;

    @Persistent(defaultFetchGroup = "true")
    private Long expirationTime;

    public UserPasswordDataObject()
    {
        this(null);
    }
    public UserPasswordDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public UserPasswordDataObject(String guid, String admin, String user, String username, String email, String openId, HashedPasswordStruct password, Boolean resetRequired, String challengeQuestion, String challengeAnswer, String status, Long lastResetTime, Long expirationTime)
    {
        this(guid, admin, user, username, email, openId, password, resetRequired, challengeQuestion, challengeAnswer, status, lastResetTime, expirationTime, null, null);
    }
    public UserPasswordDataObject(String guid, String admin, String user, String username, String email, String openId, HashedPasswordStruct password, Boolean resetRequired, String challengeQuestion, String challengeAnswer, String status, Long lastResetTime, Long expirationTime, Long createdTime, Long modifiedTime)
    {
        super(createdTime, modifiedTime);
        generatePK(guid);
        this.admin = admin;
        this.user = user;
        this.username = username;
        this.email = email;
        this.openId = openId;
        if(password != null) {
            this.password = new HashedPasswordStructDataObject(password.getUuid(), password.getPlainText(), password.getHashedText(), password.getSalt(), password.getAlgorithm());
        } else {
            this.password = null;
        }
        this.resetRequired = resetRequired;
        this.challengeQuestion = challengeQuestion;
        this.challengeAnswer = challengeAnswer;
        this.status = status;
        this.lastResetTime = lastResetTime;
        this.expirationTime = expirationTime;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        generatePK(guid);
    }
    protected void generatePK(String guid)
    {
        if(guid == null) {
            this.guid = GUID.generate();  // temporary
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to a new guid = " + this.guid);
        } else {
            this.guid = guid;
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to guid = " + this.guid);
        }
        rebuildKey();        
    }

//    @Override
//    protected Key createKey()
//    {
//        return UserPasswordDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return UserPasswordDataObject.composeKey(getGuid());
    }

    public String getAdmin()
    {
        return this.admin;
    }
    public void setAdmin(String admin)
    {
        this.admin = admin;
    }

    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    public String getUsername()
    {
        return this.username;
    }
    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getEmail()
    {
        return this.email;
    }
    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getOpenId()
    {
        return this.openId;
    }
    public void setOpenId(String openId)
    {
        this.openId = openId;
    }

    public HashedPasswordStruct getPassword()
    {
        return this.password;
    }
    public void setPassword(HashedPasswordStruct password)
    {
        if(password == null) {
            this.password = null;
            log.log(Level.INFO, "UserPasswordDataObject.setPassword(HashedPasswordStruct password): Arg password is null.");            
        } else if(password instanceof HashedPasswordStructDataObject) {
            this.password = (HashedPasswordStructDataObject) password;
        } else if(password instanceof HashedPasswordStruct) {
            this.password = new HashedPasswordStructDataObject(password.getUuid(), password.getPlainText(), password.getHashedText(), password.getSalt(), password.getAlgorithm());
        } else {
            this.password = new HashedPasswordStructDataObject();   // ????
            log.log(Level.WARNING, "UserPasswordDataObject.setPassword(HashedPasswordStruct password): Arg password is of an invalid type.");
        }
    }

    public Boolean isResetRequired()
    {
        return this.resetRequired;
    }
    public void setResetRequired(Boolean resetRequired)
    {
        this.resetRequired = resetRequired;
    }

    public String getChallengeQuestion()
    {
        return this.challengeQuestion;
    }
    public void setChallengeQuestion(String challengeQuestion)
    {
        this.challengeQuestion = challengeQuestion;
    }

    public String getChallengeAnswer()
    {
        return this.challengeAnswer;
    }
    public void setChallengeAnswer(String challengeAnswer)
    {
        this.challengeAnswer = challengeAnswer;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public Long getLastResetTime()
    {
        return this.lastResetTime;
    }
    public void setLastResetTime(Long lastResetTime)
    {
        this.lastResetTime = lastResetTime;
    }

    public Long getExpirationTime()
    {
        return this.expirationTime;
    }
    public void setExpirationTime(Long expirationTime)
    {
        this.expirationTime = expirationTime;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("guid", this.guid);
        dataMap.put("admin", this.admin);
        dataMap.put("user", this.user);
        dataMap.put("username", this.username);
        dataMap.put("email", this.email);
        dataMap.put("openId", this.openId);
        dataMap.put("password", this.password);
        dataMap.put("resetRequired", this.resetRequired);
        dataMap.put("challengeQuestion", this.challengeQuestion);
        dataMap.put("challengeAnswer", this.challengeAnswer);
        dataMap.put("status", this.status);
        dataMap.put("lastResetTime", this.lastResetTime);
        dataMap.put("expirationTime", this.expirationTime);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        UserPassword thatObj = (UserPassword) obj;
        if( (this.guid == null && thatObj.getGuid() != null)
            || (this.guid != null && thatObj.getGuid() == null)
            || !this.guid.equals(thatObj.getGuid()) ) {
            return false;
        }
        if( (this.admin == null && thatObj.getAdmin() != null)
            || (this.admin != null && thatObj.getAdmin() == null)
            || !this.admin.equals(thatObj.getAdmin()) ) {
            return false;
        }
        if( (this.user == null && thatObj.getUser() != null)
            || (this.user != null && thatObj.getUser() == null)
            || !this.user.equals(thatObj.getUser()) ) {
            return false;
        }
        if( (this.username == null && thatObj.getUsername() != null)
            || (this.username != null && thatObj.getUsername() == null)
            || !this.username.equals(thatObj.getUsername()) ) {
            return false;
        }
        if( (this.email == null && thatObj.getEmail() != null)
            || (this.email != null && thatObj.getEmail() == null)
            || !this.email.equals(thatObj.getEmail()) ) {
            return false;
        }
        if( (this.openId == null && thatObj.getOpenId() != null)
            || (this.openId != null && thatObj.getOpenId() == null)
            || !this.openId.equals(thatObj.getOpenId()) ) {
            return false;
        }
        if( (this.password == null && thatObj.getPassword() != null)
            || (this.password != null && thatObj.getPassword() == null)
            || !this.password.equals(thatObj.getPassword()) ) {
            return false;
        }
        if( (this.resetRequired == null && thatObj.isResetRequired() != null)
            || (this.resetRequired != null && thatObj.isResetRequired() == null)
            || !this.resetRequired.equals(thatObj.isResetRequired()) ) {
            return false;
        }
        if( (this.challengeQuestion == null && thatObj.getChallengeQuestion() != null)
            || (this.challengeQuestion != null && thatObj.getChallengeQuestion() == null)
            || !this.challengeQuestion.equals(thatObj.getChallengeQuestion()) ) {
            return false;
        }
        if( (this.challengeAnswer == null && thatObj.getChallengeAnswer() != null)
            || (this.challengeAnswer != null && thatObj.getChallengeAnswer() == null)
            || !this.challengeAnswer.equals(thatObj.getChallengeAnswer()) ) {
            return false;
        }
        if( (this.status == null && thatObj.getStatus() != null)
            || (this.status != null && thatObj.getStatus() == null)
            || !this.status.equals(thatObj.getStatus()) ) {
            return false;
        }
        if( (this.lastResetTime == null && thatObj.getLastResetTime() != null)
            || (this.lastResetTime != null && thatObj.getLastResetTime() == null)
            || !this.lastResetTime.equals(thatObj.getLastResetTime()) ) {
            return false;
        }
        if( (this.expirationTime == null && thatObj.getExpirationTime() != null)
            || (this.expirationTime != null && thatObj.getExpirationTime() == null)
            || !this.expirationTime.equals(thatObj.getExpirationTime()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = admin == null ? 0 : admin.hashCode();
        _hash = 31 * _hash + delta;
        delta = user == null ? 0 : user.hashCode();
        _hash = 31 * _hash + delta;
        delta = username == null ? 0 : username.hashCode();
        _hash = 31 * _hash + delta;
        delta = email == null ? 0 : email.hashCode();
        _hash = 31 * _hash + delta;
        delta = openId == null ? 0 : openId.hashCode();
        _hash = 31 * _hash + delta;
        delta = password == null ? 0 : password.hashCode();
        _hash = 31 * _hash + delta;
        delta = resetRequired == null ? 0 : resetRequired.hashCode();
        _hash = 31 * _hash + delta;
        delta = challengeQuestion == null ? 0 : challengeQuestion.hashCode();
        _hash = 31 * _hash + delta;
        delta = challengeAnswer == null ? 0 : challengeAnswer.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = lastResetTime == null ? 0 : lastResetTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = expirationTime == null ? 0 : expirationTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
