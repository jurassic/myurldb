package com.myurldb.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.myurldb.ws.UserResourcePermission;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;


@PersistenceCapable(detachable="true")
public class UserResourcePermissionDataObject extends KeyedDataObject implements UserResourcePermission
{
    private static final Logger log = Logger.getLogger(UserResourcePermissionDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(UserResourcePermissionDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(UserResourcePermissionDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String guid;

    @Persistent(defaultFetchGroup = "true")
    private String user;

    @Persistent(defaultFetchGroup = "true")
    private String permissionName;

    @Persistent(defaultFetchGroup = "true")
    private String resource;

    @Persistent(defaultFetchGroup = "true")
    private String instance;

    @Persistent(defaultFetchGroup = "true")
    private String action;

    @Persistent(defaultFetchGroup = "true")
    private Boolean permitted;

    @Persistent(defaultFetchGroup = "true")
    private String status;

    public UserResourcePermissionDataObject()
    {
        this(null);
    }
    public UserResourcePermissionDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null);
    }
    public UserResourcePermissionDataObject(String guid, String user, String permissionName, String resource, String instance, String action, Boolean permitted, String status)
    {
        this(guid, user, permissionName, resource, instance, action, permitted, status, null, null);
    }
    public UserResourcePermissionDataObject(String guid, String user, String permissionName, String resource, String instance, String action, Boolean permitted, String status, Long createdTime, Long modifiedTime)
    {
        super(createdTime, modifiedTime);
        generatePK(guid);
        this.user = user;
        this.permissionName = permissionName;
        this.resource = resource;
        this.instance = instance;
        this.action = action;
        this.permitted = permitted;
        this.status = status;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        generatePK(guid);
    }
    protected void generatePK(String guid)
    {
        if(guid == null) {
            this.guid = GUID.generate();  // temporary
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to a new guid = " + this.guid);
        } else {
            this.guid = guid;
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to guid = " + this.guid);
        }
        rebuildKey();        
    }

//    @Override
//    protected Key createKey()
//    {
//        return UserResourcePermissionDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return UserResourcePermissionDataObject.composeKey(getGuid());
    }

    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    public String getPermissionName()
    {
        return this.permissionName;
    }
    public void setPermissionName(String permissionName)
    {
        this.permissionName = permissionName;
    }

    public String getResource()
    {
        return this.resource;
    }
    public void setResource(String resource)
    {
        this.resource = resource;
    }

    public String getInstance()
    {
        return this.instance;
    }
    public void setInstance(String instance)
    {
        this.instance = instance;
    }

    public String getAction()
    {
        return this.action;
    }
    public void setAction(String action)
    {
        this.action = action;
    }

    public Boolean isPermitted()
    {
        return this.permitted;
    }
    public void setPermitted(Boolean permitted)
    {
        this.permitted = permitted;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("guid", this.guid);
        dataMap.put("user", this.user);
        dataMap.put("permissionName", this.permissionName);
        dataMap.put("resource", this.resource);
        dataMap.put("instance", this.instance);
        dataMap.put("action", this.action);
        dataMap.put("permitted", this.permitted);
        dataMap.put("status", this.status);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        UserResourcePermission thatObj = (UserResourcePermission) obj;
        if( (this.guid == null && thatObj.getGuid() != null)
            || (this.guid != null && thatObj.getGuid() == null)
            || !this.guid.equals(thatObj.getGuid()) ) {
            return false;
        }
        if( (this.user == null && thatObj.getUser() != null)
            || (this.user != null && thatObj.getUser() == null)
            || !this.user.equals(thatObj.getUser()) ) {
            return false;
        }
        if( (this.permissionName == null && thatObj.getPermissionName() != null)
            || (this.permissionName != null && thatObj.getPermissionName() == null)
            || !this.permissionName.equals(thatObj.getPermissionName()) ) {
            return false;
        }
        if( (this.resource == null && thatObj.getResource() != null)
            || (this.resource != null && thatObj.getResource() == null)
            || !this.resource.equals(thatObj.getResource()) ) {
            return false;
        }
        if( (this.instance == null && thatObj.getInstance() != null)
            || (this.instance != null && thatObj.getInstance() == null)
            || !this.instance.equals(thatObj.getInstance()) ) {
            return false;
        }
        if( (this.action == null && thatObj.getAction() != null)
            || (this.action != null && thatObj.getAction() == null)
            || !this.action.equals(thatObj.getAction()) ) {
            return false;
        }
        if( (this.permitted == null && thatObj.isPermitted() != null)
            || (this.permitted != null && thatObj.isPermitted() == null)
            || !this.permitted.equals(thatObj.isPermitted()) ) {
            return false;
        }
        if( (this.status == null && thatObj.getStatus() != null)
            || (this.status != null && thatObj.getStatus() == null)
            || !this.status.equals(thatObj.getStatus()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = user == null ? 0 : user.hashCode();
        _hash = 31 * _hash + delta;
        delta = permissionName == null ? 0 : permissionName.hashCode();
        _hash = 31 * _hash + delta;
        delta = resource == null ? 0 : resource.hashCode();
        _hash = 31 * _hash + delta;
        delta = instance == null ? 0 : instance.hashCode();
        _hash = 31 * _hash + delta;
        delta = action == null ? 0 : action.hashCode();
        _hash = 31 * _hash + delta;
        delta = permitted == null ? 0 : permitted.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
