package com.myurldb.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.myurldb.ws.UserSetting;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;


@PersistenceCapable(detachable="true")
@Inheritance(customStrategy = "complete-table")
public class UserSettingDataObject extends PersonalSettingDataObject implements UserSetting
{
    private static final Logger log = Logger.getLogger(UserSettingDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(UserSettingDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(UserSettingDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String user;

    @Persistent(defaultFetchGroup = "true")
    private String homePage;

    @Persistent(defaultFetchGroup = "true")
    private String selfBio;

    @Persistent(defaultFetchGroup = "true")
    private String userUrlDomain;

    @Persistent(defaultFetchGroup = "true")
    private String userUrlDomainNormalized;

    @Persistent(defaultFetchGroup = "true")
    private Boolean autoRedirectEnabled;

    @Persistent(defaultFetchGroup = "true")
    private Boolean viewEnabled;

    @Persistent(defaultFetchGroup = "true")
    private Boolean shareEnabled;

    @Persistent(defaultFetchGroup = "true")
    private String defaultDomain;

    @Persistent(defaultFetchGroup = "true")
    private String defaultTokenType;

    @Persistent(defaultFetchGroup = "true")
    private String defaultRedirectType;

    @Persistent(defaultFetchGroup = "true")
    private Long defaultFlashDuration;

    @Persistent(defaultFetchGroup = "true")
    private String defaultAccessType;

    @Persistent(defaultFetchGroup = "true")
    private String defaultViewType;

    @Persistent(defaultFetchGroup = "true")
    private String defaultShareType;

    @Persistent(defaultFetchGroup = "true")
    private String defaultPassphrase;

    @Persistent(defaultFetchGroup = "true")
    private String defaultSignature;

    @Persistent(defaultFetchGroup = "true")
    private Boolean useSignature;

    @Persistent(defaultFetchGroup = "true")
    private String defaultEmblem;

    @Persistent(defaultFetchGroup = "true")
    private Boolean useEmblem;

    @Persistent(defaultFetchGroup = "true")
    private String defaultBackground;

    @Persistent(defaultFetchGroup = "true")
    private Boolean useBackground;

    @Persistent(defaultFetchGroup = "true")
    private String sponsorUrl;

    @Persistent(defaultFetchGroup = "true")
    private String sponsorBanner;

    @Persistent(defaultFetchGroup = "true")
    private String sponsorNote;

    @Persistent(defaultFetchGroup = "true")
    private Boolean useSponsor;

    @Persistent(defaultFetchGroup = "true")
    private String extraParams;

    @Persistent(defaultFetchGroup = "true")
    private Long expirationDuration;

    public UserSettingDataObject()
    {
        this(null);
    }
    public UserSettingDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public UserSettingDataObject(String guid, String user, String homePage, String selfBio, String userUrlDomain, String userUrlDomainNormalized, Boolean autoRedirectEnabled, Boolean viewEnabled, Boolean shareEnabled, String defaultDomain, String defaultTokenType, String defaultRedirectType, Long defaultFlashDuration, String defaultAccessType, String defaultViewType, String defaultShareType, String defaultPassphrase, String defaultSignature, Boolean useSignature, String defaultEmblem, Boolean useEmblem, String defaultBackground, Boolean useBackground, String sponsorUrl, String sponsorBanner, String sponsorNote, Boolean useSponsor, String extraParams, Long expirationDuration)
    {
        this(guid, user, homePage, selfBio, userUrlDomain, userUrlDomainNormalized, autoRedirectEnabled, viewEnabled, shareEnabled, defaultDomain, defaultTokenType, defaultRedirectType, defaultFlashDuration, defaultAccessType, defaultViewType, defaultShareType, defaultPassphrase, defaultSignature, useSignature, defaultEmblem, useEmblem, defaultBackground, useBackground, sponsorUrl, sponsorBanner, sponsorNote, useSponsor, extraParams, expirationDuration, null, null);
    }
    public UserSettingDataObject(String guid, String user, String homePage, String selfBio, String userUrlDomain, String userUrlDomainNormalized, Boolean autoRedirectEnabled, Boolean viewEnabled, Boolean shareEnabled, String defaultDomain, String defaultTokenType, String defaultRedirectType, Long defaultFlashDuration, String defaultAccessType, String defaultViewType, String defaultShareType, String defaultPassphrase, String defaultSignature, Boolean useSignature, String defaultEmblem, Boolean useEmblem, String defaultBackground, Boolean useBackground, String sponsorUrl, String sponsorBanner, String sponsorNote, Boolean useSponsor, String extraParams, Long expirationDuration, Long createdTime, Long modifiedTime)
    {
        super(guid, createdTime, modifiedTime);
        this.user = user;
        this.homePage = homePage;
        this.selfBio = selfBio;
        this.userUrlDomain = userUrlDomain;
        this.userUrlDomainNormalized = userUrlDomainNormalized;
        this.autoRedirectEnabled = autoRedirectEnabled;
        this.viewEnabled = viewEnabled;
        this.shareEnabled = shareEnabled;
        this.defaultDomain = defaultDomain;
        this.defaultTokenType = defaultTokenType;
        this.defaultRedirectType = defaultRedirectType;
        this.defaultFlashDuration = defaultFlashDuration;
        this.defaultAccessType = defaultAccessType;
        this.defaultViewType = defaultViewType;
        this.defaultShareType = defaultShareType;
        this.defaultPassphrase = defaultPassphrase;
        this.defaultSignature = defaultSignature;
        this.useSignature = useSignature;
        this.defaultEmblem = defaultEmblem;
        this.useEmblem = useEmblem;
        this.defaultBackground = defaultBackground;
        this.useBackground = useBackground;
        this.sponsorUrl = sponsorUrl;
        this.sponsorBanner = sponsorBanner;
        this.sponsorNote = sponsorNote;
        this.useSponsor = useSponsor;
        this.extraParams = extraParams;
        this.expirationDuration = expirationDuration;
    }

//    @Override
//    protected Key createKey()
//    {
//        return UserSettingDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return UserSettingDataObject.composeKey(getGuid());
    }

    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    public String getHomePage()
    {
        return this.homePage;
    }
    public void setHomePage(String homePage)
    {
        this.homePage = homePage;
    }

    public String getSelfBio()
    {
        return this.selfBio;
    }
    public void setSelfBio(String selfBio)
    {
        this.selfBio = selfBio;
    }

    public String getUserUrlDomain()
    {
        return this.userUrlDomain;
    }
    public void setUserUrlDomain(String userUrlDomain)
    {
        this.userUrlDomain = userUrlDomain;
    }

    public String getUserUrlDomainNormalized()
    {
        return this.userUrlDomainNormalized;
    }
    public void setUserUrlDomainNormalized(String userUrlDomainNormalized)
    {
        this.userUrlDomainNormalized = userUrlDomainNormalized;
    }

    public Boolean isAutoRedirectEnabled()
    {
        return this.autoRedirectEnabled;
    }
    public void setAutoRedirectEnabled(Boolean autoRedirectEnabled)
    {
        this.autoRedirectEnabled = autoRedirectEnabled;
    }

    public Boolean isViewEnabled()
    {
        return this.viewEnabled;
    }
    public void setViewEnabled(Boolean viewEnabled)
    {
        this.viewEnabled = viewEnabled;
    }

    public Boolean isShareEnabled()
    {
        return this.shareEnabled;
    }
    public void setShareEnabled(Boolean shareEnabled)
    {
        this.shareEnabled = shareEnabled;
    }

    public String getDefaultDomain()
    {
        return this.defaultDomain;
    }
    public void setDefaultDomain(String defaultDomain)
    {
        this.defaultDomain = defaultDomain;
    }

    public String getDefaultTokenType()
    {
        return this.defaultTokenType;
    }
    public void setDefaultTokenType(String defaultTokenType)
    {
        this.defaultTokenType = defaultTokenType;
    }

    public String getDefaultRedirectType()
    {
        return this.defaultRedirectType;
    }
    public void setDefaultRedirectType(String defaultRedirectType)
    {
        this.defaultRedirectType = defaultRedirectType;
    }

    public Long getDefaultFlashDuration()
    {
        return this.defaultFlashDuration;
    }
    public void setDefaultFlashDuration(Long defaultFlashDuration)
    {
        this.defaultFlashDuration = defaultFlashDuration;
    }

    public String getDefaultAccessType()
    {
        return this.defaultAccessType;
    }
    public void setDefaultAccessType(String defaultAccessType)
    {
        this.defaultAccessType = defaultAccessType;
    }

    public String getDefaultViewType()
    {
        return this.defaultViewType;
    }
    public void setDefaultViewType(String defaultViewType)
    {
        this.defaultViewType = defaultViewType;
    }

    public String getDefaultShareType()
    {
        return this.defaultShareType;
    }
    public void setDefaultShareType(String defaultShareType)
    {
        this.defaultShareType = defaultShareType;
    }

    public String getDefaultPassphrase()
    {
        return this.defaultPassphrase;
    }
    public void setDefaultPassphrase(String defaultPassphrase)
    {
        this.defaultPassphrase = defaultPassphrase;
    }

    public String getDefaultSignature()
    {
        return this.defaultSignature;
    }
    public void setDefaultSignature(String defaultSignature)
    {
        this.defaultSignature = defaultSignature;
    }

    public Boolean isUseSignature()
    {
        return this.useSignature;
    }
    public void setUseSignature(Boolean useSignature)
    {
        this.useSignature = useSignature;
    }

    public String getDefaultEmblem()
    {
        return this.defaultEmblem;
    }
    public void setDefaultEmblem(String defaultEmblem)
    {
        this.defaultEmblem = defaultEmblem;
    }

    public Boolean isUseEmblem()
    {
        return this.useEmblem;
    }
    public void setUseEmblem(Boolean useEmblem)
    {
        this.useEmblem = useEmblem;
    }

    public String getDefaultBackground()
    {
        return this.defaultBackground;
    }
    public void setDefaultBackground(String defaultBackground)
    {
        this.defaultBackground = defaultBackground;
    }

    public Boolean isUseBackground()
    {
        return this.useBackground;
    }
    public void setUseBackground(Boolean useBackground)
    {
        this.useBackground = useBackground;
    }

    public String getSponsorUrl()
    {
        return this.sponsorUrl;
    }
    public void setSponsorUrl(String sponsorUrl)
    {
        this.sponsorUrl = sponsorUrl;
    }

    public String getSponsorBanner()
    {
        return this.sponsorBanner;
    }
    public void setSponsorBanner(String sponsorBanner)
    {
        this.sponsorBanner = sponsorBanner;
    }

    public String getSponsorNote()
    {
        return this.sponsorNote;
    }
    public void setSponsorNote(String sponsorNote)
    {
        this.sponsorNote = sponsorNote;
    }

    public Boolean isUseSponsor()
    {
        return this.useSponsor;
    }
    public void setUseSponsor(Boolean useSponsor)
    {
        this.useSponsor = useSponsor;
    }

    public String getExtraParams()
    {
        return this.extraParams;
    }
    public void setExtraParams(String extraParams)
    {
        this.extraParams = extraParams;
    }

    public Long getExpirationDuration()
    {
        return this.expirationDuration;
    }
    public void setExpirationDuration(Long expirationDuration)
    {
        this.expirationDuration = expirationDuration;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("user", this.user);
        dataMap.put("homePage", this.homePage);
        dataMap.put("selfBio", this.selfBio);
        dataMap.put("userUrlDomain", this.userUrlDomain);
        dataMap.put("userUrlDomainNormalized", this.userUrlDomainNormalized);
        dataMap.put("autoRedirectEnabled", this.autoRedirectEnabled);
        dataMap.put("viewEnabled", this.viewEnabled);
        dataMap.put("shareEnabled", this.shareEnabled);
        dataMap.put("defaultDomain", this.defaultDomain);
        dataMap.put("defaultTokenType", this.defaultTokenType);
        dataMap.put("defaultRedirectType", this.defaultRedirectType);
        dataMap.put("defaultFlashDuration", this.defaultFlashDuration);
        dataMap.put("defaultAccessType", this.defaultAccessType);
        dataMap.put("defaultViewType", this.defaultViewType);
        dataMap.put("defaultShareType", this.defaultShareType);
        dataMap.put("defaultPassphrase", this.defaultPassphrase);
        dataMap.put("defaultSignature", this.defaultSignature);
        dataMap.put("useSignature", this.useSignature);
        dataMap.put("defaultEmblem", this.defaultEmblem);
        dataMap.put("useEmblem", this.useEmblem);
        dataMap.put("defaultBackground", this.defaultBackground);
        dataMap.put("useBackground", this.useBackground);
        dataMap.put("sponsorUrl", this.sponsorUrl);
        dataMap.put("sponsorBanner", this.sponsorBanner);
        dataMap.put("sponsorNote", this.sponsorNote);
        dataMap.put("useSponsor", this.useSponsor);
        dataMap.put("extraParams", this.extraParams);
        dataMap.put("expirationDuration", this.expirationDuration);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        UserSetting thatObj = (UserSetting) obj;
        if( (this.user == null && thatObj.getUser() != null)
            || (this.user != null && thatObj.getUser() == null)
            || !this.user.equals(thatObj.getUser()) ) {
            return false;
        }
        if( (this.homePage == null && thatObj.getHomePage() != null)
            || (this.homePage != null && thatObj.getHomePage() == null)
            || !this.homePage.equals(thatObj.getHomePage()) ) {
            return false;
        }
        if( (this.selfBio == null && thatObj.getSelfBio() != null)
            || (this.selfBio != null && thatObj.getSelfBio() == null)
            || !this.selfBio.equals(thatObj.getSelfBio()) ) {
            return false;
        }
        if( (this.userUrlDomain == null && thatObj.getUserUrlDomain() != null)
            || (this.userUrlDomain != null && thatObj.getUserUrlDomain() == null)
            || !this.userUrlDomain.equals(thatObj.getUserUrlDomain()) ) {
            return false;
        }
        if( (this.userUrlDomainNormalized == null && thatObj.getUserUrlDomainNormalized() != null)
            || (this.userUrlDomainNormalized != null && thatObj.getUserUrlDomainNormalized() == null)
            || !this.userUrlDomainNormalized.equals(thatObj.getUserUrlDomainNormalized()) ) {
            return false;
        }
        if( (this.autoRedirectEnabled == null && thatObj.isAutoRedirectEnabled() != null)
            || (this.autoRedirectEnabled != null && thatObj.isAutoRedirectEnabled() == null)
            || !this.autoRedirectEnabled.equals(thatObj.isAutoRedirectEnabled()) ) {
            return false;
        }
        if( (this.viewEnabled == null && thatObj.isViewEnabled() != null)
            || (this.viewEnabled != null && thatObj.isViewEnabled() == null)
            || !this.viewEnabled.equals(thatObj.isViewEnabled()) ) {
            return false;
        }
        if( (this.shareEnabled == null && thatObj.isShareEnabled() != null)
            || (this.shareEnabled != null && thatObj.isShareEnabled() == null)
            || !this.shareEnabled.equals(thatObj.isShareEnabled()) ) {
            return false;
        }
        if( (this.defaultDomain == null && thatObj.getDefaultDomain() != null)
            || (this.defaultDomain != null && thatObj.getDefaultDomain() == null)
            || !this.defaultDomain.equals(thatObj.getDefaultDomain()) ) {
            return false;
        }
        if( (this.defaultTokenType == null && thatObj.getDefaultTokenType() != null)
            || (this.defaultTokenType != null && thatObj.getDefaultTokenType() == null)
            || !this.defaultTokenType.equals(thatObj.getDefaultTokenType()) ) {
            return false;
        }
        if( (this.defaultRedirectType == null && thatObj.getDefaultRedirectType() != null)
            || (this.defaultRedirectType != null && thatObj.getDefaultRedirectType() == null)
            || !this.defaultRedirectType.equals(thatObj.getDefaultRedirectType()) ) {
            return false;
        }
        if( (this.defaultFlashDuration == null && thatObj.getDefaultFlashDuration() != null)
            || (this.defaultFlashDuration != null && thatObj.getDefaultFlashDuration() == null)
            || !this.defaultFlashDuration.equals(thatObj.getDefaultFlashDuration()) ) {
            return false;
        }
        if( (this.defaultAccessType == null && thatObj.getDefaultAccessType() != null)
            || (this.defaultAccessType != null && thatObj.getDefaultAccessType() == null)
            || !this.defaultAccessType.equals(thatObj.getDefaultAccessType()) ) {
            return false;
        }
        if( (this.defaultViewType == null && thatObj.getDefaultViewType() != null)
            || (this.defaultViewType != null && thatObj.getDefaultViewType() == null)
            || !this.defaultViewType.equals(thatObj.getDefaultViewType()) ) {
            return false;
        }
        if( (this.defaultShareType == null && thatObj.getDefaultShareType() != null)
            || (this.defaultShareType != null && thatObj.getDefaultShareType() == null)
            || !this.defaultShareType.equals(thatObj.getDefaultShareType()) ) {
            return false;
        }
        if( (this.defaultPassphrase == null && thatObj.getDefaultPassphrase() != null)
            || (this.defaultPassphrase != null && thatObj.getDefaultPassphrase() == null)
            || !this.defaultPassphrase.equals(thatObj.getDefaultPassphrase()) ) {
            return false;
        }
        if( (this.defaultSignature == null && thatObj.getDefaultSignature() != null)
            || (this.defaultSignature != null && thatObj.getDefaultSignature() == null)
            || !this.defaultSignature.equals(thatObj.getDefaultSignature()) ) {
            return false;
        }
        if( (this.useSignature == null && thatObj.isUseSignature() != null)
            || (this.useSignature != null && thatObj.isUseSignature() == null)
            || !this.useSignature.equals(thatObj.isUseSignature()) ) {
            return false;
        }
        if( (this.defaultEmblem == null && thatObj.getDefaultEmblem() != null)
            || (this.defaultEmblem != null && thatObj.getDefaultEmblem() == null)
            || !this.defaultEmblem.equals(thatObj.getDefaultEmblem()) ) {
            return false;
        }
        if( (this.useEmblem == null && thatObj.isUseEmblem() != null)
            || (this.useEmblem != null && thatObj.isUseEmblem() == null)
            || !this.useEmblem.equals(thatObj.isUseEmblem()) ) {
            return false;
        }
        if( (this.defaultBackground == null && thatObj.getDefaultBackground() != null)
            || (this.defaultBackground != null && thatObj.getDefaultBackground() == null)
            || !this.defaultBackground.equals(thatObj.getDefaultBackground()) ) {
            return false;
        }
        if( (this.useBackground == null && thatObj.isUseBackground() != null)
            || (this.useBackground != null && thatObj.isUseBackground() == null)
            || !this.useBackground.equals(thatObj.isUseBackground()) ) {
            return false;
        }
        if( (this.sponsorUrl == null && thatObj.getSponsorUrl() != null)
            || (this.sponsorUrl != null && thatObj.getSponsorUrl() == null)
            || !this.sponsorUrl.equals(thatObj.getSponsorUrl()) ) {
            return false;
        }
        if( (this.sponsorBanner == null && thatObj.getSponsorBanner() != null)
            || (this.sponsorBanner != null && thatObj.getSponsorBanner() == null)
            || !this.sponsorBanner.equals(thatObj.getSponsorBanner()) ) {
            return false;
        }
        if( (this.sponsorNote == null && thatObj.getSponsorNote() != null)
            || (this.sponsorNote != null && thatObj.getSponsorNote() == null)
            || !this.sponsorNote.equals(thatObj.getSponsorNote()) ) {
            return false;
        }
        if( (this.useSponsor == null && thatObj.isUseSponsor() != null)
            || (this.useSponsor != null && thatObj.isUseSponsor() == null)
            || !this.useSponsor.equals(thatObj.isUseSponsor()) ) {
            return false;
        }
        if( (this.extraParams == null && thatObj.getExtraParams() != null)
            || (this.extraParams != null && thatObj.getExtraParams() == null)
            || !this.extraParams.equals(thatObj.getExtraParams()) ) {
            return false;
        }
        if( (this.expirationDuration == null && thatObj.getExpirationDuration() != null)
            || (this.expirationDuration != null && thatObj.getExpirationDuration() == null)
            || !this.expirationDuration.equals(thatObj.getExpirationDuration()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = user == null ? 0 : user.hashCode();
        _hash = 31 * _hash + delta;
        delta = homePage == null ? 0 : homePage.hashCode();
        _hash = 31 * _hash + delta;
        delta = selfBio == null ? 0 : selfBio.hashCode();
        _hash = 31 * _hash + delta;
        delta = userUrlDomain == null ? 0 : userUrlDomain.hashCode();
        _hash = 31 * _hash + delta;
        delta = userUrlDomainNormalized == null ? 0 : userUrlDomainNormalized.hashCode();
        _hash = 31 * _hash + delta;
        delta = autoRedirectEnabled == null ? 0 : autoRedirectEnabled.hashCode();
        _hash = 31 * _hash + delta;
        delta = viewEnabled == null ? 0 : viewEnabled.hashCode();
        _hash = 31 * _hash + delta;
        delta = shareEnabled == null ? 0 : shareEnabled.hashCode();
        _hash = 31 * _hash + delta;
        delta = defaultDomain == null ? 0 : defaultDomain.hashCode();
        _hash = 31 * _hash + delta;
        delta = defaultTokenType == null ? 0 : defaultTokenType.hashCode();
        _hash = 31 * _hash + delta;
        delta = defaultRedirectType == null ? 0 : defaultRedirectType.hashCode();
        _hash = 31 * _hash + delta;
        delta = defaultFlashDuration == null ? 0 : defaultFlashDuration.hashCode();
        _hash = 31 * _hash + delta;
        delta = defaultAccessType == null ? 0 : defaultAccessType.hashCode();
        _hash = 31 * _hash + delta;
        delta = defaultViewType == null ? 0 : defaultViewType.hashCode();
        _hash = 31 * _hash + delta;
        delta = defaultShareType == null ? 0 : defaultShareType.hashCode();
        _hash = 31 * _hash + delta;
        delta = defaultPassphrase == null ? 0 : defaultPassphrase.hashCode();
        _hash = 31 * _hash + delta;
        delta = defaultSignature == null ? 0 : defaultSignature.hashCode();
        _hash = 31 * _hash + delta;
        delta = useSignature == null ? 0 : useSignature.hashCode();
        _hash = 31 * _hash + delta;
        delta = defaultEmblem == null ? 0 : defaultEmblem.hashCode();
        _hash = 31 * _hash + delta;
        delta = useEmblem == null ? 0 : useEmblem.hashCode();
        _hash = 31 * _hash + delta;
        delta = defaultBackground == null ? 0 : defaultBackground.hashCode();
        _hash = 31 * _hash + delta;
        delta = useBackground == null ? 0 : useBackground.hashCode();
        _hash = 31 * _hash + delta;
        delta = sponsorUrl == null ? 0 : sponsorUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = sponsorBanner == null ? 0 : sponsorBanner.hashCode();
        _hash = 31 * _hash + delta;
        delta = sponsorNote == null ? 0 : sponsorNote.hashCode();
        _hash = 31 * _hash + delta;
        delta = useSponsor == null ? 0 : useSponsor.hashCode();
        _hash = 31 * _hash + delta;
        delta = extraParams == null ? 0 : extraParams.hashCode();
        _hash = 31 * _hash + delta;
        delta = expirationDuration == null ? 0 : expirationDuration.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
