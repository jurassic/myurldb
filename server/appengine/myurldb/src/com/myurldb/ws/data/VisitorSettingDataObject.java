package com.myurldb.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.myurldb.ws.VisitorSetting;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;


@PersistenceCapable(detachable="true")
@Inheritance(customStrategy = "complete-table")
public class VisitorSettingDataObject extends PersonalSettingDataObject implements VisitorSetting
{
    private static final Logger log = Logger.getLogger(VisitorSettingDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(VisitorSettingDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(VisitorSettingDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String user;

    @Persistent(defaultFetchGroup = "true")
    private String redirectType;

    @Persistent(defaultFetchGroup = "true")
    private Long flashDuration;

    @Persistent(defaultFetchGroup = "true")
    private String privacyType;

    public VisitorSettingDataObject()
    {
        this(null);
    }
    public VisitorSettingDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null);
    }
    public VisitorSettingDataObject(String guid, String user, String redirectType, Long flashDuration, String privacyType)
    {
        this(guid, user, redirectType, flashDuration, privacyType, null, null);
    }
    public VisitorSettingDataObject(String guid, String user, String redirectType, Long flashDuration, String privacyType, Long createdTime, Long modifiedTime)
    {
        super(guid, createdTime, modifiedTime);
        this.user = user;
        this.redirectType = redirectType;
        this.flashDuration = flashDuration;
        this.privacyType = privacyType;
    }

//    @Override
//    protected Key createKey()
//    {
//        return VisitorSettingDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return VisitorSettingDataObject.composeKey(getGuid());
    }

    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    public String getRedirectType()
    {
        return this.redirectType;
    }
    public void setRedirectType(String redirectType)
    {
        this.redirectType = redirectType;
    }

    public Long getFlashDuration()
    {
        return this.flashDuration;
    }
    public void setFlashDuration(Long flashDuration)
    {
        this.flashDuration = flashDuration;
    }

    public String getPrivacyType()
    {
        return this.privacyType;
    }
    public void setPrivacyType(String privacyType)
    {
        this.privacyType = privacyType;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("user", this.user);
        dataMap.put("redirectType", this.redirectType);
        dataMap.put("flashDuration", this.flashDuration);
        dataMap.put("privacyType", this.privacyType);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        VisitorSetting thatObj = (VisitorSetting) obj;
        if( (this.user == null && thatObj.getUser() != null)
            || (this.user != null && thatObj.getUser() == null)
            || !this.user.equals(thatObj.getUser()) ) {
            return false;
        }
        if( (this.redirectType == null && thatObj.getRedirectType() != null)
            || (this.redirectType != null && thatObj.getRedirectType() == null)
            || !this.redirectType.equals(thatObj.getRedirectType()) ) {
            return false;
        }
        if( (this.flashDuration == null && thatObj.getFlashDuration() != null)
            || (this.flashDuration != null && thatObj.getFlashDuration() == null)
            || !this.flashDuration.equals(thatObj.getFlashDuration()) ) {
            return false;
        }
        if( (this.privacyType == null && thatObj.getPrivacyType() != null)
            || (this.privacyType != null && thatObj.getPrivacyType() == null)
            || !this.privacyType.equals(thatObj.getPrivacyType()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = user == null ? 0 : user.hashCode();
        _hash = 31 * _hash + delta;
        delta = redirectType == null ? 0 : redirectType.hashCode();
        _hash = 31 * _hash + delta;
        delta = flashDuration == null ? 0 : flashDuration.hashCode();
        _hash = 31 * _hash + delta;
        delta = privacyType == null ? 0 : privacyType.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
