package com.myurldb.ws.exception;

import com.myurldb.ws.BaseException;


public class MethodNotAllowedException extends BaseException 
{
    private static final long serialVersionUID = 1L;

    public MethodNotAllowedException() 
    {
        super();
    }
    public MethodNotAllowedException(String message) 
    {
        super(message);
    }
   public MethodNotAllowedException(String message, Throwable cause) 
    {
        super(message, cause);
    }
    public MethodNotAllowedException(Throwable cause) 
    {
        super(cause);
    }

}
