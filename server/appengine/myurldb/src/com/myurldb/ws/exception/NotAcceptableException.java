package com.myurldb.ws.exception;

import com.myurldb.ws.BaseException;


public class NotAcceptableException extends BaseException 
{
    private static final long serialVersionUID = 1L;

    public NotAcceptableException() 
    {
        super();
    }
    public NotAcceptableException(String message) 
    {
        super(message);
    }
   public NotAcceptableException(String message, Throwable cause) 
    {
        super(message, cause);
    }
    public NotAcceptableException(Throwable cause) 
    {
        super(cause);
    }

}
