package com.myurldb.ws.exception;

import com.myurldb.ws.BaseException;


public class NotImplementedException extends BaseException 
{
    private static final long serialVersionUID = 1L;

    public NotImplementedException() 
    {
        super();
    }
    public NotImplementedException(String message) 
    {
        super(message);
    }
    public NotImplementedException(String message, Throwable cause) 
    {
        super(message, cause);
    }
    public NotImplementedException(Throwable cause) 
    {
        super(cause);
    }

}
