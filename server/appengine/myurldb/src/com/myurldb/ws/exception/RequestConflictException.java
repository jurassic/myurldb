package com.myurldb.ws.exception;

import com.myurldb.ws.BaseException;


public class RequestConflictException extends BaseException 
{
    private static final long serialVersionUID = 1L;

    public RequestConflictException() 
    {
        super();
    }
    public RequestConflictException(String message) 
    {
        super(message);
    }
    public RequestConflictException(String message, Throwable cause) 
    {
        super(message, cause);
    }
    public RequestConflictException(Throwable cause) 
    {
        super(cause);
    }

}
