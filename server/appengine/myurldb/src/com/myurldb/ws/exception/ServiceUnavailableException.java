package com.myurldb.ws.exception;

import com.myurldb.ws.BaseException;


public class ServiceUnavailableException extends BaseException 
{
    private static final long serialVersionUID = 1L;

    public ServiceUnavailableException() 
    {
        super();
    }
    public ServiceUnavailableException(String message) 
    {
        super(message);
    }
    public ServiceUnavailableException(String message, Throwable cause) 
    {
        super(message, cause);
    }
    public ServiceUnavailableException(Throwable cause) 
    {
        super(cause);
    }

}
