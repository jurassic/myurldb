package com.myurldb.ws.permission;

import java.util.logging.Logger;
import java.util.logging.Level;


public class AbuseTagBasePermission extends AbstractBasePermission
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(AbuseTagBasePermission.class.getName());

    public AbuseTagBasePermission()
    {
        super();
    }

    @Override
    public String getPermissionName(String action)
    {
        return "AbuseTag::" + action;
    }


}
