package com.myurldb.ws.permission;

import java.util.logging.Logger;
import java.util.logging.Level;


public class AlbumShortLinkBasePermission extends AbstractBasePermission
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(AlbumShortLinkBasePermission.class.getName());

    public AlbumShortLinkBasePermission()
    {
        super();
    }

    @Override
    public String getPermissionName(String action)
    {
        return "AlbumShortLink::" + action;
    }


}
