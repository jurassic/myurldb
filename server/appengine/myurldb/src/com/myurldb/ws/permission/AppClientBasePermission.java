package com.myurldb.ws.permission;

import java.util.logging.Logger;
import java.util.logging.Level;


public class AppClientBasePermission extends AbstractBasePermission
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(AppClientBasePermission.class.getName());

    public AppClientBasePermission()
    {
        super();
    }

    @Override
    public String getPermissionName(String action)
    {
        return "AppClient::" + action;
    }


}
