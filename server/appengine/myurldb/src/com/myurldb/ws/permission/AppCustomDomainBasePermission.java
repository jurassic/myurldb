package com.myurldb.ws.permission;

import java.util.logging.Logger;
import java.util.logging.Level;


public class AppCustomDomainBasePermission extends AbstractBasePermission
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(AppCustomDomainBasePermission.class.getName());

    public AppCustomDomainBasePermission()
    {
        super();
    }

    @Override
    public String getPermissionName(String action)
    {
        return "AppCustomDomain::" + action;
    }


}
