package com.myurldb.ws.permission;

import java.util.logging.Logger;
import java.util.logging.Level;


public class ClientUserBasePermission extends AbstractBasePermission
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ClientUserBasePermission.class.getName());

    public ClientUserBasePermission()
    {
        super();
    }

    @Override
    public String getPermissionName(String action)
    {
        return "ClientUser::" + action;
    }


}
