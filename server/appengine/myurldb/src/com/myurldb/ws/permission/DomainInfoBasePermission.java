package com.myurldb.ws.permission;

import java.util.logging.Logger;
import java.util.logging.Level;


public class DomainInfoBasePermission extends AbstractBasePermission
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(DomainInfoBasePermission.class.getName());

    public DomainInfoBasePermission()
    {
        super();
    }

    @Override
    public String getPermissionName(String action)
    {
        return "DomainInfo::" + action;
    }


}
