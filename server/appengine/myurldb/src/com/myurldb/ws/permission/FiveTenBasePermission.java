package com.myurldb.ws.permission;

import java.util.logging.Logger;
import java.util.logging.Level;


public class FiveTenBasePermission extends AbstractBasePermission
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(FiveTenBasePermission.class.getName());

    public FiveTenBasePermission()
    {
        super();
    }

    @Override
    public String getPermissionName(String action)
    {
        return "FiveTen::" + action;
    }

    @Override
    public boolean isCreatePermissionRequired()
    {
        return true;
    }

    @Override
    public boolean isReadPermissionRequired()
    {
        return false;
    }

    @Override
    public boolean isUpdatePermissionRequired()
    {
        return true;
    }

    @Override
    public boolean isDeletePermissionRequired()
    {
        return true;
    }

}
