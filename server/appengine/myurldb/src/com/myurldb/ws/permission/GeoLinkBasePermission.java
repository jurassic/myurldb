package com.myurldb.ws.permission;

import java.util.logging.Logger;
import java.util.logging.Level;


public class GeoLinkBasePermission extends AbstractBasePermission
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(GeoLinkBasePermission.class.getName());

    public GeoLinkBasePermission()
    {
        super();
    }

    @Override
    public String getPermissionName(String action)
    {
        return "GeoLink::" + action;
    }


}
