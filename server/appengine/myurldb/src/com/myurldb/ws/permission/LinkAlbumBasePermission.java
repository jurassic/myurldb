package com.myurldb.ws.permission;

import java.util.logging.Logger;
import java.util.logging.Level;


public class LinkAlbumBasePermission extends AbstractBasePermission
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(LinkAlbumBasePermission.class.getName());

    public LinkAlbumBasePermission()
    {
        super();
    }

    @Override
    public String getPermissionName(String action)
    {
        return "LinkAlbum::" + action;
    }


}
