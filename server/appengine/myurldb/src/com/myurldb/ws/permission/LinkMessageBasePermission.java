package com.myurldb.ws.permission;

import java.util.logging.Logger;
import java.util.logging.Level;


public class LinkMessageBasePermission extends AbstractBasePermission
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(LinkMessageBasePermission.class.getName());

    public LinkMessageBasePermission()
    {
        super();
    }

    @Override
    public String getPermissionName(String action)
    {
        return "LinkMessage::" + action;
    }


}
