package com.myurldb.ws.permission;

import java.util.logging.Logger;
import java.util.logging.Level;


public class LinkPassphraseBasePermission extends AbstractBasePermission
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(LinkPassphraseBasePermission.class.getName());

    public LinkPassphraseBasePermission()
    {
        super();
    }

    @Override
    public String getPermissionName(String action)
    {
        return "LinkPassphrase::" + action;
    }


}
