package com.myurldb.ws.permission;

import java.util.logging.Logger;
import java.util.logging.Level;


public class QrCodeBasePermission extends AbstractBasePermission
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(QrCodeBasePermission.class.getName());

    public QrCodeBasePermission()
    {
        super();
    }

    @Override
    public String getPermissionName(String action)
    {
        return "QrCode::" + action;
    }


}
