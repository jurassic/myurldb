package com.myurldb.ws.permission;

import java.util.logging.Logger;
import java.util.logging.Level;


public class RolePermissionBasePermission extends AbstractBasePermission
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(RolePermissionBasePermission.class.getName());

    public RolePermissionBasePermission()
    {
        super();
    }

    @Override
    public String getPermissionName(String action)
    {
        return "RolePermission::" + action;
    }


}
