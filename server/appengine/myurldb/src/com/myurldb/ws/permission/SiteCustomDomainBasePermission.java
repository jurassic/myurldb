package com.myurldb.ws.permission;

import java.util.logging.Logger;
import java.util.logging.Level;


public class SiteCustomDomainBasePermission extends AbstractBasePermission
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(SiteCustomDomainBasePermission.class.getName());

    public SiteCustomDomainBasePermission()
    {
        super();
    }

    @Override
    public String getPermissionName(String action)
    {
        return "SiteCustomDomain::" + action;
    }


}
