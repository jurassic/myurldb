package com.myurldb.ws.permission;

import java.util.logging.Logger;
import java.util.logging.Level;


public class UrlRatingBasePermission extends AbstractBasePermission
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UrlRatingBasePermission.class.getName());

    public UrlRatingBasePermission()
    {
        super();
    }

    @Override
    public String getPermissionName(String action)
    {
        return "UrlRating::" + action;
    }


}
