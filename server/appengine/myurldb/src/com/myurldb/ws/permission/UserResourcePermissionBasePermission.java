package com.myurldb.ws.permission;

import java.util.logging.Logger;
import java.util.logging.Level;


public class UserResourcePermissionBasePermission extends AbstractBasePermission
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UserResourcePermissionBasePermission.class.getName());

    public UserResourcePermissionBasePermission()
    {
        super();
    }

    @Override
    public String getPermissionName(String action)
    {
        return "UserResourcePermission::" + action;
    }


}
