package com.myurldb.ws.permission;

import java.util.logging.Logger;
import java.util.logging.Level;


public class UserResourceProhibitionBasePermission extends AbstractBasePermission
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UserResourceProhibitionBasePermission.class.getName());

    public UserResourceProhibitionBasePermission()
    {
        super();
    }

    @Override
    public String getPermissionName(String action)
    {
        return "UserResourceProhibition::" + action;
    }


}
