package com.myurldb.ws.permission;

import java.util.logging.Logger;
import java.util.logging.Level;


public class UserRoleBasePermission extends AbstractBasePermission
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UserRoleBasePermission.class.getName());

    public UserRoleBasePermission()
    {
        super();
    }

    @Override
    public String getPermissionName(String action)
    {
        return "UserRole::" + action;
    }


}
