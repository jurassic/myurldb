package com.myurldb.ws.permission;

import java.util.logging.Logger;
import java.util.logging.Level;


public class UserUsercodeBasePermission extends AbstractBasePermission
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UserUsercodeBasePermission.class.getName());

    public UserUsercodeBasePermission()
    {
        super();
    }

    @Override
    public String getPermissionName(String action)
    {
        return "UserUsercode::" + action;
    }


}
