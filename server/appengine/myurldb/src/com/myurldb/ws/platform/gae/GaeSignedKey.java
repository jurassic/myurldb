package com.myurldb.ws.platform.gae;

import com.google.appengine.api.appidentity.AppIdentityService;
import com.myurldb.ws.platform.SignedKey;

public class GaeSignedKey extends SignedKey
{
    public GaeSignedKey(String keyName, byte[] signature)
    {
        super(keyName, signature);
    }

    public GaeSignedKey(AppIdentityService.SigningResult signingResult)
    {
        super(signingResult.getKeyName(), signingResult.getSignature());
    }

}
