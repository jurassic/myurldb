package com.myurldb.ws.platform.gae;

import com.google.appengine.api.appidentity.PublicCertificate;
import com.myurldb.ws.platform.X509PublicCertificate;

public class GaeX509PublicCertificate extends X509PublicCertificate
{

    public GaeX509PublicCertificate(String certificateName,
            String certificateInPemFormat)
    {
        super(certificateName, certificateInPemFormat);
    }

    public GaeX509PublicCertificate(PublicCertificate publicCertificate)
    {
        super(publicCertificate.getCertificateName(), publicCertificate.getX509CertificateInPemFormat());
    }

    
}
