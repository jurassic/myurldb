package com.myurldb.ws.resource;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.resource.BaseResourceException;
import com.myurldb.ws.AbuseTag;
import com.myurldb.ws.stub.AbuseTagStub;
import com.myurldb.ws.stub.AbuseTagListStub;


// TBD: Partial update/overwrite?
// TBD: Field-based filtering in getAbuseTag(guid). (e.g., ?field1=x&field2=y)
// Note: Jersey (possibly, new version 1.9.1) seems to have a weird bug and
//       it throws exception with the format @Path("{guid : [0-9a-fA-F\\-]+}") (and, other variations)
//       (which somehow translates into 405 error).
// --> Workaround. Use this format: @Path("{guid: [a-zA-Z0-9\\-_]+}") across all guid path params...
public interface AbuseTagResource
{
    @GET
    @Path("all")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getAllAbuseTags(@QueryParam("ordering") String ordering, @QueryParam("offset") Long offset, @QueryParam("count") Integer count) throws BaseResourceException;

    @GET
    @Path("allkeys")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getAllAbuseTagKeys(@QueryParam("ordering") String ordering, @QueryParam("offset") Long offset, @QueryParam("count") Integer count) throws BaseResourceException;

    @GET
    @Path("keys")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response findAbuseTagKeys(@QueryParam("filter") String filter, @QueryParam("ordering") String ordering, @QueryParam("params") String params, @QueryParam("values") List<String> values, @QueryParam("grouping") String grouping, @QueryParam("unique") Boolean unique, @QueryParam("offset") Long offset, @QueryParam("count") Integer count) throws BaseResourceException;

    @GET
    @Path("subset")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getAbuseTagKeys(@QueryParam("guids") List<String> guids) throws BaseResourceException;

    @GET
    @Path("count")
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getCount(@QueryParam("filter") String filter, @QueryParam("params") String params, @QueryParam("values") List<String> values, @QueryParam("aggregate") String aggregate) throws BaseResourceException;

    @GET
    // @Path("{guid : [0-9a-f\\-]+}")     // Lower case only.
    @Path("{guid: [a-zA-Z0-9\\-_]+}")     // Support both guid and key ???  (Note: We have to be consistent!)
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getAbuseTag(@PathParam("guid") String guid) throws BaseResourceException;

    @GET
    @Path("{guid: [a-f0-9\\-]+}/{field: [a-zA-Z_][a-zA-Z0-9_]*}")
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getAbuseTag(@PathParam("guid") String guid, @PathParam("field") String field) throws BaseResourceException;

    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response findAbuseTags(@QueryParam("filter") String filter, @QueryParam("ordering") String ordering, @QueryParam("params") String params, @QueryParam("values") List<String> values, @QueryParam("grouping") String grouping, @QueryParam("unique") Boolean unique, @QueryParam("offset") Long offset, @QueryParam("count") Integer count) throws BaseResourceException;

    @POST
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response createAbuseTag(AbuseTagStub abuseTag) throws BaseResourceException;

    @PUT
    @Path("{guid: [a-zA-Z0-9\\-_]+}")     // Due to the way Jersey defines "resources", @Path("{guid: [a-f0-9\\-]+}") cannot be used...
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response updateAbuseTag(@PathParam("guid") String guid, AbuseTagStub abuseTag) throws BaseResourceException;

    //@PUT  ???
    @POST   // We can adhere to semantics of PUT=Replace, POST=update. PUT is supported in HTML form in HTML5 only.
    @Path("{guid: [a-zA-Z0-9\\-_]+}")     // Due to the way Jersey defines "resources", @Path("{guid: [a-f0-9\\-]+}") cannot be used...
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response updateAbuseTag(@PathParam("guid") String guid, @QueryParam("shortLink") String shortLink, @QueryParam("shortUrl") String shortUrl, @QueryParam("longUrl") String longUrl, @QueryParam("longUrlHash") String longUrlHash, @QueryParam("user") String user, @QueryParam("ipAddress") String ipAddress, @QueryParam("tag") Integer tag, @QueryParam("comment") String comment, @QueryParam("status") String status, @QueryParam("reviewer") String reviewer, @QueryParam("action") String action, @QueryParam("note") String note, @QueryParam("pastActions") String pastActions, @QueryParam("reviewedTime") Long reviewedTime, @QueryParam("actionTime") Long actionTime) throws BaseResourceException;

    @DELETE
    @Path("{guid: [a-zA-Z0-9\\-_]+}")     // Due to the way Jersey defines "resources", @Path("{guid: [a-f0-9\\-]+}") cannot be used...
    Response deleteAbuseTag(@PathParam("guid") String guid) throws BaseResourceException;

    @DELETE
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response deleteAbuseTags(@QueryParam("filter") String filter, @QueryParam("params") String params, @QueryParam("values") List<String> values) throws BaseResourceException;

//    @POST
//    @Path("bulk")
//    @Produces({MediaType.TEXT_PLAIN})
//    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
//    Response createAbuseTags(AbuseTagListStub abuseTags) throws BaseResourceException;

//    @PUT
//    @Path("bulk")
//    @Produces({MediaType.TEXT_PLAIN})
//    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
//    Response updateeAbuseTags(AbuseTagListStub abuseTags) throws BaseResourceException;

}
