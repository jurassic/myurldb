package com.myurldb.ws.resource;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.service.ApiConsumerService;
import com.myurldb.ws.service.impl.ApiConsumerServiceImpl;
import com.myurldb.ws.service.AppCustomDomainService;
import com.myurldb.ws.service.impl.AppCustomDomainServiceImpl;
import com.myurldb.ws.service.SiteCustomDomainService;
import com.myurldb.ws.service.impl.SiteCustomDomainServiceImpl;
import com.myurldb.ws.service.UserService;
import com.myurldb.ws.service.impl.UserServiceImpl;
import com.myurldb.ws.service.UserUsercodeService;
import com.myurldb.ws.service.impl.UserUsercodeServiceImpl;
import com.myurldb.ws.service.UserPasswordService;
import com.myurldb.ws.service.impl.UserPasswordServiceImpl;
import com.myurldb.ws.service.ExternalUserAuthService;
import com.myurldb.ws.service.impl.ExternalUserAuthServiceImpl;
import com.myurldb.ws.service.UserAuthStateService;
import com.myurldb.ws.service.impl.UserAuthStateServiceImpl;
import com.myurldb.ws.service.UserResourcePermissionService;
import com.myurldb.ws.service.impl.UserResourcePermissionServiceImpl;
import com.myurldb.ws.service.UserResourceProhibitionService;
import com.myurldb.ws.service.impl.UserResourceProhibitionServiceImpl;
import com.myurldb.ws.service.RolePermissionService;
import com.myurldb.ws.service.impl.RolePermissionServiceImpl;
import com.myurldb.ws.service.UserRoleService;
import com.myurldb.ws.service.impl.UserRoleServiceImpl;
import com.myurldb.ws.service.AppClientService;
import com.myurldb.ws.service.impl.AppClientServiceImpl;
import com.myurldb.ws.service.ClientUserService;
import com.myurldb.ws.service.impl.ClientUserServiceImpl;
import com.myurldb.ws.service.UserCustomDomainService;
import com.myurldb.ws.service.impl.UserCustomDomainServiceImpl;
import com.myurldb.ws.service.ClientSettingService;
import com.myurldb.ws.service.impl.ClientSettingServiceImpl;
import com.myurldb.ws.service.UserSettingService;
import com.myurldb.ws.service.impl.UserSettingServiceImpl;
import com.myurldb.ws.service.VisitorSettingService;
import com.myurldb.ws.service.impl.VisitorSettingServiceImpl;
import com.myurldb.ws.service.TwitterSummaryCardService;
import com.myurldb.ws.service.impl.TwitterSummaryCardServiceImpl;
import com.myurldb.ws.service.TwitterPhotoCardService;
import com.myurldb.ws.service.impl.TwitterPhotoCardServiceImpl;
import com.myurldb.ws.service.TwitterGalleryCardService;
import com.myurldb.ws.service.impl.TwitterGalleryCardServiceImpl;
import com.myurldb.ws.service.TwitterAppCardService;
import com.myurldb.ws.service.impl.TwitterAppCardServiceImpl;
import com.myurldb.ws.service.TwitterPlayerCardService;
import com.myurldb.ws.service.impl.TwitterPlayerCardServiceImpl;
import com.myurldb.ws.service.TwitterProductCardService;
import com.myurldb.ws.service.impl.TwitterProductCardServiceImpl;
import com.myurldb.ws.service.ShortPassageService;
import com.myurldb.ws.service.impl.ShortPassageServiceImpl;
import com.myurldb.ws.service.ShortLinkService;
import com.myurldb.ws.service.impl.ShortLinkServiceImpl;
import com.myurldb.ws.service.GeoLinkService;
import com.myurldb.ws.service.impl.GeoLinkServiceImpl;
import com.myurldb.ws.service.QrCodeService;
import com.myurldb.ws.service.impl.QrCodeServiceImpl;
import com.myurldb.ws.service.LinkPassphraseService;
import com.myurldb.ws.service.impl.LinkPassphraseServiceImpl;
import com.myurldb.ws.service.LinkMessageService;
import com.myurldb.ws.service.impl.LinkMessageServiceImpl;
import com.myurldb.ws.service.LinkAlbumService;
import com.myurldb.ws.service.impl.LinkAlbumServiceImpl;
import com.myurldb.ws.service.AlbumShortLinkService;
import com.myurldb.ws.service.impl.AlbumShortLinkServiceImpl;
import com.myurldb.ws.service.KeywordFolderService;
import com.myurldb.ws.service.impl.KeywordFolderServiceImpl;
import com.myurldb.ws.service.BookmarkFolderService;
import com.myurldb.ws.service.impl.BookmarkFolderServiceImpl;
import com.myurldb.ws.service.KeywordLinkService;
import com.myurldb.ws.service.impl.KeywordLinkServiceImpl;
import com.myurldb.ws.service.BookmarkLinkService;
import com.myurldb.ws.service.impl.BookmarkLinkServiceImpl;
import com.myurldb.ws.service.SpeedDialService;
import com.myurldb.ws.service.impl.SpeedDialServiceImpl;
import com.myurldb.ws.service.KeywordFolderImportService;
import com.myurldb.ws.service.impl.KeywordFolderImportServiceImpl;
import com.myurldb.ws.service.BookmarkFolderImportService;
import com.myurldb.ws.service.impl.BookmarkFolderImportServiceImpl;
import com.myurldb.ws.service.KeywordCrowdTallyService;
import com.myurldb.ws.service.impl.KeywordCrowdTallyServiceImpl;
import com.myurldb.ws.service.BookmarkCrowdTallyService;
import com.myurldb.ws.service.impl.BookmarkCrowdTallyServiceImpl;
import com.myurldb.ws.service.DomainInfoService;
import com.myurldb.ws.service.impl.DomainInfoServiceImpl;
import com.myurldb.ws.service.UrlRatingService;
import com.myurldb.ws.service.impl.UrlRatingServiceImpl;
import com.myurldb.ws.service.UserRatingService;
import com.myurldb.ws.service.impl.UserRatingServiceImpl;
import com.myurldb.ws.service.AbuseTagService;
import com.myurldb.ws.service.impl.AbuseTagServiceImpl;
import com.myurldb.ws.service.ServiceInfoService;
import com.myurldb.ws.service.impl.ServiceInfoServiceImpl;
import com.myurldb.ws.service.FiveTenService;
import com.myurldb.ws.service.impl.FiveTenServiceImpl;


// TBD:
// Implement pooling, etc. ????
public final class ServiceManager
{
    private static final Logger log = Logger.getLogger(ServiceManager.class.getName());

	private static ApiConsumerService apiConsumerService = null;
	private static AppCustomDomainService appCustomDomainService = null;
	private static SiteCustomDomainService siteCustomDomainService = null;
	private static UserService userService = null;
	private static UserUsercodeService userUsercodeService = null;
	private static UserPasswordService userPasswordService = null;
	private static ExternalUserAuthService externalUserAuthService = null;
	private static UserAuthStateService userAuthStateService = null;
	private static UserResourcePermissionService userResourcePermissionService = null;
	private static UserResourceProhibitionService userResourceProhibitionService = null;
	private static RolePermissionService rolePermissionService = null;
	private static UserRoleService userRoleService = null;
	private static AppClientService appClientService = null;
	private static ClientUserService clientUserService = null;
	private static UserCustomDomainService userCustomDomainService = null;
	private static ClientSettingService clientSettingService = null;
	private static UserSettingService userSettingService = null;
	private static VisitorSettingService visitorSettingService = null;
	private static TwitterSummaryCardService twitterSummaryCardService = null;
	private static TwitterPhotoCardService twitterPhotoCardService = null;
	private static TwitterGalleryCardService twitterGalleryCardService = null;
	private static TwitterAppCardService twitterAppCardService = null;
	private static TwitterPlayerCardService twitterPlayerCardService = null;
	private static TwitterProductCardService twitterProductCardService = null;
	private static ShortPassageService shortPassageService = null;
	private static ShortLinkService shortLinkService = null;
	private static GeoLinkService geoLinkService = null;
	private static QrCodeService qrCodeService = null;
	private static LinkPassphraseService linkPassphraseService = null;
	private static LinkMessageService linkMessageService = null;
	private static LinkAlbumService linkAlbumService = null;
	private static AlbumShortLinkService albumShortLinkService = null;
	private static KeywordFolderService keywordFolderService = null;
	private static BookmarkFolderService bookmarkFolderService = null;
	private static KeywordLinkService keywordLinkService = null;
	private static BookmarkLinkService bookmarkLinkService = null;
	private static SpeedDialService speedDialService = null;
	private static KeywordFolderImportService keywordFolderImportService = null;
	private static BookmarkFolderImportService bookmarkFolderImportService = null;
	private static KeywordCrowdTallyService keywordCrowdTallyService = null;
	private static BookmarkCrowdTallyService bookmarkCrowdTallyService = null;
	private static DomainInfoService domainInfoService = null;
	private static UrlRatingService urlRatingService = null;
	private static UserRatingService userRatingService = null;
	private static AbuseTagService abuseTagService = null;
	private static ServiceInfoService serviceInfoService = null;
	private static FiveTenService fiveTenService = null;

    // Prevents instantiation.
    private ServiceManager() {}

    // Returns a ApiConsumerService instance.
	public static ApiConsumerService getApiConsumerService() 
    {
        if(ServiceManager.apiConsumerService == null) {
            ServiceManager.apiConsumerService = new ApiConsumerServiceImpl();
        }
        return ServiceManager.apiConsumerService;
    }

    // Returns a AppCustomDomainService instance.
	public static AppCustomDomainService getAppCustomDomainService() 
    {
        if(ServiceManager.appCustomDomainService == null) {
            ServiceManager.appCustomDomainService = new AppCustomDomainServiceImpl();
        }
        return ServiceManager.appCustomDomainService;
    }

    // Returns a SiteCustomDomainService instance.
	public static SiteCustomDomainService getSiteCustomDomainService() 
    {
        if(ServiceManager.siteCustomDomainService == null) {
            ServiceManager.siteCustomDomainService = new SiteCustomDomainServiceImpl();
        }
        return ServiceManager.siteCustomDomainService;
    }

    // Returns a UserService instance.
	public static UserService getUserService() 
    {
        if(ServiceManager.userService == null) {
            ServiceManager.userService = new UserServiceImpl();
        }
        return ServiceManager.userService;
    }

    // Returns a UserUsercodeService instance.
	public static UserUsercodeService getUserUsercodeService() 
    {
        if(ServiceManager.userUsercodeService == null) {
            ServiceManager.userUsercodeService = new UserUsercodeServiceImpl();
        }
        return ServiceManager.userUsercodeService;
    }

    // Returns a UserPasswordService instance.
	public static UserPasswordService getUserPasswordService() 
    {
        if(ServiceManager.userPasswordService == null) {
            ServiceManager.userPasswordService = new UserPasswordServiceImpl();
        }
        return ServiceManager.userPasswordService;
    }

    // Returns a ExternalUserAuthService instance.
	public static ExternalUserAuthService getExternalUserAuthService() 
    {
        if(ServiceManager.externalUserAuthService == null) {
            ServiceManager.externalUserAuthService = new ExternalUserAuthServiceImpl();
        }
        return ServiceManager.externalUserAuthService;
    }

    // Returns a UserAuthStateService instance.
	public static UserAuthStateService getUserAuthStateService() 
    {
        if(ServiceManager.userAuthStateService == null) {
            ServiceManager.userAuthStateService = new UserAuthStateServiceImpl();
        }
        return ServiceManager.userAuthStateService;
    }

    // Returns a UserResourcePermissionService instance.
	public static UserResourcePermissionService getUserResourcePermissionService() 
    {
        if(ServiceManager.userResourcePermissionService == null) {
            ServiceManager.userResourcePermissionService = new UserResourcePermissionServiceImpl();
        }
        return ServiceManager.userResourcePermissionService;
    }

    // Returns a UserResourceProhibitionService instance.
	public static UserResourceProhibitionService getUserResourceProhibitionService() 
    {
        if(ServiceManager.userResourceProhibitionService == null) {
            ServiceManager.userResourceProhibitionService = new UserResourceProhibitionServiceImpl();
        }
        return ServiceManager.userResourceProhibitionService;
    }

    // Returns a RolePermissionService instance.
	public static RolePermissionService getRolePermissionService() 
    {
        if(ServiceManager.rolePermissionService == null) {
            ServiceManager.rolePermissionService = new RolePermissionServiceImpl();
        }
        return ServiceManager.rolePermissionService;
    }

    // Returns a UserRoleService instance.
	public static UserRoleService getUserRoleService() 
    {
        if(ServiceManager.userRoleService == null) {
            ServiceManager.userRoleService = new UserRoleServiceImpl();
        }
        return ServiceManager.userRoleService;
    }

    // Returns a AppClientService instance.
	public static AppClientService getAppClientService() 
    {
        if(ServiceManager.appClientService == null) {
            ServiceManager.appClientService = new AppClientServiceImpl();
        }
        return ServiceManager.appClientService;
    }

    // Returns a ClientUserService instance.
	public static ClientUserService getClientUserService() 
    {
        if(ServiceManager.clientUserService == null) {
            ServiceManager.clientUserService = new ClientUserServiceImpl();
        }
        return ServiceManager.clientUserService;
    }

    // Returns a UserCustomDomainService instance.
	public static UserCustomDomainService getUserCustomDomainService() 
    {
        if(ServiceManager.userCustomDomainService == null) {
            ServiceManager.userCustomDomainService = new UserCustomDomainServiceImpl();
        }
        return ServiceManager.userCustomDomainService;
    }

    // Returns a ClientSettingService instance.
	public static ClientSettingService getClientSettingService() 
    {
        if(ServiceManager.clientSettingService == null) {
            ServiceManager.clientSettingService = new ClientSettingServiceImpl();
        }
        return ServiceManager.clientSettingService;
    }

    // Returns a UserSettingService instance.
	public static UserSettingService getUserSettingService() 
    {
        if(ServiceManager.userSettingService == null) {
            ServiceManager.userSettingService = new UserSettingServiceImpl();
        }
        return ServiceManager.userSettingService;
    }

    // Returns a VisitorSettingService instance.
	public static VisitorSettingService getVisitorSettingService() 
    {
        if(ServiceManager.visitorSettingService == null) {
            ServiceManager.visitorSettingService = new VisitorSettingServiceImpl();
        }
        return ServiceManager.visitorSettingService;
    }

    // Returns a TwitterSummaryCardService instance.
	public static TwitterSummaryCardService getTwitterSummaryCardService() 
    {
        if(ServiceManager.twitterSummaryCardService == null) {
            ServiceManager.twitterSummaryCardService = new TwitterSummaryCardServiceImpl();
        }
        return ServiceManager.twitterSummaryCardService;
    }

    // Returns a TwitterPhotoCardService instance.
	public static TwitterPhotoCardService getTwitterPhotoCardService() 
    {
        if(ServiceManager.twitterPhotoCardService == null) {
            ServiceManager.twitterPhotoCardService = new TwitterPhotoCardServiceImpl();
        }
        return ServiceManager.twitterPhotoCardService;
    }

    // Returns a TwitterGalleryCardService instance.
	public static TwitterGalleryCardService getTwitterGalleryCardService() 
    {
        if(ServiceManager.twitterGalleryCardService == null) {
            ServiceManager.twitterGalleryCardService = new TwitterGalleryCardServiceImpl();
        }
        return ServiceManager.twitterGalleryCardService;
    }

    // Returns a TwitterAppCardService instance.
	public static TwitterAppCardService getTwitterAppCardService() 
    {
        if(ServiceManager.twitterAppCardService == null) {
            ServiceManager.twitterAppCardService = new TwitterAppCardServiceImpl();
        }
        return ServiceManager.twitterAppCardService;
    }

    // Returns a TwitterPlayerCardService instance.
	public static TwitterPlayerCardService getTwitterPlayerCardService() 
    {
        if(ServiceManager.twitterPlayerCardService == null) {
            ServiceManager.twitterPlayerCardService = new TwitterPlayerCardServiceImpl();
        }
        return ServiceManager.twitterPlayerCardService;
    }

    // Returns a TwitterProductCardService instance.
	public static TwitterProductCardService getTwitterProductCardService() 
    {
        if(ServiceManager.twitterProductCardService == null) {
            ServiceManager.twitterProductCardService = new TwitterProductCardServiceImpl();
        }
        return ServiceManager.twitterProductCardService;
    }

    // Returns a ShortPassageService instance.
	public static ShortPassageService getShortPassageService() 
    {
        if(ServiceManager.shortPassageService == null) {
            ServiceManager.shortPassageService = new ShortPassageServiceImpl();
        }
        return ServiceManager.shortPassageService;
    }

    // Returns a ShortLinkService instance.
	public static ShortLinkService getShortLinkService() 
    {
        if(ServiceManager.shortLinkService == null) {
            ServiceManager.shortLinkService = new ShortLinkServiceImpl();
        }
        return ServiceManager.shortLinkService;
    }

    // Returns a GeoLinkService instance.
	public static GeoLinkService getGeoLinkService() 
    {
        if(ServiceManager.geoLinkService == null) {
            ServiceManager.geoLinkService = new GeoLinkServiceImpl();
        }
        return ServiceManager.geoLinkService;
    }

    // Returns a QrCodeService instance.
	public static QrCodeService getQrCodeService() 
    {
        if(ServiceManager.qrCodeService == null) {
            ServiceManager.qrCodeService = new QrCodeServiceImpl();
        }
        return ServiceManager.qrCodeService;
    }

    // Returns a LinkPassphraseService instance.
	public static LinkPassphraseService getLinkPassphraseService() 
    {
        if(ServiceManager.linkPassphraseService == null) {
            ServiceManager.linkPassphraseService = new LinkPassphraseServiceImpl();
        }
        return ServiceManager.linkPassphraseService;
    }

    // Returns a LinkMessageService instance.
	public static LinkMessageService getLinkMessageService() 
    {
        if(ServiceManager.linkMessageService == null) {
            ServiceManager.linkMessageService = new LinkMessageServiceImpl();
        }
        return ServiceManager.linkMessageService;
    }

    // Returns a LinkAlbumService instance.
	public static LinkAlbumService getLinkAlbumService() 
    {
        if(ServiceManager.linkAlbumService == null) {
            ServiceManager.linkAlbumService = new LinkAlbumServiceImpl();
        }
        return ServiceManager.linkAlbumService;
    }

    // Returns a AlbumShortLinkService instance.
	public static AlbumShortLinkService getAlbumShortLinkService() 
    {
        if(ServiceManager.albumShortLinkService == null) {
            ServiceManager.albumShortLinkService = new AlbumShortLinkServiceImpl();
        }
        return ServiceManager.albumShortLinkService;
    }

    // Returns a KeywordFolderService instance.
	public static KeywordFolderService getKeywordFolderService() 
    {
        if(ServiceManager.keywordFolderService == null) {
            ServiceManager.keywordFolderService = new KeywordFolderServiceImpl();
        }
        return ServiceManager.keywordFolderService;
    }

    // Returns a BookmarkFolderService instance.
	public static BookmarkFolderService getBookmarkFolderService() 
    {
        if(ServiceManager.bookmarkFolderService == null) {
            ServiceManager.bookmarkFolderService = new BookmarkFolderServiceImpl();
        }
        return ServiceManager.bookmarkFolderService;
    }

    // Returns a KeywordLinkService instance.
	public static KeywordLinkService getKeywordLinkService() 
    {
        if(ServiceManager.keywordLinkService == null) {
            ServiceManager.keywordLinkService = new KeywordLinkServiceImpl();
        }
        return ServiceManager.keywordLinkService;
    }

    // Returns a BookmarkLinkService instance.
	public static BookmarkLinkService getBookmarkLinkService() 
    {
        if(ServiceManager.bookmarkLinkService == null) {
            ServiceManager.bookmarkLinkService = new BookmarkLinkServiceImpl();
        }
        return ServiceManager.bookmarkLinkService;
    }

    // Returns a SpeedDialService instance.
	public static SpeedDialService getSpeedDialService() 
    {
        if(ServiceManager.speedDialService == null) {
            ServiceManager.speedDialService = new SpeedDialServiceImpl();
        }
        return ServiceManager.speedDialService;
    }

    // Returns a KeywordFolderImportService instance.
	public static KeywordFolderImportService getKeywordFolderImportService() 
    {
        if(ServiceManager.keywordFolderImportService == null) {
            ServiceManager.keywordFolderImportService = new KeywordFolderImportServiceImpl();
        }
        return ServiceManager.keywordFolderImportService;
    }

    // Returns a BookmarkFolderImportService instance.
	public static BookmarkFolderImportService getBookmarkFolderImportService() 
    {
        if(ServiceManager.bookmarkFolderImportService == null) {
            ServiceManager.bookmarkFolderImportService = new BookmarkFolderImportServiceImpl();
        }
        return ServiceManager.bookmarkFolderImportService;
    }

    // Returns a KeywordCrowdTallyService instance.
	public static KeywordCrowdTallyService getKeywordCrowdTallyService() 
    {
        if(ServiceManager.keywordCrowdTallyService == null) {
            ServiceManager.keywordCrowdTallyService = new KeywordCrowdTallyServiceImpl();
        }
        return ServiceManager.keywordCrowdTallyService;
    }

    // Returns a BookmarkCrowdTallyService instance.
	public static BookmarkCrowdTallyService getBookmarkCrowdTallyService() 
    {
        if(ServiceManager.bookmarkCrowdTallyService == null) {
            ServiceManager.bookmarkCrowdTallyService = new BookmarkCrowdTallyServiceImpl();
        }
        return ServiceManager.bookmarkCrowdTallyService;
    }

    // Returns a DomainInfoService instance.
	public static DomainInfoService getDomainInfoService() 
    {
        if(ServiceManager.domainInfoService == null) {
            ServiceManager.domainInfoService = new DomainInfoServiceImpl();
        }
        return ServiceManager.domainInfoService;
    }

    // Returns a UrlRatingService instance.
	public static UrlRatingService getUrlRatingService() 
    {
        if(ServiceManager.urlRatingService == null) {
            ServiceManager.urlRatingService = new UrlRatingServiceImpl();
        }
        return ServiceManager.urlRatingService;
    }

    // Returns a UserRatingService instance.
	public static UserRatingService getUserRatingService() 
    {
        if(ServiceManager.userRatingService == null) {
            ServiceManager.userRatingService = new UserRatingServiceImpl();
        }
        return ServiceManager.userRatingService;
    }

    // Returns a AbuseTagService instance.
	public static AbuseTagService getAbuseTagService() 
    {
        if(ServiceManager.abuseTagService == null) {
            ServiceManager.abuseTagService = new AbuseTagServiceImpl();
        }
        return ServiceManager.abuseTagService;
    }

    // Returns a ServiceInfoService instance.
	public static ServiceInfoService getServiceInfoService() 
    {
        if(ServiceManager.serviceInfoService == null) {
            ServiceManager.serviceInfoService = new ServiceInfoServiceImpl();
        }
        return ServiceManager.serviceInfoService;
    }

    // Returns a FiveTenService instance.
	public static FiveTenService getFiveTenService() 
    {
        if(ServiceManager.fiveTenService == null) {
            ServiceManager.fiveTenService = new FiveTenServiceImpl();
        }
        return ServiceManager.fiveTenService;
    }

}
