package com.myurldb.ws.resource.async;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.CommonConstants;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.exception.InternalServerErrorException;
import com.myurldb.ws.exception.NotImplementedException;
import com.myurldb.ws.exception.RequestConflictException;
import com.myurldb.ws.exception.RequestForbiddenException;
import com.myurldb.ws.exception.DataStoreException;
import com.myurldb.ws.exception.ResourceGoneException;
import com.myurldb.ws.exception.ResourceNotFoundException;
import com.myurldb.ws.exception.ResourceAlreadyPresentException;
import com.myurldb.ws.exception.ServiceUnavailableException;
import com.myurldb.ws.exception.resource.BaseResourceException;
import com.myurldb.ws.resource.exception.BadRequestRsException;
import com.myurldb.ws.resource.exception.InternalServerErrorRsException;
import com.myurldb.ws.resource.exception.NotImplementedRsException;
import com.myurldb.ws.resource.exception.RequestConflictRsException;
import com.myurldb.ws.resource.exception.RequestForbiddenRsException;
import com.myurldb.ws.resource.exception.DataStoreRsException;
import com.myurldb.ws.resource.exception.ResourceGoneRsException;
import com.myurldb.ws.resource.exception.ResourceNotFoundRsException;
import com.myurldb.ws.resource.exception.ResourceAlreadyPresentRsException;
import com.myurldb.ws.resource.exception.ServiceUnavailableRsException;

import com.myurldb.ws.SiteCustomDomain;
import com.myurldb.ws.bean.SiteCustomDomainBean;
import com.myurldb.ws.stub.SiteCustomDomainListStub;
import com.myurldb.ws.stub.SiteCustomDomainStub;
import com.myurldb.ws.resource.ServiceManager;
import com.myurldb.ws.resource.SiteCustomDomainResource;


@Path("/_task/w/siteCustomDomains/")
public class AsyncSiteCustomDomainResource extends BaseAsyncResource implements SiteCustomDomainResource
{
    private static final Logger log = Logger.getLogger(AsyncSiteCustomDomainResource.class.getName());

    private UriInfo uriInfo;
    private HttpHeaders httpHeaders;
    private Request request;
    private String resourceUri;
    private String queueName = null;
    private String taskName = null;
    private Integer retryCount = null;
    private boolean dummyPayload = false;

    public AsyncSiteCustomDomainResource(@Context javax.ws.rs.core.UriInfo uriInfo, @Context javax.ws.rs.core.HttpHeaders httpHeaders, @Context javax.ws.rs.core.Request request)
    {
        this.uriInfo = uriInfo;
        this.httpHeaders = httpHeaders;
        this.request = request;
        this.resourceUri = uriInfo.getAbsolutePath().toString();
        List<String> qns = httpHeaders.getRequestHeader("X-AppEngine-QueueName");
        if(qns != null && qns.size() > 0) {
            this.queueName = qns.get(0);
        }
        List<String> tns = httpHeaders.getRequestHeader("X-AppEngine-TaskName");
        if(tns != null && tns.size() > 0) {
            this.taskName = tns.get(0);
        }
        List<String> rcs = httpHeaders.getRequestHeader("X-AppEngine-TaskRetryCount");
        if(rcs != null && rcs.size() > 0) {
            String strCount = rcs.get(0);
            try {
                this.retryCount = Integer.parseInt(strCount);
            } catch(NumberFormatException ex) {
                // ignore.
                //this.retryCount = 0;
            }
        }
        List<String> ats = httpHeaders.getRequestHeader("X-AsyncTask-Payload");
        if(ats != null && ats.size() > 0 && ats.get(0).equals("DummyPayload")) {
            this.dummyPayload = true;
        }
    }

    private Response getSiteCustomDomainList(List<SiteCustomDomain> beans) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getAllSiteCustomDomains(String ordering, Long offset, Integer count) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getAllSiteCustomDomainKeys(String ordering, Long offset, Integer count) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response findSiteCustomDomains(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response findSiteCustomDomainKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getSiteCustomDomainKeys(List<String> guids) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getSiteCustomDomain(String guid) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getSiteCustomDomain(String guid, String field) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response createSiteCustomDomain(SiteCustomDomainStub siteCustomDomain) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createSiteCustomDomain(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            SiteCustomDomainBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                SiteCustomDomainStub realStub = (SiteCustomDomainStub) getCache().get(taskName);
                if(realStub == null) {
                    // Cached object is likely gone/deleted. No point of retrying...
                    // throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                    // Just return 2xx status code so that there will be no more retries...
                    log.severe("Failed to retrieve the real stub object from cache. This task will be aborted.");
                    return Response.noContent().build();   // ????
                }
                if(log.isLoggable(Level.FINE)) log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertSiteCustomDomainStubToBean(realStub);
            } else {
                bean = convertSiteCustomDomainStubToBean(siteCustomDomain);
            }
            String guid = ServiceManager.getSiteCustomDomainService().createSiteCustomDomain(bean);
            URI createdUri = URI.create(resourceUri + "/" + guid);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createSiteCustomDomain(): Successfully processed the request: createdUri = " + createdUri.toString());
            return Response.created(createdUri).entity(guid).type(MediaType.TEXT_PLAIN).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ResourceAlreadyPresentException ex) {
            throw new ResourceAlreadyPresentRsException(ex, resourceUri);
        } catch(DataStoreException ex) {
            throw new DataStoreRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateSiteCustomDomain(String guid, SiteCustomDomainStub siteCustomDomain) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateSiteCustomDomain(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            //guid = GUID.normalize(guid);    // TBD: Validate guid?
            if(siteCustomDomain == null || !guid.equals(siteCustomDomain.getGuid())) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Path param guid = " + guid + " is different from siteCustomDomain guid = " + siteCustomDomain.getGuid());
                throw new RequestForbiddenException("Failed to update the siteCustomDomain with guid = " + guid);
            }
            SiteCustomDomainBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                SiteCustomDomainStub realStub = (SiteCustomDomainStub) getCache().get(taskName);
                if(realStub == null) {
                    // Cached object is likely gone/deleted. No point of retrying...
                    // throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                    // Just return 2xx status code so that there will be no more retries...
                    log.severe("Failed to retrieve the real stub object from cache. This task will be aborted.");
                    return Response.noContent().build();   // ????
                }
                if(log.isLoggable(Level.FINE)) log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertSiteCustomDomainStubToBean(realStub);
            } else {
                bean = convertSiteCustomDomainStubToBean(siteCustomDomain);
            }
            boolean suc = ServiceManager.getSiteCustomDomainService().updateSiteCustomDomain(bean);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to update the siteCustomDomain with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the siteCustomDomain with guid = " + guid);
            }
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateSiteCustomDomain(): Successfully processed the request: guid = " + guid);
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(DataStoreException ex) {
            throw new DataStoreRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateSiteCustomDomain(String guid, String siteDomain, String domain, String status) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response deleteSiteCustomDomain(String guid) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "deleteSiteCustomDomain(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            //guid = GUID.normalize(guid);    // TBD: Validate guid?
            boolean suc = ServiceManager.getSiteCustomDomainService().deleteSiteCustomDomain(guid);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete the siteCustomDomain with guid = " + guid);
                throw new InternalServerErrorException("Failed to delete the siteCustomDomain with guid = " + guid);
            }
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "deleteSiteCustomDomain(): Successfully processed the request: guid = " + guid);
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(DataStoreException ex) {
            throw new DataStoreRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response deleteSiteCustomDomains(String filter, String params, List<String> values) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    public static SiteCustomDomainBean convertSiteCustomDomainStubToBean(SiteCustomDomain stub)
    {
        SiteCustomDomainBean bean = new SiteCustomDomainBean();
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Empty bean is returned.");
        } else {
            bean.setGuid(stub.getGuid());
            bean.setSiteDomain(stub.getSiteDomain());
            bean.setDomain(stub.getDomain());
            bean.setStatus(stub.getStatus());
            bean.setCreatedTime(stub.getCreatedTime());
            bean.setModifiedTime(stub.getModifiedTime());
        }
        return bean;
    }

}
