package com.myurldb.ws.resource.exception;

import com.myurldb.ws.exception.resource.BaseResourceException;


public class NotImplementedRsException extends BaseResourceException 
{
    private static final long serialVersionUID = 1L;

    public NotImplementedRsException() 
    {
        super();
    }
    public NotImplementedRsException(String message) 
    {
        super(message);
    }
    public NotImplementedRsException(String message, String resource) 
    {
        super(message, resource);
    }
    public NotImplementedRsException(String message, Throwable cause) 
    {
        super(message, cause);
    }
    public NotImplementedRsException(String message, Throwable cause, String resource) 
    {
        super(message, cause, resource);
    }
    public NotImplementedRsException(Throwable cause) 
    {
        super(cause);
    }
    public NotImplementedRsException(Throwable cause, String resource) 
    {
        super(cause, resource);
    }

}
