package com.myurldb.ws.resource.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.myurldb.ws.stub.ErrorStub;

@Provider
public class RequestConflictExceptionMapper implements ExceptionMapper<RequestConflictRsException>
{
    public Response toResponse(RequestConflictRsException ex) {
        return Response.status(Status.CONFLICT)
        .entity(new ErrorStub(ex))
        //.type(MediaType.TEXT_PLAIN)
        .build();
    }

}
