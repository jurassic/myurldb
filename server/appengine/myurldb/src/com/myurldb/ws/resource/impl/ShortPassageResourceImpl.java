package com.myurldb.ws.resource.impl;

import java.io.StringWriter;
import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.ws.rs.Path;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;
import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.api.core.HttpContext;
import com.sun.jersey.oauth.server.OAuthServerRequest;
import com.sun.jersey.oauth.signature.OAuthSignatureException;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.CommonConstants;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.config.Config;
import com.myurldb.ws.auth.TwoLeggedOAuthProvider;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.exception.InternalServerErrorException;
import com.myurldb.ws.exception.NotImplementedException;
import com.myurldb.ws.exception.RequestConflictException;
import com.myurldb.ws.exception.RequestForbiddenException;
import com.myurldb.ws.exception.DataStoreException;
import com.myurldb.ws.exception.ResourceGoneException;
import com.myurldb.ws.exception.ResourceNotFoundException;
import com.myurldb.ws.exception.ResourceAlreadyPresentException;
import com.myurldb.ws.exception.ServiceUnavailableException;
import com.myurldb.ws.exception.resource.BaseResourceException;
import com.myurldb.ws.resource.exception.BadRequestRsException;
import com.myurldb.ws.resource.exception.InternalServerErrorRsException;
import com.myurldb.ws.resource.exception.NotImplementedRsException;
import com.myurldb.ws.resource.exception.RequestConflictRsException;
import com.myurldb.ws.resource.exception.RequestForbiddenRsException;
import com.myurldb.ws.resource.exception.DataStoreRsException;
import com.myurldb.ws.resource.exception.ResourceGoneRsException;
import com.myurldb.ws.resource.exception.ResourceNotFoundRsException;
import com.myurldb.ws.resource.exception.ResourceAlreadyPresentRsException;
import com.myurldb.ws.resource.exception.ServiceUnavailableRsException;
import com.myurldb.ws.resource.exception.UnauthorizedRsException;

import com.myurldb.ws.ShortPassageAttribute;
import com.myurldb.ws.ShortPassage;
import com.myurldb.ws.bean.ShortPassageBean;
import com.myurldb.ws.stub.KeyListStub;
import com.myurldb.ws.stub.ShortPassageListStub;
import com.myurldb.ws.stub.ShortPassageStub;
import com.myurldb.ws.resource.ServiceManager;
import com.myurldb.ws.resource.ShortPassageResource;
import com.myurldb.ws.resource.util.ShortPassageAttributeResourceUtil;


@Path("/shortPassages/")
public class ShortPassageResourceImpl extends BaseResourceImpl implements ShortPassageResource
{
    private static final Logger log = Logger.getLogger(ShortPassageResourceImpl.class.getName());

    private UriInfo uriInfo;
    private HttpHeaders httpHeaders;
    private Request request;
    private String resourceUri;
    private boolean isRunningOnDevel;
    private HttpContext httpContext;

    public ShortPassageResourceImpl(@Context javax.ws.rs.core.UriInfo uriInfo, @Context javax.ws.rs.core.HttpHeaders httpHeaders, @Context javax.ws.rs.core.Request request, @Context ServletContext servletContext, @Context HttpContext httpContext)
    {
        this.uriInfo = uriInfo;
        this.httpHeaders = httpHeaders;
        this.request = request;
        this.resourceUri = uriInfo.getAbsolutePath().toString();

        String serverInfo = servletContext.getServerInfo();
        if(log.isLoggable(Level.INFO)) log.info("ServerInfo = " + serverInfo);
        if(serverInfo != null && serverInfo.contains("Development")) {
            isRunningOnDevel = true;
        } else {
            isRunningOnDevel = false;
        }        

        this.httpContext = httpContext;
    }

    private Response getShortPassageList(List<ShortPassage> beans) throws BaseResourceException
    {
        log.finer("BEGIN");

        if(beans == null) {
            log.log(Level.WARNING, "Failed to retrieve the list.");
            throw new RequestForbiddenRsException("Failed to retrieve the list.");
        }
        if(beans.size() == 0) {
            log.log(Level.INFO, "Retrieved an empty list.");
            return Response.ok(new ShortPassageListStub()).cacheControl(CacheControl.valueOf("no-cache")).build();
        } else {
            long lastModifiedTime = 0L;
            List<ShortPassageStub> stubs = new ArrayList<ShortPassageStub>();
            Iterator<ShortPassage> it = beans.iterator();
            while(it.hasNext()) {
                ShortPassage bean = (ShortPassage) it.next();
                stubs.add(ShortPassageStub.convertBeanToStub(bean));
                if(bean.getModifiedTime() != null) {
                    if(bean.getModifiedTime() > lastModifiedTime) {
                        lastModifiedTime = bean.getModifiedTime();
                    }
                } else {
                    if(bean.getCreatedTime() > lastModifiedTime) {
                        lastModifiedTime = bean.getCreatedTime();
                    }
                }
            }
            return Response.ok(new ShortPassageListStub(stubs)).cacheControl(CacheControl.valueOf("no-cache")).lastModified(new Date(lastModifiedTime)).build();
        }
    }

    @Override
    public Response getAllShortPassages(String ordering, Long offset, Integer count) throws BaseResourceException
    {
        log.finer("BEGIN");

        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            List<ShortPassage> beans = ServiceManager.getShortPassageService().getAllShortPassages(ordering, offset, count);
            return getShortPassageList(beans);
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response getAllShortPassageKeys(String ordering, Long offset, Integer count) throws BaseResourceException
    {
        log.finer("BEGIN");

        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            List<String> keys = ServiceManager.getShortPassageService().getAllShortPassageKeys(ordering, offset, count);
            return Response.ok(new KeyListStub(keys)).cacheControl(CacheControl.valueOf("no-cache")).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response findShortPassages(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseResourceException
    {
        log.finer("BEGIN");

        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            List<ShortPassage> beans = ServiceManager.getShortPassageService().findShortPassages(filter, ordering, params, values, grouping, unique, offset, count);
            return getShortPassageList(beans);
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response findShortPassageKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseResourceException
    {
        log.finer("BEGIN");

        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            List<String> keys = ServiceManager.getShortPassageService().findShortPassageKeys(filter, ordering, params, values, grouping, unique, offset, count);
            return Response.ok(new KeyListStub(keys)).cacheControl(CacheControl.valueOf("no-cache")).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response getShortPassageKeys(List<String> guids) throws BaseResourceException
    {
        // TBD:
        throw new NotImplementedRsException("To be implemented", resourceUri);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        log.finer("BEGIN");

        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            Long count = ServiceManager.getShortPassageService().getCount(filter, params, values, aggregate);
            return Response.ok(count.toString()).type(MediaType.TEXT_PLAIN).cacheControl(CacheControl.valueOf("no-cache")).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response getShortPassage(String guid) throws BaseResourceException
    {
        log.finer("BEGIN");

        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            //guid = GUID.normalize(guid);    // TBD: Validate guid?
            ShortPassage bean = ServiceManager.getShortPassageService().getShortPassage(guid);

            EntityTag eTag = new EntityTag(Integer.toString(bean.hashCode()));
            Response.ResponseBuilder responseBuilder = request.evaluatePreconditions(eTag);
            if (responseBuilder != null) {  // Etag match
                log.info("ShortPassage object has not changed. Returning unmodified response code.");
                return responseBuilder.build();
            }
            log.info("Returning a full ShortPassage object.");

            ShortPassageStub stub = ShortPassageStub.convertBeanToStub(bean);
            //Date expirationDate = new Date(System.currentTimeMillis() + CommonConstants.CACHE_EXPIRATION_TIME);
            //ResponseBuilder builder = Response.ok(stub).cacheControl(CacheControl.valueOf("private")).tag(eTag).expires(expirationDate);
            ResponseBuilder builder = Response.ok(stub).cacheControl(CacheControl.valueOf("private")).tag(eTag);
            Date modifiedDate = null;
            if(bean.getModifiedTime() != null) {
                modifiedDate = new Date(bean.getModifiedTime());
            } else {
                if(bean.getCreatedTime() != null) {
                    modifiedDate = new Date(bean.getCreatedTime());
                } else {   // This should not happen.
                    modifiedDate = new Date();  // Just use "now".                        
                }
            }
            return builder.lastModified(modifiedDate).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response getShortPassage(String guid, String field) throws BaseResourceException
    {
        log.finer("BEGIN");

        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            //guid = GUID.normalize(guid);    // TBD: Validate guid?
            if(field == null || field.trim().length() == 0) {
                return getShortPassage(guid);
            }
            ShortPassage bean = ServiceManager.getShortPassageService().getShortPassage(guid);
            String value = null;
            if(bean != null) {
                if(field.equals("guid")) {
                    value = bean.getGuid();
                } else if(field.equals("owner")) {
                    String fval = bean.getOwner();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("longText")) {
                    String fval = bean.getLongText();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("shortText")) {
                    String fval = bean.getShortText();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("attribute")) {
                    ShortPassageAttribute fval = bean.getAttribute();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("readOnly")) {
                    Boolean fval = bean.isReadOnly();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("status")) {
                    String fval = bean.getStatus();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("note")) {
                    String fval = bean.getNote();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("expirationTime")) {
                    Long fval = bean.getExpirationTime();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("createdTime")) {
                    Long fval = bean.getCreatedTime();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("modifiedTime")) {
                    Long fval = bean.getModifiedTime();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                }
            }
            //Date expirationDate = new Date(System.currentTimeMillis() + CommonConstants.CACHE_EXPIRATION_TIME);
            //ResponseBuilder builder = Response.ok(value).type(MediaType.TEXT_PLAIN).cacheControl(CacheControl.valueOf("private")).expires(expirationDate);
            ResponseBuilder builder = Response.ok(value).type(MediaType.TEXT_PLAIN).cacheControl(CacheControl.valueOf("private"));
            Date modifiedDate = null;
            if(bean.getModifiedTime() != null) {
                modifiedDate = new Date(bean.getModifiedTime());
            } else {
                if(bean.getCreatedTime() != null) {
                    modifiedDate = new Date(bean.getCreatedTime());
                } else {   // This should not happen.
                    modifiedDate = new Date();  // Just use "now".                        
                }
            }
            return builder.lastModified(modifiedDate).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }        
    }

    @Override
    public Response createShortPassage(ShortPassageStub shortPassage) throws BaseResourceException
    {
        log.finer("BEGIN");

        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        if(useAsyncService()) {
            log.log(Level.INFO, "createShortPassage(): Invoking an async call.");
            String guid = shortPassage.getGuid();
            if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
                guid = GUID.generate();
                shortPassage.setGuid(guid);
            }
            Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
            //String taskName = "WsCreateShortPassage-" + guid;
            String taskName = "WsCreateShortPassage-" + guid + "-" + (new Date()).getTime();
            TaskOptions taskOpt = null;
            try {
                final JAXBContext jAXBContext = JAXBContext.newInstance(ShortPassageStub.class);
                Marshaller marshaller = null;
                StringWriter writer = null;

                // Note that the actual size will be bigger than this in xml or json format...
                int approxPayloadSize = shortPassage.toString().length() * 2;  // 2 bytes per char
                if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
                if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                    if(getCache() != null) {
                        getCache().put(taskName, shortPassage);
                    
                        ShortPassageStub dummyStub = new ShortPassageStub();
                        dummyStub.setGuid(shortPassage.getGuid());
                        marshaller = jAXBContext.createMarshaller();
                        //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                        marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                        writer = new StringWriter();
                        marshaller.marshal(dummyStub, writer);
                        String dummyPayload = writer.toString();
                        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createShortPassage(): dummyPayload = " + dummyPayload);

                        // TBD: Add Oauth params/secrets, if TwoLeggedOAuthFilter is used.
                        taskOpt = withUrl(TASK_URIPATH_PREFIX + "shortPassages/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                    } else {
                        throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                    }
                } else {
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(shortPassage, writer);
                    String payload = writer.toString();
                    if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createShortPassage(): payload = " + payload);

                    // TBD: Add Oauth params/secrets, if TwoLeggedOAuthFilter is used.
                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "shortPassages/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
                }
            } catch (JAXBException e) {
                log.log(Level.WARNING, "Marshaling failed during task enqueing.");
                throw new BaseResourceException("Marshaling failed during task enqueing.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Payload generation failed during task enqueing.");
                throw new BaseResourceException("Payload generation failed during task enqueing.", e);
            }

            queue.add(taskOpt);
            URI createdUri = URI.create(resourceUri + "/" + guid);  // The resource hasn't been created yet, but the client still might expect the location header!
            return Response.status(Status.ACCEPTED).location(createdUri).entity(guid).build();
        }
        // else

        try {
            ShortPassageBean bean = convertShortPassageStubToBean(shortPassage);
            String guid = ServiceManager.getShortPassageService().createShortPassage(bean);
            URI createdUri = URI.create(resourceUri + "/" + guid);
            return Response.created(createdUri).entity(guid).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ResourceAlreadyPresentException ex) {
            throw new ResourceAlreadyPresentRsException(ex, resourceUri);
        } catch(DataStoreException ex) {
            throw new DataStoreRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateShortPassage(String guid, ShortPassageStub shortPassage) throws BaseResourceException
    {
        log.finer("BEGIN");

        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        //guid = GUID.normalize(guid);    // TBD: Validate guid?
        if(shortPassage == null || !guid.equals(shortPassage.getGuid())) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Path param guid = " + guid + " is different from shortPassage guid = " + shortPassage.getGuid());
            throw new RequestForbiddenRsException("Failed to update the shortPassage with guid = " + guid);
        }

        if(useAsyncService()) {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateShortPassage(): Invoking an async call: guid = " + guid);
            Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
            //String taskName = "WsUpdateShortPassage-" + guid;
            String taskName = "WsUpdateShortPassage-" + guid + "-" + (new Date()).getTime();
            TaskOptions taskOpt = null;
            try {
                final JAXBContext jAXBContext = JAXBContext.newInstance(ShortPassageStub.class);
                Marshaller marshaller = null;
                StringWriter writer = null;

                // Note that the actual size will be bigger than this in xml or json format...
                int approxPayloadSize = shortPassage.toString().length() * 2;  // 2 bytes per char
                if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
                if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                    if(getCache() != null) {
                        getCache().put(taskName, shortPassage);

                        ShortPassageStub dummyStub = new ShortPassageStub();
                        dummyStub.setGuid(shortPassage.getGuid());
                        marshaller = jAXBContext.createMarshaller();
                        //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                        marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                        writer = new StringWriter();
                        marshaller.marshal(dummyStub, writer);
                        String dummyPayload = writer.toString();
                        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateShortPassage(): dummyPayload = " + dummyPayload);

                        // TBD: Add Oauth params/secrets, if TwoLeggedOAuthFilter is used.
                        taskOpt = withUrl(TASK_URIPATH_PREFIX + "shortPassages/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                    } else {
                        throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                    }
                } else {
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(shortPassage, writer);
                    String payload = writer.toString();
                    if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateShortPassage(): payload = " + payload);

                    // TBD: Add Oauth params/secrets, if TwoLeggedOAuthFilter is used.
                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "shortPassages/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
                }
            } catch (JAXBException e) {
                log.log(Level.WARNING, "Marshaling failed during task enqueing.");
                throw new BaseResourceException("Marshaling failed during task enqueing.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Payload generation failed during task enqueing.");
                throw new BaseResourceException("Payload generation failed during task enqueing.", e);
            }

            queue.add(taskOpt);
            return Response.status(Status.ACCEPTED).build();
        }
        // else

        try {
            ShortPassageBean bean = convertShortPassageStubToBean(shortPassage);
            boolean suc = ServiceManager.getShortPassageService().updateShortPassage(bean);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to update the shortPassage with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the shortPassage with guid = " + guid);
            }
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(DataStoreException ex) {
            throw new DataStoreRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateShortPassage(String guid, String owner, String longText, String shortText, String attribute, Boolean readOnly, String status, String note, Long expirationTime) throws BaseResourceException
    {
        log.finer("BEGIN");

        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            /*
            //guid = GUID.normalize(guid);    // TBD: Validate guid?
            boolean suc = ServiceManager.getShortPassageService().updateShortPassage(guid, owner, longText, shortText, attribute, readOnly, status, note, expirationTime);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to update the shortPassage with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the shortPassage with guid = " + guid);
            }
            return Response.noContent().build();
            */
            throw new NotImplementedException("This method has not been implemented yet.");
//        } catch(BadRequestException ex) {
//            throw new BadRequestRsException(ex, resourceUri);
//        } catch(ResourceNotFoundException ex) {
//            throw new ResourceNotFoundRsException(ex, resourceUri);
//        } catch(ResourceGoneException ex) {
//            throw new ResourceGoneRsException(ex, resourceUri);
//        } catch(RequestForbiddenException ex) {
//            throw new RequestForbiddenRsException(ex, resourceUri);
//        } catch(RequestConflictException ex) {
//            throw new RequestConflictRsException(ex, resourceUri);
//        } catch(DataStoreException ex) {
//            throw new DataStoreRsException(ex, resourceUri);
//        } catch(ServiceUnavailableException ex) {
//            throw new ServiceUnavailableRsException(ex, resourceUri);
//        } catch(InternalServerErrorException ex) {
//            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(NotImplementedException ex) {
            throw new NotImplementedRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response deleteShortPassage(String guid) throws BaseResourceException
    {
        log.finer("BEGIN");

        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        //guid = GUID.normalize(guid);    // TBD: Validate guid?
        if(useAsyncService()) {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "deleteShortPassage(): Invoking an async call: guid = " + guid);
            Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
            //String taskName = "WsDeleteShortPassage-" + guid;
            String taskName = "WsDeleteShortPassage-" + guid + "-" + (new Date()).getTime();

            // TBD: Add Oauth params/secrets, if TwoLeggedOAuthFilter is used.
            TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "shortPassages/" + guid).method(Method.DELETE).taskName(taskName);
            queue.add(taskOpt);
            return Response.status(Status.ACCEPTED).build();
        }
        // else

        try {
            boolean suc = ServiceManager.getShortPassageService().deleteShortPassage(guid);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete the shortPassage with guid = " + guid);
                throw new InternalServerErrorException("Failed to delete the shortPassage with guid = " + guid);
            }
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(DataStoreException ex) {
            throw new DataStoreRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response deleteShortPassages(String filter, String params, List<String> values) throws BaseResourceException
    {
        log.finer("BEGIN");

        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            Long count = ServiceManager.getShortPassageService().deleteShortPassages(filter, params, values);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Delete count = " + count);
            return Response.ok(count.toString()).type(MediaType.TEXT_PLAIN).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(DataStoreException ex) {
            throw new DataStoreRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    public static ShortPassageBean convertShortPassageStubToBean(ShortPassage stub)
    {
        ShortPassageBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null bean is returned.");
        } else {
            bean = new ShortPassageBean();
            bean.setGuid(stub.getGuid());
            bean.setOwner(stub.getOwner());
            bean.setLongText(stub.getLongText());
            bean.setShortText(stub.getShortText());
            bean.setAttribute(ShortPassageAttributeResourceUtil.convertShortPassageAttributeStubToBean(stub.getAttribute()));
            bean.setReadOnly(stub.isReadOnly());
            bean.setStatus(stub.getStatus());
            bean.setNote(stub.getNote());
            bean.setExpirationTime(stub.getExpirationTime());
            bean.setCreatedTime(stub.getCreatedTime());
            bean.setModifiedTime(stub.getModifiedTime());
        }
        return bean;
    }

    public static List<ShortPassageBean> convertShortPassageListStubToBeanList(ShortPassageListStub listStub)
    {
        if(listStub == null) {
            log.log(Level.INFO, "listStub is null. Null list is returned.");
            return null;
        } else {
            List<ShortPassageStub> stubList = listStub.getList();
            if(stubList == null) {
                log.log(Level.INFO, "Stub list is null. Null list is returned.");
                return null;                
            }
            List<ShortPassageBean> beanList = new ArrayList<ShortPassageBean>();
            if(stubList.isEmpty()) {
                log.log(Level.INFO, "Stub list is empty. Empty list is returned.");
            } else {
                for(ShortPassageStub stub : stubList) {
                    ShortPassageBean bean = convertShortPassageStubToBean(stub);
                    beanList.add(bean);                            
                }
            }
            return beanList;
        }
    }

}
