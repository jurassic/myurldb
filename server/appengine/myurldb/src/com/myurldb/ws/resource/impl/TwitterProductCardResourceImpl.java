package com.myurldb.ws.resource.impl;

import java.io.StringWriter;
import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.ws.rs.Path;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;
import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.api.core.HttpContext;
import com.sun.jersey.oauth.server.OAuthServerRequest;
import com.sun.jersey.oauth.signature.OAuthSignatureException;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.CommonConstants;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.config.Config;
import com.myurldb.ws.auth.TwoLeggedOAuthProvider;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.exception.InternalServerErrorException;
import com.myurldb.ws.exception.NotImplementedException;
import com.myurldb.ws.exception.RequestConflictException;
import com.myurldb.ws.exception.RequestForbiddenException;
import com.myurldb.ws.exception.DataStoreException;
import com.myurldb.ws.exception.ResourceGoneException;
import com.myurldb.ws.exception.ResourceNotFoundException;
import com.myurldb.ws.exception.ResourceAlreadyPresentException;
import com.myurldb.ws.exception.ServiceUnavailableException;
import com.myurldb.ws.exception.resource.BaseResourceException;
import com.myurldb.ws.resource.exception.BadRequestRsException;
import com.myurldb.ws.resource.exception.InternalServerErrorRsException;
import com.myurldb.ws.resource.exception.NotImplementedRsException;
import com.myurldb.ws.resource.exception.RequestConflictRsException;
import com.myurldb.ws.resource.exception.RequestForbiddenRsException;
import com.myurldb.ws.resource.exception.DataStoreRsException;
import com.myurldb.ws.resource.exception.ResourceGoneRsException;
import com.myurldb.ws.resource.exception.ResourceNotFoundRsException;
import com.myurldb.ws.resource.exception.ResourceAlreadyPresentRsException;
import com.myurldb.ws.resource.exception.ServiceUnavailableRsException;
import com.myurldb.ws.resource.exception.UnauthorizedRsException;

import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.TwitterProductCard;
import com.myurldb.ws.bean.TwitterProductCardBean;
import com.myurldb.ws.stub.KeyListStub;
import com.myurldb.ws.stub.TwitterProductCardListStub;
import com.myurldb.ws.stub.TwitterProductCardStub;
import com.myurldb.ws.resource.ServiceManager;
import com.myurldb.ws.resource.TwitterProductCardResource;
import com.myurldb.ws.resource.util.TwitterCardAppInfoResourceUtil;
import com.myurldb.ws.resource.util.TwitterCardProductDataResourceUtil;


@Path("/twitterProductCards/")
public class TwitterProductCardResourceImpl extends BaseResourceImpl implements TwitterProductCardResource
{
    private static final Logger log = Logger.getLogger(TwitterProductCardResourceImpl.class.getName());

    private UriInfo uriInfo;
    private HttpHeaders httpHeaders;
    private Request request;
    private String resourceUri;
    private boolean isRunningOnDevel;
    private HttpContext httpContext;

    public TwitterProductCardResourceImpl(@Context javax.ws.rs.core.UriInfo uriInfo, @Context javax.ws.rs.core.HttpHeaders httpHeaders, @Context javax.ws.rs.core.Request request, @Context ServletContext servletContext, @Context HttpContext httpContext)
    {
        this.uriInfo = uriInfo;
        this.httpHeaders = httpHeaders;
        this.request = request;
        this.resourceUri = uriInfo.getAbsolutePath().toString();

        String serverInfo = servletContext.getServerInfo();
        if(log.isLoggable(Level.INFO)) log.info("ServerInfo = " + serverInfo);
        if(serverInfo != null && serverInfo.contains("Development")) {
            isRunningOnDevel = true;
        } else {
            isRunningOnDevel = false;
        }        

        this.httpContext = httpContext;
    }

    private Response getTwitterProductCardList(List<TwitterProductCard> beans) throws BaseResourceException
    {
        log.finer("BEGIN");

        if(beans == null) {
            log.log(Level.WARNING, "Failed to retrieve the list.");
            throw new RequestForbiddenRsException("Failed to retrieve the list.");
        }
        if(beans.size() == 0) {
            log.log(Level.INFO, "Retrieved an empty list.");
            return Response.ok(new TwitterProductCardListStub()).cacheControl(CacheControl.valueOf("no-cache")).build();
        } else {
            long lastModifiedTime = 0L;
            List<TwitterProductCardStub> stubs = new ArrayList<TwitterProductCardStub>();
            Iterator<TwitterProductCard> it = beans.iterator();
            while(it.hasNext()) {
                TwitterProductCard bean = (TwitterProductCard) it.next();
                stubs.add(TwitterProductCardStub.convertBeanToStub(bean));
                if(bean.getModifiedTime() != null) {
                    if(bean.getModifiedTime() > lastModifiedTime) {
                        lastModifiedTime = bean.getModifiedTime();
                    }
                } else {
                    if(bean.getCreatedTime() > lastModifiedTime) {
                        lastModifiedTime = bean.getCreatedTime();
                    }
                }
            }
            return Response.ok(new TwitterProductCardListStub(stubs)).cacheControl(CacheControl.valueOf("no-cache")).lastModified(new Date(lastModifiedTime)).build();
        }
    }

    @Override
    public Response getAllTwitterProductCards(String ordering, Long offset, Integer count) throws BaseResourceException
    {
        log.finer("BEGIN");

        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            List<TwitterProductCard> beans = ServiceManager.getTwitterProductCardService().getAllTwitterProductCards(ordering, offset, count);
            return getTwitterProductCardList(beans);
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response getAllTwitterProductCardKeys(String ordering, Long offset, Integer count) throws BaseResourceException
    {
        log.finer("BEGIN");

        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            List<String> keys = ServiceManager.getTwitterProductCardService().getAllTwitterProductCardKeys(ordering, offset, count);
            return Response.ok(new KeyListStub(keys)).cacheControl(CacheControl.valueOf("no-cache")).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response findTwitterProductCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseResourceException
    {
        log.finer("BEGIN");

        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            List<TwitterProductCard> beans = ServiceManager.getTwitterProductCardService().findTwitterProductCards(filter, ordering, params, values, grouping, unique, offset, count);
            return getTwitterProductCardList(beans);
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response findTwitterProductCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseResourceException
    {
        log.finer("BEGIN");

        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            List<String> keys = ServiceManager.getTwitterProductCardService().findTwitterProductCardKeys(filter, ordering, params, values, grouping, unique, offset, count);
            return Response.ok(new KeyListStub(keys)).cacheControl(CacheControl.valueOf("no-cache")).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response getTwitterProductCardKeys(List<String> guids) throws BaseResourceException
    {
        // TBD:
        throw new NotImplementedRsException("To be implemented", resourceUri);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        log.finer("BEGIN");

        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            Long count = ServiceManager.getTwitterProductCardService().getCount(filter, params, values, aggregate);
            return Response.ok(count.toString()).type(MediaType.TEXT_PLAIN).cacheControl(CacheControl.valueOf("no-cache")).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response getTwitterProductCard(String guid) throws BaseResourceException
    {
        log.finer("BEGIN");

        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            //guid = GUID.normalize(guid);    // TBD: Validate guid?
            TwitterProductCard bean = ServiceManager.getTwitterProductCardService().getTwitterProductCard(guid);

            EntityTag eTag = new EntityTag(Integer.toString(bean.hashCode()));
            Response.ResponseBuilder responseBuilder = request.evaluatePreconditions(eTag);
            if (responseBuilder != null) {  // Etag match
                log.info("TwitterProductCard object has not changed. Returning unmodified response code.");
                return responseBuilder.build();
            }
            log.info("Returning a full TwitterProductCard object.");

            TwitterProductCardStub stub = TwitterProductCardStub.convertBeanToStub(bean);
            //Date expirationDate = new Date(System.currentTimeMillis() + CommonConstants.CACHE_EXPIRATION_TIME);
            //ResponseBuilder builder = Response.ok(stub).cacheControl(CacheControl.valueOf("private")).tag(eTag).expires(expirationDate);
            ResponseBuilder builder = Response.ok(stub).cacheControl(CacheControl.valueOf("private")).tag(eTag);
            Date modifiedDate = null;
            if(bean.getModifiedTime() != null) {
                modifiedDate = new Date(bean.getModifiedTime());
            } else {
                if(bean.getCreatedTime() != null) {
                    modifiedDate = new Date(bean.getCreatedTime());
                } else {   // This should not happen.
                    modifiedDate = new Date();  // Just use "now".                        
                }
            }
            return builder.lastModified(modifiedDate).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response getTwitterProductCard(String guid, String field) throws BaseResourceException
    {
        log.finer("BEGIN");

        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            //guid = GUID.normalize(guid);    // TBD: Validate guid?
            if(field == null || field.trim().length() == 0) {
                return getTwitterProductCard(guid);
            }
            TwitterProductCard bean = ServiceManager.getTwitterProductCardService().getTwitterProductCard(guid);
            String value = null;
            if(bean != null) {
                if(field.equals("guid")) {
                    value = bean.getGuid();
                } else if(field.equals("card")) {
                    String fval = bean.getCard();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("url")) {
                    String fval = bean.getUrl();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("title")) {
                    String fval = bean.getTitle();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("description")) {
                    String fval = bean.getDescription();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("site")) {
                    String fval = bean.getSite();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("siteId")) {
                    String fval = bean.getSiteId();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("creator")) {
                    String fval = bean.getCreator();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("creatorId")) {
                    String fval = bean.getCreatorId();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("image")) {
                    String fval = bean.getImage();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("imageWidth")) {
                    Integer fval = bean.getImageWidth();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("imageHeight")) {
                    Integer fval = bean.getImageHeight();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("data1")) {
                    TwitterCardProductData fval = bean.getData1();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("data2")) {
                    TwitterCardProductData fval = bean.getData2();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("createdTime")) {
                    Long fval = bean.getCreatedTime();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("modifiedTime")) {
                    Long fval = bean.getModifiedTime();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                }
            }
            //Date expirationDate = new Date(System.currentTimeMillis() + CommonConstants.CACHE_EXPIRATION_TIME);
            //ResponseBuilder builder = Response.ok(value).type(MediaType.TEXT_PLAIN).cacheControl(CacheControl.valueOf("private")).expires(expirationDate);
            ResponseBuilder builder = Response.ok(value).type(MediaType.TEXT_PLAIN).cacheControl(CacheControl.valueOf("private"));
            Date modifiedDate = null;
            if(bean.getModifiedTime() != null) {
                modifiedDate = new Date(bean.getModifiedTime());
            } else {
                if(bean.getCreatedTime() != null) {
                    modifiedDate = new Date(bean.getCreatedTime());
                } else {   // This should not happen.
                    modifiedDate = new Date();  // Just use "now".                        
                }
            }
            return builder.lastModified(modifiedDate).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }        
    }

    @Override
    public Response createTwitterProductCard(TwitterProductCardStub twitterProductCard) throws BaseResourceException
    {
        log.finer("BEGIN");

        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        if(useAsyncService()) {
            log.log(Level.INFO, "createTwitterProductCard(): Invoking an async call.");
            String guid = twitterProductCard.getGuid();
            if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
                guid = GUID.generate();
                twitterProductCard.setGuid(guid);
            }
            Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
            //String taskName = "WsCreateTwitterProductCard-" + guid;
            String taskName = "WsCreateTwitterProductCard-" + guid + "-" + (new Date()).getTime();
            TaskOptions taskOpt = null;
            try {
                final JAXBContext jAXBContext = JAXBContext.newInstance(TwitterProductCardStub.class);
                Marshaller marshaller = null;
                StringWriter writer = null;

                // Note that the actual size will be bigger than this in xml or json format...
                int approxPayloadSize = twitterProductCard.toString().length() * 2;  // 2 bytes per char
                if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
                if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                    if(getCache() != null) {
                        getCache().put(taskName, twitterProductCard);
                    
                        TwitterProductCardStub dummyStub = new TwitterProductCardStub();
                        dummyStub.setGuid(twitterProductCard.getGuid());
                        marshaller = jAXBContext.createMarshaller();
                        //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                        marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                        writer = new StringWriter();
                        marshaller.marshal(dummyStub, writer);
                        String dummyPayload = writer.toString();
                        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createTwitterProductCard(): dummyPayload = " + dummyPayload);

                        // TBD: Add Oauth params/secrets, if TwoLeggedOAuthFilter is used.
                        taskOpt = withUrl(TASK_URIPATH_PREFIX + "twitterProductCards/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                    } else {
                        throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                    }
                } else {
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(twitterProductCard, writer);
                    String payload = writer.toString();
                    if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createTwitterProductCard(): payload = " + payload);

                    // TBD: Add Oauth params/secrets, if TwoLeggedOAuthFilter is used.
                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "twitterProductCards/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
                }
            } catch (JAXBException e) {
                log.log(Level.WARNING, "Marshaling failed during task enqueing.");
                throw new BaseResourceException("Marshaling failed during task enqueing.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Payload generation failed during task enqueing.");
                throw new BaseResourceException("Payload generation failed during task enqueing.", e);
            }

            queue.add(taskOpt);
            URI createdUri = URI.create(resourceUri + "/" + guid);  // The resource hasn't been created yet, but the client still might expect the location header!
            return Response.status(Status.ACCEPTED).location(createdUri).entity(guid).build();
        }
        // else

        try {
            TwitterProductCardBean bean = convertTwitterProductCardStubToBean(twitterProductCard);
            String guid = ServiceManager.getTwitterProductCardService().createTwitterProductCard(bean);
            URI createdUri = URI.create(resourceUri + "/" + guid);
            return Response.created(createdUri).entity(guid).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ResourceAlreadyPresentException ex) {
            throw new ResourceAlreadyPresentRsException(ex, resourceUri);
        } catch(DataStoreException ex) {
            throw new DataStoreRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateTwitterProductCard(String guid, TwitterProductCardStub twitterProductCard) throws BaseResourceException
    {
        log.finer("BEGIN");

        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        //guid = GUID.normalize(guid);    // TBD: Validate guid?
        if(twitterProductCard == null || !guid.equals(twitterProductCard.getGuid())) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Path param guid = " + guid + " is different from twitterProductCard guid = " + twitterProductCard.getGuid());
            throw new RequestForbiddenRsException("Failed to update the twitterProductCard with guid = " + guid);
        }

        if(useAsyncService()) {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateTwitterProductCard(): Invoking an async call: guid = " + guid);
            Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
            //String taskName = "WsUpdateTwitterProductCard-" + guid;
            String taskName = "WsUpdateTwitterProductCard-" + guid + "-" + (new Date()).getTime();
            TaskOptions taskOpt = null;
            try {
                final JAXBContext jAXBContext = JAXBContext.newInstance(TwitterProductCardStub.class);
                Marshaller marshaller = null;
                StringWriter writer = null;

                // Note that the actual size will be bigger than this in xml or json format...
                int approxPayloadSize = twitterProductCard.toString().length() * 2;  // 2 bytes per char
                if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
                if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                    if(getCache() != null) {
                        getCache().put(taskName, twitterProductCard);

                        TwitterProductCardStub dummyStub = new TwitterProductCardStub();
                        dummyStub.setGuid(twitterProductCard.getGuid());
                        marshaller = jAXBContext.createMarshaller();
                        //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                        marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                        writer = new StringWriter();
                        marshaller.marshal(dummyStub, writer);
                        String dummyPayload = writer.toString();
                        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateTwitterProductCard(): dummyPayload = " + dummyPayload);

                        // TBD: Add Oauth params/secrets, if TwoLeggedOAuthFilter is used.
                        taskOpt = withUrl(TASK_URIPATH_PREFIX + "twitterProductCards/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                    } else {
                        throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                    }
                } else {
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(twitterProductCard, writer);
                    String payload = writer.toString();
                    if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateTwitterProductCard(): payload = " + payload);

                    // TBD: Add Oauth params/secrets, if TwoLeggedOAuthFilter is used.
                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "twitterProductCards/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
                }
            } catch (JAXBException e) {
                log.log(Level.WARNING, "Marshaling failed during task enqueing.");
                throw new BaseResourceException("Marshaling failed during task enqueing.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Payload generation failed during task enqueing.");
                throw new BaseResourceException("Payload generation failed during task enqueing.", e);
            }

            queue.add(taskOpt);
            return Response.status(Status.ACCEPTED).build();
        }
        // else

        try {
            TwitterProductCardBean bean = convertTwitterProductCardStubToBean(twitterProductCard);
            boolean suc = ServiceManager.getTwitterProductCardService().updateTwitterProductCard(bean);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to update the twitterProductCard with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the twitterProductCard with guid = " + guid);
            }
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(DataStoreException ex) {
            throw new DataStoreRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateTwitterProductCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, String data1, String data2) throws BaseResourceException
    {
        log.finer("BEGIN");

        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            /*
            //guid = GUID.normalize(guid);    // TBD: Validate guid?
            boolean suc = ServiceManager.getTwitterProductCardService().updateTwitterProductCard(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, data1, data2);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to update the twitterProductCard with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the twitterProductCard with guid = " + guid);
            }
            return Response.noContent().build();
            */
            throw new NotImplementedException("This method has not been implemented yet.");
//        } catch(BadRequestException ex) {
//            throw new BadRequestRsException(ex, resourceUri);
//        } catch(ResourceNotFoundException ex) {
//            throw new ResourceNotFoundRsException(ex, resourceUri);
//        } catch(ResourceGoneException ex) {
//            throw new ResourceGoneRsException(ex, resourceUri);
//        } catch(RequestForbiddenException ex) {
//            throw new RequestForbiddenRsException(ex, resourceUri);
//        } catch(RequestConflictException ex) {
//            throw new RequestConflictRsException(ex, resourceUri);
//        } catch(DataStoreException ex) {
//            throw new DataStoreRsException(ex, resourceUri);
//        } catch(ServiceUnavailableException ex) {
//            throw new ServiceUnavailableRsException(ex, resourceUri);
//        } catch(InternalServerErrorException ex) {
//            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(NotImplementedException ex) {
            throw new NotImplementedRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response deleteTwitterProductCard(String guid) throws BaseResourceException
    {
        log.finer("BEGIN");

        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        //guid = GUID.normalize(guid);    // TBD: Validate guid?
        if(useAsyncService()) {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "deleteTwitterProductCard(): Invoking an async call: guid = " + guid);
            Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
            //String taskName = "WsDeleteTwitterProductCard-" + guid;
            String taskName = "WsDeleteTwitterProductCard-" + guid + "-" + (new Date()).getTime();

            // TBD: Add Oauth params/secrets, if TwoLeggedOAuthFilter is used.
            TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "twitterProductCards/" + guid).method(Method.DELETE).taskName(taskName);
            queue.add(taskOpt);
            return Response.status(Status.ACCEPTED).build();
        }
        // else

        try {
            boolean suc = ServiceManager.getTwitterProductCardService().deleteTwitterProductCard(guid);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete the twitterProductCard with guid = " + guid);
                throw new InternalServerErrorException("Failed to delete the twitterProductCard with guid = " + guid);
            }
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(DataStoreException ex) {
            throw new DataStoreRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response deleteTwitterProductCards(String filter, String params, List<String> values) throws BaseResourceException
    {
        log.finer("BEGIN");

        if(isRunningOnDevel && isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
        } else {
            // Verify the signature first.
            try {
                if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                    log.warning("Two-legged OAuth verification failed.");
                    throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                }
            } catch (BadRequestException e) {
                log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
            } catch (BaseException e) {
                log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
            } catch (Exception e) {
                log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
            }
        }

        try {
            Long count = ServiceManager.getTwitterProductCardService().deleteTwitterProductCards(filter, params, values);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Delete count = " + count);
            return Response.ok(count.toString()).type(MediaType.TEXT_PLAIN).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(DataStoreException ex) {
            throw new DataStoreRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    public static TwitterProductCardBean convertTwitterProductCardStubToBean(TwitterProductCard stub)
    {
        TwitterProductCardBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null bean is returned.");
        } else {
            bean = new TwitterProductCardBean();
            bean.setGuid(stub.getGuid());
            bean.setCard(stub.getCard());
            bean.setUrl(stub.getUrl());
            bean.setTitle(stub.getTitle());
            bean.setDescription(stub.getDescription());
            bean.setSite(stub.getSite());
            bean.setSiteId(stub.getSiteId());
            bean.setCreator(stub.getCreator());
            bean.setCreatorId(stub.getCreatorId());
            bean.setImage(stub.getImage());
            bean.setImageWidth(stub.getImageWidth());
            bean.setImageHeight(stub.getImageHeight());
            bean.setData1(TwitterCardProductDataResourceUtil.convertTwitterCardProductDataStubToBean(stub.getData1()));
            bean.setData2(TwitterCardProductDataResourceUtil.convertTwitterCardProductDataStubToBean(stub.getData2()));
            bean.setCreatedTime(stub.getCreatedTime());
            bean.setModifiedTime(stub.getModifiedTime());
        }
        return bean;
    }

    public static List<TwitterProductCardBean> convertTwitterProductCardListStubToBeanList(TwitterProductCardListStub listStub)
    {
        if(listStub == null) {
            log.log(Level.INFO, "listStub is null. Null list is returned.");
            return null;
        } else {
            List<TwitterProductCardStub> stubList = listStub.getList();
            if(stubList == null) {
                log.log(Level.INFO, "Stub list is null. Null list is returned.");
                return null;                
            }
            List<TwitterProductCardBean> beanList = new ArrayList<TwitterProductCardBean>();
            if(stubList.isEmpty()) {
                log.log(Level.INFO, "Stub list is empty. Empty list is returned.");
            } else {
                for(TwitterProductCardStub stub : stubList) {
                    TwitterProductCardBean bean = convertTwitterProductCardStubToBean(stub);
                    beanList.add(bean);                            
                }
            }
            return beanList;
        }
    }

}
