package com.myurldb.ws.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.CommonConstants;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.FullNameStruct;
import com.myurldb.ws.bean.FullNameStructBean;
import com.myurldb.ws.stub.FullNameStructStub;


public class FullNameStructResourceUtil
{
    private static final Logger log = Logger.getLogger(FullNameStructResourceUtil.class.getName());

    // Static methods only.
    private FullNameStructResourceUtil() {}

    public static FullNameStructBean convertFullNameStructStubToBean(FullNameStruct stub)
    {
        FullNameStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null FullNameStructBean is returned.");
        } else {
            bean = new FullNameStructBean();
            bean.setUuid(stub.getUuid());
            bean.setDisplayName(stub.getDisplayName());
            bean.setLastName(stub.getLastName());
            bean.setFirstName(stub.getFirstName());
            bean.setMiddleName1(stub.getMiddleName1());
            bean.setMiddleName2(stub.getMiddleName2());
            bean.setMiddleInitial(stub.getMiddleInitial());
            bean.setSalutation(stub.getSalutation());
            bean.setSuffix(stub.getSuffix());
            bean.setNote(stub.getNote());
        }
        return bean;
    }

}
