package com.myurldb.ws.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.CommonConstants;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.HashedPasswordStruct;
import com.myurldb.ws.bean.HashedPasswordStructBean;
import com.myurldb.ws.stub.HashedPasswordStructStub;


public class HashedPasswordStructResourceUtil
{
    private static final Logger log = Logger.getLogger(HashedPasswordStructResourceUtil.class.getName());

    // Static methods only.
    private HashedPasswordStructResourceUtil() {}

    public static HashedPasswordStructBean convertHashedPasswordStructStubToBean(HashedPasswordStruct stub)
    {
        HashedPasswordStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null HashedPasswordStructBean is returned.");
        } else {
            bean = new HashedPasswordStructBean();
            bean.setUuid(stub.getUuid());
            bean.setPlainText(stub.getPlainText());
            bean.setHashedText(stub.getHashedText());
            bean.setSalt(stub.getSalt());
            bean.setAlgorithm(stub.getAlgorithm());
        }
        return bean;
    }

}
