package com.myurldb.ws.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.CommonConstants;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.HelpNotice;
import com.myurldb.ws.bean.HelpNoticeBean;
import com.myurldb.ws.stub.HelpNoticeStub;


public class HelpNoticeResourceUtil
{
    private static final Logger log = Logger.getLogger(HelpNoticeResourceUtil.class.getName());

    // Static methods only.
    private HelpNoticeResourceUtil() {}

    public static HelpNoticeBean convertHelpNoticeStubToBean(HelpNotice stub)
    {
        HelpNoticeBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null HelpNoticeBean is returned.");
        } else {
            bean = new HelpNoticeBean();
            bean.setUuid(stub.getUuid());
            bean.setTitle(stub.getTitle());
            bean.setContent(stub.getContent());
            bean.setFormat(stub.getFormat());
            bean.setNote(stub.getNote());
            bean.setStatus(stub.getStatus());
        }
        return bean;
    }

}
