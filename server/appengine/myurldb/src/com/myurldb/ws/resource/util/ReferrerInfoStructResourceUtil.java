package com.myurldb.ws.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.CommonConstants;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.ReferrerInfoStruct;
import com.myurldb.ws.bean.ReferrerInfoStructBean;
import com.myurldb.ws.stub.ReferrerInfoStructStub;


public class ReferrerInfoStructResourceUtil
{
    private static final Logger log = Logger.getLogger(ReferrerInfoStructResourceUtil.class.getName());

    // Static methods only.
    private ReferrerInfoStructResourceUtil() {}

    public static ReferrerInfoStructBean convertReferrerInfoStructStubToBean(ReferrerInfoStruct stub)
    {
        ReferrerInfoStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null ReferrerInfoStructBean is returned.");
        } else {
            bean = new ReferrerInfoStructBean();
            bean.setReferer(stub.getReferer());
            bean.setUserAgent(stub.getUserAgent());
            bean.setLanguage(stub.getLanguage());
            bean.setHostname(stub.getHostname());
            bean.setIpAddress(stub.getIpAddress());
            bean.setNote(stub.getNote());
        }
        return bean;
    }

}
