package com.myurldb.ws.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.CommonConstants;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.ShortPassageAttribute;
import com.myurldb.ws.bean.ShortPassageAttributeBean;
import com.myurldb.ws.stub.ShortPassageAttributeStub;


public class ShortPassageAttributeResourceUtil
{
    private static final Logger log = Logger.getLogger(ShortPassageAttributeResourceUtil.class.getName());

    // Static methods only.
    private ShortPassageAttributeResourceUtil() {}

    public static ShortPassageAttributeBean convertShortPassageAttributeStubToBean(ShortPassageAttribute stub)
    {
        ShortPassageAttributeBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null ShortPassageAttributeBean is returned.");
        } else {
            bean = new ShortPassageAttributeBean();
            bean.setDomain(stub.getDomain());
            bean.setTokenType(stub.getTokenType());
            bean.setDisplayMessage(stub.getDisplayMessage());
            bean.setRedirectType(stub.getRedirectType());
            bean.setFlashDuration(stub.getFlashDuration());
            bean.setAccessType(stub.getAccessType());
            bean.setViewType(stub.getViewType());
            bean.setShareType(stub.getShareType());
        }
        return bean;
    }

}
