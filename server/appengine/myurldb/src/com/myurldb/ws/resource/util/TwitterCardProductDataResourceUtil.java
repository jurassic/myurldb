package com.myurldb.ws.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.CommonConstants;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.bean.TwitterCardProductDataBean;
import com.myurldb.ws.stub.TwitterCardProductDataStub;


public class TwitterCardProductDataResourceUtil
{
    private static final Logger log = Logger.getLogger(TwitterCardProductDataResourceUtil.class.getName());

    // Static methods only.
    private TwitterCardProductDataResourceUtil() {}

    public static TwitterCardProductDataBean convertTwitterCardProductDataStubToBean(TwitterCardProductData stub)
    {
        TwitterCardProductDataBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null TwitterCardProductDataBean is returned.");
        } else {
            bean = new TwitterCardProductDataBean();
            bean.setData(stub.getData());
            bean.setLabel(stub.getLabel());
        }
        return bean;
    }

}
