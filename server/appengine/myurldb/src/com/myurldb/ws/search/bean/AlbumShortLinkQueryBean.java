package com.myurldb.ws.search.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.AlbumShortLink;


public class AlbumShortLinkQueryBean
{
    private static final Logger log = Logger.getLogger(AlbumShortLinkQueryBean.class.getName());

    private int rank;
    private String albumShortLinkGuid;
    private String note;

    public AlbumShortLinkQueryBean()
    {
        this(0, null, null);  
    }
    public AlbumShortLinkQueryBean(int rank, String albumShortLinkGuid, String note)
    {
        this.rank = rank;
        this.albumShortLinkGuid = albumShortLinkGuid;
        this.note = note;
    }
    
    public int getRank()
    {
        return this.rank;
    }
    public void setRank(int rank)
    {
        this.rank = rank;
    }

    public String getAlbumShortLinkGuid()
    {
        return this.albumShortLinkGuid;
    }
    public void setAlbumShortLinkGuid(String albumShortLinkGuid)
    {
        this.albumShortLinkGuid = albumShortLinkGuid;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }


    @Override
    public String toString()
    {
            StringBuilder sb = new StringBuilder();

            sb.append("rank = " + this.rank).append(";");
            sb.append("albumShortLinkGuid = " + this.albumShortLinkGuid).append(";");
            sb.append("note = " + this.note).append(";");

            return sb.toString();
    }


}

