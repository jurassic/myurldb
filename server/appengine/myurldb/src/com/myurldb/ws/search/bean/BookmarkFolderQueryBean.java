package com.myurldb.ws.search.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.BookmarkFolder;


public class BookmarkFolderQueryBean
{
    private static final Logger log = Logger.getLogger(BookmarkFolderQueryBean.class.getName());

    private int rank;
    private String bookmarkFolderGuid;
    private String title;
    private String description;
    private String category;

    public BookmarkFolderQueryBean()
    {
        this(0, null, null, null, null);  
    }
    public BookmarkFolderQueryBean(int rank, String bookmarkFolderGuid, String title, String description, String category)
    {
        this.rank = rank;
        this.bookmarkFolderGuid = bookmarkFolderGuid;
        this.title = title;
        this.description = description;
        this.category = category;
    }
    
    public int getRank()
    {
        return this.rank;
    }
    public void setRank(int rank)
    {
        this.rank = rank;
    }

    public String getBookmarkFolderGuid()
    {
        return this.bookmarkFolderGuid;
    }
    public void setBookmarkFolderGuid(String bookmarkFolderGuid)
    {
        this.bookmarkFolderGuid = bookmarkFolderGuid;
    }

    public String getTitle()
    {
        return this.title;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getDescription()
    {
        return this.description;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getCategory()
    {
        return this.category;
    }
    public void setCategory(String category)
    {
        this.category = category;
    }


    @Override
    public String toString()
    {
            StringBuilder sb = new StringBuilder();

            sb.append("rank = " + this.rank).append(";");
            sb.append("bookmarkFolderGuid = " + this.bookmarkFolderGuid).append(";");
            sb.append("title = " + this.title).append(";");
            sb.append("description = " + this.description).append(";");
            sb.append("category = " + this.category).append(";");

            return sb.toString();
    }


}

