package com.myurldb.ws.search.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.BookmarkLink;


public class BookmarkLinkQueryBean
{
    private static final Logger log = Logger.getLogger(BookmarkLinkQueryBean.class.getName());

    private int rank;
    private String bookmarkLinkGuid;
    private String memo;

    public BookmarkLinkQueryBean()
    {
        this(0, null, null);  
    }
    public BookmarkLinkQueryBean(int rank, String bookmarkLinkGuid, String memo)
    {
        this.rank = rank;
        this.bookmarkLinkGuid = bookmarkLinkGuid;
        this.memo = memo;
    }
    
    public int getRank()
    {
        return this.rank;
    }
    public void setRank(int rank)
    {
        this.rank = rank;
    }

    public String getBookmarkLinkGuid()
    {
        return this.bookmarkLinkGuid;
    }
    public void setBookmarkLinkGuid(String bookmarkLinkGuid)
    {
        this.bookmarkLinkGuid = bookmarkLinkGuid;
    }

    public String getMemo()
    {
        return this.memo;
    }
    public void setMemo(String memo)
    {
        this.memo = memo;
    }


    @Override
    public String toString()
    {
            StringBuilder sb = new StringBuilder();

            sb.append("rank = " + this.rank).append(";");
            sb.append("bookmarkLinkGuid = " + this.bookmarkLinkGuid).append(";");
            sb.append("memo = " + this.memo).append(";");

            return sb.toString();
    }


}

