package com.myurldb.ws.search.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.DomainInfo;


public class DomainInfoQueryBean
{
    private static final Logger log = Logger.getLogger(DomainInfoQueryBean.class.getName());

    private int rank;
    private String domainInfoGuid;
    private String category;

    public DomainInfoQueryBean()
    {
        this(0, null, null);  
    }
    public DomainInfoQueryBean(int rank, String domainInfoGuid, String category)
    {
        this.rank = rank;
        this.domainInfoGuid = domainInfoGuid;
        this.category = category;
    }
    
    public int getRank()
    {
        return this.rank;
    }
    public void setRank(int rank)
    {
        this.rank = rank;
    }

    public String getDomainInfoGuid()
    {
        return this.domainInfoGuid;
    }
    public void setDomainInfoGuid(String domainInfoGuid)
    {
        this.domainInfoGuid = domainInfoGuid;
    }

    public String getCategory()
    {
        return this.category;
    }
    public void setCategory(String category)
    {
        this.category = category;
    }


    @Override
    public String toString()
    {
            StringBuilder sb = new StringBuilder();

            sb.append("rank = " + this.rank).append(";");
            sb.append("domainInfoGuid = " + this.domainInfoGuid).append(";");
            sb.append("category = " + this.category).append(";");

            return sb.toString();
    }


}

