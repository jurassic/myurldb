package com.myurldb.ws.search.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.CellLatitudeLongitude;
import com.myurldb.ws.GeoCoordinateStruct;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.GeoLink;


public class GeoLinkQueryBean
{
    private static final Logger log = Logger.getLogger(GeoLinkQueryBean.class.getName());

    private int rank;
    private String geoLinkGuid;
    private GeoCoordinateStruct geoCoordinate;

    public GeoLinkQueryBean()
    {
        this(0, null, null);  
    }
    public GeoLinkQueryBean(int rank, String geoLinkGuid, GeoCoordinateStruct geoCoordinate)
    {
        this.rank = rank;
        this.geoLinkGuid = geoLinkGuid;
        this.geoCoordinate = geoCoordinate;
    }
    
    public int getRank()
    {
        return this.rank;
    }
    public void setRank(int rank)
    {
        this.rank = rank;
    }

    public String getGeoLinkGuid()
    {
        return this.geoLinkGuid;
    }
    public void setGeoLinkGuid(String geoLinkGuid)
    {
        this.geoLinkGuid = geoLinkGuid;
    }

    public GeoCoordinateStruct getGeoCoordinate()
    {
        return this.geoCoordinate;
    }
    public void setGeoCoordinate(GeoCoordinateStruct geoCoordinate)
    {
        this.geoCoordinate = geoCoordinate;
    }


    @Override
    public String toString()
    {
            StringBuilder sb = new StringBuilder();

            sb.append("rank = " + this.rank).append(";");
            sb.append("geoLinkGuid = " + this.geoLinkGuid).append(";");
            sb.append("geoCoordinate = " + this.geoCoordinate).append(";");

            return sb.toString();
    }


}

