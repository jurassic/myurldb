package com.myurldb.ws.search.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.KeywordCrowdTally;


public class KeywordCrowdTallyQueryBean
{
    private static final Logger log = Logger.getLogger(KeywordCrowdTallyQueryBean.class.getName());

    private int rank;
    private String keywordCrowdTallyGuid;
    private String keyword;

    public KeywordCrowdTallyQueryBean()
    {
        this(0, null, null);  
    }
    public KeywordCrowdTallyQueryBean(int rank, String keywordCrowdTallyGuid, String keyword)
    {
        this.rank = rank;
        this.keywordCrowdTallyGuid = keywordCrowdTallyGuid;
        this.keyword = keyword;
    }
    
    public int getRank()
    {
        return this.rank;
    }
    public void setRank(int rank)
    {
        this.rank = rank;
    }

    public String getKeywordCrowdTallyGuid()
    {
        return this.keywordCrowdTallyGuid;
    }
    public void setKeywordCrowdTallyGuid(String keywordCrowdTallyGuid)
    {
        this.keywordCrowdTallyGuid = keywordCrowdTallyGuid;
    }

    public String getKeyword()
    {
        return this.keyword;
    }
    public void setKeyword(String keyword)
    {
        this.keyword = keyword;
    }


    @Override
    public String toString()
    {
            StringBuilder sb = new StringBuilder();

            sb.append("rank = " + this.rank).append(";");
            sb.append("keywordCrowdTallyGuid = " + this.keywordCrowdTallyGuid).append(";");
            sb.append("keyword = " + this.keyword).append(";");

            return sb.toString();
    }


}

