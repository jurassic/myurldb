package com.myurldb.ws.search.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.KeywordFolder;


public class KeywordFolderQueryBean
{
    private static final Logger log = Logger.getLogger(KeywordFolderQueryBean.class.getName());

    private int rank;
    private String keywordFolderGuid;
    private String title;
    private String description;
    private String category;

    public KeywordFolderQueryBean()
    {
        this(0, null, null, null, null);  
    }
    public KeywordFolderQueryBean(int rank, String keywordFolderGuid, String title, String description, String category)
    {
        this.rank = rank;
        this.keywordFolderGuid = keywordFolderGuid;
        this.title = title;
        this.description = description;
        this.category = category;
    }
    
    public int getRank()
    {
        return this.rank;
    }
    public void setRank(int rank)
    {
        this.rank = rank;
    }

    public String getKeywordFolderGuid()
    {
        return this.keywordFolderGuid;
    }
    public void setKeywordFolderGuid(String keywordFolderGuid)
    {
        this.keywordFolderGuid = keywordFolderGuid;
    }

    public String getTitle()
    {
        return this.title;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getDescription()
    {
        return this.description;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getCategory()
    {
        return this.category;
    }
    public void setCategory(String category)
    {
        this.category = category;
    }


    @Override
    public String toString()
    {
            StringBuilder sb = new StringBuilder();

            sb.append("rank = " + this.rank).append(";");
            sb.append("keywordFolderGuid = " + this.keywordFolderGuid).append(";");
            sb.append("title = " + this.title).append(";");
            sb.append("description = " + this.description).append(";");
            sb.append("category = " + this.category).append(";");

            return sb.toString();
    }


}

