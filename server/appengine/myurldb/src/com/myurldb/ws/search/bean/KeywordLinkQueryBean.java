package com.myurldb.ws.search.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.KeywordLink;


public class KeywordLinkQueryBean
{
    private static final Logger log = Logger.getLogger(KeywordLinkQueryBean.class.getName());

    private int rank;
    private String keywordLinkGuid;
    private String memo;
    private String keyword;

    public KeywordLinkQueryBean()
    {
        this(0, null, null, null);  
    }
    public KeywordLinkQueryBean(int rank, String keywordLinkGuid, String memo, String keyword)
    {
        this.rank = rank;
        this.keywordLinkGuid = keywordLinkGuid;
        this.memo = memo;
        this.keyword = keyword;
    }
    
    public int getRank()
    {
        return this.rank;
    }
    public void setRank(int rank)
    {
        this.rank = rank;
    }

    public String getKeywordLinkGuid()
    {
        return this.keywordLinkGuid;
    }
    public void setKeywordLinkGuid(String keywordLinkGuid)
    {
        this.keywordLinkGuid = keywordLinkGuid;
    }

    public String getMemo()
    {
        return this.memo;
    }
    public void setMemo(String memo)
    {
        this.memo = memo;
    }

    public String getKeyword()
    {
        return this.keyword;
    }
    public void setKeyword(String keyword)
    {
        this.keyword = keyword;
    }


    @Override
    public String toString()
    {
            StringBuilder sb = new StringBuilder();

            sb.append("rank = " + this.rank).append(";");
            sb.append("keywordLinkGuid = " + this.keywordLinkGuid).append(";");
            sb.append("memo = " + this.memo).append(";");
            sb.append("keyword = " + this.keyword).append(";");

            return sb.toString();
    }


}

