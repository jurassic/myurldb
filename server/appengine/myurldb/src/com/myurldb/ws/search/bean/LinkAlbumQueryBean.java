package com.myurldb.ws.search.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.LinkAlbum;


public class LinkAlbumQueryBean
{
    private static final Logger log = Logger.getLogger(LinkAlbumQueryBean.class.getName());

    private int rank;
    private String linkAlbumGuid;
    private String name;
    private String summary;
    private String note;

    public LinkAlbumQueryBean()
    {
        this(0, null, null, null, null);  
    }
    public LinkAlbumQueryBean(int rank, String linkAlbumGuid, String name, String summary, String note)
    {
        this.rank = rank;
        this.linkAlbumGuid = linkAlbumGuid;
        this.name = name;
        this.summary = summary;
        this.note = note;
    }
    
    public int getRank()
    {
        return this.rank;
    }
    public void setRank(int rank)
    {
        this.rank = rank;
    }

    public String getLinkAlbumGuid()
    {
        return this.linkAlbumGuid;
    }
    public void setLinkAlbumGuid(String linkAlbumGuid)
    {
        this.linkAlbumGuid = linkAlbumGuid;
    }

    public String getName()
    {
        return this.name;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getSummary()
    {
        return this.summary;
    }
    public void setSummary(String summary)
    {
        this.summary = summary;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }


    @Override
    public String toString()
    {
            StringBuilder sb = new StringBuilder();

            sb.append("rank = " + this.rank).append(";");
            sb.append("linkAlbumGuid = " + this.linkAlbumGuid).append(";");
            sb.append("name = " + this.name).append(";");
            sb.append("summary = " + this.summary).append(";");
            sb.append("note = " + this.note).append(";");

            return sb.toString();
    }


}

