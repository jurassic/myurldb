package com.myurldb.ws.search.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.LinkMessage;


public class LinkMessageQueryBean
{
    private static final Logger log = Logger.getLogger(LinkMessageQueryBean.class.getName());

    private int rank;
    private String linkMessageGuid;
    private String message;

    public LinkMessageQueryBean()
    {
        this(0, null, null);  
    }
    public LinkMessageQueryBean(int rank, String linkMessageGuid, String message)
    {
        this.rank = rank;
        this.linkMessageGuid = linkMessageGuid;
        this.message = message;
    }
    
    public int getRank()
    {
        return this.rank;
    }
    public void setRank(int rank)
    {
        this.rank = rank;
    }

    public String getLinkMessageGuid()
    {
        return this.linkMessageGuid;
    }
    public void setLinkMessageGuid(String linkMessageGuid)
    {
        this.linkMessageGuid = linkMessageGuid;
    }

    public String getMessage()
    {
        return this.message;
    }
    public void setMessage(String message)
    {
        this.message = message;
    }


    @Override
    public String toString()
    {
            StringBuilder sb = new StringBuilder();

            sb.append("rank = " + this.rank).append(";");
            sb.append("linkMessageGuid = " + this.linkMessageGuid).append(";");
            sb.append("message = " + this.message).append(";");

            return sb.toString();
    }


}

