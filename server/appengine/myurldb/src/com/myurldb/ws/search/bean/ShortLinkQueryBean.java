package com.myurldb.ws.search.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.ReferrerInfoStruct;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.ShortLink;


public class ShortLinkQueryBean
{
    private static final Logger log = Logger.getLogger(ShortLinkQueryBean.class.getName());

    private int rank;
    private String shortLinkGuid;
    private String displayMessage;
    private String shortMessage;

    public ShortLinkQueryBean()
    {
        this(0, null, null, null);  
    }
    public ShortLinkQueryBean(int rank, String shortLinkGuid, String displayMessage, String shortMessage)
    {
        this.rank = rank;
        this.shortLinkGuid = shortLinkGuid;
        this.displayMessage = displayMessage;
        this.shortMessage = shortMessage;
    }
    
    public int getRank()
    {
        return this.rank;
    }
    public void setRank(int rank)
    {
        this.rank = rank;
    }

    public String getShortLinkGuid()
    {
        return this.shortLinkGuid;
    }
    public void setShortLinkGuid(String shortLinkGuid)
    {
        this.shortLinkGuid = shortLinkGuid;
    }

    public String getDisplayMessage()
    {
        return this.displayMessage;
    }
    public void setDisplayMessage(String displayMessage)
    {
        this.displayMessage = displayMessage;
    }

    public String getShortMessage()
    {
        return this.shortMessage;
    }
    public void setShortMessage(String shortMessage)
    {
        this.shortMessage = shortMessage;
    }


    @Override
    public String toString()
    {
            StringBuilder sb = new StringBuilder();

            sb.append("rank = " + this.rank).append(";");
            sb.append("shortLinkGuid = " + this.shortLinkGuid).append(";");
            sb.append("displayMessage = " + this.displayMessage).append(";");
            sb.append("shortMessage = " + this.shortMessage).append(";");

            return sb.toString();
    }


}

