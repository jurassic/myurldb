package com.myurldb.ws.search.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.ShortPassageAttribute;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.ShortPassage;


public class ShortPassageQueryBean
{
    private static final Logger log = Logger.getLogger(ShortPassageQueryBean.class.getName());

    private int rank;
    private String shortPassageGuid;
    private String shortText;

    public ShortPassageQueryBean()
    {
        this(0, null, null);  
    }
    public ShortPassageQueryBean(int rank, String shortPassageGuid, String shortText)
    {
        this.rank = rank;
        this.shortPassageGuid = shortPassageGuid;
        this.shortText = shortText;
    }
    
    public int getRank()
    {
        return this.rank;
    }
    public void setRank(int rank)
    {
        this.rank = rank;
    }

    public String getShortPassageGuid()
    {
        return this.shortPassageGuid;
    }
    public void setShortPassageGuid(String shortPassageGuid)
    {
        this.shortPassageGuid = shortPassageGuid;
    }

    public String getShortText()
    {
        return this.shortText;
    }
    public void setShortText(String shortText)
    {
        this.shortText = shortText;
    }


    @Override
    public String toString()
    {
            StringBuilder sb = new StringBuilder();

            sb.append("rank = " + this.rank).append(";");
            sb.append("shortPassageGuid = " + this.shortPassageGuid).append(";");
            sb.append("shortText = " + this.shortText).append(";");

            return sb.toString();
    }


}

