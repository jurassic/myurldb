package com.myurldb.ws.search.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.GeoPointStruct;
import com.myurldb.ws.StreetAddressStruct;
import com.myurldb.ws.GaeAppStruct;
import com.myurldb.ws.FullNameStruct;
import com.myurldb.ws.GaeUserStruct;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.User;


public class UserQueryBean
{
    private static final Logger log = Logger.getLogger(UserQueryBean.class.getName());

    private int rank;
    private String userGuid;
    private String nickname;
    private String location;
    private GeoPointStruct geoPoint;

    public UserQueryBean()
    {
        this(0, null, null, null, null);  
    }
    public UserQueryBean(int rank, String userGuid, String nickname, String location, GeoPointStruct geoPoint)
    {
        this.rank = rank;
        this.userGuid = userGuid;
        this.nickname = nickname;
        this.location = location;
        this.geoPoint = geoPoint;
    }
    
    public int getRank()
    {
        return this.rank;
    }
    public void setRank(int rank)
    {
        this.rank = rank;
    }

    public String getUserGuid()
    {
        return this.userGuid;
    }
    public void setUserGuid(String userGuid)
    {
        this.userGuid = userGuid;
    }

    public String getNickname()
    {
        return this.nickname;
    }
    public void setNickname(String nickname)
    {
        this.nickname = nickname;
    }

    public String getLocation()
    {
        return this.location;
    }
    public void setLocation(String location)
    {
        this.location = location;
    }

    public GeoPointStruct getGeoPoint()
    {
        return this.geoPoint;
    }
    public void setGeoPoint(GeoPointStruct geoPoint)
    {
        this.geoPoint = geoPoint;
    }


    @Override
    public String toString()
    {
            StringBuilder sb = new StringBuilder();

            sb.append("rank = " + this.rank).append(";");
            sb.append("userGuid = " + this.userGuid).append(";");
            sb.append("nickname = " + this.nickname).append(";");
            sb.append("location = " + this.location).append(";");
            sb.append("geoPoint = " + this.geoPoint).append(";");

            return sb.toString();
    }


}

