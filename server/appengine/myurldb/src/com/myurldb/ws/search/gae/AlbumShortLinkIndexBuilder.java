package com.myurldb.ws.search.gae;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.search.Field;
import com.google.appengine.api.search.GeoPoint;
import com.google.appengine.api.search.Document;
import com.google.appengine.api.search.Document.Builder;

import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.AlbumShortLink;


public class AlbumShortLinkIndexBuilder extends BaseIndexBuilder
{
    private static final Logger log = Logger.getLogger(AlbumShortLinkIndexBuilder.class.getName());
    
    public AlbumShortLinkIndexBuilder()
    {
        super("AlbumShortLinkIndex");	
    }
    
    public boolean addDocument(AlbumShortLink albumShortLink)
    {
        return addDocument(albumShortLink.getGuid(), albumShortLink.getNote());
    }

    public boolean addDocument(String guid, String note)
    {
    	if(log.isLoggable(Level.FINER)) log.finer("BEGIN: addDocument() called with guid = " + guid);

    	Builder builder = Document.newBuilder().setId(guid);
        if(note != null) {
            if(!note.isEmpty()) {
                builder.addField(Field.newBuilder().setName("note").setText(note));
            }
        }
    	Document doc = builder.build();
    	boolean suc = addDocument(doc);

    	if(log.isLoggable(Level.FINER)) log.finer("END: addDocument(). suc = " + suc);
    	return suc;
    }


}

