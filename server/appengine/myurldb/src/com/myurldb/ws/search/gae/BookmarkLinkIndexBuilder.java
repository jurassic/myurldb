package com.myurldb.ws.search.gae;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.search.Field;
import com.google.appengine.api.search.GeoPoint;
import com.google.appengine.api.search.Document;
import com.google.appengine.api.search.Document.Builder;

import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.BookmarkLink;


public class BookmarkLinkIndexBuilder extends BaseIndexBuilder
{
    private static final Logger log = Logger.getLogger(BookmarkLinkIndexBuilder.class.getName());
    
    public BookmarkLinkIndexBuilder()
    {
        super("BookmarkLinkIndex");	
    }
    
    public boolean addDocument(BookmarkLink bookmarkLink)
    {
        return addDocument(bookmarkLink.getGuid(), bookmarkLink.getMemo());
    }

    public boolean addDocument(String guid, String memo)
    {
    	if(log.isLoggable(Level.FINER)) log.finer("BEGIN: addDocument() called with guid = " + guid);

    	Builder builder = Document.newBuilder().setId(guid);
        if(memo != null) {
            if(!memo.isEmpty()) {
                builder.addField(Field.newBuilder().setName("memo").setText(memo));
            }
        }
    	Document doc = builder.build();
    	boolean suc = addDocument(doc);

    	if(log.isLoggable(Level.FINER)) log.finer("END: addDocument(). suc = " + suc);
    	return suc;
    }


}

