package com.myurldb.ws.search.gae;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.search.Field;
import com.google.appengine.api.search.GeoPoint;
import com.google.appengine.api.search.Document;
import com.google.appengine.api.search.Document.Builder;

import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.DomainInfo;


public class DomainInfoIndexBuilder extends BaseIndexBuilder
{
    private static final Logger log = Logger.getLogger(DomainInfoIndexBuilder.class.getName());
    
    public DomainInfoIndexBuilder()
    {
        super("DomainInfoIndex");	
    }
    
    public boolean addDocument(DomainInfo domainInfo)
    {
        return addDocument(domainInfo.getGuid(), domainInfo.getCategory());
    }

    public boolean addDocument(String guid, String category)
    {
    	if(log.isLoggable(Level.FINER)) log.finer("BEGIN: addDocument() called with guid = " + guid);

    	Builder builder = Document.newBuilder().setId(guid);
        if(category != null) {
            if(!category.isEmpty()) {
                builder.addField(Field.newBuilder().setName("category").setText(category));
            }
        }
    	Document doc = builder.build();
    	boolean suc = addDocument(doc);

    	if(log.isLoggable(Level.FINER)) log.finer("END: addDocument(). suc = " + suc);
    	return suc;
    }


}

