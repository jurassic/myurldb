package com.myurldb.ws.search.gae;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.search.Field;
import com.google.appengine.api.search.GeoPoint;
import com.google.appengine.api.search.Document;
import com.google.appengine.api.search.Document.Builder;

import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.CellLatitudeLongitude;
import com.myurldb.ws.GeoCoordinateStruct;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.GeoLink;


public class GeoLinkIndexBuilder extends BaseIndexBuilder
{
    private static final Logger log = Logger.getLogger(GeoLinkIndexBuilder.class.getName());
    
    public GeoLinkIndexBuilder()
    {
        super("GeoLinkIndex");	
    }
    
    public boolean addDocument(GeoLink geoLink)
    {
        return addDocument(geoLink.getGuid(), geoLink.getGeoCoordinate());
    }

    public boolean addDocument(String guid, GeoCoordinateStruct geoCoordinate)
    {
    	if(log.isLoggable(Level.FINER)) log.finer("BEGIN: addDocument() called with guid = " + guid);

    	Builder builder = Document.newBuilder().setId(guid);
        if(geoCoordinate != null) {
        	Double lat = geoCoordinate.getLatitude();
        	Double lng = geoCoordinate.getLongitude();
        	if(lat != null && lng != null) {
                builder.addField(Field.newBuilder().setName("geoCoordinate").setGeoPoint(new GeoPoint(lat, lng)));
        	}
        }
    	Document doc = builder.build();
    	boolean suc = addDocument(doc);

    	if(log.isLoggable(Level.FINER)) log.finer("END: addDocument(). suc = " + suc);
    	return suc;
    }


}

