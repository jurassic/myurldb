package com.myurldb.ws.search.gae;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.search.Field;
import com.google.appengine.api.search.GeoPoint;
import com.google.appengine.api.search.Document;
import com.google.appengine.api.search.Document.Builder;

import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.KeywordCrowdTally;


public class KeywordCrowdTallyIndexBuilder extends BaseIndexBuilder
{
    private static final Logger log = Logger.getLogger(KeywordCrowdTallyIndexBuilder.class.getName());
    
    public KeywordCrowdTallyIndexBuilder()
    {
        super("KeywordCrowdTallyIndex");	
    }
    
    public boolean addDocument(KeywordCrowdTally keywordCrowdTally)
    {
        return addDocument(keywordCrowdTally.getGuid(), keywordCrowdTally.getKeyword());
    }

    public boolean addDocument(String guid, String keyword)
    {
    	if(log.isLoggable(Level.FINER)) log.finer("BEGIN: addDocument() called with guid = " + guid);

    	Builder builder = Document.newBuilder().setId(guid);
        if(keyword != null) {
            if(!keyword.isEmpty()) {
                builder.addField(Field.newBuilder().setName("keyword").setText(keyword));
            }
        }
    	Document doc = builder.build();
    	boolean suc = addDocument(doc);

    	if(log.isLoggable(Level.FINER)) log.finer("END: addDocument(). suc = " + suc);
    	return suc;
    }


}

