package com.myurldb.ws.search.gae;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.search.Field;
import com.google.appengine.api.search.GeoPoint;
import com.google.appengine.api.search.Document;
import com.google.appengine.api.search.Document.Builder;

import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.KeywordFolder;


public class KeywordFolderIndexBuilder extends BaseIndexBuilder
{
    private static final Logger log = Logger.getLogger(KeywordFolderIndexBuilder.class.getName());
    
    public KeywordFolderIndexBuilder()
    {
        super("KeywordFolderIndex");	
    }
    
    public boolean addDocument(KeywordFolder keywordFolder)
    {
        return addDocument(keywordFolder.getGuid(), keywordFolder.getTitle(), keywordFolder.getDescription(), keywordFolder.getCategory());
    }

    public boolean addDocument(String guid, String title, String description, String category)
    {
    	if(log.isLoggable(Level.FINER)) log.finer("BEGIN: addDocument() called with guid = " + guid);

    	Builder builder = Document.newBuilder().setId(guid);
        if(title != null) {
            if(!title.isEmpty()) {
                builder.addField(Field.newBuilder().setName("title").setText(title));
            }
        }
        if(description != null) {
            if(!description.isEmpty()) {
                builder.addField(Field.newBuilder().setName("description").setText(description));
            }
        }
        if(category != null) {
            if(!category.isEmpty()) {
                builder.addField(Field.newBuilder().setName("category").setText(category));
            }
        }
    	Document doc = builder.build();
    	boolean suc = addDocument(doc);

    	if(log.isLoggable(Level.FINER)) log.finer("END: addDocument(). suc = " + suc);
    	return suc;
    }


}

