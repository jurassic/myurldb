package com.myurldb.ws.search.gae;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.search.Field;
import com.google.appengine.api.search.GeoPoint;
import com.google.appengine.api.search.Cursor;
import com.google.appengine.api.search.Query;
import com.google.appengine.api.search.QueryOptions;
import com.google.appengine.api.search.Results;
import com.google.appengine.api.search.SearchException;
import com.google.appengine.api.search.ScoredDocument;
import com.google.appengine.api.search.SortExpression;
import com.google.appengine.api.search.SortOptions;
import com.google.appengine.api.search.SearchException;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.KeywordFolder;
import com.myurldb.ws.dao.KeywordFolderDAO;
import com.myurldb.ws.service.DAOFactoryManager;
import com.myurldb.ws.search.bean.KeywordFolderQueryBean;


public class KeywordFolderQueryHelper extends BaseQueryHelper
{
    private static final Logger log = Logger.getLogger(KeywordFolderQueryHelper.class.getName());
    
    
    private KeywordFolderIndexBuilder keywordFolderIndexBuilder = null;

    public KeywordFolderQueryHelper()
    {
    	this(null);
    }
    public KeywordFolderQueryHelper(KeywordFolderIndexBuilder keywordFolderIndexBuilder)
    {
        super();
        if(keywordFolderIndexBuilder != null) {
        	this.keywordFolderIndexBuilder = keywordFolderIndexBuilder;
        } else {
        	this.keywordFolderIndexBuilder = new KeywordFolderIndexBuilder();
        }
    }

    // TBD:    
    public List<KeywordFolderQueryBean> findKeywordFolderQueryBeans(String queryString, String ordering,  Long offset, Integer count)
    {
        // TBD
    	Results<ScoredDocument> docs = findDocuments(queryString, ordering, offset, count);
    	if(docs == null) {
    		return null;
    	}
    	List<KeywordFolderQueryBean> keywordFolderQueryBeans = new ArrayList<KeywordFolderQueryBean>();
    	for(ScoredDocument doc : docs) {
    		KeywordFolderQueryBean bean = new KeywordFolderQueryBean();

    	    int rank = doc.getRank();
    		bean.setRank(rank);
    		String guid = doc.getId();
    		bean.setKeywordFolderGuid(guid);
            Field fTitle = doc.getOnlyField("title");
            String vTitle = fTitle.getText();
            bean.setTitle(vTitle);
            Field fDescription = doc.getOnlyField("description");
            String vDescription = fDescription.getText();
            bean.setDescription(vDescription);
            Field fCategory = doc.getOnlyField("category");
            String vCategory = fCategory.getText();
            bean.setCategory(vCategory);

   	        keywordFolderQueryBeans.add(bean);
    	}
        return keywordFolderQueryBeans;
    }

    // Note: This is very inefficient....
    public List<KeywordFolder> findKeywordFolders(String queryString, String ordering,  Long offset, Integer count)
    {
        // TBD
    	Results<ScoredDocument> docs = findDocuments(queryString, ordering, offset, count);
    	if(docs == null) {
    		return null;
    	}
    	KeywordFolderDAO keywordFolderDAO = DAOFactoryManager.getDAOFactory().getKeywordFolderDAO();
    	List<KeywordFolder> keywordFolders = new ArrayList<KeywordFolder>();
    	for(ScoredDocument doc : docs) {
    		String guid = doc.getId();
    		KeywordFolder keywordFolder = null;
    		try {
    			keywordFolder = keywordFolderDAO.getKeywordFolder(guid);
			} catch (BaseException e) {
				if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Failed to get KeywordFolder for guid = " + guid, e);
			}
    		if(keywordFolder != null) {
    			keywordFolders.add(keywordFolder);
    		}
    	}
        return keywordFolders;
    }


    public Results<ScoredDocument> findDocuments(String queryString, String ordering, Long offset, Integer count)
    {
        try {
        	if(count == null || count > MAX_COUNT) {
        		count = MAX_COUNT;
        	}
        	if(offset == null || offset < 0L) {
        		offset = 0L;
        	}
        	QueryOptions.Builder queryOptionsBuilder = QueryOptions.newBuilder();
        	
        	if(ordering != null && !ordering.isEmpty()) {
        		String[] parts = ordering.split("\\s+", 2);
        		SortExpression.Builder sortExpressionBuilder = SortExpression.newBuilder();
        		if(parts.length > 0) {
        			sortExpressionBuilder.setExpression(parts[0]);
        			if(parts.length > 1) {
        				if(parts[1].equalsIgnoreCase("desc")) {
        					sortExpressionBuilder.setDirection(SortExpression.SortDirection.DESCENDING);
        				} else {
        					sortExpressionBuilder.setDirection(SortExpression.SortDirection.ASCENDING);
        				}
        			} else {
        				// Default...
        				sortExpressionBuilder.setDirection(SortExpression.SortDirection.ASCENDING);
        			}
        		}
        		sortExpressionBuilder.setDefaultValue("");   // ???
        		SortExpression sortExpression = sortExpressionBuilder.build();
        		SortOptions.Builder sortOptionsBuilder = SortOptions.newBuilder(); 
        		sortOptionsBuilder.addSortExpression(sortExpression);
        		sortOptionsBuilder.setLimit((int) (count + offset));  // ???
        		SortOptions sortOptions = sortOptionsBuilder.build();
        		queryOptionsBuilder.setSortOptions(sortOptions);
        	}

        	queryOptionsBuilder.setLimit(count);
        	queryOptionsBuilder.setOffset(offset.intValue());
        	//queryOptionsBuilder.setCursor(Cursor.newBuilder().build());

        	List<String> fieldsToReturn = new ArrayList<String>();
        	List<String> fieldsToSnippet = new ArrayList<String>();        	
            fieldsToReturn.add("title");
            fieldsToReturn.add("description");
            fieldsToReturn.add("category");
        	queryOptionsBuilder.setFieldsToReturn(fieldsToReturn.toArray(new String[0]));
        	if(!fieldsToSnippet.isEmpty()) {
                queryOptionsBuilder.setFieldsToSnippet(fieldsToSnippet.toArray(new String[0]));
            }

            QueryOptions options = queryOptionsBuilder.build();
            Query query = Query.newBuilder().setOptions(options).build(queryString);
            return keywordFolderIndexBuilder.getIndex().search(query);
        } catch (SearchException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Search request with query " + queryString + " failed", e);
            return null;
        }
    }

}

