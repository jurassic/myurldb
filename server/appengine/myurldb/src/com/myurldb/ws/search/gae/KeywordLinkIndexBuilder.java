package com.myurldb.ws.search.gae;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.search.Field;
import com.google.appengine.api.search.GeoPoint;
import com.google.appengine.api.search.Document;
import com.google.appengine.api.search.Document.Builder;

import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.KeywordLink;


public class KeywordLinkIndexBuilder extends BaseIndexBuilder
{
    private static final Logger log = Logger.getLogger(KeywordLinkIndexBuilder.class.getName());
    
    public KeywordLinkIndexBuilder()
    {
        super("KeywordLinkIndex");	
    }
    
    public boolean addDocument(KeywordLink keywordLink)
    {
        return addDocument(keywordLink.getGuid(), keywordLink.getMemo(), keywordLink.getKeyword());
    }

    public boolean addDocument(String guid, String memo, String keyword)
    {
    	if(log.isLoggable(Level.FINER)) log.finer("BEGIN: addDocument() called with guid = " + guid);

    	Builder builder = Document.newBuilder().setId(guid);
        if(memo != null) {
            if(!memo.isEmpty()) {
                builder.addField(Field.newBuilder().setName("memo").setText(memo));
            }
        }
        if(keyword != null) {
            if(!keyword.isEmpty()) {
                builder.addField(Field.newBuilder().setName("keyword").setText(keyword));
            }
        }
    	Document doc = builder.build();
    	boolean suc = addDocument(doc);

    	if(log.isLoggable(Level.FINER)) log.finer("END: addDocument(). suc = " + suc);
    	return suc;
    }


}

