package com.myurldb.ws.search.gae;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.search.Field;
import com.google.appengine.api.search.GeoPoint;
import com.google.appengine.api.search.Document;
import com.google.appengine.api.search.Document.Builder;

import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.LinkMessage;


public class LinkMessageIndexBuilder extends BaseIndexBuilder
{
    private static final Logger log = Logger.getLogger(LinkMessageIndexBuilder.class.getName());
    
    public LinkMessageIndexBuilder()
    {
        super("LinkMessageIndex");	
    }
    
    public boolean addDocument(LinkMessage linkMessage)
    {
        return addDocument(linkMessage.getGuid(), linkMessage.getMessage());
    }

    public boolean addDocument(String guid, String message)
    {
    	if(log.isLoggable(Level.FINER)) log.finer("BEGIN: addDocument() called with guid = " + guid);

    	Builder builder = Document.newBuilder().setId(guid);
        if(message != null) {
            if(!message.isEmpty()) {
                builder.addField(Field.newBuilder().setName("message").setText(message));
            }
        }
    	Document doc = builder.build();
    	boolean suc = addDocument(doc);

    	if(log.isLoggable(Level.FINER)) log.finer("END: addDocument(). suc = " + suc);
    	return suc;
    }


}

