package com.myurldb.ws.search.gae;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.search.Field;
import com.google.appengine.api.search.GeoPoint;
import com.google.appengine.api.search.Cursor;
import com.google.appengine.api.search.Query;
import com.google.appengine.api.search.QueryOptions;
import com.google.appengine.api.search.Results;
import com.google.appengine.api.search.SearchException;
import com.google.appengine.api.search.ScoredDocument;
import com.google.appengine.api.search.SortExpression;
import com.google.appengine.api.search.SortOptions;
import com.google.appengine.api.search.SearchException;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.LinkMessage;
import com.myurldb.ws.dao.LinkMessageDAO;
import com.myurldb.ws.service.DAOFactoryManager;
import com.myurldb.ws.search.bean.LinkMessageQueryBean;


public class LinkMessageQueryHelper extends BaseQueryHelper
{
    private static final Logger log = Logger.getLogger(LinkMessageQueryHelper.class.getName());
    
    
    private LinkMessageIndexBuilder linkMessageIndexBuilder = null;

    public LinkMessageQueryHelper()
    {
    	this(null);
    }
    public LinkMessageQueryHelper(LinkMessageIndexBuilder linkMessageIndexBuilder)
    {
        super();
        if(linkMessageIndexBuilder != null) {
        	this.linkMessageIndexBuilder = linkMessageIndexBuilder;
        } else {
        	this.linkMessageIndexBuilder = new LinkMessageIndexBuilder();
        }
    }

    // TBD:    
    public List<LinkMessageQueryBean> findLinkMessageQueryBeans(String queryString, String ordering,  Long offset, Integer count)
    {
        // TBD
    	Results<ScoredDocument> docs = findDocuments(queryString, ordering, offset, count);
    	if(docs == null) {
    		return null;
    	}
    	List<LinkMessageQueryBean> linkMessageQueryBeans = new ArrayList<LinkMessageQueryBean>();
    	for(ScoredDocument doc : docs) {
    		LinkMessageQueryBean bean = new LinkMessageQueryBean();

    	    int rank = doc.getRank();
    		bean.setRank(rank);
    		String guid = doc.getId();
    		bean.setLinkMessageGuid(guid);
            Field fMessage = doc.getOnlyField("message");
            String vMessage = fMessage.getText();
            bean.setMessage(vMessage);

   	        linkMessageQueryBeans.add(bean);
    	}
        return linkMessageQueryBeans;
    }

    // Note: This is very inefficient....
    public List<LinkMessage> findLinkMessages(String queryString, String ordering,  Long offset, Integer count)
    {
        // TBD
    	Results<ScoredDocument> docs = findDocuments(queryString, ordering, offset, count);
    	if(docs == null) {
    		return null;
    	}
    	LinkMessageDAO linkMessageDAO = DAOFactoryManager.getDAOFactory().getLinkMessageDAO();
    	List<LinkMessage> linkMessages = new ArrayList<LinkMessage>();
    	for(ScoredDocument doc : docs) {
    		String guid = doc.getId();
    		LinkMessage linkMessage = null;
    		try {
    			linkMessage = linkMessageDAO.getLinkMessage(guid);
			} catch (BaseException e) {
				if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Failed to get LinkMessage for guid = " + guid, e);
			}
    		if(linkMessage != null) {
    			linkMessages.add(linkMessage);
    		}
    	}
        return linkMessages;
    }


    public Results<ScoredDocument> findDocuments(String queryString, String ordering, Long offset, Integer count)
    {
        try {
        	if(count == null || count > MAX_COUNT) {
        		count = MAX_COUNT;
        	}
        	if(offset == null || offset < 0L) {
        		offset = 0L;
        	}
        	QueryOptions.Builder queryOptionsBuilder = QueryOptions.newBuilder();
        	
        	if(ordering != null && !ordering.isEmpty()) {
        		String[] parts = ordering.split("\\s+", 2);
        		SortExpression.Builder sortExpressionBuilder = SortExpression.newBuilder();
        		if(parts.length > 0) {
        			sortExpressionBuilder.setExpression(parts[0]);
        			if(parts.length > 1) {
        				if(parts[1].equalsIgnoreCase("desc")) {
        					sortExpressionBuilder.setDirection(SortExpression.SortDirection.DESCENDING);
        				} else {
        					sortExpressionBuilder.setDirection(SortExpression.SortDirection.ASCENDING);
        				}
        			} else {
        				// Default...
        				sortExpressionBuilder.setDirection(SortExpression.SortDirection.ASCENDING);
        			}
        		}
        		sortExpressionBuilder.setDefaultValue("");   // ???
        		SortExpression sortExpression = sortExpressionBuilder.build();
        		SortOptions.Builder sortOptionsBuilder = SortOptions.newBuilder(); 
        		sortOptionsBuilder.addSortExpression(sortExpression);
        		sortOptionsBuilder.setLimit((int) (count + offset));  // ???
        		SortOptions sortOptions = sortOptionsBuilder.build();
        		queryOptionsBuilder.setSortOptions(sortOptions);
        	}

        	queryOptionsBuilder.setLimit(count);
        	queryOptionsBuilder.setOffset(offset.intValue());
        	//queryOptionsBuilder.setCursor(Cursor.newBuilder().build());

        	List<String> fieldsToReturn = new ArrayList<String>();
        	List<String> fieldsToSnippet = new ArrayList<String>();        	
            fieldsToReturn.add("message");
        	queryOptionsBuilder.setFieldsToReturn(fieldsToReturn.toArray(new String[0]));
        	if(!fieldsToSnippet.isEmpty()) {
                queryOptionsBuilder.setFieldsToSnippet(fieldsToSnippet.toArray(new String[0]));
            }

            QueryOptions options = queryOptionsBuilder.build();
            Query query = Query.newBuilder().setOptions(options).build(queryString);
            return linkMessageIndexBuilder.getIndex().search(query);
        } catch (SearchException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Search request with query " + queryString + " failed", e);
            return null;
        }
    }

}

