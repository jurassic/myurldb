package com.myurldb.ws.search.gae;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.search.Field;
import com.google.appengine.api.search.GeoPoint;
import com.google.appengine.api.search.Document;
import com.google.appengine.api.search.Document.Builder;

import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.ReferrerInfoStruct;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.ShortLink;


public class ShortLinkIndexBuilder extends BaseIndexBuilder
{
    private static final Logger log = Logger.getLogger(ShortLinkIndexBuilder.class.getName());
    
    public ShortLinkIndexBuilder()
    {
        super("ShortLinkIndex");	
    }
    
    public boolean addDocument(ShortLink shortLink)
    {
        return addDocument(shortLink.getGuid(), shortLink.getDisplayMessage(), shortLink.getShortMessage());
    }

    public boolean addDocument(String guid, String displayMessage, String shortMessage)
    {
    	if(log.isLoggable(Level.FINER)) log.finer("BEGIN: addDocument() called with guid = " + guid);

    	Builder builder = Document.newBuilder().setId(guid);
        if(displayMessage != null) {
            if(!displayMessage.isEmpty()) {
                builder.addField(Field.newBuilder().setName("displayMessage").setText(displayMessage));
            }
        }
        if(shortMessage != null) {
            if(!shortMessage.isEmpty()) {
                builder.addField(Field.newBuilder().setName("shortMessage").setText(shortMessage));
            }
        }
    	Document doc = builder.build();
    	boolean suc = addDocument(doc);

    	if(log.isLoggable(Level.FINER)) log.finer("END: addDocument(). suc = " + suc);
    	return suc;
    }


}

