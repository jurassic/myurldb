package com.myurldb.ws.search.gae;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.search.Field;
import com.google.appengine.api.search.GeoPoint;
import com.google.appengine.api.search.Document;
import com.google.appengine.api.search.Document.Builder;

import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.ShortPassageAttribute;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.ShortPassage;


public class ShortPassageIndexBuilder extends BaseIndexBuilder
{
    private static final Logger log = Logger.getLogger(ShortPassageIndexBuilder.class.getName());
    
    public ShortPassageIndexBuilder()
    {
        super("ShortPassageIndex");	
    }
    
    public boolean addDocument(ShortPassage shortPassage)
    {
        return addDocument(shortPassage.getGuid(), shortPassage.getShortText());
    }

    public boolean addDocument(String guid, String shortText)
    {
    	if(log.isLoggable(Level.FINER)) log.finer("BEGIN: addDocument() called with guid = " + guid);

    	Builder builder = Document.newBuilder().setId(guid);
        if(shortText != null) {
            if(!shortText.isEmpty()) {
                builder.addField(Field.newBuilder().setName("shortText").setText(shortText));
            }
        }
    	Document doc = builder.build();
    	boolean suc = addDocument(doc);

    	if(log.isLoggable(Level.FINER)) log.finer("END: addDocument(). suc = " + suc);
    	return suc;
    }


}

