package com.myurldb.ws.search.gae;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.search.Field;
import com.google.appengine.api.search.GeoPoint;
import com.google.appengine.api.search.Cursor;
import com.google.appengine.api.search.Query;
import com.google.appengine.api.search.QueryOptions;
import com.google.appengine.api.search.Results;
import com.google.appengine.api.search.SearchException;
import com.google.appengine.api.search.ScoredDocument;
import com.google.appengine.api.search.SortExpression;
import com.google.appengine.api.search.SortOptions;
import com.google.appengine.api.search.SearchException;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.util.CommonUtil;
import com.myurldb.ws.core.GUID;
import com.myurldb.ws.GeoPointStruct;
import com.myurldb.ws.StreetAddressStruct;
import com.myurldb.ws.GaeAppStruct;
import com.myurldb.ws.FullNameStruct;
import com.myurldb.ws.GaeUserStruct;
import com.myurldb.ws.bean.GeoPointStructBean;
import com.myurldb.ws.bean.StreetAddressStructBean;
import com.myurldb.ws.bean.GaeAppStructBean;
import com.myurldb.ws.bean.FullNameStructBean;
import com.myurldb.ws.bean.GaeUserStructBean;
import com.myurldb.ws.User;
import com.myurldb.ws.dao.UserDAO;
import com.myurldb.ws.service.DAOFactoryManager;
import com.myurldb.ws.search.bean.UserQueryBean;


public class UserQueryHelper extends BaseQueryHelper
{
    private static final Logger log = Logger.getLogger(UserQueryHelper.class.getName());
    
    
    private UserIndexBuilder userIndexBuilder = null;

    public UserQueryHelper()
    {
    	this(null);
    }
    public UserQueryHelper(UserIndexBuilder userIndexBuilder)
    {
        super();
        if(userIndexBuilder != null) {
        	this.userIndexBuilder = userIndexBuilder;
        } else {
        	this.userIndexBuilder = new UserIndexBuilder();
        }
    }

    // TBD:    
    public List<UserQueryBean> findUserQueryBeans(String queryString, String ordering,  Long offset, Integer count)
    {
        // TBD
    	Results<ScoredDocument> docs = findDocuments(queryString, ordering, offset, count);
    	if(docs == null) {
    		return null;
    	}
    	List<UserQueryBean> userQueryBeans = new ArrayList<UserQueryBean>();
    	for(ScoredDocument doc : docs) {
    		UserQueryBean bean = new UserQueryBean();

    	    int rank = doc.getRank();
    		bean.setRank(rank);
    		String guid = doc.getId();
    		bean.setUserGuid(guid);
            Field fNickname = doc.getOnlyField("nickname");
            String vNickname = fNickname.getText();
            bean.setNickname(vNickname);
            Field fLocation = doc.getOnlyField("location");
            String vLocation = fLocation.getText();
            bean.setLocation(vLocation);
            Field fGeoPoint = doc.getOnlyField("geoPoint");
            GeoPoint gpGeoPoint = fGeoPoint.getGeoPoint();
            GeoPointStructBean vGeoPoint = new GeoPointStructBean();
            vGeoPoint.setLatitude(gpGeoPoint.getLatitude());
            vGeoPoint.setLongitude(gpGeoPoint.getLongitude());
            bean.setGeoPoint(vGeoPoint);

   	        userQueryBeans.add(bean);
    	}
        return userQueryBeans;
    }

    // Note: This is very inefficient....
    public List<User> findUsers(String queryString, String ordering,  Long offset, Integer count)
    {
        // TBD
    	Results<ScoredDocument> docs = findDocuments(queryString, ordering, offset, count);
    	if(docs == null) {
    		return null;
    	}
    	UserDAO userDAO = DAOFactoryManager.getDAOFactory().getUserDAO();
    	List<User> users = new ArrayList<User>();
    	for(ScoredDocument doc : docs) {
    		String guid = doc.getId();
    		User user = null;
    		try {
    			user = userDAO.getUser(guid);
			} catch (BaseException e) {
				if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Failed to get User for guid = " + guid, e);
			}
    		if(user != null) {
    			users.add(user);
    		}
    	}
        return users;
    }


    public Results<ScoredDocument> findDocuments(String queryString, String ordering, Long offset, Integer count)
    {
        try {
        	if(count == null || count > MAX_COUNT) {
        		count = MAX_COUNT;
        	}
        	if(offset == null || offset < 0L) {
        		offset = 0L;
        	}
        	QueryOptions.Builder queryOptionsBuilder = QueryOptions.newBuilder();
        	
        	if(ordering != null && !ordering.isEmpty()) {
        		String[] parts = ordering.split("\\s+", 2);
        		SortExpression.Builder sortExpressionBuilder = SortExpression.newBuilder();
        		if(parts.length > 0) {
        			sortExpressionBuilder.setExpression(parts[0]);
        			if(parts.length > 1) {
        				if(parts[1].equalsIgnoreCase("desc")) {
        					sortExpressionBuilder.setDirection(SortExpression.SortDirection.DESCENDING);
        				} else {
        					sortExpressionBuilder.setDirection(SortExpression.SortDirection.ASCENDING);
        				}
        			} else {
        				// Default...
        				sortExpressionBuilder.setDirection(SortExpression.SortDirection.ASCENDING);
        			}
        		}
        		sortExpressionBuilder.setDefaultValue("");   // ???
        		SortExpression sortExpression = sortExpressionBuilder.build();
        		SortOptions.Builder sortOptionsBuilder = SortOptions.newBuilder(); 
        		sortOptionsBuilder.addSortExpression(sortExpression);
        		sortOptionsBuilder.setLimit((int) (count + offset));  // ???
        		SortOptions sortOptions = sortOptionsBuilder.build();
        		queryOptionsBuilder.setSortOptions(sortOptions);
        	}

        	queryOptionsBuilder.setLimit(count);
        	queryOptionsBuilder.setOffset(offset.intValue());
        	//queryOptionsBuilder.setCursor(Cursor.newBuilder().build());

        	List<String> fieldsToReturn = new ArrayList<String>();
        	List<String> fieldsToSnippet = new ArrayList<String>();        	
            fieldsToReturn.add("nickname");
            fieldsToReturn.add("location");
            fieldsToReturn.add("geoPoint");
        	queryOptionsBuilder.setFieldsToReturn(fieldsToReturn.toArray(new String[0]));
        	if(!fieldsToSnippet.isEmpty()) {
                queryOptionsBuilder.setFieldsToSnippet(fieldsToSnippet.toArray(new String[0]));
            }

            QueryOptions options = queryOptionsBuilder.build();
            Query query = Query.newBuilder().setOptions(options).build(queryString);
            return userIndexBuilder.getIndex().search(query);
        } catch (SearchException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Search request with query " + queryString + " failed", e);
            return null;
        }
    }

}

