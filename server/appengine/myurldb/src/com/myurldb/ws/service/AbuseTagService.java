package com.myurldb.ws.service;

import java.util.List;

import com.myurldb.ws.AbuseTag;
import com.myurldb.ws.BaseException;

// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface AbuseTagService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    AbuseTag getAbuseTag(String guid) throws BaseException;
    Object getAbuseTag(String guid, String field) throws BaseException;
    List<AbuseTag> getAbuseTags(List<String> guids) throws BaseException;
    List<AbuseTag> getAllAbuseTags() throws BaseException;
    List<AbuseTag> getAllAbuseTags(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllAbuseTagKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<AbuseTag> findAbuseTags(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<AbuseTag> findAbuseTags(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findAbuseTagKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createAbuseTag(String shortLink, String shortUrl, String longUrl, String longUrlHash, String user, String ipAddress, Integer tag, String comment, String status, String reviewer, String action, String note, String pastActions, Long reviewedTime, Long actionTime) throws BaseException;
    //String createAbuseTag(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return AbuseTag?)
    String createAbuseTag(AbuseTag abuseTag) throws BaseException;          // Returns Guid.  (Return AbuseTag?)
    Boolean updateAbuseTag(String guid, String shortLink, String shortUrl, String longUrl, String longUrlHash, String user, String ipAddress, Integer tag, String comment, String status, String reviewer, String action, String note, String pastActions, Long reviewedTime, Long actionTime) throws BaseException;
    //Boolean updateAbuseTag(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateAbuseTag(AbuseTag abuseTag) throws BaseException;
    Boolean deleteAbuseTag(String guid) throws BaseException;
    Boolean deleteAbuseTag(AbuseTag abuseTag) throws BaseException;
    Long deleteAbuseTags(String filter, String params, List<String> values) throws BaseException;

//    Integer createAbuseTags(List<AbuseTag> abuseTags) throws BaseException;
//    Boolean updateeAbuseTags(List<AbuseTag> abuseTags) throws BaseException;

}
