package com.myurldb.ws.service;

import java.util.List;

import com.myurldb.ws.AppCustomDomain;
import com.myurldb.ws.BaseException;

// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface AppCustomDomainService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    AppCustomDomain getAppCustomDomain(String guid) throws BaseException;
    Object getAppCustomDomain(String guid, String field) throws BaseException;
    List<AppCustomDomain> getAppCustomDomains(List<String> guids) throws BaseException;
    List<AppCustomDomain> getAllAppCustomDomains() throws BaseException;
    List<AppCustomDomain> getAllAppCustomDomains(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllAppCustomDomainKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<AppCustomDomain> findAppCustomDomains(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<AppCustomDomain> findAppCustomDomains(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findAppCustomDomainKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createAppCustomDomain(String appId, String siteDomain, String domain, Boolean verified, String status, Long verifiedTime) throws BaseException;
    //String createAppCustomDomain(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return AppCustomDomain?)
    String createAppCustomDomain(AppCustomDomain appCustomDomain) throws BaseException;          // Returns Guid.  (Return AppCustomDomain?)
    Boolean updateAppCustomDomain(String guid, String appId, String siteDomain, String domain, Boolean verified, String status, Long verifiedTime) throws BaseException;
    //Boolean updateAppCustomDomain(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateAppCustomDomain(AppCustomDomain appCustomDomain) throws BaseException;
    Boolean deleteAppCustomDomain(String guid) throws BaseException;
    Boolean deleteAppCustomDomain(AppCustomDomain appCustomDomain) throws BaseException;
    Long deleteAppCustomDomains(String filter, String params, List<String> values) throws BaseException;

//    Integer createAppCustomDomains(List<AppCustomDomain> appCustomDomains) throws BaseException;
//    Boolean updateeAppCustomDomains(List<AppCustomDomain> appCustomDomains) throws BaseException;

}
