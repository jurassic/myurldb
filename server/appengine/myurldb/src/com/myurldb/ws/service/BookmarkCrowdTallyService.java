package com.myurldb.ws.service;

import java.util.List;

import com.myurldb.ws.BookmarkCrowdTally;
import com.myurldb.ws.BaseException;

// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface BookmarkCrowdTallyService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    BookmarkCrowdTally getBookmarkCrowdTally(String guid) throws BaseException;
    Object getBookmarkCrowdTally(String guid, String field) throws BaseException;
    List<BookmarkCrowdTally> getBookmarkCrowdTallies(List<String> guids) throws BaseException;
    List<BookmarkCrowdTally> getAllBookmarkCrowdTallies() throws BaseException;
    List<BookmarkCrowdTally> getAllBookmarkCrowdTallies(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllBookmarkCrowdTallyKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<BookmarkCrowdTally> findBookmarkCrowdTallies(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<BookmarkCrowdTally> findBookmarkCrowdTallies(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findBookmarkCrowdTallyKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createBookmarkCrowdTally(String user, String shortLink, String domain, String token, String longUrl, String shortUrl, String tallyDate, String status, String note, Long expirationTime, String bookmarkFolder, String contentTag, String referenceElement, String elementType, String keywordLink) throws BaseException;
    //String createBookmarkCrowdTally(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return BookmarkCrowdTally?)
    String createBookmarkCrowdTally(BookmarkCrowdTally bookmarkCrowdTally) throws BaseException;          // Returns Guid.  (Return BookmarkCrowdTally?)
    Boolean updateBookmarkCrowdTally(String guid, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, String tallyDate, String status, String note, Long expirationTime, String bookmarkFolder, String contentTag, String referenceElement, String elementType, String keywordLink) throws BaseException;
    //Boolean updateBookmarkCrowdTally(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateBookmarkCrowdTally(BookmarkCrowdTally bookmarkCrowdTally) throws BaseException;
    Boolean deleteBookmarkCrowdTally(String guid) throws BaseException;
    Boolean deleteBookmarkCrowdTally(BookmarkCrowdTally bookmarkCrowdTally) throws BaseException;
    Long deleteBookmarkCrowdTallies(String filter, String params, List<String> values) throws BaseException;

//    Integer createBookmarkCrowdTallies(List<BookmarkCrowdTally> bookmarkCrowdTallies) throws BaseException;
//    Boolean updateeBookmarkCrowdTallies(List<BookmarkCrowdTally> bookmarkCrowdTallies) throws BaseException;

}
