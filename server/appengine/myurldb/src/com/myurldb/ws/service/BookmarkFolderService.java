package com.myurldb.ws.service;

import java.util.List;

import com.myurldb.ws.BookmarkFolder;
import com.myurldb.ws.BaseException;

// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface BookmarkFolderService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    BookmarkFolder getBookmarkFolder(String guid) throws BaseException;
    Object getBookmarkFolder(String guid, String field) throws BaseException;
    List<BookmarkFolder> getBookmarkFolders(List<String> guids) throws BaseException;
    List<BookmarkFolder> getAllBookmarkFolders() throws BaseException;
    List<BookmarkFolder> getAllBookmarkFolders(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllBookmarkFolderKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<BookmarkFolder> findBookmarkFolders(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<BookmarkFolder> findBookmarkFolders(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findBookmarkFolderKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createBookmarkFolder(String appClient, String clientRootDomain, String user, String title, String description, String type, String category, String parent, String aggregate, String acl, Boolean exportable, String status, String note, String keywordFolder) throws BaseException;
    //String createBookmarkFolder(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return BookmarkFolder?)
    String createBookmarkFolder(BookmarkFolder bookmarkFolder) throws BaseException;          // Returns Guid.  (Return BookmarkFolder?)
    Boolean updateBookmarkFolder(String guid, String appClient, String clientRootDomain, String user, String title, String description, String type, String category, String parent, String aggregate, String acl, Boolean exportable, String status, String note, String keywordFolder) throws BaseException;
    //Boolean updateBookmarkFolder(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateBookmarkFolder(BookmarkFolder bookmarkFolder) throws BaseException;
    Boolean deleteBookmarkFolder(String guid) throws BaseException;
    Boolean deleteBookmarkFolder(BookmarkFolder bookmarkFolder) throws BaseException;
    Long deleteBookmarkFolders(String filter, String params, List<String> values) throws BaseException;

//    Integer createBookmarkFolders(List<BookmarkFolder> bookmarkFolders) throws BaseException;
//    Boolean updateeBookmarkFolders(List<BookmarkFolder> bookmarkFolders) throws BaseException;

}
