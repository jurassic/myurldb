package com.myurldb.ws.service;

import java.util.List;

import com.myurldb.ws.ClientSetting;
import com.myurldb.ws.BaseException;

// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface ClientSettingService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    ClientSetting getClientSetting(String guid) throws BaseException;
    Object getClientSetting(String guid, String field) throws BaseException;
    List<ClientSetting> getClientSettings(List<String> guids) throws BaseException;
    List<ClientSetting> getAllClientSettings() throws BaseException;
    List<ClientSetting> getAllClientSettings(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllClientSettingKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<ClientSetting> findClientSettings(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<ClientSetting> findClientSettings(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findClientSettingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createClientSetting(String appClient, String admin, Boolean autoRedirectEnabled, Boolean viewEnabled, Boolean shareEnabled, String defaultBaseDomain, String defaultDomainType, String defaultTokenType, String defaultRedirectType, Long defaultFlashDuration, String defaultAccessType, String defaultViewType) throws BaseException;
    //String createClientSetting(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return ClientSetting?)
    String createClientSetting(ClientSetting clientSetting) throws BaseException;          // Returns Guid.  (Return ClientSetting?)
    Boolean updateClientSetting(String guid, String appClient, String admin, Boolean autoRedirectEnabled, Boolean viewEnabled, Boolean shareEnabled, String defaultBaseDomain, String defaultDomainType, String defaultTokenType, String defaultRedirectType, Long defaultFlashDuration, String defaultAccessType, String defaultViewType) throws BaseException;
    //Boolean updateClientSetting(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateClientSetting(ClientSetting clientSetting) throws BaseException;
    Boolean deleteClientSetting(String guid) throws BaseException;
    Boolean deleteClientSetting(ClientSetting clientSetting) throws BaseException;
    Long deleteClientSettings(String filter, String params, List<String> values) throws BaseException;

//    Integer createClientSettings(List<ClientSetting> clientSettings) throws BaseException;
//    Boolean updateeClientSettings(List<ClientSetting> clientSettings) throws BaseException;

}
