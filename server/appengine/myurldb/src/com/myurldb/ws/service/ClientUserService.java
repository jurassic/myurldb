package com.myurldb.ws.service;

import java.util.List;

import com.myurldb.ws.ClientUser;
import com.myurldb.ws.BaseException;

// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface ClientUserService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    ClientUser getClientUser(String guid) throws BaseException;
    Object getClientUser(String guid, String field) throws BaseException;
    List<ClientUser> getClientUsers(List<String> guids) throws BaseException;
    List<ClientUser> getAllClientUsers() throws BaseException;
    List<ClientUser> getAllClientUsers(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllClientUserKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<ClientUser> findClientUsers(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<ClientUser> findClientUsers(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findClientUserKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createClientUser(String appClient, String user, String role, Boolean provisioned, Boolean licensed, String status) throws BaseException;
    //String createClientUser(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return ClientUser?)
    String createClientUser(ClientUser clientUser) throws BaseException;          // Returns Guid.  (Return ClientUser?)
    Boolean updateClientUser(String guid, String appClient, String user, String role, Boolean provisioned, Boolean licensed, String status) throws BaseException;
    //Boolean updateClientUser(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateClientUser(ClientUser clientUser) throws BaseException;
    Boolean deleteClientUser(String guid) throws BaseException;
    Boolean deleteClientUser(ClientUser clientUser) throws BaseException;
    Long deleteClientUsers(String filter, String params, List<String> values) throws BaseException;

//    Integer createClientUsers(List<ClientUser> clientUsers) throws BaseException;
//    Boolean updateeClientUsers(List<ClientUser> clientUsers) throws BaseException;

}
