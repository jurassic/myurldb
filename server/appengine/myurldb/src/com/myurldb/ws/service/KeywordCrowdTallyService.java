package com.myurldb.ws.service;

import java.util.List;

import com.myurldb.ws.KeywordCrowdTally;
import com.myurldb.ws.BaseException;

// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface KeywordCrowdTallyService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    KeywordCrowdTally getKeywordCrowdTally(String guid) throws BaseException;
    Object getKeywordCrowdTally(String guid, String field) throws BaseException;
    List<KeywordCrowdTally> getKeywordCrowdTallies(List<String> guids) throws BaseException;
    List<KeywordCrowdTally> getAllKeywordCrowdTallies() throws BaseException;
    List<KeywordCrowdTally> getAllKeywordCrowdTallies(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllKeywordCrowdTallyKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<KeywordCrowdTally> findKeywordCrowdTallies(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<KeywordCrowdTally> findKeywordCrowdTallies(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findKeywordCrowdTallyKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createKeywordCrowdTally(String user, String shortLink, String domain, String token, String longUrl, String shortUrl, String tallyDate, String status, String note, Long expirationTime, String keywordFolder, String folderPath, String keyword, String queryKey, String scope, Boolean caseSensitive) throws BaseException;
    //String createKeywordCrowdTally(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return KeywordCrowdTally?)
    String createKeywordCrowdTally(KeywordCrowdTally keywordCrowdTally) throws BaseException;          // Returns Guid.  (Return KeywordCrowdTally?)
    Boolean updateKeywordCrowdTally(String guid, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, String tallyDate, String status, String note, Long expirationTime, String keywordFolder, String folderPath, String keyword, String queryKey, String scope, Boolean caseSensitive) throws BaseException;
    //Boolean updateKeywordCrowdTally(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateKeywordCrowdTally(KeywordCrowdTally keywordCrowdTally) throws BaseException;
    Boolean deleteKeywordCrowdTally(String guid) throws BaseException;
    Boolean deleteKeywordCrowdTally(KeywordCrowdTally keywordCrowdTally) throws BaseException;
    Long deleteKeywordCrowdTallies(String filter, String params, List<String> values) throws BaseException;

//    Integer createKeywordCrowdTallies(List<KeywordCrowdTally> keywordCrowdTallies) throws BaseException;
//    Boolean updateeKeywordCrowdTallies(List<KeywordCrowdTally> keywordCrowdTallies) throws BaseException;

}
