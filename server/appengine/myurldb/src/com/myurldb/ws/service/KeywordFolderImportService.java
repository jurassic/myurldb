package com.myurldb.ws.service;

import java.util.List;

import com.myurldb.ws.KeywordFolderImport;
import com.myurldb.ws.BaseException;

// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface KeywordFolderImportService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    KeywordFolderImport getKeywordFolderImport(String guid) throws BaseException;
    Object getKeywordFolderImport(String guid, String field) throws BaseException;
    List<KeywordFolderImport> getKeywordFolderImports(List<String> guids) throws BaseException;
    List<KeywordFolderImport> getAllKeywordFolderImports() throws BaseException;
    List<KeywordFolderImport> getAllKeywordFolderImports(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllKeywordFolderImportKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<KeywordFolderImport> findKeywordFolderImports(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<KeywordFolderImport> findKeywordFolderImports(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findKeywordFolderImportKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createKeywordFolderImport(String user, Integer precedence, String importType, String importedFolder, String importedFolderUser, String importedFolderTitle, String importedFolderPath, String status, String note, String keywordFolder) throws BaseException;
    //String createKeywordFolderImport(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return KeywordFolderImport?)
    String createKeywordFolderImport(KeywordFolderImport keywordFolderImport) throws BaseException;          // Returns Guid.  (Return KeywordFolderImport?)
    Boolean updateKeywordFolderImport(String guid, String user, Integer precedence, String importType, String importedFolder, String importedFolderUser, String importedFolderTitle, String importedFolderPath, String status, String note, String keywordFolder) throws BaseException;
    //Boolean updateKeywordFolderImport(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateKeywordFolderImport(KeywordFolderImport keywordFolderImport) throws BaseException;
    Boolean deleteKeywordFolderImport(String guid) throws BaseException;
    Boolean deleteKeywordFolderImport(KeywordFolderImport keywordFolderImport) throws BaseException;
    Long deleteKeywordFolderImports(String filter, String params, List<String> values) throws BaseException;

//    Integer createKeywordFolderImports(List<KeywordFolderImport> keywordFolderImports) throws BaseException;
//    Boolean updateeKeywordFolderImports(List<KeywordFolderImport> keywordFolderImports) throws BaseException;

}
