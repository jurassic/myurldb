package com.myurldb.ws.service;

import java.util.List;

import com.myurldb.ws.KeywordLink;
import com.myurldb.ws.BaseException;

// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface KeywordLinkService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    KeywordLink getKeywordLink(String guid) throws BaseException;
    Object getKeywordLink(String guid, String field) throws BaseException;
    List<KeywordLink> getKeywordLinks(List<String> guids) throws BaseException;
    List<KeywordLink> getAllKeywordLinks() throws BaseException;
    List<KeywordLink> getAllKeywordLinks(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllKeywordLinkKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<KeywordLink> findKeywordLinks(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<KeywordLink> findKeywordLinks(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findKeywordLinkKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createKeywordLink(String appClient, String clientRootDomain, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, Boolean internal, Boolean caching, String memo, String status, String note, Long expirationTime, String keywordFolder, String folderPath, String keyword, String queryKey, String scope, Boolean dynamic, Boolean caseSensitive) throws BaseException;
    //String createKeywordLink(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return KeywordLink?)
    String createKeywordLink(KeywordLink keywordLink) throws BaseException;          // Returns Guid.  (Return KeywordLink?)
    Boolean updateKeywordLink(String guid, String appClient, String clientRootDomain, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, Boolean internal, Boolean caching, String memo, String status, String note, Long expirationTime, String keywordFolder, String folderPath, String keyword, String queryKey, String scope, Boolean dynamic, Boolean caseSensitive) throws BaseException;
    //Boolean updateKeywordLink(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateKeywordLink(KeywordLink keywordLink) throws BaseException;
    Boolean deleteKeywordLink(String guid) throws BaseException;
    Boolean deleteKeywordLink(KeywordLink keywordLink) throws BaseException;
    Long deleteKeywordLinks(String filter, String params, List<String> values) throws BaseException;

//    Integer createKeywordLinks(List<KeywordLink> keywordLinks) throws BaseException;
//    Boolean updateeKeywordLinks(List<KeywordLink> keywordLinks) throws BaseException;

}
