package com.myurldb.ws.service;

import java.util.List;

import com.myurldb.ws.LinkPassphrase;
import com.myurldb.ws.BaseException;

// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface LinkPassphraseService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    LinkPassphrase getLinkPassphrase(String guid) throws BaseException;
    Object getLinkPassphrase(String guid, String field) throws BaseException;
    List<LinkPassphrase> getLinkPassphrases(List<String> guids) throws BaseException;
    List<LinkPassphrase> getAllLinkPassphrases() throws BaseException;
    List<LinkPassphrase> getAllLinkPassphrases(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllLinkPassphraseKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<LinkPassphrase> findLinkPassphrases(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<LinkPassphrase> findLinkPassphrases(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findLinkPassphraseKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createLinkPassphrase(String shortLink, String passphrase) throws BaseException;
    //String createLinkPassphrase(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return LinkPassphrase?)
    String createLinkPassphrase(LinkPassphrase linkPassphrase) throws BaseException;          // Returns Guid.  (Return LinkPassphrase?)
    Boolean updateLinkPassphrase(String guid, String shortLink, String passphrase) throws BaseException;
    //Boolean updateLinkPassphrase(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateLinkPassphrase(LinkPassphrase linkPassphrase) throws BaseException;
    Boolean deleteLinkPassphrase(String guid) throws BaseException;
    Boolean deleteLinkPassphrase(LinkPassphrase linkPassphrase) throws BaseException;
    Long deleteLinkPassphrases(String filter, String params, List<String> values) throws BaseException;

//    Integer createLinkPassphrases(List<LinkPassphrase> linkPassphrases) throws BaseException;
//    Boolean updateeLinkPassphrases(List<LinkPassphrase> linkPassphrases) throws BaseException;

}
