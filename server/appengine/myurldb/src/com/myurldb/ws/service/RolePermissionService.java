package com.myurldb.ws.service;

import java.util.List;

import com.myurldb.ws.RolePermission;
import com.myurldb.ws.BaseException;

// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface RolePermissionService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    RolePermission getRolePermission(String guid) throws BaseException;
    Object getRolePermission(String guid, String field) throws BaseException;
    List<RolePermission> getRolePermissions(List<String> guids) throws BaseException;
    List<RolePermission> getAllRolePermissions() throws BaseException;
    List<RolePermission> getAllRolePermissions(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllRolePermissionKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<RolePermission> findRolePermissions(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<RolePermission> findRolePermissions(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findRolePermissionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createRolePermission(String role, String permissionName, String resource, String instance, String action, String status) throws BaseException;
    //String createRolePermission(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return RolePermission?)
    String createRolePermission(RolePermission rolePermission) throws BaseException;          // Returns Guid.  (Return RolePermission?)
    Boolean updateRolePermission(String guid, String role, String permissionName, String resource, String instance, String action, String status) throws BaseException;
    //Boolean updateRolePermission(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateRolePermission(RolePermission rolePermission) throws BaseException;
    Boolean deleteRolePermission(String guid) throws BaseException;
    Boolean deleteRolePermission(RolePermission rolePermission) throws BaseException;
    Long deleteRolePermissions(String filter, String params, List<String> values) throws BaseException;

//    Integer createRolePermissions(List<RolePermission> rolePermissions) throws BaseException;
//    Boolean updateeRolePermissions(List<RolePermission> rolePermissions) throws BaseException;

}
