package com.myurldb.ws.service;

import java.util.List;

import com.myurldb.ws.ReferrerInfoStruct;
import com.myurldb.ws.ShortLink;
import com.myurldb.ws.BaseException;

// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface ShortLinkService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    ShortLink getShortLink(String guid) throws BaseException;
    Object getShortLink(String guid, String field) throws BaseException;
    List<ShortLink> getShortLinks(List<String> guids) throws BaseException;
    List<ShortLink> getAllShortLinks() throws BaseException;
    List<ShortLink> getAllShortLinks(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllShortLinkKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<ShortLink> findShortLinks(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<ShortLink> findShortLinks(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findShortLinkKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createShortLink(String appClient, String clientRootDomain, String owner, String longUrlDomain, String longUrl, String longUrlFull, String longUrlHash, Boolean reuseExistingShortUrl, Boolean failIfShortUrlExists, String domain, String domainType, String usercode, String username, String tokenPrefix, String token, String tokenType, String sassyTokenType, String shortUrl, String shortPassage, String redirectType, Long flashDuration, String accessType, String viewType, String shareType, Boolean readOnly, String displayMessage, String shortMessage, Boolean keywordEnabled, Boolean bookmarkEnabled, ReferrerInfoStruct referrerInfo, String status, String note, Long expirationTime) throws BaseException;
    //String createShortLink(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return ShortLink?)
    String createShortLink(ShortLink shortLink) throws BaseException;          // Returns Guid.  (Return ShortLink?)
    Boolean updateShortLink(String guid, String appClient, String clientRootDomain, String owner, String longUrlDomain, String longUrl, String longUrlFull, String longUrlHash, Boolean reuseExistingShortUrl, Boolean failIfShortUrlExists, String domain, String domainType, String usercode, String username, String tokenPrefix, String token, String tokenType, String sassyTokenType, String shortUrl, String shortPassage, String redirectType, Long flashDuration, String accessType, String viewType, String shareType, Boolean readOnly, String displayMessage, String shortMessage, Boolean keywordEnabled, Boolean bookmarkEnabled, ReferrerInfoStruct referrerInfo, String status, String note, Long expirationTime) throws BaseException;
    //Boolean updateShortLink(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateShortLink(ShortLink shortLink) throws BaseException;
    Boolean deleteShortLink(String guid) throws BaseException;
    Boolean deleteShortLink(ShortLink shortLink) throws BaseException;
    Long deleteShortLinks(String filter, String params, List<String> values) throws BaseException;

//    Integer createShortLinks(List<ShortLink> shortLinks) throws BaseException;
//    Boolean updateeShortLinks(List<ShortLink> shortLinks) throws BaseException;

}
