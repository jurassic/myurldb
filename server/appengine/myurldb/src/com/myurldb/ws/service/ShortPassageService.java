package com.myurldb.ws.service;

import java.util.List;

import com.myurldb.ws.ShortPassageAttribute;
import com.myurldb.ws.ShortPassage;
import com.myurldb.ws.BaseException;

// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface ShortPassageService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    ShortPassage getShortPassage(String guid) throws BaseException;
    Object getShortPassage(String guid, String field) throws BaseException;
    List<ShortPassage> getShortPassages(List<String> guids) throws BaseException;
    List<ShortPassage> getAllShortPassages() throws BaseException;
    List<ShortPassage> getAllShortPassages(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllShortPassageKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<ShortPassage> findShortPassages(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<ShortPassage> findShortPassages(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findShortPassageKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createShortPassage(String owner, String longText, String shortText, ShortPassageAttribute attribute, Boolean readOnly, String status, String note, Long expirationTime) throws BaseException;
    //String createShortPassage(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return ShortPassage?)
    String createShortPassage(ShortPassage shortPassage) throws BaseException;          // Returns Guid.  (Return ShortPassage?)
    Boolean updateShortPassage(String guid, String owner, String longText, String shortText, ShortPassageAttribute attribute, Boolean readOnly, String status, String note, Long expirationTime) throws BaseException;
    //Boolean updateShortPassage(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateShortPassage(ShortPassage shortPassage) throws BaseException;
    Boolean deleteShortPassage(String guid) throws BaseException;
    Boolean deleteShortPassage(ShortPassage shortPassage) throws BaseException;
    Long deleteShortPassages(String filter, String params, List<String> values) throws BaseException;

//    Integer createShortPassages(List<ShortPassage> shortPassages) throws BaseException;
//    Boolean updateeShortPassages(List<ShortPassage> shortPassages) throws BaseException;

}
