package com.myurldb.ws.service;

import java.util.List;

import com.myurldb.ws.SiteCustomDomain;
import com.myurldb.ws.BaseException;

// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface SiteCustomDomainService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    SiteCustomDomain getSiteCustomDomain(String guid) throws BaseException;
    Object getSiteCustomDomain(String guid, String field) throws BaseException;
    List<SiteCustomDomain> getSiteCustomDomains(List<String> guids) throws BaseException;
    List<SiteCustomDomain> getAllSiteCustomDomains() throws BaseException;
    List<SiteCustomDomain> getAllSiteCustomDomains(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllSiteCustomDomainKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<SiteCustomDomain> findSiteCustomDomains(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<SiteCustomDomain> findSiteCustomDomains(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findSiteCustomDomainKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createSiteCustomDomain(String siteDomain, String domain, String status) throws BaseException;
    //String createSiteCustomDomain(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return SiteCustomDomain?)
    String createSiteCustomDomain(SiteCustomDomain siteCustomDomain) throws BaseException;          // Returns Guid.  (Return SiteCustomDomain?)
    Boolean updateSiteCustomDomain(String guid, String siteDomain, String domain, String status) throws BaseException;
    //Boolean updateSiteCustomDomain(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateSiteCustomDomain(SiteCustomDomain siteCustomDomain) throws BaseException;
    Boolean deleteSiteCustomDomain(String guid) throws BaseException;
    Boolean deleteSiteCustomDomain(SiteCustomDomain siteCustomDomain) throws BaseException;
    Long deleteSiteCustomDomains(String filter, String params, List<String> values) throws BaseException;

//    Integer createSiteCustomDomains(List<SiteCustomDomain> siteCustomDomains) throws BaseException;
//    Boolean updateeSiteCustomDomains(List<SiteCustomDomain> siteCustomDomains) throws BaseException;

}
