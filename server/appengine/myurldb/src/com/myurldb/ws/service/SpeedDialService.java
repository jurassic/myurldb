package com.myurldb.ws.service;

import java.util.List;

import com.myurldb.ws.SpeedDial;
import com.myurldb.ws.BaseException;

// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface SpeedDialService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    SpeedDial getSpeedDial(String guid) throws BaseException;
    Object getSpeedDial(String guid, String field) throws BaseException;
    List<SpeedDial> getSpeedDials(List<String> guids) throws BaseException;
    List<SpeedDial> getAllSpeedDials() throws BaseException;
    List<SpeedDial> getAllSpeedDials(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllSpeedDialKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<SpeedDial> findSpeedDials(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<SpeedDial> findSpeedDials(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findSpeedDialKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createSpeedDial(String user, String code, String shortcut, String shortLink, String keywordLink, String bookmarkLink, String longUrl, String shortUrl, Boolean caseSensitive, String status, String note) throws BaseException;
    //String createSpeedDial(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return SpeedDial?)
    String createSpeedDial(SpeedDial speedDial) throws BaseException;          // Returns Guid.  (Return SpeedDial?)
    Boolean updateSpeedDial(String guid, String user, String code, String shortcut, String shortLink, String keywordLink, String bookmarkLink, String longUrl, String shortUrl, Boolean caseSensitive, String status, String note) throws BaseException;
    //Boolean updateSpeedDial(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateSpeedDial(SpeedDial speedDial) throws BaseException;
    Boolean deleteSpeedDial(String guid) throws BaseException;
    Boolean deleteSpeedDial(SpeedDial speedDial) throws BaseException;
    Long deleteSpeedDials(String filter, String params, List<String> values) throws BaseException;

//    Integer createSpeedDials(List<SpeedDial> speedDials) throws BaseException;
//    Boolean updateeSpeedDials(List<SpeedDial> speedDials) throws BaseException;

}
