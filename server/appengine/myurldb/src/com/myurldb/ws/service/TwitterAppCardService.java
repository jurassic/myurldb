package com.myurldb.ws.service;

import java.util.List;

import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.TwitterAppCard;
import com.myurldb.ws.BaseException;

// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface TwitterAppCardService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    TwitterAppCard getTwitterAppCard(String guid) throws BaseException;
    Object getTwitterAppCard(String guid, String field) throws BaseException;
    List<TwitterAppCard> getTwitterAppCards(List<String> guids) throws BaseException;
    List<TwitterAppCard> getAllTwitterAppCards() throws BaseException;
    List<TwitterAppCard> getAllTwitterAppCards(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllTwitterAppCardKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<TwitterAppCard> findTwitterAppCards(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<TwitterAppCard> findTwitterAppCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findTwitterAppCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createTwitterAppCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, TwitterCardAppInfo iphoneApp, TwitterCardAppInfo ipadApp, TwitterCardAppInfo googlePlayApp) throws BaseException;
    //String createTwitterAppCard(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return TwitterAppCard?)
    String createTwitterAppCard(TwitterAppCard twitterAppCard) throws BaseException;          // Returns Guid.  (Return TwitterAppCard?)
    Boolean updateTwitterAppCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, TwitterCardAppInfo iphoneApp, TwitterCardAppInfo ipadApp, TwitterCardAppInfo googlePlayApp) throws BaseException;
    //Boolean updateTwitterAppCard(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateTwitterAppCard(TwitterAppCard twitterAppCard) throws BaseException;
    Boolean deleteTwitterAppCard(String guid) throws BaseException;
    Boolean deleteTwitterAppCard(TwitterAppCard twitterAppCard) throws BaseException;
    Long deleteTwitterAppCards(String filter, String params, List<String> values) throws BaseException;

//    Integer createTwitterAppCards(List<TwitterAppCard> twitterAppCards) throws BaseException;
//    Boolean updateeTwitterAppCards(List<TwitterAppCard> twitterAppCards) throws BaseException;

}
