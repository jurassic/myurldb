package com.myurldb.ws.service;

import java.util.List;

import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.TwitterGalleryCard;
import com.myurldb.ws.BaseException;

// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface TwitterGalleryCardService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    TwitterGalleryCard getTwitterGalleryCard(String guid) throws BaseException;
    Object getTwitterGalleryCard(String guid, String field) throws BaseException;
    List<TwitterGalleryCard> getTwitterGalleryCards(List<String> guids) throws BaseException;
    List<TwitterGalleryCard> getAllTwitterGalleryCards() throws BaseException;
    List<TwitterGalleryCard> getAllTwitterGalleryCards(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllTwitterGalleryCardKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<TwitterGalleryCard> findTwitterGalleryCards(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<TwitterGalleryCard> findTwitterGalleryCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findTwitterGalleryCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createTwitterGalleryCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image0, String image1, String image2, String image3) throws BaseException;
    //String createTwitterGalleryCard(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return TwitterGalleryCard?)
    String createTwitterGalleryCard(TwitterGalleryCard twitterGalleryCard) throws BaseException;          // Returns Guid.  (Return TwitterGalleryCard?)
    Boolean updateTwitterGalleryCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image0, String image1, String image2, String image3) throws BaseException;
    //Boolean updateTwitterGalleryCard(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateTwitterGalleryCard(TwitterGalleryCard twitterGalleryCard) throws BaseException;
    Boolean deleteTwitterGalleryCard(String guid) throws BaseException;
    Boolean deleteTwitterGalleryCard(TwitterGalleryCard twitterGalleryCard) throws BaseException;
    Long deleteTwitterGalleryCards(String filter, String params, List<String> values) throws BaseException;

//    Integer createTwitterGalleryCards(List<TwitterGalleryCard> twitterGalleryCards) throws BaseException;
//    Boolean updateeTwitterGalleryCards(List<TwitterGalleryCard> twitterGalleryCards) throws BaseException;

}
