package com.myurldb.ws.service;

import java.util.List;

import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.TwitterPlayerCard;
import com.myurldb.ws.BaseException;

// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface TwitterPlayerCardService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    TwitterPlayerCard getTwitterPlayerCard(String guid) throws BaseException;
    Object getTwitterPlayerCard(String guid, String field) throws BaseException;
    List<TwitterPlayerCard> getTwitterPlayerCards(List<String> guids) throws BaseException;
    List<TwitterPlayerCard> getAllTwitterPlayerCards() throws BaseException;
    List<TwitterPlayerCard> getAllTwitterPlayerCards(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllTwitterPlayerCardKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<TwitterPlayerCard> findTwitterPlayerCards(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<TwitterPlayerCard> findTwitterPlayerCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findTwitterPlayerCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createTwitterPlayerCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, String player, Integer playerWidth, Integer playerHeight, String playerStream, String playerStreamContentType) throws BaseException;
    //String createTwitterPlayerCard(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return TwitterPlayerCard?)
    String createTwitterPlayerCard(TwitterPlayerCard twitterPlayerCard) throws BaseException;          // Returns Guid.  (Return TwitterPlayerCard?)
    Boolean updateTwitterPlayerCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, String player, Integer playerWidth, Integer playerHeight, String playerStream, String playerStreamContentType) throws BaseException;
    //Boolean updateTwitterPlayerCard(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateTwitterPlayerCard(TwitterPlayerCard twitterPlayerCard) throws BaseException;
    Boolean deleteTwitterPlayerCard(String guid) throws BaseException;
    Boolean deleteTwitterPlayerCard(TwitterPlayerCard twitterPlayerCard) throws BaseException;
    Long deleteTwitterPlayerCards(String filter, String params, List<String> values) throws BaseException;

//    Integer createTwitterPlayerCards(List<TwitterPlayerCard> twitterPlayerCards) throws BaseException;
//    Boolean updateeTwitterPlayerCards(List<TwitterPlayerCard> twitterPlayerCards) throws BaseException;

}
