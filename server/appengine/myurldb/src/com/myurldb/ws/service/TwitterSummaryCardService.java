package com.myurldb.ws.service;

import java.util.List;

import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.TwitterSummaryCard;
import com.myurldb.ws.BaseException;

// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface TwitterSummaryCardService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    TwitterSummaryCard getTwitterSummaryCard(String guid) throws BaseException;
    Object getTwitterSummaryCard(String guid, String field) throws BaseException;
    List<TwitterSummaryCard> getTwitterSummaryCards(List<String> guids) throws BaseException;
    List<TwitterSummaryCard> getAllTwitterSummaryCards() throws BaseException;
    List<TwitterSummaryCard> getAllTwitterSummaryCards(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllTwitterSummaryCardKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<TwitterSummaryCard> findTwitterSummaryCards(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<TwitterSummaryCard> findTwitterSummaryCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findTwitterSummaryCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createTwitterSummaryCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight) throws BaseException;
    //String createTwitterSummaryCard(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return TwitterSummaryCard?)
    String createTwitterSummaryCard(TwitterSummaryCard twitterSummaryCard) throws BaseException;          // Returns Guid.  (Return TwitterSummaryCard?)
    Boolean updateTwitterSummaryCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight) throws BaseException;
    //Boolean updateTwitterSummaryCard(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateTwitterSummaryCard(TwitterSummaryCard twitterSummaryCard) throws BaseException;
    Boolean deleteTwitterSummaryCard(String guid) throws BaseException;
    Boolean deleteTwitterSummaryCard(TwitterSummaryCard twitterSummaryCard) throws BaseException;
    Long deleteTwitterSummaryCards(String filter, String params, List<String> values) throws BaseException;

//    Integer createTwitterSummaryCards(List<TwitterSummaryCard> twitterSummaryCards) throws BaseException;
//    Boolean updateeTwitterSummaryCards(List<TwitterSummaryCard> twitterSummaryCards) throws BaseException;

}
