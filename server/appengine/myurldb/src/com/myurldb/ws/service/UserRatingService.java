package com.myurldb.ws.service;

import java.util.List;

import com.myurldb.ws.UserRating;
import com.myurldb.ws.BaseException;

// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface UserRatingService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    UserRating getUserRating(String guid) throws BaseException;
    Object getUserRating(String guid, String field) throws BaseException;
    List<UserRating> getUserRatings(List<String> guids) throws BaseException;
    List<UserRating> getAllUserRatings() throws BaseException;
    List<UserRating> getAllUserRatings(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllUserRatingKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<UserRating> findUserRatings(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<UserRating> findUserRatings(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findUserRatingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createUserRating(String user, Double rating, String note, Long ratedTime) throws BaseException;
    //String createUserRating(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return UserRating?)
    String createUserRating(UserRating userRating) throws BaseException;          // Returns Guid.  (Return UserRating?)
    Boolean updateUserRating(String guid, String user, Double rating, String note, Long ratedTime) throws BaseException;
    //Boolean updateUserRating(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateUserRating(UserRating userRating) throws BaseException;
    Boolean deleteUserRating(String guid) throws BaseException;
    Boolean deleteUserRating(UserRating userRating) throws BaseException;
    Long deleteUserRatings(String filter, String params, List<String> values) throws BaseException;

//    Integer createUserRatings(List<UserRating> userRatings) throws BaseException;
//    Boolean updateeUserRatings(List<UserRating> userRatings) throws BaseException;

}
