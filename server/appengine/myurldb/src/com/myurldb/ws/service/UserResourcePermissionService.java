package com.myurldb.ws.service;

import java.util.List;

import com.myurldb.ws.UserResourcePermission;
import com.myurldb.ws.BaseException;

// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface UserResourcePermissionService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    UserResourcePermission getUserResourcePermission(String guid) throws BaseException;
    Object getUserResourcePermission(String guid, String field) throws BaseException;
    List<UserResourcePermission> getUserResourcePermissions(List<String> guids) throws BaseException;
    List<UserResourcePermission> getAllUserResourcePermissions() throws BaseException;
    List<UserResourcePermission> getAllUserResourcePermissions(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllUserResourcePermissionKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<UserResourcePermission> findUserResourcePermissions(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<UserResourcePermission> findUserResourcePermissions(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findUserResourcePermissionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createUserResourcePermission(String user, String permissionName, String resource, String instance, String action, Boolean permitted, String status) throws BaseException;
    //String createUserResourcePermission(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return UserResourcePermission?)
    String createUserResourcePermission(UserResourcePermission userResourcePermission) throws BaseException;          // Returns Guid.  (Return UserResourcePermission?)
    Boolean updateUserResourcePermission(String guid, String user, String permissionName, String resource, String instance, String action, Boolean permitted, String status) throws BaseException;
    //Boolean updateUserResourcePermission(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateUserResourcePermission(UserResourcePermission userResourcePermission) throws BaseException;
    Boolean deleteUserResourcePermission(String guid) throws BaseException;
    Boolean deleteUserResourcePermission(UserResourcePermission userResourcePermission) throws BaseException;
    Long deleteUserResourcePermissions(String filter, String params, List<String> values) throws BaseException;

//    Integer createUserResourcePermissions(List<UserResourcePermission> userResourcePermissions) throws BaseException;
//    Boolean updateeUserResourcePermissions(List<UserResourcePermission> userResourcePermissions) throws BaseException;

}
