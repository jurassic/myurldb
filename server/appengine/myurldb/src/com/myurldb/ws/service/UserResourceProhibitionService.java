package com.myurldb.ws.service;

import java.util.List;

import com.myurldb.ws.UserResourceProhibition;
import com.myurldb.ws.BaseException;

// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface UserResourceProhibitionService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    UserResourceProhibition getUserResourceProhibition(String guid) throws BaseException;
    Object getUserResourceProhibition(String guid, String field) throws BaseException;
    List<UserResourceProhibition> getUserResourceProhibitions(List<String> guids) throws BaseException;
    List<UserResourceProhibition> getAllUserResourceProhibitions() throws BaseException;
    List<UserResourceProhibition> getAllUserResourceProhibitions(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllUserResourceProhibitionKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<UserResourceProhibition> findUserResourceProhibitions(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<UserResourceProhibition> findUserResourceProhibitions(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findUserResourceProhibitionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createUserResourceProhibition(String user, String permissionName, String resource, String instance, String action, Boolean prohibited, String status) throws BaseException;
    //String createUserResourceProhibition(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return UserResourceProhibition?)
    String createUserResourceProhibition(UserResourceProhibition userResourceProhibition) throws BaseException;          // Returns Guid.  (Return UserResourceProhibition?)
    Boolean updateUserResourceProhibition(String guid, String user, String permissionName, String resource, String instance, String action, Boolean prohibited, String status) throws BaseException;
    //Boolean updateUserResourceProhibition(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateUserResourceProhibition(UserResourceProhibition userResourceProhibition) throws BaseException;
    Boolean deleteUserResourceProhibition(String guid) throws BaseException;
    Boolean deleteUserResourceProhibition(UserResourceProhibition userResourceProhibition) throws BaseException;
    Long deleteUserResourceProhibitions(String filter, String params, List<String> values) throws BaseException;

//    Integer createUserResourceProhibitions(List<UserResourceProhibition> userResourceProhibitions) throws BaseException;
//    Boolean updateeUserResourceProhibitions(List<UserResourceProhibition> userResourceProhibitions) throws BaseException;

}
