package com.myurldb.ws.service;

import java.util.List;

import com.myurldb.ws.UserSetting;
import com.myurldb.ws.BaseException;

// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface UserSettingService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    UserSetting getUserSetting(String guid) throws BaseException;
    Object getUserSetting(String guid, String field) throws BaseException;
    List<UserSetting> getUserSettings(List<String> guids) throws BaseException;
    List<UserSetting> getAllUserSettings() throws BaseException;
    List<UserSetting> getAllUserSettings(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllUserSettingKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<UserSetting> findUserSettings(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<UserSetting> findUserSettings(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findUserSettingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createUserSetting(String user, String homePage, String selfBio, String userUrlDomain, String userUrlDomainNormalized, Boolean autoRedirectEnabled, Boolean viewEnabled, Boolean shareEnabled, String defaultDomain, String defaultTokenType, String defaultRedirectType, Long defaultFlashDuration, String defaultAccessType, String defaultViewType, String defaultShareType, String defaultPassphrase, String defaultSignature, Boolean useSignature, String defaultEmblem, Boolean useEmblem, String defaultBackground, Boolean useBackground, String sponsorUrl, String sponsorBanner, String sponsorNote, Boolean useSponsor, String extraParams, Long expirationDuration) throws BaseException;
    //String createUserSetting(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return UserSetting?)
    String createUserSetting(UserSetting userSetting) throws BaseException;          // Returns Guid.  (Return UserSetting?)
    Boolean updateUserSetting(String guid, String user, String homePage, String selfBio, String userUrlDomain, String userUrlDomainNormalized, Boolean autoRedirectEnabled, Boolean viewEnabled, Boolean shareEnabled, String defaultDomain, String defaultTokenType, String defaultRedirectType, Long defaultFlashDuration, String defaultAccessType, String defaultViewType, String defaultShareType, String defaultPassphrase, String defaultSignature, Boolean useSignature, String defaultEmblem, Boolean useEmblem, String defaultBackground, Boolean useBackground, String sponsorUrl, String sponsorBanner, String sponsorNote, Boolean useSponsor, String extraParams, Long expirationDuration) throws BaseException;
    //Boolean updateUserSetting(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateUserSetting(UserSetting userSetting) throws BaseException;
    Boolean deleteUserSetting(String guid) throws BaseException;
    Boolean deleteUserSetting(UserSetting userSetting) throws BaseException;
    Long deleteUserSettings(String filter, String params, List<String> values) throws BaseException;

//    Integer createUserSettings(List<UserSetting> userSettings) throws BaseException;
//    Boolean updateeUserSettings(List<UserSetting> userSettings) throws BaseException;

}
