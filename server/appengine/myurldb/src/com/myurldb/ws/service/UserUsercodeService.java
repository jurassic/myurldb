package com.myurldb.ws.service;

import java.util.List;

import com.myurldb.ws.UserUsercode;
import com.myurldb.ws.BaseException;

// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface UserUsercodeService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    UserUsercode getUserUsercode(String guid) throws BaseException;
    Object getUserUsercode(String guid, String field) throws BaseException;
    List<UserUsercode> getUserUsercodes(List<String> guids) throws BaseException;
    List<UserUsercode> getAllUserUsercodes() throws BaseException;
    List<UserUsercode> getAllUserUsercodes(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllUserUsercodeKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<UserUsercode> findUserUsercodes(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<UserUsercode> findUserUsercodes(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findUserUsercodeKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createUserUsercode(String admin, String user, String username, String usercode, String status) throws BaseException;
    //String createUserUsercode(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return UserUsercode?)
    String createUserUsercode(UserUsercode userUsercode) throws BaseException;          // Returns Guid.  (Return UserUsercode?)
    Boolean updateUserUsercode(String guid, String admin, String user, String username, String usercode, String status) throws BaseException;
    //Boolean updateUserUsercode(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateUserUsercode(UserUsercode userUsercode) throws BaseException;
    Boolean deleteUserUsercode(String guid) throws BaseException;
    Boolean deleteUserUsercode(UserUsercode userUsercode) throws BaseException;
    Long deleteUserUsercodes(String filter, String params, List<String> values) throws BaseException;

//    Integer createUserUsercodes(List<UserUsercode> userUsercodes) throws BaseException;
//    Boolean updateeUserUsercodes(List<UserUsercode> userUsercodes) throws BaseException;

}
