package com.myurldb.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.AbuseTag;
import com.myurldb.ws.bean.AbuseTagBean;
import com.myurldb.ws.dao.DAOFactory;
import com.myurldb.ws.data.AbuseTagDataObject;
import com.myurldb.ws.service.DAOFactoryManager;
import com.myurldb.ws.service.AbuseTagService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class AbuseTagServiceImpl implements AbuseTagService
{
    private static final Logger log = Logger.getLogger(AbuseTagServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // AbuseTag related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public AbuseTag getAbuseTag(String guid) throws BaseException
    {
        log.finer("BEGIN");

        AbuseTagDataObject dataObj = getDAOFactory().getAbuseTagDAO().getAbuseTag(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve AbuseTagDataObject for guid = " + guid);
            return null;  // ????
        }
        AbuseTagBean bean = new AbuseTagBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getAbuseTag(String guid, String field) throws BaseException
    {
        AbuseTagDataObject dataObj = getDAOFactory().getAbuseTagDAO().getAbuseTag(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve AbuseTagDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("shortLink")) {
            return dataObj.getShortLink();
        } else if(field.equals("shortUrl")) {
            return dataObj.getShortUrl();
        } else if(field.equals("longUrl")) {
            return dataObj.getLongUrl();
        } else if(field.equals("longUrlHash")) {
            return dataObj.getLongUrlHash();
        } else if(field.equals("user")) {
            return dataObj.getUser();
        } else if(field.equals("ipAddress")) {
            return dataObj.getIpAddress();
        } else if(field.equals("tag")) {
            return dataObj.getTag();
        } else if(field.equals("comment")) {
            return dataObj.getComment();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("reviewer")) {
            return dataObj.getReviewer();
        } else if(field.equals("action")) {
            return dataObj.getAction();
        } else if(field.equals("note")) {
            return dataObj.getNote();
        } else if(field.equals("pastActions")) {
            return dataObj.getPastActions();
        } else if(field.equals("reviewedTime")) {
            return dataObj.getReviewedTime();
        } else if(field.equals("actionTime")) {
            return dataObj.getActionTime();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<AbuseTag> getAbuseTags(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<AbuseTag> list = new ArrayList<AbuseTag>();
        List<AbuseTagDataObject> dataObjs = getDAOFactory().getAbuseTagDAO().getAbuseTags(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve AbuseTagDataObject list.");
        } else {
            Iterator<AbuseTagDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                AbuseTagDataObject dataObj = (AbuseTagDataObject) it.next();
                list.add(new AbuseTagBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<AbuseTag> getAllAbuseTags() throws BaseException
    {
        return getAllAbuseTags(null, null, null);
    }

    @Override
    public List<AbuseTag> getAllAbuseTags(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<AbuseTag> list = new ArrayList<AbuseTag>();
        List<AbuseTagDataObject> dataObjs = getDAOFactory().getAbuseTagDAO().getAllAbuseTags(ordering, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve AbuseTagDataObject list.");
        } else {
            Iterator<AbuseTagDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                AbuseTagDataObject dataObj = (AbuseTagDataObject) it.next();
                list.add(new AbuseTagBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllAbuseTagKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        List<String> keys = getDAOFactory().getAbuseTagDAO().getAllAbuseTagKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve AbuseTag key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<AbuseTag> findAbuseTags(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findAbuseTags(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<AbuseTag> findAbuseTags(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("AbuseTagServiceImpl.findAbuseTags(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<AbuseTag> list = new ArrayList<AbuseTag>();
        List<AbuseTagDataObject> dataObjs = getDAOFactory().getAbuseTagDAO().findAbuseTags(filter, ordering, params, values, grouping, unique, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find abuseTags for the given criterion.");
        } else {
            Iterator<AbuseTagDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                AbuseTagDataObject dataObj = (AbuseTagDataObject) it.next();
                list.add(new AbuseTagBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findAbuseTagKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("AbuseTagServiceImpl.findAbuseTagKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = getDAOFactory().getAbuseTagDAO().findAbuseTagKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find AbuseTag keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("AbuseTagServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getAbuseTagDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createAbuseTag(String shortLink, String shortUrl, String longUrl, String longUrlHash, String user, String ipAddress, Integer tag, String comment, String status, String reviewer, String action, String note, String pastActions, Long reviewedTime, Long actionTime) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        AbuseTagDataObject dataObj = new AbuseTagDataObject(null, shortLink, shortUrl, longUrl, longUrlHash, user, ipAddress, tag, comment, status, reviewer, action, note, pastActions, reviewedTime, actionTime);
        return createAbuseTag(dataObj);
    }

    @Override
    public String createAbuseTag(AbuseTag abuseTag) throws BaseException
    {
        log.finer("BEGIN");

        // Param abuseTag cannot be null.....
        if(abuseTag == null) {
            log.log(Level.INFO, "Param abuseTag is null!");
            throw new BadRequestException("Param abuseTag object is null!");
        }
        AbuseTagDataObject dataObj = null;
        if(abuseTag instanceof AbuseTagDataObject) {
            dataObj = (AbuseTagDataObject) abuseTag;
        } else if(abuseTag instanceof AbuseTagBean) {
            dataObj = ((AbuseTagBean) abuseTag).toDataObject();
        } else {  // if(abuseTag instanceof AbuseTag)
            //dataObj = new AbuseTagDataObject(null, abuseTag.getShortLink(), abuseTag.getShortUrl(), abuseTag.getLongUrl(), abuseTag.getLongUrlHash(), abuseTag.getUser(), abuseTag.getIpAddress(), abuseTag.getTag(), abuseTag.getComment(), abuseTag.getStatus(), abuseTag.getReviewer(), abuseTag.getAction(), abuseTag.getNote(), abuseTag.getPastActions(), abuseTag.getReviewedTime(), abuseTag.getActionTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new AbuseTagDataObject(abuseTag.getGuid(), abuseTag.getShortLink(), abuseTag.getShortUrl(), abuseTag.getLongUrl(), abuseTag.getLongUrlHash(), abuseTag.getUser(), abuseTag.getIpAddress(), abuseTag.getTag(), abuseTag.getComment(), abuseTag.getStatus(), abuseTag.getReviewer(), abuseTag.getAction(), abuseTag.getNote(), abuseTag.getPastActions(), abuseTag.getReviewedTime(), abuseTag.getActionTime());
        }
        String guid = getDAOFactory().getAbuseTagDAO().createAbuseTag(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateAbuseTag(String guid, String shortLink, String shortUrl, String longUrl, String longUrlHash, String user, String ipAddress, Integer tag, String comment, String status, String reviewer, String action, String note, String pastActions, Long reviewedTime, Long actionTime) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        AbuseTagDataObject dataObj = new AbuseTagDataObject(guid, shortLink, shortUrl, longUrl, longUrlHash, user, ipAddress, tag, comment, status, reviewer, action, note, pastActions, reviewedTime, actionTime);
        return updateAbuseTag(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateAbuseTag(AbuseTag abuseTag) throws BaseException
    {
        log.finer("BEGIN");

        // Param abuseTag cannot be null.....
        if(abuseTag == null || abuseTag.getGuid() == null) {
            log.log(Level.INFO, "Param abuseTag or its guid is null!");
            throw new BadRequestException("Param abuseTag object or its guid is null!");
        }
        AbuseTagDataObject dataObj = null;
        if(abuseTag instanceof AbuseTagDataObject) {
            dataObj = (AbuseTagDataObject) abuseTag;
        } else if(abuseTag instanceof AbuseTagBean) {
            dataObj = ((AbuseTagBean) abuseTag).toDataObject();
        } else {  // if(abuseTag instanceof AbuseTag)
            dataObj = new AbuseTagDataObject(abuseTag.getGuid(), abuseTag.getShortLink(), abuseTag.getShortUrl(), abuseTag.getLongUrl(), abuseTag.getLongUrlHash(), abuseTag.getUser(), abuseTag.getIpAddress(), abuseTag.getTag(), abuseTag.getComment(), abuseTag.getStatus(), abuseTag.getReviewer(), abuseTag.getAction(), abuseTag.getNote(), abuseTag.getPastActions(), abuseTag.getReviewedTime(), abuseTag.getActionTime());
        }
        Boolean suc = getDAOFactory().getAbuseTagDAO().updateAbuseTag(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteAbuseTag(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getAbuseTagDAO().deleteAbuseTag(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteAbuseTag(AbuseTag abuseTag) throws BaseException
    {
        log.finer("BEGIN");

        // Param abuseTag cannot be null.....
        if(abuseTag == null || abuseTag.getGuid() == null) {
            log.log(Level.INFO, "Param abuseTag or its guid is null!");
            throw new BadRequestException("Param abuseTag object or its guid is null!");
        }
        AbuseTagDataObject dataObj = null;
        if(abuseTag instanceof AbuseTagDataObject) {
            dataObj = (AbuseTagDataObject) abuseTag;
        } else if(abuseTag instanceof AbuseTagBean) {
            dataObj = ((AbuseTagBean) abuseTag).toDataObject();
        } else {  // if(abuseTag instanceof AbuseTag)
            dataObj = new AbuseTagDataObject(abuseTag.getGuid(), abuseTag.getShortLink(), abuseTag.getShortUrl(), abuseTag.getLongUrl(), abuseTag.getLongUrlHash(), abuseTag.getUser(), abuseTag.getIpAddress(), abuseTag.getTag(), abuseTag.getComment(), abuseTag.getStatus(), abuseTag.getReviewer(), abuseTag.getAction(), abuseTag.getNote(), abuseTag.getPastActions(), abuseTag.getReviewedTime(), abuseTag.getActionTime());
        }
        Boolean suc = getDAOFactory().getAbuseTagDAO().deleteAbuseTag(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteAbuseTags(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getAbuseTagDAO().deleteAbuseTags(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
