package com.myurldb.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.AlbumShortLink;
import com.myurldb.ws.bean.AlbumShortLinkBean;
import com.myurldb.ws.dao.DAOFactory;
import com.myurldb.ws.data.AlbumShortLinkDataObject;
import com.myurldb.ws.service.DAOFactoryManager;
import com.myurldb.ws.service.AlbumShortLinkService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class AlbumShortLinkServiceImpl implements AlbumShortLinkService
{
    private static final Logger log = Logger.getLogger(AlbumShortLinkServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // AlbumShortLink related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public AlbumShortLink getAlbumShortLink(String guid) throws BaseException
    {
        log.finer("BEGIN");

        AlbumShortLinkDataObject dataObj = getDAOFactory().getAlbumShortLinkDAO().getAlbumShortLink(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve AlbumShortLinkDataObject for guid = " + guid);
            return null;  // ????
        }
        AlbumShortLinkBean bean = new AlbumShortLinkBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getAlbumShortLink(String guid, String field) throws BaseException
    {
        AlbumShortLinkDataObject dataObj = getDAOFactory().getAlbumShortLinkDAO().getAlbumShortLink(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve AlbumShortLinkDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return dataObj.getUser();
        } else if(field.equals("linkAlbum")) {
            return dataObj.getLinkAlbum();
        } else if(field.equals("shortLink")) {
            return dataObj.getShortLink();
        } else if(field.equals("shortUrl")) {
            return dataObj.getShortUrl();
        } else if(field.equals("longUrl")) {
            return dataObj.getLongUrl();
        } else if(field.equals("note")) {
            return dataObj.getNote();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<AlbumShortLink> getAlbumShortLinks(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<AlbumShortLink> list = new ArrayList<AlbumShortLink>();
        List<AlbumShortLinkDataObject> dataObjs = getDAOFactory().getAlbumShortLinkDAO().getAlbumShortLinks(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve AlbumShortLinkDataObject list.");
        } else {
            Iterator<AlbumShortLinkDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                AlbumShortLinkDataObject dataObj = (AlbumShortLinkDataObject) it.next();
                list.add(new AlbumShortLinkBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<AlbumShortLink> getAllAlbumShortLinks() throws BaseException
    {
        return getAllAlbumShortLinks(null, null, null);
    }

    @Override
    public List<AlbumShortLink> getAllAlbumShortLinks(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<AlbumShortLink> list = new ArrayList<AlbumShortLink>();
        List<AlbumShortLinkDataObject> dataObjs = getDAOFactory().getAlbumShortLinkDAO().getAllAlbumShortLinks(ordering, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve AlbumShortLinkDataObject list.");
        } else {
            Iterator<AlbumShortLinkDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                AlbumShortLinkDataObject dataObj = (AlbumShortLinkDataObject) it.next();
                list.add(new AlbumShortLinkBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllAlbumShortLinkKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        List<String> keys = getDAOFactory().getAlbumShortLinkDAO().getAllAlbumShortLinkKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve AlbumShortLink key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<AlbumShortLink> findAlbumShortLinks(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findAlbumShortLinks(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<AlbumShortLink> findAlbumShortLinks(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("AlbumShortLinkServiceImpl.findAlbumShortLinks(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<AlbumShortLink> list = new ArrayList<AlbumShortLink>();
        List<AlbumShortLinkDataObject> dataObjs = getDAOFactory().getAlbumShortLinkDAO().findAlbumShortLinks(filter, ordering, params, values, grouping, unique, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find albumShortLinks for the given criterion.");
        } else {
            Iterator<AlbumShortLinkDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                AlbumShortLinkDataObject dataObj = (AlbumShortLinkDataObject) it.next();
                list.add(new AlbumShortLinkBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findAlbumShortLinkKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("AlbumShortLinkServiceImpl.findAlbumShortLinkKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = getDAOFactory().getAlbumShortLinkDAO().findAlbumShortLinkKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find AlbumShortLink keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("AlbumShortLinkServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getAlbumShortLinkDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createAlbumShortLink(String user, String linkAlbum, String shortLink, String shortUrl, String longUrl, String note, String status) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        AlbumShortLinkDataObject dataObj = new AlbumShortLinkDataObject(null, user, linkAlbum, shortLink, shortUrl, longUrl, note, status);
        return createAlbumShortLink(dataObj);
    }

    @Override
    public String createAlbumShortLink(AlbumShortLink albumShortLink) throws BaseException
    {
        log.finer("BEGIN");

        // Param albumShortLink cannot be null.....
        if(albumShortLink == null) {
            log.log(Level.INFO, "Param albumShortLink is null!");
            throw new BadRequestException("Param albumShortLink object is null!");
        }
        AlbumShortLinkDataObject dataObj = null;
        if(albumShortLink instanceof AlbumShortLinkDataObject) {
            dataObj = (AlbumShortLinkDataObject) albumShortLink;
        } else if(albumShortLink instanceof AlbumShortLinkBean) {
            dataObj = ((AlbumShortLinkBean) albumShortLink).toDataObject();
        } else {  // if(albumShortLink instanceof AlbumShortLink)
            //dataObj = new AlbumShortLinkDataObject(null, albumShortLink.getUser(), albumShortLink.getLinkAlbum(), albumShortLink.getShortLink(), albumShortLink.getShortUrl(), albumShortLink.getLongUrl(), albumShortLink.getNote(), albumShortLink.getStatus());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new AlbumShortLinkDataObject(albumShortLink.getGuid(), albumShortLink.getUser(), albumShortLink.getLinkAlbum(), albumShortLink.getShortLink(), albumShortLink.getShortUrl(), albumShortLink.getLongUrl(), albumShortLink.getNote(), albumShortLink.getStatus());
        }
        String guid = getDAOFactory().getAlbumShortLinkDAO().createAlbumShortLink(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateAlbumShortLink(String guid, String user, String linkAlbum, String shortLink, String shortUrl, String longUrl, String note, String status) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        AlbumShortLinkDataObject dataObj = new AlbumShortLinkDataObject(guid, user, linkAlbum, shortLink, shortUrl, longUrl, note, status);
        return updateAlbumShortLink(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateAlbumShortLink(AlbumShortLink albumShortLink) throws BaseException
    {
        log.finer("BEGIN");

        // Param albumShortLink cannot be null.....
        if(albumShortLink == null || albumShortLink.getGuid() == null) {
            log.log(Level.INFO, "Param albumShortLink or its guid is null!");
            throw new BadRequestException("Param albumShortLink object or its guid is null!");
        }
        AlbumShortLinkDataObject dataObj = null;
        if(albumShortLink instanceof AlbumShortLinkDataObject) {
            dataObj = (AlbumShortLinkDataObject) albumShortLink;
        } else if(albumShortLink instanceof AlbumShortLinkBean) {
            dataObj = ((AlbumShortLinkBean) albumShortLink).toDataObject();
        } else {  // if(albumShortLink instanceof AlbumShortLink)
            dataObj = new AlbumShortLinkDataObject(albumShortLink.getGuid(), albumShortLink.getUser(), albumShortLink.getLinkAlbum(), albumShortLink.getShortLink(), albumShortLink.getShortUrl(), albumShortLink.getLongUrl(), albumShortLink.getNote(), albumShortLink.getStatus());
        }
        Boolean suc = getDAOFactory().getAlbumShortLinkDAO().updateAlbumShortLink(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteAlbumShortLink(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getAlbumShortLinkDAO().deleteAlbumShortLink(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteAlbumShortLink(AlbumShortLink albumShortLink) throws BaseException
    {
        log.finer("BEGIN");

        // Param albumShortLink cannot be null.....
        if(albumShortLink == null || albumShortLink.getGuid() == null) {
            log.log(Level.INFO, "Param albumShortLink or its guid is null!");
            throw new BadRequestException("Param albumShortLink object or its guid is null!");
        }
        AlbumShortLinkDataObject dataObj = null;
        if(albumShortLink instanceof AlbumShortLinkDataObject) {
            dataObj = (AlbumShortLinkDataObject) albumShortLink;
        } else if(albumShortLink instanceof AlbumShortLinkBean) {
            dataObj = ((AlbumShortLinkBean) albumShortLink).toDataObject();
        } else {  // if(albumShortLink instanceof AlbumShortLink)
            dataObj = new AlbumShortLinkDataObject(albumShortLink.getGuid(), albumShortLink.getUser(), albumShortLink.getLinkAlbum(), albumShortLink.getShortLink(), albumShortLink.getShortUrl(), albumShortLink.getLongUrl(), albumShortLink.getNote(), albumShortLink.getStatus());
        }
        Boolean suc = getDAOFactory().getAlbumShortLinkDAO().deleteAlbumShortLink(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteAlbumShortLinks(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getAlbumShortLinkDAO().deleteAlbumShortLinks(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
