package com.myurldb.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.AppClient;
import com.myurldb.ws.bean.AppClientBean;
import com.myurldb.ws.dao.DAOFactory;
import com.myurldb.ws.data.AppClientDataObject;
import com.myurldb.ws.service.DAOFactoryManager;
import com.myurldb.ws.service.AppClientService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class AppClientServiceImpl implements AppClientService
{
    private static final Logger log = Logger.getLogger(AppClientServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // AppClient related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public AppClient getAppClient(String guid) throws BaseException
    {
        log.finer("BEGIN");

        AppClientDataObject dataObj = getDAOFactory().getAppClientDAO().getAppClient(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve AppClientDataObject for guid = " + guid);
            return null;  // ????
        }
        AppClientBean bean = new AppClientBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getAppClient(String guid, String field) throws BaseException
    {
        AppClientDataObject dataObj = getDAOFactory().getAppClientDAO().getAppClient(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve AppClientDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("owner")) {
            return dataObj.getOwner();
        } else if(field.equals("admin")) {
            return dataObj.getAdmin();
        } else if(field.equals("name")) {
            return dataObj.getName();
        } else if(field.equals("clientId")) {
            return dataObj.getClientId();
        } else if(field.equals("clientCode")) {
            return dataObj.getClientCode();
        } else if(field.equals("rootDomain")) {
            return dataObj.getRootDomain();
        } else if(field.equals("appDomain")) {
            return dataObj.getAppDomain();
        } else if(field.equals("useAppDomain")) {
            return dataObj.isUseAppDomain();
        } else if(field.equals("enabled")) {
            return dataObj.isEnabled();
        } else if(field.equals("licenseMode")) {
            return dataObj.getLicenseMode();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<AppClient> getAppClients(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<AppClient> list = new ArrayList<AppClient>();
        List<AppClientDataObject> dataObjs = getDAOFactory().getAppClientDAO().getAppClients(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve AppClientDataObject list.");
        } else {
            Iterator<AppClientDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                AppClientDataObject dataObj = (AppClientDataObject) it.next();
                list.add(new AppClientBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<AppClient> getAllAppClients() throws BaseException
    {
        return getAllAppClients(null, null, null);
    }

    @Override
    public List<AppClient> getAllAppClients(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<AppClient> list = new ArrayList<AppClient>();
        List<AppClientDataObject> dataObjs = getDAOFactory().getAppClientDAO().getAllAppClients(ordering, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve AppClientDataObject list.");
        } else {
            Iterator<AppClientDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                AppClientDataObject dataObj = (AppClientDataObject) it.next();
                list.add(new AppClientBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllAppClientKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        List<String> keys = getDAOFactory().getAppClientDAO().getAllAppClientKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve AppClient key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<AppClient> findAppClients(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findAppClients(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<AppClient> findAppClients(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("AppClientServiceImpl.findAppClients(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<AppClient> list = new ArrayList<AppClient>();
        List<AppClientDataObject> dataObjs = getDAOFactory().getAppClientDAO().findAppClients(filter, ordering, params, values, grouping, unique, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find appClients for the given criterion.");
        } else {
            Iterator<AppClientDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                AppClientDataObject dataObj = (AppClientDataObject) it.next();
                list.add(new AppClientBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findAppClientKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("AppClientServiceImpl.findAppClientKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = getDAOFactory().getAppClientDAO().findAppClientKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find AppClient keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("AppClientServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getAppClientDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createAppClient(String owner, String admin, String name, String clientId, String clientCode, String rootDomain, String appDomain, Boolean useAppDomain, Boolean enabled, String licenseMode, String status) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        AppClientDataObject dataObj = new AppClientDataObject(null, owner, admin, name, clientId, clientCode, rootDomain, appDomain, useAppDomain, enabled, licenseMode, status);
        return createAppClient(dataObj);
    }

    @Override
    public String createAppClient(AppClient appClient) throws BaseException
    {
        log.finer("BEGIN");

        // Param appClient cannot be null.....
        if(appClient == null) {
            log.log(Level.INFO, "Param appClient is null!");
            throw new BadRequestException("Param appClient object is null!");
        }
        AppClientDataObject dataObj = null;
        if(appClient instanceof AppClientDataObject) {
            dataObj = (AppClientDataObject) appClient;
        } else if(appClient instanceof AppClientBean) {
            dataObj = ((AppClientBean) appClient).toDataObject();
        } else {  // if(appClient instanceof AppClient)
            //dataObj = new AppClientDataObject(null, appClient.getOwner(), appClient.getAdmin(), appClient.getName(), appClient.getClientId(), appClient.getClientCode(), appClient.getRootDomain(), appClient.getAppDomain(), appClient.isUseAppDomain(), appClient.isEnabled(), appClient.getLicenseMode(), appClient.getStatus());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new AppClientDataObject(appClient.getGuid(), appClient.getOwner(), appClient.getAdmin(), appClient.getName(), appClient.getClientId(), appClient.getClientCode(), appClient.getRootDomain(), appClient.getAppDomain(), appClient.isUseAppDomain(), appClient.isEnabled(), appClient.getLicenseMode(), appClient.getStatus());
        }
        String guid = getDAOFactory().getAppClientDAO().createAppClient(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateAppClient(String guid, String owner, String admin, String name, String clientId, String clientCode, String rootDomain, String appDomain, Boolean useAppDomain, Boolean enabled, String licenseMode, String status) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        AppClientDataObject dataObj = new AppClientDataObject(guid, owner, admin, name, clientId, clientCode, rootDomain, appDomain, useAppDomain, enabled, licenseMode, status);
        return updateAppClient(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateAppClient(AppClient appClient) throws BaseException
    {
        log.finer("BEGIN");

        // Param appClient cannot be null.....
        if(appClient == null || appClient.getGuid() == null) {
            log.log(Level.INFO, "Param appClient or its guid is null!");
            throw new BadRequestException("Param appClient object or its guid is null!");
        }
        AppClientDataObject dataObj = null;
        if(appClient instanceof AppClientDataObject) {
            dataObj = (AppClientDataObject) appClient;
        } else if(appClient instanceof AppClientBean) {
            dataObj = ((AppClientBean) appClient).toDataObject();
        } else {  // if(appClient instanceof AppClient)
            dataObj = new AppClientDataObject(appClient.getGuid(), appClient.getOwner(), appClient.getAdmin(), appClient.getName(), appClient.getClientId(), appClient.getClientCode(), appClient.getRootDomain(), appClient.getAppDomain(), appClient.isUseAppDomain(), appClient.isEnabled(), appClient.getLicenseMode(), appClient.getStatus());
        }
        Boolean suc = getDAOFactory().getAppClientDAO().updateAppClient(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteAppClient(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getAppClientDAO().deleteAppClient(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteAppClient(AppClient appClient) throws BaseException
    {
        log.finer("BEGIN");

        // Param appClient cannot be null.....
        if(appClient == null || appClient.getGuid() == null) {
            log.log(Level.INFO, "Param appClient or its guid is null!");
            throw new BadRequestException("Param appClient object or its guid is null!");
        }
        AppClientDataObject dataObj = null;
        if(appClient instanceof AppClientDataObject) {
            dataObj = (AppClientDataObject) appClient;
        } else if(appClient instanceof AppClientBean) {
            dataObj = ((AppClientBean) appClient).toDataObject();
        } else {  // if(appClient instanceof AppClient)
            dataObj = new AppClientDataObject(appClient.getGuid(), appClient.getOwner(), appClient.getAdmin(), appClient.getName(), appClient.getClientId(), appClient.getClientCode(), appClient.getRootDomain(), appClient.getAppDomain(), appClient.isUseAppDomain(), appClient.isEnabled(), appClient.getLicenseMode(), appClient.getStatus());
        }
        Boolean suc = getDAOFactory().getAppClientDAO().deleteAppClient(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteAppClients(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getAppClientDAO().deleteAppClients(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
