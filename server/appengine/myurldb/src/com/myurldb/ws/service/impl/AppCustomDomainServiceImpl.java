package com.myurldb.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.AppCustomDomain;
import com.myurldb.ws.bean.AppCustomDomainBean;
import com.myurldb.ws.dao.DAOFactory;
import com.myurldb.ws.data.AppCustomDomainDataObject;
import com.myurldb.ws.service.DAOFactoryManager;
import com.myurldb.ws.service.AppCustomDomainService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class AppCustomDomainServiceImpl implements AppCustomDomainService
{
    private static final Logger log = Logger.getLogger(AppCustomDomainServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // AppCustomDomain related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public AppCustomDomain getAppCustomDomain(String guid) throws BaseException
    {
        log.finer("BEGIN");

        AppCustomDomainDataObject dataObj = getDAOFactory().getAppCustomDomainDAO().getAppCustomDomain(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve AppCustomDomainDataObject for guid = " + guid);
            return null;  // ????
        }
        AppCustomDomainBean bean = new AppCustomDomainBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getAppCustomDomain(String guid, String field) throws BaseException
    {
        AppCustomDomainDataObject dataObj = getDAOFactory().getAppCustomDomainDAO().getAppCustomDomain(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve AppCustomDomainDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("appId")) {
            return dataObj.getAppId();
        } else if(field.equals("siteDomain")) {
            return dataObj.getSiteDomain();
        } else if(field.equals("domain")) {
            return dataObj.getDomain();
        } else if(field.equals("verified")) {
            return dataObj.isVerified();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("verifiedTime")) {
            return dataObj.getVerifiedTime();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<AppCustomDomain> getAppCustomDomains(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<AppCustomDomain> list = new ArrayList<AppCustomDomain>();
        List<AppCustomDomainDataObject> dataObjs = getDAOFactory().getAppCustomDomainDAO().getAppCustomDomains(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve AppCustomDomainDataObject list.");
        } else {
            Iterator<AppCustomDomainDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                AppCustomDomainDataObject dataObj = (AppCustomDomainDataObject) it.next();
                list.add(new AppCustomDomainBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<AppCustomDomain> getAllAppCustomDomains() throws BaseException
    {
        return getAllAppCustomDomains(null, null, null);
    }

    @Override
    public List<AppCustomDomain> getAllAppCustomDomains(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<AppCustomDomain> list = new ArrayList<AppCustomDomain>();
        List<AppCustomDomainDataObject> dataObjs = getDAOFactory().getAppCustomDomainDAO().getAllAppCustomDomains(ordering, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve AppCustomDomainDataObject list.");
        } else {
            Iterator<AppCustomDomainDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                AppCustomDomainDataObject dataObj = (AppCustomDomainDataObject) it.next();
                list.add(new AppCustomDomainBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllAppCustomDomainKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        List<String> keys = getDAOFactory().getAppCustomDomainDAO().getAllAppCustomDomainKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve AppCustomDomain key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<AppCustomDomain> findAppCustomDomains(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findAppCustomDomains(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<AppCustomDomain> findAppCustomDomains(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("AppCustomDomainServiceImpl.findAppCustomDomains(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<AppCustomDomain> list = new ArrayList<AppCustomDomain>();
        List<AppCustomDomainDataObject> dataObjs = getDAOFactory().getAppCustomDomainDAO().findAppCustomDomains(filter, ordering, params, values, grouping, unique, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find appCustomDomains for the given criterion.");
        } else {
            Iterator<AppCustomDomainDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                AppCustomDomainDataObject dataObj = (AppCustomDomainDataObject) it.next();
                list.add(new AppCustomDomainBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findAppCustomDomainKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("AppCustomDomainServiceImpl.findAppCustomDomainKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = getDAOFactory().getAppCustomDomainDAO().findAppCustomDomainKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find AppCustomDomain keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("AppCustomDomainServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getAppCustomDomainDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createAppCustomDomain(String appId, String siteDomain, String domain, Boolean verified, String status, Long verifiedTime) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        AppCustomDomainDataObject dataObj = new AppCustomDomainDataObject(null, appId, siteDomain, domain, verified, status, verifiedTime);
        return createAppCustomDomain(dataObj);
    }

    @Override
    public String createAppCustomDomain(AppCustomDomain appCustomDomain) throws BaseException
    {
        log.finer("BEGIN");

        // Param appCustomDomain cannot be null.....
        if(appCustomDomain == null) {
            log.log(Level.INFO, "Param appCustomDomain is null!");
            throw new BadRequestException("Param appCustomDomain object is null!");
        }
        AppCustomDomainDataObject dataObj = null;
        if(appCustomDomain instanceof AppCustomDomainDataObject) {
            dataObj = (AppCustomDomainDataObject) appCustomDomain;
        } else if(appCustomDomain instanceof AppCustomDomainBean) {
            dataObj = ((AppCustomDomainBean) appCustomDomain).toDataObject();
        } else {  // if(appCustomDomain instanceof AppCustomDomain)
            //dataObj = new AppCustomDomainDataObject(null, appCustomDomain.getAppId(), appCustomDomain.getSiteDomain(), appCustomDomain.getDomain(), appCustomDomain.isVerified(), appCustomDomain.getStatus(), appCustomDomain.getVerifiedTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new AppCustomDomainDataObject(appCustomDomain.getGuid(), appCustomDomain.getAppId(), appCustomDomain.getSiteDomain(), appCustomDomain.getDomain(), appCustomDomain.isVerified(), appCustomDomain.getStatus(), appCustomDomain.getVerifiedTime());
        }
        String guid = getDAOFactory().getAppCustomDomainDAO().createAppCustomDomain(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateAppCustomDomain(String guid, String appId, String siteDomain, String domain, Boolean verified, String status, Long verifiedTime) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        AppCustomDomainDataObject dataObj = new AppCustomDomainDataObject(guid, appId, siteDomain, domain, verified, status, verifiedTime);
        return updateAppCustomDomain(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateAppCustomDomain(AppCustomDomain appCustomDomain) throws BaseException
    {
        log.finer("BEGIN");

        // Param appCustomDomain cannot be null.....
        if(appCustomDomain == null || appCustomDomain.getGuid() == null) {
            log.log(Level.INFO, "Param appCustomDomain or its guid is null!");
            throw new BadRequestException("Param appCustomDomain object or its guid is null!");
        }
        AppCustomDomainDataObject dataObj = null;
        if(appCustomDomain instanceof AppCustomDomainDataObject) {
            dataObj = (AppCustomDomainDataObject) appCustomDomain;
        } else if(appCustomDomain instanceof AppCustomDomainBean) {
            dataObj = ((AppCustomDomainBean) appCustomDomain).toDataObject();
        } else {  // if(appCustomDomain instanceof AppCustomDomain)
            dataObj = new AppCustomDomainDataObject(appCustomDomain.getGuid(), appCustomDomain.getAppId(), appCustomDomain.getSiteDomain(), appCustomDomain.getDomain(), appCustomDomain.isVerified(), appCustomDomain.getStatus(), appCustomDomain.getVerifiedTime());
        }
        Boolean suc = getDAOFactory().getAppCustomDomainDAO().updateAppCustomDomain(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteAppCustomDomain(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getAppCustomDomainDAO().deleteAppCustomDomain(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteAppCustomDomain(AppCustomDomain appCustomDomain) throws BaseException
    {
        log.finer("BEGIN");

        // Param appCustomDomain cannot be null.....
        if(appCustomDomain == null || appCustomDomain.getGuid() == null) {
            log.log(Level.INFO, "Param appCustomDomain or its guid is null!");
            throw new BadRequestException("Param appCustomDomain object or its guid is null!");
        }
        AppCustomDomainDataObject dataObj = null;
        if(appCustomDomain instanceof AppCustomDomainDataObject) {
            dataObj = (AppCustomDomainDataObject) appCustomDomain;
        } else if(appCustomDomain instanceof AppCustomDomainBean) {
            dataObj = ((AppCustomDomainBean) appCustomDomain).toDataObject();
        } else {  // if(appCustomDomain instanceof AppCustomDomain)
            dataObj = new AppCustomDomainDataObject(appCustomDomain.getGuid(), appCustomDomain.getAppId(), appCustomDomain.getSiteDomain(), appCustomDomain.getDomain(), appCustomDomain.isVerified(), appCustomDomain.getStatus(), appCustomDomain.getVerifiedTime());
        }
        Boolean suc = getDAOFactory().getAppCustomDomainDAO().deleteAppCustomDomain(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteAppCustomDomains(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getAppCustomDomainDAO().deleteAppCustomDomains(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
