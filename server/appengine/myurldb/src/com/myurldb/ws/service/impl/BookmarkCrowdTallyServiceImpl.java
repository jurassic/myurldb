package com.myurldb.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.BookmarkCrowdTally;
import com.myurldb.ws.bean.BookmarkCrowdTallyBean;
import com.myurldb.ws.dao.DAOFactory;
import com.myurldb.ws.data.BookmarkCrowdTallyDataObject;
import com.myurldb.ws.service.DAOFactoryManager;
import com.myurldb.ws.service.BookmarkCrowdTallyService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class BookmarkCrowdTallyServiceImpl implements BookmarkCrowdTallyService
{
    private static final Logger log = Logger.getLogger(BookmarkCrowdTallyServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // BookmarkCrowdTally related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public BookmarkCrowdTally getBookmarkCrowdTally(String guid) throws BaseException
    {
        log.finer("BEGIN");

        BookmarkCrowdTallyDataObject dataObj = getDAOFactory().getBookmarkCrowdTallyDAO().getBookmarkCrowdTally(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve BookmarkCrowdTallyDataObject for guid = " + guid);
            return null;  // ????
        }
        BookmarkCrowdTallyBean bean = new BookmarkCrowdTallyBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getBookmarkCrowdTally(String guid, String field) throws BaseException
    {
        BookmarkCrowdTallyDataObject dataObj = getDAOFactory().getBookmarkCrowdTallyDAO().getBookmarkCrowdTally(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve BookmarkCrowdTallyDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return dataObj.getUser();
        } else if(field.equals("shortLink")) {
            return dataObj.getShortLink();
        } else if(field.equals("domain")) {
            return dataObj.getDomain();
        } else if(field.equals("token")) {
            return dataObj.getToken();
        } else if(field.equals("longUrl")) {
            return dataObj.getLongUrl();
        } else if(field.equals("shortUrl")) {
            return dataObj.getShortUrl();
        } else if(field.equals("tallyDate")) {
            return dataObj.getTallyDate();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("note")) {
            return dataObj.getNote();
        } else if(field.equals("expirationTime")) {
            return dataObj.getExpirationTime();
        } else if(field.equals("bookmarkFolder")) {
            return dataObj.getBookmarkFolder();
        } else if(field.equals("contentTag")) {
            return dataObj.getContentTag();
        } else if(field.equals("referenceElement")) {
            return dataObj.getReferenceElement();
        } else if(field.equals("elementType")) {
            return dataObj.getElementType();
        } else if(field.equals("keywordLink")) {
            return dataObj.getKeywordLink();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<BookmarkCrowdTally> getBookmarkCrowdTallies(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<BookmarkCrowdTally> list = new ArrayList<BookmarkCrowdTally>();
        List<BookmarkCrowdTallyDataObject> dataObjs = getDAOFactory().getBookmarkCrowdTallyDAO().getBookmarkCrowdTallies(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve BookmarkCrowdTallyDataObject list.");
        } else {
            Iterator<BookmarkCrowdTallyDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                BookmarkCrowdTallyDataObject dataObj = (BookmarkCrowdTallyDataObject) it.next();
                list.add(new BookmarkCrowdTallyBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<BookmarkCrowdTally> getAllBookmarkCrowdTallies() throws BaseException
    {
        return getAllBookmarkCrowdTallies(null, null, null);
    }

    @Override
    public List<BookmarkCrowdTally> getAllBookmarkCrowdTallies(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<BookmarkCrowdTally> list = new ArrayList<BookmarkCrowdTally>();
        List<BookmarkCrowdTallyDataObject> dataObjs = getDAOFactory().getBookmarkCrowdTallyDAO().getAllBookmarkCrowdTallies(ordering, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve BookmarkCrowdTallyDataObject list.");
        } else {
            Iterator<BookmarkCrowdTallyDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                BookmarkCrowdTallyDataObject dataObj = (BookmarkCrowdTallyDataObject) it.next();
                list.add(new BookmarkCrowdTallyBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllBookmarkCrowdTallyKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        List<String> keys = getDAOFactory().getBookmarkCrowdTallyDAO().getAllBookmarkCrowdTallyKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve BookmarkCrowdTally key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<BookmarkCrowdTally> findBookmarkCrowdTallies(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findBookmarkCrowdTallies(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<BookmarkCrowdTally> findBookmarkCrowdTallies(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("BookmarkCrowdTallyServiceImpl.findBookmarkCrowdTallies(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<BookmarkCrowdTally> list = new ArrayList<BookmarkCrowdTally>();
        List<BookmarkCrowdTallyDataObject> dataObjs = getDAOFactory().getBookmarkCrowdTallyDAO().findBookmarkCrowdTallies(filter, ordering, params, values, grouping, unique, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find bookmarkCrowdTallies for the given criterion.");
        } else {
            Iterator<BookmarkCrowdTallyDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                BookmarkCrowdTallyDataObject dataObj = (BookmarkCrowdTallyDataObject) it.next();
                list.add(new BookmarkCrowdTallyBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findBookmarkCrowdTallyKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("BookmarkCrowdTallyServiceImpl.findBookmarkCrowdTallyKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = getDAOFactory().getBookmarkCrowdTallyDAO().findBookmarkCrowdTallyKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find BookmarkCrowdTally keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("BookmarkCrowdTallyServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getBookmarkCrowdTallyDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createBookmarkCrowdTally(String user, String shortLink, String domain, String token, String longUrl, String shortUrl, String tallyDate, String status, String note, Long expirationTime, String bookmarkFolder, String contentTag, String referenceElement, String elementType, String keywordLink) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        BookmarkCrowdTallyDataObject dataObj = new BookmarkCrowdTallyDataObject(null, user, shortLink, domain, token, longUrl, shortUrl, tallyDate, status, note, expirationTime, bookmarkFolder, contentTag, referenceElement, elementType, keywordLink);
        return createBookmarkCrowdTally(dataObj);
    }

    @Override
    public String createBookmarkCrowdTally(BookmarkCrowdTally bookmarkCrowdTally) throws BaseException
    {
        log.finer("BEGIN");

        // Param bookmarkCrowdTally cannot be null.....
        if(bookmarkCrowdTally == null) {
            log.log(Level.INFO, "Param bookmarkCrowdTally is null!");
            throw new BadRequestException("Param bookmarkCrowdTally object is null!");
        }
        BookmarkCrowdTallyDataObject dataObj = null;
        if(bookmarkCrowdTally instanceof BookmarkCrowdTallyDataObject) {
            dataObj = (BookmarkCrowdTallyDataObject) bookmarkCrowdTally;
        } else if(bookmarkCrowdTally instanceof BookmarkCrowdTallyBean) {
            dataObj = ((BookmarkCrowdTallyBean) bookmarkCrowdTally).toDataObject();
        } else {  // if(bookmarkCrowdTally instanceof BookmarkCrowdTally)
            //dataObj = new BookmarkCrowdTallyDataObject(null, bookmarkCrowdTally.getUser(), bookmarkCrowdTally.getShortLink(), bookmarkCrowdTally.getDomain(), bookmarkCrowdTally.getToken(), bookmarkCrowdTally.getLongUrl(), bookmarkCrowdTally.getShortUrl(), bookmarkCrowdTally.getTallyDate(), bookmarkCrowdTally.getStatus(), bookmarkCrowdTally.getNote(), bookmarkCrowdTally.getExpirationTime(), bookmarkCrowdTally.getBookmarkFolder(), bookmarkCrowdTally.getContentTag(), bookmarkCrowdTally.getReferenceElement(), bookmarkCrowdTally.getElementType(), bookmarkCrowdTally.getKeywordLink());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new BookmarkCrowdTallyDataObject(bookmarkCrowdTally.getGuid(), bookmarkCrowdTally.getUser(), bookmarkCrowdTally.getShortLink(), bookmarkCrowdTally.getDomain(), bookmarkCrowdTally.getToken(), bookmarkCrowdTally.getLongUrl(), bookmarkCrowdTally.getShortUrl(), bookmarkCrowdTally.getTallyDate(), bookmarkCrowdTally.getStatus(), bookmarkCrowdTally.getNote(), bookmarkCrowdTally.getExpirationTime(), bookmarkCrowdTally.getBookmarkFolder(), bookmarkCrowdTally.getContentTag(), bookmarkCrowdTally.getReferenceElement(), bookmarkCrowdTally.getElementType(), bookmarkCrowdTally.getKeywordLink());
        }
        String guid = getDAOFactory().getBookmarkCrowdTallyDAO().createBookmarkCrowdTally(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateBookmarkCrowdTally(String guid, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, String tallyDate, String status, String note, Long expirationTime, String bookmarkFolder, String contentTag, String referenceElement, String elementType, String keywordLink) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        BookmarkCrowdTallyDataObject dataObj = new BookmarkCrowdTallyDataObject(guid, user, shortLink, domain, token, longUrl, shortUrl, tallyDate, status, note, expirationTime, bookmarkFolder, contentTag, referenceElement, elementType, keywordLink);
        return updateBookmarkCrowdTally(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateBookmarkCrowdTally(BookmarkCrowdTally bookmarkCrowdTally) throws BaseException
    {
        log.finer("BEGIN");

        // Param bookmarkCrowdTally cannot be null.....
        if(bookmarkCrowdTally == null || bookmarkCrowdTally.getGuid() == null) {
            log.log(Level.INFO, "Param bookmarkCrowdTally or its guid is null!");
            throw new BadRequestException("Param bookmarkCrowdTally object or its guid is null!");
        }
        BookmarkCrowdTallyDataObject dataObj = null;
        if(bookmarkCrowdTally instanceof BookmarkCrowdTallyDataObject) {
            dataObj = (BookmarkCrowdTallyDataObject) bookmarkCrowdTally;
        } else if(bookmarkCrowdTally instanceof BookmarkCrowdTallyBean) {
            dataObj = ((BookmarkCrowdTallyBean) bookmarkCrowdTally).toDataObject();
        } else {  // if(bookmarkCrowdTally instanceof BookmarkCrowdTally)
            dataObj = new BookmarkCrowdTallyDataObject(bookmarkCrowdTally.getGuid(), bookmarkCrowdTally.getUser(), bookmarkCrowdTally.getShortLink(), bookmarkCrowdTally.getDomain(), bookmarkCrowdTally.getToken(), bookmarkCrowdTally.getLongUrl(), bookmarkCrowdTally.getShortUrl(), bookmarkCrowdTally.getTallyDate(), bookmarkCrowdTally.getStatus(), bookmarkCrowdTally.getNote(), bookmarkCrowdTally.getExpirationTime(), bookmarkCrowdTally.getBookmarkFolder(), bookmarkCrowdTally.getContentTag(), bookmarkCrowdTally.getReferenceElement(), bookmarkCrowdTally.getElementType(), bookmarkCrowdTally.getKeywordLink());
        }
        Boolean suc = getDAOFactory().getBookmarkCrowdTallyDAO().updateBookmarkCrowdTally(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteBookmarkCrowdTally(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getBookmarkCrowdTallyDAO().deleteBookmarkCrowdTally(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteBookmarkCrowdTally(BookmarkCrowdTally bookmarkCrowdTally) throws BaseException
    {
        log.finer("BEGIN");

        // Param bookmarkCrowdTally cannot be null.....
        if(bookmarkCrowdTally == null || bookmarkCrowdTally.getGuid() == null) {
            log.log(Level.INFO, "Param bookmarkCrowdTally or its guid is null!");
            throw new BadRequestException("Param bookmarkCrowdTally object or its guid is null!");
        }
        BookmarkCrowdTallyDataObject dataObj = null;
        if(bookmarkCrowdTally instanceof BookmarkCrowdTallyDataObject) {
            dataObj = (BookmarkCrowdTallyDataObject) bookmarkCrowdTally;
        } else if(bookmarkCrowdTally instanceof BookmarkCrowdTallyBean) {
            dataObj = ((BookmarkCrowdTallyBean) bookmarkCrowdTally).toDataObject();
        } else {  // if(bookmarkCrowdTally instanceof BookmarkCrowdTally)
            dataObj = new BookmarkCrowdTallyDataObject(bookmarkCrowdTally.getGuid(), bookmarkCrowdTally.getUser(), bookmarkCrowdTally.getShortLink(), bookmarkCrowdTally.getDomain(), bookmarkCrowdTally.getToken(), bookmarkCrowdTally.getLongUrl(), bookmarkCrowdTally.getShortUrl(), bookmarkCrowdTally.getTallyDate(), bookmarkCrowdTally.getStatus(), bookmarkCrowdTally.getNote(), bookmarkCrowdTally.getExpirationTime(), bookmarkCrowdTally.getBookmarkFolder(), bookmarkCrowdTally.getContentTag(), bookmarkCrowdTally.getReferenceElement(), bookmarkCrowdTally.getElementType(), bookmarkCrowdTally.getKeywordLink());
        }
        Boolean suc = getDAOFactory().getBookmarkCrowdTallyDAO().deleteBookmarkCrowdTally(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteBookmarkCrowdTallies(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getBookmarkCrowdTallyDAO().deleteBookmarkCrowdTallies(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
