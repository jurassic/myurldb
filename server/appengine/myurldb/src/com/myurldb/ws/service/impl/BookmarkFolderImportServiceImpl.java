package com.myurldb.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.BookmarkFolderImport;
import com.myurldb.ws.bean.BookmarkFolderImportBean;
import com.myurldb.ws.dao.DAOFactory;
import com.myurldb.ws.data.BookmarkFolderImportDataObject;
import com.myurldb.ws.service.DAOFactoryManager;
import com.myurldb.ws.service.BookmarkFolderImportService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class BookmarkFolderImportServiceImpl implements BookmarkFolderImportService
{
    private static final Logger log = Logger.getLogger(BookmarkFolderImportServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // BookmarkFolderImport related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public BookmarkFolderImport getBookmarkFolderImport(String guid) throws BaseException
    {
        log.finer("BEGIN");

        BookmarkFolderImportDataObject dataObj = getDAOFactory().getBookmarkFolderImportDAO().getBookmarkFolderImport(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve BookmarkFolderImportDataObject for guid = " + guid);
            return null;  // ????
        }
        BookmarkFolderImportBean bean = new BookmarkFolderImportBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getBookmarkFolderImport(String guid, String field) throws BaseException
    {
        BookmarkFolderImportDataObject dataObj = getDAOFactory().getBookmarkFolderImportDAO().getBookmarkFolderImport(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve BookmarkFolderImportDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return dataObj.getUser();
        } else if(field.equals("precedence")) {
            return dataObj.getPrecedence();
        } else if(field.equals("importType")) {
            return dataObj.getImportType();
        } else if(field.equals("importedFolder")) {
            return dataObj.getImportedFolder();
        } else if(field.equals("importedFolderUser")) {
            return dataObj.getImportedFolderUser();
        } else if(field.equals("importedFolderTitle")) {
            return dataObj.getImportedFolderTitle();
        } else if(field.equals("importedFolderPath")) {
            return dataObj.getImportedFolderPath();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("note")) {
            return dataObj.getNote();
        } else if(field.equals("bookmarkFolder")) {
            return dataObj.getBookmarkFolder();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<BookmarkFolderImport> getBookmarkFolderImports(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<BookmarkFolderImport> list = new ArrayList<BookmarkFolderImport>();
        List<BookmarkFolderImportDataObject> dataObjs = getDAOFactory().getBookmarkFolderImportDAO().getBookmarkFolderImports(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve BookmarkFolderImportDataObject list.");
        } else {
            Iterator<BookmarkFolderImportDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                BookmarkFolderImportDataObject dataObj = (BookmarkFolderImportDataObject) it.next();
                list.add(new BookmarkFolderImportBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<BookmarkFolderImport> getAllBookmarkFolderImports() throws BaseException
    {
        return getAllBookmarkFolderImports(null, null, null);
    }

    @Override
    public List<BookmarkFolderImport> getAllBookmarkFolderImports(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<BookmarkFolderImport> list = new ArrayList<BookmarkFolderImport>();
        List<BookmarkFolderImportDataObject> dataObjs = getDAOFactory().getBookmarkFolderImportDAO().getAllBookmarkFolderImports(ordering, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve BookmarkFolderImportDataObject list.");
        } else {
            Iterator<BookmarkFolderImportDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                BookmarkFolderImportDataObject dataObj = (BookmarkFolderImportDataObject) it.next();
                list.add(new BookmarkFolderImportBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllBookmarkFolderImportKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        List<String> keys = getDAOFactory().getBookmarkFolderImportDAO().getAllBookmarkFolderImportKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve BookmarkFolderImport key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<BookmarkFolderImport> findBookmarkFolderImports(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findBookmarkFolderImports(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<BookmarkFolderImport> findBookmarkFolderImports(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("BookmarkFolderImportServiceImpl.findBookmarkFolderImports(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<BookmarkFolderImport> list = new ArrayList<BookmarkFolderImport>();
        List<BookmarkFolderImportDataObject> dataObjs = getDAOFactory().getBookmarkFolderImportDAO().findBookmarkFolderImports(filter, ordering, params, values, grouping, unique, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find bookmarkFolderImports for the given criterion.");
        } else {
            Iterator<BookmarkFolderImportDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                BookmarkFolderImportDataObject dataObj = (BookmarkFolderImportDataObject) it.next();
                list.add(new BookmarkFolderImportBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findBookmarkFolderImportKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("BookmarkFolderImportServiceImpl.findBookmarkFolderImportKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = getDAOFactory().getBookmarkFolderImportDAO().findBookmarkFolderImportKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find BookmarkFolderImport keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("BookmarkFolderImportServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getBookmarkFolderImportDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createBookmarkFolderImport(String user, Integer precedence, String importType, String importedFolder, String importedFolderUser, String importedFolderTitle, String importedFolderPath, String status, String note, String bookmarkFolder) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        BookmarkFolderImportDataObject dataObj = new BookmarkFolderImportDataObject(null, user, precedence, importType, importedFolder, importedFolderUser, importedFolderTitle, importedFolderPath, status, note, bookmarkFolder);
        return createBookmarkFolderImport(dataObj);
    }

    @Override
    public String createBookmarkFolderImport(BookmarkFolderImport bookmarkFolderImport) throws BaseException
    {
        log.finer("BEGIN");

        // Param bookmarkFolderImport cannot be null.....
        if(bookmarkFolderImport == null) {
            log.log(Level.INFO, "Param bookmarkFolderImport is null!");
            throw new BadRequestException("Param bookmarkFolderImport object is null!");
        }
        BookmarkFolderImportDataObject dataObj = null;
        if(bookmarkFolderImport instanceof BookmarkFolderImportDataObject) {
            dataObj = (BookmarkFolderImportDataObject) bookmarkFolderImport;
        } else if(bookmarkFolderImport instanceof BookmarkFolderImportBean) {
            dataObj = ((BookmarkFolderImportBean) bookmarkFolderImport).toDataObject();
        } else {  // if(bookmarkFolderImport instanceof BookmarkFolderImport)
            //dataObj = new BookmarkFolderImportDataObject(null, bookmarkFolderImport.getUser(), bookmarkFolderImport.getPrecedence(), bookmarkFolderImport.getImportType(), bookmarkFolderImport.getImportedFolder(), bookmarkFolderImport.getImportedFolderUser(), bookmarkFolderImport.getImportedFolderTitle(), bookmarkFolderImport.getImportedFolderPath(), bookmarkFolderImport.getStatus(), bookmarkFolderImport.getNote(), bookmarkFolderImport.getBookmarkFolder());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new BookmarkFolderImportDataObject(bookmarkFolderImport.getGuid(), bookmarkFolderImport.getUser(), bookmarkFolderImport.getPrecedence(), bookmarkFolderImport.getImportType(), bookmarkFolderImport.getImportedFolder(), bookmarkFolderImport.getImportedFolderUser(), bookmarkFolderImport.getImportedFolderTitle(), bookmarkFolderImport.getImportedFolderPath(), bookmarkFolderImport.getStatus(), bookmarkFolderImport.getNote(), bookmarkFolderImport.getBookmarkFolder());
        }
        String guid = getDAOFactory().getBookmarkFolderImportDAO().createBookmarkFolderImport(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateBookmarkFolderImport(String guid, String user, Integer precedence, String importType, String importedFolder, String importedFolderUser, String importedFolderTitle, String importedFolderPath, String status, String note, String bookmarkFolder) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        BookmarkFolderImportDataObject dataObj = new BookmarkFolderImportDataObject(guid, user, precedence, importType, importedFolder, importedFolderUser, importedFolderTitle, importedFolderPath, status, note, bookmarkFolder);
        return updateBookmarkFolderImport(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateBookmarkFolderImport(BookmarkFolderImport bookmarkFolderImport) throws BaseException
    {
        log.finer("BEGIN");

        // Param bookmarkFolderImport cannot be null.....
        if(bookmarkFolderImport == null || bookmarkFolderImport.getGuid() == null) {
            log.log(Level.INFO, "Param bookmarkFolderImport or its guid is null!");
            throw new BadRequestException("Param bookmarkFolderImport object or its guid is null!");
        }
        BookmarkFolderImportDataObject dataObj = null;
        if(bookmarkFolderImport instanceof BookmarkFolderImportDataObject) {
            dataObj = (BookmarkFolderImportDataObject) bookmarkFolderImport;
        } else if(bookmarkFolderImport instanceof BookmarkFolderImportBean) {
            dataObj = ((BookmarkFolderImportBean) bookmarkFolderImport).toDataObject();
        } else {  // if(bookmarkFolderImport instanceof BookmarkFolderImport)
            dataObj = new BookmarkFolderImportDataObject(bookmarkFolderImport.getGuid(), bookmarkFolderImport.getUser(), bookmarkFolderImport.getPrecedence(), bookmarkFolderImport.getImportType(), bookmarkFolderImport.getImportedFolder(), bookmarkFolderImport.getImportedFolderUser(), bookmarkFolderImport.getImportedFolderTitle(), bookmarkFolderImport.getImportedFolderPath(), bookmarkFolderImport.getStatus(), bookmarkFolderImport.getNote(), bookmarkFolderImport.getBookmarkFolder());
        }
        Boolean suc = getDAOFactory().getBookmarkFolderImportDAO().updateBookmarkFolderImport(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteBookmarkFolderImport(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getBookmarkFolderImportDAO().deleteBookmarkFolderImport(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteBookmarkFolderImport(BookmarkFolderImport bookmarkFolderImport) throws BaseException
    {
        log.finer("BEGIN");

        // Param bookmarkFolderImport cannot be null.....
        if(bookmarkFolderImport == null || bookmarkFolderImport.getGuid() == null) {
            log.log(Level.INFO, "Param bookmarkFolderImport or its guid is null!");
            throw new BadRequestException("Param bookmarkFolderImport object or its guid is null!");
        }
        BookmarkFolderImportDataObject dataObj = null;
        if(bookmarkFolderImport instanceof BookmarkFolderImportDataObject) {
            dataObj = (BookmarkFolderImportDataObject) bookmarkFolderImport;
        } else if(bookmarkFolderImport instanceof BookmarkFolderImportBean) {
            dataObj = ((BookmarkFolderImportBean) bookmarkFolderImport).toDataObject();
        } else {  // if(bookmarkFolderImport instanceof BookmarkFolderImport)
            dataObj = new BookmarkFolderImportDataObject(bookmarkFolderImport.getGuid(), bookmarkFolderImport.getUser(), bookmarkFolderImport.getPrecedence(), bookmarkFolderImport.getImportType(), bookmarkFolderImport.getImportedFolder(), bookmarkFolderImport.getImportedFolderUser(), bookmarkFolderImport.getImportedFolderTitle(), bookmarkFolderImport.getImportedFolderPath(), bookmarkFolderImport.getStatus(), bookmarkFolderImport.getNote(), bookmarkFolderImport.getBookmarkFolder());
        }
        Boolean suc = getDAOFactory().getBookmarkFolderImportDAO().deleteBookmarkFolderImport(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteBookmarkFolderImports(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getBookmarkFolderImportDAO().deleteBookmarkFolderImports(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
