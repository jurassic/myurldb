package com.myurldb.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.BookmarkFolder;
import com.myurldb.ws.bean.BookmarkFolderBean;
import com.myurldb.ws.dao.DAOFactory;
import com.myurldb.ws.data.BookmarkFolderDataObject;
import com.myurldb.ws.service.DAOFactoryManager;
import com.myurldb.ws.service.BookmarkFolderService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class BookmarkFolderServiceImpl implements BookmarkFolderService
{
    private static final Logger log = Logger.getLogger(BookmarkFolderServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // BookmarkFolder related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public BookmarkFolder getBookmarkFolder(String guid) throws BaseException
    {
        log.finer("BEGIN");

        BookmarkFolderDataObject dataObj = getDAOFactory().getBookmarkFolderDAO().getBookmarkFolder(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve BookmarkFolderDataObject for guid = " + guid);
            return null;  // ????
        }
        BookmarkFolderBean bean = new BookmarkFolderBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getBookmarkFolder(String guid, String field) throws BaseException
    {
        BookmarkFolderDataObject dataObj = getDAOFactory().getBookmarkFolderDAO().getBookmarkFolder(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve BookmarkFolderDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("appClient")) {
            return dataObj.getAppClient();
        } else if(field.equals("clientRootDomain")) {
            return dataObj.getClientRootDomain();
        } else if(field.equals("user")) {
            return dataObj.getUser();
        } else if(field.equals("title")) {
            return dataObj.getTitle();
        } else if(field.equals("description")) {
            return dataObj.getDescription();
        } else if(field.equals("type")) {
            return dataObj.getType();
        } else if(field.equals("category")) {
            return dataObj.getCategory();
        } else if(field.equals("parent")) {
            return dataObj.getParent();
        } else if(field.equals("aggregate")) {
            return dataObj.getAggregate();
        } else if(field.equals("acl")) {
            return dataObj.getAcl();
        } else if(field.equals("exportable")) {
            return dataObj.isExportable();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("note")) {
            return dataObj.getNote();
        } else if(field.equals("keywordFolder")) {
            return dataObj.getKeywordFolder();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<BookmarkFolder> getBookmarkFolders(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<BookmarkFolder> list = new ArrayList<BookmarkFolder>();
        List<BookmarkFolderDataObject> dataObjs = getDAOFactory().getBookmarkFolderDAO().getBookmarkFolders(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve BookmarkFolderDataObject list.");
        } else {
            Iterator<BookmarkFolderDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                BookmarkFolderDataObject dataObj = (BookmarkFolderDataObject) it.next();
                list.add(new BookmarkFolderBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<BookmarkFolder> getAllBookmarkFolders() throws BaseException
    {
        return getAllBookmarkFolders(null, null, null);
    }

    @Override
    public List<BookmarkFolder> getAllBookmarkFolders(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<BookmarkFolder> list = new ArrayList<BookmarkFolder>();
        List<BookmarkFolderDataObject> dataObjs = getDAOFactory().getBookmarkFolderDAO().getAllBookmarkFolders(ordering, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve BookmarkFolderDataObject list.");
        } else {
            Iterator<BookmarkFolderDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                BookmarkFolderDataObject dataObj = (BookmarkFolderDataObject) it.next();
                list.add(new BookmarkFolderBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllBookmarkFolderKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        List<String> keys = getDAOFactory().getBookmarkFolderDAO().getAllBookmarkFolderKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve BookmarkFolder key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<BookmarkFolder> findBookmarkFolders(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findBookmarkFolders(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<BookmarkFolder> findBookmarkFolders(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("BookmarkFolderServiceImpl.findBookmarkFolders(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<BookmarkFolder> list = new ArrayList<BookmarkFolder>();
        List<BookmarkFolderDataObject> dataObjs = getDAOFactory().getBookmarkFolderDAO().findBookmarkFolders(filter, ordering, params, values, grouping, unique, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find bookmarkFolders for the given criterion.");
        } else {
            Iterator<BookmarkFolderDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                BookmarkFolderDataObject dataObj = (BookmarkFolderDataObject) it.next();
                list.add(new BookmarkFolderBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findBookmarkFolderKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("BookmarkFolderServiceImpl.findBookmarkFolderKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = getDAOFactory().getBookmarkFolderDAO().findBookmarkFolderKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find BookmarkFolder keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("BookmarkFolderServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getBookmarkFolderDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createBookmarkFolder(String appClient, String clientRootDomain, String user, String title, String description, String type, String category, String parent, String aggregate, String acl, Boolean exportable, String status, String note, String keywordFolder) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        BookmarkFolderDataObject dataObj = new BookmarkFolderDataObject(null, appClient, clientRootDomain, user, title, description, type, category, parent, aggregate, acl, exportable, status, note, keywordFolder);
        return createBookmarkFolder(dataObj);
    }

    @Override
    public String createBookmarkFolder(BookmarkFolder bookmarkFolder) throws BaseException
    {
        log.finer("BEGIN");

        // Param bookmarkFolder cannot be null.....
        if(bookmarkFolder == null) {
            log.log(Level.INFO, "Param bookmarkFolder is null!");
            throw new BadRequestException("Param bookmarkFolder object is null!");
        }
        BookmarkFolderDataObject dataObj = null;
        if(bookmarkFolder instanceof BookmarkFolderDataObject) {
            dataObj = (BookmarkFolderDataObject) bookmarkFolder;
        } else if(bookmarkFolder instanceof BookmarkFolderBean) {
            dataObj = ((BookmarkFolderBean) bookmarkFolder).toDataObject();
        } else {  // if(bookmarkFolder instanceof BookmarkFolder)
            //dataObj = new BookmarkFolderDataObject(null, bookmarkFolder.getAppClient(), bookmarkFolder.getClientRootDomain(), bookmarkFolder.getUser(), bookmarkFolder.getTitle(), bookmarkFolder.getDescription(), bookmarkFolder.getType(), bookmarkFolder.getCategory(), bookmarkFolder.getParent(), bookmarkFolder.getAggregate(), bookmarkFolder.getAcl(), bookmarkFolder.isExportable(), bookmarkFolder.getStatus(), bookmarkFolder.getNote(), bookmarkFolder.getKeywordFolder());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new BookmarkFolderDataObject(bookmarkFolder.getGuid(), bookmarkFolder.getAppClient(), bookmarkFolder.getClientRootDomain(), bookmarkFolder.getUser(), bookmarkFolder.getTitle(), bookmarkFolder.getDescription(), bookmarkFolder.getType(), bookmarkFolder.getCategory(), bookmarkFolder.getParent(), bookmarkFolder.getAggregate(), bookmarkFolder.getAcl(), bookmarkFolder.isExportable(), bookmarkFolder.getStatus(), bookmarkFolder.getNote(), bookmarkFolder.getKeywordFolder());
        }
        String guid = getDAOFactory().getBookmarkFolderDAO().createBookmarkFolder(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateBookmarkFolder(String guid, String appClient, String clientRootDomain, String user, String title, String description, String type, String category, String parent, String aggregate, String acl, Boolean exportable, String status, String note, String keywordFolder) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        BookmarkFolderDataObject dataObj = new BookmarkFolderDataObject(guid, appClient, clientRootDomain, user, title, description, type, category, parent, aggregate, acl, exportable, status, note, keywordFolder);
        return updateBookmarkFolder(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateBookmarkFolder(BookmarkFolder bookmarkFolder) throws BaseException
    {
        log.finer("BEGIN");

        // Param bookmarkFolder cannot be null.....
        if(bookmarkFolder == null || bookmarkFolder.getGuid() == null) {
            log.log(Level.INFO, "Param bookmarkFolder or its guid is null!");
            throw new BadRequestException("Param bookmarkFolder object or its guid is null!");
        }
        BookmarkFolderDataObject dataObj = null;
        if(bookmarkFolder instanceof BookmarkFolderDataObject) {
            dataObj = (BookmarkFolderDataObject) bookmarkFolder;
        } else if(bookmarkFolder instanceof BookmarkFolderBean) {
            dataObj = ((BookmarkFolderBean) bookmarkFolder).toDataObject();
        } else {  // if(bookmarkFolder instanceof BookmarkFolder)
            dataObj = new BookmarkFolderDataObject(bookmarkFolder.getGuid(), bookmarkFolder.getAppClient(), bookmarkFolder.getClientRootDomain(), bookmarkFolder.getUser(), bookmarkFolder.getTitle(), bookmarkFolder.getDescription(), bookmarkFolder.getType(), bookmarkFolder.getCategory(), bookmarkFolder.getParent(), bookmarkFolder.getAggregate(), bookmarkFolder.getAcl(), bookmarkFolder.isExportable(), bookmarkFolder.getStatus(), bookmarkFolder.getNote(), bookmarkFolder.getKeywordFolder());
        }
        Boolean suc = getDAOFactory().getBookmarkFolderDAO().updateBookmarkFolder(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteBookmarkFolder(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getBookmarkFolderDAO().deleteBookmarkFolder(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteBookmarkFolder(BookmarkFolder bookmarkFolder) throws BaseException
    {
        log.finer("BEGIN");

        // Param bookmarkFolder cannot be null.....
        if(bookmarkFolder == null || bookmarkFolder.getGuid() == null) {
            log.log(Level.INFO, "Param bookmarkFolder or its guid is null!");
            throw new BadRequestException("Param bookmarkFolder object or its guid is null!");
        }
        BookmarkFolderDataObject dataObj = null;
        if(bookmarkFolder instanceof BookmarkFolderDataObject) {
            dataObj = (BookmarkFolderDataObject) bookmarkFolder;
        } else if(bookmarkFolder instanceof BookmarkFolderBean) {
            dataObj = ((BookmarkFolderBean) bookmarkFolder).toDataObject();
        } else {  // if(bookmarkFolder instanceof BookmarkFolder)
            dataObj = new BookmarkFolderDataObject(bookmarkFolder.getGuid(), bookmarkFolder.getAppClient(), bookmarkFolder.getClientRootDomain(), bookmarkFolder.getUser(), bookmarkFolder.getTitle(), bookmarkFolder.getDescription(), bookmarkFolder.getType(), bookmarkFolder.getCategory(), bookmarkFolder.getParent(), bookmarkFolder.getAggregate(), bookmarkFolder.getAcl(), bookmarkFolder.isExportable(), bookmarkFolder.getStatus(), bookmarkFolder.getNote(), bookmarkFolder.getKeywordFolder());
        }
        Boolean suc = getDAOFactory().getBookmarkFolderDAO().deleteBookmarkFolder(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteBookmarkFolders(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getBookmarkFolderDAO().deleteBookmarkFolders(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
