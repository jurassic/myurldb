package com.myurldb.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.BookmarkLink;
import com.myurldb.ws.bean.BookmarkLinkBean;
import com.myurldb.ws.dao.DAOFactory;
import com.myurldb.ws.data.BookmarkLinkDataObject;
import com.myurldb.ws.service.DAOFactoryManager;
import com.myurldb.ws.service.BookmarkLinkService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class BookmarkLinkServiceImpl implements BookmarkLinkService
{
    private static final Logger log = Logger.getLogger(BookmarkLinkServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // BookmarkLink related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public BookmarkLink getBookmarkLink(String guid) throws BaseException
    {
        log.finer("BEGIN");

        BookmarkLinkDataObject dataObj = getDAOFactory().getBookmarkLinkDAO().getBookmarkLink(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve BookmarkLinkDataObject for guid = " + guid);
            return null;  // ????
        }
        BookmarkLinkBean bean = new BookmarkLinkBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getBookmarkLink(String guid, String field) throws BaseException
    {
        BookmarkLinkDataObject dataObj = getDAOFactory().getBookmarkLinkDAO().getBookmarkLink(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve BookmarkLinkDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("appClient")) {
            return dataObj.getAppClient();
        } else if(field.equals("clientRootDomain")) {
            return dataObj.getClientRootDomain();
        } else if(field.equals("user")) {
            return dataObj.getUser();
        } else if(field.equals("shortLink")) {
            return dataObj.getShortLink();
        } else if(field.equals("domain")) {
            return dataObj.getDomain();
        } else if(field.equals("token")) {
            return dataObj.getToken();
        } else if(field.equals("longUrl")) {
            return dataObj.getLongUrl();
        } else if(field.equals("shortUrl")) {
            return dataObj.getShortUrl();
        } else if(field.equals("internal")) {
            return dataObj.isInternal();
        } else if(field.equals("caching")) {
            return dataObj.isCaching();
        } else if(field.equals("memo")) {
            return dataObj.getMemo();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("note")) {
            return dataObj.getNote();
        } else if(field.equals("expirationTime")) {
            return dataObj.getExpirationTime();
        } else if(field.equals("bookmarkFolder")) {
            return dataObj.getBookmarkFolder();
        } else if(field.equals("contentTag")) {
            return dataObj.getContentTag();
        } else if(field.equals("referenceElement")) {
            return dataObj.getReferenceElement();
        } else if(field.equals("elementType")) {
            return dataObj.getElementType();
        } else if(field.equals("keywordLink")) {
            return dataObj.getKeywordLink();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<BookmarkLink> getBookmarkLinks(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<BookmarkLink> list = new ArrayList<BookmarkLink>();
        List<BookmarkLinkDataObject> dataObjs = getDAOFactory().getBookmarkLinkDAO().getBookmarkLinks(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve BookmarkLinkDataObject list.");
        } else {
            Iterator<BookmarkLinkDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                BookmarkLinkDataObject dataObj = (BookmarkLinkDataObject) it.next();
                list.add(new BookmarkLinkBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<BookmarkLink> getAllBookmarkLinks() throws BaseException
    {
        return getAllBookmarkLinks(null, null, null);
    }

    @Override
    public List<BookmarkLink> getAllBookmarkLinks(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<BookmarkLink> list = new ArrayList<BookmarkLink>();
        List<BookmarkLinkDataObject> dataObjs = getDAOFactory().getBookmarkLinkDAO().getAllBookmarkLinks(ordering, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve BookmarkLinkDataObject list.");
        } else {
            Iterator<BookmarkLinkDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                BookmarkLinkDataObject dataObj = (BookmarkLinkDataObject) it.next();
                list.add(new BookmarkLinkBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllBookmarkLinkKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        List<String> keys = getDAOFactory().getBookmarkLinkDAO().getAllBookmarkLinkKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve BookmarkLink key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<BookmarkLink> findBookmarkLinks(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findBookmarkLinks(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<BookmarkLink> findBookmarkLinks(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("BookmarkLinkServiceImpl.findBookmarkLinks(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<BookmarkLink> list = new ArrayList<BookmarkLink>();
        List<BookmarkLinkDataObject> dataObjs = getDAOFactory().getBookmarkLinkDAO().findBookmarkLinks(filter, ordering, params, values, grouping, unique, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find bookmarkLinks for the given criterion.");
        } else {
            Iterator<BookmarkLinkDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                BookmarkLinkDataObject dataObj = (BookmarkLinkDataObject) it.next();
                list.add(new BookmarkLinkBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findBookmarkLinkKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("BookmarkLinkServiceImpl.findBookmarkLinkKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = getDAOFactory().getBookmarkLinkDAO().findBookmarkLinkKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find BookmarkLink keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("BookmarkLinkServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getBookmarkLinkDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createBookmarkLink(String appClient, String clientRootDomain, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, Boolean internal, Boolean caching, String memo, String status, String note, Long expirationTime, String bookmarkFolder, String contentTag, String referenceElement, String elementType, String keywordLink) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        BookmarkLinkDataObject dataObj = new BookmarkLinkDataObject(null, appClient, clientRootDomain, user, shortLink, domain, token, longUrl, shortUrl, internal, caching, memo, status, note, expirationTime, bookmarkFolder, contentTag, referenceElement, elementType, keywordLink);
        return createBookmarkLink(dataObj);
    }

    @Override
    public String createBookmarkLink(BookmarkLink bookmarkLink) throws BaseException
    {
        log.finer("BEGIN");

        // Param bookmarkLink cannot be null.....
        if(bookmarkLink == null) {
            log.log(Level.INFO, "Param bookmarkLink is null!");
            throw new BadRequestException("Param bookmarkLink object is null!");
        }
        BookmarkLinkDataObject dataObj = null;
        if(bookmarkLink instanceof BookmarkLinkDataObject) {
            dataObj = (BookmarkLinkDataObject) bookmarkLink;
        } else if(bookmarkLink instanceof BookmarkLinkBean) {
            dataObj = ((BookmarkLinkBean) bookmarkLink).toDataObject();
        } else {  // if(bookmarkLink instanceof BookmarkLink)
            //dataObj = new BookmarkLinkDataObject(null, bookmarkLink.getAppClient(), bookmarkLink.getClientRootDomain(), bookmarkLink.getUser(), bookmarkLink.getShortLink(), bookmarkLink.getDomain(), bookmarkLink.getToken(), bookmarkLink.getLongUrl(), bookmarkLink.getShortUrl(), bookmarkLink.isInternal(), bookmarkLink.isCaching(), bookmarkLink.getMemo(), bookmarkLink.getStatus(), bookmarkLink.getNote(), bookmarkLink.getExpirationTime(), bookmarkLink.getBookmarkFolder(), bookmarkLink.getContentTag(), bookmarkLink.getReferenceElement(), bookmarkLink.getElementType(), bookmarkLink.getKeywordLink());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new BookmarkLinkDataObject(bookmarkLink.getGuid(), bookmarkLink.getAppClient(), bookmarkLink.getClientRootDomain(), bookmarkLink.getUser(), bookmarkLink.getShortLink(), bookmarkLink.getDomain(), bookmarkLink.getToken(), bookmarkLink.getLongUrl(), bookmarkLink.getShortUrl(), bookmarkLink.isInternal(), bookmarkLink.isCaching(), bookmarkLink.getMemo(), bookmarkLink.getStatus(), bookmarkLink.getNote(), bookmarkLink.getExpirationTime(), bookmarkLink.getBookmarkFolder(), bookmarkLink.getContentTag(), bookmarkLink.getReferenceElement(), bookmarkLink.getElementType(), bookmarkLink.getKeywordLink());
        }
        String guid = getDAOFactory().getBookmarkLinkDAO().createBookmarkLink(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateBookmarkLink(String guid, String appClient, String clientRootDomain, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, Boolean internal, Boolean caching, String memo, String status, String note, Long expirationTime, String bookmarkFolder, String contentTag, String referenceElement, String elementType, String keywordLink) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        BookmarkLinkDataObject dataObj = new BookmarkLinkDataObject(guid, appClient, clientRootDomain, user, shortLink, domain, token, longUrl, shortUrl, internal, caching, memo, status, note, expirationTime, bookmarkFolder, contentTag, referenceElement, elementType, keywordLink);
        return updateBookmarkLink(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateBookmarkLink(BookmarkLink bookmarkLink) throws BaseException
    {
        log.finer("BEGIN");

        // Param bookmarkLink cannot be null.....
        if(bookmarkLink == null || bookmarkLink.getGuid() == null) {
            log.log(Level.INFO, "Param bookmarkLink or its guid is null!");
            throw new BadRequestException("Param bookmarkLink object or its guid is null!");
        }
        BookmarkLinkDataObject dataObj = null;
        if(bookmarkLink instanceof BookmarkLinkDataObject) {
            dataObj = (BookmarkLinkDataObject) bookmarkLink;
        } else if(bookmarkLink instanceof BookmarkLinkBean) {
            dataObj = ((BookmarkLinkBean) bookmarkLink).toDataObject();
        } else {  // if(bookmarkLink instanceof BookmarkLink)
            dataObj = new BookmarkLinkDataObject(bookmarkLink.getGuid(), bookmarkLink.getAppClient(), bookmarkLink.getClientRootDomain(), bookmarkLink.getUser(), bookmarkLink.getShortLink(), bookmarkLink.getDomain(), bookmarkLink.getToken(), bookmarkLink.getLongUrl(), bookmarkLink.getShortUrl(), bookmarkLink.isInternal(), bookmarkLink.isCaching(), bookmarkLink.getMemo(), bookmarkLink.getStatus(), bookmarkLink.getNote(), bookmarkLink.getExpirationTime(), bookmarkLink.getBookmarkFolder(), bookmarkLink.getContentTag(), bookmarkLink.getReferenceElement(), bookmarkLink.getElementType(), bookmarkLink.getKeywordLink());
        }
        Boolean suc = getDAOFactory().getBookmarkLinkDAO().updateBookmarkLink(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteBookmarkLink(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getBookmarkLinkDAO().deleteBookmarkLink(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteBookmarkLink(BookmarkLink bookmarkLink) throws BaseException
    {
        log.finer("BEGIN");

        // Param bookmarkLink cannot be null.....
        if(bookmarkLink == null || bookmarkLink.getGuid() == null) {
            log.log(Level.INFO, "Param bookmarkLink or its guid is null!");
            throw new BadRequestException("Param bookmarkLink object or its guid is null!");
        }
        BookmarkLinkDataObject dataObj = null;
        if(bookmarkLink instanceof BookmarkLinkDataObject) {
            dataObj = (BookmarkLinkDataObject) bookmarkLink;
        } else if(bookmarkLink instanceof BookmarkLinkBean) {
            dataObj = ((BookmarkLinkBean) bookmarkLink).toDataObject();
        } else {  // if(bookmarkLink instanceof BookmarkLink)
            dataObj = new BookmarkLinkDataObject(bookmarkLink.getGuid(), bookmarkLink.getAppClient(), bookmarkLink.getClientRootDomain(), bookmarkLink.getUser(), bookmarkLink.getShortLink(), bookmarkLink.getDomain(), bookmarkLink.getToken(), bookmarkLink.getLongUrl(), bookmarkLink.getShortUrl(), bookmarkLink.isInternal(), bookmarkLink.isCaching(), bookmarkLink.getMemo(), bookmarkLink.getStatus(), bookmarkLink.getNote(), bookmarkLink.getExpirationTime(), bookmarkLink.getBookmarkFolder(), bookmarkLink.getContentTag(), bookmarkLink.getReferenceElement(), bookmarkLink.getElementType(), bookmarkLink.getKeywordLink());
        }
        Boolean suc = getDAOFactory().getBookmarkLinkDAO().deleteBookmarkLink(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteBookmarkLinks(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getBookmarkLinkDAO().deleteBookmarkLinks(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
