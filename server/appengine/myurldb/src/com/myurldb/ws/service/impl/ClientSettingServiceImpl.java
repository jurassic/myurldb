package com.myurldb.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.ClientSetting;
import com.myurldb.ws.bean.ClientSettingBean;
import com.myurldb.ws.dao.DAOFactory;
import com.myurldb.ws.data.ClientSettingDataObject;
import com.myurldb.ws.service.DAOFactoryManager;
import com.myurldb.ws.service.ClientSettingService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class ClientSettingServiceImpl implements ClientSettingService
{
    private static final Logger log = Logger.getLogger(ClientSettingServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // ClientSetting related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public ClientSetting getClientSetting(String guid) throws BaseException
    {
        log.finer("BEGIN");

        ClientSettingDataObject dataObj = getDAOFactory().getClientSettingDAO().getClientSetting(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve ClientSettingDataObject for guid = " + guid);
            return null;  // ????
        }
        ClientSettingBean bean = new ClientSettingBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getClientSetting(String guid, String field) throws BaseException
    {
        ClientSettingDataObject dataObj = getDAOFactory().getClientSettingDAO().getClientSetting(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve ClientSettingDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("appClient")) {
            return dataObj.getAppClient();
        } else if(field.equals("admin")) {
            return dataObj.getAdmin();
        } else if(field.equals("autoRedirectEnabled")) {
            return dataObj.isAutoRedirectEnabled();
        } else if(field.equals("viewEnabled")) {
            return dataObj.isViewEnabled();
        } else if(field.equals("shareEnabled")) {
            return dataObj.isShareEnabled();
        } else if(field.equals("defaultBaseDomain")) {
            return dataObj.getDefaultBaseDomain();
        } else if(field.equals("defaultDomainType")) {
            return dataObj.getDefaultDomainType();
        } else if(field.equals("defaultTokenType")) {
            return dataObj.getDefaultTokenType();
        } else if(field.equals("defaultRedirectType")) {
            return dataObj.getDefaultRedirectType();
        } else if(field.equals("defaultFlashDuration")) {
            return dataObj.getDefaultFlashDuration();
        } else if(field.equals("defaultAccessType")) {
            return dataObj.getDefaultAccessType();
        } else if(field.equals("defaultViewType")) {
            return dataObj.getDefaultViewType();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<ClientSetting> getClientSettings(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<ClientSetting> list = new ArrayList<ClientSetting>();
        List<ClientSettingDataObject> dataObjs = getDAOFactory().getClientSettingDAO().getClientSettings(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve ClientSettingDataObject list.");
        } else {
            Iterator<ClientSettingDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                ClientSettingDataObject dataObj = (ClientSettingDataObject) it.next();
                list.add(new ClientSettingBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<ClientSetting> getAllClientSettings() throws BaseException
    {
        return getAllClientSettings(null, null, null);
    }

    @Override
    public List<ClientSetting> getAllClientSettings(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<ClientSetting> list = new ArrayList<ClientSetting>();
        List<ClientSettingDataObject> dataObjs = getDAOFactory().getClientSettingDAO().getAllClientSettings(ordering, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve ClientSettingDataObject list.");
        } else {
            Iterator<ClientSettingDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                ClientSettingDataObject dataObj = (ClientSettingDataObject) it.next();
                list.add(new ClientSettingBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllClientSettingKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        List<String> keys = getDAOFactory().getClientSettingDAO().getAllClientSettingKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve ClientSetting key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<ClientSetting> findClientSettings(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findClientSettings(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<ClientSetting> findClientSettings(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("ClientSettingServiceImpl.findClientSettings(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<ClientSetting> list = new ArrayList<ClientSetting>();
        List<ClientSettingDataObject> dataObjs = getDAOFactory().getClientSettingDAO().findClientSettings(filter, ordering, params, values, grouping, unique, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find clientSettings for the given criterion.");
        } else {
            Iterator<ClientSettingDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                ClientSettingDataObject dataObj = (ClientSettingDataObject) it.next();
                list.add(new ClientSettingBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findClientSettingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("ClientSettingServiceImpl.findClientSettingKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = getDAOFactory().getClientSettingDAO().findClientSettingKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find ClientSetting keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("ClientSettingServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getClientSettingDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createClientSetting(String appClient, String admin, Boolean autoRedirectEnabled, Boolean viewEnabled, Boolean shareEnabled, String defaultBaseDomain, String defaultDomainType, String defaultTokenType, String defaultRedirectType, Long defaultFlashDuration, String defaultAccessType, String defaultViewType) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        ClientSettingDataObject dataObj = new ClientSettingDataObject(null, appClient, admin, autoRedirectEnabled, viewEnabled, shareEnabled, defaultBaseDomain, defaultDomainType, defaultTokenType, defaultRedirectType, defaultFlashDuration, defaultAccessType, defaultViewType);
        return createClientSetting(dataObj);
    }

    @Override
    public String createClientSetting(ClientSetting clientSetting) throws BaseException
    {
        log.finer("BEGIN");

        // Param clientSetting cannot be null.....
        if(clientSetting == null) {
            log.log(Level.INFO, "Param clientSetting is null!");
            throw new BadRequestException("Param clientSetting object is null!");
        }
        ClientSettingDataObject dataObj = null;
        if(clientSetting instanceof ClientSettingDataObject) {
            dataObj = (ClientSettingDataObject) clientSetting;
        } else if(clientSetting instanceof ClientSettingBean) {
            dataObj = ((ClientSettingBean) clientSetting).toDataObject();
        } else {  // if(clientSetting instanceof ClientSetting)
            //dataObj = new ClientSettingDataObject(null, clientSetting.getAppClient(), clientSetting.getAdmin(), clientSetting.isAutoRedirectEnabled(), clientSetting.isViewEnabled(), clientSetting.isShareEnabled(), clientSetting.getDefaultBaseDomain(), clientSetting.getDefaultDomainType(), clientSetting.getDefaultTokenType(), clientSetting.getDefaultRedirectType(), clientSetting.getDefaultFlashDuration(), clientSetting.getDefaultAccessType(), clientSetting.getDefaultViewType());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new ClientSettingDataObject(clientSetting.getGuid(), clientSetting.getAppClient(), clientSetting.getAdmin(), clientSetting.isAutoRedirectEnabled(), clientSetting.isViewEnabled(), clientSetting.isShareEnabled(), clientSetting.getDefaultBaseDomain(), clientSetting.getDefaultDomainType(), clientSetting.getDefaultTokenType(), clientSetting.getDefaultRedirectType(), clientSetting.getDefaultFlashDuration(), clientSetting.getDefaultAccessType(), clientSetting.getDefaultViewType());
        }
        String guid = getDAOFactory().getClientSettingDAO().createClientSetting(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateClientSetting(String guid, String appClient, String admin, Boolean autoRedirectEnabled, Boolean viewEnabled, Boolean shareEnabled, String defaultBaseDomain, String defaultDomainType, String defaultTokenType, String defaultRedirectType, Long defaultFlashDuration, String defaultAccessType, String defaultViewType) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        ClientSettingDataObject dataObj = new ClientSettingDataObject(guid, appClient, admin, autoRedirectEnabled, viewEnabled, shareEnabled, defaultBaseDomain, defaultDomainType, defaultTokenType, defaultRedirectType, defaultFlashDuration, defaultAccessType, defaultViewType);
        return updateClientSetting(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateClientSetting(ClientSetting clientSetting) throws BaseException
    {
        log.finer("BEGIN");

        // Param clientSetting cannot be null.....
        if(clientSetting == null || clientSetting.getGuid() == null) {
            log.log(Level.INFO, "Param clientSetting or its guid is null!");
            throw new BadRequestException("Param clientSetting object or its guid is null!");
        }
        ClientSettingDataObject dataObj = null;
        if(clientSetting instanceof ClientSettingDataObject) {
            dataObj = (ClientSettingDataObject) clientSetting;
        } else if(clientSetting instanceof ClientSettingBean) {
            dataObj = ((ClientSettingBean) clientSetting).toDataObject();
        } else {  // if(clientSetting instanceof ClientSetting)
            dataObj = new ClientSettingDataObject(clientSetting.getGuid(), clientSetting.getAppClient(), clientSetting.getAdmin(), clientSetting.isAutoRedirectEnabled(), clientSetting.isViewEnabled(), clientSetting.isShareEnabled(), clientSetting.getDefaultBaseDomain(), clientSetting.getDefaultDomainType(), clientSetting.getDefaultTokenType(), clientSetting.getDefaultRedirectType(), clientSetting.getDefaultFlashDuration(), clientSetting.getDefaultAccessType(), clientSetting.getDefaultViewType());
        }
        Boolean suc = getDAOFactory().getClientSettingDAO().updateClientSetting(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteClientSetting(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getClientSettingDAO().deleteClientSetting(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteClientSetting(ClientSetting clientSetting) throws BaseException
    {
        log.finer("BEGIN");

        // Param clientSetting cannot be null.....
        if(clientSetting == null || clientSetting.getGuid() == null) {
            log.log(Level.INFO, "Param clientSetting or its guid is null!");
            throw new BadRequestException("Param clientSetting object or its guid is null!");
        }
        ClientSettingDataObject dataObj = null;
        if(clientSetting instanceof ClientSettingDataObject) {
            dataObj = (ClientSettingDataObject) clientSetting;
        } else if(clientSetting instanceof ClientSettingBean) {
            dataObj = ((ClientSettingBean) clientSetting).toDataObject();
        } else {  // if(clientSetting instanceof ClientSetting)
            dataObj = new ClientSettingDataObject(clientSetting.getGuid(), clientSetting.getAppClient(), clientSetting.getAdmin(), clientSetting.isAutoRedirectEnabled(), clientSetting.isViewEnabled(), clientSetting.isShareEnabled(), clientSetting.getDefaultBaseDomain(), clientSetting.getDefaultDomainType(), clientSetting.getDefaultTokenType(), clientSetting.getDefaultRedirectType(), clientSetting.getDefaultFlashDuration(), clientSetting.getDefaultAccessType(), clientSetting.getDefaultViewType());
        }
        Boolean suc = getDAOFactory().getClientSettingDAO().deleteClientSetting(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteClientSettings(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getClientSettingDAO().deleteClientSettings(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
