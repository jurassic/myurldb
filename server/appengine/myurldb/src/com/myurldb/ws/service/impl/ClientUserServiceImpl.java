package com.myurldb.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.ClientUser;
import com.myurldb.ws.bean.ClientUserBean;
import com.myurldb.ws.dao.DAOFactory;
import com.myurldb.ws.data.ClientUserDataObject;
import com.myurldb.ws.service.DAOFactoryManager;
import com.myurldb.ws.service.ClientUserService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class ClientUserServiceImpl implements ClientUserService
{
    private static final Logger log = Logger.getLogger(ClientUserServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // ClientUser related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public ClientUser getClientUser(String guid) throws BaseException
    {
        log.finer("BEGIN");

        ClientUserDataObject dataObj = getDAOFactory().getClientUserDAO().getClientUser(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve ClientUserDataObject for guid = " + guid);
            return null;  // ????
        }
        ClientUserBean bean = new ClientUserBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getClientUser(String guid, String field) throws BaseException
    {
        ClientUserDataObject dataObj = getDAOFactory().getClientUserDAO().getClientUser(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve ClientUserDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("appClient")) {
            return dataObj.getAppClient();
        } else if(field.equals("user")) {
            return dataObj.getUser();
        } else if(field.equals("role")) {
            return dataObj.getRole();
        } else if(field.equals("provisioned")) {
            return dataObj.isProvisioned();
        } else if(field.equals("licensed")) {
            return dataObj.isLicensed();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<ClientUser> getClientUsers(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<ClientUser> list = new ArrayList<ClientUser>();
        List<ClientUserDataObject> dataObjs = getDAOFactory().getClientUserDAO().getClientUsers(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve ClientUserDataObject list.");
        } else {
            Iterator<ClientUserDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                ClientUserDataObject dataObj = (ClientUserDataObject) it.next();
                list.add(new ClientUserBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<ClientUser> getAllClientUsers() throws BaseException
    {
        return getAllClientUsers(null, null, null);
    }

    @Override
    public List<ClientUser> getAllClientUsers(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<ClientUser> list = new ArrayList<ClientUser>();
        List<ClientUserDataObject> dataObjs = getDAOFactory().getClientUserDAO().getAllClientUsers(ordering, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve ClientUserDataObject list.");
        } else {
            Iterator<ClientUserDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                ClientUserDataObject dataObj = (ClientUserDataObject) it.next();
                list.add(new ClientUserBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllClientUserKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        List<String> keys = getDAOFactory().getClientUserDAO().getAllClientUserKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve ClientUser key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<ClientUser> findClientUsers(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findClientUsers(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<ClientUser> findClientUsers(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("ClientUserServiceImpl.findClientUsers(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<ClientUser> list = new ArrayList<ClientUser>();
        List<ClientUserDataObject> dataObjs = getDAOFactory().getClientUserDAO().findClientUsers(filter, ordering, params, values, grouping, unique, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find clientUsers for the given criterion.");
        } else {
            Iterator<ClientUserDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                ClientUserDataObject dataObj = (ClientUserDataObject) it.next();
                list.add(new ClientUserBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findClientUserKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("ClientUserServiceImpl.findClientUserKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = getDAOFactory().getClientUserDAO().findClientUserKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find ClientUser keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("ClientUserServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getClientUserDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createClientUser(String appClient, String user, String role, Boolean provisioned, Boolean licensed, String status) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        ClientUserDataObject dataObj = new ClientUserDataObject(null, appClient, user, role, provisioned, licensed, status);
        return createClientUser(dataObj);
    }

    @Override
    public String createClientUser(ClientUser clientUser) throws BaseException
    {
        log.finer("BEGIN");

        // Param clientUser cannot be null.....
        if(clientUser == null) {
            log.log(Level.INFO, "Param clientUser is null!");
            throw new BadRequestException("Param clientUser object is null!");
        }
        ClientUserDataObject dataObj = null;
        if(clientUser instanceof ClientUserDataObject) {
            dataObj = (ClientUserDataObject) clientUser;
        } else if(clientUser instanceof ClientUserBean) {
            dataObj = ((ClientUserBean) clientUser).toDataObject();
        } else {  // if(clientUser instanceof ClientUser)
            //dataObj = new ClientUserDataObject(null, clientUser.getAppClient(), clientUser.getUser(), clientUser.getRole(), clientUser.isProvisioned(), clientUser.isLicensed(), clientUser.getStatus());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new ClientUserDataObject(clientUser.getGuid(), clientUser.getAppClient(), clientUser.getUser(), clientUser.getRole(), clientUser.isProvisioned(), clientUser.isLicensed(), clientUser.getStatus());
        }
        String guid = getDAOFactory().getClientUserDAO().createClientUser(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateClientUser(String guid, String appClient, String user, String role, Boolean provisioned, Boolean licensed, String status) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        ClientUserDataObject dataObj = new ClientUserDataObject(guid, appClient, user, role, provisioned, licensed, status);
        return updateClientUser(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateClientUser(ClientUser clientUser) throws BaseException
    {
        log.finer("BEGIN");

        // Param clientUser cannot be null.....
        if(clientUser == null || clientUser.getGuid() == null) {
            log.log(Level.INFO, "Param clientUser or its guid is null!");
            throw new BadRequestException("Param clientUser object or its guid is null!");
        }
        ClientUserDataObject dataObj = null;
        if(clientUser instanceof ClientUserDataObject) {
            dataObj = (ClientUserDataObject) clientUser;
        } else if(clientUser instanceof ClientUserBean) {
            dataObj = ((ClientUserBean) clientUser).toDataObject();
        } else {  // if(clientUser instanceof ClientUser)
            dataObj = new ClientUserDataObject(clientUser.getGuid(), clientUser.getAppClient(), clientUser.getUser(), clientUser.getRole(), clientUser.isProvisioned(), clientUser.isLicensed(), clientUser.getStatus());
        }
        Boolean suc = getDAOFactory().getClientUserDAO().updateClientUser(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteClientUser(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getClientUserDAO().deleteClientUser(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteClientUser(ClientUser clientUser) throws BaseException
    {
        log.finer("BEGIN");

        // Param clientUser cannot be null.....
        if(clientUser == null || clientUser.getGuid() == null) {
            log.log(Level.INFO, "Param clientUser or its guid is null!");
            throw new BadRequestException("Param clientUser object or its guid is null!");
        }
        ClientUserDataObject dataObj = null;
        if(clientUser instanceof ClientUserDataObject) {
            dataObj = (ClientUserDataObject) clientUser;
        } else if(clientUser instanceof ClientUserBean) {
            dataObj = ((ClientUserBean) clientUser).toDataObject();
        } else {  // if(clientUser instanceof ClientUser)
            dataObj = new ClientUserDataObject(clientUser.getGuid(), clientUser.getAppClient(), clientUser.getUser(), clientUser.getRole(), clientUser.isProvisioned(), clientUser.isLicensed(), clientUser.getStatus());
        }
        Boolean suc = getDAOFactory().getClientUserDAO().deleteClientUser(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteClientUsers(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getClientUserDAO().deleteClientUsers(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
