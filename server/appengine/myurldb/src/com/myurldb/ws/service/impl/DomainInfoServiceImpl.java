package com.myurldb.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.DomainInfo;
import com.myurldb.ws.bean.DomainInfoBean;
import com.myurldb.ws.dao.DAOFactory;
import com.myurldb.ws.data.DomainInfoDataObject;
import com.myurldb.ws.service.DAOFactoryManager;
import com.myurldb.ws.service.DomainInfoService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class DomainInfoServiceImpl implements DomainInfoService
{
    private static final Logger log = Logger.getLogger(DomainInfoServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // DomainInfo related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public DomainInfo getDomainInfo(String guid) throws BaseException
    {
        log.finer("BEGIN");

        DomainInfoDataObject dataObj = getDAOFactory().getDomainInfoDAO().getDomainInfo(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve DomainInfoDataObject for guid = " + guid);
            return null;  // ????
        }
        DomainInfoBean bean = new DomainInfoBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getDomainInfo(String guid, String field) throws BaseException
    {
        DomainInfoDataObject dataObj = getDAOFactory().getDomainInfoDAO().getDomainInfo(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve DomainInfoDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("domain")) {
            return dataObj.getDomain();
        } else if(field.equals("banned")) {
            return dataObj.isBanned();
        } else if(field.equals("urlShortener")) {
            return dataObj.isUrlShortener();
        } else if(field.equals("category")) {
            return dataObj.getCategory();
        } else if(field.equals("reputation")) {
            return dataObj.getReputation();
        } else if(field.equals("authority")) {
            return dataObj.getAuthority();
        } else if(field.equals("note")) {
            return dataObj.getNote();
        } else if(field.equals("verifiedTime")) {
            return dataObj.getVerifiedTime();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<DomainInfo> getDomainInfos(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<DomainInfo> list = new ArrayList<DomainInfo>();
        List<DomainInfoDataObject> dataObjs = getDAOFactory().getDomainInfoDAO().getDomainInfos(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve DomainInfoDataObject list.");
        } else {
            Iterator<DomainInfoDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                DomainInfoDataObject dataObj = (DomainInfoDataObject) it.next();
                list.add(new DomainInfoBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<DomainInfo> getAllDomainInfos() throws BaseException
    {
        return getAllDomainInfos(null, null, null);
    }

    @Override
    public List<DomainInfo> getAllDomainInfos(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<DomainInfo> list = new ArrayList<DomainInfo>();
        List<DomainInfoDataObject> dataObjs = getDAOFactory().getDomainInfoDAO().getAllDomainInfos(ordering, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve DomainInfoDataObject list.");
        } else {
            Iterator<DomainInfoDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                DomainInfoDataObject dataObj = (DomainInfoDataObject) it.next();
                list.add(new DomainInfoBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllDomainInfoKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        List<String> keys = getDAOFactory().getDomainInfoDAO().getAllDomainInfoKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve DomainInfo key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<DomainInfo> findDomainInfos(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findDomainInfos(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<DomainInfo> findDomainInfos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("DomainInfoServiceImpl.findDomainInfos(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<DomainInfo> list = new ArrayList<DomainInfo>();
        List<DomainInfoDataObject> dataObjs = getDAOFactory().getDomainInfoDAO().findDomainInfos(filter, ordering, params, values, grouping, unique, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find domainInfos for the given criterion.");
        } else {
            Iterator<DomainInfoDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                DomainInfoDataObject dataObj = (DomainInfoDataObject) it.next();
                list.add(new DomainInfoBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findDomainInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("DomainInfoServiceImpl.findDomainInfoKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = getDAOFactory().getDomainInfoDAO().findDomainInfoKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find DomainInfo keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("DomainInfoServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getDomainInfoDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createDomainInfo(String domain, Boolean banned, Boolean urlShortener, String category, String reputation, String authority, String note, Long verifiedTime) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        DomainInfoDataObject dataObj = new DomainInfoDataObject(null, domain, banned, urlShortener, category, reputation, authority, note, verifiedTime);
        return createDomainInfo(dataObj);
    }

    @Override
    public String createDomainInfo(DomainInfo domainInfo) throws BaseException
    {
        log.finer("BEGIN");

        // Param domainInfo cannot be null.....
        if(domainInfo == null) {
            log.log(Level.INFO, "Param domainInfo is null!");
            throw new BadRequestException("Param domainInfo object is null!");
        }
        DomainInfoDataObject dataObj = null;
        if(domainInfo instanceof DomainInfoDataObject) {
            dataObj = (DomainInfoDataObject) domainInfo;
        } else if(domainInfo instanceof DomainInfoBean) {
            dataObj = ((DomainInfoBean) domainInfo).toDataObject();
        } else {  // if(domainInfo instanceof DomainInfo)
            //dataObj = new DomainInfoDataObject(null, domainInfo.getDomain(), domainInfo.isBanned(), domainInfo.isUrlShortener(), domainInfo.getCategory(), domainInfo.getReputation(), domainInfo.getAuthority(), domainInfo.getNote(), domainInfo.getVerifiedTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new DomainInfoDataObject(domainInfo.getGuid(), domainInfo.getDomain(), domainInfo.isBanned(), domainInfo.isUrlShortener(), domainInfo.getCategory(), domainInfo.getReputation(), domainInfo.getAuthority(), domainInfo.getNote(), domainInfo.getVerifiedTime());
        }
        String guid = getDAOFactory().getDomainInfoDAO().createDomainInfo(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateDomainInfo(String guid, String domain, Boolean banned, Boolean urlShortener, String category, String reputation, String authority, String note, Long verifiedTime) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        DomainInfoDataObject dataObj = new DomainInfoDataObject(guid, domain, banned, urlShortener, category, reputation, authority, note, verifiedTime);
        return updateDomainInfo(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateDomainInfo(DomainInfo domainInfo) throws BaseException
    {
        log.finer("BEGIN");

        // Param domainInfo cannot be null.....
        if(domainInfo == null || domainInfo.getGuid() == null) {
            log.log(Level.INFO, "Param domainInfo or its guid is null!");
            throw new BadRequestException("Param domainInfo object or its guid is null!");
        }
        DomainInfoDataObject dataObj = null;
        if(domainInfo instanceof DomainInfoDataObject) {
            dataObj = (DomainInfoDataObject) domainInfo;
        } else if(domainInfo instanceof DomainInfoBean) {
            dataObj = ((DomainInfoBean) domainInfo).toDataObject();
        } else {  // if(domainInfo instanceof DomainInfo)
            dataObj = new DomainInfoDataObject(domainInfo.getGuid(), domainInfo.getDomain(), domainInfo.isBanned(), domainInfo.isUrlShortener(), domainInfo.getCategory(), domainInfo.getReputation(), domainInfo.getAuthority(), domainInfo.getNote(), domainInfo.getVerifiedTime());
        }
        Boolean suc = getDAOFactory().getDomainInfoDAO().updateDomainInfo(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteDomainInfo(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getDomainInfoDAO().deleteDomainInfo(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteDomainInfo(DomainInfo domainInfo) throws BaseException
    {
        log.finer("BEGIN");

        // Param domainInfo cannot be null.....
        if(domainInfo == null || domainInfo.getGuid() == null) {
            log.log(Level.INFO, "Param domainInfo or its guid is null!");
            throw new BadRequestException("Param domainInfo object or its guid is null!");
        }
        DomainInfoDataObject dataObj = null;
        if(domainInfo instanceof DomainInfoDataObject) {
            dataObj = (DomainInfoDataObject) domainInfo;
        } else if(domainInfo instanceof DomainInfoBean) {
            dataObj = ((DomainInfoBean) domainInfo).toDataObject();
        } else {  // if(domainInfo instanceof DomainInfo)
            dataObj = new DomainInfoDataObject(domainInfo.getGuid(), domainInfo.getDomain(), domainInfo.isBanned(), domainInfo.isUrlShortener(), domainInfo.getCategory(), domainInfo.getReputation(), domainInfo.getAuthority(), domainInfo.getNote(), domainInfo.getVerifiedTime());
        }
        Boolean suc = getDAOFactory().getDomainInfoDAO().deleteDomainInfo(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteDomainInfos(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getDomainInfoDAO().deleteDomainInfos(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
