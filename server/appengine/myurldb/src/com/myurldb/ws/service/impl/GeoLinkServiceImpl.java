package com.myurldb.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.CellLatitudeLongitude;
import com.myurldb.ws.GeoCoordinateStruct;
import com.myurldb.ws.GeoLink;
import com.myurldb.ws.bean.CellLatitudeLongitudeBean;
import com.myurldb.ws.bean.GeoCoordinateStructBean;
import com.myurldb.ws.bean.GeoLinkBean;
import com.myurldb.ws.dao.DAOFactory;
import com.myurldb.ws.data.CellLatitudeLongitudeDataObject;
import com.myurldb.ws.data.GeoCoordinateStructDataObject;
import com.myurldb.ws.data.GeoLinkDataObject;
import com.myurldb.ws.service.DAOFactoryManager;
import com.myurldb.ws.service.GeoLinkService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class GeoLinkServiceImpl implements GeoLinkService
{
    private static final Logger log = Logger.getLogger(GeoLinkServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // GeoLink related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public GeoLink getGeoLink(String guid) throws BaseException
    {
        log.finer("BEGIN");

        GeoLinkDataObject dataObj = getDAOFactory().getGeoLinkDAO().getGeoLink(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve GeoLinkDataObject for guid = " + guid);
            return null;  // ????
        }
        GeoLinkBean bean = new GeoLinkBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getGeoLink(String guid, String field) throws BaseException
    {
        GeoLinkDataObject dataObj = getDAOFactory().getGeoLinkDAO().getGeoLink(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve GeoLinkDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("shortLink")) {
            return dataObj.getShortLink();
        } else if(field.equals("shortUrl")) {
            return dataObj.getShortUrl();
        } else if(field.equals("geoCoordinate")) {
            return dataObj.getGeoCoordinate();
        } else if(field.equals("geoCell")) {
            return dataObj.getGeoCell();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<GeoLink> getGeoLinks(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<GeoLink> list = new ArrayList<GeoLink>();
        List<GeoLinkDataObject> dataObjs = getDAOFactory().getGeoLinkDAO().getGeoLinks(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve GeoLinkDataObject list.");
        } else {
            Iterator<GeoLinkDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                GeoLinkDataObject dataObj = (GeoLinkDataObject) it.next();
                list.add(new GeoLinkBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<GeoLink> getAllGeoLinks() throws BaseException
    {
        return getAllGeoLinks(null, null, null);
    }

    @Override
    public List<GeoLink> getAllGeoLinks(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<GeoLink> list = new ArrayList<GeoLink>();
        List<GeoLinkDataObject> dataObjs = getDAOFactory().getGeoLinkDAO().getAllGeoLinks(ordering, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve GeoLinkDataObject list.");
        } else {
            Iterator<GeoLinkDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                GeoLinkDataObject dataObj = (GeoLinkDataObject) it.next();
                list.add(new GeoLinkBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllGeoLinkKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        List<String> keys = getDAOFactory().getGeoLinkDAO().getAllGeoLinkKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve GeoLink key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<GeoLink> findGeoLinks(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findGeoLinks(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<GeoLink> findGeoLinks(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("GeoLinkServiceImpl.findGeoLinks(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<GeoLink> list = new ArrayList<GeoLink>();
        List<GeoLinkDataObject> dataObjs = getDAOFactory().getGeoLinkDAO().findGeoLinks(filter, ordering, params, values, grouping, unique, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find geoLinks for the given criterion.");
        } else {
            Iterator<GeoLinkDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                GeoLinkDataObject dataObj = (GeoLinkDataObject) it.next();
                list.add(new GeoLinkBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findGeoLinkKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("GeoLinkServiceImpl.findGeoLinkKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = getDAOFactory().getGeoLinkDAO().findGeoLinkKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find GeoLink keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("GeoLinkServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getGeoLinkDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createGeoLink(String shortLink, String shortUrl, GeoCoordinateStruct geoCoordinate, CellLatitudeLongitude geoCell, String status) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        GeoCoordinateStructDataObject geoCoordinateDobj = null;
        if(geoCoordinate instanceof GeoCoordinateStructBean) {
            geoCoordinateDobj = ((GeoCoordinateStructBean) geoCoordinate).toDataObject();
        } else if(geoCoordinate instanceof GeoCoordinateStruct) {
            geoCoordinateDobj = new GeoCoordinateStructDataObject(geoCoordinate.getUuid(), geoCoordinate.getLatitude(), geoCoordinate.getLongitude(), geoCoordinate.getAltitude(), geoCoordinate.isSensorUsed(), geoCoordinate.getAccuracy(), geoCoordinate.getAltitudeAccuracy(), geoCoordinate.getHeading(), geoCoordinate.getSpeed(), geoCoordinate.getNote());
        } else {
            geoCoordinateDobj = null;   // ????
        }
        CellLatitudeLongitudeDataObject geoCellDobj = null;
        if(geoCell instanceof CellLatitudeLongitudeBean) {
            geoCellDobj = ((CellLatitudeLongitudeBean) geoCell).toDataObject();
        } else if(geoCell instanceof CellLatitudeLongitude) {
            geoCellDobj = new CellLatitudeLongitudeDataObject(geoCell.getScale(), geoCell.getLatitude(), geoCell.getLongitude());
        } else {
            geoCellDobj = null;   // ????
        }
        
        GeoLinkDataObject dataObj = new GeoLinkDataObject(null, shortLink, shortUrl, geoCoordinateDobj, geoCellDobj, status);
        return createGeoLink(dataObj);
    }

    @Override
    public String createGeoLink(GeoLink geoLink) throws BaseException
    {
        log.finer("BEGIN");

        // Param geoLink cannot be null.....
        if(geoLink == null) {
            log.log(Level.INFO, "Param geoLink is null!");
            throw new BadRequestException("Param geoLink object is null!");
        }
        GeoLinkDataObject dataObj = null;
        if(geoLink instanceof GeoLinkDataObject) {
            dataObj = (GeoLinkDataObject) geoLink;
        } else if(geoLink instanceof GeoLinkBean) {
            dataObj = ((GeoLinkBean) geoLink).toDataObject();
        } else {  // if(geoLink instanceof GeoLink)
            //dataObj = new GeoLinkDataObject(null, geoLink.getShortLink(), geoLink.getShortUrl(), (GeoCoordinateStructDataObject) geoLink.getGeoCoordinate(), (CellLatitudeLongitudeDataObject) geoLink.getGeoCell(), geoLink.getStatus());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new GeoLinkDataObject(geoLink.getGuid(), geoLink.getShortLink(), geoLink.getShortUrl(), (GeoCoordinateStructDataObject) geoLink.getGeoCoordinate(), (CellLatitudeLongitudeDataObject) geoLink.getGeoCell(), geoLink.getStatus());
        }
        String guid = getDAOFactory().getGeoLinkDAO().createGeoLink(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateGeoLink(String guid, String shortLink, String shortUrl, GeoCoordinateStruct geoCoordinate, CellLatitudeLongitude geoCell, String status) throws BaseException
    {
        GeoCoordinateStructDataObject geoCoordinateDobj = null;
        if(geoCoordinate instanceof GeoCoordinateStructBean) {
            geoCoordinateDobj = ((GeoCoordinateStructBean) geoCoordinate).toDataObject();            
        } else if(geoCoordinate instanceof GeoCoordinateStruct) {
            geoCoordinateDobj = new GeoCoordinateStructDataObject(geoCoordinate.getUuid(), geoCoordinate.getLatitude(), geoCoordinate.getLongitude(), geoCoordinate.getAltitude(), geoCoordinate.isSensorUsed(), geoCoordinate.getAccuracy(), geoCoordinate.getAltitudeAccuracy(), geoCoordinate.getHeading(), geoCoordinate.getSpeed(), geoCoordinate.getNote());
        } else {
            geoCoordinateDobj = null;   // ????
        }
        CellLatitudeLongitudeDataObject geoCellDobj = null;
        if(geoCell instanceof CellLatitudeLongitudeBean) {
            geoCellDobj = ((CellLatitudeLongitudeBean) geoCell).toDataObject();            
        } else if(geoCell instanceof CellLatitudeLongitude) {
            geoCellDobj = new CellLatitudeLongitudeDataObject(geoCell.getScale(), geoCell.getLatitude(), geoCell.getLongitude());
        } else {
            geoCellDobj = null;   // ????
        }

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        GeoLinkDataObject dataObj = new GeoLinkDataObject(guid, shortLink, shortUrl, geoCoordinateDobj, geoCellDobj, status);
        return updateGeoLink(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateGeoLink(GeoLink geoLink) throws BaseException
    {
        log.finer("BEGIN");

        // Param geoLink cannot be null.....
        if(geoLink == null || geoLink.getGuid() == null) {
            log.log(Level.INFO, "Param geoLink or its guid is null!");
            throw new BadRequestException("Param geoLink object or its guid is null!");
        }
        GeoLinkDataObject dataObj = null;
        if(geoLink instanceof GeoLinkDataObject) {
            dataObj = (GeoLinkDataObject) geoLink;
        } else if(geoLink instanceof GeoLinkBean) {
            dataObj = ((GeoLinkBean) geoLink).toDataObject();
        } else {  // if(geoLink instanceof GeoLink)
            dataObj = new GeoLinkDataObject(geoLink.getGuid(), geoLink.getShortLink(), geoLink.getShortUrl(), geoLink.getGeoCoordinate(), geoLink.getGeoCell(), geoLink.getStatus());
        }
        Boolean suc = getDAOFactory().getGeoLinkDAO().updateGeoLink(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteGeoLink(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getGeoLinkDAO().deleteGeoLink(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteGeoLink(GeoLink geoLink) throws BaseException
    {
        log.finer("BEGIN");

        // Param geoLink cannot be null.....
        if(geoLink == null || geoLink.getGuid() == null) {
            log.log(Level.INFO, "Param geoLink or its guid is null!");
            throw new BadRequestException("Param geoLink object or its guid is null!");
        }
        GeoLinkDataObject dataObj = null;
        if(geoLink instanceof GeoLinkDataObject) {
            dataObj = (GeoLinkDataObject) geoLink;
        } else if(geoLink instanceof GeoLinkBean) {
            dataObj = ((GeoLinkBean) geoLink).toDataObject();
        } else {  // if(geoLink instanceof GeoLink)
            dataObj = new GeoLinkDataObject(geoLink.getGuid(), geoLink.getShortLink(), geoLink.getShortUrl(), geoLink.getGeoCoordinate(), geoLink.getGeoCell(), geoLink.getStatus());
        }
        Boolean suc = getDAOFactory().getGeoLinkDAO().deleteGeoLink(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteGeoLinks(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getGeoLinkDAO().deleteGeoLinks(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
