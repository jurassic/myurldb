package com.myurldb.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.KeywordCrowdTally;
import com.myurldb.ws.bean.KeywordCrowdTallyBean;
import com.myurldb.ws.dao.DAOFactory;
import com.myurldb.ws.data.KeywordCrowdTallyDataObject;
import com.myurldb.ws.service.DAOFactoryManager;
import com.myurldb.ws.service.KeywordCrowdTallyService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class KeywordCrowdTallyServiceImpl implements KeywordCrowdTallyService
{
    private static final Logger log = Logger.getLogger(KeywordCrowdTallyServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // KeywordCrowdTally related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public KeywordCrowdTally getKeywordCrowdTally(String guid) throws BaseException
    {
        log.finer("BEGIN");

        KeywordCrowdTallyDataObject dataObj = getDAOFactory().getKeywordCrowdTallyDAO().getKeywordCrowdTally(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve KeywordCrowdTallyDataObject for guid = " + guid);
            return null;  // ????
        }
        KeywordCrowdTallyBean bean = new KeywordCrowdTallyBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getKeywordCrowdTally(String guid, String field) throws BaseException
    {
        KeywordCrowdTallyDataObject dataObj = getDAOFactory().getKeywordCrowdTallyDAO().getKeywordCrowdTally(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve KeywordCrowdTallyDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return dataObj.getUser();
        } else if(field.equals("shortLink")) {
            return dataObj.getShortLink();
        } else if(field.equals("domain")) {
            return dataObj.getDomain();
        } else if(field.equals("token")) {
            return dataObj.getToken();
        } else if(field.equals("longUrl")) {
            return dataObj.getLongUrl();
        } else if(field.equals("shortUrl")) {
            return dataObj.getShortUrl();
        } else if(field.equals("tallyDate")) {
            return dataObj.getTallyDate();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("note")) {
            return dataObj.getNote();
        } else if(field.equals("expirationTime")) {
            return dataObj.getExpirationTime();
        } else if(field.equals("keywordFolder")) {
            return dataObj.getKeywordFolder();
        } else if(field.equals("folderPath")) {
            return dataObj.getFolderPath();
        } else if(field.equals("keyword")) {
            return dataObj.getKeyword();
        } else if(field.equals("queryKey")) {
            return dataObj.getQueryKey();
        } else if(field.equals("scope")) {
            return dataObj.getScope();
        } else if(field.equals("caseSensitive")) {
            return dataObj.isCaseSensitive();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<KeywordCrowdTally> getKeywordCrowdTallies(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<KeywordCrowdTally> list = new ArrayList<KeywordCrowdTally>();
        List<KeywordCrowdTallyDataObject> dataObjs = getDAOFactory().getKeywordCrowdTallyDAO().getKeywordCrowdTallies(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve KeywordCrowdTallyDataObject list.");
        } else {
            Iterator<KeywordCrowdTallyDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                KeywordCrowdTallyDataObject dataObj = (KeywordCrowdTallyDataObject) it.next();
                list.add(new KeywordCrowdTallyBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<KeywordCrowdTally> getAllKeywordCrowdTallies() throws BaseException
    {
        return getAllKeywordCrowdTallies(null, null, null);
    }

    @Override
    public List<KeywordCrowdTally> getAllKeywordCrowdTallies(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<KeywordCrowdTally> list = new ArrayList<KeywordCrowdTally>();
        List<KeywordCrowdTallyDataObject> dataObjs = getDAOFactory().getKeywordCrowdTallyDAO().getAllKeywordCrowdTallies(ordering, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve KeywordCrowdTallyDataObject list.");
        } else {
            Iterator<KeywordCrowdTallyDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                KeywordCrowdTallyDataObject dataObj = (KeywordCrowdTallyDataObject) it.next();
                list.add(new KeywordCrowdTallyBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllKeywordCrowdTallyKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        List<String> keys = getDAOFactory().getKeywordCrowdTallyDAO().getAllKeywordCrowdTallyKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve KeywordCrowdTally key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<KeywordCrowdTally> findKeywordCrowdTallies(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findKeywordCrowdTallies(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<KeywordCrowdTally> findKeywordCrowdTallies(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("KeywordCrowdTallyServiceImpl.findKeywordCrowdTallies(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<KeywordCrowdTally> list = new ArrayList<KeywordCrowdTally>();
        List<KeywordCrowdTallyDataObject> dataObjs = getDAOFactory().getKeywordCrowdTallyDAO().findKeywordCrowdTallies(filter, ordering, params, values, grouping, unique, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find keywordCrowdTallies for the given criterion.");
        } else {
            Iterator<KeywordCrowdTallyDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                KeywordCrowdTallyDataObject dataObj = (KeywordCrowdTallyDataObject) it.next();
                list.add(new KeywordCrowdTallyBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findKeywordCrowdTallyKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("KeywordCrowdTallyServiceImpl.findKeywordCrowdTallyKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = getDAOFactory().getKeywordCrowdTallyDAO().findKeywordCrowdTallyKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find KeywordCrowdTally keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("KeywordCrowdTallyServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getKeywordCrowdTallyDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createKeywordCrowdTally(String user, String shortLink, String domain, String token, String longUrl, String shortUrl, String tallyDate, String status, String note, Long expirationTime, String keywordFolder, String folderPath, String keyword, String queryKey, String scope, Boolean caseSensitive) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        KeywordCrowdTallyDataObject dataObj = new KeywordCrowdTallyDataObject(null, user, shortLink, domain, token, longUrl, shortUrl, tallyDate, status, note, expirationTime, keywordFolder, folderPath, keyword, queryKey, scope, caseSensitive);
        return createKeywordCrowdTally(dataObj);
    }

    @Override
    public String createKeywordCrowdTally(KeywordCrowdTally keywordCrowdTally) throws BaseException
    {
        log.finer("BEGIN");

        // Param keywordCrowdTally cannot be null.....
        if(keywordCrowdTally == null) {
            log.log(Level.INFO, "Param keywordCrowdTally is null!");
            throw new BadRequestException("Param keywordCrowdTally object is null!");
        }
        KeywordCrowdTallyDataObject dataObj = null;
        if(keywordCrowdTally instanceof KeywordCrowdTallyDataObject) {
            dataObj = (KeywordCrowdTallyDataObject) keywordCrowdTally;
        } else if(keywordCrowdTally instanceof KeywordCrowdTallyBean) {
            dataObj = ((KeywordCrowdTallyBean) keywordCrowdTally).toDataObject();
        } else {  // if(keywordCrowdTally instanceof KeywordCrowdTally)
            //dataObj = new KeywordCrowdTallyDataObject(null, keywordCrowdTally.getUser(), keywordCrowdTally.getShortLink(), keywordCrowdTally.getDomain(), keywordCrowdTally.getToken(), keywordCrowdTally.getLongUrl(), keywordCrowdTally.getShortUrl(), keywordCrowdTally.getTallyDate(), keywordCrowdTally.getStatus(), keywordCrowdTally.getNote(), keywordCrowdTally.getExpirationTime(), keywordCrowdTally.getKeywordFolder(), keywordCrowdTally.getFolderPath(), keywordCrowdTally.getKeyword(), keywordCrowdTally.getQueryKey(), keywordCrowdTally.getScope(), keywordCrowdTally.isCaseSensitive());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new KeywordCrowdTallyDataObject(keywordCrowdTally.getGuid(), keywordCrowdTally.getUser(), keywordCrowdTally.getShortLink(), keywordCrowdTally.getDomain(), keywordCrowdTally.getToken(), keywordCrowdTally.getLongUrl(), keywordCrowdTally.getShortUrl(), keywordCrowdTally.getTallyDate(), keywordCrowdTally.getStatus(), keywordCrowdTally.getNote(), keywordCrowdTally.getExpirationTime(), keywordCrowdTally.getKeywordFolder(), keywordCrowdTally.getFolderPath(), keywordCrowdTally.getKeyword(), keywordCrowdTally.getQueryKey(), keywordCrowdTally.getScope(), keywordCrowdTally.isCaseSensitive());
        }
        String guid = getDAOFactory().getKeywordCrowdTallyDAO().createKeywordCrowdTally(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateKeywordCrowdTally(String guid, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, String tallyDate, String status, String note, Long expirationTime, String keywordFolder, String folderPath, String keyword, String queryKey, String scope, Boolean caseSensitive) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        KeywordCrowdTallyDataObject dataObj = new KeywordCrowdTallyDataObject(guid, user, shortLink, domain, token, longUrl, shortUrl, tallyDate, status, note, expirationTime, keywordFolder, folderPath, keyword, queryKey, scope, caseSensitive);
        return updateKeywordCrowdTally(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateKeywordCrowdTally(KeywordCrowdTally keywordCrowdTally) throws BaseException
    {
        log.finer("BEGIN");

        // Param keywordCrowdTally cannot be null.....
        if(keywordCrowdTally == null || keywordCrowdTally.getGuid() == null) {
            log.log(Level.INFO, "Param keywordCrowdTally or its guid is null!");
            throw new BadRequestException("Param keywordCrowdTally object or its guid is null!");
        }
        KeywordCrowdTallyDataObject dataObj = null;
        if(keywordCrowdTally instanceof KeywordCrowdTallyDataObject) {
            dataObj = (KeywordCrowdTallyDataObject) keywordCrowdTally;
        } else if(keywordCrowdTally instanceof KeywordCrowdTallyBean) {
            dataObj = ((KeywordCrowdTallyBean) keywordCrowdTally).toDataObject();
        } else {  // if(keywordCrowdTally instanceof KeywordCrowdTally)
            dataObj = new KeywordCrowdTallyDataObject(keywordCrowdTally.getGuid(), keywordCrowdTally.getUser(), keywordCrowdTally.getShortLink(), keywordCrowdTally.getDomain(), keywordCrowdTally.getToken(), keywordCrowdTally.getLongUrl(), keywordCrowdTally.getShortUrl(), keywordCrowdTally.getTallyDate(), keywordCrowdTally.getStatus(), keywordCrowdTally.getNote(), keywordCrowdTally.getExpirationTime(), keywordCrowdTally.getKeywordFolder(), keywordCrowdTally.getFolderPath(), keywordCrowdTally.getKeyword(), keywordCrowdTally.getQueryKey(), keywordCrowdTally.getScope(), keywordCrowdTally.isCaseSensitive());
        }
        Boolean suc = getDAOFactory().getKeywordCrowdTallyDAO().updateKeywordCrowdTally(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteKeywordCrowdTally(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getKeywordCrowdTallyDAO().deleteKeywordCrowdTally(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteKeywordCrowdTally(KeywordCrowdTally keywordCrowdTally) throws BaseException
    {
        log.finer("BEGIN");

        // Param keywordCrowdTally cannot be null.....
        if(keywordCrowdTally == null || keywordCrowdTally.getGuid() == null) {
            log.log(Level.INFO, "Param keywordCrowdTally or its guid is null!");
            throw new BadRequestException("Param keywordCrowdTally object or its guid is null!");
        }
        KeywordCrowdTallyDataObject dataObj = null;
        if(keywordCrowdTally instanceof KeywordCrowdTallyDataObject) {
            dataObj = (KeywordCrowdTallyDataObject) keywordCrowdTally;
        } else if(keywordCrowdTally instanceof KeywordCrowdTallyBean) {
            dataObj = ((KeywordCrowdTallyBean) keywordCrowdTally).toDataObject();
        } else {  // if(keywordCrowdTally instanceof KeywordCrowdTally)
            dataObj = new KeywordCrowdTallyDataObject(keywordCrowdTally.getGuid(), keywordCrowdTally.getUser(), keywordCrowdTally.getShortLink(), keywordCrowdTally.getDomain(), keywordCrowdTally.getToken(), keywordCrowdTally.getLongUrl(), keywordCrowdTally.getShortUrl(), keywordCrowdTally.getTallyDate(), keywordCrowdTally.getStatus(), keywordCrowdTally.getNote(), keywordCrowdTally.getExpirationTime(), keywordCrowdTally.getKeywordFolder(), keywordCrowdTally.getFolderPath(), keywordCrowdTally.getKeyword(), keywordCrowdTally.getQueryKey(), keywordCrowdTally.getScope(), keywordCrowdTally.isCaseSensitive());
        }
        Boolean suc = getDAOFactory().getKeywordCrowdTallyDAO().deleteKeywordCrowdTally(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteKeywordCrowdTallies(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getKeywordCrowdTallyDAO().deleteKeywordCrowdTallies(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
