package com.myurldb.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.KeywordFolderImport;
import com.myurldb.ws.bean.KeywordFolderImportBean;
import com.myurldb.ws.dao.DAOFactory;
import com.myurldb.ws.data.KeywordFolderImportDataObject;
import com.myurldb.ws.service.DAOFactoryManager;
import com.myurldb.ws.service.KeywordFolderImportService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class KeywordFolderImportServiceImpl implements KeywordFolderImportService
{
    private static final Logger log = Logger.getLogger(KeywordFolderImportServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // KeywordFolderImport related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public KeywordFolderImport getKeywordFolderImport(String guid) throws BaseException
    {
        log.finer("BEGIN");

        KeywordFolderImportDataObject dataObj = getDAOFactory().getKeywordFolderImportDAO().getKeywordFolderImport(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve KeywordFolderImportDataObject for guid = " + guid);
            return null;  // ????
        }
        KeywordFolderImportBean bean = new KeywordFolderImportBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getKeywordFolderImport(String guid, String field) throws BaseException
    {
        KeywordFolderImportDataObject dataObj = getDAOFactory().getKeywordFolderImportDAO().getKeywordFolderImport(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve KeywordFolderImportDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return dataObj.getUser();
        } else if(field.equals("precedence")) {
            return dataObj.getPrecedence();
        } else if(field.equals("importType")) {
            return dataObj.getImportType();
        } else if(field.equals("importedFolder")) {
            return dataObj.getImportedFolder();
        } else if(field.equals("importedFolderUser")) {
            return dataObj.getImportedFolderUser();
        } else if(field.equals("importedFolderTitle")) {
            return dataObj.getImportedFolderTitle();
        } else if(field.equals("importedFolderPath")) {
            return dataObj.getImportedFolderPath();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("note")) {
            return dataObj.getNote();
        } else if(field.equals("keywordFolder")) {
            return dataObj.getKeywordFolder();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<KeywordFolderImport> getKeywordFolderImports(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<KeywordFolderImport> list = new ArrayList<KeywordFolderImport>();
        List<KeywordFolderImportDataObject> dataObjs = getDAOFactory().getKeywordFolderImportDAO().getKeywordFolderImports(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve KeywordFolderImportDataObject list.");
        } else {
            Iterator<KeywordFolderImportDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                KeywordFolderImportDataObject dataObj = (KeywordFolderImportDataObject) it.next();
                list.add(new KeywordFolderImportBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<KeywordFolderImport> getAllKeywordFolderImports() throws BaseException
    {
        return getAllKeywordFolderImports(null, null, null);
    }

    @Override
    public List<KeywordFolderImport> getAllKeywordFolderImports(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<KeywordFolderImport> list = new ArrayList<KeywordFolderImport>();
        List<KeywordFolderImportDataObject> dataObjs = getDAOFactory().getKeywordFolderImportDAO().getAllKeywordFolderImports(ordering, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve KeywordFolderImportDataObject list.");
        } else {
            Iterator<KeywordFolderImportDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                KeywordFolderImportDataObject dataObj = (KeywordFolderImportDataObject) it.next();
                list.add(new KeywordFolderImportBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllKeywordFolderImportKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        List<String> keys = getDAOFactory().getKeywordFolderImportDAO().getAllKeywordFolderImportKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve KeywordFolderImport key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<KeywordFolderImport> findKeywordFolderImports(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findKeywordFolderImports(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<KeywordFolderImport> findKeywordFolderImports(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("KeywordFolderImportServiceImpl.findKeywordFolderImports(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<KeywordFolderImport> list = new ArrayList<KeywordFolderImport>();
        List<KeywordFolderImportDataObject> dataObjs = getDAOFactory().getKeywordFolderImportDAO().findKeywordFolderImports(filter, ordering, params, values, grouping, unique, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find keywordFolderImports for the given criterion.");
        } else {
            Iterator<KeywordFolderImportDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                KeywordFolderImportDataObject dataObj = (KeywordFolderImportDataObject) it.next();
                list.add(new KeywordFolderImportBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findKeywordFolderImportKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("KeywordFolderImportServiceImpl.findKeywordFolderImportKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = getDAOFactory().getKeywordFolderImportDAO().findKeywordFolderImportKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find KeywordFolderImport keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("KeywordFolderImportServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getKeywordFolderImportDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createKeywordFolderImport(String user, Integer precedence, String importType, String importedFolder, String importedFolderUser, String importedFolderTitle, String importedFolderPath, String status, String note, String keywordFolder) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        KeywordFolderImportDataObject dataObj = new KeywordFolderImportDataObject(null, user, precedence, importType, importedFolder, importedFolderUser, importedFolderTitle, importedFolderPath, status, note, keywordFolder);
        return createKeywordFolderImport(dataObj);
    }

    @Override
    public String createKeywordFolderImport(KeywordFolderImport keywordFolderImport) throws BaseException
    {
        log.finer("BEGIN");

        // Param keywordFolderImport cannot be null.....
        if(keywordFolderImport == null) {
            log.log(Level.INFO, "Param keywordFolderImport is null!");
            throw new BadRequestException("Param keywordFolderImport object is null!");
        }
        KeywordFolderImportDataObject dataObj = null;
        if(keywordFolderImport instanceof KeywordFolderImportDataObject) {
            dataObj = (KeywordFolderImportDataObject) keywordFolderImport;
        } else if(keywordFolderImport instanceof KeywordFolderImportBean) {
            dataObj = ((KeywordFolderImportBean) keywordFolderImport).toDataObject();
        } else {  // if(keywordFolderImport instanceof KeywordFolderImport)
            //dataObj = new KeywordFolderImportDataObject(null, keywordFolderImport.getUser(), keywordFolderImport.getPrecedence(), keywordFolderImport.getImportType(), keywordFolderImport.getImportedFolder(), keywordFolderImport.getImportedFolderUser(), keywordFolderImport.getImportedFolderTitle(), keywordFolderImport.getImportedFolderPath(), keywordFolderImport.getStatus(), keywordFolderImport.getNote(), keywordFolderImport.getKeywordFolder());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new KeywordFolderImportDataObject(keywordFolderImport.getGuid(), keywordFolderImport.getUser(), keywordFolderImport.getPrecedence(), keywordFolderImport.getImportType(), keywordFolderImport.getImportedFolder(), keywordFolderImport.getImportedFolderUser(), keywordFolderImport.getImportedFolderTitle(), keywordFolderImport.getImportedFolderPath(), keywordFolderImport.getStatus(), keywordFolderImport.getNote(), keywordFolderImport.getKeywordFolder());
        }
        String guid = getDAOFactory().getKeywordFolderImportDAO().createKeywordFolderImport(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateKeywordFolderImport(String guid, String user, Integer precedence, String importType, String importedFolder, String importedFolderUser, String importedFolderTitle, String importedFolderPath, String status, String note, String keywordFolder) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        KeywordFolderImportDataObject dataObj = new KeywordFolderImportDataObject(guid, user, precedence, importType, importedFolder, importedFolderUser, importedFolderTitle, importedFolderPath, status, note, keywordFolder);
        return updateKeywordFolderImport(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateKeywordFolderImport(KeywordFolderImport keywordFolderImport) throws BaseException
    {
        log.finer("BEGIN");

        // Param keywordFolderImport cannot be null.....
        if(keywordFolderImport == null || keywordFolderImport.getGuid() == null) {
            log.log(Level.INFO, "Param keywordFolderImport or its guid is null!");
            throw new BadRequestException("Param keywordFolderImport object or its guid is null!");
        }
        KeywordFolderImportDataObject dataObj = null;
        if(keywordFolderImport instanceof KeywordFolderImportDataObject) {
            dataObj = (KeywordFolderImportDataObject) keywordFolderImport;
        } else if(keywordFolderImport instanceof KeywordFolderImportBean) {
            dataObj = ((KeywordFolderImportBean) keywordFolderImport).toDataObject();
        } else {  // if(keywordFolderImport instanceof KeywordFolderImport)
            dataObj = new KeywordFolderImportDataObject(keywordFolderImport.getGuid(), keywordFolderImport.getUser(), keywordFolderImport.getPrecedence(), keywordFolderImport.getImportType(), keywordFolderImport.getImportedFolder(), keywordFolderImport.getImportedFolderUser(), keywordFolderImport.getImportedFolderTitle(), keywordFolderImport.getImportedFolderPath(), keywordFolderImport.getStatus(), keywordFolderImport.getNote(), keywordFolderImport.getKeywordFolder());
        }
        Boolean suc = getDAOFactory().getKeywordFolderImportDAO().updateKeywordFolderImport(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteKeywordFolderImport(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getKeywordFolderImportDAO().deleteKeywordFolderImport(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteKeywordFolderImport(KeywordFolderImport keywordFolderImport) throws BaseException
    {
        log.finer("BEGIN");

        // Param keywordFolderImport cannot be null.....
        if(keywordFolderImport == null || keywordFolderImport.getGuid() == null) {
            log.log(Level.INFO, "Param keywordFolderImport or its guid is null!");
            throw new BadRequestException("Param keywordFolderImport object or its guid is null!");
        }
        KeywordFolderImportDataObject dataObj = null;
        if(keywordFolderImport instanceof KeywordFolderImportDataObject) {
            dataObj = (KeywordFolderImportDataObject) keywordFolderImport;
        } else if(keywordFolderImport instanceof KeywordFolderImportBean) {
            dataObj = ((KeywordFolderImportBean) keywordFolderImport).toDataObject();
        } else {  // if(keywordFolderImport instanceof KeywordFolderImport)
            dataObj = new KeywordFolderImportDataObject(keywordFolderImport.getGuid(), keywordFolderImport.getUser(), keywordFolderImport.getPrecedence(), keywordFolderImport.getImportType(), keywordFolderImport.getImportedFolder(), keywordFolderImport.getImportedFolderUser(), keywordFolderImport.getImportedFolderTitle(), keywordFolderImport.getImportedFolderPath(), keywordFolderImport.getStatus(), keywordFolderImport.getNote(), keywordFolderImport.getKeywordFolder());
        }
        Boolean suc = getDAOFactory().getKeywordFolderImportDAO().deleteKeywordFolderImport(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteKeywordFolderImports(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getKeywordFolderImportDAO().deleteKeywordFolderImports(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
