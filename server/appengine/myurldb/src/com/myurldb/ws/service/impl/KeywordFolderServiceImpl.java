package com.myurldb.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.KeywordFolder;
import com.myurldb.ws.bean.KeywordFolderBean;
import com.myurldb.ws.dao.DAOFactory;
import com.myurldb.ws.data.KeywordFolderDataObject;
import com.myurldb.ws.service.DAOFactoryManager;
import com.myurldb.ws.service.KeywordFolderService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class KeywordFolderServiceImpl implements KeywordFolderService
{
    private static final Logger log = Logger.getLogger(KeywordFolderServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // KeywordFolder related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public KeywordFolder getKeywordFolder(String guid) throws BaseException
    {
        log.finer("BEGIN");

        KeywordFolderDataObject dataObj = getDAOFactory().getKeywordFolderDAO().getKeywordFolder(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve KeywordFolderDataObject for guid = " + guid);
            return null;  // ????
        }
        KeywordFolderBean bean = new KeywordFolderBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getKeywordFolder(String guid, String field) throws BaseException
    {
        KeywordFolderDataObject dataObj = getDAOFactory().getKeywordFolderDAO().getKeywordFolder(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve KeywordFolderDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("appClient")) {
            return dataObj.getAppClient();
        } else if(field.equals("clientRootDomain")) {
            return dataObj.getClientRootDomain();
        } else if(field.equals("user")) {
            return dataObj.getUser();
        } else if(field.equals("title")) {
            return dataObj.getTitle();
        } else if(field.equals("description")) {
            return dataObj.getDescription();
        } else if(field.equals("type")) {
            return dataObj.getType();
        } else if(field.equals("category")) {
            return dataObj.getCategory();
        } else if(field.equals("parent")) {
            return dataObj.getParent();
        } else if(field.equals("aggregate")) {
            return dataObj.getAggregate();
        } else if(field.equals("acl")) {
            return dataObj.getAcl();
        } else if(field.equals("exportable")) {
            return dataObj.isExportable();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("note")) {
            return dataObj.getNote();
        } else if(field.equals("folderPath")) {
            return dataObj.getFolderPath();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<KeywordFolder> getKeywordFolders(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<KeywordFolder> list = new ArrayList<KeywordFolder>();
        List<KeywordFolderDataObject> dataObjs = getDAOFactory().getKeywordFolderDAO().getKeywordFolders(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve KeywordFolderDataObject list.");
        } else {
            Iterator<KeywordFolderDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                KeywordFolderDataObject dataObj = (KeywordFolderDataObject) it.next();
                list.add(new KeywordFolderBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<KeywordFolder> getAllKeywordFolders() throws BaseException
    {
        return getAllKeywordFolders(null, null, null);
    }

    @Override
    public List<KeywordFolder> getAllKeywordFolders(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<KeywordFolder> list = new ArrayList<KeywordFolder>();
        List<KeywordFolderDataObject> dataObjs = getDAOFactory().getKeywordFolderDAO().getAllKeywordFolders(ordering, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve KeywordFolderDataObject list.");
        } else {
            Iterator<KeywordFolderDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                KeywordFolderDataObject dataObj = (KeywordFolderDataObject) it.next();
                list.add(new KeywordFolderBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllKeywordFolderKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        List<String> keys = getDAOFactory().getKeywordFolderDAO().getAllKeywordFolderKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve KeywordFolder key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<KeywordFolder> findKeywordFolders(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findKeywordFolders(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<KeywordFolder> findKeywordFolders(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("KeywordFolderServiceImpl.findKeywordFolders(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<KeywordFolder> list = new ArrayList<KeywordFolder>();
        List<KeywordFolderDataObject> dataObjs = getDAOFactory().getKeywordFolderDAO().findKeywordFolders(filter, ordering, params, values, grouping, unique, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find keywordFolders for the given criterion.");
        } else {
            Iterator<KeywordFolderDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                KeywordFolderDataObject dataObj = (KeywordFolderDataObject) it.next();
                list.add(new KeywordFolderBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findKeywordFolderKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("KeywordFolderServiceImpl.findKeywordFolderKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = getDAOFactory().getKeywordFolderDAO().findKeywordFolderKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find KeywordFolder keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("KeywordFolderServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getKeywordFolderDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createKeywordFolder(String appClient, String clientRootDomain, String user, String title, String description, String type, String category, String parent, String aggregate, String acl, Boolean exportable, String status, String note, String folderPath) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        KeywordFolderDataObject dataObj = new KeywordFolderDataObject(null, appClient, clientRootDomain, user, title, description, type, category, parent, aggregate, acl, exportable, status, note, folderPath);
        return createKeywordFolder(dataObj);
    }

    @Override
    public String createKeywordFolder(KeywordFolder keywordFolder) throws BaseException
    {
        log.finer("BEGIN");

        // Param keywordFolder cannot be null.....
        if(keywordFolder == null) {
            log.log(Level.INFO, "Param keywordFolder is null!");
            throw new BadRequestException("Param keywordFolder object is null!");
        }
        KeywordFolderDataObject dataObj = null;
        if(keywordFolder instanceof KeywordFolderDataObject) {
            dataObj = (KeywordFolderDataObject) keywordFolder;
        } else if(keywordFolder instanceof KeywordFolderBean) {
            dataObj = ((KeywordFolderBean) keywordFolder).toDataObject();
        } else {  // if(keywordFolder instanceof KeywordFolder)
            //dataObj = new KeywordFolderDataObject(null, keywordFolder.getAppClient(), keywordFolder.getClientRootDomain(), keywordFolder.getUser(), keywordFolder.getTitle(), keywordFolder.getDescription(), keywordFolder.getType(), keywordFolder.getCategory(), keywordFolder.getParent(), keywordFolder.getAggregate(), keywordFolder.getAcl(), keywordFolder.isExportable(), keywordFolder.getStatus(), keywordFolder.getNote(), keywordFolder.getFolderPath());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new KeywordFolderDataObject(keywordFolder.getGuid(), keywordFolder.getAppClient(), keywordFolder.getClientRootDomain(), keywordFolder.getUser(), keywordFolder.getTitle(), keywordFolder.getDescription(), keywordFolder.getType(), keywordFolder.getCategory(), keywordFolder.getParent(), keywordFolder.getAggregate(), keywordFolder.getAcl(), keywordFolder.isExportable(), keywordFolder.getStatus(), keywordFolder.getNote(), keywordFolder.getFolderPath());
        }
        String guid = getDAOFactory().getKeywordFolderDAO().createKeywordFolder(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateKeywordFolder(String guid, String appClient, String clientRootDomain, String user, String title, String description, String type, String category, String parent, String aggregate, String acl, Boolean exportable, String status, String note, String folderPath) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        KeywordFolderDataObject dataObj = new KeywordFolderDataObject(guid, appClient, clientRootDomain, user, title, description, type, category, parent, aggregate, acl, exportable, status, note, folderPath);
        return updateKeywordFolder(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateKeywordFolder(KeywordFolder keywordFolder) throws BaseException
    {
        log.finer("BEGIN");

        // Param keywordFolder cannot be null.....
        if(keywordFolder == null || keywordFolder.getGuid() == null) {
            log.log(Level.INFO, "Param keywordFolder or its guid is null!");
            throw new BadRequestException("Param keywordFolder object or its guid is null!");
        }
        KeywordFolderDataObject dataObj = null;
        if(keywordFolder instanceof KeywordFolderDataObject) {
            dataObj = (KeywordFolderDataObject) keywordFolder;
        } else if(keywordFolder instanceof KeywordFolderBean) {
            dataObj = ((KeywordFolderBean) keywordFolder).toDataObject();
        } else {  // if(keywordFolder instanceof KeywordFolder)
            dataObj = new KeywordFolderDataObject(keywordFolder.getGuid(), keywordFolder.getAppClient(), keywordFolder.getClientRootDomain(), keywordFolder.getUser(), keywordFolder.getTitle(), keywordFolder.getDescription(), keywordFolder.getType(), keywordFolder.getCategory(), keywordFolder.getParent(), keywordFolder.getAggregate(), keywordFolder.getAcl(), keywordFolder.isExportable(), keywordFolder.getStatus(), keywordFolder.getNote(), keywordFolder.getFolderPath());
        }
        Boolean suc = getDAOFactory().getKeywordFolderDAO().updateKeywordFolder(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteKeywordFolder(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getKeywordFolderDAO().deleteKeywordFolder(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteKeywordFolder(KeywordFolder keywordFolder) throws BaseException
    {
        log.finer("BEGIN");

        // Param keywordFolder cannot be null.....
        if(keywordFolder == null || keywordFolder.getGuid() == null) {
            log.log(Level.INFO, "Param keywordFolder or its guid is null!");
            throw new BadRequestException("Param keywordFolder object or its guid is null!");
        }
        KeywordFolderDataObject dataObj = null;
        if(keywordFolder instanceof KeywordFolderDataObject) {
            dataObj = (KeywordFolderDataObject) keywordFolder;
        } else if(keywordFolder instanceof KeywordFolderBean) {
            dataObj = ((KeywordFolderBean) keywordFolder).toDataObject();
        } else {  // if(keywordFolder instanceof KeywordFolder)
            dataObj = new KeywordFolderDataObject(keywordFolder.getGuid(), keywordFolder.getAppClient(), keywordFolder.getClientRootDomain(), keywordFolder.getUser(), keywordFolder.getTitle(), keywordFolder.getDescription(), keywordFolder.getType(), keywordFolder.getCategory(), keywordFolder.getParent(), keywordFolder.getAggregate(), keywordFolder.getAcl(), keywordFolder.isExportable(), keywordFolder.getStatus(), keywordFolder.getNote(), keywordFolder.getFolderPath());
        }
        Boolean suc = getDAOFactory().getKeywordFolderDAO().deleteKeywordFolder(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteKeywordFolders(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getKeywordFolderDAO().deleteKeywordFolders(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
