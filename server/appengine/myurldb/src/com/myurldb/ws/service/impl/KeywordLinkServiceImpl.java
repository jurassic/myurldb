package com.myurldb.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.KeywordLink;
import com.myurldb.ws.bean.KeywordLinkBean;
import com.myurldb.ws.dao.DAOFactory;
import com.myurldb.ws.data.KeywordLinkDataObject;
import com.myurldb.ws.service.DAOFactoryManager;
import com.myurldb.ws.service.KeywordLinkService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class KeywordLinkServiceImpl implements KeywordLinkService
{
    private static final Logger log = Logger.getLogger(KeywordLinkServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // KeywordLink related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public KeywordLink getKeywordLink(String guid) throws BaseException
    {
        log.finer("BEGIN");

        KeywordLinkDataObject dataObj = getDAOFactory().getKeywordLinkDAO().getKeywordLink(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve KeywordLinkDataObject for guid = " + guid);
            return null;  // ????
        }
        KeywordLinkBean bean = new KeywordLinkBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getKeywordLink(String guid, String field) throws BaseException
    {
        KeywordLinkDataObject dataObj = getDAOFactory().getKeywordLinkDAO().getKeywordLink(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve KeywordLinkDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("appClient")) {
            return dataObj.getAppClient();
        } else if(field.equals("clientRootDomain")) {
            return dataObj.getClientRootDomain();
        } else if(field.equals("user")) {
            return dataObj.getUser();
        } else if(field.equals("shortLink")) {
            return dataObj.getShortLink();
        } else if(field.equals("domain")) {
            return dataObj.getDomain();
        } else if(field.equals("token")) {
            return dataObj.getToken();
        } else if(field.equals("longUrl")) {
            return dataObj.getLongUrl();
        } else if(field.equals("shortUrl")) {
            return dataObj.getShortUrl();
        } else if(field.equals("internal")) {
            return dataObj.isInternal();
        } else if(field.equals("caching")) {
            return dataObj.isCaching();
        } else if(field.equals("memo")) {
            return dataObj.getMemo();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("note")) {
            return dataObj.getNote();
        } else if(field.equals("expirationTime")) {
            return dataObj.getExpirationTime();
        } else if(field.equals("keywordFolder")) {
            return dataObj.getKeywordFolder();
        } else if(field.equals("folderPath")) {
            return dataObj.getFolderPath();
        } else if(field.equals("keyword")) {
            return dataObj.getKeyword();
        } else if(field.equals("queryKey")) {
            return dataObj.getQueryKey();
        } else if(field.equals("scope")) {
            return dataObj.getScope();
        } else if(field.equals("dynamic")) {
            return dataObj.isDynamic();
        } else if(field.equals("caseSensitive")) {
            return dataObj.isCaseSensitive();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<KeywordLink> getKeywordLinks(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<KeywordLink> list = new ArrayList<KeywordLink>();
        List<KeywordLinkDataObject> dataObjs = getDAOFactory().getKeywordLinkDAO().getKeywordLinks(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve KeywordLinkDataObject list.");
        } else {
            Iterator<KeywordLinkDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                KeywordLinkDataObject dataObj = (KeywordLinkDataObject) it.next();
                list.add(new KeywordLinkBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<KeywordLink> getAllKeywordLinks() throws BaseException
    {
        return getAllKeywordLinks(null, null, null);
    }

    @Override
    public List<KeywordLink> getAllKeywordLinks(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<KeywordLink> list = new ArrayList<KeywordLink>();
        List<KeywordLinkDataObject> dataObjs = getDAOFactory().getKeywordLinkDAO().getAllKeywordLinks(ordering, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve KeywordLinkDataObject list.");
        } else {
            Iterator<KeywordLinkDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                KeywordLinkDataObject dataObj = (KeywordLinkDataObject) it.next();
                list.add(new KeywordLinkBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllKeywordLinkKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        List<String> keys = getDAOFactory().getKeywordLinkDAO().getAllKeywordLinkKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve KeywordLink key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<KeywordLink> findKeywordLinks(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findKeywordLinks(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<KeywordLink> findKeywordLinks(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("KeywordLinkServiceImpl.findKeywordLinks(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<KeywordLink> list = new ArrayList<KeywordLink>();
        List<KeywordLinkDataObject> dataObjs = getDAOFactory().getKeywordLinkDAO().findKeywordLinks(filter, ordering, params, values, grouping, unique, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find keywordLinks for the given criterion.");
        } else {
            Iterator<KeywordLinkDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                KeywordLinkDataObject dataObj = (KeywordLinkDataObject) it.next();
                list.add(new KeywordLinkBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findKeywordLinkKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("KeywordLinkServiceImpl.findKeywordLinkKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = getDAOFactory().getKeywordLinkDAO().findKeywordLinkKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find KeywordLink keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("KeywordLinkServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getKeywordLinkDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createKeywordLink(String appClient, String clientRootDomain, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, Boolean internal, Boolean caching, String memo, String status, String note, Long expirationTime, String keywordFolder, String folderPath, String keyword, String queryKey, String scope, Boolean dynamic, Boolean caseSensitive) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        KeywordLinkDataObject dataObj = new KeywordLinkDataObject(null, appClient, clientRootDomain, user, shortLink, domain, token, longUrl, shortUrl, internal, caching, memo, status, note, expirationTime, keywordFolder, folderPath, keyword, queryKey, scope, dynamic, caseSensitive);
        return createKeywordLink(dataObj);
    }

    @Override
    public String createKeywordLink(KeywordLink keywordLink) throws BaseException
    {
        log.finer("BEGIN");

        // Param keywordLink cannot be null.....
        if(keywordLink == null) {
            log.log(Level.INFO, "Param keywordLink is null!");
            throw new BadRequestException("Param keywordLink object is null!");
        }
        KeywordLinkDataObject dataObj = null;
        if(keywordLink instanceof KeywordLinkDataObject) {
            dataObj = (KeywordLinkDataObject) keywordLink;
        } else if(keywordLink instanceof KeywordLinkBean) {
            dataObj = ((KeywordLinkBean) keywordLink).toDataObject();
        } else {  // if(keywordLink instanceof KeywordLink)
            //dataObj = new KeywordLinkDataObject(null, keywordLink.getAppClient(), keywordLink.getClientRootDomain(), keywordLink.getUser(), keywordLink.getShortLink(), keywordLink.getDomain(), keywordLink.getToken(), keywordLink.getLongUrl(), keywordLink.getShortUrl(), keywordLink.isInternal(), keywordLink.isCaching(), keywordLink.getMemo(), keywordLink.getStatus(), keywordLink.getNote(), keywordLink.getExpirationTime(), keywordLink.getKeywordFolder(), keywordLink.getFolderPath(), keywordLink.getKeyword(), keywordLink.getQueryKey(), keywordLink.getScope(), keywordLink.isDynamic(), keywordLink.isCaseSensitive());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new KeywordLinkDataObject(keywordLink.getGuid(), keywordLink.getAppClient(), keywordLink.getClientRootDomain(), keywordLink.getUser(), keywordLink.getShortLink(), keywordLink.getDomain(), keywordLink.getToken(), keywordLink.getLongUrl(), keywordLink.getShortUrl(), keywordLink.isInternal(), keywordLink.isCaching(), keywordLink.getMemo(), keywordLink.getStatus(), keywordLink.getNote(), keywordLink.getExpirationTime(), keywordLink.getKeywordFolder(), keywordLink.getFolderPath(), keywordLink.getKeyword(), keywordLink.getQueryKey(), keywordLink.getScope(), keywordLink.isDynamic(), keywordLink.isCaseSensitive());
        }
        String guid = getDAOFactory().getKeywordLinkDAO().createKeywordLink(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateKeywordLink(String guid, String appClient, String clientRootDomain, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, Boolean internal, Boolean caching, String memo, String status, String note, Long expirationTime, String keywordFolder, String folderPath, String keyword, String queryKey, String scope, Boolean dynamic, Boolean caseSensitive) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        KeywordLinkDataObject dataObj = new KeywordLinkDataObject(guid, appClient, clientRootDomain, user, shortLink, domain, token, longUrl, shortUrl, internal, caching, memo, status, note, expirationTime, keywordFolder, folderPath, keyword, queryKey, scope, dynamic, caseSensitive);
        return updateKeywordLink(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateKeywordLink(KeywordLink keywordLink) throws BaseException
    {
        log.finer("BEGIN");

        // Param keywordLink cannot be null.....
        if(keywordLink == null || keywordLink.getGuid() == null) {
            log.log(Level.INFO, "Param keywordLink or its guid is null!");
            throw new BadRequestException("Param keywordLink object or its guid is null!");
        }
        KeywordLinkDataObject dataObj = null;
        if(keywordLink instanceof KeywordLinkDataObject) {
            dataObj = (KeywordLinkDataObject) keywordLink;
        } else if(keywordLink instanceof KeywordLinkBean) {
            dataObj = ((KeywordLinkBean) keywordLink).toDataObject();
        } else {  // if(keywordLink instanceof KeywordLink)
            dataObj = new KeywordLinkDataObject(keywordLink.getGuid(), keywordLink.getAppClient(), keywordLink.getClientRootDomain(), keywordLink.getUser(), keywordLink.getShortLink(), keywordLink.getDomain(), keywordLink.getToken(), keywordLink.getLongUrl(), keywordLink.getShortUrl(), keywordLink.isInternal(), keywordLink.isCaching(), keywordLink.getMemo(), keywordLink.getStatus(), keywordLink.getNote(), keywordLink.getExpirationTime(), keywordLink.getKeywordFolder(), keywordLink.getFolderPath(), keywordLink.getKeyword(), keywordLink.getQueryKey(), keywordLink.getScope(), keywordLink.isDynamic(), keywordLink.isCaseSensitive());
        }
        Boolean suc = getDAOFactory().getKeywordLinkDAO().updateKeywordLink(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteKeywordLink(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getKeywordLinkDAO().deleteKeywordLink(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteKeywordLink(KeywordLink keywordLink) throws BaseException
    {
        log.finer("BEGIN");

        // Param keywordLink cannot be null.....
        if(keywordLink == null || keywordLink.getGuid() == null) {
            log.log(Level.INFO, "Param keywordLink or its guid is null!");
            throw new BadRequestException("Param keywordLink object or its guid is null!");
        }
        KeywordLinkDataObject dataObj = null;
        if(keywordLink instanceof KeywordLinkDataObject) {
            dataObj = (KeywordLinkDataObject) keywordLink;
        } else if(keywordLink instanceof KeywordLinkBean) {
            dataObj = ((KeywordLinkBean) keywordLink).toDataObject();
        } else {  // if(keywordLink instanceof KeywordLink)
            dataObj = new KeywordLinkDataObject(keywordLink.getGuid(), keywordLink.getAppClient(), keywordLink.getClientRootDomain(), keywordLink.getUser(), keywordLink.getShortLink(), keywordLink.getDomain(), keywordLink.getToken(), keywordLink.getLongUrl(), keywordLink.getShortUrl(), keywordLink.isInternal(), keywordLink.isCaching(), keywordLink.getMemo(), keywordLink.getStatus(), keywordLink.getNote(), keywordLink.getExpirationTime(), keywordLink.getKeywordFolder(), keywordLink.getFolderPath(), keywordLink.getKeyword(), keywordLink.getQueryKey(), keywordLink.getScope(), keywordLink.isDynamic(), keywordLink.isCaseSensitive());
        }
        Boolean suc = getDAOFactory().getKeywordLinkDAO().deleteKeywordLink(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteKeywordLinks(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getKeywordLinkDAO().deleteKeywordLinks(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
