package com.myurldb.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.LinkMessage;
import com.myurldb.ws.bean.LinkMessageBean;
import com.myurldb.ws.dao.DAOFactory;
import com.myurldb.ws.data.LinkMessageDataObject;
import com.myurldb.ws.service.DAOFactoryManager;
import com.myurldb.ws.service.LinkMessageService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class LinkMessageServiceImpl implements LinkMessageService
{
    private static final Logger log = Logger.getLogger(LinkMessageServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // LinkMessage related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public LinkMessage getLinkMessage(String guid) throws BaseException
    {
        log.finer("BEGIN");

        LinkMessageDataObject dataObj = getDAOFactory().getLinkMessageDAO().getLinkMessage(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve LinkMessageDataObject for guid = " + guid);
            return null;  // ????
        }
        LinkMessageBean bean = new LinkMessageBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getLinkMessage(String guid, String field) throws BaseException
    {
        LinkMessageDataObject dataObj = getDAOFactory().getLinkMessageDAO().getLinkMessage(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve LinkMessageDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("shortLink")) {
            return dataObj.getShortLink();
        } else if(field.equals("message")) {
            return dataObj.getMessage();
        } else if(field.equals("password")) {
            return dataObj.getPassword();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<LinkMessage> getLinkMessages(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<LinkMessage> list = new ArrayList<LinkMessage>();
        List<LinkMessageDataObject> dataObjs = getDAOFactory().getLinkMessageDAO().getLinkMessages(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve LinkMessageDataObject list.");
        } else {
            Iterator<LinkMessageDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                LinkMessageDataObject dataObj = (LinkMessageDataObject) it.next();
                list.add(new LinkMessageBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<LinkMessage> getAllLinkMessages() throws BaseException
    {
        return getAllLinkMessages(null, null, null);
    }

    @Override
    public List<LinkMessage> getAllLinkMessages(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<LinkMessage> list = new ArrayList<LinkMessage>();
        List<LinkMessageDataObject> dataObjs = getDAOFactory().getLinkMessageDAO().getAllLinkMessages(ordering, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve LinkMessageDataObject list.");
        } else {
            Iterator<LinkMessageDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                LinkMessageDataObject dataObj = (LinkMessageDataObject) it.next();
                list.add(new LinkMessageBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllLinkMessageKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        List<String> keys = getDAOFactory().getLinkMessageDAO().getAllLinkMessageKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve LinkMessage key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<LinkMessage> findLinkMessages(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findLinkMessages(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<LinkMessage> findLinkMessages(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("LinkMessageServiceImpl.findLinkMessages(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<LinkMessage> list = new ArrayList<LinkMessage>();
        List<LinkMessageDataObject> dataObjs = getDAOFactory().getLinkMessageDAO().findLinkMessages(filter, ordering, params, values, grouping, unique, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find linkMessages for the given criterion.");
        } else {
            Iterator<LinkMessageDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                LinkMessageDataObject dataObj = (LinkMessageDataObject) it.next();
                list.add(new LinkMessageBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findLinkMessageKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("LinkMessageServiceImpl.findLinkMessageKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = getDAOFactory().getLinkMessageDAO().findLinkMessageKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find LinkMessage keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("LinkMessageServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getLinkMessageDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createLinkMessage(String shortLink, String message, String password) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        LinkMessageDataObject dataObj = new LinkMessageDataObject(null, shortLink, message, password);
        return createLinkMessage(dataObj);
    }

    @Override
    public String createLinkMessage(LinkMessage linkMessage) throws BaseException
    {
        log.finer("BEGIN");

        // Param linkMessage cannot be null.....
        if(linkMessage == null) {
            log.log(Level.INFO, "Param linkMessage is null!");
            throw new BadRequestException("Param linkMessage object is null!");
        }
        LinkMessageDataObject dataObj = null;
        if(linkMessage instanceof LinkMessageDataObject) {
            dataObj = (LinkMessageDataObject) linkMessage;
        } else if(linkMessage instanceof LinkMessageBean) {
            dataObj = ((LinkMessageBean) linkMessage).toDataObject();
        } else {  // if(linkMessage instanceof LinkMessage)
            //dataObj = new LinkMessageDataObject(null, linkMessage.getShortLink(), linkMessage.getMessage(), linkMessage.getPassword());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new LinkMessageDataObject(linkMessage.getGuid(), linkMessage.getShortLink(), linkMessage.getMessage(), linkMessage.getPassword());
        }
        String guid = getDAOFactory().getLinkMessageDAO().createLinkMessage(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateLinkMessage(String guid, String shortLink, String message, String password) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        LinkMessageDataObject dataObj = new LinkMessageDataObject(guid, shortLink, message, password);
        return updateLinkMessage(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateLinkMessage(LinkMessage linkMessage) throws BaseException
    {
        log.finer("BEGIN");

        // Param linkMessage cannot be null.....
        if(linkMessage == null || linkMessage.getGuid() == null) {
            log.log(Level.INFO, "Param linkMessage or its guid is null!");
            throw new BadRequestException("Param linkMessage object or its guid is null!");
        }
        LinkMessageDataObject dataObj = null;
        if(linkMessage instanceof LinkMessageDataObject) {
            dataObj = (LinkMessageDataObject) linkMessage;
        } else if(linkMessage instanceof LinkMessageBean) {
            dataObj = ((LinkMessageBean) linkMessage).toDataObject();
        } else {  // if(linkMessage instanceof LinkMessage)
            dataObj = new LinkMessageDataObject(linkMessage.getGuid(), linkMessage.getShortLink(), linkMessage.getMessage(), linkMessage.getPassword());
        }
        Boolean suc = getDAOFactory().getLinkMessageDAO().updateLinkMessage(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteLinkMessage(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getLinkMessageDAO().deleteLinkMessage(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteLinkMessage(LinkMessage linkMessage) throws BaseException
    {
        log.finer("BEGIN");

        // Param linkMessage cannot be null.....
        if(linkMessage == null || linkMessage.getGuid() == null) {
            log.log(Level.INFO, "Param linkMessage or its guid is null!");
            throw new BadRequestException("Param linkMessage object or its guid is null!");
        }
        LinkMessageDataObject dataObj = null;
        if(linkMessage instanceof LinkMessageDataObject) {
            dataObj = (LinkMessageDataObject) linkMessage;
        } else if(linkMessage instanceof LinkMessageBean) {
            dataObj = ((LinkMessageBean) linkMessage).toDataObject();
        } else {  // if(linkMessage instanceof LinkMessage)
            dataObj = new LinkMessageDataObject(linkMessage.getGuid(), linkMessage.getShortLink(), linkMessage.getMessage(), linkMessage.getPassword());
        }
        Boolean suc = getDAOFactory().getLinkMessageDAO().deleteLinkMessage(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteLinkMessages(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getLinkMessageDAO().deleteLinkMessages(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
