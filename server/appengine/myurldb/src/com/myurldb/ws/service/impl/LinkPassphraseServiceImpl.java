package com.myurldb.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.LinkPassphrase;
import com.myurldb.ws.bean.LinkPassphraseBean;
import com.myurldb.ws.dao.DAOFactory;
import com.myurldb.ws.data.LinkPassphraseDataObject;
import com.myurldb.ws.service.DAOFactoryManager;
import com.myurldb.ws.service.LinkPassphraseService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class LinkPassphraseServiceImpl implements LinkPassphraseService
{
    private static final Logger log = Logger.getLogger(LinkPassphraseServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // LinkPassphrase related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public LinkPassphrase getLinkPassphrase(String guid) throws BaseException
    {
        log.finer("BEGIN");

        LinkPassphraseDataObject dataObj = getDAOFactory().getLinkPassphraseDAO().getLinkPassphrase(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve LinkPassphraseDataObject for guid = " + guid);
            return null;  // ????
        }
        LinkPassphraseBean bean = new LinkPassphraseBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getLinkPassphrase(String guid, String field) throws BaseException
    {
        LinkPassphraseDataObject dataObj = getDAOFactory().getLinkPassphraseDAO().getLinkPassphrase(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve LinkPassphraseDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("shortLink")) {
            return dataObj.getShortLink();
        } else if(field.equals("passphrase")) {
            return dataObj.getPassphrase();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<LinkPassphrase> getLinkPassphrases(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<LinkPassphrase> list = new ArrayList<LinkPassphrase>();
        List<LinkPassphraseDataObject> dataObjs = getDAOFactory().getLinkPassphraseDAO().getLinkPassphrases(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve LinkPassphraseDataObject list.");
        } else {
            Iterator<LinkPassphraseDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                LinkPassphraseDataObject dataObj = (LinkPassphraseDataObject) it.next();
                list.add(new LinkPassphraseBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<LinkPassphrase> getAllLinkPassphrases() throws BaseException
    {
        return getAllLinkPassphrases(null, null, null);
    }

    @Override
    public List<LinkPassphrase> getAllLinkPassphrases(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<LinkPassphrase> list = new ArrayList<LinkPassphrase>();
        List<LinkPassphraseDataObject> dataObjs = getDAOFactory().getLinkPassphraseDAO().getAllLinkPassphrases(ordering, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve LinkPassphraseDataObject list.");
        } else {
            Iterator<LinkPassphraseDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                LinkPassphraseDataObject dataObj = (LinkPassphraseDataObject) it.next();
                list.add(new LinkPassphraseBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllLinkPassphraseKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        List<String> keys = getDAOFactory().getLinkPassphraseDAO().getAllLinkPassphraseKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve LinkPassphrase key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<LinkPassphrase> findLinkPassphrases(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findLinkPassphrases(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<LinkPassphrase> findLinkPassphrases(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("LinkPassphraseServiceImpl.findLinkPassphrases(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<LinkPassphrase> list = new ArrayList<LinkPassphrase>();
        List<LinkPassphraseDataObject> dataObjs = getDAOFactory().getLinkPassphraseDAO().findLinkPassphrases(filter, ordering, params, values, grouping, unique, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find linkPassphrases for the given criterion.");
        } else {
            Iterator<LinkPassphraseDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                LinkPassphraseDataObject dataObj = (LinkPassphraseDataObject) it.next();
                list.add(new LinkPassphraseBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findLinkPassphraseKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("LinkPassphraseServiceImpl.findLinkPassphraseKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = getDAOFactory().getLinkPassphraseDAO().findLinkPassphraseKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find LinkPassphrase keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("LinkPassphraseServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getLinkPassphraseDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createLinkPassphrase(String shortLink, String passphrase) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        LinkPassphraseDataObject dataObj = new LinkPassphraseDataObject(null, shortLink, passphrase);
        return createLinkPassphrase(dataObj);
    }

    @Override
    public String createLinkPassphrase(LinkPassphrase linkPassphrase) throws BaseException
    {
        log.finer("BEGIN");

        // Param linkPassphrase cannot be null.....
        if(linkPassphrase == null) {
            log.log(Level.INFO, "Param linkPassphrase is null!");
            throw new BadRequestException("Param linkPassphrase object is null!");
        }
        LinkPassphraseDataObject dataObj = null;
        if(linkPassphrase instanceof LinkPassphraseDataObject) {
            dataObj = (LinkPassphraseDataObject) linkPassphrase;
        } else if(linkPassphrase instanceof LinkPassphraseBean) {
            dataObj = ((LinkPassphraseBean) linkPassphrase).toDataObject();
        } else {  // if(linkPassphrase instanceof LinkPassphrase)
            //dataObj = new LinkPassphraseDataObject(null, linkPassphrase.getShortLink(), linkPassphrase.getPassphrase());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new LinkPassphraseDataObject(linkPassphrase.getGuid(), linkPassphrase.getShortLink(), linkPassphrase.getPassphrase());
        }
        String guid = getDAOFactory().getLinkPassphraseDAO().createLinkPassphrase(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateLinkPassphrase(String guid, String shortLink, String passphrase) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        LinkPassphraseDataObject dataObj = new LinkPassphraseDataObject(guid, shortLink, passphrase);
        return updateLinkPassphrase(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateLinkPassphrase(LinkPassphrase linkPassphrase) throws BaseException
    {
        log.finer("BEGIN");

        // Param linkPassphrase cannot be null.....
        if(linkPassphrase == null || linkPassphrase.getGuid() == null) {
            log.log(Level.INFO, "Param linkPassphrase or its guid is null!");
            throw new BadRequestException("Param linkPassphrase object or its guid is null!");
        }
        LinkPassphraseDataObject dataObj = null;
        if(linkPassphrase instanceof LinkPassphraseDataObject) {
            dataObj = (LinkPassphraseDataObject) linkPassphrase;
        } else if(linkPassphrase instanceof LinkPassphraseBean) {
            dataObj = ((LinkPassphraseBean) linkPassphrase).toDataObject();
        } else {  // if(linkPassphrase instanceof LinkPassphrase)
            dataObj = new LinkPassphraseDataObject(linkPassphrase.getGuid(), linkPassphrase.getShortLink(), linkPassphrase.getPassphrase());
        }
        Boolean suc = getDAOFactory().getLinkPassphraseDAO().updateLinkPassphrase(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteLinkPassphrase(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getLinkPassphraseDAO().deleteLinkPassphrase(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteLinkPassphrase(LinkPassphrase linkPassphrase) throws BaseException
    {
        log.finer("BEGIN");

        // Param linkPassphrase cannot be null.....
        if(linkPassphrase == null || linkPassphrase.getGuid() == null) {
            log.log(Level.INFO, "Param linkPassphrase or its guid is null!");
            throw new BadRequestException("Param linkPassphrase object or its guid is null!");
        }
        LinkPassphraseDataObject dataObj = null;
        if(linkPassphrase instanceof LinkPassphraseDataObject) {
            dataObj = (LinkPassphraseDataObject) linkPassphrase;
        } else if(linkPassphrase instanceof LinkPassphraseBean) {
            dataObj = ((LinkPassphraseBean) linkPassphrase).toDataObject();
        } else {  // if(linkPassphrase instanceof LinkPassphrase)
            dataObj = new LinkPassphraseDataObject(linkPassphrase.getGuid(), linkPassphrase.getShortLink(), linkPassphrase.getPassphrase());
        }
        Boolean suc = getDAOFactory().getLinkPassphraseDAO().deleteLinkPassphrase(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteLinkPassphrases(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getLinkPassphraseDAO().deleteLinkPassphrases(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
