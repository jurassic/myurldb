package com.myurldb.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.QrCode;
import com.myurldb.ws.bean.QrCodeBean;
import com.myurldb.ws.dao.DAOFactory;
import com.myurldb.ws.data.QrCodeDataObject;
import com.myurldb.ws.service.DAOFactoryManager;
import com.myurldb.ws.service.QrCodeService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class QrCodeServiceImpl implements QrCodeService
{
    private static final Logger log = Logger.getLogger(QrCodeServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // QrCode related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public QrCode getQrCode(String guid) throws BaseException
    {
        log.finer("BEGIN");

        QrCodeDataObject dataObj = getDAOFactory().getQrCodeDAO().getQrCode(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve QrCodeDataObject for guid = " + guid);
            return null;  // ????
        }
        QrCodeBean bean = new QrCodeBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getQrCode(String guid, String field) throws BaseException
    {
        QrCodeDataObject dataObj = getDAOFactory().getQrCodeDAO().getQrCode(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve QrCodeDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("shortLink")) {
            return dataObj.getShortLink();
        } else if(field.equals("imageLink")) {
            return dataObj.getImageLink();
        } else if(field.equals("imageUrl")) {
            return dataObj.getImageUrl();
        } else if(field.equals("type")) {
            return dataObj.getType();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<QrCode> getQrCodes(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<QrCode> list = new ArrayList<QrCode>();
        List<QrCodeDataObject> dataObjs = getDAOFactory().getQrCodeDAO().getQrCodes(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve QrCodeDataObject list.");
        } else {
            Iterator<QrCodeDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                QrCodeDataObject dataObj = (QrCodeDataObject) it.next();
                list.add(new QrCodeBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<QrCode> getAllQrCodes() throws BaseException
    {
        return getAllQrCodes(null, null, null);
    }

    @Override
    public List<QrCode> getAllQrCodes(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<QrCode> list = new ArrayList<QrCode>();
        List<QrCodeDataObject> dataObjs = getDAOFactory().getQrCodeDAO().getAllQrCodes(ordering, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve QrCodeDataObject list.");
        } else {
            Iterator<QrCodeDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                QrCodeDataObject dataObj = (QrCodeDataObject) it.next();
                list.add(new QrCodeBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllQrCodeKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        List<String> keys = getDAOFactory().getQrCodeDAO().getAllQrCodeKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve QrCode key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<QrCode> findQrCodes(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findQrCodes(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<QrCode> findQrCodes(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("QrCodeServiceImpl.findQrCodes(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<QrCode> list = new ArrayList<QrCode>();
        List<QrCodeDataObject> dataObjs = getDAOFactory().getQrCodeDAO().findQrCodes(filter, ordering, params, values, grouping, unique, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find qrCodes for the given criterion.");
        } else {
            Iterator<QrCodeDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                QrCodeDataObject dataObj = (QrCodeDataObject) it.next();
                list.add(new QrCodeBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findQrCodeKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("QrCodeServiceImpl.findQrCodeKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = getDAOFactory().getQrCodeDAO().findQrCodeKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find QrCode keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("QrCodeServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getQrCodeDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createQrCode(String shortLink, String imageLink, String imageUrl, String type, String status) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        QrCodeDataObject dataObj = new QrCodeDataObject(null, shortLink, imageLink, imageUrl, type, status);
        return createQrCode(dataObj);
    }

    @Override
    public String createQrCode(QrCode qrCode) throws BaseException
    {
        log.finer("BEGIN");

        // Param qrCode cannot be null.....
        if(qrCode == null) {
            log.log(Level.INFO, "Param qrCode is null!");
            throw new BadRequestException("Param qrCode object is null!");
        }
        QrCodeDataObject dataObj = null;
        if(qrCode instanceof QrCodeDataObject) {
            dataObj = (QrCodeDataObject) qrCode;
        } else if(qrCode instanceof QrCodeBean) {
            dataObj = ((QrCodeBean) qrCode).toDataObject();
        } else {  // if(qrCode instanceof QrCode)
            //dataObj = new QrCodeDataObject(null, qrCode.getShortLink(), qrCode.getImageLink(), qrCode.getImageUrl(), qrCode.getType(), qrCode.getStatus());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new QrCodeDataObject(qrCode.getGuid(), qrCode.getShortLink(), qrCode.getImageLink(), qrCode.getImageUrl(), qrCode.getType(), qrCode.getStatus());
        }
        String guid = getDAOFactory().getQrCodeDAO().createQrCode(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateQrCode(String guid, String shortLink, String imageLink, String imageUrl, String type, String status) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        QrCodeDataObject dataObj = new QrCodeDataObject(guid, shortLink, imageLink, imageUrl, type, status);
        return updateQrCode(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateQrCode(QrCode qrCode) throws BaseException
    {
        log.finer("BEGIN");

        // Param qrCode cannot be null.....
        if(qrCode == null || qrCode.getGuid() == null) {
            log.log(Level.INFO, "Param qrCode or its guid is null!");
            throw new BadRequestException("Param qrCode object or its guid is null!");
        }
        QrCodeDataObject dataObj = null;
        if(qrCode instanceof QrCodeDataObject) {
            dataObj = (QrCodeDataObject) qrCode;
        } else if(qrCode instanceof QrCodeBean) {
            dataObj = ((QrCodeBean) qrCode).toDataObject();
        } else {  // if(qrCode instanceof QrCode)
            dataObj = new QrCodeDataObject(qrCode.getGuid(), qrCode.getShortLink(), qrCode.getImageLink(), qrCode.getImageUrl(), qrCode.getType(), qrCode.getStatus());
        }
        Boolean suc = getDAOFactory().getQrCodeDAO().updateQrCode(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteQrCode(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getQrCodeDAO().deleteQrCode(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteQrCode(QrCode qrCode) throws BaseException
    {
        log.finer("BEGIN");

        // Param qrCode cannot be null.....
        if(qrCode == null || qrCode.getGuid() == null) {
            log.log(Level.INFO, "Param qrCode or its guid is null!");
            throw new BadRequestException("Param qrCode object or its guid is null!");
        }
        QrCodeDataObject dataObj = null;
        if(qrCode instanceof QrCodeDataObject) {
            dataObj = (QrCodeDataObject) qrCode;
        } else if(qrCode instanceof QrCodeBean) {
            dataObj = ((QrCodeBean) qrCode).toDataObject();
        } else {  // if(qrCode instanceof QrCode)
            dataObj = new QrCodeDataObject(qrCode.getGuid(), qrCode.getShortLink(), qrCode.getImageLink(), qrCode.getImageUrl(), qrCode.getType(), qrCode.getStatus());
        }
        Boolean suc = getDAOFactory().getQrCodeDAO().deleteQrCode(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteQrCodes(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getQrCodeDAO().deleteQrCodes(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
