package com.myurldb.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.RolePermission;
import com.myurldb.ws.bean.RolePermissionBean;
import com.myurldb.ws.dao.DAOFactory;
import com.myurldb.ws.data.RolePermissionDataObject;
import com.myurldb.ws.service.DAOFactoryManager;
import com.myurldb.ws.service.RolePermissionService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class RolePermissionServiceImpl implements RolePermissionService
{
    private static final Logger log = Logger.getLogger(RolePermissionServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // RolePermission related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public RolePermission getRolePermission(String guid) throws BaseException
    {
        log.finer("BEGIN");

        RolePermissionDataObject dataObj = getDAOFactory().getRolePermissionDAO().getRolePermission(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve RolePermissionDataObject for guid = " + guid);
            return null;  // ????
        }
        RolePermissionBean bean = new RolePermissionBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getRolePermission(String guid, String field) throws BaseException
    {
        RolePermissionDataObject dataObj = getDAOFactory().getRolePermissionDAO().getRolePermission(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve RolePermissionDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("role")) {
            return dataObj.getRole();
        } else if(field.equals("permissionName")) {
            return dataObj.getPermissionName();
        } else if(field.equals("resource")) {
            return dataObj.getResource();
        } else if(field.equals("instance")) {
            return dataObj.getInstance();
        } else if(field.equals("action")) {
            return dataObj.getAction();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<RolePermission> getRolePermissions(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<RolePermission> list = new ArrayList<RolePermission>();
        List<RolePermissionDataObject> dataObjs = getDAOFactory().getRolePermissionDAO().getRolePermissions(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve RolePermissionDataObject list.");
        } else {
            Iterator<RolePermissionDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                RolePermissionDataObject dataObj = (RolePermissionDataObject) it.next();
                list.add(new RolePermissionBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<RolePermission> getAllRolePermissions() throws BaseException
    {
        return getAllRolePermissions(null, null, null);
    }

    @Override
    public List<RolePermission> getAllRolePermissions(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<RolePermission> list = new ArrayList<RolePermission>();
        List<RolePermissionDataObject> dataObjs = getDAOFactory().getRolePermissionDAO().getAllRolePermissions(ordering, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve RolePermissionDataObject list.");
        } else {
            Iterator<RolePermissionDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                RolePermissionDataObject dataObj = (RolePermissionDataObject) it.next();
                list.add(new RolePermissionBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllRolePermissionKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        List<String> keys = getDAOFactory().getRolePermissionDAO().getAllRolePermissionKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve RolePermission key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<RolePermission> findRolePermissions(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findRolePermissions(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<RolePermission> findRolePermissions(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("RolePermissionServiceImpl.findRolePermissions(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<RolePermission> list = new ArrayList<RolePermission>();
        List<RolePermissionDataObject> dataObjs = getDAOFactory().getRolePermissionDAO().findRolePermissions(filter, ordering, params, values, grouping, unique, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find rolePermissions for the given criterion.");
        } else {
            Iterator<RolePermissionDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                RolePermissionDataObject dataObj = (RolePermissionDataObject) it.next();
                list.add(new RolePermissionBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findRolePermissionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("RolePermissionServiceImpl.findRolePermissionKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = getDAOFactory().getRolePermissionDAO().findRolePermissionKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find RolePermission keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("RolePermissionServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getRolePermissionDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createRolePermission(String role, String permissionName, String resource, String instance, String action, String status) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        RolePermissionDataObject dataObj = new RolePermissionDataObject(null, role, permissionName, resource, instance, action, status);
        return createRolePermission(dataObj);
    }

    @Override
    public String createRolePermission(RolePermission rolePermission) throws BaseException
    {
        log.finer("BEGIN");

        // Param rolePermission cannot be null.....
        if(rolePermission == null) {
            log.log(Level.INFO, "Param rolePermission is null!");
            throw new BadRequestException("Param rolePermission object is null!");
        }
        RolePermissionDataObject dataObj = null;
        if(rolePermission instanceof RolePermissionDataObject) {
            dataObj = (RolePermissionDataObject) rolePermission;
        } else if(rolePermission instanceof RolePermissionBean) {
            dataObj = ((RolePermissionBean) rolePermission).toDataObject();
        } else {  // if(rolePermission instanceof RolePermission)
            //dataObj = new RolePermissionDataObject(null, rolePermission.getRole(), rolePermission.getPermissionName(), rolePermission.getResource(), rolePermission.getInstance(), rolePermission.getAction(), rolePermission.getStatus());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new RolePermissionDataObject(rolePermission.getGuid(), rolePermission.getRole(), rolePermission.getPermissionName(), rolePermission.getResource(), rolePermission.getInstance(), rolePermission.getAction(), rolePermission.getStatus());
        }
        String guid = getDAOFactory().getRolePermissionDAO().createRolePermission(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateRolePermission(String guid, String role, String permissionName, String resource, String instance, String action, String status) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        RolePermissionDataObject dataObj = new RolePermissionDataObject(guid, role, permissionName, resource, instance, action, status);
        return updateRolePermission(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateRolePermission(RolePermission rolePermission) throws BaseException
    {
        log.finer("BEGIN");

        // Param rolePermission cannot be null.....
        if(rolePermission == null || rolePermission.getGuid() == null) {
            log.log(Level.INFO, "Param rolePermission or its guid is null!");
            throw new BadRequestException("Param rolePermission object or its guid is null!");
        }
        RolePermissionDataObject dataObj = null;
        if(rolePermission instanceof RolePermissionDataObject) {
            dataObj = (RolePermissionDataObject) rolePermission;
        } else if(rolePermission instanceof RolePermissionBean) {
            dataObj = ((RolePermissionBean) rolePermission).toDataObject();
        } else {  // if(rolePermission instanceof RolePermission)
            dataObj = new RolePermissionDataObject(rolePermission.getGuid(), rolePermission.getRole(), rolePermission.getPermissionName(), rolePermission.getResource(), rolePermission.getInstance(), rolePermission.getAction(), rolePermission.getStatus());
        }
        Boolean suc = getDAOFactory().getRolePermissionDAO().updateRolePermission(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteRolePermission(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getRolePermissionDAO().deleteRolePermission(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteRolePermission(RolePermission rolePermission) throws BaseException
    {
        log.finer("BEGIN");

        // Param rolePermission cannot be null.....
        if(rolePermission == null || rolePermission.getGuid() == null) {
            log.log(Level.INFO, "Param rolePermission or its guid is null!");
            throw new BadRequestException("Param rolePermission object or its guid is null!");
        }
        RolePermissionDataObject dataObj = null;
        if(rolePermission instanceof RolePermissionDataObject) {
            dataObj = (RolePermissionDataObject) rolePermission;
        } else if(rolePermission instanceof RolePermissionBean) {
            dataObj = ((RolePermissionBean) rolePermission).toDataObject();
        } else {  // if(rolePermission instanceof RolePermission)
            dataObj = new RolePermissionDataObject(rolePermission.getGuid(), rolePermission.getRole(), rolePermission.getPermissionName(), rolePermission.getResource(), rolePermission.getInstance(), rolePermission.getAction(), rolePermission.getStatus());
        }
        Boolean suc = getDAOFactory().getRolePermissionDAO().deleteRolePermission(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteRolePermissions(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getRolePermissionDAO().deleteRolePermissions(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
