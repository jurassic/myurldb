package com.myurldb.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.ReferrerInfoStruct;
import com.myurldb.ws.ShortLink;
import com.myurldb.ws.bean.ReferrerInfoStructBean;
import com.myurldb.ws.bean.ShortLinkBean;
import com.myurldb.ws.dao.DAOFactory;
import com.myurldb.ws.data.ReferrerInfoStructDataObject;
import com.myurldb.ws.data.ShortLinkDataObject;
import com.myurldb.ws.service.DAOFactoryManager;
import com.myurldb.ws.service.ShortLinkService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class ShortLinkServiceImpl implements ShortLinkService
{
    private static final Logger log = Logger.getLogger(ShortLinkServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // ShortLink related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public ShortLink getShortLink(String guid) throws BaseException
    {
        log.finer("BEGIN");

        ShortLinkDataObject dataObj = getDAOFactory().getShortLinkDAO().getShortLink(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve ShortLinkDataObject for guid = " + guid);
            return null;  // ????
        }
        ShortLinkBean bean = new ShortLinkBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getShortLink(String guid, String field) throws BaseException
    {
        ShortLinkDataObject dataObj = getDAOFactory().getShortLinkDAO().getShortLink(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve ShortLinkDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("appClient")) {
            return dataObj.getAppClient();
        } else if(field.equals("clientRootDomain")) {
            return dataObj.getClientRootDomain();
        } else if(field.equals("owner")) {
            return dataObj.getOwner();
        } else if(field.equals("longUrlDomain")) {
            return dataObj.getLongUrlDomain();
        } else if(field.equals("longUrl")) {
            return dataObj.getLongUrl();
        } else if(field.equals("longUrlFull")) {
            return dataObj.getLongUrlFull();
        } else if(field.equals("longUrlHash")) {
            return dataObj.getLongUrlHash();
        } else if(field.equals("reuseExistingShortUrl")) {
            return dataObj.isReuseExistingShortUrl();
        } else if(field.equals("failIfShortUrlExists")) {
            return dataObj.isFailIfShortUrlExists();
        } else if(field.equals("domain")) {
            return dataObj.getDomain();
        } else if(field.equals("domainType")) {
            return dataObj.getDomainType();
        } else if(field.equals("usercode")) {
            return dataObj.getUsercode();
        } else if(field.equals("username")) {
            return dataObj.getUsername();
        } else if(field.equals("tokenPrefix")) {
            return dataObj.getTokenPrefix();
        } else if(field.equals("token")) {
            return dataObj.getToken();
        } else if(field.equals("tokenType")) {
            return dataObj.getTokenType();
        } else if(field.equals("sassyTokenType")) {
            return dataObj.getSassyTokenType();
        } else if(field.equals("shortUrl")) {
            return dataObj.getShortUrl();
        } else if(field.equals("shortPassage")) {
            return dataObj.getShortPassage();
        } else if(field.equals("redirectType")) {
            return dataObj.getRedirectType();
        } else if(field.equals("flashDuration")) {
            return dataObj.getFlashDuration();
        } else if(field.equals("accessType")) {
            return dataObj.getAccessType();
        } else if(field.equals("viewType")) {
            return dataObj.getViewType();
        } else if(field.equals("shareType")) {
            return dataObj.getShareType();
        } else if(field.equals("readOnly")) {
            return dataObj.isReadOnly();
        } else if(field.equals("displayMessage")) {
            return dataObj.getDisplayMessage();
        } else if(field.equals("shortMessage")) {
            return dataObj.getShortMessage();
        } else if(field.equals("keywordEnabled")) {
            return dataObj.isKeywordEnabled();
        } else if(field.equals("bookmarkEnabled")) {
            return dataObj.isBookmarkEnabled();
        } else if(field.equals("referrerInfo")) {
            return dataObj.getReferrerInfo();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("note")) {
            return dataObj.getNote();
        } else if(field.equals("expirationTime")) {
            return dataObj.getExpirationTime();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<ShortLink> getShortLinks(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<ShortLink> list = new ArrayList<ShortLink>();
        List<ShortLinkDataObject> dataObjs = getDAOFactory().getShortLinkDAO().getShortLinks(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve ShortLinkDataObject list.");
        } else {
            Iterator<ShortLinkDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                ShortLinkDataObject dataObj = (ShortLinkDataObject) it.next();
                list.add(new ShortLinkBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<ShortLink> getAllShortLinks() throws BaseException
    {
        return getAllShortLinks(null, null, null);
    }

    @Override
    public List<ShortLink> getAllShortLinks(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<ShortLink> list = new ArrayList<ShortLink>();
        List<ShortLinkDataObject> dataObjs = getDAOFactory().getShortLinkDAO().getAllShortLinks(ordering, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve ShortLinkDataObject list.");
        } else {
            Iterator<ShortLinkDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                ShortLinkDataObject dataObj = (ShortLinkDataObject) it.next();
                list.add(new ShortLinkBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllShortLinkKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        List<String> keys = getDAOFactory().getShortLinkDAO().getAllShortLinkKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve ShortLink key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<ShortLink> findShortLinks(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findShortLinks(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<ShortLink> findShortLinks(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("ShortLinkServiceImpl.findShortLinks(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<ShortLink> list = new ArrayList<ShortLink>();
        List<ShortLinkDataObject> dataObjs = getDAOFactory().getShortLinkDAO().findShortLinks(filter, ordering, params, values, grouping, unique, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find shortLinks for the given criterion.");
        } else {
            Iterator<ShortLinkDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                ShortLinkDataObject dataObj = (ShortLinkDataObject) it.next();
                list.add(new ShortLinkBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findShortLinkKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("ShortLinkServiceImpl.findShortLinkKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = getDAOFactory().getShortLinkDAO().findShortLinkKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find ShortLink keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("ShortLinkServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getShortLinkDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createShortLink(String appClient, String clientRootDomain, String owner, String longUrlDomain, String longUrl, String longUrlFull, String longUrlHash, Boolean reuseExistingShortUrl, Boolean failIfShortUrlExists, String domain, String domainType, String usercode, String username, String tokenPrefix, String token, String tokenType, String sassyTokenType, String shortUrl, String shortPassage, String redirectType, Long flashDuration, String accessType, String viewType, String shareType, Boolean readOnly, String displayMessage, String shortMessage, Boolean keywordEnabled, Boolean bookmarkEnabled, ReferrerInfoStruct referrerInfo, String status, String note, Long expirationTime) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        ReferrerInfoStructDataObject referrerInfoDobj = null;
        if(referrerInfo instanceof ReferrerInfoStructBean) {
            referrerInfoDobj = ((ReferrerInfoStructBean) referrerInfo).toDataObject();
        } else if(referrerInfo instanceof ReferrerInfoStruct) {
            referrerInfoDobj = new ReferrerInfoStructDataObject(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            referrerInfoDobj = null;   // ????
        }
        
        ShortLinkDataObject dataObj = new ShortLinkDataObject(null, appClient, clientRootDomain, owner, longUrlDomain, longUrl, longUrlFull, longUrlHash, reuseExistingShortUrl, failIfShortUrlExists, domain, domainType, usercode, username, tokenPrefix, token, tokenType, sassyTokenType, shortUrl, shortPassage, redirectType, flashDuration, accessType, viewType, shareType, readOnly, displayMessage, shortMessage, keywordEnabled, bookmarkEnabled, referrerInfoDobj, status, note, expirationTime);
        return createShortLink(dataObj);
    }

    @Override
    public String createShortLink(ShortLink shortLink) throws BaseException
    {
        log.finer("BEGIN");

        // Param shortLink cannot be null.....
        if(shortLink == null) {
            log.log(Level.INFO, "Param shortLink is null!");
            throw new BadRequestException("Param shortLink object is null!");
        }
        ShortLinkDataObject dataObj = null;
        if(shortLink instanceof ShortLinkDataObject) {
            dataObj = (ShortLinkDataObject) shortLink;
        } else if(shortLink instanceof ShortLinkBean) {
            dataObj = ((ShortLinkBean) shortLink).toDataObject();
        } else {  // if(shortLink instanceof ShortLink)
            //dataObj = new ShortLinkDataObject(null, shortLink.getAppClient(), shortLink.getClientRootDomain(), shortLink.getOwner(), shortLink.getLongUrlDomain(), shortLink.getLongUrl(), shortLink.getLongUrlFull(), shortLink.getLongUrlHash(), shortLink.isReuseExistingShortUrl(), shortLink.isFailIfShortUrlExists(), shortLink.getDomain(), shortLink.getDomainType(), shortLink.getUsercode(), shortLink.getUsername(), shortLink.getTokenPrefix(), shortLink.getToken(), shortLink.getTokenType(), shortLink.getSassyTokenType(), shortLink.getShortUrl(), shortLink.getShortPassage(), shortLink.getRedirectType(), shortLink.getFlashDuration(), shortLink.getAccessType(), shortLink.getViewType(), shortLink.getShareType(), shortLink.isReadOnly(), shortLink.getDisplayMessage(), shortLink.getShortMessage(), shortLink.isKeywordEnabled(), shortLink.isBookmarkEnabled(), (ReferrerInfoStructDataObject) shortLink.getReferrerInfo(), shortLink.getStatus(), shortLink.getNote(), shortLink.getExpirationTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new ShortLinkDataObject(shortLink.getGuid(), shortLink.getAppClient(), shortLink.getClientRootDomain(), shortLink.getOwner(), shortLink.getLongUrlDomain(), shortLink.getLongUrl(), shortLink.getLongUrlFull(), shortLink.getLongUrlHash(), shortLink.isReuseExistingShortUrl(), shortLink.isFailIfShortUrlExists(), shortLink.getDomain(), shortLink.getDomainType(), shortLink.getUsercode(), shortLink.getUsername(), shortLink.getTokenPrefix(), shortLink.getToken(), shortLink.getTokenType(), shortLink.getSassyTokenType(), shortLink.getShortUrl(), shortLink.getShortPassage(), shortLink.getRedirectType(), shortLink.getFlashDuration(), shortLink.getAccessType(), shortLink.getViewType(), shortLink.getShareType(), shortLink.isReadOnly(), shortLink.getDisplayMessage(), shortLink.getShortMessage(), shortLink.isKeywordEnabled(), shortLink.isBookmarkEnabled(), (ReferrerInfoStructDataObject) shortLink.getReferrerInfo(), shortLink.getStatus(), shortLink.getNote(), shortLink.getExpirationTime());
        }
        String guid = getDAOFactory().getShortLinkDAO().createShortLink(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateShortLink(String guid, String appClient, String clientRootDomain, String owner, String longUrlDomain, String longUrl, String longUrlFull, String longUrlHash, Boolean reuseExistingShortUrl, Boolean failIfShortUrlExists, String domain, String domainType, String usercode, String username, String tokenPrefix, String token, String tokenType, String sassyTokenType, String shortUrl, String shortPassage, String redirectType, Long flashDuration, String accessType, String viewType, String shareType, Boolean readOnly, String displayMessage, String shortMessage, Boolean keywordEnabled, Boolean bookmarkEnabled, ReferrerInfoStruct referrerInfo, String status, String note, Long expirationTime) throws BaseException
    {
        ReferrerInfoStructDataObject referrerInfoDobj = null;
        if(referrerInfo instanceof ReferrerInfoStructBean) {
            referrerInfoDobj = ((ReferrerInfoStructBean) referrerInfo).toDataObject();            
        } else if(referrerInfo instanceof ReferrerInfoStruct) {
            referrerInfoDobj = new ReferrerInfoStructDataObject(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            referrerInfoDobj = null;   // ????
        }

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        ShortLinkDataObject dataObj = new ShortLinkDataObject(guid, appClient, clientRootDomain, owner, longUrlDomain, longUrl, longUrlFull, longUrlHash, reuseExistingShortUrl, failIfShortUrlExists, domain, domainType, usercode, username, tokenPrefix, token, tokenType, sassyTokenType, shortUrl, shortPassage, redirectType, flashDuration, accessType, viewType, shareType, readOnly, displayMessage, shortMessage, keywordEnabled, bookmarkEnabled, referrerInfoDobj, status, note, expirationTime);
        return updateShortLink(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateShortLink(ShortLink shortLink) throws BaseException
    {
        log.finer("BEGIN");

        // Param shortLink cannot be null.....
        if(shortLink == null || shortLink.getGuid() == null) {
            log.log(Level.INFO, "Param shortLink or its guid is null!");
            throw new BadRequestException("Param shortLink object or its guid is null!");
        }
        ShortLinkDataObject dataObj = null;
        if(shortLink instanceof ShortLinkDataObject) {
            dataObj = (ShortLinkDataObject) shortLink;
        } else if(shortLink instanceof ShortLinkBean) {
            dataObj = ((ShortLinkBean) shortLink).toDataObject();
        } else {  // if(shortLink instanceof ShortLink)
            dataObj = new ShortLinkDataObject(shortLink.getGuid(), shortLink.getAppClient(), shortLink.getClientRootDomain(), shortLink.getOwner(), shortLink.getLongUrlDomain(), shortLink.getLongUrl(), shortLink.getLongUrlFull(), shortLink.getLongUrlHash(), shortLink.isReuseExistingShortUrl(), shortLink.isFailIfShortUrlExists(), shortLink.getDomain(), shortLink.getDomainType(), shortLink.getUsercode(), shortLink.getUsername(), shortLink.getTokenPrefix(), shortLink.getToken(), shortLink.getTokenType(), shortLink.getSassyTokenType(), shortLink.getShortUrl(), shortLink.getShortPassage(), shortLink.getRedirectType(), shortLink.getFlashDuration(), shortLink.getAccessType(), shortLink.getViewType(), shortLink.getShareType(), shortLink.isReadOnly(), shortLink.getDisplayMessage(), shortLink.getShortMessage(), shortLink.isKeywordEnabled(), shortLink.isBookmarkEnabled(), shortLink.getReferrerInfo(), shortLink.getStatus(), shortLink.getNote(), shortLink.getExpirationTime());
        }
        Boolean suc = getDAOFactory().getShortLinkDAO().updateShortLink(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteShortLink(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getShortLinkDAO().deleteShortLink(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteShortLink(ShortLink shortLink) throws BaseException
    {
        log.finer("BEGIN");

        // Param shortLink cannot be null.....
        if(shortLink == null || shortLink.getGuid() == null) {
            log.log(Level.INFO, "Param shortLink or its guid is null!");
            throw new BadRequestException("Param shortLink object or its guid is null!");
        }
        ShortLinkDataObject dataObj = null;
        if(shortLink instanceof ShortLinkDataObject) {
            dataObj = (ShortLinkDataObject) shortLink;
        } else if(shortLink instanceof ShortLinkBean) {
            dataObj = ((ShortLinkBean) shortLink).toDataObject();
        } else {  // if(shortLink instanceof ShortLink)
            dataObj = new ShortLinkDataObject(shortLink.getGuid(), shortLink.getAppClient(), shortLink.getClientRootDomain(), shortLink.getOwner(), shortLink.getLongUrlDomain(), shortLink.getLongUrl(), shortLink.getLongUrlFull(), shortLink.getLongUrlHash(), shortLink.isReuseExistingShortUrl(), shortLink.isFailIfShortUrlExists(), shortLink.getDomain(), shortLink.getDomainType(), shortLink.getUsercode(), shortLink.getUsername(), shortLink.getTokenPrefix(), shortLink.getToken(), shortLink.getTokenType(), shortLink.getSassyTokenType(), shortLink.getShortUrl(), shortLink.getShortPassage(), shortLink.getRedirectType(), shortLink.getFlashDuration(), shortLink.getAccessType(), shortLink.getViewType(), shortLink.getShareType(), shortLink.isReadOnly(), shortLink.getDisplayMessage(), shortLink.getShortMessage(), shortLink.isKeywordEnabled(), shortLink.isBookmarkEnabled(), shortLink.getReferrerInfo(), shortLink.getStatus(), shortLink.getNote(), shortLink.getExpirationTime());
        }
        Boolean suc = getDAOFactory().getShortLinkDAO().deleteShortLink(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteShortLinks(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getShortLinkDAO().deleteShortLinks(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
