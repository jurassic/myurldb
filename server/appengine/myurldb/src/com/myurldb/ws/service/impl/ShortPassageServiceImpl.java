package com.myurldb.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.ShortPassageAttribute;
import com.myurldb.ws.ShortPassage;
import com.myurldb.ws.bean.ShortPassageAttributeBean;
import com.myurldb.ws.bean.ShortPassageBean;
import com.myurldb.ws.dao.DAOFactory;
import com.myurldb.ws.data.ShortPassageAttributeDataObject;
import com.myurldb.ws.data.ShortPassageDataObject;
import com.myurldb.ws.service.DAOFactoryManager;
import com.myurldb.ws.service.ShortPassageService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class ShortPassageServiceImpl implements ShortPassageService
{
    private static final Logger log = Logger.getLogger(ShortPassageServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // ShortPassage related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public ShortPassage getShortPassage(String guid) throws BaseException
    {
        log.finer("BEGIN");

        ShortPassageDataObject dataObj = getDAOFactory().getShortPassageDAO().getShortPassage(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve ShortPassageDataObject for guid = " + guid);
            return null;  // ????
        }
        ShortPassageBean bean = new ShortPassageBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getShortPassage(String guid, String field) throws BaseException
    {
        ShortPassageDataObject dataObj = getDAOFactory().getShortPassageDAO().getShortPassage(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve ShortPassageDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("owner")) {
            return dataObj.getOwner();
        } else if(field.equals("longText")) {
            return dataObj.getLongText();
        } else if(field.equals("shortText")) {
            return dataObj.getShortText();
        } else if(field.equals("attribute")) {
            return dataObj.getAttribute();
        } else if(field.equals("readOnly")) {
            return dataObj.isReadOnly();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("note")) {
            return dataObj.getNote();
        } else if(field.equals("expirationTime")) {
            return dataObj.getExpirationTime();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<ShortPassage> getShortPassages(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<ShortPassage> list = new ArrayList<ShortPassage>();
        List<ShortPassageDataObject> dataObjs = getDAOFactory().getShortPassageDAO().getShortPassages(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve ShortPassageDataObject list.");
        } else {
            Iterator<ShortPassageDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                ShortPassageDataObject dataObj = (ShortPassageDataObject) it.next();
                list.add(new ShortPassageBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<ShortPassage> getAllShortPassages() throws BaseException
    {
        return getAllShortPassages(null, null, null);
    }

    @Override
    public List<ShortPassage> getAllShortPassages(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<ShortPassage> list = new ArrayList<ShortPassage>();
        List<ShortPassageDataObject> dataObjs = getDAOFactory().getShortPassageDAO().getAllShortPassages(ordering, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve ShortPassageDataObject list.");
        } else {
            Iterator<ShortPassageDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                ShortPassageDataObject dataObj = (ShortPassageDataObject) it.next();
                list.add(new ShortPassageBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllShortPassageKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        List<String> keys = getDAOFactory().getShortPassageDAO().getAllShortPassageKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve ShortPassage key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<ShortPassage> findShortPassages(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findShortPassages(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<ShortPassage> findShortPassages(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("ShortPassageServiceImpl.findShortPassages(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<ShortPassage> list = new ArrayList<ShortPassage>();
        List<ShortPassageDataObject> dataObjs = getDAOFactory().getShortPassageDAO().findShortPassages(filter, ordering, params, values, grouping, unique, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find shortPassages for the given criterion.");
        } else {
            Iterator<ShortPassageDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                ShortPassageDataObject dataObj = (ShortPassageDataObject) it.next();
                list.add(new ShortPassageBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findShortPassageKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("ShortPassageServiceImpl.findShortPassageKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = getDAOFactory().getShortPassageDAO().findShortPassageKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find ShortPassage keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("ShortPassageServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getShortPassageDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createShortPassage(String owner, String longText, String shortText, ShortPassageAttribute attribute, Boolean readOnly, String status, String note, Long expirationTime) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        ShortPassageAttributeDataObject attributeDobj = null;
        if(attribute instanceof ShortPassageAttributeBean) {
            attributeDobj = ((ShortPassageAttributeBean) attribute).toDataObject();
        } else if(attribute instanceof ShortPassageAttribute) {
            attributeDobj = new ShortPassageAttributeDataObject(attribute.getDomain(), attribute.getTokenType(), attribute.getDisplayMessage(), attribute.getRedirectType(), attribute.getFlashDuration(), attribute.getAccessType(), attribute.getViewType(), attribute.getShareType());
        } else {
            attributeDobj = null;   // ????
        }
        
        ShortPassageDataObject dataObj = new ShortPassageDataObject(null, owner, longText, shortText, attributeDobj, readOnly, status, note, expirationTime);
        return createShortPassage(dataObj);
    }

    @Override
    public String createShortPassage(ShortPassage shortPassage) throws BaseException
    {
        log.finer("BEGIN");

        // Param shortPassage cannot be null.....
        if(shortPassage == null) {
            log.log(Level.INFO, "Param shortPassage is null!");
            throw new BadRequestException("Param shortPassage object is null!");
        }
        ShortPassageDataObject dataObj = null;
        if(shortPassage instanceof ShortPassageDataObject) {
            dataObj = (ShortPassageDataObject) shortPassage;
        } else if(shortPassage instanceof ShortPassageBean) {
            dataObj = ((ShortPassageBean) shortPassage).toDataObject();
        } else {  // if(shortPassage instanceof ShortPassage)
            //dataObj = new ShortPassageDataObject(null, shortPassage.getOwner(), shortPassage.getLongText(), shortPassage.getShortText(), (ShortPassageAttributeDataObject) shortPassage.getAttribute(), shortPassage.isReadOnly(), shortPassage.getStatus(), shortPassage.getNote(), shortPassage.getExpirationTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new ShortPassageDataObject(shortPassage.getGuid(), shortPassage.getOwner(), shortPassage.getLongText(), shortPassage.getShortText(), (ShortPassageAttributeDataObject) shortPassage.getAttribute(), shortPassage.isReadOnly(), shortPassage.getStatus(), shortPassage.getNote(), shortPassage.getExpirationTime());
        }
        String guid = getDAOFactory().getShortPassageDAO().createShortPassage(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateShortPassage(String guid, String owner, String longText, String shortText, ShortPassageAttribute attribute, Boolean readOnly, String status, String note, Long expirationTime) throws BaseException
    {
        ShortPassageAttributeDataObject attributeDobj = null;
        if(attribute instanceof ShortPassageAttributeBean) {
            attributeDobj = ((ShortPassageAttributeBean) attribute).toDataObject();            
        } else if(attribute instanceof ShortPassageAttribute) {
            attributeDobj = new ShortPassageAttributeDataObject(attribute.getDomain(), attribute.getTokenType(), attribute.getDisplayMessage(), attribute.getRedirectType(), attribute.getFlashDuration(), attribute.getAccessType(), attribute.getViewType(), attribute.getShareType());
        } else {
            attributeDobj = null;   // ????
        }

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        ShortPassageDataObject dataObj = new ShortPassageDataObject(guid, owner, longText, shortText, attributeDobj, readOnly, status, note, expirationTime);
        return updateShortPassage(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateShortPassage(ShortPassage shortPassage) throws BaseException
    {
        log.finer("BEGIN");

        // Param shortPassage cannot be null.....
        if(shortPassage == null || shortPassage.getGuid() == null) {
            log.log(Level.INFO, "Param shortPassage or its guid is null!");
            throw new BadRequestException("Param shortPassage object or its guid is null!");
        }
        ShortPassageDataObject dataObj = null;
        if(shortPassage instanceof ShortPassageDataObject) {
            dataObj = (ShortPassageDataObject) shortPassage;
        } else if(shortPassage instanceof ShortPassageBean) {
            dataObj = ((ShortPassageBean) shortPassage).toDataObject();
        } else {  // if(shortPassage instanceof ShortPassage)
            dataObj = new ShortPassageDataObject(shortPassage.getGuid(), shortPassage.getOwner(), shortPassage.getLongText(), shortPassage.getShortText(), shortPassage.getAttribute(), shortPassage.isReadOnly(), shortPassage.getStatus(), shortPassage.getNote(), shortPassage.getExpirationTime());
        }
        Boolean suc = getDAOFactory().getShortPassageDAO().updateShortPassage(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteShortPassage(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getShortPassageDAO().deleteShortPassage(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteShortPassage(ShortPassage shortPassage) throws BaseException
    {
        log.finer("BEGIN");

        // Param shortPassage cannot be null.....
        if(shortPassage == null || shortPassage.getGuid() == null) {
            log.log(Level.INFO, "Param shortPassage or its guid is null!");
            throw new BadRequestException("Param shortPassage object or its guid is null!");
        }
        ShortPassageDataObject dataObj = null;
        if(shortPassage instanceof ShortPassageDataObject) {
            dataObj = (ShortPassageDataObject) shortPassage;
        } else if(shortPassage instanceof ShortPassageBean) {
            dataObj = ((ShortPassageBean) shortPassage).toDataObject();
        } else {  // if(shortPassage instanceof ShortPassage)
            dataObj = new ShortPassageDataObject(shortPassage.getGuid(), shortPassage.getOwner(), shortPassage.getLongText(), shortPassage.getShortText(), shortPassage.getAttribute(), shortPassage.isReadOnly(), shortPassage.getStatus(), shortPassage.getNote(), shortPassage.getExpirationTime());
        }
        Boolean suc = getDAOFactory().getShortPassageDAO().deleteShortPassage(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteShortPassages(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getShortPassageDAO().deleteShortPassages(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
