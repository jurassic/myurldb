package com.myurldb.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.SiteCustomDomain;
import com.myurldb.ws.bean.SiteCustomDomainBean;
import com.myurldb.ws.dao.DAOFactory;
import com.myurldb.ws.data.SiteCustomDomainDataObject;
import com.myurldb.ws.service.DAOFactoryManager;
import com.myurldb.ws.service.SiteCustomDomainService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class SiteCustomDomainServiceImpl implements SiteCustomDomainService
{
    private static final Logger log = Logger.getLogger(SiteCustomDomainServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // SiteCustomDomain related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public SiteCustomDomain getSiteCustomDomain(String guid) throws BaseException
    {
        log.finer("BEGIN");

        SiteCustomDomainDataObject dataObj = getDAOFactory().getSiteCustomDomainDAO().getSiteCustomDomain(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve SiteCustomDomainDataObject for guid = " + guid);
            return null;  // ????
        }
        SiteCustomDomainBean bean = new SiteCustomDomainBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getSiteCustomDomain(String guid, String field) throws BaseException
    {
        SiteCustomDomainDataObject dataObj = getDAOFactory().getSiteCustomDomainDAO().getSiteCustomDomain(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve SiteCustomDomainDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("siteDomain")) {
            return dataObj.getSiteDomain();
        } else if(field.equals("domain")) {
            return dataObj.getDomain();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<SiteCustomDomain> getSiteCustomDomains(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<SiteCustomDomain> list = new ArrayList<SiteCustomDomain>();
        List<SiteCustomDomainDataObject> dataObjs = getDAOFactory().getSiteCustomDomainDAO().getSiteCustomDomains(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve SiteCustomDomainDataObject list.");
        } else {
            Iterator<SiteCustomDomainDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                SiteCustomDomainDataObject dataObj = (SiteCustomDomainDataObject) it.next();
                list.add(new SiteCustomDomainBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<SiteCustomDomain> getAllSiteCustomDomains() throws BaseException
    {
        return getAllSiteCustomDomains(null, null, null);
    }

    @Override
    public List<SiteCustomDomain> getAllSiteCustomDomains(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<SiteCustomDomain> list = new ArrayList<SiteCustomDomain>();
        List<SiteCustomDomainDataObject> dataObjs = getDAOFactory().getSiteCustomDomainDAO().getAllSiteCustomDomains(ordering, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve SiteCustomDomainDataObject list.");
        } else {
            Iterator<SiteCustomDomainDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                SiteCustomDomainDataObject dataObj = (SiteCustomDomainDataObject) it.next();
                list.add(new SiteCustomDomainBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllSiteCustomDomainKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        List<String> keys = getDAOFactory().getSiteCustomDomainDAO().getAllSiteCustomDomainKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve SiteCustomDomain key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<SiteCustomDomain> findSiteCustomDomains(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findSiteCustomDomains(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<SiteCustomDomain> findSiteCustomDomains(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("SiteCustomDomainServiceImpl.findSiteCustomDomains(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<SiteCustomDomain> list = new ArrayList<SiteCustomDomain>();
        List<SiteCustomDomainDataObject> dataObjs = getDAOFactory().getSiteCustomDomainDAO().findSiteCustomDomains(filter, ordering, params, values, grouping, unique, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find siteCustomDomains for the given criterion.");
        } else {
            Iterator<SiteCustomDomainDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                SiteCustomDomainDataObject dataObj = (SiteCustomDomainDataObject) it.next();
                list.add(new SiteCustomDomainBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findSiteCustomDomainKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("SiteCustomDomainServiceImpl.findSiteCustomDomainKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = getDAOFactory().getSiteCustomDomainDAO().findSiteCustomDomainKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find SiteCustomDomain keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("SiteCustomDomainServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getSiteCustomDomainDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createSiteCustomDomain(String siteDomain, String domain, String status) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        SiteCustomDomainDataObject dataObj = new SiteCustomDomainDataObject(null, siteDomain, domain, status);
        return createSiteCustomDomain(dataObj);
    }

    @Override
    public String createSiteCustomDomain(SiteCustomDomain siteCustomDomain) throws BaseException
    {
        log.finer("BEGIN");

        // Param siteCustomDomain cannot be null.....
        if(siteCustomDomain == null) {
            log.log(Level.INFO, "Param siteCustomDomain is null!");
            throw new BadRequestException("Param siteCustomDomain object is null!");
        }
        SiteCustomDomainDataObject dataObj = null;
        if(siteCustomDomain instanceof SiteCustomDomainDataObject) {
            dataObj = (SiteCustomDomainDataObject) siteCustomDomain;
        } else if(siteCustomDomain instanceof SiteCustomDomainBean) {
            dataObj = ((SiteCustomDomainBean) siteCustomDomain).toDataObject();
        } else {  // if(siteCustomDomain instanceof SiteCustomDomain)
            //dataObj = new SiteCustomDomainDataObject(null, siteCustomDomain.getSiteDomain(), siteCustomDomain.getDomain(), siteCustomDomain.getStatus());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new SiteCustomDomainDataObject(siteCustomDomain.getGuid(), siteCustomDomain.getSiteDomain(), siteCustomDomain.getDomain(), siteCustomDomain.getStatus());
        }
        String guid = getDAOFactory().getSiteCustomDomainDAO().createSiteCustomDomain(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateSiteCustomDomain(String guid, String siteDomain, String domain, String status) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        SiteCustomDomainDataObject dataObj = new SiteCustomDomainDataObject(guid, siteDomain, domain, status);
        return updateSiteCustomDomain(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateSiteCustomDomain(SiteCustomDomain siteCustomDomain) throws BaseException
    {
        log.finer("BEGIN");

        // Param siteCustomDomain cannot be null.....
        if(siteCustomDomain == null || siteCustomDomain.getGuid() == null) {
            log.log(Level.INFO, "Param siteCustomDomain or its guid is null!");
            throw new BadRequestException("Param siteCustomDomain object or its guid is null!");
        }
        SiteCustomDomainDataObject dataObj = null;
        if(siteCustomDomain instanceof SiteCustomDomainDataObject) {
            dataObj = (SiteCustomDomainDataObject) siteCustomDomain;
        } else if(siteCustomDomain instanceof SiteCustomDomainBean) {
            dataObj = ((SiteCustomDomainBean) siteCustomDomain).toDataObject();
        } else {  // if(siteCustomDomain instanceof SiteCustomDomain)
            dataObj = new SiteCustomDomainDataObject(siteCustomDomain.getGuid(), siteCustomDomain.getSiteDomain(), siteCustomDomain.getDomain(), siteCustomDomain.getStatus());
        }
        Boolean suc = getDAOFactory().getSiteCustomDomainDAO().updateSiteCustomDomain(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteSiteCustomDomain(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getSiteCustomDomainDAO().deleteSiteCustomDomain(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteSiteCustomDomain(SiteCustomDomain siteCustomDomain) throws BaseException
    {
        log.finer("BEGIN");

        // Param siteCustomDomain cannot be null.....
        if(siteCustomDomain == null || siteCustomDomain.getGuid() == null) {
            log.log(Level.INFO, "Param siteCustomDomain or its guid is null!");
            throw new BadRequestException("Param siteCustomDomain object or its guid is null!");
        }
        SiteCustomDomainDataObject dataObj = null;
        if(siteCustomDomain instanceof SiteCustomDomainDataObject) {
            dataObj = (SiteCustomDomainDataObject) siteCustomDomain;
        } else if(siteCustomDomain instanceof SiteCustomDomainBean) {
            dataObj = ((SiteCustomDomainBean) siteCustomDomain).toDataObject();
        } else {  // if(siteCustomDomain instanceof SiteCustomDomain)
            dataObj = new SiteCustomDomainDataObject(siteCustomDomain.getGuid(), siteCustomDomain.getSiteDomain(), siteCustomDomain.getDomain(), siteCustomDomain.getStatus());
        }
        Boolean suc = getDAOFactory().getSiteCustomDomainDAO().deleteSiteCustomDomain(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteSiteCustomDomains(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getSiteCustomDomainDAO().deleteSiteCustomDomains(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
