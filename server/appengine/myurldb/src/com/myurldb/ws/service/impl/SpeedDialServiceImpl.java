package com.myurldb.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.SpeedDial;
import com.myurldb.ws.bean.SpeedDialBean;
import com.myurldb.ws.dao.DAOFactory;
import com.myurldb.ws.data.SpeedDialDataObject;
import com.myurldb.ws.service.DAOFactoryManager;
import com.myurldb.ws.service.SpeedDialService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class SpeedDialServiceImpl implements SpeedDialService
{
    private static final Logger log = Logger.getLogger(SpeedDialServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // SpeedDial related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public SpeedDial getSpeedDial(String guid) throws BaseException
    {
        log.finer("BEGIN");

        SpeedDialDataObject dataObj = getDAOFactory().getSpeedDialDAO().getSpeedDial(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve SpeedDialDataObject for guid = " + guid);
            return null;  // ????
        }
        SpeedDialBean bean = new SpeedDialBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getSpeedDial(String guid, String field) throws BaseException
    {
        SpeedDialDataObject dataObj = getDAOFactory().getSpeedDialDAO().getSpeedDial(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve SpeedDialDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return dataObj.getUser();
        } else if(field.equals("code")) {
            return dataObj.getCode();
        } else if(field.equals("shortcut")) {
            return dataObj.getShortcut();
        } else if(field.equals("shortLink")) {
            return dataObj.getShortLink();
        } else if(field.equals("keywordLink")) {
            return dataObj.getKeywordLink();
        } else if(field.equals("bookmarkLink")) {
            return dataObj.getBookmarkLink();
        } else if(field.equals("longUrl")) {
            return dataObj.getLongUrl();
        } else if(field.equals("shortUrl")) {
            return dataObj.getShortUrl();
        } else if(field.equals("caseSensitive")) {
            return dataObj.isCaseSensitive();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("note")) {
            return dataObj.getNote();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<SpeedDial> getSpeedDials(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<SpeedDial> list = new ArrayList<SpeedDial>();
        List<SpeedDialDataObject> dataObjs = getDAOFactory().getSpeedDialDAO().getSpeedDials(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve SpeedDialDataObject list.");
        } else {
            Iterator<SpeedDialDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                SpeedDialDataObject dataObj = (SpeedDialDataObject) it.next();
                list.add(new SpeedDialBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<SpeedDial> getAllSpeedDials() throws BaseException
    {
        return getAllSpeedDials(null, null, null);
    }

    @Override
    public List<SpeedDial> getAllSpeedDials(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<SpeedDial> list = new ArrayList<SpeedDial>();
        List<SpeedDialDataObject> dataObjs = getDAOFactory().getSpeedDialDAO().getAllSpeedDials(ordering, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve SpeedDialDataObject list.");
        } else {
            Iterator<SpeedDialDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                SpeedDialDataObject dataObj = (SpeedDialDataObject) it.next();
                list.add(new SpeedDialBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllSpeedDialKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        List<String> keys = getDAOFactory().getSpeedDialDAO().getAllSpeedDialKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve SpeedDial key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<SpeedDial> findSpeedDials(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findSpeedDials(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<SpeedDial> findSpeedDials(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("SpeedDialServiceImpl.findSpeedDials(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<SpeedDial> list = new ArrayList<SpeedDial>();
        List<SpeedDialDataObject> dataObjs = getDAOFactory().getSpeedDialDAO().findSpeedDials(filter, ordering, params, values, grouping, unique, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find speedDials for the given criterion.");
        } else {
            Iterator<SpeedDialDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                SpeedDialDataObject dataObj = (SpeedDialDataObject) it.next();
                list.add(new SpeedDialBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findSpeedDialKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("SpeedDialServiceImpl.findSpeedDialKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = getDAOFactory().getSpeedDialDAO().findSpeedDialKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find SpeedDial keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("SpeedDialServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getSpeedDialDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createSpeedDial(String user, String code, String shortcut, String shortLink, String keywordLink, String bookmarkLink, String longUrl, String shortUrl, Boolean caseSensitive, String status, String note) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        SpeedDialDataObject dataObj = new SpeedDialDataObject(null, user, code, shortcut, shortLink, keywordLink, bookmarkLink, longUrl, shortUrl, caseSensitive, status, note);
        return createSpeedDial(dataObj);
    }

    @Override
    public String createSpeedDial(SpeedDial speedDial) throws BaseException
    {
        log.finer("BEGIN");

        // Param speedDial cannot be null.....
        if(speedDial == null) {
            log.log(Level.INFO, "Param speedDial is null!");
            throw new BadRequestException("Param speedDial object is null!");
        }
        SpeedDialDataObject dataObj = null;
        if(speedDial instanceof SpeedDialDataObject) {
            dataObj = (SpeedDialDataObject) speedDial;
        } else if(speedDial instanceof SpeedDialBean) {
            dataObj = ((SpeedDialBean) speedDial).toDataObject();
        } else {  // if(speedDial instanceof SpeedDial)
            //dataObj = new SpeedDialDataObject(null, speedDial.getUser(), speedDial.getCode(), speedDial.getShortcut(), speedDial.getShortLink(), speedDial.getKeywordLink(), speedDial.getBookmarkLink(), speedDial.getLongUrl(), speedDial.getShortUrl(), speedDial.isCaseSensitive(), speedDial.getStatus(), speedDial.getNote());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new SpeedDialDataObject(speedDial.getGuid(), speedDial.getUser(), speedDial.getCode(), speedDial.getShortcut(), speedDial.getShortLink(), speedDial.getKeywordLink(), speedDial.getBookmarkLink(), speedDial.getLongUrl(), speedDial.getShortUrl(), speedDial.isCaseSensitive(), speedDial.getStatus(), speedDial.getNote());
        }
        String guid = getDAOFactory().getSpeedDialDAO().createSpeedDial(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateSpeedDial(String guid, String user, String code, String shortcut, String shortLink, String keywordLink, String bookmarkLink, String longUrl, String shortUrl, Boolean caseSensitive, String status, String note) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        SpeedDialDataObject dataObj = new SpeedDialDataObject(guid, user, code, shortcut, shortLink, keywordLink, bookmarkLink, longUrl, shortUrl, caseSensitive, status, note);
        return updateSpeedDial(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateSpeedDial(SpeedDial speedDial) throws BaseException
    {
        log.finer("BEGIN");

        // Param speedDial cannot be null.....
        if(speedDial == null || speedDial.getGuid() == null) {
            log.log(Level.INFO, "Param speedDial or its guid is null!");
            throw new BadRequestException("Param speedDial object or its guid is null!");
        }
        SpeedDialDataObject dataObj = null;
        if(speedDial instanceof SpeedDialDataObject) {
            dataObj = (SpeedDialDataObject) speedDial;
        } else if(speedDial instanceof SpeedDialBean) {
            dataObj = ((SpeedDialBean) speedDial).toDataObject();
        } else {  // if(speedDial instanceof SpeedDial)
            dataObj = new SpeedDialDataObject(speedDial.getGuid(), speedDial.getUser(), speedDial.getCode(), speedDial.getShortcut(), speedDial.getShortLink(), speedDial.getKeywordLink(), speedDial.getBookmarkLink(), speedDial.getLongUrl(), speedDial.getShortUrl(), speedDial.isCaseSensitive(), speedDial.getStatus(), speedDial.getNote());
        }
        Boolean suc = getDAOFactory().getSpeedDialDAO().updateSpeedDial(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteSpeedDial(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getSpeedDialDAO().deleteSpeedDial(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteSpeedDial(SpeedDial speedDial) throws BaseException
    {
        log.finer("BEGIN");

        // Param speedDial cannot be null.....
        if(speedDial == null || speedDial.getGuid() == null) {
            log.log(Level.INFO, "Param speedDial or its guid is null!");
            throw new BadRequestException("Param speedDial object or its guid is null!");
        }
        SpeedDialDataObject dataObj = null;
        if(speedDial instanceof SpeedDialDataObject) {
            dataObj = (SpeedDialDataObject) speedDial;
        } else if(speedDial instanceof SpeedDialBean) {
            dataObj = ((SpeedDialBean) speedDial).toDataObject();
        } else {  // if(speedDial instanceof SpeedDial)
            dataObj = new SpeedDialDataObject(speedDial.getGuid(), speedDial.getUser(), speedDial.getCode(), speedDial.getShortcut(), speedDial.getShortLink(), speedDial.getKeywordLink(), speedDial.getBookmarkLink(), speedDial.getLongUrl(), speedDial.getShortUrl(), speedDial.isCaseSensitive(), speedDial.getStatus(), speedDial.getNote());
        }
        Boolean suc = getDAOFactory().getSpeedDialDAO().deleteSpeedDial(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteSpeedDials(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getSpeedDialDAO().deleteSpeedDials(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
