package com.myurldb.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.TwitterAppCard;
import com.myurldb.ws.bean.TwitterCardAppInfoBean;
import com.myurldb.ws.bean.TwitterCardProductDataBean;
import com.myurldb.ws.bean.TwitterAppCardBean;
import com.myurldb.ws.dao.DAOFactory;
import com.myurldb.ws.data.TwitterCardAppInfoDataObject;
import com.myurldb.ws.data.TwitterCardProductDataDataObject;
import com.myurldb.ws.data.TwitterAppCardDataObject;
import com.myurldb.ws.service.DAOFactoryManager;
import com.myurldb.ws.service.TwitterAppCardService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class TwitterAppCardServiceImpl implements TwitterAppCardService
{
    private static final Logger log = Logger.getLogger(TwitterAppCardServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // TwitterAppCard related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public TwitterAppCard getTwitterAppCard(String guid) throws BaseException
    {
        log.finer("BEGIN");

        TwitterAppCardDataObject dataObj = getDAOFactory().getTwitterAppCardDAO().getTwitterAppCard(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve TwitterAppCardDataObject for guid = " + guid);
            return null;  // ????
        }
        TwitterAppCardBean bean = new TwitterAppCardBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getTwitterAppCard(String guid, String field) throws BaseException
    {
        TwitterAppCardDataObject dataObj = getDAOFactory().getTwitterAppCardDAO().getTwitterAppCard(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve TwitterAppCardDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("card")) {
            return dataObj.getCard();
        } else if(field.equals("url")) {
            return dataObj.getUrl();
        } else if(field.equals("title")) {
            return dataObj.getTitle();
        } else if(field.equals("description")) {
            return dataObj.getDescription();
        } else if(field.equals("site")) {
            return dataObj.getSite();
        } else if(field.equals("siteId")) {
            return dataObj.getSiteId();
        } else if(field.equals("creator")) {
            return dataObj.getCreator();
        } else if(field.equals("creatorId")) {
            return dataObj.getCreatorId();
        } else if(field.equals("image")) {
            return dataObj.getImage();
        } else if(field.equals("imageWidth")) {
            return dataObj.getImageWidth();
        } else if(field.equals("imageHeight")) {
            return dataObj.getImageHeight();
        } else if(field.equals("iphoneApp")) {
            return dataObj.getIphoneApp();
        } else if(field.equals("ipadApp")) {
            return dataObj.getIpadApp();
        } else if(field.equals("googlePlayApp")) {
            return dataObj.getGooglePlayApp();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<TwitterAppCard> getTwitterAppCards(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<TwitterAppCard> list = new ArrayList<TwitterAppCard>();
        List<TwitterAppCardDataObject> dataObjs = getDAOFactory().getTwitterAppCardDAO().getTwitterAppCards(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve TwitterAppCardDataObject list.");
        } else {
            Iterator<TwitterAppCardDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                TwitterAppCardDataObject dataObj = (TwitterAppCardDataObject) it.next();
                list.add(new TwitterAppCardBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<TwitterAppCard> getAllTwitterAppCards() throws BaseException
    {
        return getAllTwitterAppCards(null, null, null);
    }

    @Override
    public List<TwitterAppCard> getAllTwitterAppCards(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<TwitterAppCard> list = new ArrayList<TwitterAppCard>();
        List<TwitterAppCardDataObject> dataObjs = getDAOFactory().getTwitterAppCardDAO().getAllTwitterAppCards(ordering, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve TwitterAppCardDataObject list.");
        } else {
            Iterator<TwitterAppCardDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                TwitterAppCardDataObject dataObj = (TwitterAppCardDataObject) it.next();
                list.add(new TwitterAppCardBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllTwitterAppCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        List<String> keys = getDAOFactory().getTwitterAppCardDAO().getAllTwitterAppCardKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve TwitterAppCard key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<TwitterAppCard> findTwitterAppCards(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findTwitterAppCards(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<TwitterAppCard> findTwitterAppCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("TwitterAppCardServiceImpl.findTwitterAppCards(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<TwitterAppCard> list = new ArrayList<TwitterAppCard>();
        List<TwitterAppCardDataObject> dataObjs = getDAOFactory().getTwitterAppCardDAO().findTwitterAppCards(filter, ordering, params, values, grouping, unique, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find twitterAppCards for the given criterion.");
        } else {
            Iterator<TwitterAppCardDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                TwitterAppCardDataObject dataObj = (TwitterAppCardDataObject) it.next();
                list.add(new TwitterAppCardBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findTwitterAppCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("TwitterAppCardServiceImpl.findTwitterAppCardKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = getDAOFactory().getTwitterAppCardDAO().findTwitterAppCardKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find TwitterAppCard keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("TwitterAppCardServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getTwitterAppCardDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createTwitterAppCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, TwitterCardAppInfo iphoneApp, TwitterCardAppInfo ipadApp, TwitterCardAppInfo googlePlayApp) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        TwitterCardAppInfoDataObject iphoneAppDobj = null;
        if(iphoneApp instanceof TwitterCardAppInfoBean) {
            iphoneAppDobj = ((TwitterCardAppInfoBean) iphoneApp).toDataObject();
        } else if(iphoneApp instanceof TwitterCardAppInfo) {
            iphoneAppDobj = new TwitterCardAppInfoDataObject(iphoneApp.getName(), iphoneApp.getId(), iphoneApp.getUrl());
        } else {
            iphoneAppDobj = null;   // ????
        }
        TwitterCardAppInfoDataObject ipadAppDobj = null;
        if(ipadApp instanceof TwitterCardAppInfoBean) {
            ipadAppDobj = ((TwitterCardAppInfoBean) ipadApp).toDataObject();
        } else if(ipadApp instanceof TwitterCardAppInfo) {
            ipadAppDobj = new TwitterCardAppInfoDataObject(ipadApp.getName(), ipadApp.getId(), ipadApp.getUrl());
        } else {
            ipadAppDobj = null;   // ????
        }
        TwitterCardAppInfoDataObject googlePlayAppDobj = null;
        if(googlePlayApp instanceof TwitterCardAppInfoBean) {
            googlePlayAppDobj = ((TwitterCardAppInfoBean) googlePlayApp).toDataObject();
        } else if(googlePlayApp instanceof TwitterCardAppInfo) {
            googlePlayAppDobj = new TwitterCardAppInfoDataObject(googlePlayApp.getName(), googlePlayApp.getId(), googlePlayApp.getUrl());
        } else {
            googlePlayAppDobj = null;   // ????
        }
        
        TwitterAppCardDataObject dataObj = new TwitterAppCardDataObject(null, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, iphoneAppDobj, ipadAppDobj, googlePlayAppDobj);
        return createTwitterAppCard(dataObj);
    }

    @Override
    public String createTwitterAppCard(TwitterAppCard twitterAppCard) throws BaseException
    {
        log.finer("BEGIN");

        // Param twitterAppCard cannot be null.....
        if(twitterAppCard == null) {
            log.log(Level.INFO, "Param twitterAppCard is null!");
            throw new BadRequestException("Param twitterAppCard object is null!");
        }
        TwitterAppCardDataObject dataObj = null;
        if(twitterAppCard instanceof TwitterAppCardDataObject) {
            dataObj = (TwitterAppCardDataObject) twitterAppCard;
        } else if(twitterAppCard instanceof TwitterAppCardBean) {
            dataObj = ((TwitterAppCardBean) twitterAppCard).toDataObject();
        } else {  // if(twitterAppCard instanceof TwitterAppCard)
            //dataObj = new TwitterAppCardDataObject(null, twitterAppCard.getCard(), twitterAppCard.getUrl(), twitterAppCard.getTitle(), twitterAppCard.getDescription(), twitterAppCard.getSite(), twitterAppCard.getSiteId(), twitterAppCard.getCreator(), twitterAppCard.getCreatorId(), twitterAppCard.getImage(), twitterAppCard.getImageWidth(), twitterAppCard.getImageHeight(), (TwitterCardAppInfoDataObject) twitterAppCard.getIphoneApp(), (TwitterCardAppInfoDataObject) twitterAppCard.getIpadApp(), (TwitterCardAppInfoDataObject) twitterAppCard.getGooglePlayApp());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new TwitterAppCardDataObject(twitterAppCard.getGuid(), twitterAppCard.getCard(), twitterAppCard.getUrl(), twitterAppCard.getTitle(), twitterAppCard.getDescription(), twitterAppCard.getSite(), twitterAppCard.getSiteId(), twitterAppCard.getCreator(), twitterAppCard.getCreatorId(), twitterAppCard.getImage(), twitterAppCard.getImageWidth(), twitterAppCard.getImageHeight(), (TwitterCardAppInfoDataObject) twitterAppCard.getIphoneApp(), (TwitterCardAppInfoDataObject) twitterAppCard.getIpadApp(), (TwitterCardAppInfoDataObject) twitterAppCard.getGooglePlayApp());
        }
        String guid = getDAOFactory().getTwitterAppCardDAO().createTwitterAppCard(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateTwitterAppCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, TwitterCardAppInfo iphoneApp, TwitterCardAppInfo ipadApp, TwitterCardAppInfo googlePlayApp) throws BaseException
    {
        TwitterCardAppInfoDataObject iphoneAppDobj = null;
        if(iphoneApp instanceof TwitterCardAppInfoBean) {
            iphoneAppDobj = ((TwitterCardAppInfoBean) iphoneApp).toDataObject();            
        } else if(iphoneApp instanceof TwitterCardAppInfo) {
            iphoneAppDobj = new TwitterCardAppInfoDataObject(iphoneApp.getName(), iphoneApp.getId(), iphoneApp.getUrl());
        } else {
            iphoneAppDobj = null;   // ????
        }
        TwitterCardAppInfoDataObject ipadAppDobj = null;
        if(ipadApp instanceof TwitterCardAppInfoBean) {
            ipadAppDobj = ((TwitterCardAppInfoBean) ipadApp).toDataObject();            
        } else if(ipadApp instanceof TwitterCardAppInfo) {
            ipadAppDobj = new TwitterCardAppInfoDataObject(ipadApp.getName(), ipadApp.getId(), ipadApp.getUrl());
        } else {
            ipadAppDobj = null;   // ????
        }
        TwitterCardAppInfoDataObject googlePlayAppDobj = null;
        if(googlePlayApp instanceof TwitterCardAppInfoBean) {
            googlePlayAppDobj = ((TwitterCardAppInfoBean) googlePlayApp).toDataObject();            
        } else if(googlePlayApp instanceof TwitterCardAppInfo) {
            googlePlayAppDobj = new TwitterCardAppInfoDataObject(googlePlayApp.getName(), googlePlayApp.getId(), googlePlayApp.getUrl());
        } else {
            googlePlayAppDobj = null;   // ????
        }

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        TwitterAppCardDataObject dataObj = new TwitterAppCardDataObject(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, iphoneAppDobj, ipadAppDobj, googlePlayAppDobj);
        return updateTwitterAppCard(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateTwitterAppCard(TwitterAppCard twitterAppCard) throws BaseException
    {
        log.finer("BEGIN");

        // Param twitterAppCard cannot be null.....
        if(twitterAppCard == null || twitterAppCard.getGuid() == null) {
            log.log(Level.INFO, "Param twitterAppCard or its guid is null!");
            throw new BadRequestException("Param twitterAppCard object or its guid is null!");
        }
        TwitterAppCardDataObject dataObj = null;
        if(twitterAppCard instanceof TwitterAppCardDataObject) {
            dataObj = (TwitterAppCardDataObject) twitterAppCard;
        } else if(twitterAppCard instanceof TwitterAppCardBean) {
            dataObj = ((TwitterAppCardBean) twitterAppCard).toDataObject();
        } else {  // if(twitterAppCard instanceof TwitterAppCard)
            dataObj = new TwitterAppCardDataObject(twitterAppCard.getGuid(), twitterAppCard.getCard(), twitterAppCard.getUrl(), twitterAppCard.getTitle(), twitterAppCard.getDescription(), twitterAppCard.getSite(), twitterAppCard.getSiteId(), twitterAppCard.getCreator(), twitterAppCard.getCreatorId(), twitterAppCard.getImage(), twitterAppCard.getImageWidth(), twitterAppCard.getImageHeight(), twitterAppCard.getIphoneApp(), twitterAppCard.getIpadApp(), twitterAppCard.getGooglePlayApp());
        }
        Boolean suc = getDAOFactory().getTwitterAppCardDAO().updateTwitterAppCard(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteTwitterAppCard(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getTwitterAppCardDAO().deleteTwitterAppCard(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteTwitterAppCard(TwitterAppCard twitterAppCard) throws BaseException
    {
        log.finer("BEGIN");

        // Param twitterAppCard cannot be null.....
        if(twitterAppCard == null || twitterAppCard.getGuid() == null) {
            log.log(Level.INFO, "Param twitterAppCard or its guid is null!");
            throw new BadRequestException("Param twitterAppCard object or its guid is null!");
        }
        TwitterAppCardDataObject dataObj = null;
        if(twitterAppCard instanceof TwitterAppCardDataObject) {
            dataObj = (TwitterAppCardDataObject) twitterAppCard;
        } else if(twitterAppCard instanceof TwitterAppCardBean) {
            dataObj = ((TwitterAppCardBean) twitterAppCard).toDataObject();
        } else {  // if(twitterAppCard instanceof TwitterAppCard)
            dataObj = new TwitterAppCardDataObject(twitterAppCard.getGuid(), twitterAppCard.getCard(), twitterAppCard.getUrl(), twitterAppCard.getTitle(), twitterAppCard.getDescription(), twitterAppCard.getSite(), twitterAppCard.getSiteId(), twitterAppCard.getCreator(), twitterAppCard.getCreatorId(), twitterAppCard.getImage(), twitterAppCard.getImageWidth(), twitterAppCard.getImageHeight(), twitterAppCard.getIphoneApp(), twitterAppCard.getIpadApp(), twitterAppCard.getGooglePlayApp());
        }
        Boolean suc = getDAOFactory().getTwitterAppCardDAO().deleteTwitterAppCard(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteTwitterAppCards(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getTwitterAppCardDAO().deleteTwitterAppCards(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
