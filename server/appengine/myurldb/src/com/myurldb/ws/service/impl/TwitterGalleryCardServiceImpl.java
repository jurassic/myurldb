package com.myurldb.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.TwitterGalleryCard;
import com.myurldb.ws.bean.TwitterCardAppInfoBean;
import com.myurldb.ws.bean.TwitterCardProductDataBean;
import com.myurldb.ws.bean.TwitterGalleryCardBean;
import com.myurldb.ws.dao.DAOFactory;
import com.myurldb.ws.data.TwitterCardAppInfoDataObject;
import com.myurldb.ws.data.TwitterCardProductDataDataObject;
import com.myurldb.ws.data.TwitterGalleryCardDataObject;
import com.myurldb.ws.service.DAOFactoryManager;
import com.myurldb.ws.service.TwitterGalleryCardService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class TwitterGalleryCardServiceImpl implements TwitterGalleryCardService
{
    private static final Logger log = Logger.getLogger(TwitterGalleryCardServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // TwitterGalleryCard related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public TwitterGalleryCard getTwitterGalleryCard(String guid) throws BaseException
    {
        log.finer("BEGIN");

        TwitterGalleryCardDataObject dataObj = getDAOFactory().getTwitterGalleryCardDAO().getTwitterGalleryCard(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve TwitterGalleryCardDataObject for guid = " + guid);
            return null;  // ????
        }
        TwitterGalleryCardBean bean = new TwitterGalleryCardBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getTwitterGalleryCard(String guid, String field) throws BaseException
    {
        TwitterGalleryCardDataObject dataObj = getDAOFactory().getTwitterGalleryCardDAO().getTwitterGalleryCard(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve TwitterGalleryCardDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("card")) {
            return dataObj.getCard();
        } else if(field.equals("url")) {
            return dataObj.getUrl();
        } else if(field.equals("title")) {
            return dataObj.getTitle();
        } else if(field.equals("description")) {
            return dataObj.getDescription();
        } else if(field.equals("site")) {
            return dataObj.getSite();
        } else if(field.equals("siteId")) {
            return dataObj.getSiteId();
        } else if(field.equals("creator")) {
            return dataObj.getCreator();
        } else if(field.equals("creatorId")) {
            return dataObj.getCreatorId();
        } else if(field.equals("image0")) {
            return dataObj.getImage0();
        } else if(field.equals("image1")) {
            return dataObj.getImage1();
        } else if(field.equals("image2")) {
            return dataObj.getImage2();
        } else if(field.equals("image3")) {
            return dataObj.getImage3();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<TwitterGalleryCard> getTwitterGalleryCards(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<TwitterGalleryCard> list = new ArrayList<TwitterGalleryCard>();
        List<TwitterGalleryCardDataObject> dataObjs = getDAOFactory().getTwitterGalleryCardDAO().getTwitterGalleryCards(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve TwitterGalleryCardDataObject list.");
        } else {
            Iterator<TwitterGalleryCardDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                TwitterGalleryCardDataObject dataObj = (TwitterGalleryCardDataObject) it.next();
                list.add(new TwitterGalleryCardBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<TwitterGalleryCard> getAllTwitterGalleryCards() throws BaseException
    {
        return getAllTwitterGalleryCards(null, null, null);
    }

    @Override
    public List<TwitterGalleryCard> getAllTwitterGalleryCards(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<TwitterGalleryCard> list = new ArrayList<TwitterGalleryCard>();
        List<TwitterGalleryCardDataObject> dataObjs = getDAOFactory().getTwitterGalleryCardDAO().getAllTwitterGalleryCards(ordering, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve TwitterGalleryCardDataObject list.");
        } else {
            Iterator<TwitterGalleryCardDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                TwitterGalleryCardDataObject dataObj = (TwitterGalleryCardDataObject) it.next();
                list.add(new TwitterGalleryCardBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllTwitterGalleryCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        List<String> keys = getDAOFactory().getTwitterGalleryCardDAO().getAllTwitterGalleryCardKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve TwitterGalleryCard key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<TwitterGalleryCard> findTwitterGalleryCards(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findTwitterGalleryCards(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<TwitterGalleryCard> findTwitterGalleryCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("TwitterGalleryCardServiceImpl.findTwitterGalleryCards(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<TwitterGalleryCard> list = new ArrayList<TwitterGalleryCard>();
        List<TwitterGalleryCardDataObject> dataObjs = getDAOFactory().getTwitterGalleryCardDAO().findTwitterGalleryCards(filter, ordering, params, values, grouping, unique, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find twitterGalleryCards for the given criterion.");
        } else {
            Iterator<TwitterGalleryCardDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                TwitterGalleryCardDataObject dataObj = (TwitterGalleryCardDataObject) it.next();
                list.add(new TwitterGalleryCardBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findTwitterGalleryCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("TwitterGalleryCardServiceImpl.findTwitterGalleryCardKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = getDAOFactory().getTwitterGalleryCardDAO().findTwitterGalleryCardKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find TwitterGalleryCard keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("TwitterGalleryCardServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getTwitterGalleryCardDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createTwitterGalleryCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image0, String image1, String image2, String image3) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        TwitterGalleryCardDataObject dataObj = new TwitterGalleryCardDataObject(null, card, url, title, description, site, siteId, creator, creatorId, image0, image1, image2, image3);
        return createTwitterGalleryCard(dataObj);
    }

    @Override
    public String createTwitterGalleryCard(TwitterGalleryCard twitterGalleryCard) throws BaseException
    {
        log.finer("BEGIN");

        // Param twitterGalleryCard cannot be null.....
        if(twitterGalleryCard == null) {
            log.log(Level.INFO, "Param twitterGalleryCard is null!");
            throw new BadRequestException("Param twitterGalleryCard object is null!");
        }
        TwitterGalleryCardDataObject dataObj = null;
        if(twitterGalleryCard instanceof TwitterGalleryCardDataObject) {
            dataObj = (TwitterGalleryCardDataObject) twitterGalleryCard;
        } else if(twitterGalleryCard instanceof TwitterGalleryCardBean) {
            dataObj = ((TwitterGalleryCardBean) twitterGalleryCard).toDataObject();
        } else {  // if(twitterGalleryCard instanceof TwitterGalleryCard)
            //dataObj = new TwitterGalleryCardDataObject(null, twitterGalleryCard.getCard(), twitterGalleryCard.getUrl(), twitterGalleryCard.getTitle(), twitterGalleryCard.getDescription(), twitterGalleryCard.getSite(), twitterGalleryCard.getSiteId(), twitterGalleryCard.getCreator(), twitterGalleryCard.getCreatorId(), twitterGalleryCard.getImage0(), twitterGalleryCard.getImage1(), twitterGalleryCard.getImage2(), twitterGalleryCard.getImage3());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new TwitterGalleryCardDataObject(twitterGalleryCard.getGuid(), twitterGalleryCard.getCard(), twitterGalleryCard.getUrl(), twitterGalleryCard.getTitle(), twitterGalleryCard.getDescription(), twitterGalleryCard.getSite(), twitterGalleryCard.getSiteId(), twitterGalleryCard.getCreator(), twitterGalleryCard.getCreatorId(), twitterGalleryCard.getImage0(), twitterGalleryCard.getImage1(), twitterGalleryCard.getImage2(), twitterGalleryCard.getImage3());
        }
        String guid = getDAOFactory().getTwitterGalleryCardDAO().createTwitterGalleryCard(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateTwitterGalleryCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image0, String image1, String image2, String image3) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        TwitterGalleryCardDataObject dataObj = new TwitterGalleryCardDataObject(guid, card, url, title, description, site, siteId, creator, creatorId, image0, image1, image2, image3);
        return updateTwitterGalleryCard(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateTwitterGalleryCard(TwitterGalleryCard twitterGalleryCard) throws BaseException
    {
        log.finer("BEGIN");

        // Param twitterGalleryCard cannot be null.....
        if(twitterGalleryCard == null || twitterGalleryCard.getGuid() == null) {
            log.log(Level.INFO, "Param twitterGalleryCard or its guid is null!");
            throw new BadRequestException("Param twitterGalleryCard object or its guid is null!");
        }
        TwitterGalleryCardDataObject dataObj = null;
        if(twitterGalleryCard instanceof TwitterGalleryCardDataObject) {
            dataObj = (TwitterGalleryCardDataObject) twitterGalleryCard;
        } else if(twitterGalleryCard instanceof TwitterGalleryCardBean) {
            dataObj = ((TwitterGalleryCardBean) twitterGalleryCard).toDataObject();
        } else {  // if(twitterGalleryCard instanceof TwitterGalleryCard)
            dataObj = new TwitterGalleryCardDataObject(twitterGalleryCard.getGuid(), twitterGalleryCard.getCard(), twitterGalleryCard.getUrl(), twitterGalleryCard.getTitle(), twitterGalleryCard.getDescription(), twitterGalleryCard.getSite(), twitterGalleryCard.getSiteId(), twitterGalleryCard.getCreator(), twitterGalleryCard.getCreatorId(), twitterGalleryCard.getImage0(), twitterGalleryCard.getImage1(), twitterGalleryCard.getImage2(), twitterGalleryCard.getImage3());
        }
        Boolean suc = getDAOFactory().getTwitterGalleryCardDAO().updateTwitterGalleryCard(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteTwitterGalleryCard(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getTwitterGalleryCardDAO().deleteTwitterGalleryCard(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteTwitterGalleryCard(TwitterGalleryCard twitterGalleryCard) throws BaseException
    {
        log.finer("BEGIN");

        // Param twitterGalleryCard cannot be null.....
        if(twitterGalleryCard == null || twitterGalleryCard.getGuid() == null) {
            log.log(Level.INFO, "Param twitterGalleryCard or its guid is null!");
            throw new BadRequestException("Param twitterGalleryCard object or its guid is null!");
        }
        TwitterGalleryCardDataObject dataObj = null;
        if(twitterGalleryCard instanceof TwitterGalleryCardDataObject) {
            dataObj = (TwitterGalleryCardDataObject) twitterGalleryCard;
        } else if(twitterGalleryCard instanceof TwitterGalleryCardBean) {
            dataObj = ((TwitterGalleryCardBean) twitterGalleryCard).toDataObject();
        } else {  // if(twitterGalleryCard instanceof TwitterGalleryCard)
            dataObj = new TwitterGalleryCardDataObject(twitterGalleryCard.getGuid(), twitterGalleryCard.getCard(), twitterGalleryCard.getUrl(), twitterGalleryCard.getTitle(), twitterGalleryCard.getDescription(), twitterGalleryCard.getSite(), twitterGalleryCard.getSiteId(), twitterGalleryCard.getCreator(), twitterGalleryCard.getCreatorId(), twitterGalleryCard.getImage0(), twitterGalleryCard.getImage1(), twitterGalleryCard.getImage2(), twitterGalleryCard.getImage3());
        }
        Boolean suc = getDAOFactory().getTwitterGalleryCardDAO().deleteTwitterGalleryCard(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteTwitterGalleryCards(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getTwitterGalleryCardDAO().deleteTwitterGalleryCards(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
