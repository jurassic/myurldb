package com.myurldb.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.TwitterPhotoCard;
import com.myurldb.ws.bean.TwitterCardAppInfoBean;
import com.myurldb.ws.bean.TwitterCardProductDataBean;
import com.myurldb.ws.bean.TwitterPhotoCardBean;
import com.myurldb.ws.dao.DAOFactory;
import com.myurldb.ws.data.TwitterCardAppInfoDataObject;
import com.myurldb.ws.data.TwitterCardProductDataDataObject;
import com.myurldb.ws.data.TwitterPhotoCardDataObject;
import com.myurldb.ws.service.DAOFactoryManager;
import com.myurldb.ws.service.TwitterPhotoCardService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class TwitterPhotoCardServiceImpl implements TwitterPhotoCardService
{
    private static final Logger log = Logger.getLogger(TwitterPhotoCardServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // TwitterPhotoCard related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public TwitterPhotoCard getTwitterPhotoCard(String guid) throws BaseException
    {
        log.finer("BEGIN");

        TwitterPhotoCardDataObject dataObj = getDAOFactory().getTwitterPhotoCardDAO().getTwitterPhotoCard(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve TwitterPhotoCardDataObject for guid = " + guid);
            return null;  // ????
        }
        TwitterPhotoCardBean bean = new TwitterPhotoCardBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getTwitterPhotoCard(String guid, String field) throws BaseException
    {
        TwitterPhotoCardDataObject dataObj = getDAOFactory().getTwitterPhotoCardDAO().getTwitterPhotoCard(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve TwitterPhotoCardDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("card")) {
            return dataObj.getCard();
        } else if(field.equals("url")) {
            return dataObj.getUrl();
        } else if(field.equals("title")) {
            return dataObj.getTitle();
        } else if(field.equals("description")) {
            return dataObj.getDescription();
        } else if(field.equals("site")) {
            return dataObj.getSite();
        } else if(field.equals("siteId")) {
            return dataObj.getSiteId();
        } else if(field.equals("creator")) {
            return dataObj.getCreator();
        } else if(field.equals("creatorId")) {
            return dataObj.getCreatorId();
        } else if(field.equals("image")) {
            return dataObj.getImage();
        } else if(field.equals("imageWidth")) {
            return dataObj.getImageWidth();
        } else if(field.equals("imageHeight")) {
            return dataObj.getImageHeight();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<TwitterPhotoCard> getTwitterPhotoCards(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<TwitterPhotoCard> list = new ArrayList<TwitterPhotoCard>();
        List<TwitterPhotoCardDataObject> dataObjs = getDAOFactory().getTwitterPhotoCardDAO().getTwitterPhotoCards(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve TwitterPhotoCardDataObject list.");
        } else {
            Iterator<TwitterPhotoCardDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                TwitterPhotoCardDataObject dataObj = (TwitterPhotoCardDataObject) it.next();
                list.add(new TwitterPhotoCardBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<TwitterPhotoCard> getAllTwitterPhotoCards() throws BaseException
    {
        return getAllTwitterPhotoCards(null, null, null);
    }

    @Override
    public List<TwitterPhotoCard> getAllTwitterPhotoCards(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<TwitterPhotoCard> list = new ArrayList<TwitterPhotoCard>();
        List<TwitterPhotoCardDataObject> dataObjs = getDAOFactory().getTwitterPhotoCardDAO().getAllTwitterPhotoCards(ordering, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve TwitterPhotoCardDataObject list.");
        } else {
            Iterator<TwitterPhotoCardDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                TwitterPhotoCardDataObject dataObj = (TwitterPhotoCardDataObject) it.next();
                list.add(new TwitterPhotoCardBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllTwitterPhotoCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        List<String> keys = getDAOFactory().getTwitterPhotoCardDAO().getAllTwitterPhotoCardKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve TwitterPhotoCard key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<TwitterPhotoCard> findTwitterPhotoCards(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findTwitterPhotoCards(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<TwitterPhotoCard> findTwitterPhotoCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("TwitterPhotoCardServiceImpl.findTwitterPhotoCards(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<TwitterPhotoCard> list = new ArrayList<TwitterPhotoCard>();
        List<TwitterPhotoCardDataObject> dataObjs = getDAOFactory().getTwitterPhotoCardDAO().findTwitterPhotoCards(filter, ordering, params, values, grouping, unique, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find twitterPhotoCards for the given criterion.");
        } else {
            Iterator<TwitterPhotoCardDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                TwitterPhotoCardDataObject dataObj = (TwitterPhotoCardDataObject) it.next();
                list.add(new TwitterPhotoCardBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findTwitterPhotoCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("TwitterPhotoCardServiceImpl.findTwitterPhotoCardKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = getDAOFactory().getTwitterPhotoCardDAO().findTwitterPhotoCardKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find TwitterPhotoCard keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("TwitterPhotoCardServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getTwitterPhotoCardDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createTwitterPhotoCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        TwitterPhotoCardDataObject dataObj = new TwitterPhotoCardDataObject(null, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight);
        return createTwitterPhotoCard(dataObj);
    }

    @Override
    public String createTwitterPhotoCard(TwitterPhotoCard twitterPhotoCard) throws BaseException
    {
        log.finer("BEGIN");

        // Param twitterPhotoCard cannot be null.....
        if(twitterPhotoCard == null) {
            log.log(Level.INFO, "Param twitterPhotoCard is null!");
            throw new BadRequestException("Param twitterPhotoCard object is null!");
        }
        TwitterPhotoCardDataObject dataObj = null;
        if(twitterPhotoCard instanceof TwitterPhotoCardDataObject) {
            dataObj = (TwitterPhotoCardDataObject) twitterPhotoCard;
        } else if(twitterPhotoCard instanceof TwitterPhotoCardBean) {
            dataObj = ((TwitterPhotoCardBean) twitterPhotoCard).toDataObject();
        } else {  // if(twitterPhotoCard instanceof TwitterPhotoCard)
            //dataObj = new TwitterPhotoCardDataObject(null, twitterPhotoCard.getCard(), twitterPhotoCard.getUrl(), twitterPhotoCard.getTitle(), twitterPhotoCard.getDescription(), twitterPhotoCard.getSite(), twitterPhotoCard.getSiteId(), twitterPhotoCard.getCreator(), twitterPhotoCard.getCreatorId(), twitterPhotoCard.getImage(), twitterPhotoCard.getImageWidth(), twitterPhotoCard.getImageHeight());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new TwitterPhotoCardDataObject(twitterPhotoCard.getGuid(), twitterPhotoCard.getCard(), twitterPhotoCard.getUrl(), twitterPhotoCard.getTitle(), twitterPhotoCard.getDescription(), twitterPhotoCard.getSite(), twitterPhotoCard.getSiteId(), twitterPhotoCard.getCreator(), twitterPhotoCard.getCreatorId(), twitterPhotoCard.getImage(), twitterPhotoCard.getImageWidth(), twitterPhotoCard.getImageHeight());
        }
        String guid = getDAOFactory().getTwitterPhotoCardDAO().createTwitterPhotoCard(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateTwitterPhotoCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        TwitterPhotoCardDataObject dataObj = new TwitterPhotoCardDataObject(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight);
        return updateTwitterPhotoCard(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateTwitterPhotoCard(TwitterPhotoCard twitterPhotoCard) throws BaseException
    {
        log.finer("BEGIN");

        // Param twitterPhotoCard cannot be null.....
        if(twitterPhotoCard == null || twitterPhotoCard.getGuid() == null) {
            log.log(Level.INFO, "Param twitterPhotoCard or its guid is null!");
            throw new BadRequestException("Param twitterPhotoCard object or its guid is null!");
        }
        TwitterPhotoCardDataObject dataObj = null;
        if(twitterPhotoCard instanceof TwitterPhotoCardDataObject) {
            dataObj = (TwitterPhotoCardDataObject) twitterPhotoCard;
        } else if(twitterPhotoCard instanceof TwitterPhotoCardBean) {
            dataObj = ((TwitterPhotoCardBean) twitterPhotoCard).toDataObject();
        } else {  // if(twitterPhotoCard instanceof TwitterPhotoCard)
            dataObj = new TwitterPhotoCardDataObject(twitterPhotoCard.getGuid(), twitterPhotoCard.getCard(), twitterPhotoCard.getUrl(), twitterPhotoCard.getTitle(), twitterPhotoCard.getDescription(), twitterPhotoCard.getSite(), twitterPhotoCard.getSiteId(), twitterPhotoCard.getCreator(), twitterPhotoCard.getCreatorId(), twitterPhotoCard.getImage(), twitterPhotoCard.getImageWidth(), twitterPhotoCard.getImageHeight());
        }
        Boolean suc = getDAOFactory().getTwitterPhotoCardDAO().updateTwitterPhotoCard(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteTwitterPhotoCard(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getTwitterPhotoCardDAO().deleteTwitterPhotoCard(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteTwitterPhotoCard(TwitterPhotoCard twitterPhotoCard) throws BaseException
    {
        log.finer("BEGIN");

        // Param twitterPhotoCard cannot be null.....
        if(twitterPhotoCard == null || twitterPhotoCard.getGuid() == null) {
            log.log(Level.INFO, "Param twitterPhotoCard or its guid is null!");
            throw new BadRequestException("Param twitterPhotoCard object or its guid is null!");
        }
        TwitterPhotoCardDataObject dataObj = null;
        if(twitterPhotoCard instanceof TwitterPhotoCardDataObject) {
            dataObj = (TwitterPhotoCardDataObject) twitterPhotoCard;
        } else if(twitterPhotoCard instanceof TwitterPhotoCardBean) {
            dataObj = ((TwitterPhotoCardBean) twitterPhotoCard).toDataObject();
        } else {  // if(twitterPhotoCard instanceof TwitterPhotoCard)
            dataObj = new TwitterPhotoCardDataObject(twitterPhotoCard.getGuid(), twitterPhotoCard.getCard(), twitterPhotoCard.getUrl(), twitterPhotoCard.getTitle(), twitterPhotoCard.getDescription(), twitterPhotoCard.getSite(), twitterPhotoCard.getSiteId(), twitterPhotoCard.getCreator(), twitterPhotoCard.getCreatorId(), twitterPhotoCard.getImage(), twitterPhotoCard.getImageWidth(), twitterPhotoCard.getImageHeight());
        }
        Boolean suc = getDAOFactory().getTwitterPhotoCardDAO().deleteTwitterPhotoCard(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteTwitterPhotoCards(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getTwitterPhotoCardDAO().deleteTwitterPhotoCards(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
