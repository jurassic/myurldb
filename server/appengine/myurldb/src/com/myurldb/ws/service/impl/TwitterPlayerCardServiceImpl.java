package com.myurldb.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.TwitterPlayerCard;
import com.myurldb.ws.bean.TwitterCardAppInfoBean;
import com.myurldb.ws.bean.TwitterCardProductDataBean;
import com.myurldb.ws.bean.TwitterPlayerCardBean;
import com.myurldb.ws.dao.DAOFactory;
import com.myurldb.ws.data.TwitterCardAppInfoDataObject;
import com.myurldb.ws.data.TwitterCardProductDataDataObject;
import com.myurldb.ws.data.TwitterPlayerCardDataObject;
import com.myurldb.ws.service.DAOFactoryManager;
import com.myurldb.ws.service.TwitterPlayerCardService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class TwitterPlayerCardServiceImpl implements TwitterPlayerCardService
{
    private static final Logger log = Logger.getLogger(TwitterPlayerCardServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // TwitterPlayerCard related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public TwitterPlayerCard getTwitterPlayerCard(String guid) throws BaseException
    {
        log.finer("BEGIN");

        TwitterPlayerCardDataObject dataObj = getDAOFactory().getTwitterPlayerCardDAO().getTwitterPlayerCard(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve TwitterPlayerCardDataObject for guid = " + guid);
            return null;  // ????
        }
        TwitterPlayerCardBean bean = new TwitterPlayerCardBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getTwitterPlayerCard(String guid, String field) throws BaseException
    {
        TwitterPlayerCardDataObject dataObj = getDAOFactory().getTwitterPlayerCardDAO().getTwitterPlayerCard(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve TwitterPlayerCardDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("card")) {
            return dataObj.getCard();
        } else if(field.equals("url")) {
            return dataObj.getUrl();
        } else if(field.equals("title")) {
            return dataObj.getTitle();
        } else if(field.equals("description")) {
            return dataObj.getDescription();
        } else if(field.equals("site")) {
            return dataObj.getSite();
        } else if(field.equals("siteId")) {
            return dataObj.getSiteId();
        } else if(field.equals("creator")) {
            return dataObj.getCreator();
        } else if(field.equals("creatorId")) {
            return dataObj.getCreatorId();
        } else if(field.equals("image")) {
            return dataObj.getImage();
        } else if(field.equals("imageWidth")) {
            return dataObj.getImageWidth();
        } else if(field.equals("imageHeight")) {
            return dataObj.getImageHeight();
        } else if(field.equals("player")) {
            return dataObj.getPlayer();
        } else if(field.equals("playerWidth")) {
            return dataObj.getPlayerWidth();
        } else if(field.equals("playerHeight")) {
            return dataObj.getPlayerHeight();
        } else if(field.equals("playerStream")) {
            return dataObj.getPlayerStream();
        } else if(field.equals("playerStreamContentType")) {
            return dataObj.getPlayerStreamContentType();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<TwitterPlayerCard> getTwitterPlayerCards(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<TwitterPlayerCard> list = new ArrayList<TwitterPlayerCard>();
        List<TwitterPlayerCardDataObject> dataObjs = getDAOFactory().getTwitterPlayerCardDAO().getTwitterPlayerCards(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve TwitterPlayerCardDataObject list.");
        } else {
            Iterator<TwitterPlayerCardDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                TwitterPlayerCardDataObject dataObj = (TwitterPlayerCardDataObject) it.next();
                list.add(new TwitterPlayerCardBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<TwitterPlayerCard> getAllTwitterPlayerCards() throws BaseException
    {
        return getAllTwitterPlayerCards(null, null, null);
    }

    @Override
    public List<TwitterPlayerCard> getAllTwitterPlayerCards(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<TwitterPlayerCard> list = new ArrayList<TwitterPlayerCard>();
        List<TwitterPlayerCardDataObject> dataObjs = getDAOFactory().getTwitterPlayerCardDAO().getAllTwitterPlayerCards(ordering, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve TwitterPlayerCardDataObject list.");
        } else {
            Iterator<TwitterPlayerCardDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                TwitterPlayerCardDataObject dataObj = (TwitterPlayerCardDataObject) it.next();
                list.add(new TwitterPlayerCardBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllTwitterPlayerCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        List<String> keys = getDAOFactory().getTwitterPlayerCardDAO().getAllTwitterPlayerCardKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve TwitterPlayerCard key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<TwitterPlayerCard> findTwitterPlayerCards(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findTwitterPlayerCards(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<TwitterPlayerCard> findTwitterPlayerCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("TwitterPlayerCardServiceImpl.findTwitterPlayerCards(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<TwitterPlayerCard> list = new ArrayList<TwitterPlayerCard>();
        List<TwitterPlayerCardDataObject> dataObjs = getDAOFactory().getTwitterPlayerCardDAO().findTwitterPlayerCards(filter, ordering, params, values, grouping, unique, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find twitterPlayerCards for the given criterion.");
        } else {
            Iterator<TwitterPlayerCardDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                TwitterPlayerCardDataObject dataObj = (TwitterPlayerCardDataObject) it.next();
                list.add(new TwitterPlayerCardBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findTwitterPlayerCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("TwitterPlayerCardServiceImpl.findTwitterPlayerCardKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = getDAOFactory().getTwitterPlayerCardDAO().findTwitterPlayerCardKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find TwitterPlayerCard keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("TwitterPlayerCardServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getTwitterPlayerCardDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createTwitterPlayerCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, String player, Integer playerWidth, Integer playerHeight, String playerStream, String playerStreamContentType) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        TwitterPlayerCardDataObject dataObj = new TwitterPlayerCardDataObject(null, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, player, playerWidth, playerHeight, playerStream, playerStreamContentType);
        return createTwitterPlayerCard(dataObj);
    }

    @Override
    public String createTwitterPlayerCard(TwitterPlayerCard twitterPlayerCard) throws BaseException
    {
        log.finer("BEGIN");

        // Param twitterPlayerCard cannot be null.....
        if(twitterPlayerCard == null) {
            log.log(Level.INFO, "Param twitterPlayerCard is null!");
            throw new BadRequestException("Param twitterPlayerCard object is null!");
        }
        TwitterPlayerCardDataObject dataObj = null;
        if(twitterPlayerCard instanceof TwitterPlayerCardDataObject) {
            dataObj = (TwitterPlayerCardDataObject) twitterPlayerCard;
        } else if(twitterPlayerCard instanceof TwitterPlayerCardBean) {
            dataObj = ((TwitterPlayerCardBean) twitterPlayerCard).toDataObject();
        } else {  // if(twitterPlayerCard instanceof TwitterPlayerCard)
            //dataObj = new TwitterPlayerCardDataObject(null, twitterPlayerCard.getCard(), twitterPlayerCard.getUrl(), twitterPlayerCard.getTitle(), twitterPlayerCard.getDescription(), twitterPlayerCard.getSite(), twitterPlayerCard.getSiteId(), twitterPlayerCard.getCreator(), twitterPlayerCard.getCreatorId(), twitterPlayerCard.getImage(), twitterPlayerCard.getImageWidth(), twitterPlayerCard.getImageHeight(), twitterPlayerCard.getPlayer(), twitterPlayerCard.getPlayerWidth(), twitterPlayerCard.getPlayerHeight(), twitterPlayerCard.getPlayerStream(), twitterPlayerCard.getPlayerStreamContentType());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new TwitterPlayerCardDataObject(twitterPlayerCard.getGuid(), twitterPlayerCard.getCard(), twitterPlayerCard.getUrl(), twitterPlayerCard.getTitle(), twitterPlayerCard.getDescription(), twitterPlayerCard.getSite(), twitterPlayerCard.getSiteId(), twitterPlayerCard.getCreator(), twitterPlayerCard.getCreatorId(), twitterPlayerCard.getImage(), twitterPlayerCard.getImageWidth(), twitterPlayerCard.getImageHeight(), twitterPlayerCard.getPlayer(), twitterPlayerCard.getPlayerWidth(), twitterPlayerCard.getPlayerHeight(), twitterPlayerCard.getPlayerStream(), twitterPlayerCard.getPlayerStreamContentType());
        }
        String guid = getDAOFactory().getTwitterPlayerCardDAO().createTwitterPlayerCard(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateTwitterPlayerCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, String player, Integer playerWidth, Integer playerHeight, String playerStream, String playerStreamContentType) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        TwitterPlayerCardDataObject dataObj = new TwitterPlayerCardDataObject(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, player, playerWidth, playerHeight, playerStream, playerStreamContentType);
        return updateTwitterPlayerCard(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateTwitterPlayerCard(TwitterPlayerCard twitterPlayerCard) throws BaseException
    {
        log.finer("BEGIN");

        // Param twitterPlayerCard cannot be null.....
        if(twitterPlayerCard == null || twitterPlayerCard.getGuid() == null) {
            log.log(Level.INFO, "Param twitterPlayerCard or its guid is null!");
            throw new BadRequestException("Param twitterPlayerCard object or its guid is null!");
        }
        TwitterPlayerCardDataObject dataObj = null;
        if(twitterPlayerCard instanceof TwitterPlayerCardDataObject) {
            dataObj = (TwitterPlayerCardDataObject) twitterPlayerCard;
        } else if(twitterPlayerCard instanceof TwitterPlayerCardBean) {
            dataObj = ((TwitterPlayerCardBean) twitterPlayerCard).toDataObject();
        } else {  // if(twitterPlayerCard instanceof TwitterPlayerCard)
            dataObj = new TwitterPlayerCardDataObject(twitterPlayerCard.getGuid(), twitterPlayerCard.getCard(), twitterPlayerCard.getUrl(), twitterPlayerCard.getTitle(), twitterPlayerCard.getDescription(), twitterPlayerCard.getSite(), twitterPlayerCard.getSiteId(), twitterPlayerCard.getCreator(), twitterPlayerCard.getCreatorId(), twitterPlayerCard.getImage(), twitterPlayerCard.getImageWidth(), twitterPlayerCard.getImageHeight(), twitterPlayerCard.getPlayer(), twitterPlayerCard.getPlayerWidth(), twitterPlayerCard.getPlayerHeight(), twitterPlayerCard.getPlayerStream(), twitterPlayerCard.getPlayerStreamContentType());
        }
        Boolean suc = getDAOFactory().getTwitterPlayerCardDAO().updateTwitterPlayerCard(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteTwitterPlayerCard(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getTwitterPlayerCardDAO().deleteTwitterPlayerCard(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteTwitterPlayerCard(TwitterPlayerCard twitterPlayerCard) throws BaseException
    {
        log.finer("BEGIN");

        // Param twitterPlayerCard cannot be null.....
        if(twitterPlayerCard == null || twitterPlayerCard.getGuid() == null) {
            log.log(Level.INFO, "Param twitterPlayerCard or its guid is null!");
            throw new BadRequestException("Param twitterPlayerCard object or its guid is null!");
        }
        TwitterPlayerCardDataObject dataObj = null;
        if(twitterPlayerCard instanceof TwitterPlayerCardDataObject) {
            dataObj = (TwitterPlayerCardDataObject) twitterPlayerCard;
        } else if(twitterPlayerCard instanceof TwitterPlayerCardBean) {
            dataObj = ((TwitterPlayerCardBean) twitterPlayerCard).toDataObject();
        } else {  // if(twitterPlayerCard instanceof TwitterPlayerCard)
            dataObj = new TwitterPlayerCardDataObject(twitterPlayerCard.getGuid(), twitterPlayerCard.getCard(), twitterPlayerCard.getUrl(), twitterPlayerCard.getTitle(), twitterPlayerCard.getDescription(), twitterPlayerCard.getSite(), twitterPlayerCard.getSiteId(), twitterPlayerCard.getCreator(), twitterPlayerCard.getCreatorId(), twitterPlayerCard.getImage(), twitterPlayerCard.getImageWidth(), twitterPlayerCard.getImageHeight(), twitterPlayerCard.getPlayer(), twitterPlayerCard.getPlayerWidth(), twitterPlayerCard.getPlayerHeight(), twitterPlayerCard.getPlayerStream(), twitterPlayerCard.getPlayerStreamContentType());
        }
        Boolean suc = getDAOFactory().getTwitterPlayerCardDAO().deleteTwitterPlayerCard(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteTwitterPlayerCards(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getTwitterPlayerCardDAO().deleteTwitterPlayerCards(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
