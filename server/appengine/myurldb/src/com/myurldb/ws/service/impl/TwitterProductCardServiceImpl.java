package com.myurldb.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.TwitterProductCard;
import com.myurldb.ws.bean.TwitterCardAppInfoBean;
import com.myurldb.ws.bean.TwitterCardProductDataBean;
import com.myurldb.ws.bean.TwitterProductCardBean;
import com.myurldb.ws.dao.DAOFactory;
import com.myurldb.ws.data.TwitterCardAppInfoDataObject;
import com.myurldb.ws.data.TwitterCardProductDataDataObject;
import com.myurldb.ws.data.TwitterProductCardDataObject;
import com.myurldb.ws.service.DAOFactoryManager;
import com.myurldb.ws.service.TwitterProductCardService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class TwitterProductCardServiceImpl implements TwitterProductCardService
{
    private static final Logger log = Logger.getLogger(TwitterProductCardServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // TwitterProductCard related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public TwitterProductCard getTwitterProductCard(String guid) throws BaseException
    {
        log.finer("BEGIN");

        TwitterProductCardDataObject dataObj = getDAOFactory().getTwitterProductCardDAO().getTwitterProductCard(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve TwitterProductCardDataObject for guid = " + guid);
            return null;  // ????
        }
        TwitterProductCardBean bean = new TwitterProductCardBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getTwitterProductCard(String guid, String field) throws BaseException
    {
        TwitterProductCardDataObject dataObj = getDAOFactory().getTwitterProductCardDAO().getTwitterProductCard(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve TwitterProductCardDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("card")) {
            return dataObj.getCard();
        } else if(field.equals("url")) {
            return dataObj.getUrl();
        } else if(field.equals("title")) {
            return dataObj.getTitle();
        } else if(field.equals("description")) {
            return dataObj.getDescription();
        } else if(field.equals("site")) {
            return dataObj.getSite();
        } else if(field.equals("siteId")) {
            return dataObj.getSiteId();
        } else if(field.equals("creator")) {
            return dataObj.getCreator();
        } else if(field.equals("creatorId")) {
            return dataObj.getCreatorId();
        } else if(field.equals("image")) {
            return dataObj.getImage();
        } else if(field.equals("imageWidth")) {
            return dataObj.getImageWidth();
        } else if(field.equals("imageHeight")) {
            return dataObj.getImageHeight();
        } else if(field.equals("data1")) {
            return dataObj.getData1();
        } else if(field.equals("data2")) {
            return dataObj.getData2();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<TwitterProductCard> getTwitterProductCards(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<TwitterProductCard> list = new ArrayList<TwitterProductCard>();
        List<TwitterProductCardDataObject> dataObjs = getDAOFactory().getTwitterProductCardDAO().getTwitterProductCards(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve TwitterProductCardDataObject list.");
        } else {
            Iterator<TwitterProductCardDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                TwitterProductCardDataObject dataObj = (TwitterProductCardDataObject) it.next();
                list.add(new TwitterProductCardBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<TwitterProductCard> getAllTwitterProductCards() throws BaseException
    {
        return getAllTwitterProductCards(null, null, null);
    }

    @Override
    public List<TwitterProductCard> getAllTwitterProductCards(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<TwitterProductCard> list = new ArrayList<TwitterProductCard>();
        List<TwitterProductCardDataObject> dataObjs = getDAOFactory().getTwitterProductCardDAO().getAllTwitterProductCards(ordering, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve TwitterProductCardDataObject list.");
        } else {
            Iterator<TwitterProductCardDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                TwitterProductCardDataObject dataObj = (TwitterProductCardDataObject) it.next();
                list.add(new TwitterProductCardBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllTwitterProductCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        List<String> keys = getDAOFactory().getTwitterProductCardDAO().getAllTwitterProductCardKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve TwitterProductCard key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<TwitterProductCard> findTwitterProductCards(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findTwitterProductCards(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<TwitterProductCard> findTwitterProductCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("TwitterProductCardServiceImpl.findTwitterProductCards(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<TwitterProductCard> list = new ArrayList<TwitterProductCard>();
        List<TwitterProductCardDataObject> dataObjs = getDAOFactory().getTwitterProductCardDAO().findTwitterProductCards(filter, ordering, params, values, grouping, unique, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find twitterProductCards for the given criterion.");
        } else {
            Iterator<TwitterProductCardDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                TwitterProductCardDataObject dataObj = (TwitterProductCardDataObject) it.next();
                list.add(new TwitterProductCardBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findTwitterProductCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("TwitterProductCardServiceImpl.findTwitterProductCardKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = getDAOFactory().getTwitterProductCardDAO().findTwitterProductCardKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find TwitterProductCard keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("TwitterProductCardServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getTwitterProductCardDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createTwitterProductCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, TwitterCardProductData data1, TwitterCardProductData data2) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        TwitterCardProductDataDataObject data1Dobj = null;
        if(data1 instanceof TwitterCardProductDataBean) {
            data1Dobj = ((TwitterCardProductDataBean) data1).toDataObject();
        } else if(data1 instanceof TwitterCardProductData) {
            data1Dobj = new TwitterCardProductDataDataObject(data1.getData(), data1.getLabel());
        } else {
            data1Dobj = null;   // ????
        }
        TwitterCardProductDataDataObject data2Dobj = null;
        if(data2 instanceof TwitterCardProductDataBean) {
            data2Dobj = ((TwitterCardProductDataBean) data2).toDataObject();
        } else if(data2 instanceof TwitterCardProductData) {
            data2Dobj = new TwitterCardProductDataDataObject(data2.getData(), data2.getLabel());
        } else {
            data2Dobj = null;   // ????
        }
        
        TwitterProductCardDataObject dataObj = new TwitterProductCardDataObject(null, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, data1Dobj, data2Dobj);
        return createTwitterProductCard(dataObj);
    }

    @Override
    public String createTwitterProductCard(TwitterProductCard twitterProductCard) throws BaseException
    {
        log.finer("BEGIN");

        // Param twitterProductCard cannot be null.....
        if(twitterProductCard == null) {
            log.log(Level.INFO, "Param twitterProductCard is null!");
            throw new BadRequestException("Param twitterProductCard object is null!");
        }
        TwitterProductCardDataObject dataObj = null;
        if(twitterProductCard instanceof TwitterProductCardDataObject) {
            dataObj = (TwitterProductCardDataObject) twitterProductCard;
        } else if(twitterProductCard instanceof TwitterProductCardBean) {
            dataObj = ((TwitterProductCardBean) twitterProductCard).toDataObject();
        } else {  // if(twitterProductCard instanceof TwitterProductCard)
            //dataObj = new TwitterProductCardDataObject(null, twitterProductCard.getCard(), twitterProductCard.getUrl(), twitterProductCard.getTitle(), twitterProductCard.getDescription(), twitterProductCard.getSite(), twitterProductCard.getSiteId(), twitterProductCard.getCreator(), twitterProductCard.getCreatorId(), twitterProductCard.getImage(), twitterProductCard.getImageWidth(), twitterProductCard.getImageHeight(), (TwitterCardProductDataDataObject) twitterProductCard.getData1(), (TwitterCardProductDataDataObject) twitterProductCard.getData2());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new TwitterProductCardDataObject(twitterProductCard.getGuid(), twitterProductCard.getCard(), twitterProductCard.getUrl(), twitterProductCard.getTitle(), twitterProductCard.getDescription(), twitterProductCard.getSite(), twitterProductCard.getSiteId(), twitterProductCard.getCreator(), twitterProductCard.getCreatorId(), twitterProductCard.getImage(), twitterProductCard.getImageWidth(), twitterProductCard.getImageHeight(), (TwitterCardProductDataDataObject) twitterProductCard.getData1(), (TwitterCardProductDataDataObject) twitterProductCard.getData2());
        }
        String guid = getDAOFactory().getTwitterProductCardDAO().createTwitterProductCard(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateTwitterProductCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, TwitterCardProductData data1, TwitterCardProductData data2) throws BaseException
    {
        TwitterCardProductDataDataObject data1Dobj = null;
        if(data1 instanceof TwitterCardProductDataBean) {
            data1Dobj = ((TwitterCardProductDataBean) data1).toDataObject();            
        } else if(data1 instanceof TwitterCardProductData) {
            data1Dobj = new TwitterCardProductDataDataObject(data1.getData(), data1.getLabel());
        } else {
            data1Dobj = null;   // ????
        }
        TwitterCardProductDataDataObject data2Dobj = null;
        if(data2 instanceof TwitterCardProductDataBean) {
            data2Dobj = ((TwitterCardProductDataBean) data2).toDataObject();            
        } else if(data2 instanceof TwitterCardProductData) {
            data2Dobj = new TwitterCardProductDataDataObject(data2.getData(), data2.getLabel());
        } else {
            data2Dobj = null;   // ????
        }

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        TwitterProductCardDataObject dataObj = new TwitterProductCardDataObject(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, data1Dobj, data2Dobj);
        return updateTwitterProductCard(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateTwitterProductCard(TwitterProductCard twitterProductCard) throws BaseException
    {
        log.finer("BEGIN");

        // Param twitterProductCard cannot be null.....
        if(twitterProductCard == null || twitterProductCard.getGuid() == null) {
            log.log(Level.INFO, "Param twitterProductCard or its guid is null!");
            throw new BadRequestException("Param twitterProductCard object or its guid is null!");
        }
        TwitterProductCardDataObject dataObj = null;
        if(twitterProductCard instanceof TwitterProductCardDataObject) {
            dataObj = (TwitterProductCardDataObject) twitterProductCard;
        } else if(twitterProductCard instanceof TwitterProductCardBean) {
            dataObj = ((TwitterProductCardBean) twitterProductCard).toDataObject();
        } else {  // if(twitterProductCard instanceof TwitterProductCard)
            dataObj = new TwitterProductCardDataObject(twitterProductCard.getGuid(), twitterProductCard.getCard(), twitterProductCard.getUrl(), twitterProductCard.getTitle(), twitterProductCard.getDescription(), twitterProductCard.getSite(), twitterProductCard.getSiteId(), twitterProductCard.getCreator(), twitterProductCard.getCreatorId(), twitterProductCard.getImage(), twitterProductCard.getImageWidth(), twitterProductCard.getImageHeight(), twitterProductCard.getData1(), twitterProductCard.getData2());
        }
        Boolean suc = getDAOFactory().getTwitterProductCardDAO().updateTwitterProductCard(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteTwitterProductCard(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getTwitterProductCardDAO().deleteTwitterProductCard(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteTwitterProductCard(TwitterProductCard twitterProductCard) throws BaseException
    {
        log.finer("BEGIN");

        // Param twitterProductCard cannot be null.....
        if(twitterProductCard == null || twitterProductCard.getGuid() == null) {
            log.log(Level.INFO, "Param twitterProductCard or its guid is null!");
            throw new BadRequestException("Param twitterProductCard object or its guid is null!");
        }
        TwitterProductCardDataObject dataObj = null;
        if(twitterProductCard instanceof TwitterProductCardDataObject) {
            dataObj = (TwitterProductCardDataObject) twitterProductCard;
        } else if(twitterProductCard instanceof TwitterProductCardBean) {
            dataObj = ((TwitterProductCardBean) twitterProductCard).toDataObject();
        } else {  // if(twitterProductCard instanceof TwitterProductCard)
            dataObj = new TwitterProductCardDataObject(twitterProductCard.getGuid(), twitterProductCard.getCard(), twitterProductCard.getUrl(), twitterProductCard.getTitle(), twitterProductCard.getDescription(), twitterProductCard.getSite(), twitterProductCard.getSiteId(), twitterProductCard.getCreator(), twitterProductCard.getCreatorId(), twitterProductCard.getImage(), twitterProductCard.getImageWidth(), twitterProductCard.getImageHeight(), twitterProductCard.getData1(), twitterProductCard.getData2());
        }
        Boolean suc = getDAOFactory().getTwitterProductCardDAO().deleteTwitterProductCard(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteTwitterProductCards(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getTwitterProductCardDAO().deleteTwitterProductCards(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
