package com.myurldb.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.TwitterSummaryCard;
import com.myurldb.ws.bean.TwitterCardAppInfoBean;
import com.myurldb.ws.bean.TwitterCardProductDataBean;
import com.myurldb.ws.bean.TwitterSummaryCardBean;
import com.myurldb.ws.dao.DAOFactory;
import com.myurldb.ws.data.TwitterCardAppInfoDataObject;
import com.myurldb.ws.data.TwitterCardProductDataDataObject;
import com.myurldb.ws.data.TwitterSummaryCardDataObject;
import com.myurldb.ws.service.DAOFactoryManager;
import com.myurldb.ws.service.TwitterSummaryCardService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class TwitterSummaryCardServiceImpl implements TwitterSummaryCardService
{
    private static final Logger log = Logger.getLogger(TwitterSummaryCardServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // TwitterSummaryCard related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public TwitterSummaryCard getTwitterSummaryCard(String guid) throws BaseException
    {
        log.finer("BEGIN");

        TwitterSummaryCardDataObject dataObj = getDAOFactory().getTwitterSummaryCardDAO().getTwitterSummaryCard(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve TwitterSummaryCardDataObject for guid = " + guid);
            return null;  // ????
        }
        TwitterSummaryCardBean bean = new TwitterSummaryCardBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getTwitterSummaryCard(String guid, String field) throws BaseException
    {
        TwitterSummaryCardDataObject dataObj = getDAOFactory().getTwitterSummaryCardDAO().getTwitterSummaryCard(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve TwitterSummaryCardDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("card")) {
            return dataObj.getCard();
        } else if(field.equals("url")) {
            return dataObj.getUrl();
        } else if(field.equals("title")) {
            return dataObj.getTitle();
        } else if(field.equals("description")) {
            return dataObj.getDescription();
        } else if(field.equals("site")) {
            return dataObj.getSite();
        } else if(field.equals("siteId")) {
            return dataObj.getSiteId();
        } else if(field.equals("creator")) {
            return dataObj.getCreator();
        } else if(field.equals("creatorId")) {
            return dataObj.getCreatorId();
        } else if(field.equals("image")) {
            return dataObj.getImage();
        } else if(field.equals("imageWidth")) {
            return dataObj.getImageWidth();
        } else if(field.equals("imageHeight")) {
            return dataObj.getImageHeight();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<TwitterSummaryCard> getTwitterSummaryCards(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<TwitterSummaryCard> list = new ArrayList<TwitterSummaryCard>();
        List<TwitterSummaryCardDataObject> dataObjs = getDAOFactory().getTwitterSummaryCardDAO().getTwitterSummaryCards(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve TwitterSummaryCardDataObject list.");
        } else {
            Iterator<TwitterSummaryCardDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                TwitterSummaryCardDataObject dataObj = (TwitterSummaryCardDataObject) it.next();
                list.add(new TwitterSummaryCardBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<TwitterSummaryCard> getAllTwitterSummaryCards() throws BaseException
    {
        return getAllTwitterSummaryCards(null, null, null);
    }

    @Override
    public List<TwitterSummaryCard> getAllTwitterSummaryCards(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<TwitterSummaryCard> list = new ArrayList<TwitterSummaryCard>();
        List<TwitterSummaryCardDataObject> dataObjs = getDAOFactory().getTwitterSummaryCardDAO().getAllTwitterSummaryCards(ordering, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve TwitterSummaryCardDataObject list.");
        } else {
            Iterator<TwitterSummaryCardDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                TwitterSummaryCardDataObject dataObj = (TwitterSummaryCardDataObject) it.next();
                list.add(new TwitterSummaryCardBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllTwitterSummaryCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        List<String> keys = getDAOFactory().getTwitterSummaryCardDAO().getAllTwitterSummaryCardKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve TwitterSummaryCard key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<TwitterSummaryCard> findTwitterSummaryCards(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findTwitterSummaryCards(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<TwitterSummaryCard> findTwitterSummaryCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("TwitterSummaryCardServiceImpl.findTwitterSummaryCards(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<TwitterSummaryCard> list = new ArrayList<TwitterSummaryCard>();
        List<TwitterSummaryCardDataObject> dataObjs = getDAOFactory().getTwitterSummaryCardDAO().findTwitterSummaryCards(filter, ordering, params, values, grouping, unique, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find twitterSummaryCards for the given criterion.");
        } else {
            Iterator<TwitterSummaryCardDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                TwitterSummaryCardDataObject dataObj = (TwitterSummaryCardDataObject) it.next();
                list.add(new TwitterSummaryCardBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findTwitterSummaryCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("TwitterSummaryCardServiceImpl.findTwitterSummaryCardKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = getDAOFactory().getTwitterSummaryCardDAO().findTwitterSummaryCardKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find TwitterSummaryCard keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("TwitterSummaryCardServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getTwitterSummaryCardDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createTwitterSummaryCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        TwitterSummaryCardDataObject dataObj = new TwitterSummaryCardDataObject(null, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight);
        return createTwitterSummaryCard(dataObj);
    }

    @Override
    public String createTwitterSummaryCard(TwitterSummaryCard twitterSummaryCard) throws BaseException
    {
        log.finer("BEGIN");

        // Param twitterSummaryCard cannot be null.....
        if(twitterSummaryCard == null) {
            log.log(Level.INFO, "Param twitterSummaryCard is null!");
            throw new BadRequestException("Param twitterSummaryCard object is null!");
        }
        TwitterSummaryCardDataObject dataObj = null;
        if(twitterSummaryCard instanceof TwitterSummaryCardDataObject) {
            dataObj = (TwitterSummaryCardDataObject) twitterSummaryCard;
        } else if(twitterSummaryCard instanceof TwitterSummaryCardBean) {
            dataObj = ((TwitterSummaryCardBean) twitterSummaryCard).toDataObject();
        } else {  // if(twitterSummaryCard instanceof TwitterSummaryCard)
            //dataObj = new TwitterSummaryCardDataObject(null, twitterSummaryCard.getCard(), twitterSummaryCard.getUrl(), twitterSummaryCard.getTitle(), twitterSummaryCard.getDescription(), twitterSummaryCard.getSite(), twitterSummaryCard.getSiteId(), twitterSummaryCard.getCreator(), twitterSummaryCard.getCreatorId(), twitterSummaryCard.getImage(), twitterSummaryCard.getImageWidth(), twitterSummaryCard.getImageHeight());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new TwitterSummaryCardDataObject(twitterSummaryCard.getGuid(), twitterSummaryCard.getCard(), twitterSummaryCard.getUrl(), twitterSummaryCard.getTitle(), twitterSummaryCard.getDescription(), twitterSummaryCard.getSite(), twitterSummaryCard.getSiteId(), twitterSummaryCard.getCreator(), twitterSummaryCard.getCreatorId(), twitterSummaryCard.getImage(), twitterSummaryCard.getImageWidth(), twitterSummaryCard.getImageHeight());
        }
        String guid = getDAOFactory().getTwitterSummaryCardDAO().createTwitterSummaryCard(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateTwitterSummaryCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        TwitterSummaryCardDataObject dataObj = new TwitterSummaryCardDataObject(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight);
        return updateTwitterSummaryCard(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateTwitterSummaryCard(TwitterSummaryCard twitterSummaryCard) throws BaseException
    {
        log.finer("BEGIN");

        // Param twitterSummaryCard cannot be null.....
        if(twitterSummaryCard == null || twitterSummaryCard.getGuid() == null) {
            log.log(Level.INFO, "Param twitterSummaryCard or its guid is null!");
            throw new BadRequestException("Param twitterSummaryCard object or its guid is null!");
        }
        TwitterSummaryCardDataObject dataObj = null;
        if(twitterSummaryCard instanceof TwitterSummaryCardDataObject) {
            dataObj = (TwitterSummaryCardDataObject) twitterSummaryCard;
        } else if(twitterSummaryCard instanceof TwitterSummaryCardBean) {
            dataObj = ((TwitterSummaryCardBean) twitterSummaryCard).toDataObject();
        } else {  // if(twitterSummaryCard instanceof TwitterSummaryCard)
            dataObj = new TwitterSummaryCardDataObject(twitterSummaryCard.getGuid(), twitterSummaryCard.getCard(), twitterSummaryCard.getUrl(), twitterSummaryCard.getTitle(), twitterSummaryCard.getDescription(), twitterSummaryCard.getSite(), twitterSummaryCard.getSiteId(), twitterSummaryCard.getCreator(), twitterSummaryCard.getCreatorId(), twitterSummaryCard.getImage(), twitterSummaryCard.getImageWidth(), twitterSummaryCard.getImageHeight());
        }
        Boolean suc = getDAOFactory().getTwitterSummaryCardDAO().updateTwitterSummaryCard(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteTwitterSummaryCard(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getTwitterSummaryCardDAO().deleteTwitterSummaryCard(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteTwitterSummaryCard(TwitterSummaryCard twitterSummaryCard) throws BaseException
    {
        log.finer("BEGIN");

        // Param twitterSummaryCard cannot be null.....
        if(twitterSummaryCard == null || twitterSummaryCard.getGuid() == null) {
            log.log(Level.INFO, "Param twitterSummaryCard or its guid is null!");
            throw new BadRequestException("Param twitterSummaryCard object or its guid is null!");
        }
        TwitterSummaryCardDataObject dataObj = null;
        if(twitterSummaryCard instanceof TwitterSummaryCardDataObject) {
            dataObj = (TwitterSummaryCardDataObject) twitterSummaryCard;
        } else if(twitterSummaryCard instanceof TwitterSummaryCardBean) {
            dataObj = ((TwitterSummaryCardBean) twitterSummaryCard).toDataObject();
        } else {  // if(twitterSummaryCard instanceof TwitterSummaryCard)
            dataObj = new TwitterSummaryCardDataObject(twitterSummaryCard.getGuid(), twitterSummaryCard.getCard(), twitterSummaryCard.getUrl(), twitterSummaryCard.getTitle(), twitterSummaryCard.getDescription(), twitterSummaryCard.getSite(), twitterSummaryCard.getSiteId(), twitterSummaryCard.getCreator(), twitterSummaryCard.getCreatorId(), twitterSummaryCard.getImage(), twitterSummaryCard.getImageWidth(), twitterSummaryCard.getImageHeight());
        }
        Boolean suc = getDAOFactory().getTwitterSummaryCardDAO().deleteTwitterSummaryCard(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteTwitterSummaryCards(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getTwitterSummaryCardDAO().deleteTwitterSummaryCards(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
