package com.myurldb.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.UrlRating;
import com.myurldb.ws.bean.UrlRatingBean;
import com.myurldb.ws.dao.DAOFactory;
import com.myurldb.ws.data.UrlRatingDataObject;
import com.myurldb.ws.service.DAOFactoryManager;
import com.myurldb.ws.service.UrlRatingService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class UrlRatingServiceImpl implements UrlRatingService
{
    private static final Logger log = Logger.getLogger(UrlRatingServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // UrlRating related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public UrlRating getUrlRating(String guid) throws BaseException
    {
        log.finer("BEGIN");

        UrlRatingDataObject dataObj = getDAOFactory().getUrlRatingDAO().getUrlRating(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve UrlRatingDataObject for guid = " + guid);
            return null;  // ????
        }
        UrlRatingBean bean = new UrlRatingBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getUrlRating(String guid, String field) throws BaseException
    {
        UrlRatingDataObject dataObj = getDAOFactory().getUrlRatingDAO().getUrlRating(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve UrlRatingDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("domain")) {
            return dataObj.getDomain();
        } else if(field.equals("longUrl")) {
            return dataObj.getLongUrl();
        } else if(field.equals("longUrlHash")) {
            return dataObj.getLongUrlHash();
        } else if(field.equals("preview")) {
            return dataObj.getPreview();
        } else if(field.equals("flag")) {
            return dataObj.getFlag();
        } else if(field.equals("rating")) {
            return dataObj.getRating();
        } else if(field.equals("note")) {
            return dataObj.getNote();
        } else if(field.equals("ratedTime")) {
            return dataObj.getRatedTime();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<UrlRating> getUrlRatings(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<UrlRating> list = new ArrayList<UrlRating>();
        List<UrlRatingDataObject> dataObjs = getDAOFactory().getUrlRatingDAO().getUrlRatings(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve UrlRatingDataObject list.");
        } else {
            Iterator<UrlRatingDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                UrlRatingDataObject dataObj = (UrlRatingDataObject) it.next();
                list.add(new UrlRatingBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<UrlRating> getAllUrlRatings() throws BaseException
    {
        return getAllUrlRatings(null, null, null);
    }

    @Override
    public List<UrlRating> getAllUrlRatings(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<UrlRating> list = new ArrayList<UrlRating>();
        List<UrlRatingDataObject> dataObjs = getDAOFactory().getUrlRatingDAO().getAllUrlRatings(ordering, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve UrlRatingDataObject list.");
        } else {
            Iterator<UrlRatingDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                UrlRatingDataObject dataObj = (UrlRatingDataObject) it.next();
                list.add(new UrlRatingBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllUrlRatingKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        List<String> keys = getDAOFactory().getUrlRatingDAO().getAllUrlRatingKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve UrlRating key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<UrlRating> findUrlRatings(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findUrlRatings(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<UrlRating> findUrlRatings(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("UrlRatingServiceImpl.findUrlRatings(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<UrlRating> list = new ArrayList<UrlRating>();
        List<UrlRatingDataObject> dataObjs = getDAOFactory().getUrlRatingDAO().findUrlRatings(filter, ordering, params, values, grouping, unique, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find urlRatings for the given criterion.");
        } else {
            Iterator<UrlRatingDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                UrlRatingDataObject dataObj = (UrlRatingDataObject) it.next();
                list.add(new UrlRatingBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findUrlRatingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("UrlRatingServiceImpl.findUrlRatingKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = getDAOFactory().getUrlRatingDAO().findUrlRatingKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find UrlRating keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("UrlRatingServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getUrlRatingDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createUrlRating(String domain, String longUrl, String longUrlHash, String preview, String flag, Double rating, String note, Long ratedTime) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        UrlRatingDataObject dataObj = new UrlRatingDataObject(null, domain, longUrl, longUrlHash, preview, flag, rating, note, ratedTime);
        return createUrlRating(dataObj);
    }

    @Override
    public String createUrlRating(UrlRating urlRating) throws BaseException
    {
        log.finer("BEGIN");

        // Param urlRating cannot be null.....
        if(urlRating == null) {
            log.log(Level.INFO, "Param urlRating is null!");
            throw new BadRequestException("Param urlRating object is null!");
        }
        UrlRatingDataObject dataObj = null;
        if(urlRating instanceof UrlRatingDataObject) {
            dataObj = (UrlRatingDataObject) urlRating;
        } else if(urlRating instanceof UrlRatingBean) {
            dataObj = ((UrlRatingBean) urlRating).toDataObject();
        } else {  // if(urlRating instanceof UrlRating)
            //dataObj = new UrlRatingDataObject(null, urlRating.getDomain(), urlRating.getLongUrl(), urlRating.getLongUrlHash(), urlRating.getPreview(), urlRating.getFlag(), urlRating.getRating(), urlRating.getNote(), urlRating.getRatedTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new UrlRatingDataObject(urlRating.getGuid(), urlRating.getDomain(), urlRating.getLongUrl(), urlRating.getLongUrlHash(), urlRating.getPreview(), urlRating.getFlag(), urlRating.getRating(), urlRating.getNote(), urlRating.getRatedTime());
        }
        String guid = getDAOFactory().getUrlRatingDAO().createUrlRating(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateUrlRating(String guid, String domain, String longUrl, String longUrlHash, String preview, String flag, Double rating, String note, Long ratedTime) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        UrlRatingDataObject dataObj = new UrlRatingDataObject(guid, domain, longUrl, longUrlHash, preview, flag, rating, note, ratedTime);
        return updateUrlRating(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateUrlRating(UrlRating urlRating) throws BaseException
    {
        log.finer("BEGIN");

        // Param urlRating cannot be null.....
        if(urlRating == null || urlRating.getGuid() == null) {
            log.log(Level.INFO, "Param urlRating or its guid is null!");
            throw new BadRequestException("Param urlRating object or its guid is null!");
        }
        UrlRatingDataObject dataObj = null;
        if(urlRating instanceof UrlRatingDataObject) {
            dataObj = (UrlRatingDataObject) urlRating;
        } else if(urlRating instanceof UrlRatingBean) {
            dataObj = ((UrlRatingBean) urlRating).toDataObject();
        } else {  // if(urlRating instanceof UrlRating)
            dataObj = new UrlRatingDataObject(urlRating.getGuid(), urlRating.getDomain(), urlRating.getLongUrl(), urlRating.getLongUrlHash(), urlRating.getPreview(), urlRating.getFlag(), urlRating.getRating(), urlRating.getNote(), urlRating.getRatedTime());
        }
        Boolean suc = getDAOFactory().getUrlRatingDAO().updateUrlRating(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteUrlRating(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getUrlRatingDAO().deleteUrlRating(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteUrlRating(UrlRating urlRating) throws BaseException
    {
        log.finer("BEGIN");

        // Param urlRating cannot be null.....
        if(urlRating == null || urlRating.getGuid() == null) {
            log.log(Level.INFO, "Param urlRating or its guid is null!");
            throw new BadRequestException("Param urlRating object or its guid is null!");
        }
        UrlRatingDataObject dataObj = null;
        if(urlRating instanceof UrlRatingDataObject) {
            dataObj = (UrlRatingDataObject) urlRating;
        } else if(urlRating instanceof UrlRatingBean) {
            dataObj = ((UrlRatingBean) urlRating).toDataObject();
        } else {  // if(urlRating instanceof UrlRating)
            dataObj = new UrlRatingDataObject(urlRating.getGuid(), urlRating.getDomain(), urlRating.getLongUrl(), urlRating.getLongUrlHash(), urlRating.getPreview(), urlRating.getFlag(), urlRating.getRating(), urlRating.getNote(), urlRating.getRatedTime());
        }
        Boolean suc = getDAOFactory().getUrlRatingDAO().deleteUrlRating(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteUrlRatings(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getUrlRatingDAO().deleteUrlRatings(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
