package com.myurldb.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.UserCustomDomain;
import com.myurldb.ws.bean.UserCustomDomainBean;
import com.myurldb.ws.dao.DAOFactory;
import com.myurldb.ws.data.UserCustomDomainDataObject;
import com.myurldb.ws.service.DAOFactoryManager;
import com.myurldb.ws.service.UserCustomDomainService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class UserCustomDomainServiceImpl implements UserCustomDomainService
{
    private static final Logger log = Logger.getLogger(UserCustomDomainServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // UserCustomDomain related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public UserCustomDomain getUserCustomDomain(String guid) throws BaseException
    {
        log.finer("BEGIN");

        UserCustomDomainDataObject dataObj = getDAOFactory().getUserCustomDomainDAO().getUserCustomDomain(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve UserCustomDomainDataObject for guid = " + guid);
            return null;  // ????
        }
        UserCustomDomainBean bean = new UserCustomDomainBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getUserCustomDomain(String guid, String field) throws BaseException
    {
        UserCustomDomainDataObject dataObj = getDAOFactory().getUserCustomDomainDAO().getUserCustomDomain(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve UserCustomDomainDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("owner")) {
            return dataObj.getOwner();
        } else if(field.equals("domain")) {
            return dataObj.getDomain();
        } else if(field.equals("verified")) {
            return dataObj.isVerified();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("verifiedTime")) {
            return dataObj.getVerifiedTime();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<UserCustomDomain> getUserCustomDomains(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<UserCustomDomain> list = new ArrayList<UserCustomDomain>();
        List<UserCustomDomainDataObject> dataObjs = getDAOFactory().getUserCustomDomainDAO().getUserCustomDomains(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve UserCustomDomainDataObject list.");
        } else {
            Iterator<UserCustomDomainDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                UserCustomDomainDataObject dataObj = (UserCustomDomainDataObject) it.next();
                list.add(new UserCustomDomainBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<UserCustomDomain> getAllUserCustomDomains() throws BaseException
    {
        return getAllUserCustomDomains(null, null, null);
    }

    @Override
    public List<UserCustomDomain> getAllUserCustomDomains(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<UserCustomDomain> list = new ArrayList<UserCustomDomain>();
        List<UserCustomDomainDataObject> dataObjs = getDAOFactory().getUserCustomDomainDAO().getAllUserCustomDomains(ordering, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve UserCustomDomainDataObject list.");
        } else {
            Iterator<UserCustomDomainDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                UserCustomDomainDataObject dataObj = (UserCustomDomainDataObject) it.next();
                list.add(new UserCustomDomainBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllUserCustomDomainKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        List<String> keys = getDAOFactory().getUserCustomDomainDAO().getAllUserCustomDomainKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve UserCustomDomain key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<UserCustomDomain> findUserCustomDomains(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findUserCustomDomains(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<UserCustomDomain> findUserCustomDomains(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("UserCustomDomainServiceImpl.findUserCustomDomains(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<UserCustomDomain> list = new ArrayList<UserCustomDomain>();
        List<UserCustomDomainDataObject> dataObjs = getDAOFactory().getUserCustomDomainDAO().findUserCustomDomains(filter, ordering, params, values, grouping, unique, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find userCustomDomains for the given criterion.");
        } else {
            Iterator<UserCustomDomainDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                UserCustomDomainDataObject dataObj = (UserCustomDomainDataObject) it.next();
                list.add(new UserCustomDomainBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findUserCustomDomainKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("UserCustomDomainServiceImpl.findUserCustomDomainKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = getDAOFactory().getUserCustomDomainDAO().findUserCustomDomainKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find UserCustomDomain keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("UserCustomDomainServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getUserCustomDomainDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createUserCustomDomain(String owner, String domain, Boolean verified, String status, Long verifiedTime) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        UserCustomDomainDataObject dataObj = new UserCustomDomainDataObject(null, owner, domain, verified, status, verifiedTime);
        return createUserCustomDomain(dataObj);
    }

    @Override
    public String createUserCustomDomain(UserCustomDomain userCustomDomain) throws BaseException
    {
        log.finer("BEGIN");

        // Param userCustomDomain cannot be null.....
        if(userCustomDomain == null) {
            log.log(Level.INFO, "Param userCustomDomain is null!");
            throw new BadRequestException("Param userCustomDomain object is null!");
        }
        UserCustomDomainDataObject dataObj = null;
        if(userCustomDomain instanceof UserCustomDomainDataObject) {
            dataObj = (UserCustomDomainDataObject) userCustomDomain;
        } else if(userCustomDomain instanceof UserCustomDomainBean) {
            dataObj = ((UserCustomDomainBean) userCustomDomain).toDataObject();
        } else {  // if(userCustomDomain instanceof UserCustomDomain)
            //dataObj = new UserCustomDomainDataObject(null, userCustomDomain.getOwner(), userCustomDomain.getDomain(), userCustomDomain.isVerified(), userCustomDomain.getStatus(), userCustomDomain.getVerifiedTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new UserCustomDomainDataObject(userCustomDomain.getGuid(), userCustomDomain.getOwner(), userCustomDomain.getDomain(), userCustomDomain.isVerified(), userCustomDomain.getStatus(), userCustomDomain.getVerifiedTime());
        }
        String guid = getDAOFactory().getUserCustomDomainDAO().createUserCustomDomain(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateUserCustomDomain(String guid, String owner, String domain, Boolean verified, String status, Long verifiedTime) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        UserCustomDomainDataObject dataObj = new UserCustomDomainDataObject(guid, owner, domain, verified, status, verifiedTime);
        return updateUserCustomDomain(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateUserCustomDomain(UserCustomDomain userCustomDomain) throws BaseException
    {
        log.finer("BEGIN");

        // Param userCustomDomain cannot be null.....
        if(userCustomDomain == null || userCustomDomain.getGuid() == null) {
            log.log(Level.INFO, "Param userCustomDomain or its guid is null!");
            throw new BadRequestException("Param userCustomDomain object or its guid is null!");
        }
        UserCustomDomainDataObject dataObj = null;
        if(userCustomDomain instanceof UserCustomDomainDataObject) {
            dataObj = (UserCustomDomainDataObject) userCustomDomain;
        } else if(userCustomDomain instanceof UserCustomDomainBean) {
            dataObj = ((UserCustomDomainBean) userCustomDomain).toDataObject();
        } else {  // if(userCustomDomain instanceof UserCustomDomain)
            dataObj = new UserCustomDomainDataObject(userCustomDomain.getGuid(), userCustomDomain.getOwner(), userCustomDomain.getDomain(), userCustomDomain.isVerified(), userCustomDomain.getStatus(), userCustomDomain.getVerifiedTime());
        }
        Boolean suc = getDAOFactory().getUserCustomDomainDAO().updateUserCustomDomain(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteUserCustomDomain(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getUserCustomDomainDAO().deleteUserCustomDomain(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteUserCustomDomain(UserCustomDomain userCustomDomain) throws BaseException
    {
        log.finer("BEGIN");

        // Param userCustomDomain cannot be null.....
        if(userCustomDomain == null || userCustomDomain.getGuid() == null) {
            log.log(Level.INFO, "Param userCustomDomain or its guid is null!");
            throw new BadRequestException("Param userCustomDomain object or its guid is null!");
        }
        UserCustomDomainDataObject dataObj = null;
        if(userCustomDomain instanceof UserCustomDomainDataObject) {
            dataObj = (UserCustomDomainDataObject) userCustomDomain;
        } else if(userCustomDomain instanceof UserCustomDomainBean) {
            dataObj = ((UserCustomDomainBean) userCustomDomain).toDataObject();
        } else {  // if(userCustomDomain instanceof UserCustomDomain)
            dataObj = new UserCustomDomainDataObject(userCustomDomain.getGuid(), userCustomDomain.getOwner(), userCustomDomain.getDomain(), userCustomDomain.isVerified(), userCustomDomain.getStatus(), userCustomDomain.getVerifiedTime());
        }
        Boolean suc = getDAOFactory().getUserCustomDomainDAO().deleteUserCustomDomain(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteUserCustomDomains(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getUserCustomDomainDAO().deleteUserCustomDomains(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
