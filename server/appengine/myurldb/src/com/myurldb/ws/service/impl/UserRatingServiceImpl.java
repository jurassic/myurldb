package com.myurldb.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.UserRating;
import com.myurldb.ws.bean.UserRatingBean;
import com.myurldb.ws.dao.DAOFactory;
import com.myurldb.ws.data.UserRatingDataObject;
import com.myurldb.ws.service.DAOFactoryManager;
import com.myurldb.ws.service.UserRatingService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class UserRatingServiceImpl implements UserRatingService
{
    private static final Logger log = Logger.getLogger(UserRatingServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // UserRating related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public UserRating getUserRating(String guid) throws BaseException
    {
        log.finer("BEGIN");

        UserRatingDataObject dataObj = getDAOFactory().getUserRatingDAO().getUserRating(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve UserRatingDataObject for guid = " + guid);
            return null;  // ????
        }
        UserRatingBean bean = new UserRatingBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getUserRating(String guid, String field) throws BaseException
    {
        UserRatingDataObject dataObj = getDAOFactory().getUserRatingDAO().getUserRating(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve UserRatingDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return dataObj.getUser();
        } else if(field.equals("rating")) {
            return dataObj.getRating();
        } else if(field.equals("note")) {
            return dataObj.getNote();
        } else if(field.equals("ratedTime")) {
            return dataObj.getRatedTime();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<UserRating> getUserRatings(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<UserRating> list = new ArrayList<UserRating>();
        List<UserRatingDataObject> dataObjs = getDAOFactory().getUserRatingDAO().getUserRatings(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve UserRatingDataObject list.");
        } else {
            Iterator<UserRatingDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                UserRatingDataObject dataObj = (UserRatingDataObject) it.next();
                list.add(new UserRatingBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<UserRating> getAllUserRatings() throws BaseException
    {
        return getAllUserRatings(null, null, null);
    }

    @Override
    public List<UserRating> getAllUserRatings(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<UserRating> list = new ArrayList<UserRating>();
        List<UserRatingDataObject> dataObjs = getDAOFactory().getUserRatingDAO().getAllUserRatings(ordering, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve UserRatingDataObject list.");
        } else {
            Iterator<UserRatingDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                UserRatingDataObject dataObj = (UserRatingDataObject) it.next();
                list.add(new UserRatingBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllUserRatingKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        List<String> keys = getDAOFactory().getUserRatingDAO().getAllUserRatingKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve UserRating key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<UserRating> findUserRatings(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findUserRatings(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<UserRating> findUserRatings(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("UserRatingServiceImpl.findUserRatings(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<UserRating> list = new ArrayList<UserRating>();
        List<UserRatingDataObject> dataObjs = getDAOFactory().getUserRatingDAO().findUserRatings(filter, ordering, params, values, grouping, unique, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find userRatings for the given criterion.");
        } else {
            Iterator<UserRatingDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                UserRatingDataObject dataObj = (UserRatingDataObject) it.next();
                list.add(new UserRatingBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findUserRatingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("UserRatingServiceImpl.findUserRatingKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = getDAOFactory().getUserRatingDAO().findUserRatingKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find UserRating keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("UserRatingServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getUserRatingDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createUserRating(String user, Double rating, String note, Long ratedTime) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        UserRatingDataObject dataObj = new UserRatingDataObject(null, user, rating, note, ratedTime);
        return createUserRating(dataObj);
    }

    @Override
    public String createUserRating(UserRating userRating) throws BaseException
    {
        log.finer("BEGIN");

        // Param userRating cannot be null.....
        if(userRating == null) {
            log.log(Level.INFO, "Param userRating is null!");
            throw new BadRequestException("Param userRating object is null!");
        }
        UserRatingDataObject dataObj = null;
        if(userRating instanceof UserRatingDataObject) {
            dataObj = (UserRatingDataObject) userRating;
        } else if(userRating instanceof UserRatingBean) {
            dataObj = ((UserRatingBean) userRating).toDataObject();
        } else {  // if(userRating instanceof UserRating)
            //dataObj = new UserRatingDataObject(null, userRating.getUser(), userRating.getRating(), userRating.getNote(), userRating.getRatedTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new UserRatingDataObject(userRating.getGuid(), userRating.getUser(), userRating.getRating(), userRating.getNote(), userRating.getRatedTime());
        }
        String guid = getDAOFactory().getUserRatingDAO().createUserRating(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateUserRating(String guid, String user, Double rating, String note, Long ratedTime) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        UserRatingDataObject dataObj = new UserRatingDataObject(guid, user, rating, note, ratedTime);
        return updateUserRating(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateUserRating(UserRating userRating) throws BaseException
    {
        log.finer("BEGIN");

        // Param userRating cannot be null.....
        if(userRating == null || userRating.getGuid() == null) {
            log.log(Level.INFO, "Param userRating or its guid is null!");
            throw new BadRequestException("Param userRating object or its guid is null!");
        }
        UserRatingDataObject dataObj = null;
        if(userRating instanceof UserRatingDataObject) {
            dataObj = (UserRatingDataObject) userRating;
        } else if(userRating instanceof UserRatingBean) {
            dataObj = ((UserRatingBean) userRating).toDataObject();
        } else {  // if(userRating instanceof UserRating)
            dataObj = new UserRatingDataObject(userRating.getGuid(), userRating.getUser(), userRating.getRating(), userRating.getNote(), userRating.getRatedTime());
        }
        Boolean suc = getDAOFactory().getUserRatingDAO().updateUserRating(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteUserRating(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getUserRatingDAO().deleteUserRating(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteUserRating(UserRating userRating) throws BaseException
    {
        log.finer("BEGIN");

        // Param userRating cannot be null.....
        if(userRating == null || userRating.getGuid() == null) {
            log.log(Level.INFO, "Param userRating or its guid is null!");
            throw new BadRequestException("Param userRating object or its guid is null!");
        }
        UserRatingDataObject dataObj = null;
        if(userRating instanceof UserRatingDataObject) {
            dataObj = (UserRatingDataObject) userRating;
        } else if(userRating instanceof UserRatingBean) {
            dataObj = ((UserRatingBean) userRating).toDataObject();
        } else {  // if(userRating instanceof UserRating)
            dataObj = new UserRatingDataObject(userRating.getGuid(), userRating.getUser(), userRating.getRating(), userRating.getNote(), userRating.getRatedTime());
        }
        Boolean suc = getDAOFactory().getUserRatingDAO().deleteUserRating(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteUserRatings(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getUserRatingDAO().deleteUserRatings(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
