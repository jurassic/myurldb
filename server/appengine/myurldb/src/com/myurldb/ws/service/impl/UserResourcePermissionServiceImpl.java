package com.myurldb.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.UserResourcePermission;
import com.myurldb.ws.bean.UserResourcePermissionBean;
import com.myurldb.ws.dao.DAOFactory;
import com.myurldb.ws.data.UserResourcePermissionDataObject;
import com.myurldb.ws.service.DAOFactoryManager;
import com.myurldb.ws.service.UserResourcePermissionService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class UserResourcePermissionServiceImpl implements UserResourcePermissionService
{
    private static final Logger log = Logger.getLogger(UserResourcePermissionServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // UserResourcePermission related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public UserResourcePermission getUserResourcePermission(String guid) throws BaseException
    {
        log.finer("BEGIN");

        UserResourcePermissionDataObject dataObj = getDAOFactory().getUserResourcePermissionDAO().getUserResourcePermission(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve UserResourcePermissionDataObject for guid = " + guid);
            return null;  // ????
        }
        UserResourcePermissionBean bean = new UserResourcePermissionBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getUserResourcePermission(String guid, String field) throws BaseException
    {
        UserResourcePermissionDataObject dataObj = getDAOFactory().getUserResourcePermissionDAO().getUserResourcePermission(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve UserResourcePermissionDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return dataObj.getUser();
        } else if(field.equals("permissionName")) {
            return dataObj.getPermissionName();
        } else if(field.equals("resource")) {
            return dataObj.getResource();
        } else if(field.equals("instance")) {
            return dataObj.getInstance();
        } else if(field.equals("action")) {
            return dataObj.getAction();
        } else if(field.equals("permitted")) {
            return dataObj.isPermitted();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<UserResourcePermission> getUserResourcePermissions(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<UserResourcePermission> list = new ArrayList<UserResourcePermission>();
        List<UserResourcePermissionDataObject> dataObjs = getDAOFactory().getUserResourcePermissionDAO().getUserResourcePermissions(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve UserResourcePermissionDataObject list.");
        } else {
            Iterator<UserResourcePermissionDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                UserResourcePermissionDataObject dataObj = (UserResourcePermissionDataObject) it.next();
                list.add(new UserResourcePermissionBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<UserResourcePermission> getAllUserResourcePermissions() throws BaseException
    {
        return getAllUserResourcePermissions(null, null, null);
    }

    @Override
    public List<UserResourcePermission> getAllUserResourcePermissions(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<UserResourcePermission> list = new ArrayList<UserResourcePermission>();
        List<UserResourcePermissionDataObject> dataObjs = getDAOFactory().getUserResourcePermissionDAO().getAllUserResourcePermissions(ordering, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve UserResourcePermissionDataObject list.");
        } else {
            Iterator<UserResourcePermissionDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                UserResourcePermissionDataObject dataObj = (UserResourcePermissionDataObject) it.next();
                list.add(new UserResourcePermissionBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllUserResourcePermissionKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        List<String> keys = getDAOFactory().getUserResourcePermissionDAO().getAllUserResourcePermissionKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve UserResourcePermission key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<UserResourcePermission> findUserResourcePermissions(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findUserResourcePermissions(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<UserResourcePermission> findUserResourcePermissions(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("UserResourcePermissionServiceImpl.findUserResourcePermissions(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<UserResourcePermission> list = new ArrayList<UserResourcePermission>();
        List<UserResourcePermissionDataObject> dataObjs = getDAOFactory().getUserResourcePermissionDAO().findUserResourcePermissions(filter, ordering, params, values, grouping, unique, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find userResourcePermissions for the given criterion.");
        } else {
            Iterator<UserResourcePermissionDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                UserResourcePermissionDataObject dataObj = (UserResourcePermissionDataObject) it.next();
                list.add(new UserResourcePermissionBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findUserResourcePermissionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("UserResourcePermissionServiceImpl.findUserResourcePermissionKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = getDAOFactory().getUserResourcePermissionDAO().findUserResourcePermissionKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find UserResourcePermission keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("UserResourcePermissionServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getUserResourcePermissionDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createUserResourcePermission(String user, String permissionName, String resource, String instance, String action, Boolean permitted, String status) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        UserResourcePermissionDataObject dataObj = new UserResourcePermissionDataObject(null, user, permissionName, resource, instance, action, permitted, status);
        return createUserResourcePermission(dataObj);
    }

    @Override
    public String createUserResourcePermission(UserResourcePermission userResourcePermission) throws BaseException
    {
        log.finer("BEGIN");

        // Param userResourcePermission cannot be null.....
        if(userResourcePermission == null) {
            log.log(Level.INFO, "Param userResourcePermission is null!");
            throw new BadRequestException("Param userResourcePermission object is null!");
        }
        UserResourcePermissionDataObject dataObj = null;
        if(userResourcePermission instanceof UserResourcePermissionDataObject) {
            dataObj = (UserResourcePermissionDataObject) userResourcePermission;
        } else if(userResourcePermission instanceof UserResourcePermissionBean) {
            dataObj = ((UserResourcePermissionBean) userResourcePermission).toDataObject();
        } else {  // if(userResourcePermission instanceof UserResourcePermission)
            //dataObj = new UserResourcePermissionDataObject(null, userResourcePermission.getUser(), userResourcePermission.getPermissionName(), userResourcePermission.getResource(), userResourcePermission.getInstance(), userResourcePermission.getAction(), userResourcePermission.isPermitted(), userResourcePermission.getStatus());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new UserResourcePermissionDataObject(userResourcePermission.getGuid(), userResourcePermission.getUser(), userResourcePermission.getPermissionName(), userResourcePermission.getResource(), userResourcePermission.getInstance(), userResourcePermission.getAction(), userResourcePermission.isPermitted(), userResourcePermission.getStatus());
        }
        String guid = getDAOFactory().getUserResourcePermissionDAO().createUserResourcePermission(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateUserResourcePermission(String guid, String user, String permissionName, String resource, String instance, String action, Boolean permitted, String status) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        UserResourcePermissionDataObject dataObj = new UserResourcePermissionDataObject(guid, user, permissionName, resource, instance, action, permitted, status);
        return updateUserResourcePermission(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateUserResourcePermission(UserResourcePermission userResourcePermission) throws BaseException
    {
        log.finer("BEGIN");

        // Param userResourcePermission cannot be null.....
        if(userResourcePermission == null || userResourcePermission.getGuid() == null) {
            log.log(Level.INFO, "Param userResourcePermission or its guid is null!");
            throw new BadRequestException("Param userResourcePermission object or its guid is null!");
        }
        UserResourcePermissionDataObject dataObj = null;
        if(userResourcePermission instanceof UserResourcePermissionDataObject) {
            dataObj = (UserResourcePermissionDataObject) userResourcePermission;
        } else if(userResourcePermission instanceof UserResourcePermissionBean) {
            dataObj = ((UserResourcePermissionBean) userResourcePermission).toDataObject();
        } else {  // if(userResourcePermission instanceof UserResourcePermission)
            dataObj = new UserResourcePermissionDataObject(userResourcePermission.getGuid(), userResourcePermission.getUser(), userResourcePermission.getPermissionName(), userResourcePermission.getResource(), userResourcePermission.getInstance(), userResourcePermission.getAction(), userResourcePermission.isPermitted(), userResourcePermission.getStatus());
        }
        Boolean suc = getDAOFactory().getUserResourcePermissionDAO().updateUserResourcePermission(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteUserResourcePermission(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getUserResourcePermissionDAO().deleteUserResourcePermission(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteUserResourcePermission(UserResourcePermission userResourcePermission) throws BaseException
    {
        log.finer("BEGIN");

        // Param userResourcePermission cannot be null.....
        if(userResourcePermission == null || userResourcePermission.getGuid() == null) {
            log.log(Level.INFO, "Param userResourcePermission or its guid is null!");
            throw new BadRequestException("Param userResourcePermission object or its guid is null!");
        }
        UserResourcePermissionDataObject dataObj = null;
        if(userResourcePermission instanceof UserResourcePermissionDataObject) {
            dataObj = (UserResourcePermissionDataObject) userResourcePermission;
        } else if(userResourcePermission instanceof UserResourcePermissionBean) {
            dataObj = ((UserResourcePermissionBean) userResourcePermission).toDataObject();
        } else {  // if(userResourcePermission instanceof UserResourcePermission)
            dataObj = new UserResourcePermissionDataObject(userResourcePermission.getGuid(), userResourcePermission.getUser(), userResourcePermission.getPermissionName(), userResourcePermission.getResource(), userResourcePermission.getInstance(), userResourcePermission.getAction(), userResourcePermission.isPermitted(), userResourcePermission.getStatus());
        }
        Boolean suc = getDAOFactory().getUserResourcePermissionDAO().deleteUserResourcePermission(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteUserResourcePermissions(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getUserResourcePermissionDAO().deleteUserResourcePermissions(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
