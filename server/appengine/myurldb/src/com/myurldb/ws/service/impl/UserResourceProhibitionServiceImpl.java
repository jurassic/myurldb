package com.myurldb.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.UserResourceProhibition;
import com.myurldb.ws.bean.UserResourceProhibitionBean;
import com.myurldb.ws.dao.DAOFactory;
import com.myurldb.ws.data.UserResourceProhibitionDataObject;
import com.myurldb.ws.service.DAOFactoryManager;
import com.myurldb.ws.service.UserResourceProhibitionService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class UserResourceProhibitionServiceImpl implements UserResourceProhibitionService
{
    private static final Logger log = Logger.getLogger(UserResourceProhibitionServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // UserResourceProhibition related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public UserResourceProhibition getUserResourceProhibition(String guid) throws BaseException
    {
        log.finer("BEGIN");

        UserResourceProhibitionDataObject dataObj = getDAOFactory().getUserResourceProhibitionDAO().getUserResourceProhibition(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve UserResourceProhibitionDataObject for guid = " + guid);
            return null;  // ????
        }
        UserResourceProhibitionBean bean = new UserResourceProhibitionBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getUserResourceProhibition(String guid, String field) throws BaseException
    {
        UserResourceProhibitionDataObject dataObj = getDAOFactory().getUserResourceProhibitionDAO().getUserResourceProhibition(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve UserResourceProhibitionDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return dataObj.getUser();
        } else if(field.equals("permissionName")) {
            return dataObj.getPermissionName();
        } else if(field.equals("resource")) {
            return dataObj.getResource();
        } else if(field.equals("instance")) {
            return dataObj.getInstance();
        } else if(field.equals("action")) {
            return dataObj.getAction();
        } else if(field.equals("prohibited")) {
            return dataObj.isProhibited();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<UserResourceProhibition> getUserResourceProhibitions(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<UserResourceProhibition> list = new ArrayList<UserResourceProhibition>();
        List<UserResourceProhibitionDataObject> dataObjs = getDAOFactory().getUserResourceProhibitionDAO().getUserResourceProhibitions(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve UserResourceProhibitionDataObject list.");
        } else {
            Iterator<UserResourceProhibitionDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                UserResourceProhibitionDataObject dataObj = (UserResourceProhibitionDataObject) it.next();
                list.add(new UserResourceProhibitionBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<UserResourceProhibition> getAllUserResourceProhibitions() throws BaseException
    {
        return getAllUserResourceProhibitions(null, null, null);
    }

    @Override
    public List<UserResourceProhibition> getAllUserResourceProhibitions(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<UserResourceProhibition> list = new ArrayList<UserResourceProhibition>();
        List<UserResourceProhibitionDataObject> dataObjs = getDAOFactory().getUserResourceProhibitionDAO().getAllUserResourceProhibitions(ordering, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve UserResourceProhibitionDataObject list.");
        } else {
            Iterator<UserResourceProhibitionDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                UserResourceProhibitionDataObject dataObj = (UserResourceProhibitionDataObject) it.next();
                list.add(new UserResourceProhibitionBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllUserResourceProhibitionKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        List<String> keys = getDAOFactory().getUserResourceProhibitionDAO().getAllUserResourceProhibitionKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve UserResourceProhibition key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<UserResourceProhibition> findUserResourceProhibitions(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findUserResourceProhibitions(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<UserResourceProhibition> findUserResourceProhibitions(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("UserResourceProhibitionServiceImpl.findUserResourceProhibitions(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<UserResourceProhibition> list = new ArrayList<UserResourceProhibition>();
        List<UserResourceProhibitionDataObject> dataObjs = getDAOFactory().getUserResourceProhibitionDAO().findUserResourceProhibitions(filter, ordering, params, values, grouping, unique, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find userResourceProhibitions for the given criterion.");
        } else {
            Iterator<UserResourceProhibitionDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                UserResourceProhibitionDataObject dataObj = (UserResourceProhibitionDataObject) it.next();
                list.add(new UserResourceProhibitionBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findUserResourceProhibitionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("UserResourceProhibitionServiceImpl.findUserResourceProhibitionKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = getDAOFactory().getUserResourceProhibitionDAO().findUserResourceProhibitionKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find UserResourceProhibition keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("UserResourceProhibitionServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getUserResourceProhibitionDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createUserResourceProhibition(String user, String permissionName, String resource, String instance, String action, Boolean prohibited, String status) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        UserResourceProhibitionDataObject dataObj = new UserResourceProhibitionDataObject(null, user, permissionName, resource, instance, action, prohibited, status);
        return createUserResourceProhibition(dataObj);
    }

    @Override
    public String createUserResourceProhibition(UserResourceProhibition userResourceProhibition) throws BaseException
    {
        log.finer("BEGIN");

        // Param userResourceProhibition cannot be null.....
        if(userResourceProhibition == null) {
            log.log(Level.INFO, "Param userResourceProhibition is null!");
            throw new BadRequestException("Param userResourceProhibition object is null!");
        }
        UserResourceProhibitionDataObject dataObj = null;
        if(userResourceProhibition instanceof UserResourceProhibitionDataObject) {
            dataObj = (UserResourceProhibitionDataObject) userResourceProhibition;
        } else if(userResourceProhibition instanceof UserResourceProhibitionBean) {
            dataObj = ((UserResourceProhibitionBean) userResourceProhibition).toDataObject();
        } else {  // if(userResourceProhibition instanceof UserResourceProhibition)
            //dataObj = new UserResourceProhibitionDataObject(null, userResourceProhibition.getUser(), userResourceProhibition.getPermissionName(), userResourceProhibition.getResource(), userResourceProhibition.getInstance(), userResourceProhibition.getAction(), userResourceProhibition.isProhibited(), userResourceProhibition.getStatus());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new UserResourceProhibitionDataObject(userResourceProhibition.getGuid(), userResourceProhibition.getUser(), userResourceProhibition.getPermissionName(), userResourceProhibition.getResource(), userResourceProhibition.getInstance(), userResourceProhibition.getAction(), userResourceProhibition.isProhibited(), userResourceProhibition.getStatus());
        }
        String guid = getDAOFactory().getUserResourceProhibitionDAO().createUserResourceProhibition(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateUserResourceProhibition(String guid, String user, String permissionName, String resource, String instance, String action, Boolean prohibited, String status) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        UserResourceProhibitionDataObject dataObj = new UserResourceProhibitionDataObject(guid, user, permissionName, resource, instance, action, prohibited, status);
        return updateUserResourceProhibition(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateUserResourceProhibition(UserResourceProhibition userResourceProhibition) throws BaseException
    {
        log.finer("BEGIN");

        // Param userResourceProhibition cannot be null.....
        if(userResourceProhibition == null || userResourceProhibition.getGuid() == null) {
            log.log(Level.INFO, "Param userResourceProhibition or its guid is null!");
            throw new BadRequestException("Param userResourceProhibition object or its guid is null!");
        }
        UserResourceProhibitionDataObject dataObj = null;
        if(userResourceProhibition instanceof UserResourceProhibitionDataObject) {
            dataObj = (UserResourceProhibitionDataObject) userResourceProhibition;
        } else if(userResourceProhibition instanceof UserResourceProhibitionBean) {
            dataObj = ((UserResourceProhibitionBean) userResourceProhibition).toDataObject();
        } else {  // if(userResourceProhibition instanceof UserResourceProhibition)
            dataObj = new UserResourceProhibitionDataObject(userResourceProhibition.getGuid(), userResourceProhibition.getUser(), userResourceProhibition.getPermissionName(), userResourceProhibition.getResource(), userResourceProhibition.getInstance(), userResourceProhibition.getAction(), userResourceProhibition.isProhibited(), userResourceProhibition.getStatus());
        }
        Boolean suc = getDAOFactory().getUserResourceProhibitionDAO().updateUserResourceProhibition(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteUserResourceProhibition(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getUserResourceProhibitionDAO().deleteUserResourceProhibition(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteUserResourceProhibition(UserResourceProhibition userResourceProhibition) throws BaseException
    {
        log.finer("BEGIN");

        // Param userResourceProhibition cannot be null.....
        if(userResourceProhibition == null || userResourceProhibition.getGuid() == null) {
            log.log(Level.INFO, "Param userResourceProhibition or its guid is null!");
            throw new BadRequestException("Param userResourceProhibition object or its guid is null!");
        }
        UserResourceProhibitionDataObject dataObj = null;
        if(userResourceProhibition instanceof UserResourceProhibitionDataObject) {
            dataObj = (UserResourceProhibitionDataObject) userResourceProhibition;
        } else if(userResourceProhibition instanceof UserResourceProhibitionBean) {
            dataObj = ((UserResourceProhibitionBean) userResourceProhibition).toDataObject();
        } else {  // if(userResourceProhibition instanceof UserResourceProhibition)
            dataObj = new UserResourceProhibitionDataObject(userResourceProhibition.getGuid(), userResourceProhibition.getUser(), userResourceProhibition.getPermissionName(), userResourceProhibition.getResource(), userResourceProhibition.getInstance(), userResourceProhibition.getAction(), userResourceProhibition.isProhibited(), userResourceProhibition.getStatus());
        }
        Boolean suc = getDAOFactory().getUserResourceProhibitionDAO().deleteUserResourceProhibition(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteUserResourceProhibitions(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getUserResourceProhibitionDAO().deleteUserResourceProhibitions(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
