package com.myurldb.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.UserRole;
import com.myurldb.ws.bean.UserRoleBean;
import com.myurldb.ws.dao.DAOFactory;
import com.myurldb.ws.data.UserRoleDataObject;
import com.myurldb.ws.service.DAOFactoryManager;
import com.myurldb.ws.service.UserRoleService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class UserRoleServiceImpl implements UserRoleService
{
    private static final Logger log = Logger.getLogger(UserRoleServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // UserRole related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public UserRole getUserRole(String guid) throws BaseException
    {
        log.finer("BEGIN");

        UserRoleDataObject dataObj = getDAOFactory().getUserRoleDAO().getUserRole(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve UserRoleDataObject for guid = " + guid);
            return null;  // ????
        }
        UserRoleBean bean = new UserRoleBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getUserRole(String guid, String field) throws BaseException
    {
        UserRoleDataObject dataObj = getDAOFactory().getUserRoleDAO().getUserRole(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve UserRoleDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return dataObj.getUser();
        } else if(field.equals("role")) {
            return dataObj.getRole();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<UserRole> getUserRoles(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<UserRole> list = new ArrayList<UserRole>();
        List<UserRoleDataObject> dataObjs = getDAOFactory().getUserRoleDAO().getUserRoles(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve UserRoleDataObject list.");
        } else {
            Iterator<UserRoleDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                UserRoleDataObject dataObj = (UserRoleDataObject) it.next();
                list.add(new UserRoleBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<UserRole> getAllUserRoles() throws BaseException
    {
        return getAllUserRoles(null, null, null);
    }

    @Override
    public List<UserRole> getAllUserRoles(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<UserRole> list = new ArrayList<UserRole>();
        List<UserRoleDataObject> dataObjs = getDAOFactory().getUserRoleDAO().getAllUserRoles(ordering, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve UserRoleDataObject list.");
        } else {
            Iterator<UserRoleDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                UserRoleDataObject dataObj = (UserRoleDataObject) it.next();
                list.add(new UserRoleBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllUserRoleKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        List<String> keys = getDAOFactory().getUserRoleDAO().getAllUserRoleKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve UserRole key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<UserRole> findUserRoles(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findUserRoles(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<UserRole> findUserRoles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("UserRoleServiceImpl.findUserRoles(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<UserRole> list = new ArrayList<UserRole>();
        List<UserRoleDataObject> dataObjs = getDAOFactory().getUserRoleDAO().findUserRoles(filter, ordering, params, values, grouping, unique, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find userRoles for the given criterion.");
        } else {
            Iterator<UserRoleDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                UserRoleDataObject dataObj = (UserRoleDataObject) it.next();
                list.add(new UserRoleBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findUserRoleKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("UserRoleServiceImpl.findUserRoleKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = getDAOFactory().getUserRoleDAO().findUserRoleKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find UserRole keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("UserRoleServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getUserRoleDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createUserRole(String user, String role, String status) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        UserRoleDataObject dataObj = new UserRoleDataObject(null, user, role, status);
        return createUserRole(dataObj);
    }

    @Override
    public String createUserRole(UserRole userRole) throws BaseException
    {
        log.finer("BEGIN");

        // Param userRole cannot be null.....
        if(userRole == null) {
            log.log(Level.INFO, "Param userRole is null!");
            throw new BadRequestException("Param userRole object is null!");
        }
        UserRoleDataObject dataObj = null;
        if(userRole instanceof UserRoleDataObject) {
            dataObj = (UserRoleDataObject) userRole;
        } else if(userRole instanceof UserRoleBean) {
            dataObj = ((UserRoleBean) userRole).toDataObject();
        } else {  // if(userRole instanceof UserRole)
            //dataObj = new UserRoleDataObject(null, userRole.getUser(), userRole.getRole(), userRole.getStatus());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new UserRoleDataObject(userRole.getGuid(), userRole.getUser(), userRole.getRole(), userRole.getStatus());
        }
        String guid = getDAOFactory().getUserRoleDAO().createUserRole(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateUserRole(String guid, String user, String role, String status) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        UserRoleDataObject dataObj = new UserRoleDataObject(guid, user, role, status);
        return updateUserRole(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateUserRole(UserRole userRole) throws BaseException
    {
        log.finer("BEGIN");

        // Param userRole cannot be null.....
        if(userRole == null || userRole.getGuid() == null) {
            log.log(Level.INFO, "Param userRole or its guid is null!");
            throw new BadRequestException("Param userRole object or its guid is null!");
        }
        UserRoleDataObject dataObj = null;
        if(userRole instanceof UserRoleDataObject) {
            dataObj = (UserRoleDataObject) userRole;
        } else if(userRole instanceof UserRoleBean) {
            dataObj = ((UserRoleBean) userRole).toDataObject();
        } else {  // if(userRole instanceof UserRole)
            dataObj = new UserRoleDataObject(userRole.getGuid(), userRole.getUser(), userRole.getRole(), userRole.getStatus());
        }
        Boolean suc = getDAOFactory().getUserRoleDAO().updateUserRole(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteUserRole(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getUserRoleDAO().deleteUserRole(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteUserRole(UserRole userRole) throws BaseException
    {
        log.finer("BEGIN");

        // Param userRole cannot be null.....
        if(userRole == null || userRole.getGuid() == null) {
            log.log(Level.INFO, "Param userRole or its guid is null!");
            throw new BadRequestException("Param userRole object or its guid is null!");
        }
        UserRoleDataObject dataObj = null;
        if(userRole instanceof UserRoleDataObject) {
            dataObj = (UserRoleDataObject) userRole;
        } else if(userRole instanceof UserRoleBean) {
            dataObj = ((UserRoleBean) userRole).toDataObject();
        } else {  // if(userRole instanceof UserRole)
            dataObj = new UserRoleDataObject(userRole.getGuid(), userRole.getUser(), userRole.getRole(), userRole.getStatus());
        }
        Boolean suc = getDAOFactory().getUserRoleDAO().deleteUserRole(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteUserRoles(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getUserRoleDAO().deleteUserRoles(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
