package com.myurldb.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.GeoPointStruct;
import com.myurldb.ws.StreetAddressStruct;
import com.myurldb.ws.GaeAppStruct;
import com.myurldb.ws.FullNameStruct;
import com.myurldb.ws.GaeUserStruct;
import com.myurldb.ws.User;
import com.myurldb.ws.bean.GeoPointStructBean;
import com.myurldb.ws.bean.StreetAddressStructBean;
import com.myurldb.ws.bean.GaeAppStructBean;
import com.myurldb.ws.bean.FullNameStructBean;
import com.myurldb.ws.bean.GaeUserStructBean;
import com.myurldb.ws.bean.UserBean;
import com.myurldb.ws.dao.DAOFactory;
import com.myurldb.ws.data.GeoPointStructDataObject;
import com.myurldb.ws.data.StreetAddressStructDataObject;
import com.myurldb.ws.data.GaeAppStructDataObject;
import com.myurldb.ws.data.FullNameStructDataObject;
import com.myurldb.ws.data.GaeUserStructDataObject;
import com.myurldb.ws.data.UserDataObject;
import com.myurldb.ws.service.DAOFactoryManager;
import com.myurldb.ws.service.UserService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class UserServiceImpl implements UserService
{
    private static final Logger log = Logger.getLogger(UserServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // User related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public User getUser(String guid) throws BaseException
    {
        log.finer("BEGIN");

        UserDataObject dataObj = getDAOFactory().getUserDAO().getUser(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve UserDataObject for guid = " + guid);
            return null;  // ????
        }
        UserBean bean = new UserBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getUser(String guid, String field) throws BaseException
    {
        UserDataObject dataObj = getDAOFactory().getUserDAO().getUser(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve UserDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("managerApp")) {
            return dataObj.getManagerApp();
        } else if(field.equals("appAcl")) {
            return dataObj.getAppAcl();
        } else if(field.equals("gaeApp")) {
            return dataObj.getGaeApp();
        } else if(field.equals("aeryId")) {
            return dataObj.getAeryId();
        } else if(field.equals("sessionId")) {
            return dataObj.getSessionId();
        } else if(field.equals("ancestorGuid")) {
            return dataObj.getAncestorGuid();
        } else if(field.equals("name")) {
            return dataObj.getName();
        } else if(field.equals("usercode")) {
            return dataObj.getUsercode();
        } else if(field.equals("username")) {
            return dataObj.getUsername();
        } else if(field.equals("nickname")) {
            return dataObj.getNickname();
        } else if(field.equals("avatar")) {
            return dataObj.getAvatar();
        } else if(field.equals("email")) {
            return dataObj.getEmail();
        } else if(field.equals("openId")) {
            return dataObj.getOpenId();
        } else if(field.equals("gaeUser")) {
            return dataObj.getGaeUser();
        } else if(field.equals("entityType")) {
            return dataObj.getEntityType();
        } else if(field.equals("surrogate")) {
            return dataObj.isSurrogate();
        } else if(field.equals("obsolete")) {
            return dataObj.isObsolete();
        } else if(field.equals("timeZone")) {
            return dataObj.getTimeZone();
        } else if(field.equals("location")) {
            return dataObj.getLocation();
        } else if(field.equals("streetAddress")) {
            return dataObj.getStreetAddress();
        } else if(field.equals("geoPoint")) {
            return dataObj.getGeoPoint();
        } else if(field.equals("ipAddress")) {
            return dataObj.getIpAddress();
        } else if(field.equals("referer")) {
            return dataObj.getReferer();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("emailVerifiedTime")) {
            return dataObj.getEmailVerifiedTime();
        } else if(field.equals("openIdVerifiedTime")) {
            return dataObj.getOpenIdVerifiedTime();
        } else if(field.equals("authenticatedTime")) {
            return dataObj.getAuthenticatedTime();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<User> getUsers(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<User> list = new ArrayList<User>();
        List<UserDataObject> dataObjs = getDAOFactory().getUserDAO().getUsers(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve UserDataObject list.");
        } else {
            Iterator<UserDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                UserDataObject dataObj = (UserDataObject) it.next();
                list.add(new UserBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<User> getAllUsers() throws BaseException
    {
        return getAllUsers(null, null, null);
    }

    @Override
    public List<User> getAllUsers(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<User> list = new ArrayList<User>();
        List<UserDataObject> dataObjs = getDAOFactory().getUserDAO().getAllUsers(ordering, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve UserDataObject list.");
        } else {
            Iterator<UserDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                UserDataObject dataObj = (UserDataObject) it.next();
                list.add(new UserBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllUserKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        List<String> keys = getDAOFactory().getUserDAO().getAllUserKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve User key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<User> findUsers(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findUsers(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<User> findUsers(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("UserServiceImpl.findUsers(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<User> list = new ArrayList<User>();
        List<UserDataObject> dataObjs = getDAOFactory().getUserDAO().findUsers(filter, ordering, params, values, grouping, unique, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find users for the given criterion.");
        } else {
            Iterator<UserDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                UserDataObject dataObj = (UserDataObject) it.next();
                list.add(new UserBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findUserKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("UserServiceImpl.findUserKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = getDAOFactory().getUserDAO().findUserKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find User keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("UserServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getUserDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createUser(String managerApp, Long appAcl, GaeAppStruct gaeApp, String aeryId, String sessionId, String ancestorGuid, FullNameStruct name, String usercode, String username, String nickname, String avatar, String email, String openId, GaeUserStruct gaeUser, String entityType, Boolean surrogate, Boolean obsolete, String timeZone, String location, StreetAddressStruct streetAddress, GeoPointStruct geoPoint, String ipAddress, String referer, String status, Long emailVerifiedTime, Long openIdVerifiedTime, Long authenticatedTime) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        GaeAppStructDataObject gaeAppDobj = null;
        if(gaeApp instanceof GaeAppStructBean) {
            gaeAppDobj = ((GaeAppStructBean) gaeApp).toDataObject();
        } else if(gaeApp instanceof GaeAppStruct) {
            gaeAppDobj = new GaeAppStructDataObject(gaeApp.getGroupId(), gaeApp.getAppId(), gaeApp.getAppDomain(), gaeApp.getNamespace(), gaeApp.getAcl(), gaeApp.getNote());
        } else {
            gaeAppDobj = null;   // ????
        }
        FullNameStructDataObject nameDobj = null;
        if(name instanceof FullNameStructBean) {
            nameDobj = ((FullNameStructBean) name).toDataObject();
        } else if(name instanceof FullNameStruct) {
            nameDobj = new FullNameStructDataObject(name.getUuid(), name.getDisplayName(), name.getLastName(), name.getFirstName(), name.getMiddleName1(), name.getMiddleName2(), name.getMiddleInitial(), name.getSalutation(), name.getSuffix(), name.getNote());
        } else {
            nameDobj = null;   // ????
        }
        GaeUserStructDataObject gaeUserDobj = null;
        if(gaeUser instanceof GaeUserStructBean) {
            gaeUserDobj = ((GaeUserStructBean) gaeUser).toDataObject();
        } else if(gaeUser instanceof GaeUserStruct) {
            gaeUserDobj = new GaeUserStructDataObject(gaeUser.getAuthDomain(), gaeUser.getFederatedIdentity(), gaeUser.getNickname(), gaeUser.getUserId(), gaeUser.getEmail(), gaeUser.getNote());
        } else {
            gaeUserDobj = null;   // ????
        }
        StreetAddressStructDataObject streetAddressDobj = null;
        if(streetAddress instanceof StreetAddressStructBean) {
            streetAddressDobj = ((StreetAddressStructBean) streetAddress).toDataObject();
        } else if(streetAddress instanceof StreetAddressStruct) {
            streetAddressDobj = new StreetAddressStructDataObject(streetAddress.getUuid(), streetAddress.getStreet1(), streetAddress.getStreet2(), streetAddress.getCity(), streetAddress.getCounty(), streetAddress.getPostalCode(), streetAddress.getState(), streetAddress.getProvince(), streetAddress.getCountry(), streetAddress.getCountryName(), streetAddress.getNote());
        } else {
            streetAddressDobj = null;   // ????
        }
        GeoPointStructDataObject geoPointDobj = null;
        if(geoPoint instanceof GeoPointStructBean) {
            geoPointDobj = ((GeoPointStructBean) geoPoint).toDataObject();
        } else if(geoPoint instanceof GeoPointStruct) {
            geoPointDobj = new GeoPointStructDataObject(geoPoint.getUuid(), geoPoint.getLatitude(), geoPoint.getLongitude(), geoPoint.getAltitude(), geoPoint.isSensorUsed());
        } else {
            geoPointDobj = null;   // ????
        }
        
        UserDataObject dataObj = new UserDataObject(null, managerApp, appAcl, gaeAppDobj, aeryId, sessionId, ancestorGuid, nameDobj, usercode, username, nickname, avatar, email, openId, gaeUserDobj, entityType, surrogate, obsolete, timeZone, location, streetAddressDobj, geoPointDobj, ipAddress, referer, status, emailVerifiedTime, openIdVerifiedTime, authenticatedTime);
        return createUser(dataObj);
    }

    @Override
    public String createUser(User user) throws BaseException
    {
        log.finer("BEGIN");

        // Param user cannot be null.....
        if(user == null) {
            log.log(Level.INFO, "Param user is null!");
            throw new BadRequestException("Param user object is null!");
        }
        UserDataObject dataObj = null;
        if(user instanceof UserDataObject) {
            dataObj = (UserDataObject) user;
        } else if(user instanceof UserBean) {
            dataObj = ((UserBean) user).toDataObject();
        } else {  // if(user instanceof User)
            //dataObj = new UserDataObject(null, user.getManagerApp(), user.getAppAcl(), (GaeAppStructDataObject) user.getGaeApp(), user.getAeryId(), user.getSessionId(), user.getAncestorGuid(), (FullNameStructDataObject) user.getName(), user.getUsercode(), user.getUsername(), user.getNickname(), user.getAvatar(), user.getEmail(), user.getOpenId(), (GaeUserStructDataObject) user.getGaeUser(), user.getEntityType(), user.isSurrogate(), user.isObsolete(), user.getTimeZone(), user.getLocation(), (StreetAddressStructDataObject) user.getStreetAddress(), (GeoPointStructDataObject) user.getGeoPoint(), user.getIpAddress(), user.getReferer(), user.getStatus(), user.getEmailVerifiedTime(), user.getOpenIdVerifiedTime(), user.getAuthenticatedTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new UserDataObject(user.getGuid(), user.getManagerApp(), user.getAppAcl(), (GaeAppStructDataObject) user.getGaeApp(), user.getAeryId(), user.getSessionId(), user.getAncestorGuid(), (FullNameStructDataObject) user.getName(), user.getUsercode(), user.getUsername(), user.getNickname(), user.getAvatar(), user.getEmail(), user.getOpenId(), (GaeUserStructDataObject) user.getGaeUser(), user.getEntityType(), user.isSurrogate(), user.isObsolete(), user.getTimeZone(), user.getLocation(), (StreetAddressStructDataObject) user.getStreetAddress(), (GeoPointStructDataObject) user.getGeoPoint(), user.getIpAddress(), user.getReferer(), user.getStatus(), user.getEmailVerifiedTime(), user.getOpenIdVerifiedTime(), user.getAuthenticatedTime());
        }
        String guid = getDAOFactory().getUserDAO().createUser(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateUser(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String aeryId, String sessionId, String ancestorGuid, FullNameStruct name, String usercode, String username, String nickname, String avatar, String email, String openId, GaeUserStruct gaeUser, String entityType, Boolean surrogate, Boolean obsolete, String timeZone, String location, StreetAddressStruct streetAddress, GeoPointStruct geoPoint, String ipAddress, String referer, String status, Long emailVerifiedTime, Long openIdVerifiedTime, Long authenticatedTime) throws BaseException
    {
        GaeAppStructDataObject gaeAppDobj = null;
        if(gaeApp instanceof GaeAppStructBean) {
            gaeAppDobj = ((GaeAppStructBean) gaeApp).toDataObject();            
        } else if(gaeApp instanceof GaeAppStruct) {
            gaeAppDobj = new GaeAppStructDataObject(gaeApp.getGroupId(), gaeApp.getAppId(), gaeApp.getAppDomain(), gaeApp.getNamespace(), gaeApp.getAcl(), gaeApp.getNote());
        } else {
            gaeAppDobj = null;   // ????
        }
        FullNameStructDataObject nameDobj = null;
        if(name instanceof FullNameStructBean) {
            nameDobj = ((FullNameStructBean) name).toDataObject();            
        } else if(name instanceof FullNameStruct) {
            nameDobj = new FullNameStructDataObject(name.getUuid(), name.getDisplayName(), name.getLastName(), name.getFirstName(), name.getMiddleName1(), name.getMiddleName2(), name.getMiddleInitial(), name.getSalutation(), name.getSuffix(), name.getNote());
        } else {
            nameDobj = null;   // ????
        }
        GaeUserStructDataObject gaeUserDobj = null;
        if(gaeUser instanceof GaeUserStructBean) {
            gaeUserDobj = ((GaeUserStructBean) gaeUser).toDataObject();            
        } else if(gaeUser instanceof GaeUserStruct) {
            gaeUserDobj = new GaeUserStructDataObject(gaeUser.getAuthDomain(), gaeUser.getFederatedIdentity(), gaeUser.getNickname(), gaeUser.getUserId(), gaeUser.getEmail(), gaeUser.getNote());
        } else {
            gaeUserDobj = null;   // ????
        }
        StreetAddressStructDataObject streetAddressDobj = null;
        if(streetAddress instanceof StreetAddressStructBean) {
            streetAddressDobj = ((StreetAddressStructBean) streetAddress).toDataObject();            
        } else if(streetAddress instanceof StreetAddressStruct) {
            streetAddressDobj = new StreetAddressStructDataObject(streetAddress.getUuid(), streetAddress.getStreet1(), streetAddress.getStreet2(), streetAddress.getCity(), streetAddress.getCounty(), streetAddress.getPostalCode(), streetAddress.getState(), streetAddress.getProvince(), streetAddress.getCountry(), streetAddress.getCountryName(), streetAddress.getNote());
        } else {
            streetAddressDobj = null;   // ????
        }
        GeoPointStructDataObject geoPointDobj = null;
        if(geoPoint instanceof GeoPointStructBean) {
            geoPointDobj = ((GeoPointStructBean) geoPoint).toDataObject();            
        } else if(geoPoint instanceof GeoPointStruct) {
            geoPointDobj = new GeoPointStructDataObject(geoPoint.getUuid(), geoPoint.getLatitude(), geoPoint.getLongitude(), geoPoint.getAltitude(), geoPoint.isSensorUsed());
        } else {
            geoPointDobj = null;   // ????
        }

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        UserDataObject dataObj = new UserDataObject(guid, managerApp, appAcl, gaeAppDobj, aeryId, sessionId, ancestorGuid, nameDobj, usercode, username, nickname, avatar, email, openId, gaeUserDobj, entityType, surrogate, obsolete, timeZone, location, streetAddressDobj, geoPointDobj, ipAddress, referer, status, emailVerifiedTime, openIdVerifiedTime, authenticatedTime);
        return updateUser(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateUser(User user) throws BaseException
    {
        log.finer("BEGIN");

        // Param user cannot be null.....
        if(user == null || user.getGuid() == null) {
            log.log(Level.INFO, "Param user or its guid is null!");
            throw new BadRequestException("Param user object or its guid is null!");
        }
        UserDataObject dataObj = null;
        if(user instanceof UserDataObject) {
            dataObj = (UserDataObject) user;
        } else if(user instanceof UserBean) {
            dataObj = ((UserBean) user).toDataObject();
        } else {  // if(user instanceof User)
            dataObj = new UserDataObject(user.getGuid(), user.getManagerApp(), user.getAppAcl(), user.getGaeApp(), user.getAeryId(), user.getSessionId(), user.getAncestorGuid(), user.getName(), user.getUsercode(), user.getUsername(), user.getNickname(), user.getAvatar(), user.getEmail(), user.getOpenId(), user.getGaeUser(), user.getEntityType(), user.isSurrogate(), user.isObsolete(), user.getTimeZone(), user.getLocation(), user.getStreetAddress(), user.getGeoPoint(), user.getIpAddress(), user.getReferer(), user.getStatus(), user.getEmailVerifiedTime(), user.getOpenIdVerifiedTime(), user.getAuthenticatedTime());
        }
        Boolean suc = getDAOFactory().getUserDAO().updateUser(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteUser(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getUserDAO().deleteUser(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteUser(User user) throws BaseException
    {
        log.finer("BEGIN");

        // Param user cannot be null.....
        if(user == null || user.getGuid() == null) {
            log.log(Level.INFO, "Param user or its guid is null!");
            throw new BadRequestException("Param user object or its guid is null!");
        }
        UserDataObject dataObj = null;
        if(user instanceof UserDataObject) {
            dataObj = (UserDataObject) user;
        } else if(user instanceof UserBean) {
            dataObj = ((UserBean) user).toDataObject();
        } else {  // if(user instanceof User)
            dataObj = new UserDataObject(user.getGuid(), user.getManagerApp(), user.getAppAcl(), user.getGaeApp(), user.getAeryId(), user.getSessionId(), user.getAncestorGuid(), user.getName(), user.getUsercode(), user.getUsername(), user.getNickname(), user.getAvatar(), user.getEmail(), user.getOpenId(), user.getGaeUser(), user.getEntityType(), user.isSurrogate(), user.isObsolete(), user.getTimeZone(), user.getLocation(), user.getStreetAddress(), user.getGeoPoint(), user.getIpAddress(), user.getReferer(), user.getStatus(), user.getEmailVerifiedTime(), user.getOpenIdVerifiedTime(), user.getAuthenticatedTime());
        }
        Boolean suc = getDAOFactory().getUserDAO().deleteUser(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteUsers(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getUserDAO().deleteUsers(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
