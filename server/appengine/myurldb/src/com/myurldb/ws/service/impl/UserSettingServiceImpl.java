package com.myurldb.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.UserSetting;
import com.myurldb.ws.bean.UserSettingBean;
import com.myurldb.ws.dao.DAOFactory;
import com.myurldb.ws.data.UserSettingDataObject;
import com.myurldb.ws.service.DAOFactoryManager;
import com.myurldb.ws.service.UserSettingService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class UserSettingServiceImpl implements UserSettingService
{
    private static final Logger log = Logger.getLogger(UserSettingServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // UserSetting related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public UserSetting getUserSetting(String guid) throws BaseException
    {
        log.finer("BEGIN");

        UserSettingDataObject dataObj = getDAOFactory().getUserSettingDAO().getUserSetting(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve UserSettingDataObject for guid = " + guid);
            return null;  // ????
        }
        UserSettingBean bean = new UserSettingBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getUserSetting(String guid, String field) throws BaseException
    {
        UserSettingDataObject dataObj = getDAOFactory().getUserSettingDAO().getUserSetting(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve UserSettingDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return dataObj.getUser();
        } else if(field.equals("homePage")) {
            return dataObj.getHomePage();
        } else if(field.equals("selfBio")) {
            return dataObj.getSelfBio();
        } else if(field.equals("userUrlDomain")) {
            return dataObj.getUserUrlDomain();
        } else if(field.equals("userUrlDomainNormalized")) {
            return dataObj.getUserUrlDomainNormalized();
        } else if(field.equals("autoRedirectEnabled")) {
            return dataObj.isAutoRedirectEnabled();
        } else if(field.equals("viewEnabled")) {
            return dataObj.isViewEnabled();
        } else if(field.equals("shareEnabled")) {
            return dataObj.isShareEnabled();
        } else if(field.equals("defaultDomain")) {
            return dataObj.getDefaultDomain();
        } else if(field.equals("defaultTokenType")) {
            return dataObj.getDefaultTokenType();
        } else if(field.equals("defaultRedirectType")) {
            return dataObj.getDefaultRedirectType();
        } else if(field.equals("defaultFlashDuration")) {
            return dataObj.getDefaultFlashDuration();
        } else if(field.equals("defaultAccessType")) {
            return dataObj.getDefaultAccessType();
        } else if(field.equals("defaultViewType")) {
            return dataObj.getDefaultViewType();
        } else if(field.equals("defaultShareType")) {
            return dataObj.getDefaultShareType();
        } else if(field.equals("defaultPassphrase")) {
            return dataObj.getDefaultPassphrase();
        } else if(field.equals("defaultSignature")) {
            return dataObj.getDefaultSignature();
        } else if(field.equals("useSignature")) {
            return dataObj.isUseSignature();
        } else if(field.equals("defaultEmblem")) {
            return dataObj.getDefaultEmblem();
        } else if(field.equals("useEmblem")) {
            return dataObj.isUseEmblem();
        } else if(field.equals("defaultBackground")) {
            return dataObj.getDefaultBackground();
        } else if(field.equals("useBackground")) {
            return dataObj.isUseBackground();
        } else if(field.equals("sponsorUrl")) {
            return dataObj.getSponsorUrl();
        } else if(field.equals("sponsorBanner")) {
            return dataObj.getSponsorBanner();
        } else if(field.equals("sponsorNote")) {
            return dataObj.getSponsorNote();
        } else if(field.equals("useSponsor")) {
            return dataObj.isUseSponsor();
        } else if(field.equals("extraParams")) {
            return dataObj.getExtraParams();
        } else if(field.equals("expirationDuration")) {
            return dataObj.getExpirationDuration();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<UserSetting> getUserSettings(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<UserSetting> list = new ArrayList<UserSetting>();
        List<UserSettingDataObject> dataObjs = getDAOFactory().getUserSettingDAO().getUserSettings(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve UserSettingDataObject list.");
        } else {
            Iterator<UserSettingDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                UserSettingDataObject dataObj = (UserSettingDataObject) it.next();
                list.add(new UserSettingBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<UserSetting> getAllUserSettings() throws BaseException
    {
        return getAllUserSettings(null, null, null);
    }

    @Override
    public List<UserSetting> getAllUserSettings(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<UserSetting> list = new ArrayList<UserSetting>();
        List<UserSettingDataObject> dataObjs = getDAOFactory().getUserSettingDAO().getAllUserSettings(ordering, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve UserSettingDataObject list.");
        } else {
            Iterator<UserSettingDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                UserSettingDataObject dataObj = (UserSettingDataObject) it.next();
                list.add(new UserSettingBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllUserSettingKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        List<String> keys = getDAOFactory().getUserSettingDAO().getAllUserSettingKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve UserSetting key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<UserSetting> findUserSettings(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findUserSettings(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<UserSetting> findUserSettings(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("UserSettingServiceImpl.findUserSettings(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<UserSetting> list = new ArrayList<UserSetting>();
        List<UserSettingDataObject> dataObjs = getDAOFactory().getUserSettingDAO().findUserSettings(filter, ordering, params, values, grouping, unique, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find userSettings for the given criterion.");
        } else {
            Iterator<UserSettingDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                UserSettingDataObject dataObj = (UserSettingDataObject) it.next();
                list.add(new UserSettingBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findUserSettingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("UserSettingServiceImpl.findUserSettingKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = getDAOFactory().getUserSettingDAO().findUserSettingKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find UserSetting keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("UserSettingServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getUserSettingDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createUserSetting(String user, String homePage, String selfBio, String userUrlDomain, String userUrlDomainNormalized, Boolean autoRedirectEnabled, Boolean viewEnabled, Boolean shareEnabled, String defaultDomain, String defaultTokenType, String defaultRedirectType, Long defaultFlashDuration, String defaultAccessType, String defaultViewType, String defaultShareType, String defaultPassphrase, String defaultSignature, Boolean useSignature, String defaultEmblem, Boolean useEmblem, String defaultBackground, Boolean useBackground, String sponsorUrl, String sponsorBanner, String sponsorNote, Boolean useSponsor, String extraParams, Long expirationDuration) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        UserSettingDataObject dataObj = new UserSettingDataObject(null, user, homePage, selfBio, userUrlDomain, userUrlDomainNormalized, autoRedirectEnabled, viewEnabled, shareEnabled, defaultDomain, defaultTokenType, defaultRedirectType, defaultFlashDuration, defaultAccessType, defaultViewType, defaultShareType, defaultPassphrase, defaultSignature, useSignature, defaultEmblem, useEmblem, defaultBackground, useBackground, sponsorUrl, sponsorBanner, sponsorNote, useSponsor, extraParams, expirationDuration);
        return createUserSetting(dataObj);
    }

    @Override
    public String createUserSetting(UserSetting userSetting) throws BaseException
    {
        log.finer("BEGIN");

        // Param userSetting cannot be null.....
        if(userSetting == null) {
            log.log(Level.INFO, "Param userSetting is null!");
            throw new BadRequestException("Param userSetting object is null!");
        }
        UserSettingDataObject dataObj = null;
        if(userSetting instanceof UserSettingDataObject) {
            dataObj = (UserSettingDataObject) userSetting;
        } else if(userSetting instanceof UserSettingBean) {
            dataObj = ((UserSettingBean) userSetting).toDataObject();
        } else {  // if(userSetting instanceof UserSetting)
            //dataObj = new UserSettingDataObject(null, userSetting.getUser(), userSetting.getHomePage(), userSetting.getSelfBio(), userSetting.getUserUrlDomain(), userSetting.getUserUrlDomainNormalized(), userSetting.isAutoRedirectEnabled(), userSetting.isViewEnabled(), userSetting.isShareEnabled(), userSetting.getDefaultDomain(), userSetting.getDefaultTokenType(), userSetting.getDefaultRedirectType(), userSetting.getDefaultFlashDuration(), userSetting.getDefaultAccessType(), userSetting.getDefaultViewType(), userSetting.getDefaultShareType(), userSetting.getDefaultPassphrase(), userSetting.getDefaultSignature(), userSetting.isUseSignature(), userSetting.getDefaultEmblem(), userSetting.isUseEmblem(), userSetting.getDefaultBackground(), userSetting.isUseBackground(), userSetting.getSponsorUrl(), userSetting.getSponsorBanner(), userSetting.getSponsorNote(), userSetting.isUseSponsor(), userSetting.getExtraParams(), userSetting.getExpirationDuration());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new UserSettingDataObject(userSetting.getGuid(), userSetting.getUser(), userSetting.getHomePage(), userSetting.getSelfBio(), userSetting.getUserUrlDomain(), userSetting.getUserUrlDomainNormalized(), userSetting.isAutoRedirectEnabled(), userSetting.isViewEnabled(), userSetting.isShareEnabled(), userSetting.getDefaultDomain(), userSetting.getDefaultTokenType(), userSetting.getDefaultRedirectType(), userSetting.getDefaultFlashDuration(), userSetting.getDefaultAccessType(), userSetting.getDefaultViewType(), userSetting.getDefaultShareType(), userSetting.getDefaultPassphrase(), userSetting.getDefaultSignature(), userSetting.isUseSignature(), userSetting.getDefaultEmblem(), userSetting.isUseEmblem(), userSetting.getDefaultBackground(), userSetting.isUseBackground(), userSetting.getSponsorUrl(), userSetting.getSponsorBanner(), userSetting.getSponsorNote(), userSetting.isUseSponsor(), userSetting.getExtraParams(), userSetting.getExpirationDuration());
        }
        String guid = getDAOFactory().getUserSettingDAO().createUserSetting(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateUserSetting(String guid, String user, String homePage, String selfBio, String userUrlDomain, String userUrlDomainNormalized, Boolean autoRedirectEnabled, Boolean viewEnabled, Boolean shareEnabled, String defaultDomain, String defaultTokenType, String defaultRedirectType, Long defaultFlashDuration, String defaultAccessType, String defaultViewType, String defaultShareType, String defaultPassphrase, String defaultSignature, Boolean useSignature, String defaultEmblem, Boolean useEmblem, String defaultBackground, Boolean useBackground, String sponsorUrl, String sponsorBanner, String sponsorNote, Boolean useSponsor, String extraParams, Long expirationDuration) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        UserSettingDataObject dataObj = new UserSettingDataObject(guid, user, homePage, selfBio, userUrlDomain, userUrlDomainNormalized, autoRedirectEnabled, viewEnabled, shareEnabled, defaultDomain, defaultTokenType, defaultRedirectType, defaultFlashDuration, defaultAccessType, defaultViewType, defaultShareType, defaultPassphrase, defaultSignature, useSignature, defaultEmblem, useEmblem, defaultBackground, useBackground, sponsorUrl, sponsorBanner, sponsorNote, useSponsor, extraParams, expirationDuration);
        return updateUserSetting(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateUserSetting(UserSetting userSetting) throws BaseException
    {
        log.finer("BEGIN");

        // Param userSetting cannot be null.....
        if(userSetting == null || userSetting.getGuid() == null) {
            log.log(Level.INFO, "Param userSetting or its guid is null!");
            throw new BadRequestException("Param userSetting object or its guid is null!");
        }
        UserSettingDataObject dataObj = null;
        if(userSetting instanceof UserSettingDataObject) {
            dataObj = (UserSettingDataObject) userSetting;
        } else if(userSetting instanceof UserSettingBean) {
            dataObj = ((UserSettingBean) userSetting).toDataObject();
        } else {  // if(userSetting instanceof UserSetting)
            dataObj = new UserSettingDataObject(userSetting.getGuid(), userSetting.getUser(), userSetting.getHomePage(), userSetting.getSelfBio(), userSetting.getUserUrlDomain(), userSetting.getUserUrlDomainNormalized(), userSetting.isAutoRedirectEnabled(), userSetting.isViewEnabled(), userSetting.isShareEnabled(), userSetting.getDefaultDomain(), userSetting.getDefaultTokenType(), userSetting.getDefaultRedirectType(), userSetting.getDefaultFlashDuration(), userSetting.getDefaultAccessType(), userSetting.getDefaultViewType(), userSetting.getDefaultShareType(), userSetting.getDefaultPassphrase(), userSetting.getDefaultSignature(), userSetting.isUseSignature(), userSetting.getDefaultEmblem(), userSetting.isUseEmblem(), userSetting.getDefaultBackground(), userSetting.isUseBackground(), userSetting.getSponsorUrl(), userSetting.getSponsorBanner(), userSetting.getSponsorNote(), userSetting.isUseSponsor(), userSetting.getExtraParams(), userSetting.getExpirationDuration());
        }
        Boolean suc = getDAOFactory().getUserSettingDAO().updateUserSetting(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteUserSetting(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getUserSettingDAO().deleteUserSetting(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteUserSetting(UserSetting userSetting) throws BaseException
    {
        log.finer("BEGIN");

        // Param userSetting cannot be null.....
        if(userSetting == null || userSetting.getGuid() == null) {
            log.log(Level.INFO, "Param userSetting or its guid is null!");
            throw new BadRequestException("Param userSetting object or its guid is null!");
        }
        UserSettingDataObject dataObj = null;
        if(userSetting instanceof UserSettingDataObject) {
            dataObj = (UserSettingDataObject) userSetting;
        } else if(userSetting instanceof UserSettingBean) {
            dataObj = ((UserSettingBean) userSetting).toDataObject();
        } else {  // if(userSetting instanceof UserSetting)
            dataObj = new UserSettingDataObject(userSetting.getGuid(), userSetting.getUser(), userSetting.getHomePage(), userSetting.getSelfBio(), userSetting.getUserUrlDomain(), userSetting.getUserUrlDomainNormalized(), userSetting.isAutoRedirectEnabled(), userSetting.isViewEnabled(), userSetting.isShareEnabled(), userSetting.getDefaultDomain(), userSetting.getDefaultTokenType(), userSetting.getDefaultRedirectType(), userSetting.getDefaultFlashDuration(), userSetting.getDefaultAccessType(), userSetting.getDefaultViewType(), userSetting.getDefaultShareType(), userSetting.getDefaultPassphrase(), userSetting.getDefaultSignature(), userSetting.isUseSignature(), userSetting.getDefaultEmblem(), userSetting.isUseEmblem(), userSetting.getDefaultBackground(), userSetting.isUseBackground(), userSetting.getSponsorUrl(), userSetting.getSponsorBanner(), userSetting.getSponsorNote(), userSetting.isUseSponsor(), userSetting.getExtraParams(), userSetting.getExpirationDuration());
        }
        Boolean suc = getDAOFactory().getUserSettingDAO().deleteUserSetting(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteUserSettings(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getUserSettingDAO().deleteUserSettings(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
