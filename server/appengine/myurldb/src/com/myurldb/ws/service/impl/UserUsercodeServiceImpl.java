package com.myurldb.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.UserUsercode;
import com.myurldb.ws.bean.UserUsercodeBean;
import com.myurldb.ws.dao.DAOFactory;
import com.myurldb.ws.data.UserUsercodeDataObject;
import com.myurldb.ws.service.DAOFactoryManager;
import com.myurldb.ws.service.UserUsercodeService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class UserUsercodeServiceImpl implements UserUsercodeService
{
    private static final Logger log = Logger.getLogger(UserUsercodeServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // UserUsercode related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public UserUsercode getUserUsercode(String guid) throws BaseException
    {
        log.finer("BEGIN");

        UserUsercodeDataObject dataObj = getDAOFactory().getUserUsercodeDAO().getUserUsercode(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve UserUsercodeDataObject for guid = " + guid);
            return null;  // ????
        }
        UserUsercodeBean bean = new UserUsercodeBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getUserUsercode(String guid, String field) throws BaseException
    {
        UserUsercodeDataObject dataObj = getDAOFactory().getUserUsercodeDAO().getUserUsercode(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve UserUsercodeDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("admin")) {
            return dataObj.getAdmin();
        } else if(field.equals("user")) {
            return dataObj.getUser();
        } else if(field.equals("username")) {
            return dataObj.getUsername();
        } else if(field.equals("usercode")) {
            return dataObj.getUsercode();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<UserUsercode> getUserUsercodes(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<UserUsercode> list = new ArrayList<UserUsercode>();
        List<UserUsercodeDataObject> dataObjs = getDAOFactory().getUserUsercodeDAO().getUserUsercodes(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve UserUsercodeDataObject list.");
        } else {
            Iterator<UserUsercodeDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                UserUsercodeDataObject dataObj = (UserUsercodeDataObject) it.next();
                list.add(new UserUsercodeBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<UserUsercode> getAllUserUsercodes() throws BaseException
    {
        return getAllUserUsercodes(null, null, null);
    }

    @Override
    public List<UserUsercode> getAllUserUsercodes(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<UserUsercode> list = new ArrayList<UserUsercode>();
        List<UserUsercodeDataObject> dataObjs = getDAOFactory().getUserUsercodeDAO().getAllUserUsercodes(ordering, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve UserUsercodeDataObject list.");
        } else {
            Iterator<UserUsercodeDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                UserUsercodeDataObject dataObj = (UserUsercodeDataObject) it.next();
                list.add(new UserUsercodeBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllUserUsercodeKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        List<String> keys = getDAOFactory().getUserUsercodeDAO().getAllUserUsercodeKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve UserUsercode key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<UserUsercode> findUserUsercodes(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findUserUsercodes(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<UserUsercode> findUserUsercodes(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("UserUsercodeServiceImpl.findUserUsercodes(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<UserUsercode> list = new ArrayList<UserUsercode>();
        List<UserUsercodeDataObject> dataObjs = getDAOFactory().getUserUsercodeDAO().findUserUsercodes(filter, ordering, params, values, grouping, unique, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find userUsercodes for the given criterion.");
        } else {
            Iterator<UserUsercodeDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                UserUsercodeDataObject dataObj = (UserUsercodeDataObject) it.next();
                list.add(new UserUsercodeBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findUserUsercodeKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("UserUsercodeServiceImpl.findUserUsercodeKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = getDAOFactory().getUserUsercodeDAO().findUserUsercodeKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find UserUsercode keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("UserUsercodeServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getUserUsercodeDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createUserUsercode(String admin, String user, String username, String usercode, String status) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        UserUsercodeDataObject dataObj = new UserUsercodeDataObject(null, admin, user, username, usercode, status);
        return createUserUsercode(dataObj);
    }

    @Override
    public String createUserUsercode(UserUsercode userUsercode) throws BaseException
    {
        log.finer("BEGIN");

        // Param userUsercode cannot be null.....
        if(userUsercode == null) {
            log.log(Level.INFO, "Param userUsercode is null!");
            throw new BadRequestException("Param userUsercode object is null!");
        }
        UserUsercodeDataObject dataObj = null;
        if(userUsercode instanceof UserUsercodeDataObject) {
            dataObj = (UserUsercodeDataObject) userUsercode;
        } else if(userUsercode instanceof UserUsercodeBean) {
            dataObj = ((UserUsercodeBean) userUsercode).toDataObject();
        } else {  // if(userUsercode instanceof UserUsercode)
            //dataObj = new UserUsercodeDataObject(null, userUsercode.getAdmin(), userUsercode.getUser(), userUsercode.getUsername(), userUsercode.getUsercode(), userUsercode.getStatus());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new UserUsercodeDataObject(userUsercode.getGuid(), userUsercode.getAdmin(), userUsercode.getUser(), userUsercode.getUsername(), userUsercode.getUsercode(), userUsercode.getStatus());
        }
        String guid = getDAOFactory().getUserUsercodeDAO().createUserUsercode(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateUserUsercode(String guid, String admin, String user, String username, String usercode, String status) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        UserUsercodeDataObject dataObj = new UserUsercodeDataObject(guid, admin, user, username, usercode, status);
        return updateUserUsercode(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateUserUsercode(UserUsercode userUsercode) throws BaseException
    {
        log.finer("BEGIN");

        // Param userUsercode cannot be null.....
        if(userUsercode == null || userUsercode.getGuid() == null) {
            log.log(Level.INFO, "Param userUsercode or its guid is null!");
            throw new BadRequestException("Param userUsercode object or its guid is null!");
        }
        UserUsercodeDataObject dataObj = null;
        if(userUsercode instanceof UserUsercodeDataObject) {
            dataObj = (UserUsercodeDataObject) userUsercode;
        } else if(userUsercode instanceof UserUsercodeBean) {
            dataObj = ((UserUsercodeBean) userUsercode).toDataObject();
        } else {  // if(userUsercode instanceof UserUsercode)
            dataObj = new UserUsercodeDataObject(userUsercode.getGuid(), userUsercode.getAdmin(), userUsercode.getUser(), userUsercode.getUsername(), userUsercode.getUsercode(), userUsercode.getStatus());
        }
        Boolean suc = getDAOFactory().getUserUsercodeDAO().updateUserUsercode(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteUserUsercode(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getUserUsercodeDAO().deleteUserUsercode(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteUserUsercode(UserUsercode userUsercode) throws BaseException
    {
        log.finer("BEGIN");

        // Param userUsercode cannot be null.....
        if(userUsercode == null || userUsercode.getGuid() == null) {
            log.log(Level.INFO, "Param userUsercode or its guid is null!");
            throw new BadRequestException("Param userUsercode object or its guid is null!");
        }
        UserUsercodeDataObject dataObj = null;
        if(userUsercode instanceof UserUsercodeDataObject) {
            dataObj = (UserUsercodeDataObject) userUsercode;
        } else if(userUsercode instanceof UserUsercodeBean) {
            dataObj = ((UserUsercodeBean) userUsercode).toDataObject();
        } else {  // if(userUsercode instanceof UserUsercode)
            dataObj = new UserUsercodeDataObject(userUsercode.getGuid(), userUsercode.getAdmin(), userUsercode.getUser(), userUsercode.getUsername(), userUsercode.getUsercode(), userUsercode.getStatus());
        }
        Boolean suc = getDAOFactory().getUserUsercodeDAO().deleteUserUsercode(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteUserUsercodes(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getUserUsercodeDAO().deleteUserUsercodes(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
