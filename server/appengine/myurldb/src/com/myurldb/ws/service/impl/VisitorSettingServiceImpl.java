package com.myurldb.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.BadRequestException;
import com.myurldb.ws.VisitorSetting;
import com.myurldb.ws.bean.VisitorSettingBean;
import com.myurldb.ws.dao.DAOFactory;
import com.myurldb.ws.data.VisitorSettingDataObject;
import com.myurldb.ws.service.DAOFactoryManager;
import com.myurldb.ws.service.VisitorSettingService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class VisitorSettingServiceImpl implements VisitorSettingService
{
    private static final Logger log = Logger.getLogger(VisitorSettingServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // VisitorSetting related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public VisitorSetting getVisitorSetting(String guid) throws BaseException
    {
        log.finer("BEGIN");

        VisitorSettingDataObject dataObj = getDAOFactory().getVisitorSettingDAO().getVisitorSetting(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve VisitorSettingDataObject for guid = " + guid);
            return null;  // ????
        }
        VisitorSettingBean bean = new VisitorSettingBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getVisitorSetting(String guid, String field) throws BaseException
    {
        VisitorSettingDataObject dataObj = getDAOFactory().getVisitorSettingDAO().getVisitorSetting(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve VisitorSettingDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return dataObj.getUser();
        } else if(field.equals("redirectType")) {
            return dataObj.getRedirectType();
        } else if(field.equals("flashDuration")) {
            return dataObj.getFlashDuration();
        } else if(field.equals("privacyType")) {
            return dataObj.getPrivacyType();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<VisitorSetting> getVisitorSettings(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<VisitorSetting> list = new ArrayList<VisitorSetting>();
        List<VisitorSettingDataObject> dataObjs = getDAOFactory().getVisitorSettingDAO().getVisitorSettings(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve VisitorSettingDataObject list.");
        } else {
            Iterator<VisitorSettingDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                VisitorSettingDataObject dataObj = (VisitorSettingDataObject) it.next();
                list.add(new VisitorSettingBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<VisitorSetting> getAllVisitorSettings() throws BaseException
    {
        return getAllVisitorSettings(null, null, null);
    }

    @Override
    public List<VisitorSetting> getAllVisitorSettings(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<VisitorSetting> list = new ArrayList<VisitorSetting>();
        List<VisitorSettingDataObject> dataObjs = getDAOFactory().getVisitorSettingDAO().getAllVisitorSettings(ordering, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve VisitorSettingDataObject list.");
        } else {
            Iterator<VisitorSettingDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                VisitorSettingDataObject dataObj = (VisitorSettingDataObject) it.next();
                list.add(new VisitorSettingBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllVisitorSettingKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        List<String> keys = getDAOFactory().getVisitorSettingDAO().getAllVisitorSettingKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve VisitorSetting key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<VisitorSetting> findVisitorSettings(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findVisitorSettings(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<VisitorSetting> findVisitorSettings(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("VisitorSettingServiceImpl.findVisitorSettings(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<VisitorSetting> list = new ArrayList<VisitorSetting>();
        List<VisitorSettingDataObject> dataObjs = getDAOFactory().getVisitorSettingDAO().findVisitorSettings(filter, ordering, params, values, grouping, unique, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find visitorSettings for the given criterion.");
        } else {
            Iterator<VisitorSettingDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                VisitorSettingDataObject dataObj = (VisitorSettingDataObject) it.next();
                list.add(new VisitorSettingBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findVisitorSettingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("VisitorSettingServiceImpl.findVisitorSettingKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = getDAOFactory().getVisitorSettingDAO().findVisitorSettingKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find VisitorSetting keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("VisitorSettingServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getVisitorSettingDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createVisitorSetting(String user, String redirectType, Long flashDuration, String privacyType) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        VisitorSettingDataObject dataObj = new VisitorSettingDataObject(null, user, redirectType, flashDuration, privacyType);
        return createVisitorSetting(dataObj);
    }

    @Override
    public String createVisitorSetting(VisitorSetting visitorSetting) throws BaseException
    {
        log.finer("BEGIN");

        // Param visitorSetting cannot be null.....
        if(visitorSetting == null) {
            log.log(Level.INFO, "Param visitorSetting is null!");
            throw new BadRequestException("Param visitorSetting object is null!");
        }
        VisitorSettingDataObject dataObj = null;
        if(visitorSetting instanceof VisitorSettingDataObject) {
            dataObj = (VisitorSettingDataObject) visitorSetting;
        } else if(visitorSetting instanceof VisitorSettingBean) {
            dataObj = ((VisitorSettingBean) visitorSetting).toDataObject();
        } else {  // if(visitorSetting instanceof VisitorSetting)
            //dataObj = new VisitorSettingDataObject(null, visitorSetting.getUser(), visitorSetting.getRedirectType(), visitorSetting.getFlashDuration(), visitorSetting.getPrivacyType());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new VisitorSettingDataObject(visitorSetting.getGuid(), visitorSetting.getUser(), visitorSetting.getRedirectType(), visitorSetting.getFlashDuration(), visitorSetting.getPrivacyType());
        }
        String guid = getDAOFactory().getVisitorSettingDAO().createVisitorSetting(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateVisitorSetting(String guid, String user, String redirectType, Long flashDuration, String privacyType) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        VisitorSettingDataObject dataObj = new VisitorSettingDataObject(guid, user, redirectType, flashDuration, privacyType);
        return updateVisitorSetting(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateVisitorSetting(VisitorSetting visitorSetting) throws BaseException
    {
        log.finer("BEGIN");

        // Param visitorSetting cannot be null.....
        if(visitorSetting == null || visitorSetting.getGuid() == null) {
            log.log(Level.INFO, "Param visitorSetting or its guid is null!");
            throw new BadRequestException("Param visitorSetting object or its guid is null!");
        }
        VisitorSettingDataObject dataObj = null;
        if(visitorSetting instanceof VisitorSettingDataObject) {
            dataObj = (VisitorSettingDataObject) visitorSetting;
        } else if(visitorSetting instanceof VisitorSettingBean) {
            dataObj = ((VisitorSettingBean) visitorSetting).toDataObject();
        } else {  // if(visitorSetting instanceof VisitorSetting)
            dataObj = new VisitorSettingDataObject(visitorSetting.getGuid(), visitorSetting.getUser(), visitorSetting.getRedirectType(), visitorSetting.getFlashDuration(), visitorSetting.getPrivacyType());
        }
        Boolean suc = getDAOFactory().getVisitorSettingDAO().updateVisitorSetting(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteVisitorSetting(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getVisitorSettingDAO().deleteVisitorSetting(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteVisitorSetting(VisitorSetting visitorSetting) throws BaseException
    {
        log.finer("BEGIN");

        // Param visitorSetting cannot be null.....
        if(visitorSetting == null || visitorSetting.getGuid() == null) {
            log.log(Level.INFO, "Param visitorSetting or its guid is null!");
            throw new BadRequestException("Param visitorSetting object or its guid is null!");
        }
        VisitorSettingDataObject dataObj = null;
        if(visitorSetting instanceof VisitorSettingDataObject) {
            dataObj = (VisitorSettingDataObject) visitorSetting;
        } else if(visitorSetting instanceof VisitorSettingBean) {
            dataObj = ((VisitorSettingBean) visitorSetting).toDataObject();
        } else {  // if(visitorSetting instanceof VisitorSetting)
            dataObj = new VisitorSettingDataObject(visitorSetting.getGuid(), visitorSetting.getUser(), visitorSetting.getRedirectType(), visitorSetting.getFlashDuration(), visitorSetting.getPrivacyType());
        }
        Boolean suc = getDAOFactory().getVisitorSettingDAO().deleteVisitorSetting(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteVisitorSettings(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getVisitorSettingDAO().deleteVisitorSettings(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
