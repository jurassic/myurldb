package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.AbuseTag;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "abuseTags")
@XmlType(propOrder = {"abuseTag"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class AbuseTagListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(AbuseTagListStub.class.getName());

    private List<AbuseTagStub> abuseTags = null;

    public AbuseTagListStub()
    {
        this(new ArrayList<AbuseTagStub>());
    }
    public AbuseTagListStub(List<AbuseTagStub> abuseTags)
    {
        this.abuseTags = abuseTags;
    }

    public boolean isEmpty()
    {
        if(abuseTags == null) {
            return true;
        } else {
            return abuseTags.isEmpty();
        }
    }
    public int getSize()
    {
        if(abuseTags == null) {
            return 0;
        } else {
            return abuseTags.size();
        }
    }

    @XmlElement(name = "abuseTag")
    public List<AbuseTagStub> getAbuseTag()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<AbuseTagStub> getList()
    {
        return abuseTags;
    }
    public void setList(List<AbuseTagStub> abuseTags)
    {
        this.abuseTags = abuseTags;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<AbuseTagStub> it = this.abuseTags.iterator();
        while(it.hasNext()) {
            AbuseTagStub abuseTag = it.next();
            sb.append(abuseTag.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static AbuseTagListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of AbuseTagListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write AbuseTagListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write AbuseTagListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write AbuseTagListStub object as a string.", e);
        }
        
        return null;
    }
    public static AbuseTagListStub fromJsonString(String jsonStr)
    {
        try {
            AbuseTagListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, AbuseTagListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into AbuseTagListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into AbuseTagListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into AbuseTagListStub object.", e);
        }
        
        return null;
    }

}
