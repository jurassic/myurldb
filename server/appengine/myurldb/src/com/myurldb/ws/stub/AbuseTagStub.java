package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.AbuseTag;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "abuseTag")
@XmlType(propOrder = {"guid", "shortLink", "shortUrl", "longUrl", "longUrlHash", "user", "ipAddress", "tag", "comment", "status", "reviewer", "action", "note", "pastActions", "reviewedTime", "actionTime", "createdTime", "modifiedTime"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AbuseTagStub implements AbuseTag, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(AbuseTagStub.class.getName());

    private String guid;
    private String shortLink;
    private String shortUrl;
    private String longUrl;
    private String longUrlHash;
    private String user;
    private String ipAddress;
    private Integer tag;
    private String comment;
    private String status;
    private String reviewer;
    private String action;
    private String note;
    private String pastActions;
    private Long reviewedTime;
    private Long actionTime;
    private Long createdTime;
    private Long modifiedTime;

    public AbuseTagStub()
    {
        this(null);
    }
    public AbuseTagStub(AbuseTag bean)
    {
        if(bean != null) {
            this.guid = bean.getGuid();
            this.shortLink = bean.getShortLink();
            this.shortUrl = bean.getShortUrl();
            this.longUrl = bean.getLongUrl();
            this.longUrlHash = bean.getLongUrlHash();
            this.user = bean.getUser();
            this.ipAddress = bean.getIpAddress();
            this.tag = bean.getTag();
            this.comment = bean.getComment();
            this.status = bean.getStatus();
            this.reviewer = bean.getReviewer();
            this.action = bean.getAction();
            this.note = bean.getNote();
            this.pastActions = bean.getPastActions();
            this.reviewedTime = bean.getReviewedTime();
            this.actionTime = bean.getActionTime();
            this.createdTime = bean.getCreatedTime();
            this.modifiedTime = bean.getModifiedTime();
        }
    }


    @XmlElement
    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    @XmlElement
    public String getShortLink()
    {
        return this.shortLink;
    }
    public void setShortLink(String shortLink)
    {
        this.shortLink = shortLink;
    }

    @XmlElement
    public String getShortUrl()
    {
        return this.shortUrl;
    }
    public void setShortUrl(String shortUrl)
    {
        this.shortUrl = shortUrl;
    }

    @XmlElement
    public String getLongUrl()
    {
        return this.longUrl;
    }
    public void setLongUrl(String longUrl)
    {
        this.longUrl = longUrl;
    }

    @XmlElement
    public String getLongUrlHash()
    {
        return this.longUrlHash;
    }
    public void setLongUrlHash(String longUrlHash)
    {
        this.longUrlHash = longUrlHash;
    }

    @XmlElement
    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    @XmlElement
    public String getIpAddress()
    {
        return this.ipAddress;
    }
    public void setIpAddress(String ipAddress)
    {
        this.ipAddress = ipAddress;
    }

    @XmlElement
    public Integer getTag()
    {
        return this.tag;
    }
    public void setTag(Integer tag)
    {
        this.tag = tag;
    }

    @XmlElement
    public String getComment()
    {
        return this.comment;
    }
    public void setComment(String comment)
    {
        this.comment = comment;
    }

    @XmlElement
    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    @XmlElement
    public String getReviewer()
    {
        return this.reviewer;
    }
    public void setReviewer(String reviewer)
    {
        this.reviewer = reviewer;
    }

    @XmlElement
    public String getAction()
    {
        return this.action;
    }
    public void setAction(String action)
    {
        this.action = action;
    }

    @XmlElement
    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    @XmlElement
    public String getPastActions()
    {
        return this.pastActions;
    }
    public void setPastActions(String pastActions)
    {
        this.pastActions = pastActions;
    }

    @XmlElement
    public Long getReviewedTime()
    {
        return this.reviewedTime;
    }
    public void setReviewedTime(Long reviewedTime)
    {
        this.reviewedTime = reviewedTime;
    }

    @XmlElement
    public Long getActionTime()
    {
        return this.actionTime;
    }
    public void setActionTime(Long actionTime)
    {
        this.actionTime = actionTime;
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("guid", this.guid);
        dataMap.put("shortLink", this.shortLink);
        dataMap.put("shortUrl", this.shortUrl);
        dataMap.put("longUrl", this.longUrl);
        dataMap.put("longUrlHash", this.longUrlHash);
        dataMap.put("user", this.user);
        dataMap.put("ipAddress", this.ipAddress);
        dataMap.put("tag", this.tag);
        dataMap.put("comment", this.comment);
        dataMap.put("status", this.status);
        dataMap.put("reviewer", this.reviewer);
        dataMap.put("action", this.action);
        dataMap.put("note", this.note);
        dataMap.put("pastActions", this.pastActions);
        dataMap.put("reviewedTime", this.reviewedTime);
        dataMap.put("actionTime", this.actionTime);
        dataMap.put("createdTime", this.createdTime);
        dataMap.put("modifiedTime", this.modifiedTime);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = shortLink == null ? 0 : shortLink.hashCode();
        _hash = 31 * _hash + delta;
        delta = shortUrl == null ? 0 : shortUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = longUrl == null ? 0 : longUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = longUrlHash == null ? 0 : longUrlHash.hashCode();
        _hash = 31 * _hash + delta;
        delta = user == null ? 0 : user.hashCode();
        _hash = 31 * _hash + delta;
        delta = ipAddress == null ? 0 : ipAddress.hashCode();
        _hash = 31 * _hash + delta;
        delta = tag == null ? 0 : tag.hashCode();
        _hash = 31 * _hash + delta;
        delta = comment == null ? 0 : comment.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = reviewer == null ? 0 : reviewer.hashCode();
        _hash = 31 * _hash + delta;
        delta = action == null ? 0 : action.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        delta = pastActions == null ? 0 : pastActions.hashCode();
        _hash = 31 * _hash + delta;
        delta = reviewedTime == null ? 0 : reviewedTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = actionTime == null ? 0 : actionTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = createdTime == null ? 0 : createdTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static AbuseTagStub convertBeanToStub(AbuseTag bean)
    {
        AbuseTagStub stub = null;
        if(bean instanceof AbuseTagStub) {
            stub = (AbuseTagStub) bean;
        } else {
            if(bean != null) {
                stub = new AbuseTagStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static AbuseTagStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of AbuseTagStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write AbuseTagStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write AbuseTagStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write AbuseTagStub object as a string.", e);
        }
        
        return null;
    }
    public static AbuseTagStub fromJsonString(String jsonStr)
    {
        try {
            AbuseTagStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, AbuseTagStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into AbuseTagStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into AbuseTagStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into AbuseTagStub object.", e);
        }
        
        return null;
    }

}
