package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.AlbumShortLink;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "albumShortLinks")
@XmlType(propOrder = {"albumShortLink"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class AlbumShortLinkListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(AlbumShortLinkListStub.class.getName());

    private List<AlbumShortLinkStub> albumShortLinks = null;

    public AlbumShortLinkListStub()
    {
        this(new ArrayList<AlbumShortLinkStub>());
    }
    public AlbumShortLinkListStub(List<AlbumShortLinkStub> albumShortLinks)
    {
        this.albumShortLinks = albumShortLinks;
    }

    public boolean isEmpty()
    {
        if(albumShortLinks == null) {
            return true;
        } else {
            return albumShortLinks.isEmpty();
        }
    }
    public int getSize()
    {
        if(albumShortLinks == null) {
            return 0;
        } else {
            return albumShortLinks.size();
        }
    }

    @XmlElement(name = "albumShortLink")
    public List<AlbumShortLinkStub> getAlbumShortLink()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<AlbumShortLinkStub> getList()
    {
        return albumShortLinks;
    }
    public void setList(List<AlbumShortLinkStub> albumShortLinks)
    {
        this.albumShortLinks = albumShortLinks;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<AlbumShortLinkStub> it = this.albumShortLinks.iterator();
        while(it.hasNext()) {
            AlbumShortLinkStub albumShortLink = it.next();
            sb.append(albumShortLink.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static AlbumShortLinkListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of AlbumShortLinkListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write AlbumShortLinkListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write AlbumShortLinkListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write AlbumShortLinkListStub object as a string.", e);
        }
        
        return null;
    }
    public static AlbumShortLinkListStub fromJsonString(String jsonStr)
    {
        try {
            AlbumShortLinkListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, AlbumShortLinkListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into AlbumShortLinkListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into AlbumShortLinkListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into AlbumShortLinkListStub object.", e);
        }
        
        return null;
    }

}
