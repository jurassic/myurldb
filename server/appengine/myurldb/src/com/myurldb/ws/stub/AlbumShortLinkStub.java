package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.AlbumShortLink;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "albumShortLink")
@XmlType(propOrder = {"guid", "user", "linkAlbum", "shortLink", "shortUrl", "longUrl", "note", "status", "createdTime", "modifiedTime"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AlbumShortLinkStub implements AlbumShortLink, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(AlbumShortLinkStub.class.getName());

    private String guid;
    private String user;
    private String linkAlbum;
    private String shortLink;
    private String shortUrl;
    private String longUrl;
    private String note;
    private String status;
    private Long createdTime;
    private Long modifiedTime;

    public AlbumShortLinkStub()
    {
        this(null);
    }
    public AlbumShortLinkStub(AlbumShortLink bean)
    {
        if(bean != null) {
            this.guid = bean.getGuid();
            this.user = bean.getUser();
            this.linkAlbum = bean.getLinkAlbum();
            this.shortLink = bean.getShortLink();
            this.shortUrl = bean.getShortUrl();
            this.longUrl = bean.getLongUrl();
            this.note = bean.getNote();
            this.status = bean.getStatus();
            this.createdTime = bean.getCreatedTime();
            this.modifiedTime = bean.getModifiedTime();
        }
    }


    @XmlElement
    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    @XmlElement
    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    @XmlElement
    public String getLinkAlbum()
    {
        return this.linkAlbum;
    }
    public void setLinkAlbum(String linkAlbum)
    {
        this.linkAlbum = linkAlbum;
    }

    @XmlElement
    public String getShortLink()
    {
        return this.shortLink;
    }
    public void setShortLink(String shortLink)
    {
        this.shortLink = shortLink;
    }

    @XmlElement
    public String getShortUrl()
    {
        return this.shortUrl;
    }
    public void setShortUrl(String shortUrl)
    {
        this.shortUrl = shortUrl;
    }

    @XmlElement
    public String getLongUrl()
    {
        return this.longUrl;
    }
    public void setLongUrl(String longUrl)
    {
        this.longUrl = longUrl;
    }

    @XmlElement
    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    @XmlElement
    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("guid", this.guid);
        dataMap.put("user", this.user);
        dataMap.put("linkAlbum", this.linkAlbum);
        dataMap.put("shortLink", this.shortLink);
        dataMap.put("shortUrl", this.shortUrl);
        dataMap.put("longUrl", this.longUrl);
        dataMap.put("note", this.note);
        dataMap.put("status", this.status);
        dataMap.put("createdTime", this.createdTime);
        dataMap.put("modifiedTime", this.modifiedTime);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = user == null ? 0 : user.hashCode();
        _hash = 31 * _hash + delta;
        delta = linkAlbum == null ? 0 : linkAlbum.hashCode();
        _hash = 31 * _hash + delta;
        delta = shortLink == null ? 0 : shortLink.hashCode();
        _hash = 31 * _hash + delta;
        delta = shortUrl == null ? 0 : shortUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = longUrl == null ? 0 : longUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = createdTime == null ? 0 : createdTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static AlbumShortLinkStub convertBeanToStub(AlbumShortLink bean)
    {
        AlbumShortLinkStub stub = null;
        if(bean instanceof AlbumShortLinkStub) {
            stub = (AlbumShortLinkStub) bean;
        } else {
            if(bean != null) {
                stub = new AlbumShortLinkStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static AlbumShortLinkStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of AlbumShortLinkStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write AlbumShortLinkStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write AlbumShortLinkStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write AlbumShortLinkStub object as a string.", e);
        }
        
        return null;
    }
    public static AlbumShortLinkStub fromJsonString(String jsonStr)
    {
        try {
            AlbumShortLinkStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, AlbumShortLinkStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into AlbumShortLinkStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into AlbumShortLinkStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into AlbumShortLinkStub object.", e);
        }
        
        return null;
    }

}
