package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.AppBrandStruct;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "appBrandStructs")
@XmlType(propOrder = {"appBrandStruct"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class AppBrandStructListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(AppBrandStructListStub.class.getName());

    private List<AppBrandStructStub> appBrandStructs = null;

    public AppBrandStructListStub()
    {
        this(new ArrayList<AppBrandStructStub>());
    }
    public AppBrandStructListStub(List<AppBrandStructStub> appBrandStructs)
    {
        this.appBrandStructs = appBrandStructs;
    }

    public boolean isEmpty()
    {
        if(appBrandStructs == null) {
            return true;
        } else {
            return appBrandStructs.isEmpty();
        }
    }
    public int getSize()
    {
        if(appBrandStructs == null) {
            return 0;
        } else {
            return appBrandStructs.size();
        }
    }

    @XmlElement(name = "appBrandStruct")
    public List<AppBrandStructStub> getAppBrandStruct()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<AppBrandStructStub> getList()
    {
        return appBrandStructs;
    }
    public void setList(List<AppBrandStructStub> appBrandStructs)
    {
        this.appBrandStructs = appBrandStructs;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<AppBrandStructStub> it = this.appBrandStructs.iterator();
        while(it.hasNext()) {
            AppBrandStructStub appBrandStruct = it.next();
            sb.append(appBrandStruct.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static AppBrandStructListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of AppBrandStructListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write AppBrandStructListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write AppBrandStructListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write AppBrandStructListStub object as a string.", e);
        }
        
        return null;
    }
    public static AppBrandStructListStub fromJsonString(String jsonStr)
    {
        try {
            AppBrandStructListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, AppBrandStructListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into AppBrandStructListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into AppBrandStructListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into AppBrandStructListStub object.", e);
        }
        
        return null;
    }

}
