package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.AppClient;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "appClients")
@XmlType(propOrder = {"appClient"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class AppClientListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(AppClientListStub.class.getName());

    private List<AppClientStub> appClients = null;

    public AppClientListStub()
    {
        this(new ArrayList<AppClientStub>());
    }
    public AppClientListStub(List<AppClientStub> appClients)
    {
        this.appClients = appClients;
    }

    public boolean isEmpty()
    {
        if(appClients == null) {
            return true;
        } else {
            return appClients.isEmpty();
        }
    }
    public int getSize()
    {
        if(appClients == null) {
            return 0;
        } else {
            return appClients.size();
        }
    }

    @XmlElement(name = "appClient")
    public List<AppClientStub> getAppClient()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<AppClientStub> getList()
    {
        return appClients;
    }
    public void setList(List<AppClientStub> appClients)
    {
        this.appClients = appClients;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<AppClientStub> it = this.appClients.iterator();
        while(it.hasNext()) {
            AppClientStub appClient = it.next();
            sb.append(appClient.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static AppClientListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of AppClientListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write AppClientListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write AppClientListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write AppClientListStub object as a string.", e);
        }
        
        return null;
    }
    public static AppClientListStub fromJsonString(String jsonStr)
    {
        try {
            AppClientListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, AppClientListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into AppClientListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into AppClientListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into AppClientListStub object.", e);
        }
        
        return null;
    }

}
