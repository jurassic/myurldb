package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.AppClient;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "appClient")
@XmlType(propOrder = {"guid", "owner", "admin", "name", "clientId", "clientCode", "rootDomain", "appDomain", "useAppDomain", "enabled", "licenseMode", "status", "createdTime", "modifiedTime"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AppClientStub implements AppClient, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(AppClientStub.class.getName());

    private String guid;
    private String owner;
    private String admin;
    private String name;
    private String clientId;
    private String clientCode;
    private String rootDomain;
    private String appDomain;
    private Boolean useAppDomain;
    private Boolean enabled;
    private String licenseMode;
    private String status;
    private Long createdTime;
    private Long modifiedTime;

    public AppClientStub()
    {
        this(null);
    }
    public AppClientStub(AppClient bean)
    {
        if(bean != null) {
            this.guid = bean.getGuid();
            this.owner = bean.getOwner();
            this.admin = bean.getAdmin();
            this.name = bean.getName();
            this.clientId = bean.getClientId();
            this.clientCode = bean.getClientCode();
            this.rootDomain = bean.getRootDomain();
            this.appDomain = bean.getAppDomain();
            this.useAppDomain = bean.isUseAppDomain();
            this.enabled = bean.isEnabled();
            this.licenseMode = bean.getLicenseMode();
            this.status = bean.getStatus();
            this.createdTime = bean.getCreatedTime();
            this.modifiedTime = bean.getModifiedTime();
        }
    }


    @XmlElement
    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    @XmlElement
    public String getOwner()
    {
        return this.owner;
    }
    public void setOwner(String owner)
    {
        this.owner = owner;
    }

    @XmlElement
    public String getAdmin()
    {
        return this.admin;
    }
    public void setAdmin(String admin)
    {
        this.admin = admin;
    }

    @XmlElement
    public String getName()
    {
        return this.name;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    @XmlElement
    public String getClientId()
    {
        return this.clientId;
    }
    public void setClientId(String clientId)
    {
        this.clientId = clientId;
    }

    @XmlElement
    public String getClientCode()
    {
        return this.clientCode;
    }
    public void setClientCode(String clientCode)
    {
        this.clientCode = clientCode;
    }

    @XmlElement
    public String getRootDomain()
    {
        return this.rootDomain;
    }
    public void setRootDomain(String rootDomain)
    {
        this.rootDomain = rootDomain;
    }

    @XmlElement
    public String getAppDomain()
    {
        return this.appDomain;
    }
    public void setAppDomain(String appDomain)
    {
        this.appDomain = appDomain;
    }

    @XmlElement
    public Boolean isUseAppDomain()
    {
        return this.useAppDomain;
    }
    public void setUseAppDomain(Boolean useAppDomain)
    {
        this.useAppDomain = useAppDomain;
    }

    @XmlElement
    public Boolean isEnabled()
    {
        return this.enabled;
    }
    public void setEnabled(Boolean enabled)
    {
        this.enabled = enabled;
    }

    @XmlElement
    public String getLicenseMode()
    {
        return this.licenseMode;
    }
    public void setLicenseMode(String licenseMode)
    {
        this.licenseMode = licenseMode;
    }

    @XmlElement
    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("guid", this.guid);
        dataMap.put("owner", this.owner);
        dataMap.put("admin", this.admin);
        dataMap.put("name", this.name);
        dataMap.put("clientId", this.clientId);
        dataMap.put("clientCode", this.clientCode);
        dataMap.put("rootDomain", this.rootDomain);
        dataMap.put("appDomain", this.appDomain);
        dataMap.put("useAppDomain", this.useAppDomain);
        dataMap.put("enabled", this.enabled);
        dataMap.put("licenseMode", this.licenseMode);
        dataMap.put("status", this.status);
        dataMap.put("createdTime", this.createdTime);
        dataMap.put("modifiedTime", this.modifiedTime);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = owner == null ? 0 : owner.hashCode();
        _hash = 31 * _hash + delta;
        delta = admin == null ? 0 : admin.hashCode();
        _hash = 31 * _hash + delta;
        delta = name == null ? 0 : name.hashCode();
        _hash = 31 * _hash + delta;
        delta = clientId == null ? 0 : clientId.hashCode();
        _hash = 31 * _hash + delta;
        delta = clientCode == null ? 0 : clientCode.hashCode();
        _hash = 31 * _hash + delta;
        delta = rootDomain == null ? 0 : rootDomain.hashCode();
        _hash = 31 * _hash + delta;
        delta = appDomain == null ? 0 : appDomain.hashCode();
        _hash = 31 * _hash + delta;
        delta = useAppDomain == null ? 0 : useAppDomain.hashCode();
        _hash = 31 * _hash + delta;
        delta = enabled == null ? 0 : enabled.hashCode();
        _hash = 31 * _hash + delta;
        delta = licenseMode == null ? 0 : licenseMode.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = createdTime == null ? 0 : createdTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static AppClientStub convertBeanToStub(AppClient bean)
    {
        AppClientStub stub = null;
        if(bean instanceof AppClientStub) {
            stub = (AppClientStub) bean;
        } else {
            if(bean != null) {
                stub = new AppClientStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static AppClientStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of AppClientStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write AppClientStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write AppClientStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write AppClientStub object as a string.", e);
        }
        
        return null;
    }
    public static AppClientStub fromJsonString(String jsonStr)
    {
        try {
            AppClientStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, AppClientStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into AppClientStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into AppClientStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into AppClientStub object.", e);
        }
        
        return null;
    }

}
