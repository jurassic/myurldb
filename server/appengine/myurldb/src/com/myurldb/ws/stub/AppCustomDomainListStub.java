package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.AppCustomDomain;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "appCustomDomains")
@XmlType(propOrder = {"appCustomDomain"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class AppCustomDomainListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(AppCustomDomainListStub.class.getName());

    private List<AppCustomDomainStub> appCustomDomains = null;

    public AppCustomDomainListStub()
    {
        this(new ArrayList<AppCustomDomainStub>());
    }
    public AppCustomDomainListStub(List<AppCustomDomainStub> appCustomDomains)
    {
        this.appCustomDomains = appCustomDomains;
    }

    public boolean isEmpty()
    {
        if(appCustomDomains == null) {
            return true;
        } else {
            return appCustomDomains.isEmpty();
        }
    }
    public int getSize()
    {
        if(appCustomDomains == null) {
            return 0;
        } else {
            return appCustomDomains.size();
        }
    }

    @XmlElement(name = "appCustomDomain")
    public List<AppCustomDomainStub> getAppCustomDomain()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<AppCustomDomainStub> getList()
    {
        return appCustomDomains;
    }
    public void setList(List<AppCustomDomainStub> appCustomDomains)
    {
        this.appCustomDomains = appCustomDomains;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<AppCustomDomainStub> it = this.appCustomDomains.iterator();
        while(it.hasNext()) {
            AppCustomDomainStub appCustomDomain = it.next();
            sb.append(appCustomDomain.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static AppCustomDomainListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of AppCustomDomainListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write AppCustomDomainListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write AppCustomDomainListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write AppCustomDomainListStub object as a string.", e);
        }
        
        return null;
    }
    public static AppCustomDomainListStub fromJsonString(String jsonStr)
    {
        try {
            AppCustomDomainListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, AppCustomDomainListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into AppCustomDomainListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into AppCustomDomainListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into AppCustomDomainListStub object.", e);
        }
        
        return null;
    }

}
