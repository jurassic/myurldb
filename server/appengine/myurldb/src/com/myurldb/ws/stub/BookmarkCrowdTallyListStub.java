package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.BookmarkCrowdTally;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "bookmarkCrowdTallies")
@XmlType(propOrder = {"bookmarkCrowdTally"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class BookmarkCrowdTallyListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(BookmarkCrowdTallyListStub.class.getName());

    private List<BookmarkCrowdTallyStub> bookmarkCrowdTallies = null;

    public BookmarkCrowdTallyListStub()
    {
        this(new ArrayList<BookmarkCrowdTallyStub>());
    }
    public BookmarkCrowdTallyListStub(List<BookmarkCrowdTallyStub> bookmarkCrowdTallies)
    {
        this.bookmarkCrowdTallies = bookmarkCrowdTallies;
    }

    public boolean isEmpty()
    {
        if(bookmarkCrowdTallies == null) {
            return true;
        } else {
            return bookmarkCrowdTallies.isEmpty();
        }
    }
    public int getSize()
    {
        if(bookmarkCrowdTallies == null) {
            return 0;
        } else {
            return bookmarkCrowdTallies.size();
        }
    }

    @XmlElement(name = "bookmarkCrowdTally")
    public List<BookmarkCrowdTallyStub> getBookmarkCrowdTally()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<BookmarkCrowdTallyStub> getList()
    {
        return bookmarkCrowdTallies;
    }
    public void setList(List<BookmarkCrowdTallyStub> bookmarkCrowdTallies)
    {
        this.bookmarkCrowdTallies = bookmarkCrowdTallies;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<BookmarkCrowdTallyStub> it = this.bookmarkCrowdTallies.iterator();
        while(it.hasNext()) {
            BookmarkCrowdTallyStub bookmarkCrowdTally = it.next();
            sb.append(bookmarkCrowdTally.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static BookmarkCrowdTallyListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of BookmarkCrowdTallyListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write BookmarkCrowdTallyListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write BookmarkCrowdTallyListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write BookmarkCrowdTallyListStub object as a string.", e);
        }
        
        return null;
    }
    public static BookmarkCrowdTallyListStub fromJsonString(String jsonStr)
    {
        try {
            BookmarkCrowdTallyListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, BookmarkCrowdTallyListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into BookmarkCrowdTallyListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into BookmarkCrowdTallyListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into BookmarkCrowdTallyListStub object.", e);
        }
        
        return null;
    }

}
