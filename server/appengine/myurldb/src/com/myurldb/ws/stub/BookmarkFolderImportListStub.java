package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.BookmarkFolderImport;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "bookmarkFolderImports")
@XmlType(propOrder = {"bookmarkFolderImport"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class BookmarkFolderImportListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(BookmarkFolderImportListStub.class.getName());

    private List<BookmarkFolderImportStub> bookmarkFolderImports = null;

    public BookmarkFolderImportListStub()
    {
        this(new ArrayList<BookmarkFolderImportStub>());
    }
    public BookmarkFolderImportListStub(List<BookmarkFolderImportStub> bookmarkFolderImports)
    {
        this.bookmarkFolderImports = bookmarkFolderImports;
    }

    public boolean isEmpty()
    {
        if(bookmarkFolderImports == null) {
            return true;
        } else {
            return bookmarkFolderImports.isEmpty();
        }
    }
    public int getSize()
    {
        if(bookmarkFolderImports == null) {
            return 0;
        } else {
            return bookmarkFolderImports.size();
        }
    }

    @XmlElement(name = "bookmarkFolderImport")
    public List<BookmarkFolderImportStub> getBookmarkFolderImport()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<BookmarkFolderImportStub> getList()
    {
        return bookmarkFolderImports;
    }
    public void setList(List<BookmarkFolderImportStub> bookmarkFolderImports)
    {
        this.bookmarkFolderImports = bookmarkFolderImports;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<BookmarkFolderImportStub> it = this.bookmarkFolderImports.iterator();
        while(it.hasNext()) {
            BookmarkFolderImportStub bookmarkFolderImport = it.next();
            sb.append(bookmarkFolderImport.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static BookmarkFolderImportListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of BookmarkFolderImportListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write BookmarkFolderImportListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write BookmarkFolderImportListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write BookmarkFolderImportListStub object as a string.", e);
        }
        
        return null;
    }
    public static BookmarkFolderImportListStub fromJsonString(String jsonStr)
    {
        try {
            BookmarkFolderImportListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, BookmarkFolderImportListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into BookmarkFolderImportListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into BookmarkFolderImportListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into BookmarkFolderImportListStub object.", e);
        }
        
        return null;
    }

}
