package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.BookmarkFolderImport;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "bookmarkFolderImport")
@XmlType(propOrder = {"guid", "user", "precedence", "importType", "importedFolder", "importedFolderUser", "importedFolderTitle", "importedFolderPath", "status", "note", "bookmarkFolder", "createdTime", "modifiedTime"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class BookmarkFolderImportStub extends FolderImportBaseStub implements BookmarkFolderImport, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(BookmarkFolderImportStub.class.getName());

    private String bookmarkFolder;

    public BookmarkFolderImportStub()
    {
        this(null);
    }
    public BookmarkFolderImportStub(BookmarkFolderImport bean)
    {
        super(bean);
        if(bean != null) {
            this.bookmarkFolder = bean.getBookmarkFolder();
        }
    }


    @XmlElement
    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    @XmlElement
    public String getUser()
    {
        return super.getUser();
    }
    public void setUser(String user)
    {
        super.setUser(user);
    }

    @XmlElement
    public Integer getPrecedence()
    {
        return super.getPrecedence();
    }
    public void setPrecedence(Integer precedence)
    {
        super.setPrecedence(precedence);
    }

    @XmlElement
    public String getImportType()
    {
        return super.getImportType();
    }
    public void setImportType(String importType)
    {
        super.setImportType(importType);
    }

    @XmlElement
    public String getImportedFolder()
    {
        return super.getImportedFolder();
    }
    public void setImportedFolder(String importedFolder)
    {
        super.setImportedFolder(importedFolder);
    }

    @XmlElement
    public String getImportedFolderUser()
    {
        return super.getImportedFolderUser();
    }
    public void setImportedFolderUser(String importedFolderUser)
    {
        super.setImportedFolderUser(importedFolderUser);
    }

    @XmlElement
    public String getImportedFolderTitle()
    {
        return super.getImportedFolderTitle();
    }
    public void setImportedFolderTitle(String importedFolderTitle)
    {
        super.setImportedFolderTitle(importedFolderTitle);
    }

    @XmlElement
    public String getImportedFolderPath()
    {
        return super.getImportedFolderPath();
    }
    public void setImportedFolderPath(String importedFolderPath)
    {
        super.setImportedFolderPath(importedFolderPath);
    }

    @XmlElement
    public String getStatus()
    {
        return super.getStatus();
    }
    public void setStatus(String status)
    {
        super.setStatus(status);
    }

    @XmlElement
    public String getNote()
    {
        return super.getNote();
    }
    public void setNote(String note)
    {
        super.setNote(note);
    }

    @XmlElement
    public String getBookmarkFolder()
    {
        return this.bookmarkFolder;
    }
    public void setBookmarkFolder(String bookmarkFolder)
    {
        this.bookmarkFolder = bookmarkFolder;
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("bookmarkFolder", this.bookmarkFolder);

        return dataMap;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = bookmarkFolder == null ? 0 : bookmarkFolder.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static BookmarkFolderImportStub convertBeanToStub(BookmarkFolderImport bean)
    {
        BookmarkFolderImportStub stub = null;
        if(bean instanceof BookmarkFolderImportStub) {
            stub = (BookmarkFolderImportStub) bean;
        } else {
            if(bean != null) {
                stub = new BookmarkFolderImportStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static BookmarkFolderImportStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of BookmarkFolderImportStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write BookmarkFolderImportStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write BookmarkFolderImportStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write BookmarkFolderImportStub object as a string.", e);
        }
        
        return null;
    }
    public static BookmarkFolderImportStub fromJsonString(String jsonStr)
    {
        try {
            BookmarkFolderImportStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, BookmarkFolderImportStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into BookmarkFolderImportStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into BookmarkFolderImportStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into BookmarkFolderImportStub object.", e);
        }
        
        return null;
    }

}
