package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.BookmarkFolder;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "bookmarkFolders")
@XmlType(propOrder = {"bookmarkFolder"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class BookmarkFolderListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(BookmarkFolderListStub.class.getName());

    private List<BookmarkFolderStub> bookmarkFolders = null;

    public BookmarkFolderListStub()
    {
        this(new ArrayList<BookmarkFolderStub>());
    }
    public BookmarkFolderListStub(List<BookmarkFolderStub> bookmarkFolders)
    {
        this.bookmarkFolders = bookmarkFolders;
    }

    public boolean isEmpty()
    {
        if(bookmarkFolders == null) {
            return true;
        } else {
            return bookmarkFolders.isEmpty();
        }
    }
    public int getSize()
    {
        if(bookmarkFolders == null) {
            return 0;
        } else {
            return bookmarkFolders.size();
        }
    }

    @XmlElement(name = "bookmarkFolder")
    public List<BookmarkFolderStub> getBookmarkFolder()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<BookmarkFolderStub> getList()
    {
        return bookmarkFolders;
    }
    public void setList(List<BookmarkFolderStub> bookmarkFolders)
    {
        this.bookmarkFolders = bookmarkFolders;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<BookmarkFolderStub> it = this.bookmarkFolders.iterator();
        while(it.hasNext()) {
            BookmarkFolderStub bookmarkFolder = it.next();
            sb.append(bookmarkFolder.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static BookmarkFolderListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of BookmarkFolderListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write BookmarkFolderListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write BookmarkFolderListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write BookmarkFolderListStub object as a string.", e);
        }
        
        return null;
    }
    public static BookmarkFolderListStub fromJsonString(String jsonStr)
    {
        try {
            BookmarkFolderListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, BookmarkFolderListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into BookmarkFolderListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into BookmarkFolderListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into BookmarkFolderListStub object.", e);
        }
        
        return null;
    }

}
