package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.BookmarkLink;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "bookmarkLinks")
@XmlType(propOrder = {"bookmarkLink"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class BookmarkLinkListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(BookmarkLinkListStub.class.getName());

    private List<BookmarkLinkStub> bookmarkLinks = null;

    public BookmarkLinkListStub()
    {
        this(new ArrayList<BookmarkLinkStub>());
    }
    public BookmarkLinkListStub(List<BookmarkLinkStub> bookmarkLinks)
    {
        this.bookmarkLinks = bookmarkLinks;
    }

    public boolean isEmpty()
    {
        if(bookmarkLinks == null) {
            return true;
        } else {
            return bookmarkLinks.isEmpty();
        }
    }
    public int getSize()
    {
        if(bookmarkLinks == null) {
            return 0;
        } else {
            return bookmarkLinks.size();
        }
    }

    @XmlElement(name = "bookmarkLink")
    public List<BookmarkLinkStub> getBookmarkLink()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<BookmarkLinkStub> getList()
    {
        return bookmarkLinks;
    }
    public void setList(List<BookmarkLinkStub> bookmarkLinks)
    {
        this.bookmarkLinks = bookmarkLinks;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<BookmarkLinkStub> it = this.bookmarkLinks.iterator();
        while(it.hasNext()) {
            BookmarkLinkStub bookmarkLink = it.next();
            sb.append(bookmarkLink.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static BookmarkLinkListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of BookmarkLinkListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write BookmarkLinkListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write BookmarkLinkListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write BookmarkLinkListStub object as a string.", e);
        }
        
        return null;
    }
    public static BookmarkLinkListStub fromJsonString(String jsonStr)
    {
        try {
            BookmarkLinkListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, BookmarkLinkListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into BookmarkLinkListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into BookmarkLinkListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into BookmarkLinkListStub object.", e);
        }
        
        return null;
    }

}
