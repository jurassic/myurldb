package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.BookmarkLink;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "bookmarkLink")
@XmlType(propOrder = {"guid", "appClient", "clientRootDomain", "user", "shortLink", "domain", "token", "longUrl", "shortUrl", "internal", "caching", "memo", "status", "note", "expirationTime", "bookmarkFolder", "contentTag", "referenceElement", "elementType", "keywordLink", "createdTime", "modifiedTime"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class BookmarkLinkStub extends NavLinkBaseStub implements BookmarkLink, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(BookmarkLinkStub.class.getName());

    private String bookmarkFolder;
    private String contentTag;
    private String referenceElement;
    private String elementType;
    private String keywordLink;

    public BookmarkLinkStub()
    {
        this(null);
    }
    public BookmarkLinkStub(BookmarkLink bean)
    {
        super(bean);
        if(bean != null) {
            this.bookmarkFolder = bean.getBookmarkFolder();
            this.contentTag = bean.getContentTag();
            this.referenceElement = bean.getReferenceElement();
            this.elementType = bean.getElementType();
            this.keywordLink = bean.getKeywordLink();
        }
    }


    @XmlElement
    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    @XmlElement
    public String getAppClient()
    {
        return super.getAppClient();
    }
    public void setAppClient(String appClient)
    {
        super.setAppClient(appClient);
    }

    @XmlElement
    public String getClientRootDomain()
    {
        return super.getClientRootDomain();
    }
    public void setClientRootDomain(String clientRootDomain)
    {
        super.setClientRootDomain(clientRootDomain);
    }

    @XmlElement
    public String getUser()
    {
        return super.getUser();
    }
    public void setUser(String user)
    {
        super.setUser(user);
    }

    @XmlElement
    public String getShortLink()
    {
        return super.getShortLink();
    }
    public void setShortLink(String shortLink)
    {
        super.setShortLink(shortLink);
    }

    @XmlElement
    public String getDomain()
    {
        return super.getDomain();
    }
    public void setDomain(String domain)
    {
        super.setDomain(domain);
    }

    @XmlElement
    public String getToken()
    {
        return super.getToken();
    }
    public void setToken(String token)
    {
        super.setToken(token);
    }

    @XmlElement
    public String getLongUrl()
    {
        return super.getLongUrl();
    }
    public void setLongUrl(String longUrl)
    {
        super.setLongUrl(longUrl);
    }

    @XmlElement
    public String getShortUrl()
    {
        return super.getShortUrl();
    }
    public void setShortUrl(String shortUrl)
    {
        super.setShortUrl(shortUrl);
    }

    @XmlElement
    public Boolean isInternal()
    {
        return super.isInternal();
    }
    public void setInternal(Boolean internal)
    {
        super.setInternal(internal);
    }

    @XmlElement
    public Boolean isCaching()
    {
        return super.isCaching();
    }
    public void setCaching(Boolean caching)
    {
        super.setCaching(caching);
    }

    @XmlElement
    public String getMemo()
    {
        return super.getMemo();
    }
    public void setMemo(String memo)
    {
        super.setMemo(memo);
    }

    @XmlElement
    public String getStatus()
    {
        return super.getStatus();
    }
    public void setStatus(String status)
    {
        super.setStatus(status);
    }

    @XmlElement
    public String getNote()
    {
        return super.getNote();
    }
    public void setNote(String note)
    {
        super.setNote(note);
    }

    @XmlElement
    public Long getExpirationTime()
    {
        return super.getExpirationTime();
    }
    public void setExpirationTime(Long expirationTime)
    {
        super.setExpirationTime(expirationTime);
    }

    @XmlElement
    public String getBookmarkFolder()
    {
        return this.bookmarkFolder;
    }
    public void setBookmarkFolder(String bookmarkFolder)
    {
        this.bookmarkFolder = bookmarkFolder;
    }

    @XmlElement
    public String getContentTag()
    {
        return this.contentTag;
    }
    public void setContentTag(String contentTag)
    {
        this.contentTag = contentTag;
    }

    @XmlElement
    public String getReferenceElement()
    {
        return this.referenceElement;
    }
    public void setReferenceElement(String referenceElement)
    {
        this.referenceElement = referenceElement;
    }

    @XmlElement
    public String getElementType()
    {
        return this.elementType;
    }
    public void setElementType(String elementType)
    {
        this.elementType = elementType;
    }

    @XmlElement
    public String getKeywordLink()
    {
        return this.keywordLink;
    }
    public void setKeywordLink(String keywordLink)
    {
        this.keywordLink = keywordLink;
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("bookmarkFolder", this.bookmarkFolder);
        dataMap.put("contentTag", this.contentTag);
        dataMap.put("referenceElement", this.referenceElement);
        dataMap.put("elementType", this.elementType);
        dataMap.put("keywordLink", this.keywordLink);

        return dataMap;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = bookmarkFolder == null ? 0 : bookmarkFolder.hashCode();
        _hash = 31 * _hash + delta;
        delta = contentTag == null ? 0 : contentTag.hashCode();
        _hash = 31 * _hash + delta;
        delta = referenceElement == null ? 0 : referenceElement.hashCode();
        _hash = 31 * _hash + delta;
        delta = elementType == null ? 0 : elementType.hashCode();
        _hash = 31 * _hash + delta;
        delta = keywordLink == null ? 0 : keywordLink.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static BookmarkLinkStub convertBeanToStub(BookmarkLink bean)
    {
        BookmarkLinkStub stub = null;
        if(bean instanceof BookmarkLinkStub) {
            stub = (BookmarkLinkStub) bean;
        } else {
            if(bean != null) {
                stub = new BookmarkLinkStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static BookmarkLinkStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of BookmarkLinkStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write BookmarkLinkStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write BookmarkLinkStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write BookmarkLinkStub object as a string.", e);
        }
        
        return null;
    }
    public static BookmarkLinkStub fromJsonString(String jsonStr)
    {
        try {
            BookmarkLinkStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, BookmarkLinkStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into BookmarkLinkStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into BookmarkLinkStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into BookmarkLinkStub object.", e);
        }
        
        return null;
    }

}
