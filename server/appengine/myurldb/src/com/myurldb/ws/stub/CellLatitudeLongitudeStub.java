package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.CellLatitudeLongitude;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "cellLatitudeLongitude")
@XmlType(propOrder = {"scale", "latitude", "longitude"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CellLatitudeLongitudeStub implements CellLatitudeLongitude, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(CellLatitudeLongitudeStub.class.getName());

    private Integer scale;
    private Integer latitude;
    private Integer longitude;

    public CellLatitudeLongitudeStub()
    {
        this(null);
    }
    public CellLatitudeLongitudeStub(CellLatitudeLongitude bean)
    {
        if(bean != null) {
            this.scale = bean.getScale();
            this.latitude = bean.getLatitude();
            this.longitude = bean.getLongitude();
        }
    }


    @XmlElement
    public Integer getScale()
    {
        return this.scale;
    }
    public void setScale(Integer scale)
    {
        this.scale = scale;
    }

    @XmlElement
    public Integer getLatitude()
    {
        return this.latitude;
    }
    public void setLatitude(Integer latitude)
    {
        this.latitude = latitude;
    }

    @XmlElement
    public Integer getLongitude()
    {
        return this.longitude;
    }
    public void setLongitude(Integer longitude)
    {
        this.longitude = longitude;
    }


    @XmlTransient
    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getScale() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLatitude() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLongitude() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("scale", this.scale);
        dataMap.put("latitude", this.latitude);
        dataMap.put("longitude", this.longitude);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = scale == null ? 0 : scale.hashCode();
        _hash = 31 * _hash + delta;
        delta = latitude == null ? 0 : latitude.hashCode();
        _hash = 31 * _hash + delta;
        delta = longitude == null ? 0 : longitude.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static CellLatitudeLongitudeStub convertBeanToStub(CellLatitudeLongitude bean)
    {
        CellLatitudeLongitudeStub stub = null;
        if(bean instanceof CellLatitudeLongitudeStub) {
            stub = (CellLatitudeLongitudeStub) bean;
        } else {
            if(bean != null) {
                stub = new CellLatitudeLongitudeStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static CellLatitudeLongitudeStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of CellLatitudeLongitudeStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write CellLatitudeLongitudeStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write CellLatitudeLongitudeStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write CellLatitudeLongitudeStub object as a string.", e);
        }
        
        return null;
    }
    public static CellLatitudeLongitudeStub fromJsonString(String jsonStr)
    {
        try {
            CellLatitudeLongitudeStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, CellLatitudeLongitudeStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into CellLatitudeLongitudeStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into CellLatitudeLongitudeStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into CellLatitudeLongitudeStub object.", e);
        }
        
        return null;
    }

}
