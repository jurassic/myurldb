package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.ClientSetting;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "clientSettings")
@XmlType(propOrder = {"clientSetting"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class ClientSettingListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ClientSettingListStub.class.getName());

    private List<ClientSettingStub> clientSettings = null;

    public ClientSettingListStub()
    {
        this(new ArrayList<ClientSettingStub>());
    }
    public ClientSettingListStub(List<ClientSettingStub> clientSettings)
    {
        this.clientSettings = clientSettings;
    }

    public boolean isEmpty()
    {
        if(clientSettings == null) {
            return true;
        } else {
            return clientSettings.isEmpty();
        }
    }
    public int getSize()
    {
        if(clientSettings == null) {
            return 0;
        } else {
            return clientSettings.size();
        }
    }

    @XmlElement(name = "clientSetting")
    public List<ClientSettingStub> getClientSetting()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<ClientSettingStub> getList()
    {
        return clientSettings;
    }
    public void setList(List<ClientSettingStub> clientSettings)
    {
        this.clientSettings = clientSettings;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<ClientSettingStub> it = this.clientSettings.iterator();
        while(it.hasNext()) {
            ClientSettingStub clientSetting = it.next();
            sb.append(clientSetting.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static ClientSettingListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of ClientSettingListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write ClientSettingListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write ClientSettingListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write ClientSettingListStub object as a string.", e);
        }
        
        return null;
    }
    public static ClientSettingListStub fromJsonString(String jsonStr)
    {
        try {
            ClientSettingListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, ClientSettingListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into ClientSettingListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into ClientSettingListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into ClientSettingListStub object.", e);
        }
        
        return null;
    }

}
