package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.ClientSetting;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "clientSetting")
@XmlType(propOrder = {"guid", "appClient", "admin", "autoRedirectEnabled", "viewEnabled", "shareEnabled", "defaultBaseDomain", "defaultDomainType", "defaultTokenType", "defaultRedirectType", "defaultFlashDuration", "defaultAccessType", "defaultViewType", "createdTime", "modifiedTime"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ClientSettingStub extends PersonalSettingStub implements ClientSetting, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ClientSettingStub.class.getName());

    private String appClient;
    private String admin;
    private Boolean autoRedirectEnabled;
    private Boolean viewEnabled;
    private Boolean shareEnabled;
    private String defaultBaseDomain;
    private String defaultDomainType;
    private String defaultTokenType;
    private String defaultRedirectType;
    private Long defaultFlashDuration;
    private String defaultAccessType;
    private String defaultViewType;

    public ClientSettingStub()
    {
        this(null);
    }
    public ClientSettingStub(ClientSetting bean)
    {
        super(bean);
        if(bean != null) {
            this.appClient = bean.getAppClient();
            this.admin = bean.getAdmin();
            this.autoRedirectEnabled = bean.isAutoRedirectEnabled();
            this.viewEnabled = bean.isViewEnabled();
            this.shareEnabled = bean.isShareEnabled();
            this.defaultBaseDomain = bean.getDefaultBaseDomain();
            this.defaultDomainType = bean.getDefaultDomainType();
            this.defaultTokenType = bean.getDefaultTokenType();
            this.defaultRedirectType = bean.getDefaultRedirectType();
            this.defaultFlashDuration = bean.getDefaultFlashDuration();
            this.defaultAccessType = bean.getDefaultAccessType();
            this.defaultViewType = bean.getDefaultViewType();
        }
    }


    @XmlElement
    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    @XmlElement
    public String getAppClient()
    {
        return this.appClient;
    }
    public void setAppClient(String appClient)
    {
        this.appClient = appClient;
    }

    @XmlElement
    public String getAdmin()
    {
        return this.admin;
    }
    public void setAdmin(String admin)
    {
        this.admin = admin;
    }

    @XmlElement
    public Boolean isAutoRedirectEnabled()
    {
        return this.autoRedirectEnabled;
    }
    public void setAutoRedirectEnabled(Boolean autoRedirectEnabled)
    {
        this.autoRedirectEnabled = autoRedirectEnabled;
    }

    @XmlElement
    public Boolean isViewEnabled()
    {
        return this.viewEnabled;
    }
    public void setViewEnabled(Boolean viewEnabled)
    {
        this.viewEnabled = viewEnabled;
    }

    @XmlElement
    public Boolean isShareEnabled()
    {
        return this.shareEnabled;
    }
    public void setShareEnabled(Boolean shareEnabled)
    {
        this.shareEnabled = shareEnabled;
    }

    @XmlElement
    public String getDefaultBaseDomain()
    {
        return this.defaultBaseDomain;
    }
    public void setDefaultBaseDomain(String defaultBaseDomain)
    {
        this.defaultBaseDomain = defaultBaseDomain;
    }

    @XmlElement
    public String getDefaultDomainType()
    {
        return this.defaultDomainType;
    }
    public void setDefaultDomainType(String defaultDomainType)
    {
        this.defaultDomainType = defaultDomainType;
    }

    @XmlElement
    public String getDefaultTokenType()
    {
        return this.defaultTokenType;
    }
    public void setDefaultTokenType(String defaultTokenType)
    {
        this.defaultTokenType = defaultTokenType;
    }

    @XmlElement
    public String getDefaultRedirectType()
    {
        return this.defaultRedirectType;
    }
    public void setDefaultRedirectType(String defaultRedirectType)
    {
        this.defaultRedirectType = defaultRedirectType;
    }

    @XmlElement
    public Long getDefaultFlashDuration()
    {
        return this.defaultFlashDuration;
    }
    public void setDefaultFlashDuration(Long defaultFlashDuration)
    {
        this.defaultFlashDuration = defaultFlashDuration;
    }

    @XmlElement
    public String getDefaultAccessType()
    {
        return this.defaultAccessType;
    }
    public void setDefaultAccessType(String defaultAccessType)
    {
        this.defaultAccessType = defaultAccessType;
    }

    @XmlElement
    public String getDefaultViewType()
    {
        return this.defaultViewType;
    }
    public void setDefaultViewType(String defaultViewType)
    {
        this.defaultViewType = defaultViewType;
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("appClient", this.appClient);
        dataMap.put("admin", this.admin);
        dataMap.put("autoRedirectEnabled", this.autoRedirectEnabled);
        dataMap.put("viewEnabled", this.viewEnabled);
        dataMap.put("shareEnabled", this.shareEnabled);
        dataMap.put("defaultBaseDomain", this.defaultBaseDomain);
        dataMap.put("defaultDomainType", this.defaultDomainType);
        dataMap.put("defaultTokenType", this.defaultTokenType);
        dataMap.put("defaultRedirectType", this.defaultRedirectType);
        dataMap.put("defaultFlashDuration", this.defaultFlashDuration);
        dataMap.put("defaultAccessType", this.defaultAccessType);
        dataMap.put("defaultViewType", this.defaultViewType);

        return dataMap;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = appClient == null ? 0 : appClient.hashCode();
        _hash = 31 * _hash + delta;
        delta = admin == null ? 0 : admin.hashCode();
        _hash = 31 * _hash + delta;
        delta = autoRedirectEnabled == null ? 0 : autoRedirectEnabled.hashCode();
        _hash = 31 * _hash + delta;
        delta = viewEnabled == null ? 0 : viewEnabled.hashCode();
        _hash = 31 * _hash + delta;
        delta = shareEnabled == null ? 0 : shareEnabled.hashCode();
        _hash = 31 * _hash + delta;
        delta = defaultBaseDomain == null ? 0 : defaultBaseDomain.hashCode();
        _hash = 31 * _hash + delta;
        delta = defaultDomainType == null ? 0 : defaultDomainType.hashCode();
        _hash = 31 * _hash + delta;
        delta = defaultTokenType == null ? 0 : defaultTokenType.hashCode();
        _hash = 31 * _hash + delta;
        delta = defaultRedirectType == null ? 0 : defaultRedirectType.hashCode();
        _hash = 31 * _hash + delta;
        delta = defaultFlashDuration == null ? 0 : defaultFlashDuration.hashCode();
        _hash = 31 * _hash + delta;
        delta = defaultAccessType == null ? 0 : defaultAccessType.hashCode();
        _hash = 31 * _hash + delta;
        delta = defaultViewType == null ? 0 : defaultViewType.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static ClientSettingStub convertBeanToStub(ClientSetting bean)
    {
        ClientSettingStub stub = null;
        if(bean instanceof ClientSettingStub) {
            stub = (ClientSettingStub) bean;
        } else {
            if(bean != null) {
                stub = new ClientSettingStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static ClientSettingStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of ClientSettingStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write ClientSettingStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write ClientSettingStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write ClientSettingStub object as a string.", e);
        }
        
        return null;
    }
    public static ClientSettingStub fromJsonString(String jsonStr)
    {
        try {
            ClientSettingStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, ClientSettingStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into ClientSettingStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into ClientSettingStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into ClientSettingStub object.", e);
        }
        
        return null;
    }

}
