package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.ClientUser;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "clientUsers")
@XmlType(propOrder = {"clientUser"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class ClientUserListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ClientUserListStub.class.getName());

    private List<ClientUserStub> clientUsers = null;

    public ClientUserListStub()
    {
        this(new ArrayList<ClientUserStub>());
    }
    public ClientUserListStub(List<ClientUserStub> clientUsers)
    {
        this.clientUsers = clientUsers;
    }

    public boolean isEmpty()
    {
        if(clientUsers == null) {
            return true;
        } else {
            return clientUsers.isEmpty();
        }
    }
    public int getSize()
    {
        if(clientUsers == null) {
            return 0;
        } else {
            return clientUsers.size();
        }
    }

    @XmlElement(name = "clientUser")
    public List<ClientUserStub> getClientUser()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<ClientUserStub> getList()
    {
        return clientUsers;
    }
    public void setList(List<ClientUserStub> clientUsers)
    {
        this.clientUsers = clientUsers;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<ClientUserStub> it = this.clientUsers.iterator();
        while(it.hasNext()) {
            ClientUserStub clientUser = it.next();
            sb.append(clientUser.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static ClientUserListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of ClientUserListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write ClientUserListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write ClientUserListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write ClientUserListStub object as a string.", e);
        }
        
        return null;
    }
    public static ClientUserListStub fromJsonString(String jsonStr)
    {
        try {
            ClientUserListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, ClientUserListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into ClientUserListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into ClientUserListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into ClientUserListStub object.", e);
        }
        
        return null;
    }

}
