package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.ClientUser;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "clientUser")
@XmlType(propOrder = {"guid", "appClient", "user", "role", "provisioned", "licensed", "status", "createdTime", "modifiedTime"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ClientUserStub implements ClientUser, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ClientUserStub.class.getName());

    private String guid;
    private String appClient;
    private String user;
    private String role;
    private Boolean provisioned;
    private Boolean licensed;
    private String status;
    private Long createdTime;
    private Long modifiedTime;

    public ClientUserStub()
    {
        this(null);
    }
    public ClientUserStub(ClientUser bean)
    {
        if(bean != null) {
            this.guid = bean.getGuid();
            this.appClient = bean.getAppClient();
            this.user = bean.getUser();
            this.role = bean.getRole();
            this.provisioned = bean.isProvisioned();
            this.licensed = bean.isLicensed();
            this.status = bean.getStatus();
            this.createdTime = bean.getCreatedTime();
            this.modifiedTime = bean.getModifiedTime();
        }
    }


    @XmlElement
    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    @XmlElement
    public String getAppClient()
    {
        return this.appClient;
    }
    public void setAppClient(String appClient)
    {
        this.appClient = appClient;
    }

    @XmlElement
    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    @XmlElement
    public String getRole()
    {
        return this.role;
    }
    public void setRole(String role)
    {
        this.role = role;
    }

    @XmlElement
    public Boolean isProvisioned()
    {
        return this.provisioned;
    }
    public void setProvisioned(Boolean provisioned)
    {
        this.provisioned = provisioned;
    }

    @XmlElement
    public Boolean isLicensed()
    {
        return this.licensed;
    }
    public void setLicensed(Boolean licensed)
    {
        this.licensed = licensed;
    }

    @XmlElement
    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("guid", this.guid);
        dataMap.put("appClient", this.appClient);
        dataMap.put("user", this.user);
        dataMap.put("role", this.role);
        dataMap.put("provisioned", this.provisioned);
        dataMap.put("licensed", this.licensed);
        dataMap.put("status", this.status);
        dataMap.put("createdTime", this.createdTime);
        dataMap.put("modifiedTime", this.modifiedTime);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = appClient == null ? 0 : appClient.hashCode();
        _hash = 31 * _hash + delta;
        delta = user == null ? 0 : user.hashCode();
        _hash = 31 * _hash + delta;
        delta = role == null ? 0 : role.hashCode();
        _hash = 31 * _hash + delta;
        delta = provisioned == null ? 0 : provisioned.hashCode();
        _hash = 31 * _hash + delta;
        delta = licensed == null ? 0 : licensed.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = createdTime == null ? 0 : createdTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static ClientUserStub convertBeanToStub(ClientUser bean)
    {
        ClientUserStub stub = null;
        if(bean instanceof ClientUserStub) {
            stub = (ClientUserStub) bean;
        } else {
            if(bean != null) {
                stub = new ClientUserStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static ClientUserStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of ClientUserStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write ClientUserStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write ClientUserStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write ClientUserStub object as a string.", e);
        }
        
        return null;
    }
    public static ClientUserStub fromJsonString(String jsonStr)
    {
        try {
            ClientUserStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, ClientUserStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into ClientUserStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into ClientUserStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into ClientUserStub object.", e);
        }
        
        return null;
    }

}
