package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.ContactInfoStruct;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "contactInfoStructs")
@XmlType(propOrder = {"contactInfoStruct"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class ContactInfoStructListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ContactInfoStructListStub.class.getName());

    private List<ContactInfoStructStub> contactInfoStructs = null;

    public ContactInfoStructListStub()
    {
        this(new ArrayList<ContactInfoStructStub>());
    }
    public ContactInfoStructListStub(List<ContactInfoStructStub> contactInfoStructs)
    {
        this.contactInfoStructs = contactInfoStructs;
    }

    public boolean isEmpty()
    {
        if(contactInfoStructs == null) {
            return true;
        } else {
            return contactInfoStructs.isEmpty();
        }
    }
    public int getSize()
    {
        if(contactInfoStructs == null) {
            return 0;
        } else {
            return contactInfoStructs.size();
        }
    }

    @XmlElement(name = "contactInfoStruct")
    public List<ContactInfoStructStub> getContactInfoStruct()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<ContactInfoStructStub> getList()
    {
        return contactInfoStructs;
    }
    public void setList(List<ContactInfoStructStub> contactInfoStructs)
    {
        this.contactInfoStructs = contactInfoStructs;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<ContactInfoStructStub> it = this.contactInfoStructs.iterator();
        while(it.hasNext()) {
            ContactInfoStructStub contactInfoStruct = it.next();
            sb.append(contactInfoStruct.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static ContactInfoStructListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of ContactInfoStructListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write ContactInfoStructListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write ContactInfoStructListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write ContactInfoStructListStub object as a string.", e);
        }
        
        return null;
    }
    public static ContactInfoStructListStub fromJsonString(String jsonStr)
    {
        try {
            ContactInfoStructListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, ContactInfoStructListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into ContactInfoStructListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into ContactInfoStructListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into ContactInfoStructListStub object.", e);
        }
        
        return null;
    }

}
