package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.ContactInfoStruct;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "contactInfoStruct")
@XmlType(propOrder = {"uuid", "streetAddress", "locality", "region", "postalCode", "countryName", "emailAddress", "phoneNumber", "faxNumber", "website", "note"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ContactInfoStructStub implements ContactInfoStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ContactInfoStructStub.class.getName());

    private String uuid;
    private String streetAddress;
    private String locality;
    private String region;
    private String postalCode;
    private String countryName;
    private String emailAddress;
    private String phoneNumber;
    private String faxNumber;
    private String website;
    private String note;

    public ContactInfoStructStub()
    {
        this(null);
    }
    public ContactInfoStructStub(ContactInfoStruct bean)
    {
        if(bean != null) {
            this.uuid = bean.getUuid();
            this.streetAddress = bean.getStreetAddress();
            this.locality = bean.getLocality();
            this.region = bean.getRegion();
            this.postalCode = bean.getPostalCode();
            this.countryName = bean.getCountryName();
            this.emailAddress = bean.getEmailAddress();
            this.phoneNumber = bean.getPhoneNumber();
            this.faxNumber = bean.getFaxNumber();
            this.website = bean.getWebsite();
            this.note = bean.getNote();
        }
    }


    @XmlElement
    public String getUuid()
    {
        return this.uuid;
    }
    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }

    @XmlElement
    public String getStreetAddress()
    {
        return this.streetAddress;
    }
    public void setStreetAddress(String streetAddress)
    {
        this.streetAddress = streetAddress;
    }

    @XmlElement
    public String getLocality()
    {
        return this.locality;
    }
    public void setLocality(String locality)
    {
        this.locality = locality;
    }

    @XmlElement
    public String getRegion()
    {
        return this.region;
    }
    public void setRegion(String region)
    {
        this.region = region;
    }

    @XmlElement
    public String getPostalCode()
    {
        return this.postalCode;
    }
    public void setPostalCode(String postalCode)
    {
        this.postalCode = postalCode;
    }

    @XmlElement
    public String getCountryName()
    {
        return this.countryName;
    }
    public void setCountryName(String countryName)
    {
        this.countryName = countryName;
    }

    @XmlElement
    public String getEmailAddress()
    {
        return this.emailAddress;
    }
    public void setEmailAddress(String emailAddress)
    {
        this.emailAddress = emailAddress;
    }

    @XmlElement
    public String getPhoneNumber()
    {
        return this.phoneNumber;
    }
    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    @XmlElement
    public String getFaxNumber()
    {
        return this.faxNumber;
    }
    public void setFaxNumber(String faxNumber)
    {
        this.faxNumber = faxNumber;
    }

    @XmlElement
    public String getWebsite()
    {
        return this.website;
    }
    public void setWebsite(String website)
    {
        this.website = website;
    }

    @XmlElement
    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }


    @XmlTransient
    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getStreetAddress() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLocality() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getRegion() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPostalCode() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getCountryName() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getEmailAddress() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPhoneNumber() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getFaxNumber() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getWebsite() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("uuid", this.uuid);
        dataMap.put("streetAddress", this.streetAddress);
        dataMap.put("locality", this.locality);
        dataMap.put("region", this.region);
        dataMap.put("postalCode", this.postalCode);
        dataMap.put("countryName", this.countryName);
        dataMap.put("emailAddress", this.emailAddress);
        dataMap.put("phoneNumber", this.phoneNumber);
        dataMap.put("faxNumber", this.faxNumber);
        dataMap.put("website", this.website);
        dataMap.put("note", this.note);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = uuid == null ? 0 : uuid.hashCode();
        _hash = 31 * _hash + delta;
        delta = streetAddress == null ? 0 : streetAddress.hashCode();
        _hash = 31 * _hash + delta;
        delta = locality == null ? 0 : locality.hashCode();
        _hash = 31 * _hash + delta;
        delta = region == null ? 0 : region.hashCode();
        _hash = 31 * _hash + delta;
        delta = postalCode == null ? 0 : postalCode.hashCode();
        _hash = 31 * _hash + delta;
        delta = countryName == null ? 0 : countryName.hashCode();
        _hash = 31 * _hash + delta;
        delta = emailAddress == null ? 0 : emailAddress.hashCode();
        _hash = 31 * _hash + delta;
        delta = phoneNumber == null ? 0 : phoneNumber.hashCode();
        _hash = 31 * _hash + delta;
        delta = faxNumber == null ? 0 : faxNumber.hashCode();
        _hash = 31 * _hash + delta;
        delta = website == null ? 0 : website.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static ContactInfoStructStub convertBeanToStub(ContactInfoStruct bean)
    {
        ContactInfoStructStub stub = null;
        if(bean instanceof ContactInfoStructStub) {
            stub = (ContactInfoStructStub) bean;
        } else {
            if(bean != null) {
                stub = new ContactInfoStructStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static ContactInfoStructStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of ContactInfoStructStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write ContactInfoStructStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write ContactInfoStructStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write ContactInfoStructStub object as a string.", e);
        }
        
        return null;
    }
    public static ContactInfoStructStub fromJsonString(String jsonStr)
    {
        try {
            ContactInfoStructStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, ContactInfoStructStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into ContactInfoStructStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into ContactInfoStructStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into ContactInfoStructStub object.", e);
        }
        
        return null;
    }

}
