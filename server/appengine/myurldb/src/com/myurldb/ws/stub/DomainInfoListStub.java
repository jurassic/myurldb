package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.DomainInfo;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "domainInfos")
@XmlType(propOrder = {"domainInfo"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class DomainInfoListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(DomainInfoListStub.class.getName());

    private List<DomainInfoStub> domainInfos = null;

    public DomainInfoListStub()
    {
        this(new ArrayList<DomainInfoStub>());
    }
    public DomainInfoListStub(List<DomainInfoStub> domainInfos)
    {
        this.domainInfos = domainInfos;
    }

    public boolean isEmpty()
    {
        if(domainInfos == null) {
            return true;
        } else {
            return domainInfos.isEmpty();
        }
    }
    public int getSize()
    {
        if(domainInfos == null) {
            return 0;
        } else {
            return domainInfos.size();
        }
    }

    @XmlElement(name = "domainInfo")
    public List<DomainInfoStub> getDomainInfo()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<DomainInfoStub> getList()
    {
        return domainInfos;
    }
    public void setList(List<DomainInfoStub> domainInfos)
    {
        this.domainInfos = domainInfos;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<DomainInfoStub> it = this.domainInfos.iterator();
        while(it.hasNext()) {
            DomainInfoStub domainInfo = it.next();
            sb.append(domainInfo.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static DomainInfoListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of DomainInfoListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write DomainInfoListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write DomainInfoListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write DomainInfoListStub object as a string.", e);
        }
        
        return null;
    }
    public static DomainInfoListStub fromJsonString(String jsonStr)
    {
        try {
            DomainInfoListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, DomainInfoListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into DomainInfoListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into DomainInfoListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into DomainInfoListStub object.", e);
        }
        
        return null;
    }

}
