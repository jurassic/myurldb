package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.DomainInfo;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "domainInfo")
@XmlType(propOrder = {"guid", "domain", "banned", "urlShortener", "category", "reputation", "authority", "note", "verifiedTime", "createdTime", "modifiedTime"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class DomainInfoStub implements DomainInfo, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(DomainInfoStub.class.getName());

    private String guid;
    private String domain;
    private Boolean banned;
    private Boolean urlShortener;
    private String category;
    private String reputation;
    private String authority;
    private String note;
    private Long verifiedTime;
    private Long createdTime;
    private Long modifiedTime;

    public DomainInfoStub()
    {
        this(null);
    }
    public DomainInfoStub(DomainInfo bean)
    {
        if(bean != null) {
            this.guid = bean.getGuid();
            this.domain = bean.getDomain();
            this.banned = bean.isBanned();
            this.urlShortener = bean.isUrlShortener();
            this.category = bean.getCategory();
            this.reputation = bean.getReputation();
            this.authority = bean.getAuthority();
            this.note = bean.getNote();
            this.verifiedTime = bean.getVerifiedTime();
            this.createdTime = bean.getCreatedTime();
            this.modifiedTime = bean.getModifiedTime();
        }
    }


    @XmlElement
    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    @XmlElement
    public String getDomain()
    {
        return this.domain;
    }
    public void setDomain(String domain)
    {
        this.domain = domain;
    }

    @XmlElement
    public Boolean isBanned()
    {
        return this.banned;
    }
    public void setBanned(Boolean banned)
    {
        this.banned = banned;
    }

    @XmlElement
    public Boolean isUrlShortener()
    {
        return this.urlShortener;
    }
    public void setUrlShortener(Boolean urlShortener)
    {
        this.urlShortener = urlShortener;
    }

    @XmlElement
    public String getCategory()
    {
        return this.category;
    }
    public void setCategory(String category)
    {
        this.category = category;
    }

    @XmlElement
    public String getReputation()
    {
        return this.reputation;
    }
    public void setReputation(String reputation)
    {
        this.reputation = reputation;
    }

    @XmlElement
    public String getAuthority()
    {
        return this.authority;
    }
    public void setAuthority(String authority)
    {
        this.authority = authority;
    }

    @XmlElement
    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    @XmlElement
    public Long getVerifiedTime()
    {
        return this.verifiedTime;
    }
    public void setVerifiedTime(Long verifiedTime)
    {
        this.verifiedTime = verifiedTime;
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("guid", this.guid);
        dataMap.put("domain", this.domain);
        dataMap.put("banned", this.banned);
        dataMap.put("urlShortener", this.urlShortener);
        dataMap.put("category", this.category);
        dataMap.put("reputation", this.reputation);
        dataMap.put("authority", this.authority);
        dataMap.put("note", this.note);
        dataMap.put("verifiedTime", this.verifiedTime);
        dataMap.put("createdTime", this.createdTime);
        dataMap.put("modifiedTime", this.modifiedTime);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = domain == null ? 0 : domain.hashCode();
        _hash = 31 * _hash + delta;
        delta = banned == null ? 0 : banned.hashCode();
        _hash = 31 * _hash + delta;
        delta = urlShortener == null ? 0 : urlShortener.hashCode();
        _hash = 31 * _hash + delta;
        delta = category == null ? 0 : category.hashCode();
        _hash = 31 * _hash + delta;
        delta = reputation == null ? 0 : reputation.hashCode();
        _hash = 31 * _hash + delta;
        delta = authority == null ? 0 : authority.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        delta = verifiedTime == null ? 0 : verifiedTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = createdTime == null ? 0 : createdTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static DomainInfoStub convertBeanToStub(DomainInfo bean)
    {
        DomainInfoStub stub = null;
        if(bean instanceof DomainInfoStub) {
            stub = (DomainInfoStub) bean;
        } else {
            if(bean != null) {
                stub = new DomainInfoStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static DomainInfoStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of DomainInfoStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write DomainInfoStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write DomainInfoStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write DomainInfoStub object as a string.", e);
        }
        
        return null;
    }
    public static DomainInfoStub fromJsonString(String jsonStr)
    {
        try {
            DomainInfoStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, DomainInfoStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into DomainInfoStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into DomainInfoStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into DomainInfoStub object.", e);
        }
        
        return null;
    }

}
