package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.FolderBase;
import com.myurldb.ws.util.JsonUtil;


// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class FolderBaseStub implements FolderBase, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(FolderBaseStub.class.getName());

    private String guid;
    private String appClient;
    private String clientRootDomain;
    private String user;
    private String title;
    private String description;
    private String type;
    private String category;
    private String parent;
    private String aggregate;
    private String acl;
    private Boolean exportable;
    private String status;
    private String note;
    private Long createdTime;
    private Long modifiedTime;

    public FolderBaseStub()
    {
        this(null);
    }
    public FolderBaseStub(FolderBase bean)
    {
        if(bean != null) {
            this.guid = bean.getGuid();
            this.appClient = bean.getAppClient();
            this.clientRootDomain = bean.getClientRootDomain();
            this.user = bean.getUser();
            this.title = bean.getTitle();
            this.description = bean.getDescription();
            this.type = bean.getType();
            this.category = bean.getCategory();
            this.parent = bean.getParent();
            this.aggregate = bean.getAggregate();
            this.acl = bean.getAcl();
            this.exportable = bean.isExportable();
            this.status = bean.getStatus();
            this.note = bean.getNote();
            this.createdTime = bean.getCreatedTime();
            this.modifiedTime = bean.getModifiedTime();
        }
    }


    @XmlTransient
    //@JsonIgnore
    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    @XmlTransient
    //@JsonIgnore
    public String getAppClient()
    {
        return this.appClient;
    }
    public void setAppClient(String appClient)
    {
        this.appClient = appClient;
    }

    @XmlTransient
    //@JsonIgnore
    public String getClientRootDomain()
    {
        return this.clientRootDomain;
    }
    public void setClientRootDomain(String clientRootDomain)
    {
        this.clientRootDomain = clientRootDomain;
    }

    @XmlTransient
    //@JsonIgnore
    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    @XmlTransient
    //@JsonIgnore
    public String getTitle()
    {
        return this.title;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }

    @XmlTransient
    //@JsonIgnore
    public String getDescription()
    {
        return this.description;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }

    @XmlTransient
    //@JsonIgnore
    public String getType()
    {
        return this.type;
    }
    public void setType(String type)
    {
        this.type = type;
    }

    @XmlTransient
    //@JsonIgnore
    public String getCategory()
    {
        return this.category;
    }
    public void setCategory(String category)
    {
        this.category = category;
    }

    @XmlTransient
    //@JsonIgnore
    public String getParent()
    {
        return this.parent;
    }
    public void setParent(String parent)
    {
        this.parent = parent;
    }

    @XmlTransient
    //@JsonIgnore
    public String getAggregate()
    {
        return this.aggregate;
    }
    public void setAggregate(String aggregate)
    {
        this.aggregate = aggregate;
    }

    @XmlTransient
    //@JsonIgnore
    public String getAcl()
    {
        return this.acl;
    }
    public void setAcl(String acl)
    {
        this.acl = acl;
    }

    @XmlTransient
    //@JsonIgnore
    public Boolean isExportable()
    {
        return this.exportable;
    }
    public void setExportable(Boolean exportable)
    {
        this.exportable = exportable;
    }

    @XmlTransient
    //@JsonIgnore
    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    @XmlTransient
    //@JsonIgnore
    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    @XmlTransient
    //@JsonIgnore
    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    @XmlTransient
    //@JsonIgnore
    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("guid", this.guid);
        dataMap.put("appClient", this.appClient);
        dataMap.put("clientRootDomain", this.clientRootDomain);
        dataMap.put("user", this.user);
        dataMap.put("title", this.title);
        dataMap.put("description", this.description);
        dataMap.put("type", this.type);
        dataMap.put("category", this.category);
        dataMap.put("parent", this.parent);
        dataMap.put("aggregate", this.aggregate);
        dataMap.put("acl", this.acl);
        dataMap.put("exportable", this.exportable);
        dataMap.put("status", this.status);
        dataMap.put("note", this.note);
        dataMap.put("createdTime", this.createdTime);
        dataMap.put("modifiedTime", this.modifiedTime);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = appClient == null ? 0 : appClient.hashCode();
        _hash = 31 * _hash + delta;
        delta = clientRootDomain == null ? 0 : clientRootDomain.hashCode();
        _hash = 31 * _hash + delta;
        delta = user == null ? 0 : user.hashCode();
        _hash = 31 * _hash + delta;
        delta = title == null ? 0 : title.hashCode();
        _hash = 31 * _hash + delta;
        delta = description == null ? 0 : description.hashCode();
        _hash = 31 * _hash + delta;
        delta = type == null ? 0 : type.hashCode();
        _hash = 31 * _hash + delta;
        delta = category == null ? 0 : category.hashCode();
        _hash = 31 * _hash + delta;
        delta = parent == null ? 0 : parent.hashCode();
        _hash = 31 * _hash + delta;
        delta = aggregate == null ? 0 : aggregate.hashCode();
        _hash = 31 * _hash + delta;
        delta = acl == null ? 0 : acl.hashCode();
        _hash = 31 * _hash + delta;
        delta = exportable == null ? 0 : exportable.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        delta = createdTime == null ? 0 : createdTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static FolderBaseStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of FolderBaseStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write FolderBaseStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write FolderBaseStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write FolderBaseStub object as a string.", e);
        }
        
        return null;
    }
    public static FolderBaseStub fromJsonString(String jsonStr)
    {
        try {
            FolderBaseStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, FolderBaseStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into FolderBaseStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into FolderBaseStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into FolderBaseStub object.", e);
        }
        
        return null;
    }

}
