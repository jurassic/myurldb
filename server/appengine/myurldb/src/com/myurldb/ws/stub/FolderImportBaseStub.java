package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.FolderImportBase;
import com.myurldb.ws.util.JsonUtil;


// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class FolderImportBaseStub implements FolderImportBase, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(FolderImportBaseStub.class.getName());

    private String guid;
    private String user;
    private Integer precedence;
    private String importType;
    private String importedFolder;
    private String importedFolderUser;
    private String importedFolderTitle;
    private String importedFolderPath;
    private String status;
    private String note;
    private Long createdTime;
    private Long modifiedTime;

    public FolderImportBaseStub()
    {
        this(null);
    }
    public FolderImportBaseStub(FolderImportBase bean)
    {
        if(bean != null) {
            this.guid = bean.getGuid();
            this.user = bean.getUser();
            this.precedence = bean.getPrecedence();
            this.importType = bean.getImportType();
            this.importedFolder = bean.getImportedFolder();
            this.importedFolderUser = bean.getImportedFolderUser();
            this.importedFolderTitle = bean.getImportedFolderTitle();
            this.importedFolderPath = bean.getImportedFolderPath();
            this.status = bean.getStatus();
            this.note = bean.getNote();
            this.createdTime = bean.getCreatedTime();
            this.modifiedTime = bean.getModifiedTime();
        }
    }


    @XmlTransient
    //@JsonIgnore
    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    @XmlTransient
    //@JsonIgnore
    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    @XmlTransient
    //@JsonIgnore
    public Integer getPrecedence()
    {
        return this.precedence;
    }
    public void setPrecedence(Integer precedence)
    {
        this.precedence = precedence;
    }

    @XmlTransient
    //@JsonIgnore
    public String getImportType()
    {
        return this.importType;
    }
    public void setImportType(String importType)
    {
        this.importType = importType;
    }

    @XmlTransient
    //@JsonIgnore
    public String getImportedFolder()
    {
        return this.importedFolder;
    }
    public void setImportedFolder(String importedFolder)
    {
        this.importedFolder = importedFolder;
    }

    @XmlTransient
    //@JsonIgnore
    public String getImportedFolderUser()
    {
        return this.importedFolderUser;
    }
    public void setImportedFolderUser(String importedFolderUser)
    {
        this.importedFolderUser = importedFolderUser;
    }

    @XmlTransient
    //@JsonIgnore
    public String getImportedFolderTitle()
    {
        return this.importedFolderTitle;
    }
    public void setImportedFolderTitle(String importedFolderTitle)
    {
        this.importedFolderTitle = importedFolderTitle;
    }

    @XmlTransient
    //@JsonIgnore
    public String getImportedFolderPath()
    {
        return this.importedFolderPath;
    }
    public void setImportedFolderPath(String importedFolderPath)
    {
        this.importedFolderPath = importedFolderPath;
    }

    @XmlTransient
    //@JsonIgnore
    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    @XmlTransient
    //@JsonIgnore
    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    @XmlTransient
    //@JsonIgnore
    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    @XmlTransient
    //@JsonIgnore
    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("guid", this.guid);
        dataMap.put("user", this.user);
        dataMap.put("precedence", this.precedence);
        dataMap.put("importType", this.importType);
        dataMap.put("importedFolder", this.importedFolder);
        dataMap.put("importedFolderUser", this.importedFolderUser);
        dataMap.put("importedFolderTitle", this.importedFolderTitle);
        dataMap.put("importedFolderPath", this.importedFolderPath);
        dataMap.put("status", this.status);
        dataMap.put("note", this.note);
        dataMap.put("createdTime", this.createdTime);
        dataMap.put("modifiedTime", this.modifiedTime);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = user == null ? 0 : user.hashCode();
        _hash = 31 * _hash + delta;
        delta = precedence == null ? 0 : precedence.hashCode();
        _hash = 31 * _hash + delta;
        delta = importType == null ? 0 : importType.hashCode();
        _hash = 31 * _hash + delta;
        delta = importedFolder == null ? 0 : importedFolder.hashCode();
        _hash = 31 * _hash + delta;
        delta = importedFolderUser == null ? 0 : importedFolderUser.hashCode();
        _hash = 31 * _hash + delta;
        delta = importedFolderTitle == null ? 0 : importedFolderTitle.hashCode();
        _hash = 31 * _hash + delta;
        delta = importedFolderPath == null ? 0 : importedFolderPath.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        delta = createdTime == null ? 0 : createdTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static FolderImportBaseStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of FolderImportBaseStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write FolderImportBaseStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write FolderImportBaseStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write FolderImportBaseStub object as a string.", e);
        }
        
        return null;
    }
    public static FolderImportBaseStub fromJsonString(String jsonStr)
    {
        try {
            FolderImportBaseStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, FolderImportBaseStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into FolderImportBaseStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into FolderImportBaseStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into FolderImportBaseStub object.", e);
        }
        
        return null;
    }

}
