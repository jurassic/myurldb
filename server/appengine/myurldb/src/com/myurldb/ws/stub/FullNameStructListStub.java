package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.FullNameStruct;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "fullNameStructs")
@XmlType(propOrder = {"fullNameStruct"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class FullNameStructListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(FullNameStructListStub.class.getName());

    private List<FullNameStructStub> fullNameStructs = null;

    public FullNameStructListStub()
    {
        this(new ArrayList<FullNameStructStub>());
    }
    public FullNameStructListStub(List<FullNameStructStub> fullNameStructs)
    {
        this.fullNameStructs = fullNameStructs;
    }

    public boolean isEmpty()
    {
        if(fullNameStructs == null) {
            return true;
        } else {
            return fullNameStructs.isEmpty();
        }
    }
    public int getSize()
    {
        if(fullNameStructs == null) {
            return 0;
        } else {
            return fullNameStructs.size();
        }
    }

    @XmlElement(name = "fullNameStruct")
    public List<FullNameStructStub> getFullNameStruct()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<FullNameStructStub> getList()
    {
        return fullNameStructs;
    }
    public void setList(List<FullNameStructStub> fullNameStructs)
    {
        this.fullNameStructs = fullNameStructs;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<FullNameStructStub> it = this.fullNameStructs.iterator();
        while(it.hasNext()) {
            FullNameStructStub fullNameStruct = it.next();
            sb.append(fullNameStruct.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static FullNameStructListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of FullNameStructListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write FullNameStructListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write FullNameStructListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write FullNameStructListStub object as a string.", e);
        }
        
        return null;
    }
    public static FullNameStructListStub fromJsonString(String jsonStr)
    {
        try {
            FullNameStructListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, FullNameStructListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into FullNameStructListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into FullNameStructListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into FullNameStructListStub object.", e);
        }
        
        return null;
    }

}
