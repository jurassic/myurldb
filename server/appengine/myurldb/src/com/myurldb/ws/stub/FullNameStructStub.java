package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.FullNameStruct;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "fullNameStruct")
@XmlType(propOrder = {"uuid", "displayName", "lastName", "firstName", "middleName1", "middleName2", "middleInitial", "salutation", "suffix", "note"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class FullNameStructStub implements FullNameStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(FullNameStructStub.class.getName());

    private String uuid;
    private String displayName;
    private String lastName;
    private String firstName;
    private String middleName1;
    private String middleName2;
    private String middleInitial;
    private String salutation;
    private String suffix;
    private String note;

    public FullNameStructStub()
    {
        this(null);
    }
    public FullNameStructStub(FullNameStruct bean)
    {
        if(bean != null) {
            this.uuid = bean.getUuid();
            this.displayName = bean.getDisplayName();
            this.lastName = bean.getLastName();
            this.firstName = bean.getFirstName();
            this.middleName1 = bean.getMiddleName1();
            this.middleName2 = bean.getMiddleName2();
            this.middleInitial = bean.getMiddleInitial();
            this.salutation = bean.getSalutation();
            this.suffix = bean.getSuffix();
            this.note = bean.getNote();
        }
    }


    @XmlElement
    public String getUuid()
    {
        return this.uuid;
    }
    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }

    @XmlElement
    public String getDisplayName()
    {
        return this.displayName;
    }
    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }

    @XmlElement
    public String getLastName()
    {
        return this.lastName;
    }
    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    @XmlElement
    public String getFirstName()
    {
        return this.firstName;
    }
    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    @XmlElement
    public String getMiddleName1()
    {
        return this.middleName1;
    }
    public void setMiddleName1(String middleName1)
    {
        this.middleName1 = middleName1;
    }

    @XmlElement
    public String getMiddleName2()
    {
        return this.middleName2;
    }
    public void setMiddleName2(String middleName2)
    {
        this.middleName2 = middleName2;
    }

    @XmlElement
    public String getMiddleInitial()
    {
        return this.middleInitial;
    }
    public void setMiddleInitial(String middleInitial)
    {
        this.middleInitial = middleInitial;
    }

    @XmlElement
    public String getSalutation()
    {
        return this.salutation;
    }
    public void setSalutation(String salutation)
    {
        this.salutation = salutation;
    }

    @XmlElement
    public String getSuffix()
    {
        return this.suffix;
    }
    public void setSuffix(String suffix)
    {
        this.suffix = suffix;
    }

    @XmlElement
    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }


    @XmlTransient
    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getDisplayName() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLastName() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getFirstName() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getMiddleName1() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getMiddleName2() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getMiddleInitial() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSalutation() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSuffix() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("uuid", this.uuid);
        dataMap.put("displayName", this.displayName);
        dataMap.put("lastName", this.lastName);
        dataMap.put("firstName", this.firstName);
        dataMap.put("middleName1", this.middleName1);
        dataMap.put("middleName2", this.middleName2);
        dataMap.put("middleInitial", this.middleInitial);
        dataMap.put("salutation", this.salutation);
        dataMap.put("suffix", this.suffix);
        dataMap.put("note", this.note);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = uuid == null ? 0 : uuid.hashCode();
        _hash = 31 * _hash + delta;
        delta = displayName == null ? 0 : displayName.hashCode();
        _hash = 31 * _hash + delta;
        delta = lastName == null ? 0 : lastName.hashCode();
        _hash = 31 * _hash + delta;
        delta = firstName == null ? 0 : firstName.hashCode();
        _hash = 31 * _hash + delta;
        delta = middleName1 == null ? 0 : middleName1.hashCode();
        _hash = 31 * _hash + delta;
        delta = middleName2 == null ? 0 : middleName2.hashCode();
        _hash = 31 * _hash + delta;
        delta = middleInitial == null ? 0 : middleInitial.hashCode();
        _hash = 31 * _hash + delta;
        delta = salutation == null ? 0 : salutation.hashCode();
        _hash = 31 * _hash + delta;
        delta = suffix == null ? 0 : suffix.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static FullNameStructStub convertBeanToStub(FullNameStruct bean)
    {
        FullNameStructStub stub = null;
        if(bean instanceof FullNameStructStub) {
            stub = (FullNameStructStub) bean;
        } else {
            if(bean != null) {
                stub = new FullNameStructStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static FullNameStructStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of FullNameStructStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write FullNameStructStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write FullNameStructStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write FullNameStructStub object as a string.", e);
        }
        
        return null;
    }
    public static FullNameStructStub fromJsonString(String jsonStr)
    {
        try {
            FullNameStructStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, FullNameStructStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into FullNameStructStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into FullNameStructStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into FullNameStructStub object.", e);
        }
        
        return null;
    }

}
