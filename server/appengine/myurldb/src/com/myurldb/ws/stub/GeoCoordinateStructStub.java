package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.GeoCoordinateStruct;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "geoCoordinateStruct")
@XmlType(propOrder = {"uuid", "latitude", "longitude", "altitude", "sensorUsed", "accuracy", "altitudeAccuracy", "heading", "speed", "note"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class GeoCoordinateStructStub implements GeoCoordinateStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(GeoCoordinateStructStub.class.getName());

    private String uuid;
    private Double latitude;
    private Double longitude;
    private Double altitude;
    private Boolean sensorUsed;
    private String accuracy;
    private String altitudeAccuracy;
    private Double heading;
    private Double speed;
    private String note;

    public GeoCoordinateStructStub()
    {
        this(null);
    }
    public GeoCoordinateStructStub(GeoCoordinateStruct bean)
    {
        if(bean != null) {
            this.uuid = bean.getUuid();
            this.latitude = bean.getLatitude();
            this.longitude = bean.getLongitude();
            this.altitude = bean.getAltitude();
            this.sensorUsed = bean.isSensorUsed();
            this.accuracy = bean.getAccuracy();
            this.altitudeAccuracy = bean.getAltitudeAccuracy();
            this.heading = bean.getHeading();
            this.speed = bean.getSpeed();
            this.note = bean.getNote();
        }
    }


    @XmlElement
    public String getUuid()
    {
        return this.uuid;
    }
    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }

    @XmlElement
    public Double getLatitude()
    {
        return this.latitude;
    }
    public void setLatitude(Double latitude)
    {
        this.latitude = latitude;
    }

    @XmlElement
    public Double getLongitude()
    {
        return this.longitude;
    }
    public void setLongitude(Double longitude)
    {
        this.longitude = longitude;
    }

    @XmlElement
    public Double getAltitude()
    {
        return this.altitude;
    }
    public void setAltitude(Double altitude)
    {
        this.altitude = altitude;
    }

    @XmlElement
    public Boolean isSensorUsed()
    {
        return this.sensorUsed;
    }
    public void setSensorUsed(Boolean sensorUsed)
    {
        this.sensorUsed = sensorUsed;
    }

    @XmlElement
    public String getAccuracy()
    {
        return this.accuracy;
    }
    public void setAccuracy(String accuracy)
    {
        this.accuracy = accuracy;
    }

    @XmlElement
    public String getAltitudeAccuracy()
    {
        return this.altitudeAccuracy;
    }
    public void setAltitudeAccuracy(String altitudeAccuracy)
    {
        this.altitudeAccuracy = altitudeAccuracy;
    }

    @XmlElement
    public Double getHeading()
    {
        return this.heading;
    }
    public void setHeading(Double heading)
    {
        this.heading = heading;
    }

    @XmlElement
    public Double getSpeed()
    {
        return this.speed;
    }
    public void setSpeed(Double speed)
    {
        this.speed = speed;
    }

    @XmlElement
    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }


    @XmlTransient
    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getLatitude() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLongitude() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAltitude() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && isSensorUsed() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAccuracy() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAltitudeAccuracy() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHeading() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSpeed() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("uuid", this.uuid);
        dataMap.put("latitude", this.latitude);
        dataMap.put("longitude", this.longitude);
        dataMap.put("altitude", this.altitude);
        dataMap.put("sensorUsed", this.sensorUsed);
        dataMap.put("accuracy", this.accuracy);
        dataMap.put("altitudeAccuracy", this.altitudeAccuracy);
        dataMap.put("heading", this.heading);
        dataMap.put("speed", this.speed);
        dataMap.put("note", this.note);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = uuid == null ? 0 : uuid.hashCode();
        _hash = 31 * _hash + delta;
        delta = latitude == null ? 0 : latitude.hashCode();
        _hash = 31 * _hash + delta;
        delta = longitude == null ? 0 : longitude.hashCode();
        _hash = 31 * _hash + delta;
        delta = altitude == null ? 0 : altitude.hashCode();
        _hash = 31 * _hash + delta;
        delta = sensorUsed == null ? 0 : sensorUsed.hashCode();
        _hash = 31 * _hash + delta;
        delta = accuracy == null ? 0 : accuracy.hashCode();
        _hash = 31 * _hash + delta;
        delta = altitudeAccuracy == null ? 0 : altitudeAccuracy.hashCode();
        _hash = 31 * _hash + delta;
        delta = heading == null ? 0 : heading.hashCode();
        _hash = 31 * _hash + delta;
        delta = speed == null ? 0 : speed.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static GeoCoordinateStructStub convertBeanToStub(GeoCoordinateStruct bean)
    {
        GeoCoordinateStructStub stub = null;
        if(bean instanceof GeoCoordinateStructStub) {
            stub = (GeoCoordinateStructStub) bean;
        } else {
            if(bean != null) {
                stub = new GeoCoordinateStructStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static GeoCoordinateStructStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of GeoCoordinateStructStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write GeoCoordinateStructStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write GeoCoordinateStructStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write GeoCoordinateStructStub object as a string.", e);
        }
        
        return null;
    }
    public static GeoCoordinateStructStub fromJsonString(String jsonStr)
    {
        try {
            GeoCoordinateStructStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, GeoCoordinateStructStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into GeoCoordinateStructStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into GeoCoordinateStructStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into GeoCoordinateStructStub object.", e);
        }
        
        return null;
    }

}
