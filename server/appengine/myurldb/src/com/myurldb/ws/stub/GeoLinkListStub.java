package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.GeoLink;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "geoLinks")
@XmlType(propOrder = {"geoLink"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class GeoLinkListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(GeoLinkListStub.class.getName());

    private List<GeoLinkStub> geoLinks = null;

    public GeoLinkListStub()
    {
        this(new ArrayList<GeoLinkStub>());
    }
    public GeoLinkListStub(List<GeoLinkStub> geoLinks)
    {
        this.geoLinks = geoLinks;
    }

    public boolean isEmpty()
    {
        if(geoLinks == null) {
            return true;
        } else {
            return geoLinks.isEmpty();
        }
    }
    public int getSize()
    {
        if(geoLinks == null) {
            return 0;
        } else {
            return geoLinks.size();
        }
    }

    @XmlElement(name = "geoLink")
    public List<GeoLinkStub> getGeoLink()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<GeoLinkStub> getList()
    {
        return geoLinks;
    }
    public void setList(List<GeoLinkStub> geoLinks)
    {
        this.geoLinks = geoLinks;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<GeoLinkStub> it = this.geoLinks.iterator();
        while(it.hasNext()) {
            GeoLinkStub geoLink = it.next();
            sb.append(geoLink.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static GeoLinkListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of GeoLinkListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write GeoLinkListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write GeoLinkListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write GeoLinkListStub object as a string.", e);
        }
        
        return null;
    }
    public static GeoLinkListStub fromJsonString(String jsonStr)
    {
        try {
            GeoLinkListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, GeoLinkListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into GeoLinkListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into GeoLinkListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into GeoLinkListStub object.", e);
        }
        
        return null;
    }

}
