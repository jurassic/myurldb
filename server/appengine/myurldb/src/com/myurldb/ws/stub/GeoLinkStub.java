package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.CellLatitudeLongitude;
import com.myurldb.ws.GeoCoordinateStruct;
import com.myurldb.ws.GeoLink;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "geoLink")
@XmlType(propOrder = {"guid", "shortLink", "shortUrl", "geoCoordinateStub", "geoCellStub", "status", "createdTime", "modifiedTime"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class GeoLinkStub implements GeoLink, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(GeoLinkStub.class.getName());

    private String guid;
    private String shortLink;
    private String shortUrl;
    private GeoCoordinateStructStub geoCoordinate;
    private CellLatitudeLongitudeStub geoCell;
    private String status;
    private Long createdTime;
    private Long modifiedTime;

    public GeoLinkStub()
    {
        this(null);
    }
    public GeoLinkStub(GeoLink bean)
    {
        if(bean != null) {
            this.guid = bean.getGuid();
            this.shortLink = bean.getShortLink();
            this.shortUrl = bean.getShortUrl();
            this.geoCoordinate = GeoCoordinateStructStub.convertBeanToStub(bean.getGeoCoordinate());
            this.geoCell = CellLatitudeLongitudeStub.convertBeanToStub(bean.getGeoCell());
            this.status = bean.getStatus();
            this.createdTime = bean.getCreatedTime();
            this.modifiedTime = bean.getModifiedTime();
        }
    }


    @XmlElement
    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    @XmlElement
    public String getShortLink()
    {
        return this.shortLink;
    }
    public void setShortLink(String shortLink)
    {
        this.shortLink = shortLink;
    }

    @XmlElement
    public String getShortUrl()
    {
        return this.shortUrl;
    }
    public void setShortUrl(String shortUrl)
    {
        this.shortUrl = shortUrl;
    }

    @XmlElement(name = "geoCoordinate")
    @JsonIgnore
    public GeoCoordinateStructStub getGeoCoordinateStub()
    {
        return this.geoCoordinate;
    }
    public void setGeoCoordinateStub(GeoCoordinateStructStub geoCoordinate)
    {
        this.geoCoordinate = geoCoordinate;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=GeoCoordinateStructStub.class)
    public GeoCoordinateStruct getGeoCoordinate()
    {  
        return getGeoCoordinateStub();
    }
    public void setGeoCoordinate(GeoCoordinateStruct geoCoordinate)
    {
        if((geoCoordinate == null) || (geoCoordinate instanceof GeoCoordinateStructStub)) {
            setGeoCoordinateStub((GeoCoordinateStructStub) geoCoordinate);
        } else {
            // TBD
            setGeoCoordinateStub(GeoCoordinateStructStub.convertBeanToStub(geoCoordinate));
        }
    }

    @XmlElement(name = "geoCell")
    @JsonIgnore
    public CellLatitudeLongitudeStub getGeoCellStub()
    {
        return this.geoCell;
    }
    public void setGeoCellStub(CellLatitudeLongitudeStub geoCell)
    {
        this.geoCell = geoCell;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=CellLatitudeLongitudeStub.class)
    public CellLatitudeLongitude getGeoCell()
    {  
        return getGeoCellStub();
    }
    public void setGeoCell(CellLatitudeLongitude geoCell)
    {
        if((geoCell == null) || (geoCell instanceof CellLatitudeLongitudeStub)) {
            setGeoCellStub((CellLatitudeLongitudeStub) geoCell);
        } else {
            // TBD
            setGeoCellStub(CellLatitudeLongitudeStub.convertBeanToStub(geoCell));
        }
    }

    @XmlElement
    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("guid", this.guid);
        dataMap.put("shortLink", this.shortLink);
        dataMap.put("shortUrl", this.shortUrl);
        dataMap.put("geoCoordinate", this.geoCoordinate);
        dataMap.put("geoCell", this.geoCell);
        dataMap.put("status", this.status);
        dataMap.put("createdTime", this.createdTime);
        dataMap.put("modifiedTime", this.modifiedTime);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = shortLink == null ? 0 : shortLink.hashCode();
        _hash = 31 * _hash + delta;
        delta = shortUrl == null ? 0 : shortUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = geoCoordinate == null ? 0 : geoCoordinate.hashCode();
        _hash = 31 * _hash + delta;
        delta = geoCell == null ? 0 : geoCell.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = createdTime == null ? 0 : createdTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static GeoLinkStub convertBeanToStub(GeoLink bean)
    {
        GeoLinkStub stub = null;
        if(bean instanceof GeoLinkStub) {
            stub = (GeoLinkStub) bean;
        } else {
            if(bean != null) {
                stub = new GeoLinkStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static GeoLinkStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of GeoLinkStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write GeoLinkStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write GeoLinkStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write GeoLinkStub object as a string.", e);
        }
        
        return null;
    }
    public static GeoLinkStub fromJsonString(String jsonStr)
    {
        try {
            GeoLinkStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, GeoLinkStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into GeoLinkStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into GeoLinkStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into GeoLinkStub object.", e);
        }
        
        return null;
    }

}
