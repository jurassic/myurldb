package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.GeoPointStruct;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "geoPointStructs")
@XmlType(propOrder = {"geoPointStruct"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class GeoPointStructListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(GeoPointStructListStub.class.getName());

    private List<GeoPointStructStub> geoPointStructs = null;

    public GeoPointStructListStub()
    {
        this(new ArrayList<GeoPointStructStub>());
    }
    public GeoPointStructListStub(List<GeoPointStructStub> geoPointStructs)
    {
        this.geoPointStructs = geoPointStructs;
    }

    public boolean isEmpty()
    {
        if(geoPointStructs == null) {
            return true;
        } else {
            return geoPointStructs.isEmpty();
        }
    }
    public int getSize()
    {
        if(geoPointStructs == null) {
            return 0;
        } else {
            return geoPointStructs.size();
        }
    }

    @XmlElement(name = "geoPointStruct")
    public List<GeoPointStructStub> getGeoPointStruct()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<GeoPointStructStub> getList()
    {
        return geoPointStructs;
    }
    public void setList(List<GeoPointStructStub> geoPointStructs)
    {
        this.geoPointStructs = geoPointStructs;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<GeoPointStructStub> it = this.geoPointStructs.iterator();
        while(it.hasNext()) {
            GeoPointStructStub geoPointStruct = it.next();
            sb.append(geoPointStruct.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static GeoPointStructListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of GeoPointStructListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write GeoPointStructListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write GeoPointStructListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write GeoPointStructListStub object as a string.", e);
        }
        
        return null;
    }
    public static GeoPointStructListStub fromJsonString(String jsonStr)
    {
        try {
            GeoPointStructListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, GeoPointStructListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into GeoPointStructListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into GeoPointStructListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into GeoPointStructListStub object.", e);
        }
        
        return null;
    }

}
