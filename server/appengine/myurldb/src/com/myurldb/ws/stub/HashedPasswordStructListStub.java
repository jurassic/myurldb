package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.HashedPasswordStruct;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "hashedPasswordStructs")
@XmlType(propOrder = {"hashedPasswordStruct"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class HashedPasswordStructListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(HashedPasswordStructListStub.class.getName());

    private List<HashedPasswordStructStub> hashedPasswordStructs = null;

    public HashedPasswordStructListStub()
    {
        this(new ArrayList<HashedPasswordStructStub>());
    }
    public HashedPasswordStructListStub(List<HashedPasswordStructStub> hashedPasswordStructs)
    {
        this.hashedPasswordStructs = hashedPasswordStructs;
    }

    public boolean isEmpty()
    {
        if(hashedPasswordStructs == null) {
            return true;
        } else {
            return hashedPasswordStructs.isEmpty();
        }
    }
    public int getSize()
    {
        if(hashedPasswordStructs == null) {
            return 0;
        } else {
            return hashedPasswordStructs.size();
        }
    }

    @XmlElement(name = "hashedPasswordStruct")
    public List<HashedPasswordStructStub> getHashedPasswordStruct()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<HashedPasswordStructStub> getList()
    {
        return hashedPasswordStructs;
    }
    public void setList(List<HashedPasswordStructStub> hashedPasswordStructs)
    {
        this.hashedPasswordStructs = hashedPasswordStructs;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<HashedPasswordStructStub> it = this.hashedPasswordStructs.iterator();
        while(it.hasNext()) {
            HashedPasswordStructStub hashedPasswordStruct = it.next();
            sb.append(hashedPasswordStruct.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static HashedPasswordStructListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of HashedPasswordStructListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write HashedPasswordStructListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write HashedPasswordStructListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write HashedPasswordStructListStub object as a string.", e);
        }
        
        return null;
    }
    public static HashedPasswordStructListStub fromJsonString(String jsonStr)
    {
        try {
            HashedPasswordStructListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, HashedPasswordStructListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into HashedPasswordStructListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into HashedPasswordStructListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into HashedPasswordStructListStub object.", e);
        }
        
        return null;
    }

}
