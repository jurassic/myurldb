package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.HashedPasswordStruct;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "hashedPasswordStruct")
@XmlType(propOrder = {"uuid", "plainText", "hashedText", "salt", "algorithm"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class HashedPasswordStructStub implements HashedPasswordStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(HashedPasswordStructStub.class.getName());

    private String uuid;
    private String plainText;
    private String hashedText;
    private String salt;
    private String algorithm;

    public HashedPasswordStructStub()
    {
        this(null);
    }
    public HashedPasswordStructStub(HashedPasswordStruct bean)
    {
        if(bean != null) {
            this.uuid = bean.getUuid();
            this.plainText = bean.getPlainText();
            this.hashedText = bean.getHashedText();
            this.salt = bean.getSalt();
            this.algorithm = bean.getAlgorithm();
        }
    }


    @XmlElement
    public String getUuid()
    {
        return this.uuid;
    }
    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }

    @XmlElement
    public String getPlainText()
    {
        return this.plainText;
    }
    public void setPlainText(String plainText)
    {
        this.plainText = plainText;
    }

    @XmlElement
    public String getHashedText()
    {
        return this.hashedText;
    }
    public void setHashedText(String hashedText)
    {
        this.hashedText = hashedText;
    }

    @XmlElement
    public String getSalt()
    {
        return this.salt;
    }
    public void setSalt(String salt)
    {
        this.salt = salt;
    }

    @XmlElement
    public String getAlgorithm()
    {
        return this.algorithm;
    }
    public void setAlgorithm(String algorithm)
    {
        this.algorithm = algorithm;
    }


    @XmlTransient
    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getPlainText() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHashedText() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSalt() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAlgorithm() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("uuid", this.uuid);
        dataMap.put("plainText", this.plainText);
        dataMap.put("hashedText", this.hashedText);
        dataMap.put("salt", this.salt);
        dataMap.put("algorithm", this.algorithm);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = uuid == null ? 0 : uuid.hashCode();
        _hash = 31 * _hash + delta;
        delta = plainText == null ? 0 : plainText.hashCode();
        _hash = 31 * _hash + delta;
        delta = hashedText == null ? 0 : hashedText.hashCode();
        _hash = 31 * _hash + delta;
        delta = salt == null ? 0 : salt.hashCode();
        _hash = 31 * _hash + delta;
        delta = algorithm == null ? 0 : algorithm.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static HashedPasswordStructStub convertBeanToStub(HashedPasswordStruct bean)
    {
        HashedPasswordStructStub stub = null;
        if(bean instanceof HashedPasswordStructStub) {
            stub = (HashedPasswordStructStub) bean;
        } else {
            if(bean != null) {
                stub = new HashedPasswordStructStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static HashedPasswordStructStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of HashedPasswordStructStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write HashedPasswordStructStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write HashedPasswordStructStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write HashedPasswordStructStub object as a string.", e);
        }
        
        return null;
    }
    public static HashedPasswordStructStub fromJsonString(String jsonStr)
    {
        try {
            HashedPasswordStructStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, HashedPasswordStructStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into HashedPasswordStructStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into HashedPasswordStructStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into HashedPasswordStructStub object.", e);
        }
        
        return null;
    }

}
