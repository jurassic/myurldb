package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.HelpNotice;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "helpNotices")
@XmlType(propOrder = {"helpNotice"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class HelpNoticeListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(HelpNoticeListStub.class.getName());

    private List<HelpNoticeStub> helpNotices = null;

    public HelpNoticeListStub()
    {
        this(new ArrayList<HelpNoticeStub>());
    }
    public HelpNoticeListStub(List<HelpNoticeStub> helpNotices)
    {
        this.helpNotices = helpNotices;
    }

    public boolean isEmpty()
    {
        if(helpNotices == null) {
            return true;
        } else {
            return helpNotices.isEmpty();
        }
    }
    public int getSize()
    {
        if(helpNotices == null) {
            return 0;
        } else {
            return helpNotices.size();
        }
    }

    @XmlElement(name = "helpNotice")
    public List<HelpNoticeStub> getHelpNotice()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<HelpNoticeStub> getList()
    {
        return helpNotices;
    }
    public void setList(List<HelpNoticeStub> helpNotices)
    {
        this.helpNotices = helpNotices;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<HelpNoticeStub> it = this.helpNotices.iterator();
        while(it.hasNext()) {
            HelpNoticeStub helpNotice = it.next();
            sb.append(helpNotice.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static HelpNoticeListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of HelpNoticeListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write HelpNoticeListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write HelpNoticeListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write HelpNoticeListStub object as a string.", e);
        }
        
        return null;
    }
    public static HelpNoticeListStub fromJsonString(String jsonStr)
    {
        try {
            HelpNoticeListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, HelpNoticeListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into HelpNoticeListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into HelpNoticeListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into HelpNoticeListStub object.", e);
        }
        
        return null;
    }

}
