package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.KeywordCrowdTally;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "keywordCrowdTallies")
@XmlType(propOrder = {"keywordCrowdTally"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class KeywordCrowdTallyListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(KeywordCrowdTallyListStub.class.getName());

    private List<KeywordCrowdTallyStub> keywordCrowdTallies = null;

    public KeywordCrowdTallyListStub()
    {
        this(new ArrayList<KeywordCrowdTallyStub>());
    }
    public KeywordCrowdTallyListStub(List<KeywordCrowdTallyStub> keywordCrowdTallies)
    {
        this.keywordCrowdTallies = keywordCrowdTallies;
    }

    public boolean isEmpty()
    {
        if(keywordCrowdTallies == null) {
            return true;
        } else {
            return keywordCrowdTallies.isEmpty();
        }
    }
    public int getSize()
    {
        if(keywordCrowdTallies == null) {
            return 0;
        } else {
            return keywordCrowdTallies.size();
        }
    }

    @XmlElement(name = "keywordCrowdTally")
    public List<KeywordCrowdTallyStub> getKeywordCrowdTally()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<KeywordCrowdTallyStub> getList()
    {
        return keywordCrowdTallies;
    }
    public void setList(List<KeywordCrowdTallyStub> keywordCrowdTallies)
    {
        this.keywordCrowdTallies = keywordCrowdTallies;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<KeywordCrowdTallyStub> it = this.keywordCrowdTallies.iterator();
        while(it.hasNext()) {
            KeywordCrowdTallyStub keywordCrowdTally = it.next();
            sb.append(keywordCrowdTally.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static KeywordCrowdTallyListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of KeywordCrowdTallyListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write KeywordCrowdTallyListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write KeywordCrowdTallyListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write KeywordCrowdTallyListStub object as a string.", e);
        }
        
        return null;
    }
    public static KeywordCrowdTallyListStub fromJsonString(String jsonStr)
    {
        try {
            KeywordCrowdTallyListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, KeywordCrowdTallyListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into KeywordCrowdTallyListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into KeywordCrowdTallyListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into KeywordCrowdTallyListStub object.", e);
        }
        
        return null;
    }

}
