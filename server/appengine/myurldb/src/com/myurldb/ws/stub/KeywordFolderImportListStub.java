package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.KeywordFolderImport;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "keywordFolderImports")
@XmlType(propOrder = {"keywordFolderImport"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class KeywordFolderImportListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(KeywordFolderImportListStub.class.getName());

    private List<KeywordFolderImportStub> keywordFolderImports = null;

    public KeywordFolderImportListStub()
    {
        this(new ArrayList<KeywordFolderImportStub>());
    }
    public KeywordFolderImportListStub(List<KeywordFolderImportStub> keywordFolderImports)
    {
        this.keywordFolderImports = keywordFolderImports;
    }

    public boolean isEmpty()
    {
        if(keywordFolderImports == null) {
            return true;
        } else {
            return keywordFolderImports.isEmpty();
        }
    }
    public int getSize()
    {
        if(keywordFolderImports == null) {
            return 0;
        } else {
            return keywordFolderImports.size();
        }
    }

    @XmlElement(name = "keywordFolderImport")
    public List<KeywordFolderImportStub> getKeywordFolderImport()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<KeywordFolderImportStub> getList()
    {
        return keywordFolderImports;
    }
    public void setList(List<KeywordFolderImportStub> keywordFolderImports)
    {
        this.keywordFolderImports = keywordFolderImports;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<KeywordFolderImportStub> it = this.keywordFolderImports.iterator();
        while(it.hasNext()) {
            KeywordFolderImportStub keywordFolderImport = it.next();
            sb.append(keywordFolderImport.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static KeywordFolderImportListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of KeywordFolderImportListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write KeywordFolderImportListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write KeywordFolderImportListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write KeywordFolderImportListStub object as a string.", e);
        }
        
        return null;
    }
    public static KeywordFolderImportListStub fromJsonString(String jsonStr)
    {
        try {
            KeywordFolderImportListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, KeywordFolderImportListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into KeywordFolderImportListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into KeywordFolderImportListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into KeywordFolderImportListStub object.", e);
        }
        
        return null;
    }

}
