package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.KeywordFolder;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "keywordFolders")
@XmlType(propOrder = {"keywordFolder"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class KeywordFolderListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(KeywordFolderListStub.class.getName());

    private List<KeywordFolderStub> keywordFolders = null;

    public KeywordFolderListStub()
    {
        this(new ArrayList<KeywordFolderStub>());
    }
    public KeywordFolderListStub(List<KeywordFolderStub> keywordFolders)
    {
        this.keywordFolders = keywordFolders;
    }

    public boolean isEmpty()
    {
        if(keywordFolders == null) {
            return true;
        } else {
            return keywordFolders.isEmpty();
        }
    }
    public int getSize()
    {
        if(keywordFolders == null) {
            return 0;
        } else {
            return keywordFolders.size();
        }
    }

    @XmlElement(name = "keywordFolder")
    public List<KeywordFolderStub> getKeywordFolder()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<KeywordFolderStub> getList()
    {
        return keywordFolders;
    }
    public void setList(List<KeywordFolderStub> keywordFolders)
    {
        this.keywordFolders = keywordFolders;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<KeywordFolderStub> it = this.keywordFolders.iterator();
        while(it.hasNext()) {
            KeywordFolderStub keywordFolder = it.next();
            sb.append(keywordFolder.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static KeywordFolderListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of KeywordFolderListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write KeywordFolderListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write KeywordFolderListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write KeywordFolderListStub object as a string.", e);
        }
        
        return null;
    }
    public static KeywordFolderListStub fromJsonString(String jsonStr)
    {
        try {
            KeywordFolderListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, KeywordFolderListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into KeywordFolderListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into KeywordFolderListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into KeywordFolderListStub object.", e);
        }
        
        return null;
    }

}
