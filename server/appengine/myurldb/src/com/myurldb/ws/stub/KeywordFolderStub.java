package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.KeywordFolder;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "keywordFolder")
@XmlType(propOrder = {"guid", "appClient", "clientRootDomain", "user", "title", "description", "type", "category", "parent", "aggregate", "acl", "exportable", "status", "note", "folderPath", "createdTime", "modifiedTime"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class KeywordFolderStub extends FolderBaseStub implements KeywordFolder, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(KeywordFolderStub.class.getName());

    private String folderPath;

    public KeywordFolderStub()
    {
        this(null);
    }
    public KeywordFolderStub(KeywordFolder bean)
    {
        super(bean);
        if(bean != null) {
            this.folderPath = bean.getFolderPath();
        }
    }


    @XmlElement
    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    @XmlElement
    public String getAppClient()
    {
        return super.getAppClient();
    }
    public void setAppClient(String appClient)
    {
        super.setAppClient(appClient);
    }

    @XmlElement
    public String getClientRootDomain()
    {
        return super.getClientRootDomain();
    }
    public void setClientRootDomain(String clientRootDomain)
    {
        super.setClientRootDomain(clientRootDomain);
    }

    @XmlElement
    public String getUser()
    {
        return super.getUser();
    }
    public void setUser(String user)
    {
        super.setUser(user);
    }

    @XmlElement
    public String getTitle()
    {
        return super.getTitle();
    }
    public void setTitle(String title)
    {
        super.setTitle(title);
    }

    @XmlElement
    public String getDescription()
    {
        return super.getDescription();
    }
    public void setDescription(String description)
    {
        super.setDescription(description);
    }

    @XmlElement
    public String getType()
    {
        return super.getType();
    }
    public void setType(String type)
    {
        super.setType(type);
    }

    @XmlElement
    public String getCategory()
    {
        return super.getCategory();
    }
    public void setCategory(String category)
    {
        super.setCategory(category);
    }

    @XmlElement
    public String getParent()
    {
        return super.getParent();
    }
    public void setParent(String parent)
    {
        super.setParent(parent);
    }

    @XmlElement
    public String getAggregate()
    {
        return super.getAggregate();
    }
    public void setAggregate(String aggregate)
    {
        super.setAggregate(aggregate);
    }

    @XmlElement
    public String getAcl()
    {
        return super.getAcl();
    }
    public void setAcl(String acl)
    {
        super.setAcl(acl);
    }

    @XmlElement
    public Boolean isExportable()
    {
        return super.isExportable();
    }
    public void setExportable(Boolean exportable)
    {
        super.setExportable(exportable);
    }

    @XmlElement
    public String getStatus()
    {
        return super.getStatus();
    }
    public void setStatus(String status)
    {
        super.setStatus(status);
    }

    @XmlElement
    public String getNote()
    {
        return super.getNote();
    }
    public void setNote(String note)
    {
        super.setNote(note);
    }

    @XmlElement
    public String getFolderPath()
    {
        return this.folderPath;
    }
    public void setFolderPath(String folderPath)
    {
        this.folderPath = folderPath;
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("folderPath", this.folderPath);

        return dataMap;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = folderPath == null ? 0 : folderPath.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static KeywordFolderStub convertBeanToStub(KeywordFolder bean)
    {
        KeywordFolderStub stub = null;
        if(bean instanceof KeywordFolderStub) {
            stub = (KeywordFolderStub) bean;
        } else {
            if(bean != null) {
                stub = new KeywordFolderStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static KeywordFolderStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of KeywordFolderStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write KeywordFolderStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write KeywordFolderStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write KeywordFolderStub object as a string.", e);
        }
        
        return null;
    }
    public static KeywordFolderStub fromJsonString(String jsonStr)
    {
        try {
            KeywordFolderStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, KeywordFolderStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into KeywordFolderStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into KeywordFolderStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into KeywordFolderStub object.", e);
        }
        
        return null;
    }

}
