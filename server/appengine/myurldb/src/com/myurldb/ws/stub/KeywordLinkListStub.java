package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.KeywordLink;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "keywordLinks")
@XmlType(propOrder = {"keywordLink"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class KeywordLinkListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(KeywordLinkListStub.class.getName());

    private List<KeywordLinkStub> keywordLinks = null;

    public KeywordLinkListStub()
    {
        this(new ArrayList<KeywordLinkStub>());
    }
    public KeywordLinkListStub(List<KeywordLinkStub> keywordLinks)
    {
        this.keywordLinks = keywordLinks;
    }

    public boolean isEmpty()
    {
        if(keywordLinks == null) {
            return true;
        } else {
            return keywordLinks.isEmpty();
        }
    }
    public int getSize()
    {
        if(keywordLinks == null) {
            return 0;
        } else {
            return keywordLinks.size();
        }
    }

    @XmlElement(name = "keywordLink")
    public List<KeywordLinkStub> getKeywordLink()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<KeywordLinkStub> getList()
    {
        return keywordLinks;
    }
    public void setList(List<KeywordLinkStub> keywordLinks)
    {
        this.keywordLinks = keywordLinks;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<KeywordLinkStub> it = this.keywordLinks.iterator();
        while(it.hasNext()) {
            KeywordLinkStub keywordLink = it.next();
            sb.append(keywordLink.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static KeywordLinkListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of KeywordLinkListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write KeywordLinkListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write KeywordLinkListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write KeywordLinkListStub object as a string.", e);
        }
        
        return null;
    }
    public static KeywordLinkListStub fromJsonString(String jsonStr)
    {
        try {
            KeywordLinkListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, KeywordLinkListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into KeywordLinkListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into KeywordLinkListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into KeywordLinkListStub object.", e);
        }
        
        return null;
    }

}
