package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.KeywordLink;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "keywordLink")
@XmlType(propOrder = {"guid", "appClient", "clientRootDomain", "user", "shortLink", "domain", "token", "longUrl", "shortUrl", "internal", "caching", "memo", "status", "note", "expirationTime", "keywordFolder", "folderPath", "keyword", "queryKey", "scope", "dynamic", "caseSensitive", "createdTime", "modifiedTime"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class KeywordLinkStub extends NavLinkBaseStub implements KeywordLink, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(KeywordLinkStub.class.getName());

    private String keywordFolder;
    private String folderPath;
    private String keyword;
    private String queryKey;
    private String scope;
    private Boolean dynamic;
    private Boolean caseSensitive;

    public KeywordLinkStub()
    {
        this(null);
    }
    public KeywordLinkStub(KeywordLink bean)
    {
        super(bean);
        if(bean != null) {
            this.keywordFolder = bean.getKeywordFolder();
            this.folderPath = bean.getFolderPath();
            this.keyword = bean.getKeyword();
            this.queryKey = bean.getQueryKey();
            this.scope = bean.getScope();
            this.dynamic = bean.isDynamic();
            this.caseSensitive = bean.isCaseSensitive();
        }
    }


    @XmlElement
    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    @XmlElement
    public String getAppClient()
    {
        return super.getAppClient();
    }
    public void setAppClient(String appClient)
    {
        super.setAppClient(appClient);
    }

    @XmlElement
    public String getClientRootDomain()
    {
        return super.getClientRootDomain();
    }
    public void setClientRootDomain(String clientRootDomain)
    {
        super.setClientRootDomain(clientRootDomain);
    }

    @XmlElement
    public String getUser()
    {
        return super.getUser();
    }
    public void setUser(String user)
    {
        super.setUser(user);
    }

    @XmlElement
    public String getShortLink()
    {
        return super.getShortLink();
    }
    public void setShortLink(String shortLink)
    {
        super.setShortLink(shortLink);
    }

    @XmlElement
    public String getDomain()
    {
        return super.getDomain();
    }
    public void setDomain(String domain)
    {
        super.setDomain(domain);
    }

    @XmlElement
    public String getToken()
    {
        return super.getToken();
    }
    public void setToken(String token)
    {
        super.setToken(token);
    }

    @XmlElement
    public String getLongUrl()
    {
        return super.getLongUrl();
    }
    public void setLongUrl(String longUrl)
    {
        super.setLongUrl(longUrl);
    }

    @XmlElement
    public String getShortUrl()
    {
        return super.getShortUrl();
    }
    public void setShortUrl(String shortUrl)
    {
        super.setShortUrl(shortUrl);
    }

    @XmlElement
    public Boolean isInternal()
    {
        return super.isInternal();
    }
    public void setInternal(Boolean internal)
    {
        super.setInternal(internal);
    }

    @XmlElement
    public Boolean isCaching()
    {
        return super.isCaching();
    }
    public void setCaching(Boolean caching)
    {
        super.setCaching(caching);
    }

    @XmlElement
    public String getMemo()
    {
        return super.getMemo();
    }
    public void setMemo(String memo)
    {
        super.setMemo(memo);
    }

    @XmlElement
    public String getStatus()
    {
        return super.getStatus();
    }
    public void setStatus(String status)
    {
        super.setStatus(status);
    }

    @XmlElement
    public String getNote()
    {
        return super.getNote();
    }
    public void setNote(String note)
    {
        super.setNote(note);
    }

    @XmlElement
    public Long getExpirationTime()
    {
        return super.getExpirationTime();
    }
    public void setExpirationTime(Long expirationTime)
    {
        super.setExpirationTime(expirationTime);
    }

    @XmlElement
    public String getKeywordFolder()
    {
        return this.keywordFolder;
    }
    public void setKeywordFolder(String keywordFolder)
    {
        this.keywordFolder = keywordFolder;
    }

    @XmlElement
    public String getFolderPath()
    {
        return this.folderPath;
    }
    public void setFolderPath(String folderPath)
    {
        this.folderPath = folderPath;
    }

    @XmlElement
    public String getKeyword()
    {
        return this.keyword;
    }
    public void setKeyword(String keyword)
    {
        this.keyword = keyword;
    }

    @XmlElement
    public String getQueryKey()
    {
        return this.queryKey;
    }
    public void setQueryKey(String queryKey)
    {
        this.queryKey = queryKey;
    }

    @XmlElement
    public String getScope()
    {
        return this.scope;
    }
    public void setScope(String scope)
    {
        this.scope = scope;
    }

    @XmlElement
    public Boolean isDynamic()
    {
        return this.dynamic;
    }
    public void setDynamic(Boolean dynamic)
    {
        this.dynamic = dynamic;
    }

    @XmlElement
    public Boolean isCaseSensitive()
    {
        return this.caseSensitive;
    }
    public void setCaseSensitive(Boolean caseSensitive)
    {
        this.caseSensitive = caseSensitive;
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("keywordFolder", this.keywordFolder);
        dataMap.put("folderPath", this.folderPath);
        dataMap.put("keyword", this.keyword);
        dataMap.put("queryKey", this.queryKey);
        dataMap.put("scope", this.scope);
        dataMap.put("dynamic", this.dynamic);
        dataMap.put("caseSensitive", this.caseSensitive);

        return dataMap;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = keywordFolder == null ? 0 : keywordFolder.hashCode();
        _hash = 31 * _hash + delta;
        delta = folderPath == null ? 0 : folderPath.hashCode();
        _hash = 31 * _hash + delta;
        delta = keyword == null ? 0 : keyword.hashCode();
        _hash = 31 * _hash + delta;
        delta = queryKey == null ? 0 : queryKey.hashCode();
        _hash = 31 * _hash + delta;
        delta = scope == null ? 0 : scope.hashCode();
        _hash = 31 * _hash + delta;
        delta = dynamic == null ? 0 : dynamic.hashCode();
        _hash = 31 * _hash + delta;
        delta = caseSensitive == null ? 0 : caseSensitive.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static KeywordLinkStub convertBeanToStub(KeywordLink bean)
    {
        KeywordLinkStub stub = null;
        if(bean instanceof KeywordLinkStub) {
            stub = (KeywordLinkStub) bean;
        } else {
            if(bean != null) {
                stub = new KeywordLinkStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static KeywordLinkStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of KeywordLinkStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write KeywordLinkStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write KeywordLinkStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write KeywordLinkStub object as a string.", e);
        }
        
        return null;
    }
    public static KeywordLinkStub fromJsonString(String jsonStr)
    {
        try {
            KeywordLinkStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, KeywordLinkStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into KeywordLinkStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into KeywordLinkStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into KeywordLinkStub object.", e);
        }
        
        return null;
    }

}
