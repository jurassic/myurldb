package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.LinkAlbum;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "linkAlbums")
@XmlType(propOrder = {"linkAlbum"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class LinkAlbumListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(LinkAlbumListStub.class.getName());

    private List<LinkAlbumStub> linkAlbums = null;

    public LinkAlbumListStub()
    {
        this(new ArrayList<LinkAlbumStub>());
    }
    public LinkAlbumListStub(List<LinkAlbumStub> linkAlbums)
    {
        this.linkAlbums = linkAlbums;
    }

    public boolean isEmpty()
    {
        if(linkAlbums == null) {
            return true;
        } else {
            return linkAlbums.isEmpty();
        }
    }
    public int getSize()
    {
        if(linkAlbums == null) {
            return 0;
        } else {
            return linkAlbums.size();
        }
    }

    @XmlElement(name = "linkAlbum")
    public List<LinkAlbumStub> getLinkAlbum()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<LinkAlbumStub> getList()
    {
        return linkAlbums;
    }
    public void setList(List<LinkAlbumStub> linkAlbums)
    {
        this.linkAlbums = linkAlbums;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<LinkAlbumStub> it = this.linkAlbums.iterator();
        while(it.hasNext()) {
            LinkAlbumStub linkAlbum = it.next();
            sb.append(linkAlbum.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static LinkAlbumListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of LinkAlbumListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write LinkAlbumListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write LinkAlbumListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write LinkAlbumListStub object as a string.", e);
        }
        
        return null;
    }
    public static LinkAlbumListStub fromJsonString(String jsonStr)
    {
        try {
            LinkAlbumListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, LinkAlbumListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into LinkAlbumListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into LinkAlbumListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into LinkAlbumListStub object.", e);
        }
        
        return null;
    }

}
