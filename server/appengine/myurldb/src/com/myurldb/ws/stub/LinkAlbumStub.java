package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.LinkAlbum;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "linkAlbum")
@XmlType(propOrder = {"guid", "appClient", "clientRootDomain", "owner", "name", "summary", "tokenPrefix", "permalink", "shortLink", "shortUrl", "source", "referenceUrl", "autoGenerated", "maxLinkCount", "note", "status", "createdTime", "modifiedTime"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class LinkAlbumStub implements LinkAlbum, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(LinkAlbumStub.class.getName());

    private String guid;
    private String appClient;
    private String clientRootDomain;
    private String owner;
    private String name;
    private String summary;
    private String tokenPrefix;
    private String permalink;
    private String shortLink;
    private String shortUrl;
    private String source;
    private String referenceUrl;
    private Boolean autoGenerated;
    private Integer maxLinkCount;
    private String note;
    private String status;
    private Long createdTime;
    private Long modifiedTime;

    public LinkAlbumStub()
    {
        this(null);
    }
    public LinkAlbumStub(LinkAlbum bean)
    {
        if(bean != null) {
            this.guid = bean.getGuid();
            this.appClient = bean.getAppClient();
            this.clientRootDomain = bean.getClientRootDomain();
            this.owner = bean.getOwner();
            this.name = bean.getName();
            this.summary = bean.getSummary();
            this.tokenPrefix = bean.getTokenPrefix();
            this.permalink = bean.getPermalink();
            this.shortLink = bean.getShortLink();
            this.shortUrl = bean.getShortUrl();
            this.source = bean.getSource();
            this.referenceUrl = bean.getReferenceUrl();
            this.autoGenerated = bean.isAutoGenerated();
            this.maxLinkCount = bean.getMaxLinkCount();
            this.note = bean.getNote();
            this.status = bean.getStatus();
            this.createdTime = bean.getCreatedTime();
            this.modifiedTime = bean.getModifiedTime();
        }
    }


    @XmlElement
    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    @XmlElement
    public String getAppClient()
    {
        return this.appClient;
    }
    public void setAppClient(String appClient)
    {
        this.appClient = appClient;
    }

    @XmlElement
    public String getClientRootDomain()
    {
        return this.clientRootDomain;
    }
    public void setClientRootDomain(String clientRootDomain)
    {
        this.clientRootDomain = clientRootDomain;
    }

    @XmlElement
    public String getOwner()
    {
        return this.owner;
    }
    public void setOwner(String owner)
    {
        this.owner = owner;
    }

    @XmlElement
    public String getName()
    {
        return this.name;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    @XmlElement
    public String getSummary()
    {
        return this.summary;
    }
    public void setSummary(String summary)
    {
        this.summary = summary;
    }

    @XmlElement
    public String getTokenPrefix()
    {
        return this.tokenPrefix;
    }
    public void setTokenPrefix(String tokenPrefix)
    {
        this.tokenPrefix = tokenPrefix;
    }

    @XmlElement
    public String getPermalink()
    {
        return this.permalink;
    }
    public void setPermalink(String permalink)
    {
        this.permalink = permalink;
    }

    @XmlElement
    public String getShortLink()
    {
        return this.shortLink;
    }
    public void setShortLink(String shortLink)
    {
        this.shortLink = shortLink;
    }

    @XmlElement
    public String getShortUrl()
    {
        return this.shortUrl;
    }
    public void setShortUrl(String shortUrl)
    {
        this.shortUrl = shortUrl;
    }

    @XmlElement
    public String getSource()
    {
        return this.source;
    }
    public void setSource(String source)
    {
        this.source = source;
    }

    @XmlElement
    public String getReferenceUrl()
    {
        return this.referenceUrl;
    }
    public void setReferenceUrl(String referenceUrl)
    {
        this.referenceUrl = referenceUrl;
    }

    @XmlElement
    public Boolean isAutoGenerated()
    {
        return this.autoGenerated;
    }
    public void setAutoGenerated(Boolean autoGenerated)
    {
        this.autoGenerated = autoGenerated;
    }

    @XmlElement
    public Integer getMaxLinkCount()
    {
        return this.maxLinkCount;
    }
    public void setMaxLinkCount(Integer maxLinkCount)
    {
        this.maxLinkCount = maxLinkCount;
    }

    @XmlElement
    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    @XmlElement
    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("guid", this.guid);
        dataMap.put("appClient", this.appClient);
        dataMap.put("clientRootDomain", this.clientRootDomain);
        dataMap.put("owner", this.owner);
        dataMap.put("name", this.name);
        dataMap.put("summary", this.summary);
        dataMap.put("tokenPrefix", this.tokenPrefix);
        dataMap.put("permalink", this.permalink);
        dataMap.put("shortLink", this.shortLink);
        dataMap.put("shortUrl", this.shortUrl);
        dataMap.put("source", this.source);
        dataMap.put("referenceUrl", this.referenceUrl);
        dataMap.put("autoGenerated", this.autoGenerated);
        dataMap.put("maxLinkCount", this.maxLinkCount);
        dataMap.put("note", this.note);
        dataMap.put("status", this.status);
        dataMap.put("createdTime", this.createdTime);
        dataMap.put("modifiedTime", this.modifiedTime);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = appClient == null ? 0 : appClient.hashCode();
        _hash = 31 * _hash + delta;
        delta = clientRootDomain == null ? 0 : clientRootDomain.hashCode();
        _hash = 31 * _hash + delta;
        delta = owner == null ? 0 : owner.hashCode();
        _hash = 31 * _hash + delta;
        delta = name == null ? 0 : name.hashCode();
        _hash = 31 * _hash + delta;
        delta = summary == null ? 0 : summary.hashCode();
        _hash = 31 * _hash + delta;
        delta = tokenPrefix == null ? 0 : tokenPrefix.hashCode();
        _hash = 31 * _hash + delta;
        delta = permalink == null ? 0 : permalink.hashCode();
        _hash = 31 * _hash + delta;
        delta = shortLink == null ? 0 : shortLink.hashCode();
        _hash = 31 * _hash + delta;
        delta = shortUrl == null ? 0 : shortUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = source == null ? 0 : source.hashCode();
        _hash = 31 * _hash + delta;
        delta = referenceUrl == null ? 0 : referenceUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = autoGenerated == null ? 0 : autoGenerated.hashCode();
        _hash = 31 * _hash + delta;
        delta = maxLinkCount == null ? 0 : maxLinkCount.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = createdTime == null ? 0 : createdTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static LinkAlbumStub convertBeanToStub(LinkAlbum bean)
    {
        LinkAlbumStub stub = null;
        if(bean instanceof LinkAlbumStub) {
            stub = (LinkAlbumStub) bean;
        } else {
            if(bean != null) {
                stub = new LinkAlbumStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static LinkAlbumStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of LinkAlbumStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write LinkAlbumStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write LinkAlbumStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write LinkAlbumStub object as a string.", e);
        }
        
        return null;
    }
    public static LinkAlbumStub fromJsonString(String jsonStr)
    {
        try {
            LinkAlbumStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, LinkAlbumStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into LinkAlbumStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into LinkAlbumStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into LinkAlbumStub object.", e);
        }
        
        return null;
    }

}
