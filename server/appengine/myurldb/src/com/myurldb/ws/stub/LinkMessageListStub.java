package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.LinkMessage;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "linkMessages")
@XmlType(propOrder = {"linkMessage"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class LinkMessageListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(LinkMessageListStub.class.getName());

    private List<LinkMessageStub> linkMessages = null;

    public LinkMessageListStub()
    {
        this(new ArrayList<LinkMessageStub>());
    }
    public LinkMessageListStub(List<LinkMessageStub> linkMessages)
    {
        this.linkMessages = linkMessages;
    }

    public boolean isEmpty()
    {
        if(linkMessages == null) {
            return true;
        } else {
            return linkMessages.isEmpty();
        }
    }
    public int getSize()
    {
        if(linkMessages == null) {
            return 0;
        } else {
            return linkMessages.size();
        }
    }

    @XmlElement(name = "linkMessage")
    public List<LinkMessageStub> getLinkMessage()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<LinkMessageStub> getList()
    {
        return linkMessages;
    }
    public void setList(List<LinkMessageStub> linkMessages)
    {
        this.linkMessages = linkMessages;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<LinkMessageStub> it = this.linkMessages.iterator();
        while(it.hasNext()) {
            LinkMessageStub linkMessage = it.next();
            sb.append(linkMessage.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static LinkMessageListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of LinkMessageListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write LinkMessageListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write LinkMessageListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write LinkMessageListStub object as a string.", e);
        }
        
        return null;
    }
    public static LinkMessageListStub fromJsonString(String jsonStr)
    {
        try {
            LinkMessageListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, LinkMessageListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into LinkMessageListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into LinkMessageListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into LinkMessageListStub object.", e);
        }
        
        return null;
    }

}
