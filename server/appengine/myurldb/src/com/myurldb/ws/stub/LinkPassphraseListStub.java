package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.LinkPassphrase;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "linkPassphrases")
@XmlType(propOrder = {"linkPassphrase"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class LinkPassphraseListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(LinkPassphraseListStub.class.getName());

    private List<LinkPassphraseStub> linkPassphrases = null;

    public LinkPassphraseListStub()
    {
        this(new ArrayList<LinkPassphraseStub>());
    }
    public LinkPassphraseListStub(List<LinkPassphraseStub> linkPassphrases)
    {
        this.linkPassphrases = linkPassphrases;
    }

    public boolean isEmpty()
    {
        if(linkPassphrases == null) {
            return true;
        } else {
            return linkPassphrases.isEmpty();
        }
    }
    public int getSize()
    {
        if(linkPassphrases == null) {
            return 0;
        } else {
            return linkPassphrases.size();
        }
    }

    @XmlElement(name = "linkPassphrase")
    public List<LinkPassphraseStub> getLinkPassphrase()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<LinkPassphraseStub> getList()
    {
        return linkPassphrases;
    }
    public void setList(List<LinkPassphraseStub> linkPassphrases)
    {
        this.linkPassphrases = linkPassphrases;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<LinkPassphraseStub> it = this.linkPassphrases.iterator();
        while(it.hasNext()) {
            LinkPassphraseStub linkPassphrase = it.next();
            sb.append(linkPassphrase.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static LinkPassphraseListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of LinkPassphraseListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write LinkPassphraseListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write LinkPassphraseListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write LinkPassphraseListStub object as a string.", e);
        }
        
        return null;
    }
    public static LinkPassphraseListStub fromJsonString(String jsonStr)
    {
        try {
            LinkPassphraseListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, LinkPassphraseListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into LinkPassphraseListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into LinkPassphraseListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into LinkPassphraseListStub object.", e);
        }
        
        return null;
    }

}
