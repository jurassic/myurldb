package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.PersonalSetting;
import com.myurldb.ws.util.JsonUtil;


// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class PersonalSettingStub implements PersonalSetting, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(PersonalSettingStub.class.getName());

    private String guid;
    private Long createdTime;
    private Long modifiedTime;

    public PersonalSettingStub()
    {
        this(null);
    }
    public PersonalSettingStub(PersonalSetting bean)
    {
        if(bean != null) {
            this.guid = bean.getGuid();
            this.createdTime = bean.getCreatedTime();
            this.modifiedTime = bean.getModifiedTime();
        }
    }


    @XmlTransient
    //@JsonIgnore
    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    @XmlTransient
    //@JsonIgnore
    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    @XmlTransient
    //@JsonIgnore
    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("guid", this.guid);
        dataMap.put("createdTime", this.createdTime);
        dataMap.put("modifiedTime", this.modifiedTime);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = createdTime == null ? 0 : createdTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static PersonalSettingStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of PersonalSettingStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write PersonalSettingStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write PersonalSettingStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write PersonalSettingStub object as a string.", e);
        }
        
        return null;
    }
    public static PersonalSettingStub fromJsonString(String jsonStr)
    {
        try {
            PersonalSettingStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, PersonalSettingStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into PersonalSettingStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into PersonalSettingStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into PersonalSettingStub object.", e);
        }
        
        return null;
    }

}
