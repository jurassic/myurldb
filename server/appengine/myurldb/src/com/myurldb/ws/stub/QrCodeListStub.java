package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.QrCode;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "qrCodes")
@XmlType(propOrder = {"qrCode"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class QrCodeListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(QrCodeListStub.class.getName());

    private List<QrCodeStub> qrCodes = null;

    public QrCodeListStub()
    {
        this(new ArrayList<QrCodeStub>());
    }
    public QrCodeListStub(List<QrCodeStub> qrCodes)
    {
        this.qrCodes = qrCodes;
    }

    public boolean isEmpty()
    {
        if(qrCodes == null) {
            return true;
        } else {
            return qrCodes.isEmpty();
        }
    }
    public int getSize()
    {
        if(qrCodes == null) {
            return 0;
        } else {
            return qrCodes.size();
        }
    }

    @XmlElement(name = "qrCode")
    public List<QrCodeStub> getQrCode()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<QrCodeStub> getList()
    {
        return qrCodes;
    }
    public void setList(List<QrCodeStub> qrCodes)
    {
        this.qrCodes = qrCodes;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<QrCodeStub> it = this.qrCodes.iterator();
        while(it.hasNext()) {
            QrCodeStub qrCode = it.next();
            sb.append(qrCode.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static QrCodeListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of QrCodeListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write QrCodeListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write QrCodeListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write QrCodeListStub object as a string.", e);
        }
        
        return null;
    }
    public static QrCodeListStub fromJsonString(String jsonStr)
    {
        try {
            QrCodeListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, QrCodeListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into QrCodeListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into QrCodeListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into QrCodeListStub object.", e);
        }
        
        return null;
    }

}
