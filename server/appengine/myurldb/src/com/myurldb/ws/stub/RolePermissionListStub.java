package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.RolePermission;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "rolePermissions")
@XmlType(propOrder = {"rolePermission"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class RolePermissionListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(RolePermissionListStub.class.getName());

    private List<RolePermissionStub> rolePermissions = null;

    public RolePermissionListStub()
    {
        this(new ArrayList<RolePermissionStub>());
    }
    public RolePermissionListStub(List<RolePermissionStub> rolePermissions)
    {
        this.rolePermissions = rolePermissions;
    }

    public boolean isEmpty()
    {
        if(rolePermissions == null) {
            return true;
        } else {
            return rolePermissions.isEmpty();
        }
    }
    public int getSize()
    {
        if(rolePermissions == null) {
            return 0;
        } else {
            return rolePermissions.size();
        }
    }

    @XmlElement(name = "rolePermission")
    public List<RolePermissionStub> getRolePermission()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<RolePermissionStub> getList()
    {
        return rolePermissions;
    }
    public void setList(List<RolePermissionStub> rolePermissions)
    {
        this.rolePermissions = rolePermissions;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<RolePermissionStub> it = this.rolePermissions.iterator();
        while(it.hasNext()) {
            RolePermissionStub rolePermission = it.next();
            sb.append(rolePermission.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static RolePermissionListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of RolePermissionListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write RolePermissionListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write RolePermissionListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write RolePermissionListStub object as a string.", e);
        }
        
        return null;
    }
    public static RolePermissionListStub fromJsonString(String jsonStr)
    {
        try {
            RolePermissionListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, RolePermissionListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into RolePermissionListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into RolePermissionListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into RolePermissionListStub object.", e);
        }
        
        return null;
    }

}
