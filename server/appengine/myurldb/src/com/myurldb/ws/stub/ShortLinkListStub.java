package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.ShortLink;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "shortLinks")
@XmlType(propOrder = {"shortLink"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class ShortLinkListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ShortLinkListStub.class.getName());

    private List<ShortLinkStub> shortLinks = null;

    public ShortLinkListStub()
    {
        this(new ArrayList<ShortLinkStub>());
    }
    public ShortLinkListStub(List<ShortLinkStub> shortLinks)
    {
        this.shortLinks = shortLinks;
    }

    public boolean isEmpty()
    {
        if(shortLinks == null) {
            return true;
        } else {
            return shortLinks.isEmpty();
        }
    }
    public int getSize()
    {
        if(shortLinks == null) {
            return 0;
        } else {
            return shortLinks.size();
        }
    }

    @XmlElement(name = "shortLink")
    public List<ShortLinkStub> getShortLink()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<ShortLinkStub> getList()
    {
        return shortLinks;
    }
    public void setList(List<ShortLinkStub> shortLinks)
    {
        this.shortLinks = shortLinks;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<ShortLinkStub> it = this.shortLinks.iterator();
        while(it.hasNext()) {
            ShortLinkStub shortLink = it.next();
            sb.append(shortLink.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static ShortLinkListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of ShortLinkListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write ShortLinkListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write ShortLinkListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write ShortLinkListStub object as a string.", e);
        }
        
        return null;
    }
    public static ShortLinkListStub fromJsonString(String jsonStr)
    {
        try {
            ShortLinkListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, ShortLinkListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into ShortLinkListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into ShortLinkListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into ShortLinkListStub object.", e);
        }
        
        return null;
    }

}
