package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.ReferrerInfoStruct;
import com.myurldb.ws.ShortLink;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "shortLink")
@XmlType(propOrder = {"guid", "appClient", "clientRootDomain", "owner", "longUrlDomain", "longUrl", "longUrlFull", "longUrlHash", "reuseExistingShortUrl", "failIfShortUrlExists", "domain", "domainType", "usercode", "username", "tokenPrefix", "token", "tokenType", "sassyTokenType", "shortUrl", "shortPassage", "redirectType", "flashDuration", "accessType", "viewType", "shareType", "readOnly", "displayMessage", "shortMessage", "keywordEnabled", "bookmarkEnabled", "referrerInfoStub", "status", "note", "expirationTime", "createdTime", "modifiedTime"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ShortLinkStub implements ShortLink, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ShortLinkStub.class.getName());

    private String guid;
    private String appClient;
    private String clientRootDomain;
    private String owner;
    private String longUrlDomain;
    private String longUrl;
    private String longUrlFull;
    private String longUrlHash;
    private Boolean reuseExistingShortUrl;
    private Boolean failIfShortUrlExists;
    private String domain;
    private String domainType;
    private String usercode;
    private String username;
    private String tokenPrefix;
    private String token;
    private String tokenType;
    private String sassyTokenType;
    private String shortUrl;
    private String shortPassage;
    private String redirectType;
    private Long flashDuration;
    private String accessType;
    private String viewType;
    private String shareType;
    private Boolean readOnly;
    private String displayMessage;
    private String shortMessage;
    private Boolean keywordEnabled;
    private Boolean bookmarkEnabled;
    private ReferrerInfoStructStub referrerInfo;
    private String status;
    private String note;
    private Long expirationTime;
    private Long createdTime;
    private Long modifiedTime;

    public ShortLinkStub()
    {
        this(null);
    }
    public ShortLinkStub(ShortLink bean)
    {
        if(bean != null) {
            this.guid = bean.getGuid();
            this.appClient = bean.getAppClient();
            this.clientRootDomain = bean.getClientRootDomain();
            this.owner = bean.getOwner();
            this.longUrlDomain = bean.getLongUrlDomain();
            this.longUrl = bean.getLongUrl();
            this.longUrlFull = bean.getLongUrlFull();
            this.longUrlHash = bean.getLongUrlHash();
            this.reuseExistingShortUrl = bean.isReuseExistingShortUrl();
            this.failIfShortUrlExists = bean.isFailIfShortUrlExists();
            this.domain = bean.getDomain();
            this.domainType = bean.getDomainType();
            this.usercode = bean.getUsercode();
            this.username = bean.getUsername();
            this.tokenPrefix = bean.getTokenPrefix();
            this.token = bean.getToken();
            this.tokenType = bean.getTokenType();
            this.sassyTokenType = bean.getSassyTokenType();
            this.shortUrl = bean.getShortUrl();
            this.shortPassage = bean.getShortPassage();
            this.redirectType = bean.getRedirectType();
            this.flashDuration = bean.getFlashDuration();
            this.accessType = bean.getAccessType();
            this.viewType = bean.getViewType();
            this.shareType = bean.getShareType();
            this.readOnly = bean.isReadOnly();
            this.displayMessage = bean.getDisplayMessage();
            this.shortMessage = bean.getShortMessage();
            this.keywordEnabled = bean.isKeywordEnabled();
            this.bookmarkEnabled = bean.isBookmarkEnabled();
            this.referrerInfo = ReferrerInfoStructStub.convertBeanToStub(bean.getReferrerInfo());
            this.status = bean.getStatus();
            this.note = bean.getNote();
            this.expirationTime = bean.getExpirationTime();
            this.createdTime = bean.getCreatedTime();
            this.modifiedTime = bean.getModifiedTime();
        }
    }


    @XmlElement
    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    @XmlElement
    public String getAppClient()
    {
        return this.appClient;
    }
    public void setAppClient(String appClient)
    {
        this.appClient = appClient;
    }

    @XmlElement
    public String getClientRootDomain()
    {
        return this.clientRootDomain;
    }
    public void setClientRootDomain(String clientRootDomain)
    {
        this.clientRootDomain = clientRootDomain;
    }

    @XmlElement
    public String getOwner()
    {
        return this.owner;
    }
    public void setOwner(String owner)
    {
        this.owner = owner;
    }

    @XmlElement
    public String getLongUrlDomain()
    {
        return this.longUrlDomain;
    }
    public void setLongUrlDomain(String longUrlDomain)
    {
        this.longUrlDomain = longUrlDomain;
    }

    @XmlElement
    public String getLongUrl()
    {
        return this.longUrl;
    }
    public void setLongUrl(String longUrl)
    {
        this.longUrl = longUrl;
    }

    @XmlElement
    public String getLongUrlFull()
    {
        return this.longUrlFull;
    }
    public void setLongUrlFull(String longUrlFull)
    {
        this.longUrlFull = longUrlFull;
    }

    @XmlElement
    public String getLongUrlHash()
    {
        return this.longUrlHash;
    }
    public void setLongUrlHash(String longUrlHash)
    {
        this.longUrlHash = longUrlHash;
    }

    @XmlElement
    public Boolean isReuseExistingShortUrl()
    {
        return this.reuseExistingShortUrl;
    }
    public void setReuseExistingShortUrl(Boolean reuseExistingShortUrl)
    {
        this.reuseExistingShortUrl = reuseExistingShortUrl;
    }

    @XmlElement
    public Boolean isFailIfShortUrlExists()
    {
        return this.failIfShortUrlExists;
    }
    public void setFailIfShortUrlExists(Boolean failIfShortUrlExists)
    {
        this.failIfShortUrlExists = failIfShortUrlExists;
    }

    @XmlElement
    public String getDomain()
    {
        return this.domain;
    }
    public void setDomain(String domain)
    {
        this.domain = domain;
    }

    @XmlElement
    public String getDomainType()
    {
        return this.domainType;
    }
    public void setDomainType(String domainType)
    {
        this.domainType = domainType;
    }

    @XmlElement
    public String getUsercode()
    {
        return this.usercode;
    }
    public void setUsercode(String usercode)
    {
        this.usercode = usercode;
    }

    @XmlElement
    public String getUsername()
    {
        return this.username;
    }
    public void setUsername(String username)
    {
        this.username = username;
    }

    @XmlElement
    public String getTokenPrefix()
    {
        return this.tokenPrefix;
    }
    public void setTokenPrefix(String tokenPrefix)
    {
        this.tokenPrefix = tokenPrefix;
    }

    @XmlElement
    public String getToken()
    {
        return this.token;
    }
    public void setToken(String token)
    {
        this.token = token;
    }

    @XmlElement
    public String getTokenType()
    {
        return this.tokenType;
    }
    public void setTokenType(String tokenType)
    {
        this.tokenType = tokenType;
    }

    @XmlElement
    public String getSassyTokenType()
    {
        return this.sassyTokenType;
    }
    public void setSassyTokenType(String sassyTokenType)
    {
        this.sassyTokenType = sassyTokenType;
    }

    @XmlElement
    public String getShortUrl()
    {
        return this.shortUrl;
    }
    public void setShortUrl(String shortUrl)
    {
        this.shortUrl = shortUrl;
    }

    @XmlElement
    public String getShortPassage()
    {
        return this.shortPassage;
    }
    public void setShortPassage(String shortPassage)
    {
        this.shortPassage = shortPassage;
    }

    @XmlElement
    public String getRedirectType()
    {
        return this.redirectType;
    }
    public void setRedirectType(String redirectType)
    {
        this.redirectType = redirectType;
    }

    @XmlElement
    public Long getFlashDuration()
    {
        return this.flashDuration;
    }
    public void setFlashDuration(Long flashDuration)
    {
        this.flashDuration = flashDuration;
    }

    @XmlElement
    public String getAccessType()
    {
        return this.accessType;
    }
    public void setAccessType(String accessType)
    {
        this.accessType = accessType;
    }

    @XmlElement
    public String getViewType()
    {
        return this.viewType;
    }
    public void setViewType(String viewType)
    {
        this.viewType = viewType;
    }

    @XmlElement
    public String getShareType()
    {
        return this.shareType;
    }
    public void setShareType(String shareType)
    {
        this.shareType = shareType;
    }

    @XmlElement
    public Boolean isReadOnly()
    {
        return this.readOnly;
    }
    public void setReadOnly(Boolean readOnly)
    {
        this.readOnly = readOnly;
    }

    @XmlElement
    public String getDisplayMessage()
    {
        return this.displayMessage;
    }
    public void setDisplayMessage(String displayMessage)
    {
        this.displayMessage = displayMessage;
    }

    @XmlElement
    public String getShortMessage()
    {
        return this.shortMessage;
    }
    public void setShortMessage(String shortMessage)
    {
        this.shortMessage = shortMessage;
    }

    @XmlElement
    public Boolean isKeywordEnabled()
    {
        return this.keywordEnabled;
    }
    public void setKeywordEnabled(Boolean keywordEnabled)
    {
        this.keywordEnabled = keywordEnabled;
    }

    @XmlElement
    public Boolean isBookmarkEnabled()
    {
        return this.bookmarkEnabled;
    }
    public void setBookmarkEnabled(Boolean bookmarkEnabled)
    {
        this.bookmarkEnabled = bookmarkEnabled;
    }

    @XmlElement(name = "referrerInfo")
    @JsonIgnore
    public ReferrerInfoStructStub getReferrerInfoStub()
    {
        return this.referrerInfo;
    }
    public void setReferrerInfoStub(ReferrerInfoStructStub referrerInfo)
    {
        this.referrerInfo = referrerInfo;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=ReferrerInfoStructStub.class)
    public ReferrerInfoStruct getReferrerInfo()
    {  
        return getReferrerInfoStub();
    }
    public void setReferrerInfo(ReferrerInfoStruct referrerInfo)
    {
        if((referrerInfo == null) || (referrerInfo instanceof ReferrerInfoStructStub)) {
            setReferrerInfoStub((ReferrerInfoStructStub) referrerInfo);
        } else {
            // TBD
            setReferrerInfoStub(ReferrerInfoStructStub.convertBeanToStub(referrerInfo));
        }
    }

    @XmlElement
    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    @XmlElement
    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    @XmlElement
    public Long getExpirationTime()
    {
        return this.expirationTime;
    }
    public void setExpirationTime(Long expirationTime)
    {
        this.expirationTime = expirationTime;
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("guid", this.guid);
        dataMap.put("appClient", this.appClient);
        dataMap.put("clientRootDomain", this.clientRootDomain);
        dataMap.put("owner", this.owner);
        dataMap.put("longUrlDomain", this.longUrlDomain);
        dataMap.put("longUrl", this.longUrl);
        dataMap.put("longUrlFull", this.longUrlFull);
        dataMap.put("longUrlHash", this.longUrlHash);
        dataMap.put("reuseExistingShortUrl", this.reuseExistingShortUrl);
        dataMap.put("failIfShortUrlExists", this.failIfShortUrlExists);
        dataMap.put("domain", this.domain);
        dataMap.put("domainType", this.domainType);
        dataMap.put("usercode", this.usercode);
        dataMap.put("username", this.username);
        dataMap.put("tokenPrefix", this.tokenPrefix);
        dataMap.put("token", this.token);
        dataMap.put("tokenType", this.tokenType);
        dataMap.put("sassyTokenType", this.sassyTokenType);
        dataMap.put("shortUrl", this.shortUrl);
        dataMap.put("shortPassage", this.shortPassage);
        dataMap.put("redirectType", this.redirectType);
        dataMap.put("flashDuration", this.flashDuration);
        dataMap.put("accessType", this.accessType);
        dataMap.put("viewType", this.viewType);
        dataMap.put("shareType", this.shareType);
        dataMap.put("readOnly", this.readOnly);
        dataMap.put("displayMessage", this.displayMessage);
        dataMap.put("shortMessage", this.shortMessage);
        dataMap.put("keywordEnabled", this.keywordEnabled);
        dataMap.put("bookmarkEnabled", this.bookmarkEnabled);
        dataMap.put("referrerInfo", this.referrerInfo);
        dataMap.put("status", this.status);
        dataMap.put("note", this.note);
        dataMap.put("expirationTime", this.expirationTime);
        dataMap.put("createdTime", this.createdTime);
        dataMap.put("modifiedTime", this.modifiedTime);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = appClient == null ? 0 : appClient.hashCode();
        _hash = 31 * _hash + delta;
        delta = clientRootDomain == null ? 0 : clientRootDomain.hashCode();
        _hash = 31 * _hash + delta;
        delta = owner == null ? 0 : owner.hashCode();
        _hash = 31 * _hash + delta;
        delta = longUrlDomain == null ? 0 : longUrlDomain.hashCode();
        _hash = 31 * _hash + delta;
        delta = longUrl == null ? 0 : longUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = longUrlFull == null ? 0 : longUrlFull.hashCode();
        _hash = 31 * _hash + delta;
        delta = longUrlHash == null ? 0 : longUrlHash.hashCode();
        _hash = 31 * _hash + delta;
        delta = reuseExistingShortUrl == null ? 0 : reuseExistingShortUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = failIfShortUrlExists == null ? 0 : failIfShortUrlExists.hashCode();
        _hash = 31 * _hash + delta;
        delta = domain == null ? 0 : domain.hashCode();
        _hash = 31 * _hash + delta;
        delta = domainType == null ? 0 : domainType.hashCode();
        _hash = 31 * _hash + delta;
        delta = usercode == null ? 0 : usercode.hashCode();
        _hash = 31 * _hash + delta;
        delta = username == null ? 0 : username.hashCode();
        _hash = 31 * _hash + delta;
        delta = tokenPrefix == null ? 0 : tokenPrefix.hashCode();
        _hash = 31 * _hash + delta;
        delta = token == null ? 0 : token.hashCode();
        _hash = 31 * _hash + delta;
        delta = tokenType == null ? 0 : tokenType.hashCode();
        _hash = 31 * _hash + delta;
        delta = sassyTokenType == null ? 0 : sassyTokenType.hashCode();
        _hash = 31 * _hash + delta;
        delta = shortUrl == null ? 0 : shortUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = shortPassage == null ? 0 : shortPassage.hashCode();
        _hash = 31 * _hash + delta;
        delta = redirectType == null ? 0 : redirectType.hashCode();
        _hash = 31 * _hash + delta;
        delta = flashDuration == null ? 0 : flashDuration.hashCode();
        _hash = 31 * _hash + delta;
        delta = accessType == null ? 0 : accessType.hashCode();
        _hash = 31 * _hash + delta;
        delta = viewType == null ? 0 : viewType.hashCode();
        _hash = 31 * _hash + delta;
        delta = shareType == null ? 0 : shareType.hashCode();
        _hash = 31 * _hash + delta;
        delta = readOnly == null ? 0 : readOnly.hashCode();
        _hash = 31 * _hash + delta;
        delta = displayMessage == null ? 0 : displayMessage.hashCode();
        _hash = 31 * _hash + delta;
        delta = shortMessage == null ? 0 : shortMessage.hashCode();
        _hash = 31 * _hash + delta;
        delta = keywordEnabled == null ? 0 : keywordEnabled.hashCode();
        _hash = 31 * _hash + delta;
        delta = bookmarkEnabled == null ? 0 : bookmarkEnabled.hashCode();
        _hash = 31 * _hash + delta;
        delta = referrerInfo == null ? 0 : referrerInfo.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        delta = expirationTime == null ? 0 : expirationTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = createdTime == null ? 0 : createdTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static ShortLinkStub convertBeanToStub(ShortLink bean)
    {
        ShortLinkStub stub = null;
        if(bean instanceof ShortLinkStub) {
            stub = (ShortLinkStub) bean;
        } else {
            if(bean != null) {
                stub = new ShortLinkStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static ShortLinkStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of ShortLinkStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write ShortLinkStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write ShortLinkStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write ShortLinkStub object as a string.", e);
        }
        
        return null;
    }
    public static ShortLinkStub fromJsonString(String jsonStr)
    {
        try {
            ShortLinkStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, ShortLinkStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into ShortLinkStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into ShortLinkStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into ShortLinkStub object.", e);
        }
        
        return null;
    }

}
