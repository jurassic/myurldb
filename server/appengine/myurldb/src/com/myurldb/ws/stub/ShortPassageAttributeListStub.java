package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.ShortPassageAttribute;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "shortPassageAttributes")
@XmlType(propOrder = {"shortPassageAttribute"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class ShortPassageAttributeListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ShortPassageAttributeListStub.class.getName());

    private List<ShortPassageAttributeStub> shortPassageAttributes = null;

    public ShortPassageAttributeListStub()
    {
        this(new ArrayList<ShortPassageAttributeStub>());
    }
    public ShortPassageAttributeListStub(List<ShortPassageAttributeStub> shortPassageAttributes)
    {
        this.shortPassageAttributes = shortPassageAttributes;
    }

    public boolean isEmpty()
    {
        if(shortPassageAttributes == null) {
            return true;
        } else {
            return shortPassageAttributes.isEmpty();
        }
    }
    public int getSize()
    {
        if(shortPassageAttributes == null) {
            return 0;
        } else {
            return shortPassageAttributes.size();
        }
    }

    @XmlElement(name = "shortPassageAttribute")
    public List<ShortPassageAttributeStub> getShortPassageAttribute()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<ShortPassageAttributeStub> getList()
    {
        return shortPassageAttributes;
    }
    public void setList(List<ShortPassageAttributeStub> shortPassageAttributes)
    {
        this.shortPassageAttributes = shortPassageAttributes;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<ShortPassageAttributeStub> it = this.shortPassageAttributes.iterator();
        while(it.hasNext()) {
            ShortPassageAttributeStub shortPassageAttribute = it.next();
            sb.append(shortPassageAttribute.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static ShortPassageAttributeListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of ShortPassageAttributeListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write ShortPassageAttributeListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write ShortPassageAttributeListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write ShortPassageAttributeListStub object as a string.", e);
        }
        
        return null;
    }
    public static ShortPassageAttributeListStub fromJsonString(String jsonStr)
    {
        try {
            ShortPassageAttributeListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, ShortPassageAttributeListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into ShortPassageAttributeListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into ShortPassageAttributeListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into ShortPassageAttributeListStub object.", e);
        }
        
        return null;
    }

}
