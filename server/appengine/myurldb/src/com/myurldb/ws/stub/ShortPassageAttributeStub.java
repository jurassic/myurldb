package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.ShortPassageAttribute;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "shortPassageAttribute")
@XmlType(propOrder = {"domain", "tokenType", "displayMessage", "redirectType", "flashDuration", "accessType", "viewType", "shareType"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ShortPassageAttributeStub implements ShortPassageAttribute, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ShortPassageAttributeStub.class.getName());

    private String domain;
    private String tokenType;
    private String displayMessage;
    private String redirectType;
    private Long flashDuration;
    private String accessType;
    private String viewType;
    private String shareType;

    public ShortPassageAttributeStub()
    {
        this(null);
    }
    public ShortPassageAttributeStub(ShortPassageAttribute bean)
    {
        if(bean != null) {
            this.domain = bean.getDomain();
            this.tokenType = bean.getTokenType();
            this.displayMessage = bean.getDisplayMessage();
            this.redirectType = bean.getRedirectType();
            this.flashDuration = bean.getFlashDuration();
            this.accessType = bean.getAccessType();
            this.viewType = bean.getViewType();
            this.shareType = bean.getShareType();
        }
    }


    @XmlElement
    public String getDomain()
    {
        return this.domain;
    }
    public void setDomain(String domain)
    {
        this.domain = domain;
    }

    @XmlElement
    public String getTokenType()
    {
        return this.tokenType;
    }
    public void setTokenType(String tokenType)
    {
        this.tokenType = tokenType;
    }

    @XmlElement
    public String getDisplayMessage()
    {
        return this.displayMessage;
    }
    public void setDisplayMessage(String displayMessage)
    {
        this.displayMessage = displayMessage;
    }

    @XmlElement
    public String getRedirectType()
    {
        return this.redirectType;
    }
    public void setRedirectType(String redirectType)
    {
        this.redirectType = redirectType;
    }

    @XmlElement
    public Long getFlashDuration()
    {
        return this.flashDuration;
    }
    public void setFlashDuration(Long flashDuration)
    {
        this.flashDuration = flashDuration;
    }

    @XmlElement
    public String getAccessType()
    {
        return this.accessType;
    }
    public void setAccessType(String accessType)
    {
        this.accessType = accessType;
    }

    @XmlElement
    public String getViewType()
    {
        return this.viewType;
    }
    public void setViewType(String viewType)
    {
        this.viewType = viewType;
    }

    @XmlElement
    public String getShareType()
    {
        return this.shareType;
    }
    public void setShareType(String shareType)
    {
        this.shareType = shareType;
    }


    @XmlTransient
    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getDomain() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getTokenType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getDisplayMessage() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getRedirectType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getFlashDuration() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAccessType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getViewType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getShareType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("domain", this.domain);
        dataMap.put("tokenType", this.tokenType);
        dataMap.put("displayMessage", this.displayMessage);
        dataMap.put("redirectType", this.redirectType);
        dataMap.put("flashDuration", this.flashDuration);
        dataMap.put("accessType", this.accessType);
        dataMap.put("viewType", this.viewType);
        dataMap.put("shareType", this.shareType);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = domain == null ? 0 : domain.hashCode();
        _hash = 31 * _hash + delta;
        delta = tokenType == null ? 0 : tokenType.hashCode();
        _hash = 31 * _hash + delta;
        delta = displayMessage == null ? 0 : displayMessage.hashCode();
        _hash = 31 * _hash + delta;
        delta = redirectType == null ? 0 : redirectType.hashCode();
        _hash = 31 * _hash + delta;
        delta = flashDuration == null ? 0 : flashDuration.hashCode();
        _hash = 31 * _hash + delta;
        delta = accessType == null ? 0 : accessType.hashCode();
        _hash = 31 * _hash + delta;
        delta = viewType == null ? 0 : viewType.hashCode();
        _hash = 31 * _hash + delta;
        delta = shareType == null ? 0 : shareType.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static ShortPassageAttributeStub convertBeanToStub(ShortPassageAttribute bean)
    {
        ShortPassageAttributeStub stub = null;
        if(bean instanceof ShortPassageAttributeStub) {
            stub = (ShortPassageAttributeStub) bean;
        } else {
            if(bean != null) {
                stub = new ShortPassageAttributeStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static ShortPassageAttributeStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of ShortPassageAttributeStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write ShortPassageAttributeStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write ShortPassageAttributeStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write ShortPassageAttributeStub object as a string.", e);
        }
        
        return null;
    }
    public static ShortPassageAttributeStub fromJsonString(String jsonStr)
    {
        try {
            ShortPassageAttributeStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, ShortPassageAttributeStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into ShortPassageAttributeStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into ShortPassageAttributeStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into ShortPassageAttributeStub object.", e);
        }
        
        return null;
    }

}
