package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.ShortPassage;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "shortPassages")
@XmlType(propOrder = {"shortPassage"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class ShortPassageListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ShortPassageListStub.class.getName());

    private List<ShortPassageStub> shortPassages = null;

    public ShortPassageListStub()
    {
        this(new ArrayList<ShortPassageStub>());
    }
    public ShortPassageListStub(List<ShortPassageStub> shortPassages)
    {
        this.shortPassages = shortPassages;
    }

    public boolean isEmpty()
    {
        if(shortPassages == null) {
            return true;
        } else {
            return shortPassages.isEmpty();
        }
    }
    public int getSize()
    {
        if(shortPassages == null) {
            return 0;
        } else {
            return shortPassages.size();
        }
    }

    @XmlElement(name = "shortPassage")
    public List<ShortPassageStub> getShortPassage()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<ShortPassageStub> getList()
    {
        return shortPassages;
    }
    public void setList(List<ShortPassageStub> shortPassages)
    {
        this.shortPassages = shortPassages;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<ShortPassageStub> it = this.shortPassages.iterator();
        while(it.hasNext()) {
            ShortPassageStub shortPassage = it.next();
            sb.append(shortPassage.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static ShortPassageListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of ShortPassageListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write ShortPassageListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write ShortPassageListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write ShortPassageListStub object as a string.", e);
        }
        
        return null;
    }
    public static ShortPassageListStub fromJsonString(String jsonStr)
    {
        try {
            ShortPassageListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, ShortPassageListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into ShortPassageListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into ShortPassageListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into ShortPassageListStub object.", e);
        }
        
        return null;
    }

}
