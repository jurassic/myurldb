package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.ShortPassageAttribute;
import com.myurldb.ws.ShortPassage;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "shortPassage")
@XmlType(propOrder = {"guid", "owner", "longText", "shortText", "attributeStub", "readOnly", "status", "note", "expirationTime", "createdTime", "modifiedTime"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ShortPassageStub implements ShortPassage, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ShortPassageStub.class.getName());

    private String guid;
    private String owner;
    private String longText;
    private String shortText;
    private ShortPassageAttributeStub attribute;
    private Boolean readOnly;
    private String status;
    private String note;
    private Long expirationTime;
    private Long createdTime;
    private Long modifiedTime;

    public ShortPassageStub()
    {
        this(null);
    }
    public ShortPassageStub(ShortPassage bean)
    {
        if(bean != null) {
            this.guid = bean.getGuid();
            this.owner = bean.getOwner();
            this.longText = bean.getLongText();
            this.shortText = bean.getShortText();
            this.attribute = ShortPassageAttributeStub.convertBeanToStub(bean.getAttribute());
            this.readOnly = bean.isReadOnly();
            this.status = bean.getStatus();
            this.note = bean.getNote();
            this.expirationTime = bean.getExpirationTime();
            this.createdTime = bean.getCreatedTime();
            this.modifiedTime = bean.getModifiedTime();
        }
    }


    @XmlElement
    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    @XmlElement
    public String getOwner()
    {
        return this.owner;
    }
    public void setOwner(String owner)
    {
        this.owner = owner;
    }

    @XmlElement
    public String getLongText()
    {
        return this.longText;
    }
    public void setLongText(String longText)
    {
        this.longText = longText;
    }

    @XmlElement
    public String getShortText()
    {
        return this.shortText;
    }
    public void setShortText(String shortText)
    {
        this.shortText = shortText;
    }

    @XmlElement(name = "attribute")
    @JsonIgnore
    public ShortPassageAttributeStub getAttributeStub()
    {
        return this.attribute;
    }
    public void setAttributeStub(ShortPassageAttributeStub attribute)
    {
        this.attribute = attribute;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=ShortPassageAttributeStub.class)
    public ShortPassageAttribute getAttribute()
    {  
        return getAttributeStub();
    }
    public void setAttribute(ShortPassageAttribute attribute)
    {
        if((attribute == null) || (attribute instanceof ShortPassageAttributeStub)) {
            setAttributeStub((ShortPassageAttributeStub) attribute);
        } else {
            // TBD
            setAttributeStub(ShortPassageAttributeStub.convertBeanToStub(attribute));
        }
    }

    @XmlElement
    public Boolean isReadOnly()
    {
        return this.readOnly;
    }
    public void setReadOnly(Boolean readOnly)
    {
        this.readOnly = readOnly;
    }

    @XmlElement
    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    @XmlElement
    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    @XmlElement
    public Long getExpirationTime()
    {
        return this.expirationTime;
    }
    public void setExpirationTime(Long expirationTime)
    {
        this.expirationTime = expirationTime;
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("guid", this.guid);
        dataMap.put("owner", this.owner);
        dataMap.put("longText", this.longText);
        dataMap.put("shortText", this.shortText);
        dataMap.put("attribute", this.attribute);
        dataMap.put("readOnly", this.readOnly);
        dataMap.put("status", this.status);
        dataMap.put("note", this.note);
        dataMap.put("expirationTime", this.expirationTime);
        dataMap.put("createdTime", this.createdTime);
        dataMap.put("modifiedTime", this.modifiedTime);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = owner == null ? 0 : owner.hashCode();
        _hash = 31 * _hash + delta;
        delta = longText == null ? 0 : longText.hashCode();
        _hash = 31 * _hash + delta;
        delta = shortText == null ? 0 : shortText.hashCode();
        _hash = 31 * _hash + delta;
        delta = attribute == null ? 0 : attribute.hashCode();
        _hash = 31 * _hash + delta;
        delta = readOnly == null ? 0 : readOnly.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        delta = expirationTime == null ? 0 : expirationTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = createdTime == null ? 0 : createdTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static ShortPassageStub convertBeanToStub(ShortPassage bean)
    {
        ShortPassageStub stub = null;
        if(bean instanceof ShortPassageStub) {
            stub = (ShortPassageStub) bean;
        } else {
            if(bean != null) {
                stub = new ShortPassageStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static ShortPassageStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of ShortPassageStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write ShortPassageStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write ShortPassageStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write ShortPassageStub object as a string.", e);
        }
        
        return null;
    }
    public static ShortPassageStub fromJsonString(String jsonStr)
    {
        try {
            ShortPassageStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, ShortPassageStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into ShortPassageStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into ShortPassageStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into ShortPassageStub object.", e);
        }
        
        return null;
    }

}
