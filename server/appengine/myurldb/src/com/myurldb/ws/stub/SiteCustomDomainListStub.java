package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.SiteCustomDomain;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "siteCustomDomains")
@XmlType(propOrder = {"siteCustomDomain"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class SiteCustomDomainListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(SiteCustomDomainListStub.class.getName());

    private List<SiteCustomDomainStub> siteCustomDomains = null;

    public SiteCustomDomainListStub()
    {
        this(new ArrayList<SiteCustomDomainStub>());
    }
    public SiteCustomDomainListStub(List<SiteCustomDomainStub> siteCustomDomains)
    {
        this.siteCustomDomains = siteCustomDomains;
    }

    public boolean isEmpty()
    {
        if(siteCustomDomains == null) {
            return true;
        } else {
            return siteCustomDomains.isEmpty();
        }
    }
    public int getSize()
    {
        if(siteCustomDomains == null) {
            return 0;
        } else {
            return siteCustomDomains.size();
        }
    }

    @XmlElement(name = "siteCustomDomain")
    public List<SiteCustomDomainStub> getSiteCustomDomain()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<SiteCustomDomainStub> getList()
    {
        return siteCustomDomains;
    }
    public void setList(List<SiteCustomDomainStub> siteCustomDomains)
    {
        this.siteCustomDomains = siteCustomDomains;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<SiteCustomDomainStub> it = this.siteCustomDomains.iterator();
        while(it.hasNext()) {
            SiteCustomDomainStub siteCustomDomain = it.next();
            sb.append(siteCustomDomain.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static SiteCustomDomainListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of SiteCustomDomainListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write SiteCustomDomainListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write SiteCustomDomainListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write SiteCustomDomainListStub object as a string.", e);
        }
        
        return null;
    }
    public static SiteCustomDomainListStub fromJsonString(String jsonStr)
    {
        try {
            SiteCustomDomainListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, SiteCustomDomainListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into SiteCustomDomainListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into SiteCustomDomainListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into SiteCustomDomainListStub object.", e);
        }
        
        return null;
    }

}
