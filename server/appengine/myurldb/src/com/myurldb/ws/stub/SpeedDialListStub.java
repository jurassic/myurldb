package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.SpeedDial;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "speedDials")
@XmlType(propOrder = {"speedDial"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class SpeedDialListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(SpeedDialListStub.class.getName());

    private List<SpeedDialStub> speedDials = null;

    public SpeedDialListStub()
    {
        this(new ArrayList<SpeedDialStub>());
    }
    public SpeedDialListStub(List<SpeedDialStub> speedDials)
    {
        this.speedDials = speedDials;
    }

    public boolean isEmpty()
    {
        if(speedDials == null) {
            return true;
        } else {
            return speedDials.isEmpty();
        }
    }
    public int getSize()
    {
        if(speedDials == null) {
            return 0;
        } else {
            return speedDials.size();
        }
    }

    @XmlElement(name = "speedDial")
    public List<SpeedDialStub> getSpeedDial()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<SpeedDialStub> getList()
    {
        return speedDials;
    }
    public void setList(List<SpeedDialStub> speedDials)
    {
        this.speedDials = speedDials;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<SpeedDialStub> it = this.speedDials.iterator();
        while(it.hasNext()) {
            SpeedDialStub speedDial = it.next();
            sb.append(speedDial.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static SpeedDialListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of SpeedDialListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write SpeedDialListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write SpeedDialListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write SpeedDialListStub object as a string.", e);
        }
        
        return null;
    }
    public static SpeedDialListStub fromJsonString(String jsonStr)
    {
        try {
            SpeedDialListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, SpeedDialListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into SpeedDialListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into SpeedDialListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into SpeedDialListStub object.", e);
        }
        
        return null;
    }

}
