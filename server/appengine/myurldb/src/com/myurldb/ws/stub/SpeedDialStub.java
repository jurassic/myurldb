package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.SpeedDial;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "speedDial")
@XmlType(propOrder = {"guid", "user", "code", "shortcut", "shortLink", "keywordLink", "bookmarkLink", "longUrl", "shortUrl", "caseSensitive", "status", "note", "createdTime", "modifiedTime"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SpeedDialStub implements SpeedDial, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(SpeedDialStub.class.getName());

    private String guid;
    private String user;
    private String code;
    private String shortcut;
    private String shortLink;
    private String keywordLink;
    private String bookmarkLink;
    private String longUrl;
    private String shortUrl;
    private Boolean caseSensitive;
    private String status;
    private String note;
    private Long createdTime;
    private Long modifiedTime;

    public SpeedDialStub()
    {
        this(null);
    }
    public SpeedDialStub(SpeedDial bean)
    {
        if(bean != null) {
            this.guid = bean.getGuid();
            this.user = bean.getUser();
            this.code = bean.getCode();
            this.shortcut = bean.getShortcut();
            this.shortLink = bean.getShortLink();
            this.keywordLink = bean.getKeywordLink();
            this.bookmarkLink = bean.getBookmarkLink();
            this.longUrl = bean.getLongUrl();
            this.shortUrl = bean.getShortUrl();
            this.caseSensitive = bean.isCaseSensitive();
            this.status = bean.getStatus();
            this.note = bean.getNote();
            this.createdTime = bean.getCreatedTime();
            this.modifiedTime = bean.getModifiedTime();
        }
    }


    @XmlElement
    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    @XmlElement
    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    @XmlElement
    public String getCode()
    {
        return this.code;
    }
    public void setCode(String code)
    {
        this.code = code;
    }

    @XmlElement
    public String getShortcut()
    {
        return this.shortcut;
    }
    public void setShortcut(String shortcut)
    {
        this.shortcut = shortcut;
    }

    @XmlElement
    public String getShortLink()
    {
        return this.shortLink;
    }
    public void setShortLink(String shortLink)
    {
        this.shortLink = shortLink;
    }

    @XmlElement
    public String getKeywordLink()
    {
        return this.keywordLink;
    }
    public void setKeywordLink(String keywordLink)
    {
        this.keywordLink = keywordLink;
    }

    @XmlElement
    public String getBookmarkLink()
    {
        return this.bookmarkLink;
    }
    public void setBookmarkLink(String bookmarkLink)
    {
        this.bookmarkLink = bookmarkLink;
    }

    @XmlElement
    public String getLongUrl()
    {
        return this.longUrl;
    }
    public void setLongUrl(String longUrl)
    {
        this.longUrl = longUrl;
    }

    @XmlElement
    public String getShortUrl()
    {
        return this.shortUrl;
    }
    public void setShortUrl(String shortUrl)
    {
        this.shortUrl = shortUrl;
    }

    @XmlElement
    public Boolean isCaseSensitive()
    {
        return this.caseSensitive;
    }
    public void setCaseSensitive(Boolean caseSensitive)
    {
        this.caseSensitive = caseSensitive;
    }

    @XmlElement
    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    @XmlElement
    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("guid", this.guid);
        dataMap.put("user", this.user);
        dataMap.put("code", this.code);
        dataMap.put("shortcut", this.shortcut);
        dataMap.put("shortLink", this.shortLink);
        dataMap.put("keywordLink", this.keywordLink);
        dataMap.put("bookmarkLink", this.bookmarkLink);
        dataMap.put("longUrl", this.longUrl);
        dataMap.put("shortUrl", this.shortUrl);
        dataMap.put("caseSensitive", this.caseSensitive);
        dataMap.put("status", this.status);
        dataMap.put("note", this.note);
        dataMap.put("createdTime", this.createdTime);
        dataMap.put("modifiedTime", this.modifiedTime);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = user == null ? 0 : user.hashCode();
        _hash = 31 * _hash + delta;
        delta = code == null ? 0 : code.hashCode();
        _hash = 31 * _hash + delta;
        delta = shortcut == null ? 0 : shortcut.hashCode();
        _hash = 31 * _hash + delta;
        delta = shortLink == null ? 0 : shortLink.hashCode();
        _hash = 31 * _hash + delta;
        delta = keywordLink == null ? 0 : keywordLink.hashCode();
        _hash = 31 * _hash + delta;
        delta = bookmarkLink == null ? 0 : bookmarkLink.hashCode();
        _hash = 31 * _hash + delta;
        delta = longUrl == null ? 0 : longUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = shortUrl == null ? 0 : shortUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = caseSensitive == null ? 0 : caseSensitive.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        delta = createdTime == null ? 0 : createdTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static SpeedDialStub convertBeanToStub(SpeedDial bean)
    {
        SpeedDialStub stub = null;
        if(bean instanceof SpeedDialStub) {
            stub = (SpeedDialStub) bean;
        } else {
            if(bean != null) {
                stub = new SpeedDialStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static SpeedDialStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of SpeedDialStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write SpeedDialStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write SpeedDialStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write SpeedDialStub object as a string.", e);
        }
        
        return null;
    }
    public static SpeedDialStub fromJsonString(String jsonStr)
    {
        try {
            SpeedDialStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, SpeedDialStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into SpeedDialStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into SpeedDialStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into SpeedDialStub object.", e);
        }
        
        return null;
    }

}
