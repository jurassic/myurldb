package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.StreetAddressStruct;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "streetAddressStructs")
@XmlType(propOrder = {"streetAddressStruct"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class StreetAddressStructListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(StreetAddressStructListStub.class.getName());

    private List<StreetAddressStructStub> streetAddressStructs = null;

    public StreetAddressStructListStub()
    {
        this(new ArrayList<StreetAddressStructStub>());
    }
    public StreetAddressStructListStub(List<StreetAddressStructStub> streetAddressStructs)
    {
        this.streetAddressStructs = streetAddressStructs;
    }

    public boolean isEmpty()
    {
        if(streetAddressStructs == null) {
            return true;
        } else {
            return streetAddressStructs.isEmpty();
        }
    }
    public int getSize()
    {
        if(streetAddressStructs == null) {
            return 0;
        } else {
            return streetAddressStructs.size();
        }
    }

    @XmlElement(name = "streetAddressStruct")
    public List<StreetAddressStructStub> getStreetAddressStruct()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<StreetAddressStructStub> getList()
    {
        return streetAddressStructs;
    }
    public void setList(List<StreetAddressStructStub> streetAddressStructs)
    {
        this.streetAddressStructs = streetAddressStructs;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<StreetAddressStructStub> it = this.streetAddressStructs.iterator();
        while(it.hasNext()) {
            StreetAddressStructStub streetAddressStruct = it.next();
            sb.append(streetAddressStruct.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static StreetAddressStructListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of StreetAddressStructListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write StreetAddressStructListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write StreetAddressStructListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write StreetAddressStructListStub object as a string.", e);
        }
        
        return null;
    }
    public static StreetAddressStructListStub fromJsonString(String jsonStr)
    {
        try {
            StreetAddressStructListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, StreetAddressStructListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into StreetAddressStructListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into StreetAddressStructListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into StreetAddressStructListStub object.", e);
        }
        
        return null;
    }

}
