package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.StreetAddressStruct;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "streetAddressStruct")
@XmlType(propOrder = {"uuid", "street1", "street2", "city", "county", "postalCode", "state", "province", "country", "countryName", "note"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class StreetAddressStructStub implements StreetAddressStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(StreetAddressStructStub.class.getName());

    private String uuid;
    private String street1;
    private String street2;
    private String city;
    private String county;
    private String postalCode;
    private String state;
    private String province;
    private String country;
    private String countryName;
    private String note;

    public StreetAddressStructStub()
    {
        this(null);
    }
    public StreetAddressStructStub(StreetAddressStruct bean)
    {
        if(bean != null) {
            this.uuid = bean.getUuid();
            this.street1 = bean.getStreet1();
            this.street2 = bean.getStreet2();
            this.city = bean.getCity();
            this.county = bean.getCounty();
            this.postalCode = bean.getPostalCode();
            this.state = bean.getState();
            this.province = bean.getProvince();
            this.country = bean.getCountry();
            this.countryName = bean.getCountryName();
            this.note = bean.getNote();
        }
    }


    @XmlElement
    public String getUuid()
    {
        return this.uuid;
    }
    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }

    @XmlElement
    public String getStreet1()
    {
        return this.street1;
    }
    public void setStreet1(String street1)
    {
        this.street1 = street1;
    }

    @XmlElement
    public String getStreet2()
    {
        return this.street2;
    }
    public void setStreet2(String street2)
    {
        this.street2 = street2;
    }

    @XmlElement
    public String getCity()
    {
        return this.city;
    }
    public void setCity(String city)
    {
        this.city = city;
    }

    @XmlElement
    public String getCounty()
    {
        return this.county;
    }
    public void setCounty(String county)
    {
        this.county = county;
    }

    @XmlElement
    public String getPostalCode()
    {
        return this.postalCode;
    }
    public void setPostalCode(String postalCode)
    {
        this.postalCode = postalCode;
    }

    @XmlElement
    public String getState()
    {
        return this.state;
    }
    public void setState(String state)
    {
        this.state = state;
    }

    @XmlElement
    public String getProvince()
    {
        return this.province;
    }
    public void setProvince(String province)
    {
        this.province = province;
    }

    @XmlElement
    public String getCountry()
    {
        return this.country;
    }
    public void setCountry(String country)
    {
        this.country = country;
    }

    @XmlElement
    public String getCountryName()
    {
        return this.countryName;
    }
    public void setCountryName(String countryName)
    {
        this.countryName = countryName;
    }

    @XmlElement
    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }


    @XmlTransient
    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getStreet1() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getStreet2() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getCity() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getCounty() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPostalCode() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getState() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getProvince() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getCountry() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getCountryName() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("uuid", this.uuid);
        dataMap.put("street1", this.street1);
        dataMap.put("street2", this.street2);
        dataMap.put("city", this.city);
        dataMap.put("county", this.county);
        dataMap.put("postalCode", this.postalCode);
        dataMap.put("state", this.state);
        dataMap.put("province", this.province);
        dataMap.put("country", this.country);
        dataMap.put("countryName", this.countryName);
        dataMap.put("note", this.note);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = uuid == null ? 0 : uuid.hashCode();
        _hash = 31 * _hash + delta;
        delta = street1 == null ? 0 : street1.hashCode();
        _hash = 31 * _hash + delta;
        delta = street2 == null ? 0 : street2.hashCode();
        _hash = 31 * _hash + delta;
        delta = city == null ? 0 : city.hashCode();
        _hash = 31 * _hash + delta;
        delta = county == null ? 0 : county.hashCode();
        _hash = 31 * _hash + delta;
        delta = postalCode == null ? 0 : postalCode.hashCode();
        _hash = 31 * _hash + delta;
        delta = state == null ? 0 : state.hashCode();
        _hash = 31 * _hash + delta;
        delta = province == null ? 0 : province.hashCode();
        _hash = 31 * _hash + delta;
        delta = country == null ? 0 : country.hashCode();
        _hash = 31 * _hash + delta;
        delta = countryName == null ? 0 : countryName.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static StreetAddressStructStub convertBeanToStub(StreetAddressStruct bean)
    {
        StreetAddressStructStub stub = null;
        if(bean instanceof StreetAddressStructStub) {
            stub = (StreetAddressStructStub) bean;
        } else {
            if(bean != null) {
                stub = new StreetAddressStructStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static StreetAddressStructStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of StreetAddressStructStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write StreetAddressStructStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write StreetAddressStructStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write StreetAddressStructStub object as a string.", e);
        }
        
        return null;
    }
    public static StreetAddressStructStub fromJsonString(String jsonStr)
    {
        try {
            StreetAddressStructStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, StreetAddressStructStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into StreetAddressStructStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into StreetAddressStructStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into StreetAddressStructStub object.", e);
        }
        
        return null;
    }

}
