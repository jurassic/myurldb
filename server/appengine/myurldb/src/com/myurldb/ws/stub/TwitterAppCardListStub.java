package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.TwitterAppCard;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "twitterAppCards")
@XmlType(propOrder = {"twitterAppCard"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class TwitterAppCardListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(TwitterAppCardListStub.class.getName());

    private List<TwitterAppCardStub> twitterAppCards = null;

    public TwitterAppCardListStub()
    {
        this(new ArrayList<TwitterAppCardStub>());
    }
    public TwitterAppCardListStub(List<TwitterAppCardStub> twitterAppCards)
    {
        this.twitterAppCards = twitterAppCards;
    }

    public boolean isEmpty()
    {
        if(twitterAppCards == null) {
            return true;
        } else {
            return twitterAppCards.isEmpty();
        }
    }
    public int getSize()
    {
        if(twitterAppCards == null) {
            return 0;
        } else {
            return twitterAppCards.size();
        }
    }

    @XmlElement(name = "twitterAppCard")
    public List<TwitterAppCardStub> getTwitterAppCard()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<TwitterAppCardStub> getList()
    {
        return twitterAppCards;
    }
    public void setList(List<TwitterAppCardStub> twitterAppCards)
    {
        this.twitterAppCards = twitterAppCards;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<TwitterAppCardStub> it = this.twitterAppCards.iterator();
        while(it.hasNext()) {
            TwitterAppCardStub twitterAppCard = it.next();
            sb.append(twitterAppCard.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static TwitterAppCardListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of TwitterAppCardListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write TwitterAppCardListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write TwitterAppCardListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write TwitterAppCardListStub object as a string.", e);
        }
        
        return null;
    }
    public static TwitterAppCardListStub fromJsonString(String jsonStr)
    {
        try {
            TwitterAppCardListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, TwitterAppCardListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into TwitterAppCardListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into TwitterAppCardListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into TwitterAppCardListStub object.", e);
        }
        
        return null;
    }

}
