package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "twitterCardAppInfos")
@XmlType(propOrder = {"twitterCardAppInfo"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class TwitterCardAppInfoListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(TwitterCardAppInfoListStub.class.getName());

    private List<TwitterCardAppInfoStub> twitterCardAppInfos = null;

    public TwitterCardAppInfoListStub()
    {
        this(new ArrayList<TwitterCardAppInfoStub>());
    }
    public TwitterCardAppInfoListStub(List<TwitterCardAppInfoStub> twitterCardAppInfos)
    {
        this.twitterCardAppInfos = twitterCardAppInfos;
    }

    public boolean isEmpty()
    {
        if(twitterCardAppInfos == null) {
            return true;
        } else {
            return twitterCardAppInfos.isEmpty();
        }
    }
    public int getSize()
    {
        if(twitterCardAppInfos == null) {
            return 0;
        } else {
            return twitterCardAppInfos.size();
        }
    }

    @XmlElement(name = "twitterCardAppInfo")
    public List<TwitterCardAppInfoStub> getTwitterCardAppInfo()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<TwitterCardAppInfoStub> getList()
    {
        return twitterCardAppInfos;
    }
    public void setList(List<TwitterCardAppInfoStub> twitterCardAppInfos)
    {
        this.twitterCardAppInfos = twitterCardAppInfos;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<TwitterCardAppInfoStub> it = this.twitterCardAppInfos.iterator();
        while(it.hasNext()) {
            TwitterCardAppInfoStub twitterCardAppInfo = it.next();
            sb.append(twitterCardAppInfo.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static TwitterCardAppInfoListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of TwitterCardAppInfoListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write TwitterCardAppInfoListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write TwitterCardAppInfoListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write TwitterCardAppInfoListStub object as a string.", e);
        }
        
        return null;
    }
    public static TwitterCardAppInfoListStub fromJsonString(String jsonStr)
    {
        try {
            TwitterCardAppInfoListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, TwitterCardAppInfoListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into TwitterCardAppInfoListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into TwitterCardAppInfoListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into TwitterCardAppInfoListStub object.", e);
        }
        
        return null;
    }

}
