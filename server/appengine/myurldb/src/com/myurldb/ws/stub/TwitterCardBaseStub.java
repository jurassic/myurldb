package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.TwitterCardBase;
import com.myurldb.ws.util.JsonUtil;


// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class TwitterCardBaseStub implements TwitterCardBase, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(TwitterCardBaseStub.class.getName());

    private String guid;
    private String card;
    private String url;
    private String title;
    private String description;
    private String site;
    private String siteId;
    private String creator;
    private String creatorId;
    private Long createdTime;
    private Long modifiedTime;

    public TwitterCardBaseStub()
    {
        this(null);
    }
    public TwitterCardBaseStub(TwitterCardBase bean)
    {
        if(bean != null) {
            this.guid = bean.getGuid();
            this.card = bean.getCard();
            this.url = bean.getUrl();
            this.title = bean.getTitle();
            this.description = bean.getDescription();
            this.site = bean.getSite();
            this.siteId = bean.getSiteId();
            this.creator = bean.getCreator();
            this.creatorId = bean.getCreatorId();
            this.createdTime = bean.getCreatedTime();
            this.modifiedTime = bean.getModifiedTime();
        }
    }


    @XmlTransient
    //@JsonIgnore
    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    @XmlTransient
    //@JsonIgnore
    public String getCard()
    {
        return this.card;
    }
    public void setCard(String card)
    {
        this.card = card;
    }

    @XmlTransient
    //@JsonIgnore
    public String getUrl()
    {
        return this.url;
    }
    public void setUrl(String url)
    {
        this.url = url;
    }

    @XmlTransient
    //@JsonIgnore
    public String getTitle()
    {
        return this.title;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }

    @XmlTransient
    //@JsonIgnore
    public String getDescription()
    {
        return this.description;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }

    @XmlTransient
    //@JsonIgnore
    public String getSite()
    {
        return this.site;
    }
    public void setSite(String site)
    {
        this.site = site;
    }

    @XmlTransient
    //@JsonIgnore
    public String getSiteId()
    {
        return this.siteId;
    }
    public void setSiteId(String siteId)
    {
        this.siteId = siteId;
    }

    @XmlTransient
    //@JsonIgnore
    public String getCreator()
    {
        return this.creator;
    }
    public void setCreator(String creator)
    {
        this.creator = creator;
    }

    @XmlTransient
    //@JsonIgnore
    public String getCreatorId()
    {
        return this.creatorId;
    }
    public void setCreatorId(String creatorId)
    {
        this.creatorId = creatorId;
    }

    @XmlTransient
    //@JsonIgnore
    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    @XmlTransient
    //@JsonIgnore
    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("guid", this.guid);
        dataMap.put("card", this.card);
        dataMap.put("url", this.url);
        dataMap.put("title", this.title);
        dataMap.put("description", this.description);
        dataMap.put("site", this.site);
        dataMap.put("siteId", this.siteId);
        dataMap.put("creator", this.creator);
        dataMap.put("creatorId", this.creatorId);
        dataMap.put("createdTime", this.createdTime);
        dataMap.put("modifiedTime", this.modifiedTime);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = card == null ? 0 : card.hashCode();
        _hash = 31 * _hash + delta;
        delta = url == null ? 0 : url.hashCode();
        _hash = 31 * _hash + delta;
        delta = title == null ? 0 : title.hashCode();
        _hash = 31 * _hash + delta;
        delta = description == null ? 0 : description.hashCode();
        _hash = 31 * _hash + delta;
        delta = site == null ? 0 : site.hashCode();
        _hash = 31 * _hash + delta;
        delta = siteId == null ? 0 : siteId.hashCode();
        _hash = 31 * _hash + delta;
        delta = creator == null ? 0 : creator.hashCode();
        _hash = 31 * _hash + delta;
        delta = creatorId == null ? 0 : creatorId.hashCode();
        _hash = 31 * _hash + delta;
        delta = createdTime == null ? 0 : createdTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static TwitterCardBaseStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of TwitterCardBaseStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write TwitterCardBaseStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write TwitterCardBaseStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write TwitterCardBaseStub object as a string.", e);
        }
        
        return null;
    }
    public static TwitterCardBaseStub fromJsonString(String jsonStr)
    {
        try {
            TwitterCardBaseStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, TwitterCardBaseStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into TwitterCardBaseStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into TwitterCardBaseStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into TwitterCardBaseStub object.", e);
        }
        
        return null;
    }

}
