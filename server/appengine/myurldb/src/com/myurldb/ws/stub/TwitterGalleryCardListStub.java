package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.TwitterGalleryCard;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "twitterGalleryCards")
@XmlType(propOrder = {"twitterGalleryCard"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class TwitterGalleryCardListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(TwitterGalleryCardListStub.class.getName());

    private List<TwitterGalleryCardStub> twitterGalleryCards = null;

    public TwitterGalleryCardListStub()
    {
        this(new ArrayList<TwitterGalleryCardStub>());
    }
    public TwitterGalleryCardListStub(List<TwitterGalleryCardStub> twitterGalleryCards)
    {
        this.twitterGalleryCards = twitterGalleryCards;
    }

    public boolean isEmpty()
    {
        if(twitterGalleryCards == null) {
            return true;
        } else {
            return twitterGalleryCards.isEmpty();
        }
    }
    public int getSize()
    {
        if(twitterGalleryCards == null) {
            return 0;
        } else {
            return twitterGalleryCards.size();
        }
    }

    @XmlElement(name = "twitterGalleryCard")
    public List<TwitterGalleryCardStub> getTwitterGalleryCard()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<TwitterGalleryCardStub> getList()
    {
        return twitterGalleryCards;
    }
    public void setList(List<TwitterGalleryCardStub> twitterGalleryCards)
    {
        this.twitterGalleryCards = twitterGalleryCards;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<TwitterGalleryCardStub> it = this.twitterGalleryCards.iterator();
        while(it.hasNext()) {
            TwitterGalleryCardStub twitterGalleryCard = it.next();
            sb.append(twitterGalleryCard.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static TwitterGalleryCardListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of TwitterGalleryCardListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write TwitterGalleryCardListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write TwitterGalleryCardListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write TwitterGalleryCardListStub object as a string.", e);
        }
        
        return null;
    }
    public static TwitterGalleryCardListStub fromJsonString(String jsonStr)
    {
        try {
            TwitterGalleryCardListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, TwitterGalleryCardListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into TwitterGalleryCardListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into TwitterGalleryCardListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into TwitterGalleryCardListStub object.", e);
        }
        
        return null;
    }

}
