package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.TwitterGalleryCard;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "twitterGalleryCard")
@XmlType(propOrder = {"guid", "card", "url", "title", "description", "site", "siteId", "creator", "creatorId", "image0", "image1", "image2", "image3", "createdTime", "modifiedTime"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class TwitterGalleryCardStub extends TwitterCardBaseStub implements TwitterGalleryCard, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(TwitterGalleryCardStub.class.getName());

    private String image0;
    private String image1;
    private String image2;
    private String image3;

    public TwitterGalleryCardStub()
    {
        this(null);
    }
    public TwitterGalleryCardStub(TwitterGalleryCard bean)
    {
        super(bean);
        if(bean != null) {
            this.image0 = bean.getImage0();
            this.image1 = bean.getImage1();
            this.image2 = bean.getImage2();
            this.image3 = bean.getImage3();
        }
    }


    @XmlElement
    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    @XmlElement
    public String getCard()
    {
        return super.getCard();
    }
    public void setCard(String card)
    {
        super.setCard(card);
    }

    @XmlElement
    public String getUrl()
    {
        return super.getUrl();
    }
    public void setUrl(String url)
    {
        super.setUrl(url);
    }

    @XmlElement
    public String getTitle()
    {
        return super.getTitle();
    }
    public void setTitle(String title)
    {
        super.setTitle(title);
    }

    @XmlElement
    public String getDescription()
    {
        return super.getDescription();
    }
    public void setDescription(String description)
    {
        super.setDescription(description);
    }

    @XmlElement
    public String getSite()
    {
        return super.getSite();
    }
    public void setSite(String site)
    {
        super.setSite(site);
    }

    @XmlElement
    public String getSiteId()
    {
        return super.getSiteId();
    }
    public void setSiteId(String siteId)
    {
        super.setSiteId(siteId);
    }

    @XmlElement
    public String getCreator()
    {
        return super.getCreator();
    }
    public void setCreator(String creator)
    {
        super.setCreator(creator);
    }

    @XmlElement
    public String getCreatorId()
    {
        return super.getCreatorId();
    }
    public void setCreatorId(String creatorId)
    {
        super.setCreatorId(creatorId);
    }

    @XmlElement
    public String getImage0()
    {
        return this.image0;
    }
    public void setImage0(String image0)
    {
        this.image0 = image0;
    }

    @XmlElement
    public String getImage1()
    {
        return this.image1;
    }
    public void setImage1(String image1)
    {
        this.image1 = image1;
    }

    @XmlElement
    public String getImage2()
    {
        return this.image2;
    }
    public void setImage2(String image2)
    {
        this.image2 = image2;
    }

    @XmlElement
    public String getImage3()
    {
        return this.image3;
    }
    public void setImage3(String image3)
    {
        this.image3 = image3;
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("image0", this.image0);
        dataMap.put("image1", this.image1);
        dataMap.put("image2", this.image2);
        dataMap.put("image3", this.image3);

        return dataMap;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = image0 == null ? 0 : image0.hashCode();
        _hash = 31 * _hash + delta;
        delta = image1 == null ? 0 : image1.hashCode();
        _hash = 31 * _hash + delta;
        delta = image2 == null ? 0 : image2.hashCode();
        _hash = 31 * _hash + delta;
        delta = image3 == null ? 0 : image3.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static TwitterGalleryCardStub convertBeanToStub(TwitterGalleryCard bean)
    {
        TwitterGalleryCardStub stub = null;
        if(bean instanceof TwitterGalleryCardStub) {
            stub = (TwitterGalleryCardStub) bean;
        } else {
            if(bean != null) {
                stub = new TwitterGalleryCardStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static TwitterGalleryCardStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of TwitterGalleryCardStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write TwitterGalleryCardStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write TwitterGalleryCardStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write TwitterGalleryCardStub object as a string.", e);
        }
        
        return null;
    }
    public static TwitterGalleryCardStub fromJsonString(String jsonStr)
    {
        try {
            TwitterGalleryCardStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, TwitterGalleryCardStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into TwitterGalleryCardStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into TwitterGalleryCardStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into TwitterGalleryCardStub object.", e);
        }
        
        return null;
    }

}
