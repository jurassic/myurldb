package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.TwitterPlayerCard;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "twitterPlayerCard")
@XmlType(propOrder = {"guid", "card", "url", "title", "description", "site", "siteId", "creator", "creatorId", "image", "imageWidth", "imageHeight", "player", "playerWidth", "playerHeight", "playerStream", "playerStreamContentType", "createdTime", "modifiedTime"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class TwitterPlayerCardStub extends TwitterCardBaseStub implements TwitterPlayerCard, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(TwitterPlayerCardStub.class.getName());

    private String image;
    private Integer imageWidth;
    private Integer imageHeight;
    private String player;
    private Integer playerWidth;
    private Integer playerHeight;
    private String playerStream;
    private String playerStreamContentType;

    public TwitterPlayerCardStub()
    {
        this(null);
    }
    public TwitterPlayerCardStub(TwitterPlayerCard bean)
    {
        super(bean);
        if(bean != null) {
            this.image = bean.getImage();
            this.imageWidth = bean.getImageWidth();
            this.imageHeight = bean.getImageHeight();
            this.player = bean.getPlayer();
            this.playerWidth = bean.getPlayerWidth();
            this.playerHeight = bean.getPlayerHeight();
            this.playerStream = bean.getPlayerStream();
            this.playerStreamContentType = bean.getPlayerStreamContentType();
        }
    }


    @XmlElement
    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    @XmlElement
    public String getCard()
    {
        return super.getCard();
    }
    public void setCard(String card)
    {
        super.setCard(card);
    }

    @XmlElement
    public String getUrl()
    {
        return super.getUrl();
    }
    public void setUrl(String url)
    {
        super.setUrl(url);
    }

    @XmlElement
    public String getTitle()
    {
        return super.getTitle();
    }
    public void setTitle(String title)
    {
        super.setTitle(title);
    }

    @XmlElement
    public String getDescription()
    {
        return super.getDescription();
    }
    public void setDescription(String description)
    {
        super.setDescription(description);
    }

    @XmlElement
    public String getSite()
    {
        return super.getSite();
    }
    public void setSite(String site)
    {
        super.setSite(site);
    }

    @XmlElement
    public String getSiteId()
    {
        return super.getSiteId();
    }
    public void setSiteId(String siteId)
    {
        super.setSiteId(siteId);
    }

    @XmlElement
    public String getCreator()
    {
        return super.getCreator();
    }
    public void setCreator(String creator)
    {
        super.setCreator(creator);
    }

    @XmlElement
    public String getCreatorId()
    {
        return super.getCreatorId();
    }
    public void setCreatorId(String creatorId)
    {
        super.setCreatorId(creatorId);
    }

    @XmlElement
    public String getImage()
    {
        return this.image;
    }
    public void setImage(String image)
    {
        this.image = image;
    }

    @XmlElement
    public Integer getImageWidth()
    {
        return this.imageWidth;
    }
    public void setImageWidth(Integer imageWidth)
    {
        this.imageWidth = imageWidth;
    }

    @XmlElement
    public Integer getImageHeight()
    {
        return this.imageHeight;
    }
    public void setImageHeight(Integer imageHeight)
    {
        this.imageHeight = imageHeight;
    }

    @XmlElement
    public String getPlayer()
    {
        return this.player;
    }
    public void setPlayer(String player)
    {
        this.player = player;
    }

    @XmlElement
    public Integer getPlayerWidth()
    {
        return this.playerWidth;
    }
    public void setPlayerWidth(Integer playerWidth)
    {
        this.playerWidth = playerWidth;
    }

    @XmlElement
    public Integer getPlayerHeight()
    {
        return this.playerHeight;
    }
    public void setPlayerHeight(Integer playerHeight)
    {
        this.playerHeight = playerHeight;
    }

    @XmlElement
    public String getPlayerStream()
    {
        return this.playerStream;
    }
    public void setPlayerStream(String playerStream)
    {
        this.playerStream = playerStream;
    }

    @XmlElement
    public String getPlayerStreamContentType()
    {
        return this.playerStreamContentType;
    }
    public void setPlayerStreamContentType(String playerStreamContentType)
    {
        this.playerStreamContentType = playerStreamContentType;
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("image", this.image);
        dataMap.put("imageWidth", this.imageWidth);
        dataMap.put("imageHeight", this.imageHeight);
        dataMap.put("player", this.player);
        dataMap.put("playerWidth", this.playerWidth);
        dataMap.put("playerHeight", this.playerHeight);
        dataMap.put("playerStream", this.playerStream);
        dataMap.put("playerStreamContentType", this.playerStreamContentType);

        return dataMap;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = image == null ? 0 : image.hashCode();
        _hash = 31 * _hash + delta;
        delta = imageWidth == null ? 0 : imageWidth.hashCode();
        _hash = 31 * _hash + delta;
        delta = imageHeight == null ? 0 : imageHeight.hashCode();
        _hash = 31 * _hash + delta;
        delta = player == null ? 0 : player.hashCode();
        _hash = 31 * _hash + delta;
        delta = playerWidth == null ? 0 : playerWidth.hashCode();
        _hash = 31 * _hash + delta;
        delta = playerHeight == null ? 0 : playerHeight.hashCode();
        _hash = 31 * _hash + delta;
        delta = playerStream == null ? 0 : playerStream.hashCode();
        _hash = 31 * _hash + delta;
        delta = playerStreamContentType == null ? 0 : playerStreamContentType.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static TwitterPlayerCardStub convertBeanToStub(TwitterPlayerCard bean)
    {
        TwitterPlayerCardStub stub = null;
        if(bean instanceof TwitterPlayerCardStub) {
            stub = (TwitterPlayerCardStub) bean;
        } else {
            if(bean != null) {
                stub = new TwitterPlayerCardStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static TwitterPlayerCardStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of TwitterPlayerCardStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write TwitterPlayerCardStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write TwitterPlayerCardStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write TwitterPlayerCardStub object as a string.", e);
        }
        
        return null;
    }
    public static TwitterPlayerCardStub fromJsonString(String jsonStr)
    {
        try {
            TwitterPlayerCardStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, TwitterPlayerCardStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into TwitterPlayerCardStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into TwitterPlayerCardStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into TwitterPlayerCardStub object.", e);
        }
        
        return null;
    }

}
