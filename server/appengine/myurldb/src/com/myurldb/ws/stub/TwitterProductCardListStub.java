package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.TwitterProductCard;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "twitterProductCards")
@XmlType(propOrder = {"twitterProductCard"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class TwitterProductCardListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(TwitterProductCardListStub.class.getName());

    private List<TwitterProductCardStub> twitterProductCards = null;

    public TwitterProductCardListStub()
    {
        this(new ArrayList<TwitterProductCardStub>());
    }
    public TwitterProductCardListStub(List<TwitterProductCardStub> twitterProductCards)
    {
        this.twitterProductCards = twitterProductCards;
    }

    public boolean isEmpty()
    {
        if(twitterProductCards == null) {
            return true;
        } else {
            return twitterProductCards.isEmpty();
        }
    }
    public int getSize()
    {
        if(twitterProductCards == null) {
            return 0;
        } else {
            return twitterProductCards.size();
        }
    }

    @XmlElement(name = "twitterProductCard")
    public List<TwitterProductCardStub> getTwitterProductCard()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<TwitterProductCardStub> getList()
    {
        return twitterProductCards;
    }
    public void setList(List<TwitterProductCardStub> twitterProductCards)
    {
        this.twitterProductCards = twitterProductCards;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<TwitterProductCardStub> it = this.twitterProductCards.iterator();
        while(it.hasNext()) {
            TwitterProductCardStub twitterProductCard = it.next();
            sb.append(twitterProductCard.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static TwitterProductCardListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of TwitterProductCardListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write TwitterProductCardListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write TwitterProductCardListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write TwitterProductCardListStub object as a string.", e);
        }
        
        return null;
    }
    public static TwitterProductCardListStub fromJsonString(String jsonStr)
    {
        try {
            TwitterProductCardListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, TwitterProductCardListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into TwitterProductCardListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into TwitterProductCardListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into TwitterProductCardListStub object.", e);
        }
        
        return null;
    }

}
