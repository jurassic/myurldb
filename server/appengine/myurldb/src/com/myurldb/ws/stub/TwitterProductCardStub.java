package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.TwitterProductCard;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "twitterProductCard")
@XmlType(propOrder = {"guid", "card", "url", "title", "description", "site", "siteId", "creator", "creatorId", "image", "imageWidth", "imageHeight", "data1Stub", "data2Stub", "createdTime", "modifiedTime"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class TwitterProductCardStub extends TwitterCardBaseStub implements TwitterProductCard, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(TwitterProductCardStub.class.getName());

    private String image;
    private Integer imageWidth;
    private Integer imageHeight;
    private TwitterCardProductDataStub data1;
    private TwitterCardProductDataStub data2;

    public TwitterProductCardStub()
    {
        this(null);
    }
    public TwitterProductCardStub(TwitterProductCard bean)
    {
        super(bean);
        if(bean != null) {
            this.image = bean.getImage();
            this.imageWidth = bean.getImageWidth();
            this.imageHeight = bean.getImageHeight();
            this.data1 = TwitterCardProductDataStub.convertBeanToStub(bean.getData1());
            this.data2 = TwitterCardProductDataStub.convertBeanToStub(bean.getData2());
        }
    }


    @XmlElement
    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    @XmlElement
    public String getCard()
    {
        return super.getCard();
    }
    public void setCard(String card)
    {
        super.setCard(card);
    }

    @XmlElement
    public String getUrl()
    {
        return super.getUrl();
    }
    public void setUrl(String url)
    {
        super.setUrl(url);
    }

    @XmlElement
    public String getTitle()
    {
        return super.getTitle();
    }
    public void setTitle(String title)
    {
        super.setTitle(title);
    }

    @XmlElement
    public String getDescription()
    {
        return super.getDescription();
    }
    public void setDescription(String description)
    {
        super.setDescription(description);
    }

    @XmlElement
    public String getSite()
    {
        return super.getSite();
    }
    public void setSite(String site)
    {
        super.setSite(site);
    }

    @XmlElement
    public String getSiteId()
    {
        return super.getSiteId();
    }
    public void setSiteId(String siteId)
    {
        super.setSiteId(siteId);
    }

    @XmlElement
    public String getCreator()
    {
        return super.getCreator();
    }
    public void setCreator(String creator)
    {
        super.setCreator(creator);
    }

    @XmlElement
    public String getCreatorId()
    {
        return super.getCreatorId();
    }
    public void setCreatorId(String creatorId)
    {
        super.setCreatorId(creatorId);
    }

    @XmlElement
    public String getImage()
    {
        return this.image;
    }
    public void setImage(String image)
    {
        this.image = image;
    }

    @XmlElement
    public Integer getImageWidth()
    {
        return this.imageWidth;
    }
    public void setImageWidth(Integer imageWidth)
    {
        this.imageWidth = imageWidth;
    }

    @XmlElement
    public Integer getImageHeight()
    {
        return this.imageHeight;
    }
    public void setImageHeight(Integer imageHeight)
    {
        this.imageHeight = imageHeight;
    }

    @XmlElement(name = "data1")
    @JsonIgnore
    public TwitterCardProductDataStub getData1Stub()
    {
        return this.data1;
    }
    public void setData1Stub(TwitterCardProductDataStub data1)
    {
        this.data1 = data1;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=TwitterCardProductDataStub.class)
    public TwitterCardProductData getData1()
    {  
        return getData1Stub();
    }
    public void setData1(TwitterCardProductData data1)
    {
        if((data1 == null) || (data1 instanceof TwitterCardProductDataStub)) {
            setData1Stub((TwitterCardProductDataStub) data1);
        } else {
            // TBD
            setData1Stub(TwitterCardProductDataStub.convertBeanToStub(data1));
        }
    }

    @XmlElement(name = "data2")
    @JsonIgnore
    public TwitterCardProductDataStub getData2Stub()
    {
        return this.data2;
    }
    public void setData2Stub(TwitterCardProductDataStub data2)
    {
        this.data2 = data2;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=TwitterCardProductDataStub.class)
    public TwitterCardProductData getData2()
    {  
        return getData2Stub();
    }
    public void setData2(TwitterCardProductData data2)
    {
        if((data2 == null) || (data2 instanceof TwitterCardProductDataStub)) {
            setData2Stub((TwitterCardProductDataStub) data2);
        } else {
            // TBD
            setData2Stub(TwitterCardProductDataStub.convertBeanToStub(data2));
        }
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("image", this.image);
        dataMap.put("imageWidth", this.imageWidth);
        dataMap.put("imageHeight", this.imageHeight);
        dataMap.put("data1", this.data1);
        dataMap.put("data2", this.data2);

        return dataMap;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = image == null ? 0 : image.hashCode();
        _hash = 31 * _hash + delta;
        delta = imageWidth == null ? 0 : imageWidth.hashCode();
        _hash = 31 * _hash + delta;
        delta = imageHeight == null ? 0 : imageHeight.hashCode();
        _hash = 31 * _hash + delta;
        delta = data1 == null ? 0 : data1.hashCode();
        _hash = 31 * _hash + delta;
        delta = data2 == null ? 0 : data2.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static TwitterProductCardStub convertBeanToStub(TwitterProductCard bean)
    {
        TwitterProductCardStub stub = null;
        if(bean instanceof TwitterProductCardStub) {
            stub = (TwitterProductCardStub) bean;
        } else {
            if(bean != null) {
                stub = new TwitterProductCardStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static TwitterProductCardStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of TwitterProductCardStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write TwitterProductCardStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write TwitterProductCardStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write TwitterProductCardStub object as a string.", e);
        }
        
        return null;
    }
    public static TwitterProductCardStub fromJsonString(String jsonStr)
    {
        try {
            TwitterProductCardStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, TwitterProductCardStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into TwitterProductCardStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into TwitterProductCardStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into TwitterProductCardStub object.", e);
        }
        
        return null;
    }

}
