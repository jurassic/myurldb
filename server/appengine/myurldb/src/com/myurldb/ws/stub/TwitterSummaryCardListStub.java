package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.TwitterSummaryCard;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "twitterSummaryCards")
@XmlType(propOrder = {"twitterSummaryCard"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class TwitterSummaryCardListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(TwitterSummaryCardListStub.class.getName());

    private List<TwitterSummaryCardStub> twitterSummaryCards = null;

    public TwitterSummaryCardListStub()
    {
        this(new ArrayList<TwitterSummaryCardStub>());
    }
    public TwitterSummaryCardListStub(List<TwitterSummaryCardStub> twitterSummaryCards)
    {
        this.twitterSummaryCards = twitterSummaryCards;
    }

    public boolean isEmpty()
    {
        if(twitterSummaryCards == null) {
            return true;
        } else {
            return twitterSummaryCards.isEmpty();
        }
    }
    public int getSize()
    {
        if(twitterSummaryCards == null) {
            return 0;
        } else {
            return twitterSummaryCards.size();
        }
    }

    @XmlElement(name = "twitterSummaryCard")
    public List<TwitterSummaryCardStub> getTwitterSummaryCard()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<TwitterSummaryCardStub> getList()
    {
        return twitterSummaryCards;
    }
    public void setList(List<TwitterSummaryCardStub> twitterSummaryCards)
    {
        this.twitterSummaryCards = twitterSummaryCards;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<TwitterSummaryCardStub> it = this.twitterSummaryCards.iterator();
        while(it.hasNext()) {
            TwitterSummaryCardStub twitterSummaryCard = it.next();
            sb.append(twitterSummaryCard.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static TwitterSummaryCardListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of TwitterSummaryCardListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write TwitterSummaryCardListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write TwitterSummaryCardListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write TwitterSummaryCardListStub object as a string.", e);
        }
        
        return null;
    }
    public static TwitterSummaryCardListStub fromJsonString(String jsonStr)
    {
        try {
            TwitterSummaryCardListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, TwitterSummaryCardListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into TwitterSummaryCardListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into TwitterSummaryCardListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into TwitterSummaryCardListStub object.", e);
        }
        
        return null;
    }

}
