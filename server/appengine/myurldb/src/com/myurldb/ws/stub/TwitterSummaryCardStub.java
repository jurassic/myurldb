package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.TwitterSummaryCard;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "twitterSummaryCard")
@XmlType(propOrder = {"guid", "card", "url", "title", "description", "site", "siteId", "creator", "creatorId", "image", "imageWidth", "imageHeight", "createdTime", "modifiedTime"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class TwitterSummaryCardStub extends TwitterCardBaseStub implements TwitterSummaryCard, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(TwitterSummaryCardStub.class.getName());

    private String image;
    private Integer imageWidth;
    private Integer imageHeight;

    public TwitterSummaryCardStub()
    {
        this(null);
    }
    public TwitterSummaryCardStub(TwitterSummaryCard bean)
    {
        super(bean);
        if(bean != null) {
            this.image = bean.getImage();
            this.imageWidth = bean.getImageWidth();
            this.imageHeight = bean.getImageHeight();
        }
    }


    @XmlElement
    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    @XmlElement
    public String getCard()
    {
        return super.getCard();
    }
    public void setCard(String card)
    {
        super.setCard(card);
    }

    @XmlElement
    public String getUrl()
    {
        return super.getUrl();
    }
    public void setUrl(String url)
    {
        super.setUrl(url);
    }

    @XmlElement
    public String getTitle()
    {
        return super.getTitle();
    }
    public void setTitle(String title)
    {
        super.setTitle(title);
    }

    @XmlElement
    public String getDescription()
    {
        return super.getDescription();
    }
    public void setDescription(String description)
    {
        super.setDescription(description);
    }

    @XmlElement
    public String getSite()
    {
        return super.getSite();
    }
    public void setSite(String site)
    {
        super.setSite(site);
    }

    @XmlElement
    public String getSiteId()
    {
        return super.getSiteId();
    }
    public void setSiteId(String siteId)
    {
        super.setSiteId(siteId);
    }

    @XmlElement
    public String getCreator()
    {
        return super.getCreator();
    }
    public void setCreator(String creator)
    {
        super.setCreator(creator);
    }

    @XmlElement
    public String getCreatorId()
    {
        return super.getCreatorId();
    }
    public void setCreatorId(String creatorId)
    {
        super.setCreatorId(creatorId);
    }

    @XmlElement
    public String getImage()
    {
        return this.image;
    }
    public void setImage(String image)
    {
        this.image = image;
    }

    @XmlElement
    public Integer getImageWidth()
    {
        return this.imageWidth;
    }
    public void setImageWidth(Integer imageWidth)
    {
        this.imageWidth = imageWidth;
    }

    @XmlElement
    public Integer getImageHeight()
    {
        return this.imageHeight;
    }
    public void setImageHeight(Integer imageHeight)
    {
        this.imageHeight = imageHeight;
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("image", this.image);
        dataMap.put("imageWidth", this.imageWidth);
        dataMap.put("imageHeight", this.imageHeight);

        return dataMap;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = image == null ? 0 : image.hashCode();
        _hash = 31 * _hash + delta;
        delta = imageWidth == null ? 0 : imageWidth.hashCode();
        _hash = 31 * _hash + delta;
        delta = imageHeight == null ? 0 : imageHeight.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static TwitterSummaryCardStub convertBeanToStub(TwitterSummaryCard bean)
    {
        TwitterSummaryCardStub stub = null;
        if(bean instanceof TwitterSummaryCardStub) {
            stub = (TwitterSummaryCardStub) bean;
        } else {
            if(bean != null) {
                stub = new TwitterSummaryCardStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static TwitterSummaryCardStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of TwitterSummaryCardStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write TwitterSummaryCardStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write TwitterSummaryCardStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write TwitterSummaryCardStub object as a string.", e);
        }
        
        return null;
    }
    public static TwitterSummaryCardStub fromJsonString(String jsonStr)
    {
        try {
            TwitterSummaryCardStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, TwitterSummaryCardStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into TwitterSummaryCardStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into TwitterSummaryCardStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into TwitterSummaryCardStub object.", e);
        }
        
        return null;
    }

}
