package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.UrlRating;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "urlRatings")
@XmlType(propOrder = {"urlRating"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class UrlRatingListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UrlRatingListStub.class.getName());

    private List<UrlRatingStub> urlRatings = null;

    public UrlRatingListStub()
    {
        this(new ArrayList<UrlRatingStub>());
    }
    public UrlRatingListStub(List<UrlRatingStub> urlRatings)
    {
        this.urlRatings = urlRatings;
    }

    public boolean isEmpty()
    {
        if(urlRatings == null) {
            return true;
        } else {
            return urlRatings.isEmpty();
        }
    }
    public int getSize()
    {
        if(urlRatings == null) {
            return 0;
        } else {
            return urlRatings.size();
        }
    }

    @XmlElement(name = "urlRating")
    public List<UrlRatingStub> getUrlRating()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<UrlRatingStub> getList()
    {
        return urlRatings;
    }
    public void setList(List<UrlRatingStub> urlRatings)
    {
        this.urlRatings = urlRatings;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<UrlRatingStub> it = this.urlRatings.iterator();
        while(it.hasNext()) {
            UrlRatingStub urlRating = it.next();
            sb.append(urlRating.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static UrlRatingListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of UrlRatingListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write UrlRatingListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write UrlRatingListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write UrlRatingListStub object as a string.", e);
        }
        
        return null;
    }
    public static UrlRatingListStub fromJsonString(String jsonStr)
    {
        try {
            UrlRatingListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, UrlRatingListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into UrlRatingListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into UrlRatingListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into UrlRatingListStub object.", e);
        }
        
        return null;
    }

}
