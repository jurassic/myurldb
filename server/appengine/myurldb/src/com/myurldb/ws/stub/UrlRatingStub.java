package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.UrlRating;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "urlRating")
@XmlType(propOrder = {"guid", "domain", "longUrl", "longUrlHash", "preview", "flag", "rating", "note", "ratedTime", "createdTime", "modifiedTime"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class UrlRatingStub implements UrlRating, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UrlRatingStub.class.getName());

    private String guid;
    private String domain;
    private String longUrl;
    private String longUrlHash;
    private String preview;
    private String flag;
    private Double rating;
    private String note;
    private Long ratedTime;
    private Long createdTime;
    private Long modifiedTime;

    public UrlRatingStub()
    {
        this(null);
    }
    public UrlRatingStub(UrlRating bean)
    {
        if(bean != null) {
            this.guid = bean.getGuid();
            this.domain = bean.getDomain();
            this.longUrl = bean.getLongUrl();
            this.longUrlHash = bean.getLongUrlHash();
            this.preview = bean.getPreview();
            this.flag = bean.getFlag();
            this.rating = bean.getRating();
            this.note = bean.getNote();
            this.ratedTime = bean.getRatedTime();
            this.createdTime = bean.getCreatedTime();
            this.modifiedTime = bean.getModifiedTime();
        }
    }


    @XmlElement
    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    @XmlElement
    public String getDomain()
    {
        return this.domain;
    }
    public void setDomain(String domain)
    {
        this.domain = domain;
    }

    @XmlElement
    public String getLongUrl()
    {
        return this.longUrl;
    }
    public void setLongUrl(String longUrl)
    {
        this.longUrl = longUrl;
    }

    @XmlElement
    public String getLongUrlHash()
    {
        return this.longUrlHash;
    }
    public void setLongUrlHash(String longUrlHash)
    {
        this.longUrlHash = longUrlHash;
    }

    @XmlElement
    public String getPreview()
    {
        return this.preview;
    }
    public void setPreview(String preview)
    {
        this.preview = preview;
    }

    @XmlElement
    public String getFlag()
    {
        return this.flag;
    }
    public void setFlag(String flag)
    {
        this.flag = flag;
    }

    @XmlElement
    public Double getRating()
    {
        return this.rating;
    }
    public void setRating(Double rating)
    {
        this.rating = rating;
    }

    @XmlElement
    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    @XmlElement
    public Long getRatedTime()
    {
        return this.ratedTime;
    }
    public void setRatedTime(Long ratedTime)
    {
        this.ratedTime = ratedTime;
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("guid", this.guid);
        dataMap.put("domain", this.domain);
        dataMap.put("longUrl", this.longUrl);
        dataMap.put("longUrlHash", this.longUrlHash);
        dataMap.put("preview", this.preview);
        dataMap.put("flag", this.flag);
        dataMap.put("rating", this.rating);
        dataMap.put("note", this.note);
        dataMap.put("ratedTime", this.ratedTime);
        dataMap.put("createdTime", this.createdTime);
        dataMap.put("modifiedTime", this.modifiedTime);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = domain == null ? 0 : domain.hashCode();
        _hash = 31 * _hash + delta;
        delta = longUrl == null ? 0 : longUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = longUrlHash == null ? 0 : longUrlHash.hashCode();
        _hash = 31 * _hash + delta;
        delta = preview == null ? 0 : preview.hashCode();
        _hash = 31 * _hash + delta;
        delta = flag == null ? 0 : flag.hashCode();
        _hash = 31 * _hash + delta;
        delta = rating == null ? 0 : rating.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        delta = ratedTime == null ? 0 : ratedTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = createdTime == null ? 0 : createdTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static UrlRatingStub convertBeanToStub(UrlRating bean)
    {
        UrlRatingStub stub = null;
        if(bean instanceof UrlRatingStub) {
            stub = (UrlRatingStub) bean;
        } else {
            if(bean != null) {
                stub = new UrlRatingStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static UrlRatingStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of UrlRatingStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write UrlRatingStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write UrlRatingStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write UrlRatingStub object as a string.", e);
        }
        
        return null;
    }
    public static UrlRatingStub fromJsonString(String jsonStr)
    {
        try {
            UrlRatingStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, UrlRatingStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into UrlRatingStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into UrlRatingStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into UrlRatingStub object.", e);
        }
        
        return null;
    }

}
