package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.UserCustomDomain;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "userCustomDomains")
@XmlType(propOrder = {"userCustomDomain"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserCustomDomainListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UserCustomDomainListStub.class.getName());

    private List<UserCustomDomainStub> userCustomDomains = null;

    public UserCustomDomainListStub()
    {
        this(new ArrayList<UserCustomDomainStub>());
    }
    public UserCustomDomainListStub(List<UserCustomDomainStub> userCustomDomains)
    {
        this.userCustomDomains = userCustomDomains;
    }

    public boolean isEmpty()
    {
        if(userCustomDomains == null) {
            return true;
        } else {
            return userCustomDomains.isEmpty();
        }
    }
    public int getSize()
    {
        if(userCustomDomains == null) {
            return 0;
        } else {
            return userCustomDomains.size();
        }
    }

    @XmlElement(name = "userCustomDomain")
    public List<UserCustomDomainStub> getUserCustomDomain()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<UserCustomDomainStub> getList()
    {
        return userCustomDomains;
    }
    public void setList(List<UserCustomDomainStub> userCustomDomains)
    {
        this.userCustomDomains = userCustomDomains;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<UserCustomDomainStub> it = this.userCustomDomains.iterator();
        while(it.hasNext()) {
            UserCustomDomainStub userCustomDomain = it.next();
            sb.append(userCustomDomain.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static UserCustomDomainListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of UserCustomDomainListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write UserCustomDomainListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write UserCustomDomainListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write UserCustomDomainListStub object as a string.", e);
        }
        
        return null;
    }
    public static UserCustomDomainListStub fromJsonString(String jsonStr)
    {
        try {
            UserCustomDomainListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, UserCustomDomainListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into UserCustomDomainListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into UserCustomDomainListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into UserCustomDomainListStub object.", e);
        }
        
        return null;
    }

}
