package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.UserCustomDomain;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "userCustomDomain")
@XmlType(propOrder = {"guid", "owner", "domain", "verified", "status", "verifiedTime", "createdTime", "modifiedTime"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserCustomDomainStub implements UserCustomDomain, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UserCustomDomainStub.class.getName());

    private String guid;
    private String owner;
    private String domain;
    private Boolean verified;
    private String status;
    private Long verifiedTime;
    private Long createdTime;
    private Long modifiedTime;

    public UserCustomDomainStub()
    {
        this(null);
    }
    public UserCustomDomainStub(UserCustomDomain bean)
    {
        if(bean != null) {
            this.guid = bean.getGuid();
            this.owner = bean.getOwner();
            this.domain = bean.getDomain();
            this.verified = bean.isVerified();
            this.status = bean.getStatus();
            this.verifiedTime = bean.getVerifiedTime();
            this.createdTime = bean.getCreatedTime();
            this.modifiedTime = bean.getModifiedTime();
        }
    }


    @XmlElement
    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    @XmlElement
    public String getOwner()
    {
        return this.owner;
    }
    public void setOwner(String owner)
    {
        this.owner = owner;
    }

    @XmlElement
    public String getDomain()
    {
        return this.domain;
    }
    public void setDomain(String domain)
    {
        this.domain = domain;
    }

    @XmlElement
    public Boolean isVerified()
    {
        return this.verified;
    }
    public void setVerified(Boolean verified)
    {
        this.verified = verified;
    }

    @XmlElement
    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    @XmlElement
    public Long getVerifiedTime()
    {
        return this.verifiedTime;
    }
    public void setVerifiedTime(Long verifiedTime)
    {
        this.verifiedTime = verifiedTime;
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("guid", this.guid);
        dataMap.put("owner", this.owner);
        dataMap.put("domain", this.domain);
        dataMap.put("verified", this.verified);
        dataMap.put("status", this.status);
        dataMap.put("verifiedTime", this.verifiedTime);
        dataMap.put("createdTime", this.createdTime);
        dataMap.put("modifiedTime", this.modifiedTime);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = owner == null ? 0 : owner.hashCode();
        _hash = 31 * _hash + delta;
        delta = domain == null ? 0 : domain.hashCode();
        _hash = 31 * _hash + delta;
        delta = verified == null ? 0 : verified.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = verifiedTime == null ? 0 : verifiedTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = createdTime == null ? 0 : createdTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static UserCustomDomainStub convertBeanToStub(UserCustomDomain bean)
    {
        UserCustomDomainStub stub = null;
        if(bean instanceof UserCustomDomainStub) {
            stub = (UserCustomDomainStub) bean;
        } else {
            if(bean != null) {
                stub = new UserCustomDomainStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static UserCustomDomainStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of UserCustomDomainStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write UserCustomDomainStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write UserCustomDomainStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write UserCustomDomainStub object as a string.", e);
        }
        
        return null;
    }
    public static UserCustomDomainStub fromJsonString(String jsonStr)
    {
        try {
            UserCustomDomainStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, UserCustomDomainStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into UserCustomDomainStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into UserCustomDomainStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into UserCustomDomainStub object.", e);
        }
        
        return null;
    }

}
