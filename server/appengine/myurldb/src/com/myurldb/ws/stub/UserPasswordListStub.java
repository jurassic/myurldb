package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.UserPassword;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "userPasswords")
@XmlType(propOrder = {"userPassword"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserPasswordListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UserPasswordListStub.class.getName());

    private List<UserPasswordStub> userPasswords = null;

    public UserPasswordListStub()
    {
        this(new ArrayList<UserPasswordStub>());
    }
    public UserPasswordListStub(List<UserPasswordStub> userPasswords)
    {
        this.userPasswords = userPasswords;
    }

    public boolean isEmpty()
    {
        if(userPasswords == null) {
            return true;
        } else {
            return userPasswords.isEmpty();
        }
    }
    public int getSize()
    {
        if(userPasswords == null) {
            return 0;
        } else {
            return userPasswords.size();
        }
    }

    @XmlElement(name = "userPassword")
    public List<UserPasswordStub> getUserPassword()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<UserPasswordStub> getList()
    {
        return userPasswords;
    }
    public void setList(List<UserPasswordStub> userPasswords)
    {
        this.userPasswords = userPasswords;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<UserPasswordStub> it = this.userPasswords.iterator();
        while(it.hasNext()) {
            UserPasswordStub userPassword = it.next();
            sb.append(userPassword.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static UserPasswordListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of UserPasswordListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write UserPasswordListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write UserPasswordListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write UserPasswordListStub object as a string.", e);
        }
        
        return null;
    }
    public static UserPasswordListStub fromJsonString(String jsonStr)
    {
        try {
            UserPasswordListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, UserPasswordListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into UserPasswordListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into UserPasswordListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into UserPasswordListStub object.", e);
        }
        
        return null;
    }

}
