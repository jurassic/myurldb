package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.HashedPasswordStruct;
import com.myurldb.ws.UserPassword;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "userPassword")
@XmlType(propOrder = {"guid", "admin", "user", "username", "email", "openId", "passwordStub", "resetRequired", "challengeQuestion", "challengeAnswer", "status", "lastResetTime", "expirationTime", "createdTime", "modifiedTime"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserPasswordStub implements UserPassword, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UserPasswordStub.class.getName());

    private String guid;
    private String admin;
    private String user;
    private String username;
    private String email;
    private String openId;
    private HashedPasswordStructStub password;
    private Boolean resetRequired;
    private String challengeQuestion;
    private String challengeAnswer;
    private String status;
    private Long lastResetTime;
    private Long expirationTime;
    private Long createdTime;
    private Long modifiedTime;

    public UserPasswordStub()
    {
        this(null);
    }
    public UserPasswordStub(UserPassword bean)
    {
        if(bean != null) {
            this.guid = bean.getGuid();
            this.admin = bean.getAdmin();
            this.user = bean.getUser();
            this.username = bean.getUsername();
            this.email = bean.getEmail();
            this.openId = bean.getOpenId();
            this.password = HashedPasswordStructStub.convertBeanToStub(bean.getPassword());
            this.resetRequired = bean.isResetRequired();
            this.challengeQuestion = bean.getChallengeQuestion();
            this.challengeAnswer = bean.getChallengeAnswer();
            this.status = bean.getStatus();
            this.lastResetTime = bean.getLastResetTime();
            this.expirationTime = bean.getExpirationTime();
            this.createdTime = bean.getCreatedTime();
            this.modifiedTime = bean.getModifiedTime();
        }
    }


    @XmlElement
    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    @XmlElement
    public String getAdmin()
    {
        return this.admin;
    }
    public void setAdmin(String admin)
    {
        this.admin = admin;
    }

    @XmlElement
    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    @XmlElement
    public String getUsername()
    {
        return this.username;
    }
    public void setUsername(String username)
    {
        this.username = username;
    }

    @XmlElement
    public String getEmail()
    {
        return this.email;
    }
    public void setEmail(String email)
    {
        this.email = email;
    }

    @XmlElement
    public String getOpenId()
    {
        return this.openId;
    }
    public void setOpenId(String openId)
    {
        this.openId = openId;
    }

    @XmlElement(name = "password")
    @JsonIgnore
    public HashedPasswordStructStub getPasswordStub()
    {
        return this.password;
    }
    public void setPasswordStub(HashedPasswordStructStub password)
    {
        this.password = password;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=HashedPasswordStructStub.class)
    public HashedPasswordStruct getPassword()
    {  
        return getPasswordStub();
    }
    public void setPassword(HashedPasswordStruct password)
    {
        if((password == null) || (password instanceof HashedPasswordStructStub)) {
            setPasswordStub((HashedPasswordStructStub) password);
        } else {
            // TBD
            setPasswordStub(HashedPasswordStructStub.convertBeanToStub(password));
        }
    }

    @XmlElement
    public Boolean isResetRequired()
    {
        return this.resetRequired;
    }
    public void setResetRequired(Boolean resetRequired)
    {
        this.resetRequired = resetRequired;
    }

    @XmlElement
    public String getChallengeQuestion()
    {
        return this.challengeQuestion;
    }
    public void setChallengeQuestion(String challengeQuestion)
    {
        this.challengeQuestion = challengeQuestion;
    }

    @XmlElement
    public String getChallengeAnswer()
    {
        return this.challengeAnswer;
    }
    public void setChallengeAnswer(String challengeAnswer)
    {
        this.challengeAnswer = challengeAnswer;
    }

    @XmlElement
    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    @XmlElement
    public Long getLastResetTime()
    {
        return this.lastResetTime;
    }
    public void setLastResetTime(Long lastResetTime)
    {
        this.lastResetTime = lastResetTime;
    }

    @XmlElement
    public Long getExpirationTime()
    {
        return this.expirationTime;
    }
    public void setExpirationTime(Long expirationTime)
    {
        this.expirationTime = expirationTime;
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("guid", this.guid);
        dataMap.put("admin", this.admin);
        dataMap.put("user", this.user);
        dataMap.put("username", this.username);
        dataMap.put("email", this.email);
        dataMap.put("openId", this.openId);
        dataMap.put("password", this.password);
        dataMap.put("resetRequired", this.resetRequired);
        dataMap.put("challengeQuestion", this.challengeQuestion);
        dataMap.put("challengeAnswer", this.challengeAnswer);
        dataMap.put("status", this.status);
        dataMap.put("lastResetTime", this.lastResetTime);
        dataMap.put("expirationTime", this.expirationTime);
        dataMap.put("createdTime", this.createdTime);
        dataMap.put("modifiedTime", this.modifiedTime);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = admin == null ? 0 : admin.hashCode();
        _hash = 31 * _hash + delta;
        delta = user == null ? 0 : user.hashCode();
        _hash = 31 * _hash + delta;
        delta = username == null ? 0 : username.hashCode();
        _hash = 31 * _hash + delta;
        delta = email == null ? 0 : email.hashCode();
        _hash = 31 * _hash + delta;
        delta = openId == null ? 0 : openId.hashCode();
        _hash = 31 * _hash + delta;
        delta = password == null ? 0 : password.hashCode();
        _hash = 31 * _hash + delta;
        delta = resetRequired == null ? 0 : resetRequired.hashCode();
        _hash = 31 * _hash + delta;
        delta = challengeQuestion == null ? 0 : challengeQuestion.hashCode();
        _hash = 31 * _hash + delta;
        delta = challengeAnswer == null ? 0 : challengeAnswer.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = lastResetTime == null ? 0 : lastResetTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = expirationTime == null ? 0 : expirationTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = createdTime == null ? 0 : createdTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static UserPasswordStub convertBeanToStub(UserPassword bean)
    {
        UserPasswordStub stub = null;
        if(bean instanceof UserPasswordStub) {
            stub = (UserPasswordStub) bean;
        } else {
            if(bean != null) {
                stub = new UserPasswordStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static UserPasswordStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of UserPasswordStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write UserPasswordStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write UserPasswordStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write UserPasswordStub object as a string.", e);
        }
        
        return null;
    }
    public static UserPasswordStub fromJsonString(String jsonStr)
    {
        try {
            UserPasswordStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, UserPasswordStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into UserPasswordStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into UserPasswordStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into UserPasswordStub object.", e);
        }
        
        return null;
    }

}
