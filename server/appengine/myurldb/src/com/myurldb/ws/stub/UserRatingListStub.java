package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.UserRating;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "userRatings")
@XmlType(propOrder = {"userRating"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserRatingListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UserRatingListStub.class.getName());

    private List<UserRatingStub> userRatings = null;

    public UserRatingListStub()
    {
        this(new ArrayList<UserRatingStub>());
    }
    public UserRatingListStub(List<UserRatingStub> userRatings)
    {
        this.userRatings = userRatings;
    }

    public boolean isEmpty()
    {
        if(userRatings == null) {
            return true;
        } else {
            return userRatings.isEmpty();
        }
    }
    public int getSize()
    {
        if(userRatings == null) {
            return 0;
        } else {
            return userRatings.size();
        }
    }

    @XmlElement(name = "userRating")
    public List<UserRatingStub> getUserRating()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<UserRatingStub> getList()
    {
        return userRatings;
    }
    public void setList(List<UserRatingStub> userRatings)
    {
        this.userRatings = userRatings;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<UserRatingStub> it = this.userRatings.iterator();
        while(it.hasNext()) {
            UserRatingStub userRating = it.next();
            sb.append(userRating.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static UserRatingListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of UserRatingListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write UserRatingListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write UserRatingListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write UserRatingListStub object as a string.", e);
        }
        
        return null;
    }
    public static UserRatingListStub fromJsonString(String jsonStr)
    {
        try {
            UserRatingListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, UserRatingListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into UserRatingListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into UserRatingListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into UserRatingListStub object.", e);
        }
        
        return null;
    }

}
