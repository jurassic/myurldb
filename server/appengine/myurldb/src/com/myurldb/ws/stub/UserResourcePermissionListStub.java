package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.UserResourcePermission;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "userResourcePermissions")
@XmlType(propOrder = {"userResourcePermission"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserResourcePermissionListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UserResourcePermissionListStub.class.getName());

    private List<UserResourcePermissionStub> userResourcePermissions = null;

    public UserResourcePermissionListStub()
    {
        this(new ArrayList<UserResourcePermissionStub>());
    }
    public UserResourcePermissionListStub(List<UserResourcePermissionStub> userResourcePermissions)
    {
        this.userResourcePermissions = userResourcePermissions;
    }

    public boolean isEmpty()
    {
        if(userResourcePermissions == null) {
            return true;
        } else {
            return userResourcePermissions.isEmpty();
        }
    }
    public int getSize()
    {
        if(userResourcePermissions == null) {
            return 0;
        } else {
            return userResourcePermissions.size();
        }
    }

    @XmlElement(name = "userResourcePermission")
    public List<UserResourcePermissionStub> getUserResourcePermission()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<UserResourcePermissionStub> getList()
    {
        return userResourcePermissions;
    }
    public void setList(List<UserResourcePermissionStub> userResourcePermissions)
    {
        this.userResourcePermissions = userResourcePermissions;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<UserResourcePermissionStub> it = this.userResourcePermissions.iterator();
        while(it.hasNext()) {
            UserResourcePermissionStub userResourcePermission = it.next();
            sb.append(userResourcePermission.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static UserResourcePermissionListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of UserResourcePermissionListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write UserResourcePermissionListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write UserResourcePermissionListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write UserResourcePermissionListStub object as a string.", e);
        }
        
        return null;
    }
    public static UserResourcePermissionListStub fromJsonString(String jsonStr)
    {
        try {
            UserResourcePermissionListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, UserResourcePermissionListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into UserResourcePermissionListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into UserResourcePermissionListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into UserResourcePermissionListStub object.", e);
        }
        
        return null;
    }

}
