package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.UserResourceProhibition;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "userResourceProhibitions")
@XmlType(propOrder = {"userResourceProhibition"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserResourceProhibitionListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UserResourceProhibitionListStub.class.getName());

    private List<UserResourceProhibitionStub> userResourceProhibitions = null;

    public UserResourceProhibitionListStub()
    {
        this(new ArrayList<UserResourceProhibitionStub>());
    }
    public UserResourceProhibitionListStub(List<UserResourceProhibitionStub> userResourceProhibitions)
    {
        this.userResourceProhibitions = userResourceProhibitions;
    }

    public boolean isEmpty()
    {
        if(userResourceProhibitions == null) {
            return true;
        } else {
            return userResourceProhibitions.isEmpty();
        }
    }
    public int getSize()
    {
        if(userResourceProhibitions == null) {
            return 0;
        } else {
            return userResourceProhibitions.size();
        }
    }

    @XmlElement(name = "userResourceProhibition")
    public List<UserResourceProhibitionStub> getUserResourceProhibition()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<UserResourceProhibitionStub> getList()
    {
        return userResourceProhibitions;
    }
    public void setList(List<UserResourceProhibitionStub> userResourceProhibitions)
    {
        this.userResourceProhibitions = userResourceProhibitions;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<UserResourceProhibitionStub> it = this.userResourceProhibitions.iterator();
        while(it.hasNext()) {
            UserResourceProhibitionStub userResourceProhibition = it.next();
            sb.append(userResourceProhibition.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static UserResourceProhibitionListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of UserResourceProhibitionListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write UserResourceProhibitionListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write UserResourceProhibitionListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write UserResourceProhibitionListStub object as a string.", e);
        }
        
        return null;
    }
    public static UserResourceProhibitionListStub fromJsonString(String jsonStr)
    {
        try {
            UserResourceProhibitionListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, UserResourceProhibitionListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into UserResourceProhibitionListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into UserResourceProhibitionListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into UserResourceProhibitionListStub object.", e);
        }
        
        return null;
    }

}
