package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.UserResourceProhibition;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "userResourceProhibition")
@XmlType(propOrder = {"guid", "user", "permissionName", "resource", "instance", "action", "prohibited", "status", "createdTime", "modifiedTime"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserResourceProhibitionStub implements UserResourceProhibition, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UserResourceProhibitionStub.class.getName());

    private String guid;
    private String user;
    private String permissionName;
    private String resource;
    private String instance;
    private String action;
    private Boolean prohibited;
    private String status;
    private Long createdTime;
    private Long modifiedTime;

    public UserResourceProhibitionStub()
    {
        this(null);
    }
    public UserResourceProhibitionStub(UserResourceProhibition bean)
    {
        if(bean != null) {
            this.guid = bean.getGuid();
            this.user = bean.getUser();
            this.permissionName = bean.getPermissionName();
            this.resource = bean.getResource();
            this.instance = bean.getInstance();
            this.action = bean.getAction();
            this.prohibited = bean.isProhibited();
            this.status = bean.getStatus();
            this.createdTime = bean.getCreatedTime();
            this.modifiedTime = bean.getModifiedTime();
        }
    }


    @XmlElement
    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    @XmlElement
    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    @XmlElement
    public String getPermissionName()
    {
        return this.permissionName;
    }
    public void setPermissionName(String permissionName)
    {
        this.permissionName = permissionName;
    }

    @XmlElement
    public String getResource()
    {
        return this.resource;
    }
    public void setResource(String resource)
    {
        this.resource = resource;
    }

    @XmlElement
    public String getInstance()
    {
        return this.instance;
    }
    public void setInstance(String instance)
    {
        this.instance = instance;
    }

    @XmlElement
    public String getAction()
    {
        return this.action;
    }
    public void setAction(String action)
    {
        this.action = action;
    }

    @XmlElement
    public Boolean isProhibited()
    {
        return this.prohibited;
    }
    public void setProhibited(Boolean prohibited)
    {
        this.prohibited = prohibited;
    }

    @XmlElement
    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("guid", this.guid);
        dataMap.put("user", this.user);
        dataMap.put("permissionName", this.permissionName);
        dataMap.put("resource", this.resource);
        dataMap.put("instance", this.instance);
        dataMap.put("action", this.action);
        dataMap.put("prohibited", this.prohibited);
        dataMap.put("status", this.status);
        dataMap.put("createdTime", this.createdTime);
        dataMap.put("modifiedTime", this.modifiedTime);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = user == null ? 0 : user.hashCode();
        _hash = 31 * _hash + delta;
        delta = permissionName == null ? 0 : permissionName.hashCode();
        _hash = 31 * _hash + delta;
        delta = resource == null ? 0 : resource.hashCode();
        _hash = 31 * _hash + delta;
        delta = instance == null ? 0 : instance.hashCode();
        _hash = 31 * _hash + delta;
        delta = action == null ? 0 : action.hashCode();
        _hash = 31 * _hash + delta;
        delta = prohibited == null ? 0 : prohibited.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = createdTime == null ? 0 : createdTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static UserResourceProhibitionStub convertBeanToStub(UserResourceProhibition bean)
    {
        UserResourceProhibitionStub stub = null;
        if(bean instanceof UserResourceProhibitionStub) {
            stub = (UserResourceProhibitionStub) bean;
        } else {
            if(bean != null) {
                stub = new UserResourceProhibitionStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static UserResourceProhibitionStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of UserResourceProhibitionStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write UserResourceProhibitionStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write UserResourceProhibitionStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write UserResourceProhibitionStub object as a string.", e);
        }
        
        return null;
    }
    public static UserResourceProhibitionStub fromJsonString(String jsonStr)
    {
        try {
            UserResourceProhibitionStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, UserResourceProhibitionStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into UserResourceProhibitionStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into UserResourceProhibitionStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into UserResourceProhibitionStub object.", e);
        }
        
        return null;
    }

}
