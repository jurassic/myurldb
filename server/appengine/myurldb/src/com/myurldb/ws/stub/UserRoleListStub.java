package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.UserRole;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "userRoles")
@XmlType(propOrder = {"userRole"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserRoleListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UserRoleListStub.class.getName());

    private List<UserRoleStub> userRoles = null;

    public UserRoleListStub()
    {
        this(new ArrayList<UserRoleStub>());
    }
    public UserRoleListStub(List<UserRoleStub> userRoles)
    {
        this.userRoles = userRoles;
    }

    public boolean isEmpty()
    {
        if(userRoles == null) {
            return true;
        } else {
            return userRoles.isEmpty();
        }
    }
    public int getSize()
    {
        if(userRoles == null) {
            return 0;
        } else {
            return userRoles.size();
        }
    }

    @XmlElement(name = "userRole")
    public List<UserRoleStub> getUserRole()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<UserRoleStub> getList()
    {
        return userRoles;
    }
    public void setList(List<UserRoleStub> userRoles)
    {
        this.userRoles = userRoles;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<UserRoleStub> it = this.userRoles.iterator();
        while(it.hasNext()) {
            UserRoleStub userRole = it.next();
            sb.append(userRole.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static UserRoleListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of UserRoleListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write UserRoleListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write UserRoleListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write UserRoleListStub object as a string.", e);
        }
        
        return null;
    }
    public static UserRoleListStub fromJsonString(String jsonStr)
    {
        try {
            UserRoleListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, UserRoleListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into UserRoleListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into UserRoleListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into UserRoleListStub object.", e);
        }
        
        return null;
    }

}
