package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.UserSetting;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "userSettings")
@XmlType(propOrder = {"userSetting"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserSettingListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UserSettingListStub.class.getName());

    private List<UserSettingStub> userSettings = null;

    public UserSettingListStub()
    {
        this(new ArrayList<UserSettingStub>());
    }
    public UserSettingListStub(List<UserSettingStub> userSettings)
    {
        this.userSettings = userSettings;
    }

    public boolean isEmpty()
    {
        if(userSettings == null) {
            return true;
        } else {
            return userSettings.isEmpty();
        }
    }
    public int getSize()
    {
        if(userSettings == null) {
            return 0;
        } else {
            return userSettings.size();
        }
    }

    @XmlElement(name = "userSetting")
    public List<UserSettingStub> getUserSetting()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<UserSettingStub> getList()
    {
        return userSettings;
    }
    public void setList(List<UserSettingStub> userSettings)
    {
        this.userSettings = userSettings;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<UserSettingStub> it = this.userSettings.iterator();
        while(it.hasNext()) {
            UserSettingStub userSetting = it.next();
            sb.append(userSetting.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static UserSettingListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of UserSettingListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write UserSettingListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write UserSettingListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write UserSettingListStub object as a string.", e);
        }
        
        return null;
    }
    public static UserSettingListStub fromJsonString(String jsonStr)
    {
        try {
            UserSettingListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, UserSettingListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into UserSettingListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into UserSettingListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into UserSettingListStub object.", e);
        }
        
        return null;
    }

}
